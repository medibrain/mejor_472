﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Mejor.KyotofuNokyo
{
   
    /// <summary>
    /// 2019年度用フォーマット出力class furukawa 20190122091924
    /// </summary>
    class ExportNew
    {

        #region 宣言部
        

        App app { get; set; }//applicationテーブル格納用クラス変数
        int honkeKubun { get; set; }//本家区分  //5 本家区分 数値 1 ●

        //int chargeYM { get; set; }//処理月
        string chargeYM { get; set; }//処理月
   
        APP_TYPE appType { get; set; }//給種区分
        string mediYM { get; set; }//診療年月
        string syo_mark { get; set; }//被保険者証記号
        string syo_number { get; set; }//被保険者証番号


        DateTime birth { get; set; }//生年月日
        string birthforOutput { get; set; }//21.生年月日出力用


        int sex { get; set; }//性別

        DateTime startDate { get; set; }//27 施術開始日 数値 8 
        string startDateforOutput { get; set; }//27 施術開始日 数値 8 

        int days { get; set; }
        int total { get; set; }
        int charge { get; set; }//金額
        int partial { get; set; }//一部負担金
        string receNo { get; set; }//16.レセプト番号
        int henrei { get; set; }//
        int jyushinKubun { get; set; }//受信区分
        int numbering { get; set; }//ナンバリング

        //2019/01/11古川
        //新規項目の追加
           
        //int NoPerYM{get;set;}=0;                                                    //2.内部連番
        string NaibuRenban2 { get; set; } = string.Empty;                           //2.内部連番

        string SiharaisakiCode{get;set;}=string.Empty;                              //18.支払先コード

        string KumiaiCode{get;set;}=string.Empty;                                   //3.組合コード
        string HonSibuCode {get;set;}=string.Empty;                                 //4 本支部コード 数値 2 △

        

        string SeikyukeitaiKBN {get;set;}=string.Empty;                             //6 請求形態区分 数値 1 ●
        string SeikyuKeitaiBango{get;set;}=string.Empty;							//7.請求時形態番号     
        string SeikyuNengetu { get; set; } = string.Empty;                          //8.請求年月
        string FukenCode {get;set;}=string.Empty;                                   //9 府県コード 数値 2 △

        int KyusyuKBN {get;set;}                                   //10 給種区分 数値 1 ● ->下に宣言しているが、別の用途なので別宣言
        string SinryoKBN {get;set;}=string.Empty;                                   //11 診療区分 数値 1 ●

        string Bango { get; set; } = string.Empty;                                  //12.番号　各処理月毎に発番　　⇒　１２．番号（内部連番）はNO2の上１桁目に１を追加してください。


        string SeikyujiTorimatomeKbn {get;set;}="1";                       //13.請求時取りまとめ区分 1:団体 2:個人 ⇒　両方ともに「１：団体」を設定してください。
        string SiharaijiTorimatomeKbn {get;set;}="1";                       //14.支払時取りまとめ区分 1:団体 2:個人   ⇒　両方ともに「１：団体」を設定してください。

        //int NoPerRow{get;set;}=0;                                                   //15.内部連番
        string NaibuRenban15 { get; set; } = string.Empty;                            //15.内部連番　1行ごとの連番

        string sickCode1 = string.Empty;  //23 疾病コード1 数値 4 
        string sickCode2 = string.Empty;  //24 疾病コード2 数値 4 
        string sickCode3 = string.Empty;  //25 疾病コード3 数値 4 
        string sickCode4 = string.Empty;  //26 疾病コード4 数値 4 


        DateTime endDate { get; set; }                                              //28 施術終了日 数値 8 
        string strEndDate { get; set; } = string.Empty;//日付型だが、文字型にしないとオール０が入らない

        string dtDoui = string.Empty;                                               //29 同意年月日 数値 8 
        string dtUketuke = string.Empty;                                            //30 受付年月日 数値 8 

        string ItibuFutanKin = string.Empty;                                        //33 一部負担金 数値 8 
        string SejutuKaisu = string.Empty;                                          //34 施術回数 数値 3 
        string SeikyuGaku = string.Empty;                                           //35 請求額 数値 8   
        
        string KohiCode1 {get;set;}=string.Empty;                                   //36 公費コード1 数値 2 △
        string KohiCode2 {get;set;}=string.Empty;                                   //37 公費コード2 数値 2 △
        string KohiKingaku1 {get;set;}=string.Empty;                                //38 公費金額1 数値 9 △
        string KohiKingaku2 {get;set;}=string.Empty;                                //39 公費金額2 数値 9 △
        string Kanjafutangaku_Kohi1 {get;set;}=string.Empty;                        //40 患者負担額公費1 数値 8 △
        string Kanjafutangaku_Kohi2 {get;set;}=string.Empty;                        //41 患者負担額公費2 数値 8 △
        string MaruFuKBN {get;set;}=string.Empty;                                   //42 マル不区分 数値 1 △
        string TenkiKBN {get;set;}=string.Empty;                                    //43 転帰区分 数値 1 △

        string HonkeSyubetuCode { get; set; } = string.Empty;                       //44. 本家種別コード 数値 1 ●

        string KozaNo {get;set;}=string.Empty;                                      //45 口座番号 数値 8 △
        string KozaSyubetu {get;set; }=string.Empty;                                //46 口座種別 数値 1 △

        string DrName{get;set;}=string.Empty;										//47 柔整師名 文字 24 △  柔整レセ：レセプト本家種別コード ※区分の内訳は区分一覧参照

        string BankName{get;set;}=string.Empty;           		                    //48 銀行名 文字 20 －  柔整レセ：口座番号　※口座種別とセット　※個人払で支払先コードが未設定の場合は必須
        string BankBranch{get;set;}=string.Empty;                                   //49 支店名 文字 20 －  柔整レセ：口座種別　※口座番号とセット ※区分の内訳は区分一覧参照　※個人払で支払先コードが未設定の場合は必須
        string BankAccountName{get;set;}=string.Empty;                              //50 口座名義人 文字 24 －  柔整レセ：柔整師名（漢字）　※全角12文字以内


        //ソート
        string SortKeyNaibuRenban2 { get; set; } = string.Empty ;
        string SortKeyData { get; set; } = string.Empty;



        List<string> imagePahts = new List<string>();

        int changedJyushinKubun => jyushinKubun == 8 ? 1 :
            jyushinKubun == 0 ? 6 :
            jyushinKubun == 4 ? 5 : 0;

        int kyushuKubun => appType == APP_TYPE.あんま ? 4 :
            appType == APP_TYPE.鍼灸 ? 5 : 3;


        #endregion


        /// <summary>
        /// データ出力2019年以降
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <returns></returns>
        public static bool DoExportNew(int cym)
        {
            string log = string.Empty;
            string fileName;
            string strDIR_p;
            string strDIR_image;

            using (var f = new SaveFileDialog())
            {

                f.FileName = $"KJKN.DATA";

                //フィルタ文字列が逆だった
                f.Filter = "DATAファイル|KJKN.DATA";
                //f.Filter = "KJKN.DATA|DATAファイル";

                //f.FileName = $"REORIGIN.TXT";
                //f.Filter = "REORIGIN.TXT|txtファイル";

                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;

                //仕様どおりのフォルダ構成
                strDIR_p = System.IO.Path.GetDirectoryName(fileName) + "\\PunchDT";
                strDIR_image = System.IO.Path.GetDirectoryName(fileName);

                if (!System.IO.Directory.Exists(strDIR_p)) System.IO.Directory.CreateDirectory(strDIR_p);
                fileName = strDIR_p + "\\" + System.IO.Path.GetFileName(fileName);

            }
                                  

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データを取得しています");
                var list = App.GetApps(cym);
                list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                //16.レセプト番号
                //2019-01-17関西情報センター中山様より　NO１６（レセプト番号）　：「７００００１」から連番でお願いします。
                int rezeptNumber = 700000;
                //int number = 100000;

                //15.内部連番　用変数　1レコードごとの連番7桁
                int tmpNaibuRenban15 = 0;

                
                string tmpKeyNaibuRenban2curr = string.Empty;
                string tmpKeyNaibuRenban2 = string.Empty;

                var honJyu = new List<ExportNew>();
                var honHari = new List<ExportNew>();
                var honAnma = new List<ExportNew>();
                var kazokuJyu = new List<ExportNew>();
                var kazokuHari = new List<ExportNew>();
                var kazokuAnma = new List<ExportNew>();
                var all = new List<ExportNew>();

                for (int i = 0; i < list.Count; i++)
                {
                    
                    var item = list[i];
                    if (item.MediYear <= 0) continue;

                    //出力用クラス
                    var ExportNew = new ExportNew(item,cym,ref rezeptNumber,ref tmpNaibuRenban15);


                    //2.内部連番付与用ソート
                    //組合コード 
                    //本支部コード 
                    //本家区分 
                    //請求形態区分 
                    //請求形態番号 
                    //請求年月 
                    //府県コード 
                    //給種区分 
                    //診療区分 
                    //レセプト番号 

                    ExportNew.SortKeyNaibuRenban2 = ExportNew.KumiaiCode + ExportNew.HonSibuCode +
                        ExportNew.honkeKubun.ToString() + ExportNew.SeikyukeitaiKBN +
                        ExportNew.SeikyuKeitaiBango + ExportNew.chargeYM +
                        ExportNew.FukenCode + ExportNew.KyusyuKBN + ExportNew.SinryoKBN;

                    //データソート用キー
                    ExportNew.SortKeyData = ExportNew.KumiaiCode + ExportNew.HonSibuCode +
                       ExportNew.honkeKubun.ToString() + ExportNew.SeikyukeitaiKBN +
                       ExportNew.SeikyuKeitaiBango + ExportNew.chargeYM +
                       ExportNew.FukenCode + ExportNew.KyusyuKBN + ExportNew.SinryoKBN + ExportNew.numbering.ToString("000000");

                    System.Diagnostics.Debug.Print(ExportNew.SortKeyData);



                    //続紙追加
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        
                        if (list[j].MediYear >= 0) break;

                        if (list[j].MediYear != (int)APP_SPECIAL_CODE.続紙) continue;
                        ExportNew.imagePahts.Add(list[j].GetImageFullPath());
                    }
                    
                    all.Add(ExportNew);

                    /*
                    //柔整・はり・あんまごと、本人・家族ごと、6パターンに分類
                    if (ExportNew.honkeKubun == 1)
                    {
                        if (item.AppType == APP_TYPE.鍼灸) honHari.Add(ExportNew);
                        else if (item.AppType == APP_TYPE.あんま) honAnma.Add(ExportNew);
                        else honJyu.Add(ExportNew);
                    }
                    else
                    {
                        if (item.AppType == APP_TYPE.鍼灸) kazokuHari.Add(ExportNew);
                        else if (item.AppType == APP_TYPE.あんま) kazokuHari.Add(ExportNew);
                        else kazokuJyu.Add(ExportNew);
                    }
                    */
                    
                }



                //2.内部連番用
                int tmpNaibuRenban2 = 1;
                                
                //2.内部連番付与用ソート
                all.Sort((x, y) => (int)x.SortKeyNaibuRenban2.CompareTo(y.SortKeyNaibuRenban2));
                for(int r = 0; r < all.Count; r++)
                {
                    //キーが違ったら内部連番足す
                    if (r > 0 && (all[r].SortKeyNaibuRenban2 != all[r-1].SortKeyNaibuRenban2))

                    {
                        tmpNaibuRenban2++;
                    }

                    all[r].NaibuRenban2 = tmpNaibuRenban2.ToString("00000");    //2 '内部連番 数値 5  
                    all[r].Bango = "1" + all[r].NaibuRenban2;                   //12 番号 数値 6
                }

                //出力データ用最終ソート
                all.Sort((x, y) => (int)x.SortKeyData.CompareTo(y.SortKeyData));
                                    


                wf.LogPrint("データを出力しています");
                //データ出力

                //20190121145141 furukawa st ////////////////////////
                //文字コードを仕様どおりshift-jisに
                //using (var sw = new StreamWriter(fileName, false, Encoding.ASCII))
                using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("shift-jis")))
                //20190121145141 furukawa ed ////////////////////////
                {

                    if (!writeTsuduriLine(sw, all, 10001)) return false;
                    /*
                    if (!writeTsuduriLine(sw, honJyu, 10001)) return false;
                    if (!writeTsuduriLine(sw, kazokuJyu, 10002)) return false;
                    if (!writeTsuduriLine(sw, honAnma, 10003)) return false;
                    if (!writeTsuduriLine(sw, kazokuAnma, 10004)) return false;
                    if (!writeTsuduriLine(sw, honHari, 10005)) return false;
                    if (!writeTsuduriLine(sw, kazokuHari, 10006)) return false;*/
                }

                //集計出力
                var dir = $"{Path.GetDirectoryName(fileName)}";
                dir = strDIR_image;
                //20190122161813 furukawa st ////////////////////////
                //集計表は不要　仕様どおり

                // ExportNewSumCsv(cym, dir + "\\集計.csv", all);
                //20190122161813 furukawa ed ////////////////////////




                wf.LogPrint("画像を出力しています");
                //画像出力
                var fc = new TiffUtility.FastCopy();

                //20190122161858 furukawa st ////////////////////////
                //画像用フォルダ
                
                dir += $"\\{list[0].CYM.ToString("000000")+"01"}";
                //dir += $"\\{list[0].CYM.ToString("000000")}";
                //20190122161858 furukawa ed ////////////////////////


                Directory.CreateDirectory(dir);
                dir += "\\";

                wf.SetMax(all.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                foreach (var item in all)
                {
                    TiffUtility.MargeOrCopyTiff(fc, item.imagePahts, dir + item.numbering.ToString("000000") + ".TIF");
                    wf.InvokeValue++;
                }
            }

            return true;
        }


        #region データ作成
        
        

        //データ作成　
        private ExportNew(App app, int cym,ref int rezeptNumber,ref int tmpNaibuRenban15 )
            //private ExportNew(App app)
        {
            this.app = app;

            
            //2019/01/11　古川
            //足りない項目の追加
            //新フォーマットは綴り表がない代わりにレコードが長くなる　という想定!

            chargeYM = cym.ToString() + "01"; //app.ChargeYear.ToString() + app.ChargeMonth.ToString("00")+"01";      //1 処理月 数値 8   ※ yyyymm01　形式で格納

            //2 '内部連番 数値 5   各処理月毎に発番して下さい　
               //⇒　２．内部連番（綴票内部連番）NO３（組合コード）～NO１１（診療区分）単位に発番します。
               //全体レコードソート後につけることにした

            KumiaiCode = "71527";                                                           //3 組合コード 数値 5   柔整 ：組合コード　※84283固定にして下さい //関西情報センター中山様より「７１５２７」固定でお願いします。

            //2019/01/25関西情報センター中山様より
            //HonSibuCode = "00";                                                             //4 本支部コード 数値 2 △  柔整 ：※0固定にして下さい
            HonSibuCode = "";                                                             //4 本支部コード 数値 2 △  柔整 ：※0固定にして下さい


            honkeKubun = app.Family / 100;                                                //5 本家区分 数値 1   柔整 ：レセプト本家種別コード ※区分の内訳は区分一覧参照
            //honkeKubun = app.Family == 102 ? 1 : 2;                                            //5 本家区分 数値 1   柔整 ：レセプト本家種別コード ※区分の内訳は区分一覧参照
            SeikyukeitaiKBN = "4";                                                          //6 請求形態区分 数値 1   柔整 ：請求形態区分　※4固定にして下さい                      



            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない
            SeikyuKeitaiBango = "";                                                     //7 請求形態番号 数値 4 △  柔整 ：請求形態番号　※団体のコードを設定して下さい
                                                                                        //SeikyuKeitaiBango = "0000";                                                     //7 請求形態番号 数値 4 △  柔整 ：請求形態番号　※団体のコードを設定して下さい
                                                                                        //※支払時取纏区分が団体(1)の場合は必須。個人(2)の場合は入力不要

            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない

            //柔整では請求年月は基本的に省略しているので、こちらも継承する            
            //chargeYM = app.ChargeYear.ToString() + app.ChargeMonth.ToString();                //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　※省略時は取込にて自動設定します




            //20190305145255 furukawa st ////////////////////////
            //請求年月は処理年月と同じ値
                                                        //※2019/03/04 省略不可になった //関西情報センター中山様より
            SeikyuNengetu = chargeYM;                   //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　
            //SeikyuNengetu = "";                       //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　※省略時は取込にて自動設定します
            //SeikyuNengetu = "00000000";               //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　※省略時は取込にて自動設定します

            //20190305145255 furukawa ed ////////////////////////


            //関西情報センター中山様より
            //FukenCode = app.HihoPref.ToString("00");                                         //9 府県コード 数値 2 △  柔整 ：府県コード　※省略時は'0'を設定して下さい
            //府県コードは施術所の所在地による。京都の施術所なら26、大阪の施術所なら27と申請書ごとに

            //FukenCode = app.DrNum == string.Empty ? "00" : app.DrNum.Substring(1,2);         //柔整師登録番号の2,3桁目(0始まりなので1と2)
            FukenCode = app.DrNum == string.Empty ? "00" : app.DrNum.Trim();         //柔整師登録番号の2,3桁目(0始まりなので1と2)


            //10 給種区分 数値 1   柔整 ：給種区分　※3=柔整　4=按摩　5=針灸
            switch (app.AppType)
            {
                case APP_TYPE.あんま:
                    KyusyuKBN = 4;
                    break;

                case APP_TYPE.鍼灸:
                    KyusyuKBN = 5;
                    break;

                default:
                    KyusyuKBN = 3;
                    break;
            }
                                   

            SinryoKBN = "2"; //診療区分                                                     //11 診療区分 数値 1   柔整 ：診療区分　※2固定にして下さい
                                                                  
            //12 番号 数値 6   柔整 ：各処理月毎に発番して下さい　⇒　１２．番号（内部連番）はNO2の上１桁目に１を追加してください。
            //全体レコードソート後につけることにした

            SiharaijiTorimatomeKbn = SiharaijiTorimatomeKbn.ToString();                 //13 請求時取纏区分 数値 1   柔整 ：請求時取纏区分　※1=団体　2=個人　⇒　両方ともに「１：団体」を設定してください。
            SeikyujiTorimatomeKbn = SeikyujiTorimatomeKbn.ToString();                   //14 支払時取纏区分 数値 1   柔整 ：支払時取纏区分　※1=団体　2=個人⇒　両方ともに「１：団体」を設定してください。
           
            tmpNaibuRenban15++;
            NaibuRenban15 = tmpNaibuRenban15.ToString().PadLeft(7, '0');                    //15 内部連番 数値 7   パンチデータファイル毎に1から発番して下さい

            rezeptNumber++;                                                                 //16 レセプト番号 数値 6   柔整レセ：レセプト番号 → NO１６（レセプト番号）　：「７００００１」から連番でお願いします。            
            numbering = rezeptNumber;
           
            mediYM = app.YM.ToString()+"01";                                                //17 診療年月 数値 8   柔整レセ：診療年月　※ yyyymm01 形式で格納            



            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない
            SiharaisakiCode = "";                                                    //18 支払先コード 数値 7 △  柔整レセ：柔整師コード　※個人払(支払時取纏区分=2)の場合、補筆された7桁の数字をパンチ
            //SiharaisakiCode = "0000000";                                                    //18 支払先コード 数値 7 △  柔整レセ：柔整師コード　※個人払(支払時取纏区分=2)の場合、補筆された7桁の数字をパンチ
                                                                                                //支払時取り纏め区分が１固定のため、こちらも省略

            syo_mark = app.HihoNum.Split('-')[0];                                           //19 記号 数値 5   柔整レセ：記号
            syo_number = app.HihoNum.Split('-')[1];                                         //20 番号 数値 8   柔整レセ：番号

            birth = app.Birthday;                                                           //21 生年月日 数値 8   柔整レセ：生年月日　※ yyyymmdd 形式で格納　月日ブランクは0000            
            sex = app.Sex;                                                                  //22 性別 数値 1   柔整レセ：性別


            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない
            //疾病コード全省略
            sickCode1 = ""; //23 疾病コード1 数値 4 －  柔整レセ：疾病コード（１）
            sickCode2 = ""; //24 疾病コード2 数値 4 －  柔整レセ：疾病コード（２）
            sickCode3 = ""; //25 疾病コード3 数値 4 －  柔整レセ：疾病コード（３）
            sickCode4 = ""; //26 疾病コード4 数値 4 －  柔整レセ：疾病コード（４）
  
            /*        
            sickCode1 = "0000"; //23 疾病コード1 数値 4 －  柔整レセ：疾病コード（１）
            sickCode2 = "0000"; //24 疾病コード2 数値 4 －  柔整レセ：疾病コード（２）
            sickCode3 = "0000"; //25 疾病コード3 数値 4 －  柔整レセ：疾病コード（３）
            sickCode4 = "0000"; //26 疾病コード4 数値 4 －  柔整レセ：疾病コード（４）
            */

            

            startDate = app.FushoStartDate1;   //診療開始日                                 //27 施術開始日 数値 8   柔整レセ：施術開始日　※ yyyymmdd 形式で格納


            //endDate =  app.FushoFinishDate1;                                                 //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納
            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない
            strEndDate = "";
            //strEndDate = "00000000";


            //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない

            //省略
            dtDoui = "";            //29 同意年月日 数値 8 －  柔整レセ：同意年月日　※ yyyymmdd 形式で格納
            dtUketuke = "";         //30 受付年月日 数値 8 －  柔整レセ：35 未定義　※ yyyymmdd 形式で格納
            
            /*
            dtDoui = "00000000";            //29 同意年月日 数値 8 －  柔整レセ：同意年月日　※ yyyymmdd 形式で格納
            dtUketuke = "00000000";         //30 受付年月日 数値 8 －  柔整レセ：35 未定義　※ yyyymmdd 形式で格納
            */

            days = app.CountedDays;                                                         //31 施術日数 数値 3   柔整レセ：施術日数
            charge = app.Charge;                                                            //32 合計金額 数値 9   柔整レセ：合計金額




            //省略
            ItibuFutanKin = string.Empty;              //33 一部負担金 数値 8 －  柔整レセ：一部負担金
            SejutuKaisu = string.Empty;                //34 施術回数 数値 3 －  柔整レセ：施術回数
            SeikyuGaku = string.Empty;                 //35 請求額 数値 8 －  柔整レセ：請求額
            KohiCode1 = string.Empty;		           //36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
            KohiCode2 = string.Empty;                  //37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
            KohiKingaku1 = string.Empty;               //38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
            KohiKingaku2 = string.Empty;               //39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
            Kanjafutangaku_Kohi1 = string.Empty;       //40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
            Kanjafutangaku_Kohi2 = string.Empty;       //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
            MaruFuKBN = string.Empty;                  //42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
            TenkiKBN = string.Empty;                   //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止

            /*
            ItibuFutanKin = "00000000";                        //33 一部負担金 数値 8 －  柔整レセ：一部負担金
            SejutuKaisu = "000";                               //34 施術回数 数値 3 －  柔整レセ：施術回数
            SeikyuGaku = "00000000";                           //35 請求額 数値 8 －  柔整レセ：請求額
            KohiCode1 = string.Empty;					        //36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
            KohiCode2 = string.Empty;                          //37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
            KohiKingaku1 = "00";                               //38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
            KohiKingaku2 = "00";                               //39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
            Kanjafutangaku_Kohi1 = "00000000";                 //40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
            Kanjafutangaku_Kohi2 = "00000000";                 //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
            MaruFuKBN = string.Empty;                          //42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
            TenkiKBN = string.Empty;                           //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止
            */

            HonkeSyubetuCode = (app.Family % 100).ToString();                      //44 本家種別コード 数値 1                  
            //honkeKubun = app.Family / 100;                      //44 本家種別コード 数値 1 

            KozaNo = app.AccountNumber;                         //45 口座番号 数値 8 △  


            KozaSyubetu = app.BankType==0 ? "1" : app.BankType.ToString();                         //46 口座種別 数値 1 △    
            
            DrName = app.DrName;                                //47 柔整師名 文字 24 △ 
            
            //省略
            BankName = app.BankName;                            //48 銀行名 文字 20 － 
            BankBranch = app.BankBranch;                        //49 支店名 文字 20 －  
            BankAccountName = app.AccountName;                  //50 口座名義人 文字 24


            //元のコード
            /*   chargeYM = app.CYM;                                     //請求年月
               appType = app.AppType;                                  //あんま、鍼、柔性の区分                       
               days = app.CountedDays;                                 //診療実日数
               total = app.Total;                                      //決定点数            
               partial = app.Partial;                                  //一部負担金
               jyushinKubun = app.Family % 100;                        //受診者区分*/

            imagePahts.Add(app.GetImageFullPath());                 //画像パス
               
        }

        private static bool writeTsuduriLine(StreamWriter sw,List<ExportNew>l, int tsuduriNo)
        {
            if (l.Count == 0) return true;

            try
            {

                //2019年より綴表なし

                /*                var sb = new StringBuilder();

                                sb.Append("7412*");
                                sb.Append("71527");
                                sb.Append("00");
                                sb.Append(l[0].honkeKubun);
                                sb.Append("40000");
                                sb.Append(getGyymmFromAdYM(l[0].chargeYM));
                                sb.Append(new string('0', 2));
                                sb.Append(l[0].kyushuKubun);
                                sb.Append("2");
                                sb.Append(tsuduriNo.ToString("000000"));
                                sb.Append(l.Count.ToString("000000"));
                                sb.Append(new string('0', 40));
                                sw.WriteLine(sb.ToString());
                                */

                foreach (var item in l)
                {
                    if (!item.writeReceLine(sw)) return false;
                }                
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
        #endregion

        #region テキスト書込
        private bool writeReceLine(StreamWriter sw)
        {
            try
            {
                var sb = new StringBuilder();

                
                //元のコード
                /*
                sb.Append(getGyymmFromAdYM(mediYM));
                sb.Append(new string('0', 10)); //6～15
                sb.Append(mark.PadLeft(4, '0'));
                sb.Append(number.PadLeft(8, '0'));
                sb.Append(getIntJpDateWithEraNumber(birth));
                sb.Append(sex);
                sb.Append(new string('0', 4)); //36～39
                sb.Append(getIntJpDateWithEraNumber(startDate));
                sb.Append(days.ToString().PadLeft(3, '0'));
                sb.Append(total.ToString().PadLeft(7, '0'));
                sb.Append(numbering.ToString().PadLeft(6, '0'));
                sb.Append(new string('0', 80)); //63～142
                sb.Append(changedJyushinKubun);
                */


                //2019年版

                sb.Append(chargeYM + "," );                 //1 処理月 数値 8   ※ yyyymm01　形式で格納                
                sb.Append(NaibuRenban2 + ",");					//2 '内部連番 数値 5   各処理月毎に発番して下さい
                sb.Append(KumiaiCode  + "," );					//3 組合コード 数値 5   柔整 ：組合コード　※84283固定にして下さい
                sb.Append(HonSibuCode + "," );					//4 本支部コード 数値 2 △  柔整 ：※0固定にして下さい
                sb.Append(honkeKubun  + "," );					//5 本家区分 数値 1   柔整 ：レセプト本家種別コード ※区分の内訳は区分一覧参照
                sb.Append(SeikyukeitaiKBN + "," );				//6 請求形態区分 数値 1   柔整 ：請求形態区分　※4固定にして下さい
                sb.Append(SeikyuKeitaiBango + "," );            //7 請求形態番号 数値 4 △  柔整 ：請求形態番号　※団体のコードを設定して下さい　※支払時取纏区分が団体(1)の場合は必須。個人(2)の場合は入力不要                        
                sb.Append(SeikyuNengetu + ",");					//8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　※省略時は取込にて自動設定します
                sb.Append(FukenCode + "," );                    //9 府県コード 数値 2 △  柔整 ：府県コード　※省略時は'0'を設定して下さい
                sb.Append(KyusyuKBN + ",");		    			//10 給種区分 数値 1   柔整 ：給種区分　※3=柔整　4=按摩　5=針灸
                sb.Append(SinryoKBN + "," );                    //11 診療区分 数値 1   柔整 ：診療区分　※2固定にして下さい                
                sb.Append(Bango + ",");					        //12 番号 数値 6   柔整 ：各処理月毎に発番して下さい
                sb.Append(SiharaijiTorimatomeKbn + "," );					//13 請求時取纏区分 数値 1   柔整 ：請求時取纏区分　※1=団体　2=個人
                sb.Append(SeikyujiTorimatomeKbn  + "," );                   //14 支払時取纏区分 数値 1   柔整 ：支払時取纏区分　※1=団体　2=個人                
                sb.Append(NaibuRenban15 + ",");                             //15 内部連番 数値 7   パンチデータファイル毎に1から発番して下さい                
                sb.Append(numbering.ToString("000000")  + ",");				//16 レセプト番号 数値 6   柔整レセ：レセプト番号
                sb.Append(mediYM + "," );					                    //17 診療年月 数値 8   柔整レセ：診療年月　※ yyyymm01 形式で格納
                sb.Append(SiharaisakiCode + "," );                              //18 支払先コード 数値 7 △  柔整レセ：柔整師コード　※個人払(支払時取纏区分=2)の場合、補筆された7桁の数字をパンチ
                sb.Append(syo_mark.PadLeft(5,'0') + ",");						//19 記号 数値 5   柔整レセ：記号
                sb.Append(syo_number.PadLeft(8,'0') + ",");					    //20 番号 数値 8   柔整レセ：番号                
                sb.Append(birth.ToString("yyyyMMdd") + ",");					//21 生年月日 数値 8   柔整レセ：生年月日　※ yyyymmdd 形式で格納　月日ブランクは0000


                sb.Append(sex + "," );						    //22 性別 数値 1   柔整レセ：性別
                sb.Append(sickCode1 + "," );					//23 疾病コード1 数値 4 －  柔整レセ：疾病コード（１）
                sb.Append(sickCode2 + "," );					//24 疾病コード2 数値 4 －  柔整レセ：疾病コード（２）
                sb.Append(sickCode3 + "," );					//25 疾病コード3 数値 4 －  柔整レセ：疾病コード（３）
                sb.Append(sickCode4 + "," );                    //26 疾病コード4 数値 4 －  柔整レセ：疾病コード（４）

                sb.Append(startDate.ToString("yyyyMMdd") + ",");				//27 施術開始日 数値 8   柔整レセ：施術開始日　※ yyyymmdd 形式で格納
                //sb.Append(endDate.ToString("yyyyMMdd") + ",");                  //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納
                sb.Append(strEndDate + ",");                  //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納

                sb.Append(dtDoui + "," );					    //29 同意年月日 数値 8 －  柔整レセ：同意年月日　※ yyyymmdd 形式で格納
                sb.Append(dtUketuke + "," );                    //30 受付年月日 数値 8 －  柔整レセ：35 未定義　※ yyyymmdd 形式で格納

                sb.Append(days.ToString("000") + ",");                     //31 施術日数 数値 3   柔整レセ：施術日数
                sb.Append(charge.ToString("000000000") + ",");             //32 合計金額 数値 9   柔整レセ：合計金額
                

                sb.Append(ItibuFutanKin + "," );					//33 一部負担金 数値 8 －  柔整レセ：一部負担金
                sb.Append(SejutuKaisu + "," );					    //34 施術回数 数値 3 －  柔整レセ：施術回数
                sb.Append(SeikyuGaku + "," );                      //35 請求額 数値 8 －  柔整レセ：請求額



                //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない

                sb.Append(KohiCode1  + "," );				//36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
                sb.Append(KohiCode2 + "," );				//37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
                sb.Append(KohiKingaku1 + "," );				//38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
                sb.Append(KohiKingaku2 + "," );				//39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
                sb.Append(Kanjafutangaku_Kohi1 + "," );		//40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
                sb.Append(Kanjafutangaku_Kohi2 + "," );     //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
                sb.Append(MaruFuKBN + "," );				//42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
                sb.Append(TenkiKBN + "," );                 //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止

                /*
                sb.Append(KohiCode1.PadLeft(2, '0') + ",");					//36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
                sb.Append(KohiCode2.PadLeft(2, '0') + ",");					//37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
                sb.Append(KohiKingaku1.PadLeft(9, '0') + ",");				//38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
                sb.Append(KohiKingaku2.PadLeft(9, '0') + ",");				//39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
                sb.Append(Kanjafutangaku_Kohi1.PadLeft(8, '0') + ",");		//40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
                sb.Append(Kanjafutangaku_Kohi2.PadLeft(8, '0') + ",");      //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
                sb.Append(MaruFuKBN.PadLeft(1, '0') + ",");					//42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
                sb.Append(TenkiKBN.PadLeft(1, '0') + ",");                  //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止
                */



                sb.Append(HonkeSyubetuCode + "," );                    //44 本家種別コード 数値 1 



                //2019/01/25関西情報センター中山様より 省略時は0埋めでなく値をセットしない
                sb.Append(KozaNo + "," );					        //45 口座番号 数値 8 △ 
                sb.Append(KozaSyubetu.ToString() + "," );	        //46 口座種別 数値 1 △                 
                sb.Append(DrName+ "," );					        //47 柔整師名 文字 全角12 △ 
                sb.Append(BankName + "," );					        //48 銀行名 文字 全角10 
                sb.Append(BankBranch + "," );				        //49 支店名 文字 全角10 
                sb.Append(BankAccountName);					        //50 口座名義 全角12

                /*
                sb.Append(KozaNo.PadLeft(8, '0') + ",");					//45 口座番号 数値 8 △ 
                sb.Append(KozaSyubetu.ToString() + ",");					//46 口座種別 数値 1 △                 
                sb.Append(DrName.PadLeft(12, '　') + ",");					//47 柔整師名 文字 全角12 △ 
                sb.Append(BankName.PadLeft(10, '　') + ",");					//48 銀行名 文字 全角10 
                sb.Append(BankBranch.PadLeft(10, '　') + ",");					//49 支店名 文字 全角10 
                sb.Append(BankAccountName.PadLeft(12, '　'));					//50 口座名義 全角12
                */



                sw.WriteLine(sb.ToString());
               
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
        #endregion






        /// <summary>
        /// 昭和=5,平成=7
        /// </summary>
        /// <param name="adYearMonth"></param>
        /// <returns></returns>
        private static int getGyymmFromAdYM(int adYearMonth)
        {
            var y = adYearMonth / 100;
            var m = adYearMonth % 100;
            var dt = new DateTime(y, m, 1);
            var era = DateTimeEx.GetEraNumber(dt);
            var jy = DateTimeEx.GetJpYear(dt);
            era = era == 1 ? 1 : era == 2 ? 3 : era == 3 ? 5 : era == 4 ? 7 : era == 5 ? 9 : 0;

            return era * 10000 + jy * 100 + m;
        }



        /// <summary>
        /// 昭和=5,平成=7
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private static int getIntJpDateWithEraNumber(DateTime dt)
        {
            var era = DateTimeEx.GetEraNumber(dt);
            var jy = DateTimeEx.GetJpYear(dt);
            if (era == 0 || jy == 0) return 0;
            era = era == 1 ? 1 : era == 2 ? 3 : era == 3 ? 5 : era == 4 ? 7 : era == 5 ? 9 : 0;
            return era * 1000000 + jy * 10000 + dt.Month * 100 + dt.Day;
        }


        /// <summary>
        /// 件数と支給、合計金額の集計CSVを作成します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="fileName"></param>
        /// <param name="all"></param>
        /// <returns></returns>
        public static bool ExportNewSumCsv(int cym, string fileName, List<ExportNew> all)
        {
            int honCountJ = 0, honChargeJ = 0, honFutanJ = 0, honCountJK = 0, honChargeJK = 0, honFutanJK = 0;
            int honCountH = 0, honChargeH = 0, honFutanH = 0, honCountHK = 0, honChargeHK = 0, honFutanHK = 0;
            int honCountA = 0, honChargeA = 0, honFutanA = 0, honCountAK = 0, honChargeAK = 0, honFutanAK = 0;
            int kazCountJ = 0, kazChargeJ = 0, kazFutanJ = 0, kazCountJK = 0, kazChargeJK = 0, kazFutanJK = 0;
            int kazCountH = 0, kazChargeH = 0, kazFutanH = 0, kazCountHK = 0, kazChargeHK = 0, kazFutanHK = 0;
            int kazCountA = 0, kazChargeA = 0, kazFutanA = 0, kazCountAK = 0, kazChargeAK = 0, kazFutanAK = 0;


            try
            {
                foreach (var item in all)
                {
                    if (item.jyushinKubun == 8)
                    {
                        //公費あり
                        if (item.honkeKubun == 1)
                        {
                            //本人
                            if (item.kyushuKubun == 3)
                            {
                                honCountJ++;
                                honChargeJ += item.charge;
                                honFutanJ += item.partial;
                                honCountJK++;
                                honChargeJK += item.charge;
                                honFutanJK += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                honCountA++;
                                honChargeA += item.charge;
                                honFutanA += item.partial;
                                honCountAK++;
                                honChargeAK += item.charge;
                                honFutanAK += item.partial;
                            }
                            else
                            {
                                honCountH++;
                                honChargeH += item.charge;
                                honFutanH += item.partial;
                                honCountHK++;
                                honChargeHK += item.charge;
                                honFutanHK += item.partial;
                            }
                        }
                        else
                        {
                            //家族
                            if (item.kyushuKubun == 3)
                            {
                                kazCountJ++;
                                kazChargeJ += item.charge;
                                kazFutanJ += item.partial;
                                kazCountJK++;
                                kazChargeJK += item.charge;
                                kazFutanJK += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                kazCountA++;
                                kazChargeA += item.charge;
                                kazFutanA += item.partial;
                                kazCountAK++;
                                kazChargeAK += item.charge;
                                kazFutanAK += item.partial;
                            }
                            else
                            {
                                kazCountH++;
                                kazChargeH += item.charge;
                                kazFutanH += item.partial;
                                kazCountHK++;
                                kazChargeHK += item.charge;
                                kazFutanHK += item.partial;
                            }
                        }
                    }
                    else
                    {
                        //公費なし
                        if (item.honkeKubun == 1)
                        {
                            //本人
                            if (item.kyushuKubun == 3)
                            {
                                honCountJ++;
                                honChargeJ += item.charge;
                                honFutanJ += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                honCountA++;
                                honChargeA += item.charge;
                                honFutanA += item.partial;
                            }
                            else
                            {
                                honCountH++;
                                honChargeH += item.charge;
                                honFutanH += item.partial;
                            }
                        }
                        else
                        {
                            //家族
                            if (item.kyushuKubun == 3)
                            {
                                kazCountJ++;
                                kazChargeJ += item.charge;
                                kazFutanJ += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                kazCountA++;
                                kazChargeA += item.charge;
                                kazFutanA += item.partial;
                            }
                            else
                            {
                                kazCountH++;
                                kazChargeH += item.charge;
                                kazFutanH += item.partial;
                            }
                        }
                    }
                }

                using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(cym.ToString("0000年00月納品分"));

                    sw.WriteLine("\r\n柔整");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountJ},{honChargeJ},{honFutanJ}");
                    sw.WriteLine($",内、公費,{honCountJK},{honChargeJK},{honFutanJK}");
                    sw.WriteLine($"家族,,{kazCountJ},{kazChargeJ},{kazFutanJ}");
                    sw.WriteLine($",内、公費,{kazCountJK},{kazChargeJK},{kazFutanJK}");

                    sw.WriteLine("\r\n鍼灸");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountH},{honChargeH},{honFutanH}");
                    sw.WriteLine($",内、公費,{honCountHK},{honChargeHK},{honFutanHK}");
                    sw.WriteLine($"家族,,{kazCountH},{kazChargeH},{kazFutanH}");
                    sw.WriteLine($",内、公費,{kazCountHK},{kazChargeHK},{kazFutanHK}");

                    sw.WriteLine("\r\nあんま");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountA},{honChargeA},{honFutanA}");
                    sw.WriteLine($",内、公費,{honCountAK},{honChargeAK},{honFutanAK}");
                    sw.WriteLine($"家族,,{kazCountA},{kazChargeA},{kazFutanA}");
                    sw.WriteLine($",内、公費,{kazCountAK},{kazChargeAK},{kazFutanAK}");

                    sw.WriteLine("\r\n合計");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountJ + honCountH + honCountA}," +
                        $"{honChargeJ + honChargeH + honChargeA},{honFutanJ + honFutanH + honFutanA}");
                    sw.WriteLine($",内、公費,{honCountJK + honCountHK + honCountAK}," +
                        $"{honChargeJK + honChargeHK + honChargeAK},{honFutanJK + honFutanHK + honFutanAK}");
                    sw.WriteLine($"家族,,{kazCountJ + kazCountH + kazCountA}," +
                        $"{kazChargeJ + kazChargeH + kazChargeA},{kazFutanJ + kazFutanH + kazFutanA}");
                    sw.WriteLine($",内、公費,{kazCountJK + kazCountHK + kazCountAK}," +
                        $"{kazChargeJK + kazChargeHK + kazChargeAK},{kazFutanJK + kazFutanHK + kazFutanAK}");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }
    }
}
