﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.KyotofuNokyo
{
    class Export
    {
        App app { get; set; }
        int honkeKubun { get; set; }
        int chargeYM { get; set; }
        APP_TYPE appType { get; set; }

        int mediYM { get; set; }
        string mark { get; set; }
        string number { get; set; }
        DateTime birth { get; set; }
        int sex { get; set; }
        DateTime startDate { get; set; }
        int days { get; set; }
        int total { get; set; }
        int charge { get; set; }
        int partial { get; set; }
        int receNo { get; set; }
        int henrei { get; set; }
        int jyushinKubun { get; set; }
        int numbering { get; set; }
        List<string> imagePahts = new List<string>();

        int changedJyushinKubun => jyushinKubun == 8 ? 1 :
            jyushinKubun == 0 ? 6 :
            jyushinKubun == 4 ? 5 : 0;

        int kyushuKubun => appType == APP_TYPE.あんま ? 4 :
            appType == APP_TYPE.鍼灸 ? 5 : 3;

        public static bool DoExport(int cym)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = $"REORIGIN.TXT";
                f.Filter = "REORIGIN.TXT|txtファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データを取得しています");
                var list = App.GetApps(cym);
                list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                int number = 100000;
                var honJyu = new List<Export>();
                var honHari = new List<Export>();
                var honAnma = new List<Export>();
                var kazokuJyu = new List<Export>();
                var kazokuHari = new List<Export>();
                var kazokuAnma = new List<Export>();
                var all = new List<Export>();

                for (int i = 0; i < list.Count; i++)
                {
                    var item = list[i];
                    if (item.MediYear <= 0) continue;
                    number++;
                    var export = new Export(item);
                    export.numbering = number;

                    //続紙追加
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if (list[j].MediYear >= 0) break;
                        if (list[j].MediYear != (int)APP_SPECIAL_CODE.続紙) continue;
                        export.imagePahts.Add(list[j].GetImageFullPath());
                    }

                    all.Add(export);

                    //柔整・はり・あんまごと、本人・家族ごと、6パターンに分類
                    if (export.honkeKubun == 1)
                    {
                        if (item.AppType == APP_TYPE.鍼灸) honHari.Add(export);
                        else if (item.AppType == APP_TYPE.あんま) honAnma.Add(export);
                        else honJyu.Add(export);
                    }
                    else
                    {
                        if (item.AppType == APP_TYPE.鍼灸) kazokuHari.Add(export);
                        else if (item.AppType == APP_TYPE.あんま) kazokuHari.Add(export);
                        else kazokuJyu.Add(export);
                    }
                }

                wf.LogPrint("データを出力しています");
                //データ出力
                using (var sw = new StreamWriter(fileName, false, Encoding.ASCII))
                {
                    if (!writeTsuduriLine(sw, honJyu, 10001)) return false;
                    if (!writeTsuduriLine(sw, kazokuJyu, 10002)) return false;
                    if (!writeTsuduriLine(sw, honAnma, 10003)) return false;
                    if (!writeTsuduriLine(sw, kazokuAnma, 10004)) return false;
                    if (!writeTsuduriLine(sw, honHari, 10005)) return false;
                    if (!writeTsuduriLine(sw, kazokuHari, 10006)) return false;
                }

                //集計出力
                var dir = $"{Path.GetDirectoryName(fileName)}";
                ExportSumCsv(cym, dir + "\\集計.csv", all);

                wf.LogPrint("画像を出力しています");
                //画像出力
                var fc = new TiffUtility.FastCopy();
                dir += $"\\{list[0].CYM.ToString("000000")}";
                Directory.CreateDirectory(dir);
                dir += "\\";

                wf.SetMax(all.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                foreach (var item in all)
                {
                    TiffUtility.MargeOrCopyTiff(fc, item.imagePahts, dir + item.numbering.ToString("000000") + ".TIF");
                    wf.InvokeValue++;
                }
            }

            return true;
        }

        //データ作成　直接いじらず別の関数として作成する
        private Export(App app)
        {
            this.app = app;
            honkeKubun = app.Family / 100;                          //本家区分（綴表）
            chargeYM = app.CYM;                                     //請求年月
            appType = app.AppType;                                  //あんま、鍼、柔性の区分
            mediYM = app.YM;                                        //診療年月
            mark = app.HihoNum.Split('-')[0];                       //記号
            number = app.HihoNum.Split('-')[1];                     //番号
            birth = app.Birthday;                                   //生年月日
            sex = app.Sex;                                          //性別
            startDate = app.FushoStartDate1;                        //診療開始日
            days = app.CountedDays;                                 //診療実日数
            total = app.Total;                                      //決定点数
            charge = app.Charge;                                    //請求額
            partial = app.Partial;                                  //一部負担金
            jyushinKubun = app.Family % 100;                        //受診者区分
            imagePahts.Add(app.GetImageFullPath());                 //画像パス
        }

        private static bool writeTsuduriLine(StreamWriter sw,List<Export>l, int tsuduriNo)
        {
            if (l.Count == 0) return true;

            try
            {
                var sb = new StringBuilder();
                sb.Append("7412*");
                sb.Append("71527");
                sb.Append("00");
                sb.Append(l[0].honkeKubun);
                sb.Append("40000");
                sb.Append(getGyymmFromAdYM(l[0].chargeYM));
                sb.Append(new string('0', 2));
                sb.Append(l[0].kyushuKubun);
                sb.Append("2");
                sb.Append(tsuduriNo.ToString("000000"));
                sb.Append(l.Count.ToString("000000"));
                sb.Append(new string('0', 40));
                sw.WriteLine(sb.ToString());

                foreach (var item in l)
                {
                    if (!item.writeReceLine(sw)) return false;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        private bool writeReceLine(StreamWriter sw)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(getGyymmFromAdYM(mediYM));
                sb.Append(new string('0', 10)); //6～15
                sb.Append(mark.PadLeft(4, '0'));
                sb.Append(number.PadLeft(8, '0'));
                sb.Append(getIntJpDateWithEraNumber(birth));
                sb.Append(sex);
                sb.Append(new string('0', 4)); //36～39
                sb.Append(getIntJpDateWithEraNumber(startDate));
                sb.Append(days.ToString().PadLeft(3, '0'));
                sb.Append(total.ToString().PadLeft(7, '0'));
                sb.Append(numbering.ToString().PadLeft(6, '0'));
                sb.Append(new string('0', 80)); //63～142
                sb.Append(changedJyushinKubun);
                sw.WriteLine(sb.ToString());
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 昭和=5,平成=7
        /// </summary>
        /// <param name="adYearMonth"></param>
        /// <returns></returns>
        private static int getGyymmFromAdYM(int adYearMonth)
        {
            var y = adYearMonth / 100;
            var m = adYearMonth % 100;
            var dt = new DateTime(y, m, 1);
            var era = DateTimeEx.GetEraNumber(dt);
            var jy = DateTimeEx.GetJpYear(dt);
            era = era == 1 ? 1 : era == 2 ? 3 : era == 3 ? 5 : era == 4 ? 7 : era == 5 ? 9 : 0;

            return era * 10000 + jy * 100 + m;
        }

        /// <summary>
        /// 昭和=5,平成=7
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private static int getIntJpDateWithEraNumber(DateTime dt)
        {
            var era = DateTimeEx.GetEraNumber(dt);
            var jy = DateTimeEx.GetJpYear(dt);
            if (era == 0 || jy == 0) return 0;
            era = era == 1 ? 1 : era == 2 ? 3 : era == 3 ? 5 : era == 4 ? 7 : era == 5 ? 9 : 0;
            return era * 1000000 + jy * 10000 + dt.Month * 100 + dt.Day;
        }

        /// <summary>
        /// 件数と支給、合計金額の集計CSVを作成します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="fileName"></param>
        /// <param name="all"></param>
        /// <returns></returns>
        public static bool ExportSumCsv(int cym, string fileName, List<Export> all)
        {
            int honCountJ = 0, honChargeJ = 0, honFutanJ = 0, honCountJK = 0, honChargeJK = 0, honFutanJK = 0;
            int honCountH = 0, honChargeH = 0, honFutanH = 0, honCountHK = 0, honChargeHK = 0, honFutanHK = 0;
            int honCountA = 0, honChargeA = 0, honFutanA = 0, honCountAK = 0, honChargeAK = 0, honFutanAK = 0;
            int kazCountJ = 0, kazChargeJ = 0, kazFutanJ = 0, kazCountJK = 0, kazChargeJK = 0, kazFutanJK = 0;
            int kazCountH = 0, kazChargeH = 0, kazFutanH = 0, kazCountHK = 0, kazChargeHK = 0, kazFutanHK = 0;
            int kazCountA = 0, kazChargeA = 0, kazFutanA = 0, kazCountAK = 0, kazChargeAK = 0, kazFutanAK = 0;


            try
            {
                foreach (var item in all)
                {
                    if (item.jyushinKubun == 8)
                    {
                        //公費あり
                        if (item.honkeKubun == 1)
                        {
                            //本人
                            if (item.kyushuKubun == 3)
                            {
                                honCountJ++;
                                honChargeJ += item.charge;
                                honFutanJ += item.partial;
                                honCountJK++;
                                honChargeJK += item.charge;
                                honFutanJK += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                honCountA++;
                                honChargeA += item.charge;
                                honFutanA += item.partial;
                                honCountAK++;
                                honChargeAK += item.charge;
                                honFutanAK += item.partial;
                            }
                            else
                            {
                                honCountH++;
                                honChargeH += item.charge;
                                honFutanH += item.partial;
                                honCountHK++;
                                honChargeHK += item.charge;
                                honFutanHK += item.partial;
                            }
                        }
                        else
                        {
                            //家族
                            if (item.kyushuKubun == 3)
                            {
                                kazCountJ++;
                                kazChargeJ += item.charge;
                                kazFutanJ += item.partial;
                                kazCountJK++;
                                kazChargeJK += item.charge;
                                kazFutanJK += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                kazCountA++;
                                kazChargeA += item.charge;
                                kazFutanA += item.partial;
                                kazCountAK++;
                                kazChargeAK += item.charge;
                                kazFutanAK += item.partial;
                            }
                            else
                            {
                                kazCountH++;
                                kazChargeH += item.charge;
                                kazFutanH += item.partial;
                                kazCountHK++;
                                kazChargeHK += item.charge;
                                kazFutanHK += item.partial;
                            }
                        }
                    }
                    else
                    {
                        //公費なし
                        if (item.honkeKubun == 1)
                        {
                            //本人
                            if (item.kyushuKubun == 3)
                            {
                                honCountJ++;
                                honChargeJ += item.charge;
                                honFutanJ += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                honCountA++;
                                honChargeA += item.charge;
                                honFutanA += item.partial;
                            }
                            else
                            {
                                honCountH++;
                                honChargeH += item.charge;
                                honFutanH += item.partial;
                            }
                        }
                        else
                        {
                            //家族
                            if (item.kyushuKubun == 3)
                            {
                                kazCountJ++;
                                kazChargeJ += item.charge;
                                kazFutanJ += item.partial;
                            }
                            else if (item.kyushuKubun == 4)
                            {
                                kazCountA++;
                                kazChargeA += item.charge;
                                kazFutanA += item.partial;
                            }
                            else
                            {
                                kazCountH++;
                                kazChargeH += item.charge;
                                kazFutanH += item.partial;
                            }
                        }
                    }
                }

                using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(cym.ToString("0000年00月納品分"));

                    sw.WriteLine("\r\n柔整");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountJ},{honChargeJ},{honFutanJ}");
                    sw.WriteLine($",内、公費,{honCountJK},{honChargeJK},{honFutanJK}");
                    sw.WriteLine($"家族,,{kazCountJ},{kazChargeJ},{kazFutanJ}");
                    sw.WriteLine($",内、公費,{kazCountJK},{kazChargeJK},{kazFutanJK}");

                    sw.WriteLine("\r\n鍼灸");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountH},{honChargeH},{honFutanH}");
                    sw.WriteLine($",内、公費,{honCountHK},{honChargeHK},{honFutanHK}");
                    sw.WriteLine($"家族,,{kazCountH},{kazChargeH},{kazFutanH}");
                    sw.WriteLine($",内、公費,{kazCountHK},{kazChargeHK},{kazFutanHK}");

                    sw.WriteLine("\r\nあんま");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountA},{honChargeA},{honFutanA}");
                    sw.WriteLine($",内、公費,{honCountAK},{honChargeAK},{honFutanAK}");
                    sw.WriteLine($"家族,,{kazCountA},{kazChargeA},{kazFutanA}");
                    sw.WriteLine($",内、公費,{kazCountAK},{kazChargeAK},{kazFutanAK}");

                    sw.WriteLine("\r\n合計");
                    sw.WriteLine("種別,,件数,支給金額,自己負担額");
                    sw.WriteLine($"本人,,{honCountJ + honCountH + honCountA}," +
                        $"{honChargeJ + honChargeH + honChargeA},{honFutanJ + honFutanH + honFutanA}");
                    sw.WriteLine($",内、公費,{honCountJK + honCountHK + honCountAK}," +
                        $"{honChargeJK + honChargeHK + honChargeAK},{honFutanJK + honFutanHK + honFutanAK}");
                    sw.WriteLine($"家族,,{kazCountJ + kazCountH + kazCountA}," +
                        $"{kazChargeJ + kazChargeH + kazChargeA},{kazFutanJ + kazFutanH + kazFutanA}");
                    sw.WriteLine($",内、公費,{kazCountJK + kazCountHK + kazCountAK}," +
                        $"{kazChargeJK + kazChargeHK + kazChargeAK},{kazFutanJK + kazFutanHK + kazFutanAK}");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }
    }
}
