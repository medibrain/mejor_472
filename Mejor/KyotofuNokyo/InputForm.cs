﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.KyotofuNokyo
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 250);
        Point posCost = new Point(800, 1400);
        Point posDays = new Point(600, 300);
        Point posNewCont = new Point(800, 1000);
        Point posOryo = new Point(400, 300);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls,newContControls, oryoControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();
            
            //コントロールの位置によってグループを決めている
            ymControls = new Control[] { verifyBoxY, verifyBoxM };

                        
            hihoNumControls = new Control[] { verifyBoxNumM, verifyBoxNumN, verifyBoxJuryoType, };
            personControls = new Control[] { verifyBoxFamily, verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };

            //20190122132617 furukawa st ////////////////////////
            //柔整師登録番号欄を追加
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, verifyBoxDrCode, };
            //costControls = new Control[] { verifyBoxTotal, verifyBoxCharge,  };
            //20190122132617 furukawa ed ////////////////////////

            fushoControls = new Control[] { verifyBoxF1D, verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            newContControls = new Control[] { verifyCheckBoxOryo };
            oryoControls = new Control[] { verifyCheckBoxOryo };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;

        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号チェック 1文字以上かつ数字に直せること
            int numM = verifyBoxNumM.GetIntValue();
            setStatus(verifyBoxNumM, numM < 1);
            int numN = verifyBoxNumN.GetIntValue();
            setStatus(verifyBoxNumN, numN < 1);

            //受診者区分
            int type = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new[] { 0, 2, 4, 6, 8 }.Contains(type));

            
            //20190122132809 furukawa st ////////////////////////
            //柔整師登録番号を取得
            string strDrNum= verifyBoxDrCode.Value;
            setStatus(verifyBoxDrCode, strDrNum.Length !=2);
            //20190122132809 furukawa ed ////////////////////////


            //本家区分 入力2=本人を1に、入力6=家族を2に変換して記録
            int family = verifyBoxFamily.GetIntValue();
            family = family == 2 ? 1 : family == 6 ? 2 : 0;
            setStatus(verifyBoxFamily, family != 1 && family != 2);

            //性別のチェック
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日のチェック
            var birth = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            if ((type == 2 || type == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (type == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (type == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //開始日チェック
            DateTime start = DateTime.MinValue;
            int sD = verifyBoxF1D.GetIntValue();

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            int sAdY = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            //int sAdY = DateTimeEx.GetAdYearFromHs(year);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            if (!DateTimeEx.IsDate(sAdY, month, sD))
            {
                setStatus(verifyBoxF1D, true);
            }
            else
            {
                setStatus(verifyBoxF1D, false);
                start = new DateTime(sAdY, month, sD);
            }

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);




            //20190122132502 furukawa st ////////////////////////
            //柔整師登録番号チェック

            //if (verifyBoxDrCode.Text.Trim().Length != 0)
            //{
            //    var t = verifyBoxDrCode.Text.Trim();
            //    if (t.Length == 10 && (t[0] != '0' && t[0] != '1'))
            //    {
            //        //1文字目は0か1で指定されている
            //        setStatus(verifyBoxDrCode, true);
            //    }
            //    else
            //    {
            //        setStatus(verifyBoxDrCode, false);
            //    }
            //}
            //else
            //{
            //    setStatus(verifyBoxDrCode, false);
            //}
            //20190122132502 furukawa ed ////////////////////////




            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("受診者区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //受診者区分と本人/家族のチェック
            if ((type == 2 && family != 1) || ((type == 4 || type == 6) && family != 2))
            {
                verifyBoxJuryoType.BackColor = Color.GreenYellow;
                verifyBoxFamily.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("受診者区分と本人/家族のいずれか、" +
                    "または両方に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = $"{verifyBoxNumM.Text.Trim()}-{verifyBoxNumN.Text.Trim()}";
            app.Family = family * 100 + type;   //受診者区分と本人家族をまとめて記録
            app.Sex = sex;
            app.Birthday = birth;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.Partial = total - charge;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.AccountNumber = string.Empty;
            
            //20190122132348 furukawa st ////////////////////////
            //柔整師登録番号を入力        
            app.DrNum = strDrNum;
            //20190122132348 furukawa ed ////////////////////////


            


            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            app.FushoStartDate1 = start;
            
            app.Distance = verifyCheckBoxOryo.Checked ? 999 : 0;
            app.AppType = scan.AppType;

            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック

            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                //OCRデータがあれば、部位のみ挿入
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxY, labelYearInfo, labelH, labelY,
                labelInputerName};

            var visible = new Action<bool>(b =>
            {
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is IVerifiable || item is Label)) continue;
                    if (cs.Contains(item)) continue;
                    item.Visible = b;
                }
            });

            if ((verifyBoxY.Text == "--" || verifyBoxY.Text == "++") && verifyBoxY.Text.Length == 2)
            {
                //続紙その他
                visible(false);
            }
            else
            {
                //申請書の場合
                visible(true);
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// 入力済みデータをセットします
        /// </summary>
        private void setValues(App app)
        {
            var ca = new CheckInputAall(app);
            if(!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);            

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //申請書

                //家族区分 本人データ1=入力時2 家族データ2=入力時6
                int family = app.Family / 100;
                family = family == 1 ? 2 : 6;
                
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyBoxNumM, app.HihoNum.Split('-')[0], firstTime, nv);
                setValue(verifyBoxNumN, app.HihoNum.Split('-')[1], firstTime, nv);
                setValue(verifyBoxJuryoType, app.Family % 100, firstTime, nv);
                setValue(verifyBoxFamily, family, firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv && ca.ErrAcounteddays);
                setValue(verifyBoxTotal, app.Total, firstTime, nv && ca.ErrAtotal);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv && ca.ErrAcharge);

                //20190122133049 furukawa st ////////////////////////
                //柔整師登録番号を取得                
//                setValueFukencode(verifyBoxDrCode, app.DrNum, firstTime, nv);
                setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);
                //20190122133049 furukawa ed ////////////////////////


                setValue(verifyBoxF1D, app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyCheckBoxOryo, app.Distance == 999, firstTime, nv);
                setValue(verifyBoxNewCont, app.NewContType == NEW_CONT.新規 ? "1" : "2", firstTime, false);
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);
            }
            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount != 0)
            {
                dataGridViewPlist.CurrentCell = null;
                dataGridViewPlist.CurrentCell = dataGridViewPlist["Aid", 0];
            }

            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
