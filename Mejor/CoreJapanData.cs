﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    class CoreJapanData
    {
        int YM { get; set; }
        int CYM { get; set; }
        string InsNum { get; set; }
        string Num { get; set; }
        DateTime Birth { get; set; }
        SEX Sex { get; set; }
        string Name { get; set; }
        string DestName { get; set; }
        string Zip { get; set; }
        string Adds { get; set; }

        int Days { get; set; }
        int Ratio { get; set; }
        int Total { get; set; }
        int Partial { get; set; }
        int Charge { get; set; }
        string DrNum { get; set; }
        string FileNumber { get; set; }
        string FullPath { get; set; }

        public static List<CoreJapanData> Import(string fileName)
        {
            byte prevByte = 0;
            int length = 0;
            var path =System.IO.Path.GetDirectoryName(fileName)+ "\\申請書";


            using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
            {
                //一行のバイト数をまず取得
                while (fs.Position < 10000)
                {
                    byte b = (byte)fs.ReadByte();
                    if (prevByte == 0x0D && b == 0x0A)
                    {
                        length = (int)fs.Position - 2;
                        fs.Position = 0;
                        break;
                    }
                    prevByte = b;
                }

                var l = new List<CoreJapanData>();
                var bs = new byte[length];
                var allLength = fs.Length;
                int temp;
                var days5 = new int[4];

                //一行ずつバイトで読み込み
                while (fs.Position < allLength)
                {
                    fs.Read(bs, 0, length);
                    var c = new CoreJapanData();

                    int.TryParse(byteToSjis(bs, 0, 4), out temp);
                    c.YM = DateTimeEx.GetAdYearMonthFromJyymm(40000 + temp);

                    int.TryParse(byteToSjis(bs, 12, 4), out temp);
                    c.CYM = DateTimeEx.GetAdYearMonthFromJyymm(40000 + temp);

                    c.InsNum = byteToSjis(bs, 501, 8).Trim();
                    c.Num = byteToSjis(bs, 46, 8).Trim();

                    var birthStr = byteToSjis(bs, 81, 7);
                    c.Birth = DateTimeEx.GetDateFromJstr7(birthStr);

                    var sexStr = byteToSjis(bs, 88, 1);
                    c.Sex = sexStr == "1" ? SEX.男 : sexStr == "2" ? SEX.女 : SEX.Null;
                    c.Name = byteToSjis(bs, 89, 24).Trim();
                    c.DestName = byteToSjis(bs, 133, 24).Trim();
                    c.Zip = byteToSjis(bs, 157, 8).Trim();
                    c.Adds = byteToSjis(bs, 165, 60).Trim();

                    int.TryParse(byteToSjis(bs, 255, 2), out days5[0]);
                    int.TryParse(byteToSjis(bs, 289, 2), out days5[1]);
                    int.TryParse(byteToSjis(bs, 323, 2), out days5[2]);
                    int.TryParse(byteToSjis(bs, 357, 2), out days5[3]);
                    c.Days = days5.Max();

                    c.Ratio = 10 - (byteStrToInt(bs, 369, 2) / 10);
                    c.Total = byteStrToInt(bs, 371, 5);
                    c.Partial = byteStrToInt(bs, 376, 6);
                    c.Charge = byteStrToInt(bs, 382, 6);
                    c.DrNum = byteToSjis(bs, 475, 10).Trim();

                    c.FileNumber = byteToSjis(bs, 485, 12).Trim();
                    fs.Position += 2;

                    l.Add(c);
                }
                return l;
            }
        }

        private static string byteToSjis(byte[] bytes, int start, int length)
        {
            return Encoding.GetEncoding(932).GetString(bytes, start, length);
        }

        private static int byteStrToInt(byte[] bytes, int start, int length)
        {
            int temp;
            int.TryParse(byteToSjis(bytes, start, length), out temp);
            return temp;
        }

        public ViewerData ToViewData()
        {
            var vd = new ViewerData();

            vd.CYM = CYM;
            vd.YM = YM;
            vd.Sex = Sex;
            vd.Name = Name;
            vd.Zip = Zip;
            vd.Adds = Adds;
            vd.DestName = DestName;
            vd.Birth = Birth.ToInt();
            vd.Total = Total;
            vd.Partial = Partial;
            vd.Charge = Charge;
            vd.Ratio = Ratio;
            vd.DrNum = DrNum;
            vd.ImageFile = FileNumber + ".tif";

            return vd;
        }
    }
}
