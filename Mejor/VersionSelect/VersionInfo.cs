﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Mejor.VersionSelect
{
    class VersionInfo : IComparable
    {
        [DB.DbAttribute.PrimaryKey]
        public string Version { get; private set; }

        public DateTime CreateDT { get; private set; }

        public bool IsTest { get; private set; } = true;

        public bool IsForce { get; private set; } = false;

        public string Description { get; private set; }

        [DB.DbAttribute.Ignore]
        public int Version1 => int.Parse(Version.Split('.')[0]);
        [DB.DbAttribute.Ignore]
        public int Version2 => int.Parse(Version.Split('.')[1]);
        [DB.DbAttribute.Ignore]
        public int Version3 => int.Parse(Version.Split('.')[2]);
        [DB.DbAttribute.Ignore]
        public int Version4 => int.Parse(Version.Split('.')[3]);

        [DB.DbAttribute.Ignore]
        public string Directory => Settings.VersionFolder + "\\" + Version;

        [DB.DbAttribute.Ignore]
        public string IsTestSign => IsTest ? "○" : "";

        [DB.DbAttribute.Ignore]
        public string IsForceSign => IsForce ? "○" : "";

        public static bool ExsistsNewVersion()
        {
            var v = System.Diagnostics.FileVersionInfo.GetVersionInfo(Utility.ExeFullPath);
            var l = GetList();
            if (l == null || l.Count == 0) return false;
            l.Sort();

            var last = l.Last();

            return
                last.Version1 > v.FileMajorPart ? true :
                last.Version2 > v.FileMinorPart ? true :
                last.Version3 > v.FileBuildPart ? true :
                last.Version4 > v.FilePrivatePart ? true :
                false;
        }

        public static List<VersionInfo> GetList()
        {
            var db = new DB("jyusei");
            var res = db.Select<VersionInfo>("TRUE ORDER BY createdt DESC LIMIT 50");
            return res.ToList();

        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is VersionInfo info)
            {
                return
                    Version1 != info.Version1 ? Version1.CompareTo(info.Version1) :
                    Version2 != info.Version2 ? Version2.CompareTo(info.Version2) :
                    Version3 != info.Version3 ? Version3.CompareTo(info.Version3) :
                    Version4.CompareTo(info.Version4);
            }

            throw new ArgumentException();
        }

        /// <summary>
        /// 対象のファイルバージョンに入れ替え、自身を強制終了します
        /// </summary>
        //public void Change_sfx()
        //{
        //    //Cドライブのファイルではない場合はエラー
        //    if (Utility.StartupPath.Remove(2) == "\\\\")
        //    {
        //        MessageBox.Show("ネットワーク上のファイルから起動しています。" +
        //            "C:\\Mejorフォルダからに切り替えて下さい");
        //        return;
        //    }

        //    string strOldDir = $"c:\\Mejor_{Application.ProductVersion}";
        //    string strExeName = string.Empty;

        //    using (var f = new WaitForm())
        //    {
        //        f.ShowDialogOtherTask();
        //        f.LogPrint("ファイルをコピー中です");

        //        var files = System.IO.Directory.GetFiles(Directory, "*", SearchOption.AllDirectories);//バージョンフォルダ
        //        var sDirFiles = System.IO.Directory.GetFiles(Utility.StartupPath, "*", SearchOption.AllDirectories);
        //        var l = new List<string>();

        //        foreach (var item in files)
        //        {
        //            var newName = item.Replace(Directory + "\\", "c:\\");
        //            //var newName = item.Replace(Directory + "\\", Utility.StartupPath);
        //            strExeName =$"c:\\{System.IO.Path.GetFileName(item)}";

        //            //if (Array.Exists(sDirFiles, fn => fn.ToLower() == newName.ToLower()))
        //            //{


        //            //    //同じファイル名が存在した場合リネーム
        //            //    var reName= newName + ".UpdateOldTemp";
        //            //    if (File.Exists(reName)) File.Delete(reName);
        //            //    File.Move(newName, reName);
        //            //}
        //            File.Copy(item, newName, true);

        //        }

        //    }

        //    MessageBox.Show("プログラムの切り替えが完了しました。" +
        //        "終了します。多くの場合、自動でMejorが再度起動します。", "", MessageBoxButtons.OK,
        //        MessageBoxIcon.Information);

        //    //プログラム終了
        //    try
        //    {
        //        Application.Exit();
        //    }
        //    catch
        //    {
        //        try
        //        {
        //            //強制終了
        //            Environment.Exit(0);
        //        }
        //        catch
        //        {
        //            //握りつぶす
        //        }
        //    }

        //    System.IO.Directory.Move("c:\\Mejor", strOldDir);

        //    System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
        //    si.FileName = strExeName;
        //    //si.Arguments = $"a -sfx {strArchiveName} {dir}";
        //    si.UseShellExecute = false;
        //    si.CreateNoWindow = true;
        //    System.Diagnostics.Process p = new System.Diagnostics.Process();
        //    p.StartInfo = si;
        //    p.Start();
        //    p.WaitForExit();
        //    p.Close();
        //    System.IO.Directory.Move("c:\\Debug", "c:\\Mejor");



        //    //新プログラム起動
        //    System.Diagnostics.Process.Start("c:\\Mejor\\Mejor.exe");
        //}

        /// <summary>
        /// 対象のファイルバージョンに入れ替え、自身を強制終了します
        /// </summary>
        public void Change()
        {
            //Cドライブのファイルではない場合はエラー
            if (Utility.StartupPath.Remove(2) == "\\\\")
            {
                MessageBox.Show("ネットワーク上のファイルから起動しています。" +
                    "C:\\Mejorフォルダからに切り替えて下さい");
                return;
            }

            using (var f = new WaitForm())
            {
                f.ShowDialogOtherTask();
                f.LogPrint("ファイルをコピー中です");

                var files = System.IO.Directory.GetFiles(Directory, "*", SearchOption.AllDirectories);
                var sDirFiles = System.IO.Directory.GetFiles(Utility.StartupPath, "*", SearchOption.AllDirectories);
                var l = new List<string>();

                foreach (var item in files)
                {
                    var newName = item.Replace(Directory + "\\", Utility.StartupPath);

                    //20210726201906 furukawa st ////////////////////////
                    //サブフォルダ無かったら作る

                    if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(newName)))
                        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(newName));
                    //20210726201906 furukawa ed ////////////////////////



                    if (Array.Exists(sDirFiles, fn => fn.ToLower() == newName.ToLower()))
                    {


                        //同じファイル名が存在した場合リネーム
                        var reName = newName + ".UpdateOldTemp";
                        if (File.Exists(reName)) File.Delete(reName);
                        File.Move(newName, reName);
                    }
                    File.Copy(item, newName, true);

                }
            }

            MessageBox.Show("プログラムの切り替えが完了しました。" +
                "終了します。多くの場合、自動でMejorが再度起動します。", "", MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            //プログラム終了
            try
            {
                Application.Exit();
            }
            catch
            {
                try
                {
                    //強制終了
                    Environment.Exit(0);
                }
                catch
                {
                    //握りつぶす
                }
            }

            //新プログラム起動
            System.Diagnostics.Process.Start(Application.ExecutablePath);
        }

        /// <summary>
        /// バージョンアップの処理を自己解凍書庫にしたかったが全コピーでも同じじゃん。結局使わず
        /// </summary>
        /// <param name="isTest"></param>
        /// <param name="isForce"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static bool OwnFileRegister_sfx(bool isTest, bool isForce, string description)
        {
            //var path = System.Reflection.Assembly.GetExecutingAssembly().Location;//mejor.exeのパス
            //var info = new System.IO.FileInfo(path);
            //var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path);

            ////versioninfo用クラス
            //var vi = new VersionInfo();
            //vi.Version = version.ProductVersion;
            //vi.CreateDT = info.LastWriteTime;
            //vi.IsTest = isTest;
            //vi.IsForce = isForce;
            //vi.Description = description;

            //var db = new DB("jyusei");
            //using (var tran = db.CreateTransaction())
            //{
            //    try
            //    {
            //        var dir = System.IO.Path.GetDirectoryName(path);//実行ファイルのフォルダ
            //        System.IO.Directory.CreateDirectory(vi.Directory);//バージョンフォルダ

            //        //7zで自己解凍書庫作ってバージョンフォルダに移動
            //        string str7z = $"{dir}\\Archive\\7z.exe";
            //        string strArchiveName= $"{System.IO.Path.GetDirectoryName(dir)}\\Mejor_{vi.Version}.exe";

            //        System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            //        si.FileName = str7z;
            //        si.Arguments = $"a -sfx {strArchiveName} {dir}";
            //        si.UseShellExecute = false;
            //        si.CreateNoWindow = true;
            //        System.Diagnostics.Process p = new System.Diagnostics.Process();
            //        p.StartInfo = si;
            //        p.Start();
            //        p.WaitForExit();
            //        p.Close();

            //        File.Move(System.IO.Path.GetDirectoryName(dir) + $"\\Mejor_{vi.Version}.exe", vi.Directory + $"\\Mejor_{vi.Version}.exe");


            //        if (!db.Insert(vi, tran)) return false;
            //    }
            //    catch (Exception ex)
            //    {
            //        Log.ErrorWriteWithMsg(ex);
            //        return false;
            //    }

            //    tran.Commit();
            //}
            return true;
        }

        //20220227134631 furukawa st ////////////////////////
        //全ファイルコピーに変更
        
        public static bool OwnFileRegister(bool isTest, bool isForce, string description)
        {
            var path = System.Reflection.Assembly.GetExecutingAssembly().Location;//mejorフォルダパス
            var info = new System.IO.FileInfo(path);
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path);

            var vi = new VersionInfo();
            vi.Version = version.ProductVersion;
            vi.CreateDT = info.LastWriteTime;
            vi.IsTest = isTest;
            vi.IsForce = isForce;
            vi.Description = description;

            var db = new DB("jyusei");
            using (var tran = db.CreateTransaction())
            {
                try
                {
                    var dir = System.IO.Path.GetDirectoryName(path);//mejorフォルダ
                    System.IO.Directory.CreateDirectory(vi.Directory);

                    //サブフォルダの処理が手間なので全zipにしてサーバで解凍
                    ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                    
                    zip.CreateZip($"{vi.Directory}\\temp.zip", dir, true,null);                    
                    zip.ExtractZip($"{vi.Directory}\\temp.zip", vi.Directory,null);
                    System.IO.File.Delete($"{vi.Directory}\\temp.zip");//一時zipは削除

                    if (!db.Insert(vi, tran)) return false;
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }

                tran.Commit();
            }
            return true;
        }

        #region old
        //public static bool OwnFileRegister(bool isTest, bool isForce, string description)
        //{
        //    var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        //    var info = new System.IO.FileInfo(path);
        //    var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path);

        //    var vi = new VersionInfo();
        //    vi.Version = version.ProductVersion;
        //    vi.CreateDT = info.LastWriteTime;
        //    vi.IsTest = isTest;
        //    vi.IsForce = isForce;
        //    vi.Description = description;

        //    var db = new DB("jyusei");
        //    using (var tran = db.CreateTransaction())
        //    {
        //        try
        //        {
        //            var dir = System.IO.Path.GetDirectoryName(path);
        //            System.IO.Directory.CreateDirectory(vi.Directory);

        //            File.Copy(dir + "\\Mejor.exe", vi.Directory + "\\Mejor.exe");
        //            File.Copy(dir + "\\Mejor.pdb", vi.Directory + "\\Mejor.pdb");

        //            //20210104161441 furukawa st ////////////////////////
        //            //settings.xmlもコピーしないと設定が反映できない                    
        //            File.Copy(dir + "\\Settings.xml", vi.Directory + "\\Settings.xml");
        //            //20210104161441 furukawa ed ////////////////////////

        //            //20210413144541 furukawa st ////////////////////////
        //            //Repostsフォルダも更新対象。フォルダ内のファイルの場合はそのフォルダを入れてないと、更新時にコピーされない

        //            if (!System.IO.Directory.Exists(vi.Directory + "\\Reports\\")) System.IO.Directory.CreateDirectory(vi.Directory + "\\Reports\\");
        //            File.Copy(dir + "\\Reports\\HenreiDoc.rpr", vi.Directory + "\\Reports\\HenreiDoc.rpr");
        //            //20210413144541 furukawa ed ////////////////////////

        //            //20210726183439 furukawa st ////////////////////////
        //            //ストアド・sqlもコピー

        //            if (!System.IO.Directory.Exists(vi.Directory + "\\Stored\\")) System.IO.Directory.CreateDirectory(vi.Directory + "\\Stored\\");
        //            List<string> filesStored = System.IO.Directory.GetFiles(dir + $"\\Stored").ToList<string>();
        //            foreach (string filepath in filesStored)
        //            {
        //                File.Copy(dir + $"\\Stored\\{System.IO.Path.GetFileName(filepath)}", vi.Directory + $"\\Stored\\{System.IO.Path.GetFileName(filepath)}");
        //            }


        //            if (!System.IO.Directory.Exists(vi.Directory + "\\CreateDB\\")) System.IO.Directory.CreateDirectory(vi.Directory + "\\CreateDB\\");
        //            List<string> filesCreateDB = System.IO.Directory.GetFiles(dir + $"\\CreateDB").ToList<string>();
        //            foreach (string filepath in filesCreateDB)
        //            {
        //                File.Copy(dir + $"\\CreateDB\\{System.IO.Path.GetFileName(filepath)}", vi.Directory + $"\\CreateDB\\{System.IO.Path.GetFileName(filepath)}");
        //            }

        //            //20210726183439 furukawa ed ////////////////////////

        //            if (!System.IO.Directory.Exists(vi.Directory + "\\psql\\")) System.IO.Directory.CreateDirectory(vi.Directory + "\\psql\\");
        //            List<string> filespsql = System.IO.Directory.GetFiles(dir + $"\\psql").ToList<string>();
        //            foreach (string filepath in filespsql)
        //            {
        //                File.Copy(dir + $"\\psql\\{System.IO.Path.GetFileName(filepath)}", vi.Directory + $"\\psql\\{System.IO.Path.GetFileName(filepath)}");
        //            }

        //            if (!db.Insert(vi, tran)) return false;
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.ErrorWriteWithMsg(ex);
        //            return false;
        //        }

        //        tran.Commit();
        //    }
        //    return true;
        //}

        #endregion

        //20220227134631 furukawa ed ////////////////////////


        public static VersionInfo ExistsForceUpdate()
        {
            //フォルダコピーローカル関数
            void dirCopy(string s, string d)
            {
                System.IO.Directory.CreateDirectory(d);
                d = d + "\\";

                string[] files = System.IO.Directory.GetFiles(s);
                foreach (string file in files) File.Copy(file, d + Path.GetFileName(file), true);

                //サブフォルダ再帰
                string[] dirs = System.IO.Directory.GetDirectories(s);
                foreach (string dir in dirs) dirCopy(dir, d + Path.GetFileName(dir));
            }




            //20190312104313 furukawa st ////////////////////////
            //デバッグ時のCドライブ起動確認を抑制


#if (!DEBUG)


            var src = Utility.StartupPath;
            if (src.Remove(3) != "C:\\")
            {
                if (System.IO.Directory.Exists("C:\\Mejor\\"))
                {
                    //コピーが完了している＆Cドライブ以外から始めたとき、終了する
                    MessageBox.Show("このバージョンの「Mejor」はCドライブから起動する必要があります。" +
                        "Mejorを起動する際は新たにデスクトップに作成されている" +
                        "\r\n\r\n「Mejor-C」\r\n\r\n" +
                        "のショートカットをご利用ください。ショートカットがない場合、" +
                        "または特殊な環境が必要な場合は" +
                        "システム担当者までお問い合わせください。", "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
                else
                {
                    //スタートがCドライブでないときはCドライブにすべてコピー
                    using (var f = new WaitForm())
                    {
                        //フォルダ全コピー
                        f.ShowDialogOtherTask();
                        f.LogPrint("プログラム一式をCドライブにコピーしています");
                        dirCopy(src, "C:\\Mejor");

                        //デスクトップにショートカットファイルコピー
                        var linkFn = src + "Mejor-C.lnk";
                        var deskFn = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\Mejor-C.lnk";
                        File.Copy(linkFn, deskFn, true);
                    }

                    MessageBox.Show("Cドライブへ「Mejor」のコピーを完了しました。" +
                        "今後Mejorを起動する際は新たにデスクトップに作成されている" +
                        "\r\n\r\n「Mejor-C」\r\n\r\n" +
                        "のショートカットをご利用ください。ショートカットがない場合、" +
                        "または特殊な環境が必要な場合は" +
                        "システム担当者までお問い合わせください。", "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }

                //プログラム終了
                try { Application.Exit(); }
                catch { try { Environment.Exit(0); } catch { /*握りつぶす*/ } }
            }
#endif
            //20190312104313 furukawa ed ////////////////////////



            //新バージョン確認
            var l = GetList();
            l = l.FindAll(i => i.IsForce);
            l.Sort((x, y) => x.CompareTo(y));

            var info = l.Last();
            var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path).ProductVersion;

            int Version1 = int.Parse(version.Split('.')[0]);
            int Version2 = int.Parse(version.Split('.')[1]);
            int Version3 = int.Parse(version.Split('.')[2]);
            int Version4 = int.Parse(version.Split('.')[3]);

            var exists =
                Version1 != info.Version1 ? Version1.CompareTo(info.Version1) :
                Version2 != info.Version2 ? Version2.CompareTo(info.Version2) :
                Version3 != info.Version3 ? Version3.CompareTo(info.Version3) :
                Version4.CompareTo(info.Version4);

            return exists < 0 ? info : null;
        }

        public void SetIsTest(bool isTest) => IsTest = isTest;

        public void SetIsFoce(bool isForce) => IsForce = isForce;

        public bool Update()
        {
            var db = new DB("jyusei");
            try
            {
                return db.Update(this);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }

        public bool Delete()
        {
            var db = new DB("jyusei");
            try
            {
                using (var tran = db.CreateTransaction())
                {
                    //ファイル削除
                    var sql = $"DELETE FROM versioninfo WHERE version='{Version}'";
                    if (!db.Excute(sql)) return false;

                    System.IO.Directory.Delete(Directory, true);
                    tran.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }
    }
}