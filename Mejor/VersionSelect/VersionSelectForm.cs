﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.VersionSelect
{
    public partial class VersionSelectForm : Form
    {
        BindingSource bs = new BindingSource();
        List<VersionInfo> verList;
        string versionStr;

        public VersionSelectForm()
        {
            InitializeComponent();

            var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var info = new System.IO.FileInfo(path);
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path);
            versionStr = version.ProductVersion;
            labelVersionInfo.Text = "バージョン: "+versionStr;
            labelVersionDate.Text = "日時: " + info.LastWriteTime.ToString("yyyy/MM/dd HH:mm");
            labelDir.Text = "フルパス: " + path;

            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridView1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView1.DefaultCellStyle.Padding = new Padding(1);
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;

            verList = VersionInfo.GetList();
            verList.Sort((x, y) => y.CompareTo(x));
            bs.DataSource = verList;
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(VersionInfo.Version1)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.Version2)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.Version3)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.Version4)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.IsTest)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.IsForce)].Visible = false;
            dataGridView1.Columns[nameof(VersionInfo.Directory)].Visible = false;

            dataGridView1.Columns[nameof(VersionInfo.Version)].HeaderText = "バージョン";
            dataGridView1.Columns[nameof(VersionInfo.Version)].Width = 100;
            dataGridView1.Columns[nameof(VersionInfo.CreateDT)].HeaderText = "作成日";
            dataGridView1.Columns[nameof(VersionInfo.CreateDT)].Width = 110;
            dataGridView1.Columns[nameof(VersionInfo.Description)].HeaderText = "変更点";
            dataGridView1.Columns[nameof(VersionInfo.Description)].Width = 400;
            dataGridView1.Columns[nameof(VersionInfo.IsTestSign)].HeaderText = "テスト";
            dataGridView1.Columns[nameof(VersionInfo.IsTestSign)].Width = 60;
            dataGridView1.Columns[nameof(VersionInfo.IsTestSign)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns[nameof(VersionInfo.IsForceSign)].HeaderText = "強制";
            dataGridView1.Columns[nameof(VersionInfo.IsForceSign)].Width = 60;
            dataGridView1.Columns[nameof(VersionInfo.IsForceSign)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            if (User.CurrentUser.Developer)
            {
                buttonRegister.Visible = true;
                dataGridView1.ContextMenuStrip = contextMenuStrip1;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            var info = (VersionInfo)bs.Current;
            if (info == null) return;

            if (MessageBox.Show("選択中のバージョン" +
                $"{info.Version}に切り替えます。よろしいですか？",
                "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Asterisk) != DialogResult.OK) return;

            var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path).ProductVersion;

            int Version1 = int.Parse(version.Split('.')[0]);
            int Version2 = int.Parse(version.Split('.')[1]);
            int Version3 = int.Parse(version.Split('.')[2]);
            int Version4 = int.Parse(version.Split('.')[3]);

            if((Version1 != info.Version1 ? Version1.CompareTo(info.Version1) :
                Version2 != info.Version2 ? Version2.CompareTo(info.Version2) :
                Version3 != info.Version3 ? Version3.CompareTo(info.Version3) :
                Version4.CompareTo(info.Version4)) >= 0)
            {
                if (MessageBox.Show("選択中のバージョン" +
                    $"\r\n{info.Version}\r\n" +
                    $"は現在のバージョン" +
                    $"\r\n{version}\r\nよりも古いバージョンです。" +
                    $"本当に切り替えてもよろしいですか？",
                    "", MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation) != DialogResult.OK) return;
            }

            if (info.IsTest)
            {
                if (MessageBox.Show("選択中のバージョン" +
                    $"{info.Version}はテストバージョンです。通常切り替える必要はありません。" +
                    $"本当に切り替えてもよろしいですか？",
                    "", MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation) != DialogResult.OK) return;
            }

            info.Change();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            if (verList.Exists(v => v.Version == versionStr))
            {
                MessageBox.Show("現在使用中のバージョンはすでに登録されています",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            using (var f = new VersionRegisterForm())
            {
                f.ShowDialog();
            }

            showList();
        }

        private void showList()
        {
            verList = VersionInfo.GetList();
            verList.Sort((x, y) => y.CompareTo(x));
            bs.DataSource = verList;
            bs.ResetBindings(false);
        }

        private void テストフラグ変更ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var vi = (VersionInfo)bs.Current;
            if (vi == null) return;

            if (MessageBox.Show("フラグを変更してよろしいですか？",
                "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation) != DialogResult.OK) return;

            vi.SetIsTest(!vi.IsTest);
            vi.Update();
            showList();
        }

        private void 強制フラグ変更ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var vi = (VersionInfo)bs.Current;
            if (vi == null) return;

            if (MessageBox.Show("フラグを変更してよろしいですか？",
                "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation) != DialogResult.OK) return;

            vi.SetIsFoce(!vi.IsForce);
            vi.Update();
            showList();
        }

        private void 削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var vi = (VersionInfo)bs.Current;
            if (vi == null) return;

            if (MessageBox.Show("このバージョンを削除してよろしいですか？",
                "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation) != DialogResult.OK) return;
            
            vi.Delete();
            showList();
        }

        private void VersionSelectForm_Shown(object sender, EventArgs e)
        {
            //強制アップデート
            var info = VersionSelect.VersionInfo.ExistsForceUpdate();
            if (info != null)
            {
                var res = MessageBox.Show("アップデートが必要です。" +
                        "OKボタンを押してアップデートを実行してください。",
                        "アップデート確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (res != DialogResult.OK)
                {
                    res = MessageBox.Show("再確認\r\n\r\n" +
                        "アップデートが必要です。" +
                        "特別な理由がない限り、OKボタンを押してアップデートを実行してください。",
                        "アップデート再確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                    if (res != DialogResult.OK) return;
                }

                info.Change();
            }
        }

        private void 対象フォルダを開くToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var vi = (VersionInfo)bs.Current;
            if (vi == null) return;

            System.Diagnostics.Process.Start(vi.Directory);
        }


        //20190513172401 furukawa 文言コピー機能
        private void コピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int r = dataGridView1.CurrentRow.Index;
            int c = dataGridView1.CurrentCell.ColumnIndex;
            if (r < 0 || c < 0) return;

            System.Windows.Forms.Clipboard.SetText(dataGridView1.Rows[r].Cells[c].Value.ToString());
        }

        //
        
        /// <summary>
        /// 20220227183744 furukawa 外部業者用にバージョンアップファイル作成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExternal_Click(object sender, EventArgs e)
        {

            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            if (odd.ShowDialog() == DialogResult.Cancel) return;
            string strPath = odd.Name;
            
            string strBatchFileName = $"{strPath}\\versionup.bat";

            var info = (VersionInfo)bs.Current;
            string version = info.Version;
            DateTime createdt = info.CreateDT;
            bool istest = info.IsTest;
            bool isforce = info.IsForce;
            string desc = info.Description;

            System.IO.StreamWriter sw = new System.IO.StreamWriter(strBatchFileName, false, System.Text.Encoding.GetEncoding("shift-jis"));
            try
            {
           
                var dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);//実行ファイルのフォルダ
             
                //7zで自己解凍書庫作ってバージョンフォルダに移動
                string str7z = $"{dir}\\Archive\\7z.exe";
                string strArchiveName = $"{strPath}\\versionup_{version}_{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}.exe";

                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = str7z;
                si.Arguments = $"a -sfx {strArchiveName} {Settings.VersionFolder}\\{version} -xr!*.xml -xr!*.pdb -xr!*.log";//xml,pdb,log除外
                si.UseShellExecute = false;
                si.CreateNoWindow = true;
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = si;
                p.Start();
                p.WaitForExit();
                p.Close();



                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"@echo off");
                sb.AppendLine("echo expand archive versionup");
                sb.AppendLine($"{System.IO.Path.GetFileName(strArchiveName)} -oC:\\public\\JuseiSystem\\MejorVersions");
                
                sb.AppendLine("echo versionup record insert");
                sb.AppendLine($"cd /d c:\\program files\\postgresql\\11\\bin");
                sb.Append($"psql -h localhost -p 5432 -U postgres -d jyusei --command ");
                sb.AppendLine($"\" insert into versioninfo values ('{version}','{createdt}','{istest}','{isforce}','{desc}')\"");
                sb.AppendLine("pause");

                sw.Write(sb.ToString());

                MessageBox.Show("保存完了");
                System.Diagnostics.Process.Start(strPath);

            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                
            }
            finally
            {
                sw.Close();
            }

        }
    }
}
