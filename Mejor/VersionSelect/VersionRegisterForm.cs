﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.VersionSelect
{
    public partial class VersionRegisterForm : Form
    {
        string versionStr;

        public VersionRegisterForm()
        {
            InitializeComponent();

            var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var info = new System.IO.FileInfo(path);
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(path);
            versionStr = version.ProductVersion;
            labelVersionInfo.Text = versionStr;
            labelVersionDate.Text = info.LastWriteTime.ToString("yyyy/MM/dd HH:mm");

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            var res = VersionInfo.OwnFileRegister(checkBox1.Checked, checkBox2.Checked, textBox1.Text.Trim());
            if(res)
            {
                MessageBox.Show("登録に成功しました");
                Close();
            }
            else
            {
                MessageBox.Show("登録に失敗しました");
            }
        }
    }
}
