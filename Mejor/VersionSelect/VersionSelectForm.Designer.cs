﻿namespace Mejor.VersionSelect
{
    partial class VersionSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelVersionInfo = new System.Windows.Forms.Label();
            this.labelVersionDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonChange = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.labelDir = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.テストフラグ変更ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.強制フラグ変更ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.対象フォルダを開くToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.削除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.コピーToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonExternal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(691, 53);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelVersionInfo
            // 
            this.labelVersionInfo.AutoSize = true;
            this.labelVersionInfo.Location = new System.Drawing.Point(194, 20);
            this.labelVersionInfo.Name = "labelVersionInfo";
            this.labelVersionInfo.Size = new System.Drawing.Size(50, 12);
            this.labelVersionInfo.TabIndex = 2;
            this.labelVersionInfo.Text = "バージョン";
            // 
            // labelVersionDate
            // 
            this.labelVersionDate.AutoSize = true;
            this.labelVersionDate.Location = new System.Drawing.Point(194, 41);
            this.labelVersionDate.Name = "labelVersionDate";
            this.labelVersionDate.Size = new System.Drawing.Size(29, 12);
            this.labelVersionDate.TabIndex = 2;
            this.labelVersionDate.Text = "日時";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "MEJOR";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Mejor.Properties.Resources.mejoricon64;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "切替可能バージョン一覧";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 124);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(754, 229);
            this.dataGridView1.TabIndex = 5;
            // 
            // buttonChange
            // 
            this.buttonChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChange.Location = new System.Drawing.Point(692, 360);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(75, 23);
            this.buttonChange.TabIndex = 6;
            this.buttonChange.Text = "切り替え";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(13, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(753, 1);
            this.label4.TabIndex = 7;
            // 
            // buttonRegister
            // 
            this.buttonRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegister.Location = new System.Drawing.Point(13, 360);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(75, 23);
            this.buttonRegister.TabIndex = 6;
            this.buttonRegister.Text = "登録";
            this.buttonRegister.UseVisualStyleBackColor = true;
            this.buttonRegister.Visible = false;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelDir
            // 
            this.labelDir.AutoSize = true;
            this.labelDir.Location = new System.Drawing.Point(194, 62);
            this.labelDir.Name = "labelDir";
            this.labelDir.Size = new System.Drawing.Size(42, 12);
            this.labelDir.TabIndex = 2;
            this.labelDir.Text = "フルパス";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.テストフラグ変更ToolStripMenuItem,
            this.強制フラグ変更ToolStripMenuItem,
            this.対象フォルダを開くToolStripMenuItem,
            this.toolStripSeparator1,
            this.削除ToolStripMenuItem,
            this.コピーToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 120);
            // 
            // テストフラグ変更ToolStripMenuItem
            // 
            this.テストフラグ変更ToolStripMenuItem.Name = "テストフラグ変更ToolStripMenuItem";
            this.テストフラグ変更ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.テストフラグ変更ToolStripMenuItem.Text = "テストフラグ変更";
            this.テストフラグ変更ToolStripMenuItem.Click += new System.EventHandler(this.テストフラグ変更ToolStripMenuItem_Click);
            // 
            // 強制フラグ変更ToolStripMenuItem
            // 
            this.強制フラグ変更ToolStripMenuItem.Name = "強制フラグ変更ToolStripMenuItem";
            this.強制フラグ変更ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.強制フラグ変更ToolStripMenuItem.Text = "強制フラグ変更";
            this.強制フラグ変更ToolStripMenuItem.Click += new System.EventHandler(this.強制フラグ変更ToolStripMenuItem_Click);
            // 
            // 対象フォルダを開くToolStripMenuItem
            // 
            this.対象フォルダを開くToolStripMenuItem.Name = "対象フォルダを開くToolStripMenuItem";
            this.対象フォルダを開くToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.対象フォルダを開くToolStripMenuItem.Text = "対象フォルダを開く";
            this.対象フォルダを開くToolStripMenuItem.Click += new System.EventHandler(this.対象フォルダを開くToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(158, 6);
            // 
            // 削除ToolStripMenuItem
            // 
            this.削除ToolStripMenuItem.Name = "削除ToolStripMenuItem";
            this.削除ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.削除ToolStripMenuItem.Text = "削除";
            this.削除ToolStripMenuItem.Click += new System.EventHandler(this.削除ToolStripMenuItem_Click);
            // 
            // コピーToolStripMenuItem
            // 
            this.コピーToolStripMenuItem.Name = "コピーToolStripMenuItem";
            this.コピーToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.コピーToolStripMenuItem.Text = "コピー";
            this.コピーToolStripMenuItem.Click += new System.EventHandler(this.コピーToolStripMenuItem_Click);
            // 
            // buttonExternal
            // 
            this.buttonExternal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExternal.Location = new System.Drawing.Point(95, 360);
            this.buttonExternal.Name = "buttonExternal";
            this.buttonExternal.Size = new System.Drawing.Size(166, 23);
            this.buttonExternal.TabIndex = 6;
            this.buttonExternal.Text = "外部業者用登録セット出力";
            this.buttonExternal.UseVisualStyleBackColor = true;
            this.buttonExternal.Click += new System.EventHandler(this.buttonExternal_Click);
            // 
            // VersionSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 388);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonExternal);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.buttonChange);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelDir);
            this.Controls.Add(this.labelVersionDate);
            this.Controls.Add(this.labelVersionInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonOK);
            this.Name = "VersionSelectForm";
            this.Text = "バージョン情報・管理 / 切替";
            this.Shown += new System.EventHandler(this.VersionSelectForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelVersionInfo;
        private System.Windows.Forms.Label labelVersionDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonRegister;
        private System.Windows.Forms.Label labelDir;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem テストフラグ変更ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 強制フラグ変更ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 削除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 対象フォルダを開くToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem コピーToolStripMenuItem;
        private System.Windows.Forms.Button buttonExternal;
    }
}