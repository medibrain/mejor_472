﻿namespace Mejor.GaimusyoRyouyouhi
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelShobyo = new System.Windows.Forms.Panel();
            this.verifyBoxFlg = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.verifyBoxRyouyoukbn = new Mejor.VerifyBox();
            this.verifyBoxMonths = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.verifyBoxCurrencyCode = new Mejor.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelF1 = new System.Windows.Forms.Label();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxF1StY = new Mejor.VerifyBox();
            this.verifyBoxF2StY = new Mejor.VerifyBox();
            this.verifyBoxF1EdY = new Mejor.VerifyBox();
            this.verifyBoxCymY = new Mejor.VerifyBox();
            this.verifyBoxF2EdY = new Mejor.VerifyBox();
            this.verifyBoxF1StM = new Mejor.VerifyBox();
            this.verifyBoxF2StM = new Mejor.VerifyBox();
            this.verifyBoxCymM = new Mejor.VerifyBox();
            this.verifyBoxF1EdM = new Mejor.VerifyBox();
            this.verifyBoxF2EdM = new Mejor.VerifyBox();
            this.verifyBoxF1StD = new Mejor.VerifyBox();
            this.verifyBoxF2StD = new Mejor.VerifyBox();
            this.verifyBoxCymD = new Mejor.VerifyBox();
            this.verifyBoxF1EdD = new Mejor.VerifyBox();
            this.verifyBoxF2EdD = new Mejor.VerifyBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.verifyBoxZoku = new Mejor.VerifyBox();
            this.label42 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxPName = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBoxShokuin = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxKokan = new Mejor.VerifyBox();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxHonsho = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelYear = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.labelM = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelShobyo.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 695);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 250;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(6, 9);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(55, 36);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++\r\n通常:空欄";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(224, 18);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(65, 12);
            this.labelTotal.TabIndex = 33;
            this.labelTotal.Text = "本省記入欄";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(107, 722);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 681);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 681);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 681);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(10, 681);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelShobyo);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.verifyBoxZoku);
            this.panelRight.Controls.Add(this.label42);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.label21);
            this.panelRight.Controls.Add(this.label36);
            this.panelRight.Controls.Add(this.label37);
            this.panelRight.Controls.Add(this.verifyBoxBirthY);
            this.panelRight.Controls.Add(this.label38);
            this.panelRight.Controls.Add(this.verifyBoxBirthM);
            this.panelRight.Controls.Add(this.verifyBoxBirthD);
            this.panelRight.Controls.Add(this.label39);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxPName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.verifyBoxShokuin);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.verifyBoxKokan);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.verifyBoxHonsho);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 722);
            this.panelRight.TabIndex = 0;
            // 
            // panelShobyo
            // 
            this.panelShobyo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelShobyo.Controls.Add(this.verifyBoxFlg);
            this.panelShobyo.Controls.Add(this.label26);
            this.panelShobyo.Controls.Add(this.label25);
            this.panelShobyo.Controls.Add(this.verifyBoxRyouyoukbn);
            this.panelShobyo.Controls.Add(this.verifyBoxMonths);
            this.panelShobyo.Controls.Add(this.verifyBoxDays);
            this.panelShobyo.Controls.Add(this.verifyBoxTotal);
            this.panelShobyo.Controls.Add(this.label8);
            this.panelShobyo.Controls.Add(this.verifyBoxCurrencyCode);
            this.panelShobyo.Controls.Add(this.label27);
            this.panelShobyo.Controls.Add(this.label16);
            this.panelShobyo.Controls.Add(this.label6);
            this.panelShobyo.Controls.Add(this.label2);
            this.panelShobyo.Controls.Add(this.label1);
            this.panelShobyo.Controls.Add(this.labelF1);
            this.panelShobyo.Controls.Add(this.verifyBoxF1);
            this.panelShobyo.Controls.Add(this.verifyBoxF1StY);
            this.panelShobyo.Controls.Add(this.verifyBoxF2StY);
            this.panelShobyo.Controls.Add(this.verifyBoxF1EdY);
            this.panelShobyo.Controls.Add(this.verifyBoxCymY);
            this.panelShobyo.Controls.Add(this.verifyBoxF2EdY);
            this.panelShobyo.Controls.Add(this.verifyBoxF1StM);
            this.panelShobyo.Controls.Add(this.verifyBoxF2StM);
            this.panelShobyo.Controls.Add(this.verifyBoxCymM);
            this.panelShobyo.Controls.Add(this.verifyBoxF1EdM);
            this.panelShobyo.Controls.Add(this.verifyBoxF2EdM);
            this.panelShobyo.Controls.Add(this.verifyBoxF1StD);
            this.panelShobyo.Controls.Add(this.verifyBoxF2StD);
            this.panelShobyo.Controls.Add(this.verifyBoxCymD);
            this.panelShobyo.Controls.Add(this.verifyBoxF1EdD);
            this.panelShobyo.Controls.Add(this.verifyBoxF2EdD);
            this.panelShobyo.Controls.Add(this.label20);
            this.panelShobyo.Controls.Add(this.label23);
            this.panelShobyo.Controls.Add(this.label22);
            this.panelShobyo.Controls.Add(this.label9);
            this.panelShobyo.Controls.Add(this.label7);
            this.panelShobyo.Controls.Add(this.label47);
            this.panelShobyo.Controls.Add(this.label11);
            this.panelShobyo.Controls.Add(this.label45);
            this.panelShobyo.Controls.Add(this.label15);
            this.panelShobyo.Controls.Add(this.label44);
            this.panelShobyo.Controls.Add(this.label14);
            this.panelShobyo.Controls.Add(this.label17);
            this.panelShobyo.Controls.Add(this.label19);
            this.panelShobyo.Controls.Add(this.label40);
            this.panelShobyo.Controls.Add(this.label33);
            this.panelShobyo.Controls.Add(this.label13);
            this.panelShobyo.Controls.Add(this.label12);
            this.panelShobyo.Controls.Add(this.label10);
            this.panelShobyo.Controls.Add(this.label24);
            this.panelShobyo.Controls.Add(this.label18);
            this.panelShobyo.Location = new System.Drawing.Point(6, 513);
            this.panelShobyo.Name = "panelShobyo";
            this.panelShobyo.Size = new System.Drawing.Size(1000, 175);
            this.panelShobyo.TabIndex = 45;
            // 
            // verifyBoxFlg
            // 
            this.verifyBoxFlg.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFlg.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFlg.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFlg.Location = new System.Drawing.Point(550, 130);
            this.verifyBoxFlg.MaxLength = 1;
            this.verifyBoxFlg.Name = "verifyBoxFlg";
            this.verifyBoxFlg.NewLine = false;
            this.verifyBoxFlg.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxFlg.TabIndex = 60;
            this.verifyBoxFlg.Text = "0";
            this.verifyBoxFlg.TextV = "";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(584, 124);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 39);
            this.label26.TabIndex = 58;
            this.label26.Text = "給付可能：0\r\n返送：1\r\n留保：2";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label25.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label25.Location = new System.Drawing.Point(105, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(119, 26);
            this.label25.TabIndex = 58;
            this.label25.Text = "1：入院　2：外来\r\n3：歯科　4：移送";
            // 
            // verifyBoxRyouyoukbn
            // 
            this.verifyBoxRyouyoukbn.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRyouyoukbn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRyouyoukbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRyouyoukbn.Location = new System.Drawing.Point(71, 50);
            this.verifyBoxRyouyoukbn.MaxLength = 1;
            this.verifyBoxRyouyoukbn.Name = "verifyBoxRyouyoukbn";
            this.verifyBoxRyouyoukbn.NewLine = false;
            this.verifyBoxRyouyoukbn.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxRyouyoukbn.TabIndex = 10;
            this.verifyBoxRyouyoukbn.TextV = "";
            // 
            // verifyBoxMonths
            // 
            this.verifyBoxMonths.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMonths.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMonths.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMonths.Location = new System.Drawing.Point(304, 50);
            this.verifyBoxMonths.MaxLength = 2;
            this.verifyBoxMonths.Name = "verifyBoxMonths";
            this.verifyBoxMonths.NewLine = false;
            this.verifyBoxMonths.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxMonths.TabIndex = 13;
            this.verifyBoxMonths.TextV = "";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(412, 50);
            this.verifyBoxDays.MaxLength = 2;
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxDays.TabIndex = 15;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(725, 49);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxTotal.TabIndex = 50;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(664, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 39;
            this.label8.Text = "支払金額";
            // 
            // verifyBoxCurrencyCode
            // 
            this.verifyBoxCurrencyCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCurrencyCode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCurrencyCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCurrencyCode.Location = new System.Drawing.Point(578, 50);
            this.verifyBoxCurrencyCode.Name = "verifyBoxCurrencyCode";
            this.verifyBoxCurrencyCode.NewLine = false;
            this.verifyBoxCurrencyCode.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxCurrencyCode.TabIndex = 45;
            this.verifyBoxCurrencyCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxCurrencyCode.TextV = "";
            this.verifyBoxCurrencyCode.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(513, 136);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 37;
            this.label27.Text = "分類";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(513, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 12);
            this.label16.TabIndex = 37;
            this.label16.Text = "請求年月日 西暦";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(490, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 12);
            this.label6.TabIndex = 37;
            this.label6.Text = "支払通貨コード";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "入院期間 西暦";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "治癒期間 西暦";
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(9, 14);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(41, 12);
            this.labelF1.TabIndex = 0;
            this.labelF1.Text = "傷病名";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(56, 8);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(800, 23);
            this.verifyBoxF1.TabIndex = 6;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1StY
            // 
            this.verifyBoxF1StY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1StY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1StY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1StY.Location = new System.Drawing.Point(99, 102);
            this.verifyBoxF1StY.MaxLength = 4;
            this.verifyBoxF1StY.Name = "verifyBoxF1StY";
            this.verifyBoxF1StY.NewLine = false;
            this.verifyBoxF1StY.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxF1StY.TabIndex = 20;
            this.verifyBoxF1StY.TextV = "";
            this.verifyBoxF1StY.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxF1StY.Validated += new System.EventHandler(this.verifyBoxF1StY_Validated);
            // 
            // verifyBoxF2StY
            // 
            this.verifyBoxF2StY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2StY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2StY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2StY.Location = new System.Drawing.Point(99, 127);
            this.verifyBoxF2StY.MaxLength = 4;
            this.verifyBoxF2StY.Name = "verifyBoxF2StY";
            this.verifyBoxF2StY.NewLine = false;
            this.verifyBoxF2StY.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxF2StY.TabIndex = 30;
            this.verifyBoxF2StY.TextV = "";
            this.verifyBoxF2StY.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxF2StY.Validated += new System.EventHandler(this.verifyBoxF1StY_Validated);
            // 
            // verifyBoxF1EdY
            // 
            this.verifyBoxF1EdY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1EdY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1EdY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1EdY.Location = new System.Drawing.Point(294, 101);
            this.verifyBoxF1EdY.MaxLength = 4;
            this.verifyBoxF1EdY.Name = "verifyBoxF1EdY";
            this.verifyBoxF1EdY.NewLine = false;
            this.verifyBoxF1EdY.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxF1EdY.TabIndex = 23;
            this.verifyBoxF1EdY.TextV = "";
            this.verifyBoxF1EdY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxCymY
            // 
            this.verifyBoxCymY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCymY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCymY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCymY.Location = new System.Drawing.Point(611, 95);
            this.verifyBoxCymY.MaxLength = 4;
            this.verifyBoxCymY.Name = "verifyBoxCymY";
            this.verifyBoxCymY.NewLine = false;
            this.verifyBoxCymY.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxCymY.TabIndex = 55;
            this.verifyBoxCymY.TextV = "";
            this.verifyBoxCymY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2EdY
            // 
            this.verifyBoxF2EdY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2EdY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2EdY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2EdY.Location = new System.Drawing.Point(294, 126);
            this.verifyBoxF2EdY.MaxLength = 4;
            this.verifyBoxF2EdY.Name = "verifyBoxF2EdY";
            this.verifyBoxF2EdY.NewLine = false;
            this.verifyBoxF2EdY.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxF2EdY.TabIndex = 33;
            this.verifyBoxF2EdY.TextV = "";
            this.verifyBoxF2EdY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1StM
            // 
            this.verifyBoxF1StM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1StM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1StM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1StM.Location = new System.Drawing.Point(166, 102);
            this.verifyBoxF1StM.MaxLength = 2;
            this.verifyBoxF1StM.Name = "verifyBoxF1StM";
            this.verifyBoxF1StM.NewLine = false;
            this.verifyBoxF1StM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1StM.TabIndex = 21;
            this.verifyBoxF1StM.TextV = "";
            this.verifyBoxF1StM.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxF1StM.Validated += new System.EventHandler(this.verifyBoxF1StY_Validated);
            // 
            // verifyBoxF2StM
            // 
            this.verifyBoxF2StM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2StM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2StM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2StM.Location = new System.Drawing.Point(166, 127);
            this.verifyBoxF2StM.MaxLength = 2;
            this.verifyBoxF2StM.Name = "verifyBoxF2StM";
            this.verifyBoxF2StM.NewLine = false;
            this.verifyBoxF2StM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2StM.TabIndex = 31;
            this.verifyBoxF2StM.TextV = "";
            this.verifyBoxF2StM.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxF2StM.Validated += new System.EventHandler(this.verifyBoxF1StY_Validated);
            // 
            // verifyBoxCymM
            // 
            this.verifyBoxCymM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCymM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCymM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCymM.Location = new System.Drawing.Point(684, 95);
            this.verifyBoxCymM.MaxLength = 2;
            this.verifyBoxCymM.Name = "verifyBoxCymM";
            this.verifyBoxCymM.NewLine = false;
            this.verifyBoxCymM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxCymM.TabIndex = 56;
            this.verifyBoxCymM.TextV = "";
            this.verifyBoxCymM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1EdM
            // 
            this.verifyBoxF1EdM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1EdM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1EdM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1EdM.Location = new System.Drawing.Point(367, 101);
            this.verifyBoxF1EdM.MaxLength = 2;
            this.verifyBoxF1EdM.Name = "verifyBoxF1EdM";
            this.verifyBoxF1EdM.NewLine = false;
            this.verifyBoxF1EdM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1EdM.TabIndex = 24;
            this.verifyBoxF1EdM.TextV = "";
            this.verifyBoxF1EdM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2EdM
            // 
            this.verifyBoxF2EdM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2EdM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2EdM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2EdM.Location = new System.Drawing.Point(367, 126);
            this.verifyBoxF2EdM.MaxLength = 2;
            this.verifyBoxF2EdM.Name = "verifyBoxF2EdM";
            this.verifyBoxF2EdM.NewLine = false;
            this.verifyBoxF2EdM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2EdM.TabIndex = 34;
            this.verifyBoxF2EdM.TextV = "";
            this.verifyBoxF2EdM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1StD
            // 
            this.verifyBoxF1StD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1StD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1StD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1StD.Location = new System.Drawing.Point(216, 102);
            this.verifyBoxF1StD.MaxLength = 2;
            this.verifyBoxF1StD.Name = "verifyBoxF1StD";
            this.verifyBoxF1StD.NewLine = false;
            this.verifyBoxF1StD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1StD.TabIndex = 22;
            this.verifyBoxF1StD.TextV = "";
            this.verifyBoxF1StD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2StD
            // 
            this.verifyBoxF2StD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2StD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2StD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2StD.Location = new System.Drawing.Point(216, 127);
            this.verifyBoxF2StD.MaxLength = 2;
            this.verifyBoxF2StD.Name = "verifyBoxF2StD";
            this.verifyBoxF2StD.NewLine = false;
            this.verifyBoxF2StD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2StD.TabIndex = 32;
            this.verifyBoxF2StD.TextV = "";
            this.verifyBoxF2StD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxCymD
            // 
            this.verifyBoxCymD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCymD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCymD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCymD.Location = new System.Drawing.Point(734, 95);
            this.verifyBoxCymD.MaxLength = 2;
            this.verifyBoxCymD.Name = "verifyBoxCymD";
            this.verifyBoxCymD.NewLine = false;
            this.verifyBoxCymD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxCymD.TabIndex = 57;
            this.verifyBoxCymD.TextV = "";
            this.verifyBoxCymD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1EdD
            // 
            this.verifyBoxF1EdD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1EdD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1EdD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1EdD.Location = new System.Drawing.Point(417, 101);
            this.verifyBoxF1EdD.MaxLength = 2;
            this.verifyBoxF1EdD.Name = "verifyBoxF1EdD";
            this.verifyBoxF1EdD.NewLine = false;
            this.verifyBoxF1EdD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1EdD.TabIndex = 25;
            this.verifyBoxF1EdD.TextV = "";
            this.verifyBoxF1EdD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2EdD
            // 
            this.verifyBoxF2EdD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2EdD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2EdD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2EdD.Location = new System.Drawing.Point(417, 126);
            this.verifyBoxF2EdD.MaxLength = 2;
            this.verifyBoxF2EdD.Name = "verifyBoxF2EdD";
            this.verifyBoxF2EdD.NewLine = false;
            this.verifyBoxF2EdD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2EdD.TabIndex = 35;
            this.verifyBoxF2EdD.TextV = "";
            this.verifyBoxF2EdD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 4;
            this.label20.Text = "療養区分";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(363, 54);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 4;
            this.label23.Text = "実日数";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(255, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 4;
            this.label22.Text = "実月数";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(293, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "終了日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(102, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "開始日";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(148, 113);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(17, 12);
            this.label47.TabIndex = 8;
            this.label47.Text = "年";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(347, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "年";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(148, 137);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(17, 12);
            this.label45.TabIndex = 26;
            this.label45.Text = "年";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(664, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 32;
            this.label15.Text = "年";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(244, 137);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(17, 12);
            this.label44.TabIndex = 30;
            this.label44.Text = "日";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(762, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 36;
            this.label14.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(347, 136);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 32;
            this.label17.Text = "年";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(445, 136);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 36;
            this.label19.Text = "日";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(244, 113);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 12);
            this.label40.TabIndex = 12;
            this.label40.Text = "日";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(194, 113);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 12);
            this.label33.TabIndex = 10;
            this.label33.Text = "月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(445, 112);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(395, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "月";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(712, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "月";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(194, 137);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 28;
            this.label24.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(395, 136);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "月";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 42;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // verifyBoxZoku
            // 
            this.verifyBoxZoku.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZoku.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZoku.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZoku.Location = new System.Drawing.Point(709, 60);
            this.verifyBoxZoku.Name = "verifyBoxZoku";
            this.verifyBoxZoku.NewLine = false;
            this.verifyBoxZoku.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxZoku.TabIndex = 40;
            this.verifyBoxZoku.TextV = "";
            this.verifyBoxZoku.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(676, 69);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 29;
            this.label42.Text = "続柄";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(404, 59);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 24);
            this.labelBirthday.TabIndex = 20;
            this.labelBirthday.Text = "生年月日\r\n西暦";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(773, 40);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 17;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(807, 29);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 30;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(743, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 26);
            this.label21.TabIndex = 19;
            this.label21.Text = "1：本人　2：配偶者\r\n3：子　　4：父母";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label36.Location = new System.Drawing.Point(839, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 26);
            this.label36.TabIndex = 19;
            this.label36.Text = "1：男\r\n2：女";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(514, 70);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 24;
            this.label37.Text = "年";
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(465, 59);
            this.verifyBoxBirthY.MaxLength = 4;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(44, 23);
            this.verifyBoxBirthY.TabIndex = 35;
            this.verifyBoxBirthY.TextV = "";
            this.verifyBoxBirthY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(565, 70);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 26;
            this.label38.Text = "月";
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(533, 59);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 36;
            this.verifyBoxBirthM.TextV = "";
            this.verifyBoxBirthM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(586, 59);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 37;
            this.verifyBoxBirthD.TextV = "";
            this.verifyBoxBirthD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(622, 70);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 28;
            this.label39.Text = "日";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 92);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 416);
            this.scrollPictureControl1.TabIndex = 37;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxPName
            // 
            this.verifyBoxPName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPName.Location = new System.Drawing.Point(465, 30);
            this.verifyBoxPName.Name = "verifyBoxPName";
            this.verifyBoxPName.NewLine = false;
            this.verifyBoxPName.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxPName.TabIndex = 25;
            this.verifyBoxPName.TextV = "";
            this.verifyBoxPName.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(392, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 33;
            this.label3.Text = "療養者氏名";
            // 
            // verifyBoxShokuin
            // 
            this.verifyBoxShokuin.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxShokuin.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxShokuin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxShokuin.Location = new System.Drawing.Point(297, 36);
            this.verifyBoxShokuin.MaxLength = 10;
            this.verifyBoxShokuin.Name = "verifyBoxShokuin";
            this.verifyBoxShokuin.NewLine = false;
            this.verifyBoxShokuin.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxShokuin.TabIndex = 15;
            this.verifyBoxShokuin.Text = "99999";
            this.verifyBoxShokuin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxShokuin.TextV = "";
            this.verifyBoxShokuin.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(233, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 12);
            this.label5.TabIndex = 33;
            this.label5.Text = "職員コード";
            // 
            // verifyBoxKokan
            // 
            this.verifyBoxKokan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKokan.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKokan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKokan.Location = new System.Drawing.Point(297, 63);
            this.verifyBoxKokan.Name = "verifyBoxKokan";
            this.verifyBoxKokan.NewLine = false;
            this.verifyBoxKokan.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxKokan.TabIndex = 20;
            this.verifyBoxKokan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKokan.TextV = "";
            this.verifyBoxKokan.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxKokan.Validated += new System.EventHandler(this.verifyBoxKokan_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(233, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 12);
            this.label4.TabIndex = 33;
            this.label4.Text = "公館コード";
            // 
            // verifyBoxHonsho
            // 
            this.verifyBoxHonsho.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHonsho.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHonsho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHonsho.Location = new System.Drawing.Point(297, 9);
            this.verifyBoxHonsho.MaxLength = 10;
            this.verifyBoxHonsho.Name = "verifyBoxHonsho";
            this.verifyBoxHonsho.NewLine = false;
            this.verifyBoxHonsho.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxHonsho.TabIndex = 10;
            this.verifyBoxHonsho.Text = "1234";
            this.verifyBoxHonsho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHonsho.TextV = "";
            this.verifyBoxHonsho.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(97, 9);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(131, 20);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            this.labelYear.Visible = false;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(153, 9);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Visible = false;
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(190, 20);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            this.labelM.Visible = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelShobyo.ResumeLayout(false);
            this.panelShobyo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxHonsho;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxF2EdD;
        private VerifyBox verifyBoxF1EdD;
        private VerifyBox verifyBoxF2EdM;
        private VerifyBox verifyBoxF1EdM;
        private VerifyBox verifyBoxF2EdY;
        private VerifyBox verifyBoxF1EdY;
        private System.Windows.Forms.Panel panelShobyo;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxZoku;
        private System.Windows.Forms.Label label42;
        private VerifyBox verifyBoxF1StY;
        private VerifyBox verifyBoxF2StY;
        private VerifyBox verifyBoxF1StM;
        private VerifyBox verifyBoxF2StM;
        private VerifyBox verifyBoxF1StD;
        private VerifyBox verifyBoxF2StD;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label labelBirthday;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxCurrencyCode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxCymY;
        private VerifyBox verifyBoxCymM;
        private VerifyBox verifyBoxCymD;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private VerifyBox verifyBoxPName;
        private System.Windows.Forms.Label label3;
        private VerifyBox verifyBoxShokuin;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxKokan;
        private System.Windows.Forms.Label label4;
        private VerifyBox verifyBoxRyouyoukbn;
        private VerifyBox verifyBoxMonths;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelYear;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Label labelM;
        private VerifyBox verifyBoxFlg;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
    }
}