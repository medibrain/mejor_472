﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using Mejor.Pay;

namespace Mejor.GaimusyoRyouyouhi
{
    class PayCodeImporter
    {
        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "支払先コード|支払先コード*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データベースの情報を取り込んでいます");
                var dic = new Dictionary<string, PayCode>();
                var l = DB.Main.SelectAll<PayCode>();
                foreach (var item in l) dic.Add(item.Key, item);

                wf.LogPrint("Excelファイルを読み込んでいます");

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    var ex = System.IO.Path.GetExtension(fileName);

                    if (ex == ".xlsx")
                    {
                        var xls = new XSSFWorkbook(fs);

                        var sheet = xls.GetSheet("Sheet1");
                        var rowCount = sheet.LastRowNum + 2;

                        for (int i = 0; i < rowCount; i++)
                        {
                            try
                            {
                                var row = sheet.GetRow(i);
                                var prevlow = sheet.GetRow(i - 1);

                                var update = row.GetCell(1).StringCellValue.Contains("更新");

                                if (row.GetCell(1).CellType == CellType.Blank)
                                    continue;

                                var p = new Pay.PayCode();
                                p.Code = row.GetCell(5).CellType == CellType.String ?
                                    row.GetCell(5).StringCellValue.Trim() :
                                    row.GetCell(5).NumericCellValue.ToString();
                                p.UpdateDate = 20000101;
                                p.BankNo = row.GetCell(13).StringCellValue.Remove(4);
                                p.BankName = row.GetCell(13).StringCellValue.Substring(5).Trim();
                                p.BranchNo = row.GetCell(14).StringCellValue.Trim().Remove(3);
                                p.BranchName = row.GetCell(14).StringCellValue.Trim().Substring(5).Trim();
                                p.AccountNo = row.GetCell(6).CellType == CellType.String ?
                                    row.GetCell(6).StringCellValue.Trim().PadLeft(7, '0') :
                                    row.GetCell(6).NumericCellValue.ToString("0000000");
                                p.AccountName = row.GetCell(18).StringCellValue.Trim();
                                p.AccountKana = Utility.ToNarrowKatakana(row.GetCell(17).StringCellValue.Trim());

                                if (dic.ContainsKey(p.Key))
                                {
                                    if (dic[p.Key].UpdateDate > p.UpdateDate) continue;
                                    dic[p.Key] = p;
                                }
                                else
                                {
                                    dic.Add(p.Key, p);
                                }
                            }
                            catch
                            {
                                var res = MessageBox.Show($"{i + 1}行目のデータが読み込めませんでした。処理を続けますか？",
                                    "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                if (res != DialogResult.Yes) return false;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("対応しているのは新Excelファイル形式(.xlsx)です。");
                    }
                }

                wf.SetMax(dic.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {
                    if (!DB.Main.Excute("DELETE FROM paycode", null, tran))
                    {
                        MessageBox.Show("インポートに失敗しました");
                        return false;
                    }

                    var g = dic.Values
                        .Select((p, i) => new { Index = i, PayCode = p })
                        .GroupBy(x => x.Index / 10)
                        .Select(gr => gr.Select(x => x.PayCode));

                    foreach (var item in g)
                    {
                        wf.InvokeValue += item.Count();
                        if (!DB.Main.Inserts(item, tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
                MessageBox.Show("インポートが正常に終了しました");
            }

            return true;
        }

        public static List<PayCode> Select(string accountNo)
        {
            if (accountNo.Length < 7) return new List<PayCode>();
            return DB.Main.Select<PayCode>($"accountno='{accountNo}'").ToList();
        }
    }
}
