﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;


namespace Mejor.GaimusyoRyouyouhi
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 30);
        Point posPerson = new Point(60, 30);
        Point posTotal = new Point(800, 1800);
        Point posBuiName = new Point(120, 780);
        Point posBuiDate = new Point(950, 780);
        Point posNumbering = new Point(800, 30);
        Point posCharge = new Point(1000, 2000);

        Control[] ymConts, personConts, totalConts, ShobyoNameConts, ShobyoDateConts, chargeConts;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {

            InitializeComponent();

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            #region コントロールグループ化
            ymConts = new Control[] { verifyBoxY, verifyBoxM, };

            personConts = new Control[] { verifyBoxSex, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD
            ,verifyBoxZoku,verifyBoxPName,verifyBoxKokan,verifyBoxShokuin,verifyBoxHonsho
            };

            totalConts = new Control[] { verifyBoxCurrencyCode, verifyBoxTotal };

            ShobyoNameConts = new Control[] { verifyBoxF1, };

            ShobyoDateConts = new Control[] {
                verifyBoxF1EdY, verifyBoxF1EdM, verifyBoxF1EdD, verifyBoxF1StY, verifyBoxF1StM,verifyBoxF1StD,
                verifyBoxF2EdY, verifyBoxF2EdM, verifyBoxF2EdD, verifyBoxF2StY, verifyBoxF2StM,verifyBoxF2StD,
                 };
            chargeConts = new Control[] { verifyBoxCymY, verifyBoxCymM, verifyBoxCymD };
            #endregion


            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            #region 左のリスト
            //データリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 申請書移動時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        #region 登録
        /// <summary>
        /// 登録処理
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            #region 入力チェック
            //月

            int month = verifyBoxM.GetIntValue();
            //  setStatus(verifyBoxM, month < 1 || 12 < month);
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            setStatus(verifyBoxY, false);


            //本省記入欄
            int honsho = verifyBoxHonsho.GetIntValue();
            //          setStatus(verifyBoxHonsho, honsho != 1);

            //職員コード

            //20200427154004 furukawa st ////////////////////////
            //前ゼロがいるので文字列
            
            //int shokuinCD = verifyBoxShokuin.GetIntValue();
            string strShokuinCD = verifyBoxShokuin.Text.Trim();
            //20200427154004 furukawa ed ////////////////////////


            //            setStatus(verifyBoxShokuin, shokuinCD != 99999);
            //公館コード
            string strKokanCD = verifyBoxKokan.Text;
            setStatus(verifyBoxKokan, strKokanCD == string.Empty);
            //療養者氏名
            string strPname = verifyBoxPName.Text;
            setStatus(verifyBoxPName, strPname == string.Empty);
            //療養者性別
            int psex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, psex != 1 && psex != 2);


            //療養者生年月日年
            int intPBY = verifyBoxBirthY.GetIntValue();
            setStatus(verifyBoxBirthY, intPBY < 1900);
            //療養者生年月日月
            int intPBM = verifyBoxBirthM.GetIntValue();
            setStatus(verifyBoxBirthM, intPBM < 1 || intPBM > 12);
            //療養者生年月日日
            int intPBD = verifyBoxBirthD.GetIntValue();
            setStatus(verifyBoxBirthD, intPBD < 1 || intPBD > 31);

            //日付型チェック
            DateTime dtBirthday = chkDate(intPBY, intPBM, intPBD);
            //日付型であれば値は何が入ってもいいことにする
            //if (dtBirthday == DateTime.MinValue)
            //{
            //    setStatus(verifyBoxBirthY, true);
            //    setStatus(verifyBoxBirthM, true);
            //    setStatus(verifyBoxBirthD, true);
            //}


            //続柄
            int zoku = verifyBoxZoku.GetIntValue();
            setStatus(verifyBoxZoku, zoku < 1 || zoku > 4);
            //傷病名
            string strSick = verifyBoxF1.Text;
            setStatus(verifyBoxF1, strSick == string.Empty);
            //療養区分
            int ryouyouhiKubun = verifyBoxRyouyoukbn.GetIntValue();
            setStatus(verifyBoxRyouyoukbn, ryouyouhiKubun < 1 || ryouyouhiKubun > 4);
            //実月数
            int months = verifyBoxMonths.GetIntValue();
            setStatus(verifyBoxMonths, months < 0);
            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1);

            //治療開始年
            int intSTY = verifyBoxF1StY.GetIntValue();
            setStatus(verifyBoxF1StY, intSTY != -9 && (intSTY < 1900));
            //治療開始月
            int intSTM = verifyBoxF1StM.GetIntValue();
            setStatus(verifyBoxF1StM, intSTM != -9 && (intSTM < 1 || intSTM > 12));
            //治療開始日
            int intSTD = verifyBoxF1StD.GetIntValue();
            setStatus(verifyBoxF1StD, intSTD != -9 && (intSTD < 1 || intSTD > 31));


            //治療終了年
            int intEDY = verifyBoxF1EdY.GetIntValue();
            setStatus(verifyBoxF1EdY, intEDY != -9 && (intEDY < 1900));

            //治療終了月
            int intEDM = verifyBoxF1EdM.GetIntValue();
            setStatus(verifyBoxF1EdM, intEDM != -9 && (intEDM < 1 || intEDM > 12));

            //治療終了日
            int intEDD = verifyBoxF1EdD.GetIntValue();
            setStatus(verifyBoxF1EdD, intEDD !=-9 && (intEDD < 1 || intEDD > 31));




            //治療日付比較チェック

            DateTime dtST1 = chkDate(intSTY, intSTM, intSTD);
            DateTime dtED1 = chkDate(intEDY, intEDM, intEDD);

            if (dtST1 > dtED1)
            {
                setStatus(verifyBoxF1StY, true);
                setStatus(verifyBoxF1StM, true);
                setStatus(verifyBoxF1StD, true);
                setStatus(verifyBoxF1EdY, true);
                setStatus(verifyBoxF1EdM, true);
                setStatus(verifyBoxF1EdD, true);
            }


            //入院開始年
            int intNSTY = verifyBoxF2StY.GetIntValue();
            setStatus(verifyBoxF2StY, intNSTY != -9 && (intNSTY < 1900));

            //入院開始月
            int intNSTM = verifyBoxF2StM.GetIntValue();
            setStatus(verifyBoxF2StM, intNSTM != -9 && (intNSTM < 1 || intNSTM > 12));

            //入院開始日
            int intNSTD = verifyBoxF2StD.GetIntValue();
            setStatus(verifyBoxF2StD, intNSTD != -9 && (intNSTD < 1 || intNSTD > 31));



            //入院終了年
            int intNEDY = verifyBoxF2EdY.GetIntValue();
            setStatus(verifyBoxF2EdY, intNEDY != -9 && (intNEDY < 1900));

            //入院終了月
            int intNEDM = verifyBoxF2EdM.GetIntValue();
            setStatus(verifyBoxF2EdM, intNEDM != -9 && (intNEDM < 1 || intNEDM > 12));

            //入院終了日
            int intNEDD = verifyBoxF2EdD.GetIntValue();
            setStatus(verifyBoxF2EdD, intNEDD != -9 && (intNEDD < 1 || intNEDD > 31));


            //入院日付大小チェック
            DateTime dtNST = chkDate(intNSTY, intNSTM, intNSTD);
            DateTime dtNED = chkDate(intNEDY, intNEDM, intNEDD);

            if (dtNST > dtNED)
            {
                setStatus(verifyBoxF2StY, true);
                setStatus(verifyBoxF2StM, true);
                setStatus(verifyBoxF2StD, true);
                setStatus(verifyBoxF2EdY, true);
                setStatus(verifyBoxF2EdM, true);
                setStatus(verifyBoxF2EdD, true);
            }


            //支払通貨コード
            string strCurrCD = verifyBoxCurrencyCode.Text;
            setStatus(verifyBoxCurrencyCode, strCurrCD == string.Empty);

            //支払金額
            string strTotal = verifyBoxTotal.Text;
            decimal.TryParse(strTotal, out Decimal decTotal);
            setStatus(verifyBoxTotal, decTotal < 0);

            //請求年
            int intCYMY = verifyBoxCymY.GetIntValue();
            setStatus(verifyBoxCymY, intCYMY < 1900);

            //請求月
            int intCYMM = verifyBoxCymM.GetIntValue();
            setStatus(verifyBoxCymM, intCYMM < 1 || intCYMM > 12);


            //請求日
            int intCYMD = verifyBoxCymD.GetIntValue();
            setStatus(verifyBoxCymD, intCYMD < 1 || intCYMD > 31);



            DateTime dtCharge = chkDate(intCYMY, intCYMM, intCYMD);
            if (dtCharge == DateTime.MinValue)
            {
                setStatus(verifyBoxCymY, true);
                setStatus(verifyBoxCymM, true);
                setStatus(verifyBoxCymD, true);
            }



            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //20200427154407 furukawa st ////////////////////////
            //通常、返送、留保でソート順が決まるので追加
            
            //通常、返送、留保
            string strFlg = verifyBoxFlg.Text.Trim();
            setStatus(verifyBoxFlg, !new[]{ "0", "1", "2" }.Contains(strFlg));
            //20200427154407 furukawa ed ////////////////////////


            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            #endregion

            #region Appへの反映


            app.ClinicNum = honsho.ToString();          //本省記入欄

            //20200427154645 furukawa st ////////////////////////
            //前ゼロがいるので文字列で登録
            
            app.ClinicName = strShokuinCD.ToString();      //職員コード

            //app.ClinicName = shokuinCD.ToString();      //職員コード
            //20200427154645 furukawa ed ////////////////////////



            app.InsNum = strKokanCD;                     //公館コード
            app.PersonName = strPname;                  //療養者氏名
            app.Sex = psex;                             //療養者性別

            app.Birthday =dtBirthday;              //療養者生年月日

            app.Family = zoku;                  //続柄
            app.FushoName1 = strSick;           //傷病名
            app.HihoType = ryouyouhiKubun;      //療養区分
            app.FushoCourse1 = months;          //実月数
            app.FushoDays1 = days;              //実日数

            app.FushoStartDate1 = dtST1;	            //治療開始年月日            
            app.FushoFinishDate1 = dtED1;               //治療終了
            app.FushoStartDate2 = dtNST;	            //入院開始
            app.FushoFinishDate2 = dtNED;	            //入院終了
           

            app.TaggedDatas.GeneralString1 = strCurrCD;     //支払通貨コード

            //app.Total = int.Parse(strTotal);              //支払金額

            //20200514152645 furukawa st ////////////////////////
            //無いと思うが0の場合は0で登録、エクスポート時は空白
            
            //app.TaggedDatas.CalcDistance = double.Parse(strTotal);
            app.TaggedDatas.CalcDistance = strTotal == string.Empty ? 0: double.Parse(decTotal.ToString());
            //20200514152645 furukawa ed ////////////////////////

            app.FushoFinishDate5 = dtCharge;	            //請求年

            app.Sex = sex;              //性別


            //20200427154737 furukawa st ////////////////////////
            //通常、返送、留保でソート順が決まるので追加            
            app.Numbering = strFlg;
            //20200427154737 furukawa ed ////////////////////////
            #endregion



            return true;
        }


        /// <summary>
        /// 日付型にできるかチェック
        /// </summary>
        /// <param name="y">年</param>
        /// <param name="m">月</param>
        /// <param name="d">日</param>
        /// <returns>成功＝DateTime型、失敗＝DateTime.MinValue</returns>
        private DateTime chkDate(int y,int m,int d)
        {
            try
            {
                if (y > 0 && m > 0 && d > 0)
                {
                    if (DateTime.TryParse(new DateTime(y, m, d).ToString(), out DateTime dt)) return dt;
                }
                return DateTime.MinValue;
            }
            catch(Exception ex)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        #endregion

        #region ロード処理

        private void setApp(App app)
        {
            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //20201209105338 furukawa st ////////////////////////
            //コントロール使用可否の不具合対応
            
            if (!firstTime)
            {
                setInputedAppVerify(app);
            }
            else
            {
            //20201209105338 furukawa ed ////////////////////////


                //App_Flagのチェック
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    //setInputedApp(app);
                    setInputedAppVerify(app);
                }
            }
            //画像の表示
            setImage(app);
            changedReset(app);
        }


        //20200514155228 furukawa st ////////////////////////
        //2回入力にしたいという追加注文のため、関数全体を書き換え

        #region 未使用にした
        /// <summary>
        /// 2020/05/14未使用 チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        //private void setInputedApp(App app)
        //{


        //    if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
        //    var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


        //    if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
        //    {
        //        verifyBoxY.Text = "--";                
        //    }
        //    else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
        //    {
        //        verifyBoxY.Text = "++";
        //    }
        //    else
        //    {
        //        verifyBoxHonsho.Text = app.ClinicNum;                                           //本省記入欄
        //        verifyBoxShokuin.Text = app.ClinicName;                                         //職員コード
        //        verifyBoxKokan.Text = app.InsNum;                                               //公館コード
        //        verifyBoxPName.Text = app.PersonName;                                           //療養者氏名
        //        verifyBoxSex.Text = app.Sex.ToString();                                         //療養者性別
        //        verifyBoxBirthY.Text = app.Birthday.Year.ToString();                            //療養者生年月日年
        //        verifyBoxBirthM.Text = app.Birthday.Month.ToString();                           //療養者生年月日月
        //        verifyBoxBirthD.Text = app.Birthday.Day.ToString();                             //療養者生年月日日
        //        verifyBoxZoku.Text = app.Family.ToString();                                     //続柄
        //        verifyBoxF1.Text = app.FushoName1;                                              //傷病名
        //        verifyBoxRyouyoukbn.Text = app.HihoType.ToString();                             //療養区分
        //        verifyBoxMonths.Text = app.FushoCourse1.ToString();                             //実月数
        //        verifyBoxDays.Text = app.FushoDays1.ToString();                                 //実日数

        //        if (app.FushoStartDate1 !=DateTime.MinValue)
        //        { 
        //            verifyBoxF1StY.Text = app.FushoStartDate1.Year.ToString();                      //治療開始年
        //            verifyBoxF1StM.Text = app.FushoStartDate1.Month.ToString();                     //治療開始月
        //            verifyBoxF1StD.Text = app.FushoStartDate1.Day.ToString();                       //治療開始日
        //        }

        //        if (app.FushoFinishDate1 != DateTime.MinValue)
        //        {
        //            verifyBoxF1EdY.Text = app.FushoFinishDate1.Year.ToString();                     //治療終了年
        //            verifyBoxF1EdM.Text = app.FushoFinishDate1.Month.ToString();                    //治療終了月
        //            verifyBoxF1EdD.Text = app.FushoFinishDate1.Day.ToString();                      //治療終了日
        //        }

        //        if (app.FushoStartDate2 != DateTime.MinValue)
        //        {

        //            verifyBoxF2StY.Text = app.FushoStartDate2.Year.ToString();                      //入院開始年
        //            verifyBoxF2StM.Text = app.FushoStartDate2.Month.ToString();                     //入院開始月
        //            verifyBoxF2StD.Text = app.FushoStartDate2.Day.ToString();                       //入院開始日
        //        }

        //        if (app.FushoFinishDate2 != DateTime.MinValue)
        //        {

        //            verifyBoxF2EdY.Text = app.FushoFinishDate2.Year.ToString();                     //入院終了年
        //            verifyBoxF2EdM.Text = app.FushoFinishDate2.Month.ToString();                    //入院終了月
        //            verifyBoxF2EdD.Text = app.FushoFinishDate2.Day.ToString();                      //入院終了日
        //        }


        //        verifyBoxCurrencyCode.Text = app.TaggedDatas.GeneralString1;                    //支払通貨コード
        //        verifyBoxTotal.Text = app.TaggedDatas.CalcDistance.ToString();                  //支払金額

        //        verifyBoxCymY.Text = app.FushoFinishDate5.Year.ToString();                      //請求年
        //        verifyBoxCymM.Text = app.FushoFinishDate5.Month.ToString();                     //請求月
        //        verifyBoxCymD.Text = app.FushoFinishDate5.Day.ToString();                       //請求日


        //        //20200427154850 furukawa st ////////////////////////
        //        //通常、返送、留保

        //        verifyBoxFlg.Text = app.Numbering.ToString();
        //        //20200427154850 furukawa ed ////////////////////////
        //    }
        //}

        #endregion


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedAppVerify(App app)
        {


            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //verifyBoxY.Text = "--";
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //verifyBoxY.Text = "++";
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                setValue(verifyBoxHonsho, app.ClinicNum, firstTime, nv);                        //本省記入欄
                setValue(verifyBoxShokuin, app.ClinicName, firstTime, nv);                      //職員コード
                setValue(verifyBoxKokan, app.InsNum, firstTime, nv);                            //公館コード
                setValue(verifyBoxPName, app.PersonName, firstTime, nv);                        //療養者氏名
                setValue(verifyBoxSex, app.Sex.ToString(), firstTime, nv);                      //療養者性別
                setValue(verifyBoxBirthY, app.Birthday.Year.ToString(), firstTime, nv);         //療養者生年月日年
                setValue(verifyBoxBirthM, app.Birthday.Month.ToString(), firstTime, nv);        //療養者生年月日月
                setValue(verifyBoxBirthD, app.Birthday.Day.ToString(), firstTime, nv);          //療養者生年月日日
                setValue(verifyBoxZoku, app.Family.ToString(), firstTime, nv);                  //続柄
                setValue(verifyBoxF1, app.FushoName1, firstTime, nv);                           //傷病名
                setValue(verifyBoxRyouyoukbn, app.HihoType.ToString(), firstTime, nv);          //療養区分
                setValue(verifyBoxMonths, app.FushoCourse1.ToString(), firstTime, nv);          //実月数
                setValue(verifyBoxDays, app.FushoDays1.ToString(), firstTime, nv);              //実日数

                if (app.FushoStartDate1 != DateTime.MinValue)
                {
                    setValue(verifyBoxF1StY, app.FushoStartDate1.Year.ToString(), firstTime, nv);    //治療開始年
                    setValue(verifyBoxF1StM, app.FushoStartDate1.Month.ToString(), firstTime, nv);   //治療開始月
                    setValue(verifyBoxF1StD, app.FushoStartDate1.Day.ToString(), firstTime, nv);     //治療開始日
                }

                if (app.FushoFinishDate1 != DateTime.MinValue)
                {
                    setValue(verifyBoxF1EdY, app.FushoFinishDate1.Year.ToString(), firstTime, nv);   //治療終了年
                    setValue(verifyBoxF1EdM, app.FushoFinishDate1.Month.ToString(), firstTime, nv);  //治療終了月
                    setValue(verifyBoxF1EdD, app.FushoFinishDate1.Day.ToString(), firstTime, nv);    //治療終了日
                }

                if (app.FushoStartDate2 != DateTime.MinValue)
                {

                    setValue(verifyBoxF2StY, app.FushoStartDate2.Year.ToString(), firstTime, nv);    //入院開始年
                    setValue(verifyBoxF2StM, app.FushoStartDate2.Month.ToString(), firstTime, nv);   //入院開始月
                    setValue(verifyBoxF2StD, app.FushoStartDate2.Day.ToString(), firstTime, nv);     //入院開始日
                }

                if (app.FushoFinishDate2 != DateTime.MinValue)
                {

                    setValue(verifyBoxF2EdY, app.FushoFinishDate2.Year.ToString(), firstTime, nv);   //入院終了年
                    setValue(verifyBoxF2EdM, app.FushoFinishDate2.Month.ToString(), firstTime, nv);  //入院終了月
                    setValue(verifyBoxF2EdD, app.FushoFinishDate2.Day.ToString(), firstTime, nv);    //入院終了日
                }


                setValue(verifyBoxCurrencyCode, app.TaggedDatas.GeneralString1, firstTime, nv);      //支払通貨コード
                setValue(verifyBoxTotal, app.TaggedDatas.CalcDistance.ToString(), firstTime, nv);    //支払金額
                setValue(verifyBoxCymY, app.FushoFinishDate5.Year.ToString(), firstTime, nv);        //請求年
                setValue(verifyBoxCymM, app.FushoFinishDate5.Month.ToString(), firstTime, nv);       //請求月
                setValue(verifyBoxCymD, app.FushoFinishDate5.Day.ToString(), firstTime, nv);         //請求日

                setValue(verifyBoxFlg, app.Numbering.ToString(), firstTime, nv);



            }
        }



        //20200514155228 furukawa ed ////////////////////////



        #endregion

        #region 画像関連
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;

            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (ShobyoNameConts.Any(c => c.Focused)) posBuiName = pos;
            else if (ShobyoDateConts.Any(c => c.Focused)) posBuiDate = pos;

        }

        /// <summary>
        /// 全体表示ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }


        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion


        #region オブジェクトイベント


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Action<Control, bool> act = null;

            Control[] ignoreControls = new Control[] { labelYearInfo,  labelYear,
                verifyBoxY, labelInputerName, };

            act = new Action<Control, bool>((c, b) =>
            {
                foreach (Control item in c.Controls) act(item, b);
                if ((c is IVerifiable == false) && (c is Label == false)) return;
                if (ignoreControls.Contains(c)) return;
                c.Visible = b;
                if (c is IVerifiable == false) return;
                c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                act(panelRight, false);
            }
            else
            {
                //申請書の場合
                act(panelRight, true);

            }
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;

            else if (personConts.Contains(sender)) p = posPerson;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (ShobyoNameConts.Contains(sender)) p = posBuiName;
            else if (ShobyoDateConts.Contains(sender)) p = posBuiDate;
            else if (chargeConts.Contains(sender)) p = posCharge;

            else return;

            scrollPictureControl1.ScrollPosition = p;
        }


        /// <summary>
        /// 年月自動挿入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxF1StY_Validated(object sender, EventArgs e)
        {
            //基本的に年月は同じと想定する
            if ((VerifyBox)sender == verifyBoxF1StY) verifyBoxF1EdY.Text = verifyBoxF1StY.Text;
            else if ((VerifyBox)sender == verifyBoxF1StM) verifyBoxF1EdM.Text = verifyBoxF1StM.Text;
            else if ((VerifyBox)sender == verifyBoxF2StY) verifyBoxF2EdY.Text = verifyBoxF2StY.Text;
            else if ((VerifyBox)sender == verifyBoxF2StM) verifyBoxF2EdM.Text = verifyBoxF2StM.Text;
        }


        /// <summary>
        /// 大文字に変換
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxKokan_Validated(object sender, EventArgs e)
        {
            verifyBoxKokan.Text = Strings.StrConv(verifyBoxKokan.Text, VbStrConv.Uppercase);
        }


        /// <summary>
        /// フォーム表示後
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            verifyBoxY.Focus();
        }

        /// <summary>
        /// 終了ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 更新ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }



        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == "Aid")
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        #endregion



    }
}
