﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.GaimusyoRyouyouhi
{
    class Export
    {
        #region exportクラス
        [DB.DbAttribute.PrimaryKey]
        public string aid { get; set; } = string.Empty;              //プライマリキー
        public string cym { get; set; } = string.Empty;              //請求年月  パンチ年月
        public string honsho { get; set; } = string.Empty;           //本省記入欄
        public string shokuin { get; set; } = string.Empty;          //職員コード
        public string kokan { get; set; } = string.Empty;            //公館コード
        public string pname { get; set; } = string.Empty;            //療養者氏名
        public string psex { get; set; } = string.Empty;             //療養者性別
        public string pbirthy { get; set; } = string.Empty;          //療養者生年月日年
        public string pbirthm { get; set; } = string.Empty;          //療養者生年月日月
        public string pbirthd { get; set; } = string.Empty;          //療養者生年月日日
        public string zokugara { get; set; } = string.Empty;         //続柄
        public string sickname { get; set; } = string.Empty;         //傷病名
        public string ryouyoukubun { get; set; } = string.Empty;     //療養区分
        public string months { get; set; } = string.Empty;           //実月数
        public string days { get; set; } = string.Empty;             //実日数
        public string f1sty { get; set; } = string.Empty;            //治療開始年
        public string f1stm { get; set; } = string.Empty;            //治療開始月
        public string f1std { get; set; } = string.Empty;            //治療開始日
        public string f1edy { get; set; } = string.Empty;            //治療終了年
        public string f1edm { get; set; } = string.Empty;            //治療終了月
        public string f1edd { get; set; } = string.Empty;            //治療終了日
        public string f2sty { get; set; } = string.Empty;            //入院開始年
        public string f2stm { get; set; } = string.Empty;            //入院開始月
        public string f2std { get; set; } = string.Empty;            //入院開始日
        public string f2edy { get; set; } = string.Empty;            //入院終了年
        public string f2edm { get; set; } = string.Empty;            //入院終了月
        public string f2edd { get; set; } = string.Empty;            //入院終了日
        public string currencycode { get; set; } = string.Empty;     //支払通貨コード
        public string total { get; set; } = string.Empty;            //支払金額
        public string chrgy { get; set; } = string.Empty;            //請求年
        public string chrgm { get; set; } = string.Empty;            //請求月
        public string chrgd { get; set; } = string.Empty;           //請求日
        
        //20200427162308 furukawa st ////////////////////////
        //分類フラグ追加                                                                    
        public string bunruiflg { get; set; } = string.Empty;
        //20200427162308 furukawa ed ////////////////////////
        #endregion


        public static bool DoExport(int cym)
        {
            string strDir;


            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != DialogResult.OK) return false;
            strDir = dlg.Name;

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            
            wf.LogPrint("対象の申請書を取得しています");

            var file_Shift_JIS = strDir + "\\申請書_Shift_JIS" + cym.ToString() + ".csv";
            string file_UTF8= strDir + "\\申請書_UTF8_" + cym.ToString() + ".csv";


            //20200825153731 furukawa st ////////////////////////
            //出力レコードはエラー以外とする
            
            var apps = App.GetAppsWithWhere($"where a.cym={cym} and a.ayear>=0");
            //var apps = App.GetApps(cym);
            //20200825153731 furukawa ed ////////////////////////



            wf.SetMax(apps.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;

            //先にテーブル登録
            DB.Command cmd = new DB.Command(DB.Main,$"delete from export where cym='{cym}'");
            cmd.TryExecuteNonQuery();
            if (!entryExportTable(apps))
            {
                wf.Dispose();
                return false;
                
            }
            //20200428130851 furukawa st ////////////////////////
            //コマンド開放            
            cmd.Dispose();
            //20200428130851 furukawa ed ////////////////////////

            System.IO.StreamWriter swSJIS = new System.IO.StreamWriter(file_Shift_JIS, false, Encoding.GetEncoding("Shift_JIS"));
            System.IO.StreamWriter swUTF8 = new System.IO.StreamWriter(file_UTF8, false, Encoding.GetEncoding("UTF-8"));

            try
            {

                //20200611144552 furukawa st ////////////////////////
                //データソート時空欄があると意図通りにならないので、SQLで吸収

                //分類フラグ,公館コード, 公信番号（本省記入欄）,職員コード,続柄,生年月日昇順（年上から＝古い年月順＝昇順）,施術開始日

                string strsql =
                    $"select  " +
                    $"bunruiflg,aid,cym,  " +
                    $"honsho ,  " +
                    $"shokuin, kokan, pname, psex,  " +
                    $"pbirthy ,  " +
                    $"pbirthm ,  " +
                    $"pbirthd ,  " +
                    $"zokugara,  " +
                    $"sickname, ryouyoukubun, months, days,  " +
                    $"f1sty,  " +
                    $"f1stm,  " +
                    $"f1std,  " +
                    $"f1edy, f1edm, f1edd, f2sty, f2stm, f2std, f2edy, f2edm, f2edd, currencycode, total, chrgy, chrgm, chrgd " +
                    $"from export  " +
                    $"where cym='{cym}' " +
                    $"order by bunruiflg,kokan, " +
                    $"cast(case when honsho='' then '99999' else honsho end as int), " +
                    $"shokuin,"+
                    $"cast(case when zokugara='' then '9' else zokugara end as int), " +
                    $"cast(case when pbirthy='' then '9999' else pbirthy end as int), " +
                    $"cast(case when pbirthm='' then '99' else pbirthm end as int), " +
                    $"cast(case when pbirthd='' then '99' else pbirthd end as int), " +
                    $"cast(case when f1sty='' then '9999' else f1sty end as int), " +
                    $"cast(case when f1stm='' then '99' else f1stm end as int), " +
                    $"cast(case when f1std='' then '99' else f1std end as int)";


                //string strsql =
                //    $"select  " +
                //    $"bunruiflg,aid,cym,  " +
                //    $"case when honsho='' then '99999' else honsho end,  " +
                //    $"shokuin, kokan, pname, psex,  " +
                //    $"case when pbirthy='' then '9999' else pbirthy end,  " +
                //    $"case when pbirthm='' then '99' else pbirthm end,  " +
                //    $"case when pbirthd='' then '99' else pbirthd end,  " +
                //    $"case when zokugara='' then '9' else zokugara end,  " +
                //    $"sickname, ryouyoukubun, months, days,  " +
                //    $"case when f1sty='' then '9999' else f1sty end,  " +
                //    $"case when f1stm='' then '99' else f1stm end,  " +
                //    $"case when f1std='' then '99' else f1std end,  " +
                //    $"f1edy, f1edm, f1edd, f2sty, f2stm, f2std, f2edy, f2edm, f2edd, currencycode, total, chrgy, chrgm, chrgd " +
                //    $"from export  " +
                //    $"where cym='{cym}' " +
                //    $"order by bunruiflg,kokan, " +
                //    $"cast(case when honsho='' then '99999' else honsho end as int), " +
                //    $"cast(case when zokugara='' then '9' else zokugara end as int), " +
                //    $"cast(case when pbirthy='' then '9999' else pbirthy end as int), " +
                //    $"cast(case when pbirthm='' then '99' else pbirthm end as int), " +
                //    $"cast(case when pbirthd='' then '99' else pbirthd end as int), " +
                //    $"cast(case when f1sty='' then '9999' else f1sty end as int), " +
                //    $"cast(case when f1stm='' then '99' else f1stm end as int), " +
                //    $"cast(case when f1std='' then '99' else f1std end as int)";




                

                cmd = new DB.Command(DB.Main, strsql);

                var tmplist = cmd.TryExecuteReaderList();
                foreach (var item in tmplist)
                {
                    if (wf.Cancel)
                    {
                        MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                        return false;
                    }

                    wf.InvokeValue++;
                    int colcnt = 0;
                    Export exp = new Export();
                    exp.bunruiflg = item[colcnt++].ToString();
                    exp.aid = item[colcnt++].ToString();            
                    exp.cym = item[colcnt++].ToString();                               
                    exp.honsho = item[colcnt++].ToString();        //本省記入欄                   
                    exp.shokuin = item[colcnt++].ToString();       //職員コード                   
                    exp.kokan = item[colcnt++].ToString();         //公館コード                   
                    exp.pname = item[colcnt++].ToString();         //療養者氏名                   
                    exp.psex = item[colcnt++].ToString();          //療養者性別                   
                    exp.pbirthy = item[colcnt++].ToString();       //療養者生年月日年                   
                    exp.pbirthm = item[colcnt++].ToString();       //療養者生年月日月                   
                    exp.pbirthd = item[colcnt++].ToString();       //療養者生年月日日                   
                    exp.zokugara = item[colcnt++].ToString();      //続柄                   
                    exp.sickname = item[colcnt++].ToString();      //傷病名                   
                    exp.ryouyoukubun = item[colcnt++].ToString();  //療養区分                   
                    exp.months = item[colcnt++].ToString();        //実月数                   
                    exp.days = item[colcnt++].ToString();          //実日数                   
                    exp.f1sty = item[colcnt++].ToString();         //治療開始年                   
                    exp.f1stm = item[colcnt++].ToString();         //治療開始月                   
                    exp.f1std = item[colcnt++].ToString();         //治療開始日                   
                    exp.f1edy = item[colcnt++].ToString();         //治療終了年                   
                    exp.f1edm = item[colcnt++].ToString();         //治療終了月                   
                    exp.f1edd = item[colcnt++].ToString();         //治療終了日                   
                    exp.f2sty = item[colcnt++].ToString();         //入院開始年                   
                    exp.f2stm = item[colcnt++].ToString();         //入院開始月                   
                    exp.f2std = item[colcnt++].ToString();         //入院開始日                   
                    exp.f2edy = item[colcnt++].ToString();         //入院終了年                   
                    exp.f2edm = item[colcnt++].ToString();         //入院終了月                   
                    exp.f2edd = item[colcnt++].ToString();         //入院終了日                   
                    exp.currencycode = item[colcnt++].ToString();  //支払通貨コード                   
                    exp.total = item[colcnt++].ToString();         //支払金額                   
                    exp.chrgy = item[colcnt++].ToString();         //請求年                   
                    exp.chrgm = item[colcnt++].ToString();         //請求月                   
                    exp.chrgd = item[colcnt++].ToString();         //請求日

                    swSJIS.WriteLine(CreateRecord(exp));
                    swUTF8.WriteLine(CreateRecord(exp));
                }


                //List<Export> lstexp = DB.Main.Select<Export>($"cym='{cym}'").ToList();

                //foreach (var item in lstexp)
                //{
                //    if (wf.Cancel)
                //    {
                //        MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                //        return false;
                //    }

                //    wf.InvokeValue++;

                //    swSJIS.WriteLine(CreateRecord(item));
                //    swUTF8.WriteLine(CreateRecord(item));
                //}




                //20200611144552 furukawa ed ////////////////////////




            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                return false;
            }
            finally
            {
                swSJIS.Close();
                swUTF8.Close();
                wf.Dispose();
            }

            MessageBox.Show("出力が完了しました");
            return true;
        }

        
        

        /// <summary>
        /// テーブル登録
        /// </summary>
        /// <param name="lstapp"></param>
        /// <returns></returns>
        private static bool entryExportTable(List<App> lstapp)
        {
            DB.Transaction tran = new DB.Transaction(DB.Main);
            try
            {

                //2020-06-09　ソート順仕様変更
                //公館コード順→公信番号順→本人の治療日付順→配偶者の治療日付順→子供（年上からそれぞれ治療日付順）
                //＝公館コード順→公信番号順(本省記入欄)→続柄本人、配偶者、子供の治療日付順→子供の生年月日昇順→治療日付（年上からそれぞれ治療日付順）


                //20200610155024 furukawa st ////////////////////////
                //出力データソート順変更
               



                var lst = lstapp.
                   OrderBy(rec => rec.Numbering).          //分類フラグ
                   ThenBy(rec => rec.InsNum).              //公館コード               適切な列がないので、保険者番号の列に入れてる                   
                   ThenBy(rec => rec.FushoDays5).          //公信番号（本省記入欄）   適切な列がないので、負傷名５の実日数列に入れてる
                   ThenBy(rec => rec.Family).              //続柄                   
                                                           //ThenBy(rec => rec.Birthday).            //生年月日昇順（年上から＝古い年月順＝昇順）
                                                           //  ThenBy(rec => rec.FushoStartDate1).     //施術開始日
                   ToList();

                #region old

                //20200427161814 furukawa st ////////////////////////
                //ソート順に分類フラグ追加

                //var lst =lstapp.
                //    OrderBy(rec => rec.Numbering).          //分類フラグ
                //    ThenBy(rec => rec.InsNum).              //公館コード
                //    ThenBy(rec=> rec.ClinicName).           //職員コード
                //    ThenBy(rec => rec.Family).              //続柄
                //    ThenBy(rec=>rec.FushoStartDate1).       //施術開始日
                //    ToList();

                //lstapp.Sort((x,y) => x.InsNum.CompareTo(y.InsNum));
                //var lst = lstapp.OrderBy(rec => rec.InsNum).          //公館コード
                //    ThenBy(rec => rec.ClinicName).                   //職員コード
                //    ThenBy(rec => rec.Family).                      //続柄
                //    ThenBy(rec => rec.FushoStartDate1).               //施術開始日
                //    ToList();

                //20200427161814 furukawa ed ////////////////////////
                #endregion


                //20200610155024 furukawa ed ////////////////////////

                foreach (App app in lst)
                {
                    Export exp = new Export();

                    exp.aid = app.Aid.ToString();                                       //プライマリキー
                    exp.cym = app.CYM.ToString();                                       //請求年月  パンチ年月


                    exp.honsho = app.ClinicNum.ToString();                          //本省記入欄
                    exp.shokuin = app.ClinicName.ToString().PadLeft(5, '0');        //職員コード 5桁
                    exp.kokan = app.InsNum.ToString().PadLeft(4, '0');              //公館コード


                    exp.pname = app.PersonName.ToString();                          //療養者氏名
                    exp.psex = app.Sex.ToString() != "0" ? app.Sex.ToString() : "";   //療養者性別

                    if (app.Birthday != DateTime.MinValue)
                    {
                        exp.pbirthy = app.Birthday.Year.ToString().PadLeft(4, '0');                     //療養者生年月日年
                        exp.pbirthm = app.Birthday.Month.ToString().PadLeft(2, '0');                    //療養者生年月日月
                        exp.pbirthd = app.Birthday.Day.ToString().PadLeft(2, '0');                      //療養者生年月日日
                    }

                    exp.zokugara = app.Family.ToString() != "0" ? app.Family.ToString() : string.Empty;                     //続柄

                    exp.sickname = app.FushoName1.ToString();                                                               //傷病名
                    exp.ryouyoukubun = app.HihoType.ToString() != "0" ? app.HihoType.ToString() : string.Empty;             //療養区分
                    exp.months = app.FushoCourse1.ToString() != "0" ? app.FushoCourse1.ToString().PadLeft(2, '0') : string.Empty;         //実月数 2桁
                    exp.days = app.FushoDays1.ToString() != "0" ? app.FushoDays1.ToString().PadLeft(3, '0') : string.Empty;               //実日数 3桁

                    if (app.FushoStartDate1 != DateTime.MinValue)
                    {
                        exp.f1sty = app.FushoStartDate1.Year.ToString().PadLeft(4, '0');         //治療開始年
                        exp.f1stm = app.FushoStartDate1.Month.ToString().PadLeft(2, '0');        //治療開始月
                        exp.f1std = app.FushoStartDate1.Day.ToString().PadLeft(2, '0');          //治療開始日
                    }

                    if (app.FushoFinishDate1 != DateTime.MinValue)
                    {

                        exp.f1edy = app.FushoFinishDate1.Year.ToString().PadLeft(4, '0');        //治療終了年
                        exp.f1edm = app.FushoFinishDate1.Month.ToString().PadLeft(2, '0');       //治療終了月
                        exp.f1edd = app.FushoFinishDate1.Day.ToString().PadLeft(2, '0');         //治療終了日
                    }


                    if (app.FushoStartDate2 != DateTime.MinValue)
                    {

                        exp.f2sty = app.FushoStartDate2.Year.ToString().PadLeft(4, '0');         //入院開始年
                        exp.f2stm = app.FushoStartDate2.Month.ToString().PadLeft(2, '0');        //入院開始月
                        exp.f2std = app.FushoStartDate2.Day.ToString().PadLeft(2, '0');          //入院開始日
                    }


                    if (app.FushoFinishDate2 != DateTime.MinValue)
                    {
                        exp.f2edy = app.FushoFinishDate2.Year.ToString().PadLeft(4, '0');        //入院終了年
                        exp.f2edm = app.FushoFinishDate2.Month.ToString().PadLeft(2, '0');       //入院終了月
                        exp.f2edd = app.FushoFinishDate2.Day.ToString().PadLeft(2, '0');         //入院終了日
                    }
             

                    exp.currencycode = app.TaggedDatas.GeneralString1.ToString().PadLeft(2, '0');       //支払通貨コード  2桁
                    exp.total = app.TaggedDatas.CalcDistance.ToString() != "0" ? app.TaggedDatas.CalcDistance.ToString() : string.Empty;　//支払金額 このままでいい2020年4月30日出納さん


                    if (app.FushoFinishDate5 != DateTime.MinValue)
                    {
                        exp.chrgy = app.FushoFinishDate5.Year.ToString().PadLeft(4, '0');            //請求年
                        exp.chrgm = app.FushoFinishDate5.Month.ToString().PadLeft(2, '0');           //請求月
                        exp.chrgd = app.FushoFinishDate5.Day.ToString().PadLeft(2, '0');             //請求日
                    }
               

                    //20200427162401 furukawa st ////////////////////////
                    //分類フラグ追加

                    exp.bunruiflg = app.Numbering;
                    //20200427162401 furukawa ed ////////////////////////


                    if (!DB.Main.Insert(exp, tran))return false;
                    
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                tran.Rollback();
                return false;
            }
        }


        /// <summary>
        /// CSV出力
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private static string CreateRecord(Export exp)
        {

            string res = string.Empty;


            //20200430154844 furukawa st ////////////////////////
            //納品時に邪魔

            //20200427164052 furukawa st ////////////////////////
            //分類フラグをCSVの先頭に

            //switch (exp.bunruiflg)
            //{
            //    case "0":
            //        res += '\"' + exp.bunruiflg + "：通常\",";
            //        break;
            //    case "1":
            //        res += '\"' + exp.bunruiflg + "：返送\",";
            //        break;

            //    case "2":
            //        res += '\"' + exp.bunruiflg + "：留保\",";
            //        break;

            //    default:
            //        res += string.Empty + ",";
            //        break;

            //}
            //res += exp.bunruiflg != string.Empty ? '\"' + exp.bunruiflg + "\"," : ",";
            //分類（OK、返送、留保）

            //20200427164052 furukawa ed ////////////////////////


            //20200430154844 furukawa ed ////////////////////////

            res += exp.honsho != string.Empty ? '\"' + exp.honsho + "\"," : ",";                                      //本省記入欄
            res += exp.shokuin != string.Empty ? '\"' + exp.shokuin + "\"," : ",";                                        //職員コード
            res += exp.kokan != string.Empty ? '\"' + exp.kokan + "\"," : ",";                                        //公館コード
            res += exp.pname != string.Empty ? '\"' + exp.pname + "\"," : ",";                                        //療養者氏名
            res += exp.psex != string.Empty ? '\"' + exp.psex + "\"," : ",";                                          //療養者性別
            res += exp.pbirthy != string.Empty ? '\"' + exp.pbirthy + "\"," : ",";                                    //療養者生年月日年
            res += exp.pbirthm != string.Empty ? '\"' + exp.pbirthm + "\"," : ",";                                        //療養者生年月日月
            res += exp.pbirthd != string.Empty ? '\"' + exp.pbirthd + "\"," : ",";                                    //療養者生年月日日
            res += exp.zokugara != string.Empty ? '\"' + exp.zokugara + "\"," : ",";                                      //続柄
            res += exp.sickname != string.Empty ? '\"' + exp.sickname + "\"," : ",";                                      //傷病名
            res += exp.ryouyoukubun != string.Empty ? '\"' + exp.ryouyoukubun + "\"," : ",";                          //療養区分
            res += exp.months != string.Empty ? '\"' + exp.months + "\"," : ",";                                          //実月数
            res += exp.days != string.Empty ? '\"' + exp.days + "\"," : ",";                                          //実日数
            res += exp.f1sty != string.Empty ? '\"' + exp.f1sty + "\"," : ",";                                            //治療開始年
            res += exp.f1stm != string.Empty ? '\"' + exp.f1stm + "\"," : ",";                                            //治療開始月
            res += exp.f1std != string.Empty ? '\"' + exp.f1std + "\"," : ",";                                        //治療開始日
            res += exp.f1edy != string.Empty ? '\"' + exp.f1edy + "\"," : ",";                                            //治療終了年
            res += exp.f1edm != string.Empty ? '\"' + exp.f1edm + "\"," : ",";                                        //治療終了月
            res += exp.f1edd != string.Empty ? '\"' + exp.f1edd + "\"," : ",";                                            //治療終了日
            res += exp.f2sty != string.Empty ? '\"' + exp.f2sty + "\"," : ",";                                            //入院開始年
            res += exp.f2stm != string.Empty ? '\"' + exp.f2stm + "\"," : ",";                                            //入院開始月
            res += exp.f2std != string.Empty ? '\"' + exp.f2std + "\"," : ",";                                        //入院開始日
            res += exp.f2edy != string.Empty ? '\"' + exp.f2edy + "\"," : ",";                                            //入院終了年
            res += exp.f2edm != string.Empty ? '\"' + exp.f2edm + "\"," : ",";                                        //入院終了月
            res += exp.f2edd != string.Empty ? '\"' + exp.f2edd + "\"," : ",";                                            //入院終了日
            res += exp.currencycode != string.Empty ? '\"' + exp.currencycode + "\"," : ",";                              //支払通貨コード
            res += exp.total != string.Empty ? '\"' + exp.total + "\"," : ",";                                            //支払金額
            res += exp.chrgy != string.Empty ? '\"' + exp.chrgy + "\"," : ",";                                            //請求年
            res += exp.chrgm != string.Empty ? '\"' + exp.chrgm + "\"," : ",";                                        //請求月
            res += exp.chrgd != string.Empty ? '\"' + exp.chrgd + "\"" : string.Empty;                                         //請求日

            

            return res;
        }

        
  
    }
}
