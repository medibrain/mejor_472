﻿namespace Mejor
{
    partial class GroupSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.入力中ユーザーリセットToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonInputStart = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonGaibu2nd = new System.Windows.Forms.Button();
            this.buttonGaibu1st = new System.Windows.Forms.Button();
            this.buttonBatch = new System.Windows.Forms.Button();
            this.buttonRelate = new System.Windows.Forms.Button();
            this.buttonSPVerifyList = new System.Windows.Forms.Button();
            this.buttonSPVerify = new System.Windows.Forms.Button();
            this.buttonSP = new System.Windows.Forms.Button();
            this.buttonExVerify = new System.Windows.Forms.Button();
            this.buttonExInput = new System.Windows.Forms.Button();
            this.buttonVerify = new System.Windows.Forms.Button();
            this.buttonError = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtFind = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 33);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1400, 660);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.入力中ユーザーリセットToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 26);
            // 
            // 入力中ユーザーリセットToolStripMenuItem
            // 
            this.入力中ユーザーリセットToolStripMenuItem.Name = "入力中ユーザーリセットToolStripMenuItem";
            this.入力中ユーザーリセットToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.入力中ユーザーリセットToolStripMenuItem.Text = "入力中ユーザーリセット";
            this.入力中ユーザーリセットToolStripMenuItem.Click += new System.EventHandler(this.入力中ユーザーリセットToolStripMenuItem_Click);
            // 
            // buttonInputStart
            // 
            this.buttonInputStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInputStart.Location = new System.Drawing.Point(48, 10);
            this.buttonInputStart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInputStart.Name = "buttonInputStart";
            this.buttonInputStart.Size = new System.Drawing.Size(100, 31);
            this.buttonInputStart.TabIndex = 0;
            this.buttonInputStart.Text = "入力";
            this.buttonInputStart.UseVisualStyleBackColor = true;
            this.buttonInputStart.Click += new System.EventHandler(this.buttonInputStart_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(1280, 45);
            this.buttonClose.Margin = new System.Windows.Forms.Padding(4);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(100, 31);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "閉じる";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(851, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonGaibu2nd);
            this.panel1.Controls.Add(this.buttonGaibu1st);
            this.panel1.Controls.Add(this.buttonBatch);
            this.panel1.Controls.Add(this.buttonRelate);
            this.panel1.Controls.Add(this.buttonSPVerifyList);
            this.panel1.Controls.Add(this.buttonSPVerify);
            this.panel1.Controls.Add(this.buttonSP);
            this.panel1.Controls.Add(this.buttonExVerify);
            this.panel1.Controls.Add(this.buttonExInput);
            this.panel1.Controls.Add(this.buttonVerify);
            this.panel1.Controls.Add(this.buttonInputStart);
            this.panel1.Controls.Add(this.buttonError);
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 693);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1400, 81);
            this.panel1.TabIndex = 2;
            // 
            // buttonGaibu2nd
            // 
            this.buttonGaibu2nd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGaibu2nd.Location = new System.Drawing.Point(156, 45);
            this.buttonGaibu2nd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGaibu2nd.Name = "buttonGaibu2nd";
            this.buttonGaibu2nd.Size = new System.Drawing.Size(100, 31);
            this.buttonGaibu2nd.TabIndex = 9;
            this.buttonGaibu2nd.Text = "外部2回目";
            this.buttonGaibu2nd.UseVisualStyleBackColor = true;
            this.buttonGaibu2nd.Click += new System.EventHandler(this.buttonGaibu2nd_Click);
            // 
            // buttonGaibu1st
            // 
            this.buttonGaibu1st.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGaibu1st.Location = new System.Drawing.Point(48, 45);
            this.buttonGaibu1st.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGaibu1st.Name = "buttonGaibu1st";
            this.buttonGaibu1st.Size = new System.Drawing.Size(100, 31);
            this.buttonGaibu1st.TabIndex = 8;
            this.buttonGaibu1st.Text = "外部1回目";
            this.buttonGaibu1st.UseVisualStyleBackColor = true;
            this.buttonGaibu1st.Click += new System.EventHandler(this.buttonGaibu1st_Click);
            // 
            // buttonBatch
            // 
            this.buttonBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBatch.Location = new System.Drawing.Point(912, 46);
            this.buttonBatch.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBatch.Name = "buttonBatch";
            this.buttonBatch.Size = new System.Drawing.Size(219, 31);
            this.buttonBatch.TabIndex = 7;
            this.buttonBatch.Text = "バッチを申請書に関連付け";
            this.buttonBatch.UseVisualStyleBackColor = true;
            this.buttonBatch.Click += new System.EventHandler(this.buttonBatch_Click);
            // 
            // buttonRelate
            // 
            this.buttonRelate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRelate.Location = new System.Drawing.Point(912, 10);
            this.buttonRelate.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRelate.Name = "buttonRelate";
            this.buttonRelate.Size = new System.Drawing.Size(219, 31);
            this.buttonRelate.TabIndex = 7;
            this.buttonRelate.Text = "続紙を申請書に関連付け";
            this.buttonRelate.UseVisualStyleBackColor = true;
            this.buttonRelate.Click += new System.EventHandler(this.buttonRelate_Click);
            // 
            // buttonSPVerifyList
            // 
            this.buttonSPVerifyList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSPVerifyList.Location = new System.Drawing.Point(707, 45);
            this.buttonSPVerifyList.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSPVerifyList.Name = "buttonSPVerifyList";
            this.buttonSPVerifyList.Size = new System.Drawing.Size(100, 31);
            this.buttonSPVerifyList.TabIndex = 6;
            this.buttonSPVerifyList.Text = "独自件数";
            this.buttonSPVerifyList.UseVisualStyleBackColor = true;
            this.buttonSPVerifyList.Click += new System.EventHandler(this.buttonSPVerifyList_Click);
            // 
            // buttonSPVerify
            // 
            this.buttonSPVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSPVerify.Location = new System.Drawing.Point(600, 45);
            this.buttonSPVerify.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSPVerify.Name = "buttonSPVerify";
            this.buttonSPVerify.Size = new System.Drawing.Size(100, 31);
            this.buttonSPVerify.TabIndex = 6;
            this.buttonSPVerify.Text = "独自ベリ";
            this.buttonSPVerify.UseVisualStyleBackColor = true;
            this.buttonSPVerify.Click += new System.EventHandler(this.buttonSPVerify_Click);
            // 
            // buttonSP
            // 
            this.buttonSP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSP.Location = new System.Drawing.Point(492, 45);
            this.buttonSP.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSP.Name = "buttonSP";
            this.buttonSP.Size = new System.Drawing.Size(100, 31);
            this.buttonSP.TabIndex = 6;
            this.buttonSP.Text = "独自";
            this.buttonSP.UseVisualStyleBackColor = true;
            this.buttonSP.Click += new System.EventHandler(this.buttonSP_Click);
            // 
            // buttonExVerify
            // 
            this.buttonExVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExVerify.Location = new System.Drawing.Point(600, 10);
            this.buttonExVerify.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExVerify.Name = "buttonExVerify";
            this.buttonExVerify.Size = new System.Drawing.Size(100, 31);
            this.buttonExVerify.TabIndex = 5;
            this.buttonExVerify.Text = "拡張ベリ";
            this.buttonExVerify.UseVisualStyleBackColor = true;
            this.buttonExVerify.Click += new System.EventHandler(this.buttonExVerify_Click);
            // 
            // buttonExInput
            // 
            this.buttonExInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExInput.Location = new System.Drawing.Point(492, 10);
            this.buttonExInput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExInput.Name = "buttonExInput";
            this.buttonExInput.Size = new System.Drawing.Size(100, 31);
            this.buttonExInput.TabIndex = 5;
            this.buttonExInput.Text = "拡張入力";
            this.buttonExInput.UseVisualStyleBackColor = true;
            this.buttonExInput.Click += new System.EventHandler(this.buttonExInput_Click);
            // 
            // buttonVerify
            // 
            this.buttonVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonVerify.Location = new System.Drawing.Point(156, 10);
            this.buttonVerify.Margin = new System.Windows.Forms.Padding(4);
            this.buttonVerify.Name = "buttonVerify";
            this.buttonVerify.Size = new System.Drawing.Size(100, 31);
            this.buttonVerify.TabIndex = 1;
            this.buttonVerify.Text = "ベリファイ";
            this.buttonVerify.UseVisualStyleBackColor = true;
            this.buttonVerify.Click += new System.EventHandler(this.buttonVerify_Click);
            // 
            // buttonError
            // 
            this.buttonError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonError.Location = new System.Drawing.Point(1139, 10);
            this.buttonError.Margin = new System.Windows.Forms.Padding(4);
            this.buttonError.Name = "buttonError";
            this.buttonError.Size = new System.Drawing.Size(133, 31);
            this.buttonError.TabIndex = 3;
            this.buttonError.Text = "入力時エラー";
            this.buttonError.UseVisualStyleBackColor = true;
            this.buttonError.Click += new System.EventHandler(this.buttonError_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFind);
            this.panel2.Controls.Add(this.txtFind);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1400, 33);
            this.panel2.TabIndex = 0;
            // 
            // btnFind
            // 
            this.btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFind.Location = new System.Drawing.Point(1337, 3);
            this.btnFind.Margin = new System.Windows.Forms.Padding(4);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(59, 28);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "検索";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtFind
            // 
            this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFind.Location = new System.Drawing.Point(1195, 5);
            this.txtFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFind.MaxLength = 32;
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(135, 22);
            this.txtFind.TabIndex = 6;
            this.txtFind.TabStop = false;
            // 
            // GroupSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1400, 774);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GroupSelectForm";
            this.Text = "グループ選択画面";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonInputStart;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonVerify;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 入力中ユーザーリセットToolStripMenuItem;
        private System.Windows.Forms.Button buttonError;
        private System.Windows.Forms.Button buttonExVerify;
        private System.Windows.Forms.Button buttonExInput;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button buttonSP;
        private System.Windows.Forms.Button buttonSPVerify;
        private System.Windows.Forms.Button buttonSPVerifyList;
        private System.Windows.Forms.Button buttonRelate;
        private System.Windows.Forms.Button buttonBatch;
        private System.Windows.Forms.Button buttonGaibu2nd;
        private System.Windows.Forms.Button buttonGaibu1st;
    }
}