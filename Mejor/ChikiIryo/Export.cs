﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.ChikiIryo
{
    class Export
    {
        public static bool DoExport(int cym)
        {
            string dir;

            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return false;
                dir = f.SelectedPath;
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("対象の申請書を取得しています");
            var file = dir + "\\" + cym.ToString("00") + ".txt";
            var apps = App.GetApps(cym);
            
            //保留データを削除
            apps.RemoveAll(x => x.StatusFlagCheck(StatusFlag.支払保留));

            wf.SetMax(apps.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;

            wf.LogPrint("対象の申請書を出力データ形式に変換しています");
            var deList = new List<DaiwaExport>();

            for (int i = 0; i < apps.Count; i++)
            {
                var a = apps[i];

                //保留データをskip
                if (a.StatusFlagCheck(StatusFlag.支払保留))
                {
                    //次の申請書までskip（別続紙添付回避のため）
                    while (i < apps.Count - 1 && apps[i + 1].YM < 0) i++;
                    continue;
                }

                deList.Add(DaiwaExport.Create(a, "S5", "01", a.PayCode, false));
            }

            try
            {
                wf.LogPrint("大和総研データを出力しています");
                using (var sw = new System.IO.StreamWriter(file, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (var item in deList)
                    {
                        if (wf.Cancel)
                        {
                            MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                            return false;
                        }

                        wf.InvokeValue++;
                        if (item.Ayear > 0) sw.WriteLine(item.ToString());
                    }
                }

                wf.LogPrint("基金形式画像データを出力しています");
                DaiwaExport.KikinTypeExport(deList, dir, wf);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が完了しました");
            return true;
        }


        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,No.,通知番号,処理年,処理月,被保険者記号,被保険者番号,被保険者名,受療者名,性別,生年月日,年齢," +
                        "〒,住所1,施術年,施術月,合計金額,請求金額,施術日数,支払先,施術所名,ナンバリング,点検,照会理由,過誤理由,再審査理由,負傷数");
                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100,int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    var notifyNumber = $"{jcy}{jcm}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<App, string> getFushoCount = app =>
                    {
                        var c = 0;
                        if (app.FushoName1 != "") c++;
                        if (app.FushoName2 != "") c++;
                        if (app.FushoName3 != "") c++;
                        if (app.FushoName4 != "") c++;
                        if (app.FushoName5 != "") c++;
                        return c.ToString();
                    };
                    foreach (var item in list)
                    {
                        ss.Add(item.Aid.ToString());
                        ss.Add(count.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(jcy);
                        ss.Add(jcm);
                        var num = item.HihoNum.Split('-');
                        ss.Add(num.Length == 2 ? num[0] : string.Empty);
                        ss.Add(num.Length == 2 ? num[1] : string.Empty);
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(DateTimeEx.GetAge(item.Birthday, DateTime.Today).ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.PayCode);
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.Numbering.ToString());
                        ss.Add(item.InspectInfo.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");//区切り文字が「, (半角カンマ)(半角スペース)」なのでエスケープ処理
                        ss.Add("\"" + item.KagoReasonStr + "\"");
                        ss.Add("\"" + item.SaishinsaReasonStr + "\"");
                        ss.Add(getFushoCount(item));

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
