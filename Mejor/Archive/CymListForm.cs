﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Archive
{
    public partial class CymListForm : Form
    {
        public CymListForm()
        {
            InitializeComponent();
        }

        private void CymListForm_Load(object sender, EventArgs e)
        {

            LoadData();
            dispFolderList();
        }

        private bool LoadData()
        {
            //保険者のCYMリスト取得

            System.Data.DataTable dtCYM = new DataTable();
            dtCYM.Clear();

            try
            {

                
                string strsql =
                  $"select " +
                  $" s.cym ,s.status " +
                  $" from scan s " +
                  $" group by s.cym,s.status " +
                  $" order by s.cym desc";

                using (DB.Command cmd = new DB.Command(DB.Main, strsql, false))
                {
                    var l = cmd.TryExecuteReaderList();

                    dtCYM.Columns.Add("cym");
                    dtCYM.Columns.Add("arc");

                    for (int r = 0; r < l.Count; r++)
                    {
                        DataRow dr = dtCYM.NewRow();

                        dr[0] = l[r].GetValue(0);
                        dr[1] = l[r].GetValue(1);
                        
                        dtCYM.Rows.Add(dr);
                    }


                    dgv.DataSource = dtCYM;
                }

                dgv.Columns[0].HeaderText = "請求年月";
                dgv.Columns[1].HeaderText = "ステータス";
                dgv.Columns[0].Width = 100;
                dgv.Columns[1].Width = 100;

                DataGridViewCheckBoxColumn cbc = new DataGridViewCheckBoxColumn();
                cbc.Name = "archive";
                cbc.Width = 50;
                dgv.Columns.Add(cbc);

                //DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
                //bc.Width = 70;
               // bc.Text = "圧縮";
                //bc.UseColumnTextForButtonValue = true;
                    
                //dgv.Columns.Add(bc);

                //for(int r = 0; r < dgv.Rows.Count; r++)
                
                //{
                //    DataGridViewButtonCell dgvbc = new DataGridViewButtonCell();
                //    dgvbc = (DataGridViewButtonCell)dgv.Rows[r].Cells[2];

                    
                //    if (int.Parse(dgv.Rows[r].Cells[1].Value.ToString()) <= 3)
                //    {
                //        dgvbc.Value = "圧縮";
                //    }
                //    else
                //    {
                //        dgvbc.Value = "解凍";
                //    }
                //}
           
                
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                
              
            }
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dispFolderList(dgv.CurrentRow.Index);
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dispFolderList(int index=0)
        {
            WaitFormSimple wf = new WaitFormSimple();
            wf.StartPosition = FormStartPosition.CenterScreen;
            System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
            

            System.Data.DataTable dtFolder = new DataTable();
            dtFolder.Clear();
            try
            {
                string strsql =
                  $"select " +
                  $" '{Settings.ImageFolder}\\{Insurer.CurrrentInsurer.dbName}\\' || s.sid as scanid " +
                  $" from scan s " +
                  $" where cym={dgv[0, index].Value.ToString()}" +
                  //$" where cym={dgv[0, dgv.SelectedRows[0].Index].Value.ToString()}" +
                  $" group by s.sid " +
                  $" order by s.sid";

                using (DB.Command cmd = new DB.Command(DB.Main, strsql, false))
                {
                    var l = cmd.TryExecuteReaderList();

                    dtFolder.Columns.Add("fodler");
                    dtFolder.Columns.Add("存在");

                    for (int r = 0; r < l.Count; r++)
                    {
                        DataRow dr = dtFolder.NewRow();

                        dr[0] = l[r].GetValue(0);
                        dr[1] = "○";
                        if (!System.IO.Directory.Exists(dr[0].ToString())) dr[1] = "×";
                        dtFolder.Rows.Add(dr);
                    }
                }

                dgvImageFolder.DataSource = dtFolder;
                dgvImageFolder.Columns[0].HeaderText = "画像フォルダ";
                dgvImageFolder.Columns[1].HeaderText = "存在";
                dgvImageFolder.Columns[0].Width = 300;
                dgvImageFolder.Columns[1].Width = 50;

                wf.InvokeCloseDispose();


            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {

            }
        }


        private void compressFolder(string strPath)
        {
            //WaitFormSimple wf = new WaitFormSimple();
            //wf.StartPosition = FormStartPosition.CenterScreen;
            //System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());


            //Task.Run(() =>
            //{
                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = $"{Application.StartupPath}\\archive\\7za.exe";
                si.Arguments = $" a -tzip {strPath}.zip {strPath} -mx3 -sdel -o{strPath} -stm2 ";

                System.Diagnostics.Process p = new System.Diagnostics.Process();

                p.StartInfo = si;
                p.Start();
                p.WaitForExit();


          //  });

           // wf.InvokeCloseDispose();

        }

        private void buttonZip_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("全選択すると時間が掛かります。サーバ上で実行することをお勧めします。実行しますか？",
                "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            for(int r = 0; r < dgv.Rows.Count; r++)  
            //foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                    DataGridViewRow dgvr = dgv.Rows[r];
                var flg = dgvr.Cells["archive"].Value ;

                if (flg!=null && (int)flg == 1)
                {
                    dispFolderList(r);

                    foreach (DataGridViewRow imgr in dgvImageFolder.Rows)
                    {
                        compressFolder(imgr.Cells[0].Value.ToString());
                    }
                }
            }
            
        }

        private void buttonAllSelect_Click(object sender, EventArgs e)
        {
            for (int r = 0; r < dgv.Rows.Count; r++)
            {
                DataGridViewCheckBoxCell cb = new DataGridViewCheckBoxCell();
                cb = (DataGridViewCheckBoxCell)dgv.Rows[r].Cells["archive"];

                if (cb.Value == null) cb.Value= 1;
                else cb.Value = null;
            }
        }
    }
}
