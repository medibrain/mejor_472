﻿namespace Mejor.MitsubishiSeishi
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.label41 = new System.Windows.Forms.Label();
            this.labelFamily = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.labelF1 = new System.Windows.Forms.Label();
            this.labelF3 = new System.Windows.Forms.Label();
            this.labelF2 = new System.Windows.Forms.Label();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxF1FushoY = new Mejor.VerifyBox();
            this.verifyBoxF2FushoY = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxF3FushoY = new Mejor.VerifyBox();
            this.verifyBoxF2FirstY = new Mejor.VerifyBox();
            this.verifyBoxF1FushoM = new Mejor.VerifyBox();
            this.verifyBoxF3FirstY = new Mejor.VerifyBox();
            this.verifyBoxF2FushoM = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.verifyBoxF3FushoM = new Mejor.VerifyBox();
            this.verifyBoxF2FirstM = new Mejor.VerifyBox();
            this.verifyBoxF1FushoD = new Mejor.VerifyBox();
            this.verifyBoxF3FirstM = new Mejor.VerifyBox();
            this.verifyBoxF2FushoD = new Mejor.VerifyBox();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.verifyBoxF3FushoD = new Mejor.VerifyBox();
            this.verifyBoxF2FirstD = new Mejor.VerifyBox();
            this.verifyBoxF3FirstD = new Mejor.VerifyBox();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.verifyBoxF2Start = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxF3Start = new Mejor.VerifyBox();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.verifyBoxF2Finish = new Mejor.VerifyBox();
            this.label7 = new System.Windows.Forms.Label();
            this.verifyBoxF3Finish = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.label21 = new System.Windows.Forms.Label();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.verifyBoxBirthE = new Mejor.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxSeikyu = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxHnumN = new Mejor.VerifyBox();
            this.label34 = new System.Windows.Forms.Label();
            this.verifyBoxHnumS = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBoxKohi = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 695);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 40;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(115, 20);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(165, 20);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(361, 20);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(53, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保記番";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(6, 9);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(47, 24);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(53, 20);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(29, 12);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(663, 80);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 33;
            this.labelTotal.Text = "合計";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(107, 722);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 681);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 681);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 681);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(10, 681);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label23);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.verifyBoxNumbering);
            this.panelRight.Controls.Add(this.verifyBoxRatio);
            this.panelRight.Controls.Add(this.label42);
            this.panelRight.Controls.Add(this.label22);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.label41);
            this.panelRight.Controls.Add(this.labelFamily);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.label35);
            this.panelRight.Controls.Add(this.verifyBoxBirthE);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.label36);
            this.panelRight.Controls.Add(this.label37);
            this.panelRight.Controls.Add(this.verifyBoxBirthY);
            this.panelRight.Controls.Add(this.label38);
            this.panelRight.Controls.Add(this.verifyBoxBirthM);
            this.panelRight.Controls.Add(this.verifyBoxBirthD);
            this.panelRight.Controls.Add(this.label39);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.verifyBoxSeikyu);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.verifyBoxHnumN);
            this.panelRight.Controls.Add(this.label34);
            this.panelRight.Controls.Add(this.verifyBoxHnumS);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.verifyBoxKohi);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 722);
            this.panelRight.TabIndex = 0;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 42;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label23.Location = new System.Drawing.Point(646, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 24);
            this.label23.TabIndex = 14;
            this.label23.Text = "本人:2\r\n家族:6";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(424, 69);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDays.TabIndex = 30;
            this.verifyBoxDays.TextV = "";
            this.verifyBoxDays.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(524, 69);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(123, 23);
            this.verifyBoxNumbering.TabIndex = 32;
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(765, 10);
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRatio.TabIndex = 16;
            this.verifyBoxRatio.TextV = "";
            this.verifyBoxRatio.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(383, 80);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 12);
            this.label42.TabIndex = 29;
            this.label42.Text = "実日数";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(465, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 12);
            this.label22.TabIndex = 31;
            this.label22.Text = "ナンバリング";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(620, 9);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 13;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(712, 21);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 12);
            this.label41.TabIndex = 15;
            this.label41.Text = "給付割合";
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(561, 20);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(59, 12);
            this.labelFamily.TabIndex = 12;
            this.labelFamily.Text = "本人/家族";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.labelF1);
            this.panel1.Controls.Add(this.labelF3);
            this.panel1.Controls.Add(this.labelF2);
            this.panel1.Controls.Add(this.verifyBoxF1);
            this.panel1.Controls.Add(this.verifyBoxF1FushoY);
            this.panel1.Controls.Add(this.verifyBoxF2FushoY);
            this.panel1.Controls.Add(this.verifyBoxF1FirstY);
            this.panel1.Controls.Add(this.verifyBoxF3FushoY);
            this.panel1.Controls.Add(this.verifyBoxF2FirstY);
            this.panel1.Controls.Add(this.verifyBoxF1FushoM);
            this.panel1.Controls.Add(this.verifyBoxF3FirstY);
            this.panel1.Controls.Add(this.verifyBoxF2FushoM);
            this.panel1.Controls.Add(this.verifyBoxF1FirstM);
            this.panel1.Controls.Add(this.verifyBoxF3FushoM);
            this.panel1.Controls.Add(this.verifyBoxF2FirstM);
            this.panel1.Controls.Add(this.verifyBoxF1FushoD);
            this.panel1.Controls.Add(this.verifyBoxF3FirstM);
            this.panel1.Controls.Add(this.verifyBoxF2FushoD);
            this.panel1.Controls.Add(this.verifyBoxF1FirstD);
            this.panel1.Controls.Add(this.verifyBoxF3FushoD);
            this.panel1.Controls.Add(this.verifyBoxF2FirstD);
            this.panel1.Controls.Add(this.verifyBoxF3FirstD);
            this.panel1.Controls.Add(this.verifyBoxF1Start);
            this.panel1.Controls.Add(this.verifyBoxF2Start);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.verifyBoxF3Start);
            this.panel1.Controls.Add(this.verifyBoxF1Finish);
            this.panel1.Controls.Add(this.verifyBoxF2Finish);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.verifyBoxF3Finish);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.verifyBoxF3);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.verifyBoxF2);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Location = new System.Drawing.Point(0, 495);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(673, 194);
            this.panel1.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 5;
            this.label16.Text = "負傷1";
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(40, 6);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(41, 12);
            this.labelF1.TabIndex = 0;
            this.labelF1.Text = "負傷名";
            // 
            // labelF3
            // 
            this.labelF3.AutoSize = true;
            this.labelF3.Location = new System.Drawing.Point(5, 139);
            this.labelF3.Name = "labelF3";
            this.labelF3.Size = new System.Drawing.Size(35, 12);
            this.labelF3.TabIndex = 41;
            this.labelF3.Text = "負傷3";
            // 
            // labelF2
            // 
            this.labelF2.AutoSize = true;
            this.labelF2.Location = new System.Drawing.Point(5, 83);
            this.labelF2.Name = "labelF2";
            this.labelF2.Size = new System.Drawing.Size(35, 12);
            this.labelF2.TabIndex = 23;
            this.labelF2.Text = "負傷2";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(40, 21);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF1.TabIndex = 6;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FushoY
            // 
            this.verifyBoxF1FushoY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FushoY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FushoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FushoY.Location = new System.Drawing.Point(252, 20);
            this.verifyBoxF1FushoY.MaxLength = 2;
            this.verifyBoxF1FushoY.Name = "verifyBoxF1FushoY";
            this.verifyBoxF1FushoY.NewLine = false;
            this.verifyBoxF1FushoY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FushoY.TabIndex = 7;
            this.verifyBoxF1FushoY.TextV = "";
            this.verifyBoxF1FushoY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FushoY
            // 
            this.verifyBoxF2FushoY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FushoY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FushoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FushoY.Location = new System.Drawing.Point(252, 76);
            this.verifyBoxF2FushoY.MaxLength = 2;
            this.verifyBoxF2FushoY.Name = "verifyBoxF2FushoY";
            this.verifyBoxF2FushoY.NewLine = false;
            this.verifyBoxF2FushoY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FushoY.TabIndex = 25;
            this.verifyBoxF2FushoY.TextV = "";
            this.verifyBoxF2FushoY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(404, 21);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 13;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FushoY
            // 
            this.verifyBoxF3FushoY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FushoY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FushoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FushoY.Location = new System.Drawing.Point(252, 132);
            this.verifyBoxF3FushoY.MaxLength = 2;
            this.verifyBoxF3FushoY.Name = "verifyBoxF3FushoY";
            this.verifyBoxF3FushoY.NewLine = false;
            this.verifyBoxF3FushoY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FushoY.TabIndex = 43;
            this.verifyBoxF3FushoY.TextV = "";
            this.verifyBoxF3FushoY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FirstY
            // 
            this.verifyBoxF2FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FirstY.Location = new System.Drawing.Point(404, 77);
            this.verifyBoxF2FirstY.MaxLength = 2;
            this.verifyBoxF2FirstY.Name = "verifyBoxF2FirstY";
            this.verifyBoxF2FirstY.NewLine = false;
            this.verifyBoxF2FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FirstY.TabIndex = 31;
            this.verifyBoxF2FirstY.TextV = "";
            this.verifyBoxF2FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FushoM
            // 
            this.verifyBoxF1FushoM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FushoM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FushoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FushoM.Location = new System.Drawing.Point(297, 20);
            this.verifyBoxF1FushoM.MaxLength = 2;
            this.verifyBoxF1FushoM.Name = "verifyBoxF1FushoM";
            this.verifyBoxF1FushoM.NewLine = false;
            this.verifyBoxF1FushoM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FushoM.TabIndex = 9;
            this.verifyBoxF1FushoM.TextV = "";
            this.verifyBoxF1FushoM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FirstY
            // 
            this.verifyBoxF3FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FirstY.Location = new System.Drawing.Point(404, 133);
            this.verifyBoxF3FirstY.MaxLength = 2;
            this.verifyBoxF3FirstY.Name = "verifyBoxF3FirstY";
            this.verifyBoxF3FirstY.NewLine = false;
            this.verifyBoxF3FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FirstY.TabIndex = 49;
            this.verifyBoxF3FirstY.TextV = "";
            this.verifyBoxF3FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FushoM
            // 
            this.verifyBoxF2FushoM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FushoM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FushoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FushoM.Location = new System.Drawing.Point(297, 76);
            this.verifyBoxF2FushoM.MaxLength = 2;
            this.verifyBoxF2FushoM.Name = "verifyBoxF2FushoM";
            this.verifyBoxF2FushoM.NewLine = false;
            this.verifyBoxF2FushoM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FushoM.TabIndex = 27;
            this.verifyBoxF2FushoM.TextV = "";
            this.verifyBoxF2FushoM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(449, 21);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 15;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FushoM
            // 
            this.verifyBoxF3FushoM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FushoM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FushoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FushoM.Location = new System.Drawing.Point(297, 132);
            this.verifyBoxF3FushoM.MaxLength = 2;
            this.verifyBoxF3FushoM.Name = "verifyBoxF3FushoM";
            this.verifyBoxF3FushoM.NewLine = false;
            this.verifyBoxF3FushoM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FushoM.TabIndex = 45;
            this.verifyBoxF3FushoM.TextV = "";
            this.verifyBoxF3FushoM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FirstM
            // 
            this.verifyBoxF2FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FirstM.Location = new System.Drawing.Point(449, 77);
            this.verifyBoxF2FirstM.MaxLength = 2;
            this.verifyBoxF2FirstM.Name = "verifyBoxF2FirstM";
            this.verifyBoxF2FirstM.NewLine = false;
            this.verifyBoxF2FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FirstM.TabIndex = 33;
            this.verifyBoxF2FirstM.TextV = "";
            this.verifyBoxF2FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FushoD
            // 
            this.verifyBoxF1FushoD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FushoD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FushoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FushoD.Location = new System.Drawing.Point(342, 20);
            this.verifyBoxF1FushoD.MaxLength = 2;
            this.verifyBoxF1FushoD.Name = "verifyBoxF1FushoD";
            this.verifyBoxF1FushoD.NewLine = false;
            this.verifyBoxF1FushoD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FushoD.TabIndex = 11;
            this.verifyBoxF1FushoD.TextV = "";
            this.verifyBoxF1FushoD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FirstM
            // 
            this.verifyBoxF3FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FirstM.Location = new System.Drawing.Point(449, 133);
            this.verifyBoxF3FirstM.MaxLength = 2;
            this.verifyBoxF3FirstM.Name = "verifyBoxF3FirstM";
            this.verifyBoxF3FirstM.NewLine = false;
            this.verifyBoxF3FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FirstM.TabIndex = 51;
            this.verifyBoxF3FirstM.TextV = "";
            this.verifyBoxF3FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FushoD
            // 
            this.verifyBoxF2FushoD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FushoD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FushoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FushoD.Location = new System.Drawing.Point(342, 76);
            this.verifyBoxF2FushoD.MaxLength = 2;
            this.verifyBoxF2FushoD.Name = "verifyBoxF2FushoD";
            this.verifyBoxF2FushoD.NewLine = false;
            this.verifyBoxF2FushoD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FushoD.TabIndex = 29;
            this.verifyBoxF2FushoD.TextV = "";
            this.verifyBoxF2FushoD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(494, 21);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 17;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FushoD
            // 
            this.verifyBoxF3FushoD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FushoD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FushoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FushoD.Location = new System.Drawing.Point(342, 132);
            this.verifyBoxF3FushoD.MaxLength = 2;
            this.verifyBoxF3FushoD.Name = "verifyBoxF3FushoD";
            this.verifyBoxF3FushoD.NewLine = false;
            this.verifyBoxF3FushoD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FushoD.TabIndex = 47;
            this.verifyBoxF3FushoD.TextV = "";
            this.verifyBoxF3FushoD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2FirstD
            // 
            this.verifyBoxF2FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2FirstD.Location = new System.Drawing.Point(494, 77);
            this.verifyBoxF2FirstD.MaxLength = 2;
            this.verifyBoxF2FirstD.Name = "verifyBoxF2FirstD";
            this.verifyBoxF2FirstD.NewLine = false;
            this.verifyBoxF2FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2FirstD.TabIndex = 35;
            this.verifyBoxF2FirstD.TextV = "";
            this.verifyBoxF2FirstD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF3FirstD
            // 
            this.verifyBoxF3FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3FirstD.Location = new System.Drawing.Point(494, 133);
            this.verifyBoxF3FirstD.MaxLength = 2;
            this.verifyBoxF3FirstD.Name = "verifyBoxF3FirstD";
            this.verifyBoxF3FirstD.NewLine = false;
            this.verifyBoxF3FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3FirstD.TabIndex = 53;
            this.verifyBoxF3FirstD.TextV = "";
            this.verifyBoxF3FirstD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(563, 21);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 19;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2Start
            // 
            this.verifyBoxF2Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Start.Location = new System.Drawing.Point(563, 77);
            this.verifyBoxF2Start.MaxLength = 2;
            this.verifyBoxF2Start.Name = "verifyBoxF2Start";
            this.verifyBoxF2Start.NewLine = false;
            this.verifyBoxF2Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2Start.TabIndex = 37;
            this.verifyBoxF2Start.TextV = "";
            this.verifyBoxF2Start.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(621, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "終了日";
            // 
            // verifyBoxF3Start
            // 
            this.verifyBoxF3Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Start.Location = new System.Drawing.Point(563, 133);
            this.verifyBoxF3Start.MaxLength = 2;
            this.verifyBoxF3Start.Name = "verifyBoxF3Start";
            this.verifyBoxF3Start.NewLine = false;
            this.verifyBoxF3Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3Start.TabIndex = 55;
            this.verifyBoxF3Start.TextV = "";
            this.verifyBoxF3Start.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(623, 21);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 21;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF2Finish
            // 
            this.verifyBoxF2Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Finish.Location = new System.Drawing.Point(623, 77);
            this.verifyBoxF2Finish.MaxLength = 2;
            this.verifyBoxF2Finish.Name = "verifyBoxF2Finish";
            this.verifyBoxF2Finish.NewLine = false;
            this.verifyBoxF2Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF2Finish.TabIndex = 39;
            this.verifyBoxF2Finish.TextV = "";
            this.verifyBoxF2Finish.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(561, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "開始日";
            // 
            // verifyBoxF3Finish
            // 
            this.verifyBoxF3Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Finish.Location = new System.Drawing.Point(623, 133);
            this.verifyBoxF3Finish.MaxLength = 2;
            this.verifyBoxF3Finish.Name = "verifyBoxF3Finish";
            this.verifyBoxF3Finish.NewLine = false;
            this.verifyBoxF3Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF3Finish.TabIndex = 57;
            this.verifyBoxF3Finish.TextV = "";
            this.verifyBoxF3Finish.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(651, 144);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 12);
            this.label31.TabIndex = 58;
            this.label31.Text = "日";
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(40, 133);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF3.TabIndex = 42;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF3.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(651, 88);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 40;
            this.label21.Text = "日";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(40, 77);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF2.TabIndex = 24;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF2.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(250, 5);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 12);
            this.label48.TabIndex = 1;
            this.label48.Text = "負傷日";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(651, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 22;
            this.label15.Text = "日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(402, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "初検日";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(591, 144);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 56;
            this.label30.Text = "日";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(591, 88);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 12);
            this.label20.TabIndex = 38;
            this.label20.Text = "日";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(280, 31);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(17, 12);
            this.label47.TabIndex = 8;
            this.label47.Text = "年";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(591, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 20;
            this.label14.Text = "日";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(370, 143);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(17, 12);
            this.label46.TabIndex = 48;
            this.label46.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(432, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "年";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(280, 87);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(17, 12);
            this.label45.TabIndex = 26;
            this.label45.Text = "年";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(522, 144);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 54;
            this.label27.Text = "日";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(370, 87);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(17, 12);
            this.label44.TabIndex = 30;
            this.label44.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(432, 88);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 32;
            this.label17.Text = "年";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(280, 143);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 12);
            this.label43.TabIndex = 44;
            this.label43.Text = "年";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(522, 88);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 36;
            this.label19.Text = "日";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(370, 31);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 12);
            this.label40.TabIndex = 12;
            this.label40.Text = "日";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(432, 144);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 50;
            this.label25.Text = "年";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(325, 31);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 12);
            this.label33.TabIndex = 10;
            this.label33.Text = "月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(522, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "日";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(325, 143);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 12);
            this.label32.TabIndex = 46;
            this.label32.Text = "月";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(477, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "月";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(325, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 28;
            this.label24.Text = "月";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(477, 144);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(17, 12);
            this.label26.TabIndex = 52;
            this.label26.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(477, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "月";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(101, 80);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 20;
            this.labelBirthday.Text = "生年月日";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label35.Location = new System.Drawing.Point(180, 68);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 24);
            this.label35.TabIndex = 22;
            this.label35.Text = "昭：3\r\n平：4";
            // 
            // verifyBoxBirthE
            // 
            this.verifyBoxBirthE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthE.Location = new System.Drawing.Point(154, 69);
            this.verifyBoxBirthE.Name = "verifyBoxBirthE";
            this.verifyBoxBirthE.NewLine = false;
            this.verifyBoxBirthE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBirthE.TabIndex = 21;
            this.verifyBoxBirthE.TextV = "";
            this.verifyBoxBirthE.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(6, 80);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 17;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(35, 69);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 18;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label36.Location = new System.Drawing.Point(62, 68);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 24);
            this.label36.TabIndex = 19;
            this.label36.Text = "男: 1\r\n女: 2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(241, 80);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 24;
            this.label37.Text = "年";
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(209, 69);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthY.TabIndex = 23;
            this.verifyBoxBirthY.TextV = "";
            this.verifyBoxBirthY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(290, 80);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 26;
            this.label38.Text = "月";
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(258, 69);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 25;
            this.verifyBoxBirthM.TextV = "";
            this.verifyBoxBirthM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(307, 69);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 27;
            this.verifyBoxBirthD.TextV = "";
            this.verifyBoxBirthD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(339, 80);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 28;
            this.label39.Text = "日";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 132);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 363);
            this.scrollPictureControl1.TabIndex = 37;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 39;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(297, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "市町村:1\r\n老人:2";
            // 
            // verifyBoxSeikyu
            // 
            this.verifyBoxSeikyu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSeikyu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSeikyu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSeikyu.Location = new System.Drawing.Point(815, 69);
            this.verifyBoxSeikyu.Name = "verifyBoxSeikyu";
            this.verifyBoxSeikyu.NewLine = false;
            this.verifyBoxSeikyu.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxSeikyu.TabIndex = 36;
            this.verifyBoxSeikyu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSeikyu.TextV = "";
            this.verifyBoxSeikyu.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(692, 69);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 34;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxHnumN
            // 
            this.verifyBoxHnumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumN.Location = new System.Drawing.Point(466, 9);
            this.verifyBoxHnumN.Name = "verifyBoxHnumN";
            this.verifyBoxHnumN.NewLine = false;
            this.verifyBoxHnumN.Size = new System.Drawing.Size(77, 23);
            this.verifyBoxHnumN.TabIndex = 11;
            this.verifyBoxHnumN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumN.TextV = "";
            this.verifyBoxHnumN.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(786, 80);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 35;
            this.label34.Text = "請求";
            // 
            // verifyBoxHnumS
            // 
            this.verifyBoxHnumS.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumS.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumS.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumS.Location = new System.Drawing.Point(414, 9);
            this.verifyBoxHnumS.Name = "verifyBoxHnumS";
            this.verifyBoxHnumS.NewLine = false;
            this.verifyBoxHnumS.Size = new System.Drawing.Size(52, 23);
            this.verifyBoxHnumS.TabIndex = 10;
            this.verifyBoxHnumS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumS.TextV = "";
            this.verifyBoxHnumS.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "公費(扶助)";
            // 
            // verifyBoxKohi
            // 
            this.verifyBoxKohi.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKohi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKohi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKohi.Location = new System.Drawing.Point(265, 9);
            this.verifyBoxKohi.MaxLength = 1;
            this.verifyBoxKohi.Name = "verifyBoxKohi";
            this.verifyBoxKohi.NewLine = false;
            this.verifyBoxKohi.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxKohi.TabIndex = 7;
            this.verifyBoxKohi.TextV = "";
            this.verifyBoxKohi.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(82, 9);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(132, 9);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnumS;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label labelF3;
        private System.Windows.Forms.Label labelF2;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private VerifyBox verifyBoxKohi;
        private VerifyBox verifyBoxHnumN;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF3Finish;
        private VerifyBox verifyBoxF2Finish;
        private VerifyBox verifyBoxF1Finish;
        private VerifyBox verifyBoxF3Start;
        private VerifyBox verifyBoxF2Start;
        private VerifyBox verifyBoxF1Start;
        private VerifyBox verifyBoxF3FirstD;
        private VerifyBox verifyBoxF2FirstD;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF3FirstM;
        private VerifyBox verifyBoxF2FirstM;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF3FirstY;
        private VerifyBox verifyBoxF2FirstY;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxSeikyu;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label35;
        private VerifyBox verifyBoxBirthE;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label labelFamily;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label16;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxF1FushoY;
        private VerifyBox verifyBoxF2FushoY;
        private VerifyBox verifyBoxF3FushoY;
        private VerifyBox verifyBoxF1FushoM;
        private VerifyBox verifyBoxF2FushoM;
        private VerifyBox verifyBoxF3FushoM;
        private VerifyBox verifyBoxF1FushoD;
        private VerifyBox verifyBoxF2FushoD;
        private VerifyBox verifyBoxF3FushoD;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelInputerName;
    }
}