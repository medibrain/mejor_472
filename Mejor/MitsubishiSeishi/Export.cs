﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.MitsubishiSeishi
{
    class Export
    {
        public static bool DoExport(int cym)
        {
            string dir;

            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return false;
                dir = f.SelectedPath;
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            System.IO.Directory.CreateDirectory(dir + "\\image");

            wf.LogPrint("対象の申請書を取得しています");
            var file = dir + "\\" + cym.ToString() + ".txt";
            var apps = App.GetApps(cym);

            wf.SetMax(apps.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;

            var imageNames = new List<string>();
            var imageName = string.Empty;

            try
            {
                using (var sw = new System.IO.StreamWriter(file, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (var item in apps)
                    {
                        if (wf.Cancel)
                        {
                            MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                            return false;
                        }

                        wf.InvokeValue++;
                        if (item.MediYear > 0)
                        {
                            if (imageNames.Count == 1)
                            {
                                ImageUtility.SaveOneWithoutCheck(imageNames, imageName);
                            }
                            else if (imageNames.Count > 1)
                            {
                                ImageUtility.Save(imageNames, imageName);
                            }

                            var de = DaiwaExport.Create(item, "C8", "01", item.PayCode, true);
                            sw.WriteLine(de.ToString());

                            imageName = dir + "\\image\\" + item.Numbering + ".tiff";
                            imageNames.Clear();
                            imageNames.Add(item.GetImageFullPath());
                        }
                        else if (item.MediYear == (int)APP_SPECIAL_CODE.続紙)
                        {
                            imageNames.Add(item.GetImageFullPath());
                        }
                    }

                    //最終画像
                    if (imageNames.Count != 0) ImageUtility.Save(imageNames, imageName);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が完了しました");
            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
                var sdic = new Dictionary<int, Scan>();

                foreach (var item in scans) sdic.Add(item, Scan.Select(item));

                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var rex = new string[26];
                    //先頭行は見出し
                    rex[0] = "ID";
                    rex[1] = "処理年";
                    rex[2] = "処理月";
                    rex[3] = "診療年";
                    rex[4] = "診療月";
                    rex[5] = "保険者番号";
                    rex[6] = "被保険者番号";
                    rex[7] = "住所";
                    rex[8] = "氏名";
                    rex[9] = "性別";
                    rex[10] = "生年月日";
                    rex[11] = "請求区分";
                    rex[12] = "往療料";
                    rex[13] = "施術所記号";
                    rex[14] = "合計金額";
                    rex[15] = "請求金額";
                    rex[16] = "診療日数";
                    rex[17] = "ナンバリング";
                    rex[18] = "照会理由";
                    rex[19] = "点検結果";
                    rex[20] = "被保険者名";
                    rex[21] = "受療者名";
                    rex[22] = "郵便番号";
                    rex[23] = "住所";
                    rex[24] = "施術所名";
                    rex[25] = "支払先コード";

                    sw.WriteLine(string.Join(",", rex));

                    var ss = new List<string>();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.InsNum);
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.PersonName);
                        ss.Add(item.Sex == 1 ? "男" : "女");
                        ss.Add(item.Birthday.ToShortDateString());
                        ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        ss.Add(item.Distance == 999 ? "あり" : "");
                        ss.Add(item.DrNum);
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Numbering);
                        ss.Add(item.InspectDescription);
                        ss.Add(item.InspectInfo);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(item.HihoZip);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.ClinicName);
                        ss.Add(item.PayCode);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
