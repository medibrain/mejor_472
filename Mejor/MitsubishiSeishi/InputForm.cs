﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.MitsubishiSeishi
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 30);
        Point posPerson = new Point(60, 30);
        Point posTotal = new Point(800, 2060);
        Point posBuiName = new Point(120, 780);
        Point posBuiDate = new Point(950, 780);
        Point posNumbering = new Point(800, 30);

        Control[] ymConts, hnumConts, personConts, totalConts, buiNameConts, buiDateConts, numberingConts;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            ymConts = new Control[] { verifyBoxY, verifyBoxM, verifyBoxKohi };
            hnumConts = new Control[] { verifyBoxHnumS, verifyBoxHnumN, verifyBoxFamily, verifyBoxRatio };
            personConts = new Control[] { verifyBoxSex, verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD };
            totalConts = new Control[] { verifyBoxTotal, verifyBoxSeikyu };
            buiNameConts = new Control[] { verifyBoxF1, verifyBoxF1FushoY, verifyBoxF1FushoM,verifyBoxF1FushoD,
                verifyBoxF2, verifyBoxF2FushoY, verifyBoxF2FushoM,verifyBoxF2FushoD,
                verifyBoxF3, verifyBoxF3FushoY, verifyBoxF3FushoM,verifyBoxF3FushoD, };
            buiDateConts = new Control[] { verifyBoxDays,
                verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD, verifyBoxF1Start, verifyBoxF1Finish,
                verifyBoxF2FirstY, verifyBoxF2FirstM, verifyBoxF2FirstD, verifyBoxF2Start, verifyBoxF2Finish,
                verifyBoxF3FirstY, verifyBoxF3FirstM, verifyBoxF3FirstD, verifyBoxF3Start, verifyBoxF3Finish, };
            numberingConts = new Control[] { verifyBoxNumbering };

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            
            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;
            
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            setStatus(verifyBoxY, DateTime.Today.Year < adYear || adYear < DateTime.Today.Year - 5);


            /*
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year);
            setStatus(verifyBoxY, DateTime.Today.Year < adYear || adYear < DateTime.Today.Year - 5);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            */
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //公費
            var kohi = verifyBoxKohi.GetIntValue();
            if (verifyBoxKohi.Text == "") kohi = 0;//空欄が許可されていなくなっていたので修正
            setStatus(verifyBoxKohi, kohi != 0 && kohi != 1 && kohi != 2);

            //被保険者番号
            int hnumS = verifyBoxHnumS.GetIntValue();
            setStatus(verifyBoxHnumS, hnumS < 1);
            int hnumN = verifyBoxHnumN.GetIntValue();
            setStatus(verifyBoxHnumN, hnumN < 1);
            string hnum = verifyBoxHnumS.Text.Trim() + "-" + verifyBoxHnumN.Text.Trim();

            //本家区分
            int family= verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, family != 2 && family != 6);

            //割合
            int ratio= verifyBoxRatio.GetIntValue();
            setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

            //ナンバリング
            int n = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, n < 10000000 || 99999999 < n);

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || total > 200000);

            //請求
            int seikyu = verifyBoxSeikyu.GetIntValue();
            setStatus(verifyBoxSeikyu, seikyu < 100 || total < seikyu);

            //負傷
            fusho1Check(verifyBoxF1);

            var f1FushoDt = dateCheck(4, verifyBoxF1FushoY, verifyBoxF1FushoM, verifyBoxF1FushoD);
            var f1FirstDt = dateCheck(4, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            var f1Start = dateCheck(4, year, month, verifyBoxF1Start);
            var f1Finish = dateCheck(4, year, month, verifyBoxF1Finish);

            DateTime f2FushoDt, f2FirstDt, f2Start, f2Finish;
            fushoCheck(verifyBoxF2);
            if (verifyBoxF2.Text.Trim() != string.Empty)
            {
                f2FushoDt = dateCheck(4, verifyBoxF2FushoY, verifyBoxF2FushoM, verifyBoxF2FushoD);
                f2FirstDt = dateCheck(4, verifyBoxF2FirstY, verifyBoxF2FirstM, verifyBoxF2FirstD);
                f2Start = dateCheck(4, year, month, verifyBoxF2Start);
                f2Finish = dateCheck(4, year, month, verifyBoxF2Finish);
            }
            else
            {
                f2FushoDt = DateTimeEx.DateTimeNull;
                f2FirstDt = DateTimeEx.DateTimeNull;
                f2Start = DateTimeEx.DateTimeNull;
                f2Finish = DateTimeEx.DateTimeNull;
            }

            DateTime f3FushoDt, f3FirstDt, f3Start, f3Finish;
            fushoCheck(verifyBoxF3);
            if (verifyBoxF3.Text.Trim() != string.Empty)
            {
                f3FushoDt = dateCheck(4, verifyBoxF3FushoY, verifyBoxF3FushoM, verifyBoxF3FushoD);
                f3FirstDt = dateCheck(4, verifyBoxF3FirstY, verifyBoxF3FirstM, verifyBoxF3FirstD);
                f3Start = dateCheck(4, year, month, verifyBoxF3Start);
                f3Finish = dateCheck(4, year, month, verifyBoxF3Finish);
            }
            else
            {
                f3FushoDt = DateTimeEx.DateTimeNull;
                f3FirstDt = DateTimeEx.DateTimeNull;
                f3Start = DateTimeEx.DateTimeNull;
                f3Finish = DateTimeEx.DateTimeNull;
            }

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //合計金額：請求金額：本家区分のトリプルチェック
            //金額でのエラーがあればいったん登録中断
            bool ratioError = (int)(total * ratio / 10) != seikyu;
            if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
            if (ratioError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxSeikyu.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoType = kohi;
            app.HihoNum = hnum;
            app.Family = family;
            app.Ratio = ratio;
            app.Sex = sex;
            app.Birthday = birth;
            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.CountedDays = days;
            app.Total = total;
            app.Charge = seikyu;

            //申請書種別
            app.AppType = scan.AppType;

            //負傷
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoDate1 = f1FushoDt;
            app.FushoFirstDate1 = f1FirstDt;
            app.FushoStartDate1 = f1Start;
            app.FushoFinishDate1 = f1Finish;
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoDate2 = f2FushoDt;
            app.FushoFirstDate2 = f2FirstDt;
            app.FushoStartDate2 = f2Start;
            app.FushoFinishDate2 = f2Finish;
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoDate3 = f3FushoDt;
            app.FushoFirstDate3 = f3FirstDt;
            app.FushoStartDate3 = f3Start;
            app.FushoFinishDate3 = f3Finish;

            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void setApp(App app)
        {
            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                //ベリファイ入力時
                setVerify(app);
            }
            else
            {
                //App_Flagのチェック
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    setInputedApp(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    }
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        private void setVerify(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else
            {
                setVerifyVal(verifyBoxY, app.MediYear, nv);
                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                setVerifyVal(verifyBoxKohi, app.HihoType == 0 ? "" : app.HihoType.ToString(), nv);

                var sm = app.HihoNum.Split('-');
                if (sm.Length == 2)
                {
                    setVerifyVal(verifyBoxHnumS, sm[0], nv);
                    setVerifyVal(verifyBoxHnumN, sm[1], nv);
                }
                setVerifyVal(verifyBoxFamily, app.Family, nv);
                setVerifyVal(verifyBoxRatio, app.Ratio, nv);

                //個人情報
                setVerifyVal(verifyBoxSex, app.Sex, nv);
                setVerifyVal(verifyBoxBirthE, DateTimeEx.GetEraNumber(app.Birthday), nv);
                setVerifyVal(verifyBoxBirthY, DateTimeEx.GetJpYear(app.Birthday), nv);
                setVerifyVal(verifyBoxBirthM, app.Birthday.Month, nv);
                setVerifyVal(verifyBoxBirthD, app.Birthday.Day, nv);

                //診療情報
                setVerifyVal(verifyBoxNumbering, app.Numbering, nv);
                setVerifyVal(verifyBoxDays, app.CountedDays, nv);
                setVerifyVal(verifyBoxTotal, app.Total, nv);
                setVerifyVal(verifyBoxSeikyu, app.Charge, nv);

                //負傷1
                setVerifyVal(verifyBoxF1, app.FushoName1, nv);
                if (app.FushoName1 != string.Empty)
                {
                    setVerifyVal(verifyBoxF1FushoY, DateTimeEx.GetJpYear(app.FushoDate1), nv);
                    setVerifyVal(verifyBoxF1FushoM, app.FushoDate1.Month, nv);
                    setVerifyVal(verifyBoxF1FushoD, app.FushoDate1.Day, nv);
                    setVerifyVal(verifyBoxF1FirstY, DateTimeEx.GetJpYear(app.FushoFirstDate1), nv);
                    setVerifyVal(verifyBoxF1FirstM, app.FushoFirstDate1.Month, nv);
                    setVerifyVal(verifyBoxF1FirstD, app.FushoFirstDate1.Day, nv);
                    setVerifyVal(verifyBoxF1Start, app.FushoStartDate1.Day, nv);
                    setVerifyVal(verifyBoxF1Finish, app.FushoFinishDate1.Day, nv);
                }
                else
                {
                    setVerifyVal(verifyBoxF1FushoY, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FushoM, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FushoD, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FirstY, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FirstM, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FirstD, string.Empty, nv);
                    setVerifyVal(verifyBoxF1Start, string.Empty, nv);
                    setVerifyVal(verifyBoxF1Finish, string.Empty, nv);
                }

                //負傷2
                setVerifyVal(verifyBoxF2, app.FushoName2, nv);
                if(app.FushoName2 != string.Empty)
                { 
                    setVerifyVal(verifyBoxF2FushoY, DateTimeEx.GetJpYear(app.FushoDate2), nv);
                    setVerifyVal(verifyBoxF2FushoM, app.FushoDate2.Month, nv);
                    setVerifyVal(verifyBoxF2FushoD, app.FushoDate2.Day, nv);
                    setVerifyVal(verifyBoxF2FirstY, DateTimeEx.GetJpYear(app.FushoFirstDate2), nv);
                    setVerifyVal(verifyBoxF2FirstM, app.FushoFirstDate2.Month, nv);
                    setVerifyVal(verifyBoxF2FirstD, app.FushoFirstDate2.Day, nv);
                    setVerifyVal(verifyBoxF2Start, app.FushoStartDate2.Day, nv);
                    setVerifyVal(verifyBoxF2Finish, app.FushoFinishDate2.Day, nv);
                }
                else
                {
                    setVerifyVal(verifyBoxF2FushoY, string.Empty, nv);
                    setVerifyVal(verifyBoxF2FushoM, string.Empty, nv);
                    setVerifyVal(verifyBoxF2FushoD, string.Empty, nv);
                    setVerifyVal(verifyBoxF2FirstY, string.Empty, nv);
                    setVerifyVal(verifyBoxF2FirstM, string.Empty, nv);
                    setVerifyVal(verifyBoxF2FirstD, string.Empty, nv);
                    setVerifyVal(verifyBoxF2Start, string.Empty, nv);
                    setVerifyVal(verifyBoxF2Finish, string.Empty, nv);
                }

                //負傷3
                setVerifyVal(verifyBoxF3, app.FushoName3, nv);
                if (app.FushoName3 != string.Empty)
                {
                    setVerifyVal(verifyBoxF3FushoY, DateTimeEx.GetJpYear(app.FushoDate3), nv);
                    setVerifyVal(verifyBoxF3FushoM, app.FushoDate3.Month, nv);
                    setVerifyVal(verifyBoxF3FushoD, app.FushoDate3.Day, nv);
                    setVerifyVal(verifyBoxF3FirstY, DateTimeEx.GetJpYear(app.FushoFirstDate3), nv);
                    setVerifyVal(verifyBoxF3FirstM, app.FushoFirstDate3.Month, nv);
                    setVerifyVal(verifyBoxF3FirstD, app.FushoFirstDate3.Day, nv);
                    setVerifyVal(verifyBoxF3Start, app.FushoStartDate3.Day, nv);
                    setVerifyVal(verifyBoxF3Finish, app.FushoFinishDate3.Day, nv);
                }
                else
                {
                    setVerifyVal(verifyBoxF3FushoY, string.Empty, nv);
                    setVerifyVal(verifyBoxF3FushoM, string.Empty, nv);
                    setVerifyVal(verifyBoxF3FushoD, string.Empty, nv);
                    setVerifyVal(verifyBoxF3FirstY, string.Empty, nv);
                    setVerifyVal(verifyBoxF3FirstM, string.Empty, nv);
                    setVerifyVal(verifyBoxF3FirstD, string.Empty, nv);
                    setVerifyVal(verifyBoxF3Start, string.Empty, nv);
                    setVerifyVal(verifyBoxF3Finish, string.Empty, nv);
                }
            }
            missCounterReset();
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxKohi.Text = app.HihoType == 0 ? "" : app.HihoType.ToString();

                //被保険者情報
                var sm = app.HihoNum.Split('-');
                if (sm.Length == 2)
                {
                    verifyBoxHnumS.Text = sm[0];
                    verifyBoxHnumN.Text = sm[1];
                }
                verifyBoxFamily.Text= app.Family.ToString();
                verifyBoxRatio.Text = app.Ratio.ToString();

                //個人欄
                verifyBoxSex.Text = app.Sex.ToString();
                verifyBoxBirthE.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                verifyBoxBirthY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                verifyBoxBirthM.Text = app.Birthday.Month.ToString();
                verifyBoxBirthD.Text = app.Birthday.Day.ToString();

                //申請情報
                verifyBoxNumbering.Text = app.Numbering;
                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxTotal.Text = app.Total.ToString();
                verifyBoxSeikyu.Text = app.Charge.ToString();

                //負傷
                verifyBoxF1.Text = app.FushoName1;
                if(!app.FushoDate1.IsNullDate())
                {
                    verifyBoxF1FushoY.Text = DateTimeEx.GetJpYear(app.FushoDate1).ToString();
                    verifyBoxF1FushoM.Text = app.FushoDate1.Month.ToString();
                    verifyBoxF1FushoD.Text = app.FushoDate1.Day.ToString();
                }
                if (!app.FushoFirstDate1.IsNullDate())
                {
                    verifyBoxF1FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString();
                    verifyBoxF1FirstM.Text = app.FushoFirstDate1.Month.ToString();
                    verifyBoxF1FirstD.Text = app.FushoFirstDate1.Day.ToString();
                }
                if(!app.FushoStartDate1.IsNullDate())
                    verifyBoxF1Start.Text = app.FushoStartDate1.Day.ToString();
                if (!app.FushoFinishDate1.IsNullDate())
                    verifyBoxF1Finish.Text = app.FushoFinishDate1.Day.ToString();

                verifyBoxF2.Text = app.FushoName2;
                if (!app.FushoDate2.IsNullDate())
                {
                    verifyBoxF2FushoY.Text = DateTimeEx.GetJpYear(app.FushoDate2).ToString();
                    verifyBoxF2FushoM.Text = app.FushoDate2.Month.ToString();
                    verifyBoxF2FushoD.Text = app.FushoDate2.Day.ToString();
                }
                if (!app.FushoFirstDate2.IsNullDate())
                {
                    verifyBoxF2FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate2).ToString();
                    verifyBoxF2FirstM.Text = app.FushoFirstDate2.Month.ToString();
                    verifyBoxF2FirstD.Text = app.FushoFirstDate2.Day.ToString();
                }
                if (!app.FushoStartDate2.IsNullDate())
                    verifyBoxF2Start.Text = app.FushoStartDate2.Day.ToString();
                if (!app.FushoFinishDate2.IsNullDate())
                    verifyBoxF2Finish.Text = app.FushoFinishDate2.Day.ToString();

                verifyBoxF3.Text = app.FushoName3;
                if (!app.FushoDate3.IsNullDate())
                {
                    verifyBoxF3FushoY.Text = DateTimeEx.GetJpYear(app.FushoDate3).ToString();
                    verifyBoxF3FushoM.Text = app.FushoDate3.Month.ToString();
                    verifyBoxF3FushoD.Text = app.FushoDate3.Day.ToString();
                }
                if (!app.FushoFirstDate3.IsNullDate())
                {
                    verifyBoxF3FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate3).ToString();
                    verifyBoxF3FirstM.Text = app.FushoFirstDate3.Month.ToString();
                    verifyBoxF3FirstD.Text = app.FushoFirstDate3.Day.ToString();
                }
                if (!app.FushoStartDate3.IsNullDate())
                    verifyBoxF3Start.Text = app.FushoStartDate3.Day.ToString();
                if (!app.FushoFinishDate3.IsNullDate())
                    verifyBoxF3Finish.Text = app.FushoFinishDate3.Day.ToString();
            }
        }


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Action<Control, bool> act = null;

            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            act = new Action<Control, bool>((c, b) =>
            {
                foreach (Control item in c.Controls) act(item, b);
                if ((c is IVerifiable == false) && (c is Label == false)) return;
                if (ignoreControls.Contains(c)) return;
                c.Visible = b;
                if (c is IVerifiable == false) return;
                c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                act(panelRight, false);
            }
            else
            {
                //申請書の場合
                act(panelRight, true);
                buiTabStopAdjust();
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (buiNameConts.Contains(sender)) p = posBuiName;
            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (numberingConts.Contains(sender)) p = posNumbering;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == "Aid")
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            var containsF2 = verifyBoxF2.Text != string.Empty;
            verifyBoxF2FushoY.TabStop = containsF2;
            verifyBoxF2FushoM.TabStop = containsF2;
            verifyBoxF2FushoD.TabStop = containsF2;
            verifyBoxF2FirstY.TabStop = containsF2;
            verifyBoxF2FirstM.TabStop = containsF2;
            verifyBoxF2FirstD.TabStop = containsF2;
            verifyBoxF2Start.TabStop = containsF2;
            verifyBoxF2Finish.TabStop = containsF2;
            verifyBoxF3.TabStop = containsF2;

            var containsF3 = verifyBoxF3.Text != string.Empty && containsF2;
            verifyBoxF3FushoY.TabStop = containsF3;
            verifyBoxF3FushoM.TabStop = containsF3;
            verifyBoxF3FushoD.TabStop = containsF3;
            verifyBoxF3FirstY.TabStop = containsF3;
            verifyBoxF3FirstM.TabStop = containsF3;
            verifyBoxF3FirstD.TabStop = containsF3;
            verifyBoxF3Start.TabStop = containsF3;
            verifyBoxF3Finish.TabStop = containsF3;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiNameConts.Any(c => c.Focused)) posBuiName = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (numberingConts.Any(c => c.Focused)) posNumbering = pos;
        }
    }      
}
