﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Nakanoku
{
    public partial class FormImport : Form
    {

        int cym=0;

        public FormImport(int _cym)
        {
            InitializeComponent();
            cym = _cym;
            
            dispRefrece();
        }

        private void buttonCSV_Click(object sender, EventArgs e)
        {
         
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            if (RefRece.GetCountCYM(cym) > 0)
            {
                if (MessageBox.Show("既にデータが存在します。上書きしますか？", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }
            RefRece.Import(cym);
            
            dispRefrece();
        }


        private void dispRefrece()
        {
            dataGridViewRefrece.DataSource = RefRece.GetDispCount();
            dataGridViewRefrece.Columns[0].HeaderText = "メホール請求年月";
            dataGridViewRefrece.Columns[1].HeaderText = "行数";
            dataGridViewRefrece.Columns[0].Width = 150;
        }

     
    }
}
