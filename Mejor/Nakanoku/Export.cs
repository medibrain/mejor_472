﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.Nakanoku
{
    class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var wf = new WaitForm();

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "NO.,診療年,診療月,診療日数,被保険者番号,被保険者名,受療者名," +
                        "負傷名1,負傷名2,負傷名3,負傷名4,負傷名5,郵便番号,住所,施術所名,照会理由,整理番号,過誤,再審査,コメント";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    string no = string.Empty;
                    string refNum = string.Empty;
                    int count = 1;

                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        var rr = RefRece.Select(item.Numbering);

                        no = $"{item.ChargeMonth}_{count}-";
                        refNum = rr.Numbering;
                        no += refNum;

                        ss.Add(no);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName1));
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName2));
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName3));
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName4));
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName5));
                        ss.Add(item.HihoZip.PadLeft(7, '0').Insert(3, "-"));
                        ss.Add(item.HihoAdd);
                        ss.Add(item.ClinicName);
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");
                        ss.Add(refNum);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add(item.MemoInspect);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

    }
}
