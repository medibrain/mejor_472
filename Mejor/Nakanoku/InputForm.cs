﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Nakanoku

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;

        /// <summary>
        /// 国保データ柔整あはき両方
        /// </summary>
        //List<kokuhodata> lstKokuho = new List<kokuhodata>();
        List<RefRece> lstRefrece = new List<RefRece>();

        RefRece refrece = new RefRece();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount,verifyBoxCountedDays};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge,checkBoxVisit,checkBoxVisitKasan };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5 };

           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //提供データのリスト
                RefRece.getRefreceList(app.CYM);
                lstRefrece = RefRece.lstRefrece;

                setApp(app);
            }

            
            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);            
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {      
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelTotal.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelTotal.Visible = false;
                    pDoui.Visible = true;
                    break;
                    
                default:
                    panelTotal.Visible = true;
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        //private void InitGrid()
        //{


        //    dgv.Columns[nameof(refrece.Numbering)].Width = 50;
        //    dgv.Columns[nameof(refrece.ReceKey)].Width = 50;
        //    dgv.Columns[nameof(refrece.ChargeYM)].Width = 50;
        //    dgv.Columns[nameof(refrece.MediYM)].Width = 50;
        //    dgv.Columns[nameof(refrece.HihoNum)].Width = 50;
        //    dgv.Columns[nameof(refrece.Sex)].Width = 50;
        //    dgv.Columns[nameof(refrece.Birth)].Width = 50;
        //    dgv.Columns[nameof(refrece.StartDate)].Width = 50;
        //    dgv.Columns[nameof(refrece.FinishDate)].Width = 50;
        //    dgv.Columns[nameof(refrece.Days)].Width = 50;
        //    dgv.Columns[nameof(refrece.Total)].Width = 50;
        //    dgv.Columns[nameof(refrece.Partial)].Width = 50;
        //    dgv.Columns[nameof(refrece.Charge)].Width = 50;
        //    dgv.Columns[nameof(refrece.Ratio)].Width = 50;
        //    dgv.Columns[nameof(refrece.NewContType)].Width = 50;
        //    dgv.Columns[nameof(refrece.DrNum)].Width = 50;
        //    dgv.Columns[nameof(refrece.DrName)].Width = 50;

        //    dgv.Columns[nameof(refrece.Numbering)].HeaderText = "ナンバリング";
        //    dgv.Columns[nameof(refrece.ReceKey)].HeaderText = "レセプト全国共通キー";
        //    dgv.Columns[nameof(refrece.ChargeYM)].HeaderText = "請求年月";
        //    dgv.Columns[nameof(refrece.MediYM)].HeaderText = "診療年月(施術年月)";
        //    dgv.Columns[nameof(refrece.HihoNum)].HeaderText = "被保険者証番号";
        //    dgv.Columns[nameof(refrece.Sex)].HeaderText = "受療者性別";
        //    dgv.Columns[nameof(refrece.Birth)].HeaderText = "受療者生年月日";
        //    dgv.Columns[nameof(refrece.StartDate)].HeaderText = "開始日";
        //    dgv.Columns[nameof(refrece.FinishDate)].HeaderText = "終了日";
        //    dgv.Columns[nameof(refrece.Days)].HeaderText = "施術日数";
        //    dgv.Columns[nameof(refrece.Total)].HeaderText = "合計金額";
        //    dgv.Columns[nameof(refrece.Partial)].HeaderText = "患者一部負担金";
        //    dgv.Columns[nameof(refrece.Charge)].HeaderText = "請求金額";
        //    dgv.Columns[nameof(refrece.Ratio)].HeaderText = "給付割合";
        //    dgv.Columns[nameof(refrece.NewContType)].HeaderText = "新規/継続";
        //    dgv.Columns[nameof(refrece.DrNum)].HeaderText = "登録記号番号";
        //    dgv.Columns[nameof(refrece.DrName)].HeaderText = "施術師名";

        //    dgv.Columns[nameof(refrece.Numbering)].Visible = true;
        //    dgv.Columns[nameof(refrece.ReceKey)].Visible = true;
        //    dgv.Columns[nameof(refrece.ChargeYM)].Visible = true;
        //    dgv.Columns[nameof(refrece.MediYM)].Visible = true;
        //    dgv.Columns[nameof(refrece.HihoNum)].Visible = true;
        //    dgv.Columns[nameof(refrece.Sex)].Visible = true;
        //    dgv.Columns[nameof(refrece.Birth)].Visible = true;
        //    dgv.Columns[nameof(refrece.StartDate)].Visible = true;
        //    dgv.Columns[nameof(refrece.FinishDate)].Visible = true;
        //    dgv.Columns[nameof(refrece.Days)].Visible = true;
        //    dgv.Columns[nameof(refrece.Total)].Visible = true;
        //    dgv.Columns[nameof(refrece.Partial)].Visible = true;
        //    dgv.Columns[nameof(refrece.Charge)].Visible = true;
        //    dgv.Columns[nameof(refrece.Ratio)].Visible = true;
        //    dgv.Columns[nameof(refrece.NewContType)].Visible = true;
        //    dgv.Columns[nameof(refrece.DrNum)].Visible = true;
        //    dgv.Columns[nameof(refrece.DrName)].Visible = true;


        //}

        /// <summary>
        /// 提供データグリッド
        /// </summary>
        /// <param name="app"></param>
        //private void createGrid_refrece(App app = null)
        //{
        //    List<RefRece> lstimp = new List<RefRece>();

        //    string strhnum = verifyBoxHnum.Text.Trim();
        //    int intTotal = verifyBoxTotal.GetIntValue();
        //    int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

        //    foreach (RefRece item in lstRefrece)
        //    {
        //        //被保番半角、メホール請求年月、合計金額,診療年月で探す
        //        if (item.HihoNum == verifyBoxHnum.Text.Trim() &&
        //            item.ChargeYM == scan.CYM &&
        //            item.Total.ToString().Replace(",", "") == verifyBoxTotal.Text.ToString().Trim() &&
        //            item.MediYM==intymad
        //            )

        //        {
        //            lstimp.Add(item);
        //        }
        //    }

        //    dgv.DataSource = null;
        //    dgv.DataSource = lstimp;

        //    if (lstimp.Count == 0) return;

        //    InitGrid();

        //    //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
        //    if (lstRefrece.Count > 1) dgv.ClearSelection();


        //    if (app != null && app.RrID.ToString() != string.Empty)//&& app.ComNum != string.Empty)
        //    {
        //        foreach (DataGridViewRow r in dgv.Rows)
        //        {

        //            int intRRid = int.Parse(r.Cells[nameof(refrece.Numbering)].Value.ToString());

        //            //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
        //            string strcomnum = r.Cells[nameof(refrece.ReceKey)].Value.ToString();

        //            //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
        //            if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
        //            {

        //                if (r.Cells[nameof(refrece.Numbering)].Value.ToString() == app.RrID.ToString())
        //                {
        //                    r.Selected = true;
        //                }

        //                //提供データIDと違う場合の処理抜け
        //                else
        //                {
        //                    r.Selected = false;
        //                }
        //            }
        //            else
        //            {

        //                if (intTotal == int.Parse(verifyBoxTotal.Text.Trim()) && strhnum == verifyBoxHnum.Text.Trim())
        //                {
        //                    //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

        //                    if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
        //                    else r.Selected = true;

        //                }
        //                //提供データIDと違う場合の処理抜け                        
        //                else
        //                {
        //                    r.Selected = false;
        //                }
        //            }

        //        }
        //    }

        //    if (lstRefrece == null || lstRefrece.Count == 0)
        //    {
        //        labelMacthCheck.BackColor = Color.Pink;
        //        labelMacthCheck.Text = "マッチング無し";
        //        labelMacthCheck.Visible = true;
        //    }

            
        //    //マッチングOK条件に選択行が1行の場合も追加
        //    else if (lstRefrece.Count == 1 || dgv.SelectedRows.Count == 1)            
        //    {
        //        labelMacthCheck.BackColor = Color.Cyan;
        //        labelMacthCheck.Text = "マッチングOK";
        //        labelMacthCheck.Visible = true;
        //    }
        //    else
        //    {
        //        labelMacthCheck.BackColor = Color.Yellow;
        //        labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
        //        labelMacthCheck.Visible = true;
        //    }


        //}

        #endregion

           

        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            //関連レセ取得　emptytext3はApplicationのプロパティにないため、直接取得
            DB.Command cmd = DB.Main.CreateCmd($"select emptytext3 from application where aid={app.Aid}");

            //emptytext3の画像ファイル名から拡張子を抜いてrefrece.numberingと突合しrefrece取得
            refrece = RefRece.Select(cmd.TryExecuteScalar().ToString().Replace(".tif", ""));
            
            //コマンド解放しないとコマンドPoolがいっぱいになる            
            cmd.Dispose();


            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);



                    //負傷カウント                   
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);  

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //グリッド選択する                    
                    //createGrid_refrece(app);
                   
                    
                    break;
            }
        }


        #endregion

        #region 各種更新


        private bool CheckNumbering(int intNumbering)
        {
            if (intPrevNumbering == 0) return true;
            if (intPrevNumbering > intNumbering) return true;//戻ったときの対策
            if (Math.Abs(intNumbering - intPrevNumbering) == 1) return true;
            else return false;
            
        }

        /// <summary>
        /// 提供データをAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistRefRece(App app)
        {
            //申請書以外は登録しない
            if (app.MediYear < 0) return true;

            //対応データがない場合警告
            if (refrece == null) return false;
            try
            {
                app.TaggedDatas.GeneralString1 = refrece.Numbering;
                app.ComNum = refrece.ReceKey;
                app.HihoNum = refrece.HihoNum;
                app.Sex = refrece.Sex;
                app.CountedDays = refrece.Days;
                app.Total = refrece.Total;
                app.Partial = refrece.Partial;
                app.Charge = refrece.Charge;
                app.Ratio = refrece.Ratio;
                app.DrNum = refrece.DrNum.ToString();
                app.DrName = refrece.DrName.ToString();
                app.CYM = refrece.ChargeYM;
                app.Birthday = refrece.Birth;
                app.FushoStartDate1 = refrece.StartDate;
                app.FushoFinishDate1 = refrece.FinishDate;

                app.NewContType = refrece.NewContType.ToString() == "1" ? NEW_CONT.新規 : NEW_CONT.継続;

                app.TaggedDatas.GeneralString2 = refrece.atenaNum.Trim();//20210726134251 furukawa st 宛名番号追加
                                                                  

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }



        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                 

                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);
                    
                    //負傷数
                    int fushoCount = verifyBoxFushoCount.GetIntValue();
                    setStatus(verifyBoxFushoCount, fushoCount <0 || fushoCount>10);

  
                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月


                    app.AppType = scan.AppType;//申請書タイプ

                    //初検日1
                    app.FushoFirstDate1 = f1FirstDt;

                    //負傷数入力
                    app.TaggedDatas.count = fushoCount;

                    //refrece登録
                    if (!RegistRefRece(app))
                    {
                        if(MessageBox.Show("マッチングデータがありません。登録して宜しいですか？","",
                            MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.No)
                        return false;
                    }

               
                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;


                //20211011132317 furukawa st ////////////////////////
                //AUXにもApptype追加
                

                //  20210726155656 furukawa st ////////////////////////
                //  auxに提供データのナンバリングを追加                
                
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.TaggedDatas.GeneralString1)) return false;
                //  if (!Application_AUX.Update(app.Aid, tran, app.TaggedDatas.GeneralString1)) return false;
                //  20210726155656 furukawa ed ////////////////////////

                //20211011132317 furukawa ed ////////////////////////


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
         //   App app = (App)bsApp.Current;
          //  createGrid_kokuho(app);
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }


            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
               
        

        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            //往療加算は、有無がチェックされたときに有効とする　おかしな申請書が入力できなくなるのでやめとく
            //VerifyCheckBox c = (VerifyCheckBox)sender;
            //checkBoxVisitKasan.Enabled = c.Checked;
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
           // else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
