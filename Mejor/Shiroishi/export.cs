﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Shiroishi

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export
    {

        #region テーブル構造         
        public int f000AID { get;set; }=0;                                      //AID;
        public string f001No { get; set; } = string.Empty;                      //No.;
        public string f002hnum { get; set; } = string.Empty;                    //記号番号;
        public string f003pbirthday { get; set; } = string.Empty;               //生年月日;
        public string f004pgender { get; set; } = string.Empty;                 //性別;
        public string f005ym { get; set; } = string.Empty;                      //施術年月;
        public string f006CountedDays { get; set; } = string.Empty;             //診療日数;
        public string f007total { get; set; } = string.Empty;                   //合計金額;
        public string f008charge { get; set; } = string.Empty;                  //請求金額;
        public string f009firstdate1 { get; set; } = string.Empty;              //初検日;
        public string f010visit { get; set; } = string.Empty;                   //往料;
        public int f011fushoCount { get; set; } = 0;                            //負傷數;
        public string f012hihoname { get; set; } = string.Empty;                //被保険者氏名;
        public string f013pname { get; set; } = string.Empty;                   //受療者氏名;
        public string f014hihozip { get; set; } = string.Empty;                 //郵便番号（〒）;
        public string f015hihoaddress { get; set; } = string.Empty;             //住所;
        public string f016sname { get; set; } = string.Empty;                   //施術所名;
        public string f017biko { get; set; } = string.Empty;                    //備考;
        public int apptype { get; set; } = 0;                                   //申請書タイプ;
        public int cym { get; set; } = 0;                                       //メホール請求年月;
        public int ymAD { get; set; } = 0;                                      //施術年月西暦;
        public DateTime birthAD { get; set; } = DateTime.MinValue;              //生年月日西暦;
        public DateTime firstdateAD { get; set; } = DateTime.MinValue;          //初検年月日西暦;
        public string hihomark_narrow { get; set; } = string.Empty;             //被保険者証記号半角;
        public string hihonum_narrow { get; set; } = string.Empty;              //証番号半角;




        #endregion


        /// <summary>
        /// マルチTIFF候補リスト（tiffファイルのパスが入る）
        /// </summary>
        public static List<string> lstImageFilePath = new List<string>();



        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            
            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
               
                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable( cym, wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                //全出力データ取得
                List<export> lstExportJyusei = DB.Main.Select<export>($"cym={cym} and apptype={(int)APP_TYPE.柔整}").ToList();
                lstExportJyusei.Sort((x, y) => x.f000AID.CompareTo(y.f000AID));
                List<export> lstExportamma = DB.Main.Select<export>($"cym={cym} and apptype={(int)APP_TYPE.あんま}").ToList();
                lstExportJyusei.Sort((x, y) => x.f000AID.CompareTo(y.f000AID));
                List<export> lstExporthari = DB.Main.Select<export>($"cym={cym} and apptype={(int)APP_TYPE.鍼灸}").ToList();
                lstExportJyusei.Sort((x, y) => x.f000AID.CompareTo(y.f000AID));
                
                //ファイル出力
                if (!Export(lstExportJyusei,lstExportamma,lstExporthari, strNohinDir, wf)) return false;





                ////画像出力
                //if (!ExportImage(lstExport, strNohinDir, wf)) return false;

                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// {<param name="wf">waitform</param>
        /// <returns></returns>
        private static bool InsertExportTable(int cym ,WaitForm wf)
            //private static bool InsertExportTable(List<App> lstApp, int cym, WaitForm wf)
        {
            
            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();
                        
            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from export where cym='{cym}'", tran);
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            try
            {
                cmddel.TryExecuteNonQuery();
                int cnt =1;
                string tmpShisaym = string.Empty;//続紙用

                List<App> lstapp = App.GetApps(cym);
                lstapp.OrderBy(item => item.Aid);

                wf.SetMax(lstapp.Count);
                

                foreach (App a in lstapp)
                {

                    export exp = new export();                    

                    exp.f000AID = a.Aid;
                    exp.f001No = "0";//エクセル出力時に付与
                    exp.f002hnum = a.HihoNum;
                    exp.f003pbirthday = a.Birthday.ToString("yyyy/MM/dd");
                    exp.f004pgender = a.Sex == 1 ? "男" : "女";
                    exp.f005ym = a.YM.ToString();
                    exp.f006CountedDays = a.CountedDays.ToString();
                    exp.f007total = a.Total.ToString();
                    exp.f008charge = a.Charge.ToString();
                    exp.f009firstdate1 = a.FushoFirstDate1.ToString("yyyy/MM/dd");
                    exp.f010visit = a.Distance == 0 ? "無し" : "有り";
                    exp.f011fushoCount = a.TaggedDatas.count;
                    exp.f012hihoname = a.HihoName;
                    exp.f013pname = a.PersonName;
                    exp.f014hihozip = a.HihoZip;
                    exp.f015hihoaddress = a.HihoAdd;
                    exp.f016sname = a.ClinicName;
                    exp.f017biko = string.Empty;


                    exp.apptype = (int)a.AppType;
                    exp.cym = a.CYM;

                    exp.ymAD = a.YM;
                    exp.birthAD = a.Birthday;
                    exp.firstdateAD = a.FushoFirstDate1;
                    //exp.hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(, Microsoft.VisualBasic.VbStrConv.Narrow);
                    exp.hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(exp.f002hnum, Microsoft.VisualBasic.VbStrConv.Narrow);


                    if (!DB.Main.Insert<export>(exp, tran)) return false;


                    wf.LogPrint($"aid:{exp.f000AID}");
                    wf.InvokeValue++;

                    
                }
              

                tran.Commit();
                wf.LogPrint($"出力テーブル登録完了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
              
                cmddel.Dispose();
            }

               
        }


        /// <summary>
        /// 不要以外画像出力
        /// </summary>
        /// <param name="lstExport">出力するリスト</param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(List<export> lstExport , string strDir, WaitForm wf)
        {
            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ
       
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            wf.SetMax(lstExport.Count);

            try
            {
                //出力先パス
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";


                //0番目のファイルパス
             //   imageName = $"{strImageDir}\\{lstExport[0].imagefilename}";

                for (int r = 0; r < lstExport.Count(); r++)
                {
                    
                    //レセプト全国共通キーが20桁の場合、申請書と見なす

                    //if ((lstExport[r].comnum.Length==20)  && (lstImageFilePath.Count != 0))
                    //{
                        //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                        //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                        //その上で、この申請書をマルチTIFF候補リストに追加
                        wf.LogPrint($"申請書出力中:{imageName}");
                        if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                        {
                            System.Windows.Forms.MessageBox.Show(
                                System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                            return false;
                        }

                        lstImageFilePath.Clear();                        
                       // lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                   //     imageName = $"{strImageDir}\\{lstExport[r].imagefilename}";
                    //}                   
                    //else if ((lstExport[r].comnum.Length==20) && (lstImageFilePath.Count == 0))
                    //{
                    //    //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
                    //    lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                    //    wf.LogPrint($"申請書出力中:{imageName}");
                    //}

                    //else if (new string[] { "-1", "-3", "-5", "-6", "-9", "-11", "-13", "-14"}.Contains(lstExport[r].comnum))
                    //{
                    //    //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
                    //    lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                    //    wf.LogPrint($"申請書以外:{imageName}");
                    //}
               
                    
                    wf.InvokeValue++;

                }


                //最終画像出力
                //ループの最後の画像を出力
                if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            
        }


        /// <summary>
        /// DBからデータを取得してファイル出力まで
        /// <paramref name="lstExport"/>出力リスト</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        private static bool Export(List<export> lstExportJyu,List<export> lstExportamma,List<export> lstExporthari ,string strDir,WaitForm wf)            
        {

            //出力ファイル名
            string strFileName = $"{strDir}\\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_白井市国保.xlsx";

            #region 納品フォルダ作成
            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}";           //tifコピー先フォルダ
            
            string strImageDirJyu = $"{strImageDir}\\柔整";
            string strImageDiramma = $"{strImageDir}\\あんま";
            string strImageDirhari = $"{strImageDir}\\鍼灸";

            if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);                   //納品フォルダ
            if (!System.IO.Directory.Exists(strImageDirJyu)) System.IO.Directory.CreateDirectory(strImageDirJyu);   //納品フォルダ\柔整
            if (!System.IO.Directory.Exists(strImageDiramma)) System.IO.Directory.CreateDirectory(strImageDiramma); //納品フォルダ\あんま
            if (!System.IO.Directory.Exists(strImageDirhari)) System.IO.Directory.CreateDirectory(strImageDirhari); //納品フォルダ\鍼灸
            #endregion


            wf.LogPrint("ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lstExportJyu.Count<export>());

            #region NPOIコード
            System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.Create,System.IO.FileAccess.Write);           
            NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet("柔整");
            #endregion

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            #region 柔整ヘッダ
            string[] arrHeaderJyu = {
                "AID",
                "No",
                "記号番号",
                "生年月日",
                "性別",
                "施術年月",
                "診療日数",
                "合計金額",
                "初検日",
                "被保険者氏名",
                "受療者氏名",
                "郵便番号",
                "住所",
                "施術所名",
                "備考",
            };
            #endregion

            #region あんまヘッダ
            string[] arrHeaderAmma = {
                "AID",
                "No",
                "記号番号",
                "生年月日",
                "性別",
                "施術年月",
                "診療日数",
                "合計金額",
                "請求金額",
                "初検日",
                "往療",
                "被保険者氏名",
                "受療者氏名",
                "郵便番号",
                "住所",
                "施術所名",
                "備考",
            };
            #endregion

            #region はりヘッダ
            string[] arrHeaderHari = {
                "AID",
                "No",
                "記号番号",
                "生年月日",
                "性別",
                "施術年月",
                "診療日数",
                "合計金額",
                "請求金額",
                "初検日",
                "往療",
                "被保険者氏名",
                "受療者氏名",
                "郵便番号",
                "住所",
                "施術所名",
                "備考",
            };
            #endregion

            try
            {
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";

                #region 柔整

                #region ヘッダ行
                NPOI.SS.UserModel.IFont font = wb.CreateFont();
                font.FontName = "ＭＳ Pゴシック";
                font.FontHeightInPoints = 12;                
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                NPOI.SS.UserModel.ICellStyle csHeader = wb.CreateCellStyle();
                csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                csHeader.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                csHeader.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
                csHeader.SetFont(font);                

                NPOI.SS.UserModel.ICellStyle csRowstyle = wb.CreateCellStyle();
                csRowstyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Hair;
                csRowstyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Hair;
                csRowstyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
                csRowstyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Hair;                
                //csRowstyle.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                //csRowstyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                //csRowstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                //csRowstyle.SetFont(font);


                NPOI.SS.UserModel.IRow row = ws.CreateRow(0);
                row.Height = 400;
                
                //ヘッダ文字列配列で回す
                for (int arrNum = 0; arrNum <arrHeaderJyu.Length; arrNum++)
                {                                       
                    NPOI.SS.UserModel.ICell hcell = row.CreateCell(arrNum);//エクセルの列を作成
                    hcell.CellStyle = csHeader;
                    hcell.SetCellValue(arrHeaderJyu[arrNum]);//セルに入れるのは配列の文言                    
                }
                
                #endregion

                for (int r = 0; r < lstExportJyu.Count(); r++)
                {                    
                    row = ws.CreateRow(r+1);
                    
                    string strres = string.Empty;
                    imageName = $"{strImageDirJyu}\\{lstExportJyu[r].f000AID}.tif";


                    for (int c = 0; c <arrHeaderJyu.Length; c++)
                    {

                        NPOI.SS.UserModel.ICell cell = row.CreateCell(c);
                        cell.CellStyle = csRowstyle;
                                                

                        if (c == 0) cell.SetCellValue(lstExportJyu[r].f000AID);
                        if (c == 1) cell.SetCellValue((r + 1).ToString());
                        if (c == 2) cell.SetCellValue(lstExportJyu[r].hihonum_narrow);
                        if (c == 3) cell.SetCellValue(lstExportJyu[r].f003pbirthday);
                        if (c == 4) cell.SetCellValue(lstExportJyu[r].f004pgender);
                        if (c == 5) cell.SetCellValue(lstExportJyu[r].f005ym);
                        if (c == 6) cell.SetCellValue(lstExportJyu[r].f006CountedDays);
                        if (c == 7) cell.SetCellValue(lstExportJyu[r].f007total);

                        //if (colcnt == 8) cell.SetCellValue(lstExportJyu[r].f008charge);

                        if (c == 8) cell.SetCellValue(lstExportJyu[r].f009firstdate1);

                        //if (colcnt == 10) cell.SetCellValue(lstExportJyu[r].f010visit);
                        //if (colcnt == 11) cell.SetCellValue(lstExportJyu[r].f011fushoCount);

                        if (c == 9) cell.SetCellValue(lstExportJyu[r].f012hihoname);
                        if (c == 10) cell.SetCellValue(lstExportJyu[r].f013pname);
                        if (c == 11) cell.SetCellValue(lstExportJyu[r].f014hihozip);
                        if (c == 12) cell.SetCellValue(lstExportJyu[r].f015hihoaddress);

                        //if (colcnt == 16) cell.SetCellValue(lstExportJyu[r].cym);
                        //if (colcnt == 17) cell.SetCellValue(lstExportJyu[r].ymAD);
                        //if (colcnt == 18) cell.SetCellValue(lstExportJyu[r].birthAD);
                        //if (colcnt == 19) cell.SetCellValue(lstExportJyu[r].firstdateAD);
                        //if (colcnt == 20) cell.SetCellValue(lstExportJyu[r].hihomark_narrow);
                        //if (colcnt == 21) cell.SetCellValue(lstExportJyu[r].hihonum_narrow);

                        row.Cells.Add(cell);                        
                    }

                    App a = App.GetApp(lstExportJyu[r].f000AID);
                    string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());
                    
                    //被保番があるレコードのみ画像をコピー
                    if (lstExportJyu[r].f002hnum != string.Empty)
                    {
                        wf.LogPrint($"柔整画像ファイルコピー中:{strImageFileName}");
                        fc.FileCopy(strImageFileName, imageName);
                    }
                    
                    wf.InvokeValue++;
                }
                
                for (int c = 0; c < arrHeaderJyu.Length; c++) ws.AutoSizeColumn(c);                

                #endregion




                #region あんま
                wf.InvokeValue = 0;
                ws = wb.CreateSheet("あんま");

                #region ヘッダ行
           
                row = ws.CreateRow(0);
                row.Height = 400;

                for (int c = 0; c < arrHeaderAmma.Length; c++)
                {
                    NPOI.SS.UserModel.ICell hcell = row.CreateCell(c);
                    hcell.CellStyle = csHeader;

                    hcell.SetCellValue(arrHeaderAmma[c]);

                }

                //for (int r = 0; r <= 11; r++)
                //{
                //    NPOI.SS.UserModel.ICell hcell = row.CreateCell(r);
                //    hcell.CellStyle = csHeader;

                //    if (r == 0) hcell.SetCellValue("AID");
                //    if (r == 1) hcell.SetCellValue("No.");
                //    if (r == 2) hcell.SetCellValue("記号番号");
                //    if (r == 3) hcell.SetCellValue("生年月日");
                //    if (r == 4) hcell.SetCellValue("性別");
                //    if (r == 5) hcell.SetCellValue("施術年月");
                //    if (r == 6) hcell.SetCellValue("診療日数");
                //    if (r == 7) hcell.SetCellValue("合計金額");
                //    if (r == 8) hcell.SetCellValue("請求金額");
                //    if (r == 9) hcell.SetCellValue("初検日");
                //    if (r == 10) hcell.SetCellValue("往療");                    
                              
                //    if (r == 11) hcell.SetCellValue("被保険者名");
                //    if (r == 12) hcell.SetCellValue("受療者名");
                //    if (r == 13) hcell.SetCellValue("郵便番号");
                //    if (r == 14) hcell.SetCellValue("住所被保険者名");
                //    if (r == 15) hcell.SetCellValue("施術所名");
                //    if (r == 16) hcell.SetCellValue("備考");

                //    //if (r == 17) hcell.SetCellValue("申請書タイプ");
                //    //if (r == 18) hcell.SetCellValue("メホール請求年月");
                //    //if (r == 19) hcell.SetCellValue("施術年月西暦");
                //    //if (r == 20) hcell.SetCellValue("生年月日西暦");
                //    //if (r == 21) hcell.SetCellValue("初検年月日西暦");
                //    //if (r == 22) hcell.SetCellValue("被保険者証記号半角");
                //    //if (r == 23) hcell.SetCellValue("証番号半角");



                //}
                #endregion


                for (int r = 0; r < lstExportamma.Count(); r++)
                {
                    row = ws.CreateRow(r + 1);

                    string strres = string.Empty;
                    imageName = $"{strImageDiramma}\\{lstExportamma[r].f000AID}.tif";

                    for (int c = 0; c <arrHeaderAmma.Length; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(c);
                        cell.CellStyle = csRowstyle;

                        if (c == 0) cell.SetCellValue(lstExportamma[r].f000AID);
                        if (c == 1) cell.SetCellValue((r+1).ToString());
                        if (c == 2) cell.SetCellValue(lstExportamma[r].hihonum_narrow);
                        if (c == 3) cell.SetCellValue(lstExportamma[r].f003pbirthday);
                        if (c == 4) cell.SetCellValue(lstExportamma[r].f004pgender);
                        if (c == 5) cell.SetCellValue(lstExportamma[r].f005ym);
                        if (c == 6) cell.SetCellValue(lstExportamma[r].f006CountedDays);
                        if (c == 7) cell.SetCellValue(lstExportamma[r].f007total);
                        if (c == 8) cell.SetCellValue(lstExportamma[r].f008charge);
                        if (c == 9) cell.SetCellValue(lstExportamma[r].f009firstdate1);
                        if (c == 10) cell.SetCellValue(lstExportamma[r].f010visit);
                        
                        //if (c == 11) cell.SetCellValue(lstExportamma[r].f011fushoCount);

                        if (c == 11) cell.SetCellValue(lstExportamma[r].f012hihoname);
                        if (c == 12) cell.SetCellValue(lstExportamma[r].f013pname);
                        if (c == 13) cell.SetCellValue(lstExportamma[r].f014hihozip);
                        if (c == 14) cell.SetCellValue(lstExportamma[r].f015hihoaddress);
                        if (c == 15) cell.SetCellValue(lstExportamma[r].f016sname);
                        if (c == 16) cell.SetCellValue(lstExportamma[r].f017biko);

                        //if (colcnt == 18) cell.SetCellValue(lstExportamma[r].f013apptype);
                        //if (colcnt == 19) cell.SetCellValue(lstExportamma[r].cym);
                        //if (colcnt == 20) cell.SetCellValue(lstExportamma[r].ymAD);
                        //if (colcnt == 21) cell.SetCellValue(lstExportamma[r].birthAD);
                        //if (colcnt == 22) cell.SetCellValue(lstExportamma[r].firstdateAD);
                        //if (colcnt == 23) cell.SetCellValue(lstExportamma[r].hihomark_narrow);
                        //if (colcnt == 24) cell.SetCellValue(lstExportamma[r].hihonum_narrow);


                        row.Cells.Add(cell);
                    }



                    App a = App.GetApp(lstExportamma[r].f000AID);
                    string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());                    

                    //被保番があるレコードのみ画像をコピー
                    if (lstExportamma[r].f002hnum != string.Empty)
                    {
                        wf.LogPrint($"あんま画像ファイルコピー中:{strImageFileName}");
                        fc.FileCopy(strImageFileName, imageName);
                    }

                    wf.InvokeValue++;
                }

                for (int c = 0; c < arrHeaderAmma.Length; c++) ws.AutoSizeColumn(c);          
                #endregion


                #region 鍼灸
                wf.InvokeValue = 0;
                ws = wb.CreateSheet("鍼灸");

                #region ヘッダ行
             
                row = ws.CreateRow(0);
                row.Height = 400;

                for (int c = 0; c < arrHeaderHari.Length; c++)
                {
                    NPOI.SS.UserModel.ICell hcell = row.CreateCell(c);
                    hcell.CellStyle = csHeader;

                    hcell.SetCellValue(arrHeaderHari[c]);

                }

                //for (int r = 0; r <= 11; r++)
                //{
                //    NPOI.SS.UserModel.ICell hcell = row.CreateCell(r);
                //    hcell.CellStyle = csHeader;


                //    if (r == 0) hcell.SetCellValue("AID");
                //    if (r == 1) hcell.SetCellValue("No.");
                //    if (r == 2) hcell.SetCellValue("記号番号");
                //    if (r == 3) hcell.SetCellValue("生年月日");
                //    if (r == 4) hcell.SetCellValue("性別");
                //    if (r == 5) hcell.SetCellValue("施術年月");
                //    if (r == 6) hcell.SetCellValue("診療日数");
                //    if (r == 7) hcell.SetCellValue("合計金額");
                //    if (r == 8) hcell.SetCellValue("請求金額");
                //    if (r == 9) hcell.SetCellValue("初検日");
                //    if (r == 10) hcell.SetCellValue("往療");

                //    if (r == 11) hcell.SetCellValue("被保険者名");
                //    if (r == 12) hcell.SetCellValue("受療者名");
                //    if (r == 13) hcell.SetCellValue("郵便番号");
                //    if (r == 14) hcell.SetCellValue("住所被保険者名");
                //    if (r == 15) hcell.SetCellValue("施術所名");
                //    if (r == 16) hcell.SetCellValue("備考");

                //    //if (r == 17) hcell.SetCellValue("申請書タイプ");
                //    //if (r == 18) hcell.SetCellValue("メホール請求年月");
                //    //if (r == 19) hcell.SetCellValue("施術年月西暦");
                //    //if (r == 20) hcell.SetCellValue("生年月日西暦");
                //    //if (r == 21) hcell.SetCellValue("初検年月日西暦");
                //    //if (r == 22) hcell.SetCellValue("被保険者証記号半角");
                //    //if (r == 23) hcell.SetCellValue("証番号半角");

                //}
                #endregion


                for (int r = 0; r < lstExporthari.Count(); r++)
                {
                    row = ws.CreateRow(r + 1);

                    string strres = string.Empty;
                    imageName = $"{strImageDirhari}\\{lstExporthari[r].f000AID}.tif";

                    for (int c = 0; c < arrHeaderHari.Length; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(c);
                        cell.CellStyle = csRowstyle;
                        if (c == 0) cell.SetCellValue(lstExporthari[r].f000AID);
                        if (c == 1) cell.SetCellValue((r + 1).ToString());
                        if (c == 2) cell.SetCellValue(lstExporthari[r].hihonum_narrow);
                        if (c == 3) cell.SetCellValue(lstExporthari[r].f003pbirthday);
                        if (c == 4) cell.SetCellValue(lstExporthari[r].f004pgender);
                        if (c == 5) cell.SetCellValue(lstExporthari[r].f005ym);
                        if (c == 6) cell.SetCellValue(lstExporthari[r].f006CountedDays);
                        if (c == 7) cell.SetCellValue(lstExporthari[r].f007total);
                        if (c == 8) cell.SetCellValue(lstExporthari[r].f008charge);
                        if (c == 9) cell.SetCellValue(lstExporthari[r].f009firstdate1);
                        if (c == 10) cell.SetCellValue(lstExporthari[r].f010visit);

                        //if (c == 11) cell.SetCellValue(lstExporthari[r].f011fushoCount);

                        if (c == 11) cell.SetCellValue(lstExporthari[r].f012hihoname);
                        if (c == 12) cell.SetCellValue(lstExporthari[r].f013pname);
                        if (c == 13) cell.SetCellValue(lstExporthari[r].f014hihozip);
                        if (c == 14) cell.SetCellValue(lstExporthari[r].f015hihoaddress);
                        if (c == 15) cell.SetCellValue(lstExporthari[r].f016sname);
                        if (c == 16) cell.SetCellValue(lstExporthari[r].f017biko);


                        //if (colcnt == 18) cell.SetCellValue(lstExporthari[r].f013apptype);
                        //if (colcnt == 19) cell.SetCellValue(lstExporthari[r].cym);
                        //if (colcnt == 20) cell.SetCellValue(lstExporthari[r].ymAD);
                        //if (colcnt == 21) cell.SetCellValue(lstExporthari[r].birthAD);
                        //if (colcnt == 22) cell.SetCellValue(lstExporthari[r].firstdateAD);
                        //if (colcnt == 23) cell.SetCellValue(lstExporthari[r].hihomark_narrow);
                        //if (colcnt == 24) cell.SetCellValue(lstExporthari[r].hihonum_narrow);

                        row.Cells.Add(cell);
                    }

                    App a = App.GetApp(lstExporthari[r].f000AID);
                    string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());
                    
                    //被保番があるレコードのみ画像をコピー
                    if (lstExporthari[r].f002hnum != string.Empty)
                    {
                        wf.LogPrint($"鍼灸画像ファイルコピー中:{strImageFileName}");
                        fc.FileCopy(strImageFileName, imageName);
                    }

                    wf.InvokeValue++;
                }

                for (int c = 0; c < arrHeaderHari.Length; c++) ws.AutoSizeColumn(c);                

                wb.Write(fs);


                #endregion


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
                return false;
            }
            finally
            {
                #region npoiコード
                wb.Close();
                fs.Close();
                //cmd.Dispose();
                //sw.Close();            
                #endregion

            }

        }

    }
}
