﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;
using System.Linq;

namespace Mejor
{
    /// <summary>
    /// jyusei DBの保険者リスト。DBが変更されたらここも変更する必要あり
    /// </summary>
    public enum InsurerID
    {
        //20190128155748 furukawa 名古屋港湾健保追加
        //20190207091114 furukawa シャープ健保追加
        //20190406105506 furukawa 兵庫県後期広域追加
        //20190705110321 furukawa 島根広域追加　８と入れ替え
        //20190806145359 furukawa 豊橋市国保追加
        //20200219145632 furukawa 協会けんぽ広島支部追加
        //20200413151436 furukawa 外務省療養費追加
        //20200310172014 furukawa 宮城県柔整追加
        //20200511110511 furukawa 名古屋市国保追加
        //20200312181818 furukawa 荒川区柔整追加
        //20200403172039 furukawa 荒川区に変更
        //20200622173242 furukawa 静岡市あはき、千葉市国保追加
        //20200708105039 furukawa佐倉市柔整追加   
        //20200812111824 furukawa 協会けんぽ埼玉支部追加
        //20200928163245 furukawa 別府市追加
        //20201217174847 furukawa 越谷市柔整追加
        //20210115160200 sadohara 協会けんぽ追加
        //20210401173147 furukawa 品川区、茨木市、小平市、府中市、堺市、柏市、市原市追加
        //20210414091353 furukawa 千葉県松戸市
        //20210513143138 furukawa 中野区追加
        //20210520140019 furukawa 千葉県白井市
        //20210617101756 furukawa 函館市追加
        //20210713092642 furukawa 淡路市追加
        //20211116170542 furukawa 川越市追加
        //20220111163149 furukawa 学校共済テスト98、豊中市コロナワクチン接種追加
        //20220201130342 furukawa 豊中市コロナワクチン接種市外
        //20220311150338 furukawa 広島広域2022追加
        //20220322092434 furukawa 千葉広域あはき追加
        //20220325154316 furukawa 昭島市追加
        //20220326131418 furukawa 兵庫広域2022追加
        //20220414115049 furukawa 西宮市追加
        //20220420092754 furukawa 鹿児島広域追加
        //20220510140623 furukawa 茨城広域追加
        //20220609161446 furukawa 愛媛広域追加
        //20220908_1 ito 大分市国保追加

        AMAGASAKI_KOKUHO = 1,
        AICHI_TOSHI = 2,
        OSAKA_GAKKO = 3,
        OSAKA_KOIKI = 4,
        HIROSHIMA_KOIKI = 5,
        MUSASHINO_KOKUHO = 6,
        SAPPORO_KOKUHO = 7, 
        SHIMANE_KOIKI=8,//ZENKOKU_GAKKO = 8,
        GAKKO_08IBARAGI = 9, GAKKO_02AOMORI = 10, GAKKO_42NAGASAKI = 11,
        MIYAZAKI_KOIKI = 12,
        CHIKIIRYO = 13,
        NARA_KOIKI = 14,
        MITSUBISHI_SEISHI = 15,
        SHIZUOKA_KOIKI = 16,
        TOKUSHIMA_KOIKI = 17,
        KISHIWADA_KOKUHO = 18,
        OTARU_KOKUHO = 19,
        KYOTOFU_NOKYO = 20,
        GAKKO_01HOKKAIDO = 21, GAKKO_03IWATE = 22, GAKKO_04MIYAZAKI = 23, GAKKO_05AKITA = 24,
        GAKKO_06YAMAGATA = 25, GAKKO_07FUKUSHIMA = 26, GAKKO_09TOCHIGI = 27, GAKKO_10GUNMA = 28,
        GAKKO_11SAITAMA = 29, GAKKO_12CHIBA = 30, GAKKO_13TOKYO = 31, GAKKO_14KANAGAWA = 32,
        GAKKO_15NIGATA = 33, GAKKO_16TOYAMA = 34, GAKKO_17ISHIKAWA = 35, GAKKO_18FUKUI = 36,
        GAKKO_19YAMANASHI = 37, GAKKO_20NAGANO = 38, GAKKO_21GIFU = 39, GAKKO_22SHIZUOKA = 40,
        GAKKO_23AICHI = 41, GAKKO_24MIE = 42, GAKKO_25SHIGA = 43, GAKKO_26KYOTO = 44,
        GAKKO_28HYOGO = 45, GAKKO_29NARA = 46, GAKKO_30WAKAYAMA = 47, GAKKO_31TOTTORI = 48,
        GAKKO_32SHIMANE = 49, GAKKO_33OKAYAMA = 50, GAKKO_34HIROSHIMA = 51, GAKKO_35YAMAGUCHI = 52,
        GAKKO_36TOKUSHIMA = 53, GAKKO_37KAGAWA = 54, GAKKO_38EHIME = 55, GAKKO_39KOCHI = 56,
        GAKKO_40FUKUOKA = 57, GAKKO_41SAGA = 58, GAKKO_43KUMAMOTO = 59, GAKKO_44OITA = 60,
        GAKKO_45MIYAZAKI = 61, GAKKO_46KAGOSHIMA = 62, GAKKO_47OKINAWA = 63, GAKKO_99TEST = 64,
        KAGOME = 65, 
        CHUO_RADIO = 66,
        IRUMA_KOKUHO = 67, 
        HIRAKATA_KOKUHO = 68,
        NAGOYA_MOKUZAI = 69,
        YAMAZAKI_MAZAK = 70,
        FUTABA = 71,
        HIGASHIMURAYAMA_KOKUHO = 72,
        YACHIYO_KOKUHO = 73,
        KYOTO_KOIKI = 74,
        IBARAKI_SEIHO = 75,
        NARITA_KOKUHO = 76,
        ASAKURA_KOKUHO = 77,
        OTSU_KOKUHO = 78, 
        DENKI_GARASU = 79,
        NISSHIN_KOKUHO = 80, 
        HIGASHIOSAKA_KOKUHO = 81,
        MIYAGI_KOIKI =82,
        UJI_KOKUHO = 83,
        HANNAN_KOKUHO = 84,
        NAGOYA_KOUWAN = 85,
        SHARP_KENPO = 86,
        HYOGO_KOIKI = 87,
        TOYOHASHI_KOKUHO = 89,  
        KYOKAIKENPO_HIROSHIMA = 90,
        MIYAGI_KOKUHO = 91,
        ARAKAWAKU =92, //ARAKAWAKU_JYUSEI = 92, 
        GAIMUSHO_RYOUYOUHI = 93,
        SHIZUOKASHI_AHAKI =94,       
        CHIBASHI_KOKUHO =95,        
        SAKURASHI_KOKUHO=96,       
        NAGOYASHI_KOKUHO = 97,    
        KYOKAIKENPO_SAITAMA = 98,
        BEPPUSHI_KOKUHO=99,    
        KOSHIGAYASHI_KOKUHO=100,   
        KYOKAIKENPO = 101,     
        SHINAGAWAKU=102,      
        IBARAKISHI_KOKUHO=103,
        FUCHUSHI = 104,
        ICHIHARASHI=105,      
        KODAIRASHI =106,     
        SAKAISHI=107,      
        KASHIWASHI=108,    
        MATSUDOSHI=109,
        NAKANOKU=110,
        SHIROISHI=111,
        HAKODATESHI=112,
        AWAJISHI=113,
        KAWAGOESHI=114,
        GAKKO_98TEST=115,
        TOYONAKASHI_COVID19=116,
        TOYONAKASHI_COVID19_SHIGAI = 117,
        HIROSHIMA_KOIKI2022= 118,
        CHIBA_KOIKI_AHK=119,
        AKISHIMASHI=120,
        HYOGO_KOIKI2022=121,
        NISHINOMIYASHI = 122,
        KAGOSHIMA_KOIKI=123,
        IBARAKI_KOIKI=124,
        EHIME_KOIKI=125,
        OITASHI_KOKUHO = 126,
    }

    public enum INSURER_TYPE
    {
        指定なし = 0, 国民健康保険 = 1, 後期高齢 = 2, 健保組合 = 3,
        学校共済 = 4, その他 = 9
    }

    public class Insurer
    {
        public int InsurerID { set; get; }
        public string InsurerName { set; get; }
        public string dbName { set; get; }
        public string InsNumber { get; set; }
        public string FormalName { get; set; }
        public INSURER_TYPE InsurerType { get; set; }
        public int ViewIndex { get; set; }
        public string OutputPath { get; set; }
        public bool NeedVerify { get; set; }
        [DB.DbAttribute.Ignore]
        public InsurerID EnumInsID => (InsurerID) InsurerID;

        private static List<Insurer> dbList = new List<Insurer>();
        public static List<Insurer> dbListpub = new List<Insurer>();

        [DB.DbAttribute.Ignore]
        public static Insurer CurrrentInsurer { get; set; }
       
        public static string GetInsurerName(int insurerID)
        {
            var ins = dbList.Find(i => i.InsurerID == insurerID);
            return ins?.InsurerName ?? string.Empty;
        }
        public static Insurer GetInsurer(int insurerID)
        {
            return dbList.FirstOrDefault(i => i.InsurerID == insurerID);
        }

        public static INSURER_TYPE GetInsurerType(int insurerID)
        {
            var ins = dbList.Find(i => i.InsurerID == insurerID);
            return ins?.InsurerType ?? INSURER_TYPE.指定なし;
        }

        public static bool GetInsurers(bool enabledOnly)
        {
            try
            {
                //20221202 ito st プロジェクトKマスキング
                //var db = new DB("jyusei");
                //dbList = enabledOnly ?
                //    db.Select<Insurer>("enabled=true").ToList() :
                //    db.SelectAll<Insurer>().ToList();
                if (User.CurrentUser.limiteduser == true)
                {
                    var db = new DB("jyusei");
                    dbList = db.Select<Insurer>("dbname = 'hyogo_koiki2022' or dbname like 'gakko%'").ToList(); //20221206 ito
                    //dbList = db.Select<Insurer>("dbname = 'hyogo_koiki2022'").ToList();
                }
                else
                {
                    var db = new DB("jyusei");
                    dbList = enabledOnly ?
                        db.Select<Insurer>("enabled=true").ToList() :
                        db.SelectAll<Insurer>().ToList();
                }
                //20221202 ito end
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                throw ex;
            }
        }

        public static List<Insurer> GetInsurers()
        {
            dbListpub = dbList.ToList();
            return dbList.ToList();
        }
    }
}
