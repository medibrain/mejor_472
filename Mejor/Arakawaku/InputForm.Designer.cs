﻿namespace Mejor.Arakawaku
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.verifyBoxHY = new Mejor.VerifyBox();
            this.label21 = new System.Windows.Forms.Label();
            this.verifyBoxHM = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.verifyBoxRyouyouhiKubun = new Mejor.VerifyBox();
            this.verifyBoxIppanTaishoku = new Mejor.VerifyBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.verifyBoxClinicNum = new Mejor.VerifyBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.label41 = new System.Windows.Forms.Label();
            this.labelFamily = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxHnumN = new Mejor.VerifyBox();
            this.label36 = new System.Windows.Forms.Label();
            this.verifyBoxHnumS = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxBirthE = new Mejor.VerifyBox();
            this.label35 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.panelKohi = new System.Windows.Forms.Panel();
            this.verifyBoxKONum2 = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBoxKONum1 = new Mejor.VerifyBox();
            this.verifyBoxKOJUNum1 = new Mejor.VerifyBox();
            this.verifyBoxKOJUNum2 = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.label42 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.verifyBoxBui = new Mejor.VerifyBox();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.label23 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.panelKohi.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 753);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(139, 33);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(19, 13);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(40, 23);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(78, 23);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(55, 13);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保記番";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(6, 10);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(49, 39);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++\r\nヘッダ:**";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(65, 33);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(31, 13);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 782);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 753);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 753);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(26, 753);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 753);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 782);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelHeader);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 782);
            this.panelRight.TabIndex = 0;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(100, 22);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.label23);
            this.panelHeader.Controls.Add(this.label20);
            this.panelHeader.Controls.Add(this.verifyBoxHY);
            this.panelHeader.Controls.Add(this.label21);
            this.panelHeader.Controls.Add(this.verifyBoxHM);
            this.panelHeader.Controls.Add(this.label19);
            this.panelHeader.Controls.Add(this.verifyBoxRyouyouhiKubun);
            this.panelHeader.Controls.Add(this.verifyBoxIppanTaishoku);
            this.panelHeader.Controls.Add(this.label17);
            this.panelHeader.Controls.Add(this.label18);
            this.panelHeader.Controls.Add(this.verifyBoxClinicNum);
            this.panelHeader.Controls.Add(this.label16);
            this.panelHeader.Location = new System.Drawing.Point(59, 55);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(620, 65);
            this.panelHeader.TabIndex = 5;
            this.panelHeader.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 48;
            this.label20.Text = "和暦";
            this.label20.Visible = false;
            // 
            // verifyBoxHY
            // 
            this.verifyBoxHY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHY.Location = new System.Drawing.Point(41, 10);
            this.verifyBoxHY.MaxLength = 3;
            this.verifyBoxHY.Name = "verifyBoxHY";
            this.verifyBoxHY.NewLine = false;
            this.verifyBoxHY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxHY.TabIndex = 7;
            this.verifyBoxHY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHY.TextV = "";
            this.verifyBoxHY.Visible = false;
            this.verifyBoxHY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(75, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "年";
            this.label21.Visible = false;
            // 
            // verifyBoxHM
            // 
            this.verifyBoxHM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHM.Location = new System.Drawing.Point(99, 10);
            this.verifyBoxHM.MaxLength = 2;
            this.verifyBoxHM.Name = "verifyBoxHM";
            this.verifyBoxHM.NewLine = false;
            this.verifyBoxHM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxHM.TabIndex = 9;
            this.verifyBoxHM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHM.TextV = "";
            this.verifyBoxHM.Visible = false;
            this.verifyBoxHM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(134, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "月分";
            this.label19.Visible = false;
            // 
            // verifyBoxRyouyouhiKubun
            // 
            this.verifyBoxRyouyouhiKubun.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRyouyouhiKubun.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRyouyouhiKubun.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRyouyouhiKubun.Location = new System.Drawing.Point(379, 9);
            this.verifyBoxRyouyouhiKubun.MaxLength = 1;
            this.verifyBoxRyouyouhiKubun.Name = "verifyBoxRyouyouhiKubun";
            this.verifyBoxRyouyouhiKubun.NewLine = false;
            this.verifyBoxRyouyouhiKubun.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRyouyouhiKubun.TabIndex = 15;
            this.verifyBoxRyouyouhiKubun.TextV = "";
            this.verifyBoxRyouyouhiKubun.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxIppanTaishoku
            // 
            this.verifyBoxIppanTaishoku.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxIppanTaishoku.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxIppanTaishoku.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxIppanTaishoku.Location = new System.Drawing.Point(250, 10);
            this.verifyBoxIppanTaishoku.MaxLength = 1;
            this.verifyBoxIppanTaishoku.Name = "verifyBoxIppanTaishoku";
            this.verifyBoxIppanTaishoku.NewLine = false;
            this.verifyBoxIppanTaishoku.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxIppanTaishoku.TabIndex = 11;
            this.verifyBoxIppanTaishoku.TextV = "";
            this.verifyBoxIppanTaishoku.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(306, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "療養費区分\r\n";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(191, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 26);
            this.label18.TabIndex = 45;
            this.label18.Text = "一般退職\r\n区分";
            // 
            // verifyBoxClinicNum
            // 
            this.verifyBoxClinicNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxClinicNum.Location = new System.Drawing.Point(494, 10);
            this.verifyBoxClinicNum.MaxLength = 10;
            this.verifyBoxClinicNum.Name = "verifyBoxClinicNum";
            this.verifyBoxClinicNum.NewLine = false;
            this.verifyBoxClinicNum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxClinicNum.TabIndex = 20;
            this.verifyBoxClinicNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxClinicNum.TextV = "";
            this.verifyBoxClinicNum.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(411, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "医療機関番号";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.label10);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.label39);
            this.panelHnum.Controls.Add(this.verifyBoxRatio);
            this.panelHnum.Controls.Add(this.verifyBoxBirthD);
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxBirthM);
            this.panelHnum.Controls.Add(this.label41);
            this.panelHnum.Controls.Add(this.labelFamily);
            this.panelHnum.Controls.Add(this.label38);
            this.panelHnum.Controls.Add(this.verifyBoxBirthY);
            this.panelHnum.Controls.Add(this.label37);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.verifyBoxHnumN);
            this.panelHnum.Controls.Add(this.label36);
            this.panelHnum.Controls.Add(this.verifyBoxHnumS);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.verifyBoxBirthE);
            this.panelHnum.Controls.Add(this.label35);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Location = new System.Drawing.Point(158, 10);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(850, 108);
            this.panelHnum.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label10.Location = new System.Drawing.Point(387, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 36);
            this.label10.TabIndex = 44;
            this.label10.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(5, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(645, 63);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "日";
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(564, 11);
            this.verifyBoxRatio.MaxLength = 1;
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRatio.TabIndex = 18;
            this.verifyBoxRatio.TextV = "";
            this.verifyBoxRatio.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(610, 51);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 59;
            this.verifyBoxBirthD.TextV = "";
            this.verifyBoxBirthD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(355, 11);
            this.verifyBoxFamily.MaxLength = 1;
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 15;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(556, 51);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 55;
            this.verifyBoxBirthM.TextV = "";
            this.verifyBoxBirthM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(507, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 17;
            this.label41.Text = "給付割合";
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(293, 23);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(55, 13);
            this.labelFamily.TabIndex = 14;
            this.labelFamily.Text = "本家区分";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(588, 63);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 13);
            this.label38.TabIndex = 28;
            this.label38.Text = "月";
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(504, 51);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthY.TabIndex = 53;
            this.verifyBoxBirthY.TextV = "";
            this.verifyBoxBirthY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(536, 63);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 13);
            this.label37.TabIndex = 26;
            this.label37.Text = "年";
            // 
            // verifyBoxHnumN
            // 
            this.verifyBoxHnumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumN.Location = new System.Drawing.Point(191, 11);
            this.verifyBoxHnumN.Name = "verifyBoxHnumN";
            this.verifyBoxHnumN.NewLine = false;
            this.verifyBoxHnumN.Size = new System.Drawing.Size(77, 23);
            this.verifyBoxHnumN.TabIndex = 11;
            this.verifyBoxHnumN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumN.TextV = "";
            this.verifyBoxHnumN.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label36.Location = new System.Drawing.Point(356, 50);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 26);
            this.label36.TabIndex = 21;
            this.label36.Text = "男: 1\r\n女: 2";
            // 
            // verifyBoxHnumS
            // 
            this.verifyBoxHnumS.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumS.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumS.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumS.Location = new System.Drawing.Point(139, 11);
            this.verifyBoxHnumS.Name = "verifyBoxHnumS";
            this.verifyBoxHnumS.NewLine = false;
            this.verifyBoxHnumS.Size = new System.Drawing.Size(52, 23);
            this.verifyBoxHnumS.TabIndex = 10;
            this.verifyBoxHnumS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumS.TextV = "";
            this.verifyBoxHnumS.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(329, 51);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 45;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(296, 63);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 19;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxBirthE
            // 
            this.verifyBoxBirthE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthE.Location = new System.Drawing.Point(449, 51);
            this.verifyBoxBirthE.MaxLength = 1;
            this.verifyBoxBirthE.Name = "verifyBoxBirthE";
            this.verifyBoxBirthE.NewLine = false;
            this.verifyBoxBirthE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBirthE.TabIndex = 50;
            this.verifyBoxBirthE.TextV = "";
            this.verifyBoxBirthE.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label35.Location = new System.Drawing.Point(475, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(31, 39);
            this.label35.TabIndex = 24;
            this.label35.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(391, 63);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 22;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(3, 130);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 412);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.panelKohi);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.label2);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstE);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.verifyBoxDays);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstY);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstM);
            this.panelTotal.Controls.Add(this.label42);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstD);
            this.panelTotal.Controls.Add(this.verifyBoxBui);
            this.panelTotal.Controls.Add(this.verifyBoxF1Start);
            this.panelTotal.Controls.Add(this.label9);
            this.panelTotal.Controls.Add(this.verifyBoxF1Finish);
            this.panelTotal.Controls.Add(this.label22);
            this.panelTotal.Controls.Add(this.label7);
            this.panelTotal.Controls.Add(this.label15);
            this.panelTotal.Controls.Add(this.label5);
            this.panelTotal.Controls.Add(this.label14);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.label13);
            this.panelTotal.Controls.Add(this.label12);
            this.panelTotal.Location = new System.Drawing.Point(3, 551);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(800, 184);
            this.panelTotal.TabIndex = 70;
            // 
            // panelKohi
            // 
            this.panelKohi.Controls.Add(this.verifyBoxKONum2);
            this.panelKohi.Controls.Add(this.label3);
            this.panelKohi.Controls.Add(this.verifyBoxKONum1);
            this.panelKohi.Controls.Add(this.verifyBoxKOJUNum1);
            this.panelKohi.Controls.Add(this.verifyBoxKOJUNum2);
            this.panelKohi.Controls.Add(this.label6);
            this.panelKohi.Controls.Add(this.label8);
            this.panelKohi.Controls.Add(this.label4);
            this.panelKohi.Location = new System.Drawing.Point(21, 77);
            this.panelKohi.Name = "panelKohi";
            this.panelKohi.Size = new System.Drawing.Size(440, 90);
            this.panelKohi.TabIndex = 60;
            // 
            // verifyBoxKONum2
            // 
            this.verifyBoxKONum2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKONum2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKONum2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKONum2.Location = new System.Drawing.Point(111, 38);
            this.verifyBoxKONum2.MaxLength = 8;
            this.verifyBoxKONum2.Name = "verifyBoxKONum2";
            this.verifyBoxKONum2.NewLine = false;
            this.verifyBoxKONum2.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxKONum2.TabIndex = 46;
            this.verifyBoxKONum2.TextV = "";
            this.verifyBoxKONum2.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "公費負担者番号1";
            // 
            // verifyBoxKONum1
            // 
            this.verifyBoxKONum1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKONum1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKONum1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKONum1.Location = new System.Drawing.Point(111, 9);
            this.verifyBoxKONum1.MaxLength = 8;
            this.verifyBoxKONum1.Name = "verifyBoxKONum1";
            this.verifyBoxKONum1.NewLine = false;
            this.verifyBoxKONum1.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxKONum1.TabIndex = 44;
            this.verifyBoxKONum1.TextV = "";
            this.verifyBoxKONum1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxKOJUNum1
            // 
            this.verifyBoxKOJUNum1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKOJUNum1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKOJUNum1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKOJUNum1.Location = new System.Drawing.Point(325, 9);
            this.verifyBoxKOJUNum1.MaxLength = 8;
            this.verifyBoxKOJUNum1.Name = "verifyBoxKOJUNum1";
            this.verifyBoxKOJUNum1.NewLine = false;
            this.verifyBoxKOJUNum1.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxKOJUNum1.TabIndex = 45;
            this.verifyBoxKOJUNum1.TextV = "";
            this.verifyBoxKOJUNum1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxKOJUNum2
            // 
            this.verifyBoxKOJUNum2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKOJUNum2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKOJUNum2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKOJUNum2.Location = new System.Drawing.Point(325, 38);
            this.verifyBoxKOJUNum2.MaxLength = 8;
            this.verifyBoxKOJUNum2.Name = "verifyBoxKOJUNum2";
            this.verifyBoxKOJUNum2.NewLine = false;
            this.verifyBoxKOJUNum2.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxKOJUNum2.TabIndex = 47;
            this.verifyBoxKOJUNum2.TextV = "";
            this.verifyBoxKOJUNum2.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(222, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "公費受給者番号1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(222, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "公費受給者番号2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "公費負担者番号2";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(673, 28);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxCharge.TabIndex = 42;
            this.verifyBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(642, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "請求";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(149, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 39);
            this.label1.TabIndex = 40;
            this.label1.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(122, 26);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 3;
            this.verifyBoxF1FirstE.TextV = "";
            this.verifyBoxF1FirstE.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(550, 28);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 38;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(519, 38);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 37;
            this.labelTotal.Text = "合計";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(486, 28);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDays.TabIndex = 17;
            this.verifyBoxDays.TextV = "";
            this.verifyBoxDays.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(183, 26);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 4;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(228, 26);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 6;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(443, 38);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "実日数";
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(273, 26);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 8;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBui
            // 
            this.verifyBoxBui.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBui.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBui.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBui.Location = new System.Drawing.Point(56, 26);
            this.verifyBoxBui.MaxLength = 2;
            this.verifyBoxBui.Name = "verifyBoxBui";
            this.verifyBoxBui.NewLine = false;
            this.verifyBoxBui.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxBui.TabIndex = 2;
            this.verifyBoxBui.TextV = "";
            this.verifyBoxBui.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(334, 26);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 11;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(390, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "終了日";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(392, 26);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 14;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(55, 10);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "部位数";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(332, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "開始日";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(420, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "初検日";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(362, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(211, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "年";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(301, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(256, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "月";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 782);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label23.Location = new System.Drawing.Point(284, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(142, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "柔整：３　あんま：４　鍼灸：５";
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 782);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 820);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelKohi.ResumeLayout(false);
            this.panelKohi.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnumS;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnumN;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1Finish;
        private VerifyBox verifyBoxF1Start;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label35;
        private VerifyBox verifyBoxBirthE;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label labelFamily;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelHeader;
        private VerifyBox verifyBoxIppanTaishoku;
        private System.Windows.Forms.Label label18;
        private VerifyBox verifyBoxClinicNum;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxRyouyouhiKubun;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private VerifyBox verifyBoxHY;
        private System.Windows.Forms.Label label21;
        private VerifyBox verifyBoxHM;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panelKohi;
        private VerifyBox verifyBoxKONum2;
        private System.Windows.Forms.Label label3;
        private VerifyBox verifyBoxKONum1;
        private VerifyBox verifyBoxKOJUNum1;
        private VerifyBox verifyBoxKOJUNum2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private VerifyBox verifyBoxBui;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
    }
}