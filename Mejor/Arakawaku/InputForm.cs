﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Arakawaku
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800, 0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(950, 780);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);


        Control[] ymConts, hnumConts, personConts, totalConts, buiDateConts, kohiConts, headerConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号、本家区分、割合
            hnumConts = new Control[] { verifyBoxHnumS, verifyBoxHnumN, verifyBoxFamily, verifyBoxRatio };

            //被保険者性別、生年月日
            personConts = new Control[] { verifyBoxSex, verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD };

            //合計、請求
            totalConts = new Control[] { verifyBoxTotal, verifyBoxCharge };

            //初検日年号追加
            buiDateConts = new Control[] { verifyBoxDays, verifyBoxF1FirstE,
                verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD, verifyBoxF1Start, verifyBoxF1Finish,
                verifyBoxBui,};//20200518191706 furukawa 入力用部位数を初検日と同じグループに
                           

            //公費
            kohiConts = new Control[] { verifyBoxKONum1, verifyBoxKOJUNum1, verifyBoxKOJUNum2, verifyBoxKONum2, };

            //ヘッダ用パネル・コントロール
            headerConts = new Control[] { panelHeader, verifyBoxHY, verifyBoxHM, verifyBoxClinicNum, verifyBoxRyouyouhiKubun, verifyBoxIppanTaishoku, };

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            panelTotal.Visible = false;
            panelHnum.Visible = false;
            panelHeader.Visible = false;

                      


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);


          


        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            //Action<Control, bool> act = null;
            //act = new Action<Control, bool>((c, b) =>
            //{
            //    foreach (Control item in c.Controls) act(item, b);
            //    if ((c is IVerifiable == false) && (c is Label == false)) return;
            //    if (ignoreControls.Contains(c)) return;
            //    c.Visible = b;
            //    if (c is IVerifiable == false) return;
            //    c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            //});


            //続紙: --        不要: ++        ヘッダ:**
            //20200514123130 furukawa st ////////////////////////
            //続紙、不要とヘッダの表示項目変更
            
            if (verifyBoxY.Text == "**")

            //if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
                panelHeader.Visible = true;
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
                panelHeader.Visible = false;
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            //20200514123130 furukawa ed ////////////////////////


            else
            {
                //申請書の場合
                //act(panelRight, true);                
                panelHeader.Visible = false;
                panelHnum.Visible = true;
                panelTotal.Visible = true;

            }
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        #endregion

        
        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            panelHnum.Visible = false;
            panelTotal.Visible = false;
            panelHeader.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                //setVerify(app);
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //setInputedApp(app);
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }

            //20200520155122 furukawa st ////////////////////////
            //ヘッダのgeneralstring1を取得し、その申請書に紐付ける
            
            //ヘッダの情報を取得
            //このaid以下で、同じグループで、ayear=-7(バッチ、ヘッダの意味）
            string strsql = $"where aid <={app.Aid} and groupid = { app.GroupID } and ayear = -7";
            List<App> appheader = App.GetAppsWithWhere(strsql);
            if (appheader.Count == 1) strNumbering = appheader[0].TaggedDatas.GeneralString1;
            //20200520155122 furukawa ed ////////////////////////

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //20200514130158 furukawa st ////////////////////////
                //setvalueしないとステータスが更新されない
                
                //verifyBoxY.Text = "--";
                setValue(verifyBoxY, "--", firstTime, nv);
                //20200514130158 furukawa ed ////////////////////////
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //20200514130345 furukawa st ////////////////////////
                //setvalueしないとステータスが更新されない
                
                //verifyBoxY.Text = "++";
                setValue(verifyBoxY, "++", firstTime, nv);
                //20200514130345 furukawa ed ////////////////////////
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {

                setValue(verifyBoxY,"**", firstTime, nv);


                //20200418172131 furukawa st ////////////////////////
                //ヘッダの年月を登録しないので画面に反映しない
                
                //setValue(verifyBoxHM, app.MediMonth.ToString(), firstTime, nv);


                //strNumbering = $"{app.CYM}-{IppanTaishoku.ToString()}-{RyoyouhiKubun.ToString()}-{strClinicNum}";
                //テーブルに専用の列がないのでTaggedDatasから取得


                //string strMediYear = app.TaggedDatas.GeneralString1.Split('-')[0].ToString();
                //setValue(verifyBoxHY, DateTimeEx.GetJpYearFromYM(int.Parse(strMediYear)).ToString(), firstTime, nv);


                //20200418172131 furukawa ed ////////////////////////



                string strIppanTaishoku = app.TaggedDatas.GeneralString1.Split('-')[1].ToString();
                setValue(verifyBoxIppanTaishoku, strIppanTaishoku, firstTime, nv);

                string strRyouyouhiKubun= app.TaggedDatas.GeneralString1.Split('-')[2].ToString();
                setValue(verifyBoxRyouyouhiKubun, strRyouyouhiKubun, firstTime, nv);
                
                setValue(verifyBoxClinicNum, app.ClinicNum, firstTime, nv);

            }
            else
            {

                //申請書
                setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);

                
                //被保険者情報
                var sm = app.HihoNum.Split('-');
                if (sm.Length == 2)
                {
                    setValue(verifyBoxHnumS, sm[0], firstTime, nv);
                    setValue(verifyBoxHnumN, sm[1], firstTime, nv);
                }

                //本人家族
                setValue(verifyBoxFamily, app.Family.ToString(), firstTime, nv);
                //負担割合
                setValue(verifyBoxRatio, app.Ratio.ToString(), firstTime, nv);
                //被保険者性別
                setValue(verifyBoxSex, app.Sex.ToString(), firstTime, nv);


                //被保険者生年月日
                setValue(verifyBoxBirthE, DateTimeEx.GetEraNumber(app.Birthday).ToString(), firstTime, nv);
                setValue(verifyBoxBirthY, DateTimeEx.GetJpYear(app.Birthday).ToString(), firstTime, nv);
                setValue(verifyBoxBirthM, app.Birthday.Month.ToString(), firstTime, nv);
                setValue(verifyBoxBirthD, app.Birthday.Day.ToString(), firstTime, nv);
                
                //実日数 
                setValue(verifyBoxDays, app.CountedDays.ToString(), firstTime, nv);
                
                //合計金額
                setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                //請求金額
                setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                //20200518190617 furukawa st ////////////////////////
                //入力用部位数をTaggeddatas.countから取得
                
                setValue(verifyBoxBui, app.TaggedDatas.count.ToString(), firstTime, nv);
                //20200518190617 furukawa ed ////////////////////////

                //負傷                
                if (!app.FushoFirstDate1.IsNullDate())
                {

                    int wareki = 0;
                    wareki = DateTimeEx.GetEraNumberYear(app.FushoFirstDate1);
                    int we, wy;
                    we = int.Parse(wareki.ToString().Substring(0, 1));
                    wy = int.Parse(wareki.ToString().Substring(1, 2));

                    //初検日年号
                    setValue(verifyBoxF1FirstE, we.ToString(), firstTime, nv);
                    //初検日和暦年
                    setValue(verifyBoxF1FirstY, wy.ToString(), firstTime, nv);
                    //初検日月
                    setValue(verifyBoxF1FirstM, app.FushoFirstDate1.Month.ToString(), firstTime, nv);
                    //初検日日
                    setValue(verifyBoxF1FirstD, app.FushoFirstDate1.Day.ToString(), firstTime, nv);


                }
                //開始日
                if (!app.FushoStartDate1.IsNullDate())
                    setValue(verifyBoxF1Start, app.FushoStartDate1.Day.ToString(), firstTime, nv);
                                
                //終了日
                if (!app.FushoFinishDate1.IsNullDate())
                    setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day.ToString(), firstTime, nv);
                

                //公費
                string[] strKohi = app.PublcExpense.Split('|');
                int intKohiLength = strKohi.Length;
                if (intKohiLength > 0 && strKohi[0] != null) setValue(verifyBoxKONum1, strKohi[0].ToString(), firstTime, nv);
                if (intKohiLength > 1 && strKohi[1] != null) setValue(verifyBoxKOJUNum1, strKohi[1].ToString(), firstTime, nv);
                if (intKohiLength > 2 && strKohi[2] != null) setValue(verifyBoxKONum2, strKohi[2].ToString(), firstTime, nv);
                if (intKohiLength > 3 && strKohi[3] != null) setValue(verifyBoxKOJUNum2, strKohi[3].ToString(), firstTime, nv);


            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //ヘッダ
            if (verifyBoxY.Text == "**")
            {
                #region 入力チェック

                //20200418171808 furukawa st ////////////////////////
                //ヘッダの年月は不要。画像登録時の年月を全レコードに適用

                ////月
                //int month = verifyBoxHM.GetIntValue();
                //setStatus(verifyBoxHM, month < 1 || 12 < month);

                ////年
                //int year = verifyBoxHY.GetIntValue();
                //setStatus(verifyBoxHY, year < 1 || 31 < year);
                //int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                //20200418171808 furukawa ed ////////////////////////

                //一般退職区分
                int IppanTaishoku = verifyBoxIppanTaishoku.GetIntValue();
                setStatus(verifyBoxIppanTaishoku, IppanTaishoku < 1 || 9 < IppanTaishoku);

                //療養費区分
                int RyoyouhiKubun = verifyBoxRyouyouhiKubun.GetIntValue();
                setStatus(verifyBoxRyouyouhiKubun, RyoyouhiKubun < 1 || 9 < RyoyouhiKubun);

                //医療機関コード
                string strClinicNum = verifyBoxClinicNum.Text;
                setStatus(verifyBoxClinicNum, strClinicNum == string.Empty);

                //ここまでのチェックで必須エラーが検出されたらfalse
                if (hasError)
                {
                    showInputErrorMessage();
                    return false;
                }
                #endregion

                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;

                //20200418171907 furukawa st ////////////////////////
                //ヘッダの年月は不要。画像登録時の年月を全レコードに適用のため、登録しない

                //施術月
                //app.MediMonth = month;                             
                //app.YM = adYear * 100 + month;

                //20200418171907 furukawa ed ////////////////////////



                //20200615134656 furukawa st ////////////////////////
                //申請書タイプをバッチにする
                
                app.AppType = APP_TYPE.バッチ;
                //app.AppType = scan.AppType;
                //20200615134656 furukawa ed ////////////////////////



                app.ClinicNum = strClinicNum;//医療機関コード



                //どのヘッダに所属する申請書か分かるように、ヘッダのコードを持たせる


                //20200520134104 furukawa st ////////////////////////
                //ヘッダのtaggeddata.generalstring1に診療年月をもたせていたが、不要になったので削除したら、
                //バッチを表す－７となり、ロード時にハイフンでsplitしたら順序がおかしくなり、ロードされなくなった
                
                strNumbering = $"{app.CYM}-{IppanTaishoku.ToString()}-{RyoyouhiKubun.ToString()}-{strClinicNum}";
                //strNumbering = $"{app.YM}-{IppanTaishoku.ToString()}-{RyoyouhiKubun.ToString()}-{strClinicNum}";
                //20200520134104 furukawa ed ////////////////////////


                app.TaggedDatas.GeneralString1 = strNumbering;


                #endregion


            }
            //申請書
            else
            {
                #region 入力チェック

                //月
                int month = verifyBoxM.GetIntValue();
                setStatus(verifyBoxM, month < 1 || 12 < month);

                //年
                int year = verifyBoxY.GetIntValue();
                setStatus(verifyBoxY, year < 1 || 31 < year);
                int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                //公費番号
                //文字が入っている場合、公費負担者番号は8桁、公費受給者番号は7桁
                string kohinum1 = verifyBoxKONum1.Text;
                setStatus(verifyBoxKONum1, kohinum1 != string.Empty && kohinum1.Length != 8);
                string kohinum2 = verifyBoxKONum2.Text;
                setStatus(verifyBoxKONum2, kohinum2 != string.Empty && kohinum2.Length != 8);

                string kohiJunum1 = verifyBoxKOJUNum1.Text;
                setStatus(verifyBoxKOJUNum1, kohiJunum1 != string.Empty && kohiJunum1.Length != 7);
                string kohiJunum2 = verifyBoxKOJUNum2.Text;
                setStatus(verifyBoxKOJUNum2, kohiJunum2 != string.Empty && kohiJunum2.Length != 7);

                //被保険者番号
                int hnumS = verifyBoxHnumS.GetIntValue();
                setStatus(verifyBoxHnumS, hnumS < 0);

                int hnumN = verifyBoxHnumN.GetIntValue();
                setStatus(verifyBoxHnumN, hnumN < 0);

                string hnum = verifyBoxHnumS.Text.Trim() + "-" + verifyBoxHnumN.Text.Trim();


                //本家区分
                int family = verifyBoxFamily.GetIntValue();
                setStatus(verifyBoxFamily, !new[] { 2, 4, 6, 8, 0 }.Contains(family));

                //割合
                int ratio = verifyBoxRatio.GetIntValue();
                setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

                //性別
                int sex = verifyBoxSex.GetIntValue();
                setStatus(verifyBoxSex, sex < 1 || 2 < sex);

                //生年月日
                var birth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);


                //実日数
                int days = verifyBoxDays.GetIntValue();
                setStatus(verifyBoxDays, days < 1 || 31 < days);

                //合計金額
                int total = verifyBoxTotal.GetIntValue();
                setStatus(verifyBoxTotal, total < 100 || total > 200000);

                //請求金額
                int seikyu = verifyBoxCharge.GetIntValue();
                setStatus(verifyBoxCharge, seikyu < 100 || seikyu > 200000);


                //20200518190328 furukawa st ////////////////////////
                //入力用部位数追加                               
                int buicount = verifyBoxBui.GetIntValue();
                setStatus(verifyBoxBui, buicount < 1 || buicount > 5);
                //20200518190328 furukawa ed ////////////////////////


                

                //負傷数
                int fushoCount = 0;

                //負傷は１のみ
                DateTime f1FushoDt = DateTimeEx.DateTimeNull;
                DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                DateTime f1Start = DateTimeEx.DateTimeNull;
                DateTime f1Finish = DateTimeEx.DateTimeNull;


                //初検日は年号必須
                f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                //開始日
                f1Start = DateTimeEx.IsDate(adYear, month, verifyBoxF1Start.GetIntValue()) ?
                    new DateTime(adYear, month, verifyBoxF1Start.GetIntValue()) :
                    DateTimeEx.DateTimeNull;
                setStatus(verifyBoxF1Start, f1Start.IsNullDate());

                //終了日
                f1Finish = DateTimeEx.IsDate(adYear, month, verifyBoxF1Finish.GetIntValue()) ?
                    new DateTime(adYear, month, verifyBoxF1Finish.GetIntValue()) :
                    DateTimeEx.DateTimeNull;
                setStatus(verifyBoxF1Finish, f1Finish.IsNullDate());

                //ここまでのチェックで必須エラーが検出されたらfalse
                if (hasError)
                {
                    showInputErrorMessage();
                    return false;
                }

                
                //合計金額：請求金額：本家区分のトリプルチェック
                //金額でのエラーがあればいったん登録中断
                bool ratioError = (int)(total * ratio / 10) != seikyu;
                if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                if (ratioError)
                {
                    verifyBoxTotal.BackColor = Color.GreenYellow;
                    verifyBoxCharge.BackColor = Color.GreenYellow;
                    var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                        "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    if (res != System.Windows.Forms.DialogResult.OK) return false;
                }

                #endregion
                
                #region Appへの反映

                //ここから値の反映
                app.MediYear = year;                //施術年
                app.MediMonth = month;              //施術月
                app.HihoNum = hnum;                 //被保険者証記号番号
                app.Family = family;                //本家区分
                app.Ratio = ratio;                  //給付割合
                app.Sex = sex;                      //被保険者性別
                app.Birthday = birth;               //被保険者生年月日     
                app.CountedDays = days;             //実日数
                app.Total = total;                  //合計金額

                //請求金額
                app.Charge = seikyu;

                //申請書種別
                app.AppType = scan.AppType;

                //20200518190440 furukawa st ////////////////////////
                //入力用部位数をTaggeddatas.countに
                
                app.TaggedDatas.count = buicount;
                //20200518190440 furukawa ed ////////////////////////



                //負傷
                app.FushoDate1 = f1FushoDt;         //負傷年月日
                app.FushoFirstDate1 = f1FirstDt;    //初検日
                app.FushoStartDate1 = f1Start;      //開始日
                app.FushoFinishDate1 = f1Finish;    //終了日

                app.FushoName2 = fushoCount > 1 ? "Exist" : "";
                app.FushoName3 = fushoCount > 2 ? "Exist" : "";
                app.FushoName4 = fushoCount > 3 ? "Exist" : "";
                app.FushoName5 = fushoCount > 4 ? "Exist" : "";

                //公費
                app.PublcExpense = $"{kohinum1}|{kohiJunum1}|{kohinum2}|{kohiJunum2}";


                //公費１＝１，公費２＝２、公費１＆２＝１２
                if (kohinum1 != string.Empty && kohiJunum1 != string.Empty) app.HihoType = 1;
                if (kohinum2 != string.Empty && kohiJunum2 != string.Empty) app.HihoType = 2;

                if (kohinum1 != string.Empty && kohiJunum1 != string.Empty &&
                    kohinum2 != string.Empty && kohiJunum2 != string.Empty) app.HihoType = 12;

                //全部ない場合は０
                if (kohinum1 == string.Empty && kohiJunum1 == string.Empty &&
                    kohinum2 == string.Empty && kohiJunum2 == string.Empty) app.HihoType = 0;


                //どのヘッダに所属する申請書か分かるように、ヘッダのコードを持たせる
                app.TaggedDatas.GeneralString1 = strNumbering;

                #endregion

            }

            return true;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else if (verifyBoxY.Text == "**")
            {
                //ヘッダ（バッチ扱いとする）
                resetInputData(app);
              
                if (!checkApp(app))
                    //if (!checkAppHeader(app))
                    {
                    focusBack(true);
                    return false;
                }
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        
        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;
                     
            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;

            //20200409111407 furukawa st ////////////////////////
            //合計金額の位置が柔整あはきで異なるため分岐
            
            else if ((scan.AppType == APP_TYPE.柔整) &&  (totalConts.Contains(sender))) p = posTotal;
            else if((scan.AppType != APP_TYPE.柔整) && (totalConts.Contains(sender))) p = posTotalAHK;
            //else if (totalConts.Contains(sender)) p = posTotal; 
            //20200409111407 furukawa ed ////////////////////////


            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (kohiConts.Contains(sender)) p = posKohi;
            else if (headerConts.Contains(sender)) p = posHeader;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;

        }
        #endregion


    
       
    }      
}
