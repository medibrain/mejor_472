﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.Arakawaku
{
    public partial class pClsHeader
    {
        public string ym { get; set; } = string.Empty;
        public string IppanTaishoku { get; set; } = string.Empty;
        public string RyouyouhiKubun { get; set; } = string.Empty;
        public string ClinicNum { get; set; } = string.Empty;
    }

    class Export
    {
        #region メンバ
        public string aid { get; set; } = string.Empty;                   //プライマリキー
        public string cym { get; set; } = string.Empty;                   //請求年月  パンチ年月
        public string IppanTaishoku { get; set; } = string.Empty;         //一般退職区分  全桁スペース

        public string RyouyouhiKubun { get; set; } = string.Empty;           //療養費区分　全桁スペース

        public string ClinicNum { get; set; } = string.Empty;             //医療機関コード 数字10桁 全桁０
        public string MediYM { get; set; } = string.Empty;                //施術年月  全桁スペース
        public string hnum { get; set; } = string.Empty;                  //保険者証番号 記号番号 全桁０
        public string afamily { get; set; } = string.Empty;                //本扶区分 本家区分２、４、６ 全桁スペース
        public string aratio { get; set; } = string.Empty;                 //給付割合 １０、０９、０８、０７、００ 全桁０
        public string psex { get; set; } = string.Empty;                  //性別 １、２ 名前から判断
        public string pbirthday { get; set; } = string.Empty;             //生年月日 5010312 ４０１０１０１
        public string minstartdate { get; set; } = string.Empty;             //施術開始年月日 施術日の最小値　５０１０３１２ 全桁スペース
        public string maxfinishdate { get; set; } = string.Empty;            //施術終了年月日 施術日の最大値　５０１０３１２ 全桁スペース
        public string acounteddays { get; set; } = string.Empty;          //診療実日数 施術の合計日数 1
        public string atotal { get; set; } = string.Empty;                //総費用額合計 0123456 全桁スペース
        public string apartial { get; set; } = string.Empty;              //一部負担金 0123456 全桁スペース
        public string acharge { get; set; } = string.Empty;               //請求金額 0123456 全桁スペース

        public string ifirstdate1 { get; set; } = string.Empty;           //施術年月1 和暦年月　50203 全桁スペース
        public string ifee1 { get; set; } = string.Empty;                 //金額1 0123456 全桁スペース
        public string istartdate1 { get; set; } = string.Empty;           //施術開始年月日1 和暦年月日　5020312 全桁スペース
        public string ifinishdate1 { get; set; } = string.Empty;          //施術終了年月日1 和暦年月日　5020312 全桁スペース
        public string days1 { get; set; } = string.Empty;                 //診療実日数1  全桁スペース

        public string ifirstdate2 { get; set; } = string.Empty;           //施術年月2 和暦年月　50203 全桁スペース
        public string ifee2 { get; set; } = string.Empty;                 //金額2 0123456 全桁スペース
        public string istartdate2 { get; set; } = string.Empty;           //施術開始年月日2 和暦年月日　5020312 全桁スペース
        public string ifinishdate2 { get; set; } = string.Empty;          //施術終了年月日2 和暦年月日　5020312 全桁スペース
        public string days2 { get; set; } = string.Empty;                 //診療実日数2  全桁スペース

        public string ifirstdate3 { get; set; } = string.Empty;           //施術年月3 和暦年月　50203 全桁スペース
        public string ifee3 { get; set; } = string.Empty;                 //金額3 0123456 全桁スペース
        public string istartdate3 { get; set; } = string.Empty;           //施術開始年月日3 和暦年月日　5020312 全桁スペース
        public string ifinishdate3 { get; set; } = string.Empty;          //施術終了年月日3 和暦年月日　5020312 全桁スペース
        public string days3 { get; set; } = string.Empty;                 //診療実日数3  全桁スペース

        public string ifirstdate4 { get; set; } = string.Empty;           //施術年月4 和暦年月　50203 全桁スペース
        public string ifee4 { get; set; } = string.Empty;                 //金額4 0123456 全桁スペース
        public string istartdate4 { get; set; } = string.Empty;           //施術開始年月日4 和暦年月日　5020312 全桁スペース
        public string ifinishdate4 { get; set; } = string.Empty;          //施術終了年月日4 和暦年月日　5020312 全桁スペース
        public string days4 { get; set; } = string.Empty;                 //診療実日数4  全桁スペース

        public string ifirstdate5 { get; set; } = string.Empty;           //施術年月5 和暦年月　50203 全桁スペース
        public string ifee5 { get; set; } = string.Empty;                 //金額5 0123456 全桁スペース
        public string istartdate5 { get; set; } = string.Empty;           //施術開始年月日5 和暦年月日　5020312 全桁スペース
        public string ifinishdate5 { get; set; } = string.Empty;          //施術終了年月日5 和暦年月日　5020312 全桁スペース
        public string days5 { get; set; } = string.Empty;                 //診療実日数5  全桁スペース

        public string ifirstdate6 { get; set; } = string.Empty;           //施術年月6 和暦年月　50203 全桁スペース
        public string ifee6 { get; set; } = string.Empty;                 //金額6 0123456 全桁スペース
        public string istartdate6 { get; set; } = string.Empty;           //施術開始年月日6 和暦年月日　5020312 全桁スペース
        public string ifinishdate6 { get; set; } = string.Empty;          //施術終了年月日6 和暦年月日　5020312 全桁スペース
        public string days6 { get; set; } = string.Empty;                 //診療実日数6  全桁スペース
    
        public string kohif1 { get; set; } = string.Empty;                //第1公費負担者番号 前スペース埋め □1234567 全桁スペース
        public string kohij1 { get; set; } = string.Empty;                //第1公費受給者番号 前スペース埋め □1234567 全桁スペース
        public string kohif2 { get; set; } = string.Empty;                //第2公費負担者番号 前スペース埋め □1234567 全桁スペース
        public string kohij2 { get; set; } = string.Empty;                //第2公費受給者番号 前スペース埋め □1234567 全桁スペース
        #endregion

        

        /// <summary>
        /// 出力
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <returns></returns>
        public static bool DoExport(int cym)
        {
           
            //あはきの場合true
            bool flgAhk = false;

            frmExport frm = new frmExport();
            frm.ShowDialog();
            flgAhk = frm.flgAhk;
            

            bool flgNew = false;
            DialogResult res;
            res = MessageBox.Show("新仕様で出力しますか?",
                Application.ProductName,
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button3);
            switch (res)
            {
                case DialogResult.Cancel:
                    return false;
                case DialogResult.Yes:
                    flgNew = true;
                    break;
                case DialogResult.No:
                    flgNew = false;
                    break;
                default:
                    return false;
            }
            
            

            string dir;
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != DialogResult.OK) return false;
            
            dir = dlg.Name;//フォルダ名取得                        

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.LogPrint("対象の申請書を取得しています");                  
            var apps = App.GetApps(cym,InsurerID.ARAKAWAKU);


            //柔整・あはきでファイル名が違う
            string strFileName = string.Empty;
            //柔整あはきでファイル名接頭辞を変更
            if (!flgAhk)
            {
                strFileName = $"{dir}\\ry_{DateTime.Now.ToString("yyyyMMdd")}.txt";
                apps.RemoveAll(x => x.AppType==APP_TYPE.あんま || x.AppType == APP_TYPE.鍼灸);
            }
            else
            {
                strFileName = $"{dir}\\ahk_{DateTime.Now.ToString("yyyyMMdd")}.txt";
                apps.RemoveAll(x => x.AppType == APP_TYPE.柔整);
            }
            
            //ベリファイ済みでないのは出さない?曖昧なのでやめとく
            //apps.RemoveAll(x => !x.StatusFlagCheck(StatusFlag.ベリファイ済));

            //保留データを削除
            apps.RemoveAll(x => x.StatusFlagCheck(StatusFlag.支払保留));

            wf.SetMax(apps.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;
            wf.LogPrint("対象の申請書を出力データ形式に変換しています");

            //exportテーブル書き込み準備
            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Main.Excute("truncate table export", tran);


            //出力するための文字列リスト
            List<string> lstExport = new List<string>();

            try
            {
                pClsHeader clsHeader = new pClsHeader();

                #region 出力データ作成
                for (int i = 0; i < apps.Count; i++)
                {
                    var a = apps[i];

                    #region 保留データをskip
                    //保留データをskip
                    if (a.StatusFlagCheck(StatusFlag.支払保留))
                    {
                        //次の申請書までskip（別続紙添付回避のため）
                        while (i < apps.Count - 1 && apps[i + 1].YM < 0) i++;
                        continue;
                    }
                    #endregion

                    

                    if (a.MediYear == (int)APP_SPECIAL_CODE.バッチ)
                    {
                        if (a.TaggedDatas.GeneralString1.Contains('-') && 
                            a.TaggedDatas.GeneralString1.Split('-').Length == 4)
                        {
                            string[] tmp = a.TaggedDatas.GeneralString1.Split('-');

                            //20200418175002 furukawa st ////////////////////////
                            //診療年月は画像登録時の年月から取得し、入力画面から入力しないので、taggeddatasに入らずapplication.ymから取得する
                            
                            //clsHeader.ym = DateTimeEx.GetHsYearFromAd(int.Parse(tmp[0].Substring(0,4)),
                            //    int.Parse(tmp[0].Substring(4,2))).ToString();

                            clsHeader.ym = a.YM.ToString();
                            //20200418175002 furukawa ed ////////////////////////

                            clsHeader.IppanTaishoku = tmp[1];
                            clsHeader.RyouyouhiKubun = tmp[2];
                            clsHeader.ClinicNum = tmp[3];
                        }
                        continue;
                    }
                    //20200521120201 furukawa st ////////////////////////
                    //非申請書をExportしない

                    else if (a.MediYear != (int)APP_SPECIAL_CODE.バッチ && a.MediYear < 0)
                    {
                        
                        continue;
                    }
                    //20200521120201 furukawa ed ////////////////////////

                    //APPをexport型に変換

                    string strExport = string.Empty;
                    if (!CreateExportData(a, tran, clsHeader, out strExport))
                    {
                        MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                        tran.Rollback();
                        wf.Dispose();
                        return false;
                    }



                    //aidは納品物に入れない
                    strExport = strExport.Remove(0, strExport.Split(',')[0].Length + 1);

                    //療養費区分は新規仕様開始まで保留
                    if (!flgNew) strExport = strExport.Remove(8, 2);



                    lstExport.Add(strExport);
                }
                tran.Commit();

                
                #endregion


                #region テキスト出力



                wf.LogPrint("データを出力しています");
                using (var sw = new System.IO.StreamWriter(strFileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (string item_export in lstExport)
                    {
                        if (wf.Cancel)
                        {
                            MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                            return false;
                        }

                        
                        sw.WriteLine(item_export);

                        wf.InvokeValue++;
                        
                    }
                }


                #endregion

               
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                tran.Rollback();
                MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                return false;
            }
            finally
            {                
                wf.Dispose();
            }
            
            MessageBox.Show("出力が完了しました");
            return true;
        }


        /// <summary>
        /// appをexport型にして1行の文字列にする。テーブルにも登録
        /// </summary>
        /// <param name="app">appクラス</param>
        /// <param name="_tran">トランザクション</param>
        /// <param name="exdataRow">出力文字列</param>
        /// <returns>失敗=false</returns>
        public static bool CreateExportData(App app,DB.Transaction _tran,pClsHeader clsHeader,out string exdataRow)
        {
            Export exdata = new Export();
            exdataRow = string.Empty;
            try
            {
                exdata.aid = app.Aid.ToString();                                //プライマリキー

                int warekicym = DateTimeEx.GetGyymmFromAdYM(app.CYM);
                exdata.cym = warekicym.ToString();                              //請求年月 50108 パンチ年月

                //ヘッダからデータ引継
                exdata.IppanTaishoku = clsHeader.IppanTaishoku.PadLeft(1);         //一般退職区分  全桁スペース
                exdata.RyouyouhiKubun = clsHeader.RyouyouhiKubun.PadLeft(1);       //療養費区分　全桁スペース
                exdata.ClinicNum = clsHeader.ClinicNum.PadLeft(10, '0');           //医療機関コード 数字10桁 全桁０

                //施術年月  50108 全桁スペース
                exdata.MediYM = 
                    app.YM > 0 ? DateTimeEx.GetGyymmFromAdYM(app.YM).ToString().PadLeft(5) : string.Empty.PadLeft(5);


                //保険者証番号 記号番号 18-xx・xxxx のx部分のみ。どちらかが不備の場合　全桁０ 前ゼロ 全桁０
                string strhnum = app.HihoNum;
                if (strhnum.Split('-').Length != 2)
                {
                    exdata.hnum = string.Empty.PadLeft(6, '0');
                }
                else
                {
                    string[] tmphnum = strhnum.Split('-');
                    if (tmphnum[0].Length != 2) exdata.hnum = string.Empty.PadLeft(6, '0');
                    else if (tmphnum[1].Length != 4) exdata.hnum = string.Empty.PadLeft(6, '0');
                    else if (tmphnum[0].Length == 2 && tmphnum[1].Length == 4)
                        exdata.hnum = app.HihoNum.ToString().Replace("-", string.Empty).PadLeft(6, '0');//ハイフン抜いた6桁
                }

                //本扶区分 本家区分２、４、６ 全桁スペース
                exdata.afamily = new[] { "2", "4", "6" }.Contains(app.Family.ToString()) ? app.Family.ToString():string.Empty.PadLeft(1);

                exdata.aratio = app.Ratio.ToString().PadLeft(2,'0');    //給付割合 １０、０９、０８、０７、００ 全桁０
                exdata.psex = app.Sex.ToString();                       //性別 １、２ 名前から判断

                exdata.pbirthday = app.Birthday.ToString()==DateTime.MinValue.ToString() ?
                    "4010101" : DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday).ToString();  //生年月日 5010312 ４０１０１０１


                exdata.minstartdate = 
                    app.FushoStartDate1==DateTime.MinValue ? 
                    string.Empty.PadLeft(7) : DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1).ToString().PadLeft(7);                        //施術開始年月日 施術日の最小値　５０１０３１２ 全桁スペース


                exdata.maxfinishdate = 
                    app.FushoFinishDate1 == DateTime.MinValue ?
                    string.Empty.PadLeft(7) : DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFinishDate1).ToString().PadLeft(7);                      //施術終了年月日 施術日の最大値　５０１０３１２ 全桁スペース

                exdata.acounteddays = app.CountedDays.ToString()=="0" ? "001": app.CountedDays.ToString().PadLeft(3,'0');   //診療実日数 施術の合計日数 1

                exdata.atotal = app.Total.ToString().PadLeft(7,'0');                                     //総費用額合計 0123456 全桁スペース 数字の場合は前ゼロにしとく
                exdata.apartial = app.Partial.ToString().PadLeft(7, '0');                                //一部負担金 0123456 全桁スペース数字の場合は前ゼロにしとく
                exdata.acharge = app.Charge.ToString().PadLeft(7, '0');                                  //請求金額 0123456 全桁スペース数字の場合は前ゼロにしとく


                exdata.ifirstdate1 = string.Empty.PadLeft(5);                               //施術年月1 和暦年月　50203 全桁スペース
                exdata.ifee1 = string.Empty.PadLeft(7);                                     //金額1 0123456 全桁スペース
                exdata.istartdate1 = string.Empty.PadLeft(7);                               //施術開始年月日1 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate1 = string.Empty.PadLeft(7);                              //施術終了年月日1 和暦年月日　5020312 全桁スペース
                exdata.days1 = string.Empty.PadLeft(3);                                     //診療実日数1 全桁スペース

                exdata.ifirstdate2 = string.Empty.PadLeft(5);                               //施術年月2 和暦年月　50203 全桁スペース
                exdata.ifee2 = string.Empty.PadLeft(7);                                     //金額2 0123456 全桁スペース
                exdata.istartdate2 = string.Empty.PadLeft(7);                               //施術開始年月日2 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate2 = string.Empty.PadLeft(7);                              //施術終了年月日2 和暦年月日　5020312 全桁スペース
                exdata.days2 = string.Empty.PadLeft(3);                                     //診療実日数2 全桁スペース

                exdata.ifirstdate3 = string.Empty.PadLeft(5);                               //施術年月3 和暦年月　50203 全桁スペース
                exdata.ifee3 = string.Empty.PadLeft(7);                                     //金額3 0123456 全桁スペース
                exdata.istartdate3 = string.Empty.PadLeft(7);                               //施術開始年月日3 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate3 = string.Empty.PadLeft(7);                              //施術終了年月日3 和暦年月日　5020312 全桁スペース
                exdata.days3 = string.Empty.PadLeft(3);                                     //診療実日数3  全桁スペース

                exdata.ifirstdate4 = string.Empty.PadLeft(5);                               //施術年月4 和暦年月　50203 全桁スペース
                exdata.ifee4 = string.Empty.PadLeft(7);                                     //金額4 0123456 全桁スペース
                exdata.istartdate4 = string.Empty.PadLeft(7);                               //施術開始年月日4 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate4 = string.Empty.PadLeft(7);                              //施術終了年月日4 和暦年月日　5020312 全桁スペース
                exdata.days4 = string.Empty.PadLeft(3);                                     //診療実日数4  全桁スペース

                exdata.ifirstdate5 = string.Empty.PadLeft(5);                               //施術年月5 和暦年月　50203 全桁スペース
                exdata.ifee5 = string.Empty.PadLeft(7);                                     //金額5 0123456 全桁スペース
                exdata.istartdate5 = string.Empty.PadLeft(7);                               //施術開始年月日5 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate5 = string.Empty.PadLeft(7);                              //施術終了年月日5 和暦年月日　5020312 全桁スペース
                exdata.days5 = string.Empty.PadLeft(3);                                     //診療実日数5  全桁スペース

                exdata.ifirstdate6 = string.Empty.PadLeft(5);                               //施術年月6 和暦年月　50203 全桁スペース
                exdata.ifee6 = string.Empty.PadLeft(7);                                     //金額6 0123456 全桁スペース
                exdata.istartdate6 = string.Empty.PadLeft(7);                               //施術開始年月日6 和暦年月日　5020312 全桁スペース
                exdata.ifinishdate6 = string.Empty.PadLeft(7);                              //施術終了年月日6 和暦年月日　5020312 全桁スペース
                exdata.days6 = string.Empty.PadLeft(3);                                     //診療実日数6  全桁スペース




                string[] strkohi = app.PublcExpense.Split('|');

                //第1公費負担者番号 前スペース埋め □1234567 全桁スペース
                exdata.kohif1 = string.Empty.PadLeft(8);
                if (strkohi.Length > 0 && strkohi[0] != string.Empty) exdata.kohif1 = strkohi[0].ToString().PadLeft(8);

                //第1公費受給者番号 前スペース埋め □1234567 全桁スペース
                exdata.kohij1 = string.Empty.PadLeft(8);
                if (strkohi.Length > 1 && strkohi[1] != string.Empty) exdata.kohij1 = strkohi[1].ToString().PadLeft(8);

                //第2公費負担者番号 前スペース埋め □1234567 全桁スペース
                exdata.kohif2 = string.Empty.PadLeft(8);
                if (strkohi.Length > 2 && strkohi[2] != string.Empty) exdata.kohif2 = strkohi[2].ToString().PadLeft(8);

                //第2公費受給者番号 前スペース埋め □1234567 全桁スペース
                exdata.kohij2 = string.Empty.PadLeft(8);
                if (strkohi.Length > 3 && strkohi[3] != string.Empty) exdata.kohij2 = strkohi[3].ToString().PadLeft(8);

                
                if (!DB.Main.Insert(exdata, _tran)) return false;//テーブル書き込み

                //①ファイル形式・・・・・固定長テキスト※力ンマ区切りとしてますが、固定長でお願いします。=基本的に固定長で,フィールドの間にカンマの文字を挿入									
                //③文字コード・・・・ .SJIS
                //④区切り文字・・・・・カンマ
                //⑤囲み文字・・・・・なし
                //⑥改行コード・・・・・ CRLF

                //行作成
                //string exdataRow =
                 exdataRow = 
                    $"{exdata.aid},{exdata.cym},{exdata.IppanTaishoku},{exdata.RyouyouhiKubun},{exdata.ClinicNum}," +
                    $"{exdata.MediYM},{exdata.hnum},{exdata.afamily},{exdata.aratio},{exdata.psex},{exdata.pbirthday}," +
                    $"{exdata.minstartdate},{exdata.maxfinishdate},{exdata.acounteddays},{exdata.atotal},{exdata.apartial},{exdata.acharge}," +
                    $"{exdata.ifirstdate1},{exdata.ifee1},{exdata.istartdate1},{exdata.ifinishdate1},{exdata.days1}," +
                    $"{exdata.ifirstdate2},{exdata.ifee2},{exdata.istartdate2},{exdata.ifinishdate2},{exdata.days2}," +
                    $"{exdata.ifirstdate3},{exdata.ifee3},{exdata.istartdate3},{exdata.ifinishdate3},{exdata.days3}," +
                    $"{exdata.ifirstdate4},{exdata.ifee4},{exdata.istartdate4},{exdata.ifinishdate4},{exdata.days4}," +
                    $"{exdata.ifirstdate5},{exdata.ifee5},{exdata.istartdate5},{exdata.ifinishdate5},{exdata.days5}," +
                    $"{exdata.ifirstdate6},{exdata.ifee6},{exdata.istartdate6},{exdata.ifinishdate6},{exdata.days6}," +
                    $"{exdata.kohif1},{exdata.kohij1},{exdata.kohif2},{exdata.kohij2}";

                
                return true;
            }
            catch(Npgsql.NpgsqlException npgex)
            {
                MessageBox.Show(npgex.Message + $"\r\n aid={ app.Aid.ToString()}");
                return false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + $"\r\n aid={ app.Aid.ToString()}");                
                return false;
            }
          
            
        }


        //未使用
        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("shift-jis")))
                {
                    var ss = new List<string>();


                    //var notifyNumber;// = $"{jcy}{jcm}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<App, string> getFushoCount = app =>
                    {
                        var c = 0;
                        if (app.FushoName1 != "") c++;
                        if (app.FushoName2 != "") c++;
                        if (app.FushoName3 != "") c++;
                        if (app.FushoName4 != "") c++;
                        if (app.FushoName5 != "") c++;
                        return c.ToString();
                    };
                    foreach (var item in list)
                    {
                        



                        ss.Add(item.Aid.ToString());
                        ss.Add(count.ToString());
                      //  ss.Add(notifyNumber + count.ToString("000"));
                       // ss.Add(jcy);
                       // ss.Add(jcm);
                        var num = item.HihoNum.Split('-');
                        ss.Add(num.Length == 2 ? num[0] : string.Empty);
                        ss.Add(num.Length == 2 ? num[1] : string.Empty);
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(DateTimeEx.GetAge(item.Birthday, DateTime.Today).ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.PayCode);
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.Numbering.ToString());
                        ss.Add(item.InspectInfo.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");//区切り文字が「, (半角カンマ)(半角スペース)」なのでエスケープ処理
                        ss.Add("\"" + item.KagoReasonStr + "\"");
                        ss.Add("\"" + item.SaishinsaReasonStr + "\"");
                        ss.Add(getFushoCount(item));

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
