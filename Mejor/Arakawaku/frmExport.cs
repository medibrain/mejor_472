﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Arakawaku
{
    public partial class frmExport : Form
    {
        public bool flgAhk { get; set; }

        public frmExport()
        {
            InitializeComponent();
        }

        private void btnJyu_Click(object sender, EventArgs e)
        {
            flgAhk = false;
            this.Close();
        }

        private void btnAhk_Click(object sender, EventArgs e)
        {
            flgAhk = true;
            this.Close();
        }
    }
}
