﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Medivery
{
   
    public partial class MediveryRevForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();

        private PostalCodeForm pcForm;
        private List<PostalCode> selectedHistory = new List<PostalCode>();
        private int cym;
        private Medivery medivery;
        private bool clinicStorage;
        private Clinic clinic;
        private string prevTodofuken;
        private string prevSikutyoson;
        float baseRatio = 0.4f;

        protected override Control inputPanel => panelRight;


        //20190606175105 furukawa st ////////////////////////
        //過去に入力したapp
        
        List<App> lstAppForInputted;
        //20190606175105 furukawa ed ////////////////////////


        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posMediYM, posHnum, posHname, posPname, posBirthday, posHzip,
            posHaddress, posCountedDays, posDates, posTotal, posPartial, 
            posCharge, posSname, posFvisittimes, posPaymentcode, posDrName;

        Control[] mediymConts, hnumConts, hnameConts, pnameConts, birthdayConts,
            hzipConts, haddressConts, counteddaysConts, datesConts, totalConts, 
            partialConts, chargeConts, snameConts, fvisittimesConts, paymentcodeConts, 
            drnameConts;

        public MediveryRevForm(int cym, List<App> list, bool clinicStorage = false)
        {
            InitializeComponent();

            //設定テーブルロード
            medivery = Medivery.GetMediVery(Insurer.CurrrentInsurer.InsurerID);
            if (medivery == null)
            {
                this.Close();
                return;
            }
            this.cym = cym;
            this.clinicStorage = clinicStorage;

            mediymConts = new Control[] { verifyBoxMediY, verifyBoxMediM };
            hnumConts = new Control[] { verifyBoxHnum };
            hnameConts = new Control[] { verifyBoxHname };
            pnameConts = new Control[] { verifyBoxPname };
            birthdayConts = new Control[] { verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD };
            hzipConts = new Control[] { verifyBoxHzip };
            haddressConts = new Control[] { verifyBoxHaddress };
            counteddaysConts = new Control[] { verifyBoxCountedDays };
            datesConts = new Control[] { verifyBoxDates };
            totalConts = new Control[] { verifyBoxTotal };
            partialConts = new Control[] { verifyBoxPartial };
            chargeConts = new Control[] { verifyBoxCharge };
            snameConts = new Control[] { verifyBoxSname };
            fvisittimesConts = new Control[] { verifyBoxFvisittimes };
            paymentcodeConts = new Control[] { verifyBoxPaymentcode };
            drnameConts = new Control[] { verifyBoxDrName };


            //enterイベント動的割り当て
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            bsApp.DataSource = list;

            //診療日の成型イベント
            verifyBoxDates.Leave += VerifyBoxDates_Leave;
            verifyBoxDates.TextBoxV_Leave += VerifyBoxCountedDays_TextBoxV_Leave;

            //20190606175239 furukawa st ////////////////////////
            //過去に入力したappを取得            
          //  lstAppForInputted = App.GetAppsWithWhere("where trim(a.hname) <>'' ");

            //20190606175239 furukawa ed ////////////////////////

            
        }



        private bool datesStringAdjust(ref string s)
        {
            var ds = s.Split(new[] { ' ', ',', '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (ds.Length < 1) return false;

            var l = new List<int>();
            int last = 0;
            
            foreach (var item in ds)
            {  
                int.TryParse(item, out int i);
                if (i <= last || 31 < i) return false;
                l.Add(i);
                last = i;
            }

            s = string.Join(" ", l);
            return true;
        }

        private void VerifyBoxDates_Leave(object sender, EventArgs e)
        {
            var s = verifyBoxDates.Text;
            datesStringAdjust(ref s);
            verifyBoxDates.Text = s;
        }

        private void VerifyBoxCountedDays_TextBoxV_Leave(object sender, EventArgs e)
        {
            var s = verifyBoxDates.TextV;
            datesStringAdjust(ref s);
            verifyBoxDates.TextV = s;
        }

        private void initControl()
        {
            if (medivery.MediYM)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_MEDIYM);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelMediYM.Visible = true;
                    verifyBoxMediY.Visible = true;
                    verifyBoxMediM.Visible = true;
                    labelMediY.Visible = true;
                    labelMediM.Visible = true;
                    verifyBoxMediY.Tag = desc;
                    verifyBoxMediM.Tag = desc;
                    labelMediYM.Text = desc.Label;
                    posMediYM = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Hnum)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HNUM);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHnum.Visible = true;
                    verifyBoxHnum.Visible = true;
                    verifyBoxHnum.Tag = desc;
                    labelHnum.Text = desc.Label;
                    posHnum = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Hname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHname.Visible = true;
                    verifyBoxHname.Visible = true;
                    verifyBoxHname.Tag = desc;
                    labelHname.Text = desc.Label;
                    posHname = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Pname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPname.Visible = true;
                    verifyBoxPname.Visible = true;
                    verifyBoxPname.Tag = desc;
                    labelPname.Text = desc.Label;
                    posPname = new Point(desc.LocationX, desc.LocationY);
                    if (medivery.Hname) labelHnameCopy.Visible = true;
                }
            }
            if (medivery.Birthday)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_BIRTHDAY);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelBirthday.Visible = true;
                    verifyBoxBE.Visible = true;
                    verifyBoxBY.Visible = true;
                    verifyBoxBM.Visible = true;
                    verifyBoxBD.Visible = true;
                    labelBirthdayGengo.Visible = true;
                    labelBirthdayYear.Visible = true;
                    labelBirthdayMonth.Visible = true;
                    labelBirthdayDate.Visible = true;
                    verifyBoxBE.Tag = desc;
                    verifyBoxBY.Tag = desc;
                    verifyBoxBM.Tag = desc;
                    verifyBoxBD.Tag = desc;
                    labelBirthday.Text = desc.Label;
                    posBirthday = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Hzip)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HZIP);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHzip.Visible = true;
                    verifyBoxHzip.Visible = true;
                    buttonZipSearch.Visible = true;
                    verifyBoxHzip.Tag = desc;
                    labelHzip.Text = desc.Label;
                    posHzip = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Haddress)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HADDRESS);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHaddress.Visible = true;
                    verifyBoxHaddress.Visible = true;
                    buttonZipSearch.Visible = true;
                    verifyBoxHaddress.Tag = desc;
                    labelHaddress.Text = desc.Label;
                    posHaddress = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.CountedDays)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_COUNTEDDAYS);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelCountedDays.Visible = true;
                    verifyBoxCountedDays.Visible = true;
                    verifyBoxCountedDays.Tag = desc;
                    labelCountedDays.Text = desc.Label;
                    posCountedDays = new Point(desc.LocationX, desc.LocationY);
                }
            }

            if (medivery.Total)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_TOTAL);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelTotal.Visible = true;
                    verifyBoxTotal.Visible = true;
                    verifyBoxTotal.Tag = desc;
                    labelTotal.Text = desc.Label;
                    posTotal = new Point(desc.LocationX, desc.LocationY);
                }
            }
            //一部負担金
            if (medivery.Partial)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PARTIAL);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPartial.Visible = true;
                    verifyBoxPartial.Visible = true;
                    verifyBoxPartial.Tag = desc;
                    labelPartial.Text = desc.Label;
                    posPartial = new Point(desc.LocationX, desc.LocationY);
                }
            }
            //請求金額
            if (medivery.Charge)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_CHARGE);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelCharge.Visible = true;
                    verifyBoxCharge.Visible = true;
                    verifyBoxCharge.Tag = desc;
                    labelCharge.Text = desc.Label;
                    posCharge = new Point(desc.LocationX, desc.LocationY);
                }
            }
            //施術所
            if (medivery.Sname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_SNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelSname.Visible = true;
                    verifyBoxSname.Visible = true;
                    verifyBoxSname.Tag = desc;
                    labelSname.Text = desc.Label;
                    posSname = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Fvisittimes)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_FVISITTIMES);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelFvisittimes.Visible = true;
                    verifyBoxFvisittimes.Visible = true;
                    verifyBoxFvisittimes.Tag = desc;
                    labelFvisittimes.Text = desc.Label;
                    posFvisittimes = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Paymentcode)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PAYMENTCODE);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPaymentcode.Visible = true;
                    verifyBoxPaymentcode.Visible = true;
                    verifyBoxPaymentcode.Tag = desc;
                    labelPaymentcode.Text = desc.Label;
                    posPaymentcode = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.DrName)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_DRNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelDrName.Visible = true;
                    verifyBoxDrName.Visible = true;
                    verifyBoxDrName.Tag = desc;
                    labelDrName.Text = desc.Label;
                    posDrName = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Dates)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_DATES);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelDate.Visible = true;
                    verifyBoxDates.Visible = true;
                    verifyBoxDates.Tag = desc;
                    labelDate.Text = desc.Label;
                    posDates = new Point(desc.LocationX, desc.LocationY);
                }
            }

        }






        /// <summary>
        /// 20201110134316 furukawa 過去の個人情報をヒホバンで取得
        /// 個人情報セット
        /// </summary>
        private void setPS(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);
            
            PastData.PersonalData p = new PastData.PersonalData();
            p = PastData.PersonalData.SelectRecordOne($"hnum='{app.HihoNum}'");
            if (p == null) return;
            
            if (
                verifyBoxHnum.Text != string.Empty ||
                verifyBoxHname.Text != string.Empty ||
                verifyBoxHzip.Text != string.Empty ||
                verifyBoxHaddress.Text != string.Empty ||
                verifyBoxPname.Text != string.Empty ||
                verifyBoxBY.Text != string.Empty ||
                verifyBoxBM.Text != string.Empty ||
                verifyBoxBE.Text != string.Empty ||
                verifyBoxBD.Text != string.Empty
                )
            {
                if (System.Windows.Forms.MessageBox.Show(
                    "上書きしますか？",
                    Application.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }

            if (medivery.Hnum) verifyBoxHnum.Text = p.hnum;
            if (medivery.Hname) verifyBoxHname.Text = p.hname;
            if (medivery.Hzip) verifyBoxHzip.Text = p.hzip;
            if (medivery.Haddress) verifyBoxHaddress.Text = p.haddress;
            if (medivery.Pname) verifyBoxPname.Text = p.pname;
            if (medivery.Birthday) setDateValue(p.pbirthday, firstTime, nv, verifyBoxBY, verifyBoxBM, verifyBoxBE, verifyBoxBD);


        }

        /// <summary>
        /// 20201123092828 furukawa 施術所情報過去データリスト表示
        /// 施術所情報
        /// </summary>
        /// <param name="app"></param>
        private void setClinic(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);

            PastData.ClinicData cd = new PastData.ClinicData();
            cd = PastData.ClinicData.SelectRecordOne($"hnum='{app.HihoNum}'");
            if (cd == null) return;

            if (medivery.Hnum) verifyBoxHnum.Text = cd.hnum;
            if (medivery.Sname) verifyBoxSname.Text = cd.clinicname;
            if (medivery.DrName) verifyBoxDrName.Text = cd.doctorname;
            
        }


        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済)) return;

            //20201123094054 furukawa st ////////////////////////
            //いちいちポップアップが出てうっとうしいからやめて

                ////20201110134250 furukawa st ////////////////////////
                ////過去データをロード            
                //setPS(app);
                //setClinic(app);
                ////20201110134250 furukawa ed ////////////////////////
            //20201123094054 furukawa ed ////////////////////////
        }




        /// <summary>
        /// //20201112101008 furukawa 選択リストを再表示する手段がないので作った
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectlist_Click(object sender, EventArgs e)
        {
            setPS((App)bsApp.Current);
            setClinic((App)bsApp.Current);
        }


        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;

            var descs = (Medivery.Description)t.Tag;
            labelDescription.Text = descs?.Text ?? string.Empty;
            var d = (descs?.Ratio ?? 0) == 0 ? 0.4f : descs.Ratio;
            scrollPictureControl1.Ratio = d;

            Point p;
            if (mediymConts.Contains(sender)) p = posMediYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (hnameConts.Contains(sender)) p = posHname;
            else if (pnameConts.Contains(sender)) p = posPname;
            else if (birthdayConts.Contains(sender)) p = posBirthday;
            else if (hzipConts.Contains(sender)) p = posHzip;
            else if (haddressConts.Contains(sender)) p = posHaddress;
            else if (counteddaysConts.Contains(sender)) p = posCountedDays;
            else if (datesConts.Contains(sender)) p = posDates;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (partialConts.Contains(sender)) p = posPartial;
            else if (chargeConts.Contains(sender)) p = posCharge;
            else if (snameConts.Contains(sender)) p = posSname;
            else if (fvisittimesConts.Contains(sender)) p = posFvisittimes;
            else if (paymentcodeConts.Contains(sender)) p = posPaymentcode;
            else if (drnameConts.Contains(sender)) p = posDrName;
            else return;

            scrollPictureControl1.ScrollPosition = p;

            //入力欄が住所の場合
            if ((descs?.Name ?? "") == Medivery.Description.DESC_NAME_HADDRESS &&
                t.Text != "")
            {
                t.Select(t.Text.Length, 0);
            }
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void MediveryForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (pcForm != null && !pcForm.IsDisposed) pcForm.Dispose();
        }

        private void verifyBoxHzip_Leave(object sender, EventArgs e)
        {
            if (!medivery.Haddress) return;

            //郵便番号と住所入力欄があれば自動で設定する
            if (int.TryParse(verifyBoxHzip.Text, out int zipcode))
            {
                var pc = PostalCode.GetPostalCode(zipcode);
                if (pc != null)
                {
                    var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    var curAdd = verifyBoxHaddress.Text;
                    if (!curAdd.StartsWith(add)) verifyBoxHaddress.Text = add;
                }
                else verifyBoxHaddress.Text = "";
            }
        }

        private void verifyBoxPname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー
            {
                if (medivery.Hname)
                {
                    //被保険者氏名と受療者氏名入力欄があれば名前をコピーする
                    var cvb = (VerifyBox)sender;
                    cvb.Text = verifyBoxHname.Text;
                    //かつ、次のコントロールへ移る
                    SelectNextControl(verifyBoxPname, true, true, true, false);
                    return;
                }
            }
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;
            bool isEmpty(string s) => string.IsNullOrWhiteSpace(s);
            bool isZero(int i) => i == 0;
            bool checkName(string name)
            {
                if (string.IsNullOrWhiteSpace(name)) return false;//空白
                var names = name.Split('　');
                if (names.Count() < 2) return false;//全角スペース区切りなし
                return true;
            }

            foreach (var item in inputControls)
            {
                if (!(item is VerifyBox vb)) continue;
                if (!item.Visible) continue;
                string value = vb.Text;
                var desc = (Medivery.Description)vb.Tag;
                if (desc == null) continue;

                //登録値と入力値が空白の場合はアップデートしない
                switch (desc.Name)
                {
                    case Medivery.Description.DESC_NAME_MEDIYM:
                        if (isEmpty(verifyBoxMediY.Text) && isEmpty(verifyBoxMediM.Text))
                        {
                            setStatus(verifyBoxMediY, false);
                            setStatus(verifyBoxMediM, false);
                        }
                        else
                        {
                            int y = verifyBoxMediY.GetIntValue();
                            int m = verifyBoxMediM.GetIntValue();
                            var result = y > 0 && m > 0;
                            if (result)
                            {
                                app.MediYear = y;
                                app.MediMonth = m;
                            }
                            setStatus(verifyBoxMediY, !result);
                            setStatus(verifyBoxMediM, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HNUM:
                        if (isEmpty(app.HihoNum) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = !string.IsNullOrEmpty(value);
                            if (result) app.HihoNum = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HNAME:
                        if (isEmpty(app.HihoName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = checkName(value);
                            if (result) app.HihoName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PNAME:
                        if (isEmpty(app.PersonName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = checkName(value);
                            if (result) app.PersonName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_BIRTHDAY:
                        if (app.Birthday == DateTime.MinValue &&
                            isEmpty(verifyBoxBE.Text) &&
                            isEmpty(verifyBoxBY.Text) &&
                            isEmpty(verifyBoxBM.Text) &&
                            isEmpty(verifyBoxBD.Text))
                        {
                            setStatus(verifyBoxBE, false);
                            setStatus(verifyBoxBY, false);
                            setStatus(verifyBoxBM, false);
                            setStatus(verifyBoxBD, false);
                        }
                        else
                        {
                            var dt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);
                            var result = dt != DateTimeEx.DateTimeNull;
                            if (result) app.Birthday = dt;
                            setStatus(verifyBoxBE, !result);
                            setStatus(verifyBoxBY, !result);
                            setStatus(verifyBoxBM, !result);
                            setStatus(verifyBoxBD, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HZIP:
                        if (isEmpty(app.HihoZip) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            result &= value.Length == 7;
                            app.HihoZip = value;
                            setStatus(vb, false);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HADDRESS:
                        if (isEmpty(app.HihoAdd) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            app.HihoAdd = value;
                            setStatus(vb, false);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_COUNTEDDAYS:
                        if (isZero(app.CountedDays) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int d);
                            if (result) app.CountedDays = d;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_DATES:
                        var datesRes = datesStringAdjust(ref value);
                        setStatus(vb, !datesRes);
                        if (datesRes) app.TaggedDatas.Dates = value;

                        break;

                    case Medivery.Description.DESC_NAME_TOTAL:
                        if (isZero(app.Total) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int t);
                            if (result) app.Total = t;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PARTIAL:
                        if (isZero(app.Partial) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int p);
                            if (result) app.Partial = p;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_CHARGE:
                        if (isZero(app.Charge) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int c);
                            if (result) app.Charge = c;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_SNAME:
                        if (isEmpty(app.ClinicName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            if (result) app.ClinicName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_FVISITTIMES:
                        if (isZero(app.VisitTimes) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int t);
                            if (result) app.VisitTimes = t;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PAYMENTCODE:
                        if (isEmpty(app.AccountNumber) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int c);
                            if (result) app.AccountNumber = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_DRNAME:
                        if (isEmpty(app.DrName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != null;
                            if (result) app.DrName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    default:
                        MessageBox.Show("不正なコントロールが存在します。\r\nシステム担当者を呼んでください。", "エラー");
                        return false;
                }
            }

            return !hasError;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済)) return true;

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //施術所名が登録値と異なる場合の処理
            var clinicUpdated = false;
            if (clinicStorage && medivery.Sname && clinic != null)
            {
                if (!string.IsNullOrEmpty(app.ClinicName) && clinic.Name != app.ClinicName)
                {
                    var desc = (Medivery.Description)verifyBoxSname.Tag;
                    var update = MessageBox.Show(
                        $"{desc.Label}が登録されている値（{clinic.Name}）と異なっています。\r\n上書きしますか？",
                        "変更確認",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (update != DialogResult.Yes)
                    {
                        verifyBoxSname.Focus();
                        verifyBoxSname.SelectAll();
                        return false;
                    }
                    clinic.Name = app.ClinicName;
                    clinic.Update_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                        //clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    clinic.Verified = false;
                    clinic.Update();

                    //同じ施術所番号を使用しているものをすべて再チェック（今開いている一覧上のみ）
                    var list = ((List<App>)bsApp.DataSource).FindAll(a => a.StatusFlagCheck(StatusFlag.入力済));
                    var updateList = new List<App>();
                    updateList.AddRange(list.FindAll(a =>
                        a.Aid != app.Aid && a.DrNum == app.DrNum && a.ClinicName != "" && a.ClinicName != app.ClinicName));

                    if (updateList.Count > 0)
                    {
                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        var multiUpdate = MessageBox.Show(
                            $"{DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}{(cym % 100).ToString("00")}" +
                            $"分の追加ベリファイ済の申請書のうち同じ施術所番号を使用しているもので、" +
                            $"{desc.Label}が登録されている値（{clinic.Name}）と異なるものが{updateList.Count}件存在します。\r\n" +
                            $"それらの内容も変更してもよろしいですか？",
                            "変更確認",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                            /*
                            var multiUpdate = MessageBox.Show(
                                $"{DateTimeEx.GetHsYearFromAd(cym / 100)}{(cym % 100).ToString("00")}" +
                                $"分の追加ベリファイ済の申請書のうち同じ施術所番号を使用しているもので、" +
                                $"{desc.Label}が登録されている値（{clinic.Name}）と異なるものが{updateList.Count}件存在します。\r\n" +
                                $"それらの内容も変更してもよろしいですか？",
                                "変更確認",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                            */
                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        if (multiUpdate == DialogResult.Yes)
                        {
                            using (var tran = DB.Main.CreateTransaction())
                            {
                                foreach (var item in updateList)
                                {
                                    item.ClinicName = app.ClinicName;
                                    if (!item.UpdateMedivery(App.UPDATE_TYPE.Null, tran))
                                    {
                                        tran.Rollback();
                                        MessageBox.Show("変更に失敗しました。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }
                                }
                                tran.Commit();
                            }
                        }
                    }
                    //後述のVerified更新を回避する
                    clinicUpdated = true;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.Null : App.UPDATE_TYPE.SecondInput;

                //フラグ整理
                if (firstTime)
                {
                    app.StatusFlagRemove(StatusFlag.追加ベリ済);
                    app.StatusFlagSet(StatusFlag.追加入力済);
                    app.AdditionalUid1 = User.CurrentUser.UserID;
                }
                else
                {
                    app.StatusFlagSet(StatusFlag.追加ベリ済);
                    app.AdditionalUid2 = User.CurrentUser.UserID;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                tran.Commit();
            }
            
            //施術所番号が存在し、施術所名の入力がある場合
            if (clinicStorage && medivery.Sname && !string.IsNullOrEmpty(app.ClinicName))
            {
                if (clinic == null)
                {
                    //初登場だった場合は目録へ追加
                    clinic = new Clinic();
                    long.TryParse(app.DrNum, out long code);
                    clinic.Code = code;
                    clinic.Name = app.ClinicName;
                    clinic.Insert_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    clinic.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                        //clinic.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    clinic.Verified = false;
                    clinic.Insert();
                }
                else if (!clinicUpdated && clinic.Name == app.ClinicName && !clinic.Verified)
                {
                    //登録値と同じで確定フラグが立っていない場合は上書き
                    clinic.Update_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                        //clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    clinic.Verified = true;
                    clinic.UpdateVerified();
                }
            }
            return true;
        }

        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                void setPostal(PostalCode pc)
                {
                    if (pc == null) return;
                    if (medivery.Hzip) verifyBoxHzip.Text = pc.Postal_Code.ToString("0000000");
                    if (medivery.Haddress) verifyBoxHaddress.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    prevTodofuken = pc.Todofuken;
                    prevSikutyoson = pc.Sichokuson;
                    verifyBoxHaddress.Focus();
                    verifyBoxHaddress.Select(verifyBoxHaddress.Text.Length, 0);
                    selectedHistory.Add(pc);
                }

                var x = panelRight.Location.X + panelAddInput.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = this.Height - 620;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible) pcForm.Show(this);
            else pcForm.MoveDefaultFocus();
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.AdditionalUid1) +
                "\r\n入力2:  " + User.GetUserName(app.AdditionalUid2);

            //施術所名が必要なら、目録から施術所（名）情報の取得
            clinic = medivery.Sname && clinicStorage ?
                Clinic.GetClinic(app.DrNum) : null;
            
            if (app.StatusFlagCheck(StatusFlag.追加入力済)) setValues(app);

            //画像の表示
            setImage(app);
            changedReset(app);


            //20190606175029 furukawa st ////////////////////////
            //過去に入力した個人情報を表示            

                //未処理だけにしようと思ったが、どんなフラグでくるか不明なので全部通すことにする
                //if (app.StatusFlagCheck(StatusFlag.未処理))
                setInputtedData(app);
            //20190606175029 furukawa ed ////////////////////////

        }


        //20190606174925 furukawa st ////////////////////////
        //過去に入力した個人情報を表示       
        private void setInputtedData(App app)
        {

            //20190827114415 furukawa st ////////////////////////
            //保険者別にする
            
            //if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.SHARP_KENPO) return;
            //20190827114415 furukawa ed ////////////////////////



            //20190618170101 furukawa st ////////////////////////
            //先に被保番、生年月日、性別でapp取得

            StringBuilder sb = new StringBuilder();
            sb.Append(" where ");
            sb.AppendFormat("a.hnum='{0}' ", app.HihoNum);
            sb.AppendFormat("and a.pbirthday='{0}' ", app.Birthday);
            sb.AppendFormat("and a.psex='{0}' ", app.Sex);
            sb.AppendFormat("and a.sregnumber='{0}' ", app.DrNum); //20190619092816 furukawa 施術師コード追加
            sb.Append("and a.hname<>'' ");

            //sb.AppendFormat("and a.hzip='{0}' ", app.HihoZip);
            //sb.AppendFormat("and a.hname='{0}' ", app.HihoName);
            //sb.AppendFormat("and a.haddress='{0}' ", app.HihoAdd);


            lstAppForInputted = App.GetAppsWithWhere(sb.ToString());
            //20190618170101 furukawa ed ////////////////////////

            //20190618170213 furukawa st ////////////////////////
            //処理年月で降順
            
            lstAppForInputted.Sort((x, y) => y.CYM.CompareTo(x.CYM));
            //20190618170213 furukawa ed ////////////////////////


            //20190703174326 furukawa st ////////////////////////
            //２回め入力の自動入力はしない
            
            if (!firstTime) return;
            //20190703174326 furukawa ed ////////////////////////



            //複数ある場合はソートした最初に一致するレコードを選ぶ
            foreach (App apprec in lstAppForInputted)
            {

                if (apprec.HihoNum == app.HihoNum && 
                    apprec.Birthday==app.Birthday &&
                    apprec.DrNum == app.DrNum &&    //20190619092816 furukawa 施術師コード追加//
                    apprec.Sex==app.Sex)
                {
                    //20191016141640 furukawa st ////////////////////////
                    //保険者分岐が増えたので関数化
                    

                    /*      
                     *      if (verifyBoxHname.Text==string.Empty) verifyBoxHname.Text = apprec.HihoName;
                            if (verifyBoxHaddress.Text == string.Empty) verifyBoxHaddress.Text = apprec.HihoAdd;
                            if (verifyBoxHzip.Text == string.Empty) verifyBoxHzip.Text = apprec.HihoZip;
                            if (verifyBoxSname.Text == string.Empty) verifyBoxSname.Text = clinic != null ? clinic.Name : string.Empty;

                            //20190617135450 furukawa st ////////////////////////
                            //受療者は自動入力しない

                            //verifyBoxPname.Text = apprec.PersonName;
                            //20190617135450 furukawa ed ////////////////////////


                            //生年月日
                            if (verifyBoxBY.Text == string.Empty) verifyBoxBY.Text = apprec.Birthday.ToJyearShortStr();
                            if (verifyBoxBM.Text == string.Empty) verifyBoxBM.Text = apprec.Birthday.Month.ToString();
                            if (verifyBoxBD.Text == string.Empty) verifyBoxBD.Text = apprec.Birthday.Day.ToString();
                            */



                    InsurerProcess(apprec);

                    //20191016141640 furukawa ed ////////////////////////



                    break;
                }
            }
        }
        //20190606174925 furukawa ed ////////////////////////



            /// <summary>
            /// 保険者別のロード
            /// </summary>
            /// <param name="apprec"></param>
        private void InsurerProcess(App apprec)            
        {
            try
            {
                //学校共済はまとめる
                if (Insurer.GetInsurerType(Insurer.CurrrentInsurer.InsurerID) == INSURER_TYPE.学校共済)
                {
                    if (verifyBoxHname.Text == string.Empty) verifyBoxHname.Text = apprec.HihoName;                                 //被保険者名
                    if (verifyBoxHaddress.Text == string.Empty) verifyBoxHaddress.Text = apprec.HihoAdd;                            //被保険者住所
                    if (verifyBoxHzip.Text == string.Empty) verifyBoxHzip.Text = apprec.HihoZip;                                    //被保険者郵便番号
                    if (verifyBoxSname.Text == string.Empty) verifyBoxSname.Text = clinic != null ? clinic.Name : string.Empty;     //施術所名

                    //20190617135450 furukawa st ////////////////////////
                    //受療者は自動入力しない

                    //verifyBoxPname.Text = apprec.PersonName;
                    //20190617135450 furukawa ed ////////////////////////


                    //生年月日
                    if (verifyBoxBY.Text == string.Empty) verifyBoxBY.Text = apprec.Birthday.ToJyearShortStr();
                    if (verifyBoxBM.Text == string.Empty) verifyBoxBM.Text = apprec.Birthday.Month.ToString();
                    if (verifyBoxBD.Text == string.Empty) verifyBoxBD.Text = apprec.Birthday.Day.ToString();
                }
                //それ以外の保険者
                else
                {
                    switch (Insurer.CurrrentInsurer.InsurerID)
                    {
                        
                        case (int)InsurerID.SHARP_KENPO://シャープ健保
                            if (verifyBoxHname.Text == string.Empty) verifyBoxHname.Text = apprec.HihoName;         //被保険者名
                            if (verifyBoxPname.Text == string.Empty) verifyBoxPname.Text = apprec.PersonName;       //受療者名
                            if (verifyBoxDrName.Text == string.Empty) verifyBoxDrName.Text = apprec.DrName;         //柔整師名
                            if (verifyBoxSname.Text == string.Empty) verifyBoxSname.Text = apprec.ClinicName;       //施術所名

                            break;

                        default:

                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
            }
        }



        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            //20190606175850 furukawa st ////////////////////////
            //画像パスが見つからない場合抜ける
            
            if (!System.IO.File.Exists(fn.ToString()))
            {
                labelImageName.Text = "画像が見つかりません";
                return;
            }
            //20190606175850 furukawa ed ////////////////////////

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = baseRatio;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posHname;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);
            setValue(verifyBoxMediY, app.MediYear, firstTime, nv);
            setValue(verifyBoxMediM, app.MediMonth, firstTime, nv);

            foreach (var item in inputControls)
            {
                if (!(item is VerifyBox vb)) continue;
                if (!item.Visible) continue;
                var desc = (Medivery.Description)vb.Tag;
                if (desc == null) continue;
                var scan = Scan.Select(app.ScanID);

                switch (desc.Name)
                {
                    case Medivery.Description.DESC_NAME_MEDIYM:
                        setValue(verifyBoxMediY, app.MediYear, firstTime, nv);
                        setValue(verifyBoxMediM, app.MediMonth, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_HNUM:
                        setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_HNAME:
                        setValue(verifyBoxHname, app.HihoName, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_PNAME:
                        setValue(verifyBoxPname, app.PersonName, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_BIRTHDAY:
                        setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                        setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                        setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                        setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_HZIP:
                        setValue(verifyBoxHzip, app.HihoZip, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_HADDRESS:
                        setValue(verifyBoxHaddress, app.HihoAdd, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_COUNTEDDAYS:
                        setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_DATES:
                        setValue(verifyBoxDates, app.TaggedDatas.Dates, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_TOTAL:
                        setValue(verifyBoxTotal, app.Total, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_PARTIAL:
                        setValue(verifyBoxPartial, app.Partial, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_CHARGE:
                        setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_SNAME:
                        var tmpNV = clinic == null || !clinic.Verified;  //目録なし || 確定してない => ベリファイ必要
                        var name = tmpNV ? app.ClinicName : clinic.Name;
                        setValue(verifyBoxSname, name, firstTime, tmpNV);
                        break;
                    case Medivery.Description.DESC_NAME_FVISITTIMES:
                        setValue(verifyBoxFvisittimes, app.VisitTimes, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_PAYMENTCODE:
                        setValue(verifyBoxPaymentcode, app.AccountNumber, firstTime, nv);
                        break;
                    case Medivery.Description.DESC_NAME_DRNAME:
                        setValue(verifyBoxDrName, app.DrName, firstTime, nv);
                        break;
                }
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (mediymConts.Any(c => c.Focused)) posMediYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (hnameConts.Any(c => c.Focused)) posHname = pos;
            else if (pnameConts.Any(c => c.Focused)) posPname = pos;
            else if (birthdayConts.Any(c => c.Focused)) posBirthday = pos;
            else if (hzipConts.Any(c => c.Focused)) posHzip = pos;
            else if (haddressConts.Any(c => c.Focused)) posHaddress = pos;
            else if (counteddaysConts.Any(c => c.Focused)) posCountedDays = pos;
            else if (datesConts.Any(c => c.Focused)) posDates = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (partialConts.Any(c => c.Focused)) posPartial = pos;
            else if (chargeConts.Any(c => c.Focused)) posCharge = pos;
            else if (snameConts.Any(c => c.Focused)) posSname = pos;
            else if (fvisittimesConts.Any(c => c.Focused)) posFvisittimes = pos;
            else if (paymentcodeConts.Any(c => c.Focused)) posPaymentcode = pos;
            else if (drnameConts.Any(c => c.Focused)) posDrName = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void MediveryForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            var list = (List<App>)bsApp.DataSource;

            //入力タイプ選択
            using (var f = new MediveryInputSelectForm(list))
            {
                if (f.ShowDialog() != DialogResult.OK)
                {
                    this.Close();
                    return;
                }

                this.firstTime = f.IsFirstTime;

                //１回目入力未完了だがベリファイできるものだけ先に行う
                if (!f.IsFirstTime && !f.IsFirstTimeComplete)
                    list = list.FindAll(a => a.StatusFlagCheck(StatusFlag.追加入力済));
            }

            //連番を付与
            int index = 1;
            list.ForEach(c => c.ViewIndex = index++);

            //グリッドに表示
            dataGridViewPlist.DataSource = bsApp;
            foreach (DataGridViewColumn c in dataGridViewPlist.Columns) c.Visible = false;

            dataGridViewPlist.Columns[nameof(App.ViewIndex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].Width = 30;
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].HeaderText = "No.";
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].DisplayIndex = 0;
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 60;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.Aid)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].DisplayIndex = 2;

            //入力欄初期化
            dataGridViewPlist.Focus();
            initControl();
            bsApp.ResetBindings(false);
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            bsApp.CurrentChanged += BsApp_CurrentChanged;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
            focusBack(false);


            //20201123112637 furukawa st ////////////////////////
            //入力しづらいので削除

            //setPS(app);
            //20201123112637 furukawa ed ////////////////////////

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
