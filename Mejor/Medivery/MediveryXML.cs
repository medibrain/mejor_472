﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace Mejor.Medivery
{
    public class MediveryXML
    {
        [DB.DbAttribute.PrimaryKey]
        public int InsurerID { get; set; }          //保険者ID
        public bool usemedivery { get; set; }       //使用可否
        public string setting { get; set; }         //説明文等のカンマ区切り文字列
        public int userid { get; set; }             //更新ユーザ
        public DateTime dtupd { get; set; }         //更新日

        
        /// <summary>
        /// 指定保険者の設定値行を保持（入力項目名、入力項目のsetting）
        /// </summary>
        public static Dictionary<string, MediverySetting> dicMvSetting = new System.Collections.Generic.Dictionary<string, MediverySetting>();

        /// <summary>
        /// メディベリー使用するか
        /// </summary>
        public static Dictionary<string, MediveryXML> dicUseMvXML = new Dictionary<string, MediveryXML>();
 

        //入力画面項目名
        public const string FIELD_NAME_MEDIYM = "mediym";                        //診療年月
        public const string FIELD_NAME_HNUM = "hnum";                            //被保険者番号
        public const string FIELD_NAME_HNAME = "hname";                          //被保険者名
        public const string FIELD_NAME_PNAME = "pname";                          //受療者名
        public const string FIELD_NAME_BIRTHDAY = "birthday";                    //受療者生年月日
        public const string FIELD_NAME_HZIP = "hzip";                            //被保険者郵便番号
        public const string FIELD_NAME_HADDRESS = "haddress";                    //被保険者住所
        public const string FIELD_NAME_COUNTEDDAYS = "counteddays";              //施術日数
        public const string FIELD_NAME_DATES = "dates";                          //診療日
        public const string FIELD_NAME_TOTAL = "total";                          //合計金額
        public const string FIELD_NAME_PARTIAL = "partial";                      //患者一時負担金
        public const string FIELD_NAME_CHARGE = "charge";                        //請求金額
        public const string FIELD_NAME_SNAME = "sname";                          //医療機関名（施術所名
        public const string FIELD_NAME_FVISITTIMES = "fvisittimes";              //往療回数
        public const string FIELD_NAME_PAYMENTCODE = "paymentcode";              //支払先コード
        public const string FIELD_NAME_DRNAME = "drname";                        //施術者名

        //20210430093954 furukawa st ////////////////////////
        //性別追加        
        public const string FIELD_NAME_SEX = "psex";
        //20210430093954 furukawa ed ////////////////////////

        //20210510115822 furukawa st ////////////////////////
        //口座名義        
        public const string FIELD_NAME_ACCNAME = "accname";
        //20210510115822 furukawa ed ////////////////////////

        //20210706091118 furukawa st ////////////////////////
        //登録記号番号追加
        public const string FIELD_NAME_DRNUM = "drnum";
        //20210706091118 furukawa ed ////////////////////////





        /// <summary>
        /// テーブルのSettingフィールドの文字列を、項目ごとに分けて持たせるdatatable
        /// </summary>
        public static System.Data.DataTable dtMediverySetting = new System.Data.DataTable();

        /// <summary>
        /// テーブルのSettingフィールドに格納されているxml文字列の項目名
        /// </summary>
        public class MediverySetting
        {
            public bool Use { get; set; }                       //使用可否フラグ
            public string FieldName { get; set; }               //フィールド名
            public string Label { get; set; }                   //ラベルの表示文字列
            public string DescriptionText { get; set; }         //説明文
            private int locationX;
            private int locationY;
            public int LocationX { get { return locationX; } set { locationX = value == -1 ? 0 : value; } }     //コントロールの座標X
            public int LocationY { get { return locationY; } set { locationY = value == -1 ? 0 : value; } }     //コントロールの座標Y
            public bool FirstInput { get; set; }    //1回目入力するか否かフラグ
            public float Ratio { get; set; }        //画像倍率
        }



        //20210330131101 furukawa medivery使用保険者リストをメモリに持っておかないと遅い
        /// <summary>
        /// medivery使用保険者リスト
        /// </summary>
        /// <returns></returns>
        public static bool GetUseList()
        {
            var DBNAME = "jyusei";
            var curDBName = DB.GetMainDBName();

            try
            {
                if (dicUseMvXML.Count > 0) return true;

                if (curDBName != DBNAME) DB.SetMainDBName(DBNAME);

                var sql = $"SELECT * FROM medivery_setting";
                var result = DB.Main.Query<MediveryXML>(sql);
                if (result != null)
                {
                    dicUseMvXML.Clear();

                    
                    foreach (MediveryXML m in result.ToList())
                    {
                        dicUseMvXML.Add(m.InsurerID.ToString(), m);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                if (curDBName != DBNAME) DB.SetMainDBName(curDBName);
            }
        }


        /// <summary>
        /// 該当保険者の追加入力項目リストを取得します。更新にも利用できます。
        /// </summary>
        /// <param name="insID"></param>
        /// <returns></returns>
        public static bool GetMediveryList(int insID)
        {
            var DBNAME = "jyusei";
            var curDBName = DB.GetMainDBName();
            try
            {
                if (curDBName != DBNAME) DB.SetMainDBName(DBNAME);
                
                var sql = $"SELECT * FROM medivery_setting where insurerid={insID}";
                var result =DB.Main.Query<MediveryXML>(sql);
                if (result != null)
                {
                    dtMediverySetting.Clear();


                    foreach (MediveryXML m in result.ToList())
                    {
                        setSettings(m);
                    }
                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                if (curDBName != DBNAME) DB.SetMainDBName(curDBName);
            }
        }



        /// <summary>
        /// xml文字列ロード
        /// </summary>
        /// <param name="mvxml"></param>
        private static void setSettings(MediveryXML mvxml)
        {
           
            System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
            xml.LoadXml(mvxml.setting);

            //rootノード取得
            System.Xml.XmlElement root = xml.DocumentElement;


            //rootの一つ下の子の数で列作る             

            System.Xml.XmlNode firstChild = root.FirstChild;

            for (int c = 0; c < firstChild.ChildNodes.Count; c++)
            {
                if (!dtMediverySetting.Columns.Contains(firstChild.ChildNodes[c].Name)) dtMediverySetting.Columns.Add(firstChild.ChildNodes[c].Name);
            }

            if (!dtMediverySetting.Columns.Contains("insurerid")) dtMediverySetting.Columns.Add("insurerid");
            if (!dtMediverySetting.Columns.Contains("fieldname")) dtMediverySetting.Columns.Add("fieldname");

            System.Xml.XmlNode nextnd;

            //子の数分だけループ
            for (int r = 0; r < root.ChildNodes.Count; r++)
            {
                System.Data.DataRow drsetting = dtMediverySetting.NewRow();
                
                
            
                //次のノード（項目名）に入替
                nextnd = root.ChildNodes[r];
                
              

                for (int c = 0; c < dtMediverySetting.Columns.Count; c++)
                {
                    System.Xml.XmlNode fields = nextnd.ChildNodes[c];
                  
                    if (fields == null) continue;
                    //要素の値を取得
                    drsetting[c] = fields.InnerText;
                }

                drsetting["fieldname"] = nextnd.Name;
                drsetting["insurerid"] = mvxml.InsurerID;

                dtMediverySetting.Rows.Add(drsetting);

            }

        }


        /// <summary>
        /// InsurerIDからMediveryインスタンスを取得します。
        /// 登録されていない場合はnullを返します。
        /// </summary>
        /// <param name="intInsurerID"></param>
        /// <returns></returns>
        public static Dictionary<string, MediverySetting> GetMediVery(int intInsurerID)
        {

            //指定保険者のdatatable から dicMvSetting にコピー
            System.Data.DataRow[] drs = dtMediverySetting.Select($"insurerid={intInsurerID}");
            dicMvSetting.Clear();

            foreach (System.Data.DataRow dr in drs)
            {
                MediverySetting m = new MediverySetting();
                m.FieldName = dr["fieldname"].ToString();
                m.Use = bool.Parse(dr["use"].ToString());
                m.Label = dr["label"].ToString();
                m.DescriptionText = dr["desc"].ToString();
                m.LocationX = int.Parse(dr["x"].ToString() == string.Empty ? "0" : dr["x"].ToString());
                m.LocationY = int.Parse(dr["y"].ToString() == string.Empty ? "0" : dr["y"].ToString());
                m.FirstInput = bool.Parse(dr["firsttime"].ToString() == string.Empty ? "false" : dr["firsttime"].ToString());
                m.Ratio = float.Parse(dr["ratio"].ToString() == string.Empty ? "0" : dr["ratio"].ToString());

                if (!dicMvSetting.ContainsKey("fieldname")) dicMvSetting.Add(dr["fieldname"].ToString(), m);
                
            }
            
            return dicMvSetting;
        }

        /// <summary>
        /// アイテム名から対応するコントロール制御情報を取得します。
        /// 対応するものが無い場合はnullを返します。
        /// </summary>
        /// <param name="itemname"></param>
        /// <returns></returns>
        //public Description GetDescription(string itemname)
        //{
        //    if (settingDic.ContainsKey(itemname))
        //        return settingDic[itemname];
        //    else
        //        return null;
        //}

        private int toInt(string number)
        {
            int numberInt;
            if (int.TryParse(number, out numberInt)) return numberInt;
            return -1;
        }

        private bool toBool(string val)
        {
            if (bool.TryParse(val, out bool result)) return result;
            return true;
        }
    }
}
