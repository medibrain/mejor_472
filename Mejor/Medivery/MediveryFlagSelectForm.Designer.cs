﻿namespace Mejor.Medivery
{
    partial class MediveryFlagSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMedivery = new System.Windows.Forms.Button();
            this.buttonListExport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonDantai = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonMedivery
            // 
            this.buttonMedivery.BackColor = System.Drawing.Color.CornflowerBlue;
            this.buttonMedivery.ForeColor = System.Drawing.Color.White;
            this.buttonMedivery.Location = new System.Drawing.Point(585, 59);
            this.buttonMedivery.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonMedivery.Name = "buttonMedivery";
            this.buttonMedivery.Size = new System.Drawing.Size(268, 172);
            this.buttonMedivery.TabIndex = 6;
            this.buttonMedivery.Text = "追加項目入力\r\n\r\n（Medi-very）";
            this.buttonMedivery.UseVisualStyleBackColor = false;
            this.buttonMedivery.Click += new System.EventHandler(this.buttonMedivery_Click);
            // 
            // buttonListExport
            // 
            this.buttonListExport.BackColor = System.Drawing.Color.Coral;
            this.buttonListExport.Location = new System.Drawing.Point(702, 240);
            this.buttonListExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonListExport.Name = "buttonListExport";
            this.buttonListExport.Size = new System.Drawing.Size(152, 124);
            this.buttonListExport.TabIndex = 7;
            this.buttonListExport.Text = "照会リスト（csv）\r\n\r\n出力";
            this.buttonListExport.UseVisualStyleBackColor = false;
            this.buttonListExport.Click += new System.EventHandler(this.buttonListExport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(536, 211);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = ">>";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(18, 59);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(486, 307);
            this.dataGridView1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "入力対象を選択";
            // 
            // buttonDantai
            // 
            this.buttonDantai.BackColor = System.Drawing.Color.LightGreen;
            this.buttonDantai.Enabled = false;
            this.buttonDantai.ForeColor = System.Drawing.Color.Black;
            this.buttonDantai.Location = new System.Drawing.Point(585, 240);
            this.buttonDantai.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonDantai.Name = "buttonDantai";
            this.buttonDantai.Size = new System.Drawing.Size(108, 124);
            this.buttonDantai.TabIndex = 11;
            this.buttonDantai.Text = "団体情報\r\n\r\n登録";
            this.buttonDantai.UseVisualStyleBackColor = false;
            this.buttonDantai.Click += new System.EventHandler(this.buttonDantai_Click);
            // 
            // MediveryFlagSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 384);
            this.Controls.Add(this.buttonDantai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonListExport);
            this.Controls.Add(this.buttonMedivery);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MediveryFlagSelectForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Medivery";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonMedivery;
        private System.Windows.Forms.Button buttonListExport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonDantai;
    }
}