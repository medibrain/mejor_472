﻿namespace Mejor.Medivery
{
    partial class MediveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.userControlImage1 = new UserControlImage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panelUP = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textDispM = new System.Windows.Forms.TextBox();
            this.textDispY = new System.Windows.Forms.TextBox();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelAddInput = new System.Windows.Forms.Panel();
            this.labelDrName = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.verifyBoxPartial = new Mejor.VerifyBox();
            this.labelPartial = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.labelCharge = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxCountedDays = new Mejor.VerifyBox();
            this.labelCountedDays = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.labelHnum = new System.Windows.Forms.Label();
            this.verifyBoxMediM = new Mejor.VerifyBox();
            this.verifyBoxMediY = new Mejor.VerifyBox();
            this.labelMediY = new System.Windows.Forms.Label();
            this.labelMediM = new System.Windows.Forms.Label();
            this.labelMediYM = new System.Windows.Forms.Label();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.labelBirthdayGengo = new System.Windows.Forms.Label();
            this.labelBirthdayDate = new System.Windows.Forms.Label();
            this.labelBirthdayYear = new System.Windows.Forms.Label();
            this.labelBirthdayMonth = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelHnameCopy = new System.Windows.Forms.Label();
            this.verifyBoxHname = new Mejor.VerifyBox();
            this.buttonZipSearch = new System.Windows.Forms.Button();
            this.labelHname = new System.Windows.Forms.Label();
            this.labelPaymentcode = new System.Windows.Forms.Label();
            this.verifyBoxPname = new Mejor.VerifyBox();
            this.verifyBoxPaymentcode = new Mejor.VerifyBox();
            this.labelPname = new System.Windows.Forms.Label();
            this.labelFvisittimes = new System.Windows.Forms.Label();
            this.verifyBoxHzip = new Mejor.VerifyBox();
            this.verifyBoxFvisittimes = new Mejor.VerifyBox();
            this.labelHzip = new System.Windows.Forms.Label();
            this.labelSname = new System.Windows.Forms.Label();
            this.verifyBoxHaddress = new Mejor.VerifyBox();
            this.verifyBoxSname = new Mejor.VerifyBox();
            this.labelHaddress = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelStatusOCR = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            this.panelUP.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelAddInput.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(647, 5);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 53);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(320, 669);
            this.dataGridViewPlist.TabIndex = 0;
            this.dataGridViewPlist.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPlist_ColumnHeaderMouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "年";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(75, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "月 請求分";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Controls.Add(this.splitter1);
            this.panelLeft.Controls.Add(this.panelDatalist);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(3, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(591, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(323, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(268, 722);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 681);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 681);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 681);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(171, 681);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(8, 8);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(268, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(320, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 722);
            this.splitter1.TabIndex = 40;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Controls.Add(this.splitter3);
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.MinimumSize = new System.Drawing.Size(320, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(320, 722);
            this.panelDatalist.TabIndex = 1;
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 50);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(320, 3);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.Cyan;
            this.panelUP.Controls.Add(this.label3);
            this.panelUP.Controls.Add(this.textBoxCount);
            this.panelUP.Controls.Add(this.label2);
            this.panelUP.Controls.Add(this.textDispM);
            this.panelUP.Controls.Add(this.textDispY);
            this.panelUP.Controls.Add(this.label10);
            this.panelUP.Controls.Add(this.label1);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(320, 50);
            this.panelUP.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "照会予定：";
            // 
            // textBoxCount
            // 
            this.textBoxCount.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxCount.Location = new System.Drawing.Point(65, 28);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.ReadOnly = true;
            this.textBoxCount.Size = new System.Drawing.Size(32, 19);
            this.textBoxCount.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "件";
            // 
            // textDispM
            // 
            this.textDispM.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispM.Location = new System.Drawing.Point(51, 5);
            this.textDispM.Name = "textDispM";
            this.textDispM.ReadOnly = true;
            this.textDispM.Size = new System.Drawing.Size(25, 19);
            this.textDispM.TabIndex = 4;
            // 
            // textDispY
            // 
            this.textDispY.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispY.Location = new System.Drawing.Point(6, 5);
            this.textDispY.Name = "textDispY";
            this.textDispY.ReadOnly = true;
            this.textDispY.Size = new System.Drawing.Size(25, 19);
            this.textDispY.TabIndex = 2;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelAddInput);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelStatusOCR);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(594, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Padding = new System.Windows.Forms.Padding(5);
            this.panelRight.Size = new System.Drawing.Size(750, 722);
            this.panelRight.TabIndex = 0;
            // 
            // panelAddInput
            // 
            this.panelAddInput.AutoScroll = true;
            this.panelAddInput.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelAddInput.Controls.Add(this.labelDrName);
            this.panelAddInput.Controls.Add(this.verifyBoxDrName);
            this.panelAddInput.Controls.Add(this.verifyBoxPartial);
            this.panelAddInput.Controls.Add(this.labelPartial);
            this.panelAddInput.Controls.Add(this.verifyBoxCharge);
            this.panelAddInput.Controls.Add(this.labelCharge);
            this.panelAddInput.Controls.Add(this.verifyBoxTotal);
            this.panelAddInput.Controls.Add(this.labelTotal);
            this.panelAddInput.Controls.Add(this.verifyBoxCountedDays);
            this.panelAddInput.Controls.Add(this.labelCountedDays);
            this.panelAddInput.Controls.Add(this.verifyBoxHnum);
            this.panelAddInput.Controls.Add(this.labelHnum);
            this.panelAddInput.Controls.Add(this.verifyBoxMediM);
            this.panelAddInput.Controls.Add(this.verifyBoxMediY);
            this.panelAddInput.Controls.Add(this.labelMediY);
            this.panelAddInput.Controls.Add(this.labelMediM);
            this.panelAddInput.Controls.Add(this.labelMediYM);
            this.panelAddInput.Controls.Add(this.verifyBoxBD);
            this.panelAddInput.Controls.Add(this.verifyBoxBM);
            this.panelAddInput.Controls.Add(this.verifyBoxBY);
            this.panelAddInput.Controls.Add(this.verifyBoxBE);
            this.panelAddInput.Controls.Add(this.labelBirthdayGengo);
            this.panelAddInput.Controls.Add(this.labelBirthdayDate);
            this.panelAddInput.Controls.Add(this.labelBirthdayYear);
            this.panelAddInput.Controls.Add(this.labelBirthdayMonth);
            this.panelAddInput.Controls.Add(this.labelBirthday);
            this.panelAddInput.Controls.Add(this.label5);
            this.panelAddInput.Controls.Add(this.labelHnameCopy);
            this.panelAddInput.Controls.Add(this.verifyBoxHname);
            this.panelAddInput.Controls.Add(this.buttonZipSearch);
            this.panelAddInput.Controls.Add(this.labelHname);
            this.panelAddInput.Controls.Add(this.labelPaymentcode);
            this.panelAddInput.Controls.Add(this.verifyBoxPname);
            this.panelAddInput.Controls.Add(this.verifyBoxPaymentcode);
            this.panelAddInput.Controls.Add(this.labelPname);
            this.panelAddInput.Controls.Add(this.labelFvisittimes);
            this.panelAddInput.Controls.Add(this.verifyBoxHzip);
            this.panelAddInput.Controls.Add(this.verifyBoxFvisittimes);
            this.panelAddInput.Controls.Add(this.labelHzip);
            this.panelAddInput.Controls.Add(this.labelSname);
            this.panelAddInput.Controls.Add(this.verifyBoxHaddress);
            this.panelAddInput.Controls.Add(this.verifyBoxSname);
            this.panelAddInput.Controls.Add(this.labelHaddress);
            this.panelAddInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddInput.Location = new System.Drawing.Point(5, 405);
            this.panelAddInput.Name = "panelAddInput";
            this.panelAddInput.Size = new System.Drawing.Size(740, 282);
            this.panelAddInput.TabIndex = 0;
            // 
            // labelDrName
            // 
            this.labelDrName.Location = new System.Drawing.Point(15, 410);
            this.labelDrName.Name = "labelDrName";
            this.labelDrName.Size = new System.Drawing.Size(90, 12);
            this.labelDrName.TabIndex = 37;
            this.labelDrName.Text = "---";
            this.labelDrName.Visible = false;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(111, 403);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxDrName.TabIndex = 38;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Visible = false;
            this.verifyBoxDrName.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxDrName.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // verifyBoxPartial
            // 
            this.verifyBoxPartial.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPartial.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPartial.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxPartial.Location = new System.Drawing.Point(350, 303);
            this.verifyBoxPartial.MaxLength = 9;
            this.verifyBoxPartial.Name = "verifyBoxPartial";
            this.verifyBoxPartial.NewLine = false;
            this.verifyBoxPartial.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxPartial.TabIndex = 32;
            this.verifyBoxPartial.TextV = "";
            this.verifyBoxPartial.Visible = false;
            this.verifyBoxPartial.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxPartial.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelPartial
            // 
            this.labelPartial.Location = new System.Drawing.Point(254, 310);
            this.labelPartial.Name = "labelPartial";
            this.labelPartial.Size = new System.Drawing.Size(90, 12);
            this.labelPartial.TabIndex = 31;
            this.labelPartial.Text = "---";
            this.labelPartial.Visible = false;
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxCharge.Location = new System.Drawing.Point(606, 303);
            this.verifyBoxCharge.MaxLength = 9;
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxCharge.TabIndex = 34;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Visible = false;
            this.verifyBoxCharge.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxCharge.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelCharge
            // 
            this.labelCharge.Location = new System.Drawing.Point(510, 310);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(90, 12);
            this.labelCharge.TabIndex = 33;
            this.labelCharge.Text = "---";
            this.labelCharge.Visible = false;
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxTotal.Location = new System.Drawing.Point(111, 303);
            this.verifyBoxTotal.MaxLength = 9;
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxTotal.TabIndex = 30;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Visible = false;
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxTotal.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelTotal
            // 
            this.labelTotal.Location = new System.Drawing.Point(15, 310);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(90, 12);
            this.labelTotal.TabIndex = 29;
            this.labelTotal.Text = "---";
            this.labelTotal.Visible = false;
            // 
            // verifyBoxCountedDays
            // 
            this.verifyBoxCountedDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCountedDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCountedDays.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxCountedDays.Location = new System.Drawing.Point(111, 256);
            this.verifyBoxCountedDays.MaxLength = 2;
            this.verifyBoxCountedDays.Name = "verifyBoxCountedDays";
            this.verifyBoxCountedDays.NewLine = false;
            this.verifyBoxCountedDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxCountedDays.TabIndex = 28;
            this.verifyBoxCountedDays.TextV = "";
            this.verifyBoxCountedDays.Visible = false;
            this.verifyBoxCountedDays.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxCountedDays.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelCountedDays
            // 
            this.labelCountedDays.Location = new System.Drawing.Point(15, 263);
            this.labelCountedDays.Name = "labelCountedDays";
            this.labelCountedDays.Size = new System.Drawing.Size(90, 12);
            this.labelCountedDays.TabIndex = 27;
            this.labelCountedDays.Text = "---";
            this.labelCountedDays.Visible = false;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxHnum.Location = new System.Drawing.Point(390, 37);
            this.verifyBoxHnum.MaxLength = 10;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(89, 23);
            this.verifyBoxHnum.TabIndex = 7;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Visible = false;
            this.verifyBoxHnum.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxHnum.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelHnum
            // 
            this.labelHnum.Location = new System.Drawing.Point(294, 44);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(90, 12);
            this.labelHnum.TabIndex = 6;
            this.labelHnum.Text = "---";
            this.labelHnum.Visible = false;
            // 
            // verifyBoxMediM
            // 
            this.verifyBoxMediM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMediM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMediM.Location = new System.Drawing.Point(154, 37);
            this.verifyBoxMediM.MaxLength = 2;
            this.verifyBoxMediM.Name = "verifyBoxMediM";
            this.verifyBoxMediM.NewLine = false;
            this.verifyBoxMediM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxMediM.TabIndex = 4;
            this.verifyBoxMediM.TextV = "";
            this.verifyBoxMediM.Visible = false;
            this.verifyBoxMediM.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxMediM.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // verifyBoxMediY
            // 
            this.verifyBoxMediY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMediY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMediY.Location = new System.Drawing.Point(111, 37);
            this.verifyBoxMediY.MaxLength = 2;
            this.verifyBoxMediY.Name = "verifyBoxMediY";
            this.verifyBoxMediY.NewLine = false;
            this.verifyBoxMediY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxMediY.TabIndex = 2;
            this.verifyBoxMediY.TextV = "";
            this.verifyBoxMediY.Visible = false;
            this.verifyBoxMediY.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxMediY.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelMediY
            // 
            this.labelMediY.AutoSize = true;
            this.labelMediY.Location = new System.Drawing.Point(137, 47);
            this.labelMediY.Name = "labelMediY";
            this.labelMediY.Size = new System.Drawing.Size(17, 12);
            this.labelMediY.TabIndex = 3;
            this.labelMediY.Text = "年";
            this.labelMediY.Visible = false;
            // 
            // labelMediM
            // 
            this.labelMediM.AutoSize = true;
            this.labelMediM.Location = new System.Drawing.Point(180, 47);
            this.labelMediM.Name = "labelMediM";
            this.labelMediM.Size = new System.Drawing.Size(17, 12);
            this.labelMediM.TabIndex = 5;
            this.labelMediM.Text = "月";
            this.labelMediM.Visible = false;
            // 
            // labelMediYM
            // 
            this.labelMediYM.Location = new System.Drawing.Point(15, 44);
            this.labelMediYM.Name = "labelMediYM";
            this.labelMediYM.Size = new System.Drawing.Size(90, 12);
            this.labelMediYM.TabIndex = 1;
            this.labelMediYM.Text = "---";
            this.labelMediYM.Visible = false;
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(248, 138);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 20;
            this.verifyBoxBD.TextV = "";
            this.verifyBoxBD.Visible = false;
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(205, 138);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 18;
            this.verifyBoxBM.TextV = "";
            this.verifyBoxBM.Visible = false;
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(162, 138);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 16;
            this.verifyBoxBY.TextV = "";
            this.verifyBoxBY.Visible = false;
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(111, 138);
            this.verifyBoxBE.MaxLength = 1;
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 14;
            this.verifyBoxBE.TextV = "";
            this.verifyBoxBE.Visible = false;
            // 
            // labelBirthdayGengo
            // 
            this.labelBirthdayGengo.AutoSize = true;
            this.labelBirthdayGengo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelBirthdayGengo.Location = new System.Drawing.Point(137, 139);
            this.labelBirthdayGengo.Name = "labelBirthdayGengo";
            this.labelBirthdayGengo.Size = new System.Drawing.Size(25, 24);
            this.labelBirthdayGengo.TabIndex = 15;
            this.labelBirthdayGengo.Text = "昭:3\r\n平:4";
            this.labelBirthdayGengo.Visible = false;
            // 
            // labelBirthdayDate
            // 
            this.labelBirthdayDate.AutoSize = true;
            this.labelBirthdayDate.Location = new System.Drawing.Point(274, 148);
            this.labelBirthdayDate.Name = "labelBirthdayDate";
            this.labelBirthdayDate.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayDate.TabIndex = 21;
            this.labelBirthdayDate.Text = "日";
            this.labelBirthdayDate.Visible = false;
            // 
            // labelBirthdayYear
            // 
            this.labelBirthdayYear.AutoSize = true;
            this.labelBirthdayYear.Location = new System.Drawing.Point(188, 148);
            this.labelBirthdayYear.Name = "labelBirthdayYear";
            this.labelBirthdayYear.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayYear.TabIndex = 17;
            this.labelBirthdayYear.Text = "年";
            this.labelBirthdayYear.Visible = false;
            // 
            // labelBirthdayMonth
            // 
            this.labelBirthdayMonth.AutoSize = true;
            this.labelBirthdayMonth.Location = new System.Drawing.Point(231, 148);
            this.labelBirthdayMonth.Name = "labelBirthdayMonth";
            this.labelBirthdayMonth.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayMonth.TabIndex = 19;
            this.labelBirthdayMonth.Text = "月";
            this.labelBirthdayMonth.Visible = false;
            // 
            // labelBirthday
            // 
            this.labelBirthday.Location = new System.Drawing.Point(15, 145);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(90, 12);
            this.labelBirthday.TabIndex = 13;
            this.labelBirthday.Text = "---";
            this.labelBirthday.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "入力項目";
            // 
            // labelHnameCopy
            // 
            this.labelHnameCopy.AutoSize = true;
            this.labelHnameCopy.Location = new System.Drawing.Point(540, 91);
            this.labelHnameCopy.Name = "labelHnameCopy";
            this.labelHnameCopy.Size = new System.Drawing.Size(173, 12);
            this.labelHnameCopy.TabIndex = 12;
            this.labelHnameCopy.Text = "Ctrl+Enter （被保険者氏名 貼付）";
            this.labelHnameCopy.Visible = false;
            // 
            // verifyBoxHname
            // 
            this.verifyBoxHname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHname.Location = new System.Drawing.Point(111, 85);
            this.verifyBoxHname.Name = "verifyBoxHname";
            this.verifyBoxHname.NewLine = false;
            this.verifyBoxHname.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxHname.TabIndex = 9;
            this.verifyBoxHname.TextV = "";
            this.verifyBoxHname.Visible = false;
            this.verifyBoxHname.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxHname.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // buttonZipSearch
            // 
            this.buttonZipSearch.Location = new System.Drawing.Point(15, 211);
            this.buttonZipSearch.Name = "buttonZipSearch";
            this.buttonZipSearch.Size = new System.Drawing.Size(57, 33);
            this.buttonZipSearch.TabIndex = 26;
            this.buttonZipSearch.TabStop = false;
            this.buttonZipSearch.Text = "住所帳\r\n（F5）";
            this.buttonZipSearch.UseVisualStyleBackColor = true;
            this.buttonZipSearch.Visible = false;
            this.buttonZipSearch.Click += new System.EventHandler(this.buttonZipSearch_Click);
            // 
            // labelHname
            // 
            this.labelHname.Location = new System.Drawing.Point(15, 92);
            this.labelHname.Name = "labelHname";
            this.labelHname.Size = new System.Drawing.Size(90, 12);
            this.labelHname.TabIndex = 8;
            this.labelHname.Text = "---";
            this.labelHname.Visible = false;
            // 
            // labelPaymentcode
            // 
            this.labelPaymentcode.Location = new System.Drawing.Point(170, 458);
            this.labelPaymentcode.Name = "labelPaymentcode";
            this.labelPaymentcode.Size = new System.Drawing.Size(90, 12);
            this.labelPaymentcode.TabIndex = 43;
            this.labelPaymentcode.Text = "---";
            this.labelPaymentcode.Visible = false;
            // 
            // verifyBoxPname
            // 
            this.verifyBoxPname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPname.Location = new System.Drawing.Point(390, 85);
            this.verifyBoxPname.Name = "verifyBoxPname";
            this.verifyBoxPname.NewLine = false;
            this.verifyBoxPname.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxPname.TabIndex = 11;
            this.verifyBoxPname.TextV = "";
            this.verifyBoxPname.Visible = false;
            this.verifyBoxPname.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxPname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxPname_KeyPress);
            this.verifyBoxPname.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // verifyBoxPaymentcode
            // 
            this.verifyBoxPaymentcode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPaymentcode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPaymentcode.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxPaymentcode.Location = new System.Drawing.Point(266, 451);
            this.verifyBoxPaymentcode.MaxLength = 8;
            this.verifyBoxPaymentcode.Name = "verifyBoxPaymentcode";
            this.verifyBoxPaymentcode.NewLine = false;
            this.verifyBoxPaymentcode.Size = new System.Drawing.Size(75, 23);
            this.verifyBoxPaymentcode.TabIndex = 44;
            this.verifyBoxPaymentcode.TextV = "";
            this.verifyBoxPaymentcode.Visible = false;
            this.verifyBoxPaymentcode.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxPaymentcode.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelPname
            // 
            this.labelPname.Location = new System.Drawing.Point(294, 92);
            this.labelPname.Name = "labelPname";
            this.labelPname.Size = new System.Drawing.Size(90, 12);
            this.labelPname.TabIndex = 10;
            this.labelPname.Text = "---";
            this.labelPname.Visible = false;
            // 
            // labelFvisittimes
            // 
            this.labelFvisittimes.Location = new System.Drawing.Point(15, 458);
            this.labelFvisittimes.Name = "labelFvisittimes";
            this.labelFvisittimes.Size = new System.Drawing.Size(90, 12);
            this.labelFvisittimes.TabIndex = 41;
            this.labelFvisittimes.Text = "---";
            this.labelFvisittimes.Visible = false;
            // 
            // verifyBoxHzip
            // 
            this.verifyBoxHzip.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHzip.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHzip.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxHzip.Location = new System.Drawing.Point(111, 187);
            this.verifyBoxHzip.MaxLength = 7;
            this.verifyBoxHzip.Name = "verifyBoxHzip";
            this.verifyBoxHzip.NewLine = false;
            this.verifyBoxHzip.Size = new System.Drawing.Size(63, 23);
            this.verifyBoxHzip.TabIndex = 23;
            this.verifyBoxHzip.TextV = "";
            this.verifyBoxHzip.Visible = false;
            this.verifyBoxHzip.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxHzip.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxHzip_KeyPress);
            this.verifyBoxHzip.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // verifyBoxFvisittimes
            // 
            this.verifyBoxFvisittimes.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFvisittimes.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFvisittimes.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxFvisittimes.Location = new System.Drawing.Point(111, 451);
            this.verifyBoxFvisittimes.MaxLength = 3;
            this.verifyBoxFvisittimes.Name = "verifyBoxFvisittimes";
            this.verifyBoxFvisittimes.NewLine = false;
            this.verifyBoxFvisittimes.Size = new System.Drawing.Size(31, 23);
            this.verifyBoxFvisittimes.TabIndex = 42;
            this.verifyBoxFvisittimes.TextV = "";
            this.verifyBoxFvisittimes.Visible = false;
            this.verifyBoxFvisittimes.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxFvisittimes.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelHzip
            // 
            this.labelHzip.Location = new System.Drawing.Point(15, 194);
            this.labelHzip.Name = "labelHzip";
            this.labelHzip.Size = new System.Drawing.Size(90, 12);
            this.labelHzip.TabIndex = 22;
            this.labelHzip.Text = "---";
            this.labelHzip.Visible = false;
            // 
            // labelSname
            // 
            this.labelSname.Location = new System.Drawing.Point(15, 359);
            this.labelSname.Name = "labelSname";
            this.labelSname.Size = new System.Drawing.Size(90, 12);
            this.labelSname.TabIndex = 35;
            this.labelSname.Text = "---";
            this.labelSname.Visible = false;
            // 
            // verifyBoxHaddress
            // 
            this.verifyBoxHaddress.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHaddress.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHaddress.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHaddress.Location = new System.Drawing.Point(310, 187);
            this.verifyBoxHaddress.Multiline = true;
            this.verifyBoxHaddress.Name = "verifyBoxHaddress";
            this.verifyBoxHaddress.NewLine = false;
            this.verifyBoxHaddress.Size = new System.Drawing.Size(403, 40);
            this.verifyBoxHaddress.TabIndex = 25;
            this.verifyBoxHaddress.TextV = "";
            this.verifyBoxHaddress.Visible = false;
            this.verifyBoxHaddress.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxHaddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxHaddress_KeyPress);
            this.verifyBoxHaddress.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // verifyBoxSname
            // 
            this.verifyBoxSname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxSname.Location = new System.Drawing.Point(111, 352);
            this.verifyBoxSname.Name = "verifyBoxSname";
            this.verifyBoxSname.NewLine = false;
            this.verifyBoxSname.Size = new System.Drawing.Size(359, 23);
            this.verifyBoxSname.TabIndex = 36;
            this.verifyBoxSname.TextV = "";
            this.verifyBoxSname.Visible = false;
            this.verifyBoxSname.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxSname.Leave += new System.EventHandler(this.verifyBox_Leave);
            // 
            // labelHaddress
            // 
            this.labelHaddress.Location = new System.Drawing.Point(214, 194);
            this.labelHaddress.Name = "labelHaddress";
            this.labelHaddress.Size = new System.Drawing.Size(90, 12);
            this.labelHaddress.TabIndex = 24;
            this.labelHaddress.Text = "---";
            this.labelHaddress.Visible = false;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.scrollPictureControl1.Location = new System.Drawing.Point(5, 5);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(740, 400);
            this.scrollPictureControl1.TabIndex = 0;
            this.scrollPictureControl1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelInputerName);
            this.panel1.Controls.Add(this.labelDescription);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(5, 687);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(740, 30);
            this.panel1.TabIndex = 2;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(405, 5);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(144, 23);
            this.labelInputerName.TabIndex = 58;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(38, 10);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(133, 12);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "ここに入力欄の説明を記述";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "説明：";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(551, 5);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 3;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelStatusOCR
            // 
            this.labelStatusOCR.AutoSize = true;
            this.labelStatusOCR.Location = new System.Drawing.Point(19, 708);
            this.labelStatusOCR.Name = "labelStatusOCR";
            this.labelStatusOCR.Size = new System.Drawing.Size(0, 12);
            this.labelStatusOCR.TabIndex = 23;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Location = new System.Drawing.Point(0, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // MediveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.splitter2);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "MediveryForm";
            this.Text = "追加項目入力（Medi-very）";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MediveryForm_FormClosed);
            this.Shown += new System.EventHandler(this.MediveryForm_Shown);
            this.SizeChanged += new System.EventHandler(this.MediveryForm_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MediveryForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MediveryForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelDatalist.ResumeLayout(false);
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelAddInput.ResumeLayout(false);
            this.panelAddInput.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TextBox textDispM;
        private System.Windows.Forms.TextBox textDispY;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelImageName;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Label labelStatusOCR;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHname;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelHname;
        private System.Windows.Forms.Label labelHzip;
        private VerifyBox verifyBoxHzip;
        private System.Windows.Forms.Label labelPname;
        private VerifyBox verifyBoxPname;
        private System.Windows.Forms.Label labelSname;
        private VerifyBox verifyBoxSname;
        private System.Windows.Forms.Label labelHaddress;
        private VerifyBox verifyBoxHaddress;
        private System.Windows.Forms.Label labelFvisittimes;
        private VerifyBox verifyBoxFvisittimes;
        private System.Windows.Forms.Label labelPaymentcode;
        private VerifyBox verifyBoxPaymentcode;
        private System.Windows.Forms.Button buttonZipSearch;
        private System.Windows.Forms.Label labelHnameCopy;
        private System.Windows.Forms.Panel panelAddInput;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelBirthdayGengo;
        private System.Windows.Forms.Label labelBirthdayDate;
        private System.Windows.Forms.Label labelBirthdayYear;
        private System.Windows.Forms.Label labelBirthdayMonth;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label labelHnum;
        private VerifyBox verifyBoxMediM;
        private VerifyBox verifyBoxMediY;
        private System.Windows.Forms.Label labelMediY;
        private System.Windows.Forms.Label labelMediM;
        private System.Windows.Forms.Label labelMediYM;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private VerifyBox verifyBoxCountedDays;
        private System.Windows.Forms.Label labelCountedDays;
        private VerifyBox verifyBoxPartial;
        private System.Windows.Forms.Label labelPartial;
        private System.Windows.Forms.Label labelDrName;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label labelInputerName;
    }
}