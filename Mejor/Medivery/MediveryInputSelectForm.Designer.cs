﻿namespace Mejor.Medivery
{
    partial class MediveryInputSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonFirstTime = new System.Windows.Forms.Button();
            this.buttonVerify = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelCompletePercent = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelAllCount = new System.Windows.Forms.Label();
            this.labelCompleteCount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelNoinputCount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelNoinputCountVerify = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCompleteCountVerify = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAllCountVerify = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelCompletePercentVerify = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelInputEnableCountVerify = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonFirstTime
            // 
            this.buttonFirstTime.Location = new System.Drawing.Point(15, 12);
            this.buttonFirstTime.Name = "buttonFirstTime";
            this.buttonFirstTime.Size = new System.Drawing.Size(130, 44);
            this.buttonFirstTime.TabIndex = 0;
            this.buttonFirstTime.Text = "１回目";
            this.buttonFirstTime.UseVisualStyleBackColor = true;
            this.buttonFirstTime.Click += new System.EventHandler(this.buttonFirstTime_Click);
            // 
            // buttonVerify
            // 
            this.buttonVerify.Location = new System.Drawing.Point(164, 12);
            this.buttonVerify.Name = "buttonVerify";
            this.buttonVerify.Size = new System.Drawing.Size(130, 44);
            this.buttonVerify.TabIndex = 1;
            this.buttonVerify.Text = "ベリファイ";
            this.buttonVerify.UseVisualStyleBackColor = true;
            this.buttonVerify.Click += new System.EventHandler(this.buttonVerify_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(154, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(2, 120);
            this.label1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "進捗度：";
            // 
            // labelCompletePercent
            // 
            this.labelCompletePercent.AutoSize = true;
            this.labelCompletePercent.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelCompletePercent.Location = new System.Drawing.Point(78, 69);
            this.labelCompletePercent.Name = "labelCompletePercent";
            this.labelCompletePercent.Size = new System.Drawing.Size(0, 16);
            this.labelCompletePercent.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "全体件数：";
            // 
            // labelAllCount
            // 
            this.labelAllCount.Location = new System.Drawing.Point(91, 105);
            this.labelAllCount.Name = "labelAllCount";
            this.labelAllCount.Size = new System.Drawing.Size(41, 12);
            this.labelAllCount.TabIndex = 6;
            this.labelAllCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCompleteCount
            // 
            this.labelCompleteCount.Location = new System.Drawing.Point(91, 128);
            this.labelCompleteCount.Name = "labelCompleteCount";
            this.labelCompleteCount.Size = new System.Drawing.Size(41, 12);
            this.labelCompleteCount.TabIndex = 8;
            this.labelCompleteCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "完了：";
            // 
            // labelNoinputCount
            // 
            this.labelNoinputCount.Location = new System.Drawing.Point(91, 151);
            this.labelNoinputCount.Name = "labelNoinputCount";
            this.labelNoinputCount.Size = new System.Drawing.Size(41, 12);
            this.labelNoinputCount.TabIndex = 10;
            this.labelNoinputCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 9;
            this.label9.Text = "未入力：";
            // 
            // labelNoinputCountVerify
            // 
            this.labelNoinputCountVerify.Location = new System.Drawing.Point(243, 151);
            this.labelNoinputCountVerify.Name = "labelNoinputCountVerify";
            this.labelNoinputCountVerify.Size = new System.Drawing.Size(41, 12);
            this.labelNoinputCountVerify.TabIndex = 18;
            this.labelNoinputCountVerify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(177, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "未入力：";
            // 
            // labelCompleteCountVerify
            // 
            this.labelCompleteCountVerify.Location = new System.Drawing.Point(243, 128);
            this.labelCompleteCountVerify.Name = "labelCompleteCountVerify";
            this.labelCompleteCountVerify.Size = new System.Drawing.Size(41, 12);
            this.labelCompleteCountVerify.TabIndex = 16;
            this.labelCompleteCountVerify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(177, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "完了：";
            // 
            // labelAllCountVerify
            // 
            this.labelAllCountVerify.Location = new System.Drawing.Point(243, 105);
            this.labelAllCountVerify.Name = "labelAllCountVerify";
            this.labelAllCountVerify.Size = new System.Drawing.Size(41, 12);
            this.labelAllCountVerify.TabIndex = 14;
            this.labelAllCountVerify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(177, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "全体件数：";
            // 
            // labelCompletePercentVerify
            // 
            this.labelCompletePercentVerify.AutoSize = true;
            this.labelCompletePercentVerify.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelCompletePercentVerify.Location = new System.Drawing.Point(230, 69);
            this.labelCompletePercentVerify.Name = "labelCompletePercentVerify";
            this.labelCompletePercentVerify.Size = new System.Drawing.Size(0, 16);
            this.labelCompletePercentVerify.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(177, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 12);
            this.label13.TabIndex = 11;
            this.label13.Text = "進捗度：";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(222, 205);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 19;
            this.buttonCancel.Text = "閉じる";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelInputEnableCountVerify
            // 
            this.labelInputEnableCountVerify.Location = new System.Drawing.Point(243, 173);
            this.labelInputEnableCountVerify.Name = "labelInputEnableCountVerify";
            this.labelInputEnableCountVerify.Size = new System.Drawing.Size(41, 12);
            this.labelInputEnableCountVerify.TabIndex = 21;
            this.labelInputEnableCountVerify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 20;
            this.label6.Text = "（入力可能：";
            // 
            // MediveryInputSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 235);
            this.Controls.Add(this.labelInputEnableCountVerify);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelNoinputCountVerify);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelCompleteCountVerify);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelAllCountVerify);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labelCompletePercentVerify);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.labelNoinputCount);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelCompleteCount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelAllCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelCompletePercent);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonVerify);
            this.Controls.Add(this.buttonFirstTime);
            this.Name = "MediveryInputSelectForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "入力モードを選択してください";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFirstTime;
        private System.Windows.Forms.Button buttonVerify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelCompletePercent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelAllCount;
        private System.Windows.Forms.Label labelCompleteCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelNoinputCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelNoinputCountVerify;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelCompleteCountVerify;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAllCountVerify;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelCompletePercentVerify;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelInputEnableCountVerify;
        private System.Windows.Forms.Label label6;
    }
}