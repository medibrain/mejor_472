﻿using System;

namespace Mejor.Medivery
{
    partial class MediveryXMLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelAddInput = new System.Windows.Forms.Panel();
            this.lblDrCode01 = new System.Windows.Forms.Label();
            this.labelDrNum = new System.Windows.Forms.Label();
            this.verifyBoxDrNum = new Mejor.VerifyBox();
            this.labelAccname = new System.Windows.Forms.Label();
            this.verifyBoxAccName = new Mejor.VerifyBox();
            this.verifyBoxPsex = new Mejor.VerifyBox();
            this.labelPsex = new System.Windows.Forms.Label();
            this.btnSelectlist = new System.Windows.Forms.Button();
            this.labelDrName = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.verifyBoxPartial = new Mejor.VerifyBox();
            this.labelPartial = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.labelCharge = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxDates = new Mejor.VerifyBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.verifyBoxCountedDays = new Mejor.VerifyBox();
            this.labelCountedDays = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.labelHnum = new System.Windows.Forms.Label();
            this.verifyBoxMediM = new Mejor.VerifyBox();
            this.verifyBoxMediY = new Mejor.VerifyBox();
            this.labelMediY = new System.Windows.Forms.Label();
            this.labelMediM = new System.Windows.Forms.Label();
            this.labelMediYM = new System.Windows.Forms.Label();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.labelBirthdayGengo = new System.Windows.Forms.Label();
            this.labelBirthdayDate = new System.Windows.Forms.Label();
            this.labelBirthdayYear = new System.Windows.Forms.Label();
            this.labelBirthdayMonth = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelHnameCopy = new System.Windows.Forms.Label();
            this.verifyBoxHname = new Mejor.VerifyBox();
            this.buttonZipSearch = new System.Windows.Forms.Button();
            this.labelHname = new System.Windows.Forms.Label();
            this.labelPaymentcode = new System.Windows.Forms.Label();
            this.verifyBoxPname = new Mejor.VerifyBox();
            this.verifyBoxPaymentcode = new Mejor.VerifyBox();
            this.labelPname = new System.Windows.Forms.Label();
            this.labelFvisittimes = new System.Windows.Forms.Label();
            this.verifyBoxHzip = new Mejor.VerifyBox();
            this.verifyBoxFvisittimes = new Mejor.VerifyBox();
            this.labelHzip = new System.Windows.Forms.Label();
            this.labelSname = new System.Windows.Forms.Label();
            this.verifyBoxHaddress = new Mejor.VerifyBox();
            this.verifyBoxSname = new Mejor.VerifyBox();
            this.labelHaddress = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelDescription = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelAddInput.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(692, 3);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 1;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(120, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(107, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-193, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 721);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelAddInput);
            this.panelRight.Controls.Add(this.splitter3);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 0;
            // 
            // panelAddInput
            // 
            this.panelAddInput.AutoScroll = true;
            this.panelAddInput.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelAddInput.Controls.Add(this.lblDrCode01);
            this.panelAddInput.Controls.Add(this.labelDrNum);
            this.panelAddInput.Controls.Add(this.verifyBoxDrNum);
            this.panelAddInput.Controls.Add(this.labelAccname);
            this.panelAddInput.Controls.Add(this.verifyBoxAccName);
            this.panelAddInput.Controls.Add(this.verifyBoxPsex);
            this.panelAddInput.Controls.Add(this.labelPsex);
            this.panelAddInput.Controls.Add(this.btnSelectlist);
            this.panelAddInput.Controls.Add(this.labelDrName);
            this.panelAddInput.Controls.Add(this.verifyBoxDrName);
            this.panelAddInput.Controls.Add(this.verifyBoxPartial);
            this.panelAddInput.Controls.Add(this.labelPartial);
            this.panelAddInput.Controls.Add(this.verifyBoxCharge);
            this.panelAddInput.Controls.Add(this.labelCharge);
            this.panelAddInput.Controls.Add(this.verifyBoxTotal);
            this.panelAddInput.Controls.Add(this.labelTotal);
            this.panelAddInput.Controls.Add(this.verifyBoxDates);
            this.panelAddInput.Controls.Add(this.labelDate);
            this.panelAddInput.Controls.Add(this.verifyBoxCountedDays);
            this.panelAddInput.Controls.Add(this.labelCountedDays);
            this.panelAddInput.Controls.Add(this.verifyBoxHnum);
            this.panelAddInput.Controls.Add(this.labelHnum);
            this.panelAddInput.Controls.Add(this.verifyBoxMediM);
            this.panelAddInput.Controls.Add(this.verifyBoxMediY);
            this.panelAddInput.Controls.Add(this.labelMediY);
            this.panelAddInput.Controls.Add(this.labelMediM);
            this.panelAddInput.Controls.Add(this.labelMediYM);
            this.panelAddInput.Controls.Add(this.verifyBoxBD);
            this.panelAddInput.Controls.Add(this.verifyBoxBM);
            this.panelAddInput.Controls.Add(this.verifyBoxBY);
            this.panelAddInput.Controls.Add(this.verifyBoxBE);
            this.panelAddInput.Controls.Add(this.labelBirthdayGengo);
            this.panelAddInput.Controls.Add(this.labelBirthdayDate);
            this.panelAddInput.Controls.Add(this.labelBirthdayYear);
            this.panelAddInput.Controls.Add(this.labelBirthdayMonth);
            this.panelAddInput.Controls.Add(this.labelBirthday);
            this.panelAddInput.Controls.Add(this.labelHnameCopy);
            this.panelAddInput.Controls.Add(this.verifyBoxHname);
            this.panelAddInput.Controls.Add(this.buttonZipSearch);
            this.panelAddInput.Controls.Add(this.labelHname);
            this.panelAddInput.Controls.Add(this.labelPaymentcode);
            this.panelAddInput.Controls.Add(this.verifyBoxPname);
            this.panelAddInput.Controls.Add(this.verifyBoxPaymentcode);
            this.panelAddInput.Controls.Add(this.labelPname);
            this.panelAddInput.Controls.Add(this.labelFvisittimes);
            this.panelAddInput.Controls.Add(this.verifyBoxHzip);
            this.panelAddInput.Controls.Add(this.verifyBoxFvisittimes);
            this.panelAddInput.Controls.Add(this.labelHzip);
            this.panelAddInput.Controls.Add(this.labelSname);
            this.panelAddInput.Controls.Add(this.verifyBoxHaddress);
            this.panelAddInput.Controls.Add(this.verifyBoxSname);
            this.panelAddInput.Controls.Add(this.labelHaddress);
            this.panelAddInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddInput.Location = new System.Drawing.Point(0, 499);
            this.panelAddInput.Name = "panelAddInput";
            this.panelAddInput.Size = new System.Drawing.Size(1020, 192);
            this.panelAddInput.TabIndex = 0;
            // 
            // lblDrCode01
            // 
            this.lblDrCode01.AutoSize = true;
            this.lblDrCode01.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblDrCode01.Location = new System.Drawing.Point(963, 238);
            this.lblDrCode01.Name = "lblDrCode01";
            this.lblDrCode01.Size = new System.Drawing.Size(37, 24);
            this.lblDrCode01.TabIndex = 50;
            this.lblDrCode01.Text = "[協]: 0\r\n[契]: 1";
            this.lblDrCode01.Visible = false;
            // 
            // labelDrNum
            // 
            this.labelDrNum.Location = new System.Drawing.Point(726, 246);
            this.labelDrNum.Name = "labelDrNum";
            this.labelDrNum.Size = new System.Drawing.Size(90, 12);
            this.labelDrNum.TabIndex = 48;
            this.labelDrNum.Text = "登録記号番号";
            this.labelDrNum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDrNum.Visible = false;
            // 
            // verifyBoxDrNum
            // 
            this.verifyBoxDrNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxDrNum.Location = new System.Drawing.Point(816, 238);
            this.verifyBoxDrNum.Name = "verifyBoxDrNum";
            this.verifyBoxDrNum.NewLine = false;
            this.verifyBoxDrNum.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxDrNum.TabIndex = 49;
            this.verifyBoxDrNum.TextV = "";
            this.verifyBoxDrNum.Visible = false;
            // 
            // labelAccname
            // 
            this.labelAccname.Location = new System.Drawing.Point(360, 291);
            this.labelAccname.Name = "labelAccname";
            this.labelAccname.Size = new System.Drawing.Size(90, 12);
            this.labelAccname.TabIndex = 46;
            this.labelAccname.Text = "口座名義";
            this.labelAccname.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelAccname.Visible = false;
            // 
            // verifyBoxAccName
            // 
            this.verifyBoxAccName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxAccName.Location = new System.Drawing.Point(450, 283);
            this.verifyBoxAccName.Name = "verifyBoxAccName";
            this.verifyBoxAccName.NewLine = false;
            this.verifyBoxAccName.Size = new System.Drawing.Size(359, 23);
            this.verifyBoxAccName.TabIndex = 47;
            this.verifyBoxAccName.TextV = "";
            this.verifyBoxAccName.Visible = false;
            // 
            // verifyBoxPsex
            // 
            this.verifyBoxPsex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPsex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPsex.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxPsex.Location = new System.Drawing.Point(330, 47);
            this.verifyBoxPsex.MaxLength = 10;
            this.verifyBoxPsex.Name = "verifyBoxPsex";
            this.verifyBoxPsex.NewLine = false;
            this.verifyBoxPsex.Size = new System.Drawing.Size(30, 23);
            this.verifyBoxPsex.TabIndex = 11;
            this.verifyBoxPsex.TextV = "";
            this.verifyBoxPsex.Visible = false;
            // 
            // labelPsex
            // 
            this.labelPsex.Location = new System.Drawing.Point(246, 53);
            this.labelPsex.Name = "labelPsex";
            this.labelPsex.Size = new System.Drawing.Size(81, 12);
            this.labelPsex.TabIndex = 45;
            this.labelPsex.Text = "性別";
            this.labelPsex.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPsex.Visible = false;
            // 
            // btnSelectlist
            // 
            this.btnSelectlist.Location = new System.Drawing.Point(900, 2);
            this.btnSelectlist.Name = "btnSelectlist";
            this.btnSelectlist.Size = new System.Drawing.Size(100, 28);
            this.btnSelectlist.TabIndex = 44;
            this.btnSelectlist.TabStop = false;
            this.btnSelectlist.Text = "選択リスト表示(&S)";
            this.btnSelectlist.UseVisualStyleBackColor = true;
            this.btnSelectlist.Visible = false;
            this.btnSelectlist.Click += new System.EventHandler(this.btnSelectlist_Click);
            // 
            // labelDrName
            // 
            this.labelDrName.Location = new System.Drawing.Point(486, 247);
            this.labelDrName.Name = "labelDrName";
            this.labelDrName.Size = new System.Drawing.Size(80, 12);
            this.labelDrName.TabIndex = 38;
            this.labelDrName.Text = "施術師名";
            this.labelDrName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDrName.Visible = false;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(570, 240);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxDrName.TabIndex = 39;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Visible = false;
            // 
            // verifyBoxPartial
            // 
            this.verifyBoxPartial.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPartial.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPartial.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxPartial.Location = new System.Drawing.Point(338, 196);
            this.verifyBoxPartial.MaxLength = 9;
            this.verifyBoxPartial.Name = "verifyBoxPartial";
            this.verifyBoxPartial.NewLine = false;
            this.verifyBoxPartial.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxPartial.TabIndex = 33;
            this.verifyBoxPartial.TextV = "";
            this.verifyBoxPartial.Visible = false;
            // 
            // labelPartial
            // 
            this.labelPartial.Location = new System.Drawing.Point(248, 203);
            this.labelPartial.Name = "labelPartial";
            this.labelPartial.Size = new System.Drawing.Size(90, 12);
            this.labelPartial.TabIndex = 32;
            this.labelPartial.Text = "一部負担";
            this.labelPartial.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPartial.Visible = false;
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxCharge.Location = new System.Drawing.Point(594, 196);
            this.verifyBoxCharge.MaxLength = 9;
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxCharge.TabIndex = 35;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Visible = false;
            // 
            // labelCharge
            // 
            this.labelCharge.Location = new System.Drawing.Point(504, 203);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(90, 12);
            this.labelCharge.TabIndex = 34;
            this.labelCharge.Text = "請求";
            this.labelCharge.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelCharge.Visible = false;
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxTotal.Location = new System.Drawing.Point(99, 196);
            this.verifyBoxTotal.MaxLength = 9;
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxTotal.TabIndex = 31;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Visible = false;
            // 
            // labelTotal
            // 
            this.labelTotal.Location = new System.Drawing.Point(9, 203);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(90, 12);
            this.labelTotal.TabIndex = 30;
            this.labelTotal.Text = "合計";
            this.labelTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelTotal.Visible = false;
            // 
            // verifyBoxDates
            // 
            this.verifyBoxDates.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDates.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDates.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDates.Location = new System.Drawing.Point(221, 154);
            this.verifyBoxDates.MaxLength = 90;
            this.verifyBoxDates.Name = "verifyBoxDates";
            this.verifyBoxDates.NewLine = false;
            this.verifyBoxDates.Size = new System.Drawing.Size(512, 23);
            this.verifyBoxDates.TabIndex = 29;
            this.verifyBoxDates.TextV = "";
            this.verifyBoxDates.Visible = false;
            // 
            // labelDate
            // 
            this.labelDate.Location = new System.Drawing.Point(131, 161);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(90, 12);
            this.labelDate.TabIndex = 28;
            this.labelDate.Text = "診療日";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDate.Visible = false;
            // 
            // verifyBoxCountedDays
            // 
            this.verifyBoxCountedDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCountedDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCountedDays.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxCountedDays.Location = new System.Drawing.Point(99, 154);
            this.verifyBoxCountedDays.MaxLength = 2;
            this.verifyBoxCountedDays.Name = "verifyBoxCountedDays";
            this.verifyBoxCountedDays.NewLine = false;
            this.verifyBoxCountedDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxCountedDays.TabIndex = 27;
            this.verifyBoxCountedDays.TextV = "";
            this.verifyBoxCountedDays.Visible = false;
            // 
            // labelCountedDays
            // 
            this.labelCountedDays.Location = new System.Drawing.Point(9, 161);
            this.labelCountedDays.Name = "labelCountedDays";
            this.labelCountedDays.Size = new System.Drawing.Size(90, 12);
            this.labelCountedDays.TabIndex = 26;
            this.labelCountedDays.Text = "実日数";
            this.labelCountedDays.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelCountedDays.Visible = false;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxHnum.Location = new System.Drawing.Point(289, 4);
            this.verifyBoxHnum.MaxLength = 10;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(89, 23);
            this.verifyBoxHnum.TabIndex = 6;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Visible = false;
            // 
            // labelHnum
            // 
            this.labelHnum.Location = new System.Drawing.Point(199, 11);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(90, 12);
            this.labelHnum.TabIndex = 5;
            this.labelHnum.Text = "被保記番";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelHnum.Visible = false;
            // 
            // verifyBoxMediM
            // 
            this.verifyBoxMediM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMediM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMediM.Location = new System.Drawing.Point(142, 4);
            this.verifyBoxMediM.MaxLength = 2;
            this.verifyBoxMediM.Name = "verifyBoxMediM";
            this.verifyBoxMediM.NewLine = false;
            this.verifyBoxMediM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxMediM.TabIndex = 3;
            this.verifyBoxMediM.TextV = "";
            this.verifyBoxMediM.Visible = false;
            // 
            // verifyBoxMediY
            // 
            this.verifyBoxMediY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMediY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMediY.Location = new System.Drawing.Point(99, 4);
            this.verifyBoxMediY.MaxLength = 2;
            this.verifyBoxMediY.Name = "verifyBoxMediY";
            this.verifyBoxMediY.NewLine = false;
            this.verifyBoxMediY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxMediY.TabIndex = 2;
            this.verifyBoxMediY.TextV = "";
            this.verifyBoxMediY.Visible = false;
            // 
            // labelMediY
            // 
            this.labelMediY.AutoSize = true;
            this.labelMediY.Location = new System.Drawing.Point(125, 14);
            this.labelMediY.Name = "labelMediY";
            this.labelMediY.Size = new System.Drawing.Size(17, 12);
            this.labelMediY.TabIndex = 2;
            this.labelMediY.Text = "年";
            this.labelMediY.Visible = false;
            // 
            // labelMediM
            // 
            this.labelMediM.AutoSize = true;
            this.labelMediM.Location = new System.Drawing.Point(168, 14);
            this.labelMediM.Name = "labelMediM";
            this.labelMediM.Size = new System.Drawing.Size(17, 12);
            this.labelMediM.TabIndex = 4;
            this.labelMediM.Text = "月";
            this.labelMediM.Visible = false;
            // 
            // labelMediYM
            // 
            this.labelMediYM.Location = new System.Drawing.Point(9, 11);
            this.labelMediYM.Name = "labelMediYM";
            this.labelMediYM.Size = new System.Drawing.Size(90, 12);
            this.labelMediYM.TabIndex = 0;
            this.labelMediYM.Text = "診療年月";
            this.labelMediYM.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelMediYM.Visible = false;
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(592, 47);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 19;
            this.verifyBoxBD.TextV = "";
            this.verifyBoxBD.Visible = false;
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(549, 47);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 17;
            this.verifyBoxBM.TextV = "";
            this.verifyBoxBM.Visible = false;
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(506, 47);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 15;
            this.verifyBoxBY.TextV = "";
            this.verifyBoxBY.Visible = false;
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(455, 47);
            this.verifyBoxBE.MaxLength = 1;
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 13;
            this.verifyBoxBE.TextV = "";
            this.verifyBoxBE.Visible = false;
            // 
            // labelBirthdayGengo
            // 
            this.labelBirthdayGengo.AutoSize = true;
            this.labelBirthdayGengo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelBirthdayGengo.Location = new System.Drawing.Point(481, 45);
            this.labelBirthdayGengo.Name = "labelBirthdayGengo";
            this.labelBirthdayGengo.Size = new System.Drawing.Size(25, 36);
            this.labelBirthdayGengo.TabIndex = 14;
            this.labelBirthdayGengo.Text = "昭:3\r\n平:4\r\n令:5";
            this.labelBirthdayGengo.Visible = false;
            // 
            // labelBirthdayDate
            // 
            this.labelBirthdayDate.AutoSize = true;
            this.labelBirthdayDate.Location = new System.Drawing.Point(618, 57);
            this.labelBirthdayDate.Name = "labelBirthdayDate";
            this.labelBirthdayDate.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayDate.TabIndex = 20;
            this.labelBirthdayDate.Text = "日";
            this.labelBirthdayDate.Visible = false;
            // 
            // labelBirthdayYear
            // 
            this.labelBirthdayYear.AutoSize = true;
            this.labelBirthdayYear.Location = new System.Drawing.Point(532, 57);
            this.labelBirthdayYear.Name = "labelBirthdayYear";
            this.labelBirthdayYear.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayYear.TabIndex = 16;
            this.labelBirthdayYear.Text = "年";
            this.labelBirthdayYear.Visible = false;
            // 
            // labelBirthdayMonth
            // 
            this.labelBirthdayMonth.AutoSize = true;
            this.labelBirthdayMonth.Location = new System.Drawing.Point(575, 57);
            this.labelBirthdayMonth.Name = "labelBirthdayMonth";
            this.labelBirthdayMonth.Size = new System.Drawing.Size(17, 12);
            this.labelBirthdayMonth.TabIndex = 18;
            this.labelBirthdayMonth.Text = "月";
            this.labelBirthdayMonth.Visible = false;
            // 
            // labelBirthday
            // 
            this.labelBirthday.Location = new System.Drawing.Point(365, 54);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(90, 12);
            this.labelBirthday.TabIndex = 12;
            this.labelBirthday.Text = "受療者生年月日";
            this.labelBirthday.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelBirthday.Visible = false;
            // 
            // labelHnameCopy
            // 
            this.labelHnameCopy.AutoSize = true;
            this.labelHnameCopy.Location = new System.Drawing.Point(96, 32);
            this.labelHnameCopy.Name = "labelHnameCopy";
            this.labelHnameCopy.Size = new System.Drawing.Size(173, 12);
            this.labelHnameCopy.TabIndex = 11;
            this.labelHnameCopy.Text = "Ctrl+Enter （被保険者氏名 貼付）";
            this.labelHnameCopy.Visible = false;
            // 
            // verifyBoxHname
            // 
            this.verifyBoxHname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHname.Location = new System.Drawing.Point(479, 4);
            this.verifyBoxHname.Name = "verifyBoxHname";
            this.verifyBoxHname.NewLine = false;
            this.verifyBoxHname.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxHname.TabIndex = 8;
            this.verifyBoxHname.TextV = "";
            this.verifyBoxHname.Visible = false;
            // 
            // buttonZipSearch
            // 
            this.buttonZipSearch.Location = new System.Drawing.Point(174, 90);
            this.buttonZipSearch.Name = "buttonZipSearch";
            this.buttonZipSearch.Size = new System.Drawing.Size(84, 23);
            this.buttonZipSearch.TabIndex = 25;
            this.buttonZipSearch.TabStop = false;
            this.buttonZipSearch.Text = "住所帳（F5）";
            this.buttonZipSearch.UseVisualStyleBackColor = true;
            this.buttonZipSearch.Visible = false;
            this.buttonZipSearch.Click += new System.EventHandler(this.buttonZipSearch_Click);
            // 
            // labelHname
            // 
            this.labelHname.Location = new System.Drawing.Point(389, 11);
            this.labelHname.Name = "labelHname";
            this.labelHname.Size = new System.Drawing.Size(90, 12);
            this.labelHname.TabIndex = 7;
            this.labelHname.Text = "被保険者名";
            this.labelHname.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelHname.Visible = false;
            // 
            // labelPaymentcode
            // 
            this.labelPaymentcode.Location = new System.Drawing.Point(164, 291);
            this.labelPaymentcode.Name = "labelPaymentcode";
            this.labelPaymentcode.Size = new System.Drawing.Size(90, 12);
            this.labelPaymentcode.TabIndex = 42;
            this.labelPaymentcode.Text = "支払先コード";
            this.labelPaymentcode.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPaymentcode.Visible = false;
            // 
            // verifyBoxPname
            // 
            this.verifyBoxPname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPname.Location = new System.Drawing.Point(99, 47);
            this.verifyBoxPname.Name = "verifyBoxPname";
            this.verifyBoxPname.NewLine = false;
            this.verifyBoxPname.Size = new System.Drawing.Size(143, 23);
            this.verifyBoxPname.TabIndex = 10;
            this.verifyBoxPname.TextV = "";
            this.verifyBoxPname.Visible = false;
            this.verifyBoxPname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxPname_KeyPress);
            // 
            // verifyBoxPaymentcode
            // 
            this.verifyBoxPaymentcode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPaymentcode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPaymentcode.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxPaymentcode.Location = new System.Drawing.Point(254, 284);
            this.verifyBoxPaymentcode.MaxLength = 8;
            this.verifyBoxPaymentcode.Name = "verifyBoxPaymentcode";
            this.verifyBoxPaymentcode.NewLine = false;
            this.verifyBoxPaymentcode.Size = new System.Drawing.Size(75, 23);
            this.verifyBoxPaymentcode.TabIndex = 43;
            this.verifyBoxPaymentcode.TextV = "";
            this.verifyBoxPaymentcode.Visible = false;
            // 
            // labelPname
            // 
            this.labelPname.Location = new System.Drawing.Point(9, 54);
            this.labelPname.Name = "labelPname";
            this.labelPname.Size = new System.Drawing.Size(90, 12);
            this.labelPname.TabIndex = 9;
            this.labelPname.Text = "受療者名";
            this.labelPname.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPname.Visible = false;
            // 
            // labelFvisittimes
            // 
            this.labelFvisittimes.Location = new System.Drawing.Point(9, 291);
            this.labelFvisittimes.Name = "labelFvisittimes";
            this.labelFvisittimes.Size = new System.Drawing.Size(90, 12);
            this.labelFvisittimes.TabIndex = 40;
            this.labelFvisittimes.Text = "往療回数";
            this.labelFvisittimes.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelFvisittimes.Visible = false;
            // 
            // verifyBoxHzip
            // 
            this.verifyBoxHzip.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHzip.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHzip.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxHzip.Location = new System.Drawing.Point(99, 90);
            this.verifyBoxHzip.MaxLength = 7;
            this.verifyBoxHzip.Name = "verifyBoxHzip";
            this.verifyBoxHzip.NewLine = false;
            this.verifyBoxHzip.Size = new System.Drawing.Size(71, 23);
            this.verifyBoxHzip.TabIndex = 22;
            this.verifyBoxHzip.TextV = "";
            this.verifyBoxHzip.Visible = false;
            this.verifyBoxHzip.Leave += new System.EventHandler(this.verifyBoxHzip_Leave);
            // 
            // verifyBoxFvisittimes
            // 
            this.verifyBoxFvisittimes.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFvisittimes.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFvisittimes.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxFvisittimes.Location = new System.Drawing.Point(99, 284);
            this.verifyBoxFvisittimes.MaxLength = 3;
            this.verifyBoxFvisittimes.Name = "verifyBoxFvisittimes";
            this.verifyBoxFvisittimes.NewLine = false;
            this.verifyBoxFvisittimes.Size = new System.Drawing.Size(31, 23);
            this.verifyBoxFvisittimes.TabIndex = 41;
            this.verifyBoxFvisittimes.TextV = "";
            this.verifyBoxFvisittimes.Visible = false;
            // 
            // labelHzip
            // 
            this.labelHzip.Location = new System.Drawing.Point(9, 97);
            this.labelHzip.Name = "labelHzip";
            this.labelHzip.Size = new System.Drawing.Size(90, 12);
            this.labelHzip.TabIndex = 21;
            this.labelHzip.Text = "郵便番号";
            this.labelHzip.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelHzip.Visible = false;
            // 
            // labelSname
            // 
            this.labelSname.Location = new System.Drawing.Point(9, 247);
            this.labelSname.Name = "labelSname";
            this.labelSname.Size = new System.Drawing.Size(90, 12);
            this.labelSname.TabIndex = 36;
            this.labelSname.Text = "施術所名";
            this.labelSname.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelSname.Visible = false;
            // 
            // verifyBoxHaddress
            // 
            this.verifyBoxHaddress.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHaddress.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHaddress.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHaddress.Location = new System.Drawing.Point(298, 90);
            this.verifyBoxHaddress.Multiline = true;
            this.verifyBoxHaddress.Name = "verifyBoxHaddress";
            this.verifyBoxHaddress.NewLine = false;
            this.verifyBoxHaddress.Size = new System.Drawing.Size(435, 40);
            this.verifyBoxHaddress.TabIndex = 24;
            this.verifyBoxHaddress.TextV = "";
            this.verifyBoxHaddress.Visible = false;
            // 
            // verifyBoxSname
            // 
            this.verifyBoxSname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxSname.Location = new System.Drawing.Point(99, 240);
            this.verifyBoxSname.Name = "verifyBoxSname";
            this.verifyBoxSname.NewLine = false;
            this.verifyBoxSname.Size = new System.Drawing.Size(359, 23);
            this.verifyBoxSname.TabIndex = 37;
            this.verifyBoxSname.TextV = "";
            this.verifyBoxSname.Visible = false;
            // 
            // labelHaddress
            // 
            this.labelHaddress.Location = new System.Drawing.Point(208, 97);
            this.labelHaddress.Name = "labelHaddress";
            this.labelHaddress.Size = new System.Drawing.Size(90, 12);
            this.labelHaddress.TabIndex = 23;
            this.labelHaddress.Text = "住所";
            this.labelHaddress.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelHaddress.Visible = false;
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 496);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1020, 3);
            this.splitter3.TabIndex = 54;
            this.splitter3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelDescription);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Controls.Add(this.labelInputerName);
            this.panel1.Controls.Add(this.buttonRegist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 691);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 30);
            this.panel1.TabIndex = 1;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(38, 10);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(133, 12);
            this.labelDescription.TabIndex = 3;
            this.labelDescription.Text = "ここに入力欄の説明を記述";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "説明：";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(596, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 0;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(398, 3);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 2;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 200);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1020, 496);
            this.scrollPictureControl1.TabIndex = 2;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // MediveryXMLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "MediveryXMLForm";
            this.Text = "OCR Check";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MediveryForm_FormClosed);
            this.Shown += new System.EventHandler(this.MediveryForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelAddInput.ResumeLayout(false);
            this.panelAddInput.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Panel panelAddInput;
        private System.Windows.Forms.Label labelDrName;
        private VerifyBox verifyBoxDrName;
        private VerifyBox verifyBoxPartial;
        private System.Windows.Forms.Label labelPartial;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private VerifyBox verifyBoxCountedDays;
        private System.Windows.Forms.Label labelCountedDays;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label labelHnum;
        private VerifyBox verifyBoxMediM;
        private VerifyBox verifyBoxMediY;
        private System.Windows.Forms.Label labelMediY;
        private System.Windows.Forms.Label labelMediM;
        private System.Windows.Forms.Label labelMediYM;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelBirthdayGengo;
        private System.Windows.Forms.Label labelBirthdayDate;
        private System.Windows.Forms.Label labelBirthdayYear;
        private System.Windows.Forms.Label labelBirthdayMonth;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelHnameCopy;
        private VerifyBox verifyBoxHname;
        private System.Windows.Forms.Button buttonZipSearch;
        private System.Windows.Forms.Label labelHname;
        private System.Windows.Forms.Label labelPaymentcode;
        private VerifyBox verifyBoxPname;
        private VerifyBox verifyBoxPaymentcode;
        private System.Windows.Forms.Label labelPname;
        private System.Windows.Forms.Label labelFvisittimes;
        private VerifyBox verifyBoxHzip;
        private VerifyBox verifyBoxFvisittimes;
        private System.Windows.Forms.Label labelHzip;
        private System.Windows.Forms.Label labelSname;
        private VerifyBox verifyBoxHaddress;
        private VerifyBox verifyBoxSname;
        private System.Windows.Forms.Label labelHaddress;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Splitter splitter3;
        private VerifyBox verifyBoxDates;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Button btnSelectlist;
        private VerifyBox verifyBoxPsex;
        private System.Windows.Forms.Label labelPsex;
        private System.Windows.Forms.Label labelAccname;
        private VerifyBox verifyBoxAccName;
        private System.Windows.Forms.Label labelDrNum;
        private VerifyBox verifyBoxDrNum;
        private System.Windows.Forms.Label lblDrCode01;
    }
}