﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Mejor.Medivery
{
    //2019年8月27日現在未使用っぽい
    public partial class MediveryForm : Form
    {
        private const int FOCUS_TYPE_TOP = 1;
        private const int FOCUS_TYPE_ERROR = 2;
        private const int FOCUS_TYPE_NEXT = 3;

        private const float SCROLL_PICTURE_RATIO = 0.35f;

        private Medivery medivery;
        private Insurer insurer;
        private int cym;
        private List<App> appList;
        private bool clinicStorage;
        private bool firstTime;

        private string prevTodofuken;
        private string prevSikutyoson;
        private PostalCodeForm pcForm;

        private BindingSource bsApp = new BindingSource();

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posMediYM;
        Point posHnum;
        Point posHname;
        Point posPname;
        Point posBirthday;
        Point posHzip;
        Point posHaddress;
        Point posCountedDays;
        Point posTotal;
        Point posPartial;
        Point posCharge;
        Point posSname;
        Point posFvisittimes;
        Point posPaymentcode;
        Point posDrName;
        Control[] mediymConts, hnumConts, hnameConts, pnameConts, birthdayConts, 
            hzipConts, haddressConts, counteddaysConts, totalConts, partialConts, chargeConts,
            snameConts, fvisittimesConts, paymentcodeConts, drnameConts;

        private List<VerifyBox> allBoxes { get; set; }
        private List<PostalCode> selectedHistory = new List<PostalCode>();

        public class AppHolder
        {
            public App app { get; set; }
            public int no { get; set; }
            public int aid { get; set; }
            public string flag { get; set; }
            public int flagInt { get; set; }
            public string hnum { get; set; }
            public int ayear { get; set; }
            public int amonth { get; set; }
            public APP_TYPE apptype { get; set; }
            public long cliniccode { get; set; }
            public Clinic clinic { get; set; }
        }

        /// <summary>
        /// 引数：clinicStorage（施術所情報を蓄積している場合はtrue。参照：Clinic.cs）
        /// </summary>
        /// <param name="insurer"></param>
        /// <param name="cym"></param>
        /// <param name="list"></param>
        /// <param name="clinicStorage"></param>
        public MediveryForm(Insurer insurer, int cym, List<App> list, bool clinicStorage = false)
        {
            InitializeComponent();

            var medivery = Medivery.GetMediVery(insurer.InsurerID);
            if (medivery == null)
            {
                this.Close();
                return;
            }
            this.medivery = medivery;
            this.insurer = insurer;
            this.cym = cym;
            this.appList = list;
            this.clinicStorage = clinicStorage;

            //拡大縮小カテゴリ
            mediymConts = new Control[] { verifyBoxMediY, verifyBoxMediM };
            hnumConts = new Control[] { verifyBoxHnum };
            hnameConts = new Control[] { verifyBoxHname };
            pnameConts = new Control[] { verifyBoxPname };
            birthdayConts = new Control[] { verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD };
            hzipConts = new Control[] { verifyBoxHzip };
            haddressConts = new Control[] { verifyBoxHaddress };
            counteddaysConts = new Control[] { verifyBoxCountedDays };
            totalConts = new Control[] { verifyBoxTotal };
            partialConts = new Control[] { verifyBoxPartial };
            chargeConts = new Control[] { verifyBoxCharge };
            snameConts = new Control[] { verifyBoxSname };
            fvisittimesConts = new Control[] { verifyBoxFvisittimes };
            paymentcodeConts = new Control[] { verifyBoxPaymentcode };
            drnameConts = new Control[] { verifyBoxDrName };
        }

        // --------------------------------------------------------------------------------
        //
        // イベントハンドラ部
        //
        // --------------------------------------------------------------------------------

        //行選択時
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            setCurrent();

            //フォーカスをTabIndex先頭に
            moveFocus(FOCUS_TYPE_TOP);
        }

        //フォーム表示時
        private void MediveryForm_Shown(object sender, EventArgs e)
        {
            var list = appList;

            if (list.Count == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }


            //入力タイプ選択
            using (var f = new MediveryInputSelectForm(list))
            {
                var result = f.ShowDialog();
                if (result != DialogResult.OK)
                {
                    this.Close();
                    return;
                }
                this.firstTime = f.IsFirstTime;

                //１回目入力未完了だがベリファイできるものだけ先に行う
                if (!f.IsFirstTime && !f.IsFirstTimeComplete)
                {
                    var newList = new List<App>();
                    list.ForEach(a => 
                    {
                        if (a.StatusFlagCheck(StatusFlag.追加入力済)) newList.Add(a);
                    });
                    list = newList;
                }
            }            

            //リストを作成
            var holders = new List<AppHolder>();
            var count = 1;
            foreach (var item in list)
            {
                var batch = BatchSelector.GetBatchApp(this.insurer, item.Aid);

                var holder = new AppHolder();
                holder.app = item;
                holder.no = count++;
                holder.aid = item.Aid;
                flagUpdate(holder);
                holder.hnum = item.HihoNum;
                holder.ayear = item.MediYear;
                holder.amonth = item.MediMonth;
                holder.apptype = item.AppType;
                holder.cliniccode = toLong(item.DrNum);
                holders.Add(holder);
            }

            //データバインド
            bsApp.DataSource = holders;
            dataGridViewPlist.DataSource = bsApp;
            bsApp.CurrentChanged += BsApp_CurrentChanged;

            foreach (DataGridViewColumn item in dataGridViewPlist.Columns)
            {
                item.Visible = false;
            }
            dataGridViewPlist.Columns[nameof(AppHolder.no)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.no)].Width = 30;
            dataGridViewPlist.Columns[nameof(AppHolder.no)].HeaderText = "No";
            dataGridViewPlist.Columns[nameof(AppHolder.no)].DisplayIndex = 0;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].Width = 55;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].Width = 70;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].DisplayIndex = 2;
            dataGridViewPlist.Columns[nameof(AppHolder.hnum)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.hnum)].Width = 60;
            dataGridViewPlist.Columns[nameof(AppHolder.hnum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(AppHolder.hnum)].DisplayIndex = 3;
            dataGridViewPlist.Columns[nameof(AppHolder.ayear)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.ayear)].Width = 25;
            dataGridViewPlist.Columns[nameof(AppHolder.ayear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(AppHolder.ayear)].DisplayIndex = 4;
            dataGridViewPlist.Columns[nameof(AppHolder.amonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.amonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(AppHolder.amonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(AppHolder.amonth)].DisplayIndex = 5;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].DisplayIndex = 6;
            dataGridViewPlist.Columns[nameof(AppHolder.cliniccode)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.cliniccode)].HeaderText = "施術所番号";
            dataGridViewPlist.Columns[nameof(AppHolder.cliniccode)].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewPlist.Columns[nameof(AppHolder.cliniccode)].DisplayIndex = 7;

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //textDispY.Text = DateTimeEx.GetHsYearFromAd(cym / 100).ToString();
            textDispM.Text = (cym % 100).ToString();
            textDispY.Text = DateTimeEx.GetHsYearFromAd(cym / 100, int.Parse(textDispM.Text)).ToString();
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            textBoxCount.Text = list.Count().ToString();

            var verify = firstTime ? "" : "　[ベリファイ入力]";
            this.Text += $" - Insurer: {insurer.InsurerName} {verify}";

            //入力欄初期化
            initControl();

            //入力欄取得
            this.allBoxes = getAllVerifyBox(true);

            //１行目を選択して表示
            dataGridViewPlist.CurrentCell = dataGridViewPlist[1, 0];//ここで選択しておかないとCurrentCellがnullになる
            setCurrent();//ここで明示的に呼ばないと表示されない

            //フォーカスをTabIndex先頭に
            moveFocus(FOCUS_TYPE_TOP);
        }

        //「×」を押されたときは一緒に子Formも閉じる
        private void MediveryForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (pcForm != null && !pcForm.IsDisposed) pcForm.Dispose();
        }

        //登録ボタン
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            updateAndSelectNext();
        }

        //ショートカットキー
        //F5：住所情報の表示
        private void MediveryForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                //　↓　現在アクティブな入力欄がベリファイエラー且つジャンプ先だった場合、
                //　↓　PerformClickだけではEnterイベントが発行されずベリファイ入力欄が表示されない
                buttonUpdate.Focus();//これが必要
                buttonUpdate.PerformClick();
            }
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();
        }

        //EnterキーでTABキーの動作をさせる
        private void MediveryForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }


        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //画像ファイルの回転（右周り）
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var appholder = (AppHolder)bsApp.Current;
            setImage(appholder.app);
        }

        //画像ファイルの回転（左回り）
        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var appholder = (AppHolder)bsApp.Current;
            setImage(appholder.app);
        }

        //画像ファイルの差し替え
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        //拡大ポイントと説明欄の切替
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            var vb = (VerifyBox)sender;
            setDescriptionText(vb.Tag);

            Point p;

            if (mediymConts.Contains(sender)) p = posMediYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (hnameConts.Contains(sender)) p = posHname;
            else if (pnameConts.Contains(sender)) p = posPname;
            else if (birthdayConts.Contains(sender)) p = posBirthday;
            else if (hzipConts.Contains(sender)) p = posHzip;
            else if (haddressConts.Contains(sender)) p = posHaddress;
            else if (counteddaysConts.Contains(sender)) p = posCountedDays;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (partialConts.Contains(sender)) p = posPartial;
            else if (chargeConts.Contains(sender)) p = posCharge;
            else if (snameConts.Contains(sender)) p = posSname;
            else if (fvisittimesConts.Contains(sender)) p = posFvisittimes;
            else if (paymentcodeConts.Contains(sender)) p = posPaymentcode;
            else if (drnameConts.Contains(sender)) p = posDrName;
            else return;

            scrollPictureControl1.ScrollPosition = p;

            //入力欄が住所の場合
            var cvb = (VerifyBox)sender;
            var cdesc = (Medivery.Description)cvb.Tag;
            if (cdesc.Name == Medivery.Description.DESC_NAME_HADDRESS)
            {
                if (cvb.Text != "")
                {
                    cvb.Select(cvb.Text.Length, 0);
                }
            }
        }

        //Ctrlキーで被保険者氏名をコピーペースト
        private void verifyBoxPname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー
            {
                if (medivery.Hname)
                {
                    //被保険者氏名と受療者氏名入力欄があれば名前をコピーする
                    var cvb = (VerifyBox)sender;
                    cvb.Text = verifyBoxHname.Text;
                    //かつ、次のコントロールへ移る
                    moveFocus(FOCUS_TYPE_NEXT, cvb);
                    return;
                }
            }
        }

        //数値以外を排除
        private void verifyBoxHzip_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {                
                e.Handled = true;
            }
        }

        //改行を排除
        private void verifyBoxHaddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                e.Handled = true;
            }
        }

        //住所情報の表示
        private void verifyBox_Leave(object sender, EventArgs e)
        {
            var cvb = (VerifyBox)sender;
            var cdesc = (Medivery.Description)cvb.Tag;

            //入力欄が郵便番号だった場合
            if (cdesc.Name == Medivery.Description.DESC_NAME_HZIP)
            {
                cvb.Text = cvb.Text.Trim();
                if (medivery.Haddress)
                {
                    //郵便番号と住所入力欄があれば自動で設定してあげる
                    if (int.TryParse(cvb.Text, out int zipcode))
                    {
                        var pc = PostalCode.GetPostalCode(zipcode);
                        if (pc != null)
                        {
                            var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                            var curAdd = verifyBoxHaddress.Text;
                            if (!curAdd.StartsWith(add)) verifyBoxHaddress.Text = add;
                        }
                        else verifyBoxHaddress.Text = "";
                    }
                    return;
                }
            }
        }

        //住所情報の検索
        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                var x = panelRight.Location.X + panelAddInput.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = SystemInformation.CaptionHeight + SystemInformation.FrameBorderSize.Height + panelRight.Location.Y + panelAddInput.Location.Y;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible)
            {
                pcForm.Show(this);
            }
            else
            {
                pcForm.MoveDefaultFocus();
            }
        }

        private void setPostal(PostalCode pc)
        {
            if (pc == null) return;
            if (medivery.Hzip) verifyBoxHzip.Text = pc.Postal_Code.ToString("0000000");
            if (medivery.Haddress) verifyBoxHaddress.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
            prevTodofuken = pc.Todofuken;
            prevSikutyoson = pc.Sichokuson;
            verifyBoxHaddress.Focus();
            verifyBoxHaddress.Select(verifyBoxHaddress.Text.Length, 0);
            selectedHistory.Add(pc);
        }

        //ウィンドウサイズの変更
        private void MediveryForm_SizeChanged(object sender, EventArgs e)
        {
            int imputWidth = 590;

            int H = this.Height - SystemInformation.CaptionHeight;   //ウィンドウ上部のバー幅を引く
            int curW = panelLeft.Size.Width;    //左のリスト+画像領域の現在の幅
            int curH = panelLeft.Size.Height;   //左のリスト+画像領域の現在の高さ
            int maxW = this.Width - imputWidth - panelDatalist.Size.Width;
            int calcW = (int)((float)H / 1.44f);

            if (maxW >= calcW)
            {
                int d = maxW - calcW >= 50 ? 50 : maxW - calcW;
                panelDatalist.Size = new Size(250 + d, curH);
                panelLeft.Size = new Size(calcW + panelDatalist.Size.Width, curH);
            }
            else
            {
                panelDatalist.Size = new Size(250, curH);
                panelLeft.Size = new Size(maxW + panelDatalist.Size.Width, curH);
            }
        }

        //スクロールで拡大縮小
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (mediymConts.Any(c => c.Focused)) posMediYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (hnameConts.Any(c => c.Focused)) posHname = pos;
            else if (pnameConts.Any(c => c.Focused)) posPname = pos;
            else if (birthdayConts.Any(c => c.Focused)) posBirthday = pos;
            else if (hzipConts.Any(c => c.Focused)) posHzip = pos;
            else if (haddressConts.Any(c => c.Focused)) posHaddress = pos;
            else if (counteddaysConts.Any(c => c.Focused)) posCountedDays = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (partialConts.Any(c => c.Focused)) posPartial = pos;
            else if (chargeConts.Any(c => c.Focused)) posCharge = pos;
            else if (snameConts.Any(c => c.Focused)) posSname = pos;
            else if (fvisittimesConts.Any(c => c.Focused)) posFvisittimes = pos;
            else if (paymentcodeConts.Any(c => c.Focused)) posPaymentcode = pos;
            else if (drnameConts.Any(c => c.Focused)) posDrName = pos;
        }

        //左のデータ一覧のソート
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var holders = (List<AppHolder>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(AppHolder.no))
            {
                holders.Sort((x, y) => x.no.CompareTo(y.no));
            }
            else if (name == nameof(AppHolder.aid))
            {
                holders.Sort((x, y) => x.aid.CompareTo(y.aid));
            }
            else if (name == nameof(AppHolder.flag))
            {
                holders.Sort((x, y) => x.flagInt.CompareTo(y.flagInt));
            }
            else if (name == nameof(AppHolder.hnum))
            {
                holders.Sort((x, y) => x.hnum.CompareTo(y.hnum));
            }
            else if (name == nameof(AppHolder.cliniccode))
            {
                holders.Sort((x, y) => x.cliniccode.CompareTo(y.cliniccode));
            }
            else
            {
                return;
            }

            bsApp.ResetBindings(false);
        }

        //戻るボタン
        private void buttonBack_Click(object sender, EventArgs e)
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri <= 0) return;
            dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri - 1];
        }

        // --------------------------------------------------------------------------------
        //
        // Control部
        // mediveryテーブルの各項目と連結されている
        // --------------------------------------------------------------------------------

        private void initControl()
        {
            if (medivery.MediYM)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_MEDIYM);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelMediYM.Visible = true;
                    verifyBoxMediY.Visible = true;
                    verifyBoxMediM.Visible = true;
                    labelMediY.Visible = true;
                    labelMediM.Visible = true;
                    verifyBoxMediY.Tag = desc;
                    verifyBoxMediM.Tag = desc;
                    labelMediYM.Text = desc.Label;
                    posMediYM = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Hnum)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HNUM);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHnum.Visible = true;
                    verifyBoxHnum.Visible = true;
                    verifyBoxHnum.Tag = desc;
                    labelHnum.Text = desc.Label;
                    posHnum = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Hname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHname.Visible = true;
                    verifyBoxHname.Visible = true;
                    verifyBoxHname.Tag = desc;
                    labelHname.Text = desc.Label;
                    posHname = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Pname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPname.Visible = true;
                    verifyBoxPname.Visible = true;
                    verifyBoxPname.Tag = desc;
                    labelPname.Text = desc.Label;
                    posPname = new Point(desc.LocationX, desc.LocationY);
                    if (medivery.Hname) labelHnameCopy.Visible = true;
                }
            }
            if (medivery.Birthday)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_BIRTHDAY);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelBirthday.Visible = true;
                    verifyBoxBE.Visible = true;
                    verifyBoxBY.Visible = true;
                    verifyBoxBM.Visible = true;
                    verifyBoxBD.Visible = true;
                    labelBirthdayGengo.Visible = true;
                    labelBirthdayYear.Visible = true;
                    labelBirthdayMonth.Visible = true;
                    labelBirthdayDate.Visible = true;
                    verifyBoxBE.Tag = desc;
                    verifyBoxBY.Tag = desc;
                    verifyBoxBM.Tag = desc;
                    verifyBoxBD.Tag = desc;
                    labelBirthday.Text = desc.Label;
                    posBirthday = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.Hzip)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HZIP);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHzip.Visible = true;
                    verifyBoxHzip.Visible = true;
                    buttonZipSearch.Visible = true;
                    verifyBoxHzip.Tag = desc;
                    labelHzip.Text = desc.Label;
                    posHzip = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.Haddress)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_HADDRESS);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelHaddress.Visible = true;
                    verifyBoxHaddress.Visible = true;
                    buttonZipSearch.Visible = true;
                    verifyBoxHaddress.Tag = desc;
                    labelHaddress.Text = desc.Label;
                    posHaddress = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.CountedDays)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_COUNTEDDAYS);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelCountedDays.Visible = true;
                    verifyBoxCountedDays.Visible = true;
                    verifyBoxCountedDays.Tag = desc;
                    labelCountedDays.Text = desc.Label;
                    posCountedDays = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Total)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_TOTAL);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelTotal.Visible = true;
                    verifyBoxTotal.Visible = true;
                    verifyBoxTotal.Tag = desc;
                    labelTotal.Text = desc.Label;
                    posTotal = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Partial)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PARTIAL);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPartial.Visible = true;
                    verifyBoxPartial.Visible = true;
                    verifyBoxPartial.Tag = desc;
                    labelPartial.Text = desc.Label;
                    posPartial = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Charge)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_CHARGE);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelCharge.Visible = true;
                    verifyBoxCharge.Visible = true;
                    verifyBoxCharge.Tag = desc;
                    labelCharge.Text = desc.Label;
                    posCharge = new Point(desc.LocationX, desc.LocationY);
                }
            }
            if (medivery.Sname)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_SNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelSname.Visible = true;
                    verifyBoxSname.Visible = true;
                    verifyBoxSname.Tag = desc;
                    labelSname.Text = desc.Label;
                    posSname = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.Fvisittimes)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_FVISITTIMES);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelFvisittimes.Visible = true;
                    verifyBoxFvisittimes.Visible = true;
                    verifyBoxFvisittimes.Tag = desc;
                    labelFvisittimes.Text = desc.Label;
                    posFvisittimes = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.Paymentcode)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_PAYMENTCODE);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelPaymentcode.Visible = true;
                    verifyBoxPaymentcode.Visible = true;
                    verifyBoxPaymentcode.Tag = desc;
                    labelPaymentcode.Text = desc.Label;
                    posPaymentcode = new Point(desc.LocationX, desc.LocationY);
                }                
            }
            if (medivery.DrName)
            {
                var desc = medivery.GetDescription(Medivery.Description.DESC_NAME_DRNAME);
                if ((firstTime && desc.FirstInput) || !firstTime)
                {
                    labelDrName.Visible = true;
                    verifyBoxDrName.Visible = true;
                    verifyBoxDrName.Tag = desc;
                    labelDrName.Text = desc.Label;
                    posDrName = new Point(desc.LocationX, desc.LocationY);
                }
            }
        }

        private void setDescriptionText(object tag)
        {
            var descs = (Medivery.Description)tag;
            labelDescription.Text = descs.Text;
        }

        private void clearControl()
        {
            getAllVerifyBox(false).ForEach(vb => vb.AllClear());
        }

        private void setCurrent()
        {
            var appholder = (AppHolder)bsApp.Current;
            if (appholder == null) return;
            setApp(appholder);
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(AppHolder holder)
        {
            var app = holder.app;

            //画像の表示
            setImage(app);

            //入力者情報
            labelInputerName.Text = $"1:{User.GetUserName(holder.app.AdditionalUid1)}\r\n" +
                $"2:{User.GetUserName(holder.app.AdditionalUid2)}";

            //現在の表示をリセット
            allBoxes.ForEach(vb => vb.AllClear());

            //施術所名が必要なら、目録から施術所（名）情報の取得
            if (medivery.Sname)
            {
                holder.clinic = clinicStorage ? Clinic.GetClinic(holder.cliniccode) : null;
            }

            //ベリファイ入力で、ベリファイ済の場合はデータをすべて表示する
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済))
            {
                setData(app, true);
                setData(app, false);
                return;
            }

            //ベリファイ入力で、入力済の場合はデータを相違チェック用の入力欄にセットし、storage管理しているものは表示する
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加入力済))
            {
                setData(app, false);
                setDataExtra(holder);
                return;
            }

            //１回目入力で、入力済で且つベリファイ入力でない場合はデータをすべて表示する
            if (firstTime && app.StatusFlagCheck(StatusFlag.追加入力済))
            {
                setData(app, true);
                return;
            }

            //初めての入力
            setDataExtra(holder);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    float scale = 1f;
                    if (img.HorizontalResolution <= 200f && img.VerticalResolution <= 200f)
                    {
                        scale = 1.5f;//dpiが小さい画像は拡大する
                    }
                    scrollPictureControl1.Ratio = SCROLL_PICTURE_RATIO * scale;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posHname;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 入力欄にセットします。show=falseの場合は不可視の入力欄にセットします。
        /// </summary>
        /// <param name="app"></param>
        /// <param name="show"></param>
        private void setData(App app, bool show)
        {
            foreach (var vb in allBoxes)
            {
                var desc = (Medivery.Description)vb.Tag;
                var scan = Scan.Select(app.ScanID);

                switch (desc.Name)
                {
                    case Medivery.Description.DESC_NAME_MEDIYM:
                        if (show)
                        {
                            verifyBoxMediY.Text = app.MediYear.ToString();
                            verifyBoxMediM.Text = app.MediMonth.ToString();
                        }
                        else
                        {
                            verifyBoxMediY.TextV = app.MediYear.ToString();
                            verifyBoxMediM.TextV = app.MediMonth.ToString();
                        }
                        break;
                    case Medivery.Description.DESC_NAME_HNUM:
                        if (show) verifyBoxHnum.Text = app.HihoNum;
                        else verifyBoxHnum.TextV = app.HihoNum;
                        break;
                    case Medivery.Description.DESC_NAME_HNAME:
                        if (show) verifyBoxHname.Text = app.HihoName;
                        else verifyBoxHname.TextV = app.HihoName;
                        break;
                    case Medivery.Description.DESC_NAME_PNAME:
                        if (show) verifyBoxPname.Text = app.PersonName;
                        else verifyBoxPname.TextV = app.PersonName;
                        break;
                    case Medivery.Description.DESC_NAME_BIRTHDAY:
                        if (show)
                        {
                            verifyBoxBE.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                            verifyBoxBY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                            verifyBoxBM.Text = app.Birthday.Month.ToString();
                            verifyBoxBD.Text = app.Birthday.Day.ToString();
                        }
                        else
                        {
                            verifyBoxBE.TextV = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                            verifyBoxBY.TextV = DateTimeEx.GetJpYear(app.Birthday).ToString();
                            verifyBoxBM.TextV = app.Birthday.Month.ToString();
                            verifyBoxBD.TextV = app.Birthday.Day.ToString();
                        }
                        break;
                    case Medivery.Description.DESC_NAME_HZIP:
                        if (show) verifyBoxHzip.Text = app.HihoZip;
                        else verifyBoxHzip.TextV = app.HihoZip;
                        break;
                    case Medivery.Description.DESC_NAME_HADDRESS:
                        if (show) verifyBoxHaddress.Text = app.HihoAdd;
                        else verifyBoxHaddress.TextV = app.HihoAdd;
                        break;
                    case Medivery.Description.DESC_NAME_COUNTEDDAYS:
                        if (show) verifyBoxCountedDays.Text = app.CountedDays.ToString();
                        else verifyBoxCountedDays.TextV = app.CountedDays.ToString();
                        break;
                    case Medivery.Description.DESC_NAME_TOTAL:
                        if (show) verifyBoxTotal.Text = app.Total.ToString();
                        else verifyBoxTotal.TextV = app.Total.ToString();
                        break;
                    case Medivery.Description.DESC_NAME_PARTIAL:
                        if (show) verifyBoxPartial.Text = app.Partial.ToString();
                        else verifyBoxPartial.TextV = app.Partial.ToString();
                        break;
                    case Medivery.Description.DESC_NAME_CHARGE:
                        if (show) verifyBoxCharge.Text = app.Charge.ToString();
                        else verifyBoxCharge.TextV = app.Charge.ToString();
                        break;
                    case Medivery.Description.DESC_NAME_SNAME:
                        if (show) verifyBoxSname.Text = app.ClinicName;
                        else verifyBoxSname.TextV = app.ClinicName;
                        break;
                    case Medivery.Description.DESC_NAME_FVISITTIMES:
                        if (show) verifyBoxFvisittimes.Text = app.VisitTimes.ToString();
                        else verifyBoxFvisittimes.TextV = app.VisitTimes.ToString();
                        break;
                    case Medivery.Description.DESC_NAME_PAYMENTCODE:
                        if (show) verifyBoxPaymentcode.Text = app.AccountNumber;
                        else verifyBoxPaymentcode.TextV = app.AccountNumber;
                        break;
                    case Medivery.Description.DESC_NAME_DRNAME:
                        if (show) verifyBoxDrName.Text = app.DrName.ToString();
                        else verifyBoxDrName.TextV = app.DrName.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// 入力モードに関わらずデータをセットしたい場合に使用します。
        /// </summary>
        /// <param name="holder"></param>
        private void setDataExtra(AppHolder holder)
        {
            foreach (var vb in allBoxes)
            {
                var desc = (Medivery.Description)vb.Tag;
                switch (desc.Name)
                {
                    case Medivery.Description.DESC_NAME_SNAME:
                        if (holder.clinic != null && holder.clinic.Verified) verifyBoxSname.Text = holder.clinic.Name;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// ベリファイ入力をチェックします  エラーがある場合、false
        /// </summary>
        /// <returns></returns>
        private bool checkVerify()
        {
            bool success = true;
            allBoxes.ForEach(vb => success &= !vb.CheckVerify());
            return success;
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="holder"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(AppHolder holder)
        {
            var app = holder.app;

            bool err = false;

            var isEmpty = new Func<string, bool>((s) =>
            {
                return string.IsNullOrWhiteSpace(s);
            });
            var isZero = new Func<int, bool>((i) =>
            {
                return i == 0;
            });
            var setError = new Action<TextBox, bool>((t, e) =>
            {
                if (e)
                {
                    t.BackColor = Color.Pink;
                    err = true;
                }
                else
                {
                    t.BackColor = SystemColors.Info;
                }
            });

            string value;
            bool result;
            foreach (var vb in allBoxes)
            {
                value = vb.Text;
                var desc = (Medivery.Description)vb.Tag;
                
                //登録値と入力値が空白の場合はアップデートしない
                switch (desc.Name)
                {
                    case Medivery.Description.DESC_NAME_MEDIYM:
                        if (isEmpty(verifyBoxMediY.Text) && isEmpty(verifyBoxMediM.Text))
                        {
                            setError(verifyBoxMediY, false);
                            setError(verifyBoxMediM, false);
                        }
                        else
                        {
                            result = true;
                            result &= checkNumber(verifyBoxMediY.Text);
                            result &= checkNumber(verifyBoxMediM.Text);
                            if (result)
                            {
                                app.MediYear = toInt(verifyBoxMediY.Text);
                                app.MediMonth = toInt(verifyBoxMediM.Text);
                            }
                            setError(verifyBoxMediY, !result);
                            setError(verifyBoxMediM, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HNUM:
                        if (isEmpty(app.HihoNum) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= !string.IsNullOrEmpty(value);
                            if (result) app.HihoNum = value;
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HNAME:
                        if (isEmpty(app.HihoName) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkName(value);
                            if (result) app.HihoName = value;
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PNAME:
                        if (isEmpty(app.PersonName) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkName(value);
                            if (result) app.PersonName = value;
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_BIRTHDAY:
                        if (app.Birthday == DateTime.MinValue &&
                            isEmpty(verifyBoxBE.Text) &&
                            isEmpty(verifyBoxBY.Text) &&
                            isEmpty(verifyBoxBM.Text) &&
                            isEmpty(verifyBoxBD.Text))
                        {
                            setError(verifyBoxBE, false);
                            setError(verifyBoxBY, false);
                            setError(verifyBoxBM, false);
                            setError(verifyBoxBD, false);
                        }
                        else
                        {
                            result = true;
                            var dt = checkDateTime(verifyBoxBE.Text, verifyBoxBY.Text, verifyBoxBM.Text, verifyBoxBD.Text);
                            result &= dt != DateTimeEx.DateTimeNull;
                            if (result) app.Birthday = dt;
                            setError(verifyBoxBE, !result);
                            setError(verifyBoxBY, !result);
                            setError(verifyBoxBM, !result);
                            setError(verifyBoxBD, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HZIP:
                        if (isEmpty(app.HihoZip) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= value != "";
                            result &= value.Length == 7;
                            app.HihoZip = value;
                            setError(vb, false);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_HADDRESS:
                        if (isEmpty(app.HihoAdd) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= value != "";
                            app.HihoAdd = value;
                            setError(vb, false);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_COUNTEDDAYS:
                        if (isZero(app.CountedDays) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.CountedDays = toInt(value);
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_TOTAL:
                        if (isZero(app.Total) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.Total = toInt(value);
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PARTIAL:
                        if (isZero(app.Partial) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.Partial = toInt(value);
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_CHARGE:
                        if (isZero(app.Charge) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.Charge = toInt(value);
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_SNAME:
                        if (isEmpty(app.ClinicName) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= value != "";
                            if (result) app.ClinicName = value;
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_FVISITTIMES:
                        if (isZero(app.VisitTimes) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.VisitTimes = toInt(value);
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_PAYMENTCODE:
                        if (isEmpty(app.AccountNumber) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= checkNumber(value);
                            if (result) app.AccountNumber = value;
                            setError(vb, !result);
                        }
                        break;

                    case Medivery.Description.DESC_NAME_DRNAME:
                        if (isEmpty(app.DrName) && isEmpty(value)) setError(vb, false);
                        else
                        {
                            result = true;
                            result &= value != null;
                            if (result) app.DrName = value;
                            setError(vb, !result);
                        }
                        break;

                    default:
                        MessageBox.Show("不正なコントロールが存在します。\r\nシステム担当者を呼んでください。","エラー");
                        return false;
                }
            }

            return !err;
        }

        /// <summary>
        /// フォーカスを移動します。移動先はFOCUS_TYPE_...を参照してください。
        /// </summary>
        /// <param name="type"></param>
        private void moveFocus(int type, VerifyBox currentVerifyBox = null)
        {
            switch (type)
            {
                case FOCUS_TYPE_TOP:
                    if (allBoxes.Count() > 0) allBoxes[0].Focus();
                    break;
                case FOCUS_TYPE_ERROR:
                    var exisErr = false;
                    foreach(var vb in allBoxes)
                    {
                        if (vb.BackColor != SystemColors.Info)
                        {
                            exisErr = true;
                            vb.Focus();
                            break;
                        }
                    }
                    if (!exisErr) moveFocus(FOCUS_TYPE_TOP);//Errorがなければ先頭へ
                    break;
                case FOCUS_TYPE_NEXT:
                    var currIndex = -1;
                    for (int i = 0; i < allBoxes.Count; i++)
                    {
                        if (allBoxes[i] == currentVerifyBox)
                        {
                            currIndex = i;
                            break;
                        }
                    }
                    if (currIndex != -1 && currIndex != allBoxes.Count - 1) allBoxes[currIndex + 1].Focus();
                    break;
            }
        }


        // --------------------------------------------------------------------------------
        //
        // App操作部
        //
        // --------------------------------------------------------------------------------

        private void updateAndSelectNext()
        {
            //登録
            var appholder = (AppHolder)bsApp.Current;
            if (!appDBupdate(appholder)) return;

            //更新
            flagUpdate(appholder);

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri];
                    return;
                }
                this.Close();
            }
            else
            {
                dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri + 1];//index0は不可視列なので1にする
            }
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(AppHolder holder)
        {
            var app = holder.app;

            //ベリファイチェック
            if (!firstTime && !checkVerify())
            {
                MessageBox.Show("入力が一致していません。入力データを確認してください。",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                moveFocus(FOCUS_TYPE_ERROR);
                return false;
            }

            //値の点検とインスタンスの取得
            if(!checkApp(holder)) 
            {
                MessageBox.Show("エラーがあります。入力データを確認してください。",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                moveFocus(FOCUS_TYPE_ERROR);
                return false;
            }

            //施術所名が登録値と異なる場合の処理
            var clinicUpdated = false;
            var inputtingClinicName = app.ClinicName;
            if (clinicStorage && medivery.Sname && holder.clinic != null && !string.IsNullOrEmpty(inputtingClinicName) && holder.clinic.Name != inputtingClinicName)
            {
                var desc = (Medivery.Description)verifyBoxSname.Tag;
                var update = MessageBox.Show(
                    $"{desc.Label}が登録されている値（{holder.clinic.Name}）と異なっています。\r\n上書きしますか？",
                    "変更確認",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (update != DialogResult.Yes)
                {
                    verifyBoxSname.Focus();
                    verifyBoxSname.SelectAll();
                    return false;
                }
                holder.clinic.Name = inputtingClinicName;
                holder.clinic.Update_Date = DateTime.Today;
                
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                holder.clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                //holder.clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                holder.clinic.Verified = false;
                holder.clinic.Update();

                //同じ施術所番号を使用しているものをすべて再チェック（今開いている一覧上のみ）
                var list = ((List<AppHolder>)bsApp.DataSource).FindAll(a => a.app.StatusFlagCheck(StatusFlag.追加ベリ済));
                var updateList = new List<App>();
                foreach (var item in list)
                {
                    if (item.app.Aid == app.Aid) continue;//自分自身は後で更新するのでスキップ
                    if (item.app.DrNum == app.DrNum && item.app.ClinicName != "" && item.app.ClinicName != inputtingClinicName)
                    {
                        updateList.Add(item.app);
                    }
                }
                if (updateList.Count > 0)
                {
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    var multiUpdate = MessageBox.Show(
                        $"{DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}{(cym % 100).ToString("00")}" +
                        $"分の追加ベリファイ済の申請書のうち同じ施術所番号を使用しているもので、" +
                        $"{desc.Label}が登録されている値（{holder.clinic.Name}）と異なるものが{updateList.Count}件存在します。\r\n" +
                        $"それらの内容も変更してもよろしいですか？",
                        "変更確認",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    /*
                    var multiUpdate = MessageBox.Show(
                       $"{DateTimeEx.GetHsYearFromAd(cym / 100)}{(cym % 100).ToString("00")}" +
                       $"分の追加ベリファイ済の申請書のうち同じ施術所番号を使用しているもので、" +
                       $"{desc.Label}が登録されている値（{holder.clinic.Name}）と異なるものが{updateList.Count}件存在します。\r\n" +
                       $"それらの内容も変更してもよろしいですか？",
                       "変更確認",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);*/
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    if (multiUpdate == DialogResult.Yes)
                    {
                        using (var tran = DB.Main.CreateTransaction())
                        {
                            var newSname = app.ClinicName;
                            foreach (var updateItem in updateList)
                            {
                                updateItem.ClinicName = newSname;
                                if (!updateItem.UpdateMedivery(App.UPDATE_TYPE.Null, tran))
                                {
                                    tran.Rollback();
                                    MessageBox.Show("変更に失敗しました。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            tran.Commit();
                        }
                    }
                }

                clinicUpdated = true;//後述のVerified更新を回避する
            }

            //フラグ整理           
            if (firstTime)
            {
                if (app.StatusFlagCheck(StatusFlag.追加ベリ済))
                {
                    //ベリファイ済を解除する
                    app.StatusFlagRemove(StatusFlag.追加ベリ済);
                }
                app.StatusFlagSet(StatusFlag.追加入力済);
            }
            else
            {
                app.StatusFlagSet(StatusFlag.追加ベリ済);
            }

            //データベースへ反映
            if (!app.UpdateMedivery(firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput))
            {
                MessageBox.Show("登録中にエラーが発生しました。",
                   "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return false;
            }

            //施術所番号が存在し、施術所名の入力がある場合...
            if (clinicStorage && holder.cliniccode != -1 && medivery.Sname && !string.IsNullOrEmpty(inputtingClinicName))
            {
                if (holder.clinic == null)
                {
                    //初登場だった場合は目録へ追加
                    var clinic = new Clinic();
                    clinic.Code = holder.cliniccode;
                    clinic.Name = inputtingClinicName;
                    clinic.Insert_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    clinic.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                    //clinic.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    clinic.Verified = false;
                    clinic.Insert();
                }
                else if(!clinicUpdated && holder.clinic.Name == inputtingClinicName && !holder.clinic.Verified)
                {
                    //登録値と同じで確定フラグが立っていない場合は上書き
                    holder.clinic.Update_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    holder.clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                    //holder.clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    holder.clinic.Verified = true;
                    holder.clinic.UpdateVerified();
                }
            }

            return true;
        }

        /// <summary>
        /// フラグを更新します。
        /// </summary>
        private void flagUpdate(AppHolder holder)
        {
            if (holder.app.StatusFlagCheck(StatusFlag.追加ベリ済))
            {
                holder.flag = "ベリファイ済み";
                holder.flagInt = 2;
            }
            else if (holder.app.StatusFlagCheck(StatusFlag.追加入力済))
            {
                holder.flag = "入力済み";
                holder.flagInt = 1;
            }
            else
            {
                holder.flag = "未入力";
                holder.flagInt = 0;
            }
        }

        // --------------------------------------------------------------------------------
        //
        // Utility部
        //
        // --------------------------------------------------------------------------------

        private List<VerifyBox> getAllVerifyBox(bool visibleOnly)
        {
            var list = new List<VerifyBox>();
            Action<Control> get = null;
            get = cs =>
            {
                VerifyBox vb;
                foreach (Control c in cs.Controls)
                {
                    if (visibleOnly && !c.Visible) continue;
                    if (c is VerifyBox)
                    {
                        vb = (VerifyBox)c;
                        list.Add(vb);
                    }
                    if (c is Panel)
                    {
                        get(c);
                    }
                }
            };
            get(panelAddInput);
            list.Sort((x, y) => x.TabIndex.CompareTo(y.TabIndex));//タブインデックス昇順
            return list;
        }

        private bool checkName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return false;//空白
            var names = name.Split('　');
            if (names.Count() < 2) return false;//全角スペース区切りなし
            return true;
        }

        private DateTime checkDateTime(string e, string y, string m, string d)
        {
            if (string.IsNullOrWhiteSpace(e)) return DateTimeEx.DateTimeNull;//空白
            if (string.IsNullOrWhiteSpace(y)) return DateTimeEx.DateTimeNull;//空白
            if (string.IsNullOrWhiteSpace(m)) return DateTimeEx.DateTimeNull;//空白
            if (string.IsNullOrWhiteSpace(d)) return DateTimeEx.DateTimeNull;//空白
            if (!checkNumber(e)) return DateTimeEx.DateTimeNull;//数字化できない
            if (!checkNumber(y)) return DateTimeEx.DateTimeNull;//数字化できない
            if (!checkNumber(m)) return DateTimeEx.DateTimeNull;//数字化できない
            if (!checkNumber(d)) return DateTimeEx.DateTimeNull;//数字化できない
            int.TryParse(e, out int intE);
            int.TryParse(y, out int intY);
            int.TryParse(m, out int intM);
            int.TryParse(d, out int intD);

            //20190816122356 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更
            
                    //var seirekiY = DateTimeEx.GetAdYearFromEraYear(intE, intY);
            var seirekiY = int.Parse(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(intE.ToString() + intY.ToString("00") + intM.ToString("00"))).ToString().Substring(0,4));
            //20190816122356 furukawa ed ////////////////////////

            if (DateTimeEx.IsDate(seirekiY, intM, intD))
            {
                return new DateTime(seirekiY, intM, intD);
            }
            return DateTimeEx.DateTimeNull;
        }

        private bool checkNumber(string number)
        {
            if (string.IsNullOrWhiteSpace(number)) return false;
            if (long.TryParse(number, out long result)) return true;
            return false;
        }

        private int toInt(string number)
        {
            int numberInt;
            if (int.TryParse(number, out numberInt)) return numberInt;
            return -1;
        }

        private long toLong(string number)
        {
            long numberLong;
            if (long.TryParse(number, out numberLong)) return numberLong;
            return -1;
        }

    }
}
