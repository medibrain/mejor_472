﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Mejor.Medivery
{
    public partial class MediveryFlagSelectForm : Form
    {
        Insurer insurer;
        int cym;
        BindingSource bs;
        Dictionary<StatusFlag, List<App>> dic;

        public MediveryFlagSelectForm(Insurer insurer, int cym)
        {
            InitializeComponent();

            //20210217093224 furukawa st ////////////////////////
            //フォーム色を環境に合わす
            
            CommonTool.setFormColor(this);
            //20210217093224 furukawa ed ////////////////////////


            this.insurer = insurer;
            this.cym = cym;
            this.bs = new BindingSource();
            this.dic = new Dictionary<StatusFlag, List<App>>();

            updateList();

            DataGridViewColumn c;
            c = dataGridView1.Columns[nameof(FlagCountHolder.FlagType)];
            c.HeaderText = "対象";
            c.Width = 80;
            c.Visible = true;
            c = dataGridView1.Columns[nameof(FlagCountHolder.AllCount)];
            c.HeaderText = "件数";
            c.Width = 80;
            c.Visible = true;
            c = dataGridView1.Columns[nameof(FlagCountHolder.InputCount)];
            c.HeaderText = "1回目済";
            c.Width = 100;
            c.Visible = true;
            c = dataGridView1.Columns[nameof(FlagCountHolder.VerifyCount)];
            c.HeaderText = "ﾍﾞﾘﾌｧｲ済";
            c.Width = 100;
            c.Visible = true;

            if (insurer.InsurerType == INSURER_TYPE.学校共済) buttonDantai.Enabled = true;

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.Text = "Medivery　" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年{ cym % 100}月分";
            //this.Text = "Medivery　" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100)}年{ cym % 100}月分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        }

        private void updateList()
        {
            var flagCountList = getFlagCountList(cym);
            bs.DataSource = flagCountList;
            bs.ResetBindings(false);
            dataGridView1.DataSource = bs;
        }

        private List<FlagCountHolder> getFlagCountList(int cym)
        {
            var result = new List<FlagCountHolder>();

            dic.Clear();

            var flags = new StatusFlag[] { StatusFlag.照会対象, StatusFlag.過誤, StatusFlag.返戻 };
            foreach (var flag in flags)
            {
                //対象のフラグがあるappを取得。申請書種別は関係ない
                var list = App.GetAppsByStatusFlag(cym, flag);
                dic.Add(flag, list);
                var holder = new FlagCountHolder();
                holder.Flag = flag;
                holder.FlagType = flag.ToString().Replace("対象", string.Empty);
                list.ForEach(a =>
                {
                    holder.AllCount++;
                    if (a.StatusFlagCheck(StatusFlag.追加入力済)) holder.InputCount++;
                    if (a.StatusFlagCheck(StatusFlag.追加ベリ済)) holder.VerifyCount++;
                });
                result.Add(holder);
            }

            return result;
        }

        /**
         * 追加項目入力の開始
         */
        private void buttonMedivery_Click(object sender, EventArgs e)
        {
            var flag = (FlagCountHolder)bs.Current;
            if (flag == null) return;
            var list = dic[flag.Flag];



            //var f = new MediveryRevForm(cym, list,
            //    insurer.EnumInsID == InsurerID.OTARU_KOKUHO ||
            //    insurer.InsurerType == INSURER_TYPE.学校共済 ||
            //    insurer.EnumInsID == InsurerID.SHARP_KENPO);//20190827093044 furukawaシャープ健保施術所名必要 だが、返答くるまでで一旦保留


            //20210216161448 furukawa st ////////////////////////
            //xml管理型MediveryFormに変更
            
            var f = new MediveryXMLForm(cym, list,
              insurer.EnumInsID == InsurerID.OTARU_KOKUHO || insurer.InsurerType == INSURER_TYPE.学校共済);

            //      var f = new MediveryRevForm(cym, list,
            //          insurer.EnumInsID == InsurerID.OTARU_KOKUHO || insurer.InsurerType == INSURER_TYPE.学校共済);
            //20210216161448 furukawa ed ////////////////////////

            f.ShowDialog();

            updateList();
        }

        /**
         * バッチシートのみ表示し、団体情報の登録開始
         */
        private void buttonDantai_Click(object sender, EventArgs e)
        {
            var flag = (FlagCountHolder)bs.Current;
            if (flag == null) return;

            var flagList = App.GetAppsByStatusFlag(cym, flag.Flag);
            List<App> batchList;
            //if (insurer.InsurerID == (int)InsurerID.OSAKA_GAKKO)
            //{
            //    //学校共済大阪支部対応
            //    batchList = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ2}");
            //}
            //else
            //{
            //    batchList = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ}");
            //}
            batchList = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ}");

            //照会対象申請書の親バッチを取得
            var targetList = new List<App>();
            var hs = new HashSet<int>();
            batchList.Sort((x, y) => y.Aid.CompareTo(x.Aid));
            foreach (var f in flagList)
            {
                foreach (var b in batchList)
                {
                    if (b.Aid < f.Aid)
                    {
                        if (!hs.Add(b.Aid)) break;
                        targetList.Add(b);
                        break;
                    }
                }
            }

            using (var f = new MediveryDantaiForm(insurer, cym, targetList))
            {
                f.ShowDialog();
            }
        }

        /**
         * 照会リスト出力の開始
         */
        private void buttonListExport_Click(object sender, EventArgs e)
        {
            var flag = (FlagCountHolder)bs.Current;
            if (flag == null) return;

            var list = App.GetAppsByStatusFlag(cym, flag.Flag);
            if (list != null && list.Count == 0)
            {
                MessageBox.Show("出力可能な申請書が１枚もありません。", "失敗");
                return;
            }

            var exportFlag = true;
            foreach (var item in list)
            {
                if (!item.StatusFlagCheck(StatusFlag.追加ベリ済))
                {
                    exportFlag = false;
                    break;
                }
            }
            if (!exportFlag)
            {
                var export = MessageBox.Show(
                        $"追加項目のベリファイ入力が未完了なものがあります。{System.Environment.NewLine}出力を行ってもよろしいですか？",
                        "出力確認", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (export != DialogResult.Yes) return;
            }

            //var fileName = "";
            //using (var f = new SaveFileDialog())
            //{
            //    var forwardName = string.Empty;
            //    var yyMM = $"{DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00")}{(cym % 100).ToString("00")}";
            //    if (insurer.InsurerType == INSURER_TYPE.学校共済)
            //    {
            //        forwardName = $"{insurer.ViewIndex.ToString("00")}{Pref.GetPref(insurer.ViewIndex)?.Name2}_{yyMM}";
            //    }
            //    else
            //    {
            //        forwardName = $"{insurer.InsurerName}_{yyMM}";
            //    }
            //    f.FileName = $"{forwardName}文書{flag.FlagType}対象者一覧表.csv";
            //    f.Filter = "CSVファイル(*.csv)|*.csv|すべてのファイル(*.*)|*.*";
            //    f.FilterIndex = 1;
            //    f.RestoreDirectory = true;
            //    f.Title = "保存先ファイルを指定してください";

            //    if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            //    fileName = f.FileName;
            //}

            using (var f = new ListCreate.ListExportForm(list))
            {
                f.ShowDialog();
            }
        }


        public class FlagCountHolder
        {
            public StatusFlag Flag;
            public string FlagType { get; set; }
            public int AllCount { get; set; }
            public int InputCount { get; set; }
            public int VerifyCount { get; set; }
        }

    }
}
