﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Mejor.Medivery
{
    public partial class MediveryInputSelectForm : Form
    {
        public bool IsFirstTime = true;
        public bool IsFirstTimeComplete = true;

        public MediveryInputSelectForm(List<App> list)
        {
            InitializeComponent();
            this.DialogResult = DialogResult.Cancel;
            buttonVerify.Enabled = checkVerifyableAndSetInfo(list);
        }

        private bool checkVerifyableAndSetInfo(List<App> list)
        {
            var allCount = list.Count;
            decimal fstCompleteCount = 0;
            decimal fstNoinputCount = 0;
            decimal sndCompleteCount = 0;

            var inputted = false;
            foreach(var app in list)
            {

                if (!app.StatusFlagCheck(StatusFlag.追加入力済))
                {
                    //１回目未完了
                    IsFirstTimeComplete = false;
                    fstNoinputCount++;
                }

                if (app.StatusFlagCheck(StatusFlag.追加入力済))
                {
                    //１回目入力済
                    inputted = true;
                    fstCompleteCount++;
                }

                if (app.StatusFlagCheck(StatusFlag.追加ベリ済))
                {
                    //ベリファイ完了
                    inputted = true;
                    sndCompleteCount++;
                }
            }

            decimal fstCompletePercent = allCount == 0 ? 0 : (fstCompleteCount / allCount) * 100;
            decimal sndCompletePercent = allCount == 0 ? 0 : (sndCompleteCount / allCount) * 100;

            labelCompletePercent.Text = fstCompletePercent.ToString("0.00") + " %";
            labelAllCount.Text = allCount.ToString() + "件";
            labelCompleteCount.Text = fstCompleteCount.ToString() + "件";
            labelNoinputCount.Text = fstNoinputCount.ToString() + "件";
            labelCompletePercentVerify.Text = sndCompletePercent.ToString("0.00") + " %";
            labelAllCountVerify.Text = allCount.ToString() + "件";
            labelCompleteCountVerify.Text = sndCompleteCount.ToString() + "件";
            labelNoinputCountVerify.Text = (allCount - sndCompleteCount).ToString() + "件";
            labelInputEnableCountVerify.Text = fstCompleteCount.ToString() + " 件）";

            return inputted;
        }

        //１回目入力
        private void buttonFirstTime_Click(object sender, EventArgs e)
        {
            IsFirstTime = true;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        //ベリファイ入力
        private void buttonVerify_Click(object sender, EventArgs e)
        {
            IsFirstTime = false;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        //キャンセル
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
