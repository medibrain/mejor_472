﻿namespace Mejor.Medivery
{
    partial class MediveryDantaiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonNextImage = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panelUP = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textDispM = new System.Windows.Forms.TextBox();
            this.textDispY = new System.Windows.Forms.TextBox();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelAddInput = new System.Windows.Forms.Panel();
            this.radioButtonOther = new System.Windows.Forms.RadioButton();
            this.radioButtonNone = new System.Windows.Forms.RadioButton();
            this.radioButtonRenraku = new System.Windows.Forms.RadioButton();
            this.radioButtonRyosho = new System.Windows.Forms.RadioButton();
            this.labelDantaiCode = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDantaiName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelStatusOCR = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.userControlImage1 = new UserControlImage();
            this.verifyBoxHenreiOther = new Mejor.VerifyBox();
            this.verifyBoxDantaiCode = new Mejor.VerifyBox();
            this.verifyBoxDantaiName = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            this.panelUP.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelAddInput.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(647, 5);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 53);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(320, 669);
            this.dataGridViewPlist.TabIndex = 0;
            this.dataGridViewPlist.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPlist_ColumnHeaderMouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "年";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(75, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "月 請求分";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Controls.Add(this.splitter1);
            this.panelLeft.Controls.Add(this.panelDatalist);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(3, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(591, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonNextImage);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(323, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(268, 722);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonNextImage
            // 
            this.buttonNextImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextImage.Location = new System.Drawing.Point(171, 12);
            this.buttonNextImage.Name = "buttonNextImage";
            this.buttonNextImage.Size = new System.Drawing.Size(75, 23);
            this.buttonNextImage.TabIndex = 7;
            this.buttonNextImage.Text = "次の申請書";
            this.buttonNextImage.UseVisualStyleBackColor = true;
            this.buttonNextImage.Click += new System.EventHandler(this.buttonNextImage_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(171, 681);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(8, 8);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(320, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 722);
            this.splitter1.TabIndex = 40;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Controls.Add(this.splitter3);
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.MinimumSize = new System.Drawing.Size(320, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(320, 722);
            this.panelDatalist.TabIndex = 1;
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 50);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(320, 3);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.Cyan;
            this.panelUP.Controls.Add(this.label3);
            this.panelUP.Controls.Add(this.textBoxCount);
            this.panelUP.Controls.Add(this.label2);
            this.panelUP.Controls.Add(this.textDispM);
            this.panelUP.Controls.Add(this.textDispY);
            this.panelUP.Controls.Add(this.label10);
            this.panelUP.Controls.Add(this.label1);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(320, 50);
            this.panelUP.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "照会予定申請書のバッチシート：";
            // 
            // textBoxCount
            // 
            this.textBoxCount.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxCount.Location = new System.Drawing.Point(165, 28);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.ReadOnly = true;
            this.textBoxCount.Size = new System.Drawing.Size(32, 19);
            this.textBoxCount.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "件";
            // 
            // textDispM
            // 
            this.textDispM.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispM.Location = new System.Drawing.Point(51, 5);
            this.textDispM.Name = "textDispM";
            this.textDispM.ReadOnly = true;
            this.textDispM.Size = new System.Drawing.Size(25, 19);
            this.textDispM.TabIndex = 4;
            // 
            // textDispY
            // 
            this.textDispY.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispY.Location = new System.Drawing.Point(6, 5);
            this.textDispY.Name = "textDispY";
            this.textDispY.ReadOnly = true;
            this.textDispY.Size = new System.Drawing.Size(25, 19);
            this.textDispY.TabIndex = 2;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelAddInput);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelStatusOCR);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(594, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Padding = new System.Windows.Forms.Padding(5);
            this.panelRight.Size = new System.Drawing.Size(750, 722);
            this.panelRight.TabIndex = 0;
            // 
            // panelAddInput
            // 
            this.panelAddInput.AutoScroll = true;
            this.panelAddInput.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelAddInput.Controls.Add(this.radioButtonOther);
            this.panelAddInput.Controls.Add(this.radioButtonNone);
            this.panelAddInput.Controls.Add(this.radioButtonRenraku);
            this.panelAddInput.Controls.Add(this.radioButtonRyosho);
            this.panelAddInput.Controls.Add(this.labelDantaiCode);
            this.panelAddInput.Controls.Add(this.verifyBoxHenreiOther);
            this.panelAddInput.Controls.Add(this.verifyBoxDantaiCode);
            this.panelAddInput.Controls.Add(this.label4);
            this.panelAddInput.Controls.Add(this.labelDantaiName);
            this.panelAddInput.Controls.Add(this.verifyBoxDantaiName);
            this.panelAddInput.Controls.Add(this.label5);
            this.panelAddInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddInput.Location = new System.Drawing.Point(5, 405);
            this.panelAddInput.Name = "panelAddInput";
            this.panelAddInput.Size = new System.Drawing.Size(740, 282);
            this.panelAddInput.TabIndex = 0;
            // 
            // radioButtonOther
            // 
            this.radioButtonOther.AutoSize = true;
            this.radioButtonOther.Location = new System.Drawing.Point(317, 149);
            this.radioButtonOther.Name = "radioButtonOther";
            this.radioButtonOther.Size = new System.Drawing.Size(54, 16);
            this.radioButtonOther.TabIndex = 7;
            this.radioButtonOther.TabStop = true;
            this.radioButtonOther.Text = "その他";
            this.radioButtonOther.UseVisualStyleBackColor = true;
            this.radioButtonOther.CheckedChanged += new System.EventHandler(this.radioButtonOther_CheckedChanged);
            // 
            // radioButtonNone
            // 
            this.radioButtonNone.AutoSize = true;
            this.radioButtonNone.Location = new System.Drawing.Point(267, 149);
            this.radioButtonNone.Name = "radioButtonNone";
            this.radioButtonNone.Size = new System.Drawing.Size(42, 16);
            this.radioButtonNone.TabIndex = 6;
            this.radioButtonNone.TabStop = true;
            this.radioButtonNone.Text = "なし";
            this.radioButtonNone.UseVisualStyleBackColor = true;
            // 
            // radioButtonRenraku
            // 
            this.radioButtonRenraku.AutoSize = true;
            this.radioButtonRenraku.Location = new System.Drawing.Point(189, 149);
            this.radioButtonRenraku.Name = "radioButtonRenraku";
            this.radioButtonRenraku.Size = new System.Drawing.Size(70, 16);
            this.radioButtonRenraku.TabIndex = 5;
            this.radioButtonRenraku.TabStop = true;
            this.radioButtonRenraku.Text = "連絡済み";
            this.radioButtonRenraku.UseVisualStyleBackColor = true;
            // 
            // radioButtonRyosho
            // 
            this.radioButtonRyosho.AutoSize = true;
            this.radioButtonRyosho.Location = new System.Drawing.Point(111, 149);
            this.radioButtonRyosho.Name = "radioButtonRyosho";
            this.radioButtonRyosho.Size = new System.Drawing.Size(70, 16);
            this.radioButtonRyosho.TabIndex = 4;
            this.radioButtonRyosho.TabStop = true;
            this.radioButtonRyosho.Text = "了承済み";
            this.radioButtonRyosho.UseVisualStyleBackColor = true;
            // 
            // labelDantaiCode
            // 
            this.labelDantaiCode.Location = new System.Drawing.Point(28, 53);
            this.labelDantaiCode.Name = "labelDantaiCode";
            this.labelDantaiCode.Size = new System.Drawing.Size(62, 12);
            this.labelDantaiCode.TabIndex = 9;
            this.labelDantaiCode.Text = "口座番号";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(28, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "返戻付箋末尾";
            // 
            // labelDantaiName
            // 
            this.labelDantaiName.Location = new System.Drawing.Point(28, 102);
            this.labelDantaiName.Name = "labelDantaiName";
            this.labelDantaiName.Size = new System.Drawing.Size(45, 12);
            this.labelDantaiName.TabIndex = 1;
            this.labelDantaiName.Text = "団体名";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "入力項目";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(5, 687);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(740, 30);
            this.panel1.TabIndex = 2;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(551, 5);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 3;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelStatusOCR
            // 
            this.labelStatusOCR.AutoSize = true;
            this.labelStatusOCR.Location = new System.Drawing.Point(19, 708);
            this.labelStatusOCR.Name = "labelStatusOCR";
            this.labelStatusOCR.Size = new System.Drawing.Size(0, 12);
            this.labelStatusOCR.TabIndex = 23;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Location = new System.Drawing.Point(0, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(268, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // verifyBoxHenreiOther
            // 
            this.verifyBoxHenreiOther.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHenreiOther.Enabled = false;
            this.verifyBoxHenreiOther.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHenreiOther.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHenreiOther.Location = new System.Drawing.Point(377, 145);
            this.verifyBoxHenreiOther.Name = "verifyBoxHenreiOther";
            this.verifyBoxHenreiOther.NewLine = false;
            this.verifyBoxHenreiOther.Size = new System.Drawing.Size(159, 23);
            this.verifyBoxHenreiOther.TabIndex = 8;
            this.verifyBoxHenreiOther.TextV = "";
            // 
            // verifyBoxDantaiCode
            // 
            this.verifyBoxDantaiCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDantaiCode.Enabled = false;
            this.verifyBoxDantaiCode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDantaiCode.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDantaiCode.Location = new System.Drawing.Point(111, 46);
            this.verifyBoxDantaiCode.Name = "verifyBoxDantaiCode";
            this.verifyBoxDantaiCode.NewLine = false;
            this.verifyBoxDantaiCode.ReadOnly = true;
            this.verifyBoxDantaiCode.Size = new System.Drawing.Size(130, 23);
            this.verifyBoxDantaiCode.TabIndex = 10;
            this.verifyBoxDantaiCode.TextV = "";
            // 
            // verifyBoxDantaiName
            // 
            this.verifyBoxDantaiName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDantaiName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDantaiName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDantaiName.Location = new System.Drawing.Point(111, 95);
            this.verifyBoxDantaiName.Name = "verifyBoxDantaiName";
            this.verifyBoxDantaiName.NewLine = false;
            this.verifyBoxDantaiName.Size = new System.Drawing.Size(425, 23);
            this.verifyBoxDantaiName.TabIndex = 2;
            this.verifyBoxDantaiName.TextV = "";
            this.verifyBoxDantaiName.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.scrollPictureControl1.Location = new System.Drawing.Point(5, 5);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(740, 400);
            this.scrollPictureControl1.TabIndex = 0;
            this.scrollPictureControl1.TabStop = false;
            // 
            // MediveryDantaiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.splitter2);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "MediveryDantaiForm";
            this.Text = "団体名登録フォーム";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MediveryDantaiForm_FormClosed);
            this.Shown += new System.EventHandler(this.MediveryForm_Shown);
            this.SizeChanged += new System.EventHandler(this.MediveryForm_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MediveryForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MediveryForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelDatalist.ResumeLayout(false);
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelAddInput.ResumeLayout(false);
            this.panelAddInput.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TextBox textDispM;
        private System.Windows.Forms.TextBox textDispY;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelImageName;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label labelStatusOCR;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Panel panelAddInput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelDantaiName;
        private VerifyBox verifyBoxDantaiName;
        private System.Windows.Forms.Label labelDantaiCode;
        private VerifyBox verifyBoxDantaiCode;
        private System.Windows.Forms.Button buttonNextImage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButtonOther;
        private System.Windows.Forms.RadioButton radioButtonNone;
        private System.Windows.Forms.RadioButton radioButtonRenraku;
        private System.Windows.Forms.RadioButton radioButtonRyosho;
        private VerifyBox verifyBoxHenreiOther;
    }
}