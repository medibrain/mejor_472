﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Medivery
{

    /// <summary>
    /// 20210216161620 furukawa medivery管理テーブル変更に伴う、XML管理型MediveryForm
    /// </summary>
    public partial class MediveryXMLForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();

        private PostalCodeForm pcForm;
        private List<PostalCode> selectedHistory = new List<PostalCode>();
        private int cym;
        private bool clinicStorage;
        private Clinic clinic;
        private string prevTodofuken;
        private string prevSikutyoson;
        float baseRatio = 0.4f;

        protected override Control inputPanel => panelRight;


        //過去に入力したapp        
        List<App> lstAppForInputted;


        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posMediYM, posHnum, posHname, posPname, posBirthday, posHzip,
            posHaddress, posCountedDays, posDates, posTotal, posPartial,
            posCharge, posSname, posFvisittimes, posPaymentcode, posDrName,
            posPsex,//20210430094623 furukawa 性別追加
            posAccName,    //20210510115947 furukawa口座名義名
            posDrNum;//20210706093254 furukawa 登録記号番号追加
                     



        Control[] mediymConts, hnumConts, hnameConts, pnameConts, birthdayConts,
            hzipConts, haddressConts, counteddaysConts, datesConts, totalConts,
            partialConts, chargeConts, snameConts, fvisittimesConts, paymentcodeConts,
            drnameConts, psexConts,//20210430093755 furukawa 性別追加
            accnameConts,//20210510120100 furukawa 口座名義名
            drnumConts;//20210706093337 furukawa 登録記号番号追加

       

        public MediveryXMLForm(int cym, List<App> list, bool clinicStorage = false)
        {
            InitializeComponent();

            //設定テーブルロード                        
            if (!MediveryXML.GetMediveryList(Insurer.CurrrentInsurer.InsurerID))
            {
                this.Close();
                return;
            }
            this.cym = cym;
            this.clinicStorage = clinicStorage;

            mediymConts = new Control[] { verifyBoxMediY, verifyBoxMediM };
            hnumConts = new Control[] { verifyBoxHnum };
            hnameConts = new Control[] { verifyBoxHname };
            pnameConts = new Control[] { verifyBoxPname };
            birthdayConts = new Control[] { verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD };
            hzipConts = new Control[] { verifyBoxHzip };
            haddressConts = new Control[] { verifyBoxHaddress };
            counteddaysConts = new Control[] { verifyBoxCountedDays };
            datesConts = new Control[] { verifyBoxDates };
            totalConts = new Control[] { verifyBoxTotal };
            partialConts = new Control[] { verifyBoxPartial };
            chargeConts = new Control[] { verifyBoxCharge };
            snameConts = new Control[] { verifyBoxSname };
            fvisittimesConts = new Control[] { verifyBoxFvisittimes };
            paymentcodeConts = new Control[] { verifyBoxPaymentcode };
            drnameConts = new Control[] { verifyBoxDrName };

            psexConts = new Control[] { verifyBoxPsex };//20210430093847 furukawa 性別追加             
            accnameConts = new Control[] { verifyBoxAccName };//20210510120716 furukawa 口座名義名            
            drnumConts = new Control[] { verifyBoxDrNum };//20210706100712 furukawa 登録記号番号追加
                                                          

            //enterイベント動的割り当て
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            bsApp.DataSource = list;

            //診療日の成型イベント
            verifyBoxDates.Leave += VerifyBoxDates_Leave;
            verifyBoxDates.TextBoxV_Leave += VerifyBoxCountedDays_TextBoxV_Leave;

            

            //20211112130047 furukawa st ////////////////////////
            //1年前から先月までのデータを取得
            
            lstAppForInputted = App.GetAppsWithWhere($"where a.cym between {DateTimeEx.Int6YmAddMonth(cym,-12)} and  {DateTimeEx.Int6YmAddMonth(cym, -1)}  " +
                $"and a.aapptype>0 and a.pname<>''");
            lstAppForInputted.Sort((x, y) => y.CYM.CompareTo(x.CYM));
            //  過去に入力したappを取得
            //  lstAppForInputted = App.GetAppsWithWhere("where trim(a.hname) <>'' ");
            //20211112130047 furukawa ed ////////////////////////

        }



        private bool datesStringAdjust(ref string s)
        {
            var ds = s.Split(new[] { ' ', ',', '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (ds.Length < 1) return false;

            var l = new List<int>();
            int last = 0;

            foreach (var item in ds)
            {
                int.TryParse(item, out int i);
                if (i <= last || 31 < i) return false;
                l.Add(i);
                last = i;
            }

            s = string.Join(" ", l);
            return true;
        }

    

        /// <summary>
        /// コントロール初期化
        /// </summary>
        private void initControl()
        {

            Dictionary<string, MediveryXML.MediverySetting> dic = MediveryXML.GetMediVery((int)Insurer.CurrrentInsurer.InsurerID);

            MediveryXML.MediverySetting mvSetting = null;
            //MediveryXML.MediverySetting mvSetting = dic[MediveryXML.FIELD_NAME_MEDIYM];

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_MEDIYM))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_MEDIYM];

                if (mvSetting.Use)
                {
                    labelMediYM.Visible = mvSetting.Use;
                    verifyBoxMediY.Visible = mvSetting.Use;
                    verifyBoxMediM.Visible = mvSetting.Use;
                    labelMediY.Visible = mvSetting.Use;
                    labelMediM.Visible = mvSetting.Use;
                    verifyBoxMediY.Tag = mvSetting.FieldName;
                    verifyBoxMediM.Tag = mvSetting.FieldName;
                    labelMediYM.Text = mvSetting.Label;
                    posMediYM = new Point(mvSetting.LocationX, mvSetting.LocationY);
                }
            }

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_HNUM))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_HNUM];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelHnum.Visible = true;
                        verifyBoxHnum.Visible = true;
                        verifyBoxHnum.Tag = mvSetting.FieldName;
                        labelHnum.Text = mvSetting.Label;
                        posHnum = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }

                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_HNAME))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_HNAME];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelHname.Visible = true;
                        verifyBoxHname.Visible = true;
                        verifyBoxHname.Tag = mvSetting.FieldName;
                        labelHname.Text = mvSetting.Label;
                        posHname = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_PNAME))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_PNAME];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelPname.Visible = true;
                        verifyBoxPname.Visible = true;
                        verifyBoxPname.Tag = mvSetting.FieldName;
                        labelPname.Text = mvSetting.Label;
                        posPname = new Point(mvSetting.LocationX, mvSetting.LocationY);
                        if (dic[MediveryXML.FIELD_NAME_HNAME].Use) labelHnameCopy.Visible = true;
                    }
                }

            }



            //20210430094710 furukawa st ////////////////////////
            //性別追加
            if (dic.ContainsKey(MediveryXML.FIELD_NAME_SEX))
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_SEX];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelPsex.Visible = true;
                        verifyBoxPsex.Visible = true;
                        verifyBoxPsex.Tag = mvSetting.FieldName;
                        labelPsex.Text = mvSetting.Label;
                        posPsex = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }
            //20210430094710 furukawa ed ////////////////////////


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_BIRTHDAY))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_BIRTHDAY];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelBirthday.Visible = true;
                        verifyBoxBE.Visible = true;
                        verifyBoxBY.Visible = true;
                        verifyBoxBM.Visible = true;
                        verifyBoxBD.Visible = true;
                        labelBirthdayGengo.Visible = true;
                        labelBirthdayYear.Visible = true;
                        labelBirthdayMonth.Visible = true;
                        labelBirthdayDate.Visible = true;
                        verifyBoxBE.Tag = mvSetting.FieldName;
                        verifyBoxBY.Tag = mvSetting.FieldName;
                        verifyBoxBM.Tag = mvSetting.FieldName;
                        verifyBoxBD.Tag = mvSetting.FieldName;
                        labelBirthday.Text = mvSetting.Label;
                        posBirthday = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_HZIP))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_HZIP];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelHzip.Visible = true;
                        verifyBoxHzip.Visible = true;
                        buttonZipSearch.Visible = true;
                        verifyBoxHzip.Tag = mvSetting.FieldName;
                        labelHzip.Text = mvSetting.Label;
                        posHzip = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_HADDRESS))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_HADDRESS];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelHaddress.Visible = true;
                        verifyBoxHaddress.Visible = true;
                        buttonZipSearch.Visible = true;
                        verifyBoxHaddress.Tag = mvSetting.FieldName;
                        labelHaddress.Text = mvSetting.Label;
                        posHaddress = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }



            if (dic.ContainsKey(MediveryXML.FIELD_NAME_COUNTEDDAYS))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_COUNTEDDAYS];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelCountedDays.Visible = true;
                        verifyBoxCountedDays.Visible = true;
                        verifyBoxCountedDays.Tag = mvSetting.FieldName;
                        labelCountedDays.Text = mvSetting.Label;
                        posCountedDays = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }



            if (dic.ContainsKey(MediveryXML.FIELD_NAME_TOTAL))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_TOTAL];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelTotal.Visible = true;
                        verifyBoxTotal.Visible = true;
                        verifyBoxTotal.Tag = mvSetting.FieldName;
                        labelTotal.Text = mvSetting.Label;
                        posTotal = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_PARTIAL))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_PARTIAL];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelPartial.Visible = true;
                        verifyBoxPartial.Visible = true;
                        verifyBoxPartial.Tag = mvSetting.FieldName;
                        labelPartial.Text = mvSetting.Label;
                        posPartial = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }



            if (dic.ContainsKey(MediveryXML.FIELD_NAME_CHARGE))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_CHARGE];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelCharge.Visible = true;
                        verifyBoxCharge.Visible = true;
                        verifyBoxCharge.Tag = mvSetting.FieldName;
                        labelCharge.Text = mvSetting.Label;
                        posCharge = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_SNAME))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_SNAME];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelSname.Visible = true;
                        verifyBoxSname.Visible = true;
                        verifyBoxSname.Tag = mvSetting.FieldName;
                        labelSname.Text = mvSetting.Label;
                        posSname = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_FVISITTIMES))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_FVISITTIMES];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelFvisittimes.Visible = true;
                        verifyBoxFvisittimes.Visible = true;
                        verifyBoxFvisittimes.Tag = mvSetting.FieldName;
                        labelFvisittimes.Text = mvSetting.Label;
                        posFvisittimes = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_PAYMENTCODE))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_PAYMENTCODE];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelPaymentcode.Visible = true;
                        verifyBoxPaymentcode.Visible = true;
                        verifyBoxPaymentcode.Tag = mvSetting.FieldName;
                        labelPaymentcode.Text = mvSetting.Label;
                        posPaymentcode = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }


            if (dic.ContainsKey(MediveryXML.FIELD_NAME_DRNAME))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_DRNAME];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelDrName.Visible = true;
                        verifyBoxDrName.Visible = true;
                        verifyBoxDrName.Tag = mvSetting.FieldName;
                        labelDrName.Text = mvSetting.Label;
                        posDrName = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }
            }

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_DATES))//20210430105317 furukawa xmlに記載がない場合は無いものとして扱う。無いときのエラー回避
            {
                mvSetting = dic[MediveryXML.FIELD_NAME_DATES];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelDate.Visible = true;
                        verifyBoxDates.Visible = true;
                        verifyBoxDates.Tag = mvSetting.FieldName;
                        labelDate.Text = mvSetting.Label;
                        posDates = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }

            }

            //20210510120927 furukawa st ////////////////////////
            //口座名義名
            if (dic.ContainsKey(MediveryXML.FIELD_NAME_ACCNAME))

            {
                mvSetting = dic[MediveryXML.FIELD_NAME_ACCNAME];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelAccname.Visible = true;
                        verifyBoxAccName.Visible = true;
                        verifyBoxAccName.Tag = mvSetting.FieldName;
                        labelAccname.Text = mvSetting.Label;
                        posAccName = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }

            }

            //20210510120927 furukawa ed ////////////////////////

            //20210706091349 furukawa st ////////////////////////
            //登録記号番号追加

            if (dic.ContainsKey(MediveryXML.FIELD_NAME_DRNUM))

            {
                mvSetting = dic[MediveryXML.FIELD_NAME_DRNUM];

                if (mvSetting.Use)
                {
                    if ((firstTime && mvSetting.FirstInput) || !firstTime)
                    {
                        labelDrNum.Visible = true;
                        lblDrCode01.Visible = true;
                        verifyBoxDrNum.Visible = true;
                        verifyBoxDrNum.Tag = mvSetting.FieldName;
                        labelDrNum.Text = mvSetting.Label;
                        posDrNum = new Point(mvSetting.LocationX, mvSetting.LocationY);
                    }
                }

            }
            //20210706091349 furukawa ed ////////////////////////

        }




        /// <summary>
        /// 20201110134316 furukawa 過去の個人情報をヒホバンで取得
        /// 個人情報セット
        /// </summary>
        private void setPS(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);

            PastData.PersonalData p = new PastData.PersonalData();
            p = PastData.PersonalData.SelectRecordOne($"hnum='{app.HihoNum}'");
            if (p == null) return;

            if (
                verifyBoxHnum.Text != string.Empty ||
                verifyBoxHname.Text != string.Empty ||
                verifyBoxHzip.Text != string.Empty ||
                verifyBoxHaddress.Text != string.Empty ||
                verifyBoxPname.Text != string.Empty ||
                verifyBoxBY.Text != string.Empty ||
                verifyBoxBM.Text != string.Empty ||
                verifyBoxBE.Text != string.Empty ||
                verifyBoxBD.Text != string.Empty ||
                verifyBoxPsex.Text != string.Empty || //20210430095452 furukawa性別追加
                verifyBoxAccName.Text != string.Empty//20210510121054 furukawa 口座名義名

                )
            {
                if (System.Windows.Forms.MessageBox.Show(
                    "上書きしますか？",
                    Application.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }

            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HNUM].Use) verifyBoxHnum.Text = p.hnum;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HNAME].Use) verifyBoxHname.Text = p.hname;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HZIP].Use) verifyBoxHzip.Text = p.hzip;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HADDRESS].Use) verifyBoxHaddress.Text = p.haddress;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_PNAME].Use) verifyBoxPname.Text = p.pname;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_BIRTHDAY].Use) setDateValue(p.pbirthday, firstTime, nv, verifyBoxBY, verifyBoxBM, verifyBoxBE, verifyBoxBD);
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SEX].Use) verifyBoxPsex.Text = p.psex.ToString();//20210510121238 furukawa 性別を個人情報の一部として呼び出す



        }

        /// <summary>
        /// 20201123092828 furukawa 施術所情報過去データリスト表示
        /// 施術所情報
        /// </summary>
        /// <param name="app"></param>
        private void setClinic(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);

            PastData.ClinicData cd = new PastData.ClinicData();
            cd = PastData.ClinicData.SelectRecordOne($"hnum='{app.HihoNum}'");
            if (cd == null) return;

            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HNUM].Use) verifyBoxHnum.Text = cd.hnum;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Use) verifyBoxSname.Text = cd.clinicname;
            if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_DRNAME].Use) verifyBoxDrName.Text = cd.doctorname;

        }


        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済)) return;

            //20201123094054 furukawa st ////////////////////////
            //いちいちポップアップが出てうっとうしいからやめて

            ////20201110134250 furukawa st ////////////////////////
            ////過去データをロード            
            //setPS(app);
            //setClinic(app);
            ////20201110134250 furukawa ed ////////////////////////
            //20201123094054 furukawa ed ////////////////////////
        }




        /// <summary>
        /// //20201112101008 furukawa 選択リストを再表示する手段がないので作った 2021/11/11ボタンが非表示なので事実上使ってない
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectlist_Click(object sender, EventArgs e)
        {
            setPS((App)bsApp.Current);
            setClinic((App)bsApp.Current);
        }


        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;


            //コントロール別の説明文取得
            labelDescription.Text = MediveryXML.dicMvSetting[t.Tag.ToString()].DescriptionText ?? string.Empty;

            //コントロール別の画像倍率取得
            var d = (MediveryXML.dicMvSetting[t.Tag.ToString()].Ratio) == 0 ? 0.4f : MediveryXML.dicMvSetting[t.Tag.ToString()].Ratio;
            scrollPictureControl1.Ratio = d;


            Point p;
            if (mediymConts.Contains(sender)) p = posMediYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (hnameConts.Contains(sender)) p = posHname;
            else if (pnameConts.Contains(sender)) p = posPname;
            else if (psexConts.Contains(sender)) p = posPsex;//20210430100128 furukawa性別追加                                                            
            else if (birthdayConts.Contains(sender)) p = posBirthday;
            else if (hzipConts.Contains(sender)) p = posHzip;
            else if (haddressConts.Contains(sender)) p = posHaddress;
            else if (counteddaysConts.Contains(sender)) p = posCountedDays;
            else if (datesConts.Contains(sender)) p = posDates;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (partialConts.Contains(sender)) p = posPartial;
            else if (chargeConts.Contains(sender)) p = posCharge;
            else if (snameConts.Contains(sender)) p = posSname;
            else if (fvisittimesConts.Contains(sender)) p = posFvisittimes;
            else if (paymentcodeConts.Contains(sender)) p = posPaymentcode;
            else if (drnameConts.Contains(sender)) p = posDrName;
            else if (accnameConts.Contains(sender)) p = posAccName;//20210510121359 furukawa 口座名義の座標
            else if (drnumConts.Contains(sender)) p = posDrNum;//20210706100122 furukawa 登録記号番号追加
                                                               

            else return;

            scrollPictureControl1.ScrollPosition = p;

            //入力欄が住所の場合、入力カーソルを文字列の最後に持ってくる
            if ((t.Tag.ToString() ?? "") == MediveryXML.FIELD_NAME_HADDRESS &&
                t.Text != "")
            {
                t.Select(t.Text.Length, 0);
            }

        }


        /// <summary>
        /// DB登録
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;
            bool isEmpty(string s) => string.IsNullOrWhiteSpace(s);
            bool isZero(int i) => i == 0;
            bool checkName(string name)
            {
                if (string.IsNullOrWhiteSpace(name)) return false;//空白
                var names = name.Split('　');
                if (names.Count() < 2) return false;//全角スペース区切りなし
                return true;
            }

            foreach (var item in inputControls)
            {
                if (!(item is VerifyBox vb)) continue;
                if (!item.Visible) continue;
                string value = vb.Text;

                string strFld = (string)vb.Tag;

                if (strFld == null) continue;

                //登録値と入力値が空白の場合はアップデートしない
                switch (strFld)
                {
                    case MediveryXML.FIELD_NAME_MEDIYM:
                        if (isEmpty(verifyBoxMediY.Text) && isEmpty(verifyBoxMediM.Text))
                        {
                            setStatus(verifyBoxMediY, false);
                            setStatus(verifyBoxMediM, false);
                        }
                        else
                        {
                            int y = verifyBoxMediY.GetIntValue();
                            int m = verifyBoxMediM.GetIntValue();
                            var result = y > 0 && m > 0;
                            if (result)
                            {
                                app.MediYear = y;
                                app.MediMonth = m;
                            }
                            setStatus(verifyBoxMediY, !result);
                            setStatus(verifyBoxMediM, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_HNUM:
                        if (isEmpty(app.HihoNum) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = !string.IsNullOrEmpty(value);
                            if (result) app.HihoNum = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_HNAME:
                        if (isEmpty(app.HihoName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = checkName(value);
                            if (result) app.HihoName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_PNAME:
                        if (isEmpty(app.PersonName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = checkName(value);
                            if (result) app.PersonName = value;
                            setStatus(vb, !result);
                        }
                        break;


                    //20210430100316 furukawa st ////////////////////////
                    //性別追加

                    case MediveryXML.FIELD_NAME_SEX:
                        if (isZero(app.Sex) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int d);
                            if (result) app.Sex = d;
                            setStatus(vb, !result);
                        }
                        break;
                    //20210430100316 furukawa ed ////////////////////////



                    case MediveryXML.FIELD_NAME_BIRTHDAY:
                        if (app.Birthday == DateTime.MinValue &&
                            isEmpty(verifyBoxBE.Text) &&
                            isEmpty(verifyBoxBY.Text) &&
                            isEmpty(verifyBoxBM.Text) &&
                            isEmpty(verifyBoxBD.Text))
                        {
                            setStatus(verifyBoxBE, false);
                            setStatus(verifyBoxBY, false);
                            setStatus(verifyBoxBM, false);
                            setStatus(verifyBoxBD, false);
                        }
                        else
                        {
                            var dt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);
                            var result = dt != DateTimeEx.DateTimeNull;
                            if (result) app.Birthday = dt;
                            setStatus(verifyBoxBE, !result);
                            setStatus(verifyBoxBY, !result);
                            setStatus(verifyBoxBM, !result);
                            setStatus(verifyBoxBD, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_HZIP:
                        if (isEmpty(app.HihoZip) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            result &= value.Length == 7;
                            app.HihoZip = value;
                            setStatus(vb, false);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_HADDRESS:
                        if (isEmpty(app.HihoAdd) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            app.HihoAdd = value;
                            setStatus(vb, false);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_COUNTEDDAYS:
                        if (isZero(app.CountedDays) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int d);
                            if (result) app.CountedDays = d;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_DATES:
                        var datesRes = datesStringAdjust(ref value);
                        setStatus(vb, !datesRes);
                        if (datesRes) app.TaggedDatas.Dates = value;

                        break;

                    case MediveryXML.FIELD_NAME_TOTAL:
                        if (isZero(app.Total) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int t);
                            if (result) app.Total = t;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_PARTIAL:
                        if (isZero(app.Partial) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int p);
                            if (result) app.Partial = p;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_CHARGE:
                        if (isZero(app.Charge) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int c);
                            if (result) app.Charge = c;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_SNAME:
                        if (isEmpty(app.ClinicName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != "";
                            if (result) app.ClinicName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_FVISITTIMES:
                        if (isZero(app.VisitTimes) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int t);
                            if (result) app.VisitTimes = t;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_PAYMENTCODE:
                        if (isEmpty(app.AccountNumber) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = int.TryParse(value, out int c);
                            if (result) app.AccountNumber = value;
                            setStatus(vb, !result);
                        }
                        break;

                    case MediveryXML.FIELD_NAME_DRNAME:
                        if (isEmpty(app.DrName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != null;
                            if (result) app.DrName = value;
                            setStatus(vb, !result);
                        }
                        break;

                    //20210510162636 furukawa st ////////////////////////
                    //口座名義

                    case MediveryXML.FIELD_NAME_ACCNAME:
                        if (isEmpty(app.AccountName) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != null;
                            if (result) app.AccountName = value;
                            setStatus(vb, !result);
                        }
                        break;
                    //20210510162636 furukawa ed ////////////////////////

                    //20210706091313 furukawa st ////////////////////////
                    //登録記号番号追加

                    case MediveryXML.FIELD_NAME_DRNUM:
                        if (isEmpty(app.DrNum) && isEmpty(value)) setStatus(vb, false);
                        else
                        {
                            var result = value != null;
                            if (result) app.DrNum = value;
                            setStatus(vb, !result);
                        }
                        break;
                    //20210706091313 furukawa ed ////////////////////////



                    default:
                        MessageBox.Show("不正なコントロールが存在します。\r\nシステム担当者を呼んでください。", "エラー");
                        return false;
                }
            }

            return !hasError;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済)) return true;

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //施術所名が登録値と異なる場合の処理
            var clinicUpdated = false;
            if (clinicStorage && MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Use && clinic != null)
            {
                if (!string.IsNullOrEmpty(app.ClinicName) && clinic.Name != app.ClinicName)
                {
                    var update = MessageBox.Show(
                        $"{MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Label}が登録されている値（{clinic.Name}）と異なっています。\r\n上書きしますか？",
                        "変更確認",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                    if (update != DialogResult.Yes)
                    {
                        verifyBoxSname.Focus();
                        verifyBoxSname.SelectAll();
                        return false;
                    }
                    clinic.Name = app.ClinicName;
                    clinic.Update_Date = DateTime.Today;

                    clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);

                    clinic.Verified = false;
                    clinic.Update();

                    //同じ施術所番号を使用しているものをすべて再チェック（今開いている一覧上のみ）
                    var list = ((List<App>)bsApp.DataSource).FindAll(a => a.StatusFlagCheck(StatusFlag.入力済));
                    var updateList = new List<App>();
                    updateList.AddRange(list.FindAll(a =>
                        a.Aid != app.Aid && a.DrNum == app.DrNum && a.ClinicName != "" && a.ClinicName != app.ClinicName));

                    if (updateList.Count > 0)
                    {


                        var multiUpdate = MessageBox.Show(
                            $"{DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}{(cym % 100).ToString("00")}" +
                            $"分の追加ベリファイ済の申請書のうち同じ施術所番号を使用しているもので、" +
                            $"{MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Label}が登録されている値（{clinic.Name}）と異なるものが{updateList.Count}件存在します。\r\n" +
                            $"それらの内容も変更してもよろしいですか？",
                            "変更確認",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                        if (multiUpdate == DialogResult.Yes)
                        {
                            using (var tran = DB.Main.CreateTransaction())
                            {
                                foreach (var item in updateList)
                                {
                                    item.ClinicName = app.ClinicName;
                                    if (!item.UpdateMedivery(App.UPDATE_TYPE.Null, tran))
                                    {
                                        tran.Rollback();
                                        MessageBox.Show("変更に失敗しました。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }
                                }
                                tran.Commit();
                            }
                        }
                    }
                    //後述のVerified更新を回避する
                    clinicUpdated = true;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.Null : App.UPDATE_TYPE.SecondInput;

                //フラグ整理
                if (firstTime)
                {
                    app.StatusFlagRemove(StatusFlag.追加ベリ済);
                    app.StatusFlagSet(StatusFlag.追加入力済);
                    app.AdditionalUid1 = User.CurrentUser.UserID;
                }
                else
                {
                    app.StatusFlagSet(StatusFlag.追加ベリ済);
                    app.AdditionalUid2 = User.CurrentUser.UserID;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                tran.Commit();
            }

            //施術所番号が存在し、施術所名の入力がある場合
            if (clinicStorage && MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Use && !string.IsNullOrEmpty(app.ClinicName))
            {
                if (clinic == null)
                {
                    //初登場だった場合は目録へ追加
                    clinic = new Clinic();
                    long.TryParse(app.DrNum, out long code);
                    clinic.Code = code;
                    clinic.Name = app.ClinicName;
                    clinic.Insert_Date = DateTime.Today;

                    clinic.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);

                    clinic.Verified = false;
                    clinic.Insert();
                }
                else if (!clinicUpdated && clinic.Name == app.ClinicName && !clinic.Verified)
                {
                    //登録値と同じで確定フラグが立っていない場合は上書き
                    clinic.Update_Date = DateTime.Today;

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                    //clinic.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    clinic.Verified = true;
                    clinic.UpdateVerified();
                }
            }
            return true;
        }

        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                void setPostal(PostalCode pc)
                {
                    if (pc == null) return;
                    if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HZIP].Use) verifyBoxHzip.Text = pc.Postal_Code.ToString("0000000");
                    if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HADDRESS].Use) verifyBoxHaddress.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    prevTodofuken = pc.Todofuken;
                    prevSikutyoson = pc.Sichokuson;
                    verifyBoxHaddress.Focus();
                    verifyBoxHaddress.Select(verifyBoxHaddress.Text.Length, 0);
                    selectedHistory.Add(pc);
                }

                var x = panelRight.Location.X + panelAddInput.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = this.Height - 620;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible) pcForm.Show(this);
            else pcForm.MoveDefaultFocus();
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.AdditionalUid1) +
                "\r\n入力2:  " + User.GetUserName(app.AdditionalUid2);

            //施術所名が必要なら、目録から施術所（名）情報の取得
            clinic = MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_SNAME].Use && clinicStorage ?
                Clinic.GetClinic(app.DrNum) : null;

            if (app.StatusFlagCheck(StatusFlag.追加入力済)) setValues(app);

            //画像の表示
            setImage(app);
            changedReset(app);

            //過去に入力した個人情報を表示
            //未処理だけにしようと思ったが、どんなフラグでくるか不明なので全部通すことにする
            //if (app.StatusFlagCheck(StatusFlag.未処理))
            setInputtedData(app);

        }



        //過去に入力した個人情報を表示       
        private void setInputtedData(App app)
        {
            //20210528100245 furukawa st ////////////////////////
            //先に判定して無駄なロードしない

            //２回め入力の自動入力はしない
            if (!firstTime) return;
            //20210528100245 furukawa ed ////////////////////////

            //先に被保番、生年月日、性別でapp取得

            StringBuilder sb = new StringBuilder();
            sb.Append(" where ");
            sb.AppendFormat("a.hnum='{0}' ", app.HihoNum);
            sb.AppendFormat("and a.pbirthday='{0}' ", app.Birthday);
            sb.AppendFormat("and a.psex='{0}' ", app.Sex);
            sb.AppendFormat("and a.sregnumber='{0}' ", app.DrNum); //20190619092816 furukawa 施術師コード追加

            
            sb.Append("and a.hname<>'' ");
            sb.Append($"and a.aapptype={(int)app.AppType}");


            lstAppForInputted = App.GetAppsWithWhere(sb.ToString());


            //処理年月で降順            
            lstAppForInputted.Sort((x, y) => y.CYM.CompareTo(x.CYM));


            //20210528100328 furukawa st ////////////////////////
            //20210528100245に移動

            //      ２回め入力の自動入力はしない
            //      if (!firstTime) return;
            //20210528100328 furukawa ed ////////////////////////

            //複数ある場合はソートした最初に一致するレコードを選ぶ
            foreach (App apprec in lstAppForInputted)
            {

                if (apprec.HihoNum == app.HihoNum &&
                    apprec.Birthday == app.Birthday &&
                    apprec.DrNum == app.DrNum &&    //20190619092816 furukawa 施術師コード追加//
                    apprec.Sex == app.Sex)
                {

                    InsurerProcess(apprec);
                    break;
                }
            }
        }



        /// <summary>
        /// 保険者別のロード
        /// </summary>
        /// <param name="apprec"></param>
        private void InsurerProcess(App apprec)
        {
            try
            {
                //学校共済はまとめる
                if (Insurer.GetInsurerType(Insurer.CurrrentInsurer.InsurerID) == INSURER_TYPE.学校共済)
                {
                    if (verifyBoxHname.Text == string.Empty) verifyBoxHname.Text = apprec.HihoName;                                 //被保険者名
                    if (verifyBoxHaddress.Text == string.Empty) verifyBoxHaddress.Text = apprec.HihoAdd;                            //被保険者住所
                    if (verifyBoxHzip.Text == string.Empty) verifyBoxHzip.Text = apprec.HihoZip;                                    //被保険者郵便番号
                    if (verifyBoxSname.Text == string.Empty) verifyBoxSname.Text = clinic != null ? clinic.Name : string.Empty;     //施術所名


                    //生年月日
                    if (verifyBoxBY.Text == string.Empty) verifyBoxBY.Text = apprec.Birthday.ToJyearShortStr();
                    if (verifyBoxBM.Text == string.Empty) verifyBoxBM.Text = apprec.Birthday.Month.ToString();
                    if (verifyBoxBD.Text == string.Empty) verifyBoxBD.Text = apprec.Birthday.Day.ToString();
                }
                //それ以外の保険者
                else
                {
                    switch (Insurer.CurrrentInsurer.InsurerID)
                    {

                        case (int)InsurerID.SHARP_KENPO://シャープ健保
                            if (verifyBoxHname.Text == string.Empty) verifyBoxHname.Text = apprec.HihoName;         //被保険者名
                            if (verifyBoxPname.Text == string.Empty) verifyBoxPname.Text = apprec.PersonName;       //受療者名
                            if (verifyBoxDrName.Text == string.Empty) verifyBoxDrName.Text = apprec.DrName;         //柔整師名
                            if (verifyBoxSname.Text == string.Empty) verifyBoxSname.Text = apprec.ClinicName;       //施術所名

                            break;

                        default:

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
            }
        }



        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            //20190606175850 furukawa st ////////////////////////
            //画像パスが見つからない場合抜ける

            if (!System.IO.File.Exists(fn.ToString()))
            {
                labelImageName.Text = "画像が見つかりません";
                return;
            }
            //20190606175850 furukawa ed ////////////////////////

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = baseRatio;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posHname;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);
            setValue(verifyBoxMediY, app.MediYear, firstTime, nv);
            setValue(verifyBoxMediM, app.MediMonth, firstTime, nv);

            foreach (var item in inputControls)
            {
                if (!(item is VerifyBox vb)) continue;
                if (!item.Visible) continue;

                string strFld = (string)vb.Tag;
                if (strFld == string.Empty) continue;

                switch (strFld)
                {
                    case MediveryXML.FIELD_NAME_MEDIYM:
                        setValue(verifyBoxMediY, app.MediYear, firstTime, nv);
                        setValue(verifyBoxMediM, app.MediMonth, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_HNUM:
                        setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_HNAME:
                        setValue(verifyBoxHname, app.HihoName, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_PNAME:
                        setValue(verifyBoxPname, app.PersonName, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_SEX://20210430100527 furukawa 性別追加                                                    
                        setValue(verifyBoxPsex, app.Sex, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_BIRTHDAY:
                        setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                        setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                        setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                        setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_HZIP:
                        setValue(verifyBoxHzip, app.HihoZip, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_HADDRESS:
                        setValue(verifyBoxHaddress, app.HihoAdd, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_COUNTEDDAYS:
                        setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_DATES:
                        setValue(verifyBoxDates, app.TaggedDatas.Dates, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_TOTAL:
                        setValue(verifyBoxTotal, app.Total, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_PARTIAL:
                        setValue(verifyBoxPartial, app.Partial, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_CHARGE:
                        setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_SNAME:
                        var tmpNV = clinic == null || !clinic.Verified;  //目録なし || 確定してない => ベリファイ必要
                        var name = tmpNV ? app.ClinicName : clinic.Name;
                        setValue(verifyBoxSname, name, firstTime, tmpNV);
                        break;
                    case MediveryXML.FIELD_NAME_FVISITTIMES:
                        setValue(verifyBoxFvisittimes, app.VisitTimes, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_PAYMENTCODE:
                        setValue(verifyBoxPaymentcode, app.AccountNumber, firstTime, nv);
                        break;
                    case MediveryXML.FIELD_NAME_DRNAME:
                        setValue(verifyBoxDrName, app.DrName, firstTime, nv);
                        break;

                    //20210510122843 furukawa st ////////////////////////
                    //支払先（口座名義）                    
                    case MediveryXML.FIELD_NAME_ACCNAME:
                        setValue(verifyBoxAccName, app.AccountName, firstTime, nv);
                        break;
                    //20210510122843 furukawa ed ////////////////////////

                    //20210706091446 furukawa st ////////////////////////
                    //登録記号番号追加
                    
                    case MediveryXML.FIELD_NAME_DRNUM:
                        setValue(verifyBoxDrNum, app.DrNum, firstTime, nv);
                        break;
                        //20210706091446 furukawa ed ////////////////////////

                }
            }
        }


        #region オブジェクトイベント

        private void VerifyBoxDates_Leave(object sender, EventArgs e)
        {
            var s = verifyBoxDates.Text;
            datesStringAdjust(ref s);
            verifyBoxDates.Text = s;
        }

        private void VerifyBoxCountedDays_TextBoxV_Leave(object sender, EventArgs e)
        {
            var s = verifyBoxDates.TextV;
            datesStringAdjust(ref s);
            verifyBoxDates.TextV = s;
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void MediveryForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (pcForm != null && !pcForm.IsDisposed) pcForm.Dispose();
        }



        private void verifyBoxHzip_Leave(object sender, EventArgs e)
        {
            if (!MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HADDRESS].Use) return;

            //郵便番号と住所入力欄があれば自動で設定する
            if (int.TryParse(verifyBoxHzip.Text, out int zipcode))
            {
                var pc = PostalCode.GetPostalCode(zipcode);
                if (pc != null)
                {
                    var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    var curAdd = verifyBoxHaddress.Text;
                    if (!curAdd.StartsWith(add)) verifyBoxHaddress.Text = add;
                }
                else verifyBoxHaddress.Text = "";
            }
        }

        private void verifyBoxPname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー
            {
                if (MediveryXML.dicMvSetting[MediveryXML.FIELD_NAME_HNAME].Use)
                {
                    //被保険者氏名と受療者氏名入力欄があれば名前をコピーする
                    var cvb = (VerifyBox)sender;
                    cvb.Text = verifyBoxHname.Text;
                    //かつ、次のコントロールへ移る
                    SelectNextControl(verifyBoxPname, true, true, true, false);
                    return;
                }
            }
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (mediymConts.Any(c => c.Focused)) posMediYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (hnameConts.Any(c => c.Focused)) posHname = pos;
            else if (pnameConts.Any(c => c.Focused)) posPname = pos;
            else if (birthdayConts.Any(c => c.Focused)) posBirthday = pos;
            else if (hzipConts.Any(c => c.Focused)) posHzip = pos;
            else if (haddressConts.Any(c => c.Focused)) posHaddress = pos;
            else if (counteddaysConts.Any(c => c.Focused)) posCountedDays = pos;
            else if (datesConts.Any(c => c.Focused)) posDates = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (partialConts.Any(c => c.Focused)) posPartial = pos;
            else if (chargeConts.Any(c => c.Focused)) posCharge = pos;
            else if (snameConts.Any(c => c.Focused)) posSname = pos;
            else if (fvisittimesConts.Any(c => c.Focused)) posFvisittimes = pos;
            else if (paymentcodeConts.Any(c => c.Focused)) posPaymentcode = pos;
            else if (drnameConts.Any(c => c.Focused)) posDrName = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void MediveryForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            var list = (List<App>)bsApp.DataSource;

            //入力タイプ選択
            using (var f = new MediveryInputSelectForm(list))
            {
                if (f.ShowDialog() != DialogResult.OK)
                {
                    this.Close();
                    return;
                }

                this.firstTime = f.IsFirstTime;

                //１回目入力未完了だがベリファイできるものだけ先に行う
                if (!f.IsFirstTime && !f.IsFirstTimeComplete)
                    list = list.FindAll(a => a.StatusFlagCheck(StatusFlag.追加入力済));
            }

            //連番を付与
            int index = 1;
            list.ForEach(c => c.ViewIndex = index++);

            //グリッドに表示
            dataGridViewPlist.DataSource = bsApp;
            foreach (DataGridViewColumn c in dataGridViewPlist.Columns) c.Visible = false;

            dataGridViewPlist.Columns[nameof(App.ViewIndex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].Width = 30;
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].HeaderText = "No.";
            dataGridViewPlist.Columns[nameof(App.ViewIndex)].DisplayIndex = 0;
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 60;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.Aid)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusAdd)].DisplayIndex = 2;

            //入力欄初期化
            dataGridViewPlist.Focus();
            initControl();
            bsApp.ResetBindings(false);
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            bsApp.CurrentChanged += BsApp_CurrentChanged;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
            focusBack(false);


            //20201123112637 furukawa st ////////////////////////
            //入力しづらいので削除

            //setPS(app);
            //20201123112637 furukawa ed ////////////////////////

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
        #endregion


      
    }
}

