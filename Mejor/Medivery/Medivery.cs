﻿using System.Collections.Generic;
using System.Linq;

namespace Mejor.Medivery
{
    public class Medivery
    {
        public int InsurerID { get; set; }                  //保険者ID
        public bool MediYM { get; set; }                    //診療年月
        public bool Hnum { get; set; }                      //被保険者番号
        public bool Hname { get; set; }                     //被保険者名
        public bool Pname { get; set; }                     //受療者名
        public bool Birthday { get; set; }                  //受療者生年月日
        public bool Hzip { get; set; }                      //被保険者郵便番号
        public bool Haddress { get; set; }                  //被保険者住所
        public bool CountedDays { get; set; }               //施術日数
        public bool Total { get; set; }                     //合計金額
        public bool Partial { get; set; }                   //患者一時負担金
        public bool Charge { get; set; }                    //請求金額
        public bool Sname { get; set; }                     //医療機関名（施術所名
        public bool Fvisittimes { get; set; }               //往療回数
        public bool Paymentcode { get; set; }               //支払先コード
        public bool DrName { get; set; }                    //施術者名
        public bool Dates { get; set; } = false;            //診療日
        public string Description_CSV { get; set; }         //説明文等のカンマ区切り文字列

        private static Dictionary<int, Medivery> mvDic = new Dictionary<int, Medivery>();
        private Dictionary<string, Description> desDic = new Dictionary<string, Description>();

        public class Description
        {
            public const string DESC_NAME_MEDIYM = "mediym";                        //診療年月
            public const string DESC_NAME_HNUM = "hnum";                            //被保険者番号
            public const string DESC_NAME_HNAME = "hname";                          //被保険者名
            public const string DESC_NAME_PNAME = "pname";                          //受療者名
            public const string DESC_NAME_BIRTHDAY = "birthday";                    //受療者生年月日
            public const string DESC_NAME_HZIP = "hzip";                            //被保険者郵便番号
            public const string DESC_NAME_HADDRESS = "haddress";                    //被保険者住所
            public const string DESC_NAME_COUNTEDDAYS = "counteddays";              //施術日数
            public const string DESC_NAME_DATES = "dates";                          //診療日
            public const string DESC_NAME_TOTAL = "total";                          //合計金額
            public const string DESC_NAME_PARTIAL = "partial";                      //患者一時負担金
            public const string DESC_NAME_CHARGE = "charge";                        //請求金額
            public const string DESC_NAME_SNAME = "sname";                          //医療機関名（施術所名
            public const string DESC_NAME_FVISITTIMES = "fvisittimes";              //往療回数
            public const string DESC_NAME_PAYMENTCODE = "paymentcode";              //支払先コード
            public const string DESC_NAME_DRNAME = "drname";                        //施術者名


            //Description_CSVの中身
            public string Name { get; set; }         //コントロール名
            public string Label { get; set; }        //ラベルの表示文字列
            public string Text { get; set; }         //説明文
            private int locationX;
            private int locationY;
            public int LocationX { get { return locationX; } set { locationX = value == -1 ? 0 : value; } }     //コントロールの座標X
            public int LocationY { get { return locationY; } set { locationY = value == -1 ? 0 : value; } }     //コントロールの座標Y
            public bool FirstInput { get; set; }    //1回目入力するか否かフラグ
            public float Ratio { get; set; }        //画像倍率
        }


        /// <summary>
        /// 保険者別の追加入力項目リストを取得します。更新にも利用できます。
        /// </summary>
        /// <returns></returns>
        public static bool GetMediveryList()
        {
            var DBNAME = "jyusei";
            var curDBName = DB.GetMainDBName();
            try
            {
                if (curDBName != DBNAME) DB.SetMainDBName(DBNAME);

                mvDic.Clear();
                var sql = "SELECT * FROM medivery";
                var result = DB.Main.Query<Medivery>(sql);
                if (result != null)
                {
                    
                    result.ToList().ForEach(mv =>
                    {
                        mv.setDescriptions();
                        mvDic.Add(mv.InsurerID, mv);
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                if (curDBName != DBNAME) DB.SetMainDBName(curDBName);
            }
        }

        private void setDescriptions()
        {
            var itemName = "";
            var labelText = "";
            var descText = "";
            var locationX = "";
            var locationY = "";
            var firstInput = "";

            desDic.Clear();

            var records = Description_CSV.Split('\n');
            Description desc = null;
            foreach(var record in records)
            {                
                var fields = record.Split(',');
                if(fields.Count() >= 5)
                {
                    itemName = fields[0].Trim().Replace("\"", "");
                    labelText = fields[1].Trim().Replace("\"", "");
                    descText = fields[2].Trim().Replace("\"", "");
                    locationX = fields[3].Trim().Replace("\"", "");
                    locationY = fields[4].Trim().Replace("\"", "");
                    firstInput = "";
                    if (fields.Count() >= 6)
                        firstInput = fields[5].Trim().Replace("\"", "");

                    float f = 0;
                    if (fields.Count() >= 7)
                        float.TryParse(fields[6].Trim().Replace("\"", ""), out f);

                    switch (itemName)
                    {
                        case Description.DESC_NAME_MEDIYM:
                        case Description.DESC_NAME_HNUM:
                        case Description.DESC_NAME_HNAME:
                        case Description.DESC_NAME_PNAME:
                        case Description.DESC_NAME_BIRTHDAY:
                        case Description.DESC_NAME_HZIP:
                        case Description.DESC_NAME_HADDRESS:
                        case Description.DESC_NAME_COUNTEDDAYS:
                        case Description.DESC_NAME_DATES:
                        case Description.DESC_NAME_TOTAL:
                        case Description.DESC_NAME_PARTIAL:
                        case Description.DESC_NAME_CHARGE:
                        case Description.DESC_NAME_SNAME:
                        case Description.DESC_NAME_FVISITTIMES:
                        case Description.DESC_NAME_PAYMENTCODE:
                        case Description.DESC_NAME_DRNAME:
                            desc = new Description();
                            desc.Name = itemName;
                            desc.Label = labelText;
                            desc.Text = descText;
                            desc.LocationX = toInt(locationX);
                            desc.LocationY = toInt(locationY);
                            desc.FirstInput = toBool(firstInput);
                            desc.Ratio = f;
                            desDic.Add(itemName, desc);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// InsurerIDからMediveryインスタンスを取得します。
        /// 登録されていない場合はnullを返します。
        /// </summary>
        /// <param name="insurerID"></param>
        /// <returns></returns>
        public static Medivery GetMediVery(int insurerID)
        {
            Medivery mv = null;
            mvDic.TryGetValue(insurerID, out mv);
            return mv;
        }

        /// <summary>
        /// アイテム名から対応するコントロール制御情報を取得します。
        /// 対応するものが無い場合はnullを返します。
        /// </summary>
        /// <param name="itemname"></param>
        /// <returns></returns>
        public Description GetDescription(string itemname)
        {
            if (desDic.ContainsKey(itemname))
                return desDic[itemname];
            else
                return null;
        }

        private int toInt(string number)
        {
            int numberInt;
            if (int.TryParse(number, out numberInt)) return numberInt;
            return -1;
        }

        private bool toBool(string val)
        {
            if (bool.TryParse(val, out bool result)) return result;
            return true;
        }
    }
}
