﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Mejor.Medivery
{
    public partial class MediveryDantaiForm : Form
    {
        private const int FOCUS_TYPE_TOP = 1;
        private const int FOCUS_TYPE_ERROR = 2;
        private const int FOCUS_TYPE_NEXT = 3;

        private Insurer insurer;
        private int cym;
        private List<App> appList;

        private NextImageForm nextImageForm;

        private BindingSource bsApp = new BindingSource();

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posDantaiCode = new Point(100, 1200);
        Point posDantaiName = new Point(100, 900);
        Control[] dantaiCodeConts, dantaiNameConts;

        private List<VerifyBox> allBoxes { get; set; }
        private List<PostalCode> selectedHistory = new List<PostalCode>();

        public class AppHolder
        {
            public App app { get; set; }
            public int no { get; set; }
            public int aid { get; set; }
            public string flag { get; set; }
            public int flagInt { get; set; }
            public APP_TYPE apptype { get; set; }
            public string accountNumber { get; set; }
            public string accountName { get; set; }
            public Account account { get; set; }
        }

        public MediveryDantaiForm(Insurer insurer, int cym, List<App> list)
        {
            InitializeComponent();

            this.insurer = insurer;
            this.cym = cym;
            this.appList = list;

            //拡大縮小カテゴリ
            dantaiCodeConts = new Control[] { verifyBoxDantaiCode };
            dantaiNameConts = new Control[] { verifyBoxDantaiName };
        }

        //行選択時
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            setCurrent();

            //フォーカスをTabIndex先頭に
            moveFocus(FOCUS_TYPE_TOP);
        }

        //フォーム表示時
        private void MediveryForm_Shown(object sender, EventArgs e)
        {
            var list = appList;

            if (list.Count == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }            

            //リストを作成
            var holders = new List<AppHolder>();
            var count = 1;
            foreach (var item in list)
            {
                var account = Account.GetAccount(item.AccountNumber);

                var holder = new AppHolder();
                holder.app = item;
                holder.no = count++;
                holder.aid = item.Aid;
                holder.apptype = item.AppType;
                holder.account = account;
                holder.accountNumber = item.AccountNumber;
                holder.accountName = account?.Name;

                flagUpdate(holder);
                holders.Add(holder);
            }

            //データバインド
            bsApp.DataSource = holders;
            dataGridViewPlist.DataSource = bsApp;
            bsApp.CurrentChanged += BsApp_CurrentChanged;

            foreach (DataGridViewColumn item in dataGridViewPlist.Columns)
            {
                item.Visible = false;
            }
            dataGridViewPlist.Columns[nameof(AppHolder.no)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.no)].Width = 30;
            dataGridViewPlist.Columns[nameof(AppHolder.no)].HeaderText = "No";
            dataGridViewPlist.Columns[nameof(AppHolder.no)].DisplayIndex = 0;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].Width = 55;
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(AppHolder.aid)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewPlist.Columns[nameof(AppHolder.apptype)].DisplayIndex = 2;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].Width = 70;
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].HeaderText = "登録状況";
            dataGridViewPlist.Columns[nameof(AppHolder.flag)].DisplayIndex = 3;
            dataGridViewPlist.Columns[nameof(AppHolder.accountNumber)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.accountNumber)].Width = 70;
            dataGridViewPlist.Columns[nameof(AppHolder.accountNumber)].HeaderText = "口座番号";
            dataGridViewPlist.Columns[nameof(AppHolder.accountNumber)].DisplayIndex = 4;
            dataGridViewPlist.Columns[nameof(AppHolder.accountName)].Visible = true;
            dataGridViewPlist.Columns[nameof(AppHolder.accountName)].Width = 70;
            dataGridViewPlist.Columns[nameof(AppHolder.accountName)].HeaderText = "団体名";
            dataGridViewPlist.Columns[nameof(AppHolder.accountName)].DisplayIndex = 5;

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            textDispY.Text = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100).ToString();
            //textDispY.Text = DateTimeEx.GetHsYearFromAd(cym / 100).ToString();
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            textDispM.Text = (cym % 100).ToString();
            textBoxCount.Text = list.Count().ToString();

            this.Text += $" - Insurer: {insurer.InsurerName}";

            //入力欄取得
            this.allBoxes = getAllVerifyBox(true);

            //１行目を選択して表示
            dataGridViewPlist.CurrentCell = dataGridViewPlist[1, 0];//ここで選択しておかないとCurrentCellがnullになる
            setCurrent();//ここで明示的に呼ばないと表示されない

            //フォーカスをTabIndex先頭に
            moveFocus(FOCUS_TYPE_TOP);
        }

        //「×」を押されたときは一緒に子Formも閉じる
        private void MediveryDantaiForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (nextImageForm != null && !nextImageForm.IsDisposed) nextImageForm.Dispose();
        }

        //登録ボタン
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            updateAndSelectNext();
        }

        //ショートカットキー
        //F5：住所情報の表示
        private void MediveryForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                //　↓　現在アクティブな入力欄がベリファイエラー且つジャンプ先だった場合、
                //　↓　PerformClickだけではEnterイベントが発行されずベリファイ入力欄が表示されない
                buttonUpdate.Focus();//これが必要
                buttonUpdate.PerformClick();
            }
        }

        //EnterキーでTABキーの動作をさせる
        private void MediveryForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        //次の申請書ボタン
        private void buttonNextImage_Click(object sender, EventArgs e)
        {
            var appholder = (AppHolder)bsApp.Current;
            if (appholder == null) return;

            if (nextImageForm == null || nextImageForm.IsDisposed)
            {
                nextImageForm = new NextImageForm(appholder.app);
            }
            if (!nextImageForm.Visible)
            {
                nextImageForm.Show(this);
            }
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //拡大ポイント
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (dantaiCodeConts.Contains(sender)) p = posDantaiCode;
            else if (dantaiNameConts.Contains(sender)) p = posDantaiName;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        //ウィンドウサイズの変更
        private void MediveryForm_SizeChanged(object sender, EventArgs e)
        {
            int imputWidth = 590;

            int H = this.Height - SystemInformation.CaptionHeight;   //ウィンドウ上部のバー幅を引く
            int curW = panelLeft.Size.Width;    //左のリスト+画像領域の現在の幅
            int curH = panelLeft.Size.Height;   //左のリスト+画像領域の現在の高さ
            int maxW = this.Width - imputWidth - panelDatalist.Size.Width;
            int calcW = (int)((float)H / 1.44f);

            if (maxW >= calcW)
            {
                int d = maxW - calcW >= 50 ? 50 : maxW - calcW;
                panelDatalist.Size = new Size(250 + d, curH);
                panelLeft.Size = new Size(calcW + panelDatalist.Size.Width, curH);
            }
            else
            {
                panelDatalist.Size = new Size(250, curH);
                panelLeft.Size = new Size(maxW + panelDatalist.Size.Width, curH);
            }
        }

        //スクロールで拡大縮小
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (dantaiCodeConts.Any(c => c.Focused)) posDantaiCode = pos;
            else if (dantaiNameConts.Any(c => c.Focused)) posDantaiName = pos;
            else return;
        }

        //左のデータ一覧のソート
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var holders = (List<AppHolder>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(AppHolder.no))
            {
                holders.Sort((x, y) => x.no.CompareTo(y.no));
            }
            else if (name == nameof(AppHolder.aid))
            {
                holders.Sort((x, y) => x.aid.CompareTo(y.aid));
            }
            else if (name == nameof(AppHolder.flag))
            {
                holders.Sort((x, y) => x.flagInt.CompareTo(y.flagInt));
            }
            else if(name == nameof(AppHolder.accountNumber))
            {
                holders.Sort((x, y) => x.accountNumber.CompareTo(y.accountNumber));
            }
            else if (name == nameof(AppHolder.accountName))
            {
                holders.Sort((x, y) => x.accountName.CompareTo(y.accountName));
            }
            else
            {
                return;
            }

            bsApp.ResetBindings(false);
        }

        //戻るボタン
        private void buttonBack_Click(object sender, EventArgs e)
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri <= 0) return;
            dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri - 1];
        }

        // --------------------------------------------------------------------------------
        //
        // Control部
        //
        // --------------------------------------------------------------------------------

        private void clearControl()
        {
            getAllVerifyBox(false).ForEach(vb => vb.AllClear());
        }

        private void setCurrent()
        {
            var appholder = (AppHolder)bsApp.Current;
            if (appholder == null) return;
            setAppHolder(appholder);
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setAppHolder(AppHolder holder)
        {
            var app = holder.app;

            //画像の表示
            setImage(app);

            //現在の表示をリセット
            allBoxes.ForEach(vb => vb.AllClear());

            //表示
            verifyBoxDantaiCode.Text = holder.accountNumber;
            verifyBoxDantaiCode.TextV = holder.accountNumber;
            verifyBoxDantaiName.Text = holder.accountName;
            verifyBoxDantaiName.TextV = holder.accountName;

            if (holder.account == null || holder.account.HenreiWord == "了承済み") radioButtonRyosho.Checked = true;
            else if (holder.account.HenreiWord == "連絡済み") radioButtonRenraku.Checked = true;
            else if (holder.account.HenreiWord == string.Empty) radioButtonNone.Checked = true;
            else
            {
                verifyBoxHenreiOther.Text = holder.account.HenreiWord;
                radioButtonOther.Checked = true;
            }
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.35f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posDantaiCode;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// フォーカスを移動します。移動先はFOCUS_TYPE_...を参照してください。
        /// </summary>
        /// <param name="type"></param>
        private void moveFocus(int type, VerifyBox currentVerifyBox = null)
        {
            switch (type)
            {
                case FOCUS_TYPE_TOP:
                    if (allBoxes.Count() > 0) allBoxes[0].Focus();
                    break;
                case FOCUS_TYPE_ERROR:
                    var exisErr = false;
                    foreach(var vb in allBoxes)
                    {
                        if (vb.BackColor != SystemColors.Info)
                        {
                            exisErr = true;
                            vb.Focus();
                            break;
                        }
                    }
                    if (!exisErr) moveFocus(FOCUS_TYPE_TOP);//Errorがなければ先頭へ
                    break;
                case FOCUS_TYPE_NEXT:
                    var currIndex = -1;
                    for (int i = 0; i < allBoxes.Count; i++)
                    {
                        if (allBoxes[i] == currentVerifyBox)
                        {
                            currIndex = i;
                            break;
                        }
                    }
                    if (currIndex != -1 && currIndex != allBoxes.Count - 1) allBoxes[currIndex + 1].Focus();
                    break;
            }
        }


        private void updateAndSelectNext()
        {
            //登録
            var appholder = (AppHolder)bsApp.Current;
            if (!appDBupdate(appholder)) return;

            //描画更新
            flagUpdate(appholder);

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri];
                    return;
                }
                this.Close();
            }
            else
            {
                dataGridViewPlist.CurrentCell = dataGridViewPlist[1, ri + 1];//index0は不可視列なので1にする
            }
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(AppHolder holder)
        {
            var inputtingDantaiName = verifyBoxDantaiName.Text;

            //変更
            if (holder.account != null && !string.IsNullOrEmpty(inputtingDantaiName))
            {
                if (holder.account.Name != inputtingDantaiName)
                {
                    var agree = MessageBox.Show(
                        $"団体名が登録されている値（{holder.account.Name}）と異なっています。\r\n上書きしますか？",
                        "変更確認",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (agree != DialogResult.Yes)
                    {
                        verifyBoxDantaiName.Focus();
                        verifyBoxDantaiName.SelectAll();
                        return false;
                    }
                }

                holder.account.Name = inputtingDantaiName;
                holder.account.Update_Date = DateTime.Today;
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                holder.account.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                //holder.account.Update_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                holder.account.Verified = true;
                holder.account.HenreiWord = getWord();
                var result = holder.account.Update();
                if (!result)
                {
                    MessageBox.Show("Error", 
                        $"登録に失敗しました。{System.Environment.NewLine}もう一度操作を行い、再度発生する場合は担当者にお知らせ下さい。");
                    return false;
                }
                holder.accountName = holder.account.Name;
            }

            //初回登録
            if (holder.account == null
                && !string.IsNullOrEmpty(holder.accountNumber) 
                && !string.IsNullOrEmpty(inputtingDantaiName))
            {
                var account = new Account();
                account.Code = holder.accountNumber;
                account.Name = inputtingDantaiName;
                account.Insert_Date = DateTime.Today;
                
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                account.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100) * 100 + (cym % 100);
                //account.Insert_CYM = DateTimeEx.GetHsYearFromAd(cym / 100) * 100 + (cym % 100);
                //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                account.Verified = true;
                account.HenreiWord = getWord();
                var result = account.Insert();
                if (!result)
                {
                    MessageBox.Show("Error",
                        $"登録に失敗しました。{System.Environment.NewLine}もう一度操作を行い、再度発生する場合は担当者にお知らせ下さい。");
                    return false;
                }
                holder.account = account;
                holder.accountName = account.Name;
                flagUpdate(holder);
            }

            return true;
        }

        /// <summary>
        /// フラグを更新します。
        /// </summary>
        private void flagUpdate(AppHolder holder)
        {
            if (holder.account?.Verified ?? false)
            {
                holder.flag = "登録済み";
                holder.flagInt = 1;
            }
            else
            {
                holder.flag = "未登録";
                holder.flagInt = 2;
            }
        }


        private List<VerifyBox> getAllVerifyBox(bool visibleOnly)
        {
            var list = new List<VerifyBox>();
            Action<Control> get = null;
            get = cs =>
            {
                VerifyBox vb;
                foreach (Control c in cs.Controls)
                {
                    if (visibleOnly && !c.Visible) continue;
                    if (c is VerifyBox)
                    {
                        vb = (VerifyBox)c;
                        list.Add(vb);
                    }
                    if (c is Panel)
                    {
                        get(c);
                    }
                }
            };
            get(panelAddInput);
            list.Sort((x, y) => x.TabIndex.CompareTo(y.TabIndex));//タブインデックス昇順
            return list;
        }

        private void radioButtonOther_CheckedChanged(object sender, EventArgs e)
        {
            verifyBoxHenreiOther.Enabled = radioButtonOther.Checked;
        }

        private string getWord()
        {
            return radioButtonRenraku.Checked ? "連絡済み" :
                radioButtonRyosho.Checked ? "了承済み" :
                radioButtonNone.Checked ? string.Empty :
                radioButtonOther.Checked ? verifyBoxHenreiOther.Text.Trim() :
                string.Empty;
        }
    }
}
