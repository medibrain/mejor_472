﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
    //20201105162437 furukawa st ////////////////////////
    //何かの番号で名称を出すリスト画面
    //20201105162437 furukawa ed ////////////////////////
    public partial class SelectForm : Form
    {
        public string selectedstring = string.Empty;
        public TextBox _txt;
        public SelectForm(TextBox txt)
        {
            InitializeComponent();
            _txt = txt;
        }


        private void SelectForm_Load(object sender, EventArgs e)
        {
            listBox.SelectedIndex = 0;
            
        }

        private void listBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (listBox.SelectedItem != null))
            {
                _txt.Text = listBox.SelectedItem.ToString();
                this.Close();
            }

        }
    }
}
