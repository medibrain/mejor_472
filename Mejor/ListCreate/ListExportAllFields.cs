﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
 

    class ListExportAllFields
    {

        public static string strHeader=string.Empty;



        public static List<App> lstApp;
        public static int cym = 0;
        public Insurer ins = Insurer.CurrrentInsurer;

        
        static List<Pay.PayCode> lstPay = new List<Mejor.Pay.PayCode>();



        public static StringBuilder sbHeader = new StringBuilder();
        public static StringBuilder sbHeaderApp = new StringBuilder();
        public static StringBuilder sbHeaderClinic = new StringBuilder();
        public static StringBuilder sbHeaderTekiyouData = new StringBuilder();
        public static StringBuilder sbHeaderSendTo = new StringBuilder();
        public static StringBuilder sbHeaderHiho = new StringBuilder();
        public static StringBuilder sbHeaderPay = new StringBuilder();

        public static int cntApp = 0;
        public static int cntClinic = 0;
        public static int cntTekiyouData = 0;
        public static int cntSendTo = 0;
        public static int cntHiho = 0;
        public static int cntPay = 0;

        static ListExportAllFields()
        {
            //全フィールド集める
            //csv作成
            //

           
          
        }


        #region ヘッダ文字列作成

        private static string CreateHeaderApp()
        {
            sbHeaderApp.Clear();
            sbHeaderApp.Append("プライマリーキー,");
            sbHeaderApp.Append("スキャンID,");
            sbHeaderApp.Append("グループID,");
            sbHeaderApp.Append("診療和暦年,");
            sbHeaderApp.Append("診療和暦月,");
            sbHeaderApp.Append("保険者番号,");
            sbHeaderApp.Append("被保険者記号番号,");
            sbHeaderApp.Append("都道府県,");
            sbHeaderApp.Append("被保険者拡張情報,");
            sbHeaderApp.Append("被保険者名,");
            sbHeaderApp.Append("被保険者郵便番号,");
            sbHeaderApp.Append("被保険者住所,");
            sbHeaderApp.Append("受療者名,");
            sbHeaderApp.Append("受療者性別,");
            sbHeaderApp.Append("受療者生年月日,");


            //20190704143710 furukawa st ////////////////////////
            //sidフィールド（シャープ健保支払先コード）
            
            sbHeaderApp.Append("sid,");
            //20190704143710 furukawa ed ////////////////////////


            sbHeaderApp.Append("家族区分,");
            sbHeaderApp.Append("給付割合,");
            sbHeaderApp.Append("公費番号,");
            sbHeaderApp.Append("負傷1　負傷名,");
            sbHeaderApp.Append("　負傷年月日,");
            sbHeaderApp.Append("　初検日,");
            sbHeaderApp.Append("　開始日,");
            sbHeaderApp.Append("　終了日,");
            sbHeaderApp.Append("　日数,");
            sbHeaderApp.Append("　転帰,");
            sbHeaderApp.Append("負傷2　負傷名,");
            sbHeaderApp.Append("　負傷年月日,");
            sbHeaderApp.Append("　初検日,");
            sbHeaderApp.Append("　開始日,");
            sbHeaderApp.Append("　終了日,");
            sbHeaderApp.Append("　日数,");
            sbHeaderApp.Append("　転帰,");
            sbHeaderApp.Append("負傷3　負傷名,");
            sbHeaderApp.Append("　負傷年月日,");
            sbHeaderApp.Append("　初検日,");
            sbHeaderApp.Append("　開始日,");
            sbHeaderApp.Append("　終了日,");
            sbHeaderApp.Append("　日数,");
            sbHeaderApp.Append("　転帰,");
            sbHeaderApp.Append("負傷4　負傷名,");
            sbHeaderApp.Append("　負傷年月日,");
            sbHeaderApp.Append("　初検日,");
            sbHeaderApp.Append("　開始日,");
            sbHeaderApp.Append("　終了日,");
            sbHeaderApp.Append("　日数,");
            sbHeaderApp.Append("　転帰,");
            sbHeaderApp.Append("負傷5　負傷名,");
            sbHeaderApp.Append("　負傷年月日,");
            sbHeaderApp.Append("　初検日,");
            sbHeaderApp.Append("　開始日,");
            sbHeaderApp.Append("　終了日,");
            sbHeaderApp.Append("　日数,");
            sbHeaderApp.Append("　転帰,");
            sbHeaderApp.Append("往療距離,");
            sbHeaderApp.Append("往療回数,");
            sbHeaderApp.Append("往療料,");
            sbHeaderApp.Append("往療料加算,");
            sbHeaderApp.Append("施術師番号,");
            sbHeaderApp.Append("施術所郵便番号,");
            sbHeaderApp.Append("施術所住所,");
            sbHeaderApp.Append("施術所名,");
            sbHeaderApp.Append("施術所電話番号,");
            sbHeaderApp.Append("施術師名,");
            sbHeaderApp.Append("施術師カナ,");
            sbHeaderApp.Append("口座種別,");
            sbHeaderApp.Append("銀行名,");
            sbHeaderApp.Append("銀行種別,");
            sbHeaderApp.Append("支店名,");
            sbHeaderApp.Append("支店種別,");
            sbHeaderApp.Append("口座名義,");
            sbHeaderApp.Append("口座名義カナ,");
            sbHeaderApp.Append("口座番号,");
            sbHeaderApp.Append("合計額,");
            sbHeaderApp.Append("一部負担額,");
            sbHeaderApp.Append("請求額,");
            sbHeaderApp.Append("実日数,");
            sbHeaderApp.Append("ナンバリング,");
            sbHeaderApp.Append("1回目入力ユーザーID,");
            sbHeaderApp.Append("2回目入力ユーザーID,");
            sbHeaderApp.Append("点検ユーザーID,");
            sbHeaderApp.Append("あんま部位,");
            sbHeaderApp.Append("処理西暦年月,");
            sbHeaderApp.Append("診療西暦年月,");
            sbHeaderApp.Append("関連レセ情報ID,");
            sbHeaderApp.Append("1回目追加入力ユーザーID,");
            sbHeaderApp.Append("2回目追加入力ユーザーID,");
            sbHeaderApp.Append("照会メモ,");
            sbHeaderApp.Append("点検メモ,");
            sbHeaderApp.Append("メモ,");
            sbHeaderApp.Append("支払コード,");
            sbHeaderApp.Append("照会文書通知コード,");
            sbHeaderApp.Append("1回目拡張入力ユーザーID,");
            sbHeaderApp.Append("2回目拡張入力ユーザーID,");
            sbHeaderApp.Append("過誤理由,");
            sbHeaderApp.Append("再審理由,");
            sbHeaderApp.Append("返戻理由,");
            sbHeaderApp.Append("電算管理番号,");
            sbHeaderApp.Append("グループ,");
            sbHeaderApp.Append("出力メモ,");
            sbHeaderApp.Append("請求年月,");
            sbHeaderApp.Append("診療年月,");

            return sbHeaderApp.ToString();
        }

        private static string CreateHeaderClinic()
        {
            sbHeaderClinic.Clear();
            sbHeaderClinic.Append("医療機関コード,");
            sbHeaderClinic.Append("医療機関名,");
            sbHeaderClinic.Append("医療機関郵便番号,");
            sbHeaderClinic.Append("医療機関住所,");
            

            return sbHeaderClinic.ToString();
        }

        private static string CreateHeaderPay()
        {
            sbHeaderPay.Clear();
            sbHeaderPay.Append("支払先コード,");
            sbHeaderPay.Append("支払先個人団体区別,");
            sbHeaderPay.Append("支払先銀行コード,");
            sbHeaderPay.Append("支払先銀行名,");
            sbHeaderPay.Append("支払先支店番号,");
            sbHeaderPay.Append("支払先支店名,");
            sbHeaderPay.Append("支払先口座番号,");
            sbHeaderPay.Append("支払先口座名漢字,");
            sbHeaderPay.Append("支払先口座名カナ,");
            sbHeaderPay.Append("支払先更新日,");

            return sbHeaderPay.ToString();
        }

        private static string CreateHeaderHiho()
        {
            sbHeaderHiho.Clear();
            sbHeaderHiho.Append("被保険者証番号,");
            sbHeaderHiho.Append("被保険者氏名,");
            sbHeaderHiho.Append("被保険者カナ,");
            sbHeaderHiho.Append("被保険者生年月日,");
            sbHeaderHiho.Append("被保険者郵便番号,");
            sbHeaderHiho.Append("被保険者住所1,");
            sbHeaderHiho.Append("被保険者住所2,");
            sbHeaderHiho.Append("被保険者住所3,");
            

            return sbHeaderHiho.ToString();
        }

        private static string CreateHeaderSendTo()
        {
            sbHeaderSendTo.Clear();
            sbHeaderSendTo.Append("送付先証番号,");
            sbHeaderSendTo.Append("送付先氏名,");
            sbHeaderSendTo.Append("送付先カナ,");
            sbHeaderSendTo.Append("送付先生年月日,");
            sbHeaderSendTo.Append("送付先郵便番号,");
            sbHeaderSendTo.Append("送付先住所1,");
            sbHeaderSendTo.Append("送付先住所2,");
            sbHeaderSendTo.Append("送付先住所3,");


            return sbHeaderSendTo.ToString();
        }

        private static string CreateHeaderTekiyouData()
        {
            sbHeaderTekiyouData.Clear();
            sbHeaderTekiyouData.Append("資格適用データ事業所No,");
            sbHeaderTekiyouData.Append("資格適用データ証番号,");
            sbHeaderTekiyouData.Append("資格適用データ枝番,");
            sbHeaderTekiyouData.Append("資格適用データ最終枝番,");
            sbHeaderTekiyouData.Append("資格適用データ家族No,");
            sbHeaderTekiyouData.Append("資格適用データカナ氏名,");
            sbHeaderTekiyouData.Append("資格適用データ氏名,");
            sbHeaderTekiyouData.Append("資格適用データ性別,");
            sbHeaderTekiyouData.Append("資格適用データ続柄,");
            sbHeaderTekiyouData.Append("資格適用データ生年月日元号,");
            sbHeaderTekiyouData.Append("資格適用データ生年月日,");
            sbHeaderTekiyouData.Append("資格適用データ資格取得日元号,");
            sbHeaderTekiyouData.Append("資格適用データ資格取得日,");
            sbHeaderTekiyouData.Append("資格適用データ資格喪失日元号,");
            sbHeaderTekiyouData.Append("資格適用データ資格喪失日,");
            sbHeaderTekiyouData.Append("資格適用データ自己負担割合,");
            sbHeaderTekiyouData.Append("資格適用データ発効日,");
            sbHeaderTekiyouData.Append("資格適用データ郵便番号,");
            sbHeaderTekiyouData.Append("資格適用データ住所,");
            sbHeaderTekiyouData.Append("資格適用データ生年月日年月日西暦,");
            sbHeaderTekiyouData.Append("資格適用データ資格取得年月日西暦,");
            sbHeaderTekiyouData.Append("資格適用データ資格喪失年月日西暦,");
            sbHeaderTekiyouData.Append("資格適用データ被保番,");
            sbHeaderTekiyouData.Append("資格適用データ氏名コード,");
            sbHeaderTekiyouData.Append("資格適用データ発効日西暦,");



            return sbHeaderTekiyouData.ToString();


        }
        #endregion

        public static void CreateData()
        {

            using (WaitForm wf = new WaitForm())
            {

                wf.ShowDialogOtherTask();
                wf.SetMax(lstApp.Count());
                StringBuilder sbwf = new StringBuilder();

                
                List<string> lstStr = new List<string>();

                sbHeader.Clear();
                sbHeader.Append(CreateHeaderApp());

                List<HyogoKoiki.Clinic> lstClinic = new List<HyogoKoiki.Clinic>();
                List<HyogoKoiki.Person> lstPerson = new List<HyogoKoiki.Person>();
                List<HyogoKoiki.SendTo> lstSendto = new List<HyogoKoiki.SendTo>();

                List<SharpKenpo.TekiyouData> lstTekiyo = new List<SharpKenpo.TekiyouData>();

                //保険者別
                switch (Insurer.CurrrentInsurer.InsurerID)
                {

                    case (int)InsurerID.HYOGO_KOIKI:
                        //lstClinic = (List<HyogoKoiki.Clinic>)DB.Main.SelectAll<HyogoKoiki.Clinic>();
                        //lstPerson = (List<HyogoKoiki.Person>)DB.Main.SelectAll<HyogoKoiki.Person>();
                        //lstSendto = (List<HyogoKoiki.SendTo>)DB.Main.SelectAll<HyogoKoiki.SendTo>();


                        sbHeader.Append(CreateHeaderClinic());
                    //     sbHeader.Append(CreateHeaderPay());
                         sbHeader.Append(CreateHeaderHiho());
                         sbHeader.Append(CreateHeaderSendTo());
                        



                        break;

                    case (int)InsurerID.SHARP_KENPO:
                        // lstTekiyo = (List<SharpKenpo.TekiyouData>)DB.Main.SelectAll<SharpKenpo.TekiyouData>();

                        sbHeader.Append(CreateHeaderTekiyouData());
                        sbHeader.Append(CreateHeaderPay());

                        break;
                }

                sbHeader.Remove(sbHeader.ToString().Length - 1, 1);


                //bool flgList = false;

                foreach (App itemApp in lstApp)
                {
                    
                    StringBuilder sb = new StringBuilder();

                    //20210205133629 furukawa st ////////////////////////
                    //出力途中キャンセル処理実装
                    
                    if (CommonTool.WaitFormCancelProcess(wf)) return;
                    //20210205133629 furukawa ed ////////////////////////

                    #region app取得
                    //20190708182634 furukawa ダブルコーテーション付きCSVにする


                    sb.Append("\"" + itemApp.Aid + "\",\"");                        //プライマリーキー
                    sb.Append(itemApp.ScanID + "\",\"");                            //スキャンID
                    sb.Append(itemApp.GroupID + "\",\"");                           //グループID
                    sb.Append(itemApp.MediYear + "\",\"");                          //診療和暦年
                    sb.Append(itemApp.MediMonth + "\",\"");                         //診療和暦月
                    sb.Append(itemApp.InsNum + "\",\"");                            //保険者番号
                    sb.Append(itemApp.HihoNum + "\",\"");                           //被保険者記号番号
                    sb.Append(itemApp.HihoPref + "\",\"");                          //都道府県
                    sb.Append(itemApp.HihoType + "\",\"");                          //被保険者拡張情報
                    sb.Append(itemApp.HihoName + "\",\"");                          //被保険者名
                    sb.Append(itemApp.HihoZip + "\",\"");                           //被保険者郵便番号
                    sb.Append(itemApp.HihoAdd + "\",\"");                           //被保険者住所
                    sb.Append(itemApp.PersonName + "\",\"");                        //受療者名
                    sb.Append(itemApp.Sex + "\",\"");                               //受療者性別
                    sb.Append(itemApp.Birthday + "\",\"");                          //受療者生年月日


                    //20190704143510 furukawa st ////////////////////////
                    //sidフィールド（シャープ健保支払先コード）
                    sb.Append(itemApp.ClinicNum + "\",\"");      

                    //20190704143510 furukawa ed ////////////////////////



                    //sb.Append(item.Single + "\",\"");                          //単独併用区分
                    sb.Append(itemApp.Family + "\",\"");                            //家族区分
                    sb.Append(itemApp.Ratio + "\",\"");                             //給付割合
                    sb.Append(itemApp.PublcExpense + "\",\"");                      //公費番号
                                                                             //  sb.Append(item.ImageFile + "\",\"");
                                                                             //sb.Append(item.ChargeYear + "\",\"");                       
                    sb.Append(itemApp.FushoName1 + "\",\"");                             //負傷1　負傷名
                    sb.Append(itemApp.FushoDate1 + "\",\"");                           //　負傷年月日
                    sb.Append(itemApp.FushoFirstDate1 + "\",\"");                      //　初検日
                    sb.Append(itemApp.FushoStartDate1 + "\",\"");                      //　開始日
                    sb.Append(itemApp.FushoFinishDate1 + "\",\"");                     //　終了日
                    sb.Append(itemApp.FushoDays1 + "\",\"");                           //　日数
                    sb.Append(itemApp.FushoCourse1 + "\",\"");                         //　転帰
                    sb.Append(itemApp.FushoName2 + "\",\"");                           //負傷2　負傷名
                    sb.Append(itemApp.FushoDate2 + "\",\"");                           //　負傷年月日
                    sb.Append(itemApp.FushoFirstDate2 + "\",\"");                      //　初検日
                    sb.Append(itemApp.FushoStartDate2 + "\",\"");                      //　開始日
                    sb.Append(itemApp.FushoFinishDate2 + "\",\"");                     //　終了日
                    sb.Append(itemApp.FushoDays2 + "\",\"");                           //　日数
                    sb.Append(itemApp.FushoCourse2 + "\",\"");                         //　転帰
                    sb.Append(itemApp.FushoName3 + "\",\"");                           //負傷3　負傷名
                    sb.Append(itemApp.FushoDate3 + "\",\"");                           //　負傷年月日
                    sb.Append(itemApp.FushoFirstDate3 + "\",\"");                      //　初検日
                    sb.Append(itemApp.FushoStartDate3 + "\",\"");                      //　開始日
                    sb.Append(itemApp.FushoFinishDate3 + "\",\"");                     //　終了日
                    sb.Append(itemApp.FushoDays3 + "\",\"");                           //　日数
                    sb.Append(itemApp.FushoCourse3 + "\",\"");                         //　転帰
                    sb.Append(itemApp.FushoName4 + "\",\"");                           //負傷4　負傷名
                    sb.Append(itemApp.FushoDate4 + "\",\"");                           //　負傷年月日
                    sb.Append(itemApp.FushoFirstDate4 + "\",\"");                      //　初検日
                    sb.Append(itemApp.FushoStartDate4 + "\",\"");                      //　開始日
                    sb.Append(itemApp.FushoFinishDate4 + "\",\"");                     //　終了日
                    sb.Append(itemApp.FushoDays4 + "\",\"");                           //　日数
                    sb.Append(itemApp.FushoCourse4 + "\",\"");                         //　転帰
                    sb.Append(itemApp.FushoName5 + "\",\"");                           //負傷5　負傷名
                    sb.Append(itemApp.FushoDate5 + "\",\"");                           //　負傷年月日
                    sb.Append(itemApp.FushoFirstDate5 + "\",\"");                      //　初検日
                    sb.Append(itemApp.FushoStartDate5 + "\",\"");                      //　開始日
                    sb.Append(itemApp.FushoFinishDate5 + "\",\"");                     //　終了日
                    sb.Append(itemApp.FushoDays5 + "\",\"");                           //　日数
                    sb.Append(itemApp.FushoCourse5 + "\",\"");                         //　転帰


                    //  sb.Append(item. public NEW_CONT NewContType + "\",\"" );
                    sb.Append(itemApp.Distance + "\",\"");                         //往療距離  
                    sb.Append(itemApp.VisitTimes + "\",\"");                       //往療回数
                    sb.Append(itemApp.VisitFee + "\",\"");                         //往療料
                    sb.Append(itemApp.VisitAdd + "\",\"");                         //往療料加算
                    sb.Append(itemApp.DrNum + "\",\"");                            //施術師番号
                    sb.Append(itemApp.ClinicZip + "\",\"");                        //施術所郵便番号
                    sb.Append(itemApp.ClinicAdd + "\",\"");                        //施術所住所
                    sb.Append(itemApp.ClinicName + "\",\"");                       //施術所名
                    sb.Append(itemApp.ClinicTel + "\",\"");                        //施術所電話番号
                    sb.Append(itemApp.DrName + "\",\"");                           //施術師名
                    sb.Append(itemApp.DrKana + "\",\"");                           //施術師カナ
                    sb.Append(itemApp.AccountType + "\",\"");                      //口座種別
                    sb.Append(itemApp.BankName + "\",\"");                         //銀行名
                    sb.Append(itemApp.BankType + "\",\"");                         //銀行種別
                    sb.Append(itemApp.BankBranch + "\",\"");                       //支店名
                    sb.Append(itemApp.BankBranchType + "\",\"");                   //支店種別
                    sb.Append(itemApp.AccountName + "\",\"");                      //口座名義
                    sb.Append(itemApp.AccountKana + "\",\"");                      //口座名義カナ
                    sb.Append(itemApp.AccountNumber + "\",\"");                    //口座番号
                    sb.Append(itemApp.Total + "\",\"");                            //合計額
                    sb.Append(itemApp.Partial + "\",\"");                          //一部負担額
                    sb.Append(itemApp.Charge + "\",\"");                           //請求額
                    sb.Append(itemApp.CountedDays + "\",\"");                      //実日数
                    sb.Append(itemApp.Numbering + "\",\"");                        //ナンバリング
                                                                            //sb.Append(item.        public APP_TYPE AppType + "\",\"" );

                    sb.Append(itemApp.Ufirst + "\",\"");                         //1回目入力ユーザーID
                    sb.Append(itemApp.Usecond + "\",\"");                        //2回目入力ユーザーID
                    sb.Append(itemApp.Uinquiry + "\",\"");                       //点検ユーザーID
                    sb.Append(itemApp.Bui + "\",\"");                            //あんま部位
                    sb.Append(itemApp.CYM + "\",\"");                            //処理西暦年月
                    sb.Append(itemApp.YM + "\",\"");                             //診療西暦年月
                                                                          //sb.Append(item.   public StatusFlag StatusFlags + "\",\"" );                  //ステータスフラグ
                                                                          //sb.Append(item. public ShokaiReason ShokaiReason + "\",\"" );               //照会理由
                    sb.Append(itemApp.RrID + "\",\"");                            //関連レセ情報ID
                    sb.Append(itemApp.AdditionalUid1 + "\",\"");                  //1回目追加入力ユーザーID
                    sb.Append(itemApp.AdditionalUid2 + "\",\"");                  //2回目追加入力ユーザーID
                    sb.Append(itemApp.MemoShokai + "\",\"");                      //照会メモ
                    sb.Append(itemApp.MemoInspect + "\",\"");                     //点検メモ
                    sb.Append(itemApp.Memo + "\",\"");                            //メモ
                    sb.Append(itemApp.PayCode + "\",\"");                         //支払コード
                    sb.Append(itemApp.ShokaiCode + "\",\"");                      //照会文書通知コード
                                                                           //sb.Append(item.OcrData  + "\",\"" );
                    sb.Append(itemApp.UfirstEx + "\",\"");                        //1回目拡張入力ユーザーID
                    sb.Append(itemApp.UsecondEx + "\",\"");                       //2回目拡張入力ユーザーID

                    sb.Append(itemApp.KagoReasons + "\",\"");                       //過誤理由 カンマで連結しているため、csvにしたとき項目と間違うのでダブルコーテーション
                    sb.Append(itemApp.SaishinsaReasons + "\",\"");                  //再審理由
                    sb.Append(itemApp.HenreiReasons + "\",\"");                     //返戻理由

                    sb.Append(itemApp.ComNum + "\",\"");                                  //電算管理番号
                    sb.Append(itemApp.GroupNum + "\",\"");                                //グループ
                    sb.Append(itemApp.OutMemo + "\",\"");                                 //出力メモ

                    //sb.Append(item.        public TaggedDatas TaggedDatas + "\",\"" );

                    sb.Append(DateTimeEx.GetInitialEraJpYearMonth(itemApp.CYM).ToString() + "\",\"");         //請求年月和暦
                    sb.Append(DateTimeEx.GetInitialEraJpYearMonth(itemApp.YM).ToString() + "\",\"");               //診療年月和暦


                    #endregion

                    sbwf.Append(itemApp.Aid.ToString());

                    wf.InvokeValue++;
                    cntApp++;

                    //保険者別
                    switch (Insurer.CurrrentInsurer.InsurerID)
                    {

                        case (int)InsurerID.HYOGO_KOIKI:


                            #region 医療機関

                            if (!string.IsNullOrEmpty(itemApp.DrNum))
                            {

                                

                                string pref = itemApp.DrNum.ToString().Substring(1, 2);
                                string strDrNum = pref + "7" + itemApp.DrNum.Substring(3, 7);
                                //System.Diagnostics.Debug.Print(strDrNum);
                                //都道府県2桁＋"７"＋７桁

                                List<HyogoKoiki.Clinic> lstClinicTmp =
                                    (List<HyogoKoiki.Clinic>)DB.Main.Select<HyogoKoiki.Clinic>("clinicnum='" + strDrNum + "'");
                                if (lstClinicTmp.Count > 0)
                                {
                                    HyogoKoiki.Clinic c = lstClinicTmp[0];
                                    sb.Append(c.ClinicNum.ToString() + "\",\"");
                                    sb.Append(c.Name.ToString() + "\",\"");
                                    sb.Append(c.Zip.ToString() + "\",\"");
                                    sb.Append(c.Address.ToString() + "\",\"");


                                    cntClinic++;
                                    sbwf.Append(" 医療機関 ");
                                }
                                else
                                {
                                    sb.Append("\",\"\",\"\",\"\",\"");
                                    //sb.Append(",,,,");
                                }

                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"");
                                //sb.Append(",,,,");
                            }
                           

                            #endregion
                            
                            #region 被保険者

                            List<HyogoKoiki.Person> lstPersonTmp =
                                (List<HyogoKoiki.Person>)DB.Main.Select<HyogoKoiki.Person>("hihonum='" + itemApp.HihoNum + "'");
                            if (lstPersonTmp.Count > 0)
                            {
                                
                                    //被保険者              
                                    sb.Append(lstPersonTmp[0].HihoNum + "\",\"");
                                    sb.Append(lstPersonTmp[0].HihoName + "\",\"");
                                    sb.Append(lstPersonTmp[0].Kana + "\",\"");
                                    sb.Append(lstPersonTmp[0].Birthday.ToString() + "\",\"");
                                    sb.Append(lstPersonTmp[0].Zip + "\",\"");
                                    sb.Append(lstPersonTmp[0].Address1 + "\",\"");
                                    sb.Append(lstPersonTmp[0].Address2 + "\",\"");
                                    sb.Append(lstPersonTmp[0].Address3 + "\",\"");

                                    cntHiho++;


                                    sbwf.Append(" 被保険者 ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"");
                                //sb.Append(",,,,,,,,");
                            }

                            #endregion

                            #region 送付先


                            List<HyogoKoiki.SendTo> lstSendToTmp =
                                (List<HyogoKoiki.SendTo>)DB.Main.Select<HyogoKoiki.SendTo>("hihonum='" + itemApp.HihoNum + "'");

                            if (lstSendToTmp.Count > 0)
                            {
                                //送付先                 
                                sb.Append(lstSendToTmp[0].HihoNum + "\",\"");
                                sb.Append(lstSendToTmp[0].DestName + "\",\"");
                                sb.Append(lstSendToTmp[0].DestKana + "\",\"");
                                sb.Append(lstSendToTmp[0].DestZip + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress1 + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress2 + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress3 + "\",");

                                cntSendTo++;

                                sbwf.Append(" 送付先 ");
                                

                                
                               
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",");
                                //sb.Append(",,,,,,,");
                            }

                            #endregion

                            break;

                        case (int)InsurerID.SHARP_KENPO:

                            #region 資格適用データ


                            List<SharpKenpo.TekiyouData> lstTekiyoDataTmp =
                                (List<SharpKenpo.TekiyouData>)DB.Main.Select<SharpKenpo.TekiyouData>(
                                    "syono='" + itemApp.HihoNum.ToString().Split('-')[1] + "'" +
                                    " and hgender='" + itemApp.Sex.ToString() + "'" +
                                    " and birthadymd='" + itemApp.Birthday.ToString("yyyy-MM-dd") + "'");

                            if (lstTekiyoDataTmp.Count > 0)
                            {

                                sb.Append(lstTekiyoDataTmp[0].Gno + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].SyoNo + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Branch + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Branch2 + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].famNo + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hKana + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hName + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hGender + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Zokugara + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossYMD + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Ratio.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].issueDate.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].zip + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].add + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hihoNum + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].etc01 + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].issueADYMD.ToString() + "\",\"");

                                cntTekiyouData++;

                                sbwf.Append(" 資格適用データ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"," +
                                    "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"," +
                                    "\"\",\"\",\"\",\"\",\"\",\"");
                                //sb.Append(",,,,,,,,,,,,,,,,,,,,,,,,,");
                            }
                            #endregion


                            #region 支払先

                            //20200811152954 furukawa st ////////////////////////
                            //支払先コードはPKでないので、口座番号も条件に入れる
                            
                            List<Pay.PayCode> lstPayTmp =
                             (List<Pay.PayCode>)DB.Main.Select<Pay.PayCode>($"substr(code,1,9)='{itemApp.PayCode}' and accountno='{itemApp.AccountNumber}'");
                            //(List<Pay.PayCode>)DB.Main.Select<Pay.PayCode>("substr(code,1,9)='" + itemApp.PayCode + "'");

                            //20200811152954 furukawa ed ////////////////////////


                            //20190612161134 furukawa st ////////////////////////
                            //支払先コードの長さが９以上の場合のみ設定                            
                            if (lstPayTmp.Count > 0 && lstPayTmp[0].Code.Length >= 9)
                                    //if (lstPayTmp.Count > 0)
                            //20190612161134 furukawa ed ////////////////////////
                            {


                                //paycode                                
                                string strPayCode = lstPayTmp[0].Code.ToString().Substring(0, 9);
                                SharpKenpo.PayCodeImporter.PayKind pkDK = SharpKenpo.PayCodeImporter.getDK(lstPayTmp[0].Code.ToString());


                                sb.Append(lstPayTmp[0].Code + "\",\"");
                                sb.Append(pkDK.ToString() + "\",\"");
                                sb.Append(lstPayTmp[0].BankNo + "\",\"");
                                sb.Append(lstPayTmp[0].BankName + "\",\"");
                                sb.Append(lstPayTmp[0].BranchNo + "\",\"");
                                sb.Append(lstPayTmp[0].BranchName + "\",\"");
                                sb.Append(lstPayTmp[0].AccountNo + "\",\"");
                                sb.Append(lstPayTmp[0].AccountName + "\",\"");
                                sb.Append(lstPayTmp[0].AccountKana + "\",\"");
                                sb.Append(lstPayTmp[0].UpdateDate.ToString() + "\",");

                                cntPay++;
                                sbwf.Append(" 支払先 ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",");
                                //sb.Append(",,,,,,,,,,");

                            }
                            #endregion
                            break;

                          

                    }

                    wf.LogPrint(sbwf.ToString());
                    sbwf.Remove(0, sbwf.ToString().Length);
                    sb.Remove(sb.ToString().Length - 1, 1);


                    //不要な文字列削除
                    string strAdd = sb.ToString();
                    strAdd= strAdd.Replace(DateTime.MinValue.ToString(), string.Empty);
                    strAdd= strAdd.Replace(" 0:00:00", string.Empty);
                    strAdd = strAdd.Replace("\r", " ");
                    strAdd = strAdd.Replace("\n", " ");

                    lstStr.Add(strAdd);

                }



      



                var fn = string.Empty;
                //var ex = (s.ExportType == "UTF8" || s.ExportType == "SJIS") ? ".csv" : ".xlsx";

                using (var f = new SaveFileDialog())
                {
                    //f.FileName = s.FileName.Replace("{cym}", cym.ToString()).
                    //    Replace("{ins}", Insurer.CurrrentInsurer.InsurerName) + ex;

                    f.FileName = cym.ToString() + Insurer.CurrrentInsurer.InsurerName + "_UTF8.csv";
                    
                    f.Filter = "*.csv|*.csv";
                    f.FilterIndex = 0;

                    if (f.ShowDialog() != DialogResult.OK) return;
                    fn = f.FileName;


                    if (exportCsv(lstStr, fn, wf))
                    {
                        MessageBox.Show("正常終了");
                    }
                    else
                    {
                        MessageBox.Show("失敗");
                    }

                }
            }
            

            //StreamWriter sw = new StreamWriter("test.csv", false, Encoding.UTF8);

            //sw.WriteLine(sbHeader.ToString());

            //foreach(string s in lstStr)
            //{
            //    sw.WriteLine(s);

            //}
            ////foreach (ListAllFields laf in lst)
            ////{
                
            ////    sw.WriteLine(string.Join("\",\"", laf));
            ////}
            //sw.Close();

        }
       

        private static bool exportCsv(List<string> lstStr, string fileName,  WaitForm wf)
        {
            //var enc = setting.ExportType == "UTF8" ? Encoding.UTF8 :
            //   setting.ExportType == "SJIS" ? Encoding.GetEncoding("Shift_JIS") :
            //   throw new Exception("エンコード形式が不明です");

            var enc = System.Text.Encoding.GetEncoding("UTF-8");
            using (var sw = new StreamWriter(fileName, false, enc))
            {
                
                //データ書き込み
                wf.LogPrint("データを書き込み中です…");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                sw.WriteLine(sbHeader.ToString());

                foreach (string s in lstStr)
                {
                    try
                    {
                        sw.WriteLine(s);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        return false;
                    }
                }
                return true;
            }
        }

        public static void DumpAppData()
        {
            using (WaitForm wf = new WaitForm())
            {

                wf.ShowDialogOtherTask();
                wf.SetMax(lstApp.Count);

                List<string> lstStr = new List<string>();

                sbHeader.Clear();
                sbHeader.Append(CreateHeaderDumpApp());


                //保険者別
                switch (Insurer.CurrrentInsurer.InsurerID)
                {

                    case (int)InsurerID.HYOGO_KOIKI:
                        sbHeader.Append(CreateHeaderClinic());
                        sbHeader.Append(CreateHeaderHiho());
                        sbHeader.Append(CreateHeaderSendTo());
                        break;

                    case (int)InsurerID.SHARP_KENPO:
                        sbHeader.Append(CreateHeaderTekiyouData());
                        sbHeader.Append(CreateHeaderPay());
                        break;

                    //20191002155938 furukawa st ////////////////////////
                    //交付料有無、前回支給
                    case (int)InsurerID.OSAKA_KOIKI:
                        
                        
                        sbHeader.Append("交付料有り,前回支給,");
                        break;
                        //20191002155938 furukawa ed ////////////////////////


                }
                sbHeader.Remove(sbHeader.Length - 1, 1);    // 最後のカンマ削除


                foreach (App itemApp in lstApp)
                {
                    //20210205133436 furukawa st ////////////////////////
                    //出力途中キャンセル処理実装
                    
                    if (CommonTool.WaitFormCancelProcess(wf)) return;
                    //20210205133436 furukawa ed ////////////////////////


                    StringBuilder sbwf = new StringBuilder();
                    StringBuilder sb = new StringBuilder(CreateDataDumpApp(itemApp) + ",\"");

                    sbwf.Append(itemApp.Aid.ToString());

                    wf.InvokeValue++;
                    cntApp++;

                    //保険者別
                    switch (Insurer.CurrrentInsurer.InsurerID)
                    {
                        case (int)InsurerID.HYOGO_KOIKI:
                            #region 医療機関
                            if (!string.IsNullOrEmpty(itemApp.DrNum))
                            {
                                string pref = itemApp.DrNum.ToString().Substring(1, 2);
                                string strDrNum = pref + "7" + itemApp.DrNum.Substring(3, 7);
                                //都道府県2桁＋"７"＋７桁
                                List<HyogoKoiki.Clinic> lstClinicTmp =
                                    (List<HyogoKoiki.Clinic>)DB.Main.Select<HyogoKoiki.Clinic>("clinicnum='" + strDrNum + "'");
                                if (lstClinicTmp.Count > 0)
                                {
                                    HyogoKoiki.Clinic c = lstClinicTmp[0];
                                    sb.Append(c.ClinicNum.ToString() + "\",\"");
                                    sb.Append(c.Name.ToString() + "\",\"");
                                    sb.Append(c.Zip.ToString() + "\",\"");
                                    sb.Append(c.Address.ToString() + "\",\"");
                                    cntClinic++;
                                    sbwf.Append(" 医療機関 ");
                                }
                                else
                                {
                                    sb.Append("\",\"\",\"\",\"\",\"");
                                }
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"");
                            }
                            #endregion

                            #region 被保険者
                            List<HyogoKoiki.Person> lstPersonTmp =
                                (List<HyogoKoiki.Person>)DB.Main.Select<HyogoKoiki.Person>("hihonum='" + itemApp.HihoNum + "'");
                            if (lstPersonTmp.Count > 0)
                            {
                                //被保険者              
                                sb.Append(lstPersonTmp[0].HihoNum + "\",\"");
                                sb.Append(lstPersonTmp[0].HihoName + "\",\"");
                                sb.Append(lstPersonTmp[0].Kana + "\",\"");
                                sb.Append(lstPersonTmp[0].Birthday.ToString() + "\",\"");
                                sb.Append(lstPersonTmp[0].Zip + "\",\"");
                                sb.Append(lstPersonTmp[0].Address1 + "\",\"");
                                sb.Append(lstPersonTmp[0].Address2 + "\",\"");
                                sb.Append(lstPersonTmp[0].Address3 + "\",\"");

                                cntHiho++;

                                sbwf.Append(" 被保険者 ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"");
                            }
                            #endregion

                            #region 送付先
                            List<HyogoKoiki.SendTo> lstSendToTmp =
                                (List<HyogoKoiki.SendTo>)DB.Main.Select<HyogoKoiki.SendTo>("hihonum='" + itemApp.HihoNum + "'");

                            if (lstSendToTmp.Count > 0)
                            {
                                //送付先                 
                                sb.Append(lstSendToTmp[0].HihoNum + "\",\"");
                                sb.Append(lstSendToTmp[0].DestName + "\",\"");
                                sb.Append(lstSendToTmp[0].DestKana + "\",\"");
                                sb.Append(lstSendToTmp[0].DestZip + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress1 + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress2 + "\",\"");
                                sb.Append(lstSendToTmp[0].DestAddress3 + "\",");

                                cntSendTo++;

                                sbwf.Append(" 送付先 ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",");
                            }
                            #endregion
                            break;

                        case (int)InsurerID.SHARP_KENPO:
                            #region 資格適用データ
                            List<SharpKenpo.TekiyouData> lstTekiyoDataTmp =
                                (List<SharpKenpo.TekiyouData>)DB.Main.Select<SharpKenpo.TekiyouData>(
                                    "syono='" + itemApp.HihoNum.ToString().Split('-')[1] + "'" +
                                    " and hgender='" + itemApp.Sex.ToString() + "'" +
                                    " and birthadymd='" + itemApp.Birthday.ToString("yyyy-MM-dd") + "'");

                            if (lstTekiyoDataTmp.Count > 0)
                            {
                                sb.Append(lstTekiyoDataTmp[0].Gno + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].SyoNo + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Branch + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Branch2 + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].famNo + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hKana + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hName + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hGender + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Zokugara + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossG + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossYMD + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].Ratio.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].issueDate.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].zip + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].add + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].BirthADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].GetADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].LossADYMD.ToString() + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].hihoNum + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].etc01 + "\",\"");
                                sb.Append(lstTekiyoDataTmp[0].issueADYMD.ToString() + "\",\"");

                                cntTekiyouData++;

                                sbwf.Append(" 資格適用データ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"," +
                                    "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"," +
                                    "\"\",\"\",\"\",\"\",\"\",\"");
                            }
                            #endregion

                            #region 支払先
                            List<Pay.PayCode> lstPayTmp =
                             (List<Pay.PayCode>)DB.Main.Select<Pay.PayCode>("substr(code,1,9)='" + itemApp.PayCode + "'");

                            //支払先コードの長さが９以上の場合のみ設定                            
                            if (lstPayTmp.Count > 0 && lstPayTmp[0].Code.Length >= 9)
                            {
                                //paycode                                
                                SharpKenpo.PayCodeImporter.PayKind pkDK = SharpKenpo.PayCodeImporter.getDK(lstPayTmp[0].Code.ToString());

                                sb.Append(lstPayTmp[0].Code + "\",\"");
                                sb.Append(pkDK.ToString() + "\",\"");
                                sb.Append(lstPayTmp[0].BankNo + "\",\"");
                                sb.Append(lstPayTmp[0].BankName + "\",\"");
                                sb.Append(lstPayTmp[0].BranchNo + "\",\"");
                                sb.Append(lstPayTmp[0].BranchName + "\",\"");
                                sb.Append(lstPayTmp[0].AccountNo + "\",\"");
                                sb.Append(lstPayTmp[0].AccountName + "\",\"");
                                sb.Append(lstPayTmp[0].AccountKana + "\",\"");
                                sb.Append(lstPayTmp[0].UpdateDate.ToString() + "\",");

                                cntPay++;
                                sbwf.Append(" 支払先 ");
                            }
                            else
                            {
                                sb.Append("\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",");
                            }
                            #endregion
                            break;

                        //20191002160051 furukawa st ////////////////////////
                        //交付料有無、前回支給
                        
                        case (int)InsurerID.OSAKA_KOIKI:
                            sb.Append(itemApp.TaggedDatas.flgKofuUmu.ToString() + "\",\"");
                            sb.Append(itemApp.TaggedDatas.PastSupplyYM.ToString() + "\",\"");
                            break;
                            //20191002160051 furukawa ed ////////////////////////
                    }

                    wf.LogPrint(sbwf.ToString());
                    sb.Remove(sb.Length - 1, 1);

                    //不要な文字列削除
                    string strAdd = sb.ToString();
                    strAdd = strAdd.Replace(DateTime.MinValue.ToString(), string.Empty);
                    strAdd = strAdd.Replace(" 0:00:00", string.Empty);
                    strAdd = strAdd.Replace("\r", " ");
                    strAdd = strAdd.Replace("\n", " ");

                    lstStr.Add(strAdd);
                }

                using (var f = new SaveFileDialog())
                {
                    f.FileName = cym + Insurer.CurrrentInsurer.InsurerName + "_UTF8.csv";

                    f.Filter = "*.csv|*.csv";
                    f.FilterIndex = 0;

                    if (f.ShowDialog() != DialogResult.OK) return;
                    if (exportCsv(lstStr, f.FileName, wf))
                    {
                        MessageBox.Show("正常終了");
                    }
                    else
                    {
                        MessageBox.Show("失敗");
                    }
                }
            }
        }

        // OCRは文字化けする為、タグデータは文字出力不可のため除外
        private static readonly string[] DumpAppHeaderList =
        {
            "プライマリーキー(Aid)", "スキャンID(ScanID)", "グループID(GroupID)",
            "診療和暦年(MediYear)", "診療和暦月(MediMonth)",
            "保険者番号(InsNum)", "被保険者記号番号(HihoNum)", "都道府県(HihoPref)", "被保険者拡張情報(HihoType)",
            "被保険者名(HihoName)", "被保険者郵便番号(HihoZip)", "被保険者住所(HihoAdd)",
            "受療者名(PersonName)", "受療者性別(Sex)", "受療者生年月日(Birthday)",
            "sid(ClinicNum)",
            "家族区分(Family)", "給付割合(Ratio)", "公費番号(PublcExpense)",
            "(ChargeYear)", "(ChargeMonth)",
            "負傷1　負傷名(FushoName1)", "負傷1　負傷年月日(FushoDate1)", "負傷1　初検日(FushoFirstDate1)",
            "負傷1　開始日(FushoStartDate1)", "負傷1　終了日(FushoFinishDate1)",
            "負傷1　日数(FushoDays1)", "負傷1　転帰(FushoCourse1)",
            "負傷2　負傷名(FushoName2)", "負傷2　負傷年月日(FushoDate2)", "負傷2　初検日(FushoFirstDate2)",
            "負傷2　開始日(FushoStartDate2)", "負傷2　終了日(FushoFinishDate2)",
            "負傷2　日数(FushoDays2)", "負傷2　転帰(FushoCourse2)",
            "負傷3　負傷名(FushoName3)", "負傷3　負傷年月日(FushoDate3)", "負傷3　初検日(FushoFirstDate3)",
            "負傷3　開始日(FushoStartDate3)", "負傷3　終了日(FushoFinishDate3)",
            "負傷3　日数(FushoDays3)", "負傷3　転帰(FushoCourse3)",
            "負傷4　負傷名(FushoName4)", "負傷4　負傷年月日(FushoDate4)", "負傷4　初検日(FushoFirstDate4)",
            "負傷4　開始日(FushoStartDate4)", "負傷4　終了日(FushoFinishDate4)",
            "負傷4　日数(FushoDays4)", "負傷4　転帰(FushoCourse4)",
            "負傷5　負傷名(FushoName5)", "負傷5　負傷年月日(FushoDate5)", "負傷5　初検日(FushoFirstDate5)",
            "負傷5　開始日(FushoStartDate5)", "負傷5　終了日(FushoFinishDate5)",
            "負傷5　日数(FushoDays5)", "負傷5　転帰(FushoCourse5)",
            "請求区分(NewContType)",
            "往療距離(Distance)", "往療回数(VisitTimes)", "往療料(VisitFee)", "往療料加算(VisitAdd)",
            "施術師番号(DrNum)", "施術所郵便番号(ClinicZip)", "施術所住所(ClinicAdd)",
            "施術所名(ClinicName)", "施術所電話番号(ClinicTel)", "施術師名(DrName)", "施術師カナ(DrKana)",
            "口座種別(AccountType)", "銀行名(BankName)", "銀行種別(BankType)", "支店名(BankBranch)", "支店種別(BankBranchType)",
            "口座名義(AccountName)", "口座名義カナ(AccountKana)", "口座番号(AccountNumber)",
            "合計額(Total)", "一部負担額(Partial)", "請求額(Charge)", "実日数(CountedDays)",
            "ナンバリング(Numbering)", "(AppType)",
            "1回目入力ユーザーID(Ufirst)", "2回目入力ユーザーID(Usecond)", "点検ユーザーID(Uinquiry)", "あんま部位(Bui)",
            "処理西暦年月(CYM)", "診療西暦年月(YM)",
            "ステータスフラグ(StatusFlags)", "照会理由(ShokaiReason)", "関連レセ情報ID(RrID)",
            "1回目追加入力ユーザーID(AdditionalUid1)", "2回目追加入力ユーザーID(AdditionalUid2)",
            "照会メモ(MemoShokai)", "点検メモ(MemoInspect)", "メモ(Memo)",
            "支払コード(PayCode)", "照会文書通知コード(ShokaiCode)",
            "1回目拡張入力ユーザーID(UfirstEx)", "2回目拡張入力ユーザーID(UsecondEx)",
            "過誤理由(KagoReasons)", "再審理由(SaishinsaReasons)", "返戻理由(HenreiReasons)",
            "電算管理番号(ComNum)", "グループ(GroupNum)", "出力メモ(OutMemo)",
            "請求年月和暦(JCYM)", "診療年月和暦(JYM)",
            "負傷数(FushoCount)", "(InputStatus)", "(InputStatusEx)", "(InputStatusAdd)",
            "照会/返戻(ShokaiHenreiStatus)", "点検結果(TenkenResult)", "往療点検結果(OryoResult)", "過誤理由文字列(KagoReasonStr)",
            "再審査理由文字列(SaishinsaReasonStr)", "照会理由文字列(ShokaiReasonStr)", "照会理由文字列(ShokaiResultStr)",
            "点検詳細(InspectDescription)", "返戻理由文字列(HenreiReasonStr)", "点検結果(InspectInfo)",
            "ステータス(InputOrderNumber)",
            "入力負傷数(taggeddatas.count)",//20191031165544 furukawa taggeddatas負傷数足す

            //20200712200829 furukawa st ////////////////////////
            //汎用文字列全部出す

            "交付有無(taggeddatas.flgKofuumu)",
            "同意有無(taggeddatas.flgSejutuDouiumu)",

            "汎用文字列1(taggeddatas.GeneralString1)",
            "汎用文字列2(taggeddatas.GeneralString2)",
            "汎用文字列3(taggeddatas.GeneralString3)",
            "汎用文字列4(taggeddatas.GeneralString4)",
            "汎用文字列5(taggeddatas.GeneralString5)",
            "汎用文字列6(taggeddatas.GeneralString6)",
            "汎用文字列7(taggeddatas.GeneralString7)",
            "汎用文字列8(taggeddatas.GeneralString8)",
            //20200712200829 furukawa ed ////////////////////////


        };

        private static string CreateHeaderDumpApp()
        {
            return string.Join(",", DumpAppHeaderList) + ",";
        }

        private static string CreateDataDumpApp(App app)
        {
            string[] data =
            {
                app.Aid.ToString(), app.ScanID.ToString(), app.GroupID.ToString(),
                app.MediYear.ToString(), app.MediMonth.ToString(),
                app.InsNum, app.HihoNum, app.HihoPref.ToString(), app.HihoType.ToString(),
                app.HihoName, app.HihoZip, app.HihoAdd,
                app.PersonName, app.Sex.ToString(), app.Birthday.ToString(),
                app.ClinicNum,
                app.Family.ToString(), app.Ratio.ToString(), app.PublcExpense,
                app.ChargeYear.ToString(), app.ChargeMonth.ToString(),
                app.FushoName1, app.FushoDate1.ToString(), app.FushoFirstDate1.ToString(),
                app.FushoStartDate1.ToString(), app.FushoFinishDate1.ToString(),
                app.FushoDays1.ToString(), app.FushoCourse1.ToString(),
                app.FushoName2, app.FushoDate2.ToString(), app.FushoFirstDate2.ToString(),
                app.FushoStartDate2.ToString(), app.FushoFinishDate2.ToString(),
                app.FushoDays2.ToString(), app.FushoCourse2.ToString(),
                app.FushoName3, app.FushoDate3.ToString(), app.FushoFirstDate3.ToString(),
                app.FushoStartDate3.ToString(), app.FushoFinishDate3.ToString(),
                app.FushoDays3.ToString(), app.FushoCourse3.ToString(),
                app.FushoName4, app.FushoDate4.ToString(), app.FushoFirstDate4.ToString(),
                app.FushoStartDate4.ToString(), app.FushoFinishDate4.ToString(),
                app.FushoDays4.ToString(), app.FushoCourse4.ToString(),
                app.FushoName5, app.FushoDate5.ToString(), app.FushoFirstDate5.ToString(),
                app.FushoStartDate5.ToString(), app.FushoFinishDate5.ToString(),
                app.FushoDays5.ToString(), app.FushoCourse5.ToString(),
                app.NewContType.ToString(),
                app.Distance.ToString(), app.VisitTimes.ToString(), app.VisitFee.ToString(), app.VisitAdd.ToString(),
                app.DrNum, app.ClinicZip, app.ClinicAdd,
                app.ClinicName, app.ClinicTel, app.DrName, app.DrKana,
                app.AccountType.ToString(), app.BankName, app.BankType.ToString(), app.BankBranch, app.BankBranchType.ToString(),
                app.AccountName, app.AccountKana, app.AccountNumber,
                app.Total.ToString(), app.Partial.ToString(), app.Charge.ToString(), app.CountedDays.ToString(),
                app.Numbering, app.AppType.ToString(),
                app.Ufirst.ToString(), app.Usecond.ToString(), app.Uinquiry.ToString(), app.Bui.ToString(),
                app.CYM.ToString(), app.YM.ToString(),

                //20191016163620 furukawa st ////////////////////////
                //ステータスフラグがカンマ区切りになっちまうのでカンマを｜に変換

                app.StatusFlags.ToString().Replace(',','|'), app.ShokaiReason.ToString().Replace(',','|'), app.RrID.ToString(),
                //app.StatusFlags.ToString(), app.ShokaiReason.ToString(), app.RrID.ToString(),
                //20191016163620 furukawa ed ////////////////////////


                app.AdditionalUid1.ToString(), app.AdditionalUid2.ToString(),
                app.MemoShokai, app.MemoInspect, app.Memo,
                app.PayCode, app.ShokaiCode,
                app.UfirstEx.ToString(), app.UsecondEx.ToString(),


                //20191016165221 furukawa st ////////////////////////
                //メモの中のカンマが区切りになっちまうのでカンマを｜に変換

                app.KagoReasons.ToString().Replace(",","|"), app.SaishinsaReasons.ToString().Replace(",","|"), app.HenreiReasons.ToString().Replace(",","|"),
                app.ComNum, app.GroupNum, app.OutMemo.Replace(",","|"),
                //app.KagoReasons.ToString(), app.SaishinsaReasons.ToString(), app.HenreiReasons.ToString(),
                //app.ComNum, app.GroupNum, app.OutMemo,
                //20191016165221 furukawa ed ////////////////////////



                app.JCYM, app.JYM,
                app.FushoCount.ToString(), app.InputStatus.ToString(), app.InputStatusEx.ToString(), app.InputStatusAdd,
                app.ShokaiHenreiStatus, app.TenkenResult, app.OryoResult, app.KagoReasonStr,
                app.SaishinsaReasonStr, app.ShokaiReasonStr, app.ShokaiResultStr,
                app.InspectDescription, app.HenreiReasonStr, app.InspectInfo,
                app.InputOrderNumber.ToString(),
                app.TaggedDatas.count.ToString(),//20191031165408 furukawa taggeddatas負傷数足す


                //20200712200647 furukawa st ////////////////////////
                //汎用文字列全部出す

                app.TaggedDatas.flgKofuUmu.ToString(),
                app.TaggedDatas.flgSejutuDouiUmu.ToString(),                
                app.TaggedDatas.GeneralString1.ToString(),
                app.TaggedDatas.GeneralString2.ToString(),
                app.TaggedDatas.GeneralString3.ToString(),
                app.TaggedDatas.GeneralString4.ToString(),
                app.TaggedDatas.GeneralString5.ToString(),
                app.TaggedDatas.GeneralString6.ToString(),
                app.TaggedDatas.GeneralString7.ToString(),
                app.TaggedDatas.GeneralString8.ToString(),
                //20200712200647 furukawa ed ////////////////////////


            };
            return string.Join(",", data.Select(item => $"\"{item}\""));
        }
    }
}