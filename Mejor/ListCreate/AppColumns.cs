﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mejor.ListCreate
{
    class AppColumns
    {
        static int scanInsID = 0;
        static int batchInsID = 0;
        static int subBatchInsID = 0;
        static int accountInsID = 0;
        static int scanCym = 0;
        static int batchCym = 0;
        static int subBatchCym = 0;

        static Dictionary<int, Scan> _scans = new Dictionary<int, Scan>();
        static List<App> _batchs = new List<App>();
        static List<App> _subBatchs = new List<App>();
        static Dictionary<string, Account> _accounts = new Dictionary<string, Account>();

        static Dictionary<int, Scan> getScans(int cym)
        {
            if (cym != scanCym || Insurer.CurrrentInsurer.InsurerID != scanInsID)
            {
                scanCym = cym;
                scanInsID = Insurer.CurrrentInsurer.InsurerID;
                _scans.Clear();
                var l = Scan.GetScanListYM(cym);
                l.ForEach(s => _scans.Add(s.SID, s));
            }

            return _scans;
        }

        static List<App> getBatchs(int cym)
        {
            if (cym != batchCym || Insurer.CurrrentInsurer.InsurerID != batchInsID)
            {
                batchCym = cym;
                batchInsID = Insurer.CurrrentInsurer.InsurerID;
                _batchs.Clear();
                var where = $"WHERE a.aapptype=" +
                    ((int)APP_TYPE.バッチ).ToString() +
                    $"AND a.CYM={cym} ";
                _batchs = App.GetAppsWithWhere(where)?.ToList() ?? new List<App>();
                _batchs.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            return _batchs;
        }

        static List<App> getSubBatchs(int cym)
        {
            if (cym != subBatchCym || Insurer.CurrrentInsurer.InsurerID != subBatchInsID)
            {
                subBatchCym = cym;
                subBatchInsID = Insurer.CurrrentInsurer.InsurerID;
                _subBatchs.Clear();
                var where = $"WHERE a.aapptype=" +
                    ((int)APP_TYPE.バッチ2).ToString() +
                    $"AND a.CYM={cym} ";
                _subBatchs = App.GetAppsWithWhere(where)?.ToList() ?? new List<App>();
                _subBatchs.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            return _subBatchs;
        }

        static Dictionary<string, Account> getAccounts()
        {
            if (Insurer.CurrrentInsurer.InsurerID != accountInsID)
            {
                accountInsID = Insurer.CurrrentInsurer.InsurerID;
                _accounts.Clear();
                if (Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済)
                {
                    var l = Account.GetAllAccount();
                    foreach (var item in l) _accounts.Add(item.Code, item);
                }
            }
            return _accounts;
        }

        public App App { get; }
        public AppColumns(App app) => this.App = app;//検索結果のリストに表示される列

        public bool Select { get => App.Select; set => App.Select = value; }
        public int ViewIndex
        {
            get => App.ViewIndex;
            set => App.ViewIndex = value;
        }

        public int AID => App.Aid;                                                  //プライマリーキー
        public int ScanID => App.ScanID;                                            //スキャンID
        public int GroupID => App.GroupID;                                          //グループID

        public int MediYear => App.YM;                                              //診療和暦年
        public int MediMonth => App.YM % 100;                                       //診療和暦月
        public string InsNum => Insurer.CurrrentInsurer.InsNumber;                  //保険者番号
        public string InputInsNum => App.InsNum;                                    

        public string HihoNum => App.HihoNum;                                       //被保険者記号番号
        public int HihoPref => App.HihoPref;                                        //都道府県
        public int HihoType => App.HihoType;                                        //被保険者拡張情報
        public string HihoName => App.HihoName;                                     //被保険者名
        public string HihoZip => App.HihoZip;                                       //被保険者郵便番号
        public string HihoZipHyphen => App.HihoZip.Length > 3 ?
            App.HihoZip.Remove(3) + "-" + App.HihoZip.Substring(3) :
            App.HihoZip;
        public string HihoAdd => App.HihoAdd;                                       //被保険者住所
        public string HihoAdd1 => App.HihoAdd.Contains("　") ?
            App.HihoAdd.Remove(App.HihoAdd.IndexOf("　")) :
            App.HihoAdd;
        public string HihoAdd2 => App.HihoAdd.Contains("　") ?
            App.HihoAdd.Substring(App.HihoAdd.IndexOf("　") + 1) :
            string.Empty;

        public string PersonName => App.PersonName;                                         //受療者名
        public string Sex => App.Sex == 1 ? "男" : App.Sex == 2 ? "女" : "";                //受療者性別
        public DateTime Birthday => App.Birthday;                                           //受療者生年月日
        public DateTime BirthdayEra => App.Birthday;
        public DateTime BirthdayYear => App.Birthday;
        public int BirthdayMonth => App.Birthday.Month;
        public int BirthdayDay => App.Birthday.Day;
        public int MediAge => DateTimeEx.GetAge(App.Birthday, YM);                          //受療時年齢
        public int ChargeAge => DateTimeEx.GetAge(App.Birthday, CYM);                       //請求月時年齢
        public int TodayAge => DateTimeEx.GetAge(App.Birthday, DateTime.Today);             //満年齢

        public string ClinicNum => App.ClinicNum;           //施術所コード（医療機関コード）
        public int Family => App.Family;                    //家族区分
        public int Ratio => App.Ratio;                      //給付割合

        public int ChargeYear => App.CYM;                   //請求年
        public int ChargeMonth => App.CYM % 100;            //請求月

        public string FushoName1 => App.FushoName1;                         //負傷1負傷名        
        public DateTime FushoDate1 => App.FushoDate1;                       //負傷年月日
        public DateTime FushoFirstDate1 => App.FushoFirstDate1;             //初検日
        public DateTime FushoStartDate1 => App.FushoStartDate1;             //開始日
        public DateTime FushoFinishDate1 => App.FushoFinishDate1;           //終了日
        public int FushoDays1 => App.FushoDays1;                            //日数
        public int FushoCourse1 => App.FushoCourse1;                        //転帰

        public string FushoName2 => App.FushoName2;                         //負傷2負傷名
        public DateTime FushoDate2 => App.FushoDate2;                       //負傷年月日
        public DateTime FushoFirstDate2 => App.FushoFirstDate2;             //初検日
        public DateTime FushoStartDate2 => App.FushoStartDate2;             //開始日
        public DateTime FushoFinishDate2 => App.FushoFinishDate2;           //終了日
        public int FushoDays2 => App.FushoDays2;                            //日数
        public int FushoCourse2 => App.FushoCourse2;                        //転帰

        public string FushoName3 => App.FushoName3;                         //負傷3負傷名
        public DateTime FushoDate3 => App.FushoDate3;                       //負傷年月日
        public DateTime FushoFirstDate3 => App.FushoFirstDate3;             //初検日
        public DateTime FushoStartDate3 => App.FushoStartDate3;             //開始日
        public DateTime FushoFinishDate3 => App.FushoFinishDate3;           //終了日
        public int FushoDays3 => App.FushoDays3;                            //日数
        public int FushoCourse3 => App.FushoCourse3;                        //転帰

        public string FushoName4 => App.FushoName4;                         //負傷4負傷名
        public DateTime FushoDate4 => App.FushoDate4;                       //負傷年月日
        public DateTime FushoFirstDate4 => App.FushoFirstDate4;             //初検日
        public DateTime FushoStartDate4 => App.FushoStartDate4;             //開始日
        public DateTime FushoFinishDate4 => App.FushoFinishDate4;           //終了日
        public int FushoDays4 => App.FushoDays4;                            //日数
        public int FushoCourse4 => App.FushoCourse4;                        //転帰

        public string FushoName5 => App.FushoName5;                         //負傷5負傷名
        public DateTime FushoDate5 => App.FushoDate5;                       //負傷年月日
        public DateTime FushoFirstDate5 => App.FushoFirstDate5;             //初検日
        public DateTime FushoStartDate5 => App.FushoStartDate5;             //開始日
        public DateTime FushoFinishDate5 => App.FushoFinishDate5;           //終了日
        public int FushoDays5 => App.FushoDays5;                            //日数
        public int FushoCourse5 => App.FushoCourse5;                        //転帰

        public string NewContType => App.NewContType == NEW_CONT.新規 ? "新規" :            //新規継続
            App.NewContType == NEW_CONT.継続 ? "継続" : string.Empty;
        // 尼崎があるので!=で判定する
        public string Distance => App.Distance != 0 ? "○" : "";             //往療距離
        public int VisitTimes => App.VisitTimes;                             //往療回数
        public int VisitFee => App.VisitFee;                                 //往療料
        public int VisitAdd => App.VisitAdd;                                 //往療料加算

        public string DrNum => App.DrNum;                                           //施術師番号
        public string ClinicZip => App.ClinicZip;                                   //施術所郵便番号
        public string ClinicZipHyphen => App.ClinicZip.Length > 3 ?                 
            App.ClinicZip.Remove(3) + "-" + App.ClinicZip.Substring(3) :
            App.ClinicZip;
        public string ClinicAdd => App.ClinicAdd;                               //施術所住所
        public string ClinicName => App.ClinicName;                             //施術所名
        public string ClinicTel => App.ClinicTel;                               //施術所電話番号
        public string DrName => App.DrName;                                     //施術師名
        public string DrKana => App.DrKana;                                     //施術師カナ
        public int AccountType => App.AccountType;                              //口座種別
        public string BankName => App.BankName;                                 //銀行名
        public int BankType => App.BankType;                                    //銀行種別
        public string BankBranch => App.BankBranch;                             //支店名
        public int BankBranchType => App.BankBranchType;                        //支店種別
        public string AccountName => App.AccountName;                           //口座名義
        public string AccountKana => App.AccountKana;                           //口座名義カナ
        public string AccountNumber => App.AccountNumber;                       //口座番号

        public int Total => App.Total;                                          //合計額
        public int Partial => App.Partial;                                      //一部負担額
        public int Charge => App.Charge;                                        //請求額
        public int Days => App.CountedDays;                                     //実日数
        public string Numbering => App.Numbering;                               //ナンバリング
        public string AppType => App.AppType.ToString();                        //申請書種別

        public int Bui => App.Bui;                                              //部位数

        public int CYM => App.CYM;                                              //処理西暦年月
        public int YM => App.YM;                                                //診療西暦年月
        public string ShokaiReasons => App.ShokaiReasonStr;                     //照会理由
        public int RrID { get; set; }                                           //関連レセ情報ID

        public string InputStatus => App.InputStatus.ToString();    
        public string ShokaiHenreiStatus => App.ShokaiHenreiStatus;
        public string TenkenResult => App.TenkenResult;
        public string OryoResult => App.OryoResult;
        public string Memo => App.Memo;                                         //メモ
        public string MemoShokai => App.MemoShokai;                             //照会メモ
        public string MemoInspect => App.MemoInspect;                           //点検メモ
        public string OutMemo => App.OutMemo;                                   //出力メモ

        public string PayCode => App.PayCode;                                   //支払コード
        public string ShokaiCode => App.ShokaiCode;                             //照会文書通知コード

        public string ShokaiResult => App.ShokaiResultStr;                      //照会理由
        public string KagoReasons => App.KagoReasonStr;                         //過誤理由
        public string SaishinsaReasons => App.SaishinsaReasonStr;               //再審理由
        public string HenreiReasons => App.HenreiReasonStr;                     //返戻理由
        public string ComNum => App.ComNum;                                     //電算管理番号
        public string GroupNum => App.GroupNum;                                 //グループ
        public string DestName => App.TaggedDatas.DestName;
        public string DestZip => App.TaggedDatas.DestZip;
        public string DestAdd => App.TaggedDatas.DestAdd;

        public DateTime ContactDt => App.TaggedDatas.ContactDt;
        public string ContactName => App.TaggedDatas.ContactName;
        public string ContactStatus => App.TaggedDatas.ContactStatusStr;
        public string Dates => App.TaggedDatas.Dates;
        public string KouhiNum => App.TaggedDatas.KouhiNum;
        public string JukyuNum => App.TaggedDatas.JukyuNum;

        public double AppDistance => App.TaggedDatas.AppDistance;
        public string DistCalcClinicAdd => App.TaggedDatas.DistCalcClinicAdd;
        public string DistCalcAdd => App.TaggedDatas.DistCalcAdd;
        public double CalcDistance => App.TaggedDatas.CalcDistance;

        public string ShokaiFlag => App.StatusFlagCheck(StatusFlag.照会対象) ? "○" : "";
        public string InspectFlag => App.StatusFlagCheck(StatusFlag.点検対象) ? "○" : "";
        public string OryoInspectFlag => App.StatusFlagCheck(StatusFlag.往療点検対象) ? "○" : "";
        public string HenreiFlag => App.StatusFlagCheck(StatusFlag.返戻) ? "○" : "";
        public string PendFlag => App.StatusFlagCheck(StatusFlag.支払保留) ? "○" : "";
        public string InputErrorFlag => App.StatusFlagCheck(StatusFlag.入力時エラー) ? "○" : "";
        public string TelFlag => App.StatusFlagCheck(StatusFlag.架電対象) ? "○" : "";
        public string PaidFlag => App.StatusFlagCheck(StatusFlag.支払済) ? "○" : "";
        public string Transaction1Flag => App.StatusFlagCheck(StatusFlag.処理1) ? "○" : "";
        public string Transaction2Flag => App.StatusFlagCheck(StatusFlag.処理2) ? "○" : "";
        public string Transaction3Flag => App.StatusFlagCheck(StatusFlag.処理3) ? "○" : "";
        public string Transaction4Flag => App.StatusFlagCheck(StatusFlag.処理4) ? "○" : "";
        public string Transaction5Flag => App.StatusFlagCheck(StatusFlag.処理5) ? "○" : "";

        public string ScanNote
        {
            get
            {
                var ss = getScans(CYM);
                return ss.ContainsKey(ScanID) ? ss[ScanID].Note1 : string.Empty;
            }
        }

        public string BatchNo
        {
            get
            {
                var bs = getBatchs(CYM);
                var batchNo = bs[0].Numbering;
                foreach (var item in bs)
                {
                    if (item.Aid > AID) return batchNo;
                    batchNo = item.Numbering;
                }
                return batchNo;
            }
        }


        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        public string GakkoShokaiCodeCreate =>
            $"{DateTimeEx.GetHsYearFromAd(CYM / 100, CYM % 100).ToString("00")}" +
            $"{(CYM % 100).ToString("00")}-" +
            $"{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-" +
            $"{ViewIndex.ToString("000")}";

        /* public string GakkoShokaiCodeCreate =>
     $"{DateTimeEx.GetHsYearFromAd(CYM / 100).ToString("00")}" +
     $"{(CYM % 100).ToString("00")}-" +
     $"{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-" +
     $"{ViewIndex.ToString("000")}";*/
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


        public string GakkoDantaiName
        {
            get
            {
                var bs = getBatchs(CYM);
                if (bs.Count == 0) return string.Empty;
                var acs = getAccounts();
                if (acs.Count == 0) return string.Empty;

                var num = bs[0].AccountNumber;
                foreach (var item in bs)
                {
                    if (item.Aid > AID) break;
                    num = item.AccountNumber;
                }

                return acs.ContainsKey(num) ? acs[num].Name : string.Empty;
            }
        }

        public string GakkoAccountNo
        {
            get
            {
                var bs = getBatchs(CYM);

                if (bs.Count == 0) return string.Empty;

                var num = bs[0].AccountNumber;
                foreach (var item in bs)
                {
                    if (item.Aid > AID) return num;
                    num = item.AccountNumber;
                }

                return num;
            }
        }

        public string GakkoHenreiCode =>
            Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済 &&
            App.StatusFlagCheck(StatusFlag.返戻) ?
                $"{App.CYM % 10000}-" +
                $"{Insurer.CurrrentInsurer.InsurerName.Substring(7, 2)}-" +
                $"{App.Aid.ToString("00000000")}" :
                string.Empty;

        public string Blank => string.Empty;

        // AppColumnsには無かった分の追加（リスト表示用）
        public string JCYM => App.JCYM;
        public string JYM => App.JYM;
        public string PublcExpense => App.PublcExpense;
        public int AppChargeYear => App.ChargeYear;
        public int AppChargeMonth => App.ChargeMonth;
        public int Ufirst => App.Ufirst;
        public int Usecond => App.Usecond;
        public int Uinquiry => App.Uinquiry;
        public int UfirstEx => App.UfirstEx;
        public int UsecondEx => App.UsecondEx;
        public int AdditionalUid1 => App.AdditionalUid1;
        public int AdditionalUid2 => App.AdditionalUid2;
        public int FushoCount => App.FushoCount;
        public string InputStatusEx => App.InputStatusEx.ToString();
        public string InputStatusAdd => App.InputStatusAdd;
        public string InspectDescription => App.InspectDescription;
        public string InspectInfo => App.InspectInfo;
        public int InputOrderNumber => App.InputOrderNumber;

        // AppColumnsには無かった分(TaggedDatas)の追加（リスト表示用）
        public int RelationAID => App.TaggedDatas.RelationAID;
        public string Kana => App.TaggedDatas.Kana;
        public string DestKana => App.TaggedDatas.DestKana;
        public string InsName => App.TaggedDatas.InsName;
        public DateTime DouiDate => App.TaggedDatas.DouiDate;
        public string PastSupplyYM => App.TaggedDatas.PastSupplyYM;
        public string flgSejutuDouiUmu => App.TaggedDatas.flgSejutuDouiUmu ? "○" : "";
        public string flgKofuUmu => App.TaggedDatas.flgKofuUmu ? "○" : "";
        public string flgSikaku_SikakuNotMatch => App.TaggedDatas.flgSikaku_SikakuNotMatch ? "○" : "";
        public string flgSikaku_BirthNotMatch => App.TaggedDatas.flgSikaku_BirthNotMatch ? "○" : "";
        public string flgSikaku_GenderNotMatch => App.TaggedDatas.flgSikaku_GenderNotMatch ? "○" : "";
        public string flgSikaku_RatioNotMatch => App.TaggedDatas.flgSikaku_RatioNotMatch ? "○" : "";
        public string flgSikaku_RangeNotMatch => App.TaggedDatas.flgSikaku_RangeNotMatch ? "○" : "";
        public string flgSikaku_SinryoBeforeIssue => App.TaggedDatas.flgSikaku_SinryoBeforeIssue ? "○" : "";
        public int count => App.TaggedDatas.count;
        public string GeneralString1 => App.TaggedDatas.GeneralString1;

        //20210408111004 furukawa st ////////////////////////
        //汎用文字列2～8を追加
        
        public string GeneralString2 => App.TaggedDatas.GeneralString2;
        public string GeneralString3 => App.TaggedDatas.GeneralString3;
        public string GeneralString4 => App.TaggedDatas.GeneralString4;
        public string GeneralString5 => App.TaggedDatas.GeneralString5;
        public string GeneralString6 => App.TaggedDatas.GeneralString6;
        public string GeneralString7 => App.TaggedDatas.GeneralString7;
        public string GeneralString8 => App.TaggedDatas.GeneralString8;
        //20210408111004 furukawa ed ////////////////////////
    }
}
