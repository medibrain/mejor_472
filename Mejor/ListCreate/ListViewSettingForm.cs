﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    /// <summary>
    /// リスト作成画面の表示項目設定画面
    /// </summary>
    public partial class ListViewSettingForm : Form
    {
        class DisplayItem
        {
            // キー（非表示）
            public string Key;
            // 表示するかどうかのチェック
            public bool Visible { get; set; }
            // 表示名
            public string Name { get; set; }
        }

        // ユーザに選択させないキー
        private const string IgnoreKey = nameof(App.Select);

        // 現在のリスト作成画面の表示設定リスト
        private Dictionary<string, ListCreateForm.HeaderSetting> list;
        private readonly BindingSource source = new BindingSource();

        public ListViewSettingForm(Dictionary<string, ListCreateForm.HeaderSetting> list)
        {
            InitializeComponent();

            this.list = list;

            var newList = new List<DisplayItem>();
            source.DataSource = newList;
            dataGridViewSetting.DataSource = source;
            dataGridViewSetting.AutoGenerateColumns = false;

            // 表示順に並び替えして表示
            var sortedList = list.OrderBy(item => item.Value.DisplayIndex).ToArray();
            foreach (var pair in sortedList)
            {
                var key = pair.Key;
                var item = list[key];
                if (key != IgnoreKey)
                {
                    newList.Add(new DisplayItem { Key = key, Visible = item.Visible, Name = item.Name });
                }
            }

            source.ResetBindings(true);

            // ヘッダの設定
            var nameColumn = dataGridViewSetting.Columns[nameof(DisplayItem.Name)];
            if (nameColumn != null)
            {
                nameColumn.ReadOnly = true;
                nameColumn.HeaderText = "項目名";
                nameColumn.Width = 200;
            }
            var visibleColumn = dataGridViewSetting.Columns[nameof(DisplayItem.Visible)];
            if (visibleColumn != null)
            {
                visibleColumn.ReadOnly = false;
                visibleColumn.HeaderText = "表示";
                visibleColumn.Width = 50;
            }
            dataGridViewSetting.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void ButtonOK_Click(object sender, System.EventArgs e)
        {
            var items = source.DataSource as List<DisplayItem>;
            if (items == null)
            {
                return;
            }

            // 表示順と表示状態を更新
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                list[item.Key].DisplayIndex = i + 1;
                list[item.Key].Visible = item.Visible;
            }
            list[IgnoreKey].DisplayIndex = 0;

            Close();
        }

        private void ButtonCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void ButtonDown_Click(object sender, System.EventArgs e)
        {
            // 「↓」を押したので入れ替え
            var cell = dataGridViewSetting.CurrentCell;
            var newList = source.DataSource as List<DisplayItem>;
            if (cell == null || newList == null)
            {
                return;
            }

            var index = cell.RowIndex;
            if (index >= newList.Count)
            {
                return;
            }
            var temp = newList[index];
            newList.RemoveAt(index);
            newList.Insert(index + 1, temp);
            source.ResetBindings(false);
            // 入れ替え対象を選択状態に
            dataGridViewSetting.CurrentCell = dataGridViewSetting.Rows[index + 1].Cells[0];
        }

        private void ButtonUp_Click(object sender, System.EventArgs e)
        {
            // 「↑」を押したので入れ替え
            var cell = dataGridViewSetting.CurrentCell;
            var newList = source.DataSource as List<DisplayItem>;
            if (cell == null || newList == null)
            {
                return;
            }

            var index = cell.RowIndex;
            if (index <= 0)
            {
                return;
            }
            var temp = newList[index];
            newList.RemoveAt(index);
            newList.Insert(index - 1, temp);
            source.ResetBindings(false);
            // 入れ替え対象を選択状態に
            dataGridViewSetting.CurrentCell = dataGridViewSetting.Rows[index - 1].Cells[0];
        }

        // 「先頭」ボタン（表示項目が多いので先頭ボタンだけまず追加）
        private void buttonTop_Click(object sender, System.EventArgs e)
        {
            var cell = dataGridViewSetting.CurrentCell;
            var newList = source.DataSource as List<DisplayItem>;
            if (cell == null || newList == null)
            {
                return;
            }

            var index = cell.RowIndex;
            if (index <= 0)
            {
                return;
            }
            var temp = newList[index];
            newList.RemoveAt(index);
            newList.Insert(0, temp);
            source.ResetBindings(false);
            // 入れ替え対象を選択状態に
            dataGridViewSetting.CurrentCell = dataGridViewSetting.Rows[0].Cells[0];
        }
    }
}
