﻿namespace Mejor.ListCreate
{
    partial class ListExportSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.textBoxCell = new System.Windows.Forms.TextBox();
            this.labelCell = new System.Windows.Forms.Label();
            this.checkBoxExcelBase = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.radioButtonInsurerType = new System.Windows.Forms.RadioButton();
            this.radioButtonInsurer = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxZenkaku = new System.Windows.Forms.CheckBox();
            this.checkBoxDateZero = new System.Windows.Forms.CheckBox();
            this.panelDate = new System.Windows.Forms.Panel();
            this.radioButtonJPEra = new System.Windows.Forms.RadioButton();
            this.radioButtonJpNone = new System.Windows.Forms.RadioButton();
            this.radioButtonJpShort = new System.Windows.Forms.RadioButton();
            this.radioButtonJpKanji = new System.Windows.Forms.RadioButton();
            this.radioButtonAd = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.panelExcel = new System.Windows.Forms.Panel();
            this.radioButtonData = new System.Windows.Forms.RadioButton();
            this.radioButtonMoji = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.panelSeparater = new System.Windows.Forms.Panel();
            this.radioButtonSepNone = new System.Windows.Forms.RadioButton();
            this.radioButtonKanji = new System.Windows.Forms.RadioButton();
            this.radioButtonSlash = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxOnName = new System.Windows.Forms.TextBox();
            this.textBoxOnSample = new System.Windows.Forms.TextBox();
            this.textBoxOnWord = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxOnHeader = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBoxHeader = new System.Windows.Forms.CheckBox();
            this.radioButtonExcel = new System.Windows.Forms.RadioButton();
            this.radioButtonSJIS = new System.Windows.Forms.RadioButton();
            this.radioButtonUTF8 = new System.Windows.Forms.RadioButton();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewOn = new System.Windows.Forms.DataGridView();
            this.cms = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewOff = new System.Windows.Forms.DataGridView();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonTextUTF8 = new System.Windows.Forms.RadioButton();
            this.radioButtonTextSJIS = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxSep = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxFind = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelDate.SuspendLayout();
            this.panelExcel.SuspendLayout();
            this.panelSeparater.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOn)).BeginInit();
            this.cms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOff)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(1380, 808);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 31);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "登録";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelete.Location = new System.Drawing.Point(15, 808);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(126, 31);
            this.buttonDelete.TabIndex = 5;
            this.buttonDelete.Text = "設定の削除";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(1488, 808);
            this.buttonClose.Margin = new System.Windows.Forms.Padding(4);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(100, 31);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.Text = "キャンセル";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // textBoxCell
            // 
            this.textBoxCell.Enabled = false;
            this.textBoxCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCell.Location = new System.Drawing.Point(111, 116);
            this.textBoxCell.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCell.Name = "textBoxCell";
            this.textBoxCell.Size = new System.Drawing.Size(67, 24);
            this.textBoxCell.TabIndex = 47;
            // 
            // labelCell
            // 
            this.labelCell.AutoSize = true;
            this.labelCell.Enabled = false;
            this.labelCell.Location = new System.Drawing.Point(39, 120);
            this.labelCell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCell.Name = "labelCell";
            this.labelCell.Size = new System.Drawing.Size(62, 16);
            this.labelCell.TabIndex = 46;
            this.labelCell.Text = "開始セル";
            // 
            // checkBoxExcelBase
            // 
            this.checkBoxExcelBase.AutoSize = true;
            this.checkBoxExcelBase.Enabled = false;
            this.checkBoxExcelBase.Location = new System.Drawing.Point(38, 91);
            this.checkBoxExcelBase.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxExcelBase.Name = "checkBoxExcelBase";
            this.checkBoxExcelBase.Size = new System.Drawing.Size(134, 20);
            this.checkBoxExcelBase.TabIndex = 45;
            this.checkBoxExcelBase.Text = "ベースファイル使用";
            this.checkBoxExcelBase.UseVisualStyleBackColor = true;
            this.checkBoxExcelBase.CheckedChanged += new System.EventHandler(this.checkBoxExcelBase_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonAll);
            this.panel1.Controls.Add(this.radioButtonInsurerType);
            this.panel1.Controls.Add(this.radioButtonInsurer);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Location = new System.Drawing.Point(17, 471);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 102);
            this.panel1.TabIndex = 44;
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Location = new System.Drawing.Point(9, 76);
            this.radioButtonAll.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(63, 20);
            this.radioButtonAll.TabIndex = 8;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "すべて";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            // 
            // radioButtonInsurerType
            // 
            this.radioButtonInsurerType.AutoSize = true;
            this.radioButtonInsurerType.Location = new System.Drawing.Point(9, 50);
            this.radioButtonInsurerType.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonInsurerType.Name = "radioButtonInsurerType";
            this.radioButtonInsurerType.Size = new System.Drawing.Size(103, 20);
            this.radioButtonInsurerType.TabIndex = 8;
            this.radioButtonInsurerType.TabStop = true;
            this.radioButtonInsurerType.Text = "保険者タイプ";
            this.radioButtonInsurerType.UseVisualStyleBackColor = true;
            // 
            // radioButtonInsurer
            // 
            this.radioButtonInsurer.AutoSize = true;
            this.radioButtonInsurer.Location = new System.Drawing.Point(9, 26);
            this.radioButtonInsurer.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonInsurer.Name = "radioButtonInsurer";
            this.radioButtonInsurer.Size = new System.Drawing.Size(71, 20);
            this.radioButtonInsurer.TabIndex = 8;
            this.radioButtonInsurer.TabStop = true;
            this.radioButtonInsurer.Text = "保険者";
            this.radioButtonInsurer.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 5);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 16);
            this.label14.TabIndex = 7;
            this.label14.Text = "設定対象";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.checkBoxZenkaku);
            this.groupBox2.Controls.Add(this.checkBoxDateZero);
            this.groupBox2.Controls.Add(this.panelDate);
            this.groupBox2.Controls.Add(this.panelExcel);
            this.groupBox2.Controls.Add(this.panelSeparater);
            this.groupBox2.Controls.Add(this.textBoxOnName);
            this.groupBox2.Controls.Add(this.textBoxOnSample);
            this.groupBox2.Controls.Add(this.textBoxOnWord);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBoxOnHeader);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(1408, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(180, 788);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "項目別設定";
            // 
            // checkBoxZenkaku
            // 
            this.checkBoxZenkaku.AutoSize = true;
            this.checkBoxZenkaku.Location = new System.Drawing.Point(17, 643);
            this.checkBoxZenkaku.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxZenkaku.Name = "checkBoxZenkaku";
            this.checkBoxZenkaku.Size = new System.Drawing.Size(57, 20);
            this.checkBoxZenkaku.TabIndex = 12;
            this.checkBoxZenkaku.Text = "全角";
            this.checkBoxZenkaku.UseVisualStyleBackColor = true;
            this.checkBoxZenkaku.Visible = false;
            this.checkBoxZenkaku.CheckedChanged += new System.EventHandler(this.checkBoxZenkaku_CheckedChanged);
            // 
            // checkBoxDateZero
            // 
            this.checkBoxDateZero.AutoSize = true;
            this.checkBoxDateZero.Location = new System.Drawing.Point(17, 612);
            this.checkBoxDateZero.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDateZero.Name = "checkBoxDateZero";
            this.checkBoxDateZero.Size = new System.Drawing.Size(122, 20);
            this.checkBoxDateZero.TabIndex = 9;
            this.checkBoxDateZero.Text = "月日0(ゼロ)埋め";
            this.checkBoxDateZero.UseVisualStyleBackColor = true;
            this.checkBoxDateZero.CheckedChanged += new System.EventHandler(this.checkBoxDateZero_CheckedChanged);
            // 
            // panelDate
            // 
            this.panelDate.Controls.Add(this.radioButtonJPEra);
            this.panelDate.Controls.Add(this.radioButtonJpNone);
            this.panelDate.Controls.Add(this.radioButtonJpShort);
            this.panelDate.Controls.Add(this.radioButtonJpKanji);
            this.panelDate.Controls.Add(this.radioButtonAd);
            this.panelDate.Controls.Add(this.label12);
            this.panelDate.Location = new System.Drawing.Point(12, 463);
            this.panelDate.Margin = new System.Windows.Forms.Padding(4);
            this.panelDate.Name = "panelDate";
            this.panelDate.Size = new System.Drawing.Size(163, 141);
            this.panelDate.TabIndex = 8;
            // 
            // radioButtonJPEra
            // 
            this.radioButtonJPEra.AutoSize = true;
            this.radioButtonJPEra.Location = new System.Drawing.Point(30, 48);
            this.radioButtonJPEra.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonJPEra.Name = "radioButtonJPEra";
            this.radioButtonJPEra.Size = new System.Drawing.Size(86, 20);
            this.radioButtonJPEra.TabIndex = 4;
            this.radioButtonJPEra.TabStop = true;
            this.radioButtonJPEra.Text = "和暦数字";
            this.radioButtonJPEra.UseVisualStyleBackColor = true;
            this.radioButtonJPEra.CheckedChanged += new System.EventHandler(this.radioButtonDateView_CheckedChanged);
            // 
            // radioButtonJpNone
            // 
            this.radioButtonJpNone.AutoSize = true;
            this.radioButtonJpNone.Location = new System.Drawing.Point(30, 113);
            this.radioButtonJpNone.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonJpNone.Name = "radioButtonJpNone";
            this.radioButtonJpNone.Size = new System.Drawing.Size(110, 20);
            this.radioButtonJpNone.TabIndex = 4;
            this.radioButtonJpNone.TabStop = true;
            this.radioButtonJpNone.Text = "和暦年号なし";
            this.radioButtonJpNone.UseVisualStyleBackColor = true;
            this.radioButtonJpNone.CheckedChanged += new System.EventHandler(this.radioButtonDateView_CheckedChanged);
            // 
            // radioButtonJpShort
            // 
            this.radioButtonJpShort.AutoSize = true;
            this.radioButtonJpShort.Location = new System.Drawing.Point(30, 91);
            this.radioButtonJpShort.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonJpShort.Name = "radioButtonJpShort";
            this.radioButtonJpShort.Size = new System.Drawing.Size(83, 20);
            this.radioButtonJpShort.TabIndex = 3;
            this.radioButtonJpShort.TabStop = true;
            this.radioButtonJpShort.Text = "和暦ABC";
            this.radioButtonJpShort.UseVisualStyleBackColor = true;
            this.radioButtonJpShort.CheckedChanged += new System.EventHandler(this.radioButtonDateView_CheckedChanged);
            // 
            // radioButtonJpKanji
            // 
            this.radioButtonJpKanji.AutoSize = true;
            this.radioButtonJpKanji.Location = new System.Drawing.Point(30, 70);
            this.radioButtonJpKanji.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonJpKanji.Name = "radioButtonJpKanji";
            this.radioButtonJpKanji.Size = new System.Drawing.Size(86, 20);
            this.radioButtonJpKanji.TabIndex = 2;
            this.radioButtonJpKanji.TabStop = true;
            this.radioButtonJpKanji.Text = "和暦漢字";
            this.radioButtonJpKanji.UseVisualStyleBackColor = true;
            this.radioButtonJpKanji.CheckedChanged += new System.EventHandler(this.radioButtonDateView_CheckedChanged);
            // 
            // radioButtonAd
            // 
            this.radioButtonAd.AutoSize = true;
            this.radioButtonAd.Location = new System.Drawing.Point(30, 26);
            this.radioButtonAd.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonAd.Name = "radioButtonAd";
            this.radioButtonAd.Size = new System.Drawing.Size(56, 20);
            this.radioButtonAd.TabIndex = 1;
            this.radioButtonAd.TabStop = true;
            this.radioButtonAd.Text = "西暦";
            this.radioButtonAd.UseVisualStyleBackColor = true;
            this.radioButtonAd.CheckedChanged += new System.EventHandler(this.radioButtonDateView_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "歴表示";
            // 
            // panelExcel
            // 
            this.panelExcel.Controls.Add(this.radioButtonData);
            this.panelExcel.Controls.Add(this.radioButtonMoji);
            this.panelExcel.Controls.Add(this.label13);
            this.panelExcel.Location = new System.Drawing.Point(11, 272);
            this.panelExcel.Margin = new System.Windows.Forms.Padding(4);
            this.panelExcel.Name = "panelExcel";
            this.panelExcel.Size = new System.Drawing.Size(163, 73);
            this.panelExcel.TabIndex = 6;
            // 
            // radioButtonData
            // 
            this.radioButtonData.AutoSize = true;
            this.radioButtonData.Location = new System.Drawing.Point(31, 26);
            this.radioButtonData.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonData.Name = "radioButtonData";
            this.radioButtonData.Size = new System.Drawing.Size(90, 20);
            this.radioButtonData.TabIndex = 1;
            this.radioButtonData.TabStop = true;
            this.radioButtonData.Text = "数値/日付";
            this.radioButtonData.UseVisualStyleBackColor = true;
            this.radioButtonData.CheckedChanged += new System.EventHandler(this.radioButtonData_CheckedChanged);
            // 
            // radioButtonMoji
            // 
            this.radioButtonMoji.AutoSize = true;
            this.radioButtonMoji.Location = new System.Drawing.Point(31, 47);
            this.radioButtonMoji.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonMoji.Name = "radioButtonMoji";
            this.radioButtonMoji.Size = new System.Drawing.Size(71, 20);
            this.radioButtonMoji.TabIndex = 2;
            this.radioButtonMoji.TabStop = true;
            this.radioButtonMoji.Text = "文字列";
            this.radioButtonMoji.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "Excel時データ";
            // 
            // panelSeparater
            // 
            this.panelSeparater.Controls.Add(this.radioButtonSepNone);
            this.panelSeparater.Controls.Add(this.radioButtonKanji);
            this.panelSeparater.Controls.Add(this.radioButtonSlash);
            this.panelSeparater.Controls.Add(this.label11);
            this.panelSeparater.Location = new System.Drawing.Point(11, 356);
            this.panelSeparater.Margin = new System.Windows.Forms.Padding(4);
            this.panelSeparater.Name = "panelSeparater";
            this.panelSeparater.Size = new System.Drawing.Size(163, 96);
            this.panelSeparater.TabIndex = 7;
            // 
            // radioButtonSepNone
            // 
            this.radioButtonSepNone.AutoSize = true;
            this.radioButtonSepNone.Location = new System.Drawing.Point(31, 68);
            this.radioButtonSepNone.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonSepNone.Name = "radioButtonSepNone";
            this.radioButtonSepNone.Size = new System.Drawing.Size(50, 20);
            this.radioButtonSepNone.TabIndex = 3;
            this.radioButtonSepNone.TabStop = true;
            this.radioButtonSepNone.Text = "なし";
            this.radioButtonSepNone.UseVisualStyleBackColor = true;
            this.radioButtonSepNone.CheckedChanged += new System.EventHandler(this.radioButtonSepalate_CheckedChanged);
            // 
            // radioButtonKanji
            // 
            this.radioButtonKanji.AutoSize = true;
            this.radioButtonKanji.Location = new System.Drawing.Point(31, 47);
            this.radioButtonKanji.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonKanji.Name = "radioButtonKanji";
            this.radioButtonKanji.Size = new System.Drawing.Size(56, 20);
            this.radioButtonKanji.TabIndex = 2;
            this.radioButtonKanji.TabStop = true;
            this.radioButtonKanji.Text = "漢字";
            this.radioButtonKanji.UseVisualStyleBackColor = true;
            this.radioButtonKanji.CheckedChanged += new System.EventHandler(this.radioButtonSepalate_CheckedChanged);
            // 
            // radioButtonSlash
            // 
            this.radioButtonSlash.AutoSize = true;
            this.radioButtonSlash.Location = new System.Drawing.Point(31, 26);
            this.radioButtonSlash.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonSlash.Name = "radioButtonSlash";
            this.radioButtonSlash.Size = new System.Drawing.Size(78, 20);
            this.radioButtonSlash.TabIndex = 1;
            this.radioButtonSlash.TabStop = true;
            this.radioButtonSlash.Text = "スラッシュ";
            this.radioButtonSlash.UseVisualStyleBackColor = true;
            this.radioButtonSlash.CheckedChanged += new System.EventHandler(this.radioButtonSepalate_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 5);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "日付区切り";
            // 
            // textBoxOnName
            // 
            this.textBoxOnName.Location = new System.Drawing.Point(9, 42);
            this.textBoxOnName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOnName.Name = "textBoxOnName";
            this.textBoxOnName.ReadOnly = true;
            this.textBoxOnName.Size = new System.Drawing.Size(160, 22);
            this.textBoxOnName.TabIndex = 1;
            // 
            // textBoxOnSample
            // 
            this.textBoxOnSample.Location = new System.Drawing.Point(13, 710);
            this.textBoxOnSample.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOnSample.Multiline = true;
            this.textBoxOnSample.Name = "textBoxOnSample";
            this.textBoxOnSample.ReadOnly = true;
            this.textBoxOnSample.Size = new System.Drawing.Size(160, 70);
            this.textBoxOnSample.TabIndex = 11;
            // 
            // textBoxOnWord
            // 
            this.textBoxOnWord.Location = new System.Drawing.Point(9, 176);
            this.textBoxOnWord.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOnWord.Multiline = true;
            this.textBoxOnWord.Name = "textBoxOnWord";
            this.textBoxOnWord.Size = new System.Drawing.Size(160, 77);
            this.textBoxOnWord.TabIndex = 5;
            this.textBoxOnWord.TextChanged += new System.EventHandler(this.textBoxOnWord_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 688);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "サンプル";
            // 
            // textBoxOnHeader
            // 
            this.textBoxOnHeader.Location = new System.Drawing.Point(9, 94);
            this.textBoxOnHeader.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOnHeader.Name = "textBoxOnHeader";
            this.textBoxOnHeader.Size = new System.Drawing.Size(160, 22);
            this.textBoxOnHeader.TabIndex = 3;
            this.textBoxOnHeader.TextChanged += new System.EventHandler(this.textBoxOnHeader_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 127);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 45);
            this.label8.TabIndex = 4;
            this.label8.Text = "追加ワード\r\n＋を前に付けると前に追加\r\n＋を付けない場合は後に追加";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 74);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "ヘッダー";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "項目名";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(121, 610);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(65, 22);
            this.numericUpDown1.TabIndex = 12;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // checkBoxHeader
            // 
            this.checkBoxHeader.AutoSize = true;
            this.checkBoxHeader.Location = new System.Drawing.Point(14, 240);
            this.checkBoxHeader.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxHeader.Name = "checkBoxHeader";
            this.checkBoxHeader.Size = new System.Drawing.Size(165, 20);
            this.checkBoxHeader.TabIndex = 35;
            this.checkBoxHeader.Text = "ヘッダー(見出し行)出力";
            this.checkBoxHeader.UseVisualStyleBackColor = true;
            // 
            // radioButtonExcel
            // 
            this.radioButtonExcel.AutoSize = true;
            this.radioButtonExcel.Location = new System.Drawing.Point(19, 68);
            this.radioButtonExcel.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonExcel.Name = "radioButtonExcel";
            this.radioButtonExcel.Size = new System.Drawing.Size(59, 20);
            this.radioButtonExcel.TabIndex = 34;
            this.radioButtonExcel.TabStop = true;
            this.radioButtonExcel.Text = "Excel";
            this.radioButtonExcel.CheckedChanged += new System.EventHandler(this.radioButtonExcel_CheckedChanged);
            // 
            // radioButtonSJIS
            // 
            this.radioButtonSJIS.AutoSize = true;
            this.radioButtonSJIS.Location = new System.Drawing.Point(19, 44);
            this.radioButtonSJIS.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonSJIS.Name = "radioButtonSJIS";
            this.radioButtonSJIS.Size = new System.Drawing.Size(103, 20);
            this.radioButtonSJIS.TabIndex = 33;
            this.radioButtonSJIS.TabStop = true;
            this.radioButtonSJIS.Text = "CSV Shift JIS";
            // 
            // radioButtonUTF8
            // 
            this.radioButtonUTF8.AutoSize = true;
            this.radioButtonUTF8.Location = new System.Drawing.Point(19, 22);
            this.radioButtonUTF8.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonUTF8.Name = "radioButtonUTF8";
            this.radioButtonUTF8.Size = new System.Drawing.Size(90, 20);
            this.radioButtonUTF8.TabIndex = 32;
            this.radioButtonUTF8.TabStop = true;
            this.radioButtonUTF8.Text = "CSV UTF8";
            this.radioButtonUTF8.UseVisualStyleBackColor = true;
            // 
            // buttonDown
            // 
            this.buttonDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDown.Location = new System.Drawing.Point(628, 581);
            this.buttonDown.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(55, 31);
            this.buttonDown.TabIndex = 42;
            this.buttonDown.Text = "↓";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUp.Location = new System.Drawing.Point(628, 543);
            this.buttonUp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(55, 31);
            this.buttonUp.TabIndex = 41;
            this.buttonUp.Text = "↑";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(13, 160);
            this.textBoxFileName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(317, 22);
            this.textBoxFileName.TabIndex = 30;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(13, 102);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(317, 22);
            this.textBoxName.TabIndex = 27;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(13, 48);
            this.textBoxID.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.ReadOnly = true;
            this.textBoxID.Size = new System.Drawing.Size(317, 22);
            this.textBoxID.TabIndex = 25;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(10, 188);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 26);
            this.label15.TabIndex = 29;
            this.label15.Text = "{cym} で処理年月\r\n{ins} で保険者名";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 144);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 16);
            this.label9.TabIndex = 28;
            this.label9.Text = "初期ファイル名";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 86);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "設定名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 16);
            this.label3.TabIndex = 24;
            this.label3.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(394, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 36;
            this.label1.Text = "出力項目";
            // 
            // dataGridViewOn
            // 
            this.dataGridViewOn.AllowUserToAddRows = false;
            this.dataGridViewOn.AllowUserToDeleteRows = false;
            this.dataGridViewOn.AllowUserToResizeRows = false;
            this.dataGridViewOn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewOn.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewOn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOn.ContextMenuStrip = this.cms;
            this.dataGridViewOn.Location = new System.Drawing.Point(691, 28);
            this.dataGridViewOn.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewOn.MultiSelect = false;
            this.dataGridViewOn.Name = "dataGridViewOn";
            this.dataGridViewOn.ReadOnly = true;
            this.dataGridViewOn.RowHeadersVisible = false;
            this.dataGridViewOn.RowTemplate.Height = 21;
            this.dataGridViewOn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOn.Size = new System.Drawing.Size(703, 773);
            this.dataGridViewOn.TabIndex = 40;
            this.toolTip.SetToolTip(this.dataGridViewOn, "右クリックで項目コピー");
            // 
            // cms
            // 
            this.cms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu1,
            this.menu2});
            this.cms.Name = "cms";
            this.cms.Size = new System.Drawing.Size(156, 48);
            // 
            // menu1
            // 
            this.menu1.Name = "menu1";
            this.menu1.Size = new System.Drawing.Size(155, 22);
            this.menu1.Text = "項目を複製";
            this.menu1.Click += new System.EventHandler(this.menu1_Click);
            // 
            // menu2
            // 
            this.menu2.Name = "menu2";
            this.menu2.Size = new System.Drawing.Size(155, 22);
            this.menu2.Text = "複製項目を削除";
            this.menu2.Click += new System.EventHandler(this.menu2_Click);
            // 
            // dataGridViewOff
            // 
            this.dataGridViewOff.AllowUserToAddRows = false;
            this.dataGridViewOff.AllowUserToDeleteRows = false;
            this.dataGridViewOff.AllowUserToResizeRows = false;
            this.dataGridViewOff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewOff.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewOff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOff.Location = new System.Drawing.Point(398, 66);
            this.dataGridViewOff.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewOff.MultiSelect = false;
            this.dataGridViewOff.Name = "dataGridViewOff";
            this.dataGridViewOff.ReadOnly = true;
            this.dataGridViewOff.RowHeadersVisible = false;
            this.dataGridViewOff.RowTemplate.Height = 21;
            this.dataGridViewOff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOff.Size = new System.Drawing.Size(222, 735);
            this.dataGridViewOff.TabIndex = 37;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemove.Location = new System.Drawing.Point(628, 185);
            this.buttonRemove.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(55, 80);
            this.buttonRemove.TabIndex = 39;
            this.buttonRemove.Text = "←\r\n項目\r\n削除";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(628, 73);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(55, 80);
            this.buttonAdd.TabIndex = 38;
            this.buttonAdd.Text = "→\r\n項目\r\n追加";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 612);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "サンプルデータ";
            // 
            // radioButtonTextUTF8
            // 
            this.radioButtonTextUTF8.AutoSize = true;
            this.radioButtonTextUTF8.Location = new System.Drawing.Point(184, 22);
            this.radioButtonTextUTF8.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonTextUTF8.Name = "radioButtonTextUTF8";
            this.radioButtonTextUTF8.Size = new System.Drawing.Size(147, 20);
            this.radioButtonTextUTF8.TabIndex = 32;
            this.radioButtonTextUTF8.TabStop = true;
            this.radioButtonTextUTF8.Text = "テキストファイルUTF8";
            this.radioButtonTextUTF8.UseVisualStyleBackColor = true;
            // 
            // radioButtonTextSJIS
            // 
            this.radioButtonTextSJIS.AutoSize = true;
            this.radioButtonTextSJIS.Location = new System.Drawing.Point(184, 44);
            this.radioButtonTextSJIS.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonTextSJIS.Name = "radioButtonTextSJIS";
            this.radioButtonTextSJIS.Size = new System.Drawing.Size(160, 20);
            this.radioButtonTextSJIS.TabIndex = 33;
            this.radioButtonTextSJIS.TabStop = true;
            this.radioButtonTextSJIS.Text = "テキストファイルShift JIS";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(192, 77);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 16);
            this.label16.TabIndex = 48;
            this.label16.Text = "区切り";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxSep);
            this.groupBox1.Controls.Add(this.radioButtonUTF8);
            this.groupBox1.Controls.Add(this.radioButtonSJIS);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.radioButtonTextUTF8);
            this.groupBox1.Controls.Add(this.radioButtonTextSJIS);
            this.groupBox1.Controls.Add(this.textBoxCell);
            this.groupBox1.Controls.Add(this.radioButtonExcel);
            this.groupBox1.Controls.Add(this.labelCell);
            this.groupBox1.Controls.Add(this.checkBoxExcelBase);
            this.groupBox1.Location = new System.Drawing.Point(7, 286);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 159);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力形式";
            // 
            // comboBoxSep
            // 
            this.comboBoxSep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSep.FormattingEnabled = true;
            this.comboBoxSep.Items.AddRange(new object[] {
            "|",
            "=",
            ";",
            "TAB文字"});
            this.comboBoxSep.Location = new System.Drawing.Point(246, 74);
            this.comboBoxSep.Name = "comboBoxSep";
            this.comboBoxSep.Size = new System.Drawing.Size(98, 24);
            this.comboBoxSep.TabIndex = 50;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxID);
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBoxName);
            this.groupBox3.Controls.Add(this.checkBoxHeader);
            this.groupBox3.Controls.Add(this.textBoxFileName);
            this.groupBox3.Location = new System.Drawing.Point(12, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 648);
            this.groupBox3.TabIndex = 51;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ファイル設定";
            // 
            // textBoxFind
            // 
            this.textBoxFind.Location = new System.Drawing.Point(398, 33);
            this.textBoxFind.Name = "textBoxFind";
            this.textBoxFind.Size = new System.Drawing.Size(221, 22);
            this.textBoxFind.TabIndex = 53;
            this.toolTip.SetToolTip(this.textBoxFind, "検索項目を入力してEnter");
            this.textBoxFind.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxFind_KeyUp);
            // 
            // toolTip
            // 
            this.toolTip.AutomaticDelay = 10;
            this.toolTip.AutoPopDelay = 10000;
            this.toolTip.InitialDelay = 10;
            this.toolTip.IsBalloon = true;
            this.toolTip.ReshowDelay = 2;
            // 
            // ListExportSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1604, 844);
            this.Controls.Add(this.textBoxFind);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewOn);
            this.Controls.Add(this.dataGridViewOff);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonOK);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1344, 747);
            this.Name = "ListExportSettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "リスト出力設定";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelDate.ResumeLayout(false);
            this.panelDate.PerformLayout();
            this.panelExcel.ResumeLayout(false);
            this.panelExcel.PerformLayout();
            this.panelSeparater.ResumeLayout(false);
            this.panelSeparater.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOn)).EndInit();
            this.cms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOff)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.TextBox textBoxCell;
        private System.Windows.Forms.Label labelCell;
        private System.Windows.Forms.CheckBox checkBoxExcelBase;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.RadioButton radioButtonInsurerType;
        private System.Windows.Forms.RadioButton radioButtonInsurer;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxDateZero;
        private System.Windows.Forms.Panel panelDate;
        private System.Windows.Forms.RadioButton radioButtonJpNone;
        private System.Windows.Forms.RadioButton radioButtonJpShort;
        private System.Windows.Forms.RadioButton radioButtonJpKanji;
        private System.Windows.Forms.RadioButton radioButtonAd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panelExcel;
        private System.Windows.Forms.RadioButton radioButtonData;
        private System.Windows.Forms.RadioButton radioButtonMoji;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panelSeparater;
        private System.Windows.Forms.RadioButton radioButtonSepNone;
        private System.Windows.Forms.RadioButton radioButtonKanji;
        private System.Windows.Forms.RadioButton radioButtonSlash;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxOnName;
        private System.Windows.Forms.TextBox textBoxOnSample;
        private System.Windows.Forms.TextBox textBoxOnWord;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxOnHeader;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBoxHeader;
        private System.Windows.Forms.RadioButton radioButtonExcel;
        private System.Windows.Forms.RadioButton radioButtonSJIS;
        private System.Windows.Forms.RadioButton radioButtonUTF8;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewOn;
        private System.Windows.Forms.DataGridView dataGridViewOff;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxZenkaku;
        private System.Windows.Forms.RadioButton radioButtonTextUTF8;
        private System.Windows.Forms.RadioButton radioButtonTextSJIS;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBoxSep;
        private System.Windows.Forms.ContextMenuStrip cms;
        private System.Windows.Forms.ToolStripMenuItem menu1;
        private System.Windows.Forms.ToolStripMenuItem menu2;
        private System.Windows.Forms.TextBox textBoxFind;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.RadioButton radioButtonJPEra;
    }
}