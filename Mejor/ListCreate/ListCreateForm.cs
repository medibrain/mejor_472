﻿using Mejor.Inspect;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    public partial class ListCreateForm : Form
    {
        // 検索結果表示欄のヘッダの設定値用
        public class HeaderSetting
        {
            public string Name;
            public int DisplayIndex;
            public int Width;
            public bool Visible;
            public bool ReadOnly;
        }

        // 表示設定値（初期値）
        private readonly Dictionary<string, HeaderSetting> displayList = new Dictionary<string, HeaderSetting>
        {
            { nameof(AppColumns.Select), new HeaderSetting { Name = "", DisplayIndex = 0, Width = 25, Visible = true, ReadOnly = false }},
            { nameof(AppColumns.ViewIndex), new HeaderSetting { Name = "行", DisplayIndex = 1, Width = 25, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.JCYM), new HeaderSetting { Name = "処理月", DisplayIndex = 2, Width = 50, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.JYM), new HeaderSetting { Name = "診療月", DisplayIndex = 3, Width = 50, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.AID), new HeaderSetting { Name = "ID", DisplayIndex = 4, Width = 70, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.HihoNum), new HeaderSetting { Name = "被保番", DisplayIndex = 5, Width = 70, Visible = true, ReadOnly = true }},

            //20200418114721 furukawa st ////////////////////////
//施術師/施術所が入っているのでヘッダを修正。但しどっちかは決まってない
            { nameof(AppColumns.DrNum), new HeaderSetting { Name = "施術師/施術所番号", DisplayIndex = 6, Width = 80, Visible = true, ReadOnly = true }},
            //{ nameof(AppColumns.DrNum), new HeaderSetting { Name = "施術所No", DisplayIndex = 6, Width = 70, Visible = true, ReadOnly = true }},
//20200418114721 furukawa ed ////////////////////////

            { nameof(AppColumns.Total), new HeaderSetting { Name = "合計金額", DisplayIndex = 7, Width = 70, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.Days), new HeaderSetting { Name = "日", DisplayIndex = 8, Width = 25, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.AppType), new HeaderSetting { Name = "種類", DisplayIndex = 9, Width = 40, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.ShokaiHenreiStatus), new HeaderSetting { Name = "照会/返戻", DisplayIndex = 10, Width = 70, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.TenkenResult), new HeaderSetting { Name = "点検", DisplayIndex = 11, Width = 70, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.Memo), new HeaderSetting { Name = "メモ", DisplayIndex = 12, Width = 100, Visible = true, ReadOnly = true }},
            { nameof(AppColumns.ScanID), new HeaderSetting { Name = "スキャンID", DisplayIndex=13, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GroupID), new HeaderSetting { Name = "グループID", DisplayIndex=14, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.MediYear), new HeaderSetting { Name = "診療和暦年", DisplayIndex=15, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.MediMonth), new HeaderSetting { Name = "診療和暦月", DisplayIndex=16, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputInsNum), new HeaderSetting { Name = "保険者番号",  DisplayIndex=17, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HihoPref), new HeaderSetting { Name = "都道府県", DisplayIndex=18, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HihoType), new HeaderSetting { Name = "被保険者拡張情報", DisplayIndex=19, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HihoName), new HeaderSetting { Name = "被保険者名", DisplayIndex=20, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HihoZip), new HeaderSetting { Name = "被保険者郵便番号", DisplayIndex=21, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HihoAdd), new HeaderSetting { Name = "被保険者住所", DisplayIndex=22, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PersonName), new HeaderSetting { Name = "受療者名", DisplayIndex=23, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Sex), new HeaderSetting { Name = "受療者性別", DisplayIndex=24, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Birthday), new HeaderSetting { Name = "受療者生年月日", DisplayIndex=25, Width = 100, Visible = false, ReadOnly = true }},
            
            //20200418114504 furukawa st ////////////////////////
//施術所/登録記号番号等が入っているのでヘッダを修正。但しどっちかは決まってない

            { nameof(AppColumns.ClinicNum), new HeaderSetting { Name = "施術所/登録記号番号", DisplayIndex=26, Width = 110, Visible = false, ReadOnly = true }},
            //{ nameof(AppColumns.ClinicNum), new HeaderSetting { Name = "sid", DisplayIndex=26, Width = 100, Visible = false, ReadOnly = true }},
//20200418114504 furukawa ed ////////////////////////

            { nameof(AppColumns.Family), new HeaderSetting { Name = "家族区分", DisplayIndex=27, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Ratio), new HeaderSetting { Name = "給付割合", DisplayIndex=28, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PublcExpense), new HeaderSetting { Name = "公費番号", DisplayIndex=29, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AppChargeYear), new HeaderSetting { Name = "ChargeYear", DisplayIndex=30, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AppChargeMonth), new HeaderSetting { Name = "ChargeMonth", DisplayIndex=31, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoName1), new HeaderSetting { Name = "負傷1 負傷名", DisplayIndex=32, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDate1), new HeaderSetting { Name = "負傷1 負傷年月日", DisplayIndex=33, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFirstDate1), new HeaderSetting { Name = "負傷1 初検日", DisplayIndex=34, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoStartDate1), new HeaderSetting { Name = "負傷1 開始日", DisplayIndex=35, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFinishDate1), new HeaderSetting { Name = "負傷1 終了日", DisplayIndex=36, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDays1), new HeaderSetting { Name = "負傷1 日数", DisplayIndex=37, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCourse1), new HeaderSetting { Name = "負傷1 転帰", DisplayIndex=38, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoName2), new HeaderSetting { Name = "負傷2 負傷名", DisplayIndex=39, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDate2), new HeaderSetting { Name = "負傷2 負傷年月日", DisplayIndex=40, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFirstDate2), new HeaderSetting { Name = "負傷2 初検日", DisplayIndex=41, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoStartDate2), new HeaderSetting { Name = "負傷2 開始日", DisplayIndex=42, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFinishDate2), new HeaderSetting { Name = "負傷2 終了日", DisplayIndex=43, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDays2), new HeaderSetting { Name = "負傷2 日数", DisplayIndex=44, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCourse2), new HeaderSetting { Name = "負傷2 転帰", DisplayIndex=45, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoName3), new HeaderSetting { Name = "負傷3 負傷名", DisplayIndex=46, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDate3), new HeaderSetting { Name = "負傷3 負傷年月日", DisplayIndex=47, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFirstDate3), new HeaderSetting { Name = "負傷3 初検日", DisplayIndex=48, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoStartDate3), new HeaderSetting { Name = "負傷3 開始日", DisplayIndex=49, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFinishDate3), new HeaderSetting { Name = "負傷3 終了日", DisplayIndex=50, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDays3), new HeaderSetting { Name = "負傷3 日数", DisplayIndex=51, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCourse3), new HeaderSetting { Name = "負傷3 転帰", DisplayIndex=52, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoName4), new HeaderSetting { Name = "負傷4 負傷名", DisplayIndex=53, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDate4), new HeaderSetting { Name = "負傷4 負傷年月日", DisplayIndex=54, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFirstDate4), new HeaderSetting { Name = "負傷4 初検日", DisplayIndex=55, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoStartDate4), new HeaderSetting { Name = "負傷4 開始日", DisplayIndex=56, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFinishDate4), new HeaderSetting { Name = "負傷4 終了日", DisplayIndex=57, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDays4), new HeaderSetting { Name = "負傷4 日数", DisplayIndex=58, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCourse4), new HeaderSetting { Name = "負傷4 転帰", DisplayIndex=59, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoName5), new HeaderSetting { Name = "負傷5 負傷名", DisplayIndex=60, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDate5), new HeaderSetting { Name = "負傷5 負傷年月日", DisplayIndex=61, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFirstDate5), new HeaderSetting { Name = "負傷5 初検日", DisplayIndex=62, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoStartDate5), new HeaderSetting { Name = "負傷5 開始日", DisplayIndex=63, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoFinishDate5), new HeaderSetting { Name = "負傷5 終了日", DisplayIndex=64, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoDays5), new HeaderSetting { Name = "負傷5 日数", DisplayIndex=65, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCourse5), new HeaderSetting { Name = "負傷5 転帰", DisplayIndex=66, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.NewContType), new HeaderSetting { Name = "請求区分", DisplayIndex=67, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Distance), new HeaderSetting { Name = "往療距離", DisplayIndex=68, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.VisitTimes), new HeaderSetting { Name = "往療回数", DisplayIndex=69, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.VisitFee), new HeaderSetting { Name = "往療料", DisplayIndex=70, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.VisitAdd), new HeaderSetting { Name = "往療料加算", DisplayIndex=71, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ClinicZip), new HeaderSetting { Name = "施術所郵便番号", DisplayIndex=72, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ClinicAdd), new HeaderSetting { Name = "施術所住所", DisplayIndex=73, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ClinicName), new HeaderSetting { Name = "施術所名", DisplayIndex=74, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ClinicTel), new HeaderSetting { Name = "施術所電話番号", DisplayIndex=75, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DrName), new HeaderSetting { Name = "施術師名", DisplayIndex=76, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DrKana), new HeaderSetting { Name = "施術師カナ", DisplayIndex=77, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AccountType), new HeaderSetting { Name = "口座種別", DisplayIndex=78, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.BankName), new HeaderSetting { Name = "銀行名", DisplayIndex=79, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.BankType), new HeaderSetting { Name = "銀行種別", DisplayIndex=80, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.BankBranch), new HeaderSetting { Name = "支店名", DisplayIndex=81, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.BankBranchType), new HeaderSetting { Name = "支店種別", DisplayIndex=82, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AccountName), new HeaderSetting { Name = "口座名義", DisplayIndex=83, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AccountKana), new HeaderSetting { Name = "口座名義カナ", DisplayIndex=84, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AccountNumber), new HeaderSetting { Name = "口座番号", DisplayIndex=85, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Partial), new HeaderSetting { Name = "一部負担額", DisplayIndex=86, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Charge), new HeaderSetting { Name = "請求額", DisplayIndex=87, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Numbering), new HeaderSetting { Name = "ナンバリング", DisplayIndex=88, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Ufirst), new HeaderSetting { Name = "1回目入力ユーザーID", DisplayIndex=89, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Usecond), new HeaderSetting { Name = "2回目入力ユーザーID", DisplayIndex=90, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Uinquiry), new HeaderSetting { Name = "点検ユーザーID", DisplayIndex=91, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.UfirstEx), new HeaderSetting { Name = "1回目拡張入力ユーザーID", DisplayIndex=104, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.UsecondEx), new HeaderSetting { Name = "2回目拡張入力ユーザーID", DisplayIndex=105, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AdditionalUid1), new HeaderSetting { Name = "1回目追加入力ユーザーID", DisplayIndex=98, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AdditionalUid2), new HeaderSetting { Name = "2回目追加入力ユーザーID", DisplayIndex=99, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Bui), new HeaderSetting { Name = "あんま部位", DisplayIndex=92, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.CYM), new HeaderSetting { Name = "処理西暦年月", DisplayIndex=93, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.YM), new HeaderSetting { Name = "診療西暦年月", DisplayIndex=94, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.RrID), new HeaderSetting { Name = "関連レセ情報ID", DisplayIndex=97, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.MemoShokai), new HeaderSetting { Name = "照会メモ", DisplayIndex=100, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.MemoInspect), new HeaderSetting { Name = "点検メモ", DisplayIndex=101, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PayCode), new HeaderSetting { Name = "支払コード", DisplayIndex=102, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ShokaiCode), new HeaderSetting { Name = "照会文書通知コード", DisplayIndex=103, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.SaishinsaReasons), new HeaderSetting { Name = "再審理由", DisplayIndex=107, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HenreiReasons), new HeaderSetting { Name = "返戻理由", DisplayIndex=108, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.KagoReasons), new HeaderSetting { Name = "過誤理由", DisplayIndex=117, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ShokaiReasons), new HeaderSetting { Name = "照会理由", DisplayIndex=119, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ShokaiResult), new HeaderSetting { Name = "照会結果", DisplayIndex=120, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ComNum), new HeaderSetting { Name = "電算管理番号", DisplayIndex=109, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GroupNum), new HeaderSetting { Name = "グループ", DisplayIndex=110, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.OutMemo), new HeaderSetting { Name = "出力メモ", DisplayIndex=111, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.FushoCount), new HeaderSetting { Name = "負傷数", DisplayIndex=112, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputStatus), new HeaderSetting { Name = "入力状況", DisplayIndex=113, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputStatusEx), new HeaderSetting { Name = "拡張入力状況", DisplayIndex=114, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputStatusAdd), new HeaderSetting { Name = "追加入力状況", DisplayIndex=115, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.OryoResult), new HeaderSetting { Name = "往療点検結果", DisplayIndex=116, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InspectDescription), new HeaderSetting { Name = "点検詳細", DisplayIndex=121, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InspectInfo), new HeaderSetting { Name = "点検結果", DisplayIndex=123, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputOrderNumber), new HeaderSetting { Name = "ステータス", DisplayIndex=124, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.MediAge), new HeaderSetting { Name = "診療月年齢", DisplayIndex=125, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ChargeAge), new HeaderSetting { Name = "請求月年齢", DisplayIndex=126, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.TodayAge), new HeaderSetting { Name = "現在年齢", DisplayIndex=127, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DestName), new HeaderSetting { Name = "送付先氏名", DisplayIndex=128, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DestZip), new HeaderSetting { Name = "送付先〒", DisplayIndex=129, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DestAdd), new HeaderSetting { Name = "送付先住所", DisplayIndex=130, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ContactDt), new HeaderSetting { Name = "連絡日時", DisplayIndex=131, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ContactName), new HeaderSetting { Name = "連絡了承者名", DisplayIndex=132, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ContactStatus), new HeaderSetting { Name = "事前連絡不要/団体", DisplayIndex=133, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Dates), new HeaderSetting { Name = "診療日", DisplayIndex=134, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.KouhiNum), new HeaderSetting { Name = "公費番号", DisplayIndex=135, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.JukyuNum), new HeaderSetting { Name = "受給者番号", DisplayIndex=136, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.AppDistance), new HeaderSetting { Name = "申請距離", DisplayIndex=137, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DistCalcClinicAdd), new HeaderSetting { Name = "距離計算施術所住所", DisplayIndex=138, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DistCalcAdd), new HeaderSetting { Name = "距離計算被保険者住所", DisplayIndex=139, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.CalcDistance), new HeaderSetting { Name = "計算距離", DisplayIndex=140, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ShokaiFlag), new HeaderSetting { Name = "照会対象", DisplayIndex=141, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InspectFlag), new HeaderSetting { Name = "点検対象", DisplayIndex=142, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.OryoInspectFlag), new HeaderSetting { Name = "往療点検対象", DisplayIndex=143, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.HenreiFlag), new HeaderSetting { Name = "返戻対象", DisplayIndex=144, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PendFlag), new HeaderSetting { Name = "支払保留", DisplayIndex=145, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InputErrorFlag), new HeaderSetting { Name = "入力時エラー", DisplayIndex=146, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.TelFlag), new HeaderSetting { Name = "架電対象", DisplayIndex=147, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PaidFlag), new HeaderSetting { Name = "支払済", DisplayIndex=148, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Transaction1Flag), new HeaderSetting { Name = "処理1", DisplayIndex=149, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Transaction2Flag), new HeaderSetting { Name = "処理2", DisplayIndex=150, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Transaction3Flag), new HeaderSetting { Name = "処理3", DisplayIndex=151, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Transaction4Flag), new HeaderSetting { Name = "処理4", DisplayIndex=152, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Transaction5Flag), new HeaderSetting { Name = "処理5", DisplayIndex=153, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.ScanNote), new HeaderSetting { Name = "Note", DisplayIndex=154, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.BatchNo), new HeaderSetting { Name = "バッチ番号", DisplayIndex=155, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GakkoShokaiCodeCreate), new HeaderSetting { Name = "学校照会番号(生成)", DisplayIndex=156, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GakkoDantaiName), new HeaderSetting { Name = "学校共済団体名", DisplayIndex=157, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GakkoAccountNo), new HeaderSetting { Name = "学校口座番号", DisplayIndex=158, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GakkoHenreiCode), new HeaderSetting { Name = "学校返戻管理番号", DisplayIndex=159, Width = 200, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.RelationAID), new HeaderSetting { Name = "RelationAID", DisplayIndex=160, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.Kana), new HeaderSetting { Name = "Kana", DisplayIndex=161, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DestKana), new HeaderSetting { Name = "送付先カナ", DisplayIndex=162, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.InsName), new HeaderSetting { Name = "InsName", DisplayIndex=163, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.DouiDate), new HeaderSetting { Name = "同意日", DisplayIndex=164, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.PastSupplyYM), new HeaderSetting { Name = "前回支給年月", DisplayIndex=165, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSejutuDouiUmu), new HeaderSetting { Name = "施術同意書有無", DisplayIndex=166, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgKofuUmu), new HeaderSetting { Name = "交付料有無", DisplayIndex=167, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_SikakuNotMatch), new HeaderSetting { Name = "SikakuNotMatch", DisplayIndex=168, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_BirthNotMatch), new HeaderSetting { Name = "BirthNotMatch", DisplayIndex=169, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_GenderNotMatch), new HeaderSetting { Name = "GenderNotMatch", DisplayIndex=170, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_RatioNotMatch), new HeaderSetting { Name = "RatioNotMatch", DisplayIndex=171, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_RangeNotMatch), new HeaderSetting { Name = "RangeNotMatch", DisplayIndex=172, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.flgSikaku_SinryoBeforeIssue), new HeaderSetting { Name = "SinryoBeforeIssue", DisplayIndex=173, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.count), new HeaderSetting { Name = "count", DisplayIndex=174, Width = 100, Visible = false, ReadOnly = true }},
            { nameof(AppColumns.GeneralString1), new HeaderSetting { Name = "汎用文字列", DisplayIndex=175, Width = 100, Visible = false, ReadOnly = true }},
        };

        private BindingSource bs = new BindingSource();
        private int cym;
        private ShokaiReason shokaiReason = ShokaiReason.なし;
        private Inspect.FamilyImageForm familyImageForm = null;

        private readonly ListViewSetting settings = new ListViewSetting(Insurer.CurrrentInsurer);

        private App app => ((AppColumns)bs.Current)?.App;

        public ListCreateForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            initialize();
        }

        public ListCreateForm(App app)
        {
            InitializeComponent();
            this.cym = app.CYM;
            initialize();

            if (app.HihoNum.Length == 0)
            {
                MessageBox.Show("被保険者番号が確定していないデータです。検索は実行されません。");
            }
            else
            {
                MessageBox.Show("指定されたデータと同一の被保険者番号で検索します。");
                userControlSearch1.HihoNum = app.HihoNum;
                search();
            }
        }

        private void initialize()
        {

            userControlSearch1.SetCym(cym);

            返戻付箋印刷ToolStripMenuItem.Enabled =
                Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済;


            //20190530195855 furukawa st ////////////////////////
            //全項目出力機能の可視化

            btnAllFldExport.Visible =
                Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.SHARP_KENPO ||
                Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.HYOGO_KOIKI;
            //20190530195855 furukawa ed ////////////////////////

            bs.DataSource = new List<AppColumns>();
            dataGridView1.DataSource = bs;
            dataGridView1.AutoGenerateColumns = false;

            LoadDisplaySetting();

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            labelY.Text = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100).ToString() + " 年度　" +
                (cym % 100).ToString() + " 月処理分";
            //labelY.Text = "平成 " + DateTimeEx.GetHsYearFromAd(cym / 100).ToString() + " 年度　" +
            //  (cym % 100).ToString() + " 月処理分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            countMain();

            userControlOryo1.ResetControl();
            bs.CurrentChanged += Bs_CurrentChanged;
        }

        /// <summary>
        /// 検索ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            var searchList = new List<AppColumns>();
            shokaiReason = userControlSearch1.GetSearchCondition();

            var wf = new WaitFormSimple();
            try
            {
                wf.StartPosition = FormStartPosition.CenterScreen;
                System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
                while (!wf.Visible) System.Threading.Thread.Sleep(10);

                scrollPictureControlMain.Clear();
                searchList = userControlSearch1.GetApps().Select(item => new AppColumns(item)).ToList();

                int idx = 1;
                searchList.ForEach(a =>
                {
                    a.Select = true;
                    a.ViewIndex = idx++;
                });

                bs.DataSource = searchList;
                bs.ResetBindings(false);
                dataGridView1.ClearSelection();
                dataGridView1.DataSource = bs;

                //20200418114821 furukawa st ////////////////////////
                //ヘッダ文字列を長くしたので高さ調整
                
                dataGridView1.ColumnHeadersHeight = 40;
                //20200418114821 furukawa ed ////////////////////////

            }
            finally
            {
                wf.InvokeCloseDispose();
            }

            if (searchList.Count > 0)
            {
                dataGridView1.Rows[0].Selected = true;
            }


            if (searchList.Count == 0)
            {
                MessageBox.Show("検索条件に合致したデータはありませんでした。",
                    "検索結果情報", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            buttonAll.Text = "全解除";
            countSub();
        }


        //20190530195645 furukawa 全項目出力機能        
        private void btnAllFldExport_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("全項目出力します。宜しいですか？", Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            ListExportAllFields lstAll = new ListExportAllFields();
            ListExportAllFields.lstApp = userControlSearch1.GetApps();
            ListExportAllFields.cym = this.cym;
            ListExportAllFields.CreateData();

        }


        private bool pictureFill = true;
        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            if (familyImageForm != null && !familyImageForm.IsDisposed)
            {
                familyImageForm.Dispose();
            }

            inspectControl1.SetApp(app);
            if (app == null)
            {
                scrollPictureControlMain.Clear();
                scrollPictureControlSub.Clear();
                return;
            }

            //20220531170605 furukawa st ////////////////////////
            //往療計算の複数保険者対応
            
            switch (Insurer.CurrrentInsurer.EnumInsID) {
                case InsurerID.OSAKA_KOIKI:
                case InsurerID.IBARAKI_KOIKI:
                    userControlOryo1.SetApp(app);
                    break;
                default:
                    break;
            }


            //      if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.OSAKA_KOIKI)                          
            //              userControlOryo1.SetApp(app);
            //20220531170605 furukawa ed ////////////////////////

            countSub();

            try
            {
                var fn = app.GetImageFullPath();

                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //Main
                    scrollPictureControlMain.SetImage(img, fn);
                    if (pictureFill) scrollPictureControlMain.SetPictureBoxFill();

                    //Sub
                    scrollPictureControlSub.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            buttonUpdate.Enabled = false;
        }

        /// <summary>
        /// フォーム上部のカウンタ更新
        /// </summary>
        private void countMain()
        {
            int all, kago, gigi, oryogigi, syokai, ad, adOryo;
            App.InspectCount(cym, out all, out kago, out oryogigi, out gigi, out syokai, out ad, out adOryo);
            textBoxAll.Text = all.ToString();
            textBoxKago.Text = kago.ToString();
            textBoxGigi.Text = gigi.ToString();
            textBoxSyokai.Text = syokai.ToString();
        }

        /// <summary>
        /// 検索部のカウンタ更新
        /// </summary>
        private void countSub()
        {
            var l = (List<AppColumns>)bs.DataSource;
            textBoxS1.Text = l.Count.ToString();
            textBoxS2.Text = l.Count(x => x.App.StatusFlagCheck(StatusFlag.過誤)).ToString();
            textBoxS3.Text = l.Count(x => x.App.StatusFlagCheck(StatusFlag.再審査)).ToString();
            textBoxS4.Text = l.Count(x => x.App.StatusFlagCheck(StatusFlag.照会対象)).ToString();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCellAddress.Y == 0 ||
                dataGridView1.CurrentCellAddress.Y == -1) return;

            //20200422113937 furukawa st ////////////////////////
            //表示セルをカレントセルにすると落ちる
            
            //dataGridView1.CurrentCell = dataGridView1[0, dataGridView1.CurrentCell.RowIndex - 1];         
            dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex - 1].Selected = true;
            //20200422113937 furukawa ed ////////////////////////

        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCellAddress.Y == dataGridView1.RowCount - 1)
            {
                MessageBox.Show("現在選択されているデータは最終データです");
                return;
            }
            bs.MoveNext();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            scrollPictureControlMain.SetPictureBoxFill();
        }

        private void ListCreateForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                buttonNext.PerformClick();
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                buttonBack.PerformClick();
            }
            else if (e.KeyCode == Keys.F10)
            {
                buttonSearch.PerformClick();
            }
            else if (e.KeyCode == Keys.F12)
            {
                buttonUpdate.PerformClick();
            }
        }

        /// <summary>
        /// 印刷ボタン プレビュー表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrint_Click(object sender, EventArgs e)
        {
            var apps = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select).Select(r => r.App).ToList();
            if (apps.Count == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            using (var f = new PrintExForm(apps))
            {
                f.ShowDialog();
            }
        }

        /// <summary>
        /// CSV出力ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCSV_Click(object sender, EventArgs e)
        {
            var count = ((List<AppColumns>)bs.DataSource).Count(r => r.App.Select);
            if (count == 0)
            {
                MessageBox.Show("現在チェックされているデータがありません。");
                return;
            }

            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select).Select(r => r.App);
            using (var f = new ListExportForm(reces)) f.ShowDialog();
        }

        private void buttonSyokaiAdd_Click(object sender, EventArgs e)
        {
            var c = ((List<AppColumns>)bs.DataSource).Count(r => r.App.Select);
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            var sr = ShokaiReason.なし;
            using (var f = new Shokai.ShokaiReasonForm())
            {
                f.Flags = shokaiReason;
                if (f.ShowDialog() != DialogResult.OK) return;
                sr = f.Flags;
            }

            if (MessageBox.Show("現在チェックされている " + c.ToString() + " 件の申請書を\r\n" +
                "照会理由: " + sr.ToString() +
                "で照会対象とします。よろしいですか？",
                "照会確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK)
                return;

         

            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select);

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.StartPosition = FormStartPosition.CenterScreen;
                wf.ShowDialogOtherTask();

                while (!wf.Visible) System.Threading.Thread.Sleep(10);
                wf.SetMax(c);

                foreach (var item in reces)
                {
                    item.App.StatusFlagSet(StatusFlag.照会対象);
                    item.App.ShokaiReasonSet(sr);
                    item.App.UpdateINQ();
                    wf.InvokeValue++;
                }
            }

            countMain();
            bs.ResetBindings(false);
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            var l = (List<AppColumns>)bs.DataSource;
            if (buttonAll.Text == "全選択")
            {
                l.ForEach(r => r.App.Select = true);
                buttonAll.Text = "全解除";
            }
            else
            {
                l.ForEach(r => r.App.Select = false);
                buttonAll.Text = "全選択";
            }
            bs.ResetBindings(false);
        }

        private void dataGridViewKubun_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            e.PaintParts = e.PaintParts &= ~DataGridViewPaintParts.Focus;
        }

        private void プリンター設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new PrinterSelectForm(Settings.ShokaiPrinterName))
            {
                f.ShowDialog();
                Settings.ShokaiPrinterName = f.SelectPrinterName;
            }
        }

        private void buttonShokaiRemove_Click(object sender, EventArgs e)
        {
            shokaiFlagRemove();
        }

        private void shokaiFlagRemove()
        {
            var c = ((List<AppColumns>)bs.DataSource).Count(r => r.App.Select && r.App.StatusFlagCheck(StatusFlag.照会対象));
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (MessageBox.Show($"現在チェックされていて、かつ現在照会対象となっている " +
                $"{c.ToString()} 件の申請書を照会対象から除外します。\r\nよろしいですか？",
                "除外確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK) return;

            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select);

            using (var wf = new WaitForm())
            {
                wf.StartPosition = FormStartPosition.CenterScreen;
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();

                wf.SetMax(c);

                foreach (var item in reces)
                {
                    item.App.StatusFlagRemove(StatusFlag.照会対象);
                    item.App.Uinquiry = User.CurrentUser.UserID;
                    item.App.ShokaiReasonOverwrite(ShokaiReason.なし);
                    item.App.UpdateINQ();

                    wf.InvokeValue++;
                }
            }

            bs.ResetBindings(false);
            MessageBox.Show("対象の除外が完了しました");
            countMain();
            bs.ResetBindings(false);
        }

        /// <summary>
        /// 次のIDの画像を別ウインドウで表示します。
        /// IDが存在しない場合は表示できません。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNextIDImage_Click(object sender, EventArgs e)
        {
            if (app == null) return;
            using (var f = new NextImageForm(app)) f.ShowDialog();
        }

        private void userControlImageMain_LocationChanged(object sender, EventArgs e)
        {
            var pt = scrollPictureControlMain.Location;
            pt.Offset(2, 4);
            buttonImageFill.Location = pt;
            pt.Offset(78, 1);
        }

        private void inspectControl_DataChanged(object sender, EventArgs e)
        {
            buttonUpdate.Enabled = true;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (app == null) return;

            if (!inspectControl1.CanUpdate(app, true)) return;
            inspectControl1.DataSetToApp(app);

            //距離計算部分
            if (app.Distance > 0)
            {
                app.TaggedDatas.DistCalcAdd = userControlOryo1.PatientCalcAdd;
                app.TaggedDatas.DistCalcClinicAdd = userControlOryo1.Clinic_OsakaKoikiCalcAdd;
                app.TaggedDatas.AppDistance = userControlOryo1.AppDistance_OsakaKoiki;
                app.TaggedDatas.CalcDistance = userControlOryo1.CalcDistance_OsakaKoiki;
            }

            app.UpdateINQ();
            bs.MoveNext();
        }

        private void buttonTenkenAdd_Click(object sender, EventArgs e)
        {
            flagAdd(StatusFlag.点検対象);
        }

        private void buttonOther_Click(object sender, EventArgs e)
        {
            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select).Select(r => r.App).ToList();
            if (reces.Count == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            using (var f = new StatusSetForm(reces))
            {
                f.ShowDialog();
            }

            countMain();
            bs.ResetBindings(false);
        }

        private void flagAdd(StatusFlag flag)
        {
            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select);
            if (reces.Count == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (MessageBox.Show($"現在チェックされている {reces.Count} 件の申請書を" +
                $"{flag.ToString()}とします。よろしいですか？",
                "対象確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK)
                return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.StartPosition = FormStartPosition.CenterScreen;
                wf.ShowDialogOtherTask();
                wf.SetMax(reces.Count);

                foreach (var item in reces)
                {
                    item.App.StatusFlagSet(flag);
                    item.App.UpdateINQ();
                    wf.InvokeValue++;
                }
            }

            countMain();
            bs.ResetBindings(false);
        }

        private void buttonTenkenRemove_Click(object sender, EventArgs e)
        {
            tenkenFlagRemove();
        }

        private void tenkenFlagRemove()
        {
            var c = ((List<AppColumns>)bs.DataSource).Count(r => r.App.Select && r.App.StatusFlagCheck(StatusFlag.点検対象));
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (MessageBox.Show($"現在チェックされていて、かつ現在点検対象となっている " +
                $"{c.ToString()} 件の申請書を点検対象から除外します。また点検済みの場合、" +
                $"点検結果が失われます。\r\n本当によろしいですか？",
                "除外確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK) return;

            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.Select);

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.StartPosition = FormStartPosition.CenterScreen;
                wf.ShowDialogOtherTask();
                wf.SetMax(c);

                foreach (var item in reces)
                {
                    item.App.StatusFlagRemove(StatusFlag.点検対象 | StatusFlag.点検済 |
                        StatusFlag.保留 | StatusFlag.過誤 | StatusFlag.再審査);
                    item.App.Uinquiry = User.CurrentUser.UserID;
                    //item.AppNote.Note = string.Empty;
                    //item.AppNote.KagoReasonOverwrite(0);
                    //item.AppNote.GigiFushoOverwrite(0);
                    //item.AppNote.GigiReasonOverwrite(0);
                    item.App.UpdateINQ();

                    wf.InvokeValue++;
                }
            }

            bs.ResetBindings(false);
            MessageBox.Show("対象の除外が完了しました");
            countMain();
            bs.ResetBindings(false);
        }

        private void oryoFlagRemove()
        {
            var c = ((List<AppColumns>)bs.DataSource).Count(r => r.App.Select && r.App.StatusFlagCheck(StatusFlag.往療点検対象));
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (MessageBox.Show($"現在チェックされていて、かつ現在往療点検対象となっている " +
                $"{c.ToString()} 件の申請書を往療点検対象から除外します。また往療点検済みの場合、" +
                $"点検結果が失われます。\r\n本当によろしいですか？",
                "除外確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK) return;

            var reces = ((List<AppColumns>)bs.DataSource).FindAll(r => r.App.Select);

            using (var wf = new WaitForm())
            {
                wf.StartPosition = FormStartPosition.CenterScreen;
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.SetMax(c);

                foreach (var item in reces)
                {
                    item.App.StatusFlagRemove(StatusFlag.往療点検対象 |
                        StatusFlag.往療点検済 | StatusFlag.往療疑義);
                    item.App.Uinquiry = User.CurrentUser.UserID;
                    //item.AppNote.OryoNote = string.Empty;
                    item.App.UpdateINQ();

                    wf.InvokeValue++;
                }
            }

            bs.ResetBindings(false);
            MessageBox.Show("対象の除外が完了しました");
            countMain();
            bs.ResetBindings(false);
        }

        private void buttonFamilyRece_Click(object sender, EventArgs e)
        {
            if (app == null) return;
            if (familyImageForm != null && !familyImageForm.IsDisposed)
            {
                familyImageForm.Dispose();
            }

            familyImageForm = new Inspect.FamilyImageForm(app, this, false);
            familyImageForm.Show(this);
        }

        string sortCellName = string.Empty;
        bool sortASC = true;
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var source = dataGridView1.DataSource;
            if (source == null || !(source is BindingSource bs)) return;
            if (e.ColumnIndex < 0) return;

            var name = dataGridView1.Columns[e.ColumnIndex].Name;
            if (name == sortCellName) sortASC = !sortASC;
            else sortASC = true;
            sortCellName = name;

            if (!(bs.DataSource is List<AppColumns>)) return;
            var t = typeof(AppColumns);
            var ps = t.GetProperties();
            var p = Array.Find(ps, pp => pp.Name == name);
            if (p == null) return;

            ((List<AppColumns>)(bs.DataSource)).Sort((x, y) =>
            {
                var xp = p.GetValue(x, null);
                var yp = p.GetValue(y, null);

                //nullを最小に
                if (xp == null && yp == null) return 0;
                if (xp == null) return sortASC ? -1 : 1;
                if (yp == null) return sortASC ? 1 : -1;

                int compareRes = 0;
                if (p.PropertyType == typeof(int?)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(int)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(DateTime)) compareRes = ((DateTime)xp).CompareTo(((DateTime)yp));
                else if (p.PropertyType == typeof(string)) compareRes = ((string)xp).CompareTo(((string)yp));
                else compareRes = (xp.ToString()).CompareTo(yp.ToString());

                return sortASC ? compareRes : -compareRes;
            });

            bs.ResetBindings(false);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (app == null) return;

            InputStarter.Start(app.GroupID, INPUT_TYPE.First, app.Aid);

        }

        private void 返戻付箋印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Insurer.CurrrentInsurer.InsurerType != INSURER_TYPE.学校共済) return;

            var apps = (IEnumerable<AppColumns>)bs.DataSource;
            if (apps == null) return;
            apps = apps.Where(x => x.App.StatusFlagCheck(StatusFlag.返戻));
            if (apps.Count() == 0)
            {
                MessageBox.Show("選択されているデータに返戻対象はありません。");
                return;
            }

            var res = MessageBox.Show(
                $"選択されているデータのうち、返戻対象に指定されている{apps.Count()}件の" +
                $"返戻伝票を印刷します。よろしいですか？", "返戻付箋印刷確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res != DialogResult.OK) return;

            ZenkokuGakko.HenreiDoc.HenreiPrints(apps.Select(r => r.App).ToList());

        }

        private void 通常表示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureFill = true;
            splitterImage.Visible = false;
            scrollPictureControlSub.Visible = false;
            scrollPictureControlMain.SetPictureBoxFill();
        }

        const float imageRatio = 0.6f;
        private void 署名点検ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureFill = false;
            scrollPictureControlSub.Height = (panelImage.Height - 4) / 2;
            splitterImage.Visible = true;
            scrollPictureControlSub.Visible = true;
            scrollPictureControlMain.Ratio = imageRatio;
            scrollPictureControlSub.Ratio = imageRatio;
            scrollPictureControlMain.ScrollPosition = new Point(100, 450);
            scrollPictureControlSub.ScrollPosition = new Point(1400, 2500);
        }

        private void 負傷原因点検ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureFill = false;
            scrollPictureControlSub.Height = (panelImage.Height - 4) / 2;
            splitterImage.Visible = true;
            scrollPictureControlSub.Visible = true;
            scrollPictureControlMain.Ratio = imageRatio;
            scrollPictureControlSub.Ratio = imageRatio + 0.2f;
            scrollPictureControlMain.ScrollPosition = new Point(100, 700);
            scrollPictureControlSub.ScrollPosition = new Point(1200, 500);
        }

        private void 長期理由点検ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureFill = false;
            scrollPictureControlSub.Height = (panelImage.Height - 4) / 2;
            splitterImage.Visible = true;
            scrollPictureControlSub.Visible = true;
            scrollPictureControlMain.Ratio = imageRatio;
            scrollPictureControlSub.Ratio = imageRatio;
            scrollPictureControlMain.ScrollPosition = new Point(100, 700);
            scrollPictureControlSub.ScrollPosition = new Point(100, 1900);
        }

        private void buttonFamilyShokai_Click(object sender, EventArgs e)
        {
            if (app == null) return;

            using (var f = new Shokai.ShokaiInputForm(app))
            {
                f.ShowDialog();
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            using (var f = new ImportStatusForm())
            {
                f.ShowDialog();
            }
        }

        private void BtnDBDump_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("全データを出力します。宜しいですか？", Application.ProductName, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            ListExportAllFields.lstApp = userControlSearch1.GetApps();
            ListExportAllFields.cym = cym;
            ListExportAllFields.DumpAppData();

        }

        private void 被保番コピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (app == null)
            {
                return;
            }

            Clipboard.SetText(app.HihoNum);
        }

        // 「表示設定」ボタン
        private void ButtonViewSetting_Click(object sender, EventArgs e)
        {
            var form = new ListViewSettingForm(displayList);
            if (form.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var result = settings.Save(displayList);
            if (!result)
            {
                MessageBox.Show("表示設定の保存に失敗しました！（次回は初期状態で表示されます）");
            }

            var list = new Dictionary<int, DataGridViewColumn>();
            // 表示列設定(ヘッダは書き換え済みなのでNameでマッチング)
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (displayList.ContainsKey(column.Name))
                {
                    var setting = displayList[column.Name];
                    column.Width = setting.Width;
                    column.HeaderText = setting.Name;
                    column.ReadOnly = setting.ReadOnly;

                    if (setting.Visible)
                    {
                        column.Visible = setting.Visible;
                        list[setting.DisplayIndex] = column;
                        continue;
                    }
                }
                else
                {
                    column.ReadOnly = true;
                }


                // 表示対象でないものは1番最後に（これが無いとずれるので）
                column.DisplayIndex = dataGridView1.Columns.Count - 1;
                column.Visible = false;
            }

            // 最後に並び替え（途中でするとずれる）
            foreach (var key in list.Keys.OrderBy(key => key))
            {
                list[key].DisplayIndex = key;
            }
            dataGridView1.Refresh();
        }

        // 表示項目の設定読込
        private void LoadDisplaySetting()
        {
            var result = settings.Load(displayList);
            if (!result)
            {
                MessageBox.Show("表示設定の読み込みに失敗しました！（初期状態で表示されます）");
            }

            var list = new Dictionary<int, DataGridViewColumn>();
            // 表示列設定
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.FillWeight = 1; // 大量の列を表示する場合用
                if (displayList.ContainsKey(column.HeaderText))
                {
                    var setting = displayList[column.HeaderText];
                    column.Name = column.HeaderText;
                    column.Width = setting.Width;
                    column.HeaderText = setting.Name;
                    column.ReadOnly = setting.ReadOnly;

                    if (setting.Visible)
                    {
                        column.Visible = setting.Visible;
                        list[setting.DisplayIndex] = column;
                        continue;
                    }
                }
                else
                {
                    column.ReadOnly = true;
                }

                // 表示対象でないものは1番最後に（これが無いとずれるので）
                column.DisplayIndex = dataGridView1.Columns.Count - 1;
                column.Visible = false;
            }

            // 最後に並び替え（途中でするとずれる）
            foreach (var key in list.Keys.OrderBy(key => key))
            {
                list[key].DisplayIndex = key;
            }
        }

        private void buttonInput_Click(object sender, EventArgs e)
        {
            userControlSearch1.GetApps();
            using (var f = new Mejor.OtherInput.OtherInputListForm(cym, Insurer.CurrrentInsurer))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        //20201008165053 furukawa st ////////////////////////
        //contextMenuStripCopyに点検画面を追加
    
        private void 点検画面toolStripMenuItem_Click(object sender, EventArgs e)
        {
            InspectForm f = new InspectForm(app.Aid);
            f.Show();
        }
        //20201008165053 furukawa ed ////////////////////////


        //20210702142106 furukawa st ////////////////////////
        //印刷エラーになる場合、画像を直で開けるようにバイパス
        
        private void ファイル参照toolStripMenuItem_Click(object sender, EventArgs e)
        {
            string strFilePath=app.GetImageFullPath();
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            si.FileName = strFilePath;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = si;
            p.Start();
        }


        //20210702142106 furukawa ed ////////////////////////

        //20211022114039 furukawa st ////////////////////////
        //施術所番号コピー
        
        private void 施術所番号コピーtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (app == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(app.ClinicNum))
            {
                MessageBox.Show("施術所番号の情報を持っていません","",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return;
            }

            Clipboard.SetText(app.ClinicNum);
        }
        //20211022114039 furukawa ed ////////////////////////

        //20211022114710 furukawa st ////////////////////////
        //施術師番号コピー
        
        private void 施術師番号コピーtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (app == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(app.DrNum))
            {
                MessageBox.Show("施術師番号の情報を持っていません", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Clipboard.SetText(app.DrNum);
        }
        //20211022114710 furukawa ed ////////////////////////
    }
}