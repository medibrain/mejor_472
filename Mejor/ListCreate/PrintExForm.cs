﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Mejor
{
    public partial class PrintExForm : Form
    {
        private List<App> apps;

        public PrintExForm(List<App> apps)
        {
            InitializeComponent();
            this.apps = apps;

            //20210629142535 furukawa st ////////////////////////
            //フォーム色を環境に合わす
            
            CommonTool.setFormColor(this);
            //20210629142535 furukawa ed ////////////////////////


            string pName;
            using (var pd = new PrintDocument())
            {
                pName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                cmbPrinter.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == pName)
                    cmbPrinter.SelectedIndex = i;
            }

            if (cmbPrinter.Items.Contains(Settings.DefaultPrinterName))
            {
                cmbPrinter.SelectedItem = Settings.DefaultPrinterName;
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Print(false);
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            Print(true);
        }


        //20210629161609 furukawa st ////////////////////////
        //続紙印刷機能付き
        
        /// <summary>
        /// 印刷続紙機能付き
        /// </summary>
        /// <param name="isPreview"></param>
        private void Print(bool isPreview)
        {
            //印刷リスト
            var l = new List<KeyValuePair<App, bool>>();
            
            StringBuilder sb = new StringBuilder();

            foreach (var item in apps)
            {
                //20210723105224 furukawa st ////////////////////////
                //マルチTIFFは現状の印刷機能には対応していないので、リストからの直接印刷に誘導

                List<Image> lstImage = TiffUtility.GetFrameImages(item.GetImageFullPath());
                if (lstImage.Count > 1)
                {
                    MessageBox.Show($"この申請書 AID: {item.Aid} はマルチページです。\r\n" +
                        $"マルチページ印刷はマルチページで画像登録し、画像ファイルを分割している保険者しか印刷できません。\r\n\r\n" +
                        "この申請書を印刷するにはリストを右クリックし、「画像を直接開く」　その後その画面から印刷お願いします", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }
                //20210723105224 furukawa ed ////////////////////////


                if (checkBoxApp.Checked)
                {
                    l.Add(new KeyValuePair<App, bool>(item, false));
                }

                //20211115152734 furukawa st ////////////////////////
                //学校共済用返戻付箋もAppと同じ情報を取得
                
                if (checkBoxFusen.Checked)
                {                    
                    l.Add(new KeyValuePair<App, bool>(item, false));
                }
                //20211115152734 furukawa ed ////////////////////////


                if (checkBoxZokushi.Checked)
                {

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($" application_aux.parentaid={item.Aid} ");//続紙の申請書AIDで検索
                    List<Application_AUX> lstAUX = Application_AUX.Select(sb.ToString());
                    lstAUX.Sort((x, y) => x.aid.CompareTo(y.aid));

                    //続紙を印刷リストに追加
                    foreach (Application_AUX aux in lstAUX)
                    {
                        App a = App.GetApp(aux.aid);
                        l.Add(new KeyValuePair<App, bool>(a, false));
                    }
                }

                //続紙の種類
                if (checkBoxDoui.Checked || checkBoxDouiura.Checked || checkBoxChouki.Checked || checkBoxFukugo.Checked ||
                    checkBoxJotai.Checked || checkBoxOryo.Checked || checkBoxSejutu.Checked || checkBoxSoukatsu.Checked)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($" application_aux.parentaid={item.Aid} ");//続紙の申請書AIDで検索
                    List<Application_AUX> lstAUX = Application_AUX.Select(sb.ToString());
                    lstAUX.Sort((x, y) => x.aid.CompareTo(y.aid));

                    //続紙を印刷リストに追加
                    foreach (Application_AUX aux in lstAUX)
                    {
                        //続紙のAIDからapplicationデータを引っ張る
                        App appitem = App.GetApp(aux.aid);

                        if (checkBoxDoui.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.同意書)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxDouiura.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.同意書裏)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxChouki.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.長期)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxFukugo.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.複合)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxJotai.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.状態記入書)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxOryo.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.往療内訳)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxSejutu.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.施術報告書)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }
                        if (checkBoxSoukatsu.Checked)
                        {
                            if (appitem.AppType == APP_TYPE.総括票)
                            {
                                App a = App.GetApp(aux.aid);
                                l.Add(new KeyValuePair<App, bool>(a, false));
                            }
                        }





                }
                }



                if (checkBoxRef.Checked)
                {
                    if (item.TaggedDatas.RelationAID != 0)
                    {
                        var subApp = App.GetApp(item.TaggedDatas.RelationAID);
                        if (subApp == null) continue;
                        l.Add(new KeyValuePair<App, bool>(subApp, true));
                    }
                }
            }

            if (l.Count == 0)
            {
                MessageBox.Show("印刷するデータがありません。対象を正しく選択してください。", "データなし",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var pn = cmbPrinter.SelectedItem.ToString();
            if (pn == string.Empty) pn = Settings.DefaultPrinterName;
            var ip = new ImagePrint(pn);



            //20211115152837 furukawa st ////////////////////////
            //学校共済用返戻付箋はhenreiDoc.rprなのでプレビューができない。なのでmicrosoft print to pdfで一旦PDFにしてしまう
            
            if (checkBoxFusen.Checked)
            {
                //学校共済 返戻付箋印刷用関数への変換
                List<App> lstapp = new List<App>();
                foreach(KeyValuePair<App,bool> kv in l)
                {
                    lstapp.Add(kv.Key);
                }

                System.Drawing.Printing.PrintDocument pd = new PrintDocument();
                string dpname = pd.PrinterSettings.PrinterName;//現在のデフォルトプリンタを控える
                string pdfprinter = "Microsoft Print to PDF";//デフォルトで入っているPDFプリンタと想定する
                Type t = Type.GetTypeFromProgID("Wscript.Network");
                object wshNetwork = Activator.CreateInstance(t);

                try
                {
                    //プレビューはmicrosoft print to pdfでやらせる
                    if (isPreview)
                    {
                        //デフォルトプリンタをpdfprinterに置き換え
                        t.InvokeMember("SetDefaultPrinter", System.Reflection.BindingFlags.InvokeMethod, null, wshNetwork,
                            new object[] { $"{pdfprinter}" });                    
                                                
                        ZenkokuGakko.HenreiDoc.HenreiPrints(lstapp);
                    }
                    else                        
                    {
                        //デフォルトプリンタを選択したプリンタに置き換え
                        t.InvokeMember("SetDefaultPrinter", System.Reflection.BindingFlags.InvokeMethod, null, wshNetwork,
                            new object[] { $"{cmbPrinter.Text.Trim()}" });

                    
                        ZenkokuGakko.HenreiDoc.HenreiPrints(lstapp);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{pdfprinter}がインストールされていません。印刷のみ可能です。", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                finally
                {
                    //控えたデフォルトプリンタに戻す
                    t.InvokeMember("SetDefaultPrinter", System.Reflection.BindingFlags.InvokeMethod, null, wshNetwork, new object[] { $"{dpname}" });
                }

            }
            //他の印刷物
            else
            {
                ip.PrintWithRelations(l, isPreview, false,
                checkBoxSukashi.Checked, checkBoxYm.Checked, checkBoxTotal.Checked);
                if (!isPreview) this.Close();
            }



            //      ip.PrintWithRelations(l, isPreview, false,
            //          checkBoxSukashi.Checked, checkBoxYm.Checked, checkBoxTotal.Checked);
            //      if (!isPreview) this.Close();





            //20211115152837 furukawa ed ////////////////////////
        }




        #region old
        //private void Print(bool isPreview)
        //{
        //    //印刷リスト
        //    var l = new List<KeyValuePair<App, bool>>();

        //    foreach (var item in apps)
        //    {
        //        if (checkBoxApp.Checked)
        //        {
        //            l.Add(new KeyValuePair<App, bool>(item, false));
        //        }


        //        if (checkBoxRef.Checked)
        //        {
        //            if (item.TaggedDatas.RelationAID != 0)
        //            {
        //                var subApp = App.GetApp(item.TaggedDatas.RelationAID);
        //                if (subApp == null) continue;
        //                l.Add(new KeyValuePair<App, bool>(subApp, true));
        //            }
        //        }
        //    }

        //    if (l.Count == 0)
        //    {
        //        MessageBox.Show("印刷するデータがありません。対象を正しく選択してください。", "データなし",
        //            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //        return;
        //    }

        //    var pn = cmbPrinter.SelectedItem.ToString();
        //    if (pn == string.Empty) pn = Settings.DefaultPrinterName;
        //    var ip = new ImagePrint(pn);

        //    ip.PrintWithRelations(l, isPreview, false,
        //        checkBoxSukashi.Checked, checkBoxYm.Checked, checkBoxTotal.Checked);
        //    if (!isPreview) this.Close();
        //}
        #endregion

        //20210629161609 furukawa ed ////////////////////////

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBoxRef_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSukashi.Enabled = checkBoxRef.Checked;
            checkBoxYm.Enabled = checkBoxRef.Checked;
            checkBoxTotal.Enabled = checkBoxRef.Checked;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return;

            var fc = new TiffUtility.FastCopy();
            var dir = f.Name;
            System.IO.Directory.CreateDirectory(dir);

            foreach (var item in apps)
            {
                var c = item.GetImageFullPath();
                var fn = dir + "\\" + System.IO.Path.GetFileName(c);

                fc.FileCopy(c, fn);
            }

            MessageBox.Show("画像の一括保存が終わりました");
            this.Close();
        }



        //20210629151240 furukawa st ////////////////////////
        //続紙全部の場合は個々のチェックは無効
        
        private void checkBoxZokushi_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxDoui.Enabled = !checkBoxZokushi.Checked;
            checkBoxDouiura.Enabled = !checkBoxZokushi.Checked;
            checkBoxOryo.Enabled = !checkBoxZokushi.Checked;
            checkBoxFukugo.Enabled = !checkBoxZokushi.Checked;
            checkBoxJotai.Enabled = !checkBoxZokushi.Checked;
            checkBoxSoukatsu.Enabled = !checkBoxZokushi.Checked;
            checkBoxSejutu.Enabled = !checkBoxZokushi.Checked;
            checkBoxChouki.Enabled = !checkBoxZokushi.Checked;
            
        }

        //20210629151240 furukawa ed ////////////////////////


        //20211115154350 furukawa st ////////////////////////
        //他のチェックボックスは解除

        private void checkBoxFusen_CheckedChanged(object sender, EventArgs e)
        {

            checkBoxApp.Checked = false;
            checkBoxRef.Checked = false;
            checkBoxZokushi.Checked = false;
            checkBoxDoui.Checked = false;
            checkBoxDouiura.Checked = false;
            checkBoxOryo.Checked = false;
            checkBoxFukugo.Checked = false;
            checkBoxJotai.Checked = false;
            checkBoxSoukatsu.Checked = false;
            checkBoxSejutu.Checked = false;
            checkBoxChouki.Checked = false;
            //cmbPrinter.Enabled = !checkBoxFusen.Checked;
        }

        //20211115154350 furukawa ed ////////////////////////
        


    }
}
