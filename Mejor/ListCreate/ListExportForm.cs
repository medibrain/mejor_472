﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    public partial class ListExportForm : Form
    {
        BindingSource bsSetting = new BindingSource();
        public static DB db = new DB("jyusei");

        IEnumerable<App> apps;
        public ListExportForm(IEnumerable<App> apps)
        {
            InitializeComponent();
            this.apps = apps;


            //20200731163847 furukawa st ////////////////////////
            //保険者専用リスト作成ボタン表示保険者
            
            switch ((InsurerID)Insurer.CurrrentInsurer.InsurerID) { 
                case InsurerID.MIYAGI_KOKUHO:
                case InsurerID.NAGOYASHI_KOKUHO:
                    btnHout.Visible = true;
                break;

                default:
                    btnHout.Visible = false;
                    break;
            
            }
            //20200731163847 furukawa ed ////////////////////////




            //連番付与
            int vi = 1;
            foreach (var item in apps) item.ViewIndex = vi++;

            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;

            dataGridView1.DataSource = bsSetting;
            bsSetting.CurrentChanged += BsSetting_CurrentChanged;
            getList();

            foreach (DataGridViewColumn c in dataGridView1.Columns)
            {
                if (c.Name == nameof(ListExportSetting.Name)) continue;
                if (c.Name == nameof(ListExportSetting.TargetName)) continue;
                if (c.Name == nameof(ListExportSetting.ExportTypeDescription)) continue;
                c.Visible = false;
            }

            dataGridView1.Columns[nameof(ListExportSetting.Name)].Width = 180;
            dataGridView1.Columns[nameof(ListExportSetting.Name)].DisplayIndex = 0;
            dataGridView1.Columns[nameof(ListExportSetting.Name)].HeaderText = "設定名";
            dataGridView1.Columns[nameof(ListExportSetting.TargetName)].Width = 180;
            dataGridView1.Columns[nameof(ListExportSetting.TargetName)].DisplayIndex = 1;
            dataGridView1.Columns[nameof(ListExportSetting.TargetName)].HeaderText = "対象保険者";
            dataGridView1.Columns[nameof(ListExportSetting.ExportTypeDescription)].HeaderText = "ファイル形式";
            dataGridView1.Columns[nameof(ListExportSetting.ExportTypeDescription)].DisplayIndex = 2;
            
            //20210501112444 furukawa st ////////////////////////
            //画面フォントサイズ変更により、画面サイズ変更            
            dataGridView1.Columns[nameof(ListExportSetting.ExportTypeDescription)].Width = 120;
            //20210501112444 furukawa ed ////////////////////////


            Shown += (s, e) => buttonOK.Focus();
        }

        private void BsSetting_CurrentChanged(object sender, EventArgs e)
        {
            select();
        }

        private void select()
        {
            var s = (ListExportSetting)bsSetting.Current;
            buttonOK.Enabled = s != null;
            buttonEdit.Enabled = s.ID != 0;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            setting();
        }

        private void setting()
        {
            var s = (ListExportSetting)bsSetting.Current;
            if (s == null) return;
            using (var f = new ListExportSettingForm(db, s, apps))
            {
                f.ShowDialog();
                getList();
            }
        }

        private void getList()
        {
            var l = new List<ListExportSetting>();
            var id = Insurer.CurrrentInsurer.InsurerID;
            l.Add(new ListExportSetting() { Name = "従来CSV出力" });
            l.AddRange(ListExportSetting.GetAllSetting(db, id));
            bsSetting.DataSource = l;
            bsSetting.ResetBindings(false);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            var s = (ListExportSetting)bsSetting.Current;
            if (s == null) return;

            //旧型式出力
            if (s.ID == 0)
            {
                exportOldCsv(apps);
                return;
            }

            var fn = string.Empty;

            //20210501114042 furukawa st ////////////////////////
            //CSVでない、その他テキストファイルの追加
            
            var ex = (s.ExportType == "UTF8" || s.ExportType == "SJIS") ? ".csv" :
                (s.ExportType.Contains("TEXTUTF8") || s.ExportType.Contains("TEXTSJIS")) ? ".txt" :
                ".xlsx";

            //var ex = (s.ExportType == "UTF8" || s.ExportType == "SJIS") ? ".csv" : ".xlsx";
            //20210501114042 furukawa ed ////////////////////////



            using (var f = new SaveFileDialog())
            {
                f.FileName = s.FileName.Replace("{cym}", apps.First().CYM.ToString()).
                    Replace("{ins}", Insurer.CurrrentInsurer.InsurerName) + ex;
                if (f.ShowDialog() != DialogResult.OK) return;
                fn = f.FileName;
            }
            
            if (!ListExport.Export(apps, fn, s))
            {
                MessageBox.Show("出力に失敗しました");
                return;
            }
            MessageBox.Show("出力が完了しました");
            this.Close();
        }

        private void exportOldCsv(IEnumerable<App> apps)
        {
            int cym = apps.First().CYM;
            int count = apps.Count();
            if (MessageBox.Show("現在チェックされている" + count.ToString() +
                "件のデータをCSVファイルに出力します。\r\nよろしいですか？",
                "CSV出力確認",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question)
                != System.Windows.Forms.DialogResult.OK) return;

            var reces = apps.ToList();

            var fileName = "";
            using (var f = new SaveFileDialog())
            {
                f.FileName = "ExportList.csv";
                f.Filter = "CSVファイル(*.csv)|*.csv|すべてのファイル(*.*)|*.*";
                f.FilterIndex = 1;
                f.RestoreDirectory = true;
                f.Title = "保存先ファイルを指定してください";

                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }

            var ins = Insurer.CurrrentInsurer;
            int insurerID = ins.InsurerID;

            if (ins.InsurerType == INSURER_TYPE.学校共済)
            {
                //if (ins.InsurerID == (int)InsurerID.OSAKA_GAKKO) OsakaGakko.Export.ListExport(reces, fileName, cym);
                //else ZenkokuGakko.ExportRev.ListExport(reces, fileName, cym);
                ZenkokuGakko.ExportRev.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.MIYAZAKI_KOIKI)
            {
                MiyazakiKoiki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.HIROSHIMA_KOIKI)
            {
                HiroshimaKoiki.ExportOld.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.NARA_KOIKI)
            {
                NaraKoiki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.SHIZUOKA_KOIKI)
            {
                ShizuokaKoiki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.TOKUSHIMA_KOIKI)
            {
                TokushimaKoiki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.OSAKA_KOIKI)
            {
                OsakaKoiki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.CHIKIIRYO)
            {
                ChikiIryo.Export.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.AICHI_TOSHI)
            {
                AichiToshi.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.MUSASHINO_KOKUHO)
            {
                Musashino.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.MITSUBISHI_SEISHI)
            {
                MitsubishiSeishi.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.OTARU_KOKUHO)
            {
                OtaruKokuho.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.IRUMA_KOKUHO)
            {
                IrumaKokuho.ListData.Export(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.YAMAZAKI_MAZAK)
            {
                YamazakiMazak.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.HIRAKATA_KOKUHO)
            {
                HirakataKokuho.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.AMAGASAKI_KOKUHO)
            {
                Amagasaki.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.NAGOYA_MOKUZAI)
            {
                NagoyaMokuzai.Export.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO)
            {
                HigashimurayamaKokuho.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.FUTABA)
            {
                Futaba.Export.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.KAGOME)
            {
                Kagome.Export.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.YACHIYO_KOKUHO)
            {
                YachiyoKokuho.ListExport.Export(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.KYOTO_KOIKI)
            {
                KyotoKoiki.ListExport.Export(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.NARITA_KOKUHO)
            {
                NaritaKokuho.Export.ListExport(reces, fileName);
                return;
            }
            else if (insurerID == (int)InsurerID.CHUO_RADIO)
            {
                ChuoRadio.Export.ListExport(reces, fileName, cym);
                return;
            }
            else if (insurerID == (int)InsurerID.ASAKURA_KOKUHO)
            {
                AsakuraKokuho.Export.ListExport(reces, fileName);
                return;
            }

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    var rex = new string[20];
                    //先頭行は見出し
                    rex[0] = "ID";
                    rex[1] = "処理年";
                    rex[2] = "処理月";
                    rex[3] = "診療年";
                    rex[4] = "診療月";
                    rex[5] = "保険者番号";
                    rex[6] = "被保険者番号";
                    rex[7] = "住所";
                    rex[8] = "氏名";
                    rex[9] = "性別";
                    rex[10] = "生年月日";
                    rex[11] = "請求区分";
                    rex[12] = "往療料";
                    rex[13] = "施術所記号";
                    rex[14] = "合計金額";
                    rex[15] = "請求金額";
                    rex[16] = "診療日数";
                    rex[17] = "ナンバリング";
                    rex[18] = "照会理由";
                    rex[19] = "点検結果";

                    sw.WriteLine(string.Join(",", rex));

                    foreach (var item in reces)
                    {
                        rex[0] = item.Aid.ToString();
                        rex[1] = item.ChargeYear.ToString();
                        rex[2] = item.ChargeMonth.ToString();
                        rex[3] = item.MediYear.ToString();
                        rex[4] = item.MediMonth.ToString();
                        rex[5] = item.InsNum;
                        rex[6] = item.HihoNum;
                        rex[7] = item.HihoAdd;
                        rex[8] = item.PersonName;
                        rex[9] = item.Sex == 1 ? "男" : "女";
                        rex[10] = item.Birthday.ToShortDateString();
                        rex[11] = item.NewContType == NEW_CONT.新規 ? "新規" : "継続";
                        rex[12] = item.Distance == 999 ? "あり" : "";
                        rex[13] = item.DrNum;
                        rex[14] = item.Total.ToString();
                        rex[15] = item.Charge.ToString();
                        rex[16] = item.CountedDays.ToString();
                        rex[17] = item.Numbering;
                        rex[18] = item.ShokaiReasonStr;
                        rex[19] = item.InspectInfo;

                        sw.WriteLine(string.Join(",", rex));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CSV出力に失敗しました。\r\n" + ex.Message);
                Log.ErrorWrite(ex);
                return;
            }

            MessageBox.Show("CSV出力が完了しました。");
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            using (var f = new ListExportSettingForm(db, null, apps))
            {
                f.ShowDialog();
                getList();
            }

        }

        private void 項目設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setting();
        }

        private void 削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var s = (ListExportSetting)bsSetting.Current;
            if (s == null) return;

            var res = MessageBox.Show("現在選択中のリスト出力設定を削除します。もとには戻せませんが、本当によろしいですか？",
                "削除確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;
            if(!s.Delete(db))
            {
                MessageBox.Show("削除に失敗しました");
                return;
            }

            getList();
        }

        private void 設定コピーして新規ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var s = (ListExportSetting)bsSetting.Current;
            if (s == null) return;

            var cs = s.CopyNew();
            using (var f = new ListExportSettingForm(db, cs, apps))
            {
                f.ShowDialog();
                getList();
            }
        }



        //20200423121405 furukawa 
        /// <summary>
        /// 保険者別出力機能
        /// リスト作成画面でappから検索し、その検索結果を各保険者フォーマットに送る
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHout_Click(object sender, EventArgs e)
        {
            List<App> app= this.apps.ToList();
            
            switch (Insurer.CurrrentInsurer.InsurerID)
            {
                case (int)InsurerID.MIYAGI_KOKUHO:

                    //20200730171801 furukawa st ////////////////////////
                    //当初CYMを使用して月ごとにエクセルブックを分けていたが、月マタギで一気に出すように仕様変更要望
                    
                    MiyagiKokuho.Export.ExportData(app);
                    //MiyagiKokuho.Export.ExportData(app[0].CYM, app);

                    //20200730171801 furukawa ed ////////////////////////

                    break;

                //20200526155518 furukawa st ////////////////////////
                //名古屋市国保はリスト出力の保険者個別から出力
                
                case (int)InsurerID.NAGOYASHI_KOKUHO:
                    NagoyashiKokuho.Export.ExportData(app[0].CYM, app);
                    break;
                //20200526155518 furukawa ed ////////////////////////


                default:
                    break;
            }
        }
    }
}
