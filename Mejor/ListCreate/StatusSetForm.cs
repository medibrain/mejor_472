﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    public partial class StatusSetForm : Form
    {
        List<App> apps;

        public StatusSetForm(List<App> l)
        {
            apps = l;
            InitializeComponent();
        }

        private void radioButtonHenrei_CheckedChanged(object sender, EventArgs e)
        {
            panelHenrei.Enabled = 
                (radioButtonHenrei.Checked || radioButtonTel.Checked) && radioButtonAdd.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var f = radioButtonHenrei.Checked ? StatusFlag.返戻 :
                radioButtonOuryo.Checked ? StatusFlag.往療点検対象 :
                radioButtonHoryu.Checked ? StatusFlag.支払保留 :
                radioButtonTel.Checked ? StatusFlag.架電対象 :
                radioButtonShiharai.Checked ? StatusFlag.支払済 :
                radioButtonPro1.Checked ? StatusFlag.処理1 :
                radioButtonPro2.Checked ? StatusFlag.処理2 :
                radioButtonPro3.Checked ? StatusFlag.処理3 :
                radioButtonPro4.Checked ? StatusFlag.処理4 :
                radioButtonPro5.Checked ? StatusFlag.処理5 :
                StatusFlag.未処理;

            if (f == StatusFlag.未処理)
            {
                MessageBox.Show("対象種類を選んでください");
                return;
            }

            if(!radioButtonAdd.Checked && !radioButtonRemove.Checked)
            {
                MessageBox.Show("管理を選んでください");
                return;
            }

            if (radioButtonAdd.Checked) add(f);
            else remove(f);
        }

        private void add(StatusFlag f)
        {
            HenreiReasons rs = 0;
            string sub = string.Empty;
            if(f == StatusFlag.返戻 || f == StatusFlag.架電対象)
            {
                if (checkBoxHenBui.Checked) { rs |= HenreiReasons.負傷部位; sub += "負傷部位 "; }
                if (checkBoxHenReason.Checked) { rs |= HenreiReasons.負傷原因; sub += "負傷原因 "; }
                if (checkBoxHenJiki.Checked) { rs |= HenreiReasons.負傷時期; sub += "負傷時期 "; }
                if (checkBoxHenKago.Checked) { rs |= HenreiReasons.過誤; sub += "過誤 "; }
                if (checkBoxHenKega.Checked) { rs |= HenreiReasons.けが外; sub += "けが以外 "; }
                if (checkBoxHenDays.Checked) { rs |= HenreiReasons.受療日数; sub += "受療日数 "; }
                if (checkBoxHenWork.Checked) { rs |= HenreiReasons.勤務中; sub += "勤務中 "; }
                if (checkBoxHenOther.Checked) { rs |= HenreiReasons.その他; sub += "その他 "; }
            }

            if (MessageBox.Show($"現在チェックされている {apps.Count} 件の申請書を" +
                ((f == StatusFlag.返戻 || f == StatusFlag.架電対象) ? 
                    rs == 0 ? "理由なしで" : "理由: [ " + sub + "]で" : string.Empty) +
                $"{f}に指定します。本当によろしいですか？",
                "対象確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK)
                return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.SetMax(apps.Count);

                foreach (var item in apps)
                {
                    item.StatusFlagSet(f);
                    item.HenreiReasonsSet(rs);
                    item.UpdateINQ();
                    wf.InvokeValue++;
                }
            }
            this.Close();
        }

        private void remove(StatusFlag f)
        {
            var sub = f == StatusFlag.往療点検対象 ? "また往療点検済みの場合、点検結果が失われます。" :
                f == StatusFlag.返戻 ? "また返戻理由が失われます。" :
                f == StatusFlag.架電対象 ? "また架電理由が失われます。" :
                string.Empty;

            var l = apps.FindAll(x => x.StatusFlagCheck(f));

            if (MessageBox.Show($"現在チェックされていて、かつ{f}となっている " +
                $"{l.Count} 件の申請書を{f}から除外します。" +
                $"{sub}" +
                $"\r\n本当によろしいですか？",
                "除外確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK) return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.SetMax(l.Count);

                foreach (var item in l)
                {
                    if (f == StatusFlag.往療点検対象)
                    {
                        item.StatusFlagRemove(StatusFlag.往療点検対象 | StatusFlag.往療点検済 | StatusFlag.往療疑義);
                        item.Uinquiry = User.CurrentUser.UserID;
                        //item.AppNote.OryoNote = string.Empty;
                    }
                    else if (f == StatusFlag.支払保留)
                    {
                        item.StatusFlagRemove(StatusFlag.支払保留);
                    }
                    else if (f == StatusFlag.返戻)
                    {
                        item.StatusFlagRemove(StatusFlag.返戻);
                        if (!item.StatusFlagCheck(StatusFlag.架電対象))
                        {
                            item.HenreiReasons = HenreiReasons.なし;
                        }
                    }
                    else if (f == StatusFlag.架電対象)
                    {
                        item.StatusFlagRemove(StatusFlag.架電対象);
                        if (!item.StatusFlagCheck(StatusFlag.返戻))
                        {
                            item.HenreiReasons = HenreiReasons.なし;
                        }
                    }
                    else if (f == StatusFlag.支払済)
                    {
                        item.StatusFlagRemove(StatusFlag.支払済);
                    }
                    else if (f == StatusFlag.処理1)
                    {
                        item.StatusFlagRemove(StatusFlag.処理1);
                    }
                    else if (f == StatusFlag.処理2)
                    {
                        item.StatusFlagRemove(StatusFlag.処理2);
                    }
                    else if (f == StatusFlag.処理3)
                    {
                        item.StatusFlagRemove(StatusFlag.処理3);
                    }
                    else if (f == StatusFlag.処理4)
                    {
                        item.StatusFlagRemove(StatusFlag.処理4);
                    }
                    else if (f == StatusFlag.処理5)
                    {
                        item.StatusFlagRemove(StatusFlag.処理5);
                    }
                    item.UpdateINQ();
                    wf.InvokeValue++;
                }
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
