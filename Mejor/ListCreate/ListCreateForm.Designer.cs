﻿namespace Mejor.ListCreate
{
    partial class ListCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCopy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.被保番コピーToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.点検画面toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ファイル参照toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.groupBoxInfo = new System.Windows.Forms.GroupBox();
            this.textBoxSyokai = new System.Windows.Forms.TextBox();
            this.labelY = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxKago = new System.Windows.Forms.TextBox();
            this.textBoxGigi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAll = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.splitContainerLeft = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.contextMenuStripPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.プリンター設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.返戻付箋印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonCSV = new System.Windows.Forms.Button();
            this.buttonViewSetting = new System.Windows.Forms.Button();
            this.btnDBDump = new System.Windows.Forms.Button();
            this.btnAllFldExport = new System.Windows.Forms.Button();
            this.buttonOther = new System.Windows.Forms.Button();
            this.buttonTenkenRemove = new System.Windows.Forms.Button();
            this.buttonTenkenAdd = new System.Windows.Forms.Button();
            this.buttonShokaiRemove = new System.Windows.Forms.Button();
            this.buttonShokaiAdd = new System.Windows.Forms.Button();
            this.textBoxS4 = new System.Windows.Forms.TextBox();
            this.labelSearchSyokai = new System.Windows.Forms.Label();
            this.textBoxS3 = new System.Windows.Forms.TextBox();
            this.textBoxS2 = new System.Windows.Forms.TextBox();
            this.textBoxS1 = new System.Windows.Forms.TextBox();
            this.buttonAll = new System.Windows.Forms.Button();
            this.labelSearchGigi = new System.Windows.Forms.Label();
            this.labelSearchKago = new System.Windows.Forms.Label();
            this.labelSearchTotal = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panelCenter = new System.Windows.Forms.Panel();
            this.buttonFamilyShokai = new System.Windows.Forms.Button();
            this.buttonFamilyRece = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.contextMenuStripImage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.通常表示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.署名点検ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.負傷原因点検ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.長期理由点検ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonNextIDImage = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelImage = new System.Windows.Forms.Panel();
            this.splitterImage = new System.Windows.Forms.Splitter();
            this.施術所番号コピーtoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.施術師番号コピーtoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.scrollPictureControlMain = new Mejor.ScrollPictureControlRev();
            this.scrollPictureControlSub = new Mejor.ScrollPictureControlRev();
            this.inspectControl1 = new Mejor.Inspect.InspectControl();
            this.userControlSearch1 = new Mejor.UserControlSearch();
            this.userControlOryo1 = new Mejor.UserControlOryo();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStripCopy.SuspendLayout();
            this.groupBoxInfo.SuspendLayout();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
            this.splitContainerLeft.Panel1.SuspendLayout();
            this.splitContainerLeft.Panel2.SuspendLayout();
            this.splitContainerLeft.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStripPrint.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panelCenter.SuspendLayout();
            this.contextMenuStripImage.SuspendLayout();
            this.panelImage.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStripCopy;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(609, 431);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // contextMenuStripCopy
            // 
            this.contextMenuStripCopy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.被保番コピーToolStripMenuItem,
            this.施術所番号コピーtoolStripMenuItem1,
            this.施術師番号コピーtoolStripMenuItem1,
            this.toolStripSeparator3,
            this.点検画面toolStripMenuItem,
            this.toolStripSeparator4,
            this.ファイル参照toolStripMenuItem});
            this.contextMenuStripCopy.Name = "contextMenuStripCopy";
            this.contextMenuStripCopy.Size = new System.Drawing.Size(181, 148);
            // 
            // 被保番コピーToolStripMenuItem
            // 
            this.被保番コピーToolStripMenuItem.Name = "被保番コピーToolStripMenuItem";
            this.被保番コピーToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.被保番コピーToolStripMenuItem.Text = "被保番コピー";
            this.被保番コピーToolStripMenuItem.Click += new System.EventHandler(this.被保番コピーToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // 点検画面toolStripMenuItem
            // 
            this.点検画面toolStripMenuItem.Name = "点検画面toolStripMenuItem";
            this.点検画面toolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.点検画面toolStripMenuItem.Text = "点検画面";
            this.点検画面toolStripMenuItem.Click += new System.EventHandler(this.点検画面toolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(177, 6);
            // 
            // ファイル参照toolStripMenuItem
            // 
            this.ファイル参照toolStripMenuItem.Name = "ファイル参照toolStripMenuItem";
            this.ファイル参照toolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ファイル参照toolStripMenuItem.Text = "画像を直接開く";
            this.ファイル参照toolStripMenuItem.Click += new System.EventHandler(this.ファイル参照toolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "総枚数：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "疑義数：";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(36, 792);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 0;
            this.buttonBack.Text = "PgDn << 前";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNext.Location = new System.Drawing.Point(196, 792);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = "次 >> PgUp";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // groupBoxInfo
            // 
            this.groupBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxInfo.Controls.Add(this.textBoxSyokai);
            this.groupBoxInfo.Controls.Add(this.labelY);
            this.groupBoxInfo.Controls.Add(this.label21);
            this.groupBoxInfo.Controls.Add(this.textBoxKago);
            this.groupBoxInfo.Controls.Add(this.textBoxGigi);
            this.groupBoxInfo.Controls.Add(this.label1);
            this.groupBoxInfo.Controls.Add(this.textBoxAll);
            this.groupBoxInfo.Controls.Add(this.label3);
            this.groupBoxInfo.Controls.Add(this.label4);
            this.groupBoxInfo.Location = new System.Drawing.Point(4, -1);
            this.groupBoxInfo.Name = "groupBoxInfo";
            this.groupBoxInfo.Size = new System.Drawing.Size(292, 74);
            this.groupBoxInfo.TabIndex = 0;
            this.groupBoxInfo.TabStop = false;
            // 
            // textBoxSyokai
            // 
            this.textBoxSyokai.Location = new System.Drawing.Point(72, 49);
            this.textBoxSyokai.Name = "textBoxSyokai";
            this.textBoxSyokai.ReadOnly = true;
            this.textBoxSyokai.Size = new System.Drawing.Size(73, 19);
            this.textBoxSyokai.TabIndex = 68;
            this.textBoxSyokai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(6, 13);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(125, 12);
            this.labelY.TabIndex = 0;
            this.labelY.Text = "平成  年度　  月処理分";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(19, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 69;
            this.label21.Text = "照会数：";
            // 
            // textBoxKago
            // 
            this.textBoxKago.Location = new System.Drawing.Point(209, 29);
            this.textBoxKago.Name = "textBoxKago";
            this.textBoxKago.ReadOnly = true;
            this.textBoxKago.Size = new System.Drawing.Size(73, 19);
            this.textBoxKago.TabIndex = 3;
            this.textBoxKago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxGigi
            // 
            this.textBoxGigi.Location = new System.Drawing.Point(209, 49);
            this.textBoxGigi.Name = "textBoxGigi";
            this.textBoxGigi.ReadOnly = true;
            this.textBoxGigi.Size = new System.Drawing.Size(73, 19);
            this.textBoxGigi.TabIndex = 5;
            this.textBoxGigi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "過誤数：";
            // 
            // textBoxAll
            // 
            this.textBoxAll.Location = new System.Drawing.Point(72, 29);
            this.textBoxAll.Name = "textBoxAll";
            this.textBoxAll.ReadOnly = true;
            this.textBoxAll.Size = new System.Drawing.Size(73, 19);
            this.textBoxAll.TabIndex = 1;
            this.textBoxAll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonSearch.Location = new System.Drawing.Point(513, 6);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(72, 48);
            this.buttonSearch.TabIndex = 25;
            this.buttonSearch.Text = "検索(F10)";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.splitContainerLeft);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(611, 818);
            this.panelLeft.TabIndex = 35;
            // 
            // splitContainerLeft
            // 
            this.splitContainerLeft.BackColor = System.Drawing.SystemColors.Info;
            this.splitContainerLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.splitContainerLeft.Name = "splitContainerLeft";
            this.splitContainerLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerLeft.Panel1
            // 
            this.splitContainerLeft.Panel1.AutoScroll = true;
            this.splitContainerLeft.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainerLeft.Panel2
            // 
            this.splitContainerLeft.Panel2.AutoScroll = true;
            this.splitContainerLeft.Panel2.Controls.Add(this.tabControl1);
            this.splitContainerLeft.Size = new System.Drawing.Size(611, 818);
            this.splitContainerLeft.SplitterDistance = 433;
            this.splitContainerLeft.SplitterWidth = 6;
            this.splitContainerLeft.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(609, 377);
            this.tabControl1.TabIndex = 74;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(601, 351);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "検索";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.buttonImport);
            this.panel1.Controls.Add(this.buttonPrint);
            this.panel1.Controls.Add(this.buttonCSV);
            this.panel1.Controls.Add(this.buttonViewSetting);
            this.panel1.Controls.Add(this.btnDBDump);
            this.panel1.Controls.Add(this.btnAllFldExport);
            this.panel1.Controls.Add(this.buttonOther);
            this.panel1.Controls.Add(this.buttonTenkenRemove);
            this.panel1.Controls.Add(this.buttonTenkenAdd);
            this.panel1.Controls.Add(this.buttonShokaiRemove);
            this.panel1.Controls.Add(this.buttonShokaiAdd);
            this.panel1.Controls.Add(this.textBoxS4);
            this.panel1.Controls.Add(this.labelSearchSyokai);
            this.panel1.Controls.Add(this.userControlSearch1);
            this.panel1.Controls.Add(this.textBoxS3);
            this.panel1.Controls.Add(this.textBoxS2);
            this.panel1.Controls.Add(this.textBoxS1);
            this.panel1.Controls.Add(this.buttonAll);
            this.panel1.Controls.Add(this.labelSearchGigi);
            this.panel1.Controls.Add(this.labelSearchKago);
            this.panel1.Controls.Add(this.labelSearchTotal);
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(595, 345);
            this.panel1.TabIndex = 34;
            // 
            // buttonImport
            // 
            this.buttonImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImport.Location = new System.Drawing.Point(449, 317);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(68, 23);
            this.buttonImport.TabIndex = 67;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrint.ContextMenuStrip = this.contextMenuStripPrint;
            this.buttonPrint.Location = new System.Drawing.Point(381, 317);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(68, 23);
            this.buttonPrint.TabIndex = 33;
            this.buttonPrint.Text = "印刷";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // contextMenuStripPrint
            // 
            this.contextMenuStripPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.プリンター設定ToolStripMenuItem,
            this.toolStripSeparator1,
            this.返戻付箋印刷ToolStripMenuItem});
            this.contextMenuStripPrint.Name = "contextMenuStrip1";
            this.contextMenuStripPrint.Size = new System.Drawing.Size(147, 54);
            // 
            // プリンター設定ToolStripMenuItem
            // 
            this.プリンター設定ToolStripMenuItem.Name = "プリンター設定ToolStripMenuItem";
            this.プリンター設定ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.プリンター設定ToolStripMenuItem.Text = "プリンター設定";
            this.プリンター設定ToolStripMenuItem.Click += new System.EventHandler(this.プリンター設定ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // 返戻付箋印刷ToolStripMenuItem
            // 
            this.返戻付箋印刷ToolStripMenuItem.Name = "返戻付箋印刷ToolStripMenuItem";
            this.返戻付箋印刷ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.返戻付箋印刷ToolStripMenuItem.Text = "返戻付箋印刷";
            this.返戻付箋印刷ToolStripMenuItem.Click += new System.EventHandler(this.返戻付箋印刷ToolStripMenuItem_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCSV.Location = new System.Drawing.Point(517, 317);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(68, 23);
            this.buttonCSV.TabIndex = 34;
            this.buttonCSV.Text = "Export";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // buttonViewSetting
            // 
            this.buttonViewSetting.Location = new System.Drawing.Point(515, 351);
            this.buttonViewSetting.Name = "buttonViewSetting";
            this.buttonViewSetting.Size = new System.Drawing.Size(68, 23);
            this.buttonViewSetting.TabIndex = 76;
            this.buttonViewSetting.Text = "表示設定";
            this.buttonViewSetting.UseVisualStyleBackColor = true;
            this.buttonViewSetting.Click += new System.EventHandler(this.ButtonViewSetting_Click);
            // 
            // btnDBDump
            // 
            this.btnDBDump.Location = new System.Drawing.Point(515, 282);
            this.btnDBDump.Name = "btnDBDump";
            this.btnDBDump.Size = new System.Drawing.Size(68, 23);
            this.btnDBDump.TabIndex = 75;
            this.btnDBDump.Text = "DB吐出";
            this.btnDBDump.UseVisualStyleBackColor = true;
            this.btnDBDump.Click += new System.EventHandler(this.BtnDBDump_Click);
            // 
            // btnAllFldExport
            // 
            this.btnAllFldExport.Location = new System.Drawing.Point(515, 243);
            this.btnAllFldExport.Name = "btnAllFldExport";
            this.btnAllFldExport.Size = new System.Drawing.Size(68, 33);
            this.btnAllFldExport.TabIndex = 74;
            this.btnAllFldExport.Text = "全項目\r\nExport";
            this.btnAllFldExport.UseVisualStyleBackColor = true;
            this.btnAllFldExport.Click += new System.EventHandler(this.btnAllFldExport_Click);
            // 
            // buttonOther
            // 
            this.buttonOther.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOther.Location = new System.Drawing.Point(303, 317);
            this.buttonOther.Name = "buttonOther";
            this.buttonOther.Size = new System.Drawing.Size(68, 23);
            this.buttonOther.TabIndex = 67;
            this.buttonOther.Text = "その他";
            this.buttonOther.UseVisualStyleBackColor = true;
            this.buttonOther.Click += new System.EventHandler(this.buttonOther_Click);
            // 
            // buttonTenkenRemove
            // 
            this.buttonTenkenRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTenkenRemove.Location = new System.Drawing.Point(226, 317);
            this.buttonTenkenRemove.Name = "buttonTenkenRemove";
            this.buttonTenkenRemove.Size = new System.Drawing.Size(68, 23);
            this.buttonTenkenRemove.TabIndex = 67;
            this.buttonTenkenRemove.Text = "点検解除";
            this.buttonTenkenRemove.UseVisualStyleBackColor = true;
            this.buttonTenkenRemove.Click += new System.EventHandler(this.buttonTenkenRemove_Click);
            // 
            // buttonTenkenAdd
            // 
            this.buttonTenkenAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTenkenAdd.Location = new System.Drawing.Point(158, 317);
            this.buttonTenkenAdd.Name = "buttonTenkenAdd";
            this.buttonTenkenAdd.Size = new System.Drawing.Size(68, 23);
            this.buttonTenkenAdd.TabIndex = 67;
            this.buttonTenkenAdd.Text = "点検対象";
            this.buttonTenkenAdd.UseVisualStyleBackColor = true;
            this.buttonTenkenAdd.Click += new System.EventHandler(this.buttonTenkenAdd_Click);
            // 
            // buttonShokaiRemove
            // 
            this.buttonShokaiRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShokaiRemove.Location = new System.Drawing.Point(81, 317);
            this.buttonShokaiRemove.Name = "buttonShokaiRemove";
            this.buttonShokaiRemove.Size = new System.Drawing.Size(68, 23);
            this.buttonShokaiRemove.TabIndex = 67;
            this.buttonShokaiRemove.Text = "照会解除";
            this.buttonShokaiRemove.UseVisualStyleBackColor = true;
            this.buttonShokaiRemove.Click += new System.EventHandler(this.buttonShokaiRemove_Click);
            // 
            // buttonShokaiAdd
            // 
            this.buttonShokaiAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShokaiAdd.Location = new System.Drawing.Point(13, 317);
            this.buttonShokaiAdd.Name = "buttonShokaiAdd";
            this.buttonShokaiAdd.Size = new System.Drawing.Size(68, 23);
            this.buttonShokaiAdd.TabIndex = 67;
            this.buttonShokaiAdd.Text = "照会対象";
            this.buttonShokaiAdd.UseVisualStyleBackColor = true;
            this.buttonShokaiAdd.Click += new System.EventHandler(this.buttonSyokaiAdd_Click);
            // 
            // textBoxS4
            // 
            this.textBoxS4.Location = new System.Drawing.Point(515, 190);
            this.textBoxS4.Name = "textBoxS4";
            this.textBoxS4.ReadOnly = true;
            this.textBoxS4.Size = new System.Drawing.Size(68, 19);
            this.textBoxS4.TabIndex = 73;
            this.textBoxS4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelSearchSyokai
            // 
            this.labelSearchSyokai.AutoSize = true;
            this.labelSearchSyokai.Location = new System.Drawing.Point(515, 178);
            this.labelSearchSyokai.Name = "labelSearchSyokai";
            this.labelSearchSyokai.Size = new System.Drawing.Size(59, 12);
            this.labelSearchSyokai.TabIndex = 72;
            this.labelSearchSyokai.Text = "照会判定：";
            // 
            // textBoxS3
            // 
            this.textBoxS3.Location = new System.Drawing.Point(515, 153);
            this.textBoxS3.Name = "textBoxS3";
            this.textBoxS3.ReadOnly = true;
            this.textBoxS3.Size = new System.Drawing.Size(68, 19);
            this.textBoxS3.TabIndex = 68;
            this.textBoxS3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxS2
            // 
            this.textBoxS2.Location = new System.Drawing.Point(515, 118);
            this.textBoxS2.Name = "textBoxS2";
            this.textBoxS2.ReadOnly = true;
            this.textBoxS2.Size = new System.Drawing.Size(68, 19);
            this.textBoxS2.TabIndex = 67;
            this.textBoxS2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxS1
            // 
            this.textBoxS1.Location = new System.Drawing.Point(515, 74);
            this.textBoxS1.Name = "textBoxS1";
            this.textBoxS1.ReadOnly = true;
            this.textBoxS1.Size = new System.Drawing.Size(68, 19);
            this.textBoxS1.TabIndex = 6;
            this.textBoxS1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonAll
            // 
            this.buttonAll.Location = new System.Drawing.Point(515, 214);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(68, 23);
            this.buttonAll.TabIndex = 35;
            this.buttonAll.Text = "全解除";
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // labelSearchGigi
            // 
            this.labelSearchGigi.AutoSize = true;
            this.labelSearchGigi.Location = new System.Drawing.Point(515, 142);
            this.labelSearchGigi.Name = "labelSearchGigi";
            this.labelSearchGigi.Size = new System.Drawing.Size(59, 12);
            this.labelSearchGigi.TabIndex = 66;
            this.labelSearchGigi.Text = "疑義判定：";
            // 
            // labelSearchKago
            // 
            this.labelSearchKago.AutoSize = true;
            this.labelSearchKago.Location = new System.Drawing.Point(515, 106);
            this.labelSearchKago.Name = "labelSearchKago";
            this.labelSearchKago.Size = new System.Drawing.Size(59, 12);
            this.labelSearchKago.TabIndex = 15;
            this.labelSearchKago.Text = "過誤判定：";
            // 
            // labelSearchTotal
            // 
            this.labelSearchTotal.AutoSize = true;
            this.labelSearchTotal.Location = new System.Drawing.Point(515, 59);
            this.labelSearchTotal.Name = "labelSearchTotal";
            this.labelSearchTotal.Size = new System.Drawing.Size(59, 12);
            this.labelSearchTotal.TabIndex = 47;
            this.labelSearchTotal.Text = "検出枚数：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.userControlOryo1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(601, 351);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "往療情報";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panelCenter
            // 
            this.panelCenter.BackColor = System.Drawing.Color.AliceBlue;
            this.panelCenter.Controls.Add(this.buttonFamilyShokai);
            this.panelCenter.Controls.Add(this.buttonFamilyRece);
            this.panelCenter.Controls.Add(this.buttonUpdate);
            this.panelCenter.Controls.Add(this.inspectControl1);
            this.panelCenter.Controls.Add(this.buttonExit);
            this.panelCenter.Controls.Add(this.buttonNext);
            this.panelCenter.Controls.Add(this.buttonBack);
            this.panelCenter.Controls.Add(this.groupBoxInfo);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelCenter.Location = new System.Drawing.Point(614, 0);
            this.panelCenter.MinimumSize = new System.Drawing.Size(280, 0);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(300, 818);
            this.panelCenter.TabIndex = 36;
            // 
            // buttonFamilyShokai
            // 
            this.buttonFamilyShokai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonFamilyShokai.Location = new System.Drawing.Point(81, 763);
            this.buttonFamilyShokai.Name = "buttonFamilyShokai";
            this.buttonFamilyShokai.Size = new System.Drawing.Size(75, 23);
            this.buttonFamilyShokai.TabIndex = 6;
            this.buttonFamilyShokai.Text = "関連照会";
            this.buttonFamilyShokai.UseVisualStyleBackColor = true;
            this.buttonFamilyShokai.Click += new System.EventHandler(this.buttonFamilyShokai_Click);
            // 
            // buttonFamilyRece
            // 
            this.buttonFamilyRece.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonFamilyRece.Location = new System.Drawing.Point(0, 763);
            this.buttonFamilyRece.Name = "buttonFamilyRece";
            this.buttonFamilyRece.Size = new System.Drawing.Size(75, 23);
            this.buttonFamilyRece.TabIndex = 6;
            this.buttonFamilyRece.Text = "関連レセ";
            this.buttonFamilyRece.UseVisualStyleBackColor = true;
            this.buttonFamilyRece.Click += new System.EventHandler(this.buttonFamilyRece_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.Location = new System.Drawing.Point(225, 763);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 5;
            this.buttonUpdate.Text = "更新(F12)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExit.Location = new System.Drawing.Point(124, 792);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(60, 23);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.ContextMenuStrip = this.contextMenuStripImage;
            this.buttonImageFill.Location = new System.Drawing.Point(2, 3);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 66;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // contextMenuStripImage
            // 
            this.contextMenuStripImage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.通常表示ToolStripMenuItem,
            this.toolStripSeparator2,
            this.署名点検ToolStripMenuItem,
            this.負傷原因点検ToolStripMenuItem,
            this.長期理由点検ToolStripMenuItem});
            this.contextMenuStripImage.Name = "contextMenuStripImage";
            this.contextMenuStripImage.Size = new System.Drawing.Size(147, 98);
            // 
            // 通常表示ToolStripMenuItem
            // 
            this.通常表示ToolStripMenuItem.Name = "通常表示ToolStripMenuItem";
            this.通常表示ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.通常表示ToolStripMenuItem.Text = "通常表示";
            this.通常表示ToolStripMenuItem.Click += new System.EventHandler(this.通常表示ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // 署名点検ToolStripMenuItem
            // 
            this.署名点検ToolStripMenuItem.Name = "署名点検ToolStripMenuItem";
            this.署名点検ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.署名点検ToolStripMenuItem.Text = "署名点検";
            this.署名点検ToolStripMenuItem.Click += new System.EventHandler(this.署名点検ToolStripMenuItem_Click);
            // 
            // 負傷原因点検ToolStripMenuItem
            // 
            this.負傷原因点検ToolStripMenuItem.Name = "負傷原因点検ToolStripMenuItem";
            this.負傷原因点検ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.負傷原因点検ToolStripMenuItem.Text = "負傷原因点検";
            this.負傷原因点検ToolStripMenuItem.Click += new System.EventHandler(this.負傷原因点検ToolStripMenuItem_Click);
            // 
            // 長期理由点検ToolStripMenuItem
            // 
            this.長期理由点検ToolStripMenuItem.Name = "長期理由点検ToolStripMenuItem";
            this.長期理由点検ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.長期理由点検ToolStripMenuItem.Text = "長期理由点検";
            this.長期理由点検ToolStripMenuItem.Click += new System.EventHandler(this.長期理由点検ToolStripMenuItem_Click);
            // 
            // buttonNextIDImage
            // 
            this.buttonNextIDImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextIDImage.Location = new System.Drawing.Point(357, 3);
            this.buttonNextIDImage.Name = "buttonNextIDImage";
            this.buttonNextIDImage.Size = new System.Drawing.Size(68, 23);
            this.buttonNextIDImage.TabIndex = 67;
            this.buttonNextIDImage.Text = "次ID画像";
            this.buttonNextIDImage.UseVisualStyleBackColor = true;
            this.buttonNextIDImage.Click += new System.EventHandler(this.buttonNextIDImage_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(611, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 818);
            this.splitter1.TabIndex = 68;
            this.splitter1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(914, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 818);
            this.splitter2.TabIndex = 69;
            this.splitter2.TabStop = false;
            // 
            // panelImage
            // 
            this.panelImage.Controls.Add(this.buttonImageFill);
            this.panelImage.Controls.Add(this.buttonNextIDImage);
            this.panelImage.Controls.Add(this.scrollPictureControlMain);
            this.panelImage.Controls.Add(this.splitterImage);
            this.panelImage.Controls.Add(this.scrollPictureControlSub);
            this.panelImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImage.Location = new System.Drawing.Point(917, 0);
            this.panelImage.Name = "panelImage";
            this.panelImage.Size = new System.Drawing.Size(427, 818);
            this.panelImage.TabIndex = 70;
            // 
            // splitterImage
            // 
            this.splitterImage.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitterImage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterImage.Location = new System.Drawing.Point(0, 688);
            this.splitterImage.Name = "splitterImage";
            this.splitterImage.Size = new System.Drawing.Size(427, 4);
            this.splitterImage.TabIndex = 69;
            this.splitterImage.TabStop = false;
            this.splitterImage.Visible = false;
            // 
            // 施術所番号コピーtoolStripMenuItem1
            // 
            this.施術所番号コピーtoolStripMenuItem1.Name = "施術所番号コピーtoolStripMenuItem1";
            this.施術所番号コピーtoolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.施術所番号コピーtoolStripMenuItem1.Text = "施術所番号コピー";
            this.施術所番号コピーtoolStripMenuItem1.Click += new System.EventHandler(this.施術所番号コピーtoolStripMenuItem1_Click);
            // 
            // 施術師番号コピーtoolStripMenuItem1
            // 
            this.施術師番号コピーtoolStripMenuItem1.Name = "施術師番号コピーtoolStripMenuItem1";
            this.施術師番号コピーtoolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.施術師番号コピーtoolStripMenuItem1.Text = "施術師/者番号コピー";
            this.施術師番号コピーtoolStripMenuItem1.Click += new System.EventHandler(this.施術師番号コピーtoolStripMenuItem1_Click);
            // 
            // scrollPictureControlMain
            // 
            this.scrollPictureControlMain.BackColor = System.Drawing.Color.LightSteelBlue;
            this.scrollPictureControlMain.ButtonsVisible = false;
            this.scrollPictureControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControlMain.Location = new System.Drawing.Point(0, 0);
            this.scrollPictureControlMain.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControlMain.Name = "scrollPictureControlMain";
            this.scrollPictureControlMain.Ratio = 1F;
            this.scrollPictureControlMain.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControlMain.Size = new System.Drawing.Size(427, 688);
            this.scrollPictureControlMain.TabIndex = 71;
            // 
            // scrollPictureControlSub
            // 
            this.scrollPictureControlSub.BackColor = System.Drawing.Color.LightSteelBlue;
            this.scrollPictureControlSub.ButtonsVisible = false;
            this.scrollPictureControlSub.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.scrollPictureControlSub.Location = new System.Drawing.Point(0, 692);
            this.scrollPictureControlSub.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControlSub.Name = "scrollPictureControlSub";
            this.scrollPictureControlSub.Ratio = 1F;
            this.scrollPictureControlSub.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControlSub.Size = new System.Drawing.Size(427, 126);
            this.scrollPictureControlSub.TabIndex = 70;
            this.scrollPictureControlSub.Visible = false;
            // 
            // inspectControl1
            // 
            this.inspectControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inspectControl1.Enabled = false;
            this.inspectControl1.Location = new System.Drawing.Point(0, 74);
            this.inspectControl1.MinimumSize = new System.Drawing.Size(280, 0);
            this.inspectControl1.Name = "inspectControl1";
            this.inspectControl1.RelationAid = 0;
            this.inspectControl1.RelationButtonVisible = true;
            this.inspectControl1.Size = new System.Drawing.Size(300, 684);
            this.inspectControl1.TabIndex = 3;
            this.inspectControl1.DataChanged += new System.EventHandler(this.inspectControl_DataChanged);
            // 
            // userControlSearch1
            // 
            this.userControlSearch1.Location = new System.Drawing.Point(-1, 3);
            this.userControlSearch1.Name = "userControlSearch1";
            this.userControlSearch1.Size = new System.Drawing.Size(510, 393);
            this.userControlSearch1.TabIndex = 69;
            // 
            // userControlOryo1
            // 
            this.userControlOryo1.Location = new System.Drawing.Point(0, 0);
            this.userControlOryo1.MinimumSize = new System.Drawing.Size(500, 309);
            this.userControlOryo1.Name = "userControlOryo1";
            this.userControlOryo1.Size = new System.Drawing.Size(534, 309);
            this.userControlOryo1.TabIndex = 0;
            // 
            // ListCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 818);
            this.Controls.Add(this.panelImage);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelLeft);
            this.KeyPreview = true;
            this.Name = "ListCreateForm";
            this.Text = "文書照会作成";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ListCreateForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStripCopy.ResumeLayout(false);
            this.groupBoxInfo.ResumeLayout(false);
            this.groupBoxInfo.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.splitContainerLeft.Panel1.ResumeLayout(false);
            this.splitContainerLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
            this.splitContainerLeft.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStripPrint.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panelCenter.ResumeLayout(false);
            this.contextMenuStripImage.ResumeLayout(false);
            this.panelImage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.GroupBox groupBoxInfo;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelCenter;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.TextBox textBoxKago;
        private System.Windows.Forms.TextBox textBoxAll;
        private System.Windows.Forms.TextBox textBoxGigi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.Label labelSearchGigi;
        private System.Windows.Forms.Label labelSearchKago;
        private System.Windows.Forms.Label labelSearchTotal;
        private System.Windows.Forms.TextBox textBoxSyokai;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxS3;
        private System.Windows.Forms.TextBox textBoxS2;
        private System.Windows.Forms.TextBox textBoxS1;
        private UserControlSearch userControlSearch1;
        private System.Windows.Forms.TextBox textBoxS4;
        private System.Windows.Forms.Label labelSearchSyokai;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private UserControlOryo userControlOryo1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripPrint;
        private System.Windows.Forms.ToolStripMenuItem プリンター設定ToolStripMenuItem;
        private System.Windows.Forms.Button buttonNextIDImage;
        private Inspect.InspectControl inspectControl1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonFamilyRece;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 返戻付箋印刷ToolStripMenuItem;
        private System.Windows.Forms.Panel panelImage;
        private System.Windows.Forms.Splitter splitterImage;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripImage;
        private System.Windows.Forms.ToolStripMenuItem 通常表示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 署名点検ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 負傷原因点検ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 長期理由点検ToolStripMenuItem;
        private ScrollPictureControlRev scrollPictureControlSub;
        private ScrollPictureControlRev scrollPictureControlMain;
        private System.Windows.Forms.Button buttonFamilyShokai;
        private System.Windows.Forms.Button btnAllFldExport;
        private System.Windows.Forms.Button btnDBDump;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCopy;
        private System.Windows.Forms.ToolStripMenuItem 被保番コピーToolStripMenuItem;
        private System.Windows.Forms.Button buttonViewSetting;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 点検画面toolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainerLeft;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem ファイル参照toolStripMenuItem;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonOther;
        private System.Windows.Forms.Button buttonTenkenRemove;
        private System.Windows.Forms.Button buttonTenkenAdd;
        private System.Windows.Forms.Button buttonShokaiRemove;
        private System.Windows.Forms.Button buttonShokaiAdd;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonCSV;
        private System.Windows.Forms.ToolStripMenuItem 施術所番号コピーtoolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 施術師番号コピーtoolStripMenuItem1;
    }
}