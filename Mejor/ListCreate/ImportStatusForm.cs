﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    public partial class ImportStatusForm : Form
    {
        BindingSource bs = new BindingSource();

        public ImportStatusForm()
        {
            InitializeComponent();
            bs.DataSource = new List<App>();
            dataGridView1.DataSource = bs;

            for (int j = 0; j < dataGridView1.ColumnCount; j++)
            {
                dataGridView1.Columns[j].Visible = false;
                dataGridView1.Columns[j].ReadOnly = true;
            }
            dataGridView1.Columns[nameof(App.Aid)].Visible = true;
            dataGridView1.Columns[nameof(App.Aid)].Width = 70;
            dataGridView1.Columns[nameof(App.Aid)].HeaderText = "ID";

            dataGridView1.Columns[nameof(App.JCYM)].Visible = true;
            dataGridView1.Columns[nameof(App.JCYM)].Width = 50;
            dataGridView1.Columns[nameof(App.JCYM)].HeaderText = "処理月";
            dataGridView1.Columns[nameof(App.JYM)].Visible = true;
            dataGridView1.Columns[nameof(App.JYM)].Width = 50;
            dataGridView1.Columns[nameof(App.JYM)].HeaderText = "診療月";

            dataGridView1.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridView1.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridView1.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(App.AppType)].Visible = true;
            dataGridView1.Columns[nameof(App.AppType)].Width = 40;
            dataGridView1.Columns[nameof(App.AppType)].HeaderText = "種類";
            dataGridView1.Columns[nameof(App.ShokaiHenreiStatus)].Visible = true;
            dataGridView1.Columns[nameof(App.ShokaiHenreiStatus)].Width = 70;
            dataGridView1.Columns[nameof(App.ShokaiHenreiStatus)].HeaderText = "照会/返戻";
            dataGridView1.Columns[nameof(App.TenkenResult)].Visible = true;
            dataGridView1.Columns[nameof(App.TenkenResult)].Width = 70;
            dataGridView1.Columns[nameof(App.TenkenResult)].HeaderText = "点検";
            dataGridView1.Columns[nameof(App.CountedDays)].Visible = true;
            dataGridView1.Columns[nameof(App.CountedDays)].Width = 25;
            dataGridView1.Columns[nameof(App.CountedDays)].HeaderText = "日";
            dataGridView1.Columns[nameof(App.DrNum)].Visible = true;
            dataGridView1.Columns[nameof(App.DrNum)].Width = 70;
            dataGridView1.Columns[nameof(App.DrNum)].HeaderText = "施術所No";
            dataGridView1.Columns[nameof(App.Total)].Visible = true;
            dataGridView1.Columns[nameof(App.Total)].Width = 70;
            dataGridView1.Columns[nameof(App.Total)].HeaderText = "合計金額";
            dataGridView1.Columns[nameof(App.Select)].Visible = true;
            dataGridView1.Columns[nameof(App.Select)].Width = 25;
            dataGridView1.Columns[nameof(App.Select)].HeaderText = "";
            dataGridView1.Columns[nameof(App.Select)].ReadOnly = false;
            dataGridView1.Columns[nameof(App.Select)].DisplayIndex = 0;
            dataGridView1.Columns[nameof(App.ViewIndex)].Width = 25;
            dataGridView1.Columns[nameof(App.ViewIndex)].HeaderText = "行";
            dataGridView1.Columns[nameof(App.ViewIndex)].ReadOnly = true;
            dataGridView1.Columns[nameof(App.ViewIndex)].Visible = true;
            dataGridView1.Columns[nameof(App.ViewIndex)].DisplayIndex = 1;
            dataGridView1.Columns[nameof(App.JCYM)].DisplayIndex = 2;
            dataGridView1.Columns[nameof(App.JYM)].DisplayIndex = 3;
        }

        private void radioButtonHenrei_CheckedChanged(object sender, EventArgs e)
        {
            panelHenrei.Enabled = 
                (radioButtonHenrei.Checked || radioButtonTel.Checked) && radioButtonAdd.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var apps = (List<App>)bs.DataSource;
            apps.RemoveAll(app => !app.Select);

            if(apps.Count==0)
            {
                MessageBox.Show("対象となるデータがありません\r\n" +
                    "先にAIDを記述したCSVファイルを読み込んだ上で処理を開始して下さい");
                return;
            }

            var f =
                radioButtonShokai.Checked ? StatusFlag.照会対象 :
                radioButtonTenken.Checked ? StatusFlag.点検対象 :
                radioButtonHenrei.Checked ? StatusFlag.返戻 :
                radioButtonOuryo.Checked ? StatusFlag.往療点検対象 :
                radioButtonHoryu.Checked ? StatusFlag.支払保留 :
                radioButtonTel.Checked ? StatusFlag.架電対象 :
                radioButtonShiharai.Checked ? StatusFlag.支払済 :
                radioButtonPro1.Checked ? StatusFlag.処理1 :
                radioButtonPro2.Checked ? StatusFlag.処理2 :
                radioButtonPro3.Checked ? StatusFlag.処理3 :
                radioButtonPro4.Checked ? StatusFlag.処理4 :
                radioButtonPro5.Checked ? StatusFlag.処理5 :
                StatusFlag.未処理;

            if (f == StatusFlag.未処理)
            {
                MessageBox.Show("対象種類を選んでください");
                return;
            }

            if(!radioButtonAdd.Checked && !radioButtonRemove.Checked)
            {
                MessageBox.Show("管理を選んでください");
                return;
            }
            
            if (radioButtonAdd.Checked) add(f, apps);
            else remove(f, apps);
        }

        private void add(StatusFlag f, List<App> apps)
        {
            HenreiReasons rs = 0;
            string sub = string.Empty;
            if(f == StatusFlag.返戻 || f == StatusFlag.架電対象)
            {
                if (checkBoxHenBui.Checked) { rs |= HenreiReasons.負傷部位; sub += "負傷部位 "; }
                if (checkBoxHenReason.Checked) { rs |= HenreiReasons.負傷原因; sub += "負傷原因 "; }
                if (checkBoxHenJiki.Checked) { rs |= HenreiReasons.負傷時期; sub += "負傷時期 "; }
                if (checkBoxHenKago.Checked) { rs |= HenreiReasons.過誤; sub += "過誤 "; }
                if (checkBoxHenKega.Checked) { rs |= HenreiReasons.けが外; sub += "けが以外 "; }
                if (checkBoxHenDays.Checked) { rs |= HenreiReasons.受療日数; sub += "受療日数 "; }
                if (checkBoxHenWork.Checked) { rs |= HenreiReasons.勤務中; sub += "勤務中 "; }
                if (checkBoxHenOther.Checked) { rs |= HenreiReasons.その他; sub += "その他 "; }
            }

            if (MessageBox.Show($"現在チェックされている {apps.Count} 件の申請書を" +
                ((f == StatusFlag.返戻 || f == StatusFlag.架電対象) ? 
                    rs == 0 ? "理由なしで" : "理由: [ " + sub + "]で" : string.Empty) +
                $"{f}に指定します。本当によろしいですか？",
                "対象確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK)
                return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.SetMax(apps.Count);

                foreach (var item in apps)
                {
                    item.StatusFlagSet(f);
                    item.HenreiReasonsSet(rs);
                    item.UpdateINQ();
                    wf.InvokeValue++;
                }
            }
            this.Close();
        }

        private void remove(StatusFlag f, List<App> apps)
        {
            var sub = f == StatusFlag.往療点検対象 ? "また往療点検済みの場合、点検結果が失われます。" :
                f == StatusFlag.返戻 ? "また返戻理由が失われます。" :
                f == StatusFlag.架電対象 ? "また架電理由が失われます。" :
                string.Empty;

            var l = apps.FindAll(x => x.StatusFlagCheck(f));

            if (MessageBox.Show($"現在チェックされていて、かつ{f}となっている " +
                $"{l.Count} 件の申請書を{f}から除外します。" +
                $"{sub}" +
                $"\r\n本当によろしいですか？",
                "除外確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                != DialogResult.OK) return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.SetMax(l.Count);

                foreach (var item in l)
                {
                    if (f == StatusFlag.往療点検対象)
                    {
                        item.StatusFlagRemove(StatusFlag.往療点検対象 | StatusFlag.往療点検済 | StatusFlag.往療疑義);
                        item.Uinquiry = User.CurrentUser.UserID;
                        //item.AppNote.OryoNote = string.Empty;
                    }
                    else if (f == StatusFlag.支払保留)
                    {
                        item.StatusFlagRemove(StatusFlag.支払保留);
                    }
                    else if (f == StatusFlag.返戻)
                    {
                        item.StatusFlagRemove(StatusFlag.返戻);
                        if (!item.StatusFlagCheck(StatusFlag.架電対象))
                        {
                            item.HenreiReasons = HenreiReasons.なし;
                        }
                    }
                    else if (f == StatusFlag.架電対象)
                    {
                        item.StatusFlagRemove(StatusFlag.架電対象);
                        if (!item.StatusFlagCheck(StatusFlag.返戻))
                        {
                            item.HenreiReasons = HenreiReasons.なし;
                        }
                    }
                    else if (f == StatusFlag.支払済)
                    {
                        item.StatusFlagRemove(StatusFlag.支払済);
                    }
                    else if (f == StatusFlag.処理1)
                    {
                        item.StatusFlagRemove(StatusFlag.処理1);
                    }
                    else if (f == StatusFlag.処理2)
                    {
                        item.StatusFlagRemove(StatusFlag.処理2);
                    }
                    else if (f == StatusFlag.処理3)
                    {
                        item.StatusFlagRemove(StatusFlag.処理3);
                    }
                    else if (f == StatusFlag.処理4)
                    {
                        item.StatusFlagRemove(StatusFlag.処理4);
                    }
                    else if (f == StatusFlag.処理5)
                    {
                        item.StatusFlagRemove(StatusFlag.処理5);
                    }
                    item.UpdateINQ();
                    wf.InvokeValue++;
                }
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ImportStatusForm_Load(object sender, EventArgs e)
        {
            //readCSV();
        }

        private void readCSV()
        {
            var csv = string.Empty;

            using (var f = new OpenFileDialog())
            {
                f.Title = "CSV形式で、1行目はヘッダ、１列目に対象となるAIDが記述されたファイルを準備して下さい";
                f.Filter = "CSVファイル|*.csv";

                if (f.ShowDialog() != DialogResult.OK) return;
                csv = f.FileName;
            }

            textBox1.Text = csv;

            bs.Clear();

            var l = new List<int>();
            var error = false;
            int lineNo = 1;

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("CSVファイルを読み込んでいます…");

                try
                {
                    using (var sr = new System.IO.StreamReader(csv, Encoding.UTF8))
                    {
                        //最初の一行はヘッダー
                        sr.ReadLine();

                        while (sr.Peek() > 0)
                        {
                            lineNo++;
                            var s = sr.ReadLine().Split(',');
                            if (s.Length < 1)
                            {
                                error = true;
                                wf.LogPrint($"{lineNo}行目のデータが読み取れませんでした");
                                continue;
                            }

                            if (s[0].Length > 1)
                            {
                                if (!int.TryParse(s[0], out int i))
                                {
                                    wf.LogPrint($"{lineNo}行目のAIDが数字に変換できませんでした　データ:[{s[0]}]");
                                    continue;
                                }
                                l.Add(i);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    wf.LogPrint("CSVの読み取りでエラーが発生しました\r\n" + ex.Message);
                    error = true;

                }

                if (error)
                {
                    var res = MessageBox.Show("途中でエラーが発生しています。ログを記録しますか？", "",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (res == DialogResult.OK)
                    {
                        using (var f = new SaveFileDialog())
                        {
                            f.FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".log";
                            if (f.ShowDialog() == DialogResult.OK)
                            {
                                wf.LogSave(f.FileName);
                            }
                        }
                    }

                    res = MessageBox.Show($"エラー無く読み込めたデータ、{l.Count}件のみで処理を進てもよろしいですか？");
                    if (res != DialogResult.OK) return;
                }

                error = false;
                wf.LogPrint("CSVのAIDを元に、入力データを読み込んでいます…");
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.SetMax(l.Count);

                var apps = new List<App>();
                foreach (var item in l)
                {
                    wf.InvokeValue++;
                    var app = App.GetApp(item);
                    if (app == null)
                    {
                        error = true;
                        wf.LogPrint($"存在しないAID指定されています　AID:[{item}]");
                        continue;
                    }
                    apps.Add(app);
                }

                if (error)
                {
                    var res = MessageBox.Show("途中でエラーが発生しています。ログを記録しますか？", "",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (res == DialogResult.OK)
                    {
                        using (var f = new SaveFileDialog())
                        {
                            f.FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".log";
                            if (f.ShowDialog() == DialogResult.OK)
                            {
                                wf.LogSave(f.FileName);
                            }
                        }
                    }
                }

                apps.ForEach(item => item.Select = true);
                bs.DataSource = apps;
            }
            bs.ResetBindings(false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            readCSV();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
