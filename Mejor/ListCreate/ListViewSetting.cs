﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Mejor.ListCreate
{
    /* listviewsettingテーブル
       create table listviewsetting(
            insurerid integer not null,
            isinsurertype boolean not null,
            settings json not null,
            constraint listviewsetting_pk
            primary key (insurerid, isinsurertype)
       );
     */

    /// <summary>
    /// listviewsettingテーブルのsettingの中のJSONの定義
    /// </summary>
    [DataContract]
    public class ListViewSettingJSON
    {
        /// <summary>
        /// {"List":[{"設定名1":表示有無},{"設定名2":表示有無}, ...]}のJSON形式
        /// </summary>
        [DataMember]
        public List<Dictionary<string, bool>> List { get; set; }
    }

    /// <summary>
    /// リスト作成画面のリスト表示項目の設定用
    /// <br />
    /// 保険者毎にDBに保持される
    /// </summary>
    public class ListViewSetting
    {
        private const string TableName = "listviewsetting";
        private readonly int id;
        private readonly bool isInsurer;

        /// <summary>
        /// リスト表示項目の設定用クラスを生成する
        /// </summary>
        /// <param name="insurer">対象の保険者</param>
        public ListViewSetting(Insurer insurer)
        {
            // 学校共済は保険者ごとではなくまとめて扱う
            isInsurer = insurer.InsurerType != INSURER_TYPE.学校共済;
            id = isInsurer ? insurer.InsurerID : (int)insurer.InsurerType;
        }

        /// <summary>
        /// 設定を読み込む
        /// <br />
        /// 初期状態と突き合わされるので、DBに保存されていても初期状態に含まれない場合は無視されて表示されない
        /// </summary>
        /// <param name="defaultList">初期状態のリスト</param>
        /// <returns>読み込み結果</returns>
        public bool Load(Dictionary<string, ListCreateForm.HeaderSetting> defaultList)
        {
            var db = new DB("jyusei");
            var result = db.Query<string>($@"SELECT settings FROM {TableName} 
                WHERE insurerid = @id AND isinsurertype = @type;",
                new { id, type = isInsurer })?.ToList();
            if (result == null)
            {
                return false;
            }

            // 既存の設定がない場合は成功扱い
            if (result.Count == 0)
            {
                return true;
            }

            // JSON文字列→ListViewSettingJSON
            var text = result.First();
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(text)))
            {
                var setting = new DataContractJsonSerializerSettings
                {
                    UseSimpleDictionaryFormat = true
                };
                var serializer = new DataContractJsonSerializer(typeof(ListViewSettingJSON), setting);

                if (serializer.ReadObject(stream) is ListViewSettingJSON json)
                {
                    for (int i = 0; i < json.List.Count; i++)
                    {
                        foreach (var key in json.List[i].Keys)
                        {
                            if (defaultList.ContainsKey(key))
                            {
                                var item = defaultList[key];
                                item.DisplayIndex = i;
                                item.Visible = json.List[i][key];
                            }
                        }
                    }
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        /// <param name="settings">現在の設定</param>
        /// <returns>保存結果</returns>
        public bool Save(Dictionary<string, ListCreateForm.HeaderSetting> settings)
        {
            // 表示順に並び替え
            var list = settings.OrderBy(item => item.Value.DisplayIndex)
                .Select(item => new Dictionary<string, bool> { { item.Key, item.Value.Visible } });
            var obj = new ListViewSettingJSON { List = list.ToList() };

            // ListViewSettingJSON→JSON文字列
            using (var stream = new MemoryStream())
            {
                var setting = new DataContractJsonSerializerSettings()
                {
                    UseSimpleDictionaryFormat = true
                };
                var serializer = new DataContractJsonSerializer(typeof(ListViewSettingJSON), setting);
                serializer.WriteObject(stream, obj);

                var json = Encoding.UTF8.GetString(stream.ToArray());

                var db = new DB("jyusei");

                using (var tran = db.CreateTransaction())
                {
                    // UPSERTのかわり
                    db.Excute($@"DELETE FROM {TableName} 
                        WHERE insurerid = @id AND isinsurertype = @type;",
                        new { id, type = isInsurer }, tran);

                    var result = db.Excute($@"INSERT INTO {TableName} 
                        (insurerid, isinsurertype, settings) VALUES(@id, @type, CAST(@json as json));",
                        new { id, type = isInsurer, json }, tran);

                    if (!result)
                    {
                        tran.Rollback();
                        return false;
                    }

                    tran.Commit();
                    return true;
                }
            }
        }
    }
}
