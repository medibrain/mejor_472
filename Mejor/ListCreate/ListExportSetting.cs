﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Mejor.ListCreate
{
    public class ListExportSetting
    {
        public static void SetSampleApp(App app) => sampleAppColumns = new AppColumns(app);

        static AppColumns sampleAppColumns = null;

        [Flags]
        public enum SettingFlag
        {
            Null = 0,
            DateAd = 0x01, DateJpShrot = 0x02, DateJpFull = 0x04, DateJpNone =0x08,
            SeparateKanji = 0x10, SeparateSlash = 0x20, SeparateNone = 0x40,
            DataText = 0x100, DateZero = 0x400,
            Zenkaku=0x800,//20190501173311 furukawa 全角フラグ追加
            DateJpEraNum=0x1000,//20210602110643 furukawa 和暦数字書式追加
                                

        }

        public enum ValueType
        {
            Null = 0, Integer = 1, Date = 2, YearMonth = 3, Year = 4, String = 5,
            Era = 6, Double = 7,
            Blank = 99999
        }

        public class ColumnInfo
        {
            public string Name { get; private set; }
            public string Description { get; private set; }
            public int ViewIndex { get; set; }
            public ValueType ValueType { get; private set; }

            public object SampleValue { get; private set; }

            public ColumnInfo(string name, string description, ValueType type)
            {
                Name = name;
                Description = description;
                ValueType = type;
            }
        }

        public class Column
        {
            static Dictionary<string, PropertyInfo> dic = new Dictionary<string, PropertyInfo>();

            static Column()
            {
                var t = typeof(AppColumns);
                var ps = t.GetProperties();
                foreach (var item in ps) dic.Add(item.Name, item);
            }

            /// <summary>
            /// 出力プロパティ名
            /// </summary>
            public string Name => ColumnInfo.Name;
            public string Description => ColumnInfo.Description;
            
            /// <summary>
            /// CSV出力時ヘッダー
            /// </summary>
            public string Header { get; set; } = string.Empty;

            public int Index { get; set; }
            public SettingFlag SettingFlags { get; set; }
            public string AfterWord { get; set; }
            public ColumnInfo ColumnInfo { get; private set; }

            public void SettingFlagSet(SettingFlag flag) => SettingFlags |= flag;
            public void SettingFlagRemove(SettingFlag flag) => SettingFlags &= ~flag;
            public bool SettingFlagCheck(SettingFlag flag) => (SettingFlags & flag) == flag;

            public PropertyInfo PropertyInfo => dic.ContainsKey(Name) ? dic[Name] : null;

            public string Sample
            {
                get
                {
                    if (sampleAppColumns == null)
                        return CreateString(ColumnInfo.SampleValue);

                    var v = PropertyInfo.GetValue(sampleAppColumns);
                    return CreateString(v);
                }
            }

            public string CreateString(object value)
            {
                if (ColumnInfo.ValueType == ValueType.String)
                {

                    //20190507191024 furukawa st ////////////////////////
                    //全角チェックボックスやめる（影響範囲が大きい）


                    //      20190501173819 furukawa st ////////////////////////
                    //      全角フラグのときは全角、それ以外は半角


                    //      if (SettingFlagCheck(SettingFlag.Zenkaku))
                    //      {
                    //          return Microsoft.VisualBasic.Strings.StrConv((string)value,Microsoft.VisualBasic.VbStrConv.Wide) + AfterWord;
                    //      }
                    //      else
                    //      {
                    //          return Microsoft.VisualBasic.Strings.StrConv((string)value, Microsoft.VisualBasic.VbStrConv.Narrow) + AfterWord;
                    //      }


                    //      if (ColumnInfo.ValueType == ValueType.String)
                    //      {
                        
                    
                    //20210519175859 furukawa st ///////////////////////                    
                    //追加文言を前後どちらかに付与
                    if (AfterWord!=null && AfterWord.Length>0 && AfterWord.Substring(0, 1) == "+") return AfterWord.Substring(1) + (string)value;
                    else return (string)value + AfterWord;


                    //      return (string)value + AfterWord;
                    //      20210519175859 furukawa ed ////////////////////////




                    //      20190501173819 furukawa ed ////////////////////////

                    //}
                    //20190507191024 furukawa ed ////////////////////////
                }



                if (ColumnInfo.ValueType == ValueType.Integer)
                {
                    //20210519181332 furukawa st ////////////////////////
                    //追加文言を前後どちらかに付与
                    
                    if (AfterWord != null && AfterWord.Length > 0 && AfterWord.Substring(0, 1) == "+") return AfterWord.Substring(1) + ((int)value).ToString();
                    else return ((int)value).ToString() + AfterWord;

                    //      return ((int)value).ToString() + AfterWord;
                    //20210519181332 furukawa ed ////////////////////////
                }

                //年月
                if (ColumnInfo.ValueType == ValueType.YearMonth)
                {
                    int y = ((int)value) / 100;
                    int m = ((int)value) % 100;

                    //西暦
                    if (SettingFlagCheck(SettingFlag.DateAd))
                    {
                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                y.ToString("0000") + "/" + m.ToString("00") :
                                y.ToString() + "/" + m.ToString();
                        }
                        else if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                y.ToString("0000") + "年" + m.ToString("00") + "月" :
                                y.ToString() + "年" + m.ToString() + "月";
                        }                        
                        else
                        {
                            return y.ToString("0000") + m.ToString("00");
                        }
                    }

                    //和暦
                    else if (SettingFlagCheck(SettingFlag.DateJpShrot) ||
                        SettingFlagCheck(SettingFlag.DateJpFull) ||
                        SettingFlagCheck(SettingFlag.DateJpNone) ||
                        SettingFlagCheck(SettingFlag.DateJpEraNum))//20210602110643 furukawa 和暦数字書式追加
                                                                   
                    {
                        var dt = new DateTime(y, m, 1);

                        //年号 //20210602110643 furukawa 和暦数字書式追加 最後に足さないと、途中で判定されるとそっちに行ってしまう
                        var e = SettingFlagCheck(SettingFlag.DateJpNone) ? string.Empty :
                            SettingFlagCheck(SettingFlag.DateJpShrot) ? DateTimeEx.GetInitialEra(dt) :
                            SettingFlagCheck(SettingFlag.DateJpFull) ? DateTimeEx.GetEra(dt):
                            SettingFlagCheck(SettingFlag.DateJpEraNum) ? DateTimeEx.GetEraNumber(dt).ToString() :
                            DateTimeEx.GetEra(dt);
                        

                        //年の数字
                        y = DateTimeEx.GetJpYear(dt);

                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") + "/" + m.ToString("00") :
                                e + y.ToString() + "/" + m.ToString();
                        }
                        else if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") + "年" + m.ToString("00") + "月" :
                                e + y.ToString() + "年" + m.ToString() + "月";
                        }                     
                        else
                        {
                            return e + y.ToString("00") + m.ToString("00");
                        }
                    }
                    else
                    {
                        return y.ToString("0000") + m.ToString("00");
                    }
                }
                //年号
                if (ColumnInfo.ValueType == ValueType.Era)
                {
                    DateTime dt;
                    if (value is int y) dt = new DateTime(y / 100, y % 100, 1);
                    else if (value is DateTime) dt = (DateTime)value;
                    else return string.Empty;
                    if (dt.IsNullDate()) return string.Empty;

                    if (SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot))
                    {
                        return DateTimeEx.GetInitialEra(dt);
                    }
                    else if (SettingFlagCheck(ListExportSetting.SettingFlag.DateJpFull))
                    {
                        return DateTimeEx.GetEra(dt);
                    }
                    //20210602110643 furukawa st ////////////////////////
                    //和暦数字書式追加                    
                    else if (SettingFlagCheck(ListExportSetting.SettingFlag.DateJpEraNum))
                    {
                        return DateTimeEx.GetEraNumber(dt).ToString();
                    }
                    //20210602110643 furukawa ed ////////////////////////
                    return string.Empty;
                }

                //年月日
                if (ColumnInfo.ValueType == ValueType.Date)
                {
                    var dt = (DateTime)value;
                    if (dt.IsNullDate()) return string.Empty;

                    //西暦
                    if (SettingFlagCheck(SettingFlag.DateAd))
                    {
                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                dt.ToString("yyyy/MM/dd") :
                                dt.ToString("yyyy/M/d");
                        }
                        if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                dt.ToString("yyyy年MM月dd日") :
                                dt.ToString("yyyy年M月d日");
                        }
                        else
                        {
                            return dt.ToString("yyyyMMdd");
                        }
                    }
                    //和暦
                    else if (SettingFlagCheck(SettingFlag.DateJpShrot) ||
                        SettingFlagCheck(SettingFlag.DateJpFull) ||
                        SettingFlagCheck(SettingFlag.DateJpNone) ||
                        SettingFlagCheck(SettingFlag.DateJpEraNum))//20210602110643 furukawa和暦数字書式追加
                    {
                        //年号 //20210602110643 furukawa 和暦数字書式追加 最後に足さないと、途中で判定されるとそっちに行ってしまう
                        var e = SettingFlagCheck(SettingFlag.DateJpNone) ? string.Empty :
                            SettingFlagCheck(SettingFlag.DateJpShrot) ? DateTimeEx.GetInitialEra(dt) :
                            SettingFlagCheck(SettingFlag.DateJpFull) ? DateTimeEx.GetEra(dt) :
                            SettingFlagCheck(SettingFlag.DateJpEraNum) ? DateTimeEx.GetEraNumber(dt).ToString() :
                            DateTimeEx.GetEra(dt);


                        var y = DateTimeEx.GetJpYear(dt);

                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") + "/" + dt.ToString("MM/dd") :
                                e + y.ToString() + "/" + dt.ToString("M/d");
                        }
                        else if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") + "年" + dt.ToString("MM月dd日") :
                                e + y.ToString() + "年" + dt.ToString("M月d日");
                        }
                        else
                        {
                            return e + y.ToString("00") + dt.ToString("MMdd");
                        }
                    }
                    else
                        return dt.ToString("yyyyMMdd");
                }

                //年
                if (ColumnInfo.ValueType == ValueType.Year)
                {
                    int y, m;
                    DateTime dt;
                    if (value is int iv)
                    {
                        y = ((int)value) / 100;
                        m = ((int)value) % 100;
                        dt = new DateTime(y, m, 1);
                    }
                    else if (value is DateTime)
                    {
                        dt = (DateTime)value;
                        y = dt.Year;
                        m = dt.Month;
                    }
                    else
                    {
                        return string.Empty;
                    }

                    if (SettingFlagCheck(SettingFlag.DateAd))
                    {
                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                y.ToString("0000") : y.ToString();
                        }
                        else if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                y.ToString("0000") + "年" : y.ToString() + "年";
                        }
                        else
                        {
                            return y.ToString("0000");
                        }
                    }

                    //和暦年
                    else if (SettingFlagCheck(SettingFlag.DateJpShrot) ||
                        SettingFlagCheck(SettingFlag.DateJpFull) ||
                        SettingFlagCheck(SettingFlag.DateJpNone) ||
                        SettingFlagCheck(SettingFlag.DateJpEraNum))//20210602110643 furukawa 和暦数字書式追加
                    {
                        //年号 //20210602110643 furukawa 和暦数字書式追加 最後に足さないと、途中で判定されるとそっちに行ってしまう
                        var e = SettingFlagCheck(SettingFlag.DateJpNone) ? string.Empty :
                            SettingFlagCheck(SettingFlag.DateJpShrot) ? DateTimeEx.GetInitialEra(dt) :
                            SettingFlagCheck(SettingFlag.DateJpFull) ? DateTimeEx.GetEra(dt) :
                            SettingFlagCheck(SettingFlag.DateJpEraNum) ? DateTimeEx.GetEraNumber(dt).ToString() :
                            DateTimeEx.GetEra(dt);

                        y = DateTimeEx.GetJpYear(dt);

                        if (SettingFlagCheck(SettingFlag.SeparateSlash))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") : e + y.ToString();
                        }
                        else if (SettingFlagCheck(SettingFlag.SeparateKanji))
                        {
                            return SettingFlagCheck(SettingFlag.DateZero) ?
                                e + y.ToString("00") + "年" : e + y.ToString() + "年";
                        }
                        else
                        {
                            return e + y.ToString("00");
                        }
                    }
                    else
                    {
                        return y.ToString("0000");
                    }
                }

                if (ColumnInfo.ValueType == ValueType.Double)
                {
                    //20210519181404 furukawa st ////////////////////////
                    //追加文言を前後どちらかに付与
                    
                    if (AfterWord != null && AfterWord.Length > 0 && AfterWord.Substring(0, 1) == "+") return AfterWord.Substring(1) + ((double)value).ToString("0.##");
                    else return ((double)value).ToString("0.##") + AfterWord;
                    //      return ((double)value).ToString("0.##") + AfterWord;
                    //20210519181404 furukawa ed ////////////////////////
                }
                return string.Empty;
            }

            public Column(ColumnInfo info, string header, int index, SettingFlag flags, string afterWord)
            {
                if (!columnInfos.ContainsKey(info.Name)) return;
                ColumnInfo = info;
                Header = header;
                Index = index;
                SettingFlags = flags;

                //20210519162405 furukawa st ////////////////////////
                //追加の文言が登録されてない
                
                AfterWord = afterWord;
                //20210519162405 furukawa ed ////////////////////////
            }

            //列設定ロード
            public Column(ColumnInfo info)
            {
                if (!columnInfos.ContainsKey(info.Name)) return;
                ColumnInfo = info;
                Header = info.Description;

                if (info.ValueType == ValueType.Date)
                {
                    SettingFlagSet(SettingFlag.DataText);
                    SettingFlagSet(SettingFlag.SeparateSlash);
                    SettingFlagSet(SettingFlag.DateAd);
                    SettingFlagSet(SettingFlag.DateZero);
                    //SettingFlagSet(SettingFlag.DateJpEraNum);//20210602110643 furukawa和暦数字書式追加
                }
                else if (info.ValueType == ValueType.YearMonth)
                {
                    SettingFlagSet(SettingFlag.DataText);
                    SettingFlagSet(SettingFlag.SeparateSlash);
                    SettingFlagSet(SettingFlag.DateAd);
                    SettingFlagSet(SettingFlag.DateZero);
                //    SettingFlagSet(SettingFlag.DateJpEraNum);//20210602110643 furukawa 和暦数字書式追加
                }
                else if (info.ValueType == ValueType.Year)
                {
                    SettingFlagSet(SettingFlag.SeparateNone);
                    SettingFlagSet(SettingFlag.DateAd);
                   // SettingFlagSet(SettingFlag.DateJpEraNum);//20210602110643 furukawa 和暦数字書式追加
                }
                else if (info.ValueType == ValueType.Era)
                {
                    SettingFlagSet(SettingFlag.DataText);
                    SettingFlagSet(SettingFlag.DateJpFull);
                   // SettingFlagSet(SettingFlag.DateJpEraNum);//20210602110643 furukawa 和暦数字書式追加

                }
            }

            public string SaveStr => $"{Name}||{Header}||{Index}||{(int)SettingFlags}||{AfterWord}";

            public static Column LoadFromStr(string loadStr)
            {
                var ss = loadStr.Split(new string[] { "||" }, StringSplitOptions.None);
                if (!columnInfos.ContainsKey(ss[0])) return null;
                var ci = columnInfos[ss[0]];
                var s = new Column(ci, ss[1], int.Parse(ss[2]), (SettingFlag)int.Parse(ss[3]), ss[4]);
                return s;
            }
        }
        
        static ListExportSetting()
        {
            var l = new List<ColumnInfo>();

            l.Add(new ColumnInfo(nameof(AppColumns.AID), "AID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.ScanID), "スキャンID",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.GroupID), "グループID",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.ViewIndex), "連番",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.CYM), "処理年月",  ValueType.YearMonth));
            l.Add(new ColumnInfo(nameof(AppColumns.ChargeYear), "処理年",  ValueType.Year));
            l.Add(new ColumnInfo(nameof(AppColumns.ChargeMonth), "処理月",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.YM), "診療年月",  ValueType.YearMonth));
            l.Add(new ColumnInfo(nameof(AppColumns.MediYear), "診療年",  ValueType.Year));
            l.Add(new ColumnInfo(nameof(AppColumns.MediMonth), "診療月",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.InsNum), "保険者番号(通常)",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InputInsNum), "保険者番号(入力別)", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoNum), "被保険者番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoPref), "都道府県",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoZip), "被保険者〒",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoZipHyphen), "被保険者〒(ハイフン)",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoAdd), "被保険者住所",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoAdd1), "被保険者住所1",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoAdd2), "被保険者住所2",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoName), "被保険者名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.AppType), "申請種類",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Birthday), "生年月日", ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.BirthdayEra), "生年月日-年号", ValueType.Era));
            l.Add(new ColumnInfo(nameof(AppColumns.BirthdayYear), "生年月日-年", ValueType.Year));
            l.Add(new ColumnInfo(nameof(AppColumns.BirthdayMonth), "生年月日-月", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.BirthdayDay), "生年月日-日", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.PersonName), "受療者名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Sex), "性別",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Family), "本家区分",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.MediAge), "診療月年齢", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.ChargeAge), "請求月年齢", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.TodayAge), "現在年齢", ValueType.Integer));
            
            l.Add(new ColumnInfo(nameof(AppColumns.FushoName1), "負傷1負傷名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoName2), "負傷2負傷名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoName3), "負傷3負傷名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoName4), "負傷4負傷名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoName5), "負傷5負傷名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDate1), "負傷1負傷日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDate2), "負傷2負傷日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDate3), "負傷3負傷日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDate4), "負傷4負傷日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDate5), "負傷5負傷日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFirstDate1), "負傷1初検日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFirstDate2), "負傷2初検日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFirstDate3), "負傷3初検日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFirstDate4), "負傷4初検日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFirstDate5), "負傷5初検日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoStartDate1), "負傷1開始日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoStartDate2), "負傷2開始日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoStartDate3), "負傷3開始日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoStartDate4), "負傷4開始日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoStartDate5), "負傷5開始日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCourse1), "負傷1転帰",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCourse2), "負傷2転帰",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCourse3), "負傷3転帰",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCourse4), "負傷4転帰",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCourse5), "負傷5転帰",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFinishDate1), "負傷1終了日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFinishDate2), "負傷2終了日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFinishDate3), "負傷3終了日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFinishDate4), "負傷4終了日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoFinishDate5), "負傷5終了日",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDays1), "負傷1実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDays2), "負傷2実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDays3), "負傷3実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDays4), "負傷4実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoDays5), "負傷5実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Bui), "あんま部位",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.NewContType), "新規継続",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Total), "合計",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Days), "実日数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Charge), "請求額",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Ratio), "給付割合",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Partial), "負担額",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Numbering), "ナンバリング",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.RrID), "申請書情報ID",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicNum), "施術所番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicTel), "施術所TEL",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicZip), "施術所〒",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicZipHyphen), "施術所〒(ハイフン)", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicAdd), "施術所住所",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ClinicName), "施術所名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Distance), "往療有無",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DrKana), "施術師カナ",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DrName), "施術師名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DrNum), "施術師番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.BankName), "銀行名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.BankType), "銀行種別", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.BankBranch), "銀行支店",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.BankBranchType), "銀行支店種別",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.AccountNumber), "口座番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.AccountType), "口座種別",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.AccountName), "口座名義",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.AccountKana), "口座名義カナ", ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.InputStatus), "入力状況",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.VisitAdd), "加算往療",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.VisitFee), "往療",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.VisitTimes), "往療回数",  ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.MemoInspect), "点検メモ",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.MemoShokai), "照会メモ",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Memo), "メモ",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.OutMemo), "出力メモ",  ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.TenkenResult), "点検結果",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ShokaiHenreiStatus), "照会/返戻",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.OryoResult), "往療点検結果",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.KagoReasons), "過誤理由",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ShokaiReasons), "照会理由",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.SaishinsaReasons), "再審査理由",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HenreiReasons), "返戻理由",  ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.PayCode), "支払コード",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ShokaiCode), "照会番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ShokaiResult), "照会結果",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ComNum), "電算管理番号", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GroupNum), "グループ番号", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.KouhiNum), "公費番号", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.JukyuNum), "受給者番号", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DestName), "送付先氏名", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DestZip), "送付先〒", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DestAdd), "送付先住所", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ContactDt), "連絡日時",  ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.ContactName), "連絡了承者名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ContactStatus), "事前連絡不要/団体",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.ScanNote), "Note",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.BatchNo), "バッチ番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Dates), "診療日", ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.AppDistance), "申請距離", ValueType.Double));
            l.Add(new ColumnInfo(nameof(AppColumns.DistCalcClinicAdd), "距離計算施術所住所", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DistCalcAdd), "距離計算被保険者住所", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.CalcDistance), "計算距離", ValueType.Double));

            l.Add(new ColumnInfo(nameof(AppColumns.GakkoShokaiCodeCreate), "学校照会番号(生成)", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GakkoDantaiName), "学校共済団体名",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GakkoAccountNo), "学校口座番号",  ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GakkoHenreiCode), "学校返戻管理番号", ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.ShokaiFlag), "照会対象", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InspectFlag), "点検対象", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.OryoInspectFlag), "往療点検対象", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HenreiFlag), "返戻対象", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.PendFlag), "支払保留", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InputErrorFlag), "入力時エラー", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.TelFlag), "架電対象", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.PaidFlag), "支払済", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Transaction1Flag), "処理1", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Transaction2Flag), "処理2", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Transaction3Flag), "処理3", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Transaction4Flag), "処理4", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Transaction5Flag), "処理5", ValueType.String));

            l.Add(new ColumnInfo(nameof(AppColumns.Blank), "ブランク",  ValueType.Blank));

            l.Add(new ColumnInfo(nameof(AppColumns.JCYM), "処理月", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.JYM), "診療月", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.HihoType), "被保険者拡張情報", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.PublcExpense), "公費番号", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.Ufirst), "1回目入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Usecond), "2回目入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Uinquiry), "点検ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.UfirstEx), "1回目拡張入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.UsecondEx), "2回目拡張入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.AdditionalUid1), "1回目追加入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.AdditionalUid2), "2回目追加入力ユーザーID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.FushoCount), "負傷数", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.InputStatusEx), "拡張入力状況", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InputStatusAdd), "追加入力状況", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InspectDescription), "点検詳細", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InspectInfo), "点検結果", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InputOrderNumber), "ステータス", ValueType.Integer));

            //taggedatas項目
            l.Add(new ColumnInfo(nameof(AppColumns.RelationAID), "RelationAID", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.Kana), "Kana", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DestKana), "送付先カナ", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.InsName), "InsName", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.DouiDate), "同意日", ValueType.Date));
            l.Add(new ColumnInfo(nameof(AppColumns.PastSupplyYM), "前回支給年月", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSejutuDouiUmu), "施術同意書有無", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgKofuUmu), "交付料有無", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_SikakuNotMatch), "SikakuNotMatch", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_BirthNotMatch), "BirthNotMatch", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_GenderNotMatch), "GenderNotMatch", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_RatioNotMatch), "RatioNotMatch", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_RangeNotMatch), "RangeNotMatch", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.flgSikaku_SinryoBeforeIssue), "SinryoBeforeIssue", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.count), "count", ValueType.Integer));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString1), "汎用文字列1", ValueType.String));

            //20210408111115 furukawa st ////////////////////////
            //汎用文字列2～8を追加
            
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString2), "汎用文字列2", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString3), "汎用文字列3", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString4), "汎用文字列4", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString5), "汎用文字列5", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString6), "汎用文字列6", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString7), "汎用文字列7", ValueType.String));
            l.Add(new ColumnInfo(nameof(AppColumns.GeneralString8), "汎用文字列8", ValueType.String));
            //20210408111115 furukawa ed ////////////////////////

            for (int i = 0; i < l.Count; i++)
            {
                l[i].ViewIndex = i;
                columnInfos.Add(l[i].Name, l[i]);
            }
        }

        private static Dictionary<string, ColumnInfo> columnInfos = new Dictionary<string, ColumnInfo>();

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int ID { get; private set; } = 0;
        public int InsurerID { get; set; } = 0;
        public string Name { get; set; } = string.Empty;
        public string FileName { get; set; } = "ExportList";
        public string PropSaveStr
        {
            get
            {
                return string.Join("\t", Columns.Select(x => x.SaveStr));
            }
            private set
            {
                Columns.Clear();
                var ps = value.Split('\t');
                foreach (var item in ps)
                {
                    var s = Column.LoadFromStr(item);
                    if (s == null) continue;
                    Columns.Add(s);
                }
            }
        }

        public string ExportType { get; set; } = string.Empty;



        //20210501134355 furukawa st ////////////////////////
        //CSVでない、その他テキストファイルの追加。区切り文字も表示するため、ExportTypeそのまま出してる
        [DB.DbAttribute.Ignore]
        public string ExportTypeDescription =>
            ID == 0 ? string.Empty :
            ExportType == "UTF8" ? "CSV (UTF8)" :
            ExportType == "SJIS" ? "CSV (Shift JIS)" :
            ExportType.Contains("TEXTUTF8") ? ExportType :
            ExportType.Contains("TEXTSJIS") ? ExportType :
            "Excel";

        //public string ExportTypeDescription =>
        //    ID == 0 ? string.Empty :
        //    ExportType == "UTF8" ? "CSV (UTF8)" :
        //    ExportType == "SJIS" ? "CSV (Shift JIS)" :
        //    "Excel";
        //20210501134355 furukawa ed ////////////////////////



        public bool HeaderWrite { get; set; } = true;

        [DB.DbAttribute.Ignore]
        public List<Column> Columns { get; private set; } = new List<Column>();

        [DB.DbAttribute.Ignore]
        public string TargetName => ID == 0 ? "" :
            InsurerID == 0 ? "すべて" :
            InsurerID > 0 ? Insurer.GetInsurerName(InsurerID) :
            ((INSURER_TYPE)(-InsurerID)).ToString();

        public static List<ColumnInfo>GetAllColumns() =>
            columnInfos.Values.ToList();

        public List<ColumnInfo>GetSelectedCols()
        {
            var l = new List<ColumnInfo>();
            foreach (var item in Columns)
            {
                if (!columnInfos.ContainsKey(item.Name)) continue;
                l.Add(columnInfos[item.Name]);
            }
            return l;
        }

        /// <summary>
        /// Insert成功時はIDを、Update成功時は0を、失敗した場合は-1を返します
        /// </summary>
        /// <returns></returns>
        public int Upsert(DB db, DB.Transaction tran)
        {
            var sql = "SELECT EXISTS(SELECT id FROM listexportsetting)";
            if ((ID != 0 && db.Query<bool>(sql).First()))
            {
                return db.Update(this, tran) ? 0 : -1;
            }
            else
            {
                var id = db.InsertReturning(this, tran);
                return id == null || id.Count() == 0 ? -1 : id.First().id;
            }
        }

        public static List<ListExportSetting> GetAllSetting(DB db, int insurerID)
        {
            var ins = Insurer.GetInsurer(insurerID);
            if (ins == null) return new List<ListExportSetting>();

            var insurerType = -(int)ins.InsurerType;
            var sql = $"{nameof(ListExportSetting.InsurerID)} IN ({insurerID}, {insurerType}, 0)" +
                $"ORDER BY id;";
            return db.Select<ListExportSetting>(sql).ToList();
        }

        public bool Delete(DB db)
        {
            var sql = $"DELETE FROM {nameof(ListExportSetting)} WHERE id={ID};";
            return db.Excute(sql);
        }

        public ListExportSetting CopyNew()
        {
            var s = (ListExportSetting)this.MemberwiseClone();
            s.ID = 0;
            s.Name = string.Empty;
            return s;
        }
    }
}
