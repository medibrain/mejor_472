﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ListCreate
{
    public partial class ListExportSettingForm : Form
    {
        DB db = null;
        BindingSource bsOn = new BindingSource();
        BindingSource bsOff = new BindingSource();
        ListExportSetting setting;
        IEnumerable<App> apps;

        public ListExportSettingForm(DB db, ListExportSetting setting, IEnumerable<App> apps)
        {
            InitializeComponent();
            this.db = db;
            this.setting = setting ?? new ListExportSetting();
            this.apps = apps;
            numericUpDown1.Value = 1;
            numericUpDown1.Minimum = 1;
            numericUpDown1.Maximum = apps.Count();

            //20210501093751 furukawa st ////////////////////////
            //環境に合わせて色づけ            
            CommonTool.setFormColor(this);
            //20210501093751 furukawa ed ////////////////////////

            dataGridViewOn.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridViewOn.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridViewOff.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridViewOff.DefaultCellStyle.SelectionForeColor = Color.Black;

            bsOn.DataSource = new List<ListExportSetting.Column>();
            bsOff.DataSource = new List<ListExportSetting.Column>();

            var off = ListExportSetting.GetAllColumns();
            off.ForEach(c => bsOff.Add(new ListExportSetting.Column(c)));

            dataGridViewOn.DataSource = bsOn;
            dataGridViewOff.DataSource = bsOff;

            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Name)].Visible = false;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.SaveStr)].Visible = false;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.ColumnInfo)].Visible = false;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.SettingFlags)].Visible = false;

            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Index)].Width = 30;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Index)].HeaderText = string.Empty;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Index)].DisplayIndex = 0;
            
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Description)].HeaderText = "項目名";

            //20210501111304 furukawa st ////////////////////////
            //画面フォントサイズ変更により、画面サイズ変更            
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Description)].Width = 180;
            //dataGridViewOn.Columns[nameof(ListExportSetting.Column.Description)].Width = 140;
            //20210501111304 furukawa ed ////////////////////////

            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Header)].HeaderText = "ヘッダー";
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Header)].Width = 150;

            dataGridViewOn.Columns[nameof(ListExportSetting.Column.AfterWord)].HeaderText = "追加ワード";
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.AfterWord)].Width = 150;
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Sample)].HeaderText = "実サンプル";

            //20210501111304 furukawa st ////////////////////////
            //画面フォントサイズ変更により、画面サイズ変更
            dataGridViewOn.Columns[nameof(ListExportSetting.Column.Sample)].Width = 180;
            //dataGridViewOn.Columns[nameof(ListExportSetting.Column.Sample)].Width = 152;
            //20210501111304 furukawa ed ////////////////////////

            dataGridViewOn.Columns[nameof(ListExportSetting.Column.PropertyInfo)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Name)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.SaveStr)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.ColumnInfo)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Header)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.SettingFlags)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Index)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.AfterWord)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Sample)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.PropertyInfo)].Visible = false;
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Description)].HeaderText = "項目名";


            //20210501111304 furukawa st ////////////////////////
            //画面フォントサイズ変更により、画面サイズ変更
            dataGridViewOff.Columns[nameof(ListExportSetting.Column.Description)].Width = 180;
            //dataGridViewOff.Columns[nameof(ListExportSetting.Column.Description)].Width = 125;
            //20210501111304 furukawa ed ////////////////////////

            Shown += (s, e) => setSetting();

            bsOn.CurrentChanged += (s, e) => setColumn();

            var ins = Insurer.CurrrentInsurer;
            radioButtonInsurer.Text = ins.InsurerName;
            radioButtonInsurerType.Text = ins.InsurerType.ToString();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var c = (ListExportSetting.Column)bsOff.Current;
            if (c == null) return;

            if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Blank)
            {
                var info = new ListExportSetting.ColumnInfo(nameof(AppColumns.Blank), "ブランク", ListExportSetting.ValueType.Blank);
                c = new ListExportSetting.Column(info);
                c.Index = 9999;
                bsOn.Add(c);
            }
            else
            {
                c.Index = 9999;
                bsOn.Add(c);
                bsOff.RemoveCurrent();
            }
            showReset();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (c.ColumnInfo.ValueType != ListExportSetting.ValueType.Blank) bsOff.Add(c);
            bsOn.RemoveCurrent();

            showReset();
        }

        private void showReset()
        {
            //Index再割り当て
            var ons = (List<ListExportSetting.Column>)bsOn.DataSource;
            ons.Sort((x, y) => x.Index.CompareTo(y.Index));
            for (int i = 0; i < ons.Count; i++) ons[i].Index = i + 1;

            var offs = (List<ListExportSetting.Column>)bsOff.DataSource;
            offs.Sort((x, y) => x.ColumnInfo.ViewIndex.CompareTo(y.ColumnInfo.ViewIndex));

            bsOn.ResetBindings(false);
            bsOff.ResetBindings(false);
        }


        /// <summary>
        /// 上に
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUp_Click(object sender, EventArgs e)
        {
            var l = (List<ListExportSetting.Column>)bsOn.DataSource;
            int index = bsOn.Position;
            if (index == 0) return;

            l[index].Index--;
            l[index - 1].Index++;

            bsOn.Position = index - 1;
            showReset();
        }

        /// <summary>
        /// 下に
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDown_Click(object sender, EventArgs e)
        {
            var l = (List<ListExportSetting.Column>)bsOn.DataSource;
            int index = bsOn.Position;
            if (index + 1 == l.Count) return;

            l[index].Index++;
            l[index + 1].Index--;

            bsOn.Position = index + 1;
            showReset();
        }

        /// <summary>
        /// 登録
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (!Upsert())
            {
                MessageBox.Show("設定の保存に失敗しました");
            }
            else
            {
                MessageBox.Show("設定を保存しました");
                this.Close();
            }
        }

        private bool Upsert()
        {
            if (textBoxName.Text.Trim().Length == 0)
            {
                MessageBox.Show("設定名を必ず指定して下さい");
                return false;
            }

            var checkFn = textBoxFileName.Text.Trim();
            checkFn = checkFn.Replace("{cym}", "").Replace("{ins}", "");
            if(checkFn.Contains("{") || checkFn.Contains("}"))
            {
                MessageBox.Show("初期ファイル名が不正です");
                return false;
            }

            var dest = string.Empty;
            var cellAdd = string.Empty;

            string strSep = string.Empty;
            strSep = comboBoxSep.Text.Trim();

            //Excelベースファイル使用時
            try
            {
                if (radioButtonExcel.Checked && checkBoxExcelBase.Checked)
                {
                    //セルの確認
                    cellAdd = textBoxCell.Text.Trim().ToUpper();
                    if (cellAdd.Length != 2 || (cellAdd[0] < 'A' || 'I' < cellAdd[0]) || (cellAdd[1] < '0' || '9' < cellAdd[1]))
                    {
                        MessageBox.Show("開始セルが不正です。A1～I9で指定して下さい。");
                        return false;
                    }

                    //ベースファイルの更新
                    if (setting.ID == 0 || (setting.ID > 0 && !string.IsNullOrEmpty(excelBaseFileName)))
                    {
                        //ベースファイルを保存or上書き
                        dest = Settings.BaseExcelDir + "\\" +
                            setting.ID.ToString("000000") + "_" + System.IO.Path.GetFileName(excelBaseFileName);
                        System.IO.File.Copy(excelBaseFileName, dest, true);

                        //いままでのベースファイルがあれば削除
                        var fs = System.IO.Directory.EnumerateFiles(Settings.BaseExcelDir, setting.ID.ToString("000000") + "_*.xlsx");
                        foreach (var item in fs)
                        {
                            if (item == dest) continue;
                            System.IO.File.Delete(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ベースファイルの保存に失敗しました\r\n" + ex.Message);
                return false;
            }

            setting.Columns.Clear();

            foreach (ListExportSetting.Column item in bsOn)
            {
                setting.Columns.Add(item);
            }
            setting.Name = textBoxName.Text.Trim();
            setting.FileName = textBoxFileName.Text.Trim();

            //20210501095042 furukawa st ////////////////////////
            //その他テキストファイルの追加
            
            setting.ExportType = 
                radioButtonUTF8.Checked ? "UTF8" :
                radioButtonSJIS.Checked ? "SJIS" :
                radioButtonTextUTF8.Checked ? "TEXTUTF8"+ strSep :
                radioButtonTextSJIS.Checked ? "TEXTSJIS"+ strSep :
                checkBoxExcelBase.Checked ? "ExcelBase|" + cellAdd :
                "Excel";

            //setting.ExportType = radioButtonUTF8.Checked ? "UTF8" :
            //    radioButtonSJIS.Checked ? "SJIS" :
            //    checkBoxExcelBase.Checked ? "ExcelBase|" + cellAdd :
            //    "Excel";
            //20210501095042 furukawa ed ////////////////////////


            setting.HeaderWrite = checkBoxHeader.Checked;
            setting.InsurerID = radioButtonInsurer.Checked ? Insurer.CurrrentInsurer.InsurerID :
                radioButtonInsurerType.Checked ? -(int)Insurer.CurrrentInsurer.InsurerType : 0;

            using (var tran = db.CreateTransaction())
            {
                int id = setting.Upsert(db, tran);

                if (id == -1) return false;
                if (id > 0 && checkBoxExcelBase.Checked)
                {
                    //新規時
                    try
                    {
                        var rename = Settings.BaseExcelDir + "\\" +
                            id.ToString("000000") + "_" + System.IO.Path.GetFileName(excelBaseFileName);
                        System.IO.File.Move(dest, rename);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ベースファイルの保存に失敗しました\r\n"+ex.Message);
                        return false;
                    }
                }
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// ファイル設定のロード
        /// </summary>
        private void setSetting()
        {
            bsOff.Clear();
            bsOn.Clear();
            textBoxCell.Clear();

            buttonOK.Text = setting.ID == 0 ? "登録" : "更新";
            textBoxID.Text = setting.ID.ToString();
            textBoxName.Text = setting.Name;
            textBoxFileName.Text = setting.FileName;
            checkBoxHeader.Checked = setting.HeaderWrite;
            radioButtonInsurer.Checked = setting.InsurerID > 0;
            radioButtonInsurerType.Checked = setting.InsurerID < 0;
            radioButtonAll.Checked = setting.InsurerID == 0;

            //出力形式
            checkBoxExcelBase.CheckedChanged -= checkBoxExcelBase_CheckedChanged;
            checkBoxExcelBase.Checked = false;
            checkBoxExcelBase.Enabled = false;
            textBoxCell.Enabled = false;
            if (string.IsNullOrWhiteSpace(setting.ExportType) || setting.ExportType=="UTF8")
            {
                radioButtonUTF8.Checked = true;
            }
            else if(setting.ExportType =="SJIS")
            {
                radioButtonSJIS.Checked = true;
            }

     
            else if (setting.ExportType == "Excel")
            {
                radioButtonExcel.Checked = true;
                checkBoxExcelBase.Enabled = true;
            }
            else
            {
                //20210501101044 furukawa st ////////////////////////
                //その他テキストファイルの追加
                if (setting.ExportType.Contains("TEXTUTF8"))
                {
                    radioButtonTextUTF8.Checked = true;
                    comboBoxSep.Text = setting.ExportType.Substring(8);
                }
                else if (setting.ExportType.Contains("TEXTSJIS"))
                {
                    radioButtonTextSJIS.Checked = true;
                    comboBoxSep.Text = setting.ExportType.Substring(8);
                }
                else
                {


                    radioButtonExcel.Checked = true;
                    checkBoxExcelBase.Checked = true;
                    checkBoxExcelBase.Enabled = true;
                    textBoxCell.Enabled = true;
                    textBoxCell.Text = setting.ExportType.Split('|')[1];

                }

                //          radioButtonExcel.Checked = true;
                //          checkBoxExcelBase.Checked = true;
                //          checkBoxExcelBase.Enabled = true;
                //          textBoxCell.Enabled = true;
                //          textBoxCell.Text = setting.ExportType.Split('|')[1];
                //20210501101044 furukawa ed ////////////////////////


                }
                checkBoxExcelBase.CheckedChanged += checkBoxExcelBase_CheckedChanged;

            ////選択カラム
            //var selected = new Dictionary<string, ListExportSetting.Column>();
            //setting.Columns.ForEach(c => selected.Add(c.Name, c));
            //var l = ListExportSetting.GetAllColumns();

            //foreach (var item in l)
            //{
            //    if (selected.ContainsKey(item.Name))
            //    {
            //        bsOn.Add(selected[item.Name]);
            //    }
            //    else
            //    {
            //        bsOff.Add(new ListExportSetting.Column(item));
            //    }
            //}

            //選択カラム
            var cols = new Dictionary<string, ListExportSetting.ColumnInfo>();
            ListExportSetting.GetAllColumns().ForEach(c => cols.Add(c.Name, c));

            foreach (var item in setting.Columns)
            {
                bsOn.Add(item);
                if (item.ColumnInfo.ValueType == ListExportSetting.ValueType.Blank) continue;
                cols.Remove(item.Name);
            }
            foreach (var item in cols.Values)
            {
                bsOff.Add(new ListExportSetting.Column(item));
            }
            



            showReset();
        }

        bool coliumnNowSetting = false;
        /// <summary>
        /// 出力項目の設定ロード
        /// </summary>
        private void setColumn()
        {
            coliumnNowSetting = true;

            var c = (ListExportSetting.Column)bsOn.Current;
            radioButtonMoji.Checked = false;
            radioButtonData.Checked = false;
            radioButtonSlash.Checked = false;
            radioButtonKanji.Checked = false;
            radioButtonSepNone.Checked = false;
            radioButtonAd.Checked = false;
            radioButtonJpKanji.Checked = false;
            radioButtonJpShort.Checked = false;
            radioButtonJpNone.Checked = false;

            //20210602110209 furukawa st ////////////////////////
            //和暦数字書式追加
            radioButtonJPEra.Checked = false;
            //20210602110209 furukawa ed ////////////////////////



            if (c == null)
            {
                textBoxOnName.Clear();
                textBoxOnHeader.Clear();
                textBoxOnWord.Clear();
                textBoxOnSample.Clear();
            }
            else
            {
                textBoxOnName.Text = c.Description;
                textBoxOnHeader.Text = c.Header;
                textBoxOnWord.Text = c.AfterWord;
                textBoxOnSample.Text = c.Sample;

                //年月　表記
                if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Date ||
                    c.ColumnInfo.ValueType == ListExportSetting.ValueType.YearMonth)
                {
                    //excel時データ
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                        radioButtonMoji.Checked = true;
                    else
                        radioButtonData.Checked = true;

                    //日付区切り
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.SeparateNone))
                        radioButtonSepNone.Checked = true;
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.SeparateKanji))
                        radioButtonKanji.Checked = true;
                    else
                        radioButtonSlash.Checked = true;


                    //歴表示　日付書式
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpFull))
                        radioButtonJpKanji.Checked = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpEraNum))
                        radioButtonJPEra.Checked = true;
                    //20210602110209 furukawa ed ////////////////////////



                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot))
                        radioButtonJpShort.Checked = true;
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpNone))
                        radioButtonJpNone.Checked = true;
                    else
                        radioButtonAd.Checked = true;


                    //年月ゼロ埋め
                    checkBoxDateZero.Checked =
                        c.SettingFlagCheck(ListExportSetting.SettingFlag.DateZero);


                }
                //年表記
                else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Year)
                {
                    //excel時データ
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                        radioButtonMoji.Checked = true;
                    else
                        radioButtonData.Checked = true;

                    //日付区切り
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.SeparateKanji))
                        radioButtonKanji.Checked = true;
                    else
                        radioButtonSlash.Checked = true;

                    //歴表示　日付書式
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpFull))
                        radioButtonJpKanji.Checked = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpEraNum))
                        radioButtonJPEra.Checked = true;
                    //20210602110209 furukawa ed ////////////////////////


                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot))
                        radioButtonJpShort.Checked = true;
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpNone))
                        radioButtonJpNone.Checked = true;
                    else
                        radioButtonAd.Checked = true;


                }

                //和暦年号
                else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Era)
                {
                    //excel時データ
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                        radioButtonMoji.Checked = true;
                    else
                        radioButtonData.Checked = true;

                    //年号表記
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot))
                        radioButtonJpShort.Checked = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    else if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DateJpEraNum))
                        radioButtonJPEra.Checked = true;
                    //20210602110209 furukawa ed ////////////////////////


                    else
                        radioButtonJpKanji.Checked = true;
                }
                //数字
                else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Integer)
                {
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                        radioButtonMoji.Checked = true;
                    else
                        radioButtonData.Checked = true;
                }

                //小数値
                else if(c.ColumnInfo.ValueType == ListExportSetting.ValueType.Double)
                {
                    if (c.SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                        radioButtonMoji.Checked = true;
                    else
                        radioButtonData.Checked = true;
                }
            }

            //20190501182038 furukawa st ////////////////////////
            //全角フラグ追加

            checkBoxZenkaku.Checked =
                        c.SettingFlagCheck(ListExportSetting.SettingFlag.Zenkaku);
            //20190501182038 furukawa ed ////////////////////////


            coliumnNowSetting = false;
            columnSettingControlsAdjust();
        }

        private void radioButtonExcel_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxExcelBase.Enabled = radioButtonExcel.Checked;
            columnSettingControlsAdjust();
        }


        /// <summary>
        /// 列定義に合わせてセンシティブ制御
        /// </summary>
        private void columnSettingControlsAdjust()
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Null ||
                c.ColumnInfo.ValueType == ListExportSetting.ValueType.Era ||
                c.ColumnInfo.ValueType == ListExportSetting.ValueType.String ||
                c.ColumnInfo.ValueType == ListExportSetting.ValueType.Blank)
            {
                radioButtonData.Enabled = false;
                radioButtonMoji.Checked = true;

                //20190501174335 furukawa st ////////////////////////
                //全角フラグ追加
                checkBoxZenkaku.Enabled = true;
                //20190501174335 furukawa ed ////////////////////////
            }
            else
            {
                radioButtonData.Enabled = true;

                //20190501174335 furukawa st ////////////////////////
                //全角フラグ追加
                checkBoxZenkaku.Checked = false;
                checkBoxZenkaku.Enabled = false;
                //20190501174335 furukawa ed ////////////////////////
            }


            //日付
            if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Date)
            {
                //数値扱いの場合
                if (radioButtonData.Checked)
                {
                    radioButtonSlash.Checked = true;
                    radioButtonSlash.Enabled = true;
                    radioButtonKanji.Enabled = false;
                    radioButtonSepNone.Enabled = false;

                    radioButtonAd.Checked = true;
                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = false;
                    radioButtonJpShort.Enabled = false;
                    radioButtonJpNone.Enabled = false;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = false;
                    //20210602110209 furukawa ed ////////////////////////


                    checkBoxDateZero.Checked = true;
                    checkBoxDateZero.Enabled = false;


                }
                //文字列の場合
                else
                {
                    radioButtonSlash.Enabled = true;
                    radioButtonKanji.Enabled = true;
                    radioButtonSepNone.Enabled = true;

                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = true;
                    radioButtonJpShort.Enabled = true;
                    radioButtonJpNone.Enabled = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = true;
                    //20210602110209 furukawa ed ////////////////////////


                    checkBoxDateZero.Enabled = true;


                }
            }
            //年月
            else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.YearMonth)
            {
                //数値扱いの場合
                if (radioButtonData.Checked)
                {
                    radioButtonSepNone.Checked = true;
                    radioButtonSlash.Enabled = false;
                    radioButtonKanji.Enabled = false;
                    radioButtonSepNone.Enabled = true;

                    if (radioButtonJpKanji.Checked || radioButtonJpShort.Checked)
                        radioButtonAd.Checked = true;

                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = false;
                    radioButtonJpShort.Enabled = false;
                    radioButtonJpNone.Enabled = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = false;
                    //20210602110209 furukawa ed ////////////////////////


                    checkBoxDateZero.Checked = true;
                    checkBoxDateZero.Enabled = false;

                }
                //文字列扱い
                else
                {
                    radioButtonSlash.Enabled = true;
                    radioButtonKanji.Enabled = true;
                    radioButtonSepNone.Enabled = true;

                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = true;
                    radioButtonJpShort.Enabled = true;
                    radioButtonJpNone.Enabled = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = true;
                    //20210602110209 furukawa ed ////////////////////////


                    checkBoxDateZero.Enabled = true;


                }
            }

            else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Year)
            {
                if (radioButtonData.Checked)
                {
                    radioButtonSepNone.Checked = true;
                    radioButtonSlash.Enabled = false;
                    radioButtonKanji.Enabled = false;
                    radioButtonSepNone.Enabled = true;

                    if (radioButtonJpKanji.Checked || radioButtonJpShort.Checked)
                        radioButtonAd.Checked = true;
                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = false;
                    radioButtonJpShort.Enabled = false;
                    radioButtonJpNone.Enabled = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = false;
                    //20210602110209 furukawa ed ////////////////////////


                }
                else
                {
                    radioButtonSlash.Enabled = false;
                    radioButtonKanji.Enabled = true;
                    radioButtonSepNone.Enabled = true;

                    radioButtonAd.Enabled = true;
                    radioButtonJpKanji.Enabled = true;
                    radioButtonJpShort.Enabled = true;
                    radioButtonJpNone.Enabled = true;

                    //20210602110209 furukawa st ////////////////////////
                    //和暦数字書式追加
                    radioButtonJPEra.Enabled = true;
                    //20210602110209 furukawa ed ////////////////////////


                }

                checkBoxDateZero.Checked = false;
                checkBoxDateZero.Enabled = false;

            }
            else if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Era)
            {
                radioButtonSepNone.Checked = true;
                radioButtonSlash.Enabled = false;
                radioButtonKanji.Enabled = false;
                radioButtonSepNone.Enabled = true;

                if(radioButtonAd.Checked || radioButtonJpNone.Checked)
                    radioButtonJpKanji.Checked = true;
                radioButtonAd.Enabled = false;
                radioButtonJpKanji.Enabled = true;
                radioButtonJpShort.Enabled = true;
                radioButtonJpNone.Enabled = false;

                //20210602110209 furukawa st ////////////////////////
                //和暦数字書式追加
                radioButtonJPEra.Enabled = true;
                //20210602110209 furukawa ed ////////////////////////


                checkBoxDateZero.Checked = false;
                checkBoxDateZero.Enabled = false;



            }
            else
            {
                radioButtonSlash.Enabled = false;
                radioButtonKanji.Enabled = false;
                radioButtonSepNone.Enabled = false;

                radioButtonAd.Enabled = false;
                radioButtonJpKanji.Enabled = false;
                radioButtonJpShort.Enabled = false;
                radioButtonJpNone.Enabled = false;

                //20210602110209 furukawa st ////////////////////////
                //和暦数字書式追加
                radioButtonJPEra.Enabled = true;
                //20210602110209 furukawa ed ////////////////////////


                checkBoxDateZero.Checked = false;
                checkBoxDateZero.Enabled = false;
            }

            textBoxOnSample.Text = c.Sample;
        }

        private void textBoxOnHeader_TextChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;
            c.Header = textBoxOnHeader.Text.Trim();
            textBoxOnSample.Text = c.Sample;
        }

        private void textBoxOnWord_TextChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;
            c.AfterWord = textBoxOnWord.Text.Trim();
            textBoxOnSample.Text = c.Sample;
        }

        private void checkBoxDateZero_CheckedChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (checkBoxDateZero.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateZero);
            else
                c.SettingFlagRemove(ListExportSetting.SettingFlag.DateZero);

            columnSettingControlsAdjust();
        }



        //20190501175142 furukawa st ////////////////////////
        //全角チェックボックスイベント
        private void checkBoxZenkaku_CheckedChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (checkBoxZenkaku.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.Zenkaku);
            else
                c.SettingFlagRemove(ListExportSetting.SettingFlag.Zenkaku);

            columnSettingControlsAdjust();
        }


        private void radioButtonData_CheckedChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (radioButtonMoji.Checked)
            {
                c.SettingFlagSet(ListExportSetting.SettingFlag.DataText);
            }
            else
            {
                c.SettingFlagRemove(ListExportSetting.SettingFlag.DataText);
            }
            
            columnSettingControlsAdjust();
        }

        private void radioButtonSepalate_CheckedChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            c.SettingFlagRemove(ListExportSetting.SettingFlag.SeparateSlash |
                ListExportSetting.SettingFlag.SeparateKanji | ListExportSetting.SettingFlag.SeparateNone);
            if (radioButtonKanji.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.SeparateKanji);
            else if (radioButtonSlash.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.SeparateSlash);
            else if (radioButtonSepNone.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.SeparateNone);

            columnSettingControlsAdjust();
        }

        private void radioButtonDateView_CheckedChanged(object sender, EventArgs e)
        {
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            c.SettingFlagRemove(ListExportSetting.SettingFlag.DateAd | ListExportSetting.SettingFlag.DateJpFull |
                ListExportSetting.SettingFlag.DateJpShrot | ListExportSetting.SettingFlag.DateJpNone);
            if (radioButtonAd.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateAd);
            else if (radioButtonJpKanji.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateJpFull);
            else if (radioButtonJpShort.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateJpShrot);
            else if (radioButtonJpNone.Checked)
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateJpNone);

            //20210602110209 furukawa st ////////////////////////
            //和暦数字書式追加
            
            else if (radioButtonJPEra.Checked)
            {
                c.SettingFlagSet(ListExportSetting.SettingFlag.DateJpEraNum);
                //和暦数字を選択したときは区切りを付けない
                radioButtonSlash.Checked = false;
                radioButtonKanji.Checked = false;
                radioButtonSepNone.Checked = true;
            }
            //20210602110209 furukawa ed ////////////////////////


            columnSettingControlsAdjust();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (setting == null) return;

            var res = MessageBox.Show("現在表示中の設定を削除します。元には戻せませんが本当によろしいですか？",
                "削除確認", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;

            if (setting.Delete(db)) this.Close();
            else MessageBox.Show("削除に失敗しました");
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("保存せずに設定を終了しますがよろしいですか？", "キャンセル確認",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res != DialogResult.OK) return;
            this.Close();
        }

        string excelBaseFileName = string.Empty;
        private void checkBoxExcelBase_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxExcelBase.Checked)
            {
                labelCell.Enabled = false;
                textBoxCell.Enabled = false;
                return;
            }
            if (coliumnNowSetting) return;

            using (var f = new OpenFileDialog())
            {
                f.Filter = "Excelファイル|*.xlsx";
                if (f.ShowDialog() != DialogResult.OK)
                {
                    excelBaseFileName = string.Empty;
                    if (setting == null || !setting.ExportType.Contains("ExcelBase"))
                    {
                        checkBoxExcelBase.Checked = false;
                        labelCell.Enabled = false;
                        textBoxCell.Enabled = false;
                    }
                }
                else
                {
                    excelBaseFileName = f.FileName;
                    labelCell.Enabled = true;
                    textBoxCell.Enabled = true;
                    if (string.IsNullOrWhiteSpace(textBoxCell.Text)) textBoxCell.Text = "A1";
                }
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int index = (int)numericUpDown1.Value;
            var app = apps.ElementAt(index - 1);
            ListExportSetting.SetSampleApp(app);
            bsOn.ResetBindings(false);
            
            if (coliumnNowSetting) return;
            var c = (ListExportSetting.Column)bsOn.Current;
            textBoxOnSample.Text = c?.Sample ?? string.Empty;
        }

        private void menu1_Click(object sender, EventArgs e)
        {

            var c = (ListExportSetting.Column)bsOff.Current;
            
            //使用もとをoffではなくonから取る
            c = (ListExportSetting.Column)bsOn.Current;

            //取った項目のコピーを作成
            ListExportSetting.Column col = new ListExportSetting.Column(c.ColumnInfo,c.Header+"コピー",c.Index+1000,c.SettingFlags,c.AfterWord);
            c = col;

            if (c == null) return;

            if (c.ColumnInfo.ValueType == ListExportSetting.ValueType.Blank)
            {
                var info = new ListExportSetting.ColumnInfo(nameof(AppColumns.Blank), "ブランク", ListExportSetting.ValueType.Blank);
                c = new ListExportSetting.Column(info);                
                c.Index = 9999;
                bsOn.Add(c);
            }
            else
            {
                //c.Index = 9999;
                bsOn.Add(c);

                //既に使っている項目のコピーなので未使用フィールドは使ったことにしない
                //bsOff.RemoveCurrent();
            }

            //複製フィールドが分かるように区別しとく
            //c.ColumnInfo.ViewIndex += 1000;
            


            var ons = (List<ListExportSetting.Column>)bsOn.DataSource;
            ons.Sort((x, y) => x.Index.CompareTo(y.Index));
            for (int i = 0; i < ons.Count; i++)
            {
                if (ons[i].Index < 1000) ons[i].Index = i + 1;
                else ons[i].Index += 1;
            }

            //既に使っている項目のコピーなので未使用フィールドは使ったことにしない
            //var offs = (List<ListExportSetting.Column>)bsOff.DataSource;
            //offs.Sort((x, y) => x.ColumnInfo.ViewIndex.CompareTo(y.ColumnInfo.ViewIndex));

            bsOn.ResetBindings(false);
            
            //関係ない状態なので触らない
            //bsOff.ResetBindings(false);


        }

        private void menu2_Click(object sender, EventArgs e)
        {
            var c = (ListExportSetting.Column)bsOn.Current;
            if (c == null) return;

            if (c.ColumnInfo.ValueType != ListExportSetting.ValueType.Blank) bsOff.Add(c);

            if (c.Index < 1000) {
                MessageBox.Show("複製フィールドではありません。削除は「←項目削除」から行ってください", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            bsOn.RemoveCurrent();


            var ons = (List<ListExportSetting.Column>)bsOn.DataSource;
            ons.Sort((x, y) => x.Index.CompareTo(y.Index));
            for (int i = 0; i < ons.Count; i++) ons[i].Index = i + 1;

            //既に使っている項目のコピーなので未使用フィールドは使ったことにしない
            //var offs = (List<ListExportSetting.Column>)bsOff.DataSource;
            //offs.Sort((x, y) => x.ColumnInfo.ViewIndex.CompareTo(y.ColumnInfo.ViewIndex));

            bsOn.ResetBindings(false);

            //関係ない状態なので触らない
            //bsOff.ResetBindings(false);
        }
      

        private void textBoxFind_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter && textBoxFind.Text.Trim()!=string.Empty)
            {
                foreach(DataGridViewRow r in dataGridViewOff.Rows)
                {
                    if (r.Cells[1].Value.ToString().Contains(textBoxFind.Text.Trim()))
                    {
                        dataGridViewOff.FirstDisplayedScrollingRowIndex = r.Index;
                        dataGridViewOff.Rows[r.Index].Selected = true;
                        break;
                    }
                }
                
            }
        }
    }
}
