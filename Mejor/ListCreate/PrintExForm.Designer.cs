﻿namespace Mejor
{
    partial class PrintExForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPrinter = new System.Windows.Forms.ComboBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxApp = new System.Windows.Forms.CheckBox();
            this.checkBoxRef = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxSukashi = new System.Windows.Forms.CheckBox();
            this.checkBoxYm = new System.Windows.Forms.CheckBox();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.buttonPreview = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.checkBoxZokushi = new System.Windows.Forms.CheckBox();
            this.checkBoxDoui = new System.Windows.Forms.CheckBox();
            this.checkBoxDouiura = new System.Windows.Forms.CheckBox();
            this.checkBoxOryo = new System.Windows.Forms.CheckBox();
            this.checkBoxSejutu = new System.Windows.Forms.CheckBox();
            this.gpprint = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxChouki = new System.Windows.Forms.CheckBox();
            this.checkBoxFukugo = new System.Windows.Forms.CheckBox();
            this.checkBoxSoukatsu = new System.Windows.Forms.CheckBox();
            this.checkBoxJotai = new System.Windows.Forms.CheckBox();
            this.checkBoxFusen = new System.Windows.Forms.CheckBox();
            this.gpprint.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.Location = new System.Drawing.Point(131, 223);
            this.cmbPrinter.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(452, 26);
            this.cmbPrinter.TabIndex = 8;
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint.Location = new System.Drawing.Point(256, 294);
            this.buttonPrint.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(112, 35);
            this.buttonPrint.TabIndex = 9;
            this.buttonPrint.Text = "印刷";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(499, 294);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 35);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 228);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "プリンター";
            // 
            // checkBoxApp
            // 
            this.checkBoxApp.AutoSize = true;
            this.checkBoxApp.Checked = true;
            this.checkBoxApp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxApp.Location = new System.Drawing.Point(25, 24);
            this.checkBoxApp.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxApp.Name = "checkBoxApp";
            this.checkBoxApp.Size = new System.Drawing.Size(72, 22);
            this.checkBoxApp.TabIndex = 1;
            this.checkBoxApp.Text = "申請書";
            this.checkBoxApp.UseVisualStyleBackColor = true;
            // 
            // checkBoxRef
            // 
            this.checkBoxRef.AutoSize = true;
            this.checkBoxRef.Location = new System.Drawing.Point(120, 24);
            this.checkBoxRef.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRef.Name = "checkBoxRef";
            this.checkBoxRef.Size = new System.Drawing.Size(102, 22);
            this.checkBoxRef.TabIndex = 2;
            this.checkBoxRef.Text = "参考申請書";
            this.checkBoxRef.UseVisualStyleBackColor = true;
            this.checkBoxRef.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 196);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "参考印刷項目";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // checkBoxSukashi
            // 
            this.checkBoxSukashi.AutoSize = true;
            this.checkBoxSukashi.Checked = true;
            this.checkBoxSukashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSukashi.Enabled = false;
            this.checkBoxSukashi.Location = new System.Drawing.Point(174, 193);
            this.checkBoxSukashi.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxSukashi.Name = "checkBoxSukashi";
            this.checkBoxSukashi.Size = new System.Drawing.Size(156, 22);
            this.checkBoxSukashi.TabIndex = 4;
            this.checkBoxSukashi.Text = "「参考申請書」透かし";
            this.checkBoxSukashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxYm
            // 
            this.checkBoxYm.AutoSize = true;
            this.checkBoxYm.Enabled = false;
            this.checkBoxYm.Location = new System.Drawing.Point(358, 193);
            this.checkBoxYm.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxYm.Name = "checkBoxYm";
            this.checkBoxYm.Size = new System.Drawing.Size(87, 22);
            this.checkBoxYm.TabIndex = 5;
            this.checkBoxYm.Text = "診療年月";
            this.checkBoxYm.UseVisualStyleBackColor = true;
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Enabled = false;
            this.checkBoxTotal.Location = new System.Drawing.Point(474, 193);
            this.checkBoxTotal.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(87, 22);
            this.checkBoxTotal.TabIndex = 6;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            // 
            // buttonPreview
            // 
            this.buttonPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreview.Location = new System.Drawing.Point(378, 294);
            this.buttonPreview.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(112, 35);
            this.buttonPreview.TabIndex = 10;
            this.buttonPreview.Text = "プレビュー";
            this.buttonPreview.UseVisualStyleBackColor = true;
            this.buttonPreview.Click += new System.EventHandler(this.buttonPreview_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.Location = new System.Drawing.Point(25, 294);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(112, 35);
            this.buttonSave.TabIndex = 9;
            this.buttonSave.Text = "一括保存";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // checkBoxZokushi
            // 
            this.checkBoxZokushi.AutoSize = true;
            this.checkBoxZokushi.Location = new System.Drawing.Point(25, 54);
            this.checkBoxZokushi.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxZokushi.Name = "checkBoxZokushi";
            this.checkBoxZokushi.Size = new System.Drawing.Size(87, 22);
            this.checkBoxZokushi.TabIndex = 2;
            this.checkBoxZokushi.Text = "続紙全部";
            this.checkBoxZokushi.UseVisualStyleBackColor = true;
            this.checkBoxZokushi.CheckedChanged += new System.EventHandler(this.checkBoxZokushi_CheckedChanged);
            // 
            // checkBoxDoui
            // 
            this.checkBoxDoui.AutoSize = true;
            this.checkBoxDoui.Location = new System.Drawing.Point(120, 54);
            this.checkBoxDoui.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDoui.Name = "checkBoxDoui";
            this.checkBoxDoui.Size = new System.Drawing.Size(72, 22);
            this.checkBoxDoui.TabIndex = 2;
            this.checkBoxDoui.Text = "同意書";
            this.checkBoxDoui.UseVisualStyleBackColor = true;
            this.checkBoxDoui.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxDouiura
            // 
            this.checkBoxDouiura.AutoSize = true;
            this.checkBoxDouiura.Location = new System.Drawing.Point(234, 54);
            this.checkBoxDouiura.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDouiura.Name = "checkBoxDouiura";
            this.checkBoxDouiura.Size = new System.Drawing.Size(87, 22);
            this.checkBoxDouiura.TabIndex = 2;
            this.checkBoxDouiura.Text = "同意書裏";
            this.checkBoxDouiura.UseVisualStyleBackColor = true;
            this.checkBoxDouiura.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxOryo
            // 
            this.checkBoxOryo.AutoSize = true;
            this.checkBoxOryo.Location = new System.Drawing.Point(327, 54);
            this.checkBoxOryo.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOryo.Name = "checkBoxOryo";
            this.checkBoxOryo.Size = new System.Drawing.Size(102, 22);
            this.checkBoxOryo.TabIndex = 2;
            this.checkBoxOryo.Text = "往療内訳書";
            this.checkBoxOryo.UseVisualStyleBackColor = true;
            this.checkBoxOryo.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxSejutu
            // 
            this.checkBoxSejutu.AutoSize = true;
            this.checkBoxSejutu.Location = new System.Drawing.Point(439, 54);
            this.checkBoxSejutu.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxSejutu.Name = "checkBoxSejutu";
            this.checkBoxSejutu.Size = new System.Drawing.Size(102, 22);
            this.checkBoxSejutu.TabIndex = 2;
            this.checkBoxSejutu.Text = "施術報告書";
            this.checkBoxSejutu.UseVisualStyleBackColor = true;
            this.checkBoxSejutu.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // gpprint
            // 
            this.gpprint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpprint.Controls.Add(this.label2);
            this.gpprint.Controls.Add(this.checkBoxApp);
            this.gpprint.Controls.Add(this.checkBoxRef);
            this.gpprint.Controls.Add(this.checkBoxZokushi);
            this.gpprint.Controls.Add(this.checkBoxChouki);
            this.gpprint.Controls.Add(this.checkBoxFukugo);
            this.gpprint.Controls.Add(this.checkBoxSoukatsu);
            this.gpprint.Controls.Add(this.checkBoxJotai);
            this.gpprint.Controls.Add(this.checkBoxDoui);
            this.gpprint.Controls.Add(this.checkBoxFusen);
            this.gpprint.Controls.Add(this.checkBoxSejutu);
            this.gpprint.Controls.Add(this.checkBoxDouiura);
            this.gpprint.Controls.Add(this.checkBoxOryo);
            this.gpprint.Location = new System.Drawing.Point(25, 12);
            this.gpprint.Name = "gpprint";
            this.gpprint.Size = new System.Drawing.Size(583, 154);
            this.gpprint.TabIndex = 12;
            this.gpprint.TabStop = false;
            this.gpprint.Text = "印刷対象";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(321, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "※入力にて選別されていない保険者は出力できません";
            // 
            // checkBoxChouki
            // 
            this.checkBoxChouki.AutoSize = true;
            this.checkBoxChouki.Location = new System.Drawing.Point(439, 84);
            this.checkBoxChouki.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxChouki.Name = "checkBoxChouki";
            this.checkBoxChouki.Size = new System.Drawing.Size(57, 22);
            this.checkBoxChouki.TabIndex = 2;
            this.checkBoxChouki.Text = "長期";
            this.checkBoxChouki.UseVisualStyleBackColor = true;
            this.checkBoxChouki.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxFukugo
            // 
            this.checkBoxFukugo.AutoSize = true;
            this.checkBoxFukugo.Location = new System.Drawing.Point(327, 84);
            this.checkBoxFukugo.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxFukugo.Name = "checkBoxFukugo";
            this.checkBoxFukugo.Size = new System.Drawing.Size(57, 22);
            this.checkBoxFukugo.TabIndex = 2;
            this.checkBoxFukugo.Text = "複合";
            this.checkBoxFukugo.UseVisualStyleBackColor = true;
            this.checkBoxFukugo.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxSoukatsu
            // 
            this.checkBoxSoukatsu.AutoSize = true;
            this.checkBoxSoukatsu.Location = new System.Drawing.Point(234, 84);
            this.checkBoxSoukatsu.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxSoukatsu.Name = "checkBoxSoukatsu";
            this.checkBoxSoukatsu.Size = new System.Drawing.Size(72, 22);
            this.checkBoxSoukatsu.TabIndex = 2;
            this.checkBoxSoukatsu.Text = "総括表";
            this.checkBoxSoukatsu.UseVisualStyleBackColor = true;
            this.checkBoxSoukatsu.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxJotai
            // 
            this.checkBoxJotai.AutoSize = true;
            this.checkBoxJotai.Location = new System.Drawing.Point(120, 84);
            this.checkBoxJotai.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxJotai.Name = "checkBoxJotai";
            this.checkBoxJotai.Size = new System.Drawing.Size(102, 22);
            this.checkBoxJotai.TabIndex = 2;
            this.checkBoxJotai.Text = "状態記入書";
            this.checkBoxJotai.UseVisualStyleBackColor = true;
            this.checkBoxJotai.CheckedChanged += new System.EventHandler(this.checkBoxRef_CheckedChanged);
            // 
            // checkBoxFusen
            // 
            this.checkBoxFusen.AutoSize = true;
            this.checkBoxFusen.Location = new System.Drawing.Point(439, 24);
            this.checkBoxFusen.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxFusen.Name = "checkBoxFusen";
            this.checkBoxFusen.Size = new System.Drawing.Size(87, 22);
            this.checkBoxFusen.TabIndex = 2;
            this.checkBoxFusen.Text = "返戻付箋";
            this.checkBoxFusen.UseVisualStyleBackColor = true;
            this.checkBoxFusen.CheckedChanged += new System.EventHandler(this.checkBoxFusen_CheckedChanged);
            // 
            // PrintExForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 342);
            this.Controls.Add(this.gpprint);
            this.Controls.Add(this.checkBoxTotal);
            this.Controls.Add(this.checkBoxYm);
            this.Controls.Add(this.checkBoxSukashi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonPreview);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.cmbPrinter);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PrintExForm";
            this.Text = "申請書画像一括印刷";
            this.gpprint.ResumeLayout(false);
            this.gpprint.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPrinter;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxApp;
        private System.Windows.Forms.CheckBox checkBoxRef;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxSukashi;
        private System.Windows.Forms.CheckBox checkBoxYm;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.Button buttonPreview;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox checkBoxZokushi;
        private System.Windows.Forms.CheckBox checkBoxDoui;
        private System.Windows.Forms.CheckBox checkBoxDouiura;
        private System.Windows.Forms.CheckBox checkBoxOryo;
        private System.Windows.Forms.CheckBox checkBoxSejutu;
        private System.Windows.Forms.GroupBox gpprint;
        private System.Windows.Forms.CheckBox checkBoxSoukatsu;
        private System.Windows.Forms.CheckBox checkBoxJotai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxChouki;
        private System.Windows.Forms.CheckBox checkBoxFukugo;
        private System.Windows.Forms.CheckBox checkBoxFusen;
    }
}