﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using NPOI.XSSF.UserModel;
using NPOI.SS.Formula;
using NPOI.SS.Formula.PTG;

namespace Mejor.ListCreate
{
    class ListExport
    {
        public static bool Export(IEnumerable<App> apps, string fileName, ListExportSetting setting)
        {
            int index = 1;
            foreach (var item in apps)
            {
                item.ViewIndex = index++;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.SetMax(apps.Count());

                if (setting.ExportType == "UTF8" || setting.ExportType == "SJIS")
                {
                    return exportCsv(apps, fileName, setting, wf);
                }
                //20210501114248 furukawa st ////////////////////////
                //CSVでない、その他テキストファイルの追加                
                else if (setting.ExportType.Contains("TEXTUTF8") || setting.ExportType.Contains("TEXTSJIS"))
                {
                    return exportText(apps, fileName, setting, wf);
                }
                //20210501114248 furukawa ed ////////////////////////

                else
                {
                    return exportExcel(apps, fileName, setting, wf);
                }
            }
        }

        private static bool exportExcel(IEnumerable<App> apps, string fileName, ListExportSetting setting, WaitForm wf)
        {
            bool newFileBook = setting.ExportType == "Excel";
            XSSFWorkbook book;
            NPOI.SS.UserModel.ISheet sheet;

            int rowIndex = 0;
            int columnIndex = 0;


            try
            {


                if (newFileBook)
                {
                    book = new XSSFWorkbook();
                    sheet = book.CreateSheet("Sheet1");
                }
                else
                {
                    wf.LogPrint("ベースファイルをコピー中です…");
                    var baseName = Directory.GetFiles(Settings.BaseExcelDir, setting.ID.ToString("000000") + "*.xlsx").FirstOrDefault();
                    if (!File.Exists(baseName ?? string.Empty))
                        throw new Exception("ベースに指定されているExcelファイルが見つかりませんでした");
                    File.Copy(baseName, fileName, true);

                    using (var fs = new FileStream(fileName, FileMode.OpenOrCreate))
                        book = new XSSFWorkbook(fs);
                    var cellAdd = setting.ExportType.Split('|')[1];
                    columnIndex = cellAdd[0] - 'A';
                    rowIndex = cellAdd[1] - '1';
                    sheet = book.GetSheetAt(0);
                }

                var ss = setting.Columns;
                var fe = book.GetCreationHelper().CreateFormulaEvaluator();
                var parsingBook = XSSFEvaluationWorkbook.Create(book);

                //ヘッダーの書き込み
                if (setting.HeaderWrite)
                {
                    wf.LogPrint("ヘッダーを書き込み中です…");
                    var row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
                    for (int i = 0; i < ss.Count; i++)
                    {
                        var cell = row.GetCell(i + columnIndex) ?? row.CreateCell(i + columnIndex);
                        cell.SetCellValue(ss[i].Header);
                    }
                    rowIndex++;
                }

                //データ書き込み
                bool firstRow = true;
                wf.LogPrint("データを書き込み中です…");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                foreach (var app in apps)
                {
                    wf.InvokeValue++;
                    try
                    {
                        var row = sheet.GetRow(rowIndex) ?? sheet.CreateRow(rowIndex);
                        if (!firstRow) row.Height = sheet.GetRow(rowIndex - 1).Height;

                        var ac = new AppColumns(app);

                        for (int i = 0; i < ss.Count; i++)
                        {
                            var cell = row.GetCell(i + columnIndex) ?? row.CreateCell(i + columnIndex);
                            var v = ss[i].PropertyInfo.GetValue(ac);

                            //属性(書式)コピー
                            if (!newFileBook)
                            {
                                if (firstRow)
                                {
                                    if (cell.CellType == NPOI.SS.UserModel.CellType.Formula)
                                    {
                                        var formula = cell.CellFormula;
                                        cell.CellFormula = formula;
                                    }
                                }
                                else
                                {
                                    var sCell = sheet.GetRow(rowIndex - 1).GetCell(i + columnIndex);
                                    cell.CellStyle = sCell.CellStyle;

                                    if (sCell.CellType == NPOI.SS.UserModel.CellType.Formula)
                                    {
                                        //数式の時、参照セルを1行ずらして数式コピー
                                        var formula = sCell.CellFormula;
                                        var ptg = FormulaParser.Parse(formula, parsingBook);

                                        foreach (var item in ptg)
                                        {
                                            if (item is RefPtgBase refPtg)
                                            {
                                                if (refPtg.IsRowRelative)
                                                {
                                                    refPtg.Row++;
                                                }
                                            }
                                            else if (item is AreaPtg areaPtg)
                                            {
                                                if (areaPtg.IsFirstRowRelative) areaPtg.FirstRow++;
                                                if (areaPtg.IsLastRowRelative) areaPtg.LastRow++;
                                            }
                                        }

                                        cell.CellFormula = FormulaRenderer.ToFormulaString(parsingBook, ptg);
                                    }
                                }
                            }

                            //値
                            if (ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DataText))
                            {
                                var s = ss[i].CreateString(v);
                                if (!string.IsNullOrWhiteSpace(s))
                                    cell.SetCellValue(ss[i].CreateString(v));
                            }
                            else
                            {
                                if (ss[i].ColumnInfo.ValueType == ListExportSetting.ValueType.Blank)
                                {
                                    continue;
                                }
                                else if (ss[i].ColumnInfo.ValueType == ListExportSetting.ValueType.Year)
                                {
                                    //年の時、特殊操作
                                    if (v is int y)
                                    {
                                        if (ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot) ||
                                            ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpFull) ||
                                            ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpNone))
                                        {
                                            y = DateTimeEx.GetJpYearFromYM(y);
                                            cell.SetCellValue(y);
                                        }
                                        else
                                        {
                                            y = y / 100;
                                            cell.SetCellValue(y);
                                        }
                                    }
                                    else if (v is DateTime dt)
                                    {
                                        if (ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpShrot) ||
                                            ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpFull) ||
                                            ss[i].SettingFlagCheck(ListExportSetting.SettingFlag.DateJpNone))
                                        {
                                            y = DateTimeEx.GetJpYear(dt);
                                            cell.SetCellValue(y);
                                        }
                                        else
                                        {
                                            y = dt.Year / 100;
                                            cell.SetCellValue(y);
                                        }
                                    }
                                    else
                                    {
                                        cell.SetCellValue(v.ToString());
                                    }
                                }
                                else if (ss[i].ColumnInfo.ValueType == ListExportSetting.ValueType.Era)
                                {
                                    cell.SetCellValue(ss[i].CreateString(v));
                                }
                                else
                                {
                                    if (v is int iValue)
                                    {
                                        //20210519181705 furukawa st ////////////////////////
                                        //追加文言を前後どちらかに付与


                                        if (ss[i].AfterWord.Length > 0 && ss[i].AfterWord.Substring(0, 1) == "+") cell.SetCellValue(ss[i].AfterWord.Substring(1) + iValue.ToString());
                                        else
                                        {
                                            //20211027142459 furukawa st ////////////////////////
                                            //追加文言がない場合は従来通り数値扱い

                                            if (ss[i].AfterWord != string.Empty) cell.SetCellValue(iValue.ToString() + ss[i].AfterWord);
                                            else cell.SetCellValue(iValue);
                                        }

                                        //    cell.SetCellValue(iValue.ToString() + ss[i].AfterWord);
                                        //20211027142459 furukawa ed ////////////////////////

                                    }
                                    //      if (v is int iValue) cell.SetCellValue(iValue);
                                    //20210519181705 furukawa ed ////////////////////////

                                    else if (v is DateTime dtValue) { if (!dtValue.IsNullDate()) cell.SetCellValue(dtValue); }
                                    else if (v is double dValue)
                                    {
                                        //20210519181748 furukawa st ////////////////////////
                                        //追加文言を前後どちらかに付与

                                        //20211027142609 furukawa st ////////////////////////
                                        //追加文言がない場合は従来通り日付扱い

                                        if (ss[i].AfterWord.Length > 0 && ss[i].AfterWord.Substring(0, 1) == "+") cell.SetCellValue(ss[i].AfterWord.Substring(1) + dValue.ToString());
                                        else
                                        {
                                            if (ss[i].AfterWord != string.Empty) cell.SetCellValue(dValue.ToString() + ss[i].AfterWord);
                                            else cell.SetCellValue(dValue);
                                        }
                                        //      else cell.SetCellValue(dValue.ToString() + ss[i].AfterWord);
                                        //20211027142609 furukawa ed ////////////////////////          

                                    }
                                    //      else if (v is double dValue) cell.SetCellValue(dValue);
                                    //20210519181748 furukawa ed ////////////////////////


                                    //20210519181900 furukawa st ////////////////////////
                                    //追加文言を前後どちらかに付与

                                    else if (v is string str)
                                    {
                                        if (ss[i].AfterWord.Length > 0 && ss[i].AfterWord.Substring(0, 1) == "+") cell.SetCellValue(ss[i].AfterWord.Substring(1) + str);
                                        else cell.SetCellValue(str + ss[i].AfterWord);

                                    }
                                    else
                                    {
                                        if (ss[i].AfterWord.Length > 0 && ss[i].AfterWord.Substring(0, 1) == "+") cell.SetCellValue(ss[i].AfterWord.Substring(1) + v.ToString());
                                        else cell.SetCellValue(v.ToString() + ss[i].AfterWord);
                                    }

                                    //      else if (v is string str) cell.SetCellValue(str);
                                    //      else cell.SetCellValue(v.ToString());
                                    //20210519181900 furukawa ed ////////////////////////
                                }
                            }
                        }
                        firstRow = false;
                        rowIndex++;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        return false;
                    }
                }

                using (var fs = new FileStream(fileName, FileMode.Create)) book.Write(fs);
                book.Close();
                return true;


            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        private static bool exportCsv(IEnumerable<App> apps, string fileName, ListExportSetting setting, WaitForm wf)
        {
            var enc = setting.ExportType == "UTF8" ? Encoding.UTF8 :
               setting.ExportType == "SJIS" ? Encoding.GetEncoding("Shift_JIS") :
               throw new Exception("エンコード形式が不明です");

            using (var sw = new StreamWriter(fileName, false, enc))
            {
                var ss = setting.Columns;

                //ヘッダーの書き込み
                if (setting.HeaderWrite)
                {
                    wf.LogPrint("ヘッダーを書き込み中です…");
                    var h = string.Join(",", ss.Select(s => s.Header));
                    sw.WriteLine(h);
                }

                //対象プロパティ一覧を抽出
                var t = typeof(AppColumns);
                var ps = t.GetProperties();
                var dic = new Dictionary<string, PropertyInfo>();
                foreach (var item in ps) dic.Add(item.Name, item);

                //データ書き込み
                wf.LogPrint("データを書き込み中です…");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                foreach (var app in apps)
                {
                    wf.InvokeValue++;
                    try
                    {
                        var av = new AppColumns(app);
                        var l = new List<string>();

                        foreach (var item in ss)
                        {
                            var v = item.PropertyInfo.GetValue(av);
                            l.Add(item.CreateString(v));
                        }

                        sw.WriteLine(string.Join(",", l));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        return false;
                    }
                }
                return true;
            }
        }




        /// <summary>
        /// 20210501131426 furukawa CSVでない、その他テキストファイルの作成
        /// </summary>
        /// <param name="apps"></param>
        /// <param name="fileName"></param>
        /// <param name="setting"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool exportText(IEnumerable<App> apps, string fileName, ListExportSetting setting, WaitForm wf)
        {
            var enc = setting.ExportType.Contains("UTF8") ? Encoding.UTF8 :
               setting.ExportType.Contains("SJIS") ? Encoding.GetEncoding("Shift_JIS") :
               throw new Exception("エンコード形式が不明です");

            //区切り文字
            string strSep = string.Empty;
            strSep = setting.ExportType.Substring(8);
            if (strSep == "TAB文字") strSep = "\t";

            using (var sw = new StreamWriter(fileName, false, enc))
            {
                var ss = setting.Columns;

                //ヘッダーの書き込み
                if (setting.HeaderWrite)
                {
                    wf.LogPrint("ヘッダーを書き込み中です…");
                    var h = string.Join(strSep, ss.Select(s => s.Header));
                    sw.WriteLine(h);
                }

                //対象プロパティ一覧を抽出
                var t = typeof(AppColumns);
                var ps = t.GetProperties();
                var dic = new Dictionary<string, PropertyInfo>();
                foreach (var item in ps) dic.Add(item.Name, item);

                //データ書き込み
                wf.LogPrint("データを書き込み中です…");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                foreach (var app in apps)
                {
                    wf.InvokeValue++;
                    try
                    {
                        var av = new AppColumns(app);
                        var l = new List<string>();

                        foreach (var item in ss)
                        {
                            var v = item.PropertyInfo.GetValue(av);
                            l.Add(item.CreateString(v));
                        }

                        sw.WriteLine(string.Join(strSep, l));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
