﻿namespace Mejor.ListCreate
{
    partial class StatusSetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonOuryo = new System.Windows.Forms.RadioButton();
            this.radioButtonHenrei = new System.Windows.Forms.RadioButton();
            this.radioButtonHoryu = new System.Windows.Forms.RadioButton();
            this.radioButtonTel = new System.Windows.Forms.RadioButton();
            this.radioButtonAdd = new System.Windows.Forms.RadioButton();
            this.radioButtonRemove = new System.Windows.Forms.RadioButton();
            this.panelHenrei = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxHenKago = new System.Windows.Forms.CheckBox();
            this.checkBoxHenOther = new System.Windows.Forms.CheckBox();
            this.checkBoxHenWork = new System.Windows.Forms.CheckBox();
            this.checkBoxHenKega = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason = new System.Windows.Forms.CheckBox();
            this.checkBoxHenDays = new System.Windows.Forms.CheckBox();
            this.checkBoxHenJiki = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonPro2 = new System.Windows.Forms.RadioButton();
            this.radioButtonPro1 = new System.Windows.Forms.RadioButton();
            this.radioButtonShiharai = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.radioButtonPro4 = new System.Windows.Forms.RadioButton();
            this.radioButtonPro3 = new System.Windows.Forms.RadioButton();
            this.radioButtonPro5 = new System.Windows.Forms.RadioButton();
            this.panelHenrei.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonOuryo
            // 
            this.radioButtonOuryo.AutoSize = true;
            this.radioButtonOuryo.Location = new System.Drawing.Point(27, 27);
            this.radioButtonOuryo.Name = "radioButtonOuryo";
            this.radioButtonOuryo.Size = new System.Drawing.Size(71, 16);
            this.radioButtonOuryo.TabIndex = 0;
            this.radioButtonOuryo.TabStop = true;
            this.radioButtonOuryo.Text = "往療点検";
            this.radioButtonOuryo.UseVisualStyleBackColor = true;
            // 
            // radioButtonHenrei
            // 
            this.radioButtonHenrei.AutoSize = true;
            this.radioButtonHenrei.Location = new System.Drawing.Point(27, 46);
            this.radioButtonHenrei.Name = "radioButtonHenrei";
            this.radioButtonHenrei.Size = new System.Drawing.Size(47, 16);
            this.radioButtonHenrei.TabIndex = 1;
            this.radioButtonHenrei.TabStop = true;
            this.radioButtonHenrei.Text = "返戻";
            this.radioButtonHenrei.UseVisualStyleBackColor = true;
            this.radioButtonHenrei.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // radioButtonHoryu
            // 
            this.radioButtonHoryu.AutoSize = true;
            this.radioButtonHoryu.Location = new System.Drawing.Point(27, 65);
            this.radioButtonHoryu.Name = "radioButtonHoryu";
            this.radioButtonHoryu.Size = new System.Drawing.Size(71, 16);
            this.radioButtonHoryu.TabIndex = 2;
            this.radioButtonHoryu.TabStop = true;
            this.radioButtonHoryu.Text = "支払保留";
            this.radioButtonHoryu.UseVisualStyleBackColor = true;
            // 
            // radioButtonTel
            // 
            this.radioButtonTel.AutoSize = true;
            this.radioButtonTel.Location = new System.Drawing.Point(27, 84);
            this.radioButtonTel.Name = "radioButtonTel";
            this.radioButtonTel.Size = new System.Drawing.Size(47, 16);
            this.radioButtonTel.TabIndex = 3;
            this.radioButtonTel.TabStop = true;
            this.radioButtonTel.Text = "架電";
            this.radioButtonTel.UseVisualStyleBackColor = true;
            this.radioButtonTel.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // radioButtonAdd
            // 
            this.radioButtonAdd.AutoSize = true;
            this.radioButtonAdd.Location = new System.Drawing.Point(17, 23);
            this.radioButtonAdd.Name = "radioButtonAdd";
            this.radioButtonAdd.Size = new System.Drawing.Size(71, 16);
            this.radioButtonAdd.TabIndex = 0;
            this.radioButtonAdd.TabStop = true;
            this.radioButtonAdd.Text = "対象追加";
            this.radioButtonAdd.UseVisualStyleBackColor = true;
            this.radioButtonAdd.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // radioButtonRemove
            // 
            this.radioButtonRemove.AutoSize = true;
            this.radioButtonRemove.Location = new System.Drawing.Point(108, 23);
            this.radioButtonRemove.Name = "radioButtonRemove";
            this.radioButtonRemove.Size = new System.Drawing.Size(71, 16);
            this.radioButtonRemove.TabIndex = 1;
            this.radioButtonRemove.TabStop = true;
            this.radioButtonRemove.Text = "対象解除";
            this.radioButtonRemove.UseVisualStyleBackColor = true;
            // 
            // panelHenrei
            // 
            this.panelHenrei.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHenrei.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelHenrei.Controls.Add(this.label4);
            this.panelHenrei.Controls.Add(this.checkBoxHenKago);
            this.panelHenrei.Controls.Add(this.checkBoxHenOther);
            this.panelHenrei.Controls.Add(this.checkBoxHenWork);
            this.panelHenrei.Controls.Add(this.checkBoxHenKega);
            this.panelHenrei.Controls.Add(this.checkBoxHenBui);
            this.panelHenrei.Controls.Add(this.checkBoxHenReason);
            this.panelHenrei.Controls.Add(this.checkBoxHenDays);
            this.panelHenrei.Controls.Add(this.checkBoxHenJiki);
            this.panelHenrei.Enabled = false;
            this.panelHenrei.Location = new System.Drawing.Point(13, 48);
            this.panelHenrei.Name = "panelHenrei";
            this.panelHenrei.Size = new System.Drawing.Size(315, 51);
            this.panelHenrei.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "返戻/架電理由";
            // 
            // checkBoxHenKago
            // 
            this.checkBoxHenKago.AutoSize = true;
            this.checkBoxHenKago.Location = new System.Drawing.Point(250, 18);
            this.checkBoxHenKago.Name = "checkBoxHenKago";
            this.checkBoxHenKago.Size = new System.Drawing.Size(48, 16);
            this.checkBoxHenKago.TabIndex = 4;
            this.checkBoxHenKago.Text = "過誤";
            this.checkBoxHenKago.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenOther
            // 
            this.checkBoxHenOther.AutoSize = true;
            this.checkBoxHenOther.Location = new System.Drawing.Point(250, 34);
            this.checkBoxHenOther.Name = "checkBoxHenOther";
            this.checkBoxHenOther.Size = new System.Drawing.Size(55, 16);
            this.checkBoxHenOther.TabIndex = 8;
            this.checkBoxHenOther.Text = "その他";
            this.checkBoxHenOther.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenWork
            // 
            this.checkBoxHenWork.AutoSize = true;
            this.checkBoxHenWork.Location = new System.Drawing.Point(172, 34);
            this.checkBoxHenWork.Name = "checkBoxHenWork";
            this.checkBoxHenWork.Size = new System.Drawing.Size(60, 16);
            this.checkBoxHenWork.TabIndex = 7;
            this.checkBoxHenWork.Text = "勤務中";
            this.checkBoxHenWork.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenKega
            // 
            this.checkBoxHenKega.AutoSize = true;
            this.checkBoxHenKega.Location = new System.Drawing.Point(18, 34);
            this.checkBoxHenKega.Name = "checkBoxHenKega";
            this.checkBoxHenKega.Size = new System.Drawing.Size(68, 16);
            this.checkBoxHenKega.TabIndex = 5;
            this.checkBoxHenKega.Text = "けが以外";
            this.checkBoxHenKega.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui
            // 
            this.checkBoxHenBui.AutoSize = true;
            this.checkBoxHenBui.Location = new System.Drawing.Point(18, 18);
            this.checkBoxHenBui.Name = "checkBoxHenBui";
            this.checkBoxHenBui.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenBui.TabIndex = 1;
            this.checkBoxHenBui.Text = "負傷部位";
            this.checkBoxHenBui.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason
            // 
            this.checkBoxHenReason.AutoSize = true;
            this.checkBoxHenReason.Location = new System.Drawing.Point(94, 18);
            this.checkBoxHenReason.Name = "checkBoxHenReason";
            this.checkBoxHenReason.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenReason.TabIndex = 2;
            this.checkBoxHenReason.Text = "負傷原因";
            this.checkBoxHenReason.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenDays
            // 
            this.checkBoxHenDays.AutoSize = true;
            this.checkBoxHenDays.Location = new System.Drawing.Point(94, 34);
            this.checkBoxHenDays.Name = "checkBoxHenDays";
            this.checkBoxHenDays.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenDays.TabIndex = 6;
            this.checkBoxHenDays.Text = "受療日数";
            this.checkBoxHenDays.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenJiki
            // 
            this.checkBoxHenJiki.AutoSize = true;
            this.checkBoxHenJiki.Location = new System.Drawing.Point(172, 18);
            this.checkBoxHenJiki.Name = "checkBoxHenJiki";
            this.checkBoxHenJiki.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenJiki.TabIndex = 3;
            this.checkBoxHenJiki.Text = "負傷時期";
            this.checkBoxHenJiki.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonPro5);
            this.groupBox1.Controls.Add(this.radioButtonPro4);
            this.groupBox1.Controls.Add(this.radioButtonPro3);
            this.groupBox1.Controls.Add(this.radioButtonOuryo);
            this.groupBox1.Controls.Add(this.radioButtonHenrei);
            this.groupBox1.Controls.Add(this.radioButtonHoryu);
            this.groupBox1.Controls.Add(this.radioButtonPro2);
            this.groupBox1.Controls.Add(this.radioButtonPro1);
            this.groupBox1.Controls.Add(this.radioButtonShiharai);
            this.groupBox1.Controls.Add(this.radioButtonTel);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(245, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "対象種類";
            // 
            // radioButtonPro2
            // 
            this.radioButtonPro2.AutoSize = true;
            this.radioButtonPro2.Location = new System.Drawing.Point(154, 46);
            this.radioButtonPro2.Name = "radioButtonPro2";
            this.radioButtonPro2.Size = new System.Drawing.Size(53, 16);
            this.radioButtonPro2.TabIndex = 6;
            this.radioButtonPro2.TabStop = true;
            this.radioButtonPro2.Text = "処理2";
            this.radioButtonPro2.UseVisualStyleBackColor = true;
            this.radioButtonPro2.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // radioButtonPro1
            // 
            this.radioButtonPro1.AutoSize = true;
            this.radioButtonPro1.Location = new System.Drawing.Point(154, 27);
            this.radioButtonPro1.Name = "radioButtonPro1";
            this.radioButtonPro1.Size = new System.Drawing.Size(53, 16);
            this.radioButtonPro1.TabIndex = 5;
            this.radioButtonPro1.TabStop = true;
            this.radioButtonPro1.Text = "処理1";
            this.radioButtonPro1.UseVisualStyleBackColor = true;
            this.radioButtonPro1.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // radioButtonShiharai
            // 
            this.radioButtonShiharai.AutoSize = true;
            this.radioButtonShiharai.Location = new System.Drawing.Point(27, 103);
            this.radioButtonShiharai.Name = "radioButtonShiharai";
            this.radioButtonShiharai.Size = new System.Drawing.Size(70, 16);
            this.radioButtonShiharai.TabIndex = 4;
            this.radioButtonShiharai.TabStop = true;
            this.radioButtonShiharai.Text = "支払済み";
            this.radioButtonShiharai.UseVisualStyleBackColor = true;
            this.radioButtonShiharai.CheckedChanged += new System.EventHandler(this.radioButtonHenrei_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonAdd);
            this.groupBox2.Controls.Add(this.radioButtonRemove);
            this.groupBox2.Controls.Add(this.panelHenrei);
            this.groupBox2.Location = new System.Drawing.Point(263, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(339, 108);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "管理";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(447, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(527, 139);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "キャンセル";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // radioButtonPro4
            // 
            this.radioButtonPro4.AutoSize = true;
            this.radioButtonPro4.Location = new System.Drawing.Point(154, 84);
            this.radioButtonPro4.Name = "radioButtonPro4";
            this.radioButtonPro4.Size = new System.Drawing.Size(53, 16);
            this.radioButtonPro4.TabIndex = 8;
            this.radioButtonPro4.TabStop = true;
            this.radioButtonPro4.Text = "処理4";
            this.radioButtonPro4.UseVisualStyleBackColor = true;
            // 
            // radioButtonPro3
            // 
            this.radioButtonPro3.AutoSize = true;
            this.radioButtonPro3.Location = new System.Drawing.Point(154, 65);
            this.radioButtonPro3.Name = "radioButtonPro3";
            this.radioButtonPro3.Size = new System.Drawing.Size(53, 16);
            this.radioButtonPro3.TabIndex = 7;
            this.radioButtonPro3.TabStop = true;
            this.radioButtonPro3.Text = "処理3";
            this.radioButtonPro3.UseVisualStyleBackColor = true;
            // 
            // radioButtonPro5
            // 
            this.radioButtonPro5.AutoSize = true;
            this.radioButtonPro5.Location = new System.Drawing.Point(154, 103);
            this.radioButtonPro5.Name = "radioButtonPro5";
            this.radioButtonPro5.Size = new System.Drawing.Size(53, 16);
            this.radioButtonPro5.TabIndex = 9;
            this.radioButtonPro5.TabStop = true;
            this.radioButtonPro5.Text = "処理5";
            this.radioButtonPro5.UseVisualStyleBackColor = true;
            // 
            // StatusSetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 176);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "StatusSetForm";
            this.Text = "フラグ一括管理";
            this.panelHenrei.ResumeLayout(false);
            this.panelHenrei.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonOuryo;
        private System.Windows.Forms.RadioButton radioButtonHenrei;
        private System.Windows.Forms.RadioButton radioButtonHoryu;
        private System.Windows.Forms.RadioButton radioButtonTel;
        private System.Windows.Forms.RadioButton radioButtonAdd;
        private System.Windows.Forms.RadioButton radioButtonRemove;
        private System.Windows.Forms.Panel panelHenrei;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxHenOther;
        private System.Windows.Forms.CheckBox checkBoxHenWork;
        private System.Windows.Forms.CheckBox checkBoxHenKega;
        private System.Windows.Forms.CheckBox checkBoxHenBui;
        private System.Windows.Forms.CheckBox checkBoxHenReason;
        private System.Windows.Forms.CheckBox checkBoxHenDays;
        private System.Windows.Forms.CheckBox checkBoxHenJiki;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton radioButtonPro2;
        private System.Windows.Forms.RadioButton radioButtonPro1;
        private System.Windows.Forms.RadioButton radioButtonShiharai;
        private System.Windows.Forms.CheckBox checkBoxHenKago;
        private System.Windows.Forms.RadioButton radioButtonPro5;
        private System.Windows.Forms.RadioButton radioButtonPro4;
        private System.Windows.Forms.RadioButton radioButtonPro3;
    }
}