﻿namespace Mejor
{
    partial class PriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPriceTotalInput = new System.Windows.Forms.TextBox();
            this.textBoxPriceSeikyuInput = new System.Windows.Forms.TextBox();
            this.buttonPriceTotalCheck = new System.Windows.Forms.Button();
            this.buttonPriceSeikyuCheck = new System.Windows.Forms.Button();
            this.labelPriceTotalResult = new System.Windows.Forms.Label();
            this.labelPriceSeikyuResult = new System.Windows.Forms.Label();
            this.textBoxPriceTotal = new System.Windows.Forms.TextBox();
            this.textBoxPriceSeikyu = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonCsv = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 451);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "合計金額（合算）：";
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(133, 16);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ここに保険者と年月";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 485);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "請求金額（合算）：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(230, 451);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "円";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(230, 485);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "円";
            // 
            // textBoxPriceTotalInput
            // 
            this.textBoxPriceTotalInput.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPriceTotalInput.Location = new System.Drawing.Point(295, 444);
            this.textBoxPriceTotalInput.Name = "textBoxPriceTotalInput";
            this.textBoxPriceTotalInput.Size = new System.Drawing.Size(160, 26);
            this.textBoxPriceTotalInput.TabIndex = 4;
            this.textBoxPriceTotalInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxPriceSeikyuInput
            // 
            this.textBoxPriceSeikyuInput.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPriceSeikyuInput.Location = new System.Drawing.Point(295, 478);
            this.textBoxPriceSeikyuInput.Name = "textBoxPriceSeikyuInput";
            this.textBoxPriceSeikyuInput.Size = new System.Drawing.Size(160, 26);
            this.textBoxPriceSeikyuInput.TabIndex = 10;
            this.textBoxPriceSeikyuInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonPriceTotalCheck
            // 
            this.buttonPriceTotalCheck.Location = new System.Drawing.Point(461, 446);
            this.buttonPriceTotalCheck.Name = "buttonPriceTotalCheck";
            this.buttonPriceTotalCheck.Size = new System.Drawing.Size(61, 23);
            this.buttonPriceTotalCheck.TabIndex = 5;
            this.buttonPriceTotalCheck.Text = "同値確認";
            this.buttonPriceTotalCheck.UseVisualStyleBackColor = true;
            this.buttonPriceTotalCheck.Click += new System.EventHandler(this.buttonPriceTotalCheck_Click);
            // 
            // buttonPriceSeikyuCheck
            // 
            this.buttonPriceSeikyuCheck.Location = new System.Drawing.Point(461, 480);
            this.buttonPriceSeikyuCheck.Name = "buttonPriceSeikyuCheck";
            this.buttonPriceSeikyuCheck.Size = new System.Drawing.Size(61, 23);
            this.buttonPriceSeikyuCheck.TabIndex = 11;
            this.buttonPriceSeikyuCheck.Text = "同値確認";
            this.buttonPriceSeikyuCheck.UseVisualStyleBackColor = true;
            this.buttonPriceSeikyuCheck.Click += new System.EventHandler(this.buttonPriceSeikyuCheck_Click);
            // 
            // labelPriceTotalResult
            // 
            this.labelPriceTotalResult.AutoSize = true;
            this.labelPriceTotalResult.ForeColor = System.Drawing.Color.Red;
            this.labelPriceTotalResult.Location = new System.Drawing.Point(528, 451);
            this.labelPriceTotalResult.Name = "labelPriceTotalResult";
            this.labelPriceTotalResult.Size = new System.Drawing.Size(29, 12);
            this.labelPriceTotalResult.TabIndex = 6;
            this.labelPriceTotalResult.Text = "結果";
            this.labelPriceTotalResult.Visible = false;
            // 
            // labelPriceSeikyuResult
            // 
            this.labelPriceSeikyuResult.AutoSize = true;
            this.labelPriceSeikyuResult.ForeColor = System.Drawing.Color.Red;
            this.labelPriceSeikyuResult.Location = new System.Drawing.Point(528, 485);
            this.labelPriceSeikyuResult.Name = "labelPriceSeikyuResult";
            this.labelPriceSeikyuResult.Size = new System.Drawing.Size(29, 12);
            this.labelPriceSeikyuResult.TabIndex = 12;
            this.labelPriceSeikyuResult.Text = "結果";
            this.labelPriceSeikyuResult.Visible = false;
            // 
            // textBoxPriceTotal
            // 
            this.textBoxPriceTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPriceTotal.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPriceTotal.Location = new System.Drawing.Point(128, 450);
            this.textBoxPriceTotal.Name = "textBoxPriceTotal";
            this.textBoxPriceTotal.ReadOnly = true;
            this.textBoxPriceTotal.Size = new System.Drawing.Size(96, 15);
            this.textBoxPriceTotal.TabIndex = 13;
            this.textBoxPriceTotal.TabStop = false;
            this.textBoxPriceTotal.Text = "---,---";
            this.textBoxPriceTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxPriceSeikyu
            // 
            this.textBoxPriceSeikyu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPriceSeikyu.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPriceSeikyu.Location = new System.Drawing.Point(128, 484);
            this.textBoxPriceSeikyu.Name = "textBoxPriceSeikyu";
            this.textBoxPriceSeikyu.ReadOnly = true;
            this.textBoxPriceSeikyu.Size = new System.Drawing.Size(96, 15);
            this.textBoxPriceSeikyu.TabIndex = 14;
            this.textBoxPriceSeikyu.TabStop = false;
            this.textBoxPriceSeikyu.Text = "---,---";
            this.textBoxPriceSeikyu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Location = new System.Drawing.Point(8, 37);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(572, 392);
            this.dataGridView1.TabIndex = 15;
            // 
            // buttonCsv
            // 
            this.buttonCsv.Location = new System.Drawing.Point(505, 8);
            this.buttonCsv.Name = "buttonCsv";
            this.buttonCsv.Size = new System.Drawing.Size(75, 23);
            this.buttonCsv.TabIndex = 16;
            this.buttonCsv.Text = "CSV";
            this.buttonCsv.UseVisualStyleBackColor = true;
            this.buttonCsv.Click += new System.EventHandler(this.buttonCsv_Click);
            // 
            // PriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 522);
            this.Controls.Add(this.buttonCsv);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBoxPriceSeikyu);
            this.Controls.Add(this.textBoxPriceTotal);
            this.Controls.Add(this.labelPriceSeikyuResult);
            this.Controls.Add(this.labelPriceTotalResult);
            this.Controls.Add(this.buttonPriceSeikyuCheck);
            this.Controls.Add(this.buttonPriceTotalCheck);
            this.Controls.Add(this.textBoxPriceSeikyuInput);
            this.Controls.Add(this.textBoxPriceTotalInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.label1);
            this.Name = "PriceForm";
            this.Text = "金額確認";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPriceTotalInput;
        private System.Windows.Forms.TextBox textBoxPriceSeikyuInput;
        private System.Windows.Forms.Button buttonPriceTotalCheck;
        private System.Windows.Forms.Button buttonPriceSeikyuCheck;
        private System.Windows.Forms.Label labelPriceTotalResult;
        private System.Windows.Forms.Label labelPriceSeikyuResult;
        private System.Windows.Forms.TextBox textBoxPriceTotal;
        private System.Windows.Forms.TextBox textBoxPriceSeikyu;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonCsv;
    }
}