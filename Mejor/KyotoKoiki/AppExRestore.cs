﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.KyotoKoiki
{
    /// <summary>
    /// 単価情報等復元のためのクラス
    /// </summary>
    [DB.DbAttribute.DifferentTableName("appex")]
    class AppExRestore
    {
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; private set; }
        public int ShokenPrice { get; set; }

        public int hariCost { get; set; }
        public int hariCount { get; set; }
        public int hariDenCost { get; set; }
        public int hariDenCount { get; set; }
        public int kyuCost { get; set; }
        public int kyuCount { get; set; }
        public int kyuDenCost { get; set; }
        public int kyuDenCount { get; set; }
        public int heiCost { get; set; }
        public int heiCount { get; set; }
        public int heiDenCost { get; set; }
        public int heiDenCount { get; set; }
        public int denCost { get; set; }
        public int denCount { get; set; }

        public int PartCost { get; set; }
        public int PartCount { get; set; }
        public int PartCountCount { get; set; }
        public int BodyCost { get; set; }
        public int BodyCount { get; set; }
        public int RightUpperCost { get; set; }
        public int RightUpperCount { get; set; }
        public int LeftUpperCost { get; set; }
        public int LeftUpperCount { get; set; }
        public int RightLowerCost { get; set; }
        public int RightLowerCount { get; set; }
        public int LeftLowerCost { get; set; }
        public int LeftLowerCount { get; set; }
        public int ToshuCost { get; set; }
        public int ToshuPartCount { get; set; }
        public int ToshuCount { get; set; }
        public int WarmCost { get; set; }
        public int WarmCount { get; set; }
        public int EleWarmCost { get; set; }
        public int EleWarmCount { get; set; }

        public DateTime DouiDt { get; set; } = DateTime.MinValue;

        public static void GetFromCSV()
        {
            var fileName = string.Empty;
            using (var f = new System.Windows.Forms.OpenFileDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }

                var dir = new Dictionary<string, string[]>();
            var sql = "SELECT aid, numbering, hnum FROM application WHERE cym=201704;";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                var aids = cmd.TryExecuteReaderList();

                foreach (var item in aids)
                {
                    if (string.IsNullOrEmpty((string)item[1])) continue;
                    dir.Add("2904" + (string)item[1], new[] { ((int)item[0]).ToString(), (string)item[2] });
                }
            }

            int convert(string str)
            {
                int.TryParse(str, out int c);
                return c;
            }


            using (var sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
            using (var wr =new System.IO.StreamWriter("restore.log",false, Encoding.UTF8))
            {
                sr.ReadLine();

                using (var tran = DB.Main.CreateTransaction())
                {
                    while (sr.Peek() > 0)
                    {
                        var r = new AppExRestore();

                        var ss = sr.ReadLine().Split(',');
                        var numbering = ss[0];
                        var hiho = ss[2];
                        var ym = int.Parse(ss[6]);

                        if (!dir.ContainsKey(numbering) || dir[numbering][1] != hiho)
                        {
                            wr.WriteLine(numbering);
                            //System.Windows.Forms.MessageBox.Show(numbering);
                            continue;
                        }

                        r.AID = int.Parse(dir[numbering][0]);

                        //初検料
                        r.ShokenPrice = convert(ss[13]);

                        //針単価	針回数	灸単価	灸回数	針灸併用単価	鍼灸併用回数	電療単価	電療回数	針電療単価	針電療回数	灸電療単価	灸電療回数	針灸併用電療単価	鍼灸併用電療回数
                        r.hariCost = convert(ss[14]);
                        r.hariCount = convert(ss[15]);
                        r.hariDenCost = convert(ss[22]);
                        r.hariDenCount = convert(ss[23]);
                        r.kyuCost = convert(ss[16]);
                        r.kyuCount = convert(ss[17]);
                        r.kyuDenCost = convert(ss[24]);
                        r.kyuDenCount = convert(ss[25]);
                        r.heiCost = convert(ss[18]);
                        r.heiCount = convert(ss[19]);
                        r.heiDenCost = convert(ss[26]);
                        r.heiDenCount = convert(ss[27]);
                        r.denCost = convert(ss[20]);
                        r.denCount = convert(ss[21]);

                        //あんま局所単価	あんま局所数	あんま局所回数	躯幹単価	躯幹回数	右上肢単価	右上肢回数	左上肢単価	左上肢回数	右下肢単価	右下肢回数	左下肢単価	左下肢回数
                        r.PartCost = convert(ss[28]);
                        r.PartCount = convert(ss[29]);
                        r.PartCountCount = convert(ss[30]);
                        r.BodyCost = convert(ss[31]);
                        r.BodyCount = convert(ss[32]);
                        r.RightUpperCost = convert(ss[33]);
                        r.RightUpperCount = convert(ss[34]);
                        r.LeftUpperCost = convert(ss[35]);
                        r.LeftUpperCount = convert(ss[36]);
                        r.RightLowerCost = convert(ss[37]);
                        r.RightLowerCount = convert(ss[38]);
                        r.LeftLowerCost = convert(ss[39]);
                        r.LeftLowerCount = convert(ss[40]);

                        //変形徒手単価	変形徒手数	変形徒手回数	温庵単価	温庵回数	温庵電気単価	温庵電気回数
                        r.ToshuCost = convert(ss[41]);
                        r.ToshuPartCount = convert(ss[42]);
                        r.ToshuCount = convert(ss[43]);
                        r.WarmCost = convert(ss[44]);
                        r.WarmCount = convert(ss[45]);
                        r.EleWarmCost = convert(ss[46]);
                        r.EleWarmCount = convert(ss[47]);

                        DateTime dt;
                        if (DateTime.TryParse(ss[64], out dt)) r.DouiDt = dt;
                        else r.DouiDt = DateTimeEx.DateTimeNull;

                        if (!DB.Main.Update(r, tran))
                            return;
                    }

                    tran.Commit();
                }
            }
        }
    }
}
