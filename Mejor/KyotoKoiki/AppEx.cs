﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.KyotoKoiki
{
    class AppEx
    {
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; private set; }
        public int ShokenPrice { get; set; }

        public int hariCost { get; set; }
        public int hariCount { get; set; }
        public int hariDenCost { get; set; }
        public int hariDenCount { get; set; }
        public int kyuCost { get; set; }
        public int kyuCount { get; set; }
        public int kyuDenCost { get; set; }
        public int kyuDenCount { get; set; }
        public int heiCost { get; set; }
        public int heiCount { get; set; }
        public int heiDenCost { get; set; }
        public int heiDenCount { get; set; }
        public int denCost { get; set; }
        public int denCount { get; set; }

        public int PartCost { get; set; }
        public int PartCount { get; set; }
        public int PartCountCount { get; set; }
        public int BodyCost { get; set; }
        public int BodyCount { get; set; }
        public int RightUpperCost { get; set; }
        public int RightUpperCount { get; set; }
        public int LeftUpperCost { get; set; }
        public int LeftUpperCount { get; set; }
        public int RightLowerCost { get; set; }
        public int RightLowerCount { get; set; }
        public int LeftLowerCost { get; set; }
        public int LeftLowerCount { get; set; }
        public int ToshuCost { get; set; }
        public int ToshuPartCount { get; set; }
        public int ToshuCount { get; set; }
        public int WarmCost { get; set; }
        public int WarmCount { get; set; }
        public int EleWarmCost { get; set; }
        public int EleWarmCount { get; set; }

        public DateTime DouiDt { get; set; } = DateTime.MinValue;

        public int Visit1Dis { get; set; }
        public int Visit1Cost { get; set; }
        public int Visit1Count { get; set; }
        public int Visit2Dis { get; set; }
        public int Visit2Cost { get; set; }
        public int Visit2Count { get; set; }
        public int Visit3Dis { get; set; }
        public int Visit3Cost { get; set; }
        public int Visit3Count { get; set; }
        public int Visit4Dis { get; set; }
        public int Visit4Cost { get; set; }
        public int Visit4Count { get; set; }
        public bool VisitAnbun { get; set; } = false;
        public bool VisitOver5 { get; set; } = false;

        private AppEx() { }
        public AppEx(int aid) => AID = aid;

        public static AppEx Select(int aid)
        {
            return DB.Main.Select<AppEx>($"aid={aid}").FirstOrDefault();
        }

        public static Dictionary<int, AppEx> GetAppExDic(int cym)
        {
            var sql = "SELECT appex.* FROM appex, application " +
                $"WHERE application.aid=appex.aid AND application.cym={cym};";

            var l = DB.Main.Query<AppEx>(sql);

            var dic = new Dictionary<int, AppEx>();
            l.ToList().ForEach(x => dic.Add(x.AID, x));
            return dic;
        }

        private bool exists(int aid)
        {
            return DB.Main.Query<bool>($"SELECT EXISTS(SELECT aid FROM appex WHERE aid={aid})").First();
        }

        private bool insert(DB.Transaction tran)
        {
            return DB.Main.Insert(this, tran);
        }

        private bool update(DB.Transaction tran)
        {
            return DB.Main.Update(this, tran);
        }

        public bool Upsert(DB.Transaction tran)
        {
            var b = exists(AID);
            if (b) return update(tran);
            return insert(tran);
        }
    }
}
