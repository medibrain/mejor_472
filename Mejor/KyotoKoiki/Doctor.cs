﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace Mejor.KyotoKoiki
{
    class Doctor
    {
        public enum PAY_TYPE { Null = 0, 個人 = 1, 個人別送 = 2, 団体 = 3 }

        [DB.DbAttribute.PrimaryKey]
        public string Code { get; private set; }
        public string Name { get; private set; }
        public string GroupName { get; private set; }
        public PAY_TYPE PayType { get; private set; }

        /// <summary>
        /// コードをキーとするDoctor一覧を取得します
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, Doctor> GetDoctorDic()
        {
            var d = new Dictionary<string, Doctor>();
            DB.Main.SelectAll<Doctor>().ToList().ForEach(x => d.Add(x.Code, x));
            return d;
        }

        public static bool Import()
        {
            var fn = string.Empty;

            using (var f = new OpenFileDialog())
            {
                f.Title = "施術師マスター更新";
                f.Filter = "施術師一覧Excelファイル|*施術師CD及び送付先一覧*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fn = f.FileName;
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            //現在の一覧取得
            var hs = new HashSet<string>();
            DB.Main.Query<string>("SELECT code FROM doctor;").ToList().ForEach(s => hs.Add(s));

            try
            {
                using (var tran = DB.Main.CreateTransaction())
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open))
                {
                    var book = new XSSFWorkbook(fs);

                    ISheet sheet = null;
                    for (int i = 0; i < book.NumberOfSheets; i++)
                    {
                        var s = book.GetSheetAt(i);
                        if (s.SheetName.Contains("最新医療機関抜粋"))
                        {
                            sheet = s;
                            break;
                        }
                    }

                    if (sheet == null)
                    {
                        MessageBox.Show("指定されたファイル内に最新医療機関抜粋のシートが見つかりませんでした");
                        return false;
                    }

                    var rowCount = sheet.LastRowNum;
                    wf.SetMax(rowCount + 1);
                    wf.BarStyle = ProgressBarStyle.Continuous;

                    //1行目はヘッダ
                    for (int i = 1; i < rowCount + 1; i++)
                    {
                        wf.InvokeValue++;
                        try
                        {
                            var row = sheet.GetRow(i);
                            if (row.Cells[0].CellType != CellType.Numeric) continue;

                            var dr = new Doctor();
                            var pt = row.GetCell(7).StringCellValue;
                            dr.Code = row.GetCell(0).NumericCellValue.ToString();
                            dr.Name = row.GetCell(2).StringCellValue;
                            dr.GroupName = row.GetCell(6)?.StringCellValue ?? string.Empty;
                            dr.PayType = pt == "個人" ? PAY_TYPE.個人 :
                                pt == "個人（別送）" ? PAY_TYPE.個人別送 :
                                pt == "団体" ? PAY_TYPE.団体 :
                                throw new Exception("支払先区分が判別できません");

                            //DB更新
                            if (hs.Add(dr.Code))
                            {
                                if (!DB.Main.Insert(dr, tran))
                                    throw new Exception("登録に失敗しました");
                            }
                            else
                            {
                                if (!DB.Main.Update(dr, tran))
                                    throw new Exception("更新に失敗しました");
                            }
                        }
                        catch (Exception ex)
                        {
                            var res = MessageBox.Show($"更新中、{i}行目でエラーが発生しました。" +
                                $"\r\n{ex.Message}\r\n" +
                                $"この行を無視して、続きを登録しますか？" +
                                $"\r\n\r\nいいえを選択すると更新を中止します。", "",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (res == DialogResult.No) return false;
                            continue;
                        }
                    }
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }
    }
}
