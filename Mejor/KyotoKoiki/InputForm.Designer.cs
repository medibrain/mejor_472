﻿using System;

namespace Mejor.KyotoKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelCommon = new System.Windows.Forms.Panel();
            this.panelAppData = new System.Windows.Forms.Panel();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxFirstM = new Mejor.VerifyBox();
            this.verifyBoxFirstD = new Mejor.VerifyBox();
            this.lblDrCode = new System.Windows.Forms.Label();
            this.verifyBoxTourokuNum = new Mejor.VerifyBox();
            this.verifyBoxFirstNengo = new Mejor.VerifyBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.verifyBoxFirstY = new Mejor.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.label28 = new System.Windows.Forms.Label();
            this.verifyBoxFinish = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxStart = new Mejor.VerifyBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panelDouiDates = new System.Windows.Forms.Panel();
            this.verifyBoxDouiNengo3 = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxDouiNengo2 = new Mejor.VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.verifyBoxDouiD3 = new Mejor.VerifyBox();
            this.verifyBoxDouiM3 = new Mejor.VerifyBox();
            this.verifyBoxDouiY3 = new Mejor.VerifyBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxDouiD2 = new Mejor.VerifyBox();
            this.verifyBoxDouiM2 = new Mejor.VerifyBox();
            this.verifyBoxDouiY2 = new Mejor.VerifyBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.panelApp = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.verifyBoxInsNumHeader = new Mejor.VerifyBox();
            this.verifyBoxInsNum = new Mejor.VerifyBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.verifyBoxDrNum = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.label16 = new System.Windows.Forms.Label();
            this.verifyBoxPersonName = new Mejor.VerifyBox();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxNum = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelDoui = new System.Windows.Forms.Panel();
            this.labelDouiFusho = new System.Windows.Forms.Label();
            this.verifyBoxDouiFusho4 = new Mejor.VerifyBox();
            this.verifyBoxDouiFusho3 = new Mejor.VerifyBox();
            this.verifyBoxDouiFusho2 = new Mejor.VerifyBox();
            this.verifyBoxDouiFusho1 = new Mejor.VerifyBox();
            this.verifyCheckBoxDouiVisit = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxLeftLower = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxRightLower = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxLeftUpper = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxRightUpper = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxAnmaAll = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxBody = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxDouiToshu = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxDouiMasserge = new Mejor.VerifyCheckBox();
            this.labelDouiType = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.verifyBoxDouiD = new Mejor.VerifyBox();
            this.verifyBoxDouiM = new Mejor.VerifyBox();
            this.verifyBoxDouiY = new Mejor.VerifyBox();
            this.label50 = new System.Windows.Forms.Label();
            this.labelDouiVisit = new System.Windows.Forms.Label();
            this.labelDouiBui = new System.Windows.Forms.Label();
            this.labelDouiFushoInfo = new System.Windows.Forms.Label();
            this.panelFutter = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelCommon.SuspendLayout();
            this.panelAppData.SuspendLayout();
            this.panelDouiDates.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.panelApp.SuspendLayout();
            this.panelDoui.SuspendLayout();
            this.panelFutter.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(831, 2);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 0;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(117, 34);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(165, 34);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(307, 5);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(79, 13);
            this.labelHnum.TabIndex = 13;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(55, 34);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(31, 13);
            this.labelH.TabIndex = 1;
            this.labelH.Text = "和暦";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(120, 759);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 781);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 781);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 754);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 754);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 754);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-2236, 754);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 781);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panelCommon);
            this.panelRight.Controls.Add(this.panelHeader);
            this.panelRight.Controls.Add(this.panelFutter);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 781);
            this.panelRight.TabIndex = 0;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 74);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 136);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1020, 583);
            this.scrollPictureControl1.TabIndex = 1;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelCommon
            // 
            this.panelCommon.Controls.Add(this.panelAppData);
            this.panelCommon.Controls.Add(this.panelDouiDates);
            this.panelCommon.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCommon.Location = new System.Drawing.Point(0, 657);
            this.panelCommon.Name = "panelCommon";
            this.panelCommon.Size = new System.Drawing.Size(1020, 96);
            this.panelCommon.TabIndex = 2;
            // 
            // panelAppData
            // 
            this.panelAppData.Controls.Add(this.checkBoxVisitKasan);
            this.panelAppData.Controls.Add(this.checkBoxVisit);
            this.panelAppData.Controls.Add(this.verifyBoxFirstM);
            this.panelAppData.Controls.Add(this.verifyBoxFirstD);
            this.panelAppData.Controls.Add(this.lblDrCode);
            this.panelAppData.Controls.Add(this.verifyBoxTourokuNum);
            this.panelAppData.Controls.Add(this.verifyBoxFirstNengo);
            this.panelAppData.Controls.Add(this.label21);
            this.panelAppData.Controls.Add(this.label25);
            this.panelAppData.Controls.Add(this.label29);
            this.panelAppData.Controls.Add(this.verifyBoxCharge);
            this.panelAppData.Controls.Add(this.verifyBoxTotal);
            this.panelAppData.Controls.Add(this.label8);
            this.panelAppData.Controls.Add(this.verifyBoxFirstY);
            this.panelAppData.Controls.Add(this.label27);
            this.panelAppData.Controls.Add(this.labelCharge);
            this.panelAppData.Controls.Add(this.verifyBoxDays);
            this.panelAppData.Controls.Add(this.label28);
            this.panelAppData.Controls.Add(this.verifyBoxFinish);
            this.panelAppData.Controls.Add(this.labelTotal);
            this.panelAppData.Controls.Add(this.labelDays);
            this.panelAppData.Controls.Add(this.verifyBoxStart);
            this.panelAppData.Controls.Add(this.label12);
            this.panelAppData.Controls.Add(this.label13);
            this.panelAppData.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelAppData.Location = new System.Drawing.Point(0, 0);
            this.panelAppData.Name = "panelAppData";
            this.panelAppData.Size = new System.Drawing.Size(720, 96);
            this.panelAppData.TabIndex = 38;
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(441, 17);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisitKasan.TabIndex = 25;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.Enter += new System.EventHandler(this.item_Enter);
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(351, 17);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisit.TabIndex = 20;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxFirstM
            // 
            this.verifyBoxFirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstM.Location = new System.Drawing.Point(116, 18);
            this.verifyBoxFirstM.Name = "verifyBoxFirstM";
            this.verifyBoxFirstM.NewLine = false;
            this.verifyBoxFirstM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFirstM.TabIndex = 5;
            this.verifyBoxFirstM.TextV = "";
            // 
            // verifyBoxFirstD
            // 
            this.verifyBoxFirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstD.Location = new System.Drawing.Point(161, 18);
            this.verifyBoxFirstD.Name = "verifyBoxFirstD";
            this.verifyBoxFirstD.NewLine = false;
            this.verifyBoxFirstD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFirstD.TabIndex = 7;
            this.verifyBoxFirstD.TextV = "";
            // 
            // lblDrCode
            // 
            this.lblDrCode.AutoSize = true;
            this.lblDrCode.Location = new System.Drawing.Point(325, 54);
            this.lblDrCode.Name = "lblDrCode";
            this.lblDrCode.Size = new System.Drawing.Size(79, 13);
            this.lblDrCode.TabIndex = 121;
            this.lblDrCode.Text = "登録記号番号";
            // 
            // verifyBoxTourokuNum
            // 
            this.verifyBoxTourokuNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTourokuNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTourokuNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTourokuNum.Location = new System.Drawing.Point(410, 49);
            this.verifyBoxTourokuNum.Name = "verifyBoxTourokuNum";
            this.verifyBoxTourokuNum.NewLine = false;
            this.verifyBoxTourokuNum.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxTourokuNum.TabIndex = 60;
            this.verifyBoxTourokuNum.TextV = "";
            // 
            // verifyBoxFirstNengo
            // 
            this.verifyBoxFirstNengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstNengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstNengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstNengo.Location = new System.Drawing.Point(37, 18);
            this.verifyBoxFirstNengo.MaxLength = 1;
            this.verifyBoxFirstNengo.Name = "verifyBoxFirstNengo";
            this.verifyBoxFirstNengo.NewLine = false;
            this.verifyBoxFirstNengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFirstNengo.TabIndex = 2;
            this.verifyBoxFirstNengo.TextV = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label21.Location = new System.Drawing.Point(3, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 26);
            this.label21.TabIndex = 39;
            this.label21.Text = "平:４\r\n令:５";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(34, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 40;
            this.label25.Text = "年号";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(69, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "初検年月日";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(628, 18);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxCharge.TabIndex = 37;
            this.verifyBoxCharge.TextV = "";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(534, 18);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxTotal.TabIndex = 35;
            this.verifyBoxTotal.TextV = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(142, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "月";
            // 
            // verifyBoxFirstY
            // 
            this.verifyBoxFirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstY.Location = new System.Drawing.Point(71, 18);
            this.verifyBoxFirstY.Name = "verifyBoxFirstY";
            this.verifyBoxFirstY.NewLine = false;
            this.verifyBoxFirstY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFirstY.TabIndex = 3;
            this.verifyBoxFirstY.TextV = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(97, 30);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "年";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(626, 3);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(55, 13);
            this.labelCharge.TabIndex = 36;
            this.labelCharge.Text = "請求金額";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(312, 18);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxDays.TabIndex = 14;
            this.verifyBoxDays.TextV = "";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(187, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(19, 13);
            this.label28.TabIndex = 8;
            this.label28.Text = "日";
            // 
            // verifyBoxFinish
            // 
            this.verifyBoxFinish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFinish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFinish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFinish.Location = new System.Drawing.Point(263, 18);
            this.verifyBoxFinish.Name = "verifyBoxFinish";
            this.verifyBoxFinish.NewLine = false;
            this.verifyBoxFinish.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFinish.TabIndex = 12;
            this.verifyBoxFinish.TextV = "";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(534, 3);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(55, 13);
            this.labelTotal.TabIndex = 34;
            this.labelTotal.Text = "合計金額";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(312, 3);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(43, 13);
            this.labelDays.TabIndex = 13;
            this.labelDays.Text = "実日数";
            // 
            // verifyBoxStart
            // 
            this.verifyBoxStart.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStart.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStart.Location = new System.Drawing.Point(215, 18);
            this.verifyBoxStart.Name = "verifyBoxStart";
            this.verifyBoxStart.NewLine = false;
            this.verifyBoxStart.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxStart.TabIndex = 10;
            this.verifyBoxStart.TextV = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(213, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "開始日";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(261, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "終了日";
            // 
            // panelDouiDates
            // 
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiNengo3);
            this.panelDouiDates.Controls.Add(this.label26);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiNengo2);
            this.panelDouiDates.Controls.Add(this.label24);
            this.panelDouiDates.Controls.Add(this.label22);
            this.panelDouiDates.Controls.Add(this.label23);
            this.panelDouiDates.Controls.Add(this.label14);
            this.panelDouiDates.Controls.Add(this.label15);
            this.panelDouiDates.Controls.Add(this.label19);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiD3);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiM3);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiY3);
            this.panelDouiDates.Controls.Add(this.label20);
            this.panelDouiDates.Controls.Add(this.label2);
            this.panelDouiDates.Controls.Add(this.label6);
            this.panelDouiDates.Controls.Add(this.label9);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiD2);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiM2);
            this.panelDouiDates.Controls.Add(this.verifyBoxDouiY2);
            this.panelDouiDates.Controls.Add(this.label11);
            this.panelDouiDates.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelDouiDates.Location = new System.Drawing.Point(563, 0);
            this.panelDouiDates.Name = "panelDouiDates";
            this.panelDouiDates.Size = new System.Drawing.Size(457, 96);
            this.panelDouiDates.TabIndex = 39;
            // 
            // verifyBoxDouiNengo3
            // 
            this.verifyBoxDouiNengo3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiNengo3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiNengo3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiNengo3.Location = new System.Drawing.Point(233, 20);
            this.verifyBoxDouiNengo3.MaxLength = 1;
            this.verifyBoxDouiNengo3.Name = "verifyBoxDouiNengo3";
            this.verifyBoxDouiNengo3.NewLine = false;
            this.verifyBoxDouiNengo3.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiNengo3.TabIndex = 30;
            this.verifyBoxDouiNengo3.TextV = "";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label26.Location = new System.Drawing.Point(202, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(30, 26);
            this.label26.TabIndex = 42;
            this.label26.Text = "平:４\r\n令:５";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // verifyBoxDouiNengo2
            // 
            this.verifyBoxDouiNengo2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiNengo2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiNengo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiNengo2.Location = new System.Drawing.Point(40, 20);
            this.verifyBoxDouiNengo2.MaxLength = 1;
            this.verifyBoxDouiNengo2.Name = "verifyBoxDouiNengo2";
            this.verifyBoxDouiNengo2.NewLine = false;
            this.verifyBoxDouiNengo2.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiNengo2.TabIndex = 2;
            this.verifyBoxDouiNengo2.TextV = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(230, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "年号";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label22.Location = new System.Drawing.Point(6, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 26);
            this.label22.TabIndex = 42;
            this.label22.Text = "平:４\r\n令:５";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(37, 4);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "年号";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(334, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "月";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(291, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "年";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(377, 31);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "日";
            // 
            // verifyBoxDouiD3
            // 
            this.verifyBoxDouiD3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiD3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiD3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiD3.Location = new System.Drawing.Point(351, 20);
            this.verifyBoxDouiD3.Name = "verifyBoxDouiD3";
            this.verifyBoxDouiD3.NewLine = false;
            this.verifyBoxDouiD3.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiD3.TabIndex = 36;
            this.verifyBoxDouiD3.TextV = "";
            // 
            // verifyBoxDouiM3
            // 
            this.verifyBoxDouiM3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiM3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiM3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiM3.Location = new System.Drawing.Point(308, 20);
            this.verifyBoxDouiM3.Name = "verifyBoxDouiM3";
            this.verifyBoxDouiM3.NewLine = false;
            this.verifyBoxDouiM3.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiM3.TabIndex = 34;
            this.verifyBoxDouiM3.TextV = "";
            // 
            // verifyBoxDouiY3
            // 
            this.verifyBoxDouiY3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiY3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiY3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiY3.Location = new System.Drawing.Point(265, 20);
            this.verifyBoxDouiY3.Name = "verifyBoxDouiY3";
            this.verifyBoxDouiY3.NewLine = false;
            this.verifyBoxDouiY3.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiY3.TabIndex = 32;
            this.verifyBoxDouiY3.TextV = "";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(263, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "同意年月日3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(141, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "月";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "年";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(184, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "日";
            // 
            // verifyBoxDouiD2
            // 
            this.verifyBoxDouiD2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiD2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiD2.Location = new System.Drawing.Point(158, 20);
            this.verifyBoxDouiD2.Name = "verifyBoxDouiD2";
            this.verifyBoxDouiD2.NewLine = false;
            this.verifyBoxDouiD2.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiD2.TabIndex = 29;
            this.verifyBoxDouiD2.TextV = "";
            // 
            // verifyBoxDouiM2
            // 
            this.verifyBoxDouiM2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiM2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiM2.Location = new System.Drawing.Point(115, 20);
            this.verifyBoxDouiM2.Name = "verifyBoxDouiM2";
            this.verifyBoxDouiM2.NewLine = false;
            this.verifyBoxDouiM2.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiM2.TabIndex = 27;
            this.verifyBoxDouiM2.TextV = "";
            // 
            // verifyBoxDouiY2
            // 
            this.verifyBoxDouiY2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiY2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiY2.Location = new System.Drawing.Point(72, 20);
            this.verifyBoxDouiY2.Name = "verifyBoxDouiY2";
            this.verifyBoxDouiY2.NewLine = false;
            this.verifyBoxDouiY2.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiY2.TabIndex = 25;
            this.verifyBoxDouiY2.TextV = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(70, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "同意年月日2";
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.verifyBoxY);
            this.panelHeader.Controls.Add(this.verifyBoxM);
            this.panelHeader.Controls.Add(this.labelM);
            this.panelHeader.Controls.Add(this.labelY);
            this.panelHeader.Controls.Add(this.labelH);
            this.panelHeader.Controls.Add(this.labelYearInfo);
            this.panelHeader.Controls.Add(this.panelApp);
            this.panelHeader.Controls.Add(this.panelDoui);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1020, 74);
            this.panelHeader.TabIndex = 0;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(87, 22);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(139, 22);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextV = "";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(5, 4);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(49, 26);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // panelApp
            // 
            this.panelApp.Controls.Add(this.label42);
            this.panelApp.Controls.Add(this.verifyBoxInsNumHeader);
            this.panelApp.Controls.Add(this.verifyBoxInsNum);
            this.panelApp.Controls.Add(this.label17);
            this.panelApp.Controls.Add(this.label7);
            this.panelApp.Controls.Add(this.label18);
            this.panelApp.Controls.Add(this.verifyBoxDrNum);
            this.panelApp.Controls.Add(this.verifyBoxNumbering);
            this.panelApp.Controls.Add(this.label16);
            this.panelApp.Controls.Add(this.verifyBoxPersonName);
            this.panelApp.Controls.Add(this.label4);
            this.panelApp.Controls.Add(this.verifyBoxNum);
            this.panelApp.Controls.Add(this.verifyBoxBD);
            this.panelApp.Controls.Add(this.label5);
            this.panelApp.Controls.Add(this.verifyBoxBM);
            this.panelApp.Controls.Add(this.verifyBoxBY);
            this.panelApp.Controls.Add(this.labelSex);
            this.panelApp.Controls.Add(this.verifyBoxBE);
            this.panelApp.Controls.Add(this.labelBirthday);
            this.panelApp.Controls.Add(this.verifyBoxSex);
            this.panelApp.Controls.Add(this.labelHnum);
            this.panelApp.Controls.Add(this.label10);
            this.panelApp.Controls.Add(this.label3);
            this.panelApp.Location = new System.Drawing.Point(200, 0);
            this.panelApp.Name = "panelApp";
            this.panelApp.Size = new System.Drawing.Size(835, 73);
            this.panelApp.TabIndex = 31;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(8, 5);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(61, 13);
            this.label42.TabIndex = 6;
            this.label42.Text = "ナンバリング";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxInsNumHeader
            // 
            this.verifyBoxInsNumHeader.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNumHeader.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNumHeader.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxInsNumHeader.Location = new System.Drawing.Point(217, 22);
            this.verifyBoxInsNumHeader.Name = "verifyBoxInsNumHeader";
            this.verifyBoxInsNumHeader.NewLine = false;
            this.verifyBoxInsNumHeader.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxInsNumHeader.TabIndex = 11;
            this.verifyBoxInsNumHeader.TabStop = false;
            this.verifyBoxInsNumHeader.TextV = "";
            this.verifyBoxInsNumHeader.Leave += new System.EventHandler(this.verifyBoxNum_Leave);
            // 
            // verifyBoxInsNum
            // 
            this.verifyBoxInsNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxInsNum.Location = new System.Drawing.Point(257, 22);
            this.verifyBoxInsNum.Name = "verifyBoxInsNum";
            this.verifyBoxInsNum.NewLine = false;
            this.verifyBoxInsNum.Size = new System.Drawing.Size(44, 23);
            this.verifyBoxInsNum.TabIndex = 12;
            this.verifyBoxInsNum.TextV = "";
            this.verifyBoxInsNum.Leave += new System.EventHandler(this.verifyBoxNum_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(728, 34);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "月";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(399, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "受療者名";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(685, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "年";
            // 
            // verifyBoxDrNum
            // 
            this.verifyBoxDrNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrNum.Location = new System.Drawing.Point(100, 22);
            this.verifyBoxDrNum.Name = "verifyBoxDrNum";
            this.verifyBoxDrNum.NewLine = false;
            this.verifyBoxDrNum.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxDrNum.TabIndex = 9;
            this.verifyBoxDrNum.TextV = "";
            this.verifyBoxDrNum.Leave += new System.EventHandler(this.verifyBoxNum_Leave);
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(10, 22);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(84, 23);
            this.verifyBoxNumbering.TabIndex = 7;
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Leave += new System.EventHandler(this.verifyBoxNum_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(771, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "日";
            // 
            // verifyBoxPersonName
            // 
            this.verifyBoxPersonName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPersonName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPersonName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.verifyBoxPersonName.Location = new System.Drawing.Point(401, 22);
            this.verifyBoxPersonName.Name = "verifyBoxPersonName";
            this.verifyBoxPersonName.NewLine = false;
            this.verifyBoxPersonName.Size = new System.Drawing.Size(136, 23);
            this.verifyBoxPersonName.TabIndex = 18;
            this.verifyBoxPersonName.TextV = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(577, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 26);
            this.label4.TabIndex = 21;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // verifyBoxNum
            // 
            this.verifyBoxNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNum.Location = new System.Drawing.Point(309, 22);
            this.verifyBoxNum.Name = "verifyBoxNum";
            this.verifyBoxNum.NewLine = false;
            this.verifyBoxNum.Size = new System.Drawing.Size(84, 23);
            this.verifyBoxNum.TabIndex = 14;
            this.verifyBoxNum.TextV = "";
            this.verifyBoxNum.Leave += new System.EventHandler(this.verifyBoxNum_Leave);
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(745, 22);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 29;
            this.verifyBoxBD.TextV = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(634, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 39);
            this.label5.TabIndex = 24;
            this.label5.Text = "大:2\r\n昭:3\r\n平:4";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(702, 22);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 27;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(659, 22);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 25;
            this.verifyBoxBY.TextV = "";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(548, 5);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 19;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(608, 22);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 23;
            this.verifyBoxBE.TextV = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(606, 4);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 22;
            this.labelBirthday.Text = "生年月日";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(550, 22);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 20;
            this.verifyBoxSex.TextV = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(215, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "保険者番号";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "施術師コード";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelDoui
            // 
            this.panelDoui.Controls.Add(this.labelDouiFusho);
            this.panelDoui.Controls.Add(this.verifyBoxDouiFusho4);
            this.panelDoui.Controls.Add(this.verifyBoxDouiFusho3);
            this.panelDoui.Controls.Add(this.verifyBoxDouiFusho2);
            this.panelDoui.Controls.Add(this.verifyBoxDouiFusho1);
            this.panelDoui.Controls.Add(this.verifyCheckBoxDouiVisit);
            this.panelDoui.Controls.Add(this.verifyCheckBoxLeftLower);
            this.panelDoui.Controls.Add(this.verifyCheckBoxRightLower);
            this.panelDoui.Controls.Add(this.verifyCheckBoxLeftUpper);
            this.panelDoui.Controls.Add(this.verifyCheckBoxRightUpper);
            this.panelDoui.Controls.Add(this.verifyCheckBoxAnmaAll);
            this.panelDoui.Controls.Add(this.verifyCheckBoxBody);
            this.panelDoui.Controls.Add(this.verifyCheckBoxDouiToshu);
            this.panelDoui.Controls.Add(this.verifyCheckBoxDouiMasserge);
            this.panelDoui.Controls.Add(this.labelDouiType);
            this.panelDoui.Controls.Add(this.label43);
            this.panelDoui.Controls.Add(this.label45);
            this.panelDoui.Controls.Add(this.label46);
            this.panelDoui.Controls.Add(this.verifyBoxDouiD);
            this.panelDoui.Controls.Add(this.verifyBoxDouiM);
            this.panelDoui.Controls.Add(this.verifyBoxDouiY);
            this.panelDoui.Controls.Add(this.label50);
            this.panelDoui.Controls.Add(this.labelDouiVisit);
            this.panelDoui.Controls.Add(this.labelDouiBui);
            this.panelDoui.Controls.Add(this.labelDouiFushoInfo);
            this.panelDoui.Location = new System.Drawing.Point(200, 0);
            this.panelDoui.Name = "panelDoui";
            this.panelDoui.Size = new System.Drawing.Size(835, 73);
            this.panelDoui.TabIndex = 31;
            // 
            // labelDouiFusho
            // 
            this.labelDouiFusho.AutoSize = true;
            this.labelDouiFusho.Location = new System.Drawing.Point(110, 5);
            this.labelDouiFusho.Name = "labelDouiFusho";
            this.labelDouiFusho.Size = new System.Drawing.Size(55, 13);
            this.labelDouiFusho.TabIndex = 3;
            this.labelDouiFusho.Text = "同意負傷";
            this.labelDouiFusho.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxDouiFusho4
            // 
            this.verifyBoxDouiFusho4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiFusho4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiFusho4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiFusho4.Location = new System.Drawing.Point(190, 22);
            this.verifyBoxDouiFusho4.Name = "verifyBoxDouiFusho4";
            this.verifyBoxDouiFusho4.NewLine = false;
            this.verifyBoxDouiFusho4.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiFusho4.TabIndex = 7;
            this.verifyBoxDouiFusho4.TextV = "";
            // 
            // verifyBoxDouiFusho3
            // 
            this.verifyBoxDouiFusho3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiFusho3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiFusho3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiFusho3.Location = new System.Drawing.Point(164, 22);
            this.verifyBoxDouiFusho3.Name = "verifyBoxDouiFusho3";
            this.verifyBoxDouiFusho3.NewLine = false;
            this.verifyBoxDouiFusho3.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiFusho3.TabIndex = 6;
            this.verifyBoxDouiFusho3.TextV = "";
            // 
            // verifyBoxDouiFusho2
            // 
            this.verifyBoxDouiFusho2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiFusho2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiFusho2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiFusho2.Location = new System.Drawing.Point(138, 22);
            this.verifyBoxDouiFusho2.Name = "verifyBoxDouiFusho2";
            this.verifyBoxDouiFusho2.NewLine = false;
            this.verifyBoxDouiFusho2.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiFusho2.TabIndex = 5;
            this.verifyBoxDouiFusho2.TextV = "";
            // 
            // verifyBoxDouiFusho1
            // 
            this.verifyBoxDouiFusho1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiFusho1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiFusho1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiFusho1.Location = new System.Drawing.Point(112, 22);
            this.verifyBoxDouiFusho1.Name = "verifyBoxDouiFusho1";
            this.verifyBoxDouiFusho1.NewLine = false;
            this.verifyBoxDouiFusho1.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiFusho1.TabIndex = 4;
            this.verifyBoxDouiFusho1.TextV = "";
            // 
            // verifyCheckBoxDouiVisit
            // 
            this.verifyCheckBoxDouiVisit.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxDouiVisit.CheckedV = false;
            this.verifyCheckBoxDouiVisit.Location = new System.Drawing.Point(631, 22);
            this.verifyCheckBoxDouiVisit.Name = "verifyCheckBoxDouiVisit";
            this.verifyCheckBoxDouiVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxDouiVisit.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxDouiVisit.TabIndex = 16;
            this.verifyCheckBoxDouiVisit.Text = "同意";
            this.verifyCheckBoxDouiVisit.UseVisualStyleBackColor = false;
            // 
            // verifyCheckBoxLeftLower
            // 
            this.verifyCheckBoxLeftLower.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxLeftLower.CheckedV = false;
            this.verifyCheckBoxLeftLower.Location = new System.Drawing.Point(556, 22);
            this.verifyCheckBoxLeftLower.Name = "verifyCheckBoxLeftLower";
            this.verifyCheckBoxLeftLower.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxLeftLower.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxLeftLower.TabIndex = 14;
            this.verifyCheckBoxLeftLower.Text = "左下肢";
            this.verifyCheckBoxLeftLower.UseVisualStyleBackColor = false;
            this.verifyCheckBoxLeftLower.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnma_CheckedChanged);
            // 
            // verifyCheckBoxRightLower
            // 
            this.verifyCheckBoxRightLower.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxRightLower.CheckedV = false;
            this.verifyCheckBoxRightLower.Location = new System.Drawing.Point(489, 22);
            this.verifyCheckBoxRightLower.Name = "verifyCheckBoxRightLower";
            this.verifyCheckBoxRightLower.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxRightLower.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxRightLower.TabIndex = 13;
            this.verifyCheckBoxRightLower.Text = "右下肢";
            this.verifyCheckBoxRightLower.UseVisualStyleBackColor = false;
            this.verifyCheckBoxRightLower.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnma_CheckedChanged);
            // 
            // verifyCheckBoxLeftUpper
            // 
            this.verifyCheckBoxLeftUpper.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxLeftUpper.CheckedV = false;
            this.verifyCheckBoxLeftUpper.Location = new System.Drawing.Point(422, 22);
            this.verifyCheckBoxLeftUpper.Name = "verifyCheckBoxLeftUpper";
            this.verifyCheckBoxLeftUpper.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxLeftUpper.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxLeftUpper.TabIndex = 12;
            this.verifyCheckBoxLeftUpper.Text = "左上肢";
            this.verifyCheckBoxLeftUpper.UseVisualStyleBackColor = false;
            this.verifyCheckBoxLeftUpper.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnma_CheckedChanged);
            // 
            // verifyCheckBoxRightUpper
            // 
            this.verifyCheckBoxRightUpper.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxRightUpper.CheckedV = false;
            this.verifyCheckBoxRightUpper.Location = new System.Drawing.Point(355, 22);
            this.verifyCheckBoxRightUpper.Name = "verifyCheckBoxRightUpper";
            this.verifyCheckBoxRightUpper.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxRightUpper.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxRightUpper.TabIndex = 11;
            this.verifyCheckBoxRightUpper.Text = "右上肢";
            this.verifyCheckBoxRightUpper.UseVisualStyleBackColor = false;
            this.verifyCheckBoxRightUpper.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnma_CheckedChanged);
            // 
            // verifyCheckBoxAnmaAll
            // 
            this.verifyCheckBoxAnmaAll.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxAnmaAll.CheckedV = false;
            this.verifyCheckBoxAnmaAll.Location = new System.Drawing.Point(218, 22);
            this.verifyCheckBoxAnmaAll.Name = "verifyCheckBoxAnmaAll";
            this.verifyCheckBoxAnmaAll.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxAnmaAll.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxAnmaAll.TabIndex = 9;
            this.verifyCheckBoxAnmaAll.Text = "全部位";
            this.verifyCheckBoxAnmaAll.UseVisualStyleBackColor = false;
            this.verifyCheckBoxAnmaAll.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnmaAll_CheckedChanged);
            // 
            // verifyCheckBoxBody
            // 
            this.verifyCheckBoxBody.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxBody.CheckedV = false;
            this.verifyCheckBoxBody.Location = new System.Drawing.Point(288, 22);
            this.verifyCheckBoxBody.Name = "verifyCheckBoxBody";
            this.verifyCheckBoxBody.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxBody.Size = new System.Drawing.Size(67, 25);
            this.verifyCheckBoxBody.TabIndex = 10;
            this.verifyCheckBoxBody.Text = "体幹";
            this.verifyCheckBoxBody.UseVisualStyleBackColor = false;
            this.verifyCheckBoxBody.CheckedChanged += new System.EventHandler(this.verifyCheckBoxAnma_CheckedChanged);
            // 
            // verifyCheckBoxDouiToshu
            // 
            this.verifyCheckBoxDouiToshu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxDouiToshu.CheckedV = false;
            this.verifyCheckBoxDouiToshu.Location = new System.Drawing.Point(86, 22);
            this.verifyCheckBoxDouiToshu.Name = "verifyCheckBoxDouiToshu";
            this.verifyCheckBoxDouiToshu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxDouiToshu.Size = new System.Drawing.Size(80, 25);
            this.verifyCheckBoxDouiToshu.TabIndex = 2;
            this.verifyCheckBoxDouiToshu.Text = "変形徒手";
            this.verifyCheckBoxDouiToshu.UseVisualStyleBackColor = false;
            // 
            // verifyCheckBoxDouiMasserge
            // 
            this.verifyCheckBoxDouiMasserge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxDouiMasserge.CheckedV = false;
            this.verifyCheckBoxDouiMasserge.Location = new System.Drawing.Point(6, 22);
            this.verifyCheckBoxDouiMasserge.Name = "verifyCheckBoxDouiMasserge";
            this.verifyCheckBoxDouiMasserge.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxDouiMasserge.Size = new System.Drawing.Size(80, 25);
            this.verifyCheckBoxDouiMasserge.TabIndex = 1;
            this.verifyCheckBoxDouiMasserge.Text = "マッサージ";
            this.verifyCheckBoxDouiMasserge.UseVisualStyleBackColor = false;
            // 
            // labelDouiType
            // 
            this.labelDouiType.AutoSize = true;
            this.labelDouiType.Location = new System.Drawing.Point(5, 5);
            this.labelDouiType.Name = "labelDouiType";
            this.labelDouiType.Size = new System.Drawing.Size(79, 13);
            this.labelDouiType.TabIndex = 0;
            this.labelDouiType.Text = "同意施術種類";
            this.labelDouiType.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(774, 34);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(19, 13);
            this.label43.TabIndex = 21;
            this.label43.Text = "月";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(731, 34);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(19, 13);
            this.label45.TabIndex = 19;
            this.label45.Text = "年";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(817, 34);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(19, 13);
            this.label46.TabIndex = 23;
            this.label46.Text = "日";
            // 
            // verifyBoxDouiD
            // 
            this.verifyBoxDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiD.Location = new System.Drawing.Point(791, 22);
            this.verifyBoxDouiD.Name = "verifyBoxDouiD";
            this.verifyBoxDouiD.NewLine = false;
            this.verifyBoxDouiD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiD.TabIndex = 22;
            this.verifyBoxDouiD.TextV = "";
            // 
            // verifyBoxDouiM
            // 
            this.verifyBoxDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiM.Location = new System.Drawing.Point(748, 22);
            this.verifyBoxDouiM.Name = "verifyBoxDouiM";
            this.verifyBoxDouiM.NewLine = false;
            this.verifyBoxDouiM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiM.TabIndex = 20;
            this.verifyBoxDouiM.TextV = "";
            // 
            // verifyBoxDouiY
            // 
            this.verifyBoxDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDouiY.Location = new System.Drawing.Point(705, 22);
            this.verifyBoxDouiY.Name = "verifyBoxDouiY";
            this.verifyBoxDouiY.NewLine = false;
            this.verifyBoxDouiY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDouiY.TabIndex = 18;
            this.verifyBoxDouiY.TextV = "";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(703, 5);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(73, 13);
            this.label50.TabIndex = 17;
            this.label50.Text = "同意年月日1";
            // 
            // labelDouiVisit
            // 
            this.labelDouiVisit.AutoSize = true;
            this.labelDouiVisit.Location = new System.Drawing.Point(629, 5);
            this.labelDouiVisit.Name = "labelDouiVisit";
            this.labelDouiVisit.Size = new System.Drawing.Size(55, 13);
            this.labelDouiVisit.TabIndex = 15;
            this.labelDouiVisit.Text = "往療同意";
            this.labelDouiVisit.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDouiBui
            // 
            this.labelDouiBui.AutoSize = true;
            this.labelDouiBui.Location = new System.Drawing.Point(218, 5);
            this.labelDouiBui.Name = "labelDouiBui";
            this.labelDouiBui.Size = new System.Drawing.Size(55, 13);
            this.labelDouiBui.TabIndex = 9;
            this.labelDouiBui.Text = "同意部位";
            this.labelDouiBui.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDouiFushoInfo
            // 
            this.labelDouiFushoInfo.AutoSize = true;
            this.labelDouiFushoInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDouiFushoInfo.Location = new System.Drawing.Point(112, 52);
            this.labelDouiFushoInfo.Name = "labelDouiFushoInfo";
            this.labelDouiFushoInfo.Size = new System.Drawing.Size(405, 13);
            this.labelDouiFushoInfo.TabIndex = 8;
            this.labelDouiFushoInfo.Text = "1.神経痛 2.リウマチ 3.頚腕症候群 4.五十肩 5.腰痛症 6.頸椎捻挫後遺症 7.その他";
            // 
            // panelFutter
            // 
            this.panelFutter.Controls.Add(this.buttonBack);
            this.panelFutter.Controls.Add(this.buttonRegist);
            this.panelFutter.Controls.Add(this.labelInputerName);
            this.panelFutter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFutter.Location = new System.Drawing.Point(0, 753);
            this.panelFutter.Name = "panelFutter";
            this.panelFutter.Size = new System.Drawing.Size(1020, 28);
            this.panelFutter.TabIndex = 5;
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(735, 2);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 1;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(13, 2);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 2;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(590, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "変形徒手";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 781);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 781);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelCommon.ResumeLayout(false);
            this.panelAppData.ResumeLayout(false);
            this.panelAppData.PerformLayout();
            this.panelDouiDates.ResumeLayout(false);
            this.panelDouiDates.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panelApp.ResumeLayout(false);
            this.panelApp.PerformLayout();
            this.panelDoui.ResumeLayout(false);
            this.panelDoui.PerformLayout();
            this.panelFutter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Label labelInputerName;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxNum;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxPersonName;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxInsNum;
        private VerifyBox verifyBoxDrNum;
        private VerifyBox verifyBoxFinish;
        private VerifyBox verifyBoxStart;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxFirstD;
        private VerifyBox verifyBoxFirstM;
        private VerifyBox verifyBoxFirstY;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelCommon;
        private System.Windows.Forms.Panel panelFutter;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label42;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Panel panelDoui;
        private VerifyCheckBox verifyCheckBoxDouiToshu;
        private VerifyCheckBox verifyCheckBoxDouiMasserge;
        private System.Windows.Forms.Label labelDouiType;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labelDouiFusho;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private VerifyBox verifyBoxDouiFusho1;
        private VerifyBox verifyBoxDouiD;
        private VerifyBox verifyBoxDouiM;
        private VerifyBox verifyBoxDouiY;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label labelDouiBui;
        private System.Windows.Forms.Panel panelApp;
        private System.Windows.Forms.Label labelDouiFushoInfo;
        private VerifyCheckBox verifyCheckBoxDouiVisit;
        private VerifyCheckBox verifyCheckBoxLeftLower;
        private VerifyCheckBox verifyCheckBoxRightLower;
        private VerifyCheckBox verifyCheckBoxLeftUpper;
        private VerifyCheckBox verifyCheckBoxRightUpper;
        private VerifyCheckBox verifyCheckBoxBody;
        private System.Windows.Forms.Label labelDouiVisit;
        private VerifyBox verifyBoxInsNumHeader;
        private VerifyBox verifyBoxDouiFusho4;
        private VerifyBox verifyBoxDouiFusho3;
        private VerifyBox verifyBoxDouiFusho2;
        private VerifyCheckBox verifyCheckBoxAnmaAll;
        private System.Windows.Forms.Panel panelDouiDates;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private VerifyBox verifyBoxDouiD3;
        private VerifyBox verifyBoxDouiM3;
        private VerifyBox verifyBoxDouiY3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private VerifyBox verifyBoxDouiD2;
        private VerifyBox verifyBoxDouiM2;
        private VerifyBox verifyBoxDouiY2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panelAppData;
        private VerifyBox verifyBoxDouiNengo3;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxDouiNengo2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxFirstNengo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblDrCode;
        private VerifyBox verifyBoxTourokuNum;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
    }
}