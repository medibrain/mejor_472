﻿using System;

namespace Mejor.KyotoKoiki
{
    partial class SubInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelCommon = new System.Windows.Forms.Panel();
            this.verifyCheckOver5 = new Mejor.VerifyCheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.verifyBoxSaiDouiD = new Mejor.VerifyBox();
            this.textBoxFirst = new System.Windows.Forms.TextBox();
            this.verifyBoxSaiDouiY = new Mejor.VerifyBox();
            this.verifyBoxSaiDouiM = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxNewCost = new Mejor.VerifyBox();
            this.verifyCheckBoxAnbun = new Mejor.VerifyCheckBox();
            this.verifyBoxVisit4Dis = new Mejor.VerifyBox();
            this.verifyBoxVisit4Count = new Mejor.VerifyBox();
            this.verifyBoxVisit4Cost = new Mejor.VerifyBox();
            this.verifyBoxVisit3Dis = new Mejor.VerifyBox();
            this.verifyBoxVisit3Count = new Mejor.VerifyBox();
            this.verifyBoxVisit3Cost = new Mejor.VerifyBox();
            this.verifyBoxVisit2Dis = new Mejor.VerifyBox();
            this.verifyBoxVisit2Count = new Mejor.VerifyBox();
            this.verifyBoxVisit2Cost = new Mejor.VerifyBox();
            this.verifyBoxVisit1Dis = new Mejor.VerifyBox();
            this.verifyBoxVisit1Count = new Mejor.VerifyBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.verifyBoxVisit1Cost = new Mejor.VerifyBox();
            this.panelHari = new System.Windows.Forms.Panel();
            this.verifyBoxHariCost = new Mejor.VerifyBox();
            this.verifyBoxKyuDenCount = new Mejor.VerifyBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.verifyBoxKyuDenCost = new Mejor.VerifyBox();
            this.verifyBoxHariCount = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.verifyBoxHeiCost = new Mejor.VerifyBox();
            this.verifyBoxDenCount = new Mejor.VerifyBox();
            this.verifyBoxHeiDenCount = new Mejor.VerifyBox();
            this.verifyBoxKyuCount = new Mejor.VerifyBox();
            this.verifyBoxHariDenCost = new Mejor.VerifyBox();
            this.verifyBoxHeiCount = new Mejor.VerifyBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxKyuCost = new Mejor.VerifyBox();
            this.verifyBoxHariDenCount = new Mejor.VerifyBox();
            this.label32 = new System.Windows.Forms.Label();
            this.verifyBoxDenCost = new Mejor.VerifyBox();
            this.verifyBoxHeiDenCost = new Mejor.VerifyBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panelAnma = new System.Windows.Forms.Panel();
            this.verifyBoxPartCountCount = new Mejor.VerifyBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxPartCost = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxPartCount = new Mejor.VerifyBox();
            this.verifyBoxEleWarmCount = new Mejor.VerifyBox();
            this.verifyBoxWarmCount = new Mejor.VerifyBox();
            this.verifyBoxToshuPartCount = new Mejor.VerifyBox();
            this.label47 = new System.Windows.Forms.Label();
            this.verifyBoxToshuCount = new Mejor.VerifyBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.verifyBoxLeftLowerCount = new Mejor.VerifyBox();
            this.verifyBoxBodyCost = new Mejor.VerifyBox();
            this.verifyBoxEleWarmCost = new Mejor.VerifyBox();
            this.verifyBoxWarmCost = new Mejor.VerifyBox();
            this.verifyBoxToshuCost = new Mejor.VerifyBox();
            this.verifyBoxBodyCount = new Mejor.VerifyBox();
            this.verifyBoxLeftLowerCost = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.verifyBoxRightUpperCost = new Mejor.VerifyBox();
            this.verifyBoxRightLowerCount = new Mejor.VerifyBox();
            this.verifyBoxRightUpperCount = new Mejor.VerifyBox();
            this.verifyBoxRightLowerCost = new Mejor.VerifyBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.verifyBoxLeftUpperCost = new Mejor.VerifyBox();
            this.verifyBoxLeftUpperCount = new Mejor.VerifyBox();
            this.panelFutter = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelCommon.SuspendLayout();
            this.panelHari.SuspendLayout();
            this.panelAnma.SuspendLayout();
            this.panelFutter.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(831, 2);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 0;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(120, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-630, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panelCommon);
            this.panelRight.Controls.Add(this.panelHari);
            this.panelRight.Controls.Add(this.panelAnma);
            this.panelRight.Controls.Add(this.panelFutter);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 0;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1020, 491);
            this.scrollPictureControl1.TabIndex = 0;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelCommon
            // 
            this.panelCommon.Controls.Add(this.verifyCheckOver5);
            this.panelCommon.Controls.Add(this.label55);
            this.panelCommon.Controls.Add(this.label54);
            this.panelCommon.Controls.Add(this.label53);
            this.panelCommon.Controls.Add(this.label19);
            this.panelCommon.Controls.Add(this.label23);
            this.panelCommon.Controls.Add(this.label24);
            this.panelCommon.Controls.Add(this.label3);
            this.panelCommon.Controls.Add(this.label25);
            this.panelCommon.Controls.Add(this.verifyBoxSaiDouiD);
            this.panelCommon.Controls.Add(this.textBoxFirst);
            this.panelCommon.Controls.Add(this.verifyBoxSaiDouiY);
            this.panelCommon.Controls.Add(this.verifyBoxSaiDouiM);
            this.panelCommon.Controls.Add(this.label2);
            this.panelCommon.Controls.Add(this.verifyBoxNewCost);
            this.panelCommon.Controls.Add(this.verifyCheckBoxAnbun);
            this.panelCommon.Controls.Add(this.verifyBoxVisit4Dis);
            this.panelCommon.Controls.Add(this.verifyBoxVisit4Count);
            this.panelCommon.Controls.Add(this.verifyBoxVisit4Cost);
            this.panelCommon.Controls.Add(this.verifyBoxVisit3Dis);
            this.panelCommon.Controls.Add(this.verifyBoxVisit3Count);
            this.panelCommon.Controls.Add(this.verifyBoxVisit3Cost);
            this.panelCommon.Controls.Add(this.verifyBoxVisit2Dis);
            this.panelCommon.Controls.Add(this.verifyBoxVisit2Count);
            this.panelCommon.Controls.Add(this.verifyBoxVisit2Cost);
            this.panelCommon.Controls.Add(this.verifyBoxVisit1Dis);
            this.panelCommon.Controls.Add(this.verifyBoxVisit1Count);
            this.panelCommon.Controls.Add(this.label40);
            this.panelCommon.Controls.Add(this.label39);
            this.panelCommon.Controls.Add(this.label36);
            this.panelCommon.Controls.Add(this.label52);
            this.panelCommon.Controls.Add(this.label21);
            this.panelCommon.Controls.Add(this.verifyBoxVisit1Cost);
            this.panelCommon.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCommon.Location = new System.Drawing.Point(0, 491);
            this.panelCommon.Name = "panelCommon";
            this.panelCommon.Size = new System.Drawing.Size(1020, 68);
            this.panelCommon.TabIndex = 1;
            // 
            // verifyCheckOver5
            // 
            this.verifyCheckOver5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckOver5.CheckedV = false;
            this.verifyCheckOver5.Location = new System.Drawing.Point(383, 17);
            this.verifyCheckOver5.Name = "verifyCheckOver5";
            this.verifyCheckOver5.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckOver5.Size = new System.Drawing.Size(64, 24);
            this.verifyCheckOver5.TabIndex = 13;
            this.verifyCheckOver5.Text = "5以上";
            this.verifyCheckOver5.UseVisualStyleBackColor = false;
            this.verifyCheckOver5.CheckedChanged += new System.EventHandler(this.verifyCheckBoxVisit_CheckedChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label55.Location = new System.Drawing.Point(550, 43);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(17, 12);
            this.label55.TabIndex = 20;
            this.label55.Text = "回";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label54.Location = new System.Drawing.Point(511, 43);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(29, 12);
            this.label54.TabIndex = 18;
            this.label54.Text = "単価";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label53.Location = new System.Drawing.Point(467, 43);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(29, 12);
            this.label53.TabIndex = 16;
            this.label53.Text = "距離";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(255, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 8;
            this.label19.Text = "月";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(212, 28);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 6;
            this.label23.Text = "年";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(298, 28);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 10;
            this.label24.Text = "日";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "初検年月日";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(184, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 12);
            this.label25.TabIndex = 4;
            this.label25.Text = "再同意年月日";
            // 
            // verifyBoxSaiDouiD
            // 
            this.verifyBoxSaiDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSaiDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSaiDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSaiDouiD.Location = new System.Drawing.Point(272, 17);
            this.verifyBoxSaiDouiD.Name = "verifyBoxSaiDouiD";
            this.verifyBoxSaiDouiD.NewLine = false;
            this.verifyBoxSaiDouiD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSaiDouiD.TabIndex = 9;
            this.verifyBoxSaiDouiD.TextV = "";
            // 
            // textBoxFirst
            // 
            this.textBoxFirst.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxFirst.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFirst.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFirst.Location = new System.Drawing.Point(87, 17);
            this.textBoxFirst.Name = "textBoxFirst";
            this.textBoxFirst.ReadOnly = true;
            this.textBoxFirst.Size = new System.Drawing.Size(84, 23);
            this.textBoxFirst.TabIndex = 3;
            this.textBoxFirst.TabStop = false;
            // 
            // verifyBoxSaiDouiY
            // 
            this.verifyBoxSaiDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSaiDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSaiDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSaiDouiY.Location = new System.Drawing.Point(186, 17);
            this.verifyBoxSaiDouiY.Name = "verifyBoxSaiDouiY";
            this.verifyBoxSaiDouiY.NewLine = false;
            this.verifyBoxSaiDouiY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSaiDouiY.TabIndex = 5;
            this.verifyBoxSaiDouiY.TextV = "";
            // 
            // verifyBoxSaiDouiM
            // 
            this.verifyBoxSaiDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSaiDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSaiDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSaiDouiM.Location = new System.Drawing.Point(229, 17);
            this.verifyBoxSaiDouiM.Name = "verifyBoxSaiDouiM";
            this.verifyBoxSaiDouiM.NewLine = false;
            this.verifyBoxSaiDouiM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSaiDouiM.TabIndex = 7;
            this.verifyBoxSaiDouiM.TextV = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "初検料";
            // 
            // verifyBoxNewCost
            // 
            this.verifyBoxNewCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCost.Location = new System.Drawing.Point(8, 17);
            this.verifyBoxNewCost.Name = "verifyBoxNewCost";
            this.verifyBoxNewCost.NewLine = false;
            this.verifyBoxNewCost.Size = new System.Drawing.Size(67, 23);
            this.verifyBoxNewCost.TabIndex = 1;
            this.verifyBoxNewCost.TextV = "";
            this.verifyBoxNewCost.TextChanged += new System.EventHandler(this.verifyBoxNewCost_TextChanged);
            // 
            // verifyCheckBoxAnbun
            // 
            this.verifyCheckBoxAnbun.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxAnbun.CheckedV = false;
            this.verifyCheckBoxAnbun.Location = new System.Drawing.Point(325, 17);
            this.verifyCheckBoxAnbun.Name = "verifyCheckBoxAnbun";
            this.verifyCheckBoxAnbun.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxAnbun.Size = new System.Drawing.Size(58, 24);
            this.verifyCheckBoxAnbun.TabIndex = 12;
            this.verifyCheckBoxAnbun.Text = "按分";
            this.verifyCheckBoxAnbun.UseVisualStyleBackColor = false;
            this.verifyCheckBoxAnbun.CheckedChanged += new System.EventHandler(this.verifyCheckBoxVisit_CheckedChanged);
            // 
            // verifyBoxVisit4Dis
            // 
            this.verifyBoxVisit4Dis.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit4Dis.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit4Dis.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit4Dis.Location = new System.Drawing.Point(810, 17);
            this.verifyBoxVisit4Dis.Name = "verifyBoxVisit4Dis";
            this.verifyBoxVisit4Dis.NewLine = false;
            this.verifyBoxVisit4Dis.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxVisit4Dis.TabIndex = 30;
            this.verifyBoxVisit4Dis.TextV = "";
            this.verifyBoxVisit4Dis.TextChanged += new System.EventHandler(this.verifyBoxVisitDisControls_TextChanged);
            // 
            // verifyBoxVisit4Count
            // 
            this.verifyBoxVisit4Count.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit4Count.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit4Count.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit4Count.Location = new System.Drawing.Point(897, 17);
            this.verifyBoxVisit4Count.Name = "verifyBoxVisit4Count";
            this.verifyBoxVisit4Count.NewLine = false;
            this.verifyBoxVisit4Count.Size = new System.Drawing.Size(24, 23);
            this.verifyBoxVisit4Count.TabIndex = 32;
            this.verifyBoxVisit4Count.TextV = "";
            // 
            // verifyBoxVisit4Cost
            // 
            this.verifyBoxVisit4Cost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit4Cost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit4Cost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit4Cost.Location = new System.Drawing.Point(855, 17);
            this.verifyBoxVisit4Cost.Name = "verifyBoxVisit4Cost";
            this.verifyBoxVisit4Cost.NewLine = false;
            this.verifyBoxVisit4Cost.Size = new System.Drawing.Size(42, 23);
            this.verifyBoxVisit4Cost.TabIndex = 31;
            this.verifyBoxVisit4Cost.TextV = "";
            // 
            // verifyBoxVisit3Dis
            // 
            this.verifyBoxVisit3Dis.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit3Dis.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit3Dis.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit3Dis.Location = new System.Drawing.Point(693, 17);
            this.verifyBoxVisit3Dis.Name = "verifyBoxVisit3Dis";
            this.verifyBoxVisit3Dis.NewLine = false;
            this.verifyBoxVisit3Dis.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxVisit3Dis.TabIndex = 26;
            this.verifyBoxVisit3Dis.TextV = "";
            this.verifyBoxVisit3Dis.TextChanged += new System.EventHandler(this.verifyBoxVisitDisControls_TextChanged);
            // 
            // verifyBoxVisit3Count
            // 
            this.verifyBoxVisit3Count.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit3Count.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit3Count.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit3Count.Location = new System.Drawing.Point(780, 17);
            this.verifyBoxVisit3Count.Name = "verifyBoxVisit3Count";
            this.verifyBoxVisit3Count.NewLine = false;
            this.verifyBoxVisit3Count.Size = new System.Drawing.Size(24, 23);
            this.verifyBoxVisit3Count.TabIndex = 28;
            this.verifyBoxVisit3Count.TextV = "";
            // 
            // verifyBoxVisit3Cost
            // 
            this.verifyBoxVisit3Cost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit3Cost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit3Cost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit3Cost.Location = new System.Drawing.Point(738, 17);
            this.verifyBoxVisit3Cost.Name = "verifyBoxVisit3Cost";
            this.verifyBoxVisit3Cost.NewLine = false;
            this.verifyBoxVisit3Cost.Size = new System.Drawing.Size(42, 23);
            this.verifyBoxVisit3Cost.TabIndex = 27;
            this.verifyBoxVisit3Cost.TextV = "";
            // 
            // verifyBoxVisit2Dis
            // 
            this.verifyBoxVisit2Dis.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit2Dis.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit2Dis.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit2Dis.Location = new System.Drawing.Point(576, 17);
            this.verifyBoxVisit2Dis.Name = "verifyBoxVisit2Dis";
            this.verifyBoxVisit2Dis.NewLine = false;
            this.verifyBoxVisit2Dis.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxVisit2Dis.TabIndex = 22;
            this.verifyBoxVisit2Dis.TextV = "";
            this.verifyBoxVisit2Dis.TextChanged += new System.EventHandler(this.verifyBoxVisitDisControls_TextChanged);
            // 
            // verifyBoxVisit2Count
            // 
            this.verifyBoxVisit2Count.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit2Count.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit2Count.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit2Count.Location = new System.Drawing.Point(663, 17);
            this.verifyBoxVisit2Count.Name = "verifyBoxVisit2Count";
            this.verifyBoxVisit2Count.NewLine = false;
            this.verifyBoxVisit2Count.Size = new System.Drawing.Size(24, 23);
            this.verifyBoxVisit2Count.TabIndex = 24;
            this.verifyBoxVisit2Count.TextV = "";
            // 
            // verifyBoxVisit2Cost
            // 
            this.verifyBoxVisit2Cost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit2Cost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit2Cost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit2Cost.Location = new System.Drawing.Point(621, 17);
            this.verifyBoxVisit2Cost.Name = "verifyBoxVisit2Cost";
            this.verifyBoxVisit2Cost.NewLine = false;
            this.verifyBoxVisit2Cost.Size = new System.Drawing.Size(42, 23);
            this.verifyBoxVisit2Cost.TabIndex = 23;
            this.verifyBoxVisit2Cost.TextV = "";
            // 
            // verifyBoxVisit1Dis
            // 
            this.verifyBoxVisit1Dis.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit1Dis.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit1Dis.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit1Dis.Location = new System.Drawing.Point(459, 17);
            this.verifyBoxVisit1Dis.Name = "verifyBoxVisit1Dis";
            this.verifyBoxVisit1Dis.NewLine = false;
            this.verifyBoxVisit1Dis.Size = new System.Drawing.Size(45, 23);
            this.verifyBoxVisit1Dis.TabIndex = 15;
            this.verifyBoxVisit1Dis.TextV = "";
            this.verifyBoxVisit1Dis.TextChanged += new System.EventHandler(this.verifyBoxVisitDisControls_TextChanged);
            // 
            // verifyBoxVisit1Count
            // 
            this.verifyBoxVisit1Count.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit1Count.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit1Count.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit1Count.Location = new System.Drawing.Point(546, 17);
            this.verifyBoxVisit1Count.Name = "verifyBoxVisit1Count";
            this.verifyBoxVisit1Count.NewLine = false;
            this.verifyBoxVisit1Count.Size = new System.Drawing.Size(24, 23);
            this.verifyBoxVisit1Count.TabIndex = 19;
            this.verifyBoxVisit1Count.TextV = "";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(810, 3);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 12);
            this.label40.TabIndex = 29;
            this.label40.Text = "往療4";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(693, 3);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 12);
            this.label39.TabIndex = 25;
            this.label39.Text = "往療3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(574, 3);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 12);
            this.label36.TabIndex = 21;
            this.label36.Text = "往療2";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Enabled = false;
            this.label52.Location = new System.Drawing.Point(325, 3);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(77, 12);
            this.label52.TabIndex = 11;
            this.label52.Text = "往療特殊処理";
            this.label52.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(457, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 12);
            this.label21.TabIndex = 14;
            this.label21.Text = "往療1";
            // 
            // verifyBoxVisit1Cost
            // 
            this.verifyBoxVisit1Cost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisit1Cost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisit1Cost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisit1Cost.Location = new System.Drawing.Point(504, 17);
            this.verifyBoxVisit1Cost.Name = "verifyBoxVisit1Cost";
            this.verifyBoxVisit1Cost.NewLine = false;
            this.verifyBoxVisit1Cost.Size = new System.Drawing.Size(42, 23);
            this.verifyBoxVisit1Cost.TabIndex = 17;
            this.verifyBoxVisit1Cost.TextV = "";
            // 
            // panelHari
            // 
            this.panelHari.Controls.Add(this.verifyBoxHariCost);
            this.panelHari.Controls.Add(this.verifyBoxKyuDenCount);
            this.panelHari.Controls.Add(this.label41);
            this.panelHari.Controls.Add(this.label35);
            this.panelHari.Controls.Add(this.verifyBoxKyuDenCost);
            this.panelHari.Controls.Add(this.verifyBoxHariCount);
            this.panelHari.Controls.Add(this.label9);
            this.panelHari.Controls.Add(this.label15);
            this.panelHari.Controls.Add(this.label34);
            this.panelHari.Controls.Add(this.verifyBoxHeiCost);
            this.panelHari.Controls.Add(this.verifyBoxDenCount);
            this.panelHari.Controls.Add(this.verifyBoxHeiDenCount);
            this.panelHari.Controls.Add(this.verifyBoxKyuCount);
            this.panelHari.Controls.Add(this.verifyBoxHariDenCost);
            this.panelHari.Controls.Add(this.verifyBoxHeiCount);
            this.panelHari.Controls.Add(this.label61);
            this.panelHari.Controls.Add(this.label60);
            this.panelHari.Controls.Add(this.label59);
            this.panelHari.Controls.Add(this.label58);
            this.panelHari.Controls.Add(this.label57);
            this.panelHari.Controls.Add(this.label56);
            this.panelHari.Controls.Add(this.label26);
            this.panelHari.Controls.Add(this.verifyBoxKyuCost);
            this.panelHari.Controls.Add(this.verifyBoxHariDenCount);
            this.panelHari.Controls.Add(this.label32);
            this.panelHari.Controls.Add(this.verifyBoxDenCost);
            this.panelHari.Controls.Add(this.verifyBoxHeiDenCost);
            this.panelHari.Controls.Add(this.label33);
            this.panelHari.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelHari.Location = new System.Drawing.Point(0, 559);
            this.panelHari.Name = "panelHari";
            this.panelHari.Size = new System.Drawing.Size(1020, 68);
            this.panelHari.TabIndex = 2;
            // 
            // verifyBoxHariCost
            // 
            this.verifyBoxHariCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHariCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHariCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHariCost.Location = new System.Drawing.Point(8, 18);
            this.verifyBoxHariCost.Name = "verifyBoxHariCost";
            this.verifyBoxHariCost.NewLine = false;
            this.verifyBoxHariCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxHariCost.TabIndex = 6;
            this.verifyBoxHariCost.TextV = "";
            this.verifyBoxHariCost.TextChanged += new System.EventHandler(this.verifyBoxHariCost_TextChanged);
            // 
            // verifyBoxKyuDenCount
            // 
            this.verifyBoxKyuDenCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyuDenCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyuDenCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyuDenCount.Location = new System.Drawing.Point(528, 18);
            this.verifyBoxKyuDenCount.Name = "verifyBoxKyuDenCount";
            this.verifyBoxKyuDenCount.NewLine = false;
            this.verifyBoxKyuDenCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyuDenCount.TabIndex = 22;
            this.verifyBoxKyuDenCount.TextV = "";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(282, 3);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 12);
            this.label41.TabIndex = 14;
            this.label41.Text = "電療";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(558, 3);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 12);
            this.label35.TabIndex = 23;
            this.label35.Text = "鍼灸併用+電療";
            // 
            // verifyBoxKyuDenCost
            // 
            this.verifyBoxKyuDenCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyuDenCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyuDenCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyuDenCost.Location = new System.Drawing.Point(468, 18);
            this.verifyBoxKyuDenCost.Name = "verifyBoxKyuDenCost";
            this.verifyBoxKyuDenCost.NewLine = false;
            this.verifyBoxKyuDenCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxKyuDenCost.TabIndex = 21;
            this.verifyBoxKyuDenCost.TextV = "";
            this.verifyBoxKyuDenCost.TextChanged += new System.EventHandler(this.verifyBoxKyuDenCost_TextChanged);
            // 
            // verifyBoxHariCount
            // 
            this.verifyBoxHariCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHariCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHariCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHariCount.Location = new System.Drawing.Point(68, 18);
            this.verifyBoxHariCount.Name = "verifyBoxHariCount";
            this.verifyBoxHariCount.NewLine = false;
            this.verifyBoxHariCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxHariCount.TabIndex = 7;
            this.verifyBoxHariCount.TextV = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(466, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 20;
            this.label9.Text = "灸+電療";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(98, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 8;
            this.label15.Text = "灸";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 3);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 12);
            this.label34.TabIndex = 5;
            this.label34.Text = "針";
            // 
            // verifyBoxHeiCost
            // 
            this.verifyBoxHeiCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHeiCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHeiCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHeiCost.Location = new System.Drawing.Point(192, 18);
            this.verifyBoxHeiCost.Name = "verifyBoxHeiCost";
            this.verifyBoxHeiCost.NewLine = false;
            this.verifyBoxHeiCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxHeiCost.TabIndex = 12;
            this.verifyBoxHeiCost.TextV = "";
            this.verifyBoxHeiCost.TextChanged += new System.EventHandler(this.verifyBoxHeiCost_TextChanged);
            // 
            // verifyBoxDenCount
            // 
            this.verifyBoxDenCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDenCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDenCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDenCount.Location = new System.Drawing.Point(344, 18);
            this.verifyBoxDenCount.Name = "verifyBoxDenCount";
            this.verifyBoxDenCount.NewLine = false;
            this.verifyBoxDenCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDenCount.TabIndex = 16;
            this.verifyBoxDenCount.TextV = "";
            // 
            // verifyBoxHeiDenCount
            // 
            this.verifyBoxHeiDenCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHeiDenCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHeiDenCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHeiDenCount.Location = new System.Drawing.Point(620, 18);
            this.verifyBoxHeiDenCount.Name = "verifyBoxHeiDenCount";
            this.verifyBoxHeiDenCount.NewLine = false;
            this.verifyBoxHeiDenCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxHeiDenCount.TabIndex = 25;
            this.verifyBoxHeiDenCount.TextV = "";
            // 
            // verifyBoxKyuCount
            // 
            this.verifyBoxKyuCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyuCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyuCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyuCount.Location = new System.Drawing.Point(160, 18);
            this.verifyBoxKyuCount.Name = "verifyBoxKyuCount";
            this.verifyBoxKyuCount.NewLine = false;
            this.verifyBoxKyuCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyuCount.TabIndex = 10;
            this.verifyBoxKyuCount.TextV = "";
            // 
            // verifyBoxHariDenCost
            // 
            this.verifyBoxHariDenCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHariDenCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHariDenCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHariDenCost.Location = new System.Drawing.Point(376, 18);
            this.verifyBoxHariDenCost.Name = "verifyBoxHariDenCost";
            this.verifyBoxHariDenCost.NewLine = false;
            this.verifyBoxHariDenCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxHariDenCost.TabIndex = 18;
            this.verifyBoxHariDenCost.TextV = "";
            this.verifyBoxHariDenCost.TextChanged += new System.EventHandler(this.verifyBoxHariDenCost_TextChanged);
            // 
            // verifyBoxHeiCount
            // 
            this.verifyBoxHeiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHeiCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHeiCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHeiCount.Location = new System.Drawing.Point(252, 18);
            this.verifyBoxHeiCount.Name = "verifyBoxHeiCount";
            this.verifyBoxHeiCount.NewLine = false;
            this.verifyBoxHeiCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxHeiCount.TabIndex = 13;
            this.verifyBoxHeiCount.TextV = "";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label61.Location = new System.Drawing.Point(560, 44);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(69, 12);
            this.label61.TabIndex = 26;
            this.label61.Text = "1,550 (1,540)";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label60.Location = new System.Drawing.Point(468, 44);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(69, 12);
            this.label60.TabIndex = 26;
            this.label60.Text = "1,330 (1,300)";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label59.Location = new System.Drawing.Point(376, 44);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(69, 12);
            this.label59.TabIndex = 26;
            this.label59.Text = "1,330 (1,300)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label58.Location = new System.Drawing.Point(284, 44);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(17, 12);
            this.label58.TabIndex = 26;
            this.label58.Text = "30";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label57.Location = new System.Drawing.Point(192, 44);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(69, 12);
            this.label57.TabIndex = 26;
            this.label57.Text = "1,520 (1,510)";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label56.Location = new System.Drawing.Point(100, 44);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(69, 12);
            this.label56.TabIndex = 26;
            this.label56.Text = "1,300 (1,270)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label26.Location = new System.Drawing.Point(8, 44);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(69, 12);
            this.label26.TabIndex = 26;
            this.label26.Text = "1,300 (1,270)";
            // 
            // verifyBoxKyuCost
            // 
            this.verifyBoxKyuCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyuCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyuCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyuCost.Location = new System.Drawing.Point(100, 18);
            this.verifyBoxKyuCost.Name = "verifyBoxKyuCost";
            this.verifyBoxKyuCost.NewLine = false;
            this.verifyBoxKyuCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxKyuCost.TabIndex = 9;
            this.verifyBoxKyuCost.TextV = "";
            this.verifyBoxKyuCost.TextChanged += new System.EventHandler(this.verifyBoxKyuCost_TextChanged);
            // 
            // verifyBoxHariDenCount
            // 
            this.verifyBoxHariDenCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHariDenCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHariDenCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHariDenCount.Location = new System.Drawing.Point(436, 18);
            this.verifyBoxHariDenCount.Name = "verifyBoxHariDenCount";
            this.verifyBoxHariDenCount.NewLine = false;
            this.verifyBoxHariDenCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxHariDenCount.TabIndex = 19;
            this.verifyBoxHariDenCount.TextV = "";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(190, 3);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 11;
            this.label32.Text = "鍼灸併用";
            // 
            // verifyBoxDenCost
            // 
            this.verifyBoxDenCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDenCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDenCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDenCost.Location = new System.Drawing.Point(284, 18);
            this.verifyBoxDenCost.Name = "verifyBoxDenCost";
            this.verifyBoxDenCost.NewLine = false;
            this.verifyBoxDenCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxDenCost.TabIndex = 15;
            this.verifyBoxDenCost.TextV = "";
            this.verifyBoxDenCost.TextChanged += new System.EventHandler(this.verifyBoxDenCost_TextChanged);
            // 
            // verifyBoxHeiDenCost
            // 
            this.verifyBoxHeiDenCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHeiDenCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHeiDenCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHeiDenCost.Location = new System.Drawing.Point(560, 18);
            this.verifyBoxHeiDenCost.Name = "verifyBoxHeiDenCost";
            this.verifyBoxHeiDenCost.NewLine = false;
            this.verifyBoxHeiDenCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxHeiDenCost.TabIndex = 24;
            this.verifyBoxHeiDenCost.TextV = "";
            this.verifyBoxHeiDenCost.TextChanged += new System.EventHandler(this.verifyBoxHeiDenCost_TextChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(374, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(47, 12);
            this.label33.TabIndex = 17;
            this.label33.Text = "針+電療";
            // 
            // panelAnma
            // 
            this.panelAnma.Controls.Add(this.verifyBoxPartCountCount);
            this.panelAnma.Controls.Add(this.label48);
            this.panelAnma.Controls.Add(this.label5);
            this.panelAnma.Controls.Add(this.label10);
            this.panelAnma.Controls.Add(this.label4);
            this.panelAnma.Controls.Add(this.label8);
            this.panelAnma.Controls.Add(this.label49);
            this.panelAnma.Controls.Add(this.label6);
            this.panelAnma.Controls.Add(this.label38);
            this.panelAnma.Controls.Add(this.label51);
            this.panelAnma.Controls.Add(this.label37);
            this.panelAnma.Controls.Add(this.verifyBoxPartCost);
            this.panelAnma.Controls.Add(this.label31);
            this.panelAnma.Controls.Add(this.verifyBoxPartCount);
            this.panelAnma.Controls.Add(this.verifyBoxEleWarmCount);
            this.panelAnma.Controls.Add(this.verifyBoxWarmCount);
            this.panelAnma.Controls.Add(this.verifyBoxToshuPartCount);
            this.panelAnma.Controls.Add(this.label47);
            this.panelAnma.Controls.Add(this.verifyBoxToshuCount);
            this.panelAnma.Controls.Add(this.label44);
            this.panelAnma.Controls.Add(this.label63);
            this.panelAnma.Controls.Add(this.label62);
            this.panelAnma.Controls.Add(this.label11);
            this.panelAnma.Controls.Add(this.verifyBoxLeftLowerCount);
            this.panelAnma.Controls.Add(this.verifyBoxBodyCost);
            this.panelAnma.Controls.Add(this.verifyBoxEleWarmCost);
            this.panelAnma.Controls.Add(this.verifyBoxWarmCost);
            this.panelAnma.Controls.Add(this.verifyBoxToshuCost);
            this.panelAnma.Controls.Add(this.verifyBoxBodyCount);
            this.panelAnma.Controls.Add(this.verifyBoxLeftLowerCost);
            this.panelAnma.Controls.Add(this.label14);
            this.panelAnma.Controls.Add(this.label30);
            this.panelAnma.Controls.Add(this.verifyBoxRightUpperCost);
            this.panelAnma.Controls.Add(this.verifyBoxRightLowerCount);
            this.panelAnma.Controls.Add(this.verifyBoxRightUpperCount);
            this.panelAnma.Controls.Add(this.verifyBoxRightLowerCost);
            this.panelAnma.Controls.Add(this.label20);
            this.panelAnma.Controls.Add(this.label22);
            this.panelAnma.Controls.Add(this.verifyBoxLeftUpperCost);
            this.panelAnma.Controls.Add(this.verifyBoxLeftUpperCount);
            this.panelAnma.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAnma.Location = new System.Drawing.Point(0, 627);
            this.panelAnma.Name = "panelAnma";
            this.panelAnma.Size = new System.Drawing.Size(1020, 68);
            this.panelAnma.TabIndex = 3;
            // 
            // verifyBoxPartCountCount
            // 
            this.verifyBoxPartCountCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPartCountCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPartCountCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPartCountCount.Location = new System.Drawing.Point(94, 18);
            this.verifyBoxPartCountCount.Name = "verifyBoxPartCountCount";
            this.verifyBoxPartCountCount.NewLine = false;
            this.verifyBoxPartCountCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxPartCountCount.TabIndex = 3;
            this.verifyBoxPartCountCount.TextV = "";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label48.Location = new System.Drawing.Point(683, 44);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(17, 12);
            this.label48.TabIndex = 0;
            this.label48.Text = "回";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label5.Location = new System.Drawing.Point(608, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "単価";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label10.Location = new System.Drawing.Point(148, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 17;
            this.label10.Text = "単価";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label4.Location = new System.Drawing.Point(24, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 17;
            this.label4.Text = "単価";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(197, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "回";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label49.Location = new System.Drawing.Point(222, 44);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(84, 12);
            this.label49.TabIndex = 0;
            this.label49.Text = "「0」で単価コピー";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label6.Location = new System.Drawing.Point(99, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "回";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(800, 3);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 12);
            this.label38.TabIndex = 26;
            this.label38.Text = "温庵電気";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(590, 3);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(53, 12);
            this.label51.TabIndex = 23;
            this.label51.Text = "変形徒手";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(708, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(29, 12);
            this.label37.TabIndex = 23;
            this.label37.Text = "温庵";
            // 
            // verifyBoxPartCost
            // 
            this.verifyBoxPartCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPartCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPartCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPartCost.Location = new System.Drawing.Point(8, 18);
            this.verifyBoxPartCost.Name = "verifyBoxPartCost";
            this.verifyBoxPartCost.NewLine = false;
            this.verifyBoxPartCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxPartCost.TabIndex = 1;
            this.verifyBoxPartCost.TextV = "";
            this.verifyBoxPartCost.TextChanged += new System.EventHandler(this.verifyBoxPartCost_TextChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(498, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 16;
            this.label31.Text = "左下肢";
            // 
            // verifyBoxPartCount
            // 
            this.verifyBoxPartCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPartCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPartCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPartCount.Location = new System.Drawing.Point(68, 18);
            this.verifyBoxPartCount.Name = "verifyBoxPartCount";
            this.verifyBoxPartCount.NewLine = false;
            this.verifyBoxPartCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxPartCount.TabIndex = 2;
            this.verifyBoxPartCount.TextV = "";
            // 
            // verifyBoxEleWarmCount
            // 
            this.verifyBoxEleWarmCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxEleWarmCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxEleWarmCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxEleWarmCount.Location = new System.Drawing.Point(862, 18);
            this.verifyBoxEleWarmCount.Name = "verifyBoxEleWarmCount";
            this.verifyBoxEleWarmCount.NewLine = false;
            this.verifyBoxEleWarmCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxEleWarmCount.TabIndex = 28;
            this.verifyBoxEleWarmCount.TextV = "";
            // 
            // verifyBoxWarmCount
            // 
            this.verifyBoxWarmCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxWarmCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxWarmCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxWarmCount.Location = new System.Drawing.Point(770, 18);
            this.verifyBoxWarmCount.Name = "verifyBoxWarmCount";
            this.verifyBoxWarmCount.NewLine = false;
            this.verifyBoxWarmCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxWarmCount.TabIndex = 25;
            this.verifyBoxWarmCount.TextV = "";
            // 
            // verifyBoxToshuPartCount
            // 
            this.verifyBoxToshuPartCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxToshuPartCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxToshuPartCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxToshuPartCount.Location = new System.Drawing.Point(652, 18);
            this.verifyBoxToshuPartCount.Name = "verifyBoxToshuPartCount";
            this.verifyBoxToshuPartCount.NewLine = false;
            this.verifyBoxToshuPartCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxToshuPartCount.TabIndex = 21;
            this.verifyBoxToshuPartCount.TextV = "";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label47.Location = new System.Drawing.Point(651, 44);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(29, 12);
            this.label47.TabIndex = 0;
            this.label47.Text = "局所";
            // 
            // verifyBoxToshuCount
            // 
            this.verifyBoxToshuCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxToshuCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxToshuCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxToshuCount.Location = new System.Drawing.Point(678, 18);
            this.verifyBoxToshuCount.Name = "verifyBoxToshuCount";
            this.verifyBoxToshuCount.NewLine = false;
            this.verifyBoxToshuCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxToshuCount.TabIndex = 22;
            this.verifyBoxToshuCount.TextV = "";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label44.Location = new System.Drawing.Point(67, 44);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(29, 12);
            this.label44.TabIndex = 0;
            this.label44.Text = "局所";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label63.Location = new System.Drawing.Point(802, 44);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(23, 12);
            this.label63.TabIndex = 26;
            this.label63.Text = "110";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label62.Location = new System.Drawing.Point(710, 44);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(17, 12);
            this.label62.TabIndex = 26;
            this.label62.Text = "80";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "局所数指定";
            // 
            // verifyBoxLeftLowerCount
            // 
            this.verifyBoxLeftLowerCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxLeftLowerCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxLeftLowerCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxLeftLowerCount.Location = new System.Drawing.Point(560, 18);
            this.verifyBoxLeftLowerCount.Name = "verifyBoxLeftLowerCount";
            this.verifyBoxLeftLowerCount.NewLine = false;
            this.verifyBoxLeftLowerCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxLeftLowerCount.TabIndex = 18;
            this.verifyBoxLeftLowerCount.TextV = "";
            // 
            // verifyBoxBodyCost
            // 
            this.verifyBoxBodyCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBodyCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBodyCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBodyCost.Location = new System.Drawing.Point(132, 18);
            this.verifyBoxBodyCost.Name = "verifyBoxBodyCost";
            this.verifyBoxBodyCost.NewLine = false;
            this.verifyBoxBodyCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxBodyCost.TabIndex = 5;
            this.verifyBoxBodyCost.TextV = "";
            this.verifyBoxBodyCost.TextChanged += new System.EventHandler(this.verifyBoxBodyCost_TextChanged);
            // 
            // verifyBoxEleWarmCost
            // 
            this.verifyBoxEleWarmCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxEleWarmCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxEleWarmCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxEleWarmCost.Location = new System.Drawing.Point(802, 18);
            this.verifyBoxEleWarmCost.Name = "verifyBoxEleWarmCost";
            this.verifyBoxEleWarmCost.NewLine = false;
            this.verifyBoxEleWarmCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxEleWarmCost.TabIndex = 27;
            this.verifyBoxEleWarmCost.TextV = "";
            this.verifyBoxEleWarmCost.TextChanged += new System.EventHandler(this.verifyBoxEleWarmCost_TextChanged);
            this.verifyBoxEleWarmCost.Leave += new System.EventHandler(this.verifyBoxEleWarmCost_TextChanged);
            // 
            // verifyBoxWarmCost
            // 
            this.verifyBoxWarmCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxWarmCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxWarmCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxWarmCost.Location = new System.Drawing.Point(710, 18);
            this.verifyBoxWarmCost.Name = "verifyBoxWarmCost";
            this.verifyBoxWarmCost.NewLine = false;
            this.verifyBoxWarmCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxWarmCost.TabIndex = 24;
            this.verifyBoxWarmCost.TextV = "";
            this.verifyBoxWarmCost.TextChanged += new System.EventHandler(this.verifyBoxWarmCost_TextChanged);
            this.verifyBoxWarmCost.Leave += new System.EventHandler(this.verifyBoxWarmCost_TextChanged);
            // 
            // verifyBoxToshuCost
            // 
            this.verifyBoxToshuCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxToshuCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxToshuCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxToshuCost.Location = new System.Drawing.Point(592, 18);
            this.verifyBoxToshuCost.Name = "verifyBoxToshuCost";
            this.verifyBoxToshuCost.NewLine = false;
            this.verifyBoxToshuCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxToshuCost.TabIndex = 20;
            this.verifyBoxToshuCost.TextV = "";
            this.verifyBoxToshuCost.TextChanged += new System.EventHandler(this.verifyBoxToshuCost_TextChanged);
            this.verifyBoxToshuCost.Leave += new System.EventHandler(this.verifyBoxToshuCost_TextChanged);
            // 
            // verifyBoxBodyCount
            // 
            this.verifyBoxBodyCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBodyCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBodyCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBodyCount.Location = new System.Drawing.Point(192, 18);
            this.verifyBoxBodyCount.Name = "verifyBoxBodyCount";
            this.verifyBoxBodyCount.NewLine = false;
            this.verifyBoxBodyCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBodyCount.TabIndex = 6;
            this.verifyBoxBodyCount.TextV = "";
            // 
            // verifyBoxLeftLowerCost
            // 
            this.verifyBoxLeftLowerCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxLeftLowerCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxLeftLowerCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxLeftLowerCost.Location = new System.Drawing.Point(500, 18);
            this.verifyBoxLeftLowerCost.Name = "verifyBoxLeftLowerCost";
            this.verifyBoxLeftLowerCost.NewLine = false;
            this.verifyBoxLeftLowerCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxLeftLowerCost.TabIndex = 17;
            this.verifyBoxLeftLowerCost.TextV = "";
            this.verifyBoxLeftLowerCost.TextChanged += new System.EventHandler(this.verifyBoxLeftLowerCost_TextChanged);
            this.verifyBoxLeftLowerCost.Leave += new System.EventHandler(this.verifyBoxLeftLowerCost_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(130, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 4;
            this.label14.Text = "躯幹";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(406, 3);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 13;
            this.label30.Text = "右下肢";
            // 
            // verifyBoxRightUpperCost
            // 
            this.verifyBoxRightUpperCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRightUpperCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRightUpperCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRightUpperCost.Location = new System.Drawing.Point(224, 18);
            this.verifyBoxRightUpperCost.Name = "verifyBoxRightUpperCost";
            this.verifyBoxRightUpperCost.NewLine = false;
            this.verifyBoxRightUpperCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxRightUpperCost.TabIndex = 8;
            this.verifyBoxRightUpperCost.TextV = "";
            this.verifyBoxRightUpperCost.TextChanged += new System.EventHandler(this.verifyBoxRightUpperCost_TextChanged);
            this.verifyBoxRightUpperCost.Leave += new System.EventHandler(this.verifyBoxRightUpperCost_Leave);
            // 
            // verifyBoxRightLowerCount
            // 
            this.verifyBoxRightLowerCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRightLowerCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRightLowerCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRightLowerCount.Location = new System.Drawing.Point(468, 18);
            this.verifyBoxRightLowerCount.Name = "verifyBoxRightLowerCount";
            this.verifyBoxRightLowerCount.NewLine = false;
            this.verifyBoxRightLowerCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRightLowerCount.TabIndex = 15;
            this.verifyBoxRightLowerCount.TextV = "";
            // 
            // verifyBoxRightUpperCount
            // 
            this.verifyBoxRightUpperCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRightUpperCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRightUpperCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRightUpperCount.Location = new System.Drawing.Point(284, 18);
            this.verifyBoxRightUpperCount.Name = "verifyBoxRightUpperCount";
            this.verifyBoxRightUpperCount.NewLine = false;
            this.verifyBoxRightUpperCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRightUpperCount.TabIndex = 9;
            this.verifyBoxRightUpperCount.TextV = "";
            // 
            // verifyBoxRightLowerCost
            // 
            this.verifyBoxRightLowerCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRightLowerCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRightLowerCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRightLowerCost.Location = new System.Drawing.Point(408, 18);
            this.verifyBoxRightLowerCost.Name = "verifyBoxRightLowerCost";
            this.verifyBoxRightLowerCost.NewLine = false;
            this.verifyBoxRightLowerCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxRightLowerCost.TabIndex = 14;
            this.verifyBoxRightLowerCost.TextV = "";
            this.verifyBoxRightLowerCost.TextChanged += new System.EventHandler(this.verifyBoxRightLowerCost_TextChanged);
            this.verifyBoxRightLowerCost.Leave += new System.EventHandler(this.verifyBoxRightLowerCost_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(222, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 7;
            this.label20.Text = "右上肢";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(314, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 10;
            this.label22.Text = "左上肢";
            // 
            // verifyBoxLeftUpperCost
            // 
            this.verifyBoxLeftUpperCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxLeftUpperCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxLeftUpperCost.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxLeftUpperCost.Location = new System.Drawing.Point(316, 18);
            this.verifyBoxLeftUpperCost.Name = "verifyBoxLeftUpperCost";
            this.verifyBoxLeftUpperCost.NewLine = false;
            this.verifyBoxLeftUpperCost.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxLeftUpperCost.TabIndex = 11;
            this.verifyBoxLeftUpperCost.TextV = "";
            this.verifyBoxLeftUpperCost.TextChanged += new System.EventHandler(this.verifyBoxLeftUpperCost_TextChanged);
            this.verifyBoxLeftUpperCost.Leave += new System.EventHandler(this.verifyBoxLeftUpperCost_Leave);
            // 
            // verifyBoxLeftUpperCount
            // 
            this.verifyBoxLeftUpperCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxLeftUpperCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxLeftUpperCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxLeftUpperCount.Location = new System.Drawing.Point(376, 18);
            this.verifyBoxLeftUpperCount.Name = "verifyBoxLeftUpperCount";
            this.verifyBoxLeftUpperCount.NewLine = false;
            this.verifyBoxLeftUpperCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxLeftUpperCount.TabIndex = 12;
            this.verifyBoxLeftUpperCount.TextV = "";
            // 
            // panelFutter
            // 
            this.panelFutter.Controls.Add(this.buttonBack);
            this.panelFutter.Controls.Add(this.buttonRegist);
            this.panelFutter.Controls.Add(this.labelInputerName);
            this.panelFutter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFutter.Location = new System.Drawing.Point(0, 695);
            this.panelFutter.Name = "panelFutter";
            this.panelFutter.Size = new System.Drawing.Size(1020, 26);
            this.panelFutter.TabIndex = 5;
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(735, 2);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 1;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(13, 2);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 2;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(590, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "変形徒手";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // SubInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Name = "SubInputForm";
            this.Text = "OCR Check";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelCommon.ResumeLayout(false);
            this.panelCommon.PerformLayout();
            this.panelHari.ResumeLayout(false);
            this.panelHari.PerformLayout();
            this.panelAnma.ResumeLayout(false);
            this.panelAnma.PerformLayout();
            this.panelFutter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panelCommon;
        private System.Windows.Forms.Panel panelFutter;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxVisit1Count;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label36;
        private VerifyBox verifyBoxVisit1Cost;
        private VerifyBox verifyBoxVisit4Dis;
        private VerifyBox verifyBoxVisit4Count;
        private VerifyBox verifyBoxVisit4Cost;
        private VerifyBox verifyBoxVisit3Dis;
        private VerifyBox verifyBoxVisit3Count;
        private VerifyBox verifyBoxVisit3Cost;
        private VerifyBox verifyBoxVisit2Dis;
        private VerifyBox verifyBoxVisit2Count;
        private VerifyBox verifyBoxVisit2Cost;
        private VerifyBox verifyBoxVisit1Dis;
        private VerifyCheckBox verifyCheckBoxAnbun;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panelHari;
        private VerifyBox verifyBoxHariCost;
        private VerifyBox verifyBoxKyuDenCount;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label35;
        private VerifyBox verifyBoxKyuDenCost;
        private VerifyBox verifyBoxHariCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label34;
        private VerifyBox verifyBoxHeiCost;
        private VerifyBox verifyBoxDenCount;
        private VerifyBox verifyBoxHeiDenCount;
        private VerifyBox verifyBoxKyuCount;
        private VerifyBox verifyBoxHariDenCost;
        private VerifyBox verifyBoxHeiCount;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxKyuCost;
        private VerifyBox verifyBoxHariDenCount;
        private System.Windows.Forms.Label label32;
        private VerifyBox verifyBoxDenCost;
        private VerifyBox verifyBoxHeiDenCost;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panelAnma;
        private VerifyBox verifyBoxPartCountCount;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxPartCost;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxPartCount;
        private VerifyBox verifyBoxEleWarmCount;
        private VerifyBox verifyBoxWarmCount;
        private VerifyBox verifyBoxToshuPartCount;
        private System.Windows.Forms.Label label47;
        private VerifyBox verifyBoxToshuCount;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxLeftLowerCount;
        private VerifyBox verifyBoxBodyCost;
        private VerifyBox verifyBoxEleWarmCost;
        private VerifyBox verifyBoxWarmCost;
        private VerifyBox verifyBoxToshuCost;
        private VerifyBox verifyBoxBodyCount;
        private VerifyBox verifyBoxLeftLowerCost;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label30;
        private VerifyBox verifyBoxRightUpperCost;
        private VerifyBox verifyBoxRightLowerCount;
        private VerifyBox verifyBoxRightUpperCount;
        private VerifyBox verifyBoxRightLowerCost;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private VerifyBox verifyBoxLeftUpperCost;
        private VerifyBox verifyBoxLeftUpperCount;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxNewCost;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxSaiDouiD;
        private VerifyBox verifyBoxSaiDouiY;
        private VerifyBox verifyBoxSaiDouiM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFirst;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private VerifyCheckBox verifyCheckOver5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
    }
}