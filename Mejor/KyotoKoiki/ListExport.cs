﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.KyotoKoiki
{
    class ListExport
    {

        public static bool Export(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var l = new List<string>();
                    //先頭行は見出し
                    l.Add("ID");
                    l.Add("処理年");
                    l.Add("処理月");
                    l.Add("診療年");
                    l.Add("診療月");
                    l.Add("保険者番号");
                    l.Add("被保険者番号");
                    l.Add("氏名");
                    l.Add("性別");
                    l.Add("生年月日");
                    l.Add("郵便番号");
                    l.Add("住所");
                    l.Add("種別");
                    l.Add("請求区分");
                    l.Add("傷病名");
                    l.Add("施術師番号");
                    l.Add("合計金額");
                    l.Add("請求金額");
                    l.Add("診療日数");
                    l.Add("ナンバリング");
                    l.Add("照会理由");
                    l.Add("メモ");

                    sw.WriteLine(string.Join(",", l));

                    var ss = new List<string>();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.InsNum);
                        ss.Add(item.HihoNum);
                        ss.Add(item.PersonName);
                        ss.Add(item.Sex == 1 ? "男" : "女");
                        ss.Add(item.Birthday.ToShortDateString());
                        ss.Add(item.HihoZip);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.AppType.ToString());
                        ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        ss.Add(Fusho.GetShinkyuFushoName(item.FushoName1));
                        ss.Add(item.DrNum);
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Numbering);
                        ss.Add(item.ShokaiReason.ToString().Replace(",", " "));
                        ss.Add(item.Memo);


                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }

    }
}
