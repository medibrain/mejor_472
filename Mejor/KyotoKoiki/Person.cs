﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.KyotoKoiki
{
    class Person
    {
        [DB.DbAttribute.PrimaryKey]
        public string HihoNum { get; set; } = string.Empty;
        //public string InsNum { get; set; } = string.Empty;
        //public DateTime Birth { get; set; } = DateTimeEx.DateTimeNull;
        //public int Sex { get; set; }
        public string Name { get; set; } = string.Empty;
        public int LastAID { get; set; }
        public bool Verified { get; set; } = false;

        public static Person Select(string num)
        {
            return DB.Main.Select<Person>(new { hihonum = num }).FirstOrDefault();
        }

        private bool insert()
        {
            return DB.Main.Insert(this);
        }

        private bool update()
        {
            return DB.Main.Update(this);
        }

        public static bool Upsert(App app, bool forceVerified)
        {
            if (app.HihoNum.Length != 8) return true;
            if (string.IsNullOrEmpty(app.PersonName)) return true;
            if (app.Birthday.IsNullDate()) return true;

            var p = Select(app.HihoNum);
            if (p == null)
            {
                p = new Person();
                p.HihoNum = app.HihoNum;
                //p.InsNum = app.InsNum;
                //p.Birth = app.Birthday;
                //p.Sex = app.Sex;
                p.Name = app.PersonName;
                p.LastAID = app.Aid;
                p.Verified = forceVerified ? true : false;
                return p.insert();
            }
            else
            {
                //if (p.InsNum == app.InsNum &&
                //    p.Name == app.PersonName &&
                //    p.Birth == app.Birthday &&
                //    p.Sex == app.Sex)
                if (p.Name == app.PersonName)
                {
                    if (p.Verified) return true;
                    if (p.LastAID == app.Aid) return true;
                    p.LastAID = app.Aid;
                    p.Verified = true;
                    return p.update();
                }
                else
                {
                    //p.InsNum = app.InsNum;
                    p.Name = app.PersonName;
                    //p.Birth = app.Birthday;
                    //p.Sex = app.Sex;
                    p.Verified = forceVerified ? true : false;
                    p.LastAID = app.Aid;
                    return p.update();
                }
            }
        }
    }
}