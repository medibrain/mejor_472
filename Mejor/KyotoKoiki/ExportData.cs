﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace Mejor.KyotoKoiki
{
    class ExportData
    {
        private App app;
        private AppEx appex;
        private List<App> douiApps =new List<App>();
        private bool hasOryoPage = false;

        private string ymNumbering => app.ChargeYear.ToString("00") +
            app.ChargeMonth.ToString("00") + app.Numbering;

        string dateToStr(DateTime dt) => dt.IsNullDate() ? "" : dt.ToString("yyyy/M/d");

        private int costCountCalc(int cost, int count) =>
            cost == 0 ? 0 : count == 0 ? 0 : count;

        const string header = "ナンバリング,保険者番号,被保険者番号,性別,生年月日,氏名," +
            "診療年月,医療費区分,施術師コード,初検日,開始日,終了日,実日数," +
            "初検料,針単価,針回数,灸単価,灸回数,針灸併用単価,鍼灸併用回数,電療単価,電療回数," +
            "針電療単価,針電療回数,灸電療単価,灸電療回数,針灸併用電療単価,鍼灸併用電療回数," +
            "あんま局所単価,あんま局所数,あんま局所回数,躯幹単価,躯幹回数,右上肢単価,右上肢回数," +
            "左上肢単価,左上肢回数,右下肢単価,右下肢回数,左下肢単価,左下肢回数," +
            "変形徒手単価,変形徒手数,変形徒手回数,温庵単価,温庵回数,温庵電気単価,温庵電気回数," +
            "往療按分,往療別紙,往療5種以上,往療1距離,往療1単価,往療1回数,往療2距離,往療2単価,往療2回数," +
            "往療3距離,往療3単価,往療3回数,往療4距離,往療4単価,往療4回数," +
            "給付割合,請求額,合計額,同意年月日1,同意年月日2,同意年月日3,同意年月日4,同意年月日5," +
            "再同意年月日,同意種類,同意部位,往療同意,メモ";

        const string header2018 = "ナンバリング,診療年月,被保険者番号," +
            "氏名,性別,生年月日,初検年月日,施術開始年月日,施術終了年月日,実日数,合計金額,請求金額";

        public static bool Export(int cym, DateTime exportDt, bool samExport, bool excelExport, bool imageExport)
        {
            string dir, excelName, samName, imageDir;

            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != DialogResult.OK) return true;
            dir = d.Name;

            samName = dir + $"\\JKD08M0010201_KD08F001N.SAM_{DateTime.Today.ToString("yyyyMMdd")}";
            imageDir = dir + "\\image";
            excelName = dir + $"\\京都広域全件リスト{ cym}.xlsx";

            if (imageExport) Directory.CreateDirectory(imageDir);

            var imageName = string.Empty;
            var images = new List<string>();
            var fc = new TiffUtility.FastCopy();

            using (var wf = new WaitForm())
            {
                try
                {
                    wf.ShowDialogOtherTask();

                    wf.LogPrint("申請書を取得しています");
                    var apps = App.GetApps(cym);
                    wf.LogPrint("申請書追加情報を取得しています");
                    var exDic = AppEx.GetAppExDic(cym);
                    wf.LogPrint("施術師情報を取得しています");
                    var dic = Doctor.GetDoctorDic();
                    wf.LogPrint("ナンバリングのチェックをしています");
                    Sam.CheckNumbering(apps, wf);

                    wf.LogPrint((samExport ? "SAM " : "") + (excelExport ? "Excel " : "") + (imageExport ? "画像 " : "") + "の出力中です");
                    wf.SetMax(apps.Count);
                    wf.BarStyle = ProgressBarStyle.Continuous;

                    var book = excelExport ? new XSSFWorkbook() : null;
                    var sheet = book?.CreateSheet("全件リスト");

                    using (var fs = samExport ? new FileStream(samName, FileMode.Create) : null)
                    {
                        int count = 0;
                        if (excelExport) writeExcelHeader(sheet, cym);

                        var b = Sam.CreateHeader();
                        if (samExport) fs.Write(b, 0, b.Length);

                        for (int i = 0; i < apps.Count; i++)
                        {
                            wf.InvokeValue++;
                            var app = apps[i];

                            if (app.StatusFlagCheck(StatusFlag.支払保留))
                            {
                                wf.LogPrint($"支払保留に指定されているため、出力から除外しました AID:{app.Aid}");
                                images.Clear();
                                continue;
                            }

                            //SAM出力
                            //続紙画像
                            if (app.AppType == APP_TYPE.同意書 || app.AppType == APP_TYPE.続紙 ||
                                app.AppType == APP_TYPE.往療内訳 || app.AppType == APP_TYPE.長期)
                            {
                                images.Add(app.GetImageFullPath());
                            }
                            else if (app.AppType == APP_TYPE.鍼灸 || app.AppType == APP_TYPE.あんま)
                            {
                                //画像出力
                                if (imageExport && images.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                                    TiffUtility.MargeOrCopyTiff(fc, images, imageName);

                                count++;
                                images.Clear();
                                images.Add(app.GetImageFullPath());

                                //20200514095110 furukawa st ////////////////////////
                                //京都広域用のレセプト管理番号の内訳仕様変更に伴い、画像ファイル名を西暦yymmm+打番とする
                                
                                imageName = imageDir + "\\" + app.CYM.ToString().Substring(2,4) + app.Numbering + ".tif";

                                //imageName = imageDir + "\\" + app.ChargeYear.ToString("00") +
                                //    app.ChargeMonth.ToString("00") + app.Numbering + ".tif";
                                //20200514095110 furukawa ed ////////////////////////


                                //SAMデータ出力 返戻は飛ばし
                                if (samExport && !app.StatusFlagCheck(StatusFlag.返戻))
                                {
                                    var sum = new Sam(app, count);
                                    b = sum.CreateRecord(dic, exportDt, wf);
                                    if (b.Length != 1000) wf.LogPrint($"バイト数の確認が必要です AID:{app.Aid}");
                                    fs.Write(b, 0, b.Length);
                                }
                            }

                            //Excel
                            if (!excelExport || app.YM < 0) continue;

                            var before2018 = cym < 201804;
                            AppEx appEx = null;
                            if (before2018)
                            {
                                if (!exDic.ContainsKey(app.Aid))
                                {
                                    wf.LogPrint($"AID[{app.Aid}]の追加情報が見つかりませんでした");
                                }
                                else
                                {
                                    appEx = exDic[app.Aid];
                                }
                            }

                            var le = new ExportData(app, appEx);

                            //同意書、往療内訳を取得
                            for (int j = i + 1; j < apps.Count; j++)
                            {
                                if (apps[j].YM > 0) break;
                                if (apps[j].AppType == APP_TYPE.同意書) le.douiApps.Add(apps[j]);
                                if (apps[j].AppType == APP_TYPE.往療内訳) le.hasOryoPage = true;
                            }
                            le.douiApps.Sort((x, y) => y.FushoFirstDate1.CompareTo(x.FushoFirstDate1));

                            if (before2018)
                            {
                                le.writeExcelLine(sheet);
                                if (le.douiApps.Count == 0)
                                {
                                    wf.LogPrint($"同意書が見つかりませんでした　AID:{app.Aid}　ナンバリング:{app.Numbering}");
                                }
                            }
                            else
                            {
                                le.writeExcelLine2018(sheet);
                            }
                        }

                        //最終画像出力
                        if (imageExport && images.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                            TiffUtility.MargeOrCopyTiff(fc, images, imageName);

                        //SAMファイルフッタ書き込み
                        b = Sam.CreateFooter(count);
                        if (samExport) fs.Write(b, 0, b.Length);

                        //Excelファイル保存
                        if (excelExport)
                        {
                            using (var efs = new FileStream(excelName, FileMode.Create))
                            {
                                book.Write(efs);
                            }
                            book.Close();
                        }
                    }

                    wf.LogPrint("出力が完了しました");
                    return true;
                }
                catch (Exception ex)
                {
                    wf.LogPrint($"エラーが発生しました: {ex.Message}");
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
                finally
                {
                    var log = Path.GetDirectoryName(excelName);
                    log += $"\\Export{DateTime.Now.ToString("yyyyMMddHHmmss")}.log";
                    wf.LogSave(log);
                }
            }
        }

        public ExportData(App app, AppEx appEx)
        {
            this.app = app;
            this.appex = appEx;
        }

        private string douiDt1 => douiApps.Count < 1 ? "" :
            douiApps[0].FushoFirstDate1 == DateTime.MinValue ? "" : douiApps[0].FushoFirstDate1.ToString("yyyy/M/d");
        private string douiDt2 => douiApps.Count < 2 ? "" :
            douiApps[1].FushoFirstDate1 == DateTime.MinValue ? "" : douiApps[1].FushoFirstDate1.ToString("yyyy/M/d");
        private string douiDt3 => douiApps.Count < 3 ? "" :
            douiApps[2].FushoFirstDate1 == DateTime.MinValue ? "" : douiApps[2].FushoFirstDate1.ToString("yyyy/M/d");
        private string douiDt4 => douiApps.Count < 4 ? "" :
            douiApps[3].FushoFirstDate1 == DateTime.MinValue ? "" : douiApps[3].FushoFirstDate1.ToString("yyyy/M/d");
        private string douiDt5 => douiApps.Count < 5 ? "" :
            douiApps[4].FushoFirstDate1 == DateTime.MinValue ? "" : douiApps[4].FushoFirstDate1.ToString("yyyy/M/d");

        private string douiType => (douiApps.Count == 0 ? "" :
            app.AppType == APP_TYPE.鍼灸 ? "" :
            app.AppType == APP_TYPE.あんま ?
                (douiApps[0].FushoName1.Length != 0 ? douiApps[0].FushoName1 + " " : "") + douiApps[0].FushoName2 :
            string.Empty).TrimEnd();

        private string anmaDouiBui => douiApps.Count == 0 ? "" :
            app.AppType == APP_TYPE.鍼灸 ?
                (douiApps[0].FushoName1.Length != 0 ? $"{Fusho.GetShinkyuFushoName(douiApps[0].FushoName1)} " : "") +
                (douiApps[0].FushoName2.Length != 0 ? $"{Fusho.GetShinkyuFushoName(douiApps[0].FushoName2)} " : "") +
                (douiApps[0].FushoName3.Length != 0 ? $"{Fusho.GetShinkyuFushoName(douiApps[0].FushoName3)} " : "") +
                (douiApps[0].FushoName4.Length != 0 ? $"{Fusho.GetShinkyuFushoName(douiApps[0].FushoName4)} " : "") +
                (douiApps[0].FushoName5.Length != 0 ? $"{Fusho.GetShinkyuFushoName(douiApps[0].FushoName5)} " : "") :
            (((douiApps[0].Bui / 10000 % 10 == 1) ? "体幹 " : "") +
            ((douiApps[0].Bui / 1000 % 10 == 1) ? "右上肢 " : "") +
            ((douiApps[0].Bui / 100 % 10 == 1) ? "左上肢 " : "") +
            ((douiApps[0].Bui / 10 % 10 == 1) ? "右下肢 " : "") +
            ((douiApps[0].Bui % 10 == 1) ? "左下肢 " : "")).TrimEnd();

        private string douiOryo => douiApps.Count == 0 ? "" :
            douiApps[0].Distance == 999 ? "○" : "";

        private string memo => app.Memo;

        private static void writeExcelHeader(ISheet sheet, int cym)
        {
            var hs = (cym < 201800) ? header.Split(',') : header2018.Split(',');
            var row = sheet.CreateRow(0);

            for (int i = 0; i < hs.Length; i++)
            {
                row.CreateCell(i).SetCellValue(hs[i]);
            }
        }

        private void writeExcelLine(ISheet sheet)
        {
            var rowIndex = sheet.LastRowNum;
            var row = sheet.CreateRow(rowIndex + 1);
            int columnIndex = 0;

            void writeStrCell(string value)
            {
                if (!string.IsNullOrEmpty(value)) row.CreateCell(columnIndex).SetCellValue(value);
                columnIndex++;
            }

            void writeIntCell(int value)
            {
                if (value != 0) row.CreateCell(columnIndex).SetCellValue(value);
                columnIndex++;
            }

            void writeIntCell1000(int value)
            {
                if (value != 0) row.CreateCell(columnIndex).SetCellValue((double)value / 1000d);
                columnIndex++;
            }

            //ナンバリング,保険者番号,被保険者番号,性別,生年月日,氏名,
            writeStrCell(ymNumbering);
            writeStrCell(app.InsNum);
            writeStrCell(app.HihoNum);
            writeIntCell(app.Sex);
            writeStrCell(dateToStr(app.Birthday));
            writeStrCell(app.PersonName);

            //診療年月,医療費区分,施術師コード,初検日,開始日,終了日,実日数, 
            writeIntCell(app.YM);
            writeStrCell(app.AppType.ToString());
            writeStrCell(app.DrNum);
            writeStrCell(dateToStr(app.FushoFirstDate1));
            writeStrCell(dateToStr(app.FushoStartDate1));
            writeStrCell(dateToStr(app.FushoFinishDate1));
            writeIntCell(app.CountedDays);

            //初検料,針単価,針回数,灸単価,灸回数,針灸併用単価,鍼灸併用回数,電療単価,電療回数,
            writeIntCell(appex?.ShokenPrice ?? 0);
            writeIntCell(appex?.hariCost ?? 0);
            writeIntCell(costCountCalc(appex?.hariCost ?? 0, appex?.hariCount ?? 0));
            writeIntCell(appex?.kyuCost ?? 0);
            writeIntCell(costCountCalc(appex?.kyuCost ?? 0, appex?.kyuCount ?? 0));
            writeIntCell(appex?.heiCost ?? 0);
            writeIntCell(costCountCalc(appex?.heiCost ?? 0, appex?.heiCount ?? 0));
            writeIntCell(appex?.denCost ?? 0);
            writeIntCell(costCountCalc(appex?.denCost ?? 0, appex?.denCount ?? 0));

            //針電療単価,針電療回数,灸電療単価,灸電療回数,針灸併用電療単価,鍼灸併用電療回数,
            writeIntCell(appex?.hariDenCost ?? 0);
            writeIntCell(costCountCalc(appex?.hariDenCost ?? 0, appex?.hariDenCount ?? 0));
            writeIntCell(appex?.kyuDenCost ?? 0);
            writeIntCell(costCountCalc(appex?.kyuDenCost ?? 0, appex?.kyuDenCount ?? 0));
            writeIntCell(appex?.heiDenCost ?? 0);
            writeIntCell(costCountCalc(appex?.heiDenCost ?? 0, appex?.heiDenCount ?? 0));

            //あんま局所単価,あんま局所数,あんま局所回数,躯幹単価,躯幹回数,右上肢単価,右上肢回数,
            writeIntCell(appex?.PartCost ?? 0);
            writeIntCell(costCountCalc(appex?.PartCost ?? 0, appex?.PartCount ?? 0));
            writeIntCell(costCountCalc(appex?.PartCost ?? 0, appex?.PartCountCount ?? 0));
            writeIntCell(appex?.BodyCost ?? 0);
            writeIntCell(costCountCalc(appex?.BodyCost ?? 0, appex?.BodyCount ?? 0));
            writeIntCell(appex?.RightUpperCost ?? 0);
            writeIntCell(costCountCalc(appex?.RightUpperCost ?? 0, appex?.RightUpperCount ?? 0));

            //左上肢単価,左上肢回数,右下肢単価,右下肢回数,左下肢単価,左下肢回数,
            writeIntCell(appex?.LeftUpperCost ?? 0);
            writeIntCell(costCountCalc(appex?.LeftUpperCost ?? 0, appex?.LeftUpperCount ?? 0));
            writeIntCell(appex?.RightLowerCost ?? 0);
            writeIntCell(costCountCalc(appex?.RightLowerCost ?? 0, appex?.RightLowerCount ?? 0));
            writeIntCell(appex?.LeftLowerCost ?? 0);
            writeIntCell(costCountCalc(appex?.LeftLowerCost ?? 0, appex?.LeftLowerCount ?? 0));

            //変形徒手単価,変形徒手数,変形徒手回数,温庵単価,温庵回数,温庵電気単価,温庵電気回数,
            writeIntCell(appex?.ToshuCost ?? 0);
            writeIntCell(costCountCalc(appex?.ToshuCost ?? 0, appex?.ToshuPartCount ?? 0));
            writeIntCell(costCountCalc(appex?.ToshuCost ?? 0, appex?.ToshuCount ?? 0));
            writeIntCell(appex?.WarmCost ?? 0);
            writeIntCell(costCountCalc(appex?.WarmCost ?? 0, appex?.WarmCount ?? 0));
            writeIntCell(appex?.EleWarmCost ?? 0);
            writeIntCell(costCountCalc(appex?.EleWarmCost ?? 0, appex?.EleWarmCount ?? 0));

            //往療按分,往療別紙,往療5種以上,往療1距離,往療1単価,往療1回数,往療2距離,往療2単価,往療2回数,
            writeStrCell(appex?.VisitAnbun ?? false ? "○" : "");
            writeStrCell(hasOryoPage ? "○" : "");
            writeStrCell(appex?.VisitOver5 ?? false ? "○" : "");
            writeIntCell1000(appex?.Visit1Dis ?? 0);
            writeIntCell(appex?.Visit1Cost ?? 0);
            writeIntCell(appex?.Visit1Count ?? 0);
            writeIntCell1000(appex?.Visit2Dis ?? 0);
            writeIntCell(appex?.Visit2Cost ?? 0);
            writeIntCell(appex?.Visit2Count ?? 0);

            //往療3距離,往療3単価,往療3回数,往療4距離,往療4単価,往療4回数,
            writeIntCell1000(appex?.Visit3Dis ?? 0);
            writeIntCell(appex?.Visit3Cost ?? 0);
            writeIntCell(appex?.Visit3Count ?? 0);
            writeIntCell1000(appex?.Visit4Dis ?? 0);
            writeIntCell(appex?.Visit4Cost ?? 0);
            writeIntCell(appex?.Visit4Count ?? 0);

            //給付割合,請求額,合計額,同意年月日1,同意年月日2,同意年月日3,同意年月日4,同意年月日5,"
            writeIntCell(app.Ratio);
            writeIntCell(app.Charge);
            writeIntCell(app.Total);
            writeStrCell(douiDt1);
            writeStrCell(douiDt2);
            writeStrCell(douiDt3);
            writeStrCell(douiDt4);
            writeStrCell(douiDt5);

            //再同意年月日,同意種類,同意部位,往療同意
            writeStrCell(dateToStr(appex?.DouiDt ?? DateTimeEx.DateTimeNull));
            writeStrCell(douiType);
            writeStrCell(anmaDouiBui);
            writeStrCell(douiOryo);

            //メモ
            writeStrCell(memo);
        }

        private void writeExcelLine2018(ISheet sheet)
        {
            var rowIndex = sheet.LastRowNum;
            var row = sheet.CreateRow(rowIndex + 1);
            int columnIndex = 0;

            void writeStrCell(string value)
            {
                if (!string.IsNullOrEmpty(value)) row.CreateCell(columnIndex).SetCellValue(value);
                columnIndex++;
            }

            void writeIntCell(int value)
            {
                if (value != 0) row.CreateCell(columnIndex).SetCellValue(value);
                columnIndex++;
            }

            //ナンバリング,診療年月,被保険者番号,
            writeStrCell(ymNumbering);
            writeIntCell(app.YM);
            writeStrCell(app.HihoNum);

            //氏名,性別,生年月日,
            writeStrCell(app.PersonName);
            writeIntCell(app.Sex);
            writeStrCell(dateToStr(app.Birthday));

            //初検年月日,施術開始年月日,施術終了年月日,
            writeStrCell(dateToStr(app.FushoFirstDate1));
            writeStrCell(dateToStr(app.FushoStartDate1));
            writeStrCell(dateToStr(app.FushoFinishDate1));

            //実日数,合計金額,請求金額
            writeIntCell(app.CountedDays);
            writeIntCell(app.Total);
            writeIntCell(app.Charge);
        }
    }
}
