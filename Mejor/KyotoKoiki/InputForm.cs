﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.KyotoKoiki
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 400);
        Point posDays = new Point(100, 400);
        //Point posDays = new Point(400, 0);
        Point posNewCont = new Point(400, 0);
        Point posOryo = new Point(400, 800);
        Point posCost = new Point(800, 800);
        Point posSaiDoui = new Point(800, 2000);
        Point posDoui = new Point(400, 700);
        Point posTourokuNum = new Point(0, 1900);//20200510150408 furukawa 登録記号番号用座標
                                                 

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls,newContControls, oryoControls, saiDouiControls, douiControls,
            tourokuNumControls;//20200510150441 furukawa登録記号番号用コントロール

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hihoNumControls = new Control[] { verifyBoxNum, };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, checkBoxVisit,checkBoxVisitKasan};//20200514111235 furukawa往料、加算チェックボックスの追加
                                                                                                              


            //20200510150549 furukawa st ////////////////////////
            //コントロールグループを修正
            fushoControls = new Control[] { verifyBoxFirstNengo, verifyBoxFirstY, verifyBoxFirstM, verifyBoxFirstD };
            newContControls = new Control[] { };// verifyBoxFirstY, verifyBoxFirstM, verifyBoxFirstD };
            //20200510150549 furukawa ed ////////////////////////


            oryoControls = new Control[] { };
            saiDouiControls = new Control[] { };
            douiControls = new Control[] { verifyCheckBoxDouiMasserge, verifyCheckBoxDouiToshu,
                verifyBoxDouiFusho1, verifyBoxDouiFusho2, verifyBoxDouiFusho3, verifyBoxDouiFusho4, };

            //20200510150527 furukawa st ////////////////////////
            //登録記号番号用コントロール
            
            tourokuNumControls = new Control[] { verifyBoxTourokuNum, };
            //20200510150527 furukawa ed ////////////////////////


            void func(Control c)
            {
                foreach (Control item in c.Controls)
                {
                    if (item is IVerifiable) item.Enter += item_Enter;
                    func(item);
                }
            }
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
            
            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            setApp((App)bsApp.Current);
            verifyBoxY.Focus();

            //鍼灸あんま切り替え
            if(scan.AppType== APP_TYPE.鍼灸)
            {
                labelDouiType.Visible = false;
                verifyCheckBoxDouiMasserge.Visible = false;
                verifyCheckBoxDouiToshu.Visible = false;
                labelDouiBui.Visible = false;
                verifyCheckBoxAnmaAll.Visible = false;
                verifyCheckBoxBody.Visible = false;
                verifyCheckBoxRightUpper.Visible = false;
                verifyCheckBoxLeftUpper.Visible = false;
                verifyCheckBoxRightLower.Visible = false;
                verifyCheckBoxLeftLower.Visible = false;
                labelDouiVisit.Visible = false;
                verifyCheckBoxDouiVisit.Visible = false;
            }
            else if(scan.AppType== APP_TYPE.あんま)
            {
                labelDouiFusho.Visible = false;
                verifyBoxDouiFusho1.Visible = false;
                verifyBoxDouiFusho2.Visible = false;
                verifyBoxDouiFusho3.Visible = false;
                verifyBoxDouiFusho4.Visible = false;
                labelDouiFushoInfo.Visible = false;
            }
            else
            {
                throw new Exception("鍼灸/あんま以外の入力には対応していません");
            }
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (Control)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;

            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (saiDouiControls.Contains(t)) scrollPictureControl1.ScrollPosition = posSaiDoui;
            else if (douiControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDoui;
            else if (tourokuNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posTourokuNum;//20200510150634 furukawa 登録記号番号用座標
                                                                                                          

        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力チェック
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);
            if (year < 0) year = 0;

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            if (month < 0) month = 0;

            //ナンバリング
            int numbering = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, numbering < 1 || verifyBoxNumbering.Text.Trim().Length != 6);

            //施術師コード
            long.TryParse(verifyBoxDrNum.Text, out long drNum);
            setStatus(verifyBoxDrNum, verifyBoxDrNum.Text.Trim().Length != 10 || drNum < 1);

            //被保険者番号 8文字かつ数字に直せること
            int num = verifyBoxNum.GetIntValue();
            setStatus(verifyBoxNum, num < 1 || verifyBoxNum.Text.Trim().Length != 8);

            //保険者番号
            int insNumHeader = verifyBoxInsNumHeader.GetIntValue();
            setStatus(verifyBoxInsNumHeader, insNumHeader != 3926);
            int insNum = verifyBoxInsNum.GetIntValue();
            setStatus(verifyBoxInsNum, insNum < 1 || verifyBoxInsNum.Text.Trim().Length != 4);

            //氏名
            setStatus(verifyBoxPersonName, verifyBoxPersonName.Text.Trim().Length < 3);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);
            if (sex < 0) sex = 0;

            //生年月日
            var birth = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //20200510120337 furukawa st ////////////////////////
            //令和対応
            

            //初検年月日
            var firstDt = dateCheck(verifyBoxFirstNengo, verifyBoxFirstY, verifyBoxFirstM, verifyBoxFirstD);

            //20200518180138 furukawa st ////////////////////////
            //開始/終了日空欄時対応
            
            //開始日
            int startday = verifyBoxStart.Text == string.Empty ? 0: int.Parse(verifyBoxStart.Text);
            var startDt = dateCheck(verifyBoxY, verifyBoxM, startday);
                  //var startDt = dateCheck(verifyBoxY, verifyBoxM, int.Parse(verifyBoxStart.Text));


            //終了日
            int finishday = verifyBoxFinish.Text == string.Empty ? 0 : int.Parse(verifyBoxFinish.Text);
            var finishDt = dateCheck(verifyBoxY, verifyBoxM, finishday);
                //var finishDt = dateCheck(verifyBoxY, verifyBoxM, int.Parse(verifyBoxFinish.Text));

            //20200518180138 furukawa ed ////////////////////////


                            ////初検年月日
                            ///
                            //var firstDt = dateCheck(4, verifyBoxFirstY, verifyBoxFirstM, verifyBoxFirstD);

                            ////開始日
                            //var startDt = dateCheck(4, year, month, verifyBoxStart);

                            ////終了日
                            //var finishDt = dateCheck(4, year, month, verifyBoxFinish);
            //20200510120337 furukawa ed ////////////////////////


            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);
            if (days < 0) days = 0;

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);
            if (total < 0) total = 0;

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);
            if (charge < 0) charge = 0;


            //20200510144335 furukawa st ////////////////////////
            //京都広域用のレセプト管理番号の内訳仕様変更に伴う登録記号番号追加
            
            string strTourokuNum = verifyBoxTourokuNum.Text.Trim() == string.Empty ? string.Empty.PadLeft(10,'0'): verifyBoxTourokuNum.Text.Trim();
            setStatus(verifyBoxTourokuNum, strTourokuNum == string.Empty || strTourokuNum.Length!=10);
            //20200510144335 furukawa ed ////////////////////////



            //合計と請求から受療区分と給付割合を取得
            int ratio = 0, ratio7 = total * 70 / 100, ratio9 = total * 90 / 100;
            if (total == 0 || charge == 0)
            {
                setStatus(verifyBoxTotal, true);
                setStatus(verifyBoxCharge, true);
            }
            else if (ratio7 - 10 < charge && charge < ratio7 + 10)
            {
                ratio = 7;
                setStatus(verifyBoxTotal, false);
                setStatus(verifyBoxCharge, false);
            }
            else if (ratio9 - 10 < charge && charge < ratio9 + 10)
            {
                ratio = 9;
                setStatus(verifyBoxTotal, false);
                setStatus(verifyBoxCharge, false);
            }
            else
            {
                setStatus(verifyBoxTotal, true);
                setStatus(verifyBoxCharge, true);
            }

            //金額エラー
            if (ratio == 0)
            {
                if (MessageBox.Show("合計金額と請求金額から、給付区分と給付割合を推測できませんでした。" +
                    "給付区分と給付割合は記録されませんが、このまま登録してよろしいですか？",
                    "金額エラー", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) != DialogResult.OK) return false;
            }

            //エラー確認
            if (hasError)
            {
                if (MessageBox.Show("エラー項目があります。このまま登録してよろしいですか？",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) != DialogResult.OK) return false;
            }
            #endregion


            #region appへの反映

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.DrNum = verifyBoxDrNum.Text.Trim();
            app.HihoNum = verifyBoxNum.Text.Trim();
            app.InsNum = verifyBoxInsNumHeader.Text.Trim() + verifyBoxInsNum.Text.Trim();
            app.Ratio = ratio;
            app.Family = ratio == 9 ? 8 : ratio == 7 ? 0 : -1;
            app.PersonName = verifyBoxPersonName.Text.Trim();
            app.Sex = sex;
            app.Birthday = birth;

            app.FushoFirstDate1 = firstDt;
            app.FushoStartDate1 = startDt;
            app.FushoFinishDate1 = finishDt;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.Partial = total - charge > 0 ? total - charge : 0;
            app.AppType = scan.AppType;

            //20200510144126 furukawa st ////////////////////////
            //京都広域用のレセプト管理番号の内訳仕様変更に伴う登録記号番号追加
            
            app.ClinicNum = strTourokuNum;
            //20200510144126 furukawa ed ////////////////////////


            //20200514110144 furukawa st ////////////////////////
            //往料、加算の追加
            
            app.Distance = checkBoxVisit.Checked ? 999 : 0;//往療
            app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;//往療加算あり
            //20200514110144 furukawa ed ////////////////////////

            
            #endregion

            return true;
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkDoui(App app)
        {
            hasError = false;
            
            //同意年月日
            var douiDt1 = dateCheck(4, verifyBoxDouiY, verifyBoxDouiM, verifyBoxDouiD);
            var douiDt2 = DateTimeEx.DateTimeNull;
            var douiDt3 = DateTimeEx.DateTimeNull;

            if (verifyBoxDouiY2.Text.Trim()!=string.Empty ||
                verifyBoxDouiM2.Text.Trim() != string.Empty ||
                verifyBoxDouiD2.Text.Trim() != string.Empty)
            {
                douiDt2 = dateCheck(4, verifyBoxDouiY2, verifyBoxDouiM2, verifyBoxDouiD2);
            }

            if (verifyBoxDouiY3.Text.Trim() != string.Empty ||
                verifyBoxDouiM3.Text.Trim() != string.Empty ||
                verifyBoxDouiD3.Text.Trim() != string.Empty)
            {
                douiDt3 = dateCheck(4, verifyBoxDouiY3, verifyBoxDouiM3, verifyBoxDouiD3);
            }

            if (scan.AppType == APP_TYPE.鍼灸)
            {
                //鍼灸負傷
                int fushoNo1 = verifyBoxDouiFusho1.GetIntValue();
                setStatus(verifyBoxDouiFusho1, fushoNo1 < 1 || 7 < fushoNo1);
                int fushoNo2 = verifyBoxDouiFusho2.GetIntValue();
                setStatus(verifyBoxDouiFusho2, fushoNo2 != (int)VerifyBox.ERROR_CODE.NULL && (fushoNo2 < 1 || 7 < fushoNo2));
                int fushoNo3 = verifyBoxDouiFusho3.GetIntValue();
                setStatus(verifyBoxDouiFusho3, fushoNo3 != (int)VerifyBox.ERROR_CODE.NULL && (fushoNo3 < 1 || 7 < fushoNo3));
                int fushoNo4 = verifyBoxDouiFusho4.GetIntValue();
                setStatus(verifyBoxDouiFusho4, fushoNo4 != (int)VerifyBox.ERROR_CODE.NULL && (fushoNo4 < 1 || 7 < fushoNo4));

                //エラー確認
                if (hasError)
                {
                    if (MessageBox.Show("エラー項目があります。このまま登録してよろしいですか？",
                        "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2) != DialogResult.OK) return false;
                }

                app.FushoName1 = (0 < fushoNo1 && fushoNo1 < 8) ? fushoNo1.ToString() : string.Empty;
                app.FushoName2 = (0 < fushoNo2 && fushoNo2 < 8) ? fushoNo2.ToString() : string.Empty;
                app.FushoName3 = (0 < fushoNo3 && fushoNo3 < 8) ? fushoNo3.ToString() : string.Empty;
                app.FushoName4 = (0 < fushoNo4 && fushoNo4 < 8) ? fushoNo4.ToString() : string.Empty;
            }
            else
            {
                //同意施術
                app.FushoName1 = verifyCheckBoxDouiMasserge.Checked ? "マッサージ" : "";
                app.FushoName2 = verifyCheckBoxDouiToshu.Checked ? "変形徒手" : "";

                //同意部位
                int bui = 0;
                if (verifyCheckBoxBody.Checked) bui += 10000;
                if (verifyCheckBoxRightUpper.Checked) bui += 1000;
                if (verifyCheckBoxLeftUpper.Checked) bui += 100;
                if (verifyCheckBoxRightLower.Checked) bui += 10;
                if (verifyCheckBoxLeftLower.Checked) bui += 1;

                //エラー確認
                if (hasError)
                {
                    if (MessageBox.Show("エラー項目があります。このまま登録してよろしいですか？",
                        "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2) != DialogResult.OK) return false;
                }

                app.Bui = bui;
                app.Distance = verifyCheckBoxDouiVisit.Checked ? 999 : 0;
            }

            app.FushoFirstDate1 = douiDt1;
            app.FushoFirstDate2 = douiDt2;
            app.FushoFirstDate3 = douiDt3;
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "//")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.往療内訳;
                app.AppType = APP_TYPE.往療内訳;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else if (verifyBoxY.Text == "..")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.長期;
                app.AppType = APP_TYPE.長期;
            }
            else if (verifyBoxY.Text == "**")
            {
                resetInputData(app);

                if (!checkDoui(app))
                {
                    focusBack(true);
                    return false;
                }

                app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                app.AppType = APP_TYPE.同意書;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                if (app.AppType > 0)
                {
                    if (!Person.Upsert(app, !firstTime)) return false;
                }

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            if (app == null)
            {
                labelInputerName.Text = "入力1:\r\n入力2";
                return;
            }

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
                if (!firstTime && !app.StatusFlagCheck(StatusFlag.ベリファイ済))
                    verifyBoxInsNumHeader.Text = "3926";
            }
            else
            {
                verifyBoxInsNumHeader.Text = "3926";
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                   
                    scrollPictureControl1.Ratio = verifyBoxY.Text.Trim() == "**" ? 0.3f : 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxY, labelYearInfo, labelH, labelY,
                labelInputerName};

            void visible(bool b)
            {
                verifyBoxM.Visible = b;
                labelM.Visible = b;
            }

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"
                || verifyBoxY.Text == "//" || verifyBoxY.Text == "..")
            {
                //続紙その他
                visible(false);
                panelApp.Visible = false;
                panelDoui.Visible = false;
                panelAppData.Visible = false;
                panelDouiDates.Visible = false;
                scrollPictureControl1.Ratio = 0.4f;
            }
            else if (verifyBoxY.Text == "**")
            {
                //同意書
                visible(false);
                panelApp.Visible = false;
                panelDoui.Visible = true;
                panelAppData.Visible = false;
                panelDouiDates.Visible = true;
                scrollPictureControl1.Ratio = 0.3f;
            }
            else
            {
                //申請書
                visible(true);
                panelApp.Visible = true;
                panelDoui.Visible = false;
                panelAppData.Visible = true;
                panelDouiDates.Visible = false;
                scrollPictureControl1.Ratio = 0.4f;
            }
        }


        /// <summary>
        /// 受療者名ろーど
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxNum_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(verifyBoxPersonName.Text)) return;
            var p = Person.Select(verifyBoxNum.Text.Trim());
            if (p == null || !p.Verified) return;

            verifyBoxPersonName.Text = p.Name;
            verifyBoxSex.Focus();
        }

        private void verifyCheckBoxAnmaAll_CheckedChanged(object sender, EventArgs e)
        {
            if (verifyCheckBoxAnmaAll.Checked)
            {
                verifyCheckBoxBody.Checked = true;
                verifyCheckBoxRightUpper.Checked = true;
                verifyCheckBoxLeftUpper.Checked = true;
                verifyCheckBoxRightLower.Checked = true;
                verifyCheckBoxLeftLower.Checked = true;

                verifyCheckBoxBody.TabStop = false;
                verifyCheckBoxRightUpper.TabStop = false;
                verifyCheckBoxLeftUpper.TabStop = false;
                verifyCheckBoxRightLower.TabStop = false;
                verifyCheckBoxLeftLower.TabStop = false;

            }
            else
            {
                verifyCheckBoxBody.TabStop = true;
                verifyCheckBoxRightUpper.TabStop = true;
                verifyCheckBoxLeftUpper.TabStop = true;
                verifyCheckBoxRightLower.TabStop = true;
                verifyCheckBoxLeftLower.TabStop = true;

                if (verifyCheckBoxBody.Checked &&
                  verifyCheckBoxRightUpper.Checked &&
                  verifyCheckBoxLeftUpper.Checked &&
                  verifyCheckBoxRightLower.Checked &&
                  verifyCheckBoxLeftLower.Checked)
                {
                    verifyCheckBoxBody.Checked = false;
                    verifyCheckBoxRightUpper.Checked = false;
                    verifyCheckBoxLeftUpper.Checked = false;
                    verifyCheckBoxRightLower.Checked = false;
                    verifyCheckBoxLeftLower.Checked = false;
                }
            }
        }

        private void verifyCheckBoxAnma_CheckedChanged(object sender, EventArgs e)
        {
            if (verifyCheckBoxBody.Checked &&
                verifyCheckBoxRightUpper.Checked &&
                verifyCheckBoxLeftUpper.Checked &&
                verifyCheckBoxRightLower.Checked &&
                verifyCheckBoxLeftLower.Checked)
            {
                verifyCheckBoxAnmaAll.Checked = true;
                verifyCheckBoxBody.TabStop = false;
                verifyCheckBoxRightUpper.TabStop = false;
                verifyCheckBoxLeftUpper.TabStop = false;
                verifyCheckBoxRightLower.TabStop = false;
                verifyCheckBoxLeftLower.TabStop = false;
            }
            else
            {
                verifyCheckBoxAnmaAll.Checked = false;
                verifyCheckBoxBody.TabStop = true;
                verifyCheckBoxRightUpper.TabStop = true;
                verifyCheckBoxLeftUpper.TabStop = true;
                verifyCheckBoxRightLower.TabStop = true;
                verifyCheckBoxLeftLower.TabStop = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (saiDouiControls.Contains(t)) posSaiDoui = pos;
            else if (douiControls.Contains(t)) posDoui = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// 入力済みのデータをセットします
        /// </summary>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.AppType == APP_TYPE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.AppType == APP_TYPE.往療内訳)
            {
                setValue(verifyBoxY, "//", firstTime, nv);
            }
            else if (app.AppType == APP_TYPE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else if(app.AppType == APP_TYPE.長期)
            {
                setValue(verifyBoxY, "..", firstTime, nv);
            }
            else if (app.AppType == APP_TYPE.同意書)
            {
                setValue(verifyBoxY, "**", firstTime, nv);

                #region 同意書処理
                //同意年月日
                if (!app.FushoFirstDate1.IsNullDate())
                {
                    setValue(verifyBoxDouiY, DateTimeEx.GetJpYear(app.FushoFirstDate1), firstTime, nv);
                    setValue(verifyBoxDouiM, app.FushoFirstDate1.Month, firstTime, nv);
                    setValue(verifyBoxDouiD, app.FushoFirstDate1.Day, firstTime, nv);
                }
                if (!app.FushoFirstDate2.IsNullDate())
                {
                    setValue(verifyBoxDouiY2, DateTimeEx.GetJpYear(app.FushoFirstDate2), firstTime, nv);
                    setValue(verifyBoxDouiM2, app.FushoFirstDate2.Month, firstTime, nv);
                    setValue(verifyBoxDouiD2, app.FushoFirstDate2.Day, firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxDouiY2, "", firstTime, nv);
                    setValue(verifyBoxDouiM2, "", firstTime, nv);
                    setValue(verifyBoxDouiD2, "", firstTime, nv);
                }
                if (!app.FushoFirstDate3.IsNullDate())
                {
                    setValue(verifyBoxDouiY3, DateTimeEx.GetJpYear(app.FushoFirstDate3), firstTime, nv);
                    setValue(verifyBoxDouiM3, app.FushoFirstDate3.Month, firstTime, nv);
                    setValue(verifyBoxDouiD3, app.FushoFirstDate3.Day, firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxDouiY3, "", firstTime, nv);
                    setValue(verifyBoxDouiM3, "", firstTime, nv);
                    setValue(verifyBoxDouiD3, "", firstTime, nv);
                }

                if (scan.AppType == APP_TYPE.鍼灸)
                {
                    //鍼灸負傷
                    setValue(verifyBoxDouiFusho1, app.FushoName1, firstTime, nv);
                    setValue(verifyBoxDouiFusho2, app.FushoName2, firstTime, nv);
                    setValue(verifyBoxDouiFusho3, app.FushoName3, firstTime, nv);
                    setValue(verifyBoxDouiFusho4, app.FushoName4, firstTime, nv);
                }
                else if (scan.AppType == APP_TYPE.あんま)
                {
                    //同意施術
                    setValue(verifyCheckBoxDouiMasserge, !string.IsNullOrEmpty(app.FushoName1), firstTime, nv);
                    setValue(verifyCheckBoxDouiToshu, !string.IsNullOrEmpty(app.FushoName2), firstTime, nv);

                    //同意部位
                    setValue(verifyCheckBoxBody, app.Bui / 10000 % 10 == 1, firstTime, nv);
                    setValue(verifyCheckBoxRightUpper, app.Bui / 1000 % 10 == 1, firstTime, nv);
                    setValue(verifyCheckBoxLeftUpper, app.Bui / 100 % 10 == 1, firstTime, nv);
                    setValue(verifyCheckBoxRightLower, app.Bui / 10 % 10 == 1, firstTime, nv);
                    setValue(verifyCheckBoxLeftLower, app.Bui % 10 == 1, firstTime, nv);
                    setValue(verifyCheckBoxAnmaAll, app.Bui == 11111, firstTime, nv);

                    //往療同意
                    setValue(verifyCheckBoxDouiVisit, app.Distance > 0, firstTime, nv);
                }
                #endregion

            }
            else
            {
                #region 申請書処理

                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyBoxNumbering, app.Numbering, firstTime, nv);
                setValue(verifyBoxDrNum, app.DrNum, firstTime, nv);
                setValue(verifyBoxNum, app.HihoNum, firstTime, nv);
                setValue(verifyBoxInsNumHeader, app.InsNum.Length > 4 ? app.InsNum.Remove(4) : app.InsNum, firstTime, nv);
                setValue(verifyBoxInsNum, app.InsNum.Length > 4 ? app.InsNum.Substring(4) : string.Empty, firstTime, nv);
                setValue(verifyBoxPersonName, app.PersonName, firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                if (app.Birthday.IsNullDate())
                {
                    setValue(verifyBoxBE, string.Empty, firstTime, nv);
                    setValue(verifyBoxBY, string.Empty, firstTime, nv);
                    setValue(verifyBoxBM, string.Empty, firstTime, nv);
                    setValue(verifyBoxBD, string.Empty, firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                    setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                    setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                    setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);
                }

                if (app.FushoFirstDate1.IsNullDate())
                {
                    //20200510121704 furukawa st ////////////////////////
                    //年号追加
                    
                    setValue(verifyBoxFirstNengo, string.Empty, firstTime, nv);
                    //20200510121704 furukawa ed ////////////////////////

                    setValue(verifyBoxFirstY, string.Empty, firstTime, nv);
                    setValue(verifyBoxFirstM, string.Empty, firstTime, nv);
                    setValue(verifyBoxFirstD, string.Empty, firstTime, nv);
                }
                else
                {
                    //初検日

                    //20200510121527 furukawa st ////////////////////////
                    //年号追加
                    
                    setValue(verifyBoxFirstNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate1),firstTime,nv);
                    //20200510121527 furukawa ed ////////////////////////


                    setValue(verifyBoxFirstY, DateTimeEx.GetJpYear(app.FushoFirstDate1), firstTime, nv);
                    setValue(verifyBoxFirstM, app.FushoFirstDate1.Month, firstTime, nv);
                    setValue(verifyBoxFirstD, app.FushoFirstDate1.Day, firstTime, nv);
                }

                setValue(verifyBoxStart, app.FushoStartDate1.IsNullDate() ? 0 : app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyBoxFinish, app.FushoFinishDate1.IsNullDate() ? 0 : app.FushoFinishDate1.Day, firstTime, nv);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);

                setValue(verifyBoxTotal, app.Total, firstTime, nv);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);

                //20200510142450 furukawa st ////////////////////////
                //京都広域用のレセプト管理番号の内訳仕様変更に伴う追加
                
                setValue(verifyBoxTourokuNum, app.ClinicNum, firstTime, nv);
                //20200510142450 furukawa ed ////////////////////////

                //20200514110627 furukawa st ////////////////////////
                //往料、加算の追加
                
                //往療
                setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);
                //往療料加算
                setValue(checkBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);
                //20200514110627 furukawa ed ////////////////////////
                #endregion

            }

            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
