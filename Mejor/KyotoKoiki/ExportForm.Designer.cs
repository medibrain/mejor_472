﻿namespace Mejor.KyotoKoiki
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExport = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxSam = new System.Windows.Forms.CheckBox();
            this.checkBoxCsv = new System.Windows.Forms.CheckBox();
            this.checkBoxImage = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonExport
            // 
            this.buttonExport.Location = new System.Drawing.Point(260, 102);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(75, 23);
            this.buttonExport.TabIndex = 0;
            this.buttonExport.Text = "出力";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(341, 102);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "データ提出日";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(26, 106);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 19);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "出力データ種類";
            // 
            // checkBoxSam
            // 
            this.checkBoxSam.AutoSize = true;
            this.checkBoxSam.Checked = true;
            this.checkBoxSam.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSam.Location = new System.Drawing.Point(26, 43);
            this.checkBoxSam.Name = "checkBoxSam";
            this.checkBoxSam.Size = new System.Drawing.Size(82, 16);
            this.checkBoxSam.TabIndex = 4;
            this.checkBoxSam.Text = "SAMファイル";
            this.checkBoxSam.UseVisualStyleBackColor = true;
            // 
            // checkBoxCsv
            // 
            this.checkBoxCsv.AutoSize = true;
            this.checkBoxCsv.Checked = true;
            this.checkBoxCsv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCsv.Location = new System.Drawing.Point(126, 43);
            this.checkBoxCsv.Name = "checkBoxCsv";
            this.checkBoxCsv.Size = new System.Drawing.Size(86, 16);
            this.checkBoxCsv.TabIndex = 4;
            this.checkBoxCsv.Text = "Excelファイル";
            this.checkBoxCsv.UseVisualStyleBackColor = true;
            // 
            // checkBoxImage
            // 
            this.checkBoxImage.AutoSize = true;
            this.checkBoxImage.Checked = true;
            this.checkBoxImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxImage.Location = new System.Drawing.Point(230, 43);
            this.checkBoxImage.Name = "checkBoxImage";
            this.checkBoxImage.Size = new System.Drawing.Size(82, 16);
            this.checkBoxImage.TabIndex = 4;
            this.checkBoxImage.Text = "画像ファイル";
            this.checkBoxImage.UseVisualStyleBackColor = true;
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 152);
            this.Controls.Add(this.checkBoxImage);
            this.Controls.Add(this.checkBoxCsv);
            this.Controls.Add(this.checkBoxSam);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonExport);
            this.Name = "ExportForm";
            this.Text = "京都広域出力";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxSam;
        private System.Windows.Forms.CheckBox checkBoxCsv;
        private System.Windows.Forms.CheckBox checkBoxImage;
    }
}