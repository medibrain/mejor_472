﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.KyotoKoiki
{
    class Sam
    {
        App app;
        int number;
        string drNum => app.DrNum.PadLeft(10, '0');

        public Sam(App app, int number)
        {
            this.app = app;
            this.number = number;
        }

        public static byte[] CreateHeader()
        {
            //20200510173132 furukawa st ////////////////////////
            //情報種別を「D16」とする(R2_03ヘッダ・データ・トレイラ仕様（別表１－１）3ｼｰﾄ.xlsx)

            var s = "10000000D16201269999        ";
            //var s = "10000000016201269999        ";
            //20200510173132 furukawa ed ////////////////////////


            s += DateTime.Now.ToString("yyyyMMddHHmmss");
            s = s.PadRight(1000);                       //データレコードと同じ長さにする
            return Encoding.UTF8.GetBytes(s);
        }

        public static byte[] CreateFooter(int count)
        {
            var s = "39999999";
            s += count.ToString("0000000");
            s = s.PadRight(1000);
            return Encoding.UTF8.GetBytes(s);
        }

        public byte[] CreateRecord(Dictionary<string, Doctor> dic, DateTime exportDt, WaitForm wf)
        {
            var bl = new List<byte>();

            void addUTF8(string str) => bl.AddRange(Encoding.UTF8.GetBytes(str));
            void addUTF16(string str) => bl.AddRange(Encoding.BigEndianUnicode.GetBytes(str));
            void addGyymmddUTF8(DateTime dt) => 
                addUTF8((dt.IsNullDate() ? 0 : DateTimeEx.GetIntJpDateWithEraNumber(dt)).ToString("0000000"));


            addUTF8("2" + number.ToString("0000000"));//レコード識別子＋レコード番号

            //20200510103159 furukawa st ////////////////////////
            //京都広域用のレセプト管理番号の内訳仕様変更
            
            //京都広域用レセプト管理番号　登録記号番号10桁＋施術師コード10桁＋受付年月（西暦2桁＋月2桁）＋打番6桁=30桁
            string strReceiptMngNo = 
                app.ClinicNum.PadLeft(10, '0') +
                drNum +
                app.CYM.ToString().Substring(2, 4) +
                app.Numbering.PadLeft(6, '0');

            addUTF8(strReceiptMngNo);  //レセプト管理番号(医療機関番号+ナンバリング)
            //addUTF8((drNum + app.Numbering).PadLeft(30, '0'));  //レセプト管理番号(医療機関番号+ナンバリング)

            //20200510103159 furukawa ed ////////////////////////


            addUTF8(DateTimeEx.GetEraNumberYearFromYYYYMM(app.YM).ToString("000"));     //診療年 
            addUTF8((app.YM % 100).ToString("00"));                                     //診療月
            addUTF8(drNum);                                                             //医療機関 (都道府県番号 +点数表コード + 医療機関等コード)
            addUTF8("1");                                                               //保険種別コード 
            addUTF8(app.Family < 0 ? "00" : app.Family.ToString("00"));                 //区分コード  
            addUTF8(app.InsNum.PadLeft(8, '0').Substring(0, 8));                        //保険者番号  
            addUTF8(app.HihoNum.PadLeft(8, '0').Substring(0, 8));                       //被保険者番号  
            addUTF8((app.Ratio * 10).ToString("000"));                                  //給付割合  
            addUTF8(app.AppType == APP_TYPE.鍼灸 ? "06" :                               //療養費区分コード 
                app.AppType == APP_TYPE.あんま ? "05" :
                throw new Exception("鍼灸/あんま以外の申請書が指定されています"));
            addGyymmddUTF8(app.FushoStartDate1);                    //施術開始年月日 
            addGyymmddUTF8(app.FushoFinishDate1);                   //施術終了年月日 
            addUTF8(new string('0', 30));                           //月別診療年月 年月1,2,3,4,5,6
            addUTF8(app.CountedDays.ToString("00"));                //診療実日数
            addUTF8(new string('0', 12));                           //月別診療実日数 日数1,2,3,4,5,6
            addUTF8(app.Charge.ToString("00000000"));               //請求金額      
            addUTF8(app.Charge.ToString("00000000"));               //決定金額  
            addUTF8(app.Total.ToString("00000000"));                //費用額（合計）  
            addUTF8(new string('0', 48));                           //月別費用金額 金額1,2,3,4,5,6 
            addUTF8(app.Partial.ToString("00000000"));              //負担金額   
            addUTF8(new string('0', 15));                          //食事療養費 (回数  決定金額 標準負担額 標準負担額)
            addUTF8(new string(' ', 15));                          //公費1 スペース部(負担者番号 受給者番号)
            addUTF8(new string('0', 47));                          //公費1 0埋め部(日数 請求金額 決定金額 公費対象負担金額 公費患者負担額 食事回数 食事決定金額 食事標準負担額)
            addUTF8(new string(' ', 15));                          //公費2 スペース部(負担者番号 受給者番号)
            addUTF8(new string('0', 47));                          //公費2 0埋め部(日数 請求金額 決定金額 公費対象負担金額 公費患者負担額 食事回数 食事決定金額 食事標準負担額)
            var dr = dic.ContainsKey(drNum) ? dic[drNum] : null;
            if (dr == null) wf.LogPrint($"施術師情報が見つかりません " +
                $"AID:{app.Aid} ナンバリング:{ app.Numbering} 施術師コード:{app.DrNum}");
            addUTF8(dr == null ? "0" : dr.PayType == Doctor.PAY_TYPE.団体 ? "3" : "2");  //支払先区分　要設定
            addUTF8(new string(' ', 10));                           //支払先医療機関 (都道府県番号 + 点数表コード + 番号)
            addGyymmddUTF8(exportDt);                               //受付日 通常翌月25日
            addUTF8(new string('0', 7));                            //決定年月日
            addUTF8(new string('0', 7));                            //支出年月日
            addUTF8(app.Charge.ToString("00000000"));               //支給決定額  
            addUTF8(new string(' ', 24));         //未使用 (充当額 ,支給調整額 ,支給額 )
            addUTF8(new string(' ', 23));         //申請者 英数部( 郵便番号 電話番号)
            addUTF16(new string('　', 145));      //申請者 漢字部(氏名（漢字） 住所（漢字） 被保険者との関係)
            addUTF8(new string(' ', 22));         //振込先 英数部(金融機関区分コード ,金融機関コード ,金融機関店舗コード ,預金種別コード,口座番号)
            addUTF16(new string('　', 100));      //振込先 漢字部(口座名義人氏名（カナ）)
            addUTF8(new string(' ', 47));         //予備

            return bl.ToArray();
        }

        public static bool CheckNumbering(List<App> list, WaitForm wf)
        {
            var error = false;
            list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            int num1 = 0, num2 = 0, num3 = 0;
            App app1 = null, app2 = null, app3 = null;
            var dic = new Dictionary<string, List<int>>();

            foreach (var item in list)
            {
                if (item.AppType < APP_TYPE.NULL) continue;
                if (!dic.ContainsKey(item.Numbering)) dic.Add(item.Numbering, new List<int>());
                dic[item.Numbering].Add(item.Aid);

                if (!int.TryParse(item.Numbering, out int temp))
                {
                    wf.LogPrint($"数値にできないナンバリングがあります　AID:{item.Aid}　　ナンバリング:{item.Numbering}");
                    error = true;
                }

                app1 = app2;
                app2 = app3;
                app3 = item;
                num1 = num2;
                num2 = num3;
                num3 = temp;

                if (((num1 != 0 && Math.Abs(num2 - num1) > 8) || (num2 != 0 && Math.Abs(num2 - num3) > 8)) && dic.Count>3)
                {
                    wf.LogPrint($"前後との比較で確認の必要なナンバリングがあります　" +
                        $"AID:{app2.Aid}　ナンバリング:{app2.Numbering}　前:{app1.Numbering}　後:{app3.Numbering}");
                    error = true;
                }
            }

            foreach (var item in dic)
            {
                if(item.Value.Count>1)
                {
                    wf.LogPrint($"重複しているナンバリングがあります　" +
                        $"ナンバリング:{item.Key}　AID:{string.Join(",", item.Value)}");
                    error = true;
                }
            }
            return error;
        }
    }
}
