﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.KyotoKoiki
{
    public partial class SubInputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        private AppEx appEx = null;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 400);
        Point posDays = new Point(400, 0);
        Point posNewCont = new Point(400, 0);
        Point posOryo = new Point(400, 800);
        Point posCost = new Point(800, 800);
        Point posSaiDoui = new Point(800, 2000);
        Point posDoui = new Point(400, 700);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, saiDouiControls;

        public SubInputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { };
            hihoNumControls = new Control[] { };
            personControls = new Control[] {  };
            dayControls = new Control[] {  };
            costControls = new Control[] {  };
            fushoControls = new Control[] { verifyBoxHariCost, verifyBoxHariCount, verifyBoxHariDenCost, verifyBoxHariDenCount,
                verifyBoxKyuCost, verifyBoxKyuCount, verifyBoxKyuDenCost, verifyBoxKyuDenCount,
                verifyBoxHeiCost, verifyBoxHeiCount, verifyBoxHeiDenCost, verifyBoxHeiDenCount,
                verifyBoxDenCost, verifyBoxDenCount, verifyBoxPartCost, verifyBoxPartCount, verifyBoxPartCountCount,
                verifyBoxBodyCost, verifyBoxBodyCount,verifyBoxRightUpperCost, verifyBoxRightUpperCount, verifyBoxLeftUpperCost, verifyBoxLeftUpperCount,
                verifyBoxRightLowerCost, verifyBoxRightLowerCount, verifyBoxLeftLowerCost, verifyBoxLeftLowerCount, verifyBoxToshuCost,
                verifyBoxToshuPartCount, verifyBoxToshuCount,
                verifyBoxWarmCost, verifyBoxWarmCount, verifyBoxEleWarmCost, verifyBoxEleWarmCount};
            newContControls = new Control[] { verifyBoxNewCost };
            oryoControls = new Control[] { verifyCheckBoxAnbun, verifyCheckOver5,
                verifyBoxVisit1Dis, verifyBoxVisit1Cost, verifyBoxVisit1Count, verifyBoxVisit2Dis, verifyBoxVisit2Cost, verifyBoxVisit2Count,
                verifyBoxVisit3Dis, verifyBoxVisit3Cost, verifyBoxVisit3Count, verifyBoxVisit4Dis, verifyBoxVisit4Cost, verifyBoxVisit4Count, };
            saiDouiControls = new Control[] { verifyBoxSaiDouiY, verifyBoxSaiDouiM, verifyBoxSaiDouiD };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is IVerifiable) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);
            list = list.FindAll(x => x.YM >= 0);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            setApp((App)bsApp.Current);
            (verifyBoxNewCost.Enabled ? verifyBoxNewCost : verifyBoxSaiDouiY).Focus();

            //鍼灸あんま切り替え
            if (scan.AppType == APP_TYPE.鍼灸)
            {
                panelAnma.Visible = false;
            }
            else if (scan.AppType == APP_TYPE.あんま)
            {
                panelHari.Visible = false;
            }
            else
            {
                throw new Exception("鍼灸/あんま以外の入力には対応していません");
            }
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (Control)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (saiDouiControls.Contains(t)) scrollPictureControl1.ScrollPosition = posSaiDoui;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app, AppEx ax)
        {
            hasError = false;

            //同意年月日
            var douiDt = getDate(4, verifyBoxSaiDouiY, verifyBoxSaiDouiM, verifyBoxSaiDouiD);

            //初検料
            int newCost = verifyBoxNewCost.GetIntValue();
            if (newCost == (int)VerifyBox.ERROR_CODE.NULL) newCost = 0;
            setStatus(verifyBoxNewCost, newCost < 0);

            //単価と回数のチェック用ローカル関数
            void costCountCheck(VerifyBox costBox, out int cost, VerifyBox countBox, out int count)
            {
                cost = costBox.GetIntValue();
                if (cost == (int)VerifyBox.ERROR_CODE.NULL) cost = 0;
                setStatus(costBox, cost < 0);

                count = countBox.GetIntValue();
                if (count == (int)VerifyBox.ERROR_CODE.NULL) count = 0;
                setStatus(countBox, cost > 0 && count <= 0);
            }

            int costHari = 0, countHari = 0, costHariDen = 0, countHariDen = 0, costKyu = 0, countKyu = 0,
                costKyuDen = 0, countKyuDen = 0, costHei = 0, countHei = 0, costHeiDen = 0, countHeiDen = 0,
                costDen = 0, countDen = 0;
            int costPart = 0, countPart = 0, countPartCount = 0, costBody = 0, countBody = 0,
                costRightUpper = 0, countRightUpper = 0, costLeftUpper = 0, countLeftUpper = 0,
                costRightLower = 0, countRightLower = 0, costLeftLower = 0, countLeftLower = 0,
                costToshu = 0, countToshuPart = 0, countToshu = 0, costWarm = 0, countWarm = 0,
                costEleWarm = 0, countEleWarm = 0;


            if (scan.AppType == APP_TYPE.鍼灸)
            {
                //針
                costCountCheck(verifyBoxHariCost, out costHari, verifyBoxHariCount, out countHari);
                //針+電療
                costCountCheck(verifyBoxHariDenCost, out costHariDen, verifyBoxHariDenCount, out countHariDen);
                //灸
                costCountCheck(verifyBoxKyuCost, out costKyu, verifyBoxKyuCount, out countKyu);
                //灸+電療
                costCountCheck(verifyBoxKyuDenCost, out costKyuDen, verifyBoxKyuDenCount, out countKyuDen);
                //併用
                costCountCheck(verifyBoxHeiCost, out costHei, verifyBoxHeiCount, out countHei);
                //併用+電療
                costCountCheck(verifyBoxHeiDenCost, out costHeiDen, verifyBoxHeiDenCount, out countHeiDen);
                //電療
                costCountCheck(verifyBoxDenCost, out costDen, verifyBoxDenCount, out countDen);
            }
            else if (scan.AppType == APP_TYPE.あんま)
            {
                //局所数指定
                costCountCheck(verifyBoxPartCost, out costPart, verifyBoxPartCount, out countPart);
                costCountCheck(verifyBoxPartCost, out costPart, verifyBoxPartCountCount, out countPartCount);
                //躯幹
                costCountCheck(verifyBoxBodyCost, out costBody, verifyBoxBodyCount, out countBody);
                //右上肢
                costCountCheck(verifyBoxRightUpperCost, out costRightUpper, verifyBoxRightUpperCount, out countRightUpper);
                //左上肢
                costCountCheck(verifyBoxLeftUpperCost, out costLeftUpper, verifyBoxLeftUpperCount, out countLeftUpper);
                //右下肢
                costCountCheck(verifyBoxRightLowerCost, out costRightLower, verifyBoxRightLowerCount, out countRightLower);
                //左下肢
                costCountCheck(verifyBoxLeftLowerCost, out costLeftLower, verifyBoxLeftLowerCount, out countLeftLower);
                //変形徒手
                costCountCheck(verifyBoxToshuCost, out costToshu, verifyBoxToshuPartCount, out countToshuPart);
                costCountCheck(verifyBoxToshuCost, out costToshu, verifyBoxToshuCount, out countToshu);
                //温庵
                costCountCheck(verifyBoxWarmCost, out costWarm, verifyBoxWarmCount, out countWarm);
                //温庵電気
                costCountCheck(verifyBoxEleWarmCost, out costEleWarm, verifyBoxEleWarmCount, out countEleWarm);
            }

            //往療のチェック用ローカル関数
            void visitCheck(VerifyBox disBox ,VerifyBox costBox, VerifyBox countBox, out int dis1000, out int cost, out int count)
            {
                dis1000 = disBox.GetInt1000Value();
                if (dis1000 == (int)VerifyBox.ERROR_CODE.NULL) dis1000 = 0;
                setStatus(costBox, dis1000 < 0);

                cost = costBox.GetIntValue();
                if (cost == (int)VerifyBox.ERROR_CODE.NULL) cost = 0;
                setStatus(costBox, cost < 0);

                count = countBox.GetIntValue();
                if (count == (int)VerifyBox.ERROR_CODE.NULL) count = 0;
                setStatus(countBox, count < 0);
            }

            //往療
            visitCheck(verifyBoxVisit1Dis, verifyBoxVisit1Cost, verifyBoxVisit1Count,
                out int v1Dis, out int v1Cost, out int v1Count);
            visitCheck(verifyBoxVisit2Dis, verifyBoxVisit2Cost, verifyBoxVisit2Count,
                out int v2Dis, out int v2Cost, out int v2Count);
            visitCheck(verifyBoxVisit3Dis, verifyBoxVisit3Cost, verifyBoxVisit3Count,
                out int v3Dis, out int v3Cost, out int v3Count);
            visitCheck(verifyBoxVisit4Dis, verifyBoxVisit4Cost, verifyBoxVisit4Count,
                out int v4Dis, out int v4Cost, out int v4Count);

            //値の反映
            ax.ShokenPrice = newCost;
            ax.hariCost = costHari;
            ax.hariCount = countHari;
            ax.hariDenCost = costHariDen;
            ax.hariDenCount = countHariDen;
            ax.kyuCost = costKyu;
            ax.kyuCount = countKyu;
            ax.kyuDenCost = costKyuDen;
            ax.kyuDenCount = countKyuDen;
            ax.heiCost = costHei;
            ax.heiCount = countHei;
            ax.heiDenCost = costHeiDen;
            ax.heiDenCount = countHeiDen;
            ax.denCost = costDen;
            ax.denCount = countDen;
            ax.PartCost = costPart;
            ax.PartCount = countPart;
            ax.PartCountCount = countPartCount;
            ax.BodyCost = costBody;
            ax.BodyCount = countBody;
            ax.RightUpperCost = costRightUpper;
            ax.RightUpperCount = countRightUpper;
            ax.LeftUpperCost = costLeftUpper;
            ax.LeftUpperCount = countLeftUpper;
            ax.RightLowerCost = costRightLower;
            ax.RightLowerCount = countRightLower;
            ax.LeftLowerCost = costLeftLower;
            ax.LeftLowerCount = countLeftLower;
            ax.ToshuCost = costToshu;
            ax.ToshuPartCount = countToshuPart;
            ax.ToshuCount = countToshu;
            ax.WarmCost = costWarm;
            ax.WarmCount = countWarm;
            ax.EleWarmCost = costEleWarm;
            ax.EleWarmCount = countEleWarm;
            ax.DouiDt = douiDt;

            ax.VisitAnbun = verifyCheckBoxAnbun.Checked;
            ax.VisitOver5 = verifyCheckOver5.Checked;

            if (ax.VisitAnbun || ax.VisitOver5)
            {
                ax.Visit1Dis = 0;
                ax.Visit1Cost = 0;
                ax.Visit1Count = 0;
                ax.Visit2Dis = 0;
                ax.Visit2Cost = 0;
                ax.Visit2Count = 0;
                ax.Visit3Dis = 0;
                ax.Visit3Cost = 0;
                ax.Visit3Count = 0;
                ax.Visit4Dis = 0;
                ax.Visit4Cost = 0;
                ax.Visit4Count = 0;
            }
            else
            {
                ax.Visit1Dis = v1Dis;
                ax.Visit1Cost = v1Cost;
                ax.Visit1Count = v1Count;
                ax.Visit2Dis = v2Dis;
                ax.Visit2Cost = v2Cost;
                ax.Visit2Count = v2Count;
                ax.Visit3Dis = v3Dis;
                ax.Visit3Cost = v3Cost;
                ax.Visit3Count = v3Count;
                ax.Visit4Dis = v4Dis;
                ax.Visit4Cost = v4Cost;
                ax.Visit4Count = v4Count;
            }

            app.Distance = ax.VisitAnbun || ax.VisitOver5 || v1Dis != 0 ? 999 : 0;
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;

            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.拡張ベリ済)) return true;

            if (!checkApp(app, appEx))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInputEx : App.UPDATE_TYPE.SecondInputEx;
                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                if (!appEx.Upsert(tran)) return false;

                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            if (app == null)
            {
                labelInputerName.Text = "入力1:\r\n入力2";
                appEx = null;
                return;
            }

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.UfirstEx) +
                "\r\n入力2:  " + User.GetUserName(app.UsecondEx);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }

            //あんま時初検料なし
            var nv = firstTime || !app.StatusFlagCheck(StatusFlag.拡張ベリ済);
            verifyBoxNewCost.Enabled = scan.AppType != APP_TYPE.あんま && nv;

            //Enable再調整
            visitControlEnableAdjust();

            verifyBoxHariCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHariCost.Text) && nv;
            verifyBoxHariDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHariDenCost.Text) && nv;
            verifyBoxKyuCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxKyuCost.Text) && nv;
            verifyBoxKyuDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxKyuDenCost.Text) && nv;
            verifyBoxHeiCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHeiCost.Text) && nv;
            verifyBoxHeiDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHeiDenCost.Text) && nv;
            verifyBoxDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxDenCost.Text) && nv;
            verifyBoxPartCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxPartCost.Text) && nv;
            verifyBoxPartCountCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxPartCost.Text) && nv;
            verifyBoxBodyCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text) && nv;
            verifyBoxRightUpperCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxRightUpperCost.Text) && nv;
            verifyBoxLeftUpperCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxLeftUpperCost.Text) && nv;
            verifyBoxRightLowerCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxRightLowerCost.Text) && nv;
            verifyBoxLeftLowerCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxLeftLowerCost.Text) && nv;
            verifyBoxToshuPartCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxToshuCost.Text) && nv;
            verifyBoxToshuCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxToshuCost.Text) && nv;
            verifyBoxWarmCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxWarmCost.Text) && nv;
            verifyBoxEleWarmCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxEleWarmCost.Text) && nv;

            //画像の表示
            setImage(app);
            if (app.StatusFlagCheck(StatusFlag.拡張入力済)) changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        ///////////////////////////////////////////////////
        // 往療関係のコントロール調整
        ///////////////////////////////////////////////////
        private void verifyCheckBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            visitControlEnableAdjust();   
        }
        
        private void verifyBoxVisitDisControls_TextChanged(object sender, EventArgs e)
        {
            visitControlEnableAdjust();
        }

        private void visitControlEnableAdjust()
        {
            var app = (App)bsApp.Current;
            var nv = firstTime || !app.StatusFlagCheck(StatusFlag.拡張ベリ済);

            verifyCheckBoxAnbun.Enabled = !verifyCheckOver5.Checked && nv;
            verifyCheckOver5.Enabled = !verifyCheckBoxAnbun.Checked && nv;

            if (verifyCheckBoxAnbun.Checked || verifyCheckOver5.Checked)
            {
                verifyBoxVisit1Dis.Enabled = false;
                verifyBoxVisit1Cost.Enabled = false;
                verifyBoxVisit1Count.Enabled = false;
                verifyBoxVisit2Dis.Enabled = false;
                verifyBoxVisit2Cost.Enabled = false;
                verifyBoxVisit2Count.Enabled = false;
                verifyBoxVisit3Dis.Enabled = false;
                verifyBoxVisit3Cost.Enabled = false;
                verifyBoxVisit3Count.Enabled = false;
                verifyBoxVisit4Dis.Enabled = false;
                verifyBoxVisit4Cost.Enabled = false;
                verifyBoxVisit4Count.Enabled = false;
            }
            else
            {
                verifyBoxVisit1Dis.Enabled = true && nv;
                verifyBoxVisit2Dis.Enabled = true && nv;
                verifyBoxVisit3Dis.Enabled = true && nv;
                verifyBoxVisit4Dis.Enabled = true && nv;
                var v1 = !string.IsNullOrWhiteSpace(verifyBoxVisit1Dis.Text) && nv;
                var v2 = !string.IsNullOrWhiteSpace(verifyBoxVisit2Dis.Text) && nv;
                var v3 = !string.IsNullOrWhiteSpace(verifyBoxVisit3Dis.Text) && nv;
                var v4 = !string.IsNullOrWhiteSpace(verifyBoxVisit4Dis.Text) && nv;
                verifyBoxVisit1Cost.Enabled = v1;
                verifyBoxVisit2Cost.Enabled = v2;
                verifyBoxVisit3Cost.Enabled = v3;
                verifyBoxVisit4Cost.Enabled = v4;
                verifyBoxVisit1Count.Enabled = v1;
                verifyBoxVisit2Count.Enabled = v2;
                verifyBoxVisit3Count.Enabled = v3;
                verifyBoxVisit4Count.Enabled = v4;
            }
        }


        ///////////////////////////////////////////////////
        // 鍼灸関係のコントロール調整
        ///////////////////////////////////////////////////
        private void verifyBoxHariCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxHariCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHariCost.Text);
        }

        private void verifyBoxHariDenCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxHariDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHariDenCost.Text);
        }

        private void verifyBoxKyuCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxKyuCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxKyuCost.Text);
        }

        private void verifyBoxKyuDenCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxKyuDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxKyuDenCost.Text);
        }

        private void verifyBoxHeiCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxHeiCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHeiCost.Text);
        }

        private void verifyBoxHeiDenCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxHeiDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxHeiDenCost.Text);
        }

        private void verifyBoxDenCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxDenCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxDenCost.Text);
        }

        ///////////////////////////////////////////////////
        // あんま関係のコントロール調整
        ///////////////////////////////////////////////////
        private void verifyBoxPartCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxPartCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxPartCost.Text);
            verifyBoxPartCountCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxPartCost.Text);
        }

        private void verifyBoxBodyCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxBodyCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text);
        }

        private void verifyBoxRightUpperCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxRightUpperCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxRightUpperCost.Text);
        }

        private void verifyBoxLeftUpperCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxLeftUpperCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxLeftUpperCost.Text);
        }

        private void verifyBoxRightLowerCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxRightLowerCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxRightLowerCost.Text);
        }

        private void verifyBoxLeftLowerCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxLeftLowerCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxLeftLowerCost.Text);
        }

        private void verifyBoxToshuCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxToshuPartCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxToshuCost.Text);
            verifyBoxToshuCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxToshuCost.Text);
        }

        private void verifyBoxWarmCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxWarmCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxWarmCost.Text);
        }

        private void verifyBoxNewCost_TextChanged(object sender, EventArgs e)
        {
            var b = string.IsNullOrWhiteSpace(verifyBoxNewCost.Text);
            verifyBoxSaiDouiY.Enabled = b;
            verifyBoxSaiDouiM.Enabled = b;
            verifyBoxSaiDouiD.Enabled = b;
        }

        private void verifyBoxEleWarmCost_TextChanged(object sender, EventArgs e)
        {
            verifyBoxEleWarmCount.Enabled = !string.IsNullOrWhiteSpace(verifyBoxEleWarmCost.Text);
        }


        ///////////////////////////////////////
        // 単価コピー
        ///////////////////////////////////////
        private void verifyBoxRightUpperCost_Leave(object sender, EventArgs e)
        {
            if (verifyBoxRightUpperCost.Text != "0") return;
            else if (!string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text)) verifyBoxRightUpperCost.Text = verifyBoxBodyCost.Text;
        }

        private void verifyBoxLeftUpperCost_Leave(object sender, EventArgs e)
        {
            if (verifyBoxLeftUpperCost.Text != "0") return;
            else if (!string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text)) verifyBoxLeftUpperCost.Text = verifyBoxBodyCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxRightUpperCost.Text)) verifyBoxLeftUpperCost.Text = verifyBoxRightUpperCost.Text;
        }

        private void verifyBoxRightLowerCost_Leave(object sender, EventArgs e)
        {
            if (verifyBoxRightLowerCost.Text != "0") return;
            else if (!string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text)) verifyBoxRightLowerCost.Text = verifyBoxBodyCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxRightUpperCost.Text)) verifyBoxRightLowerCost.Text = verifyBoxRightUpperCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxLeftUpperCost.Text)) verifyBoxRightLowerCost.Text = verifyBoxLeftUpperCost.Text;
        }
        private void verifyBoxLeftLowerCost_Leave(object sender, EventArgs e)
        {
            if (verifyBoxLeftLowerCost.Text != "0") return;
            else if (!string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text)) verifyBoxLeftLowerCost.Text = verifyBoxBodyCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxRightUpperCost.Text)) verifyBoxLeftLowerCost.Text = verifyBoxRightUpperCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxBodyCost.Text)) verifyBoxLeftLowerCost.Text = verifyBoxBodyCost.Text;
            else if (!string.IsNullOrWhiteSpace(verifyBoxRightLowerCost.Text)) verifyBoxLeftLowerCost.Text = verifyBoxRightLowerCost.Text;
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (saiDouiControls.Contains(t)) posSaiDoui = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// 入力済みのデータをセットします
        /// </summary>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.拡張ベリ済);

            //申請書
            appEx = AppEx.Select(app.Aid) ?? new AppEx(app.Aid);

            textBoxFirst.Text = app.FushoFirstDate1.IsNullDate() ?
                string.Empty : textBoxFirst.Text = DateTimeEx.ToJDateShortStr(app.FushoFirstDate1);

            setValue(verifyBoxNewCost, appEx.ShokenPrice, firstTime, nv);

            if (appEx.DouiDt != DateTime.MinValue)
            {
                setValue(verifyBoxSaiDouiY, DateTimeEx.GetJpYear(appEx.DouiDt), firstTime, nv);
                setValue(verifyBoxSaiDouiM, appEx.DouiDt.Month, firstTime, nv);
                setValue(verifyBoxSaiDouiD, appEx.DouiDt.Day, firstTime, nv);
            }
            else
            {
                setValue(verifyBoxSaiDouiY, string.Empty, firstTime, nv);
                setValue(verifyBoxSaiDouiM, string.Empty, firstTime, nv);
                setValue(verifyBoxSaiDouiD, string.Empty, firstTime, nv);
            }

            if (app.AppType == APP_TYPE.鍼灸)
            {
                setValue(verifyBoxHariCost, appEx.hariCost, firstTime, nv);
                setValue(verifyBoxHariCount, appEx.hariCount, firstTime, nv);
                setValue(verifyBoxHariDenCost, appEx.hariDenCost, firstTime, nv);
                setValue(verifyBoxHariDenCount, appEx.hariDenCount, firstTime, nv);
                setValue(verifyBoxKyuCost, appEx.kyuCost, firstTime, nv);
                setValue(verifyBoxKyuCount, appEx.kyuCount, firstTime, nv);
                setValue(verifyBoxKyuDenCost, appEx.kyuDenCost, firstTime, nv);
                setValue(verifyBoxKyuDenCount, appEx.kyuDenCount, firstTime, nv);
                setValue(verifyBoxHeiCost, appEx.heiCost, firstTime, nv);
                setValue(verifyBoxHeiCount, appEx.heiCount, firstTime, nv);
                setValue(verifyBoxHeiDenCost, appEx.heiDenCost, firstTime, nv);
                setValue(verifyBoxHeiDenCount, appEx.heiDenCount, firstTime, nv);
                setValue(verifyBoxDenCost, appEx.denCost, firstTime, nv);
                setValue(verifyBoxDenCount, appEx.denCount, firstTime, nv);
            }
            else if (app.AppType == APP_TYPE.あんま)
            {
                setValue(verifyBoxPartCost, appEx.PartCost, firstTime, nv);
                setValue(verifyBoxPartCount, appEx.PartCount, firstTime, nv);
                setValue(verifyBoxPartCountCount, appEx.PartCountCount, firstTime, nv);
                setValue(verifyBoxBodyCost, appEx.BodyCost, firstTime, nv);
                setValue(verifyBoxBodyCount, appEx.BodyCount, firstTime, nv);
                setValue(verifyBoxRightUpperCost, appEx.RightUpperCost, firstTime, nv);
                setValue(verifyBoxRightUpperCount, appEx.RightUpperCount, firstTime, nv);
                setValue(verifyBoxLeftUpperCost, appEx.LeftUpperCost, firstTime, nv);
                setValue(verifyBoxLeftUpperCount, appEx.LeftUpperCount, firstTime, nv);
                setValue(verifyBoxRightLowerCost, appEx.RightLowerCost, firstTime, nv);
                setValue(verifyBoxRightLowerCount, appEx.RightLowerCount, firstTime, nv);
                setValue(verifyBoxLeftLowerCost, appEx.LeftLowerCost, firstTime, nv);
                setValue(verifyBoxLeftLowerCount, appEx.LeftLowerCount, firstTime, nv);
                setValue(verifyBoxToshuCost, appEx.ToshuCost, firstTime, nv);
                setValue(verifyBoxToshuPartCount, appEx.ToshuPartCount, firstTime, nv);
                setValue(verifyBoxToshuCount, appEx.ToshuCount, firstTime, nv);
                setValue(verifyBoxWarmCost, appEx.WarmCost, firstTime, nv);
                setValue(verifyBoxWarmCount, appEx.WarmCount, firstTime, nv);
                setValue(verifyBoxEleWarmCost, appEx.EleWarmCost, firstTime, nv);
                setValue(verifyBoxEleWarmCount, appEx.EleWarmCount, firstTime, nv);
            }

            setValue(verifyCheckBoxAnbun, appEx.VisitAnbun, firstTime, nv);
            setValue(verifyCheckOver5, appEx.VisitOver5, firstTime, nv);
            setValue1000(verifyBoxVisit1Dis, appEx.Visit1Dis, firstTime, nv);
            setValue(verifyBoxVisit1Cost, appEx.Visit1Cost, firstTime, nv);
            setValue(verifyBoxVisit1Count, appEx.Visit1Count, firstTime, nv);
            setValue1000(verifyBoxVisit2Dis, appEx.Visit2Dis, firstTime, nv);
            setValue(verifyBoxVisit2Cost, appEx.Visit2Cost, firstTime, nv);
            setValue(verifyBoxVisit2Count, appEx.Visit2Count, firstTime, nv);
            setValue1000(verifyBoxVisit3Dis, appEx.Visit3Dis, firstTime, nv);
            setValue(verifyBoxVisit3Cost, appEx.Visit3Cost, firstTime, nv);
            setValue(verifyBoxVisit3Count, appEx.Visit3Count, firstTime, nv);
            setValue1000(verifyBoxVisit4Dis, appEx.Visit4Dis, firstTime, nv);
            setValue(verifyBoxVisit4Cost, appEx.Visit4Cost, firstTime, nv);
            setValue(verifyBoxVisit4Count, appEx.Visit4Count, firstTime, nv);

            //ベリファイ痔、単価/回数のみの場合、距離に0を補完
            if (!firstTime)
            {
                var zero = new Action<VerifyBox, VerifyBox, VerifyBox>((dis, cost, count) =>
                      {
                          if (!string.IsNullOrWhiteSpace(dis.TextV)) return;
                          if (string.IsNullOrWhiteSpace(cost.TextV) && string.IsNullOrWhiteSpace(count.TextV)) return;
                          dis.TextV = "0";
                      });
                zero(verifyBoxVisit1Dis, verifyBoxVisit1Cost, verifyBoxVisit1Count);
                zero(verifyBoxVisit2Dis, verifyBoxVisit2Cost, verifyBoxVisit2Count);
                zero(verifyBoxVisit3Dis, verifyBoxVisit3Cost, verifyBoxVisit3Count);
                zero(verifyBoxVisit4Dis, verifyBoxVisit4Cost, verifyBoxVisit4Count);
            }

            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            (verifyBoxNewCost.Enabled ? verifyBoxNewCost : verifyBoxSaiDouiY).Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
