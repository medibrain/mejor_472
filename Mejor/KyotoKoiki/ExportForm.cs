﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.KyotoKoiki
{
    public partial class ExportForm : Form
    {
        int cym;
        public ExportForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            this.Text += "  " + cym.ToString();
            int m, d;

            m = DateTimeEx.Int6YmAddMonth(cym, 1);
            d = 25;

            var dt = new DateTime(m / 100, m % 100, d);
            dateTimePicker1.Value = dt;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {

            if(!checkBoxSam.Checked && !checkBoxCsv.Checked && !checkBoxImage.Checked)
            {
                MessageBox.Show("出力項目を選んでください");
                return;
            }

            this.Close();
            ExportData.Export(cym, dateTimePicker1.Value,
                checkBoxSam.Checked, checkBoxCsv.Checked, checkBoxImage.Checked);
        }
    }
}
