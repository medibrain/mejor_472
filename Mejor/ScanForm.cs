﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Mejor
{
    public partial class ScanForm : Form
    {
        int insurerID = 0;


        //20191010183915 furukawa 
        /// <summary>
        /// ファイル名＝ナンバリングではないが、ナンバリングが必要なフラグ           
        /// </summary>
        bool NeedNumbering = false;

        /// <summary>
        /// 突合データとApplicaitonのキーとするため、オリジナルファイル名をtaggeddataに記録しておく保険者
        /// </summary>
        bool keepOrgFileName = false;

        /// <summary>
        /// 画像ファイル名をNumberingとして登録
        /// </summary>
        bool fileNameIsNumbering = false;

        /// <summary>
        /// ナンバリングのハイフンを無視する
        /// </summary>
        bool ignoreHyphen = false;

        /// <summary>
        /// マルチページかどうか
        /// </summary>
        bool isMultipage = false;


        public ScanForm(Insurer ins)
        {
            InitializeComponent();
            this.Text = "画像管理 - " + ins.InsurerName;
            this.insurerID = ins.InsurerID;

            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;

            //20190424113120 furukawa st ////////////////////////
            //コンボからテキストに変更要望

            this.txtG.Text = "5";


                //20190401154005 furukawa st ////////////////////////
                //和暦コンボ追加、デフォルトは令和

                //this.cmbWareki.SelectedIndex = 1;
                //20190401154005 furukawa ed ////////////////////////


            //20190424113120 furukawa ed ////////////////////////


            //20190424092731 furukawa st ////////////////////////
            //本番サーバでない場合、フォーム背景色をThistleにする
            CommonTool.setFormColor(this);
            //20190424092731 furukawa ed ////////////////////////
        }

        #region 終了
        //終了
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region フォルダ個別画像登録

        /// <summary>
        /// フォルダ個別画像登録
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFolderSelect_Click(object sender, EventArgs e)
        {

            int y, m;

            //20190424141619 furukawa st ////////////////////////
            //比較用に西暦の年を取得
            //int todayJY = DateTimeEx.GetJpYear(DateTime.Today);
            int todayY = DateTime.Today.Year;
            int cmpY = 0;
            //20190424141619 furukawa ed ////////////////////////

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }

            //20190424142938 furukawa st ////////////////////////
            //テキストボックスの値から西暦年を取得
            cmpY = DateTimeEx.GetAdYearFromHs(int.Parse(textBoxYear.Text.PadLeft(2,'0') + textBoxMonth.Text.PadLeft(2,'0')));
            //20190424142938 furukawa ed ////////////////////////

            string fn = "";

            //上に移動
            /*
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }*/


            //20190424141759 furukawa st ////////////////////////
            //西暦で比較
            if (cmpY < todayY - 1 || cmpY > todayY + 1)
                //else if (y < todayJY - 1 || y > todayJY + 1)
                //20190424141759 furukawa ed ////////////////////////

            {
                MessageBox.Show("請求年度の指定が不正です\r\n(" + DateTimeEx.GetJpYear(DateTime.Today) + "±1)");
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("請求月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            if (textBoxNote1.Text.Length != 0 && textBoxNote1.Text.Length < 4)
            {
                MessageBox.Show("Note1を確認してください");
                return;
            }


            //20190424143205 furukawa st ////////////////////////
            //西暦で比較したので不要

                //20190401173950 furukawa st ////////////////////////
                //和暦チェック
                //if (!ChkWareki(y, m)) return;
                //20190401173950 furukawa ed ////////////////////////


            //20190424143205 furukawa ed ////////////////////////


            int appTypeNum = 0;
            APP_TYPE appType = APP_TYPE.NULL;

            if (radioButtonJyu.Checked)
            {
                appTypeNum = 1;
                appType = APP_TYPE.柔整;
            }
            else if (radioButtonShinkyu.Checked)
            {
                appTypeNum = 7;
                appType = APP_TYPE.鍼灸;
            }
            else if (radioButtonAnma.Checked)
            {
                appTypeNum = 8;
                appType = APP_TYPE.あんま;
            }

            if (appTypeNum==0)
            {
                var res1 = MessageBox.Show("申請書種類が選択されていません", "",
                 MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return;
            fn = f.Name;

            //using (var f = new FolderBrowserDialog())
            //{
            //    if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            //    fn = f.SelectedPath;
            //}

            //指定されたフォルダが存在するかチェック
            if (!System.IO.Directory.Exists(fn))
            {
                MessageBox.Show("フォルダが存在しません");
                return;
            }


            //20210528154927 furukawa st ////////////////////////
            //tif/tiffだけカウントしないと、取り込む人が不安になる
            
            var fl = System.IO.Directory.GetFiles(fn,"*.tif");
            //          var fl = System.IO.Directory.GetFiles(fn);
            var fcount = fl.Count();

            //tiffでも見る
            if (fcount == 0)
            {
                fl = System.IO.Directory.GetFiles(fn, "*.tiff");
                fcount = fl.Count();
            }
            //20210528154927 furukawa ed ////////////////////////



            if (fcount == 0)
            {
                //20210528155210 furukawa st ////////////////////////
                //わかりやすく
                
                MessageBox.Show("tif/tiff画像ファイルが存在しません");
                //      MessageBox.Show("ファイルが存在しません");
                //20210528155210 furukawa ed ////////////////////////

                return;
            }
            
            //20190208120031 furukawa st ////////////////////////
            //保険者ごとのフォルダ名チェック
            if (!ChkFolderName(fn))return;
            //20190208120031 furukawa ed ////////////////////////


            //画像読み取り確認メッセージ
            var res = MessageBox.Show(
                fcount.ToString() +
                "件の画像の取り込みを開始します。よろしいですか？", "画像取り込み",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.OK) return;

            //取り込み中、プログレスバーの表示
            var wf = new WaitForm();


            //20191128161653 furukawa st ////////////////////////
            //2カ所あるので1カ所にまとめた
            
            InsurerSpecialProcess();

            #region old

            ////突合データとApplicaitonのキーとするため、オリジナルファイル名をtaggeddataに記録しておく保険者
            //var keepOrgFileName = insurerID == (int)InsurerID.OSAKA_KOIKI;




            ////20190414191423 furukawa st ////////////////////////
            ////兵庫広域　画像ファイル名をNumberingとして登録


            //var fileNameIsNumbering =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI ||
            //    insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO ||
            //    insurerID == (int)InsurerID.HYOGO_KOIKI;
            //var ignoreHyphen =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI;


            ///*
            //var fileNameIsNumbering =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI || insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO;
            //var ignoreHyphen =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI;
            //*/
            ////20190414191423 furukawa ed ////////////////////////


            ////島根広域はナンバリングする
            ////ファイル名＝ナンバリングではないが、ナンバリングが必要なフラグ
            //NeedNumbering=
            //    insurerID == (int)InsurerID.SHIMANE_KOIKI;


            ////豊橋国保ナンバリングやっぱいらん 伸作さん2019/10/10

            ////NeedNumbering =
            ////    insurerID == (int)InsurerID.TOYOHASHI_KOKUHO;
            ////20191010194630 furukawa ed ////////////////////////





            ////マルチページモードの確認
            //var isMultipage = false;

            ////20190411112146 furukawa st ////////////////////////
            ////兵庫県後期広域マルチTIFF対応

            ////        if (insurerID == (int)InsurerID.OSAKA_KOIKI)
            ////{


            //switch (insurerID)
            //{
            //    case (int)InsurerID.OSAKA_KOIKI:
            //    case (int)InsurerID.HYOGO_KOIKI:
            //    case (int)InsurerID.HIGASHIOSAKA_KOKUHO://20191113132647 furukawa 東大阪国保マルチTIFF対応

            //        var multipageResult = MessageBox.Show(
            //            "この保険者はマルチページTIFFが含まれると予想されます。\r\nマルチページ対応モードで開始してもよろしいですか？", "確認",
            //            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            //        switch (multipageResult)
            //        {
            //            case DialogResult.Yes:
            //                isMultipage = true;
            //                break;
            //            case DialogResult.No:
            //                isMultipage = false;
            //                break;
            //            default: return;
            //        }
            //        // }

            //        break;

            //}

            ////20190411112146 furukawa ed ////////////////////////


            #endregion


            //20191128161653 furukawa ed ////////////////////////


            //20211012124032 furukawa st ////////////////////////
            //DB速度改善のためcymでパーティションテーブルの作成
            
            if (!CreatePartitionTable(GetCYM()))
            {
                MessageBox.Show("パーティションテーブル作成失敗");
                return;
            }
            //20211012124032 furukawa ed ////////////////////////


            try
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.Max = fcount;
                wf.ShowDialogOtherTask();
                wf.LogPrint("画像取り込み処理を開始しました");

                //フォルダの移動、Scanテーブルの登録、Appテーブルへの登録
                var appTypeStr = appTypeNum == 1 ? string.Empty : appTypeNum.ToString();



                //20191010183751 furukawa st ////////////////////////
                //ナンバリング用引数追加に伴う引数変更


                //20191128170332 furukawa st ////////////////////////
                //オリジナルファイル名保持フラグ追加

                var scanedResult = isMultipage ?
                 Scan.DoScanMultipage(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen, insurerID, keepOrgFileName) :
                 Scan.DoScan(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen, insurerID, keepOrgFileName);



                //      var scanedResult = isMultipage ?
                //          Scan.DoScanMultipage(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen,insurerID) :
                //          Scan.DoScan(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering,NeedNumbering, ignoreHyphen,insurerID);

                //20191128170332 furukawa ed ////////////////////////




                //20190414190533 furukawa st ////////////////////////
                //ファイル名=numbering、ハイフン無視フラグ追加



                //var scanedResult = isMultipage ?
                //    Scan.DoScanMultipage(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf ,fileNameIsNumbering, ignoreHyphen) :
                //    Scan.DoScan(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering, ignoreHyphen);



                /*
                var scanedResult = isMultipage ?
                    Scan.DoScanMultipage(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf) :
                    Scan.DoScan(fn, y, m, textBoxNote1.Text, appTypeStr, appType, wf, fileNameIsNumbering, ignoreHyphen);
                    */

                //20190414190533 furukawa ed ////////////////////////



                //20191010183751 furukawa ed ////////////////////////


                if (!scanedResult)
                {
                    MessageBox.Show("取り込みに失敗しました\r\n" + fn);
                    return;
                }
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("取り込みが終了しました");
        }
        #endregion

        
        /// <summary>
        /// 画像登録時のパラメータ
        /// </summary>
        public class ScanCondition
        {
            public int year;
            public int month;
            public string note1;
            public string note2;
            public int maxProgressValue;
        }


        #region 20190208115057 furukawa  保険者ごとのフォルダ名チェック
        /// <summary>
        /// 20190208115057 furukawa  保険者ごとのフォルダ名チェック
        /// </summary>
        /// <param name="strFolderName">フォルダ名</param>
        /// <returns>false：フォルダ名が規定に沿っていない</returns>
        private bool ChkFolderName(string strFolderName)
        {
            switch(insurerID)
            {
                case (int)InsurerID.SHARP_KENPO:
                    if (strFolderName.IndexOf("071") > 0) return true;
                    if (strFolderName.IndexOf("072") > 0) return true;
                    if (strFolderName.IndexOf("171") > 0) return true;
                    if (strFolderName.IndexOf("172") > 0) return true;
                    MessageBox.Show("画像フォルダは0071,0072,0171,0172で分けてください",
                        Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    break;



                //20200630164544 furukawa st ////////////////////////
                //静岡市あはき用フォルダ名。フォルダ名から設定値を取得するので間違い駄目
                
                case (int)InsurerID.SHIZUOKASHI_AHAKI:
                    if(!System.Text.RegularExpressions.Regex.IsMatch(strFolderName, ".+区(一般|退職家族|退職本人)"))
                    {
                        MessageBox.Show("画像フォルダは○○区一般,○○区退職家族,○○区退職本人で分けてください",
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        return true;
                    }
                    break;
                //20200630164544 furukawa ed ////////////////////////    


                default:

                    return true;

            }
            return false;
        }
        #endregion



        #region 20191128161459 furukawa 保険者別処理フラグたてる

        /// <summary>
        /// 保険者別特別設定
        /// </summary>
        private void InsurerSpecialProcess()
        {

            //突合データとApplicaitonのキーとするため、オリジナルファイル名をtaggeddata->emptytext3に記録しておく保険者
            //ナンバリングを別の用途で使用している場合
            keepOrgFileName =
                insurerID == (int)InsurerID.OSAKA_KOIKI ||
                insurerID == (int)InsurerID.AMAGASAKI_KOKUHO ||     //20200411195245 furukawa 尼崎のオリジナルファイル名を控えておく
                insurerID == (int)InsurerID.SHINAGAWAKU ||          //20210406141827 furukawa ファイル名にハイフンが入るためint型のNumbringに入らないのでこちらに変更
                insurerID == (int)InsurerID.ICHIHARASHI ||          //20210411170855 furukawa 市原市は突合キーが国保連レセプト番号
                insurerID == (int)InsurerID.KASHIWASHI ||           //20210412151313 furukawa 柏市は突合キーが国保連レセプト番号
                insurerID == (int)InsurerID.MATSUDOSHI ||           //20210416163336 furukawa 松戸市追加
                insurerID == (int)InsurerID.SAKAISHI ||             //20210422191836 furukawa 堺市あはき　画像名がレセプト全国共通キーで紐付け
                insurerID == (int)InsurerID.NAKANOKU ||             //20210513165707 furukawa 中野区追加
                insurerID == (int)InsurerID.IBARAKISHI_KOKUHO;      //20210516104954 furukawa 茨木市あはき




            //取り込み画像ファイル名がそのままレセプト全国共通キー（電算管理番号）の場合、ファイル名をNumberingとして登録する


            fileNameIsNumbering =
                insurerID == (int)InsurerID.MIYAZAKI_KOIKI ||
                insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO ||
                insurerID == (int)InsurerID.HYOGO_KOIKI ||
                insurerID == (int)InsurerID.KYOKAIKENPO_HIROSHIMA ||      //20200227092938 furukawa インポートデータと結合させるため、協会けんぽ広島支部のファイル名を取得
                insurerID == (int)InsurerID.MIYAGI_KOKUHO ||              //20200615180816 furukawa ファイル名がレセプト全国共通キー
                insurerID == (int)InsurerID.CHIBASHI_KOKUHO ||              //20200721121608 furukawa 千葉市国保はファイル名が「国保連レセプトキー」
                insurerID == (int)InsurerID.KODAIRASHI ||                      //20210420103036 furukawa 小平市
                insurerID == (int)InsurerID.HYOGO_KOIKI2022;                //20220327131554 furukawa 兵庫広域2022 柔整のみ追加
                                                            


            //20210406141709 furukawa st ////////////////////////
            //ファイル名にハイフンが入るためint型のNumbringに入らない
            //insurerID == (int)InsurerID.SHINAGAWAKU;                    //20210404093935 furukawa インポートデータと結合させるため、品川区のファイル名を取得
            //20210406141709 furukawa ed ////////////////////////


            ignoreHyphen =
                insurerID == (int)InsurerID.MIYAZAKI_KOIKI;


            //[ファイル名＝ナンバリング]ではないが、ナンバリングが必要なフラグ
            NeedNumbering =
                insurerID == (int)InsurerID.SHIMANE_KOIKI;
                                                              


            //マルチTIFF
            switch (insurerID)
            {
                case (int)InsurerID.OSAKA_KOIKI:
                case (int)InsurerID.HYOGO_KOIKI:
                case (int)InsurerID.HIGASHIOSAKA_KOKUHO:
                case (int)InsurerID.SAKAISHI:           //20210423192050 furukawa堺市あはきマルチ
                case (int)InsurerID.IBARAKISHI_KOKUHO:      //20210507094300 furukawa 茨木市（あはきだけ
                case (int)InsurerID.HYOGO_KOIKI2022:        //20220327131711 furukawa 兵庫広域2022マルチtiff柔整
                                                      



                    var multipageResult = MessageBox.Show(
                        "この保険者はマルチページTIFFが含まれると予想されます。\r\nマルチページ対応モードで開始してもよろしいですか？", "確認",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    switch (multipageResult)
                    {
                        case DialogResult.Yes:
                            isMultipage = true;
                            break;
                        case DialogResult.No:
                            isMultipage = false;
                            break;
                        default: return;
                    }
                    break;
            }



   
        }
        #endregion


        //20211012123913 furukawa st ////////////////////////
        //テキストボックスからCYM(請求年月)を作成する

        private int GetCYM()
        {

            int.TryParse(textBoxYear.Text.Trim(), out int y);
            int.TryParse(textBoxMonth.Text.Trim(), out int m);

            int cym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;
            return cym;
        }
        //20211012123913 furukawa ed ////////////////////////


        //20211012113008 furukawa st ////////////////////////
        //DB速度改善のためcymでパーティションテーブルの作成
        //※パーティションテーブルを作るためにはAPplicationテーブルをパーティションテーブルとして再作成する必要あり

        /// <summary>
        /// パーティションテーブル作成
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        private static bool CreatePartitionTable(int _cym)
        {
            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = null;
            try
            {
                //パーティションテーブルの作成

                
                
                switch (Insurer.CurrrentInsurer.InsurerID)
                {
                    //20220222165002 furukawa 兵庫広域、広島広域追加         
                    //20220311150450 furukawa 広島広域2022追加
                    //20220322094448 furukawa 千葉広域あはき追加
                    //20220326131717 furukawa 兵庫広域2022追加
                    //20220421144627 furukawa 鹿児島広域追加
                    //20220425101635 furukawa 奈良広域追加
                    //20220510170618 furukawa 茨城広域追加
                    

                    case (int)InsurerID.OSAKA_KOIKI:
                    case (int)InsurerID.HIROSHIMA_KOIKI:
                    case (int)InsurerID.HYOGO_KOIKI:
                    case (int)InsurerID.HIROSHIMA_KOIKI2022:
                    case (int)InsurerID.CHIBA_KOIKI_AHK:
                    case (int)InsurerID.HYOGO_KOIKI2022:
                    case (int)InsurerID.KAGOSHIMA_KOIKI:
                    case (int)InsurerID.NARA_KOIKI:
                    case (int)InsurerID.IBARAKI_KOIKI:


                        cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS application_{_cym} partition of application for values in ({_cym})", tran);
                        cmd.TryExecuteNonQuery();

                        //appcounter用トリガはapplicationテーブル本体ではなくパーティションじゃないと動作しない
                        string strTrigger =
                            $"DROP TRIGGER IF EXISTS trigger_appcounter_update ON public.application_{_cym}; " +
                            $"CREATE TRIGGER trigger_appcounter_update " +
                            $"BEFORE INSERT OR DELETE OR UPDATE " +
                            $"ON public.application_{_cym} " +
                            $"FOR EACH ROW " +
                            $"EXECUTE FUNCTION public.appcounter_update(); ";
                        
                        cmd = DB.Main.CreateCmd(strTrigger, tran);
                        cmd.TryExecuteNonQuery();



                        cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS application_aux_{_cym} partition of application_aux for values in ({_cym})",tran);
                        cmd.TryExecuteNonQuery();

                        cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS scan_{_cym} partition of scan for values in ({_cym})", tran);
                        cmd.TryExecuteNonQuery();

                        cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS scangroup_{DateTime.Now.ToString("yyyyMMdd")} partition of scangroup for values in ('{DateTime.Now.ToString("yyyy-MM-dd")}')", tran);
                        cmd.TryExecuteNonQuery();

                        cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS refrece_{_cym} partition of refrece for values in ({_cym})", tran);
                        cmd.TryExecuteNonQuery();
                        
                        //一時データなのでやらない方向で
                        //cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS tdata_{_cym} partition of tdata for values in ({_cym})", tran);
                        //cmd.TryExecuteNonQuery();                        
                        //cmd = DB.Main.CreateCmd($"create table IF NOT EXISTS tdata_ahk_{_cym} partition of tdata_ahk for values in ({_cym})", tran);
                        //cmd.TryExecuteNonQuery();

                        break;

                    default: break;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                //20211018152411 furukawa st ////////////////////////
                //コマンドがない場合回避
                
                if (cmd != null)
                    //20211018152411 furukawa ed ////////////////////////
                    cmd.Dispose();
                
            }


        }
        //20211012113008 furukawa ed ////////////////////////



        #region フォルダ一括登録
        /// <summary>
        /// フォルダ一括登録
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllFolder_Click(object sender, EventArgs e)
        {
            int y, m;

            #region 請求年チェック


            //20190424141916 furukawa st ////////////////////////
            //比較用に西暦の年を取得
            //int todayJY = DateTimeEx.GetJpYear(DateTime.Today);
            int todayY = DateTime.Today.Year;
            int cmpY = 0;
            //20190424141916 furukawa ed ////////////////////////

            string fn;
            string[] subFolders;

            int imageFiles = 0;

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }
            else if (y == 99)
            {
                if (MessageBox.Show("請求年度に99年が指定されています。" +
                    "\r\nテスト用としての登録になりますが、よろしいですか？", "テスト登録確認",
                     MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                    != DialogResult.OK) return;
            }

            //20190424143044 furukawa st ////////////////////////
            //テキストボックスの値から西暦年を取得
            cmpY = DateTimeEx.GetAdYearFromHs(int.Parse(textBoxYear.Text.PadLeft(2, '0') + textBoxMonth.Text.PadLeft(2, '0')));
            //20190424143044 furukawa ed ////////////////////////

            //20190424142039 furukawa st ////////////////////////
            //西暦で比較

            if (cmpY < todayY - 1 || cmpY > todayY + 1)
            //else if (y < todayJY - 1 || y > todayJY + 1)
            //20190424142039 furukawa ed ////////////////////////

            {
                MessageBox.Show("請求年度の指定が不正です\r\n(" + DateTimeEx.GetJpYear(DateTime.Today) + "±1)");                
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("請求月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }


            //20190424143253 furukawa st ////////////////////////
            //西暦で比較したので不要

            //20190401173111 furukawa st ////////////////////////
            //和暦チェック
            //if (!ChkWareki(y, m)) return;
            //20190401173111 furukawa ed ////////////////////////

            //20190424143253 furukawa ed ////////////////////////


            #endregion

            #region 申請書タイプ判定

            int appTypeNum = 0;
            APP_TYPE appType = APP_TYPE.NULL;

            if (radioButtonJyu.Checked)
            {
                appTypeNum = 1;
                appType = APP_TYPE.柔整;
            }
            else if (radioButtonShinkyu.Checked)
            {
                appTypeNum = 7;
                appType = APP_TYPE.鍼灸;
            }
            else if (radioButtonAnma.Checked)
            {
                appTypeNum = 8;
                appType = APP_TYPE.あんま;
            }
            else
            {
                MessageBox.Show("申請書種類が選択されていません", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            #endregion

            #region tiff/tifファイル数カウント

            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return;
            fn = f.Name;

            try
            {
                subFolders = System.IO.Directory.GetDirectories(fn, "*", System.IO.SearchOption.AllDirectories);
                foreach (var item in subFolders)
                {

                    //20190208120130 furukawa st ////////////////////////
                    //保険者ごとのフォルダ名チェック
                    if (!ChkFolderName(item.ToString())) return;                
                    //20190208120130 furukawa ed ////////////////////////

                    imageFiles += System.IO.Directory.GetFiles(item, "*", System.IO.SearchOption.TopDirectoryOnly).Count(s =>
                    {
                        var ex = System.IO.Path.GetExtension(s).ToLower();
                        return ex == ".tiff" || ex == ".tif";
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("アクセス権のないフォルダが検出されました\r\n\r\n" + ex,
                    "画像取り込み", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int ct = subFolders.Count();

            if (imageFiles == 0)
            {
                MessageBox.Show("指定されたフォルダには、tif画像の含まれたサブフォルダが存在しません。" +
                    "一括取り込みの際には、指定フォルダ内にサブフォルダが必要です。",
                    "フォルダエラー", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var res = MessageBox.Show(ct + " 個のフォルダ内に " +
                imageFiles + " 個のtiff画像ファイルが検出されました。\r\n" +
                "Note1に各フォルダ名が指定されます。画像の一括取り込みを開始しますか?",
                "画像取り込み", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == System.Windows.Forms.DialogResult.Cancel) return;
            
            #endregion


            //20191128161610 furukawa st ////////////////////////
            //2カ所あるので1カ所にまとめた

            InsurerSpecialProcess();

            #region old
            ////取り込み 画像ファイル名がそのままレセプト全国共通キー（電算管理番号）の場合、ファイル名をNumberingとして登録する

            ////20190414183454 furukawa st ////////////////////////
            ////兵庫広域　画像ファイル名をNumberingとして登録
            //var fileNameIsNumbering =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI ||
            //    insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO ||
            //    insurerID == (int)InsurerID.HYOGO_KOIKI;
            ////insurerID == (int)InsurerID.MIYAZAKI_KOIKI || insurerID == (int)InsurerID.HIGASHIMURAYAMA_KOKUHO;
            ////20190414183454 furukawa ed ////////////////////////


            //var ignoreHyphen =
            //    insurerID == (int)InsurerID.MIYAZAKI_KOIKI;



            //        //20191010194527 furukawa st ////////////////////////
            //        // 豊橋国保ナンバリングやっぱいらん 伸作さん2019/10/10

            //        //bool NeedNumbering =
            //        //    insurerID == (int)InsurerID.TOYOHASHI_KOKUHO;
            //        //20191010194527 furukawa ed ////////////////////////




            ////マルチページモードの確認
            //var isMultipage = false;




            ////20190411105229 furukawa st ////////////////////////
            ////兵庫県後期広域マルチTIFF対応

            ////if (insurerID == (int)InsurerID.OSAKA_KOIKI)
            ////{


            //switch (insurerID)
            //{
            //    case (int)InsurerID.OSAKA_KOIKI:
            //    case (int)InsurerID.HYOGO_KOIKI:
            //    case (int)InsurerID.HIGASHIOSAKA_KOKUHO://20191113132117 furukawa 東大阪国保マルチTIFF対応

            //        var multipageResult = MessageBox.Show(
            //            "この保険者はマルチページTIFFが含まれると予想されます。\r\nマルチページ対応モードで開始してもよろしいですか？", "確認",
            //            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            //        switch (multipageResult)
            //        {
            //            case DialogResult.Yes:
            //                isMultipage = true;
            //                break;
            //            case DialogResult.No:
            //                isMultipage = false;
            //                break;
            //            default: return;
            //        }



            //     break;
            // }

            //// }



            ////20190411105229 furukawa ed ////////////////////////
            #endregion

            //20191128161610 furukawa ed ////////////////////////


            //20211012130320 furukawa st ////////////////////////
            //DB速度改善のためcymでパーティションテーブルの作成
            if (!CreatePartitionTable(GetCYM()))
            {
                MessageBox.Show("パーティションテーブル作成失敗");
                return;
            }
            //20211012130320 furukawa ed ////////////////////////

            var wf = new WaitForm();
            wf.BarStyle = ProgressBarStyle.Continuous;
            wf.ShowDialogOtherTask();

            try
            {
                wf.SetMax(imageFiles);

                //20190424144040 furukawa st ////////////////////////
                //メッセージ間違い

                wf.LogPrint("画像取り込み処理を開始しました  フォルダ数は" + subFolders.Length.ToString() + "です");
                //wf.LogPrint("画像取り込み処理を開始しました  フォルダ数は" + subFolders.ToString() + "です");
                //20190424144040 furukawa ed ////////////////////////


                foreach (var item in subFolders)
                {
                    var fl = System.IO.Directory.GetFiles(item).Where(s =>
                    {
                        var ex = System.IO.Path.GetExtension(s).ToLower();
                        return ex == ".tiff" || ex == ".tif";
                    });
                    
                    //ファイル数をカウント
                    var fcount = fl.Count();

                    //指定されたフォルダにtifファイルが存在するかチェック
                    if (fcount != 0)
                    {
                        wf.LogPrint(item + "より取込みを開始します　tiffファイル数は" + fcount.ToString() + "です");
                        int pos = item.LastIndexOf("\\");
                        string note1 = item.Substring(pos + 1);

                        //フォルダの移動、Scanテーブルの登録、Appテーブルへの登録
                        var appTypeStr = appTypeNum == 1 ? string.Empty : appTypeNum.ToString();




                        //20191010183956 furukawa st ////////////////////////
                        //ナンバリング用引数追加に伴う引数変更


                        //20191128170558 furukawa st ////////////////////////
                        //オリジナルファイル名保持フラグ追加
                        
                        var scanedResult = isMultipage ?
                            Scan.DoScanMultipage(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen, insurerID, keepOrgFileName) :
                            Scan.DoScan(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen, insurerID, keepOrgFileName);


                                //var scanedResult = isMultipage ?
                                //    Scan.DoScanMultipage(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen,insurerID) :
                                //    Scan.DoScan(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, NeedNumbering, ignoreHyphen,insurerID);

                        //20191128170558 furukawa ed ////////////////////////

                                        //20190414190840 furukawa st ////////////////////////
                                        //ファイル名=numbering、ハイフン無視フラグ追加



                                        //var scanedResult = isMultipage ?
                                        //    Scan.DoScanMultipage(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, ignoreHyphen):
                                        //    Scan.DoScan(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, ignoreHyphen);

                                        /*
                                        var scanedResult = isMultipage ?
                                            Scan.DoScanMultipage(item, y, m, note1, appTypeStr, appType, wf) :
                                            Scan.DoScan(item, y, m, note1, appTypeStr, appType, wf, fileNameIsNumbering, ignoreHyphen);
                                            */
                                        //20190414190840 furukawa ed ////////////////////////


                        //20191010183956 furukawa ed ////////////////////////

                        if (!scanedResult)
                        {
                            MessageBox.Show("以下のフォルダで画像取り込みに失敗しました\r\n\r\r\n" + item);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                wf.LogPrint("エラーが発生しました　詳しくはログを確認して下さい");
                Log.ErrorWriteWithMsg(ex);
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("取り込みが終了しました");
            selectScanList();
        }

        #endregion

        #region 処理年月でScanを抽出、表示
        /// <summary>
        /// 処理年月でScanを抽出、表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            selectScanList();
        }
        #endregion


        private void selectScanList()
        {
            int y;
            int m;
            int todayJY = DateTimeEx.GetJpYear(DateTime.Today);

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("請求月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            //20190401155635 furukawa st ////////////////////////
            //新元号対応

            
            int cym = 0;
            if (!ChkWareki(y, m, out cym)) return;


            //int cym = DateTimeEx.GetAdYear(4, y) * 100 + m;


            //20190401155635 furukawa ed ////////////////////////



            var list = Scan.GetScanListYM(cym);
            dataGridView1.DataSource = list;
            dataGridView1.Columns[nameof(Scan.SID)].Width = 40;
            dataGridView1.Columns[nameof(Scan.ScanDate)].Width = 70;
            dataGridView1.Columns[nameof(Scan.Cyear)].Width = 25;
            dataGridView1.Columns[nameof(Scan.Cyear)].HeaderText = "年";
            dataGridView1.Columns[nameof(Scan.Cmonth)].Width = 25;
            dataGridView1.Columns[nameof(Scan.Cmonth)].HeaderText = "月";
            dataGridView1.Columns[nameof(Scan.Note1)].Width = 50;
            dataGridView1.Columns[nameof(Scan.Note2)].Width = 50;
            dataGridView1.Columns[nameof(Scan.Status)].Width = 70;
            dataGridView1.Columns[nameof(Scan.ImageFolder)].Width = 300;


            //20200204180404 furukawa st ////////////////////////
            //削除チェックボックス列
            
            DataGridViewCheckBoxColumn dgvcbc = new DataGridViewCheckBoxColumn();
            dgvcbc.HeaderText = "削除";
            dgvcbc.Name = "削除";
            dgvcbc.Width = 50;
            

            if (dataGridView1.Columns.Contains("削除"))
            {
                //20200304222742 furukawa st ////////////////////////
                //チェックボックスの値がNULLの場合、判定エラーが起きるので初期化
                
                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    r.Cells[0].Value = false;
                }
                //20200304222742 furukawa ed ////////////////////////
                return;
            }
            dataGridView1.Columns.Insert(0, dgvcbc);
            //20200204180404 furukawa ed ////////////////////////

            //20200304222742 furukawa st ////////////////////////
            //チェックボックスの値がNULLの場合、判定エラーが起きるので初期化

            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                r.Cells[0].Value = false;
            }
            //20200304222742 furukawa st ////////////////////////
            //チェックボックスの値がNULLの場合、判定エラーが起きるので初期化

        }





        #region buttonDelete_Click ScanID単位でデータを削除

        /// <summary>
        /// ScanID単位でデータを削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {

            //20191211111228 furukawa st ////////////////////////
            //null対策
            
            if (dataGridView1.Rows.Count <= 0) return;
            //20191211111228 furukawa ed ////////////////////////


            int sid =0 ;
            if(dataGridView1[0, dataGridView1.CurrentCell.RowIndex]!=null)
                sid =(int)dataGridView1[1, dataGridView1.CurrentCell.RowIndex].Value;


            //20190903100419 furukawa st ////////////////////////
            //NOでも削除されるのを修正
            

            if (MessageBox.Show("SID単位でデータを削除します。\r\n\r\n" +
                "この操作は現在このSIDを作業している入力者に致命的な影響を及ぼします。\r\n" +
                "他の入力者がこのSIDを作業していないことを必ず確認してください。\r\n\r\n" +
                "ScanID= " + sid + " のデータを全て削除しますか？", "重大な警告",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {


                //MessageBox.Show("SID単位でデータを削除します。\r\n\r\n" +
                //    "この操作は現在このSIDを作業している入力者に致命的な影響を及ぼします。\r\n" +
                //    "他の入力者がこのSIDを作業していないことを必ず確認してください。\r\n\r\n" +
                //    "ScanID= " + sid + " のデータを全て削除しますか？", "重大な警告",
                //    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                ////Scanテーブル、ScanGroupテーブル、Appテーブルからそれぞれ削除。トランザクション必要か。
                
                
                //20190903100419 furukawa ed ////////////////////////


                try
                {
                    //20200204181041 furukawa st ////////////////////////
                    //数が多いので一括削除処理つける
                    
                    for (int r=0;r<dataGridView1.Rows.Count;r++)
                    {
                        if ((bool)dataGridView1[0, r].Value == true)
                        {
                            sid = (int)dataGridView1[1, r].Value;
                            App.DeleteBySID(sid);
                            ScanGroup.DeleteBySID(sid);
                            Scan.DeleteBySID(sid);

                            //20211007142319 furukawa st ////////////////////////
                            //AUXも同時に削除しないと重複が出る
                            
                            Application_AUX.DeleteByScanID(sid);
                            //20211007142319 furukawa ed ////////////////////////


                            lbldel.Text = $"{sid}を削除しました。";
                            Application.DoEvents();
                        }
                    }



                    //App.DeleteBySID(sid);
                    //ScanGroup.DeleteBySID(sid);
                    //Scan.DeleteBySID(sid);

                    //20200204181041 furukawa ed ////////////////////////

                }
                catch
                {
                    MessageBox.Show("データ削除に失敗しました。", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                MessageBox.Show("ScanID= " + sid + " のデータを全て削除しました。");
            }

            selectScanList();
        }
        #endregion

        #region 和暦チェック  20190401172932 furukawa 

        /// <summary>
        /// 和暦チェック
        /// </summary>
        /// <param name="y">年</param>
        /// <param name="m">月</param>
        /// <param name="cym">処理年月（out）</param>
        /// <returns>不正=false</returns>
        private bool ChkWareki(int y,int m,out int cym)
        {
            cym = 0;
            //20190816120344 furukawa st ////////////////////////
            //チェックをDateTimeExにする
            
            cym = DateTimeEx.GetAdYearFromHs(int.Parse(y.ToString("00") + m.ToString("00"))) * 100 + m;



                            //if (txtG.Text=="5")
                            ////if (cmbWareki.SelectedIndex == 1)
                            //{
                            //    if ((y == 1 && m >= 5) || (y >= 2 && m >= 1 && m <= 12))
                            //    {

                            //        cym = DateTimeEx.GetAdYearFromEraYear(5, y) * 100 + m;

                            //    }
                            //    else if (y == 1 && m <= 4)
                            //    {
                            //        y = 31;
                            //        cym = DateTimeEx.GetAdYearFromEraYear(4, y) * 100 + m;
                            //        //MessageBox.Show("和暦が不正です");
                            //        //return false ;
                            //    }
                            //}

                            //else if (txtG.Text!="5")
                            ////            else if (cmbWareki.SelectedIndex == 0)
                            //{
                            //    if ((y == 31 && m <= 4) || (y <= 30 && m >= 1 && m <= 12))
                            //    {
                            //        cym = DateTimeEx.GetAdYearFromEraYear(4, y) * 100 + m;
                            //    }
                            //    else if (y >= 31 && m >= 5)
                            //    {
                            //        y = 1;
                            //        cym = DateTimeEx.GetAdYearFromEraYear(5, y) * 100 + m;

                            //        //MessageBox.Show("和暦が不正です");
                            //        //return false;

                            //    }
                            //}


            //20190816120344 furukawa ed ////////////////////////



            return true;
        }

        /// <summary>
        /// 和暦チェック
        /// </summary>
        /// <param name="y">年</param>
        /// <param name="m">月</param>
        /// <returns>不正＝False</returns>
        private bool ChkWareki(int y, int m)
        {
            //20190503154853 furukawa st ////////////////////////
            //DateTimeEx.GetAdYearFromHsの引数修正
            int tmpy = DateTimeEx.GetAdYearFromHs(y*100+m);
            //int tmpy = DateTimeEx.GetAdYearFromHs(y);
            //20190503154853 furukawa ed ////////////////////////

            if (txtG.Text == "5")
            //if (cmbWareki.SelectedIndex == 1)
            {
                if ((y == 1 && m >= 5) || (y >= 2 && m >= 1 && m <= 12))
                {
                    return true;
                }
                else if (y == 1 && m <= 4)
                {
                    MessageBox.Show("和暦が不正です");
                    return false;
                }
            }
            else if (txtG.Text != "5")
            //else if (cmbWareki.SelectedIndex == 0)
            {
                if ((y == 31 && m <= 4) || (y <= 30 && m >= 1 && m <= 12))
                {
                    return true;
                }
                else if (y >= 31 && m >= 5)
                {
                    MessageBox.Show("和暦が不正です");
                    return false;

                }
            }
            return false;
            
            
        }

        #endregion



        
        /// <summary>
        /// 20200204181041 furukawa 数が多いので一括削除処理つける
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != 0) return;
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                if (r.Cells[0].Value == null || (bool)r.Cells[0].Value == false) r.Cells[0].Value = true;                
                else r.Cells[0].Value = false;
            }
        }

        /// <summary>
        ///  20200204180318 furukawa 削除一括チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != 0) return;
            if (!dataGridView1.Columns.Contains("削除")) return;
            if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value == null) return;

             if ((bool)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value == true)
            {
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value = false;
                return;
            }
            if ((bool)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value == false)
            {
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value = true;
                return;
            }
        }


        //20200304231537 furukawa st ////////////////////////
        //複数チェックをスペースで
        
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Space) return;
            
            foreach (DataGridViewCell cb in dataGridView1.SelectedCells)
            {
                if (cb.ColumnIndex == 0)
                {

                    if (cb.GetType().Equals(typeof(DataGridViewCheckBoxCell)))
                        cb.Value = !(bool)cb.Value;
                }
            }
        }
        //20200304231537 furukawa ed ////////////////////////
    }
}
