﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20211224095631 furukawa ユーザ権限管理画面を別にした


namespace Mejor
{

    public partial class SettingForm_user : Form
    {
        DataTable dtInsurer = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int dgvCurrentRowIndex = 0;

    

        public SettingForm_user()
        {
            InitializeComponent();

            //20210128102133 furukawa st ////////////////////////
            //背景色変更してわかりやすいように            
            CommonTool.setFormColor(this);
            //20210128102133 furukawa ed ////////////////////////

            disp();

            //20211227155902 furukawa st ////////////////////////
            //複製ボタンは開発者、管理者のみ            
            btncopy.Enabled = User.CurrentUser.Developer || User.CurrentUser.Admin;
            //20211227155902 furukawa ed ////////////////////////
        }



        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {

            if (Settings.DataBaseHost.Contains("aws"))
            {
                //20220214134153 furukawa st ////////////////////////
                //NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                
                cn.ConnectionString =
                   $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password=medibrain1128;CommandTimeout={Settings.CommandTimeout};sslmode=1;convert infinity datetime = true"; 
            }
            else
            {
                //jyuseiのDBだけはsettings.xmlから取得
                cn.ConnectionString =
                    $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout={Settings.CommandTimeout};convert infinity datetime = true";
                //20220214134153 furukawa ed ////////////////////////
            }


            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);
            
            sb.AppendLine(" select ");
            sb.AppendLine(" userid  		ユーザID	         ,");
            sb.AppendLine(" name  			ユーザ名漢字  	     ,");
            sb.AppendLine(" loginname  		ログイン名	         ,");
            sb.AppendLine(" pass  			パスワード	         ,");
            sb.AppendLine(" belong  		所属		         ,");
            sb.AppendLine(" enable  		有効フラグ	         ,");
            sb.AppendLine(" admin    		管理者		         ,");
            sb.AppendLine(" developer  		開発者		         ,");
            sb.AppendLine(" manager  		ユーザ側マネージャ	 ,");
            sb.AppendLine(" generaluser  	一般ユーザ           ,");
            sb.AppendLine(" limiteduser  	制限ユーザ           ,");
            sb.AppendLine(" entrydate  		作成日		         ,");
            sb.AppendLine(" updatedate  	更新日		         ,");

            sb.AppendLine(" entryuser  		作成者		         ,");
            sb.AppendLine(" updateuser  	更新者		          ");

            sb.AppendLine(" from                 ");
            sb.AppendLine(" users_setting        ");

            sb.AppendLine(" order by userid      ");

            cmd.CommandText = sb.ToString();



            da.SelectCommand = cmd;            
            da.Fill(dtInsurer);
            dgv.DataSource = dtInsurer;
            dgv.Columns["ユーザID"].Width = 80;
            dgv.Columns["ユーザ名漢字"].Width = 100;
            dgv.Columns["ログイン名"].Width = 150;
            dgv.Columns["パスワード"].Width = 150;
            dgv.Columns["所属"].Width = 100;
            dgv.Columns["有効フラグ"].Width = 50;
            dgv.Columns["管理者"].Width = 50;
            dgv.Columns["開発者"].Width = 50;
            dgv.Columns["ユーザ側マネージャ"].Width = 50;
            dgv.Columns["一般ユーザ"].Width = 50;
            dgv.Columns["制限ユーザ"].Width = 50;
            dgv.Columns["作成日"].Width = 100;
            dgv.Columns["更新日"].Width = 100;
            dgv.Columns["更新者"].Width = 100;
            dgv.Columns["更新者"].Width = 100;


            DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            bc.Text = "削除";
            bc.Width = 60;
            bc.Name = "削除";
            bc.UseColumnTextForButtonValue = true;

            if (!dgv.Columns.Contains("削除")) dgv.Columns.Insert(0, bc);

            cmbDup.Items.Clear();
            foreach(DataGridViewColumn c in dgv.Columns)
            {
                cmbDup.Items.Add(c.Name);
            }
            
            //パスワードは開発者、管理者以外見せない
            dgv.Columns["パスワード"].Visible = User.CurrentUser.Developer || User.CurrentUser.Admin;

            //権限に応じた設定画面にする
            dgv.Columns["ユーザ側マネージャ"].Visible = (User.CurrentUser.Developer || User.CurrentUser.Admin || User.CurrentUser.manager);
            dgv.Columns["管理者"].Visible = (User.CurrentUser.Developer || User.CurrentUser.Admin);
            dgv.Columns["開発者"].Visible = User.CurrentUser.Developer;


            if (dgvCurrentRowIndex > dgv.Rows.Count - 1) { dgv.FirstDisplayedScrollingRowIndex = dgv.Rows.Count - 1; }
            else { dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex; }
            dgv.Columns[2].Frozen = true;//
            
        }

        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            upd();

            CommonTool.Restart_Mejor();
            
        }


        private void upd()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            foreach (DataRow dr in dtInsurer.Rows)
            {

                if (dr.RowState == DataRowState.Added)
                {
                    col = 0;

                    
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($" insert into users_setting (" +
                        $"userid, name, loginname, pass, belong, enable, admin, developer, manager, generaluser, limiteduser, entrydate ,entryuser " +
                        $") values (");

                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	'{11}',	'{12}'  ",
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), DateTime.Now.ToString("yyyy-MM-dd"),User.CurrentUser.Name);




                    sb.AppendLine(");");

                    lstsql.Add(sb.ToString());
                }
                else if (dr.RowState == DataRowState.Modified)
                {
                    col = 0;
                    int orgid = int.Parse(dr[0,DataRowVersion.Original].ToString());
                  

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update users_setting set ");

                    sb.AppendFormat("	userid  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	name  		    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	loginname  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	pass  		    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	belong  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	enable  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	admin    	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	developer  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	manager  	    ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	generaluser     ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	limiteduser     ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	entrydate       ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	updatedate  	='{0}',",DateTime.Now.ToString("yyyy-MM-dd"));
                    sb.AppendFormat("	updateuser  	='{0}' ", User.CurrentUser.Name);

                    sb.AppendFormat("	where userid		='{0}';", orgid);

                    lstsql.Add(sb.ToString());
                }


            }

            if (lstsql.Count == 0) return;

       

            //cmd.CommandText = sb.ToString();
            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {

                foreach (string s in lstsql)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新した");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtInsurer.Clear();

                cn.Close();
                disp();
            }


        }




        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            
            sb.AppendLine("select max(userid) id from users_setting where userid<9000");


            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

        /// <summary>
        /// 複製ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btncopy_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index < 0) return;

            DataGridViewRow dgvr = new DataGridViewRow();

            dgvr = dgv.Rows[dgv.CurrentRow.Index];
            DataRow dr;

            dr = dtInsurer.NewRow();
            
            for(int r=1;r<dgvr.Cells.Count;r++)
            {
                if (r == 1)
                {
                    //最大のID取得
                    dr[r-1] = getMaxID();
                }
                else
                {
                    dr[r - 1] = dgvr.Cells[r].Value ?? string.Empty;
                }
            }

            dtInsurer.Rows.Add(dr);
            upd();
        }



        /// <summary>
        /// 削除ボタン列
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 0) return;
            if (!(User.CurrentUser.Developer || User.CurrentUser.Admin))
            {
                MessageBox.Show($"管理者以外は物理削除できません。\r\n有効フラグをオフにするとそのユーザは使用不可にできます",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }



            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv.CurrentRow.Index;

            string id = dgv.Rows[dgv.CurrentRow.Index].Cells[1].Value.ToString();
            

            if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
                Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {


                try
                {

                    
                    cmd.CommandText = $"delete from users_setting where userid='{id}'";
                    cmd.ExecuteNonQuery();
                   
                    MessageBox.Show("削除した");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dtInsurer.Clear();

                    cn.Close();
                    disp();
           

                }
            }
        }

        private void SettingForm_Insurer_Shown(object sender, EventArgs e)
        {
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }



        //重複チェックを複製時にできるように関数化
        
        /// <summary>
        /// フィールドの重複値チェック
        /// </summary>
        /// <param name="fld">フィールド名</param>
        /// <returns>重複有り=true</returns>
        private bool duplicateCheck(string fld="")
        {
            cmbDup.Text = fld;
            if (fld == string.Empty) return false;            

            //重複フラグ
            bool flgDuplicate = false;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach (DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach (DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString())
                    {

                        cnt++;
                        if (cnt > 1)
                        {

                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                            flgDuplicate = true;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }

                }
              
            }
            if (flgDuplicate) return true;
            else return false;
        }



        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {

            
            //重複チェックを複製時にできるように関数化
            
            duplicateCheck(cmbDup.Text);

            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                foreach (DataGridViewRow r in dgv.Rows)
                {
                    //氏名の検索
                    if (r.Cells[2].Value.ToString().Trim().Contains(textBoxFind.Text.Trim()))
                    {
                        dgv.FirstDisplayedScrollingRowIndex = r.Index;
                        return;
                    }
                }
            }
        }
    }
}
