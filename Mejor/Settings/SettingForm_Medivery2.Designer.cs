﻿namespace Mejor
{
    partial class SettingForm_Medivery2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.lblserver = new System.Windows.Forms.Label();
            this.dgv_setting = new System.Windows.Forms.DataGridView();
            this.dgv_ins = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonInitSetting = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_setting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(1231, 494);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(135, 48);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(1094, 494);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(6);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(135, 48);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "更新";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // lblserver
            // 
            this.lblserver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblserver.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblserver.Location = new System.Drawing.Point(18, 513);
            this.lblserver.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblserver.Name = "lblserver";
            this.lblserver.Size = new System.Drawing.Size(854, 29);
            this.lblserver.TabIndex = 5;
            this.lblserver.Text = "label1";
            this.lblserver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgv_setting
            // 
            this.dgv_setting.AllowUserToAddRows = false;
            this.dgv_setting.AllowUserToDeleteRows = false;
            this.dgv_setting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_setting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_setting.Location = new System.Drawing.Point(0, 0);
            this.dgv_setting.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_setting.Name = "dgv_setting";
            this.dgv_setting.Size = new System.Drawing.Size(789, 473);
            this.dgv_setting.TabIndex = 7;
            // 
            // dgv_ins
            // 
            this.dgv_ins.AllowUserToAddRows = false;
            this.dgv_ins.AllowUserToDeleteRows = false;
            this.dgv_ins.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ins.Location = new System.Drawing.Point(0, 0);
            this.dgv_ins.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_ins.Name = "dgv_ins";
            this.dgv_ins.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ins.Size = new System.Drawing.Size(567, 473);
            this.dgv_ins.TabIndex = 8;
            this.dgv_ins.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ins_CellContentClick);
            this.dgv_ins.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ins_CellEnter);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgv_ins);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_setting);
            this.splitContainer1.Size = new System.Drawing.Size(1360, 473);
            this.splitContainer1.SplitterDistance = 567;
            this.splitContainer1.TabIndex = 9;
            // 
            // buttonInitSetting
            // 
            this.buttonInitSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInitSetting.Location = new System.Drawing.Point(947, 494);
            this.buttonInitSetting.Margin = new System.Windows.Forms.Padding(6);
            this.buttonInitSetting.Name = "buttonInitSetting";
            this.buttonInitSetting.Size = new System.Drawing.Size(135, 48);
            this.buttonInitSetting.TabIndex = 2;
            this.buttonInitSetting.Text = "設定初期化";
            this.buttonInitSetting.UseVisualStyleBackColor = true;
            this.buttonInitSetting.Click += new System.EventHandler(this.buttonInitSetting_Click);
            // 
            // SettingForm_Medivery2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 561);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.lblserver);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonInitSetting);
            this.Controls.Add(this.buttonOK);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SettingForm_Medivery2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Medivery設定";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_setting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ins)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label lblserver;
        private System.Windows.Forms.DataGridView dgv_setting;
        private System.Windows.Forms.DataGridView dgv_ins;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonInitSetting;
    }
}