﻿namespace Mejor
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.textBoxImageFolder = new System.Windows.Forms.TextBox();
            this.textBoxGroupCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btn_insurer = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxDBPort = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxBaseExcelDir = new System.Windows.Forms.TextBox();
            this.groupBoxJyusei = new System.Windows.Forms.GroupBox();
            this.cmbDB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxVersionFolder = new System.Windows.Forms.TextBox();
            this.textBoxDBPass = new System.Windows.Forms.TextBox();
            this.lblDBPassword = new System.Windows.Forms.Label();
            this.textBoxDBUser = new System.Windows.Forms.TextBox();
            this.lblDBUser = new System.Windows.Forms.Label();
            this.groupBoxJyusei.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.Location = new System.Drawing.Point(289, 355);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(112, 35);
            this.buttonOK.TabIndex = 50;
            this.buttonOK.Text = "保存";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.Location = new System.Drawing.Point(457, 355);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 35);
            this.buttonCancel.TabIndex = 60;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxIP
            // 
            this.textBoxIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIP.Location = new System.Drawing.Point(345, 30);
            this.textBoxIP.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(491, 24);
            this.textBoxIP.TabIndex = 20;
            // 
            // textBoxImageFolder
            // 
            this.textBoxImageFolder.Location = new System.Drawing.Point(184, 103);
            this.textBoxImageFolder.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxImageFolder.Name = "textBoxImageFolder";
            this.textBoxImageFolder.Size = new System.Drawing.Size(634, 24);
            this.textBoxImageFolder.TabIndex = 30;
            // 
            // textBoxGroupCount
            // 
            this.textBoxGroupCount.Location = new System.Drawing.Point(184, 220);
            this.textBoxGroupCount.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGroupCount.Name = "textBoxGroupCount";
            this.textBoxGroupCount.Size = new System.Drawing.Size(73, 24);
            this.textBoxGroupCount.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(225, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Server Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 108);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "画像フォルダ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 224);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Group ID カウント:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 287);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "プリンター:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(202, 285);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(376, 26);
            this.comboBox1.TabIndex = 30;
            // 
            // btn_insurer
            // 
            this.btn_insurer.Location = new System.Drawing.Point(713, 284);
            this.btn_insurer.Name = "btn_insurer";
            this.btn_insurer.Size = new System.Drawing.Size(123, 27);
            this.btn_insurer.TabIndex = 11;
            this.btn_insurer.Text = "保険者設定画面";
            this.btn_insurer.UseVisualStyleBackColor = true;
            this.btn_insurer.Visible = false;
            this.btn_insurer.Click += new System.EventHandler(this.btn_insurer_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 69);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 18);
            this.label11.TabIndex = 12;
            this.label11.Text = "DBPort:";
            // 
            // textBoxDBPort
            // 
            this.textBoxDBPort.Location = new System.Drawing.Point(145, 66);
            this.textBoxDBPort.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDBPort.Name = "textBoxDBPort";
            this.textBoxDBPort.Size = new System.Drawing.Size(73, 24);
            this.textBoxDBPort.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 146);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(167, 18);
            this.label12.TabIndex = 14;
            this.label12.Text = "リスト作成用Excelフォルダ:";
            // 
            // textBoxBaseExcelDir
            // 
            this.textBoxBaseExcelDir.Location = new System.Drawing.Point(184, 143);
            this.textBoxBaseExcelDir.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxBaseExcelDir.Name = "textBoxBaseExcelDir";
            this.textBoxBaseExcelDir.Size = new System.Drawing.Size(634, 24);
            this.textBoxBaseExcelDir.TabIndex = 33;
            // 
            // groupBoxJyusei
            // 
            this.groupBoxJyusei.Controls.Add(this.cmbDB);
            this.groupBoxJyusei.Controls.Add(this.label13);
            this.groupBoxJyusei.Controls.Add(this.label12);
            this.groupBoxJyusei.Controls.Add(this.label1);
            this.groupBoxJyusei.Controls.Add(this.textBoxVersionFolder);
            this.groupBoxJyusei.Controls.Add(this.label3);
            this.groupBoxJyusei.Controls.Add(this.textBoxGroupCount);
            this.groupBoxJyusei.Controls.Add(this.textBoxBaseExcelDir);
            this.groupBoxJyusei.Controls.Add(this.textBoxIP);
            this.groupBoxJyusei.Controls.Add(this.textBoxDBPass);
            this.groupBoxJyusei.Controls.Add(this.lblDBPassword);
            this.groupBoxJyusei.Controls.Add(this.textBoxDBUser);
            this.groupBoxJyusei.Controls.Add(this.lblDBUser);
            this.groupBoxJyusei.Controls.Add(this.textBoxDBPort);
            this.groupBoxJyusei.Controls.Add(this.label2);
            this.groupBoxJyusei.Controls.Add(this.label11);
            this.groupBoxJyusei.Controls.Add(this.textBoxImageFolder);
            this.groupBoxJyusei.Location = new System.Drawing.Point(18, 12);
            this.groupBoxJyusei.Name = "groupBoxJyusei";
            this.groupBoxJyusei.Size = new System.Drawing.Size(848, 254);
            this.groupBoxJyusei.TabIndex = 10;
            this.groupBoxJyusei.TabStop = false;
            this.groupBoxJyusei.Text = "JyuseiDB設定";
            // 
            // cmbDB
            // 
            this.cmbDB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDB.FormattingEnabled = true;
            this.cmbDB.Items.AddRange(new object[] {
            "AWS",
            "aws-test",
            "外部委託先サーバ",
            "外部委託先サーバ2"});
            this.cmbDB.Location = new System.Drawing.Point(12, 29);
            this.cmbDB.Name = "cmbDB";
            this.cmbDB.Size = new System.Drawing.Size(194, 26);
            this.cmbDB.TabIndex = 14;
            this.cmbDB.SelectedValueChanged += new System.EventHandler(this.cmbDB_SelectedValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 185);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 18);
            this.label13.TabIndex = 14;
            this.label13.Text = "バージョン管理フォルダ:";
            // 
            // textBoxVersionFolder
            // 
            this.textBoxVersionFolder.Location = new System.Drawing.Point(184, 182);
            this.textBoxVersionFolder.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxVersionFolder.Name = "textBoxVersionFolder";
            this.textBoxVersionFolder.Size = new System.Drawing.Size(634, 24);
            this.textBoxVersionFolder.TabIndex = 33;
            // 
            // textBoxDBPass
            // 
            this.textBoxDBPass.Location = new System.Drawing.Point(628, 66);
            this.textBoxDBPass.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDBPass.Name = "textBoxDBPass";
            this.textBoxDBPass.Size = new System.Drawing.Size(208, 24);
            this.textBoxDBPass.TabIndex = 28;
            // 
            // lblDBPassword
            // 
            this.lblDBPassword.AutoSize = true;
            this.lblDBPassword.Location = new System.Drawing.Point(520, 69);
            this.lblDBPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDBPassword.Name = "lblDBPassword";
            this.lblDBPassword.Size = new System.Drawing.Size(100, 18);
            this.lblDBPassword.TabIndex = 12;
            this.lblDBPassword.Text = "DBPassword:";
            // 
            // textBoxDBUser
            // 
            this.textBoxDBUser.Location = new System.Drawing.Point(375, 66);
            this.textBoxDBUser.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDBUser.Name = "textBoxDBUser";
            this.textBoxDBUser.Size = new System.Drawing.Size(137, 24);
            this.textBoxDBUser.TabIndex = 25;
            // 
            // lblDBUser
            // 
            this.lblDBUser.AutoSize = true;
            this.lblDBUser.Location = new System.Drawing.Point(302, 69);
            this.lblDBUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDBUser.Name = "lblDBUser";
            this.lblDBUser.Size = new System.Drawing.Size(65, 18);
            this.lblDBUser.TabIndex = 12;
            this.lblDBUser.Text = "DBUser:";
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 403);
            this.Controls.Add(this.groupBoxJyusei);
            this.Controls.Add(this.btn_insurer);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SettingForm";
            this.Text = "メホール本体設定";
            this.Shown += new System.EventHandler(this.FormSetting_Shown);
            this.groupBoxJyusei.ResumeLayout(false);
            this.groupBoxJyusei.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.TextBox textBoxImageFolder;
        private System.Windows.Forms.TextBox textBoxGroupCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_insurer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxDBPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxBaseExcelDir;
        private System.Windows.Forms.GroupBox groupBoxJyusei;
        private System.Windows.Forms.TextBox textBoxDBPass;
        private System.Windows.Forms.Label lblDBPassword;
        private System.Windows.Forms.TextBox textBoxDBUser;
        private System.Windows.Forms.Label lblDBUser;
        private System.Windows.Forms.ComboBox cmbDB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxVersionFolder;
    }
}