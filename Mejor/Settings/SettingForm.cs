﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Mejor
{
    public partial class SettingForm : Form
    {
        BindingSource bs = new BindingSource();

        public SettingForm()
        {
            InitializeComponent();
            User.GetUsers();

            textBoxIP.Text = Settings.DataBaseHost;
            textBoxImageFolder.Text = Settings.ImageFolder;
            textBoxGroupCount.Text = Settings.GroupCount.ToString();

            //20200212143302 furukawa st ////////////////////////
            //ポート、エクセルフォルダ設定ロード
            
            textBoxBaseExcelDir.Text = Settings.BaseExcelDir;
            textBoxDBPort.Text = Settings.dbPort;
            //20200212143302 furukawa ed ////////////////////////




            //20200317165008 furukawa st ////////////////////////
            //ユーザ、パスワードをSettings.xmlから表示
            
            textBoxDBUser.Text = Settings.JyuseiDBUser;
            textBoxDBPass.Text = Settings.JyuseiDBPassword;
            //20200317165008 furukawa ed ////////////////////////

            //20200317182351 furukawa st ////////////////////////
            //コンボの設定値取得
            
            cmbDB.SelectedIndex = int.Parse(Settings.CurrentComboIndex);
            //20200317182351 furukawa ed ////////////////////////

            //20211224100006 furukawa st ////////////////////////
            //ユーザ権限管理画面を別にしたので不要

            //  dataGridViewUser.DataSource = bs;
            //  UserListUpdate();
            //  dataGridViewUser.Columns[nameof(User.Pass)].Visible = false;
            //  bs.CurrentItemChanged += Bs_CurrentItemChanged;
            //  setUser();

            //20211224100006 furukawa ed ////////////////////////

            string defPrinterName;
            using (var pd = new PrintDocument())
            {
                defPrinterName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == defPrinterName)
                    comboBox1.SelectedIndex = i;
            }

            var pn = Settings.DefaultPrinterName;
            if (comboBox1.Items.Contains(pn)) comboBox1.SelectedItem = pn;

            //20211015091853 furukawa st ////////////////////////
            //JyudeiDB設定はDeveloperのみ
            
            groupBoxJyusei.Enabled = User.CurrentUser.Developer;
            //20211015091853 furukawa ed ////////////////////////

            //2021/12/23　開発者と管理者は見せる
            groupBoxJyusei.Visible = User.CurrentUser.Developer || User.CurrentUser.Admin;

        }



        private void Bs_CurrentItemChanged(object sender, EventArgs e)
        {
        //20211224100152 furukawa st ////////////////////////
        //ユーザ権限管理画面を別にしたので不要
        //          setUser();
        //20211224100152 furukawa ed ////////////////////////
        }



        private void setUser()
        {

            //20211224100242 furukawa st ////////////////////////
            //ユーザ権限管理画面を別にしたので不要




            //      var u =(User)bs.Current;

            //      if (u.UserID == 0)
            //      {
            //          textBoxUserID.Clear();
            //          textBoxUserID.ReadOnly = false;
            //          textBoxuserName.Clear();
            //          textBoxUserPass.Clear();
            //          textBoxUserRoman.Clear();
            //          checkBoxEnable.Checked = true;
            //          checkBoxAdmin.Checked = false;
            //          buttonAddUser.Text = "追加";
            //      }
            //      else
            //      {
            //          textBoxUserID.Text = u.UserID.ToString();
            //          textBoxUserID.ReadOnly = true;
            //          textBoxuserName.Text = u.Name;
            //          textBoxUserPass.Text = u.Pass;
            //          textBoxUserRoman.Text = u.LoginName;
            //          checkBoxEnable.Checked = u.Enable;
            //          checkBoxAdmin.Checked = u.Admin;
            //          buttonAddUser.Text = "変更";
            //      }


            //      buttonAddUser.Enabled = true;

            //20211224100242 furukawa ed ////////////////////////
        }




        private void buttonOK_Click(object sender, EventArgs e)
        {

            Settings.DataBaseHost = textBoxIP.Text;
            Settings.ImageFolder = textBoxImageFolder.Text;
            Settings.DefaultPrinterName = (string)comboBox1.SelectedItem;


            //20200219113906 furukawa st ////////////////////////
            //ポート、エクセルフォルダ設定書き込み
            
            Settings.BaseExcelDir = textBoxBaseExcelDir.Text.Trim();
            Settings.dbPort = textBoxDBPort.Text.Trim();
            //20200219113906 furukawa ed ////////////////////////

            //20200317170332 furukawa st ////////////////////////
            //ユーザ、パスワードをSettings.xml書き込み
            
            Settings.JyuseiDBUser = textBoxDBUser.Text.Trim();
            Settings.JyuseiDBPassword = textBoxDBPass.Text.Trim();
            //20200317170332 furukawa ed ////////////////////////

            //20200317182451 furukawa st ////////////////////////
            //コンボの設定値保存
            
            Settings.CurrentComboIndex = cmbDB.SelectedIndex.ToString();
            //20200317182451 furukawa ed ////////////////////////


            //現在のdbサーバ名称
            Settings.CurrentServer = cmbDB.Text;


            int gc;
            int.TryParse(textBoxGroupCount.Text, out gc);
            Settings.GroupCount = gc;

            Properties.Settings.Default.Save();

            this.Close();

            //20200302140839 furukawa st ////////////////////////
            //設定を有効化するため再起動
            
            MessageBox.Show("Mejorが再度起動します。", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //プログラム終了
            try
            {
                Application.Exit();
            }
            catch
            {
                try
                {
                    //強制終了
                    Environment.Exit(0);
                }
                catch
                {
                    //握りつぶす
                }
            }

            //新プログラム起動
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            //20200302140839 furukawa ed ////////////////////////
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// User管理機能(追加・変更のみ)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            //20211224100413 furukawa st ////////////////////////
            //ユーザ権限管理画面を別にしたので不要

            #region 不要
            ////データチェック
            //if (textBoxUserID.Text == "" || textBoxuserName.Text == ""
            //    || textBoxUserRoman.Text == "" || textBoxUserPass.Text == "")
            //{
            //    MessageBox.Show("空白の欄があります");
            //    return;
            //}

            //int userid;
            //userid = int.TryParse(textBoxUserID.Text, out userid) ? userid : 0;
            //if (userid == 0)
            //{
            //    MessageBox.Show("UserIDには数字のみ入力してください");
            //    return;
            //}

            //bool update = true;
            //if (((User)bs.Current).UserID == 0)
            //{
            //    if (User.GetList().FirstOrDefault(item => item.UserID == userid) != null)
            //    {
            //        MessageBox.Show("指定されたIDは既に使用されています");
            //        return;
            //    }
            //    update = false;
            //}

            //var msg = (update ? "選択中のユーザーを更新します。" : "新規ユーザーを登録します。") + "よろしいですか？";
            //if (MessageBox.Show(msg, "ユーザー確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
            //     DialogResult.OK) return;

            //if (update)
            //{
            //    var u = User.GetUser(userid);
            //    u.Name = textBoxuserName.Text;
            //    u.LoginName = textBoxUserRoman.Text;
            //    u.Pass = textBoxUserPass.Text;
            //    u.Enable = checkBoxEnable.Checked;
            //    u.Admin = checkBoxAdmin.Checked;
            //    u.Update();
            //}
            //else
            //{
            //    var u = new User(userid, textBoxuserName.Text);
            //    u.LoginName = textBoxUserRoman.Text;
            //    u.Pass = textBoxUserPass.Text;
            //    u.Enable = checkBoxEnable.Checked;
            //    u.Admin = checkBoxAdmin.Checked;
            //    u.Insert();
            //}

            ////表示を更新
            //UserListUpdate();
            //MessageBox.Show(update ? "更新しました" : "登録しました");
            #endregion

            //20211224100413 furukawa ed ////////////////////////
        }

        //Userリスト表示の更新
        private void UserListUpdate()
        {
            //20211224100542 furukawa st ////////////////////////
            //ユーザ権限管理画面を別にしたので不要
            
            //      User.GetUsers();
            //      var l = User.GetList();

            //      l.Add(new User(0, "新規"));
            //      l.Sort((x, y) => x.UserID.CompareTo(y.UserID));
            //      bs.DataSource = l;

            
            //      dgSetting();


            //20211224100542 furukawa ed ////////////////////////
        }

        private void FormSetting_Shown(object sender, EventArgs e)
        {
            //20200205182111 furukawa st ////////////////////////
            //開発者のみIP関連変更可能

            //20210128101921 furukawa st ////////////////////////
            //別機能に分離したので不要
            //btn_insurer.Visible = User.CurrentUser.Developer;
            //20210128101921 furukawa ed ////////////////////////



            textBoxIP.ReadOnly = !User.CurrentUser.Developer;
            textBoxImageFolder.ReadOnly = !User.CurrentUser.Developer;

            //20200205182111 furukawa ed ////////////////////////

            //20200212143109 furukawa st ////////////////////////
            //ポート、エクセルフォルダは開発者のみ変更可能
            
            textBoxBaseExcelDir.ReadOnly = !User.CurrentUser.Developer;
            textBoxDBPort.ReadOnly = !User.CurrentUser.Developer;
            //20200212143109 furukawa ed ////////////////////////

            //20200317183623 furukawa st ////////////////////////
            //DBユーザ・パスワード、コンボは開発者以外編集させない
            
            textBoxDBUser.Visible = User.CurrentUser.Developer;
            textBoxDBPass.Visible = User.CurrentUser.Developer;
            lblDBUser.Visible = User.CurrentUser.Developer;
            lblDBPassword.Visible = User.CurrentUser.Developer;
            cmbDB.Enabled= User.CurrentUser.Developer;

            //20200317183623 furukawa ed ////////////////////////

            textBoxVersionFolder.ReadOnly = !User.CurrentUser.Developer;

            CommonTool.setFormColor(this);


            //20211224112804 furukawa st ////////////////////////
            //プリンタ設定はマネージャにもさせる

            //  if (User.CurrentUser.Admin) return;
            //  MessageBox.Show("現在ログインしているユーザーには管理権限がありません", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //  this.Close();
            //20211224112804 furukawa ed ////////////////////////
        }

        //20190505095405 furukawa st ////////////////////////
        //グリッド調整、文字サイズ調整(デザイナ)

        //private void dgSetting()
        //{
        //    this.dataGridViewUser.Columns["userID"].Width = 50;
        //    this.dataGridViewUser.Columns["Name"].Width = 150;
        //    this.dataGridViewUser.Columns["LoginName"].Width = 200;
        //    this.dataGridViewUser.Columns["Enable"].Width = 70;
        //    this.dataGridViewUser.Columns["Admin"].Width = 70;

        //}


        //20190505095405 furukawa ed ////////////////////////


        private void btn_insurer_Click(object sender, EventArgs e)
        {
            SettingForm_Insurer frm = new SettingForm_Insurer();
            frm.ShowDialog();
        }

      

        //20200531095938 furukawa st ////////////////////////
        //DBユーザ・パスワード・ポート・Excelフォルダ、画像フォルダもサーバ別に設定
        
        private void cmbDB_SelectedValueChanged(object sender, EventArgs e)
        {
            Settings.getServerSettings();
            

            using (var dt = Settings.dsSettings.Tables[cmbDB.Text])
            {
                if (dt == null) return;
                textBoxIP.Text = dt.Rows[0]["DBIP"].ToString();
                textBoxDBPass.Text = dt.Rows[0]["jyuseiPassword"].ToString();
                textBoxDBUser.Text = dt.Rows[0]["jyuseiUser"].ToString();
                textBoxDBPort.Text = dt.Rows[0]["port"].ToString();
                textBoxBaseExcelDir.Text = dt.Rows[0]["BaseExcelDir"].ToString();
                textBoxImageFolder.Text = dt.Rows[0]["ImageFolder"].ToString();
                textBoxVersionFolder.Text = dt.Rows[0]["VersionFolder"].ToString();
            }




            //switch (cmbDB.Text)
            //{
            //    case "AWS":
            //        using (var dt = Settings.dsSettings.Tables["AWS"])
            //        {
            //            textBoxIP.Text = dt.Rows[0]["DBIP"].ToString();//Settings.GetProductionIP;
            //            textBoxDBPass.Text = dt.Rows[0]["jyuseiPassword"].ToString();
            //            textBoxDBUser.Text = dt.Rows[0]["jyuseiUser"].ToString();
            //            textBoxDBPort.Text = dt.Rows[0]["port"].ToString();
            //            textBoxBaseExcelDir.Text = dt.Rows[0]["BaseExcelDir"].ToString();
            //            textBoxImageFolder.Text = dt.Rows[0]["ImageFolder"].ToString();
            //        }
            //        break;

            //    case "社内本番サーバ":
            //        textBoxIP.Text = Settings.GetInternalDBIP;
            //        textBoxDBPass.Text = Settings.GetInternalJyuseiPassword;
            //        textBoxDBUser.Text = Settings.GetInternalJyuseiUser;
            //        textBoxDBPort.Text = Settings.GetInternal_port;
            //        textBoxBaseExcelDir.Text = Settings.GetInternal_BaseExcelDir;
            //        textBoxImageFolder.Text = Settings.GetInternal_ImageFolder;

            //        break;

            //    case "試験Postgres11":
            //        textBoxIP.Text = Settings.GetTestDBIP;
            //        textBoxDBPass.Text = Settings.GetTestJyuseiPassword;
            //        textBoxDBUser.Text = Settings.GetTestJyuseiUser;
            //        textBoxDBPort.Text = Settings.GetTest_port;
            //        textBoxBaseExcelDir.Text = Settings.GetTest_BaseExcelDir;
            //        textBoxImageFolder.Text = Settings.GetTest_ImageFolder;
            //        break;

            //    case "試験Postgres9.6":
            //        textBoxIP.Text = Settings.GetTestDBIP;
            //        textBoxDBPass.Text = Settings.GetTestJyuseiPassword;
            //        textBoxDBUser.Text = Settings.GetTestJyuseiUser;
            //        textBoxDBPort.Text = Settings.GetTest_port;
            //        textBoxBaseExcelDir.Text = Settings.GetTest_BaseExcelDir;
            //        textBoxImageFolder.Text = Settings.GetTest_ImageFolder;

            //        break;

            //    default:
            //        break;

            //}
        }

        //20200531095938 furukawa ed ////////////////////////



        //20211224100719 furukawa st ////////////////////////
        //ユーザ権限管理画面を別にしたので不要

        //      20211014150501 furukawa st ////////////////////////
        //      ユーザ名検索

        //      int start = 0;//次の行から検索するための目印
        //      private void buttonFind_Click(object sender, EventArgs e)
        //      {

        //          //if (dataGridViewUser.Rows.Count <= 0) return;

        //          //if (start > dataGridViewUser.RowCount-1) start = 0;

        //          //dataGridViewUser.FirstDisplayedScrollingRowIndex = 0;
        //          //for (int r = start; r < dataGridViewUser.RowCount; r++) 

        //          //{

        //          //    DataGridViewRow dgvr = dataGridViewUser.Rows[r];

        //          //    if (dgvr.Cells["LoginName"].Value != null && 
        //          //        Microsoft.VisualBasic.Strings.StrConv(dgvr.Cells["LoginName"].Value.ToString(),Microsoft.VisualBasic.VbStrConv.Lowercase).Contains
        //          //        (Microsoft.VisualBasic.Strings.StrConv(textBoxFind.Text.Trim(), Microsoft.VisualBasic.VbStrConv.Lowercase)))
        //          //    {
        //          //        dataGridViewUser.FirstDisplayedScrollingRowIndex = dgvr.Index;
        //          //        start = dgvr.Index + 1;
        //          //        break;
        //          //    }
        //          //    if (dgvr.Cells["Name"].Value != null && dgvr.Cells["Name"].Value.ToString().Contains(textBoxFind.Text.Trim()))
        //          //    {
        //          //        dataGridViewUser.FirstDisplayedScrollingRowIndex = dgvr.Index;
        //          //        start = dgvr.Index + 1;
        //          //        break;
        //          //    }

        //          //    if (r ==dataGridViewUser.RowCount-1) start = 0;
        //          //}
        //      }
        //      20211014150501 furukawa ed ////////////////////////


        //20211224100719 furukawa ed ////////////////////////
    }
}
