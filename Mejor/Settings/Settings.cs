﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Mejor
{
    class Settings
    {
        //20200212141640 furukawa st ////////////////////////
        //リスト作成画面のベースとなるExcelフォルダパスはSettings.xmlに持たせたので削除

        //public static string BaseExcelDir => "\\\\192.168.200.100\\public\\JuseiSystem\\BaseExcels";
        //20200212141640 furukawa ed ////////////////////////

        
        public static System.Data.DataSet dsSettings = new System.Data.DataSet();//20200531140011 furukawa Settings用DataSet
        public static System.Data.DataTable dtServerSettings = new System.Data.DataTable();//20200531140051 furukawa サーバ設定定義用DataTable
                                                                                           

        static string fileName = System.Windows.Forms.Application.StartupPath + "\\settings.xml";
        static XDocument xdoc;

        public enum mejor_enviroment
        {
            Production,
            InternalServer,
            Test,
            Local,
            Other,
        }


        static Settings()
        {
            if (System.IO.File.Exists(fileName))
            {
                xdoc = XDocument.Load(fileName);
            }
            else
            {
                xdoc = new XDocument();
                xdoc.Add(new XElement("Settings"));
                xdoc.Save(fileName);
            }
        }


        private static void setValue(string name, string value)
        {
            xdoc.Element("Settings").SetElementValue(name, value);
            xdoc.Save(fileName);
        }

        private static string getValue(string name)
        {
            if (xdoc.Element("Settings").Element(name) == null) return string.Empty;
            return xdoc.Element("Settings").Element(name).Value;
        }


       /// <summary>
       /// サーバ設定をDataSetにロード
       /// </summary>
        public static void getServerSettings()
        {            
            dsSettings.ReadXml("settings.xml");
            dtServerSettings = dsSettings.Tables["ServerSetting"];
        }


        /// <summary>
        /// 共通設定プリンタ
        /// </summary>
        public static string DefaultPrinterName
        {
            get { return getValue("DefaultPrinterName"); }
            set { setValue("DefaultPrinterName", value); }
        }

        /// <summary>
        /// 点検画面での使用プリンタ名
        /// </summary>
        public static string InspectPrinterName
        {
            get { return getValue("InspectPrinterName"); }
            set { setValue("InspectPrinterName", value); }
        }

        /// <summary>
        /// 照会画面での使用プリンタ名
        /// </summary>
        public static string ShokaiPrinterName
        {
            get { return getValue("ShokaiPrinterName"); }
            set { setValue("ShokaiPrinterName", value); }
        }

    
        /// <summary>
        /// データベースのホストアドレス
        /// </summary>
        public static string DataBaseHost
        {
            get {
                //20200531140348 furukawa st ////////////////////////
                //現在使用中のサーバ名からIPを取得                               
                return dsSettings.Tables[CurrentServer].Rows[0]["DBIP"].ToString();
                //    return getValue("DataBaseHost");
                //20200531140348 furukawa ed ////////////////////////


            }
            set { setValue("DataBaseHost", value); }
        }

        /// <summary>
        /// データベースのタイムアウト秒
        /// </summary>
        public static string CommandTimeout
        {
            get { return getValue("CommandTimeout"); }
            set { setValue("CommandTimeout", value); }
        }

        /// <summary>
        /// 印刷時マージン
        /// </summary>
        public static int PrintMargin
        {
            get { return int.Parse(getValue("PrintMargin")); }
            set { setValue("PrintMargin", value.ToString()); }
        }

        /// <summary>
        /// 1グループに割り当てられる画像枚数
        /// </summary>
        public static int GroupCount
        {
            get { return int.Parse(getValue("GroupCount")); }
            set { setValue("GroupCount", value.ToString()); }
        }

        /// <summary>
        /// スキャン画像の保存先フォルダ
        /// </summary>
        public static string ImageFolder
        {
            get { return getValue("ImageFolder"); }
            set { setValue("ImageFolder", value); }
        }

        /// <summary>
        /// バージョン管理フォルダ
        /// </summary>
        public static string VersionFolder
        {
            get { return getValue("VersionFolder"); }
            set { setValue("VersionFolder", value); }
        }


        //20191128091135 furukawa st ////////////////////////
        //postgresqlのポート指定もsettingsから取得
        
        /// <summary>
        /// db接続ポート
        /// </summary>
        public static string dbPort
        {
            get { return getValue("port"); }
            set { setValue("port", value); }
        }
        //20191128091135 furukawa ed ////////////////////////


        //20200212141554 furukawa st ////////////////////////
        //リスト作成画面のベースとなるExcelフォルダパスの取得/設定
        
        public static string BaseExcelDir
        {
            get { return getValue("BaseExcelDir"); }
            set { setValue("BaseExcelDir", value); }
        }
        //20200212141554 furukawa ed ////////////////////////


        //20200313183910 furukawa st ////////////////////////
        //ユーザ、パスワードをSettings.xmlから取得
        
        public static string JyuseiDBUser
        {
            get { return getValue("jyusei_user"); }
            set { setValue("jyusei_user",value); }
        }

        public static string JyuseiDBPassword
        {
            get { return getValue("jyusei_password"); }
            set { setValue("jyusei_password",value); }
        }
        //20200313183910 furukawa ed ////////////////////////



        //20200531135645 furukawa st ////////////////////////
        //サーバごとに定義を持たせたのでそこから取る


                        //20200313182521 furukawa st ////////////////////////
                        //サーバIPをsettins.xmlで設定してロードする

                        //public static string GetProductionIP
                        //{
                        //    get { return getValue("ProductionDBIP"); }            
                        //}
                        //public static string GetTestDBIP
                        //{
                        //    get { return getValue("TestDBIP"); }
                        //}
                        //public static string GetInternalDBIP
                        //{
                        //    get { return getValue("InternalDBIP"); }
                        //}
                        ////20200313182521 furukawa ed ////////////////////////

                        ////20200531094558 furukawa st ////////////////////////
                        ////DBユーザ・パスワード・ポートもサーバ別に設定

                        //public static string GetProductionJyuseiUser
                        //{
                        //    get { return getValue("Production_jyuseiUser"); }
                        //}
                        //public static string GetTestJyuseiUser
                        //{
                        //    get { return getValue("Test_jyuseiUser"); }
                        //}
                        //public static string GetInternalJyuseiUser
                        //{
                        //    get { return getValue("Internal_jyuseiUser"); }
                        //}

                        //public static string GetProductionJyuseiPassword
                        //{
                        //    get { return getValue("Production_jyuseiPassword"); }
                        //}
                        //public static string GetTestJyuseiPassword
                        //{
                        //    get { return getValue("Test_jyuseiPassword"); }
                        //}
                        //public static string GetInternalJyuseiPassword
                        //{
                        //    get { return getValue("Internal_jyuseiPassword"); }
                        //}

                        //public static string GetProduction_port
                        //{
                        //    get { return getValue("Production_port"); }
                        //}
                        //public static string GetTest_port
                        //{
                        //    get { return getValue("Test_port"); }
                        //}
                        //public static string GetInternal_port
                        //{
                        //    get { return getValue("Internal_port"); }
                        //}


                        //public static string GetProduction_ImageFolder
                        //{
                        //    get { return getValue("Production_ImageFolder"); }
                        //}
                        //public static string GetTest_ImageFolder
                        //{
                        //    get { return getValue("Test_ImageFolder"); }
                        //}
                        //public static string GetInternal_ImageFolder
                        //{
                        //    get { return getValue("Internal_ImageFolder"); }
                        //}

                        //public static string GetProduction_BaseExcelDir
                        //{
                        //    get { return getValue("Production_BaseExcelDir"); }
                        //}
                        //public static string GetTest_BaseExcelDir
                        //{
                        //    get { return getValue("Test_BaseExcelDir"); }
                        //}
                        //public static string GetInternal_BaseExcelDir
                        //{
                        //    get { return getValue("Internal_BaseExcelDir"); }
                        //}
                        //20200531094558 furukawa ed ////////////////////////


                        //20200313182621 furukawa st ////////////////////////
                        //現在どこに接続しているか確認

                        //public static mejor_enviroment IsEnvironment(string strtmp)
                        //{
                        //    if (strtmp == GetProductionIP) return mejor_enviroment.Production;
                        //    if (strtmp == GetTestDBIP) return mejor_enviroment.Test;
                        //    if (strtmp == GetInternalDBIP) return mejor_enviroment.InternalServer;

                        //    return mejor_enviroment.Other;
                        //}
                        //20200313182621 furukawa ed ////////////////////////




        //20200531135645 furukawa ed ////////////////////////

        public static string CurrentComboIndex
        {
            get { return getValue("currentComboIndex"); }
            set { setValue("currentComboIndex", value); }
        }



        //20200531140303 furukawa st ////////////////////////
        //現在使用中のサーバ名

        /// <summary>
        /// 現在使用中のサーバ名
        /// </summary>
        public static string CurrentServer
        {
            get { return getValue("currentServer"); }
            set { setValue("currentServer", value); }
        }
        //20200531140303 furukawa ed ////////////////////////
    }
}
