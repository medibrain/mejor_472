﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class SettingMenuForm : Form
    {
        public SettingMenuForm()
        {
            InitializeComponent();

            CommonTool.setFormColor(this);

            //保険者設定ボタン
            buttonInsurerSettings.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;


            //medivery使用保険者設定

            //20211224100844 furukawa st ////////////////////////
            //ボタンを削除した

            //      20210225134848 furukawa st ////////////////////////
            //      旧mediveryボタンはもう使わないので開発者のみ            
            //      buttonMediveryUseInsurer.Enabled = User.CurrentUser.Developer;
            //            buttonMediveryUseInsurer.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            //      20210225134848 furukawa ed ////////////////////////

            //20211224100844 furukawa ed ////////////////////////



            //20211224101040 furukawa st ////////////////////////
            //medivery設定はmanagerも使用
            buttonMediveryUseInsurerXML.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer || User.CurrentUser.manager;
            //buttonMediveryUseInsurerXML.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer 
            //20211224101040 furukawa ed ////////////////////////



            //20210217095107 furukawa st ////////////////////////
            //Medivery使用保険者管理ボタン（xml）            
            //      buttonMediveryUseInsurerXML.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            //20210217095107 furukawa ed ////////////////////////


            //システム設定ボタン

            //20211224112024 furukawa st ////////////////////////
            //システム設定画面は開発者、管理者、マネージャ。プリンタ設定の関係で
            
            buttonSystem.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer || User.CurrentUser.manager;

            
            //      20211015091550 furukawa st ////////////////////////
            //      システム設定をdeveloperだけでなくadminも

            //      buttonSystem.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            //                buttonSystem.Enabled = User.CurrentUser.Developer;
            //      20211015091550 furukawa ed ////////////////////////
            //20211224112024 furukawa ed ////////////////////////

            //20210210145754 furukawa st ////////////////////////
            //誤操作防止のためマスタインポートも管理者権限以上

            buttonSick.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            buttonModify.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            //20210210145754 furukawa ed ////////////////////////
            
            

            //20210224125520 furukawa st ////////////////////////
            //mejor使用保険者一覧            
            buttonInsurerList.Enabled = User.CurrentUser.Admin || User.CurrentUser.Developer;
            //20210224125520 furukawa ed ////////////////////////

            

            //20211224101404 furukawa st ////////////////////////
            //ユーザ設定画面は開発者、管理者、マネージャ            
            buttonUserSetting.Enabled = User.CurrentUser.Developer || User.CurrentUser.Admin || User.CurrentUser.manager;
            //20211224101404 furukawa ed ////////////////////////

        }

        private void buttonSick_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return;
                MediMatch.Sick.Import(f.FileName);
            }
        }

        private void buttonSystem_Click(object sender, EventArgs e)
        {
            using (var f = new SettingForm()) f.ShowDialog();
        }

        private void buttonModify_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return;
                MediMatch.Modify.Import(f.FileName);
            }
        }

        private void buttonInputLog_Click(object sender, EventArgs e)
        {
            using (var f = new InputLogForm()) f.ShowDialog();
        }

        private void buttonInputLogOutput_Click(object sender, EventArgs e)
        {
            Form f = new Mejor.InputLogOutput.OutputSetting();
            f.ShowDialog();
        }

        private void buttonInsurerSettings_Click(object sender, EventArgs e)
        {
            SettingForm_Insurer frm = new SettingForm_Insurer();
            frm.ShowDialog();

        }

        //20220214132746 furukawa st ////////////////////////
        //不要        
        //private void buttonMediveryUseInsurer_Click(object sender, EventArgs e)
        //{
        //    SettingForm_Medivery frm = new SettingForm_Medivery();
        //    frm.ShowDialog();
        //}
        //20220214132746 furukawa ed ////////////////////////


        /// <summary>
        /// //20210217095213 furukawa Medivery使用保険者管理ボタン（xml）        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMediveryUseInsurerXML_Click(object sender, EventArgs e)
        {
            SettingForm_Medivery2 frm = new SettingForm_Medivery2();
            frm.ShowDialog();
        }



        /// <summary>
        /// //20210224135208 furukawa mejorに登録されている保険者一覧を出す
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInsurerList_Click(object sender, EventArgs e)
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            string strDB = DB.GetMainDBName();
            string strConn = string.Empty;
            DataTable dt = new DataTable();

            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.DefaultExt = ".csv";
                sfd.FileName = $"{DateTime.Now.ToString("メホール登録保険者一覧_yyyyMMdd_hhmmss")}.csv";
                sfd.Title = "一覧CSV出力";
                if (sfd.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBox.Show("中止"); 
                    return;
                }

                //20220214132908 furukawa NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わった
                strConn = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password={Settings.JyuseiDBPassword};CommandTimeout={Settings.CommandTimeout};sslmode=1;convert infinity datetime = true"; 

                cn.ConnectionString = strConn;
                cn.Open();

                cmd.Connection = cn;
                cmd.CommandText = "select * from insurer_setting_mejor";
                Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();
                
                
                da.SelectCommand = cmd;
                da.Fill(dt);

                System.IO.StreamWriter sw = new System.IO.StreamWriter(sfd.FileName,false, System.Text.Encoding.GetEncoding("shift-jis"));

                string strWrite = string.Empty;

                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    strWrite += $"{dt.Columns[c].ColumnName},";
                }
                strWrite = strWrite.Substring(0, strWrite.Length - 1);
                sw.WriteLine(strWrite);

                for (int r=0;r<dt.Rows.Count;r++)
                {
                    strWrite = string.Empty;
                    
                    for (int c = 0; c < dt.Columns.Count; c++)
                    {
                        strWrite += $"{dt.Rows[r][c].ToString()},";
                    }

                    strWrite = strWrite.Substring(0, strWrite.Length - 1);

                    sw.WriteLine(strWrite);
                }


                sw.Close();

                System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo();
                pi.FileName = sfd.FileName;
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = pi;
                p.Start();

           
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name+"\r\n"+ex.Message);
            }
            finally
            {
                dt.Dispose();
                cmd.Dispose();
                cn.Close();
            }
        }
        //20210716154324 furukawa st ////////////////////////
        //現在の入力状況
        
        private void buttonCurrentInput_Click(object sender, EventArgs e)
        {
            CurrentInput f = new CurrentInput();
            f.ShowDialog();
        }
        //20210716154324 furukawa ed ////////////////////////



        //20211224101446 furukawa st ////////////////////////
        //ユーザ設定画面ロード
        
        private void buttonUserSetting_Click(object sender, EventArgs e)
        {
            SettingForm_user f = new SettingForm_user();
            f.ShowDialog();
        }
        //20211224101446 furukawa ed ////////////////////////





    }
}
