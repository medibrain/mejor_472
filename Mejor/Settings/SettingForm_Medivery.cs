﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
    //20210128101307 furukawa Mediveryの設定画面
    

    public partial class SettingForm_Medivery : Form
    {
        DataTable dtMedivery = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        Object tmpCellValue = null;//セルの現在値

        int dgvCurrentRowIndex = 0;

        public SettingForm_Medivery()
        {
            InitializeComponent();

            CommonTool.setFormColor(this);

            disp();
            
        }


        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {

            if (Settings.DataBaseHost.Contains("aws"))
            {
                cn.ConnectionString =
                   $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password=medibrain1128;CommandTimeout={Settings.CommandTimeout};sslmode=1";
            }
            else
            {
                //jyuseiのDBだけはsettings.xmlから取得
                cn.ConnectionString =
                    $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout={Settings.CommandTimeout};";
            }


            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine(" select ");

            sb.AppendLine("  i.insurerid           as 保険者ID ");                          //保険者ID（メホール上）
            sb.AppendLine(" ,i.insurername         as 保険者名");                           //保険者名            
            sb.AppendLine(" , m.hname              as 被保険者名 ");                        //被保険者名
            sb.AppendLine(" , m.pname              as 受療者名 ");                          //受療者名
            sb.AppendLine(" , m.hzip               as 被保険者郵便番号 ");                  //被保険者郵便番号
            sb.AppendLine(" , m.haddress           as 被保険者住所 ");                      //被保険者住所
            sb.AppendLine(" , m.sname              as 医療機関名（施術所名 ");              //医療機関名（施術所名
            sb.AppendLine(" , m.fvisittimes        as 施術日数 ");                          //施術日数
            sb.AppendLine(" , m.paymentcode        as 支払先コード ");                      //支払先コード
            sb.AppendLine(" , m.birthday           as 受療者生年月日 ");                    //受療者生年月日
            sb.AppendLine(" , m.hnum               as 被保険者番号 ");                      //被保険者番号
            sb.AppendLine(" , m.mediym             as 診療年月 ");                          //診療年月
            sb.AppendLine(" , m.counteddays        as 往療回数 ");                          //往療回数
            sb.AppendLine(" , m.total              as 合計金額 ");                          //合計金額
            sb.AppendLine(" , m.charge             as 請求金額 ");                          //請求金額
            sb.AppendLine(" , m.partial            as 患者一時負担金 ");                    //患者一時負担金
            sb.AppendLine(" , m.drname             as 施術者名 ");                          //施術者名
            sb.AppendLine(" , m.accountname        as 口座名義 ");                          //口座名義
            sb.AppendLine(" , m.dates              as 診療日 ");                            //診療日
            sb.AppendLine(" , m.description_csv    as 説明文等のカンマ区切り文字列 "); 		//説明文等のカンマ区切り文字列

            sb.AppendLine(" from  ");
            sb.AppendLine(" medivery m ");
            sb.AppendLine(" right join  ");
            sb.AppendLine(" insurer i on  ");
            sb.AppendLine(" m.insurerid=i.insurerid ");
            sb.AppendLine(" order by i.insurerid ");



            cmd.CommandText = sb.ToString();


            da.SelectCommand = cmd;            
            da.Fill(dtMedivery);
            dgv.DataSource = dtMedivery;

            dgv.Columns["保険者ID"].Width = 60;
            dgv.Columns["保険者名"].Width = 180;            
            dgv.Columns["被保険者名"].Width = 50;
            dgv.Columns["受療者名"].Width = 50;
            dgv.Columns["被保険者郵便番号"].Width = 50;
            dgv.Columns["被保険者住所"].Width = 50;
            dgv.Columns["医療機関名（施術所名"].Width = 50;
            dgv.Columns["施術日数"].Width = 50;
            dgv.Columns["支払先コード"].Width = 50;
            dgv.Columns["受療者生年月日"].Width = 50;
            dgv.Columns["被保険者番号"].Width = 50;
            dgv.Columns["診療年月"].Width = 50;
            dgv.Columns["往療回数"].Width = 50;
            dgv.Columns["合計金額"].Width = 50;
            dgv.Columns["請求金額"].Width = 50;
            dgv.Columns["患者一時負担金"].Width = 50;
            dgv.Columns["施術者名"].Width = 50;
            dgv.Columns["口座名義"].Width = 50;
            dgv.Columns["診療日"].Width = 50;
            dgv.Columns["説明文等のカンマ区切り文字列"].Width = 550;

            
            DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            if (!dgv.Columns.Contains("削除"))
            {
                bc.Name = "削除";
                bc.Text = "削除";
                bc.HeaderText = "削除";
                bc.UseColumnTextForButtonValue = true;
                bc.Width = 60;
                dgv.Columns.Insert(0, bc);
            }
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
            dgv.Columns[2].Frozen = true;
            
        }

        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            upd();

            CommonTool.Restart_Mejor();
        }

        private void ChkUsing(int insurerid,Npgsql.NpgsqlCommand cmd)
        {
            try
            {
                sb.Remove(0, sb.ToString().Length);
                sb.AppendLine($"select * from medivery where insurerid={insurerid}");
                cmd.CommandText = sb.ToString();

                var res = cmd.ExecuteScalar();

                if (res == null)
                {
                   
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($"insert into medivery values({insurerid},false,false,false,false,false,false,false,'{string.Empty}',false,false," +
                        $"false,false,false,false,false,false,false,false)");
                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
        }

        private void upd()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            int dgv_insurerid = int.Parse(dgv.Rows[dgv.CurrentRow.Index].Cells["保険者ID"].Value.ToString());
            
            DataRow dr = dtMedivery.Select($"保険者ID={dgv_insurerid}")[0];
            
            
         //   foreach (DataRow dr in dtMedivery.Rows)
         //   {
               


                if (dr.RowState == DataRowState.Added)
                {
                    //col = 0;

                    //sb.Remove(0, sb.ToString().Length);
                    //sb.AppendLine(" insert into medivery values  (");

                    
                    //sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	'{11}',	'{12}',	'{13}' ,'{14}' ,'{15}' ",
                    //     dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //     dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //     dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //     dr[col++].ToString(), dr[col++].ToString());


                    //sb.AppendLine(");");

                    //lstsql.Add(sb.ToString());
                }
                else if (dr.RowState == DataRowState.Modified)
                {

                    

                    //新規作成はChkUsingでやっているので更新しかあり得ない

                    col = 2;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update medivery set ");

                    sb.AppendFormat("   hname      ={0}", dr["被保険者名"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , pname      ={0}", dr["受療者名"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , hzip       ={0}", dr["被保険者郵便番号"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , haddress   ={0}", dr["被保険者住所"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , sname      ={0}", dr["医療機関名（施術所名"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , fvisittimes={0}", dr["施術日数"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , paymentcode={0}", dr["支払先コード"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , birthday   ={0}", dr["受療者生年月日"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , hnum       ={0}", dr["被保険者番号"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , mediym     ={0}", dr["診療年月"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , counteddays={0}", dr["往療回数"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , total      ={0}", dr["合計金額"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , charge     ={0}", dr["請求金額"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , partial    ={0}", dr["患者一時負担金"].ToString() == "True" ? true : false);
                    sb.AppendFormat(" , drname     ={0}", dr["施術者名"].ToString() == "True" ? true : false);
                    // sb.AppendFormat(" , accountname  ={0}",dr["口座名義"].ToString()=="True" ? true:false);
                    sb.AppendFormat(" , dates    ={0}", dr["診療日"].ToString() == "True" ? true : false);


                    string strDescription = string.Empty;
                

                    //db項目名、画面項目名、説明文、X、Y、1回目入力させるか、画像倍率
                    string strmediym = "\"mediym\",\"診療年月\",\"\",\"0\",\"0\",\"false\"";
                    string strhnum = "\"hnum\",\"記号番号\",\"ハイフンがある場合は入力\",\"800\",\"0\",\"false\"";
                    string strhname = "\"hname\",\"被保険者氏名\",\"姓名の間は全角スペース\",\"80\",\"280\",\"true\",\"0.7\"";
                    string strpname = "\"pname\",\"受療者氏名\",\"姓名の間は全角スペース\",\"80\",\"280\",\"true\",\"0.7\"";
                    string strbirthday = "\"birthday\",\"生年月日\",\"\",\"80\",\"220\",\"false\"";
                    string strhzip = "\"hzip\",\"郵便番号（〒）\",\"ハイフンなし\",\"800\",\"220\",\"true\",\"0.7\"";
                    string strhaddress = "\"haddress\",\"住所\",\"枝番号は半角\",\"800\",\"220\",\"true\",\"0.7\"";
                    string strcounteddays = "\"counteddays\",\"実日数\",\"\",\"300\",\"800\",\"false\"";
                    string strtotal = "\"total\",\"合計\",\"\",\"800\",\"1500\",\"false\"";
                    string strcharge = "\"charge\",\"請求金額\",\"\",\"800\",\"1500\",\"false\"";
                    string strsname = "\"sname\",\"施術所名,\"\",\"80\",\"2700\",\"true\",\"0.7\"";
                    string strpaymentcode = "\"paymentcode\",\"支払先コード\",\"半角数字（5桁）\",\"800\",\"0\"";
                    string strfvisittimes = "\"fvisittimes\",\"往療回数\",\"\",\"425\",\"1200\"";
                    string strdrname = "\"drname\",\"柔整師名\",\"姓名の間は全角スペース\",\"80\",\"2300\",\"false\"";
                    string strdates = "\"dates\",\"診療日\",\"スペースまたはドット(.)区切りですべての診療日を入力\",\"80\",\"1100\"";
                    string strpartial= "\"partial\",\"一部負担\",\"\",\"800\",\"1500\",\"false\"";


                    strDescription += dr["被保険者名"].ToString() == "True" ? strhname + "\r\n": string.Empty;
                    strDescription += dr["受療者名"].ToString() == "True" ? strpname + "\r\n": string.Empty;
                    strDescription += dr["被保険者郵便番号"].ToString() == "True" ? strhzip + "\r\n": string.Empty;
                    strDescription += dr["被保険者住所"].ToString() == "True" ? strhaddress + "\r\n": string.Empty;
                    strDescription += dr["医療機関名（施術所名"].ToString() == "True" ? strsname + "\r\n": string.Empty;
                    strDescription += dr["施術日数"].ToString() == "True" ? strfvisittimes + "\r\n": string.Empty;
                    strDescription += dr["支払先コード"].ToString() == "True" ? strpaymentcode + "\r\n": string.Empty;
                    strDescription += dr["受療者生年月日"].ToString() == "True" ? strbirthday + "\r\n": string.Empty;
                    strDescription += dr["被保険者番号"].ToString() == "True" ? strhnum + "\r\n": string.Empty;
                    strDescription += dr["診療年月"].ToString() == "True" ? strmediym + "\r\n": string.Empty;
                    strDescription += dr["往療回数"].ToString() == "True" ? strcounteddays + "\r\n": string.Empty;
                    strDescription += dr["合計金額"].ToString() == "True" ? strtotal + "\r\n": string.Empty;
                    strDescription += dr["請求金額"].ToString() == "True" ? strcharge + "\r\n": string.Empty;
                    strDescription += dr["患者一時負担金"].ToString() == "True" ? strpartial + "\r\n": string.Empty;
                    strDescription += dr["施術者名"].ToString() == "True" ? strdrname + "\r\n": string.Empty;
                    //未使用 strDescription += dr["口座名義"].ToString() == "True" ? straccountname+ "\r\n": string.Empty;
                    strDescription += dr["診療日"].ToString() == "True" ? strdates + "\r\n": string.Empty;


                    sb.AppendFormat(" ,	description_csv		 ='{0}' ",strDescription);
                    sb.AppendFormat("	where insurerid		={0};", dgv_insurerid);

                    lstsql.Add(sb.ToString());
            //    }


            }

            if (lstsql.Count == 0) return;

            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {
                cmd.Transaction = tran;
                foreach (string s in lstsql)
                {
                    ChkUsing(dgv_insurerid,cmd);

                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新した");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtMedivery.Clear();

                cn.Close();
                disp();
            }


        }


        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine("select max(insurerid) id from insurer");
            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

        /// <summary>
        /// 複製ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btncopy_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index < 0) return;

            DataGridViewRow dgvr = new DataGridViewRow();

            dgvr = dgv.Rows[dgv.CurrentRow.Index];
            DataRow dr;

            dr = dtMedivery.NewRow();
            
            for(int r=1;r<dgvr.Cells.Count;r++)
            {
                if (r == 1)
                {
                    //最大のID取得
                    dr[r-1] = getMaxID();
                }
                else
                {
                    dr[r-1] = dgvr.Cells[r].Value.ToString();
                }
            }

            dtMedivery.Rows.Add(dr);
            upd();
        }


        /// <summary>
        /// セルクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

           //以下ボタン処理

            if (e.ColumnIndex != 0) return;

            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv.CurrentRow.Index;

            string id = dgv.Rows[dgv.CurrentRow.Index].Cells[1].Value.ToString();


            if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
                Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {


                sb.Remove(0, sb.ToString().Length);
                sb.AppendLine($" delete from  medivery where insurerid={id}");


                cmd.CommandText = sb.ToString();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Mediveryテーブルから削除しました。画面から行は消えません");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dtMedivery.Clear();

                    cn.Close();
                    disp();
                }
            }
        }

        private void SettingForm_Medivery_Shown(object sender, EventArgs e)
        {
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }

        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {
            string fld = "ｽｷｬﾝｵｰﾀﾞｰ";//デフォルトだけ決めとく
            fld = cmbDup.Text;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach(DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach(DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString()){
                        
                        cnt++;
                        if (cnt > 1)
                        {
                            
                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }
                  
                }

            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //現在の値を取っておく
            tmpCellValue = dgv.CurrentCell.Value;

        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.BackColor = Color.Red;
            dgv.CurrentCell.Style = cs;
     
        }

      
    }
}
