﻿namespace Mejor
{
    partial class SettingForm_Medivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.btncopy = new System.Windows.Forms.Button();
            this.btnChkDup = new System.Windows.Forms.Button();
            this.cmbDup = new System.Windows.Forms.ComboBox();
            this.lblserver = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 12);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(1760, 789);
            this.dgv.TabIndex = 0;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(1660, 813);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 35);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(1540, 813);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(112, 35);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "更新";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // btncopy
            // 
            this.btncopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btncopy.Location = new System.Drawing.Point(1420, 813);
            this.btncopy.Margin = new System.Windows.Forms.Padding(4);
            this.btncopy.Name = "btncopy";
            this.btncopy.Size = new System.Drawing.Size(112, 35);
            this.btncopy.TabIndex = 2;
            this.btncopy.Text = "複製";
            this.btncopy.UseVisualStyleBackColor = true;
            this.btncopy.Visible = false;
            this.btncopy.Click += new System.EventHandler(this.btncopy_Click);
            // 
            // btnChkDup
            // 
            this.btnChkDup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChkDup.Location = new System.Drawing.Point(887, 814);
            this.btnChkDup.Margin = new System.Windows.Forms.Padding(4);
            this.btnChkDup.Name = "btnChkDup";
            this.btnChkDup.Size = new System.Drawing.Size(112, 35);
            this.btnChkDup.TabIndex = 2;
            this.btnChkDup.Text = "重複チェック";
            this.btnChkDup.UseVisualStyleBackColor = true;
            this.btnChkDup.Visible = false;
            this.btnChkDup.Click += new System.EventHandler(this.btnChkDup_Click);
            // 
            // cmbDup
            // 
            this.cmbDup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbDup.FormattingEnabled = true;
            this.cmbDup.Location = new System.Drawing.Point(1006, 822);
            this.cmbDup.Name = "cmbDup";
            this.cmbDup.Size = new System.Drawing.Size(160, 21);
            this.cmbDup.TabIndex = 4;
            this.cmbDup.Visible = false;
            // 
            // lblserver
            // 
            this.lblserver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblserver.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblserver.Location = new System.Drawing.Point(12, 820);
            this.lblserver.Name = "lblserver";
            this.lblserver.Size = new System.Drawing.Size(591, 21);
            this.lblserver.TabIndex = 5;
            this.lblserver.Text = "label1";
            this.lblserver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SettingForm_Medivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1784, 862);
            this.Controls.Add(this.lblserver);
            this.Controls.Add(this.cmbDup);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.btnChkDup);
            this.Controls.Add(this.btncopy);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.dgv);
            this.Name = "SettingForm_Medivery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Medivery設定";
            this.Shown += new System.EventHandler(this.SettingForm_Medivery_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button btncopy;
        private System.Windows.Forms.Button btnChkDup;
        private System.Windows.Forms.ComboBox cmbDup;
        private System.Windows.Forms.Label lblserver;
    }
}