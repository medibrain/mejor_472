﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20210716154232 furukawa st ////////////////////////
//現在の入力状況



namespace Mejor
{

    public partial class CurrentInput:Form
    {
       
        public CurrentInput()
        {
            InitializeComponent();

            //20210128102133 furukawa st ////////////////////////
            //背景色変更してわかりやすいように
            
            CommonTool.setFormColor(this);
            //20210128102133 furukawa ed ////////////////////////

            disp();
            
        }



        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {
            DataTable dtInput = new DataTable();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();


            if (cn.State == ConnectionState.Open) cn.Close();

            if (Settings.DataBaseHost.Contains("aws"))
            {
                //20220214132411 furukawa NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                cn.ConnectionString =
                   $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password=medibrain1128;CommandTimeout={Settings.CommandTimeout};sslmode=1;convert infinity datetime=true";
            }
            else
            {
                //jyuseiのDBだけはsettings.xmlから取得
                //20220214133647 furukawa NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                
                cn.ConnectionString =
                    $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout={Settings.CommandTimeout};convert infinity datetime=true";
            }


            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine(" select * from current_input");
            
            
            cmd.CommandText = sb.ToString();



            da.SelectCommand = cmd;            
            da.Fill(dtInput);
            dgv.DataSource = dtInput;

            cmd.Dispose();
            cn.Close();

          
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpd_Click(object sender, EventArgs e)
        {
            disp();
        }
    }
}
