﻿namespace Mejor
{
    partial class SettingMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSystem = new System.Windows.Forms.Button();
            this.buttonSick = new System.Windows.Forms.Button();
            this.buttonModify = new System.Windows.Forms.Button();
            this.buttonInputLog = new System.Windows.Forms.Button();
            this.buttonInputLogOutput = new System.Windows.Forms.Button();
            this.buttonInsurerSettings = new System.Windows.Forms.Button();
            this.buttonMediveryUseInsurerXML = new System.Windows.Forms.Button();
            this.buttonInsurerList = new System.Windows.Forms.Button();
            this.buttonCurrentInput = new System.Windows.Forms.Button();
            this.buttonUserSetting = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSystem
            // 
            this.buttonSystem.Enabled = false;
            this.buttonSystem.Location = new System.Drawing.Point(46, 36);
            this.buttonSystem.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSystem.Name = "buttonSystem";
            this.buttonSystem.Size = new System.Drawing.Size(211, 46);
            this.buttonSystem.TabIndex = 0;
            this.buttonSystem.Text = "メホール本体設定";
            this.buttonSystem.UseVisualStyleBackColor = true;
            this.buttonSystem.Click += new System.EventHandler(this.buttonSystem_Click);
            // 
            // buttonSick
            // 
            this.buttonSick.Location = new System.Drawing.Point(285, 36);
            this.buttonSick.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSick.Name = "buttonSick";
            this.buttonSick.Size = new System.Drawing.Size(211, 46);
            this.buttonSick.TabIndex = 5;
            this.buttonSick.Text = "傷病名マスター更新";
            this.buttonSick.UseVisualStyleBackColor = true;
            this.buttonSick.Click += new System.EventHandler(this.buttonSick_Click);
            // 
            // buttonModify
            // 
            this.buttonModify.Location = new System.Drawing.Point(285, 89);
            this.buttonModify.Margin = new System.Windows.Forms.Padding(4);
            this.buttonModify.Name = "buttonModify";
            this.buttonModify.Size = new System.Drawing.Size(211, 46);
            this.buttonModify.TabIndex = 6;
            this.buttonModify.Text = "修飾語マスター更新";
            this.buttonModify.UseVisualStyleBackColor = true;
            this.buttonModify.Click += new System.EventHandler(this.buttonModify_Click);
            // 
            // buttonInputLog
            // 
            this.buttonInputLog.Location = new System.Drawing.Point(285, 214);
            this.buttonInputLog.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInputLog.Name = "buttonInputLog";
            this.buttonInputLog.Size = new System.Drawing.Size(211, 46);
            this.buttonInputLog.TabIndex = 8;
            this.buttonInputLog.Text = "入力ログ集計";
            this.buttonInputLog.UseVisualStyleBackColor = true;
            this.buttonInputLog.Click += new System.EventHandler(this.buttonInputLog_Click);
            // 
            // buttonInputLogOutput
            // 
            this.buttonInputLogOutput.Location = new System.Drawing.Point(285, 160);
            this.buttonInputLogOutput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInputLogOutput.Name = "buttonInputLogOutput";
            this.buttonInputLogOutput.Size = new System.Drawing.Size(211, 46);
            this.buttonInputLogOutput.TabIndex = 7;
            this.buttonInputLogOutput.Text = "入力ログ出力";
            this.buttonInputLogOutput.UseVisualStyleBackColor = true;
            this.buttonInputLogOutput.Click += new System.EventHandler(this.buttonInputLogOutput_Click);
            // 
            // buttonInsurerSettings
            // 
            this.buttonInsurerSettings.Enabled = false;
            this.buttonInsurerSettings.Location = new System.Drawing.Point(46, 160);
            this.buttonInsurerSettings.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInsurerSettings.Name = "buttonInsurerSettings";
            this.buttonInsurerSettings.Size = new System.Drawing.Size(211, 46);
            this.buttonInsurerSettings.TabIndex = 2;
            this.buttonInsurerSettings.Text = "メホール保険者設定";
            this.buttonInsurerSettings.UseVisualStyleBackColor = true;
            this.buttonInsurerSettings.Click += new System.EventHandler(this.buttonInsurerSettings_Click);
            // 
            // buttonMediveryUseInsurerXML
            // 
            this.buttonMediveryUseInsurerXML.Enabled = false;
            this.buttonMediveryUseInsurerXML.Location = new System.Drawing.Point(46, 278);
            this.buttonMediveryUseInsurerXML.Margin = new System.Windows.Forms.Padding(4);
            this.buttonMediveryUseInsurerXML.Name = "buttonMediveryUseInsurerXML";
            this.buttonMediveryUseInsurerXML.Size = new System.Drawing.Size(211, 46);
            this.buttonMediveryUseInsurerXML.TabIndex = 4;
            this.buttonMediveryUseInsurerXML.Text = "Medivery使用保険者設定";
            this.buttonMediveryUseInsurerXML.UseVisualStyleBackColor = true;
            this.buttonMediveryUseInsurerXML.Click += new System.EventHandler(this.buttonMediveryUseInsurerXML_Click);
            // 
            // buttonInsurerList
            // 
            this.buttonInsurerList.Enabled = false;
            this.buttonInsurerList.Location = new System.Drawing.Point(46, 214);
            this.buttonInsurerList.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInsurerList.Name = "buttonInsurerList";
            this.buttonInsurerList.Size = new System.Drawing.Size(211, 46);
            this.buttonInsurerList.TabIndex = 3;
            this.buttonInsurerList.Text = "メホール登録保険者一覧出力";
            this.buttonInsurerList.UseVisualStyleBackColor = true;
            this.buttonInsurerList.Click += new System.EventHandler(this.buttonInsurerList_Click);
            // 
            // buttonCurrentInput
            // 
            this.buttonCurrentInput.Location = new System.Drawing.Point(285, 278);
            this.buttonCurrentInput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCurrentInput.Name = "buttonCurrentInput";
            this.buttonCurrentInput.Size = new System.Drawing.Size(211, 46);
            this.buttonCurrentInput.TabIndex = 9;
            this.buttonCurrentInput.Text = "30分以内の入力状況";
            this.buttonCurrentInput.UseVisualStyleBackColor = true;
            this.buttonCurrentInput.Click += new System.EventHandler(this.buttonCurrentInput_Click);
            // 
            // buttonUserSetting
            // 
            this.buttonUserSetting.Location = new System.Drawing.Point(46, 90);
            this.buttonUserSetting.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUserSetting.Name = "buttonUserSetting";
            this.buttonUserSetting.Size = new System.Drawing.Size(211, 46);
            this.buttonUserSetting.TabIndex = 1;
            this.buttonUserSetting.Text = "メホールユーザ設定";
            this.buttonUserSetting.UseVisualStyleBackColor = true;
            this.buttonUserSetting.Click += new System.EventHandler(this.buttonUserSetting_Click);
            // 
            // SettingMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 369);
            this.Controls.Add(this.buttonInsurerList);
            this.Controls.Add(this.buttonInputLogOutput);
            this.Controls.Add(this.buttonCurrentInput);
            this.Controls.Add(this.buttonInputLog);
            this.Controls.Add(this.buttonModify);
            this.Controls.Add(this.buttonSick);
            this.Controls.Add(this.buttonMediveryUseInsurerXML);
            this.Controls.Add(this.buttonInsurerSettings);
            this.Controls.Add(this.buttonUserSetting);
            this.Controls.Add(this.buttonSystem);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "SettingMenuForm";
            this.Text = "各種設定";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSystem;
        private System.Windows.Forms.Button buttonSick;
        private System.Windows.Forms.Button buttonModify;
        private System.Windows.Forms.Button buttonInputLog;
        private System.Windows.Forms.Button buttonInputLogOutput;
        private System.Windows.Forms.Button buttonInsurerSettings;
        private System.Windows.Forms.Button buttonMediveryUseInsurerXML;
        private System.Windows.Forms.Button buttonInsurerList;
        private System.Windows.Forms.Button buttonCurrentInput;
        private System.Windows.Forms.Button buttonUserSetting;
    }
}