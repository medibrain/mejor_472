﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
    //20210128101307 furukawa Mediveryの設定画面改良型
    

    public partial class SettingForm_Medivery2 : Form
    {
        DataTable dtMedivery = new DataTable();
        DataSet dsMedivery_setting = new DataSet();

        //設定用
        DataTable dtSetting = new DataTable();

        /// <summary>
        /// medivery_setting初期xml文字列
        /// //20210510130233 furukawa 性別/口座名義追加
        /// //20210706091657 furukawa 登録記号番号追加
        /// </summary>
        
        //20220711133351 furukawa st ////////////////////////
        //外部業者用csv出力時に邪魔なので改行コードを削除        
        public static string strInitSetting =
            "<fields>" +
            "<hname><use>false</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>" +
            "<pname><use>false</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>" +
            "<psex><use>false</use><label>受療者性別</label><desc>男=1、女=2</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></psex>" + 
            "<hzip><use>false</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>" +
            "<haddress><use>false</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>" +
            "<sname><use>false</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>" +
            "<fvisittimes><use>false</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>" +
            "<paymentcode><use>false</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>" +
            "<accname><use>false</use><label>口座名義</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></accname>" +
            "<birthday><use>false</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>" +
            "<hnum><use>false</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>" +
            "<mediym><use>false</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>" +
            "<counteddays><use>false</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>" +
            "<total><use>false</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>" +
            "<charge><use>false</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>" +
            "<partial><use>false</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>" +
            "<drname><use>false</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>" +
            "<drnum><use>false</use><label>登録記号番号</label><desc></desc><x>1200</x><y>2300</y><firsttime>true</firsttime><ratio>0.7</ratio></drnum>" +
            "<dates><use>false</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>" +
            "</fields>";


        //public static string strInitSetting =
        // "<fields>" +
        // "<hname><use>false</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>\r\n" +
        // "<pname><use>false</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>\r\n" +
        // "<psex><use>false</use><label>受療者性別</label><desc>男=1、女=2</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></psex>\r\n" +
        // "<hzip><use>false</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>\r\n" +
        // "<haddress><use>false</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>\r\n" +
        // "<sname><use>false</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>\r\n" +
        // "<fvisittimes><use>false</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>\r\n" +
        // "<paymentcode><use>false</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>\r\n" +
        // "<accname><use>false</use><label>口座名義</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></accname>\r\n" +
        // "<birthday><use>false</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>\r\n" +
        // "<hnum><use>false</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>\r\n" +
        // "<mediym><use>false</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>\r\n" +
        // "<counteddays><use>false</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>\r\n" +
        // "<total><use>false</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>\r\n" +
        // "<charge><use>false</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>\r\n" +
        // "<partial><use>false</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>\r\n" +
        // "<drname><use>false</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>\r\n" +
        // "<drnum><use>false</use><label>登録記号番号</label><desc></desc><x>1200</x><y>2300</y><firsttime>true</firsttime><ratio>0.7</ratio></drnum>\r\n" +
        // "<dates><use>false</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>\r\n" +
        // "</fields>";

        //20220711133351 furukawa ed ////////////////////////





        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        
        int dgvCurrentRowIndex = 0;

        public SettingForm_Medivery2()
        {
            InitializeComponent();

            CommonTool.setFormColor(this);

            disp_ins();
            
        }


        /// <summary>
        /// 保険者側表示
        /// </summary>
        private void disp_ins()
        {

            if (Settings.DataBaseHost.Contains("aws"))
            {
                //20220214134044 furukawa st ////////////////////////
                //NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                
                cn.ConnectionString =
                   $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password=medibrain1128;CommandTimeout={Settings.CommandTimeout};sslmode=1;convert infinity datetime = true";
            }
            else
            {
                //jyuseiのDBだけはsettings.xmlから取得
                cn.ConnectionString =
                    $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout={Settings.CommandTimeout};convert infinity datetime = true";
                //20220214134044 furukawa ed ////////////////////////
            }


            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine(" select ");

            sb.AppendLine("  i.insurerid    as 保険者ID ");           //保険者ID（メホール上）
            sb.AppendLine(" ,i.insurername  as 保険者名");            //保険者名    
            sb.AppendLine(" ,m.usemedivery  as medivery使用");        //medivery自体を使用するか
            sb.AppendLine(" ,m.setting      as setting");             //設定xml文字列                       
            sb.AppendLine(" , m.dtupd       as 更新日 ");             //更新日
            sb.AppendLine(" , u.name    as 更新ユーザ "); 	          //更新者

            sb.AppendLine(" from  ");
            sb.AppendLine(" medivery_setting m ");
            sb.AppendLine(" right join  ");
            sb.AppendLine(" insurer i on  ");
            sb.AppendLine(" m.insurerid=i.insurerid ");

            //20220630161434 furukawa st ////////////////////////
            //usersテーブルからusers_settingテーブルに移行
            
            sb.AppendLine(" left join users_setting u on ");
            //sb.AppendLine(" left join users u on ");
            //20220630161434 furukawa ed ////////////////////////


            sb.AppendLine(" m.userid=u.userid ");

            sb.AppendLine(" order by i.insurerid ");



            cmd.CommandText = sb.ToString();


            da.SelectCommand = cmd;            
            da.Fill(dtMedivery);
          

            dgv_ins.DataSource = dtMedivery;
            dgv_ins.Columns["保険者ID"].Width = 60;
            dgv_ins.Columns["保険者名"].Width = 180;
            dgv_ins.Columns["medivery使用"].Width = 60;
            dgv_ins.Columns["setting"].Width = 180;
            dgv_ins.Columns["更新日"].Width = 100;
            dgv_ins.Columns["更新ユーザ"].Width = 100;

            dgv_ins.Columns["setting"].Visible = false;

            dgv_ins.Columns["保険者ID"].ReadOnly = true;
            dgv_ins.Columns["保険者名"].ReadOnly = true;
            dgv_ins.Columns["setting"].ReadOnly = true;
            dgv_ins.Columns["更新日"].ReadOnly = true;
            dgv_ins.Columns["更新ユーザ"].ReadOnly = true;

        }



        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            update_medivery_settings();

            CommonTool.Restart_Mejor();
        }


   
        /// <summary>
        /// 更新
        /// </summary>
        private void update_medivery_settings()
        { 
            try
            {
                foreach (DataRow r in dtSetting.Rows)
                {
                    //if (r.RowState == DataRowState.Modified)
                    if (r.RowState != DataRowState.Unchanged)
                    {
                        string strIns = dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["保険者id"].Value.ToString();
                        //string strIns = r["保険者ID"].ToString();
                        upd_setting(strIns);
                    }
                }

                foreach (DataRow r in dtMedivery.Rows)
                {
                    //if (r.RowState == DataRowState.Modified)
                    if (r.RowState != DataRowState.Unchanged)
                    {
                        string strIns = dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["保険者id"].Value.ToString();
                        
                        upd_ins(strIns);
                    }
                }

                MessageBox.Show("正常終了");
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                
            }
            finally
            {
                cmd.Dispose();
            }
        }

     

        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine("select max(insurerid) id from insurer");
            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

        /// <summary>
        /// セルクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

           //以下ボタン処理

            if (e.ColumnIndex != 0) return;

            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv_ins.CurrentRow.Index;

            string id = dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells[1].Value.ToString();


            if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
                Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {


                sb.Remove(0, sb.ToString().Length);
                sb.AppendLine($" delete from  medivery where insurerid={id}");


                cmd.CommandText = sb.ToString();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Mediveryテーブルから削除しました。画面から行は消えません");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dtMedivery.Clear();

                    cn.Close();
                    disp_ins();
                }
            }
        }

          
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// 更新用xml文字列作成
        /// </summary>
        /// <param name="strXML"></param>
        /// <returns></returns>
        private string CreateXMLStringForUpdate(string strXML)
        {
            string res = string.Empty;

            //xml文字列ロード
            System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
            xml.LoadXml(strXML.Replace("\r\n", ""));
            //xml.LoadXml(strXML);

            //rootノード取得
            System.Xml.XmlElement root = xml.DocumentElement;

            //rootノード付ける

            //20220711133512 furukawa st ////////////////////////
            //外部業者用csv出力時に邪魔なので改行コードを削除            
            res += $"<{root.Name}>";
            //res += $"<{root.Name}>\r\n";
            //20220711133512 furukawa ed ////////////////////////


            //子の数分だけループ
            System.Xml.XmlNode nextNode = root.FirstChild;

            for (int r = 0; r < root.ChildNodes.Count; r++)
            {
                if (dtSetting.Rows.Count == 0) break;

                DataRow drsetting = dtSetting.Rows[r];

                //次のノード（項目名）に入替
                nextNode = root.ChildNodes[r];

                //項目名ノード開始
                res += $"<{nextNode.Name}>";

                for (int c = 0; c < dtSetting.Columns.Count; c++)
                {
                    //次の要素に入替
                    System.Xml.XmlNode fields = nextNode.ChildNodes[c];


                    //要素の値を取得
                    fields.InnerText = drsetting[c].ToString();

                    res += fields.OuterXml;


                }
                //項目名ノード閉じる
                //20220711133544 furukawa st ////////////////////////
                //外部業者用csv出力時に邪魔なので改行コードを削除                
                res += $"</{nextNode.Name}>";
                //res += $"</{nextNode.Name}>\r\n";
                //20220711133544 furukawa ed ////////////////////////
            }

            //rootノード閉じる
            res += $"</{root.Name}>";

            return res;

        }
   
        /// <summary>
        /// 設定更新処理
        /// </summary>
        /// <param name="strInsID">保険者ID</param>
        private void upd_setting(string strInsID)
        {
            string res = string.Empty;
            DataRow[] dr = dtMedivery.Select($"保険者ID={strInsID}");

            if (dr.Length == 0) return;

            string strxml = dr[0]["setting"].ToString();
            
            res = CreateXMLStringForUpdate(strxml);                      

            try
            {
                string orgDB=DB.GetMainDBName();
                DB.SetMainDBName("jyusei");


                bool usemedivery=bool.Parse(dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["medivery使用"].Value.ToString());

                DB.Command cmd = DB.Main.CreateCmd(
                    $"update medivery_setting set " +
                    $" usemedivery='{usemedivery}'" +
                    $",setting='{res}'" +
                    $",dtupd='{DateTime.Now.ToString()}'" +
                    $",userid={User.CurrentUser.UserID}" +
                    $" where insurerid={strInsID}");

                cmd.TryExecuteNonQuery();

                DB.SetMainDBName(orgDB);

            }
            catch(Exception ex)
            {
                
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                
            }
            
        }


        /// <summary>
        /// 設定更新処理
        /// </summary>
        /// <param name="strInsID">保険者ID</param>
        private void upd_ins(string strInsID)
        {
            string res = string.Empty;
            DataRow[] dr = dtMedivery.Select($"保険者ID={strInsID}");

            if (dr.Length == 0) return;
     
            try
            {
                string orgDB = DB.GetMainDBName();
                DB.SetMainDBName("jyusei");


                bool usemedivery = bool.Parse(dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["medivery使用"].Value.ToString());

                DB.Command cmd = DB.Main.CreateCmd(
                    $"update medivery_setting set " +
                    $" usemedivery='{usemedivery}'" +                  
                    $",dtupd='{DateTime.Now.ToString()}'" +
                    $",userid={User.CurrentUser.UserID}" +
                    $" where insurerid={strInsID}");

                cmd.TryExecuteNonQuery();

                DB.SetMainDBName(orgDB);

            }
            catch (Exception ex)
            {

                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);

            }

        }



        /// <summary>
        /// 設定項目表示
        /// </summary>
        private void dispSetting()
        {
            DataRow[] dr = dtMedivery.Select($"保険者ID={dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells[0].Value.ToString()}");
        
            string strxml = dr[0]["setting"].ToString();

            //使用する場合のみ設定詳細表示
            bool flgdisp = bool.Parse(dr[0]["medivery使用"].ToString());

            dgv_setting.DataSource = null;
            dtSetting.Clear();

            try
            {

                #region xml文字列ロード

                System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                xml.LoadXml(strxml);

                //rootノード取得
                System.Xml.XmlElement root = xml.DocumentElement;

                dtSetting.Clear();

                //使用する場合のみ設定詳細表示
                if (!flgdisp) return;


                //rootの一つ下の子の数で列作る             

                System.Xml.XmlNode firstChild = root.FirstChild;
                for (int c = 0; c < firstChild.ChildNodes.Count; c++)
                {
                    if (!dtSetting.Columns.Contains(firstChild.ChildNodes[c].Name)) dtSetting.Columns.Add(firstChild.ChildNodes[c].Name);
                }

                System.Xml.XmlNode nextnd;

                //子の数分だけループ
                for (int r = 0; r < root.ChildNodes.Count; r++)
                {
                    DataRow drsetting = dtSetting.NewRow();

                    //次のノード（項目名）に入替
                    nextnd = root.ChildNodes[r];

                    for (int c = 0; c < dtSetting.Columns.Count; c++)
                    {
                        System.Xml.XmlNode fields = nextnd.ChildNodes[c];

                        if (fields == null) continue;
                        //要素の値を取得
                        drsetting[c] = fields.InnerText;
                    }

                    dtSetting.Rows.Add(drsetting);

                }

                dgv_setting.DataSource = dtSetting;

                //ステータスがaddedのままになるので一旦コミット
                dtSetting.AcceptChanges();
#endregion


                DataGridViewCellStyle cs = new DataGridViewCellStyle();
                cs.BackColor = Color.LightGray;


                dgv_setting.Columns[0].HeaderText = "使用";
                dgv_setting.Columns[1].HeaderText = "項目名";
                dgv_setting.Columns[2].HeaderText = "説明";
                dgv_setting.Columns[3].HeaderText = "X座標";
                dgv_setting.Columns[4].HeaderText = "Y座標";
                dgv_setting.Columns[5].HeaderText = "1回目入力";
                dgv_setting.Columns[6].HeaderText = "画像";

                dgv_setting.Columns[0].Width = 70;
                dgv_setting.Columns[1].Width = 120;
                dgv_setting.Columns[2].Width = 180;
                dgv_setting.Columns[3].Width = 90;
                dgv_setting.Columns[4].Width = 90;
                //dgv_setting.Columns[5].Width=50;
                dgv_setting.Columns[6].Width = 70;

                dgv_setting.Columns[1].ReadOnly = true;
                dgv_setting.Columns[2].ReadOnly = true;
                dgv_setting.Columns[3].ReadOnly = true;
                dgv_setting.Columns[4].ReadOnly = true;
                //dgv_setting.Columns[5].ReadOnly=true;
                dgv_setting.Columns[6].ReadOnly = true;

                dgv_setting.Columns[1].DefaultCellStyle = cs;
                dgv_setting.Columns[2].DefaultCellStyle = cs;
                dgv_setting.Columns[3].DefaultCellStyle = cs;
                dgv_setting.Columns[4].DefaultCellStyle = cs;
                // dgv_setting.Columns[5].DefaultCellStyle=cs;
                dgv_setting.Columns[6].DefaultCellStyle = cs;


            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);

            }
        }

      
        /// <summary>
        /// セル移動した場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ins_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dispSetting();
        }

        /// <summary>
        /// 変更した場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ins_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgv_ins.CommitEdit(DataGridViewDataErrorContexts.Commit);
            if (bool.Parse(dgv_ins.Rows[e.RowIndex].Cells["medivery使用"].Value.ToString())) dispSetting();
            else dgv_setting.DataSource = null;
        }

        /// <summary>
        /// 設定xml文字列初期化
        /// </summary>
        /// <param name="strInsID"></param>
        /// <returns></returns>
        private bool InitSetting(string strInsID)
        {
            if (MessageBox.Show($"{Insurer.GetInsurer(int.Parse(strInsID)).InsurerName} の設定を初期化します。宜しいですか？",
             Application.ProductName, MessageBoxButtons.YesNo,
             MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return true;

            string orgDB = DB.GetMainDBName();
            DB.SetMainDBName("jyusei");

            try
            {
                bool usemedivery = bool.Parse(dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["medivery使用"].Value.ToString());

                DB.Command cmd = DB.Main.CreateCmd(
                    $"update medivery_setting set " +
                    $" usemedivery='{usemedivery}'" +
                    $",setting='{strInitSetting}'" +
                    $",dtupd='{DateTime.Now.ToString()}'" +
                    $",userid={User.CurrentUser.UserID}" +
                    $" where insurerid={strInsID}");

                cmd.TryExecuteNonQuery();

                //再起動
                CommonTool.Restart_Mejor();

                return true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;

            }
            finally
            {
                cmd.Dispose();
                DB.SetMainDBName(orgDB);
            }

        }

        private void buttonInitSetting_Click(object sender, EventArgs e)
        {
            string strIns = dgv_ins.Rows[dgv_ins.CurrentRow.Index].Cells["保険者id"].Value.ToString();
            if (!InitSetting(strIns)) MessageBox.Show("初期化に失敗しました。");

        }
    }
}
