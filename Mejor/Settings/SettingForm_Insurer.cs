﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20200131104223 furukawa 保険者別設定画面


namespace Mejor
{

    public partial class SettingForm_Insurer : Form
    {
        DataTable dtInsurer = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int dgvCurrentRowIndex = 0;

        /// <summary>
        /// medivery_setting初期xml文字列
        /// //20210528153429 furukawa 性別、口座名義追加        
        /// </summary>

        //20220711141210 furukawa st ////////////////////////
        //2カ所あると紛らわしいのでMedivery2のを使う
        
        string strInitSetting = SettingForm_Medivery2.strInitSetting;
        //      string strInitSetting = 
        //      "<fields>" +
        //      "<hname><use>false</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>\r\n" +
        //      "<pname><use>false</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>\r\n" +
        //      "<psex><use>false</use><label>受療者性別</label><desc>男=1、女=2</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></psex>\r\n"+
        //      "<hzip><use>false</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>\r\n" +
        //      "<haddress><use>false</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>\r\n" +
        //      "<sname><use>false</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>\r\n" +
        //      "<fvisittimes><use>false</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>\r\n" +
        //      "<paymentcode><use>false</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>\r\n" +
        //      "<accname><use>true</use><label>口座名義</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></accname>\r\n" +
        //      "<birthday><use>false</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>\r\n" +
        //      "<hnum><use>false</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>\r\n" +
        //      "<mediym><use>false</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>\r\n" +
        //      "<counteddays><use>false</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>\r\n" +
        //      "<total><use>false</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>\r\n" +
        //      "<charge><use>false</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>\r\n" +
        //      "<partial><use>false</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>\r\n" +
        //      "<drname><use>false</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>\r\n" +
        //      "<dates><use>false</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>\r\n" +
        //      "</fields>";
        //20220711141210 furukawa ed ////////////////////////


        public SettingForm_Insurer()
        {
            InitializeComponent();

            //20210128102133 furukawa st ////////////////////////
            //背景色変更してわかりやすいように
            
            CommonTool.setFormColor(this);
            //20210128102133 furukawa ed ////////////////////////

            disp();
            
        }



        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {

            if (Settings.DataBaseHost.Contains("aws"))
            {
                //20220214132543 furukawa NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった

                cn.ConnectionString =
                   $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                   $"Password=medibrain1128;CommandTimeout={Settings.CommandTimeout};sslmode=1;convert infinity datetime = true";
            }
            else
            {
                //jyuseiのDBだけはsettings.xmlから取得
                //20220214133756 furukawa NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                cn.ConnectionString =
                    $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout={Settings.CommandTimeout};convert infinity datetime=true";
            }


            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);
            
            sb.AppendLine(" select ");
            sb.AppendLine(" insurerid		保険者用ID,      ");
            sb.AppendLine(" insurername		保険者名,        ");
            sb.AppendLine(" dbname			DB名,            ");
            sb.AppendLine(" scanorder		ｽｷｬﾝｵｰﾀﾞｰ,       ");
            sb.AppendLine(" enabled			メホール上取扱,  ");
            sb.AppendLine(" insnumber		保険者番号,      ");

            //20200423163748 furukawa st ////////////////////////
            //日本語名間違い修正
            
            sb.AppendLine(" formalname		保険者正式名称,    ");
            //sb.AppendLine(" formalname		データ格納先,    ");
            //20200423163748 furukawa ed ////////////////////////


            sb.AppendLine(" insurertype		保険者タイプ,    ");
            sb.AppendLine(" viewindex		リスト表示順,    ");
            sb.AppendLine(" outputpath		データ出力先,    ");
            sb.AppendLine(" needverify		ベリファイ有無,  ");
            sb.AppendLine(" dbserver		DBサーバ,        ");
            sb.AppendLine(" dbport			DBポート,        ");
            sb.AppendLine(" imagefolderpath	画像フォルダ,     ");


            //20200910195330 furukawa st ////////////////////////
            //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応                       
            sb.AppendLine(" dbuser		    DBユーザ,        ");
            sb.AppendLine(" dbpass			DBパスワード        ");
            //20200910195330 furukawa ed ////////////////////////


            sb.AppendLine(" from                         ");
            sb.AppendLine(" insurer                      ");

            sb.AppendLine(" order by insurerid           ");

            cmd.CommandText = sb.ToString();



            da.SelectCommand = cmd;            
            da.Fill(dtInsurer);
            dgv.DataSource = dtInsurer;
            dgv.Columns["保険者用ID"].Width = 50;
            dgv.Columns["保険者名"].Width = 150;
            dgv.Columns["DB名"].Width = 150;
            dgv.Columns["ｽｷｬﾝｵｰﾀﾞｰ"].Width = 50;
            dgv.Columns["メホール上取扱"].Width = 50;
            dgv.Columns["保険者番号"].Width = 80;

            //20200423163840 furukawa st ////////////////////////
            //日本語名間違い修正
            
            dgv.Columns["保険者正式名称"].Width = 150;
            //dgv.Columns["データ格納先"].Width = 150;
            //20200423163840 furukawa ed ////////////////////////


            dgv.Columns["保険者タイプ"].Width = 50;
            dgv.Columns["リスト表示順"].Width = 50;
            dgv.Columns["データ出力先"].Width = 300;
            dgv.Columns["ベリファイ有無"].Width = 50;
            dgv.Columns["DBサーバ"].Width = 100;
            dgv.Columns["DBポート"].Width = 50;
            dgv.Columns["画像フォルダ"].Width = 150;

            //20200910195401 furukawa st ////////////////////////
            //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応
            
            dgv.Columns["DBユーザ"].Width = 100;
            dgv.Columns["DBパスワード"].Width = 100;
            //20200910195401 furukawa ed ////////////////////////


            DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            bc.Text = "削除";
            bc.Width = 60;
            bc.Name = "削除";
            bc.UseColumnTextForButtonValue = true;

            if (!dgv.Columns.Contains("削除")) dgv.Columns.Insert(0, bc);

            cmbDup.Items.Clear();
            foreach(DataGridViewColumn c in dgv.Columns)
            {
                cmbDup.Items.Add(c.Name);
            }



            //20210225135855 furukawa st ////////////////////////
            //DB周り、削除は開発者のみ            
            dgv.Columns["DB名"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["DBサーバ"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["DBポート"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["画像フォルダ"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["DBユーザ"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["DBパスワード"].ReadOnly = !User.CurrentUser.Developer;
            dgv.Columns["削除"].ReadOnly = !User.CurrentUser.Developer;
            //20210225135855 furukawa ed ////////////////////////

            if (dgvCurrentRowIndex > dgv.Rows.Count - 1) { dgv.FirstDisplayedScrollingRowIndex = dgv.Rows.Count - 1; }
            else { dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex; }
            dgv.Columns[2].Frozen = true;//
            
        }

        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            UpdateInsurerTable();

            //20210128102324 furukawa st ////////////////////////
            //保険者変更を適用させるため再起動
            
            CommonTool.Restart_Mejor();
            //20210128102324 furukawa ed ////////////////////////
        }


        private void UpdateInsurerTable()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            //20211222161755 furukawa st ////////////////////////
            //db名重複チェック
            
            if (duplicateCheck("db名"))
            {
                dgv.FirstDisplayedScrollingRowIndex = dgv.Rows.Count-1;
                MessageBox.Show("DB名に重複があります。メホールが起動できなくなりますので違うDB名を付けてください"
                    ,Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation); ;
                return;
            }
            //20211222161755 furukawa ed ////////////////////////


            foreach (DataRow dr in dtInsurer.Rows)
            {

                if (dr.RowState == DataRowState.Added)
                {
                    col = 0;

                    //20210215101231 furukawa st ////////////////////////
                    //新保険者追加時medivery_settingにも追加
                    
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($" insert into medivery_setting values ({dr[0]},false,'{strInitSetting}',{User.CurrentUser.UserID},'{DateTime.Now.ToString()}');");
                    lstsql.Add(sb.ToString());
                    //20210215101231 furukawa ed ////////////////////////


                    //20201222113809 furukawa st ////////////////////////
                    //insurer_optionにも登録

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($" insert into insurer_option values ('{dr[0]}',1,1,1);");
                    lstsql.Add(sb.ToString());
                    //20201222113809 furukawa ed ////////////////////////


                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into insurer values  (");



                    //20200910195430 furukawa st ////////////////////////
                    //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応
                    
                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	'{11}',	'{12}',	'{13}' ,'{14}' ,'{15}' ",
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString());



                                //sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	'{11}',	'{12}',	'{13}' ",
                                //dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                                //dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                                //dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString());

                    //20200910195430 furukawa ed ////////////////////////


                    sb.AppendLine(");");

                    lstsql.Add(sb.ToString());
                }
                else if (dr.RowState == DataRowState.Modified)
                {
                    col = 1;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update insurer set ");

                    sb.AppendFormat("	insurername	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbname		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	scanorder	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	enabled		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	insnumber	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	formalname	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	insurertype	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	viewindex	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	outputpath	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	needverify	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbserver	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbport		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	imagefolderpath		='{0}', ", dr[col++].ToString());

                    //20200910195503 furukawa st ////////////////////////
                    //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応
                    
                    sb.AppendFormat("	dbuser	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbpass	        ='{0}' ", dr[col++].ToString());
                    //20200910195503 furukawa ed ////////////////////////

                    sb.AppendFormat("	where insurerid		='{0}';", dr[0].ToString());

                    lstsql.Add(sb.ToString());
                }


            }

            if (lstsql.Count == 0) return;

            //20210120180614 furukawa st ////////////////////////
            //DB存在チェックし、無かったら作成するバッチを走らせる                       
            chkDBExists();
            //20210120180614 furukawa ed ////////////////////////



            //cmd.CommandText = sb.ToString();
            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {

                foreach (string s in lstsql)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新した");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtInsurer.Clear();

                cn.Close();
                disp();
            }


        }


        /// <summary>
        /// dbが実際に無かったら作成する
        /// </summary>
        private void chkDBExists()
        {
            List<string> lstdbname = new List<string>();
            DataTable dtDB = new DataTable();            
            sb.Clear();
            sb.AppendLine("select datname from pg_database");
            cmd.CommandText = sb.ToString();

            Npgsql.NpgsqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lstdbname.Add(dr["datname"].ToString());
            }
            //20220221173633 furukawa st ////////////////////////
            //datareaderクローズ忘れ            
            dr.Close();
            //20220221173633 furukawa ed ////////////////////////


            foreach (DataGridViewRow r in dgv.Rows)
            {                                
                //DB名とenabled=trueの場合に調査
                if (!lstdbname.Contains(r.Cells[3].Value) && r.Cells[5].Value.ToString()=="True")
                {
                    if (MessageBox.Show($"DB {r.Cells[2].Value} が存在しません。作成しますか？",
                        "mejor",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //作成はバッチから実行する
                        System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                        string strarg = $" {r.Cells[12].Value} {r.Cells[13].Value} {r.Cells[3].Value} ";
                        psi.Arguments = strarg;

                        psi.FileName = Application.StartupPath + "\\CreateDB\\new_insurer.bat";
                        psi.UseShellExecute = true;

                        System.Diagnostics.Process p = new System.Diagnostics.Process();
                        p.StartInfo = psi;
                        

                        p.Start();
                        p.WaitForExit();

                        //20211118135506 furukawa st ////////////////////////
                        //ocrのinsurerテーブルに直接登録                       
                        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
                        Npgsql.NpgsqlCommand cmd2 = new Npgsql.NpgsqlCommand();
                        try
                        {
                            cn.ConnectionString = "server=192.168.111.2;port=5440;User id=postgres;password=pass;database=jyusei;";
                            cn.Open();

                            cmd2.Connection = cn;
                            cmd2.CommandText = $"insert into insurer (insurerid,insurername, dbname, scanorder, enabled, insnumber,   " +
                                $"  formalname, insurertype, viewindex, outputpath, needverify, " +
                                $"	dbserver, dbport, imagefolderpath, dbuser, dbpass) values ( " +
                                $"{r.Cells[1].Value},'{r.Cells[2].Value}','{r.Cells[3].Value}','{r.Cells[4].Value}','{r.Cells[5].Value}','{r.Cells[6].Value}'," +
                                $"'{r.Cells[7].Value}','{r.Cells[8].Value}','{r.Cells[9].Value}','{r.Cells[10].Value}','{r.Cells[11].Value}', " +
                                //20220421132807 furukawa st ////////////////////////
                                //OCRdb専用パラメータ
                                
                                $"'localhost','5440','\\\\192.168.110.13\\scan','postgres','pass')";
                            //$"'{r.Cells[12].Value}','{r.Cells[13].Value}','{r.Cells[14].Value}','{r.Cells[15].Value}','{r.Cells[16].Value}')";
                            //20220421132807 furukawa ed ////////////////////////

                            cmd2.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name+"\r\n"+ex.Message);

                        }
                        finally
                        {
                            cmd2.Dispose();
                            cn.Close();
                        }
                        //20211118135506 furukawa ed ////////////////////////
                    }
                }
            }

            
        }



        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            //20210713111026 furukawa st ////////////////////////
            //海外療養費を統合するまで999を最大とする
            
            sb.AppendLine("select max(insurerid) id from insurer where insurerid<1000");
            //sb.AppendLine("select max(insurerid) id from insurer ");
            //20210713111026 furukawa ed ////////////////////////


            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

        /// <summary>
        /// 複製ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btncopy_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index < 0) return;

            DataGridViewRow dgvr = new DataGridViewRow();

            dgvr = dgv.Rows[dgv.CurrentRow.Index];
            DataRow dr;

            dr = dtInsurer.NewRow();
            
            for(int r=1;r<dgvr.Cells.Count;r++)
            {
                if (r == 1)
                {
                    //最大のID取得
                    dr[r-1] = getMaxID();
                }
                else
                {
                    dr[r-1] = dgvr.Cells[r].Value.ToString();
                }
            }

            dtInsurer.Rows.Add(dr);
            UpdateInsurerTable();
        }


        private void deleteDataBase(DataGridViewRow r)
        {
            if (MessageBox.Show($"DB {r.Cells[2].Value} をDBサーバから削除しますか？",
                      "mejor", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                string strarg = $" {r.Cells[12].Value} {r.Cells[13].Value} {r.Cells[3].Value} {r.Cells[1].Value}";
                psi.Arguments = strarg;

                psi.FileName = Application.StartupPath + "\\CreateDB\\del_insurer.bat";
                psi.UseShellExecute = true;

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = psi;


                p.Start();
                p.WaitForExit();
            }
        }

        /// <summary>
        /// 削除ボタン列
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 0) return;

            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv.CurrentRow.Index;

            string id = dgv.Rows[dgv.CurrentRow.Index].Cells[1].Value.ToString();
            

            if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
                Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {


                //20210713112319 furukawa st ////////////////////////
                //dbごと削除に変更
                
                //sb.Remove(0, sb.ToString().Length);
                //sb.AppendLine(" delete from insurer where insurerid=");
                //sb.AppendFormat("'{0}'", id);               

                //cmd.CommandText = sb.ToString();
                try
                {
                    //cmd.ExecuteNonQuery();

                    deleteDataBase(dgv.CurrentRow);

                    //20210713112319 furukawa ed ////////////////////////




                    MessageBox.Show("削除した");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    dtInsurer.Clear();

                    cn.Close();
                    disp();

                    //20210204165439 furukawa st ////////////////////////
                    //削除後再起動して保険者設定を再ロード
                    
                    CommonTool.Restart_Mejor();

                    //20210204165439 furukawa ed ////////////////////////
                }
            }
        }

        private void SettingForm_Insurer_Shown(object sender, EventArgs e)
        {
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }



        //20211222160126 furukawa st ////////////////////////
        //重複チェックを複製時にできるように関数化
        
        /// <summary>
        /// フィールドの重複値チェック
        /// </summary>
        /// <param name="fld">フィールド名</param>
        /// <returns>重複有り=true</returns>
        private bool duplicateCheck(string fld="")
        {
            cmbDup.Text = fld;
            if (fld == string.Empty) return false;            

            //重複フラグ
            bool flgDuplicate = false;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach (DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach (DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString())
                    {

                        cnt++;
                        if (cnt > 1)
                        {

                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                            flgDuplicate = true;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }

                }
              
            }
            if (flgDuplicate) return true;
            else return false;
        }
        //20211222160126 furukawa ed ////////////////////////



        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {

            //20211222160242 furukawa st ////////////////////////
            //重複チェックを複製時にできるように関数化
            
            duplicateCheck(cmbDup.Text);

            #region old
            //string fld = "ｽｷｬﾝｵｰﾀﾞｰ";//デフォルトだけ決めとく
            //fld = cmbDup.Text;

            //dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            //foreach(DataGridViewRow r in dgv.Rows)
            //{
            //    var tmp = r.Cells[fld].Value.ToString();
            //    int cnt = 0;

            //    foreach(DataGridViewRow r2 in dgv.Rows)
            //    {
            //        if (tmp == r2.Cells[fld].Value.ToString()){

            //            cnt++;
            //            if (cnt > 1)
            //            {

            //                r.DefaultCellStyle.BackColor = Color.Coral;
            //                r2.DefaultCellStyle.BackColor = Color.Coral;
            //            }
            //            else
            //            {
            //                r.DefaultCellStyle = null;
            //                dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
            //            }

            //        }
            //        else
            //        {
            //            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
            //        }

            //    }

            //}

            #endregion

            //20211222160242 furukawa ed ////////////////////////
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        /// <summary>
        /// 20220304171943 furukawa 外部業者向け新保険者DB作成バッチ作成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExternalBatch_Click(object sender, EventArgs e)
        {
            //本社のDBを外部業者で複製できるようにバッチを作る

            int intInsurerID = 0;
            string strDBName = string.Empty;

            System.IO.StreamWriter sw = null;

            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            odd.ShowDialog();
            string strOutputFolder = $"{odd.Name}\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
            if (!System.IO.Directory.Exists(strOutputFolder)) System.IO.Directory.CreateDirectory(strOutputFolder);
            if (strOutputFolder == string.Empty) return;

            
            

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            string strDB = DB.GetMainDBName();
            
            try
            {
                //jyuseiを操作する
                DB.SetMainDBName("jyusei");

                //現在選択している行のInsurerIDとDB名を取得
                if (!int.TryParse(dgv.CurrentRow.Cells[1].Value.ToString(), out intInsurerID)) return;
                strDBName = dgv.CurrentRow.Cells[3].Value.ToString();


                //20220714122531 furukawa st ////////////////////////
                //postgresのフォルダ存在確認                
                string strPostgresDir = "c:\\program files\\postgresql\\11\\bin";
                //cになければdを確認
                if (!System.IO.Directory.Exists(strPostgresDir)) strPostgresDir = "d:\\program files\\postgresql\\11\\bin";
                //dになければ終了
                if (!System.IO.Directory.Exists(strPostgresDir))
                {
                    MessageBox.Show("PostgreSQLのフォルダが見つかりません。処理を続行できません",
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                
                string strPGDUMP = $"{strPostgresDir}\\pg_dump.exe";
                //  string strPGDUMP = "c:\\program files\\postgresql\\11\\bin\\pg_dump.exe";

                //20220714122531 furukawa ed ////////////////////////


                string strDBDumpFile = $"{strOutputFolder}\\{strDBName}.dump";

                wf.LogPrint("DBのdump作成中...");

                //1.本社のdbをdump（スキーマだけ）               
                sw = new System.IO.StreamWriter($"{strOutputFolder}\\DB.bat");

                StringBuilder sb = new StringBuilder();
                sb.Clear();
                sb.AppendLine($"@echo off");
                sb.AppendLine($"chcp 65001");
                sb.AppendLine($"echo output db dump...");

                //20220714122713 furukawa st ////////////////////////
                //存在するPostgresフォルダを使う                
                sb.AppendLine($"cd /d {strPostgresDir}");
                //  sb.AppendLine($"cd /d c:\\program files\\postgresql\\11\\bin");
                //20220714122713 furukawa ed ////////////////////////


                sb.AppendLine($"pg_dump -C -h {DB.getDBHost()} -p 5432 -U postgres " +
                    $"-d {strDBName} --schema-only >{strDBDumpFile}");
                sw.WriteLine(sb.ToString());
                sw.Close();
                
                if (!System.IO.File.Exists(strPGDUMP))
                {
                    MessageBox.Show("pg_dump.exeが見つかりません。処理を続行できません",
                        Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation); 
                    return;
                }

                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                //進行状況を見せる
                si.UseShellExecute = false;
                si.CreateNoWindow =true;
                
                System.Diagnostics.Process.Start($"{strOutputFolder}\\DB.bat").WaitForExit();


                wf.LogPrint("insurer.csv作成中...");
                //2.jyusei.insurerの該当DBレコードをcsvエクスポート
                sb.Clear();


                //20220708155657 furukawa st ////////////////////////
                //ダブルコーテーションがあると取り込みエラーになるので外す                
                sb.AppendLine($"copy (select * from insurer where insurerid={intInsurerID}) TO STDOUT with csv ");
                //      sb.AppendLine($"copy (select * from insurer where insurerid={intInsurerID}) TO STDOUT with csv force quote *");  
                //20220708155657 furukawa ed ////////////////////////

                //20220708161250 furukawa st ////////////////////////
                //文字コードをsjisにしないとutf8変換エラーが出る                
                if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\insurer.csv", sb.ToString(),"shift-jis"))
                //      if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\insurer.csv", sb.ToString()))
                //20220708161250 furukawa ed ////////////////////////
                {
                    MessageBox.Show("insurer.csv出力に失敗しました");
                    return;
                }


                wf.LogPrint("insurer_option.csv作成中...");
                //3.jyusei.insurer_optionの該当DBレコードをcsvエクスポート
                sb.Clear();

                //20220708155754 furukawa st ////////////////////////
                //ダブルコーテーションがあると取り込みエラーになるので外す                
                sb.AppendLine($"copy (select * from insurer_option where insurerid={intInsurerID}) TO STDOUT with csv ");
                //      sb.AppendLine($"copy (select * from insurer_option where insurerid={intInsurerID}) TO STDOUT with csv force quote *");
                //20220708155754 furukawa ed ////////////////////////

                //20220708161409 furukawa st ////////////////////////
                //文字コードをsjisにしないとutf8変換エラーが出る                
                if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\insurer_option.csv", sb.ToString(),"shift-jis"))
                //      if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\insurer_option.csv", sb.ToString()))
                //20220708161409 furukawa ed ////////////////////////
                {
                    MessageBox.Show("insurer_option.csv出力に失敗しました");
                    return;
                }

                wf.LogPrint("medivery_setting.csv作成中...");
                //4.jyusei.medivery_settingの該当DBレコードをcsvエクスポート
                sb.Clear();

                //20220712085937 furukawa st ////////////////////////
                //日付null対策                
                sb.AppendLine($"copy (select * from medivery_setting where insurerid={intInsurerID}) TO STDOUT with csv null as '0001-01-01' ");
                //      20220708155824 furukawa st ////////////////////////
                //      ダブルコーテーションがあると取り込みエラーになるので外す                
                //      sb.AppendLine($"copy (select * from medivery_setting where insurerid={intInsurerID}) TO STDOUT with csv ");
                //            sb.AppendLine($"copy (select * from medivery_setting where insurerid={intInsurerID}) TO STDOUT with csv force quote *");
                //      20220708155824 furukawa ed ////////////////////////

                //20220712085937 furukawa ed ////////////////////////

                //20220708161459 furukawa st ////////////////////////
                //文字コードをsjisにしないとutf8変換エラーが出る                
                if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\medivery_setting.csv", sb.ToString(),"shift-jis"))
                //      if (!CommonTool.CsvDirectExportFromTable($"{strOutputFolder}\\medivery_setting.csv", sb.ToString()))
                //20220708161459 furukawa ed ////////////////////////
                {
                    MessageBox.Show("medivery_setting.csv出力に失敗しました");
                    return;
                }

                //20220711152405 furukawa st ////////////////////////
                //改行コードを削除するためcsvを再編集しないと、copy from でエラーが出る                
                System.IO.StreamReader tmpsr = new System.IO.StreamReader($"{strOutputFolder}\\medivery_setting.csv", System.Text.Encoding.GetEncoding($"shift-jis"));
                string tmp=tmpsr.ReadToEnd().Replace("\r\n", "");
                tmpsr.Close();
                System.IO.StreamWriter tmpsw = new System.IO.StreamWriter($"{strOutputFolder}\\medivery_setting.csv",false, System.Text.Encoding.GetEncoding($"shift-jis"));
                tmpsw.Write(tmp);
                tmpsw.Close();
                //20220711152405 furukawa ed ////////////////////////



                wf.LogPrint("自己解凍書庫作成中...");
                var dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);//実行ファイルのフォルダ

                //7zで自己解凍書庫
                string str7z = $"{dir}\\Archive\\7z.exe";
                string strArchiveName = $"{strOutputFolder}\\DB.exe";

                si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = str7z;
                si.Arguments = $"a -sfx {strArchiveName} {strOutputFolder}\\*.* -sdel";
                si.UseShellExecute = false;
                si.CreateNoWindow = true;
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                //p = new System.Diagnostics.Process();
                p.StartInfo = si;
                p.Start();
                p.WaitForExit();
                p.Close();

                sw = new System.IO.StreamWriter($"{strOutputFolder}\\外部業者向けDB作成_{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}.bat");

                //5.一つのバッチファイルに纏める

                wf.LogPrint("バッチファイル作成中...");

                //20220714122900 furukawa st ////////////////////////
                //存在するPostgresフォルダを使う                
                string strPsql = $"{strPostgresDir}\\psql.exe";
                //  string strPsql= "c:\\program files\\postgresql\\11\\bin\\psql.exe";
                //20220714122900 furukawa ed ////////////////////////


                sb.Clear();
                
                sb.AppendLine($"@echo off");

                //20220708155924 furukawa st ////////////////////////
                //不要
                //sb.AppendLine($"chcp 65001");
                //20220708155924 furukawa ed ////////////////////////


                //20220708155959 furukawa st ////////////////////////
                //相対パスにしないと相手先で動かない                
                sb.AppendLine($"db.exe");
                //  sb.AppendLine($"{strArchiveName}");
                //20220708155959 furukawa ed ////////////////////////



                //20220708155852 furukawa st ////////////////////////
                //Delimiterつけないと１列目に全部入れようとする
                sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy insurer from insurer.csv delimiter \',\' \";");
                //  sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy insurer from insurer.csv\";");
                //20220708155852 furukawa ed ////////////////////////

                //20220708175542 furukawa st ////////////////////////
                //DBホストは11.2に固定しないとlocalhostだと自分PCだけしか見えない
                
                sb.AppendLine($" \"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei " +
                    $"--command=\"UPDATE insurer SET dbserver = '192.168.11.2', dbport = 5432, " +
                    $"imagefolderpath = '\\\\192.168.11.2\\scan', dbuser = 'postgres', dbpass = 'pass' " +
                    $"where insurerid={intInsurerID};");

                //      20220304211231 furukawa st ////////////////////////
                //      外部業者向けDB設定               
                //      sb.AppendLine($" \"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei " +
                //          $"--command=\"UPDATE insurer SET dbserver = 'localhost', dbport = 5432, " +
                //          $"imagefolderpath = '\\\\192.168.11.2\\scan', dbuser = 'postgres', dbpass = 'pass' " +
                //          $"where insurerid={intInsurerID};");
                //      20220304211231 furukawa ed ////////////////////////
                //20220708175542 furukawa ed ////////////////////////

                //20220708160038 furukawa st ////////////////////////
                //Delimiterつけないと１列目に全部入れようとする

                sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy insurer_option from insurer_option.csv delimiter \',\' \";");
                sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy medivery_setting from medivery_setting.csv delimiter \',\' \";");
                //sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy insurer_option from insurer_option.csv\";");
                //sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d jyusei --command=\"\\copy medivery_setting from medivery_setting.csv\";");
                //20220708160038 furukawa ed ////////////////////////


                //20220712085851 furukawa st ////////////////////////
                //不要なセミコロン削除                
                sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres --command=\" create database {strDBName}\"");
                //sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres --command=\" create database {strDBName}\";");
                //20220712085851 furukawa ed ////////////////////////



                //20220712091200 furukawa st ////////////////////////
                //不要なセミコロン削除
                
                sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d {strDBName} <{strDBName}.dump");

                //      20220708162340 furukawa st ////////////////////////
                //      相対パスにしないと相手先で動かない                

                //      sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d {strDBName} <{strDBName}.dump;");
                //            sb.AppendLine($"\"{strPsql}\" -h localhost -p 5432 -U postgres -d {strDBName} <{strDBDumpFile};");
                //      20220708162340 furukawa ed ////////////////////////
                //20220712091200 furukawa ed ////////////////////////

                sw.WriteLine(sb.ToString());

                //不要なバッチ削除
                System.IO.File.Delete($"{strOutputFolder}\\DB.bat");
                //フォルダ開く
                System.Diagnostics.Process.Start($"{strOutputFolder}");
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                sw.Close();
                wf.Dispose();
                DB.SetMainDBName(strDB);//現在のdbを元に戻す
            }


        }
    }
}
