﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;

namespace Mejor
{
    public enum REASON
    {
        署名不一致=1,
        転帰欄未記入=2,
        再検料=3,
        実日数20日以上=21,
        同世帯4人以上=22
    }

    public enum CHECK_FLAG
    {
        NULL = 0,
        AUTO = 1,
        NO = 2,
        YES = 3
    }

    class AutoCheck
    {
        List<App> appList;
        int year;
        int month;

        /// <summary>
        /// オートチェックを作成します
        /// </summary>
        /// <param name="list">同一世帯3か月分のApp</param>
        /// <param name="checkYear"></param>
        /// <param name="checkMonth"></param>
        public AutoCheck(List<App> list, int checkYear, int checkMonth)
        {
            appList = list;
            year = checkYear;
            month = checkMonth;
        }

        public void Check()
        {
            dateCHeck();
            family4Check();


        }

        private void checkErrorWrite(App app, REASON reason, string note)
        {
            using (var cmd = DB.Main.CreateCmd("INSERT INTO check "
                + "(aid, reason, flag, date, userid, note) "
                + "VALUES(:aid, :reason, :flag, :date, :userid, :note);"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = app.Aid;
                cmd.Parameters.Add("reason", NpgsqlDbType.Integer).Value = (int)reason;
                cmd.Parameters.Add("flag", NpgsqlDbType.Integer).Value = (int)CHECK_FLAG.AUTO;
                cmd.Parameters.Add("date", NpgsqlDbType.Integer).Value = DateTime.Today;
                cmd.Parameters.Add("userid", NpgsqlDbType.Integer).Value = 0;
                cmd.Parameters.Add("note", NpgsqlDbType.Text).Value = note;
            }
        }


        private void dateCHeck()
        {
            foreach (var item in appList)
            {
                if (item.ChargeYear != year) continue;
                if (item.ChargeMonth != month) continue;

                if (item.CountedDays < 20) continue;
                checkErrorWrite(item, REASON.実日数20日以上, "");
            }
        }

        /// <summary>
        /// 同一月世帯ごと4件以上
        /// </summary>
        private void family4Check()
        {
            var dic = new Dictionary<string, int>();

            foreach (var item in appList)
            {
                if (item.ChargeYear != year) continue;
                if (item.ChargeMonth != month) continue;

                if (!dic.ContainsKey(item.ClinicNum)) dic.Add(item.ClinicNum, 0);
                dic[item.ClinicNum]++;
            }

            foreach (var item in dic)
            {
                if (item.Value < 4) continue;

                var res = appList.FindAll(app => 
                    app.ClinicNum == item.Key && app.ChargeYear != year && app.ChargeYear != year);

                foreach (var app in res)
                {
                    checkErrorWrite(app, REASON.同世帯4人以上, "");
                }
            }
        }
    }
}
