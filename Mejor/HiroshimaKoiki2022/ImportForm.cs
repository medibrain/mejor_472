﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HiroshimaKoiki2022
{
    public partial class ImportForm : Form
    {
        public ImportForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            APP_TYPE apptype;

            if (radioButtonJyu.Checked) apptype = APP_TYPE.柔整;
            else if (radioButtonShin.Checked) apptype = APP_TYPE.鍼灸;
            else if (radioButtonAnma.Checked) apptype = APP_TYPE.あんま;
            else
            {
                MessageBox.Show("申請書の種類を指定して下さい");
                return;
            }

            using (var f = new OpenFileDialog())
            {
                f.Filter = "CSVファイル|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                RefRece.Import(f.FileName, apptype);
            }

            this.Close();
        }
    }
}
