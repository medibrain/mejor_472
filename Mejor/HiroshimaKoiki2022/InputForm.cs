﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.HiroshimaKoiki2022
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsRefRece;
        private InputMode inputMode;
        protected override Control inputPanel => panelRight;
        int cym;
        
        private bool firstTime = false;

        //過去に入力したapp
        List<App> lstPastData = new List<App>();

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 60);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1200);
        Point posTotal = new Point(800, 1600);
        Point posBui = new Point(80, 750);

        #region コンストラクタ

        public InputForm(InputMode mode, ScanGroup sGroup,bool firsttime, int aid = 0)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode == InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");


            //柔整は1回入力
            if (sGroup.AppType == APP_TYPE.柔整)
            {
                if (!firsttime)
                {
                    MessageBox.Show("柔整はベリファイ無しです",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    return;
                }
            }

            this.firstTime = firsttime;

            panelInfo.Visible = true;
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;

            this.scanGroup = sGroup;
            var list = new List<App>();
            list = App.GetAppsGID(scanGroup.GroupID);

            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));

            //Appリスト
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            

            //各グリッド表示調整
            initializeGridView();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            
            var app = (App)bsApp.Current;
            cym = app.CYM;
            LoadPastData();//過去1ヶ月データロード

            if (app != null) setApp(app);


        }

        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            initializeGridView();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }


        #endregion


        /// <summary>
        /// 過去1ヶ月前入力データロード
        /// </summary>
        private void LoadPastData()
        {
        
            int Past1Month = DateTimeEx.Int6YmAddMonth(cym, -1);
            lstPastData = App.GetApps(Past1Month);
            lstPastData.Sort((x, y) => x.HihoNum.CompareTo(y.HihoNum));

        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;
            bsApp.ResetBindings(false);

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            focusBack(false);
        }


        private void initializeGridView()
        {
            foreach (DataGridViewColumn c in dataGridViewPlist.Columns) c.Visible = false;
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            //広域データ欄
            bsRefRece = new BindingSource();
            bsRefRece.DataSource = new List<RefRece>();
            dataGridRefRece.DataSource = bsRefRece;

            foreach (DataGridViewColumn c in dataGridRefRece.Columns) c.Visible = false;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Width = 50;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Width = 50;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].HeaderText = "年月";
            dataGridRefRece.Columns[nameof(RefRece.Num)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Num)].Width = 70;
            dataGridRefRece.Columns[nameof(RefRece.Num)].HeaderText = "被保番";
            dataGridRefRece.Columns[nameof(RefRece.Name)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Name)].Width = 95;
            dataGridRefRece.Columns[nameof(RefRece.Name)].HeaderText = "氏名";
            dataGridRefRece.Columns[nameof(RefRece.DrName)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.DrName)].Width = 100;
            dataGridRefRece.Columns[nameof(RefRece.DrName)].HeaderText = "代表者";
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Width = 80;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].HeaderText = "施術所番号";
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].Width = 100;
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].HeaderText = "施術所";
            dataGridRefRece.Columns[nameof(RefRece.Total)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Total)].Width = 50;
            dataGridRefRece.Columns[nameof(RefRece.Total)].HeaderText = "合計";
            dataGridRefRece.Columns[nameof(RefRece.AID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.AID)].Width = 60;

            //pgupで選択行が変わるのを防ぐ
            dataGridRefRece.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            if (app == null)
            {
                clearApp();
                return;
            }

            setApp(app);
            if (app.StatusFlagCheck(StatusFlag.自動マッチ済)) verifyBoxNewCont.Focus();
            else verifyBoxY.Focus();
        }
        
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }

            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            //選択中参照レセとAIDが違う場合は変更がなくてもUpdate
            var app = (App)bsApp.Current;
            var rr = (RefRece)bsRefRece.Current;
            if (app != null && rr != null && rr.AID != app.Aid) setDataChanged(true);

            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            if (scan.AppType == APP_TYPE.柔整)
            {
                var app = (App)bsApp.Current;
                selectRefRece(app);
            }
        }

        private void selectRefRece(App app)
        {
            bsRefRece.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefRece.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時
                int cym = scan.CYM;
                int mym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;
                
                l = RefRece.SerchByInput(cym, mym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefRece.DataSource = l;
            bsRefRece.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridRefRece.RowCount; i++)
                {
                    var rrid = (int)dataGridRefRece[nameof(RefRece.RrID), i].Value;
                    if (app.RrID == rrid)
                    {
                        //既に一意の広域データとマッチング済みの場合。
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridRefRece.CurrentCell = dataGridRefRece[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridRefRece.CurrentCell = null;
            }
        }


        /// <summary>
        /// あはき専用項目
        /// </summary>
        /// <param name="app"></param>
        private bool checkAppAHK(App app)
        {


            //保険者番号
            string strInsNum =  verifyBoxInsNum.Text;
            setStatus(verifyBoxInsNum, strInsNum == string.Empty || (strInsNum != string.Empty && strInsNum.Length != 4));

            //被保険者名
            string strHihoName = verifyBoxHihoname.Text.Trim();
            setStatus(verifyBoxHihoname, strHihoName == string.Empty);

            //被保険者名カナ
            string strHihoNameKana = verifyBoxHihonameKana.Text.Trim();
            setStatus(verifyBoxHihonameKana, strHihoNameKana == string.Empty);

            //性別
            int gender = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, !new string[] { "1","2"}.Contains(gender.ToString()));


            //生年月日
            DateTime dtPBirthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);
            setStatus(verifyBoxBirthY, dtPBirthday == DateTime.MinValue);
            setStatus(verifyBoxBirthM, dtPBirthday == DateTime.MinValue);
            setStatus(verifyBoxBirthD, dtPBirthday == DateTime.MinValue);
            setStatus(verifyBoxBirthE, dtPBirthday == DateTime.MinValue);

            //給付割合                
            setStatus(verifyBoxRatio, !int.TryParse(verifyBoxRatio.Text, out int intRatio) || intRatio<7);

            //公費負担者番号 あれば8桁
            string strPublicExpense = verifyBoxPublicExpanse.Text;
            setStatus(verifyBoxPublicExpanse, strPublicExpense!=string.Empty && strPublicExpense.Length!=8);

            //公費受給者番号　あれば7桁
            string strPublicReci = verifyBoxReci.Text;
            setStatus(verifyBoxReci, strPublicReci != string.Empty && strPublicReci.Length != 7);

            //初検日
            DateTime dtFirst1 = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            setStatus(verifyBoxF1FirstY, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstM, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstD, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstE, dtFirst1 == DateTime.MinValue);

            //開始日
            int era = DateTimeEx.GetEraNumber(new DateTime(DateTimeEx.GetAdYearFromHs(verifyBoxY.GetInt100Value() + verifyBoxM.GetIntValue()), verifyBoxM.GetIntValue(), 1));
            DateTime dtStart1 = verifyBoxF1Start.Text == string.Empty ? DateTime.MinValue : dateCheck(era, verifyBoxY, verifyBoxM, verifyBoxF1Start);
            
            //入力チームより空欄入力可能とする
            //setStatus(verifyBoxF1Start, dtStart1==DateTime.MinValue);

            //bool flgStart1 = int.TryParse(verifyBoxF1Start.Text, out int intStart1);
            //setStatus(verifyBoxF1Start, !flgStart1 || !(intStart1 >= 1 && intStart1 <= 31));

            //終了日
            DateTime dtFinish1 = verifyBoxF1Finish.Text == string.Empty ? DateTime.MinValue : dateCheck(era, verifyBoxY, verifyBoxM, verifyBoxF1Finish);

            //入力チームより空欄入力可能とする
            //setStatus(verifyBoxF1Finish, dtFinish1 == DateTime.MinValue);


            //bool flgFinish1 = int.TryParse(verifyBoxF1Finish.Text, out int intFinish1);
            //setStatus(verifyBoxF1Finish, !flgFinish1 || !(intFinish1 >= 1 && intFinish1 <= 31));

            //交付あり
            bool flgKofu = vcSejutu.Checked;

            //前回交付年月
            string PastKofu = DateTimeEx.GetAdYearMonthFromJyymm(vbSejutuG.GetIntValue()*10000+ vbSejutuY.GetInt100Value()+vbSejutuM.GetIntValue()).ToString();


            //施術所番号 無い場合があるのでチェック外す
            string strClinicNum = verifyBoxClinicNum.Text.Trim();
            //20220509161936 furukawa st ////////////////////////
            //施術所番号は10桁のみ許可
            
            setStatus(verifyBoxClinicNum, strClinicNum == string.Empty || (strClinicNum != string.Empty && strClinicNum.Length!=10));
            //20220509161936 furukawa ed ////////////////////////


            //施術所名
            string strClinicName = verifyBoxClinicName.Text;
            setStatus(verifyBoxClinicName, strClinicName == string.Empty);

            //施術師名
            string strDrName = verifyBoxDrName.Text;
            setStatus(verifyBoxDrName, strDrName == string.Empty);

            //請求額
            bool flgChage = int.TryParse(verifyBoxCharge.Text, out int intCharge);
            setStatus(verifyBoxCharge, !flgChage || intCharge <= 0);

            //実日数
            bool flgCountedDays = int.TryParse(verifyBoxCountedDays.Text, out int intCountedDays);
            setStatus(verifyBoxCountedDays, !flgCountedDays || intCountedDays <= 0);

            int total = verifyBoxTotal.GetIntValue();

            //割合チェック
            bool ratioError = !CommonTool.CheckChargeFromRatio(total, intRatio, intCharge);

            if (ratioError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                verifyBoxRatio.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }


            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }



            //保険者番号 3934と合体させる
            app.InsNum = labelInsNum1.Text + strInsNum;

            //被保険者名
            app.HihoName = strHihoName;

            //被保険者名カナ
            app.TaggedDatas.Kana = strHihoNameKana;

            //性別
            app.Sex = gender;

            //生年月日            
            app.Birthday = dtPBirthday;

            //給付割合
            app.Ratio = intRatio;

            //公費負担者番号
            //app.PublcExpense = strPublicExpense;
            app.TaggedDatas.KouhiNum = strPublicExpense;

            //公費受給者番号
            app.TaggedDatas.JukyuNum = strPublicReci;

            //初検日
            app.FushoFirstDate1 = dtFirst1;

            //開始日
            app.FushoStartDate1 = dtStart1;// new DateTime(DateTimeEx.GetAdYearFromHs(verifyBoxY.GetInt100Value() + verifyBoxM.GetIntValue()), verifyBoxM.GetIntValue(), intStart1);

            //終了日
            app.FushoFinishDate1 = dtFinish1;// new DateTime(DateTimeEx.GetAdYearFromHs(verifyBoxY.GetInt100Value() + verifyBoxM.GetIntValue()), verifyBoxM.GetIntValue(), intFinish1);

            //施術所番号
            app.ClinicNum = strClinicNum;

            //施術所名
            app.ClinicName = strClinicName;

            //施術師名
            app.DrName = strDrName;

            //請求額
            app.Charge = intCharge;

            //実日数
            app.CountedDays = intCountedDays;

            //申請書タイプ
            app.AppType = scan.AppType;

            //交付あり
            app.TaggedDatas.flgKofuUmu = flgKofu;

            //前回交付
            app.TaggedDatas.PastSupplyYM = PastKofu;

            return true;
        }


        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            long hnumTemp;
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out hnumTemp));

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //新規・継続の抜けチェック
            int newCont = verifyBoxNewCont.GetIntValue();
            if (scan.AppType == APP_TYPE.柔整)
            {
                setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);
            }

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //往療料
            bool visit = verifyCheckBoxVisit.Checked;

            //加算あり
            bool visitkasan = verifyCheckBoxVisitKasan.Checked;

            //柔整師登録記号番号
            string strDrCode = verifyBoxDrCode.Text.Trim();
            
            //20220509140134 furukawa st ////////////////////////
            //柔整師登録記号番号は10桁のみ許可
            
            setStatus(verifyBoxDrCode, strDrCode == string.Empty || (strDrCode != string.Empty && strDrCode.Length!=10));
            //      setStatus(verifyBoxDrCode, strDrCode == string.Empty);
            //20220509140134 furukawa ed ////////////////////////

            //20221215_1 ito st /////前回支給日は空白を許容する必要があるため、3つとも空白の場合は日付チェックを飛ばす
            if (vbSejutuG.Text != "" && vbSejutuY.Text != "" && vbSejutuM.Text != "") 
            {
                //20221020_1 ito st /////前回支給日チェック
                ////前回支給元号
                int pastgengo = vbSejutuG.GetIntValue();
                setStatus(vbSejutuG, pastgengo < 4 || 5 < pastgengo); //元号は4か5のみ

                //前回支給年
                int pastyear = vbSejutuY.GetIntValue();
                //setStatus(vbSejutuY, pastyear < app.ChargeYear - 7 || app.ChargeYear < pastyear);
                int pastwestyear = DateTimeEx.GetAdYearMonthFromJyymm(vbSejutuG.GetIntValue() * 10000 + vbSejutuY.GetInt100Value() + vbSejutuM.GetIntValue());
                setStatus(vbSejutuY, pastwestyear < cym - 700 || pastwestyear > cym); //とりあえず7年前(cym-700)より前の年月はエラーとする

                //前回支給月
                int pastmonth = vbSejutuM.GetIntValue();
                setStatus(vbSejutuM, pastmonth < 1 || 12 < pastmonth);
                //20221020_1 ito end /////
            }
            //20221215_1 ito ed /////

            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                if(!checkAppAHK(app)) return false;
            }


            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text;
            app.Total = total;
            app.Distance = visit ? 999 : 0;            
            app.VisitAdd = visitkasan ? 999 : 0;

            app.DrNum = strDrCode;

            //新規・継続
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();


            if (scan.AppType == APP_TYPE.柔整)
            {
                //広域データからのデータコピー
                var rr = (RefRece)dataGridRefRece.CurrentRow.DataBoundItem;
                app.PersonName = rr.Name;
                app.ClinicName = rr.ClinicName;
                app.ClinicNum = rr.ClinicNum;
                app.CountedDays = rr.Days;
                app.DrName = rr.DrName;
                app.ClinicAdd = rr.ClinicAdd;
                app.ClinicZip = rr.ClinicZip;
                app.HihoZip = rr.DestZip == "" ? rr.Zip : rr.DestZip;
                app.HihoAdd = rr.DestAdd == "" ? rr.Add : rr.DestAdd;
                app.ComNum = rr.SearchNum;
                app.AppType = rr.AppType;
                app.RrID = rr.RrID;
                app.TaggedDatas.DestName = rr.DestName;
                app.TaggedDatas.Kana = rr.Kana;
            }

            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            switch (verifyBoxY.Text) 
            {
                case "--":
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;
                case "++":
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;
                case "..":
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.AppType = APP_TYPE.長期;
                    break;
                case "**":
                    //エラー
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                    break;

                case "901":
                    //施術同意書
                    resetInputData(app);
                    hasError = false;
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);
                    app.TaggedDatas.DouiDate = dtDoui;


                    break;

                case "902":
                    //施術同意書裏
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case "911":
                    //施術報告書
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case "921":
                    //状態記入書
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;


                default:
                    //申請書の場合
                    if (scan.AppType == APP_TYPE.柔整)
                    {
                        if (dataGridRefRece.CurrentCell == null)
                        {
                            //20220511095043 furukawa st ////////////////////////
                            //メッセージ変更
                            
                            if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                                  Application.ProductName,
                                  MessageBoxButtons.YesNo,
                                  MessageBoxIcon.Exclamation,
                                  MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;


                            //      MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                            //          "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                            //          "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //      return false;
                            //20220511095043 furukawa ed ////////////////////////
                        }

                        var oldRridStr = app.Numbering;
                    }

                    if (!checkApp(app))
                    {
                        focusBack(true);
                        return false;
                    }
                    break;
            }

            //ベリファイチェック あはきのみベリファイ
            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                if (!firstTime && !checkVerify()) return false;
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {

                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; // 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;



                //広域データ更新
                if (scan.AppType == APP_TYPE.柔整)
                {
                    if (app.RrID == 0)
                    {
                        RefRece.AidDelete(app.Aid, tran);
                    }
                    else
                    {
                        var kd = (RefRece)dataGridRefRece.CurrentRow.DataBoundItem;
                        if (kd.AID != app.Aid) RefRece.AidUpdate(kd.RrID, app.Aid, tran);
                    }
                }

                jyuTran.Commit();
                tran.Commit();
            }

            return true;
        }

        /// <summary>
        /// Appを表示します
        /// </summary>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            InitControl();

            //App_Flagのチェック
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }
            else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }


            //提供データは柔整のみ
            if (scan.AppType == APP_TYPE.柔整)
            {
                //広域データ表示
                selectRefRece(app);
            }


            
            changedReset(app);
        }

        private void clearApp()
        {
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            iVerifiableAllClear(panelRight);
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            bsRefRece.Clear();
            bsRefRece.ResetBindings(false);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //メイン画像の表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                var ocr = app.OcrData.Split(',');
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (string.IsNullOrEmpty(appsg.note2))
                    {
                        //OCRデータがあれば、部位のみ挿入
                        if (!string.IsNullOrWhiteSpace(app.OcrData))
                        {
                            verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                            verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                            verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                            verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                            verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                        }

                        //新規/継続
                        if (ocr.Length > 127)
                        {
                            if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                            if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                        }
                    }
                }
                catch
                {
                    //握り潰し問題なし
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            try
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxHnum.Text = app.HihoNum;
                verifyBoxTotal.Text = app.Total.ToString();

                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (string.IsNullOrEmpty(appsg.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }

                    //新規/継続
                    if (ocr.Length > 127)
                    {
                        if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                        if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                    }
                }
            }
            catch
            {
                //握り潰し問題なし
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            switch (app.MediYear) 
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    //続紙                    
                    setValue(verifyBoxY,"--", firstTime, nv);
                    break;
                case (int)APP_SPECIAL_CODE.不要:
                    //その他
                    setValue(verifyBoxY, "++", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.長期:
                    setValue(verifyBoxY, "..", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.エラー:
                    //エラー
                    setValue(verifyBoxY, "**", firstTime, nv);                    
                    break;
                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, "901", firstTime, nv);

                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    break;
                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, "902", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, "911", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, "921", firstTime, nv);
                    
                    break;
                default:

                    if (scan.AppType == APP_TYPE.柔整)
                    {
                        //申請書年月
                        verifyBoxY.Text = app.MediYear.ToString();
                        verifyBoxM.Text = app.MediMonth.ToString();

                        //被保険者番号
                        verifyBoxHnum.Text = app.HihoNum;

                        //合計金額
                        verifyBoxTotal.Text = app.Total.ToString();

                        verifyBoxF1.Text = app.FushoName1;
                        verifyBoxF2.Text = app.FushoName2;
                        verifyBoxF3.Text = app.FushoName3;
                        verifyBoxF4.Text = app.FushoName4;
                        verifyBoxF5.Text = app.FushoName5;

                        //新規/継続
                        verifyBoxNewCont.Text =
                            app.NewContType == NEW_CONT.新規 ? "1" :
                            app.NewContType == NEW_CONT.継続 ? "2" : "";

                        //往療
                        verifyCheckBoxVisit.Checked = app.Distance == 999;

                        //突合情報
                        int oldRrid = 0;
                        int.TryParse(app.Numbering, out oldRrid);


                        //柔整師登録記号番号
                        verifyBoxDrCode.Text = app.DrNum.ToString();
                    }

                    
                    else if(scan.AppType==APP_TYPE.あんま||scan.AppType==APP_TYPE.鍼灸)
                    {

                        //申請書年月
                        setValue(verifyBoxY, app.MediYear, firstTime, nv);
                        setValue(verifyBoxM, app.MediMonth, firstTime, nv);

                        //被保険者番号
                        setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                        

                        //合計金額
                        setValue(verifyBoxTotal, app.Total, firstTime, nv);
                        

                        setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
                        setValue(verifyBoxF2, app.FushoName2, firstTime, nv);
                        setValue(verifyBoxF3, app.FushoName3, firstTime, nv);
                        setValue(verifyBoxF4, app.FushoName4, firstTime, nv);
                        setValue(verifyBoxF5, app.FushoName5, firstTime, nv);
                      

                        //新規/継続
                        setValue(verifyBoxNewCont, (int)app.NewContType, firstTime, nv);
                        

                        //往療
                        //setValue(verifyCheckBoxVisit, app.Distance == 999, firstTime, nv);
                        

                        //突合情報
                        int oldRrid = 0;
                        int.TryParse(app.Numbering, out oldRrid);


                        //柔整師登録記号番号
                        setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);
                        

                        //保険者番号  3934を省いた後ろ4桁表示
                        setValue(verifyBoxInsNum, app.InsNum.Substring(4), firstTime, nv);
                        //setValue(verifyBoxInsNum, app.InsNum, firstTime, nv);

                        //被保険者名                        
                        setValue(verifyBoxHihoname, app.HihoName, firstTime, nv);
                        
                        //被保険者名カナ                        
                        setValue(verifyBoxHihonameKana, app.TaggedDatas.Kana, firstTime, nv);

                        //性別
                        setValue(verifyBoxSex, app.Sex, firstTime, nv);

                        //生年月日
                        setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                        //給付割合
                        setValue(verifyBoxRatio, app.Ratio, firstTime, nv);                        

                        //公費負担者番号
                        //setValue(verifyBoxPublicExpanse, app.PublcExpense, firstTime, nv);
                        setValue(verifyBoxPublicExpanse, app.TaggedDatas.KouhiNum, firstTime, nv);

                        //公費受給者番号
                        setValue(verifyBoxReci, app.TaggedDatas.JukyuNum, firstTime, nv);

                        //初検日
                        setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                        //開始日
                        setValue(verifyBoxF1Start, app.FushoStartDate1==DateTime.MinValue ? string.Empty: app.FushoStartDate1.Day.ToString(), firstTime, nv);
                        
                        //終了日
                        setValue(verifyBoxF1Finish, app.FushoFinishDate1==DateTime.MinValue ? string.Empty: app.FushoFinishDate1.Day.ToString(), firstTime, nv);

                        //施術所番号
                        setValue(verifyBoxClinicNum, app.ClinicNum, firstTime, nv);
                        
                        //施術所名
                        setValue(verifyBoxClinicName, app.ClinicName, firstTime, nv);                        

                        //施術師名
                        setValue(verifyBoxDrName, app.DrName, firstTime, nv);
                        
                        //請求額
                        setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                        
                        //実日数
                        setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                        //往療料
                        setValue(verifyCheckBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);

                        //20220419163146 furukawa st ////////////////////////
                        //往療チェックボックスに関係なく値を入れる
                        
                        //      if (verifyCheckBoxVisit.Checked)
                        //      {
                        //          //加算あり                                                
                        //          setValue(verifyCheckBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);
                        //      }

                        //加算あり                                                
                        setValue(verifyCheckBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);
                        //20220419163146 furukawa ed ////////////////////////


                        //前回交付
                        setValue(vcSejutu, app.TaggedDatas.flgKofuUmu, firstTime, nv);

                        //20220419163256 furukawa st ////////////////////////
                        //前回支給チェックボックスに関係なく値を入れる

                        //      if (vcSejutu.Checked)
                        //      {


                        //20220419171224 furukawa st ////////////////////////
                        //ゼロ判定追加
                        if (int.TryParse(app.TaggedDatas.PastSupplyYM, out int intPast) && intPast > 0) { 
                        //   if (int.TryParse(app.TaggedDatas.PastSupplyYM, out int intPast)){

                        //20220419171224 furukawa ed ////////////////////////

                            int intSejutuG = int.Parse(DateTimeEx.GetEraNumberYearFromYYYYMM(intPast).ToString().Substring(0, 1));
                                int intSejutuY = int.Parse(DateTimeEx.GetEraNumberYearFromYYYYMM(intPast).ToString().Substring(1, 2));
                                int intSejutuM = int.Parse(intPast.ToString().Substring(4, 2));
                                setValue(vbSejutuG, intSejutuG, firstTime, nv);
                                setValue(vbSejutuY, intSejutuY, firstTime, nv);
                                setValue(vbSejutuM, intSejutuM, firstTime, nv);
                            }
                        //      }
                        //20220419163256 furukawa ed ////////////////////////

                    }



                    break;
            }


          
        }


        /// <summary>
        /// コントロール使用可否
        /// </summary>
        private void InitControl()
        {

            #region 初期化
            panelAHK1.Visible = false;
            panelAHK2.Visible = false;
            panelAHK3.Visible = false;
            panelAHK4.Visible = false;
            verifyBoxF1.Visible = false;
            verifyBoxF2.Visible = false;
            verifyBoxF3.Visible = false;
            verifyBoxF4.Visible = false;
            verifyBoxF5.Visible = false;
            labelF1.Visible = false;
            labelF2.Visible = false;
            labelF3.Visible = false;
            labelF4.Visible = false;
            labelF5.Visible = false;
            label26.Visible = false;

            verifyCheckBoxVisit.Visible = false;
            verifyCheckBoxVisitKasan.Visible = false;
            verifyCheckBoxVisitKasan.Enabled = false;

            verifyBoxTotal.Visible = false;
            verifyBoxCharge.Visible = false;
            verifyBoxCountedDays.Visible = false;
            verifyBoxDrCode.Visible = false;
            labelTotal.Visible = false;
            labelCharge.Visible = false;
            labelDays.Visible = false;
            labelDrCode.Visible = false;
            labelDrCode2.Visible = false;

            verifyBoxHnum.Visible = false;
            verifyBoxM.Visible = false;
            verifyBoxNewCont.Visible = false;

            labelHnum.Visible = false;
            labelM.Visible = false;
            labelNewCont.Visible = false;
            labelNewCont2.Visible = false;

            dataGridRefRece.Visible = false;

            labelAppStatus.Visible = false;

            verifyBoxDrName.Visible = false;
            labelDrName.Visible = false;

            verifyBoxRatio.Visible = false;
            labelRatio.Visible = false;

            labelSex.Visible = false;
            labelSex2.Visible = false;
            verifyBoxSex.Visible = false;

            pDoui.Visible = false;

            pSejutu.Visible = false;
            pSejutu.Enabled = false;
            vcSejutu.Visible = false;

            #endregion


            //続紙、白バッジ、その他の場合、入力なし
            if (verifyBoxY.Text == clsInputKind.続紙 ||
                verifyBoxY.Text == clsInputKind.不要 ||
                verifyBoxY.Text == clsInputKind.長期 ||
                verifyBoxY.Text == clsInputKind.エラー ||
                verifyBoxY.Text == clsInputKind.施術同意書裏 ||
                verifyBoxY.Text == clsInputKind.施術報告書 ||
                verifyBoxY.Text == clsInputKind.状態記入書) return;
            
            //同意書

            if (verifyBoxY.Text == clsInputKind.施術同意書)
            {
                pDoui.Visible = true;
                return;
            }

            //申請書の場合

            if (scan.AppType == APP_TYPE.柔整)
            {
                verifyBoxNewCont.Visible = true;
                labelNewCont.Visible = true;
                labelNewCont2.Visible = true;
                dataGridRefRece.Visible = true;
            }
            else
            {
                panelAHK1.Visible = true;
                panelAHK2.Visible = true;
                panelAHK3.Visible = true;
                panelAHK4.Visible = true;
                label26.Visible = true;

                    
                verifyCheckBoxVisitKasan.Visible = true;

                //20220427171752 furukawa st ////////////////////////
                //無効でもいけそう

                //      20220419163410 furukawa st ////////////////////////
                //      有効にしないとベリファイテキストに値が入らない                
                //      if (!firstTime) verifyCheckBoxVisitKasan.Enabled = true;
                //      20220419163410 furukawa ed ////////////////////////
                
                //20220427171752 furukawa ed ////////////////////////
                

                verifyBoxCharge.Visible = true;
                verifyBoxCountedDays.Visible = true;

                labelCharge.Visible = true;
                labelDays.Visible = true;

                verifyBoxDrName.Visible = true;
                labelDrName.Visible = true;

                verifyBoxRatio.Visible = true;
                labelRatio.Visible = true;

                labelSex.Visible = true;
                labelSex2.Visible = true;
                verifyBoxSex.Visible = true;


                pSejutu.Visible = true;

                //20220427171846 furukawa st ////////////////////////
                //無効でもいけそう

                //      20220419164411 furukawa st ////////////////////////
                //      有効にしないとベリファイテキストに値が入らない                
                //      if (!firstTime) pSejutu.Enabled = true;
                //      20220419164411 furukawa ed ////////////////////////
                //20220427171846 furukawa ed ////////////////////////

                vcSejutu.Visible = true;
            }


            //柔整あはき共通
            verifyBoxF1.Visible = true;
            verifyBoxF2.Visible = true;
            verifyBoxF3.Visible = true;
            verifyBoxF4.Visible = true;
            verifyBoxF5.Visible = true;
            labelF1.Visible = true;
            labelF2.Visible = true;
            labelF3.Visible = true;
            labelF4.Visible = true;
            labelF5.Visible = true;
            
            verifyCheckBoxVisit.Visible = true;
            
            verifyBoxTotal.Visible = true;
            labelTotal.Visible = true;

            verifyBoxDrCode.Visible = true;
            labelDrCode.Visible = true;
            labelDrCode2.Visible = true;

            verifyBoxHnum.Visible = true;
            verifyBoxM.Visible = true;
                

            labelHnum.Visible = true;
            labelM.Visible = true;
                

                

            labelAppStatus.Visible = true;

            buiTabStopAdjust();
           
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            InitControl();  
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;
            else if (sender == verifyBoxTotal) p = posTotal;
            else if (sender == verifyBoxNewCont) p = posHnum;
            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;
            else if (sender == verifyCheckBoxVisit) p = posVisit;

            else if (sender == panelAHK1) p = posHnum;
            else if (sender == panelAHK2) p = posTotal;
            else if (sender == panelAHK3) p = posHnum;
            else if (sender == panelAHK4) p = posHnum;

            else if (sender == verifyBoxClinicNum) p = posHnum;//医療機関コード
            else if (sender == verifyBoxClinicName) p = posTotal;//施術所名
            else if (sender == verifyBoxDrName) p = posTotal;//施術師名
            else if (sender == verifyBoxDrCode) p = posTotal;//柔整師登録記号番号

            else if (sender == pDoui) p = posTotal;//同意年月日

            else if(sender==vcSejutu) p = posVisit;//前回交付
            else if(sender==pSejutu) p = posVisit;

            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (verifyBoxY.Focused == true) posYM = pos;
            else if (verifyBoxM.Focused == true) posYM = pos;
            else if (verifyBoxHnum.Focused == true) posHnum = pos;
            else if (verifyBoxNewCont.Focused == true) posNew = pos;
            else if (verifyBoxTotal.Focused == true) posTotal = pos;
            else if (verifyBoxF1 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF2 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF3 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF4 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF5 == this.ActiveControl) posBui = pos;
        }

        private void checkBoxMatchWhere_CheckedChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectRefRece(app);
        }

        private void verifyCheckBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            verifyCheckBoxVisitKasan.Enabled = verifyCheckBoxVisit.Checked;
        }


        /// <summary>
        /// 過去1ヶ月のデータロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            foreach (App a in lstPastData)
            {
                if (verifyBoxHnum.Text == a.HihoNum)
                {
                    verifyBoxHihoname.Text = a.HihoName;
                    verifyBoxHihonameKana.Text = a.TaggedDatas.Kana;
                    verifyBoxSex.Text = a.Sex.ToString();
                    setDateValue(a.Birthday, firstTime,nv,  verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                    return;
                }
            }
        }

        /// <summary>
        /// 過去1ヶ月のデータロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxClinicNum_Validated(object sender, EventArgs e)
        {
         
            foreach (App a in lstPastData)
            {
                if (verifyBoxClinicNum.Text == a.ClinicNum)
                {
                    verifyBoxClinicName.Text = a.ClinicName;
                    verifyBoxDrName.Text = a.DrName;
                    verifyBoxDrCode.Text = a.DrNum;
                    return;
                }
            }
        }

        private void vcSejutu_CheckedChanged(object sender, EventArgs e)
        {
            pSejutu.Enabled = vcSejutu.Checked;
        }

      
    }
}
