﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HiroshimaKoiki2022
{
    /// <summary>
    /// 広島広域専用のViewerData（大阪広域のをコピー）
    /// 
    /// 画像の中に特定の続紙を持っているかどうかのフラグも出力する
    /// （広島広域専用のカスタマイズ分だけ実装し他は元のViewerDataを継承で対応）
    /// </summary>
    class ViewerData : Mejor.ViewerData
    {
        // 同意書があるかどうか
        public bool HasDouisyo { get; set; } = false;
        // 施術報告書があるかどうか
        public bool HasSejutsuHoukokusyo { get; set; } = false;
        // 状態記入書があるかどうか
        public bool HasJoutaikinyusyo { get; set; } = false;

        // 交付料有無
        public bool HasKoufuryo { get; set; } = false;
        // 前回支給
        public string ZenkaiSikyu { get; set; } = "";

        // CSV出力用のヘッダ
        public new static string Header
            => Mejor.ViewerData.Header
               + "," + "HasDouisyo,HasSejutsuHoukokusyo,HasJoutaikinyusyo"
               + "," + "HasKoufuryo,ZenkaiSikyu";

        /// <summary>
        /// Appデータからビューワー用のデータを生成する
        /// </summary>
        /// <param name="app">Appデータ</param>
        /// <returns>ビューワー用のデータ</returns>
        public new static ViewerData CreateViewerData(App app)
        {
            var vd = new ViewerData();
            vd.apply(app);
            return vd;
        }

        /// <summary>
        /// ビューワー用のCSVのヘッダ（項目名）を作成する
        /// </summary>
        /// <returns>CSV文字列</returns>
        public new string CreateCsvLine()
        {
            var line = base.CreateCsvLine();

            bool[] flags = { HasDouisyo, HasSejutsuHoukokusyo, HasJoutaikinyusyo, HasKoufuryo };
            // SQLiteなので true => "1", false => "0" へ変換
            var stringFlags = flags.Select(flag => flag ? "\"1\"" : "\"0\"");

            // 元のCSVヘッダの最後に追加
            return line + "," + string.Join(",", stringFlags) + "," + ZenkaiSikyu;
        }

        /// <summary>
        /// ビューアデータを作成します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="apps"></param>
        /// <param name="infoFileName"></param>
        /// <param name="wf"></param>
        /// <returns>出力数</returns>
        public new static int Export(int cym, List<App> apps, string infoFileName, WaitForm wf)
        {
            var dir = Path.GetDirectoryName(infoFileName);
            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            int receCount = 0;

            try
            {
                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {

                        //中断処理
                        
                        if (CommonTool.WaitFormCancelProcess(wf))
                        {                            
                            wf.LogPrint("Viewerデータの作成を中止しました");                            
                            return -1;
                        }


                        wf.InvokeValue = i;
                        var app = apps[i];

                        tiffs.Add(app.GetImageFullPath());

                        var vd = CreateViewerData(app);
                        // 画像データの生成
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].MediYear > 0) break;

                            // 画像の種別に応じてフラグを立てる
                            switch (apps[i].MediYear)
                            {
                                case (int)APP_SPECIAL_CODE.続紙:
                                case (int)APP_SPECIAL_CODE.長期:
                                case (int)APP_SPECIAL_CODE.往療内訳:
                                    // フラグは立てずに画像の出力だけ
                                    break;
                                case (int)APP_SPECIAL_CODE.同意書:
                                case (int)APP_SPECIAL_CODE.同意書裏:
                                    vd.HasDouisyo = true;
                                    break;
                                case (int)APP_SPECIAL_CODE.施術報告書:
                                    vd.HasSejutsuHoukokusyo = true;
                                    break;
                                case (int)APP_SPECIAL_CODE.状態記入書:
                                    vd.HasJoutaikinyusyo = true;
                                    break;
                                default:
                                    continue;
                            }

                            tiffs.Add(apps[i].GetImageFullPath());
                        }

                        // 交付料有無、前回支給
                        vd.HasKoufuryo = app.TaggedDatas.flgKofuUmu;
                        vd.ZenkaiSikyu = app.TaggedDatas.PastSupplyYM;

                        sw.WriteLine(vd.CreateCsvLine());

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                wf.LogPrint("ViewerInfoデータを作成しています");
                if (!CreateInfo(infoFileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return -1;
            }
            catch (Exception ex)
            {
                wf.LogPrint(ex.Message);
                wf.LogPrint("Viewerデータの作成に失敗しました");
                Log.ErrorWriteWithMsg(ex);
                return -1;
            }
            return receCount;
        }
    }
}
