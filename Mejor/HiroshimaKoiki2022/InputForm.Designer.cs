﻿namespace Mejor.HiroshimaKoiki2022
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelAppStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelNewCont2 = new System.Windows.Forms.Label();
            this.labelNewCont = new System.Windows.Forms.Label();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.pSejutu = new System.Windows.Forms.Panel();
            this.vbSejutuG = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblSejutu = new System.Windows.Forms.Label();
            this.lblSejutuM = new System.Windows.Forms.Label();
            this.lblSejutuY = new System.Windows.Forms.Label();
            this.vbSejutuM = new Mejor.VerifyBox();
            this.vbSejutuY = new Mejor.VerifyBox();
            this.vcSejutu = new Mejor.VerifyCheckBox();
            this.pDoui = new System.Windows.Forms.Panel();
            this.vbDouiG = new Mejor.VerifyBox();
            this.vbDouiD = new Mejor.VerifyBox();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDoui = new System.Windows.Forms.Label();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.panelAHK4 = new System.Windows.Forms.Panel();
            this.labelInsNum1 = new System.Windows.Forms.Label();
            this.verifyBoxReci = new Mejor.VerifyBox();
            this.label10 = new System.Windows.Forms.Label();
            this.verifyBoxInsNum = new Mejor.VerifyBox();
            this.verifyBoxPublicExpanse = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panelAHK1 = new System.Windows.Forms.Panel();
            this.labelSex2 = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.verifyBoxBirthE = new Mejor.VerifyBox();
            this.verifyBoxHihonameKana = new Mejor.VerifyBox();
            this.verifyBoxHihoname = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.verifyCheckBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.panelAHK3 = new System.Windows.Forms.Panel();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.labelDrName = new System.Windows.Forms.Label();
            this.labelRatio = new System.Windows.Forms.Label();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.labelDrCode2 = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.labelDrCode = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxCountedDays = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.panelMatchWhere = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.checkBoxNumber = new System.Windows.Forms.CheckBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelF1 = new System.Windows.Forms.Label();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyCheckBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.labelF2 = new System.Windows.Forms.Label();
            this.labelF3 = new System.Windows.Forms.Label();
            this.labelF4 = new System.Windows.Forms.Label();
            this.labelF5 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dataGridRefRece = new System.Windows.Forms.DataGridView();
            this.panelAHK2 = new System.Windows.Forms.Panel();
            this.verifyBoxClinicName = new Mejor.VerifyBox();
            this.verifyBoxClinicNum = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.pSejutu.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.panelAHK4.SuspendLayout();
            this.panelAHK1.SuspendLayout();
            this.panelAHK3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMatchWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).BeginInit();
            this.panelAHK2.SuspendLayout();
            this.SuspendLayout();
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(148, 20);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(203, 20);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(652, 22);
            this.verifyBoxHnum.MaxLength = 8;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxHnum.TabIndex = 7;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.textBox_Enter);
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(571, 6);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(186, 31);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(241, 31);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(652, 7);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(41, 12);
            this.labelHnum.TabIndex = 6;
            this.labelHnum.Text = "被保番";
            // 
            // labelAppStatus
            // 
            this.labelAppStatus.AutoSize = true;
            this.labelAppStatus.Location = new System.Drawing.Point(921, 10);
            this.labelAppStatus.Name = "labelAppStatus";
            this.labelAppStatus.Size = new System.Drawing.Size(58, 12);
            this.labelAppStatus.TabIndex = 10;
            this.labelAppStatus.Text = "AppStatus";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // labelNewCont2
            // 
            this.labelNewCont2.AutoSize = true;
            this.labelNewCont2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelNewCont2.Location = new System.Drawing.Point(903, 81);
            this.labelNewCont2.Name = "labelNewCont2";
            this.labelNewCont2.Size = new System.Drawing.Size(41, 24);
            this.labelNewCont2.TabIndex = 14;
            this.labelNewCont2.Text = "新規：1\r\n継続：2";
            // 
            // labelNewCont
            // 
            this.labelNewCont.AutoSize = true;
            this.labelNewCont.Location = new System.Drawing.Point(832, 81);
            this.labelNewCont.Name = "labelNewCont";
            this.labelNewCont.Size = new System.Drawing.Size(29, 24);
            this.labelNewCont.TabIndex = 12;
            this.labelNewCont.Text = "新規\r\n継続";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(862, 82);
            this.verifyBoxNewCont.MaxLength = 1;
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxNewCont.TabIndex = 25;
            this.verifyBoxNewCont.TextV = "";
            this.verifyBoxNewCont.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(922, 30);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 11;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(590, 584);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 8;
            this.labelTotal.Text = "合計";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(621, 576);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 74;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.textBox_Enter);
            this.verifyBoxTotal.Leave += new System.EventHandler(this.verifyBoxTotal_Leave);
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 768);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 768);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(73, 743);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(1, 743);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(37, 743);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-191, 743);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 768);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.pSejutu);
            this.panelRight.Controls.Add(this.vcSejutu);
            this.panelRight.Controls.Add(this.pDoui);
            this.panelRight.Controls.Add(this.verifyBoxDrName);
            this.panelRight.Controls.Add(this.panelAHK4);
            this.panelRight.Controls.Add(this.panelAHK1);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisitKasan);
            this.panelRight.Controls.Add(this.panelAHK3);
            this.panelRight.Controls.Add(this.labelDrName);
            this.panelRight.Controls.Add(this.verifyBoxHnum);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelRatio);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxDrCode);
            this.panelRight.Controls.Add(this.labelDrCode2);
            this.panelRight.Controls.Add(this.verifyBoxCharge);
            this.panelRight.Controls.Add(this.verifyBoxRatio);
            this.panelRight.Controls.Add(this.labelDrCode);
            this.panelRight.Controls.Add(this.labelCharge);
            this.panelRight.Controls.Add(this.labelNewCont2);
            this.panelRight.Controls.Add(this.verifyBoxNewCont);
            this.panelRight.Controls.Add(this.labelDays);
            this.panelRight.Controls.Add(this.labelNewCont);
            this.panelRight.Controls.Add(this.verifyBoxCountedDays);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.label1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelF1);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisit);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.labelF2);
            this.panelRight.Controls.Add(this.labelAppStatus);
            this.panelRight.Controls.Add(this.labelF3);
            this.panelRight.Controls.Add(this.labelF4);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.labelF5);
            this.panelRight.Controls.Add(this.label26);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.dataGridRefRece);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.panelAHK2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 768);
            this.panelRight.TabIndex = 0;
            // 
            // pSejutu
            // 
            this.pSejutu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pSejutu.Controls.Add(this.vbSejutuG);
            this.pSejutu.Controls.Add(this.label15);
            this.pSejutu.Controls.Add(this.lblSejutu);
            this.pSejutu.Controls.Add(this.lblSejutuM);
            this.pSejutu.Controls.Add(this.lblSejutuY);
            this.pSejutu.Controls.Add(this.vbSejutuM);
            this.pSejutu.Controls.Add(this.vbSejutuY);
            this.pSejutu.Enabled = false;
            this.pSejutu.Location = new System.Drawing.Point(301, 572);
            this.pSejutu.Name = "pSejutu";
            this.pSejutu.Size = new System.Drawing.Size(260, 40);
            this.pSejutu.TabIndex = 71;
            this.pSejutu.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // vbSejutuG
            // 
            this.vbSejutuG.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuG.Location = new System.Drawing.Point(97, 7);
            this.vbSejutuG.MaxLength = 2;
            this.vbSejutuG.Name = "vbSejutuG";
            this.vbSejutuG.NewLine = false;
            this.vbSejutuG.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuG.TabIndex = 51;
            this.vbSejutuG.TextV = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(59, 7);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 24);
            this.label15.TabIndex = 83;
            this.label15.Text = "4:平成\r\n5:令和";
            // 
            // lblSejutu
            // 
            this.lblSejutu.AutoSize = true;
            this.lblSejutu.Enabled = false;
            this.lblSejutu.Location = new System.Drawing.Point(5, 12);
            this.lblSejutu.Name = "lblSejutu";
            this.lblSejutu.Size = new System.Drawing.Size(53, 12);
            this.lblSejutu.TabIndex = 39;
            this.lblSejutu.Text = "前回支給";
            // 
            // lblSejutuM
            // 
            this.lblSejutuM.AutoSize = true;
            this.lblSejutuM.Location = new System.Drawing.Point(225, 17);
            this.lblSejutuM.Name = "lblSejutuM";
            this.lblSejutuM.Size = new System.Drawing.Size(17, 12);
            this.lblSejutuM.TabIndex = 41;
            this.lblSejutuM.Text = "月";
            // 
            // lblSejutuY
            // 
            this.lblSejutuY.AutoSize = true;
            this.lblSejutuY.Location = new System.Drawing.Point(174, 17);
            this.lblSejutuY.Name = "lblSejutuY";
            this.lblSejutuY.Size = new System.Drawing.Size(17, 12);
            this.lblSejutuY.TabIndex = 40;
            this.lblSejutuY.Text = "年";
            // 
            // vbSejutuM
            // 
            this.vbSejutuM.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuM.Location = new System.Drawing.Point(196, 7);
            this.vbSejutuM.MaxLength = 2;
            this.vbSejutuM.Name = "vbSejutuM";
            this.vbSejutuM.NewLine = false;
            this.vbSejutuM.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuM.TabIndex = 53;
            this.vbSejutuM.TextV = "";
            // 
            // vbSejutuY
            // 
            this.vbSejutuY.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuY.Location = new System.Drawing.Point(145, 7);
            this.vbSejutuY.MaxLength = 2;
            this.vbSejutuY.Name = "vbSejutuY";
            this.vbSejutuY.NewLine = false;
            this.vbSejutuY.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuY.TabIndex = 52;
            this.vbSejutuY.TextV = "";
            // 
            // vcSejutu
            // 
            this.vcSejutu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.vcSejutu.BackColor = System.Drawing.SystemColors.Info;
            this.vcSejutu.CheckedV = false;
            this.vcSejutu.Location = new System.Drawing.Point(202, 577);
            this.vcSejutu.Name = "vcSejutu";
            this.vcSejutu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.vcSejutu.Size = new System.Drawing.Size(95, 25);
            this.vcSejutu.TabIndex = 70;
            this.vcSejutu.Text = "交付料あり";
            this.vcSejutu.UseVisualStyleBackColor = false;
            this.vcSejutu.CheckedChanged += new System.EventHandler(this.vcSejutu_CheckedChanged);
            this.vcSejutu.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Controls.Add(this.label3);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Location = new System.Drawing.Point(273, 18);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(300, 46);
            this.pDoui.TabIndex = 5;
            this.pDoui.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(100, 5);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(247, 5);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(195, 5);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(141, 5);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(55, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 24);
            this.label3.TabIndex = 84;
            this.label3.Text = "4:平成\r\n5:令和";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Location = new System.Drawing.Point(4, 5);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(41, 24);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(225, 14);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(17, 12);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(278, 14);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(17, 12);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(173, 14);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(17, 12);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(869, 627);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxDrName.TabIndex = 88;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // panelAHK4
            // 
            this.panelAHK4.Controls.Add(this.labelInsNum1);
            this.panelAHK4.Controls.Add(this.verifyBoxReci);
            this.panelAHK4.Controls.Add(this.label10);
            this.panelAHK4.Controls.Add(this.verifyBoxInsNum);
            this.panelAHK4.Controls.Add(this.verifyBoxPublicExpanse);
            this.panelAHK4.Controls.Add(this.label22);
            this.panelAHK4.Controls.Add(this.label21);
            this.panelAHK4.Location = new System.Drawing.Point(283, 5);
            this.panelAHK4.Name = "panelAHK4";
            this.panelAHK4.Size = new System.Drawing.Size(360, 60);
            this.panelAHK4.TabIndex = 6;
            this.panelAHK4.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelInsNum1
            // 
            this.labelInsNum1.AutoSize = true;
            this.labelInsNum1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelInsNum1.Location = new System.Drawing.Point(229, 20);
            this.labelInsNum1.Name = "labelInsNum1";
            this.labelInsNum1.Size = new System.Drawing.Size(39, 16);
            this.labelInsNum1.TabIndex = 145;
            this.labelInsNum1.Text = "3934";
            this.labelInsNum1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // verifyBoxReci
            // 
            this.verifyBoxReci.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxReci.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxReci.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxReci.Location = new System.Drawing.Point(110, 17);
            this.verifyBoxReci.Name = "verifyBoxReci";
            this.verifyBoxReci.NewLine = false;
            this.verifyBoxReci.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxReci.TabIndex = 9;
            this.verifyBoxReci.TextV = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(113, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 12);
            this.label10.TabIndex = 144;
            this.label10.Text = "公費受給者番号";
            // 
            // verifyBoxInsNum
            // 
            this.verifyBoxInsNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxInsNum.Location = new System.Drawing.Point(269, 17);
            this.verifyBoxInsNum.Name = "verifyBoxInsNum";
            this.verifyBoxInsNum.NewLine = false;
            this.verifyBoxInsNum.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxInsNum.TabIndex = 10;
            this.verifyBoxInsNum.TextV = "";
            this.verifyBoxInsNum.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxPublicExpanse
            // 
            this.verifyBoxPublicExpanse.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPublicExpanse.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPublicExpanse.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPublicExpanse.Location = new System.Drawing.Point(8, 17);
            this.verifyBoxPublicExpanse.Name = "verifyBoxPublicExpanse";
            this.verifyBoxPublicExpanse.NewLine = false;
            this.verifyBoxPublicExpanse.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxPublicExpanse.TabIndex = 7;
            this.verifyBoxPublicExpanse.TextV = "";
            this.verifyBoxPublicExpanse.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 12);
            this.label22.TabIndex = 6;
            this.label22.Text = "公費負担者番号";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(227, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 143;
            this.label21.Text = "保険者番号";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelAHK1
            // 
            this.panelAHK1.Controls.Add(this.labelSex2);
            this.panelAHK1.Controls.Add(this.verifyBoxSex);
            this.panelAHK1.Controls.Add(this.labelSex);
            this.panelAHK1.Controls.Add(this.verifyBoxBirthD);
            this.panelAHK1.Controls.Add(this.verifyBoxBirthM);
            this.panelAHK1.Controls.Add(this.verifyBoxBirthY);
            this.panelAHK1.Controls.Add(this.verifyBoxBirthE);
            this.panelAHK1.Controls.Add(this.verifyBoxHihonameKana);
            this.panelAHK1.Controls.Add(this.verifyBoxHihoname);
            this.panelAHK1.Controls.Add(this.label23);
            this.panelAHK1.Controls.Add(this.label39);
            this.panelAHK1.Controls.Add(this.label46);
            this.panelAHK1.Controls.Add(this.label38);
            this.panelAHK1.Controls.Add(this.label37);
            this.panelAHK1.Controls.Add(this.labelBirthday);
            this.panelAHK1.Controls.Add(this.label35);
            this.panelAHK1.Location = new System.Drawing.Point(145, 59);
            this.panelAHK1.Name = "panelAHK1";
            this.panelAHK1.Size = new System.Drawing.Size(680, 65);
            this.panelAHK1.TabIndex = 10;
            this.panelAHK1.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelSex2
            // 
            this.labelSex2.AutoSize = true;
            this.labelSex2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelSex2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelSex2.Location = new System.Drawing.Point(366, 21);
            this.labelSex2.Name = "labelSex2";
            this.labelSex2.Size = new System.Drawing.Size(29, 24);
            this.labelSex2.TabIndex = 86;
            this.labelSex2.Text = "男: 1\r\n女: 2";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(339, 22);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 19;
            this.verifyBoxSex.TextV = "";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(336, 8);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 85;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(599, 20);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 23;
            this.verifyBoxBirthD.TextV = "";
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(545, 20);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 22;
            this.verifyBoxBirthM.TextV = "";
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(493, 20);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthY.TabIndex = 21;
            this.verifyBoxBirthY.TextV = "";
            // 
            // verifyBoxBirthE
            // 
            this.verifyBoxBirthE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthE.Location = new System.Drawing.Point(434, 20);
            this.verifyBoxBirthE.MaxLength = 1;
            this.verifyBoxBirthE.Name = "verifyBoxBirthE";
            this.verifyBoxBirthE.NewLine = false;
            this.verifyBoxBirthE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBirthE.TabIndex = 20;
            this.verifyBoxBirthE.TextV = "";
            // 
            // verifyBoxHihonameKana
            // 
            this.verifyBoxHihonameKana.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihonameKana.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihonameKana.ImeMode = System.Windows.Forms.ImeMode.Katakana;
            this.verifyBoxHihonameKana.Location = new System.Drawing.Point(164, 23);
            this.verifyBoxHihonameKana.MaxLength = 25;
            this.verifyBoxHihonameKana.Name = "verifyBoxHihonameKana";
            this.verifyBoxHihonameKana.NewLine = false;
            this.verifyBoxHihonameKana.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxHihonameKana.TabIndex = 18;
            this.verifyBoxHihonameKana.TextV = "";
            // 
            // verifyBoxHihoname
            // 
            this.verifyBoxHihoname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihoname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihoname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHihoname.Location = new System.Drawing.Point(8, 23);
            this.verifyBoxHihoname.MaxLength = 25;
            this.verifyBoxHihoname.Name = "verifyBoxHihoname";
            this.verifyBoxHihoname.NewLine = false;
            this.verifyBoxHihoname.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxHihoname.TabIndex = 15;
            this.verifyBoxHihoname.TextV = "";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(161, 6);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 12);
            this.label23.TabIndex = 72;
            this.label23.Text = "被保険者名カナ";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(635, 31);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 66;
            this.label39.Text = "日";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(5, 6);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 12);
            this.label46.TabIndex = 72;
            this.label46.Text = "被保険者名";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(578, 31);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 65;
            this.label38.Text = "月";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(526, 31);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 64;
            this.label37.Text = "年";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(403, 20);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(29, 24);
            this.labelBirthday.TabIndex = 62;
            this.labelBirthday.Text = "生年\r\n月日";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label35.Location = new System.Drawing.Point(461, 10);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 36);
            this.label35.TabIndex = 63;
            this.label35.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyCheckBoxVisitKasan
            // 
            this.verifyCheckBoxVisitKasan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisitKasan.CheckedV = false;
            this.verifyCheckBoxVisitKasan.Enabled = false;
            this.verifyCheckBoxVisitKasan.Location = new System.Drawing.Point(94, 577);
            this.verifyCheckBoxVisitKasan.Name = "verifyCheckBoxVisitKasan";
            this.verifyCheckBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisitKasan.Size = new System.Drawing.Size(87, 25);
            this.verifyCheckBoxVisitKasan.TabIndex = 65;
            this.verifyCheckBoxVisitKasan.Text = "加算有り";
            this.verifyCheckBoxVisitKasan.UseVisualStyleBackColor = false;
            // 
            // panelAHK3
            // 
            this.panelAHK3.Controls.Add(this.verifyBoxF1Finish);
            this.panelAHK3.Controls.Add(this.label2);
            this.panelAHK3.Controls.Add(this.verifyBoxF1Start);
            this.panelAHK3.Controls.Add(this.label9);
            this.panelAHK3.Controls.Add(this.label7);
            this.panelAHK3.Controls.Add(this.verifyBoxF1FirstD);
            this.panelAHK3.Controls.Add(this.label5);
            this.panelAHK3.Controls.Add(this.verifyBoxF1FirstY);
            this.panelAHK3.Controls.Add(this.label6);
            this.panelAHK3.Controls.Add(this.verifyBoxF1FirstM);
            this.panelAHK3.Controls.Add(this.label14);
            this.panelAHK3.Controls.Add(this.label11);
            this.panelAHK3.Controls.Add(this.label13);
            this.panelAHK3.Controls.Add(this.label12);
            this.panelAHK3.Controls.Add(this.verifyBoxF1FirstE);
            this.panelAHK3.Location = new System.Drawing.Point(145, 123);
            this.panelAHK3.Name = "panelAHK3";
            this.panelAHK3.Size = new System.Drawing.Size(364, 65);
            this.panelAHK3.TabIndex = 27;
            this.panelAHK3.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(289, 22);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 56;
            this.verifyBoxF1Finish.TextV = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(46, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(231, 22);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 53;
            this.verifyBoxF1Start.TextV = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(287, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 55;
            this.label9.Text = "終了日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(229, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 52;
            this.label7.Text = "開始日";
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(170, 22);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 50;
            this.verifyBoxF1FirstD.TextV = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(317, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 57;
            this.label5.Text = "日";
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(80, 22);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 46;
            this.verifyBoxF1FirstY.TextV = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(78, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 45;
            this.label6.Text = "初検日";
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(125, 22);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 48;
            this.verifyBoxF1FirstM.TextV = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(259, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 54;
            this.label14.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(108, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 47;
            this.label11.Text = "年";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(198, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 51;
            this.label13.Text = "日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(153, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 49;
            this.label12.Text = "月";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(19, 22);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 44;
            this.verifyBoxF1FirstE.TextV = "";
            // 
            // labelDrName
            // 
            this.labelDrName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDrName.AutoSize = true;
            this.labelDrName.Location = new System.Drawing.Point(815, 633);
            this.labelDrName.Name = "labelDrName";
            this.labelDrName.Size = new System.Drawing.Size(53, 12);
            this.labelDrName.TabIndex = 141;
            this.labelDrName.Text = "施術師名";
            this.labelDrName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRatio
            // 
            this.labelRatio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRatio.AutoSize = true;
            this.labelRatio.Location = new System.Drawing.Point(860, 583);
            this.labelRatio.Name = "labelRatio";
            this.labelRatio.Size = new System.Drawing.Size(29, 12);
            this.labelRatio.TabIndex = 66;
            this.labelRatio.Text = "割合";
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrCode.Location = new System.Drawing.Point(645, 628);
            this.verifyBoxDrCode.MaxLength = 10;
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxDrCode.TabIndex = 85;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelDrCode2
            // 
            this.labelDrCode2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDrCode2.AutoSize = true;
            this.labelDrCode2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDrCode2.Location = new System.Drawing.Point(763, 627);
            this.labelDrCode2.Name = "labelDrCode2";
            this.labelDrCode2.Size = new System.Drawing.Size(37, 24);
            this.labelDrCode2.TabIndex = 75;
            this.labelDrCode2.Text = "[協]: 0\r\n[契]: 1";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(766, 576);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxCharge.TabIndex = 75;
            this.verifyBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(891, 575);
            this.verifyBoxRatio.MaxLength = 2;
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxRatio.TabIndex = 77;
            this.verifyBoxRatio.TextV = "";
            this.verifyBoxRatio.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelDrCode
            // 
            this.labelDrCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDrCode.AutoSize = true;
            this.labelDrCode.Location = new System.Drawing.Point(591, 627);
            this.labelDrCode.Name = "labelDrCode";
            this.labelDrCode.Size = new System.Drawing.Size(53, 24);
            this.labelDrCode.TabIndex = 73;
            this.labelDrCode.Text = "柔整師\r\n登録番号";
            // 
            // labelCharge
            // 
            this.labelCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(711, 584);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(53, 12);
            this.labelCharge.TabIndex = 39;
            this.labelCharge.Text = "請求金額";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(527, 130);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(53, 12);
            this.labelDays.TabIndex = 40;
            this.labelDays.Text = "診療日数";
            // 
            // verifyBoxCountedDays
            // 
            this.verifyBoxCountedDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCountedDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCountedDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCountedDays.Location = new System.Drawing.Point(527, 146);
            this.verifyBoxCountedDays.MaxLength = 3;
            this.verifyBoxCountedDays.Name = "verifyBoxCountedDays";
            this.verifyBoxCountedDays.NewLine = false;
            this.verifyBoxCountedDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxCountedDays.TabIndex = 30;
            this.verifyBoxCountedDays.TextV = "";
            this.verifyBoxCountedDays.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 84);
            this.label1.TabIndex = 36;
            this.label1.Text = "続紙    : \"--\"     \r\n不要    : \"++\"\r\n長期    : \"..\"     \r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r" +
    "\n施術報告書  : \"911\"\r\n状態記入書  : \"921\"\r\n";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelInputerName);
            this.panel1.Controls.Add(this.panelMatchWhere);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 736);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 32);
            this.panel1.TabIndex = 100;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.AutoSize = true;
            this.labelInputerName.Location = new System.Drawing.Point(3, 4);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(35, 12);
            this.labelInputerName.TabIndex = 35;
            this.labelInputerName.Text = "入力：";
            // 
            // panelMatchWhere
            // 
            this.panelMatchWhere.Controls.Add(this.label4);
            this.panelMatchWhere.Controls.Add(this.checkBoxTotal);
            this.panelMatchWhere.Controls.Add(this.checkBoxNumber);
            this.panelMatchWhere.Location = new System.Drawing.Point(228, 6);
            this.panelMatchWhere.Name = "panelMatchWhere";
            this.panelMatchWhere.Size = new System.Drawing.Size(242, 23);
            this.panelMatchWhere.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "突合候補条件";
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Checked = true;
            this.checkBoxTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTotal.Location = new System.Drawing.Point(160, 4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(72, 16);
            this.checkBoxTotal.TabIndex = 38;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            this.checkBoxTotal.CheckedChanged += new System.EventHandler(this.checkBoxMatchWhere_CheckedChanged);
            // 
            // checkBoxNumber
            // 
            this.checkBoxNumber.AutoSize = true;
            this.checkBoxNumber.Checked = true;
            this.checkBoxNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNumber.Location = new System.Drawing.Point(94, 4);
            this.checkBoxNumber.Name = "checkBoxNumber";
            this.checkBoxNumber.Size = new System.Drawing.Size(60, 16);
            this.checkBoxNumber.TabIndex = 37;
            this.checkBoxNumber.Text = "被保番";
            this.checkBoxNumber.UseVisualStyleBackColor = true;
            this.checkBoxNumber.CheckedChanged += new System.EventHandler(this.checkBoxMatchWhere_CheckedChanged);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(475, 6);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 30;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelF1
            // 
            this.labelF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(1, 514);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(47, 12);
            this.labelF1.TabIndex = 17;
            this.labelF1.Text = "負傷名1";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(204, 529);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF2.TabIndex = 37;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF2.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(407, 529);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF3.TabIndex = 40;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF3.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyCheckBoxVisit
            // 
            this.verifyCheckBoxVisit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisit.CheckedV = false;
            this.verifyCheckBoxVisit.Location = new System.Drawing.Point(4, 577);
            this.verifyCheckBoxVisit.Margin = new System.Windows.Forms.Padding(5, 3, 0, 0);
            this.verifyCheckBoxVisit.Name = "verifyCheckBoxVisit";
            this.verifyCheckBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisit.Size = new System.Drawing.Size(87, 25);
            this.verifyCheckBoxVisit.TabIndex = 60;
            this.verifyCheckBoxVisit.Text = "往療あり";
            this.verifyCheckBoxVisit.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisit.CheckedChanged += new System.EventHandler(this.verifyCheckBoxVisit_CheckedChanged);
            this.verifyCheckBoxVisit.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(1, 529);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF1.TabIndex = 35;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 190);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 314);
            this.scrollPictureControl1.TabIndex = 28;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(610, 529);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF4.TabIndex = 42;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF4.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(813, 529);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF5.TabIndex = 44;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelF2
            // 
            this.labelF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF2.AutoSize = true;
            this.labelF2.Location = new System.Drawing.Point(202, 514);
            this.labelF2.Name = "labelF2";
            this.labelF2.Size = new System.Drawing.Size(47, 12);
            this.labelF2.TabIndex = 19;
            this.labelF2.Text = "負傷名2";
            // 
            // labelF3
            // 
            this.labelF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF3.AutoSize = true;
            this.labelF3.Location = new System.Drawing.Point(405, 514);
            this.labelF3.Name = "labelF3";
            this.labelF3.Size = new System.Drawing.Size(47, 12);
            this.labelF3.TabIndex = 21;
            this.labelF3.Text = "負傷名3";
            // 
            // labelF4
            // 
            this.labelF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF4.AutoSize = true;
            this.labelF4.Location = new System.Drawing.Point(610, 513);
            this.labelF4.Name = "labelF4";
            this.labelF4.Size = new System.Drawing.Size(47, 12);
            this.labelF4.TabIndex = 23;
            this.labelF4.Text = "負傷名4";
            // 
            // labelF5
            // 
            this.labelF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF5.AutoSize = true;
            this.labelF5.Location = new System.Drawing.Point(810, 513);
            this.labelF5.Name = "labelF5";
            this.labelF5.Size = new System.Drawing.Size(47, 12);
            this.labelF5.TabIndex = 25;
            this.labelF5.Text = "負傷名5";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(3, 558);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(484, 12);
            this.label26.TabIndex = 27;
            this.label26.Text = "鍼灸時：　　1.神経痛　2.リウマチ　3.頚腕症候群　4.五十肩　5.腰痛症　6.頸椎捻挫後遺症　7.その他";
            // 
            // dataGridRefRece
            // 
            this.dataGridRefRece.AllowUserToAddRows = false;
            this.dataGridRefRece.AllowUserToDeleteRows = false;
            this.dataGridRefRece.AllowUserToResizeRows = false;
            this.dataGridRefRece.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridRefRece.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridRefRece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridRefRece.Location = new System.Drawing.Point(4, 667);
            this.dataGridRefRece.Name = "dataGridRefRece";
            this.dataGridRefRece.ReadOnly = true;
            this.dataGridRefRece.RowHeadersVisible = false;
            this.dataGridRefRece.RowTemplate.Height = 21;
            this.dataGridRefRece.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRefRece.Size = new System.Drawing.Size(1000, 65);
            this.dataGridRefRece.StandardTab = true;
            this.dataGridRefRece.TabIndex = 29;
            this.dataGridRefRece.TabStop = false;
            // 
            // panelAHK2
            // 
            this.panelAHK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelAHK2.Controls.Add(this.verifyBoxClinicName);
            this.panelAHK2.Controls.Add(this.verifyBoxClinicNum);
            this.panelAHK2.Controls.Add(this.label19);
            this.panelAHK2.Controls.Add(this.label20);
            this.panelAHK2.Location = new System.Drawing.Point(28, 617);
            this.panelAHK2.Name = "panelAHK2";
            this.panelAHK2.Size = new System.Drawing.Size(490, 40);
            this.panelAHK2.TabIndex = 79;
            this.panelAHK2.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxClinicName
            // 
            this.verifyBoxClinicName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxClinicName.Location = new System.Drawing.Point(261, 11);
            this.verifyBoxClinicName.Name = "verifyBoxClinicName";
            this.verifyBoxClinicName.NewLine = false;
            this.verifyBoxClinicName.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxClinicName.TabIndex = 8;
            this.verifyBoxClinicName.TextV = "";
            this.verifyBoxClinicName.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxClinicNum
            // 
            this.verifyBoxClinicNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxClinicNum.Location = new System.Drawing.Point(55, 12);
            this.verifyBoxClinicNum.MaxLength = 10;
            this.verifyBoxClinicNum.Name = "verifyBoxClinicNum";
            this.verifyBoxClinicNum.NewLine = false;
            this.verifyBoxClinicNum.Size = new System.Drawing.Size(160, 23);
            this.verifyBoxClinicNum.TabIndex = 7;
            this.verifyBoxClinicNum.TextV = "";
            this.verifyBoxClinicNum.Enter += new System.EventHandler(this.textBox_Enter);
            this.verifyBoxClinicNum.Validated += new System.EventHandler(this.verifyBoxClinicNum_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(224, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 24);
            this.label19.TabIndex = 141;
            this.label19.Text = "施術\r\n所名";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 141;
            this.label20.Text = "施術\r\n所番号";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 768);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 768);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.pSejutu.ResumeLayout(false);
            this.pSejutu.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.panelAHK4.ResumeLayout(false);
            this.panelAHK4.PerformLayout();
            this.panelAHK1.ResumeLayout(false);
            this.panelAHK1.PerformLayout();
            this.panelAHK3.ResumeLayout(false);
            this.panelAHK3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelMatchWhere.ResumeLayout(false);
            this.panelMatchWhere.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).EndInit();
            this.panelAHK2.ResumeLayout(false);
            this.panelAHK2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelAppStatus;
        private System.Windows.Forms.DataGridView dataGridRefRece;
        private System.Windows.Forms.Label labelNewCont2;
        private System.Windows.Forms.Label labelNewCont;
        private VerifyBox verifyBoxNewCont;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label labelF5;
        private System.Windows.Forms.Label labelF4;
        private System.Windows.Forms.Label labelF3;
        private System.Windows.Forms.Label labelF2;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyCheckBox verifyCheckBoxVisit;
        private System.Windows.Forms.Panel panelMatchWhere;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.CheckBox checkBoxNumber;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxCountedDays;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.Label labelCharge;
        private VerifyCheckBox verifyCheckBoxVisitKasan;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxF1FirstE;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1Start;
        private System.Windows.Forms.Label label9;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxBirthD;
        private VerifyBox verifyBoxBirthM;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthE;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label labelBirthday;
        private VerifyBox verifyBoxHihoname;
        private System.Windows.Forms.Label label46;
        private VerifyBox verifyBoxDrCode;
        private System.Windows.Forms.Label labelDrCode2;
        private System.Windows.Forms.Label labelDrCode;
        private System.Windows.Forms.Label labelDrName;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private VerifyBox verifyBoxClinicNum;
        private VerifyBox verifyBoxClinicName;
        private VerifyBox verifyBoxInsNum;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelRatio;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Label label22;
        private VerifyBox verifyBoxPublicExpanse;
        private VerifyBox verifyBoxHihonameKana;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panelAHK1;
        private System.Windows.Forms.Panel panelAHK2;
        private System.Windows.Forms.Panel panelAHK3;
        private System.Windows.Forms.Panel panelAHK4;
        private VerifyBox verifyBoxReci;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelSex2;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelInsNum1;
        private System.Windows.Forms.Panel pDoui;
        private VerifyBox vbDouiG;
        private VerifyBox vbDouiD;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDoui;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private System.Windows.Forms.Panel pSejutu;
        private VerifyBox vbSejutuG;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSejutu;
        private System.Windows.Forms.Label lblSejutuM;
        private System.Windows.Forms.Label lblSejutuY;
        private VerifyBox vbSejutuM;
        private VerifyBox vbSejutuY;
        private VerifyCheckBox vcSejutu;
    }
}