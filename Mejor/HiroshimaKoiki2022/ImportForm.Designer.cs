﻿namespace Mejor.HiroshimaKoiki2022
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonJyu = new System.Windows.Forms.RadioButton();
            this.radioButtonShin = new System.Windows.Forms.RadioButton();
            this.radioButtonAnma = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(345, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "インポート";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "審査月を指定して下さい";
            // 
            // radioButtonJyu
            // 
            this.radioButtonJyu.AutoSize = true;
            this.radioButtonJyu.Location = new System.Drawing.Point(71, 59);
            this.radioButtonJyu.Name = "radioButtonJyu";
            this.radioButtonJyu.Size = new System.Drawing.Size(47, 16);
            this.radioButtonJyu.TabIndex = 6;
            this.radioButtonJyu.TabStop = true;
            this.radioButtonJyu.Text = "柔整";
            this.radioButtonJyu.UseVisualStyleBackColor = true;
            // 
            // radioButtonShin
            // 
            this.radioButtonShin.AutoSize = true;
            this.radioButtonShin.Location = new System.Drawing.Point(142, 59);
            this.radioButtonShin.Name = "radioButtonShin";
            this.radioButtonShin.Size = new System.Drawing.Size(47, 16);
            this.radioButtonShin.TabIndex = 6;
            this.radioButtonShin.TabStop = true;
            this.radioButtonShin.Text = "鍼灸";
            this.radioButtonShin.UseVisualStyleBackColor = true;
            // 
            // radioButtonAnma
            // 
            this.radioButtonAnma.AutoSize = true;
            this.radioButtonAnma.Location = new System.Drawing.Point(213, 59);
            this.radioButtonAnma.Name = "radioButtonAnma";
            this.radioButtonAnma.Size = new System.Drawing.Size(104, 16);
            this.radioButtonAnma.TabIndex = 6;
            this.radioButtonAnma.TabStop = true;
            this.radioButtonAnma.Text = "あんま・マッサージ";
            this.radioButtonAnma.UseVisualStyleBackColor = true;
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 99);
            this.Controls.Add(this.radioButtonAnma);
            this.Controls.Add(this.radioButtonShin);
            this.Controls.Add(this.radioButtonJyu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "ImportForm";
            this.Text = "データインポート 広島広域";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonJyu;
        private System.Windows.Forms.RadioButton radioButtonShin;
        private System.Windows.Forms.RadioButton radioButtonAnma;
    }
}