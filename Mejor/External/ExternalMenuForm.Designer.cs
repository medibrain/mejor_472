﻿namespace Mejor
{
    partial class ExternalMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonImportTif = new System.Windows.Forms.Button();
            this.buttonTifcopy = new System.Windows.Forms.Button();
            this.buttonImportMediApp = new System.Windows.Forms.Button();
            this.buttonInputedAppOut = new System.Windows.Forms.Button();
            this.buttonComp = new System.Windows.Forms.Button();
            this.buttonImportExternalApp = new System.Windows.Forms.Button();
            this.buttonAppOut = new System.Windows.Forms.Button();
            this.buttonTifCompress = new System.Windows.Forms.Button();
            this.groupBoxMedi = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBoxGaibu = new System.Windows.Forms.GroupBox();
            this.checkBoxResetEnable = new System.Windows.Forms.CheckBox();
            this.buttonResetInputData = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBoxTestData = new System.Windows.Forms.CheckBox();
            this.groupBoxMedi.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxGaibu.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonImportTif
            // 
            this.buttonImportTif.Location = new System.Drawing.Point(22, 33);
            this.buttonImportTif.Margin = new System.Windows.Forms.Padding(4);
            this.buttonImportTif.Name = "buttonImportTif";
            this.buttonImportTif.Size = new System.Drawing.Size(244, 55);
            this.buttonImportTif.TabIndex = 15;
            this.buttonImportTif.Text = "マスキング画像取込";
            this.buttonImportTif.UseVisualStyleBackColor = true;
            this.buttonImportTif.Click += new System.EventHandler(this.buttonImportTif_Click);
            // 
            // buttonTifcopy
            // 
            this.buttonTifcopy.Location = new System.Drawing.Point(43, 39);
            this.buttonTifcopy.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTifcopy.Name = "buttonTifcopy";
            this.buttonTifcopy.Size = new System.Drawing.Size(244, 55);
            this.buttonTifcopy.TabIndex = 14;
            this.buttonTifcopy.Text = "マスキング用画像出力";
            this.buttonTifcopy.UseVisualStyleBackColor = true;
            this.buttonTifcopy.Click += new System.EventHandler(this.buttonTifcopy_Click);
            // 
            // buttonImportMediApp
            // 
            this.buttonImportMediApp.Location = new System.Drawing.Point(22, 96);
            this.buttonImportMediApp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonImportMediApp.Name = "buttonImportMediApp";
            this.buttonImportMediApp.Size = new System.Drawing.Size(244, 55);
            this.buttonImportMediApp.TabIndex = 11;
            this.buttonImportMediApp.Text = "データ取込";
            this.buttonImportMediApp.UseVisualStyleBackColor = true;
            this.buttonImportMediApp.Click += new System.EventHandler(this.buttonImportMediApp_Click);
            // 
            // buttonInputedAppOut
            // 
            this.buttonInputedAppOut.Location = new System.Drawing.Point(52, 281);
            this.buttonInputedAppOut.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInputedAppOut.Name = "buttonInputedAppOut";
            this.buttonInputedAppOut.Size = new System.Drawing.Size(244, 55);
            this.buttonInputedAppOut.TabIndex = 12;
            this.buttonInputedAppOut.Text = "入力済みデータ出力";
            this.buttonInputedAppOut.UseVisualStyleBackColor = true;
            this.buttonInputedAppOut.Click += new System.EventHandler(this.buttonAppOut_Click);
            // 
            // buttonComp
            // 
            this.buttonComp.Location = new System.Drawing.Point(43, 428);
            this.buttonComp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonComp.Name = "buttonComp";
            this.buttonComp.Size = new System.Drawing.Size(244, 55);
            this.buttonComp.TabIndex = 10;
            this.buttonComp.Text = "本社・外部業者　比較";
            this.buttonComp.UseVisualStyleBackColor = true;
            this.buttonComp.Click += new System.EventHandler(this.buttonComp_Click);
            // 
            // buttonImportExternalApp
            // 
            this.buttonImportExternalApp.Location = new System.Drawing.Point(43, 365);
            this.buttonImportExternalApp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonImportExternalApp.Name = "buttonImportExternalApp";
            this.buttonImportExternalApp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonImportExternalApp.Size = new System.Drawing.Size(244, 55);
            this.buttonImportExternalApp.TabIndex = 9;
            this.buttonImportExternalApp.Text = "外部業者入力済みデータ\r\nインポート";
            this.buttonImportExternalApp.UseVisualStyleBackColor = true;
            this.buttonImportExternalApp.Click += new System.EventHandler(this.buttonImportMediApp_Click);
            // 
            // buttonAppOut
            // 
            this.buttonAppOut.Location = new System.Drawing.Point(16, 111);
            this.buttonAppOut.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAppOut.Name = "buttonAppOut";
            this.buttonAppOut.Size = new System.Drawing.Size(244, 55);
            this.buttonAppOut.TabIndex = 8;
            this.buttonAppOut.Text = "外部業者用申請書データ出力";
            this.buttonAppOut.UseVisualStyleBackColor = true;
            this.buttonAppOut.Click += new System.EventHandler(this.buttonAppOut_Click);
            // 
            // buttonTifCompress
            // 
            this.buttonTifCompress.Location = new System.Drawing.Point(16, 32);
            this.buttonTifCompress.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTifCompress.Name = "buttonTifCompress";
            this.buttonTifCompress.Size = new System.Drawing.Size(244, 55);
            this.buttonTifCompress.TabIndex = 10;
            this.buttonTifCompress.Text = "マスキング画像書庫化";
            this.buttonTifCompress.UseVisualStyleBackColor = true;
            this.buttonTifCompress.Click += new System.EventHandler(this.buttonTifCompress_Click);
            // 
            // groupBoxMedi
            // 
            this.groupBoxMedi.Controls.Add(this.groupBox3);
            this.groupBoxMedi.Controls.Add(this.buttonImportExternalApp);
            this.groupBoxMedi.Controls.Add(this.buttonTifcopy);
            this.groupBoxMedi.Controls.Add(this.buttonComp);
            this.groupBoxMedi.Location = new System.Drawing.Point(28, 26);
            this.groupBoxMedi.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxMedi.Name = "groupBoxMedi";
            this.groupBoxMedi.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxMedi.Size = new System.Drawing.Size(338, 492);
            this.groupBoxMedi.TabIndex = 16;
            this.groupBoxMedi.TabStop = false;
            this.groupBoxMedi.Text = "本社用";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxTestData);
            this.groupBox3.Controls.Add(this.buttonTifCompress);
            this.groupBox3.Controls.Add(this.buttonAppOut);
            this.groupBox3.Location = new System.Drawing.Point(27, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(289, 235);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "外部業者への出力";
            // 
            // groupBoxGaibu
            // 
            this.groupBoxGaibu.Controls.Add(this.checkBoxResetEnable);
            this.groupBoxGaibu.Controls.Add(this.buttonResetInputData);
            this.groupBoxGaibu.Controls.Add(this.groupBox4);
            this.groupBoxGaibu.Controls.Add(this.buttonInputedAppOut);
            this.groupBoxGaibu.Location = new System.Drawing.Point(406, 26);
            this.groupBoxGaibu.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxGaibu.Name = "groupBoxGaibu";
            this.groupBoxGaibu.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxGaibu.Size = new System.Drawing.Size(342, 494);
            this.groupBoxGaibu.TabIndex = 17;
            this.groupBoxGaibu.TabStop = false;
            this.groupBoxGaibu.Text = "外部業者用";
            // 
            // checkBoxResetEnable
            // 
            this.checkBoxResetEnable.AutoSize = true;
            this.checkBoxResetEnable.Location = new System.Drawing.Point(52, 411);
            this.checkBoxResetEnable.Name = "checkBoxResetEnable";
            this.checkBoxResetEnable.Size = new System.Drawing.Size(71, 19);
            this.checkBoxResetEnable.TabIndex = 18;
            this.checkBoxResetEnable.Text = "有効化";
            this.checkBoxResetEnable.UseVisualStyleBackColor = true;
            this.checkBoxResetEnable.CheckedChanged += new System.EventHandler(this.checkBoxResetEnable_CheckedChanged);
            // 
            // buttonResetInputData
            // 
            this.buttonResetInputData.Enabled = false;
            this.buttonResetInputData.Location = new System.Drawing.Point(52, 436);
            this.buttonResetInputData.Name = "buttonResetInputData";
            this.buttonResetInputData.Size = new System.Drawing.Size(244, 38);
            this.buttonResetInputData.TabIndex = 17;
            this.buttonResetInputData.Text = "入力データリセット";
            this.buttonResetInputData.UseVisualStyleBackColor = true;
            this.buttonResetInputData.Click += new System.EventHandler(this.buttonResetInputData_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonImportMediApp);
            this.groupBox4.Controls.Add(this.buttonImportTif);
            this.groupBox4.Location = new System.Drawing.Point(30, 39);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(288, 178);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "取込作業";
            // 
            // checkBoxTestData
            // 
            this.checkBoxTestData.AutoSize = true;
            this.checkBoxTestData.Checked = true;
            this.checkBoxTestData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTestData.Location = new System.Drawing.Point(16, 180);
            this.checkBoxTestData.Name = "checkBoxTestData";
            this.checkBoxTestData.Size = new System.Drawing.Size(138, 19);
            this.checkBoxTestData.TabIndex = 12;
            this.checkBoxTestData.Text = "テスト入力用データ";
            this.checkBoxTestData.UseVisualStyleBackColor = true;
            // 
            // ExternalMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 565);
            this.Controls.Add(this.groupBoxGaibu);
            this.Controls.Add(this.groupBoxMedi);
            this.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExternalMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "外部業者用メニュー";
            this.groupBoxMedi.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxGaibu.ResumeLayout(false);
            this.groupBoxGaibu.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonImportTif;
        private System.Windows.Forms.Button buttonTifcopy;
        private System.Windows.Forms.Button buttonImportMediApp;
        private System.Windows.Forms.Button buttonInputedAppOut;
        private System.Windows.Forms.Button buttonComp;
        private System.Windows.Forms.Button buttonImportExternalApp;
        private System.Windows.Forms.Button buttonAppOut;
        private System.Windows.Forms.Button buttonTifCompress;
        private System.Windows.Forms.GroupBox groupBoxMedi;
        private System.Windows.Forms.GroupBox groupBoxGaibu;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxResetEnable;
        private System.Windows.Forms.Button buttonResetInputData;
        private System.Windows.Forms.CheckBox checkBoxTestData;
    }
}