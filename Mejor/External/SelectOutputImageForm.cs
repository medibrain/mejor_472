﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
   
    public partial class SelectOutputImageForm : Form
    {

        /// <summary>
        /// 選択したScanリスト
        /// </summary>
        public List<Scan> ListScan = new List<Scan>();

        /// <summary>
        /// 表示用データテーブル
        /// </summary>
        DataTable dtDisp = new DataTable();
        int cym = 0;
        
        /// <summary>
        /// 表示データ
        /// </summary>
        List<ScanToExternal> lstStE = new List<ScanToExternal>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="_cym">cym</param>
        /// <param name="flgDataoutput">データ出力時＝true</param>
        public SelectOutputImageForm(int _cym,bool flgDataoutput=false)
        {
            InitializeComponent();
            CommonTool.setFormColor(this);

            //マスキング用画像出力時にはテーブル初期化
            if (!flgDataoutput)
            {
                if (!InitTable())
                {
                    MessageBox.Show($"テーブル初期化に失敗しました", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    this.DialogResult = DialogResult.Cancel;
                    return;
                }
            }
          
            
            cym = _cym;
            labelInsurer.Text = $"{Insurer.CurrrentInsurer.InsurerName} {_cym.ToString().Substring(0,4)}/{_cym.ToString().Substring(4)}";
            disp();

            //zip化したスキャンIDは先にチェックを付けておく
            if (flgDataoutput)
            {
                foreach(DataGridViewRow dr in dgv.Rows)
                {
                    if (dr.Cells[nameof(ScanToExternal.zipdate)].Value.ToString() != string.Empty && 
                        dr.Cells[nameof(ScanToExternal.zipdate)].Value.ToString() != DateTime.MinValue.ToString())
                    {
                        dr.Cells[nameof(ScanToExternal.sel)].Value = true;
                    }
                    else dr.Cells[nameof(ScanToExternal.sel)].Value = false;
                }
            }

        }

      
        /// <summary>
        /// 初期表示処理
        /// </summary>
        private void disp()
        {
            InitDataTable();
            
            //既存のレコードを取得し、なければScanから取得する
            lstStE = GetScanToExternal(cym);
            if (lstStE == null || lstStE.Count==0)
            {
                List<Scan> lstScan = Scan.GetScanListYM(cym);
                lstScan.Sort((x, y) => x.SID.CompareTo(y.SID));

                foreach (Scan s in lstScan)
                {
                    ScanToExternal r = new ScanToExternal();
                    r.sel = false;
                    r.scanid = s.SID;
                    r.apptype = s.AppType;
                    r.note1 = s.Note1;
                    r.note2 = s.Note2;
                    r.status = s.Status;
                    //遅いのでやめた
                    //r.fileCount = System.IO.Directory.GetFiles(s.ImageFolder, "*.tif", System.IO.SearchOption.TopDirectoryOnly).Length;

                    lstStE.Add(r);
                }
            }          

            dgv.DataSource = lstStE;

            InitGrid();
            Initcmb();
        }

        /// <summary>
        /// 申請書タイプコンボ
        /// </summary>
        private void Initcmb()
        {

            List<APP_TYPE> lst = new List<APP_TYPE>();
            foreach(var item in lstStE)
            {
                if (lst.Contains((APP_TYPE)item.apptype)) continue;
                lst.Add((APP_TYPE)item.apptype);
            }
            
            comboBoxKind.DataSource = lst;
        }


        /// <summary>
        /// 既存のレコードを取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private List<ScanToExternal> GetScanToExternal(int cym)
        {
            List<ScanToExternal> lstSte = new List<ScanToExternal>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from scan_to_external where cym={cym} order by scanid");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var list=cmd.TryExecuteReaderList();
                foreach(var ste in list)
                {
                    ScanToExternal cls = new ScanToExternal();
                    cls.scanid = int.Parse(ste[0].ToString());
                    cls.apptype = (APP_TYPE)ste[1];
                    cls.note1   =ste[2].ToString();
                    cls.note2   =ste[3].ToString();
                    cls.status = (SCAN_STATUS)ste[4];

                    if (DateTime.TryParse(ste[5].ToString(), out DateTime tmpOutdate)) cls.outdate = tmpOutdate;
                    else cls.outdate = DateTime.MinValue;
                    cls.outuser =ste[6].ToString();

                    if (DateTime.TryParse(ste[7].ToString(), out DateTime tmpZipdate)) cls.zipdate = tmpZipdate;
                    else cls.zipdate = DateTime.MinValue;
                    cls.zipuser = ste[8].ToString();
                    cls.cym = int.Parse(ste[9].ToString());

                    lstSte.Add(cls);

                }
                return lstSte;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// 表示データ
        /// </summary>
        private void InitDataTable()
        {
            dtDisp.Columns.Add("select");
            dtDisp.Columns.Add("scanid");
            dtDisp.Columns.Add("kind");
            dtDisp.Columns.Add("note1");
            dtDisp.Columns.Add("note2");
            //遅くて使えない　dtDisp.Columns.Add("filecount");
            dtDisp.Columns.Add("outdate");
            dtDisp.Columns.Add("outuser");
            dtDisp.Columns.Add("zipdate");
            dtDisp.Columns.Add("zipuser");


        }

        /// <summary>
        /// グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            ScanToExternal r = new ScanToExternal();
            dgv.Columns[nameof(r.sel)].Width = 50;

            dgv.Columns[nameof(r.sel)].HeaderText = "選択";
            dgv.Columns[nameof(r.scanid)].HeaderText = "スキャンID";
            dgv.Columns[nameof(r.apptype)].HeaderText = "種類";
            
            dgv.Columns[nameof(r.outdate)].HeaderText = "出力日";
            dgv.Columns[nameof(r.outuser)].HeaderText = "出力ユーザ";

            dgv.Columns[nameof(r.zipdate)].HeaderText = "書庫化日";
            dgv.Columns[nameof(r.zipuser)].HeaderText = "書庫化ユーザ";
            dgv.Columns[nameof(r.zipuser)].Width = 120;

            dgv.Columns[nameof(r.sel)].ReadOnly =    false;
            dgv.Columns[nameof(r.scanid)].ReadOnly = true;
            dgv.Columns[nameof(r.apptype)].ReadOnly =true;
            dgv.Columns[nameof(r.note1)].ReadOnly = true;
            dgv.Columns[nameof(r.note2)].ReadOnly = true;
            dgv.Columns[nameof(r.outdate)].ReadOnly =true;
            dgv.Columns[nameof(r.outuser)].ReadOnly = true;
            dgv.Columns[nameof(r.zipdate)].ReadOnly = true;
            dgv.Columns[nameof(r.zipuser)].ReadOnly = true;
            dgv.Columns[nameof(r.cym)].ReadOnly = true;

            dgv.Columns[nameof(r.cym)].Visible = false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 申請書種別コンボ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach(DataGridViewRow dr in dgv.Rows)
            {
                if (dr.Cells["apptype"].Value.ToString() == comboBoxKind.Text)
                {
                     dr.Cells["sel"].Value = checkBoxAll.Checked;                    
                }                
            }
        }


        /// <summary>
        /// 出力ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOutput_Click(object sender, EventArgs e)
        {
            ListScan.Clear();

            foreach(DataGridViewRow dr in dgv.Rows)
            {
                if ((bool)dr.Cells["sel"].Value == true) 
                {
                    if (int.TryParse(dr.Cells["scanid"].Value.ToString(), out int tmpsid))
                    {
                        ListScan.Add(Scan.Select(tmpsid));
                    }
                }
            }
            
            //this.Close();
        }


        /// <summary>
        /// scan_to_externalテーブル初期化
        /// </summary>
        /// <returns></returns>
        private bool InitTable()
        {
            //scanテーブルから、scan_to_externalに無いscanidだけを登録
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($" Create Table if not exists  ");
            sb.AppendLine($" scan_to_external( ");
            sb.AppendLine($" scanid int  not null  default 0 , ");
            sb.AppendLine($" apptype int  not null  default 0 , ");
            sb.AppendLine($" note1 varchar  not null  default '' , ");
            sb.AppendLine($" note2 varchar  not null  default '' , ");
            sb.AppendLine($" status int  not null  default 0 , ");
            sb.AppendLine($" outdate date  not null  default '0001-01-01' , ");
            sb.AppendLine($" outuser varchar  not null  default '' , ");
            sb.AppendLine($" zipdate date  not null  default '0001-01-01' , ");
            sb.AppendLine($" zipuser varchar  not null  default '' , ");
            sb.AppendLine($" cym int  not null  default 0 , primary key (scanid));comment on column scan_to_external.scanid IS 'scan.sid'; ");
            sb.AppendLine($" comment on column scan_to_external.apptype IS 'scan.apptype(APP_TYPE)'; ");
            sb.AppendLine($" comment on column scan_to_external.note1 IS 'scan.note1'; ");
            sb.AppendLine($" comment on column scan_to_external.note2 IS 'scan.note2'; ");
            sb.AppendLine($" comment on column scan_to_external.status IS 'scan.status(SCAN_STATUS)'; ");
            sb.AppendLine($" comment on column scan_to_external.outdate IS '出力日'; ");
            sb.AppendLine($" comment on column scan_to_external.outuser IS '出力ユーザ'; ");
            sb.AppendLine($" comment on column scan_to_external.zipdate IS 'マスキング済み画像zipしたか'; ");
            sb.AppendLine($" comment on column scan_to_external.zipuser IS 'マスキング済み画像zipしたユーザ'; ");
            sb.AppendLine($" comment on column scan_to_external.cym IS 'メホール請求年月'; ");

            //20220725213438 furukawa st ////////////////////////
            //インデックスが無ければ作成
            
            sb.AppendLine($" CREATE INDEX if not exists idx_scan_to_external_1 ON public.scan_to_external USING btree (apptype ASC NULLS LAST,cym ASC NULLS LAST)    TABLESPACE pg_default; ");
            //sb.AppendLine($" CREATE INDEX idx_scan_to_external_1 ON public.scan_to_external USING btree (apptype ASC NULLS LAST,cym ASC NULLS LAST)    TABLESPACE pg_default; ");
            //20220725213438 furukawa ed ////////////////////////



            sb.AppendLine($"insert into scan_to_external (scanid,apptype,note1,note2,status,cym) ");
            sb.AppendLine($"select sid,apptype,note1,note2,status,cym ");
            sb.AppendLine($"from scan ");
            sb.AppendLine($"on conflict (scanid) do nothing;");

            WaitForm wf = new WaitForm();
            DB.Command cmd = null;
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("テーブル初期化中...");
                cmd = DB.Main.CreateCmd(sb.ToString());

                if (!cmd.TryExecuteNonQuery()) return false;
                
                wf.LogPrint("テーブル初期化終了");
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                wf.Dispose();
            }
            
        }

        /// <summary>
        /// scan_to_externalの更新
        /// </summary>
        /// <param name="scanid"></param>
        /// <returns></returns>
        public bool Update(int scanid)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"update scan_to_external set ");
            sb.AppendLine($" outdate='{DateTime.Now.ToString("yyyy-MM-dd")}'");
            sb.AppendLine($",outuser='{User.CurrentUser.Name}'");
            sb.AppendLine($" where scanid={scanid}");
            

            DB.Command cmd = null;
            try
            {
                cmd = DB.Main.CreateCmd(sb.ToString());
                if (!cmd.TryExecuteNonQuery()) return false;

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// チェックを付けた行のスキャンIDをリストで取得
        /// </summary>
        /// <returns>スキャンIDのLIST</returns>
        public List<int> GetCheckedScanID()
        {
            List<int> retList = new List<int>();
            
            try
            {
                foreach (DataGridViewRow dr in dgv.Rows)
                {
                    if (bool.Parse(dr.Cells[nameof(ScanToExternal.sel)].Value.ToString()) == true)
                    {
                        retList.Add(int.Parse(dr.Cells[nameof(ScanToExternal.scanid)].Value.ToString()));
                    }
                }
                return retList;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                
            }
        }

        /// <summary>
        /// 複数行選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                foreach (DataGridViewRow dr in dgv.Rows)
                {
                    if (dr.Selected)
                    {
                        if (bool.Parse(dr.Cells[nameof(ScanToExternal.sel)].Value.ToString()) == true) dr.Cells[nameof(ScanToExternal.sel)].Value = false;
                        else dr.Cells[nameof(ScanToExternal.sel)].Value = true;
                    }
                }

            }
        }

       
    }

    /// <summary>
    /// 外部業者に出したスキャンのリスト
    /// </summary>
    public partial class ScanToExternal
    {
        public bool sel { get; set; } = false;
        
        [DB.DbAttribute.PrimaryKey]
        public int scanid { get;set; }=0;                               //scan.sid;
        public APP_TYPE apptype { get; set; } = 0;                      //scan.apptype;
        public string note1 { get; set; } = string.Empty;               //scan.note1;
        public string note2 { get; set; } = string.Empty;               //scan.note2;
        public SCAN_STATUS status { get; set; } = 0;                    //scan.status;
        public DateTime outdate { get; set; } = DateTime.MinValue;      //出力日;
        public string outuser { get; set; } = string.Empty;             //出力ユーザ;
        public DateTime zipdate { get; set; } = DateTime.MinValue;      //マスキング済み画像zipしたか;
        public string zipuser { get; set; } = string.Empty;             //マスキング済み画像zipしたユーザ;
        public int cym { get; set; } = 0;                               //メホール請求年月;
                                 
        /// <summary>
        /// zip化したscanをscan_to_externalから取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<ScanToExternal> getScanToExternalZipped(int cym)
        {
            List<ScanToExternal> retList = new List<ScanToExternal>();
            DB.Command cmd = null;
            try
            {
                StringBuilder sb = new StringBuilder();
                //zip化の最過去を1990年と想定しておく（Date.Minvalueでは-infinityとなって取れないかもなので）
                sb.AppendLine($"select * from scan_to_external where cym={cym} and zipdate>='1990-01-01' and zipuser<>''");
                cmd = DB.Main.CreateCmd(sb.ToString());
                var list=cmd.TryExecuteReaderList();
                foreach(var item in list)
                {
                    ScanToExternal ste = new ScanToExternal();
                    ste.scanid = int.Parse(item[0].ToString());
                    ste.apptype = (APP_TYPE)int.Parse(item[1].ToString());
                    ste.note1 = item[2].ToString();
                    ste.note2 = item[3].ToString();
                    ste.status = (SCAN_STATUS)int.Parse(item[4].ToString());
                    
                    if (DateTime.TryParse(item[5].ToString(), out DateTime tmpoutdate)) ste.outdate = tmpoutdate;
                    ste.outuser = item[6].ToString();
                    
                    if (DateTime.TryParse(item[7].ToString(), out DateTime tmpzipdate)) ste.zipdate = tmpzipdate;
                    ste.zipuser = item[8].ToString();
                    ste.cym = int.Parse(item[9].ToString());
                    retList.Add(ste);
                }
                return retList;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }


       
    }
}
