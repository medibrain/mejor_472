﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Mejor
{
    public partial class ExternalMenuForm : Form
    {
        int cym = 0;

        //20220829_2 ito ////// copyコマンドでトリガが立つようなので不要。
        ////20220816_2 ito //////
        //int appCnt = 0;

        //20220715142607 furukawa st ////////////////////////
        //zipパスワード        
        public static string strZipPassword="#Medi=1128#";
        //20220715142607 furukawa ed ////////////////////////


 
        public ExternalMenuForm(int _cym = 0)
        {
            InitializeComponent();
            CommonTool.setFormColor(this);
            cym = _cym;

            //20220225112938 furukawa st ////////////////////////
            //ユーザによって使えるボタンを制御する
            
            Sensitive();
            //20220225112938 furukawa ed ////////////////////////
        }


        /// <summary>
        /// 20220225112608 furukawa ユーザによって使えるボタンを制御する        
        /// </summary>
        private void Sensitive()
        {
            if (User.CurrentUser.belong == "メディブレーン")
            {
                if (!User.CurrentUser.Developer) groupBoxGaibu.Enabled = false;
            }
            if (User.CurrentUser.belong == "外部業者")
            {
                groupBoxMedi.Enabled = false;
            }
        }

        
        /// <summary>
        /// 20220216182138 furukawa マスク用画像ファイルをscanフォルダからコピー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTifcopy_Click(object sender, EventArgs e)
        {
            //20220704153922 furukawa st ////////////////////////
            //マスキング用画像選択画面
            
            SelectOutputImageForm frm = new SelectOutputImageForm(cym);
            if (frm.ShowDialog() == DialogResult.Cancel) return;            
            //20220704153922 furukawa ed ////////////////////////


            //選択している保険者のscanフォルダにある画像を出力する

            //出力先決定
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() == DialogResult.Cancel) return;
            string strOutFolder = dlg.Name;

            //画像登録後のフォルダからファイルをコピー
            string strDBName =Insurer.CurrrentInsurer.dbName;



            //20220704154002 furukawa st ////////////////////////
            //マスキング用画像選択画面で選択されていない場合抜ける
            
            var scanlist = frm.ListScan;
            if (scanlist.Count == 0)
            {
                MessageBox.Show($"選択されていません。終了します");
                return;
            }
            //      var scanlist = Scan.GetScanListYM(cym);
            //20220704154002 furukawa ed ////////////////////////



            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                //ここで拾っておかないとループ中に変わる
                string strNow = DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss");

                //スキャンIDフォルダをすべて取得し、中のファイルを全コピー
                foreach (Scan s in scanlist)
                {
                    //scanフォルダから出力フォルダへコピー
                    string strSrcPath = $"{App.GetImageFolderPath(DB.GetMainDBName())}\\{strDBName}\\{s.SID}";
                    string strDestPath = $"{strOutFolder}\\マスク用画像_{strNow}出力_{Insurer.CurrrentInsurer.InsurerName}_{s.AppType}\\{strDBName}\\{s.SID}";
                    if (!System.IO.Directory.Exists(strDestPath)) System.IO.Directory.CreateDirectory(strDestPath);

                    List<string> lstTif = System.IO.Directory.GetFiles(strSrcPath, "*.tif").ToList<string>();

                    wf.SetMax(lstTif.Count);


                    wf.InvokeValue = 0;


                    foreach (string strFile in lstTif)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf))
                        {
                            if (MessageBox.Show("画像コピーを中止しますか？",
                                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                return;
                        }

                        string strFileName = System.IO.Path.GetFileName(strFile);
                        System.IO.File.Copy(strFile, $"{strDestPath}\\{strFileName}");
                        wf.LogPrint($"{strFile} => {strDestPath}\\{strFileName}");
                        wf.InvokeValue++;
                    }
                    frm.Update(s.SID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message,Application.ProductName,
                    MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

            }
            finally
            {
                wf.Dispose();
            }



        }

        /// <summary>
        /// scangroup csv出力
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strSID"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool CreateScanGroupCSV(string strFileName, string strSID, WaitForm wf)
        {
            StringBuilder sbsql = new StringBuilder();
            DB.Command cmd = null;

            try
            {
                //2.出力用一時テーブルを作成し、そこにデータをコピー
                wf.LogPrint("scangroup一時テーブル作成");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"create table scangroup_forOutput as select * from scangroup where scanid in ({strSID})");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();


                //20220713162635 furukawa st ////////////////////////
                //テストデータの場合、ScanID以外を全て初期化して出力。本番データの場合は初期化せず全て出力                
                if (checkBoxTestData.Checked)
                {
                //20220713162635 furukawa ed ////////////////////////

                    //20220302170136 furukawa st ////////////////////////
                    //データの初期化（テストデータはすでにデータが入力済みのためステータスを戻す）

                    wf.LogPrint("scangroup一時テーブル初期化");
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($"update scangroup_forOutput set status={(int)GroupStatus.OCR済み}, ");
                    sbsql.AppendLine($"checkdate='0001-01-01', ");
                    sbsql.AppendLine($"inquirydate='0001-01-01' ");
                    sbsql.AppendLine($" where scanid in ({strSID})");
                    cmd = DB.Main.CreateCmd(sbsql.ToString());
                    cmd.TryExecuteNonQuery();
                    //20220302170136 furukawa ed ////////////////////////
                }



                //3.2のテーブルからbegintextexportで一気にcsv出力
                wf.LogPrint("scangroup一時テーブルからcsvファイル作成");
                StringBuilder sbCopyTo = new StringBuilder();
                sbCopyTo.AppendLine($"copy scangroup_forOutput TO STDOUT with csv FORCE QUOTE * ");


                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbCopyTo.ToString()))
                {
                    MessageBox.Show("scangroup csvファイル作成失敗", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                //4.2を削除
                wf.LogPrint("scangroup一時テーブル削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"drop table scangroup_forOutput");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();
                cmd.Dispose();
            }
        }

        /// <summary>
        /// scan csv出力
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strSID"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool CreateScanCSV(string strFileName, string strSID, WaitForm wf)
        {
            StringBuilder sbsql = new StringBuilder();
            DB.Command cmd = null;

            try
            {
                //2.出力用一時テーブルを作成し、そこにデータをコピー
                wf.LogPrint("scan一時テーブル作成");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"create table scan_forOutput as select * from scan where sid in ({strSID})");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();

                //20220713162747 furukawa st ////////////////////////
                //テストデータの場合、ScanID以外を全て初期化して出力。本番データの場合は初期化せず全て出力                
                if (checkBoxTestData.Checked)
                {
                //20220713162747 furukawa ed ////////////////////////

                    //20220302165739 furukawa st ////////////////////////
                    //データの初期化（テストデータはすでにデータが入力済みのためステータスを戻す）                
                    wf.LogPrint("scan一時データ初期化");
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($"update scan_forOutput set status={(int)SCAN_STATUS.OCR済み} where sid in ({strSID})");
                    cmd = DB.Main.CreateCmd(sbsql.ToString());
                    cmd.TryExecuteNonQuery();
                    //20220302165739 furukawa ed ////////////////////////

                }

                //3.2のテーブルからbegintextexportで一気にcsv出力
                wf.LogPrint("scan一時テーブルからcsvファイル作成");
                StringBuilder sbCopyTo = new StringBuilder();
                sbCopyTo.AppendLine($"copy scan_forOutput TO STDOUT with csv FORCE QUOTE * ");


                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbCopyTo.ToString()))
                {
                    MessageBox.Show("scan csvファイル作成失敗", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                //4.2を削除
                wf.LogPrint("scan一時テーブル削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"drop table scan_forOutput");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();
                cmd.Dispose();
            }
        }


        /// <summary>
        /// //20220302185654 furukawa APPLICATIONAUXにも最低限のレコードが必要        
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strSID"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool CreateApplicationAUXCSV(string strFileName, string strSID, WaitForm wf)
        {
            StringBuilder sbsql = new StringBuilder();
            DB.Command cmd = null;

            try
            {
                //2.出力用一時テーブルを作成し、そこにデータをコピー
                wf.LogPrint("application_aux一時テーブル作成");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"create table application_aux_forOutput as select * from application_aux where scanid in ({strSID})");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();

                //20220713162357 furukawa st ////////////////////////
                //テストデータの場合、AID以外を全て初期化して出力。本番データの場合は初期化せず全て出力                
                if (checkBoxTestData.Checked)
                {
                //20220713162357 furukawa ed ////////////////////////

                    //日付の初期化
                    wf.LogPrint("application_aux一時テーブル初期化");
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($"update application_aux_forOutput set ");
                    sbsql.AppendLine($"  matchingid01date='0001-01-01' ");
                    sbsql.AppendLine($" ,matchingid02date='0001-01-01' ");
                    sbsql.AppendLine($" ,matchingid03date='0001-01-01' ");
                    sbsql.AppendLine($" ,matchingid04date='0001-01-01' ");
                    sbsql.AppendLine($" ,matchingid05date='0001-01-01' ");
                    sbsql.AppendLine($" where scanid in ({strSID})");

                    cmd = DB.Main.CreateCmd(sbsql.ToString());
                    cmd.TryExecuteNonQuery();
                }

                //3.2のテーブルからbegintextexportで一気にcsv出力
                wf.LogPrint("application_aux一時テーブルからcsvファイル作成");
                StringBuilder sbCopyTo = new StringBuilder();
                sbCopyTo.AppendLine($"copy application_aux_forOutput TO STDOUT with csv FORCE QUOTE * ");


                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbCopyTo.ToString()))
                {
                    MessageBox.Show("application_aux csvファイル作成失敗", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                //4.2を削除
                wf.LogPrint("application_aux一時テーブル削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"drop table application_aux_forOutput");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();
                cmd.Dispose();
            }
        }


        /// <summary>
        /// application_csv作成
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strSID">必要なSIDカンマ区切り文字列</param>
        /// <param name="wf"></param>
        /// <param name="forExternal">外部業者向けフラグ</param>
        /// <returns></returns>
        private bool CreateApplicationCSV(string strFileName, string strSID, WaitForm wf, bool forExternal = true)
        {
            StringBuilder sbsql = new StringBuilder();
            DB.Command cmd = null;

            try
            {
                
                //2.出力用一時テーブルを作成し、そこにデータをコピー
                wf.LogPrint("Application一時テーブル作成");
                sbsql.Remove(0, sbsql.ToString().Length);     
                
                sbsql.AppendLine($"create table if not exists Application_forOutput as select * from application where scanid in ({strSID})");
                //sbsql.AppendLine($"create table Application_forOutput as select * from application where scanid in ({strSID})");
                
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();


                //20220302164333 furukawa st ////////////////////////
                //データの初期化（テストデータはすでにデータが入力済みのためそれを削除）
                if (forExternal)
                {
                    //本社から外部業者へ

                    //20220713162151 furukawa st ////////////////////////
                    //テストデータの場合、AID以外を全て初期化して出力。本番データの場合は初期化せず全て出力
                    
                    if (checkBoxTestData.Checked)
                    {
                        //20220713162151 furukawa ed ////////////////////////


                        wf.LogPrint("Application一時テーブルデータ初期化");
                        #region sql
                        sbsql.Remove(0, sbsql.ToString().Length);
                        sbsql.AppendLine(" UPDATE Application_forOutput ");
                        sbsql.AppendLine(" 	SET ");
                        sbsql.AppendLine("   ayear		            =0");
                        sbsql.AppendLine(" , amonth		            =0");
                        sbsql.AppendLine(" , inum		            =''");
                        sbsql.AppendLine(" , hnum		            =''");
                        sbsql.AppendLine(" , hpref		            =0");
                        sbsql.AppendLine(" , htype		            =0");
                        sbsql.AppendLine(" , hname		            =''");
                        sbsql.AppendLine(" , hzip		            =''");
                        sbsql.AppendLine(" , haddress	            =''");
                        sbsql.AppendLine(" , pname		            =''");
                        sbsql.AppendLine(" , psex		            =0");
                        sbsql.AppendLine(" , pbirthday	            ='0001-01-01'");
                        sbsql.AppendLine(" , asingle	            =0");
                        sbsql.AppendLine(" , afamily	            =0");
                        sbsql.AppendLine(" , aratio		            =0");
                        sbsql.AppendLine(" , publcexpense	        =''");
                        //sbsql.AppendLine(" --, emptytext1	        =''");
                        //sbsql.AppendLine(" --, emptyint1	        =0");
                        //sbsql.AppendLine(" --, emptytext2	        =''");
                        sbsql.AppendLine(" , ainspectdate	        ='0001-01-01'");
                        //sbsql.AppendLine(" --, emptyint2	        =0");
                        //sbsql.AppendLine(" --, emptyint3	        =0");
                        //sbsql.AppendLine(" --, aimagefile	        =''");
                        //sbsql.AppendLine(" --, emptytext3	        =''");
                        //sbsql.AppendLine(" , achargeyear	        =0");
                        //sbsql.AppendLine(" , achargemonth	        =0");

                        sbsql.AppendLine(" , iname1		            =''");
                        sbsql.AppendLine(" , idate1		            ='0001-01-01'");
                        sbsql.AppendLine(" , ifirstdate1            ='0001-01-01'");
                        sbsql.AppendLine(" , istartdate1            ='0001-01-01'");
                        sbsql.AppendLine(" , ifinishdate1		    ='0001-01-01'");
                        sbsql.AppendLine(" , idays1		            =0");
                        sbsql.AppendLine(" , icourse1	            =0");
                        sbsql.AppendLine(" , ifee1		            =0");
                        sbsql.AppendLine(" , iname2		            =''");
                        sbsql.AppendLine(" , idate2		            ='0001-01-01'");
                        sbsql.AppendLine(" , ifirstdate2            ='0001-01-01'");
                        sbsql.AppendLine(" , istartdate2            ='0001-01-01'");
                        sbsql.AppendLine(" , ifinishdate2		    ='0001-01-01'");
                        sbsql.AppendLine(" , idays2		            =0");
                        sbsql.AppendLine(" , icourse2	            =0");
                        sbsql.AppendLine(" , ifee2		            =0");
                        sbsql.AppendLine(" , iname3		            =''");
                        sbsql.AppendLine(" , idate3		            ='0001-01-01'");
                        sbsql.AppendLine(" , ifirstdate3            ='0001-01-01'");
                        sbsql.AppendLine(" , istartdate3            ='0001-01-01'");
                        sbsql.AppendLine(" , ifinishdate3		    ='0001-01-01'");
                        sbsql.AppendLine(" , idays3		            =0");
                        sbsql.AppendLine(" , icourse3	            =0");
                        sbsql.AppendLine(" , ifee3		            =0");
                        sbsql.AppendLine(" , iname4		            =''");
                        sbsql.AppendLine(" , idate4		            ='0001-01-01'");
                        sbsql.AppendLine(" , ifirstdate4            ='0001-01-01'");
                        sbsql.AppendLine(" , istartdate4            ='0001-01-01'");
                        sbsql.AppendLine(" , ifinishdate4		    ='0001-01-01'");
                        sbsql.AppendLine(" , idays4		            =0");
                        sbsql.AppendLine(" , icourse4	            =0");
                        sbsql.AppendLine(" , ifee4		            =0");
                        sbsql.AppendLine(" , iname5		            =''");
                        sbsql.AppendLine(" , idate5		            ='0001-01-01'");
                        sbsql.AppendLine(" , ifirstdate5            ='0001-01-01'");
                        sbsql.AppendLine(" , istartdate5            ='0001-01-01'");
                        sbsql.AppendLine(" , ifinishdate5		    ='0001-01-01'");
                        sbsql.AppendLine(" , idays5		            =0");
                        sbsql.AppendLine(" , icourse5	            =0");
                        sbsql.AppendLine(" , ifee5		            =0");

                        sbsql.AppendLine(" , fchargetype            =0");
                        sbsql.AppendLine(" , fdistance	            =0");
                        sbsql.AppendLine(" , fvisittimes            =0");
                        sbsql.AppendLine(" , fvisitfee	            =0");
                        sbsql.AppendLine(" , fvisitadd	            =0");

                        sbsql.AppendLine(" , sid		            =''");
                        sbsql.AppendLine(" , sregnumber	            =''");
                        sbsql.AppendLine(" , szip		            =''");
                        sbsql.AppendLine(" , saddress	            =''");
                        sbsql.AppendLine(" , sname		            =''");
                        sbsql.AppendLine(" , stel		            =''");
                        sbsql.AppendLine(" , sdoctor	            =''");
                        sbsql.AppendLine(" , skana		            =''");
                        sbsql.AppendLine(" , bacctype	            =0");
                        sbsql.AppendLine(" , bname		            =''");
                        sbsql.AppendLine(" , btype		            =0");
                        sbsql.AppendLine(" , bbranch	            =''");
                        sbsql.AppendLine(" , bbranchtype            =0");
                        sbsql.AppendLine(" , baccname	            =''");
                        sbsql.AppendLine(" , bkana		            =''");
                        sbsql.AppendLine(" , baccnumber	            =''");

                        sbsql.AppendLine(" , atotal		            =0");
                        sbsql.AppendLine(" , apartial	            =0");
                        sbsql.AppendLine(" , acharge	            =0");
                        sbsql.AppendLine(" , acounteddays		    =0");
                        sbsql.AppendLine(" , numbering		        =''");
                        //sbsql.AppendLine(" --, aapptype		=0");
                        sbsql.AppendLine(" , note		            =''");
                        sbsql.AppendLine(" , ufirst		            =0");
                        sbsql.AppendLine(" , usecond	        	=0");
                        sbsql.AppendLine(" , uinquiry	        	=0");
                        sbsql.AppendLine(" , bui		            =0");
                        //sbsql.AppendLine(" --, cym		=0");
                        sbsql.AppendLine(" , ym	            	   =0");
                        sbsql.AppendLine(" , statusflags		   =0");
                        sbsql.AppendLine(" , shokaireason		   =0");
                        sbsql.AppendLine(" , rrid	        	   =0");
                        sbsql.AppendLine(" , inspectreasons		   =0");
                        sbsql.AppendLine(" , additionaluid1		   =0");
                        sbsql.AppendLine(" , additionaluid2		   =0");
                        sbsql.AppendLine(" , memo_shokai		   =''");
                        sbsql.AppendLine(" , memo_inspect		   =''");
                        sbsql.AppendLine(" , memo		           =''");
                        sbsql.AppendLine(" , paycode	           =''");
                        sbsql.AppendLine(" , shokaicode	           =''");
                        sbsql.AppendLine(" , ocrdata	           =''");
                        sbsql.AppendLine(" , ufirstex	           =0");
                        sbsql.AppendLine(" , usecondex	           =0");
                        sbsql.AppendLine(" , kagoreasons           =0");
                        sbsql.AppendLine(" , saishinsareasons	   =0");
                        sbsql.AppendLine(" , henreireasons		   =0");
                        sbsql.AppendLine(" , taggeddatas		   =''");
                        sbsql.AppendLine(" , comnum		           =''");
                        sbsql.AppendLine(" , groupnum	           =''");
                        sbsql.AppendLine(" , outmemo	           =''");
                        sbsql.AppendLine(" , kagoreasons_xml	   =''");
                        sbsql.AppendLine($" where scanid in ({strSID})");
                        #endregion

                        cmd = DB.Main.CreateCmd(sbsql.ToString());
                        cmd.TryExecuteNonQuery();
                    }
                }
                else
                {   
                    //外部業者から本社へ
                    wf.LogPrint("Application一時テーブル日付データ初期化");
                    #region sql
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine(" UPDATE Application_forOutput ");
                    sbsql.AppendLine(" 	SET ");
                    sbsql.AppendLine("    pbirthday	      =     case when pbirthday	        ='-infinity' then '0001-01-01' else pbirthday	    end ");
                    sbsql.AppendLine(" ,  ainspectdate    =     case when ainspectdate      ='-infinity' then '0001-01-01' else ainspectdate    end ");
                    sbsql.AppendLine(" ,  idate1		  =     case when idate1		    ='-infinity' then '0001-01-01' else idate1		    end ");
                    sbsql.AppendLine(" ,  ifirstdate1     =     case when ifirstdate1       ='-infinity' then '0001-01-01' else ifirstdate1     end ");
                    sbsql.AppendLine(" ,  istartdate1     =     case when istartdate1       ='-infinity' then '0001-01-01' else istartdate1     end ");
                    sbsql.AppendLine(" ,  ifinishdate1    =     case when ifinishdate1      ='-infinity' then '0001-01-01' else ifinishdate1    end ");
                    sbsql.AppendLine(" ,  idate2		  =     case when idate2		    ='-infinity' then '0001-01-01' else idate2		    end ");
                    sbsql.AppendLine(" ,  ifirstdate2     =     case when ifirstdate2       ='-infinity' then '0001-01-01' else ifirstdate2     end ");
                    sbsql.AppendLine(" ,  istartdate2     =     case when istartdate2       ='-infinity' then '0001-01-01' else istartdate2     end ");
                    sbsql.AppendLine(" ,  ifinishdate2    =     case when ifinishdate2      ='-infinity' then '0001-01-01' else ifinishdate2    end ");
                    sbsql.AppendLine(" ,  idate3		  =     case when idate3		    ='-infinity' then '0001-01-01' else idate3		    end ");
                    sbsql.AppendLine(" ,  ifirstdate3     =     case when ifirstdate3       ='-infinity' then '0001-01-01' else ifirstdate3     end ");
                    sbsql.AppendLine(" ,  istartdate3     =     case when istartdate3       ='-infinity' then '0001-01-01' else istartdate3     end ");
                    sbsql.AppendLine(" ,  ifinishdate3    =     case when ifinishdate3      ='-infinity' then '0001-01-01' else ifinishdate3    end ");
                    sbsql.AppendLine(" ,  idate4		  =     case when idate4		    ='-infinity' then '0001-01-01' else idate4		    end ");
                    sbsql.AppendLine(" ,  ifirstdate4     =     case when ifirstdate4       ='-infinity' then '0001-01-01' else ifirstdate4     end ");
                    sbsql.AppendLine(" ,  istartdate4     =     case when istartdate4       ='-infinity' then '0001-01-01' else istartdate4     end ");
                    sbsql.AppendLine(" ,  ifinishdate4    =     case when ifinishdate4      ='-infinity' then '0001-01-01' else ifinishdate4    end ");
                    sbsql.AppendLine(" ,  idate5		  =     case when idate5		    ='-infinity' then '0001-01-01' else idate5		    end ");
                    sbsql.AppendLine(" ,  ifirstdate5     =     case when ifirstdate5       ='-infinity' then '0001-01-01' else ifirstdate5     end ");
                    sbsql.AppendLine(" ,  istartdate5     =     case when istartdate5       ='-infinity' then '0001-01-01' else istartdate5     end ");
                    sbsql.AppendLine(" ,  ifinishdate5    =     case when ifinishdate5      ='-infinity' then '0001-01-01' else ifinishdate5    end ");

                    sbsql.AppendLine($" where scanid in ({strSID})");
                    #endregion

                    cmd = DB.Main.CreateCmd(sbsql.ToString());
                    cmd.TryExecuteNonQuery();
                }
                //20220302164333 furukawa ed ////////////////////////


                //3.2のテーブルからbegintextexportで一気にcsv出力
                wf.LogPrint("Application一時テーブルからcsvファイル作成");
                StringBuilder sbCopyTo = new StringBuilder();
                sbCopyTo.AppendLine($"copy Application_forOutput TO STDOUT with csv FORCE QUOTE * ");


                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbCopyTo.ToString()))
                {
                    MessageBox.Show("Application csvファイル作成失敗", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;

                }

                //20220829_2 ito ////// copyコマンドでトリガが立つようなので不要。
                ////20220816_2 ito  st /////
                ////applicationをカウントして、バッチファイル作成時に代入する
                //StringBuilder sbAppCnt = new StringBuilder();
                //sbAppCnt.AppendLine($"SELECT count(*) FROM Application_forOutput;");
                //cmd = DB.Main.CreateCmd(sbAppCnt.ToString());
                //var l = cmd.TryExecuteReaderList();
                //foreach (var item in l)
                //{
                //    appCnt = Convert.ToInt32(item[0]);
                //}
                ////20220816_2 ito ed /////

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                //4.2を削除
                wf.LogPrint("Application一時テーブル削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"drop table Application_forOutput");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();
                cmd.Dispose();
            }

        }


        //20220714180318 furukawa st ////////////////////////
        //入力時にrefreceが必要な保険者用
        

        /// <summary>
        /// refrece_csv作成
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="cym">必要なSIDカンマ区切り文字列</param>
        /// <param name="wf"></param>
        /// <param name="forExternal">外部業者向けフラグ</param>
        /// <returns></returns>
        private bool CreateRefreceCSV(string strFileName, int cym, WaitForm wf, bool forExternal = true)
        {
            switch ((int)Insurer.CurrrentInsurer.InsurerID) 
            {
                case (int)InsurerID.OSAKA_KOIKI:
                    break;

                //上記以外の保険者は抜ける
                default:
                    return true;

            }



            StringBuilder sbsql = new StringBuilder();
            DB.Command cmd = null;

            try
            {

                //2.出力用一時テーブルを作成し、そこにデータをコピー
                wf.LogPrint("Refrece一時テーブル作成");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"create table Refrece_forOutput as select * from Refrece where cym= {cym}");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();



                //本社から外部業者へ
                if (forExternal)
                {
                                        
                    //テストデータの場合、rrID以外を全て初期化して出力。本番データの場合は初期化せず全て出力
                    if (checkBoxTestData.Checked)
                    {
                        wf.LogPrint("Refrece一時テーブルデータ初期化");
                        #region sql
                        sbsql.Remove(0, sbsql.ToString().Length);
                        sbsql.AppendLine($" UPDATE Refrece_forOutput ");
                        sbsql.AppendLine($" SET ");
                        sbsql.AppendLine($"  apptype=0 ");
                        sbsql.AppendLine($" , cym=0 ");
                        sbsql.AppendLine($" , mym=0 ");
                        sbsql.AppendLine($" , insnum='' ");
                        sbsql.AppendLine($" , num='' ");
                        sbsql.AppendLine($" , name='' ");
                        sbsql.AppendLine($" , days=0 ");
                        sbsql.AppendLine($" , total=0 ");
                        sbsql.AppendLine($" , charge=0 ");
                        sbsql.AppendLine($" , clinicnum='' ");
                        sbsql.AppendLine($" , clinicname='' ");
                        sbsql.AppendLine($" , drnum='' ");
                        sbsql.AppendLine($" , drname='' ");
                        sbsql.AppendLine($" , searchnum='' ");
                        sbsql.AppendLine($" , importid=0 ");
                        sbsql.AppendLine($" , aid=0 ");
                        sbsql.AppendLine($" , kana='' ");
                        sbsql.AppendLine($" , zip='' ");
                        sbsql.AppendLine($" , add='' ");
                        sbsql.AppendLine($" , birthday='0001-01-01' ");
                        sbsql.AppendLine($" , destname='' ");
                        sbsql.AppendLine($" , destkana='' ");
                        sbsql.AppendLine($" , destzip= '' ");
                        sbsql.AppendLine($" , destadd= '' ");
                        sbsql.AppendLine($" , family=0 ");
                        sbsql.AppendLine($" , ratio=0 ");
                        sbsql.AppendLine($" , groupcode='' ");
                        sbsql.AppendLine($" , insname='' ");
                        sbsql.AppendLine($" , startdate='0001-01-01' ");
                        sbsql.AppendLine($" , merge=false ");
                        sbsql.AppendLine($" , advance=false ");

                        #endregion

                        cmd = DB.Main.CreateCmd(sbsql.ToString());
                        cmd.TryExecuteNonQuery();
                    }
                }
                else
                {
                    //外部業者から本社へ
                    wf.LogPrint("Refrece一時テーブル日付データ初期化");
                    #region sql
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine(" UPDATE Refrece_forOutput ");
                    sbsql.AppendLine(" 	SET ");
                    sbsql.AppendLine("    birthday	    =     case when birthday      ='-infinity' then '0001-01-01' else birthday	    end ");
                    sbsql.AppendLine(" ,  startdate     =     case when startdate     ='-infinity' then '0001-01-01' else startdate     end ");
                    
                    
                    #endregion

                    cmd = DB.Main.CreateCmd(sbsql.ToString());
                    cmd.TryExecuteNonQuery();
                }
                


                //3.2のテーブルからbegintextexportで一気にcsv出力
                wf.LogPrint("Refrece一時テーブルからcsvファイル作成");
                StringBuilder sbCopyTo = new StringBuilder();
                sbCopyTo.AppendLine($"copy Refrece_forOutput TO STDOUT with csv FORCE QUOTE * ");


                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbCopyTo.ToString()))
                {
                    MessageBox.Show("Refrece csvファイル作成失敗", Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;

                }
                return true;



            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                //4.2を削除
                wf.LogPrint("Refrece一時テーブル削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"drop table Refrece_forOutput");
                cmd = DB.Main.CreateCmd(sbsql.ToString());
                cmd.TryExecuteNonQuery();
                cmd.Dispose();
            }

        }

        //20220714180318 furukawa ed ////////////////////////



        /// <summary>
        /// appcounterに仮レコードを入れないとcymが取れずにエラー落ちする
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool CreateAppCounterBatch(string strFileName,int cym,WaitForm wf)
        {
            StringBuilder sbBatch = new StringBuilder();
            
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("shift-jis"));

            try
            {
                sbBatch.Remove(0, sbBatch.ToString().Length);
          
                //psqlのパスワードは 外部業者サーバのpgpass.confに書いてある前提
                wf.LogPrint("AppCounter仮登録バッチ作成");
                sbBatch.AppendLine($"@rem {cym}");   //20220816_2 ito /////
                sbBatch.AppendLine($"@echo off");
                sbBatch.AppendLine($"cd /d c:\\program files\\postgresql\\11\\bin");

                //20220829_2 ito ////// 既にレコードがある場合は上書きしないように変更。また「いったん1を書込み→すぐに0に変更」していたが意味が無いと思われるため削除。
                //sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                //sbBatch.AppendLine($"\"insert into appcounter values ({cym},1);\"");//仮レコード1を入れないと画面に出ない
                //sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                //sbBatch.AppendLine($"\"update appcounter set counter=0 where cym={cym};\"");//戻さないと1件多くカウントすることになる
                sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                sbBatch.AppendLine($"\"INSERT INTO appcounter(cym, counter) select {cym}, 0 WHERE NOT EXISTS(SELECT cym FROM appcounter WHERE cym = {cym});\"");
                //20220829_2 ito //////

                //20220816_2 ito  st /////
                //大阪広域はパーティションテーブルを作成する必要あり
                if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.OSAKA_KOIKI)
                {
                    //20220829_2 ito ////// copyコマンドでトリガが立つようなので不要。
                    ////外部業者用取込はcopyのため、トリガファンクションappcounter_update()が反応しないため、最初からAppカウント数を代入する
                    ////また、もし該当CYMのレコードが存在すれば足し算する
                    ////↑足し算がふさわしいかは要検討。COPYコマンドによるCSVインポートを使用している間は問題無いか。COPYは重複するとロールバックして1個もINSERTされないため。
                    //sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    //sbBatch.AppendLine($"\"insert into appcounter values ({cym},{this.appCnt}) on conflict (cym) do update set counter = appcounter.counter + {this.appCnt};\"");

                    //1.scangroupフォルダ
                    //cymに含まれるscanIDの、scandateの一覧を取得
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"SELECT scandate FROM scan where cym = {cym} group by scandate;");
                    DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
                    try
                    {
                        var l = cmd.TryExecuteReaderList();

                        foreach (var item in l)
                        {
                            var scanDate = Convert.ToDateTime(item[0]);
                            sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                            sbBatch.AppendLine($"\"create table IF NOT EXISTS scangroup_{scanDate.ToString("yyyyMMdd")} partition of scangroup for values in ('{scanDate.ToString("yyyy-MM-dd")}');\"");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                          MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                    }

                    //2.applicationフォルダ
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"create table IF NOT EXISTS application_{cym} partition of application for values in ({cym});\"");

                    //20220829_2 ito st ////// copyコマンドでトリガが立つが、そもそもトリガを作成していなかったので追加。
                    //applicationパーティションテーブルにトリガを追加　（ScanForm.cs 行620）
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"DROP TRIGGER IF EXISTS trigger_appcounter_update ON public.application_{cym};\"");
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"CREATE TRIGGER trigger_appcounter_update " +
                        $"BEFORE INSERT OR DELETE OR UPDATE " +
                        $"ON public.application_{cym} " +
                        $"FOR EACH ROW " +
                        $"EXECUTE FUNCTION public.appcounter_update();\"");
                    //20220829_2 ito ed //////

                    //3.application_aux,scan,refreceフォルダ
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"create table IF NOT EXISTS application_aux_{cym} partition of application_aux for values in ({cym});\"");
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"create table IF NOT EXISTS scan_{cym} partition of scan for values in ({cym});\"");
                    sbBatch.Append($"psql -h localhost -p 5432 -U postgres -d {DB.GetMainDBName()} --command ");
                    sbBatch.AppendLine($"\"create table IF NOT EXISTS refrece_{cym} partition of refrece for values in ({cym});\"");
                }
                else
                {
                }
                //20220816_2 ito  ed /////

                sw.WriteLine(sbBatch.ToString());


                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                sw.Close();
            }
        }

       
        /// <summary>
        /// applicationテーブルをcsvに出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAppOut_Click(object sender, EventArgs e)
        {
            //出力先決定            
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            if (odd.ShowDialog() == DialogResult.Cancel) return;
            string strOutFolderName = odd.Name;

            string strInusurerName = Insurer.CurrrentInsurer.InsurerName;


            
            //var scanlist = Scan.GetScanListYM(cym);
            //=>マスキング画像出力したScanid(scan_to_external)だけを取ってこないと、その保険者全部を持ってくることになり、重複エラーの可能性大

            WaitForm wf = new WaitForm();
            

            try
            {
                //20220705161012 furukawa st ////////////////////////
                //処理分岐により削除

                //wf.LogPrint("ScanID取得");
                ////1.必要なscanidを取得
                //string strSID = string.Empty;
                //foreach (Scan s in scanlist)
                //{
                //    strSID += $"{s.SID.ToString()},";
                //}
                //strSID = strSID.Substring(0, strSID.Length - 1);
                //20220705161012 furukawa ed ////////////////////////


                //押したボタンによってファイル名を変えておく
                //本社側出力はApplication.csv,Scan.csv,ScanGroup.csv
                //外部業者側出力はApplication_external.csv
                Button b = (Button)sender;
                if (b.Name == "buttonAppOut")
                {
                    //1.必要なscanidを取得

                    //20220713185332 furukawa st ////////////////////////
                    //美祢市に送るスキャンIDを選択させる。画像出力でzip化したスキャンIDは基本選択している
                    
                    SelectOutputImageForm frm = new SelectOutputImageForm(cym,true);
                    if (frm.ShowDialog() == DialogResult.Cancel) return;
                    var scanlist=frm.GetCheckedScanID();
                    if(scanlist.Count==0)
                    {
                        MessageBox.Show($"出力する行が選択されていません。処理を中止します"
                            , Application.ProductName
                            , MessageBoxButtons.OK
                            , MessageBoxIcon.Information);
                        return;
                    }
                    
                    string strSID = string.Empty;
                    foreach (int sid in scanlist)
                    {
                        strSID +=$"{sid},";
                    }
                    strSID = strSID.Substring(0, strSID.Length - 1);


                    //      20220705160653 furukawa st ////////////////////////
                    //      zip化したScanフォルダ(美祢市に送る)のScanID取得
                    //      var scanlist = ScanToExternal.getScanToExternalZipped(cym);
                    //      wf.ShowDialogOtherTask();
                    //      wf.LogPrint("ScanID取得");
                    //      //1.必要なscanidを取得
                    //      string strSID = string.Empty;
                    //      foreach (ScanToExternal s in scanlist)
                    //      {
                    //          strSID += $"{s.scanid.ToString()},";
                    //      }
                    //      strSID = strSID.Substring(0, strSID.Length - 1);
                    //      20220705160653 furukawa ed ////////////////////////

                    //20220713185332 furukawa ed ////////////////////////
                    
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("外部業者用データ作成中");

                    string strCreateDate = DateTime.Now.ToString("yyyy-MM-dd_HHmmss");
                    string strOutputFolderName = $"{strOutFolderName}\\{strCreateDate}出力";
                    if (!System.IO.Directory.Exists($"{strOutputFolderName}"))
                        System.IO.Directory.CreateDirectory($"{strOutputFolderName}");

                    if (!CreateAppCounterBatch($"{strOutFolderName}\\外部業者用データ_{strInusurerName}_{strCreateDate}出力_AppCounter.bat", cym, wf)) return;
                    if (!CreateApplicationCSV($"{strOutputFolderName}\\外部業者用_Application.csv", strSID, wf)) return;

                    //20220302185547 furukawa st ////////////////////////
                    //APPLICATIONAUXも入れとかないとupdateできない                    
                    if (!CreateApplicationAUXCSV($"{strOutputFolderName}\\外部業者用_Application_AUX.csv", strSID, wf)) return;
                    //20220302185547 furukawa ed ////////////////////////

                    if (!CreateScanCSV($"{strOutputFolderName}\\外部業者用_Scan.csv", strSID, wf)) return;
                    if (!CreateScanGroupCSV($"{strOutputFolderName}\\外部業者用_ScanGroup.csv", strSID, wf)) return;

                    //提供データが必要な場合                 
                    if (!CreateRefreceCSV($"{strOutputFolderName}\\外部業者用_Refrece.csv",cym, wf)) return;
                    

                    //ファイルが多数あると漏れる恐れがあるのでzipにして固めておく
                    wf.LogPrint("外部業者用データZIP化中");
                    ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();

                    //20220715141441 furukawa st ////////////////////////
                    //zipパスワードを掛けておく                    
                    zip.Password=strZipPassword;
                    //20220715141441 furukawa ed ////////////////////////


                    zip.CreateZip($"{strOutFolderName}\\外部業者用データ_{strInusurerName}_{strCreateDate}出力.zip", $"{strOutputFolderName}", true, "csv");
                    System.IO.Directory.Delete($"{strOutputFolderName}", true);
                }
                else
                {
                    //20220705161557 furukawa st ////////////////////////
                    //処理分岐により追加
                    wf.ShowDialogOtherTask();
                    var scanlist = Scan.GetScanListYM(cym);

                    wf.LogPrint("ScanID取得");
                    //1.必要なscanidを取得
                    string strSID = string.Empty;
                    foreach (Scan s in scanlist)
                    {
                        strSID += $"{s.SID.ToString()},";
                    }
                    strSID = strSID.Substring(0, strSID.Length - 1);
                    //20220705161557 furukawa ed ////////////////////////

                    wf.LogPrint("本社向けデータ作成中");

                    //20220308164050 furukawa st ////////////////////////
                    //ファイル名で保険者名・年月がわかるように
                    
                    if (!CreateApplicationCSV($"{strOutFolderName}\\{cym}_{strInusurerName}_本社向け_Application_external.csv", strSID, wf,false)) return;
                    //if (!CreateApplicationCSV($"{strOutFolderName}\\本社向け_Application_external.csv", strSID, wf, false)) return;
                    //20220308164050 furukawa ed ////////////////////////

                    //20220715160210 furukawa st ////////////////////////
                    //外部業者から本社向けのrefrece作成
                    if (!CreateRefreceCSV($"{strOutFolderName}\\{cym}_{strInusurerName}_本社向け_refrece_external.csv", cym, wf, false)) return;
                    //20220715160210 furukawa ed ////////////////////////


                }

                wf.LogPrint("終了");

                //20220308124859 furukawa st ////////////////////////
                //他と統一するためメッセージ
                
                MessageBox.Show("終了");
                //20220308124859 furukawa ed ////////////////////////

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            finally
            {         
                wf.Dispose();
                
            }

        }


        /// <summary>
        /// Applicationのcsvファイルインポート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImportMediApp_Click(object sender, EventArgs e)
        {
            //1.ファイル選択
            //2.インポート         

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Remove(0, sb.ToString().Length);
                string strInusrerName = Insurer.CurrrentInsurer.InsurerName;

                //押したボタンによって取り込み先テーブルを変える

                Button b = (Button)sender;

                if (b.Name == "buttonImportMediApp")
                {
                    OpenFileDialog dlg = new OpenFileDialog();

                    //20220816_2 ito st ///// 大阪広域はパーティションテーブルを作成する必要があるため、バッチは必ず実行する
                    if (cym == 0 || Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.OSAKA_KOIKI )
                    //if (cym == 0)
                    //20220816_2 ito ed /////
                    {

                        dlg.Filter = "バッチファイル|*.bat";
                        if (dlg.ShowDialog() == DialogResult.Cancel) return;
                        System.Diagnostics.Process p = new System.Diagnostics.Process();
                        p.StartInfo.FileName = dlg.FileName;

                        p.StartInfo.UseShellExecute = true;
                        p.StartInfo.CreateNoWindow = false;

                        p.Start();
                        p.WaitForExit();
                        p.Close();

                        //20220816_2 ito st /////
                        StreamReader sr = new StreamReader(dlg.FileName);
                        string str = sr.ReadLine();
                        if (str.Length >= 10) int.TryParse(str.Substring(str.Length - 6, 6), out cym); 
                        //20220816_2 ito end /////
                    }



                    //zipファイルを選択させる
                    dlg.Filter = "外部業者用zipファイル|*.zip";
                    if (dlg.ShowDialog() == DialogResult.Cancel) return;
                    string strFileName = dlg.FileName;
                    string strExtractFolderName = System.IO.Path.GetDirectoryName(strFileName);
                    
                    if(!strFileName.Contains(strInusrerName))
                    {
                        MessageBox.Show("取り込む保険者が間違っています。ファイル名と選択中の保険者名が一致しているか確認してください",
                            Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        return;
                    }


                    //解凍
                    ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                    
                    //20220715142655 furukawa st ////////////////////////
                    //解凍時のパスワード                    
                    zip.Password = strZipPassword;
                    //20220715142655 furukawa ed ////////////////////////

                    zip.ExtractZip(strFileName, strExtractFolderName, "csv");


                    //外部業者の取込はapplication,scan,scangroupの3つ

                    //strFileName = $"{strExtractFolderName}\\外部業者用_appcounter.csv";
                    //wf.LogPrint($"{strFileName}取込中");
                    //wf.LogPrint($"appcounter {cym}分インポート");

                    //sb.AppendLine($"copy appcounter from stdin with csv quote '\"' ");
                    //if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    //{
                    //    MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                    //        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //    return;
                    //}
                    //System.IO.File.Delete(strFileName);


                    //外部業者が本社のAppを取り込む場合はApplicationテーブルへ
                    strFileName = $"{strExtractFolderName}\\外部業者用_Application.csv";
                    wf.LogPrint($"{strFileName}取込中");
                    wf.LogPrint($"application {cym}分インポート");
                    sb.Clear();
                    sb.AppendLine($"copy application from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    System.IO.File.Delete(strFileName);


                    //20220302185857 furukawa st ////////////////////////
                    //APPLICATIONAUXも入れとかないとupdateできない                    
                    strFileName = $"{strExtractFolderName}\\外部業者用_Application_AUX.csv";
                    wf.LogPrint($"{strFileName}取込中");
                    wf.LogPrint($"application_aux {cym}分インポート");
                    sb.Clear();
                    sb.AppendLine($"copy application_aux from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    System.IO.File.Delete(strFileName);
                    //20220302185857 furukawa ed ////////////////////////


                    //scan,scangroupも取込                                       
                    strFileName = $"{strExtractFolderName}\\外部業者用_Scan.csv";
                    wf.LogPrint($"{strFileName}取込中");
                    wf.LogPrint($"scan {cym}分インポート");
                    sb.Clear();
                    sb.AppendLine($"copy scan from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    System.IO.File.Delete(strFileName);

                    strFileName = $"{strExtractFolderName}\\外部業者用_ScanGroup.csv";
                    wf.LogPrint($"{strFileName}取込中");
                    wf.LogPrint($"scangroup {cym}分インポート");
                    sb.Clear();
                    sb.AppendLine($"copy scangroup from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    System.IO.File.Delete(strFileName);


                    //20220715155926 furukawa st ////////////////////////
                    //本社からのrefreceをインポート
                    
                    strFileName = $"{strExtractFolderName}\\外部業者用_refrece.csv";
                    wf.LogPrint($"{strFileName}取込中");
                    wf.LogPrint($"refrece {cym}分インポート");
                    sb.Clear();
                    sb.AppendLine($"copy refrece from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    System.IO.File.Delete(strFileName);
                    //20220715155926 furukawa ed ////////////////////////


                }
                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Filter = $"{cym}_{Insurer.CurrrentInsurer.InsurerName}_本社向け_Application_external_csvファイル|{cym}_{Insurer.CurrrentInsurer.InsurerName}_本社向け_Application_external.csv";
                    //dlg.Filter = "本社向け_Application_external_csvファイル|本社向け_Application_external.csv";
                    if (dlg.ShowDialog() == DialogResult.Cancel) return;

                    string strFileName = dlg.FileName;

                    wf.LogPrint($"{strFileName}取込中");

                    //本社が外部業者のAppを取り込む場合はApplication_external(比較用)テーブルへ


                    //先に削除 =>　2022/02/18　削除してしまうと、月1回しかインポートできないことになる
                    //運用上毎日インポートの可能性もあるので、その分は残しておかないとその月全体のレコードがあとから見れない
                    //ただcopyコマンドでは重複キーがあると全部弾かれるので、一旦別テーブルに入れてupdateかける？？

                    //wf.LogPrint($"application_external {cym}分削除");
                    //sb.Remove(0, sb.ToString().Length);
                    //sb.AppendLine($"delete from application_external where cym={cym};");
                    //using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                    //{
                    //    cmd.TryExecuteNonQuery();
                    //}



                    #region Application一時テーブルにインポート
                    wf.LogPrint($"application_external_tmp 作成");
                    sb.Remove(0, sb.ToString().Length);

                    //20220318114334 furukawa st ////////////////////////
                    //テーブルがあったら作らない
                    
                    sb.AppendLine($"create table if not exists application_external_tmp as table application with no data;");
                    sb.AppendLine($"truncate table application_external_tmp ;");
                    //sb.AppendLine($"create table application_external_tmp as table application with no data;");
                    //20220318114334 furukawa ed ////////////////////////

                    using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                    {
                        cmd.TryExecuteNonQuery();
                    }

                    
                    wf.LogPrint($"application_external_tmp {cym}分インポート");
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine($"copy application_external_tmp from stdin with csv quote '\"' ");
                    if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                    {
                        MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    #endregion

                    #region application_externalに追加
                    //AIDの重複しないレコードを追加
                    wf.LogPrint($"application_externalに追加");
                    sb.Remove(0, sb.ToString().Length);

                    sb.AppendLine($" insert into application_external (");
                    sb.AppendLine($" aid,scanid,groupid,ayear,amonth,inum,hnum,hpref,htype,hname,hzip,haddress,pname,psex,pbirthday,asingle,afamily,aratio,");
                    sb.AppendLine($" publcexpense,emptytext1,emptyint1,emptytext2,ainspectdate,emptyint2,emptyint3,aimagefile,emptytext3,achargeyear,achargemonth,");
                    sb.AppendLine($" iname1,idate1,ifirstdate1,istartdate1,ifinishdate1,idays1,icourse1,ifee1,");
                    sb.AppendLine($" iname2,idate2,ifirstdate2,istartdate2,ifinishdate2,idays2,icourse2,ifee2,");
                    sb.AppendLine($" iname3,idate3,ifirstdate3,istartdate3,ifinishdate3,idays3,icourse3,ifee3,");
                    sb.AppendLine($" iname4,idate4,ifirstdate4,istartdate4,ifinishdate4,idays4,icourse4,ifee4,");
                    sb.AppendLine($" iname5,idate5,ifirstdate5,istartdate5,ifinishdate5,idays5,icourse5,ifee5,");
                    sb.AppendLine($" fchargetype,fdistance,fvisittimes,fvisitfee,fvisitadd,");
                    sb.AppendLine($" sid,sregnumber,szip,saddress,sname,stel,sdoctor,skana,bacctype,bname,btype,bbranch,bbranchtype,baccname,bkana,baccnumber,");
                    sb.AppendLine($" atotal,apartial,acharge,acounteddays,numbering,aapptype,note,ufirst,usecond,uinquiry,bui,cym,ym,statusflags,");
                    sb.AppendLine($" shokaireason,rrid,inspectreasons,additionaluid1,additionaluid2,memo_shokai,memo_inspect,memo,paycode,shokaicode,ocrdata,");
                    sb.AppendLine($" ufirstex,usecondex,kagoreasons,saishinsareasons,henreireasons,taggeddatas,comnum,groupnum,outmemo,kagoreasons_xml, ");
                    sb.AppendLine($" importuserid,importdate ");
                    sb.AppendLine($" ) select ");
                    sb.AppendLine($" aid,scanid,groupid,ayear,amonth,inum,hnum,hpref,htype,hname,hzip,haddress,pname,psex,pbirthday,asingle,afamily,aratio,");
                    sb.AppendLine($" publcexpense,emptytext1,emptyint1,emptytext2,ainspectdate,emptyint2,emptyint3,aimagefile,emptytext3,achargeyear,achargemonth,");
                    sb.AppendLine($" iname1,idate1,ifirstdate1,istartdate1,ifinishdate1,idays1,icourse1,ifee1,");
                    sb.AppendLine($" iname2,idate2,ifirstdate2,istartdate2,ifinishdate2,idays2,icourse2,ifee2,");
                    sb.AppendLine($" iname3,idate3,ifirstdate3,istartdate3,ifinishdate3,idays3,icourse3,ifee3,");
                    sb.AppendLine($" iname4,idate4,ifirstdate4,istartdate4,ifinishdate4,idays4,icourse4,ifee4,");
                    sb.AppendLine($" iname5,idate5,ifirstdate5,istartdate5,ifinishdate5,idays5,icourse5,ifee5,");
                    sb.AppendLine($" fchargetype,fdistance,fvisittimes,fvisitfee,fvisitadd,");
                    sb.AppendLine($" sid,sregnumber,szip,saddress,sname,stel,sdoctor,skana,bacctype,bname,btype,bbranch,bbranchtype,baccname,bkana,baccnumber,");
                    sb.AppendLine($" atotal,apartial,acharge,acounteddays,numbering,aapptype,note,ufirst,usecond,uinquiry,bui,cym,ym,statusflags,");
                    sb.AppendLine($" shokaireason,rrid,inspectreasons,additionaluid1,additionaluid2,memo_shokai,memo_inspect,memo,paycode,shokaicode,ocrdata,");
                    sb.AppendLine($" ufirstex,usecondex,kagoreasons,saishinsareasons,henreireasons,taggeddatas,comnum,groupnum,outmemo,kagoreasons_xml ");

                    //20220318112642 furukawa st ////////////////////////
                    //カンマ抜け
                    sb.AppendLine($" ,{User.CurrentUser.UserID},'{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}' ");
                                                                                                                  
                    //sb.AppendLine($" {User.CurrentUser.UserID},{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} ");
                    //20220318112642 furukawa ed ////////////////////////


                    sb.AppendLine($"  from application_external_tmp ");

                    sb.AppendLine($" where not exists (");
                    sb.AppendLine($" select aid from application_external where cym={cym} ) ");

                    //主キー重複の場合何もしない
                    sb.AppendLine($" on conflict on constraint application_external_pkey ");
                    sb.AppendLine($" do nothing ;");

                    #endregion


                    using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                    {
                        cmd.TryExecuteNonQuery();
                    }


                    //20220715190709 furukawa st ////////////////////////
                    //保険者別処理追加
                    
                    switch (Insurer.CurrrentInsurer.InsurerID)
                    {
                        case (int)InsurerID.OSAKA_KOIKI:

                            #region application更新
                            wf.LogPrint($"application更新");
                            sb.Clear();
                            sb.AppendLine($" UPDATE application");
                            sb.AppendLine($" SET");
                            sb.AppendLine($"  scanid=				 e.scanid,");
                            sb.AppendLine($"  groupid=               e.groupid,");
                            sb.AppendLine($"  ayear=                 e.ayear,");
                            sb.AppendLine($"  amonth=                e.amonth,");
                            sb.AppendLine($"  inum=                  e.inum,");
                            sb.AppendLine($"  hnum=                  e.hnum,");
                            sb.AppendLine($"  hpref=                 e.hpref,");
                            sb.AppendLine($"  htype=                 e.htype,");
                            sb.AppendLine($"  hname=                 e.hname,");
                            sb.AppendLine($"  hzip=                  e.hzip,");
                            sb.AppendLine($"  haddress=              e.haddress,");
                            sb.AppendLine($"  pname=                 e.pname,");
                            sb.AppendLine($"  psex=                  e.psex,");
                            sb.AppendLine($"  pbirthday=             e.pbirthday,");
                            sb.AppendLine($"  asingle=               e.asingle,");
                            sb.AppendLine($"  afamily=               e.afamily,");
                            sb.AppendLine($"  aratio=                e.aratio,");
                            sb.AppendLine($"  publcexpense=          e.publcexpense,");
                            sb.AppendLine($"  emptytext1=            e.emptytext1,");
                            sb.AppendLine($"  emptyint1=             e.emptyint1,");
                            sb.AppendLine($"  emptytext2=            e.emptytext2,");
                            sb.AppendLine($"  ainspectdate=          e.ainspectdate,");
                            sb.AppendLine($"  emptyint2=             e.emptyint2,");
                            sb.AppendLine($"  emptyint3=             e.emptyint3,");
                            sb.AppendLine($"  aimagefile=            e.aimagefile,");
                            sb.AppendLine($"  emptytext3=            e.emptytext3,");
                            sb.AppendLine($"  achargeyear=           e.achargeyear,");
                            sb.AppendLine($"  achargemonth=          e.achargemonth,");
                            sb.AppendLine($"  iname1=                e.iname1,");
                            sb.AppendLine($"  idate1=                e.idate1,");
                            sb.AppendLine($"  ifirstdate1=           e.ifirstdate1,");
                            sb.AppendLine($"  istartdate1=           e.istartdate1,");
                            sb.AppendLine($"  ifinishdate1=          e.ifinishdate1,");
                            sb.AppendLine($"  idays1=                e.idays1,");
                            sb.AppendLine($"  icourse1=              e.icourse1,");
                            sb.AppendLine($"  ifee1=                 e.ifee1,");
                            sb.AppendLine($"  iname2=                e.iname2,");
                            sb.AppendLine($"  idate2=                e.idate2,");
                            sb.AppendLine($"  ifirstdate2=           e.ifirstdate2,");
                            sb.AppendLine($"  istartdate2=           e.istartdate2,");
                            sb.AppendLine($"  ifinishdate2=          e.ifinishdate2,");
                            sb.AppendLine($"  idays2=                e.idays2,");
                            sb.AppendLine($"  icourse2=              e.icourse2,");
                            sb.AppendLine($"  ifee2=                 e.ifee2,");
                            sb.AppendLine($"  iname3=                e.iname3,");
                            sb.AppendLine($"  idate3=                e.idate3,");
                            sb.AppendLine($"  ifirstdate3=           e.ifirstdate3,");
                            sb.AppendLine($"  istartdate3=           e.istartdate3,");
                            sb.AppendLine($"  ifinishdate3=          e.ifinishdate3,");
                            sb.AppendLine($"  idays3=                e.idays3,");
                            sb.AppendLine($"  icourse3=              e.icourse3,");
                            sb.AppendLine($"  ifee3=                 e.ifee3,");
                            sb.AppendLine($"  iname4=                e.iname4,");
                            sb.AppendLine($"  idate4=                e.idate4,");
                            sb.AppendLine($"  ifirstdate4=           e.ifirstdate4,");
                            sb.AppendLine($"  istartdate4=           e.istartdate4,");
                            sb.AppendLine($"  ifinishdate4=          e.ifinishdate4,");
                            sb.AppendLine($"  idays4=                e.idays4,");
                            sb.AppendLine($"  icourse4=              e.icourse4,");
                            sb.AppendLine($"  ifee4=                 e.ifee4,");
                            sb.AppendLine($"  iname5=                e.iname5,");
                            sb.AppendLine($"  idate5=                e.idate5,");
                            sb.AppendLine($"  ifirstdate5=           e.ifirstdate5,");
                            sb.AppendLine($"  istartdate5=           e.istartdate5,");
                            sb.AppendLine($"  ifinishdate5=          e.ifinishdate5,");
                            sb.AppendLine($"  idays5=                e.idays5,");
                            sb.AppendLine($"  icourse5=              e.icourse5,");
                            sb.AppendLine($"  ifee5=                 e.ifee5,");
                            sb.AppendLine($"  fchargetype=           e.fchargetype,");
                            sb.AppendLine($"  fdistance=             e.fdistance,");
                            sb.AppendLine($"  fvisittimes=           e.fvisittimes,");
                            sb.AppendLine($"  fvisitfee=             e.fvisitfee,");
                            sb.AppendLine($"  fvisitadd=             e.fvisitadd,");
                            sb.AppendLine($"  sid=                   e.sid,");
                            sb.AppendLine($"  sregnumber=            e.sregnumber,");
                            sb.AppendLine($"  szip=                  e.szip,");
                            sb.AppendLine($"  saddress=              e.saddress,");
                            sb.AppendLine($"  sname=                 e.sname,");
                            sb.AppendLine($"  stel=                  e.stel,");
                            sb.AppendLine($"  sdoctor=               e.sdoctor,");
                            sb.AppendLine($"  skana=                 e.skana,");
                            sb.AppendLine($"  bacctype=              e.bacctype,");
                            sb.AppendLine($"  bname=                 e.bname,");
                            sb.AppendLine($"  btype=                 e.btype,");
                            sb.AppendLine($"  bbranch=               e.bbranch,");
                            sb.AppendLine($"  bbranchtype=           e.bbranchtype,");
                            sb.AppendLine($"  baccname=              e.baccname,");
                            sb.AppendLine($"  bkana=                 e.bkana,");
                            sb.AppendLine($"  baccnumber=            e.baccnumber,");
                            sb.AppendLine($"  atotal=                e.atotal,");
                            sb.AppendLine($"  apartial=              e.apartial,");
                            sb.AppendLine($"  acharge=               e.acharge,");
                            sb.AppendLine($"  acounteddays=          e.acounteddays,");
                            sb.AppendLine($"  numbering=             e.numbering,");
                            sb.AppendLine($"  aapptype=              e.aapptype,");
                            sb.AppendLine($"  note=                  e.note,");
                            sb.AppendLine($"  ufirst=                e.ufirst,");
                            sb.AppendLine($"  usecond=               e.usecond,");
                            sb.AppendLine($"  uinquiry=              e.uinquiry,");
                            sb.AppendLine($"  bui=                   e.bui,");
                            sb.AppendLine($"  cym=                   e.cym,");
                            sb.AppendLine($"  ym=                    e.ym,");
                            sb.AppendLine($"  statusflags=           e.statusflags,");
                            sb.AppendLine($"  shokaireason=          e.shokaireason,");
                            sb.AppendLine($"  rrid=                  e.rrid,");
                            sb.AppendLine($"  inspectreasons=        e.inspectreasons,");
                            sb.AppendLine($"  additionaluid1=        e.additionaluid1,");
                            sb.AppendLine($"  additionaluid2=        e.additionaluid2,");
                            sb.AppendLine($"  memo_shokai=           e.memo_shokai,");
                            sb.AppendLine($"  memo_inspect=          e.memo_inspect,");
                            sb.AppendLine($"  memo=                  e.memo,");
                            sb.AppendLine($"  paycode=               e.paycode,");
                            sb.AppendLine($"  shokaicode=            e.shokaicode,");
                            sb.AppendLine($"  ocrdata=               e.ocrdata,");
                            sb.AppendLine($"  ufirstex=              e.ufirstex,");
                            sb.AppendLine($"  usecondex=             e.usecondex,");
                            sb.AppendLine($"  kagoreasons=           e.kagoreasons,");
                            sb.AppendLine($"  saishinsareasons=      e.saishinsareasons,");
                            sb.AppendLine($"  henreireasons=         e.henreireasons,");
                            sb.AppendLine($"  taggeddatas=           e.taggeddatas,");
                            sb.AppendLine($"  comnum=                e.comnum,");
                            sb.AppendLine($"  groupnum=              e.groupnum,");
                            sb.AppendLine($"  outmemo=               e.outmemo,");
                            sb.AppendLine($"  kagoreasons_xml=       e.kagoreasons_xml");
                            sb.AppendLine($" from application_external e");
                            sb.AppendLine($" WHERE application.aid=e.aid");
                            sb.AppendLine($" and application.cym={cym};");  //202207291530 ito //
                            using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                            {
                                cmd.TryExecuteNonQuery();
                            }
                            #endregion

                            #region refrece一時テーブルにインポート

                            dlg = new OpenFileDialog();
                            dlg.Filter = $"{cym}_{Insurer.CurrrentInsurer.InsurerName}_本社向け_refrece_external_csvファイル|{cym}_{Insurer.CurrrentInsurer.InsurerName}_本社向け_refrece_external.csv";
                            if (dlg.ShowDialog() == DialogResult.Cancel) return;

                            strFileName = dlg.FileName;

                            wf.LogPrint($"{strFileName}取込中");
                            wf.LogPrint($"refrece_external_tmp 作成");

                            //テーブルがあったら作らない
                            sb.Clear();
                            sb.AppendLine($"create table if not exists refrece_external_tmp as table refrece with no data;");
                            sb.AppendLine($"truncate table refrece_external_tmp ;");

                            using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                            {
                                cmd.TryExecuteNonQuery();
                            }


                            wf.LogPrint($"refrece_external_tmp {cym}分インポート");
                            sb.Remove(0, sb.ToString().Length);
                            sb.AppendLine($"copy refrece_external_tmp from stdin with csv quote '\"' ");
                            if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
                            {
                                MessageBox.Show("csvファイルインポート失敗", Application.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                            #endregion

                            #region refreceを一時テーブルから更新
                            wf.LogPrint($"refrece更新");
                            sb.Clear();
                            sb.AppendLine($" UPDATE refrece");
                            sb.AppendLine($" SET ");
                            sb.AppendLine($"   apptype		=t.apptype	");
                            sb.AppendLine($" , cym			=t.cym		");
                            sb.AppendLine($" , mym			=t.mym		");
                            sb.AppendLine($" , insnum		=t.insnum	");
                            sb.AppendLine($" , num			=t.num		");
                            sb.AppendLine($" , name			=t.name		");
                            sb.AppendLine($" , days			=t.days		");
                            sb.AppendLine($" , total		=t.total		");
                            sb.AppendLine($" , charge		=t.charge	");
                            sb.AppendLine($" , clinicnum	=t.clinicnum	");
                            sb.AppendLine($" , clinicname	=t.clinicname");
                            sb.AppendLine($" , drnum		=t.drnum		");
                            sb.AppendLine($" , drname		=t.drname	");
                            sb.AppendLine($" , searchnum	=t.searchnum	");
                            sb.AppendLine($" , importid		=t.importid	");
                            sb.AppendLine($" , aid			=t.aid		");
                            sb.AppendLine($" , kana			=t.kana		");
                            sb.AppendLine($" , zip			=t.zip		");
                            sb.AppendLine($" , add			=t.add		");
                            sb.AppendLine($" , birthday		=t.birthday	");
                            sb.AppendLine($" , destname		=t.destname	");
                            sb.AppendLine($" , destkana		=t.destkana	");
                            sb.AppendLine($" , destzip		=t.destzip	");
                            sb.AppendLine($" , destadd		=t.destadd	");
                            sb.AppendLine($" , family		=t.family	");
                            sb.AppendLine($" , ratio		=t.ratio		");
                            sb.AppendLine($" , groupcode	=t.groupcode	");
                            sb.AppendLine($" , insname		=t.insname	");
                            sb.AppendLine($" , startdate	=t.startdate	");
                            sb.AppendLine($" , merge		=t.merge		");
                            sb.AppendLine($" , advance		=t.advance	");
                            sb.AppendLine($" from refrece_external_tmp t ");
                            sb.AppendLine($" WHERE refrece.rrid=t.rrid and refrece.cym=t.cym and refrece.cym={cym};");


                            using (DB.Command cmd = DB.Main.CreateCmd(sb.ToString()))
                            {
                                cmd.TryExecuteNonQuery();
                            }


                            #endregion

                            break;
                    }
                    //20220715190709 furukawa ed ////////////////////////

                }


                wf.LogPrint($"取込終了");
                MessageBox.Show("終了");

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 本社・外部業者比較処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonComp_Click(object sender, EventArgs e)
        {
            //compare_resultにapplication_external.aidを登録
            //applicationとapplication_externalを比較し不一致項目を1としてcompare_resultに登録
            //compare_reusltの項目をすべて足し算して0がOK,1以上がNG
            //NGのAID、GroupIDでcsvを作成する



            DB.Command cmd=null;
            DB.Transaction tran = new DB.Transaction(DB.Main);

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                StringBuilder sbsql = new StringBuilder();

                wf.LogPrint($"比較テーブル{cym}分削除");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"delete from compare_result where cym_chargeym={cym};");
                cmd = new DB.Command(sbsql.ToString(), tran);
                cmd.TryExecuteNonQuery();


                wf.LogPrint($"比較テーブルにAID登録");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"insert into compare_result (aid,cym_chargeym) select aid,cym from application where cym={cym};");
                cmd = new DB.Command(sbsql.ToString(), tran);
                cmd.TryExecuteNonQuery();


                wf.LogPrint($"比較処理");

                //20220318154833 furukawa st ////////////////////////
                //比較処理は保険者ごとの入力項目ごとになる
                
                switch (Insurer.CurrrentInsurer.InsurerID)
                {
                    case (int)InsurerID.GAKKO_01HOKKAIDO:
                    case (int)InsurerID.GAKKO_02AOMORI:
                    case (int)InsurerID.GAKKO_03IWATE:
                    case (int)InsurerID.GAKKO_04MIYAZAKI:
                    case (int)InsurerID.GAKKO_05AKITA:
                    case (int)InsurerID.GAKKO_06YAMAGATA:
                    case (int)InsurerID.GAKKO_07FUKUSHIMA:
                    case (int)InsurerID.GAKKO_08IBARAGI:
                    case (int)InsurerID.GAKKO_09TOCHIGI:
                    case (int)InsurerID.GAKKO_10GUNMA:
                    case (int)InsurerID.GAKKO_11SAITAMA:
                    case (int)InsurerID.GAKKO_12CHIBA:
                    case (int)InsurerID.GAKKO_13TOKYO:
                    case (int)InsurerID.GAKKO_14KANAGAWA:
                    case (int)InsurerID.GAKKO_15NIGATA:
                    case (int)InsurerID.GAKKO_16TOYAMA:
                    case (int)InsurerID.GAKKO_17ISHIKAWA:
                    case (int)InsurerID.GAKKO_18FUKUI:
                    case (int)InsurerID.GAKKO_19YAMANASHI:
                    case (int)InsurerID.GAKKO_20NAGANO:
                    case (int)InsurerID.GAKKO_21GIFU:
                    case (int)InsurerID.GAKKO_22SHIZUOKA:
                    case (int)InsurerID.GAKKO_23AICHI:
                    case (int)InsurerID.GAKKO_24MIE:
                    case (int)InsurerID.GAKKO_25SHIGA:
                    case (int)InsurerID.GAKKO_26KYOTO:
                    case (int)InsurerID.OSAKA_GAKKO:
                    case (int)InsurerID.GAKKO_28HYOGO:
                    case (int)InsurerID.GAKKO_29NARA:
                    case (int)InsurerID.GAKKO_30WAKAYAMA:
                    case (int)InsurerID.GAKKO_31TOTTORI:
                    case (int)InsurerID.GAKKO_32SHIMANE:
                    case (int)InsurerID.GAKKO_33OKAYAMA:
                    case (int)InsurerID.GAKKO_34HIROSHIMA:
                    case (int)InsurerID.GAKKO_35YAMAGUCHI:
                    case (int)InsurerID.GAKKO_36TOKUSHIMA:
                    case (int)InsurerID.GAKKO_37KAGAWA:
                    case (int)InsurerID.GAKKO_38EHIME:
                    case (int)InsurerID.GAKKO_39KOCHI:
                    case (int)InsurerID.GAKKO_40FUKUOKA:
                    case (int)InsurerID.GAKKO_41SAGA:
                    case (int)InsurerID.GAKKO_42NAGASAKI:
                    case (int)InsurerID.GAKKO_43KUMAMOTO:
                    case (int)InsurerID.GAKKO_44OITA:
                    case (int)InsurerID.GAKKO_45MIYAZAKI:
                    case (int)InsurerID.GAKKO_46KAGOSHIMA:
                    case (int)InsurerID.GAKKO_47OKINAWA:

                        #region 比較処理SQL_入力項目その他必要なデータだけにしたもの
                        sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"update compare_result set ");

                sbsql.AppendLine($" scanid 				= case when a.scanid 				= e.scanid then 0 else 1 end , ");
                sbsql.AppendLine($" groupid 			= case when a.groupid 				= e.groupid then 0 else 1 end , ");
                sbsql.AppendLine($" ayear 				= case when a.ayear 				= e.ayear then 0 else 1 end , ");
                sbsql.AppendLine($" amonth 				= case when a.amonth 				= e.amonth then 0 else 1 end , ");
                //sbsql.AppendLine($" inum 				= case when a.inum 					= e.inum then 0 else 1 end , ");
                sbsql.AppendLine($" hnum 				= case when a.hnum 					= e.hnum then 0 else 1 end , ");
                //sbsql.AppendLine($" hpref 				= case when a.hpref 				= e.hpref then 0 else 1 end , ");
                sbsql.AppendLine($" htype 				= case when a.htype 				= e.htype then 0 else 1 end , ");
                //sbsql.AppendLine($" hname 				= case when a.hname 				= e.hname then 0 else 1 end , ");
                //sbsql.AppendLine($" hzip 				= case when a.hzip 					= e.hzip then 0 else 1 end , ");
                //sbsql.AppendLine($" haddress 			= case when a.haddress 				= e.haddress then 0 else 1 end , ");
                //sbsql.AppendLine($" pname 				= case when a.pname 				= e.pname then 0 else 1 end , ");
                sbsql.AppendLine($" psex 				= case when a.psex 					= e.psex then 0 else 1 end , ");
                sbsql.AppendLine($" pbirthday 			= case when a.pbirthday 			= e.pbirthday then 0 else 1 end , ");
                //sbsql.AppendLine($" asingle 			= case when a.asingle 				= e.asingle then 0 else 1 end , ");
                sbsql.AppendLine($" afamily 			= case when a.afamily 				= e.afamily then 0 else 1 end , ");
                //sbsql.AppendLine($" aratio 				= case when a.aratio 				= e.aratio then 0 else 1 end , ");
                //sbsql.AppendLine($" publcexpense 		= case when a.publcexpense 			= e.publcexpense then 0 else 1 end , ");
                //sbsql.AppendLine($" emptytext1 			= case when a.emptytext1 			= e.emptytext1 then 0 else 1 end , ");
                //sbsql.AppendLine($" acheckflag 			= case when a.acheckflag 			= e.acheckflag then 0 else 1 end , ");
                //sbsql.AppendLine($" emptytext2 			= case when a.emptytext2 			= e.emptytext2 then 0 else 1 end , ");
                //sbsql.AppendLine($" ainspectdate 		= case when a.ainspectdate 			= e.ainspectdate then 0 else 1 end , ");
                //sbsql.AppendLine($" ascore 				= case when a.ascore 				= e.ascore then 0 else 1 end , ");
                //sbsql.AppendLine($" ainquiry 			= case when a.ainquiry 				= e.ainquiry then 0 else 1 end , ");
                //sbsql.AppendLine($" aimagefile 			= case when a.aimagefile 			= e.aimagefile then 0 else 1 end , ");
                //sbsql.AppendLine($" emptytext3 			= case when a.emptytext3 			= e.emptytext3 then 0 else 1 end , ");
                sbsql.AppendLine($" achargeyear 		= case when a.achargeyear 			= e.achargeyear then 0 else 1 end , ");
                sbsql.AppendLine($" achargemonth 		= case when a.achargemonth 			= e.achargemonth then 0 else 1 end , ");
                sbsql.AppendLine($" iname1 				= case when a.iname1 				= e.iname1 then 0 else 1 end , ");
                //sbsql.AppendLine($" idate1 				= case when a.idate1 				= e.idate1 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifirstdate1 		= case when a.ifirstdate1 			= e.ifirstdate1 then 0 else 1 end , ");
                //sbsql.AppendLine($" istartdate1 		= case when a.istartdate1 			= e.istartdate1 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifinishdate1 		= case when a.ifinishdate1 			= e.ifinishdate1 then 0 else 1 end , ");
                //sbsql.AppendLine($" idays1 				= case when a.idays1 				= e.idays1 then 0 else 1 end , ");
                //sbsql.AppendLine($" icourse1 			= case when a.icourse1 				= e.icourse1 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifee1 				= case when a.ifee1 				= e.ifee1 then 0 else 1 end , ");
                sbsql.AppendLine($" iname2 				= case when a.iname2 				= e.iname2 then 0 else 1 end , ");
                //sbsql.AppendLine($" idate2 				= case when a.idate2 				= e.idate2 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifirstdate2 		= case when a.ifirstdate2 			= e.ifirstdate2 then 0 else 1 end , ");
                //sbsql.AppendLine($" istartdate2 		= case when a.istartdate2 			= e.istartdate2 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifinishdate2 		= case when a.ifinishdate2 			= e.ifinishdate2 then 0 else 1 end , ");
                //sbsql.AppendLine($" idays2 				= case when a.idays2 				= e.idays2 then 0 else 1 end , ");
                //sbsql.AppendLine($" icourse2 			= case when a.icourse2 				= e.icourse2 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifee2 				= case when a.ifee2 				= e.ifee2 then 0 else 1 end , ");
                sbsql.AppendLine($" iname3 				= case when a.iname3 				= e.iname3 then 0 else 1 end , ");
                //sbsql.AppendLine($" idate3 				= case when a.idate3 				= e.idate3 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifirstdate3 		= case when a.ifirstdate3 			= e.ifirstdate3 then 0 else 1 end , ");
                //sbsql.AppendLine($" istartdate3 		= case when a.istartdate3 			= e.istartdate3 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifinishdate3 		= case when a.ifinishdate3 			= e.ifinishdate3 then 0 else 1 end , ");
                //sbsql.AppendLine($" idays3 				= case when a.idays3 				= e.idays3 then 0 else 1 end , ");
                //sbsql.AppendLine($" icourse3 			= case when a.icourse3 				= e.icourse3 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifee3 				= case when a.ifee3 				= e.ifee3 then 0 else 1 end , ");
                sbsql.AppendLine($" iname4 				= case when a.iname4 				= e.iname4 then 0 else 1 end , ");
                //sbsql.AppendLine($" idate4 				= case when a.idate4 				= e.idate4 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifirstdate4 		= case when a.ifirstdate4 			= e.ifirstdate4 then 0 else 1 end , ");
                //sbsql.AppendLine($" istartdate4 		= case when a.istartdate4 			= e.istartdate4 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifinishdate4 		= case when a.ifinishdate4 			= e.ifinishdate4 then 0 else 1 end , ");
                //sbsql.AppendLine($" idays4 				= case when a.idays4 				= e.idays4 then 0 else 1 end , ");
                //sbsql.AppendLine($" icourse4 			= case when a.icourse4 				= e.icourse4 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifee4 				= case when a.ifee4 				= e.ifee4 then 0 else 1 end , ");
                sbsql.AppendLine($" iname5 				= case when a.iname5 				= e.iname5 then 0 else 1 end , ");
                //sbsql.AppendLine($" idate5 				= case when a.idate5 				= e.idate5 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifirstdate5 		= case when a.ifirstdate5 			= e.ifirstdate5 then 0 else 1 end , ");
                //sbsql.AppendLine($" istartdate5 		= case when a.istartdate5 			= e.istartdate5 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifinishdate5 		= case when a.ifinishdate5 			= e.ifinishdate5 then 0 else 1 end , ");
                //sbsql.AppendLine($" idays5 				= case when a.idays5 				= e.idays5 then 0 else 1 end , ");
                //sbsql.AppendLine($" icourse5 			= case when a.icourse5 				= e.icourse5 then 0 else 1 end , ");
                //sbsql.AppendLine($" ifee5 				= case when a.ifee5 				= e.ifee5 then 0 else 1 end , ");
                //sbsql.AppendLine($" fchargetype 		= case when a.fchargetype 			= e.fchargetype then 0 else 1 end , ");
                //sbsql.AppendLine($" fdistance 			= case when a.fdistance 			= e.fdistance then 0 else 1 end , ");
                //sbsql.AppendLine($" fvisittimes 		= case when a.fvisittimes 			= e.fvisittimes then 0 else 1 end , ");
                //sbsql.AppendLine($" fvisitfee 			= case when a.fvisitfee 			= e.fvisitfee then 0 else 1 end , ");
                //sbsql.AppendLine($" fvisitadd 			= case when a.fvisitadd 			= e.fvisitadd then 0 else 1 end , ");
                //sbsql.AppendLine($" fmetal 				= case when a.fmetal 				= e.fmetal then 0 else 1 end , ");
                //sbsql.AppendLine($" finfo 				= case when a.finfo 				= e.finfo then 0 else 1 end , ");
                //sbsql.AppendLine($" fsubtotal 			= case when a.fsubtotal 			= e.fsubtotal then 0 else 1 end , ");
                sbsql.AppendLine($" sid 				= case when a.sid 					= e.sid then 0 else 1 end , ");
                sbsql.AppendLine($" sregnumber 			= case when a.sregnumber 			= e.sregnumber then 0 else 1 end , ");
                //sbsql.AppendLine($" szip 				= case when a.szip 					= e.szip then 0 else 1 end , ");
                //sbsql.AppendLine($" saddress 			= case when a.saddress 				= e.saddress then 0 else 1 end , ");
                //sbsql.AppendLine($" sname 				= case when a.sname 				= e.sname then 0 else 1 end , ");
                //sbsql.AppendLine($" stel 				= case when a.stel 					= e.stel then 0 else 1 end , ");
                //sbsql.AppendLine($" sdoctor 			= case when a.sdoctor 				= e.sdoctor then 0 else 1 end , ");
                //sbsql.AppendLine($" skana 				= case when a.skana 				= e.skana then 0 else 1 end , ");
                //sbsql.AppendLine($" sentrust 			= case when a.sentrust 				= e.sentrust then 0 else 1 end , ");
                //sbsql.AppendLine($" bpaydiv 			= case when a.bpaydiv 				= e.bpaydiv then 0 else 1 end , ");
                //sbsql.AppendLine($" bacctype 			= case when a.bacctype 				= e.bacctype then 0 else 1 end , ");
                //sbsql.AppendLine($" bname 				= case when a.bname 				= e.bname then 0 else 1 end , ");
                //sbsql.AppendLine($" btype 				= case when a.btype 				= e.btype then 0 else 1 end , ");
                //sbsql.AppendLine($" bbranch 			= case when a.bbranch 				= e.bbranch then 0 else 1 end , ");
                //sbsql.AppendLine($" bbranchtype 		= case when a.bbranchtype 			= e.bbranchtype then 0 else 1 end , ");
                //sbsql.AppendLine($" baccname 			= case when a.baccname 				= e.baccname then 0 else 1 end , ");
                //sbsql.AppendLine($" bkana 				= case when a.bkana 				= e.bkana then 0 else 1 end , ");
                //sbsql.AppendLine($" baccnumber 			= case when a.baccnumber 			= e.baccnumber then 0 else 1 end , ");
                sbsql.AppendLine($" atotal 				= case when a.atotal 				= e.atotal then 0 else 1 end , ");
                sbsql.AppendLine($" apartial 			= case when a.apartial 				= e.apartial then 0 else 1 end , ");
                sbsql.AppendLine($" acharge 			= case when a.acharge 				= e.acharge then 0 else 1 end , ");
                sbsql.AppendLine($" acounteddays 		= case when a.acounteddays 			= e.acounteddays then 0 else 1 end , ");
                sbsql.AppendLine($" numbering 			= case when a.numbering 			= e.numbering then 0 else 1 end , ");
                sbsql.AppendLine($" aapptype 			= case when a.aapptype 				= e.aapptype then 0 else 1 end , ");
                //sbsql.AppendLine($" aall 				= case when a.aall 					= e.aall then 0 else 1 end , ");
                //sbsql.AppendLine($" note 				= case when a.note 	    			= e.note then 0 else 1 end , ");
                ////sbsql.AppendLine($" anote 				= case when a.anote 				= e.anote then 0 else 1 end , ");
                //sbsql.AppendLine($" ufirst 				= case when a.ufirst 				= e.ufirst then 0 else 1 end , ");
                //sbsql.AppendLine($" usecond 			= case when a.usecond 				= e.usecond then 0 else 1 end , ");
                //sbsql.AppendLine($" uinquiry 			= case when a.uinquiry 				= e.uinquiry then 0 else 1 end , ");
                //sbsql.AppendLine($" bui 				= case when a.bui 					= e.bui then 0 else 1 end , ");
                sbsql.AppendLine($" cym 				= case when a.cym 					= e.cym then 0 else 1 end , ");
                sbsql.AppendLine($" ym 					= case when a.ym 					= e.ym then 0 else 1 end , ");
                //sbsql.AppendLine($" statusflags 		= case when a.statusflags 			= e.statusflags then 0 else 1 end , ");
                //sbsql.AppendLine($" shokaireason 		= case when a.shokaireason 			= e.shokaireason then 0 else 1 end , ");
                //sbsql.AppendLine($" rrid 				= case when a.rrid 					= e.rrid then 0 else 1 end , ");
                //sbsql.AppendLine($" inspectreasons 		= case when a.inspectreasons 		= e.inspectreasons then 0 else 1 end , ");
                //sbsql.AppendLine($" additionaluid1 		= case when a.additionaluid1 		= e.additionaluid1 then 0 else 1 end , ");
                //sbsql.AppendLine($" additionaluid2 		= case when a.additionaluid2 		= e.additionaluid2 then 0 else 1 end , ");
                //sbsql.AppendLine($" memo_shokai 		= case when a.memo_shokai 			= e.memo_shokai then 0 else 1 end , ");
                //sbsql.AppendLine($" memo_inspect 		= case when a.memo_inspect 			= e.memo_inspect then 0 else 1 end , ");
                //sbsql.AppendLine($" memo 				= case when a.memo 					= e.memo then 0 else 1 end , ");
                //sbsql.AppendLine($" paycode 			= case when a.paycode 				= e.paycode then 0 else 1 end , ");
                //sbsql.AppendLine($" shokaicode 			= case when a.shokaicode 			= e.shokaicode then 0 else 1 end , ");
                //sbsql.AppendLine($" ocrdata 			= case when a.ocrdata 				= e.ocrdata then 0 else 1 end , ");
                //sbsql.AppendLine($" ufirstex 			= case when a.ufirstex 				= e.ufirstex then 0 else 1 end , ");
                //sbsql.AppendLine($" usecondex 			= case when a.usecondex 			= e.usecondex then 0 else 1 end , ");
                //sbsql.AppendLine($" kagoreasons 		= case when a.kagoreasons 			= e.kagoreasons then 0 else 1 end , ");
                //sbsql.AppendLine($" saishinsareasons 	= case when a.saishinsareasons 		= e.saishinsareasons then 0 else 1 end , ");
                //sbsql.AppendLine($" henreireasons 		= case when a.henreireasons 		= e.henreireasons then 0 else 1 end , ");
                sbsql.AppendLine($" taggeddatas 		= case when a.taggeddatas 			= e.taggeddatas then 0 else 1 end , ");
                //sbsql.AppendLine($" comnum 				= case when a.comnum 				= e.comnum then 0 else 1 end , ");
                //sbsql.AppendLine($" groupnum 			= case when a.groupnum 				= e.groupnum then 0 else 1 end , ");
                //sbsql.AppendLine($" outmemo 			= case when a.outmemo 				= e.outmemo then 0 else 1 end , ");
                //sbsql.AppendLine($" kagoreasons_xml 	= case when cast(coalesce(a.kagoreasons_xml,'') as text) 		= cast(coalesce(e.kagoreasons_xml,'') as text) then 0 else 1 end , ");


                sbsql.AppendLine($" comparedate 	= '{DateTime.Now.ToString("yyyy-MM-dd")}' ");


                sbsql.AppendLine($" from application a  ");
                sbsql.AppendLine($" inner join application_external e on ");
                sbsql.AppendLine($" a.aid=e.aid ");
                sbsql.AppendLine($" where a.cym={cym}  ");
                sbsql.AppendLine($" and compare_result.aid=e.aid ");
                        #endregion

                        #region 比較処理SQL_old 入力項目以外も入っているので、関係ないデータのマッチングも結果に反映されてた
                        //sbsql.Remove(0, sbsql.ToString().Length);
                        //sbsql.AppendLine($"update compare_result set ");

                        //sbsql.AppendLine($" scanid 				= case when a.scanid 				= e.scanid then 0 else 1 end , ");
                        //sbsql.AppendLine($" groupid 			= case when a.groupid 				= e.groupid then 0 else 1 end , ");
                        //sbsql.AppendLine($" ayear 				= case when a.ayear 				= e.ayear then 0 else 1 end , ");
                        //sbsql.AppendLine($" amonth 				= case when a.amonth 				= e.amonth then 0 else 1 end , ");
                        //sbsql.AppendLine($" inum 				= case when a.inum 					= e.inum then 0 else 1 end , ");
                        //sbsql.AppendLine($" hnum 				= case when a.hnum 					= e.hnum then 0 else 1 end , ");
                        //sbsql.AppendLine($" hpref 				= case when a.hpref 				= e.hpref then 0 else 1 end , ");
                        //sbsql.AppendLine($" htype 				= case when a.htype 				= e.htype then 0 else 1 end , ");
                        //sbsql.AppendLine($" hname 				= case when a.hname 				= e.hname then 0 else 1 end , ");
                        //sbsql.AppendLine($" hzip 				= case when a.hzip 					= e.hzip then 0 else 1 end , ");
                        //sbsql.AppendLine($" haddress 			= case when a.haddress 				= e.haddress then 0 else 1 end , ");
                        //sbsql.AppendLine($" pname 				= case when a.pname 				= e.pname then 0 else 1 end , ");
                        //sbsql.AppendLine($" psex 				= case when a.psex 					= e.psex then 0 else 1 end , ");
                        //sbsql.AppendLine($" pbirthday 			= case when a.pbirthday 			= e.pbirthday then 0 else 1 end , ");
                        //sbsql.AppendLine($" asingle 			= case when a.asingle 				= e.asingle then 0 else 1 end , ");
                        //sbsql.AppendLine($" afamily 			= case when a.afamily 				= e.afamily then 0 else 1 end , ");
                        //sbsql.AppendLine($" aratio 				= case when a.aratio 				= e.aratio then 0 else 1 end , ");
                        //sbsql.AppendLine($" publcexpense 		= case when a.publcexpense 			= e.publcexpense then 0 else 1 end , ");
                        //sbsql.AppendLine($" emptytext1 			= case when a.emptytext1 			= e.emptytext1 then 0 else 1 end , ");
                        ////sbsql.AppendLine($" acheckflag 			= case when a.acheckflag 			= e.acheckflag then 0 else 1 end , ");
                        //sbsql.AppendLine($" emptytext2 			= case when a.emptytext2 			= e.emptytext2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ainspectdate 		= case when a.ainspectdate 			= e.ainspectdate then 0 else 1 end , ");
                        ////sbsql.AppendLine($" ascore 				= case when a.ascore 				= e.ascore then 0 else 1 end , ");
                        ////sbsql.AppendLine($" ainquiry 			= case when a.ainquiry 				= e.ainquiry then 0 else 1 end , ");
                        //sbsql.AppendLine($" aimagefile 			= case when a.aimagefile 			= e.aimagefile then 0 else 1 end , ");
                        //sbsql.AppendLine($" emptytext3 			= case when a.emptytext3 			= e.emptytext3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" achargeyear 		= case when a.achargeyear 			= e.achargeyear then 0 else 1 end , ");
                        //sbsql.AppendLine($" achargemonth 		= case when a.achargemonth 			= e.achargemonth then 0 else 1 end , ");
                        //sbsql.AppendLine($" iname1 				= case when a.iname1 				= e.iname1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idate1 				= case when a.idate1 				= e.idate1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifirstdate1 		= case when a.ifirstdate1 			= e.ifirstdate1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" istartdate1 		= case when a.istartdate1 			= e.istartdate1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifinishdate1 		= case when a.ifinishdate1 			= e.ifinishdate1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idays1 				= case when a.idays1 				= e.idays1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" icourse1 			= case when a.icourse1 				= e.icourse1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifee1 				= case when a.ifee1 				= e.ifee1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" iname2 				= case when a.iname2 				= e.iname2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idate2 				= case when a.idate2 				= e.idate2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifirstdate2 		= case when a.ifirstdate2 			= e.ifirstdate2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" istartdate2 		= case when a.istartdate2 			= e.istartdate2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifinishdate2 		= case when a.ifinishdate2 			= e.ifinishdate2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idays2 				= case when a.idays2 				= e.idays2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" icourse2 			= case when a.icourse2 				= e.icourse2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifee2 				= case when a.ifee2 				= e.ifee2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" iname3 				= case when a.iname3 				= e.iname3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idate3 				= case when a.idate3 				= e.idate3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifirstdate3 		= case when a.ifirstdate3 			= e.ifirstdate3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" istartdate3 		= case when a.istartdate3 			= e.istartdate3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifinishdate3 		= case when a.ifinishdate3 			= e.ifinishdate3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idays3 				= case when a.idays3 				= e.idays3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" icourse3 			= case when a.icourse3 				= e.icourse3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifee3 				= case when a.ifee3 				= e.ifee3 then 0 else 1 end , ");
                        //sbsql.AppendLine($" iname4 				= case when a.iname4 				= e.iname4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idate4 				= case when a.idate4 				= e.idate4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifirstdate4 		= case when a.ifirstdate4 			= e.ifirstdate4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" istartdate4 		= case when a.istartdate4 			= e.istartdate4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifinishdate4 		= case when a.ifinishdate4 			= e.ifinishdate4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idays4 				= case when a.idays4 				= e.idays4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" icourse4 			= case when a.icourse4 				= e.icourse4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifee4 				= case when a.ifee4 				= e.ifee4 then 0 else 1 end , ");
                        //sbsql.AppendLine($" iname5 				= case when a.iname5 				= e.iname5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idate5 				= case when a.idate5 				= e.idate5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifirstdate5 		= case when a.ifirstdate5 			= e.ifirstdate5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" istartdate5 		= case when a.istartdate5 			= e.istartdate5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifinishdate5 		= case when a.ifinishdate5 			= e.ifinishdate5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" idays5 				= case when a.idays5 				= e.idays5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" icourse5 			= case when a.icourse5 				= e.icourse5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" ifee5 				= case when a.ifee5 				= e.ifee5 then 0 else 1 end , ");
                        //sbsql.AppendLine($" fchargetype 		= case when a.fchargetype 			= e.fchargetype then 0 else 1 end , ");
                        //sbsql.AppendLine($" fdistance 			= case when a.fdistance 			= e.fdistance then 0 else 1 end , ");
                        //sbsql.AppendLine($" fvisittimes 		= case when a.fvisittimes 			= e.fvisittimes then 0 else 1 end , ");
                        //sbsql.AppendLine($" fvisitfee 			= case when a.fvisitfee 			= e.fvisitfee then 0 else 1 end , ");
                        //sbsql.AppendLine($" fvisitadd 			= case when a.fvisitadd 			= e.fvisitadd then 0 else 1 end , ");
                        ////sbsql.AppendLine($" fmetal 				= case when a.fmetal 				= e.fmetal then 0 else 1 end , ");
                        ////sbsql.AppendLine($" finfo 				= case when a.finfo 				= e.finfo then 0 else 1 end , ");
                        ////sbsql.AppendLine($" fsubtotal 			= case when a.fsubtotal 			= e.fsubtotal then 0 else 1 end , ");
                        //sbsql.AppendLine($" sid 				= case when a.sid 					= e.sid then 0 else 1 end , ");
                        //sbsql.AppendLine($" sregnumber 			= case when a.sregnumber 			= e.sregnumber then 0 else 1 end , ");
                        //sbsql.AppendLine($" szip 				= case when a.szip 					= e.szip then 0 else 1 end , ");
                        //sbsql.AppendLine($" saddress 			= case when a.saddress 				= e.saddress then 0 else 1 end , ");
                        //sbsql.AppendLine($" sname 				= case when a.sname 				= e.sname then 0 else 1 end , ");
                        //sbsql.AppendLine($" stel 				= case when a.stel 					= e.stel then 0 else 1 end , ");
                        //sbsql.AppendLine($" sdoctor 			= case when a.sdoctor 				= e.sdoctor then 0 else 1 end , ");
                        //sbsql.AppendLine($" skana 				= case when a.skana 				= e.skana then 0 else 1 end , ");
                        ////sbsql.AppendLine($" sentrust 			= case when a.sentrust 				= e.sentrust then 0 else 1 end , ");
                        ////sbsql.AppendLine($" bpaydiv 			= case when a.bpaydiv 				= e.bpaydiv then 0 else 1 end , ");
                        //sbsql.AppendLine($" bacctype 			= case when a.bacctype 				= e.bacctype then 0 else 1 end , ");
                        //sbsql.AppendLine($" bname 				= case when a.bname 				= e.bname then 0 else 1 end , ");
                        //sbsql.AppendLine($" btype 				= case when a.btype 				= e.btype then 0 else 1 end , ");
                        //sbsql.AppendLine($" bbranch 			= case when a.bbranch 				= e.bbranch then 0 else 1 end , ");
                        //sbsql.AppendLine($" bbranchtype 		= case when a.bbranchtype 			= e.bbranchtype then 0 else 1 end , ");
                        //sbsql.AppendLine($" baccname 			= case when a.baccname 				= e.baccname then 0 else 1 end , ");
                        //sbsql.AppendLine($" bkana 				= case when a.bkana 				= e.bkana then 0 else 1 end , ");
                        //sbsql.AppendLine($" baccnumber 			= case when a.baccnumber 			= e.baccnumber then 0 else 1 end , ");
                        //sbsql.AppendLine($" atotal 				= case when a.atotal 				= e.atotal then 0 else 1 end , ");
                        //sbsql.AppendLine($" apartial 			= case when a.apartial 				= e.apartial then 0 else 1 end , ");
                        //sbsql.AppendLine($" acharge 			= case when a.acharge 				= e.acharge then 0 else 1 end , ");
                        //sbsql.AppendLine($" acounteddays 		= case when a.acounteddays 			= e.acounteddays then 0 else 1 end , ");
                        //sbsql.AppendLine($" numbering 			= case when a.numbering 			= e.numbering then 0 else 1 end , ");
                        //sbsql.AppendLine($" aapptype 			= case when a.aapptype 				= e.aapptype then 0 else 1 end , ");
                        ////sbsql.AppendLine($" aall 				= case when a.aall 					= e.aall then 0 else 1 end , ");
                        ////sbsql.AppendLine($" note 				= case when a.note 	    			= e.note then 0 else 1 end , ");
                        ////sbsql.AppendLine($" anote 				= case when a.anote 				= e.anote then 0 else 1 end , ");
                        //sbsql.AppendLine($" ufirst 				= case when a.ufirst 				= e.ufirst then 0 else 1 end , ");
                        //sbsql.AppendLine($" usecond 			= case when a.usecond 				= e.usecond then 0 else 1 end , ");
                        //sbsql.AppendLine($" uinquiry 			= case when a.uinquiry 				= e.uinquiry then 0 else 1 end , ");
                        //sbsql.AppendLine($" bui 				= case when a.bui 					= e.bui then 0 else 1 end , ");
                        //sbsql.AppendLine($" cym 				= case when a.cym 					= e.cym then 0 else 1 end , ");
                        //sbsql.AppendLine($" ym 					= case when a.ym 					= e.ym then 0 else 1 end , ");
                        //sbsql.AppendLine($" statusflags 		= case when a.statusflags 			= e.statusflags then 0 else 1 end , ");
                        //sbsql.AppendLine($" shokaireason 		= case when a.shokaireason 			= e.shokaireason then 0 else 1 end , ");
                        //sbsql.AppendLine($" rrid 				= case when a.rrid 					= e.rrid then 0 else 1 end , ");
                        //sbsql.AppendLine($" inspectreasons 		= case when a.inspectreasons 		= e.inspectreasons then 0 else 1 end , ");
                        //sbsql.AppendLine($" additionaluid1 		= case when a.additionaluid1 		= e.additionaluid1 then 0 else 1 end , ");
                        //sbsql.AppendLine($" additionaluid2 		= case when a.additionaluid2 		= e.additionaluid2 then 0 else 1 end , ");
                        //sbsql.AppendLine($" memo_shokai 		= case when a.memo_shokai 			= e.memo_shokai then 0 else 1 end , ");
                        //sbsql.AppendLine($" memo_inspect 		= case when a.memo_inspect 			= e.memo_inspect then 0 else 1 end , ");
                        //sbsql.AppendLine($" memo 				= case when a.memo 					= e.memo then 0 else 1 end , ");
                        //sbsql.AppendLine($" paycode 			= case when a.paycode 				= e.paycode then 0 else 1 end , ");
                        //sbsql.AppendLine($" shokaicode 			= case when a.shokaicode 			= e.shokaicode then 0 else 1 end , ");
                        //sbsql.AppendLine($" ocrdata 			= case when a.ocrdata 				= e.ocrdata then 0 else 1 end , ");
                        //sbsql.AppendLine($" ufirstex 			= case when a.ufirstex 				= e.ufirstex then 0 else 1 end , ");
                        //sbsql.AppendLine($" usecondex 			= case when a.usecondex 			= e.usecondex then 0 else 1 end , ");
                        //sbsql.AppendLine($" kagoreasons 		= case when a.kagoreasons 			= e.kagoreasons then 0 else 1 end , ");
                        //sbsql.AppendLine($" saishinsareasons 	= case when a.saishinsareasons 		= e.saishinsareasons then 0 else 1 end , ");
                        //sbsql.AppendLine($" henreireasons 		= case when a.henreireasons 		= e.henreireasons then 0 else 1 end , ");
                        //sbsql.AppendLine($" taggeddatas 		= case when a.taggeddatas 			= e.taggeddatas then 0 else 1 end , ");
                        //sbsql.AppendLine($" comnum 				= case when a.comnum 				= e.comnum then 0 else 1 end , ");
                        //sbsql.AppendLine($" groupnum 			= case when a.groupnum 				= e.groupnum then 0 else 1 end , ");
                        //sbsql.AppendLine($" outmemo 			= case when a.outmemo 				= e.outmemo then 0 else 1 end , ");
                        //sbsql.AppendLine($" kagoreasons_xml 	= case when cast(coalesce(a.kagoreasons_xml,'') as text) 		= cast(coalesce(e.kagoreasons_xml,'') as text) then 0 else 1 end , ");


                        //sbsql.AppendLine($" comparedate 	= '{DateTime.Now.ToString("yyyy-MM-dd")}' ");


                        //sbsql.AppendLine($" from application a  ");
                        //sbsql.AppendLine($" inner join application_external e on ");
                        //sbsql.AppendLine($" a.aid=e.aid ");
                        //sbsql.AppendLine($" where a.cym={cym}  ");
                        //sbsql.AppendLine($" and compare_result.aid=e.aid ");
                        #endregion
                        break;
                        
                }

                //20220318154833 furukawa ed ////////////////////////




                cmd = new DB.Command(sbsql.ToString(), tran);
                cmd.TryExecuteNonQuery();


                //全項目を足し算し、1以上がNGあり
                //0の場合はすべて一致

                wf.LogPrint($"比較結果登録");

                #region 比較結果SQL
                sbsql.Remove(0, sbsql.ToString().Length);

                sbsql.AppendLine($"update compare_result set ");
                sbsql.Append($" result = case when   ");
                sbsql.Append($" compare_result.scanid+  ");
                sbsql.Append($" compare_result.groupid+  ");
                sbsql.Append($" compare_result.ayear+  ");
                sbsql.Append($" compare_result.amonth+  ");
                sbsql.Append($" compare_result.inum+  ");
                sbsql.Append($" compare_result.hnum+  ");
                sbsql.Append($" compare_result.hpref+  ");
                sbsql.Append($" compare_result.htype+  ");
                sbsql.Append($" compare_result.hname+  ");
                sbsql.Append($" compare_result.hzip+  ");
                sbsql.Append($" compare_result.haddress+  ");
                sbsql.Append($" compare_result.pname+  ");
                sbsql.Append($" compare_result.psex+  ");
                sbsql.Append($" compare_result.pbirthday+  ");
                sbsql.Append($" compare_result.asingle+  ");
                sbsql.Append($" compare_result.afamily+  ");
                sbsql.Append($" compare_result.aratio+  ");
                sbsql.Append($" compare_result.publcexpense+  ");
                sbsql.Append($" compare_result.emptytext1+  ");
                //sbsql.Append($" acheckflag+  ");
                sbsql.Append($" compare_result.emptytext2+  ");
                //sbsql.Append($" ainspectdate+  ");
                //sbsql.Append($" ascore+  ");
                //sbsql.Append($" compare_result.ainquiry+  ");
                sbsql.Append($" compare_result.aimagefile+  ");
                sbsql.Append($" compare_result.emptytext3+  ");
                sbsql.Append($" compare_result.achargeyear+  ");
                sbsql.Append($" compare_result.achargemonth+  ");
                sbsql.Append($" compare_result.iname1+  ");
                sbsql.Append($" compare_result.idate1+  ");
                sbsql.Append($" compare_result.ifirstdate1+  ");
                sbsql.Append($" compare_result.istartdate1+  ");
                sbsql.Append($" compare_result.ifinishdate1+  ");
                sbsql.Append($" compare_result.idays1+  ");
                sbsql.Append($" compare_result.icourse1+  ");
                sbsql.Append($" compare_result.ifee1+  ");
                sbsql.Append($" compare_result.iname2+  ");
                sbsql.Append($" compare_result.idate2+  ");
                sbsql.Append($" compare_result.ifirstdate2+  ");
                sbsql.Append($" compare_result.istartdate2+  ");
                sbsql.Append($" compare_result.ifinishdate2+  ");
                sbsql.Append($" compare_result.idays2+  ");
                sbsql.Append($" compare_result.icourse2+  ");
                sbsql.Append($" compare_result.ifee2+  ");
                sbsql.Append($" compare_result.iname3+  ");
                sbsql.Append($" compare_result.idate3+  ");
                sbsql.Append($" compare_result.ifirstdate3+  ");
                sbsql.Append($" compare_result.istartdate3+  ");
                sbsql.Append($" compare_result.ifinishdate3+  ");
                sbsql.Append($" compare_result.idays3+  ");
                sbsql.Append($" compare_result.icourse3+  ");
                sbsql.Append($" compare_result.ifee3+  ");
                sbsql.Append($" compare_result.iname4+  ");
                sbsql.Append($" compare_result.idate4+  ");
                sbsql.Append($" compare_result.ifirstdate4+  ");
                sbsql.Append($" compare_result.istartdate4+  ");
                sbsql.Append($" compare_result.ifinishdate4+  ");
                sbsql.Append($" compare_result.idays4+  ");
                sbsql.Append($" compare_result.icourse4+  ");
                sbsql.Append($" compare_result.ifee4+  ");
                sbsql.Append($" compare_result.iname5+  ");
                sbsql.Append($" compare_result.idate5+  ");
                sbsql.Append($" compare_result.ifirstdate5+  ");
                sbsql.Append($" compare_result.istartdate5+  ");
                sbsql.Append($" compare_result.ifinishdate5+  ");
                sbsql.Append($" compare_result.idays5+  ");
                sbsql.Append($" compare_result.icourse5+  ");
                sbsql.Append($" compare_result.ifee5+  ");
                sbsql.Append($" compare_result.fchargetype+  ");
                sbsql.Append($" compare_result.fdistance+  ");
                sbsql.Append($" compare_result.fvisittimes+  ");
                sbsql.Append($" compare_result.fvisitfee+  ");
                sbsql.Append($" compare_result.fvisitadd+  ");
                //sbsql.Append($" fmetal+  ");
                //sbsql.Append($" finfo+  ");
                //sbsql.Append($" fsubtotal+  ");
                sbsql.Append($" compare_result.sid+  ");
                sbsql.Append($" compare_result.sregnumber+  ");
                sbsql.Append($" compare_result.szip+  ");
                sbsql.Append($" compare_result.saddress+  ");
                sbsql.Append($" compare_result.sname+  ");
                sbsql.Append($" compare_result.stel+  ");
                sbsql.Append($" compare_result.sdoctor+  ");
                sbsql.Append($" compare_result.skana+  ");
                //sbsql.Append($" sentrust+  ");
                //sbsql.Append($" bpaydiv+  ");
                sbsql.Append($" compare_result.bacctype+  ");
                sbsql.Append($" compare_result.bname+  ");
                sbsql.Append($" compare_result.btype+  ");
                sbsql.Append($" compare_result.bbranch+  ");
                sbsql.Append($" compare_result.bbranchtype+  ");
                sbsql.Append($" compare_result.baccname+  ");
                sbsql.Append($" compare_result.bkana+  ");
                sbsql.Append($" compare_result.baccnumber+  ");
                sbsql.Append($" compare_result.atotal+  ");
                sbsql.Append($" compare_result.apartial+  ");
                sbsql.Append($" compare_result.acharge+  ");
                sbsql.Append($" compare_result.acounteddays+  ");
                sbsql.Append($" compare_result.numbering+  ");
                sbsql.Append($" compare_result.aapptype+  ");
                //sbsql.Append($" aall+  ");
                //sbsql.Append($" note+  ");
                //sbsql.Append($" anote+  ");
                //sbsql.Append($" ufirst+  ");
                //sbsql.Append($" usecond+  ");
                //sbsql.Append($" uinquiry+  ");
                sbsql.Append($" compare_result.bui+  ");
                sbsql.Append($" compare_result.cym+  ");
                sbsql.Append($" compare_result.ym+  ");
                sbsql.Append($" compare_result.statusflags+  ");
                sbsql.Append($" compare_result.shokaireason+  ");
                sbsql.Append($" compare_result.rrid+  ");
                sbsql.Append($" compare_result.inspectreasons+  ");
                //sbsql.Append($" additionaluid1+  ");
                //sbsql.Append($" additionaluid2+  ");
                sbsql.Append($" compare_result.memo_shokai+  ");
                sbsql.Append($" compare_result.memo_inspect+  ");
                sbsql.Append($" compare_result.memo+  ");
                sbsql.Append($" compare_result.paycode+  ");
                sbsql.Append($" compare_result.shokaicode+  ");
                sbsql.Append($" compare_result.ocrdata+  ");
                //sbsql.Append($" ufirstex+  ");
                //sbsql.Append($" usecondex+  ");
                sbsql.Append($" compare_result.kagoreasons+  ");
                sbsql.Append($" compare_result.saishinsareasons+  ");
                sbsql.Append($" compare_result.henreireasons+  ");
                sbsql.Append($" compare_result.taggeddatas+  ");
                sbsql.Append($" compare_result.comnum+  ");
                sbsql.Append($" compare_result.groupnum+  ");
                sbsql.Append($" compare_result.outmemo+  ");
                sbsql.Append($" compare_result.kagoreasons_xml  ");
                sbsql.Append($" =0 then 0 else 1 end ");

                sbsql.AppendLine($" from application a  ");
                sbsql.AppendLine($" inner join application_external e on ");
                sbsql.AppendLine($" a.aid=e.aid ");
                sbsql.AppendLine($" where a.cym={cym}  ");
                sbsql.AppendLine($" and compare_result.aid=e.aid ");
                #endregion

                cmd = new DB.Command(sbsql.ToString(), tran);
                cmd.TryExecuteNonQuery();



                wf.LogPrint("比較結果CSVファイル作成");

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "比較結果CSVファイル作成";
                dlg.FileName = $"不一致リスト_{DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss")}.csv";
                if (dlg.ShowDialog() == DialogResult.Cancel) return;
                string strFileName = dlg.FileName;


                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($" copy (select a.groupid,a.aid from compare_result " +
                    $"inner join application a on a.aid=compare_result.aid " +
                    $"where result=1 and cym_chargeym={cym} order by aid) to stdout with csv header ");

                if (!CommonTool.CsvDirectExportFromTable(strFileName, sbsql.ToString()))
                {
                    MessageBox.Show("csvファイル作成失敗", Application.ProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }


                wf.LogPrint("終了");

                tran.Commit();
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                tran.Rollback();
            }
            finally
            {
                cmd.Dispose();
                wf.Dispose();

            }

        }


        /// <summary>
        /// zip化したScanフォルダ(美祢市に送った)のScanIDにzip化時刻を入れる
        /// </summary>
        /// <param name="strFolderName"></param>
        /// <returns></returns>
        private bool UpdateScanToExternalByZipFolder(string strFolderName)
        {

            DB.Transaction tran = DB.Main.CreateTransaction();
            
            try
            {
                
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(strFolderName);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($" update scan_to_external set ");
                sb.AppendLine($" zipdate='{DateTime.Now}'");
                sb.AppendLine($",zipuser='{User.CurrentUser.Name}'");
                sb.Append($" where scanid in (");
                foreach(System.IO.DirectoryInfo subdi in di.GetDirectories("*",System.IO.SearchOption.AllDirectories))
                {
                    if (int.TryParse(subdi.Name, out int tmpname)) sb.Append($"{tmpname},");
                }
                sb.Remove(sb.ToString().Length-1,1);
                sb.Append(");");

                using(DB.Command cmd = DB.Main.CreateCmd(sb.ToString(),tran)){
                    cmd.TryExecuteNonQuery();
                }

                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {

                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message
                    , Application.ProductName
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Exclamation);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }


        /// <summary>
        /// 本社側マスキング済み画像ファイルを外部業者向けにzip化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTifCompress_Click(object sender, EventArgs e)
        {
            //フォルダ指定
            //自己解凍書庫にする=>面倒なのでzipでいく
            //解凍先はローカルサーバのscanフォルダ固定=>解凍時（画像インポート時）に固定パスにしておく

            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() == DialogResult.Cancel) return;
            string strFolderName = dlg.Name;
         
            //ScanIDを取得しscan_to_externalにzip化した時刻を記載する。
            //その時刻を元にapplication,aux,scan等csvを必要なレコード分つくる

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "保存先選択";            
            sfd.Filter = "zipファイル|*.zip";
            sfd.DefaultExt = ".zip";
            sfd.FileName = $"外部業者取込画像_{Insurer.CurrrentInsurer.InsurerName}_{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力.zip";

            if (sfd.ShowDialog() == DialogResult.Cancel)
            {
                MessageBox.Show("中止します");
                return;
            }

            string strZipName = sfd.FileName;
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //20220705153142 furukawa st ////////////////////////
                //zip化したScanフォルダを記録する
                
                wf.LogPrint("画像zip化フォルダ記録中...");
                if (!UpdateScanToExternalByZipFolder(strFolderName))
                {
                    MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像zip化フォルダ記録に失敗しました"
                        , Application.ProductName
                        , MessageBoxButtons.OK
                        , MessageBoxIcon.Exclamation);
                    wf.Dispose();
                    return;
                }
                //20220705153142 furukawa ed ////////////////////////



                wf.LogPrint("画像zip化中");
                ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                zip.CompressionLevel = ICSharpCode.SharpZipLib.Zip.Compression.Deflater.CompressionLevel.DEFAULT_COMPRESSION;
                zip.CreateZip(strZipName, strFolderName, true, "tiff|tif");
                
                wf.LogPrint("画像zip化完了");
                MessageBox.Show("外部業者取込画像ZIP作成完了");


            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            finally
            {
                wf.Dispose();
            }
          
            
        }


        /// <summary>
        /// メディからの画像ファイルzipを外部業者が取り込む
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImportTif_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "取込画像zip選択";
            ofd.Filter = "外部業者取込画像ファイル|*.zip";

            if (ofd.ShowDialog() == DialogResult.Cancel) return;
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            string strZipName = ofd.FileName;
            string strTarget = $"c:\\scan";
            if(!strZipName.Contains(Insurer.CurrrentInsurer.InsurerName))
            {
                MessageBox.Show("取り込む保険者が間違っています。ファイル名と選択中の保険者名が一致しているか確認してください",
                             Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                wf.LogSave("画像取込中...");

                ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();                
                zip.ExtractZip(strZipName, strTarget, "tif");

                wf.LogSave("完了");
                MessageBox.Show("画像取込完了");
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                wf.Dispose();
            }

        }


        private void checkBoxResetEnable_CheckedChanged(object sender, EventArgs e)
        {
            buttonResetInputData.Enabled = checkBoxResetEnable.Checked;
        }
        
        
        /// <summary>
        /// 20220306093937 furukawa 入力したデータを入力していない状態に戻す
        /// </summary>
        /// <returns></returns>
        private bool InputDataReset()
        {

            //現在の保険者のCYMのスキャンリIDリスト取得し、リセット用条件とする
            string strInusurerName = Insurer.CurrrentInsurer.InsurerName;
            var scanlist = Scan.GetScanListYM(cym);

            string strSID = string.Empty;
            foreach (Scan s in scanlist)
            {
                strSID += $"{s.SID.ToString()},";
            }
            strSID = strSID.Substring(0, strSID.Length - 1);

            StringBuilder sbsql = new StringBuilder();

            DB.Command cmd = null;
            DB.Transaction tran = null;
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //入力データのリセット
                //application,application_aux,scangroupステータス
                tran = DB.Main.CreateTransaction();

                wf.LogPrint("Applicationテーブルデータ初期化");
                #region sql
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine(" UPDATE Application ");
                sbsql.AppendLine(" 	SET ");
                sbsql.AppendLine("   ayear		            =0");
                sbsql.AppendLine(" , amonth		            =0");
                sbsql.AppendLine(" , inum		            =''");
                sbsql.AppendLine(" , hnum		            =''");
                sbsql.AppendLine(" , hpref		            =0");
                sbsql.AppendLine(" , htype		            =0");
                sbsql.AppendLine(" , hname		            =''");
                sbsql.AppendLine(" , hzip		            =''");
                sbsql.AppendLine(" , haddress	            =''");
                sbsql.AppendLine(" , pname		            =''");
                sbsql.AppendLine(" , psex		            =0");
                sbsql.AppendLine(" , pbirthday	            ='0001-01-01'");
                sbsql.AppendLine(" , asingle	            =0");
                sbsql.AppendLine(" , afamily	            =0");
                sbsql.AppendLine(" , aratio		            =0");
                sbsql.AppendLine(" , publcexpense	        =''");
                //sbsql.AppendLine(" --, emptytext1	        =''");
                //sbsql.AppendLine(" --, emptyint1	        =0");
                //sbsql.AppendLine(" --, emptytext2	        =''");
                sbsql.AppendLine(" , ainspectdate	        ='0001-01-01'");
                //sbsql.AppendLine(" --, emptyint2	        =0");
                //sbsql.AppendLine(" --, emptyint3	        =0");
                //sbsql.AppendLine(" --, aimagefile	        =''");
                //sbsql.AppendLine(" --, emptytext3	        =''");
                //sbsql.AppendLine(" , achargeyear	        =0");
                //sbsql.AppendLine(" , achargemonth	        =0");

                sbsql.AppendLine(" , iname1		            =''");
                sbsql.AppendLine(" , idate1		            ='0001-01-01'");
                sbsql.AppendLine(" , ifirstdate1            ='0001-01-01'");
                sbsql.AppendLine(" , istartdate1            ='0001-01-01'");
                sbsql.AppendLine(" , ifinishdate1		    ='0001-01-01'");
                sbsql.AppendLine(" , idays1		            =0");
                sbsql.AppendLine(" , icourse1	            =0");
                sbsql.AppendLine(" , ifee1		            =0");
                sbsql.AppendLine(" , iname2		            =''");
                sbsql.AppendLine(" , idate2		            ='0001-01-01'");
                sbsql.AppendLine(" , ifirstdate2            ='0001-01-01'");
                sbsql.AppendLine(" , istartdate2            ='0001-01-01'");
                sbsql.AppendLine(" , ifinishdate2		    ='0001-01-01'");
                sbsql.AppendLine(" , idays2		            =0");
                sbsql.AppendLine(" , icourse2	            =0");
                sbsql.AppendLine(" , ifee2		            =0");
                sbsql.AppendLine(" , iname3		            =''");
                sbsql.AppendLine(" , idate3		            ='0001-01-01'");
                sbsql.AppendLine(" , ifirstdate3            ='0001-01-01'");
                sbsql.AppendLine(" , istartdate3            ='0001-01-01'");
                sbsql.AppendLine(" , ifinishdate3		    ='0001-01-01'");
                sbsql.AppendLine(" , idays3		            =0");
                sbsql.AppendLine(" , icourse3	            =0");
                sbsql.AppendLine(" , ifee3		            =0");
                sbsql.AppendLine(" , iname4		            =''");
                sbsql.AppendLine(" , idate4		            ='0001-01-01'");
                sbsql.AppendLine(" , ifirstdate4            ='0001-01-01'");
                sbsql.AppendLine(" , istartdate4            ='0001-01-01'");
                sbsql.AppendLine(" , ifinishdate4		    ='0001-01-01'");
                sbsql.AppendLine(" , idays4		            =0");
                sbsql.AppendLine(" , icourse4	            =0");
                sbsql.AppendLine(" , ifee4		            =0");
                sbsql.AppendLine(" , iname5		            =''");
                sbsql.AppendLine(" , idate5		            ='0001-01-01'");
                sbsql.AppendLine(" , ifirstdate5            ='0001-01-01'");
                sbsql.AppendLine(" , istartdate5            ='0001-01-01'");
                sbsql.AppendLine(" , ifinishdate5		    ='0001-01-01'");
                sbsql.AppendLine(" , idays5		            =0");
                sbsql.AppendLine(" , icourse5	            =0");
                sbsql.AppendLine(" , ifee5		            =0");

                sbsql.AppendLine(" , fchargetype            =0");
                sbsql.AppendLine(" , fdistance	            =0");
                sbsql.AppendLine(" , fvisittimes            =0");
                sbsql.AppendLine(" , fvisitfee	            =0");
                sbsql.AppendLine(" , fvisitadd	            =0");

                sbsql.AppendLine(" , sid		            =''");
                sbsql.AppendLine(" , sregnumber	            =''");
                sbsql.AppendLine(" , szip		            =''");
                sbsql.AppendLine(" , saddress	            =''");
                sbsql.AppendLine(" , sname		            =''");
                sbsql.AppendLine(" , stel		            =''");
                sbsql.AppendLine(" , sdoctor	            =''");
                sbsql.AppendLine(" , skana		            =''");
                sbsql.AppendLine(" , bacctype	            =0");
                sbsql.AppendLine(" , bname		            =''");
                sbsql.AppendLine(" , btype		            =0");
                sbsql.AppendLine(" , bbranch	            =''");
                sbsql.AppendLine(" , bbranchtype            =0");
                sbsql.AppendLine(" , baccname	            =''");
                sbsql.AppendLine(" , bkana		            =''");
                sbsql.AppendLine(" , baccnumber	            =''");

                sbsql.AppendLine(" , atotal		            =0");
                sbsql.AppendLine(" , apartial	            =0");
                sbsql.AppendLine(" , acharge	            =0");
                sbsql.AppendLine(" , acounteddays		    =0");
                sbsql.AppendLine(" , numbering		        =''");
                //sbsql.AppendLine(" --, aapptype		=0");
                sbsql.AppendLine(" , note		            =''");
                sbsql.AppendLine(" , ufirst		            =0");
                sbsql.AppendLine(" , usecond	        	=0");
                sbsql.AppendLine(" , uinquiry	        	=0");
                sbsql.AppendLine(" , bui		            =0");
                //sbsql.AppendLine(" --, cym		=0");
                sbsql.AppendLine(" , ym	            	   =0");
                sbsql.AppendLine(" , statusflags		   =0");
                sbsql.AppendLine(" , shokaireason		   =0");
                sbsql.AppendLine(" , rrid	        	   =0");
                sbsql.AppendLine(" , inspectreasons		   =0");
                sbsql.AppendLine(" , additionaluid1		   =0");
                sbsql.AppendLine(" , additionaluid2		   =0");
                sbsql.AppendLine(" , memo_shokai		   =''");
                sbsql.AppendLine(" , memo_inspect		   =''");
                sbsql.AppendLine(" , memo		           =''");
                sbsql.AppendLine(" , paycode	           =''");
                sbsql.AppendLine(" , shokaicode	           =''");
                sbsql.AppendLine(" , ocrdata	           =''");
                sbsql.AppendLine(" , ufirstex	           =0");
                sbsql.AppendLine(" , usecondex	           =0");
                sbsql.AppendLine(" , kagoreasons           =0");
                sbsql.AppendLine(" , saishinsareasons	   =0");
                sbsql.AppendLine(" , henreireasons		   =0");
                sbsql.AppendLine(" , taggeddatas		   =''");
                sbsql.AppendLine(" , comnum		           =''");
                sbsql.AppendLine(" , groupnum	           =''");
                sbsql.AppendLine(" , outmemo	           =''");
                sbsql.AppendLine(" , kagoreasons_xml	   =''");
                sbsql.AppendLine($" where scanid in ({strSID})");
                #endregion

                cmd = DB.Main.CreateCmd(sbsql.ToString(),tran);
                cmd.TryExecuteNonQuery();


                wf.LogPrint("application_auxテーブル初期化");
                #region sql
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"update application_aux set ");
                sbsql.AppendLine($"  matchingid01date='0001-01-01' ");
                sbsql.AppendLine($" ,matchingid02date='0001-01-01' ");
                sbsql.AppendLine($" ,matchingid03date='0001-01-01' ");
                sbsql.AppendLine($" ,matchingid04date='0001-01-01' ");
                sbsql.AppendLine($" ,matchingid05date='0001-01-01' ");
                sbsql.AppendLine($" where scanid in ({strSID})");
                #endregion

                cmd = DB.Main.CreateCmd(sbsql.ToString(),tran);
                cmd.TryExecuteNonQuery();


                wf.LogPrint("scanデータ初期化");
                #region sql
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"update scan set status={(int)SCAN_STATUS.OCR済み} where sid in ({strSID})");
                #endregion
                cmd = DB.Main.CreateCmd(sbsql.ToString(),tran);
                cmd.TryExecuteNonQuery();


                wf.LogPrint("scangroupテーブル初期化");
                #region sql
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"update scangroup set status={(int)GroupStatus.OCR済み}, ");
                sbsql.AppendLine($"checkdate='0001-01-01', ");
                sbsql.AppendLine($"inquirydate='0001-01-01' ");
                sbsql.AppendLine($" where scanid in ({strSID})");
                #endregion
                cmd = DB.Main.CreateCmd(sbsql.ToString(),tran);
                cmd.TryExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("完了");

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message, Application.ProductName,
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();

                //20220422134653 furukawa st ////////////////////////
                //waitform閉じる                
                wf.Dispose();
                //20220422134653 furukawa ed ////////////////////////
            }

        }

        /// <summary>
        /// 20220306094112 furukawa 入力したデータを入力していない状態に戻すボタンを有効にするチェックボックス
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonResetInputData_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("選択中のメホール請求年月の保険者の入力データをすべてリセット（削除）します。元に戻せません。宜しいですか？", Application.ProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            if (!InputDataReset()) return;
        }
    }
}
