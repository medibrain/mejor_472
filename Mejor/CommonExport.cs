﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    class CommonExport
    {
        public static void LongImageExport(int cym, string imgSaveDir)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint($"{cym.ToString("0000年00月")}の申請書を取得しています");

                var l = App.GetApps(cym);
                var appGroup = new List<App>();

                var fc = new TiffUtility.FastCopy();

                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.LogPrint("対象画像を抽出し、コピーしています");
                int count = 0;
                int imgCount = 0;

                try
                {
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        if (item.YM > 0)
                        {
                            if (appGroup.Count > 1 && appGroup[0].AppType != APP_TYPE.柔整)
                            {
                                //申請書と長期がある場合
                                count++;
                                foreach (var app in appGroup)
                                {
                                    imgCount++;
                                    try
                                    {
                                        var fileName = imgSaveDir + "\\" + app.Aid.ToString("000000000000") + ".tif";
                                        fc.FileCopy(app.GetImageFullPath(), fileName);
                                    }
                                    catch (Exception ex)
                                    {
                                        wf.LogPrint($"画像の保存に失敗しました:AID{app.Aid}\r\n{ex.Message}");
                                    }
                                }
                            }

                            appGroup.Clear();
                            appGroup.Add(item);
                        }

                        if ((APP_TYPE)item.YM == APP_TYPE.長期) appGroup.Add(item);
                    }
                }
                finally
                {
                    wf.LogPrint($"出力が終わりました\r\n申請書数:{count}\r\n画像数:{imgCount}");
                    wf.LogSave($"{imgSaveDir + "\\" + DateTime.Now.ToString("yyyyMMddHHmmss")}.log");
                }
            }
        }
    }
}
