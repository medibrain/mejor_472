﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Toyonakashi_covid19_shigai
{
    public partial class FormImport : Form
    {

        int cym=0;

        public FormImport(int _cym)
        {
            InitializeComponent();
            cym = _cym;
            dispKokuho();
            dispRefrece();
        }

        private void buttonCSV_Click(object sender, EventArgs e)
        {
            if (dataimport.GetCountCYM(cym) > 0)
            {
                if (MessageBox.Show("既にデータが存在します。上書きしますか？", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }

            dataimport.ImportMain(cym);

            dispKokuho();
            dispRefrece();
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            //再取込は上書き前提とする

            //if (dataimport_chargelist.GetCountCYM(cym) > 0)
            //{
            //    if (MessageBox.Show("既にデータが存在します。上書きしますか？", "",
            //        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            //}
            dataimport_chargelist.ImportMain(cym);
            //dispKokuho();
            //dispRefrece();
        }

        private void dispKokuho()
        {
            dataGridViewKokuho.DataSource=dataimport.GetDispCount();
            dataGridViewKokuho.Columns[0].HeaderText = "メホール請求年月";
            dataGridViewKokuho.Columns[1].HeaderText = "行数";
            dataGridViewKokuho.Columns[0].Width = 150;
        }


        private void dispRefrece()
        {
            dataGridViewRefrece.DataSource = dataimport_chargelist.GetDispCount();
            dataGridViewRefrece.Columns[0].HeaderText = "メホール請求年月";
            dataGridViewRefrece.Columns[1].HeaderText = "行数";
            dataGridViewRefrece.Columns[0].Width = 150;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataimport_chargelist.ImportMain(cym,true);
        }

        //20220303144541 furukawa st ////////////////////////
        //不要データ削除ボタン
        
        private void buttonDel_Click(object sender, EventArgs e)
        {            
            if (!int.TryParse(dataGridViewKokuho.CurrentRow.Cells[0].Value.ToString(), out cym)) return;
            dataimport.deleteCYM(cym);
            dispKokuho();

        }
        //20220303144541 furukawa ed ////////////////////////
    }
}
