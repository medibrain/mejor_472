﻿namespace Mejor.Toyonakashi_covid19_shigai
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelym = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.verifyBoxAID = new Mejor.VerifyBox();
            this.verifyBoxBatchNo = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.verifyBoxNOB = new Mejor.VerifyBox();
            this.verifyBoxDrHD = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxClinicName = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxHosNum = new Mejor.VerifyBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.verifyBoxZikan = new Mejor.VerifyBox();
            this.panelBarcode = new System.Windows.Forms.Panel();
            this.verifyBoxBarCode = new Mejor.VerifyBox();
            this.verifyBoxTicket = new Mejor.VerifyBox();
            this.verifyBoxInsNum = new Mejor.VerifyBox();
            this.verifyBoxTimes = new Mejor.VerifyBox();
            this.verifyBoxKenshu = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelInsNum = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.verifyBoxPossible = new Mejor.VerifyBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.verifyBoxKibou = new Mejor.VerifyBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.verifyBoxDrSign = new Mejor.VerifyBox();
            this.label57 = new System.Windows.Forms.Label();
            this.verifyBoxTaion = new Mejor.VerifyBox();
            this.verifyBox6down = new Mejor.VerifyBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.verifyBox2w = new Mejor.VerifyBox();
            this.verifyBoxSign = new Mejor.VerifyBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.verifyBoxDateSign = new Mejor.VerifyBox();
            this.verifyBoxZikangai = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panelSID = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.verifyBoxSesshuD = new Mejor.VerifyBox();
            this.verifyBoxSesshuY = new Mejor.VerifyBox();
            this.verifyBoxSesshuM = new Mejor.VerifyBox();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxHos = new Mejor.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxDr = new Mejor.VerifyBox();
            this.verifyBoxLot = new Mejor.VerifyBox();
            this.verifyBoxPlace = new Mejor.VerifyBox();
            this.verifyBoxMaker = new Mejor.VerifyBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.verifyBoxY6DK = new Mejor.VerifyBox();
            this.verifyBoxS6DK = new Mejor.VerifyBox();
            this.verifyBoxY6D = new Mejor.VerifyBox();
            this.verifyBoxY6UK = new Mejor.VerifyBox();
            this.verifyBoxS6D = new Mejor.VerifyBox();
            this.verifyBoxS6UK = new Mejor.VerifyBox();
            this.verifyBoxY6U = new Mejor.VerifyBox();
            this.verifyBoxS6U = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.labelocr = new System.Windows.Forms.Label();
            this.verifyBoxKind = new Mejor.VerifyBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelym.SuspendLayout();
            this.panelBarcode.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelSID.SuspendLayout();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 695);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(78, 22);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(143, 23);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(7, 20);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(29, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "券種";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(6, 23);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(29, 12);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            this.labelHs.Visible = false;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 695);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 695);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-625, 695);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 695);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelym);
            this.panelRight.Controls.Add(this.panelBarcode);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.panelSID);
            this.panelRight.Controls.Add(this.panelHeader);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.labelocr);
            this.panelRight.Controls.Add(this.verifyBoxKind);
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 722);
            this.panelRight.TabIndex = 0;
            // 
            // panelym
            // 
            this.panelym.Controls.Add(this.label12);
            this.panelym.Controls.Add(this.buttonAdd);
            this.panelym.Controls.Add(this.verifyBoxAID);
            this.panelym.Controls.Add(this.verifyBoxBatchNo);
            this.panelym.Controls.Add(this.label8);
            this.panelym.Controls.Add(this.verifyBoxNOB);
            this.panelym.Controls.Add(this.verifyBoxDrHD);
            this.panelym.Controls.Add(this.label3);
            this.panelym.Controls.Add(this.label61);
            this.panelym.Controls.Add(this.verifyBoxM);
            this.panelym.Controls.Add(this.verifyBoxClinicName);
            this.panelym.Controls.Add(this.label1);
            this.panelym.Controls.Add(this.labelM);
            this.panelym.Controls.Add(this.verifyBoxY);
            this.panelym.Controls.Add(this.verifyBoxHosNum);
            this.panelym.Controls.Add(this.label44);
            this.panelym.Controls.Add(this.labelHs);
            this.panelym.Controls.Add(this.label48);
            this.panelym.Controls.Add(this.labelYear);
            this.panelym.Controls.Add(this.label47);
            this.panelym.Controls.Add(this.verifyBoxZikan);
            this.panelym.Location = new System.Drawing.Point(165, 9);
            this.panelym.Name = "panelym";
            this.panelym.Size = new System.Drawing.Size(800, 111);
            this.panelym.TabIndex = 5;
            this.panelym.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(664, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 12);
            this.label12.TabIndex = 87;
            this.label12.Text = "AID";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(587, 44);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(70, 22);
            this.buttonAdd.TabIndex = 86;
            this.buttonAdd.TabStop = false;
            this.buttonAdd.Text = "画像紐付";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // verifyBoxAID
            // 
            this.verifyBoxAID.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAID.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAID.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxAID.Location = new System.Drawing.Point(693, 43);
            this.verifyBoxAID.MaxLength = 10;
            this.verifyBoxAID.Name = "verifyBoxAID";
            this.verifyBoxAID.NewLine = false;
            this.verifyBoxAID.Size = new System.Drawing.Size(98, 23);
            this.verifyBoxAID.TabIndex = 85;
            this.verifyBoxAID.TabStop = false;
            this.verifyBoxAID.Text = "1234567890";
            this.verifyBoxAID.TextV = "";
            // 
            // verifyBoxBatchNo
            // 
            this.verifyBoxBatchNo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBatchNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBatchNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBatchNo.Location = new System.Drawing.Point(611, 14);
            this.verifyBoxBatchNo.MaxLength = 24;
            this.verifyBoxBatchNo.Name = "verifyBoxBatchNo";
            this.verifyBoxBatchNo.NewLine = false;
            this.verifyBoxBatchNo.Size = new System.Drawing.Size(180, 23);
            this.verifyBoxBatchNo.TabIndex = 39;
            this.verifyBoxBatchNo.TabStop = false;
            this.verifyBoxBatchNo.Text = "123456-7890123456-7";
            this.verifyBoxBatchNo.TextV = "";
            this.verifyBoxBatchNo.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(542, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 24);
            this.label8.TabIndex = 23;
            this.label8.Text = "バッチ番号\r\n（自動生成）";
            // 
            // verifyBoxNOB
            // 
            this.verifyBoxNOB.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNOB.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNOB.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNOB.Location = new System.Drawing.Point(631, 79);
            this.verifyBoxNOB.MaxLength = 10;
            this.verifyBoxNOB.Name = "verifyBoxNOB";
            this.verifyBoxNOB.NewLine = false;
            this.verifyBoxNOB.Size = new System.Drawing.Size(58, 23);
            this.verifyBoxNOB.TabIndex = 12;
            this.verifyBoxNOB.Text = "1234";
            this.verifyBoxNOB.TextV = "";
            this.verifyBoxNOB.Visible = false;
            this.verifyBoxNOB.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxNOB.Validated += new System.EventHandler(this.verifyBoxNOB_Validated);
            // 
            // verifyBoxDrHD
            // 
            this.verifyBoxDrHD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrHD.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrHD.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrHD.Location = new System.Drawing.Point(100, 71);
            this.verifyBoxDrHD.MaxLength = 10;
            this.verifyBoxDrHD.Name = "verifyBoxDrHD";
            this.verifyBoxDrHD.NewLine = false;
            this.verifyBoxDrHD.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxDrHD.TabIndex = 40;
            this.verifyBoxDrHD.Text = "あいうえおかきくけこ";
            this.verifyBoxDrHD.TextV = "";
            this.verifyBoxDrHD.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 23;
            this.label3.Text = "医師名";
            this.label3.Visible = false;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(582, 85);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 12);
            this.label61.TabIndex = 23;
            this.label61.Text = "No B類";
            this.label61.Visible = false;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(104, 15);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxClinicName
            // 
            this.verifyBoxClinicName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicName.Location = new System.Drawing.Point(10, 60);
            this.verifyBoxClinicName.MaxLength = 255;
            this.verifyBoxClinicName.Multiline = true;
            this.verifyBoxClinicName.Name = "verifyBoxClinicName";
            this.verifyBoxClinicName.NewLine = false;
            this.verifyBoxClinicName.Size = new System.Drawing.Size(50, 37);
            this.verifyBoxClinicName.TabIndex = 35;
            this.verifyBoxClinicName.Text = "あいうえおかきくけこあいうえおかきくけこあいうえおかきくけこ";
            this.verifyBoxClinicName.TextV = "";
            this.verifyBoxClinicName.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "医療機関";
            this.label1.Visible = false;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(39, 15);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 0;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxHosNum
            // 
            this.verifyBoxHosNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHosNum.Location = new System.Drawing.Point(421, 17);
            this.verifyBoxHosNum.MaxLength = 10;
            this.verifyBoxHosNum.Name = "verifyBoxHosNum";
            this.verifyBoxHosNum.NewLine = false;
            this.verifyBoxHosNum.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxHosNum.TabIndex = 30;
            this.verifyBoxHosNum.Text = "1234567890";
            this.verifyBoxHosNum.TextV = "";
            this.verifyBoxHosNum.Validated += new System.EventHandler(this.verifyBoxHosNum_Validated);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(321, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(92, 12);
            this.label44.TabIndex = 23;
            this.label44.Text = "医療機関等コード\r\n";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label48.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label48.Location = new System.Drawing.Point(506, 58);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(66, 48);
            this.label48.TabIndex = 82;
            this.label48.Text = "1 : 通常\r\n2 : 時間外\r\n3 : 休日\r\n9 : 記載無し";
            this.label48.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(414, 77);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(53, 12);
            this.label47.TabIndex = 81;
            this.label47.Text = "時間外等";
            this.label47.Visible = false;
            // 
            // verifyBoxZikan
            // 
            this.verifyBoxZikan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZikan.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZikan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZikan.Location = new System.Drawing.Point(474, 66);
            this.verifyBoxZikan.MaxLength = 1;
            this.verifyBoxZikan.Name = "verifyBoxZikan";
            this.verifyBoxZikan.NewLine = false;
            this.verifyBoxZikan.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxZikan.TabIndex = 10;
            this.verifyBoxZikan.TextV = "";
            this.verifyBoxZikan.Visible = false;
            this.verifyBoxZikan.Enter += new System.EventHandler(this.item_Enter);
            // 
            // panelBarcode
            // 
            this.panelBarcode.Controls.Add(this.verifyBoxBarCode);
            this.panelBarcode.Controls.Add(this.verifyBoxTicket);
            this.panelBarcode.Controls.Add(this.verifyBoxInsNum);
            this.panelBarcode.Controls.Add(this.verifyBoxTimes);
            this.panelBarcode.Controls.Add(this.verifyBoxKenshu);
            this.panelBarcode.Controls.Add(this.label14);
            this.panelBarcode.Controls.Add(this.label2);
            this.panelBarcode.Controls.Add(this.label6);
            this.panelBarcode.Controls.Add(this.label4);
            this.panelBarcode.Controls.Add(this.labelInsNum);
            this.panelBarcode.Controls.Add(this.labelHnum);
            this.panelBarcode.Location = new System.Drawing.Point(364, 6);
            this.panelBarcode.Name = "panelBarcode";
            this.panelBarcode.Size = new System.Drawing.Size(550, 92);
            this.panelBarcode.TabIndex = 4;
            // 
            // verifyBoxBarCode
            // 
            this.verifyBoxBarCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBarCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBarCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBarCode.Location = new System.Drawing.Point(369, 37);
            this.verifyBoxBarCode.MaxLength = 18;
            this.verifyBoxBarCode.Name = "verifyBoxBarCode";
            this.verifyBoxBarCode.NewLine = false;
            this.verifyBoxBarCode.Size = new System.Drawing.Size(160, 23);
            this.verifyBoxBarCode.TabIndex = 16;
            this.verifyBoxBarCode.TabStop = false;
            this.verifyBoxBarCode.Text = "212720351234567890";
            this.verifyBoxBarCode.TextV = "";
            this.verifyBoxBarCode.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxTicket
            // 
            this.verifyBoxTicket.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTicket.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTicket.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTicket.Location = new System.Drawing.Point(263, 37);
            this.verifyBoxTicket.MaxLength = 10;
            this.verifyBoxTicket.Name = "verifyBoxTicket";
            this.verifyBoxTicket.NewLine = false;
            this.verifyBoxTicket.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxTicket.TabIndex = 14;
            this.verifyBoxTicket.Text = "1234567890";
            this.verifyBoxTicket.TextV = "";
            this.verifyBoxTicket.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxTicket.Validated += new System.EventHandler(this.verifyBoxTicket_Validated);
            // 
            // verifyBoxInsNum
            // 
            this.verifyBoxInsNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxInsNum.Location = new System.Drawing.Point(157, 37);
            this.verifyBoxInsNum.MaxLength = 6;
            this.verifyBoxInsNum.Name = "verifyBoxInsNum";
            this.verifyBoxInsNum.NewLine = false;
            this.verifyBoxInsNum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxInsNum.TabIndex = 11;
            this.verifyBoxInsNum.Text = "272035";
            this.verifyBoxInsNum.TextV = "";
            this.verifyBoxInsNum.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxTimes
            // 
            this.verifyBoxTimes.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTimes.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTimes.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTimes.Location = new System.Drawing.Point(69, 38);
            this.verifyBoxTimes.MaxLength = 1;
            this.verifyBoxTimes.Name = "verifyBoxTimes";
            this.verifyBoxTimes.NewLine = false;
            this.verifyBoxTimes.Size = new System.Drawing.Size(50, 23);
            this.verifyBoxTimes.TabIndex = 8;
            this.verifyBoxTimes.TextV = "";
            this.verifyBoxTimes.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxKenshu
            // 
            this.verifyBoxKenshu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKenshu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKenshu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKenshu.Location = new System.Drawing.Point(10, 38);
            this.verifyBoxKenshu.MaxLength = 1;
            this.verifyBoxKenshu.Name = "verifyBoxKenshu";
            this.verifyBoxKenshu.NewLine = false;
            this.verifyBoxKenshu.Size = new System.Drawing.Size(50, 23);
            this.verifyBoxKenshu.TabIndex = 5;
            this.verifyBoxKenshu.TextV = "";
            this.verifyBoxKenshu.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(66, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 9;
            this.label14.Text = "回数";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "回目";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(368, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "バーコード番号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(267, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "券番号(マッチング)";
            // 
            // labelInsNum
            // 
            this.labelInsNum.AutoSize = true;
            this.labelInsNum.Location = new System.Drawing.Point(158, 8);
            this.labelInsNum.Name = "labelInsNum";
            this.labelInsNum.Size = new System.Drawing.Size(108, 24);
            this.labelInsNum.TabIndex = 9;
            this.labelInsNum.Text = "保険者番号\r\n(マッチングデータ優先)";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(5, 119);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1010, 443);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.verifyBoxPossible);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.label54);
            this.panel1.Controls.Add(this.label56);
            this.panel1.Controls.Add(this.verifyBoxKibou);
            this.panel1.Controls.Add(this.label60);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label58);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.label51);
            this.panel1.Controls.Add(this.label52);
            this.panel1.Controls.Add(this.label53);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.verifyBoxDrSign);
            this.panel1.Controls.Add(this.label57);
            this.panel1.Controls.Add(this.verifyBoxTaion);
            this.panel1.Controls.Add(this.verifyBox6down);
            this.panel1.Controls.Add(this.label59);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.verifyBox2w);
            this.panel1.Controls.Add(this.verifyBoxSign);
            this.panel1.Controls.Add(this.label55);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.verifyBoxDateSign);
            this.panel1.Controls.Add(this.verifyBoxZikangai);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Location = new System.Drawing.Point(183, 325);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 150);
            this.panel1.TabIndex = 260;
            // 
            // verifyBoxPossible
            // 
            this.verifyBoxPossible.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPossible.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPossible.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPossible.Location = new System.Drawing.Point(53, 40);
            this.verifyBoxPossible.MaxLength = 1;
            this.verifyBoxPossible.Name = "verifyBoxPossible";
            this.verifyBoxPossible.NewLine = false;
            this.verifyBoxPossible.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxPossible.TabIndex = 10;
            this.verifyBoxPossible.TextV = "";
            this.verifyBoxPossible.Visible = false;
            this.verifyBoxPossible.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(346, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 81;
            this.label18.Text = "接種希望";
            this.label18.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(-4, 50);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 12);
            this.label45.TabIndex = 81;
            this.label45.Text = "接種可能";
            this.label45.Visible = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label54.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label54.Location = new System.Drawing.Point(602, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(42, 24);
            this.label54.TabIndex = 82;
            this.label54.Text = "1 : 有り\r\n2 : 無し";
            this.label54.Visible = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label56.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label56.Location = new System.Drawing.Point(535, 98);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(63, 24);
            this.label56.TabIndex = 82;
            this.label56.Text = "1 : 6歳以上\r\n2 : 6歳未満";
            this.label56.Visible = false;
            // 
            // verifyBoxKibou
            // 
            this.verifyBoxKibou.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKibou.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKibou.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKibou.Location = new System.Drawing.Point(405, 40);
            this.verifyBoxKibou.MaxLength = 1;
            this.verifyBoxKibou.Name = "verifyBoxKibou";
            this.verifyBoxKibou.NewLine = false;
            this.verifyBoxKibou.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKibou.TabIndex = 28;
            this.verifyBoxKibou.TextV = "";
            this.verifyBoxKibou.Visible = false;
            this.verifyBoxKibou.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label60.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label60.Location = new System.Drawing.Point(265, 114);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(66, 36);
            this.label60.TabIndex = 82;
            this.label60.Text = "1 : はい\r\n2 : いいえ\r\n3 : 記載無し";
            this.label60.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label17.Location = new System.Drawing.Point(373, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 48);
            this.label17.TabIndex = 82;
            this.label17.Text = "1 : 通常\r\n2 : 時間外\r\n3 : 休日\r\n9 : 欄無し=ヘッダ";
            this.label17.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label58.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label58.Location = new System.Drawing.Point(91, 118);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(42, 24);
            this.label58.TabIndex = 82;
            this.label58.Text = "1 : 有り\r\n2 : 無し";
            this.label58.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(176, 50);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 12);
            this.label49.TabIndex = 81;
            this.label49.Text = "医師署名";
            this.label49.Visible = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(656, 50);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(29, 12);
            this.label51.TabIndex = 81;
            this.label51.Text = "署名";
            this.label51.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label52.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label52.Location = new System.Drawing.Point(719, 41);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(42, 24);
            this.label52.TabIndex = 82;
            this.label52.Text = "1 : 有り\r\n2 : 無し";
            this.label52.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(517, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(53, 12);
            this.label53.TabIndex = 81;
            this.label53.Text = "日付署名";
            this.label53.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label50.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label50.Location = new System.Drawing.Point(264, 42);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(42, 24);
            this.label50.TabIndex = 82;
            this.label50.Text = "1 : 有り\r\n2 : 無し";
            this.label50.Visible = false;
            // 
            // verifyBoxDrSign
            // 
            this.verifyBoxDrSign.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrSign.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrSign.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrSign.Location = new System.Drawing.Point(237, 41);
            this.verifyBoxDrSign.MaxLength = 1;
            this.verifyBoxDrSign.Name = "verifyBoxDrSign";
            this.verifyBoxDrSign.NewLine = false;
            this.verifyBoxDrSign.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDrSign.TabIndex = 16;
            this.verifyBoxDrSign.TextV = "";
            this.verifyBoxDrSign.Visible = false;
            this.verifyBoxDrSign.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(3, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 12);
            this.label57.TabIndex = 81;
            this.label57.Text = "体温記載";
            this.label57.Visible = false;
            // 
            // verifyBoxTaion
            // 
            this.verifyBoxTaion.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTaion.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTaion.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTaion.Location = new System.Drawing.Point(60, 118);
            this.verifyBoxTaion.MaxLength = 1;
            this.verifyBoxTaion.Name = "verifyBoxTaion";
            this.verifyBoxTaion.NewLine = false;
            this.verifyBoxTaion.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxTaion.TabIndex = 18;
            this.verifyBoxTaion.TextV = "";
            this.verifyBoxTaion.Visible = false;
            this.verifyBoxTaion.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBox6down
            // 
            this.verifyBox6down.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox6down.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox6down.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox6down.Location = new System.Drawing.Point(508, 97);
            this.verifyBox6down.MaxLength = 1;
            this.verifyBox6down.Name = "verifyBox6down";
            this.verifyBox6down.NewLine = false;
            this.verifyBox6down.Size = new System.Drawing.Size(26, 23);
            this.verifyBox6down.TabIndex = 24;
            this.verifyBox6down.TextV = "";
            this.verifyBox6down.Visible = false;
            this.verifyBox6down.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(156, 116);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(69, 24);
            this.label59.TabIndex = 81;
            this.label59.Text = "2週間以内の\r\n予防接種";
            this.label59.Visible = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label46.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label46.Location = new System.Drawing.Point(80, 33);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(74, 36);
            this.label46.TabIndex = 82;
            this.label46.Text = "1 : 可能\r\n2 : 見合わせる\r\n3 : 記載無し";
            this.label46.Visible = false;
            // 
            // verifyBox2w
            // 
            this.verifyBox2w.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox2w.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox2w.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox2w.Location = new System.Drawing.Point(233, 119);
            this.verifyBox2w.MaxLength = 1;
            this.verifyBox2w.Name = "verifyBox2w";
            this.verifyBox2w.NewLine = false;
            this.verifyBox2w.Size = new System.Drawing.Size(26, 23);
            this.verifyBox2w.TabIndex = 22;
            this.verifyBox2w.TextV = "";
            this.verifyBox2w.Visible = false;
            this.verifyBox2w.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSign
            // 
            this.verifyBoxSign.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSign.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSign.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSign.Location = new System.Drawing.Point(692, 40);
            this.verifyBoxSign.MaxLength = 1;
            this.verifyBoxSign.Name = "verifyBoxSign";
            this.verifyBoxSign.NewLine = false;
            this.verifyBoxSign.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSign.TabIndex = 33;
            this.verifyBoxSign.TextV = "";
            this.verifyBoxSign.Visible = false;
            this.verifyBoxSign.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(453, 107);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(47, 12);
            this.label55.TabIndex = 81;
            this.label55.Text = "6歳未満";
            this.label55.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(432, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 36);
            this.label19.TabIndex = 82;
            this.label19.Text = "1 : 希望する\r\n2 : 希望しない\r\n3 : 記載無し";
            this.label19.Visible = false;
            // 
            // verifyBoxDateSign
            // 
            this.verifyBoxDateSign.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDateSign.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDateSign.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDateSign.Location = new System.Drawing.Point(575, 40);
            this.verifyBoxDateSign.MaxLength = 1;
            this.verifyBoxDateSign.Name = "verifyBoxDateSign";
            this.verifyBoxDateSign.NewLine = false;
            this.verifyBoxDateSign.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDateSign.TabIndex = 30;
            this.verifyBoxDateSign.TextV = "";
            this.verifyBoxDateSign.Visible = false;
            this.verifyBoxDateSign.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxZikangai
            // 
            this.verifyBoxZikangai.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZikangai.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZikangai.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZikangai.Location = new System.Drawing.Point(346, 95);
            this.verifyBoxZikangai.MaxLength = 1;
            this.verifyBoxZikangai.Name = "verifyBoxZikangai";
            this.verifyBoxZikangai.NewLine = false;
            this.verifyBoxZikangai.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxZikangai.TabIndex = 20;
            this.verifyBoxZikangai.TextV = "";
            this.verifyBoxZikangai.Visible = false;
            this.verifyBoxZikangai.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(289, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 81;
            this.label15.Text = "時間外等";
            this.label15.Visible = false;
            // 
            // panelSID
            // 
            this.panelSID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelSID.Controls.Add(this.label21);
            this.panelSID.Controls.Add(this.label11);
            this.panelSID.Controls.Add(this.label20);
            this.panelSID.Controls.Add(this.verifyBoxSesshuD);
            this.panelSID.Controls.Add(this.verifyBoxSesshuY);
            this.panelSID.Controls.Add(this.verifyBoxSesshuM);
            this.panelSID.Controls.Add(this.label37);
            this.panelSID.Controls.Add(this.verifyBoxHos);
            this.panelSID.Controls.Add(this.label38);
            this.panelSID.Controls.Add(this.label39);
            this.panelSID.Controls.Add(this.label30);
            this.panelSID.Controls.Add(this.label9);
            this.panelSID.Controls.Add(this.verifyBoxDr);
            this.panelSID.Controls.Add(this.verifyBoxLot);
            this.panelSID.Controls.Add(this.verifyBoxPlace);
            this.panelSID.Controls.Add(this.verifyBoxMaker);
            this.panelSID.Controls.Add(this.label62);
            this.panelSID.Controls.Add(this.label29);
            this.panelSID.Controls.Add(this.label28);
            this.panelSID.Controls.Add(this.label13);
            this.panelSID.Controls.Add(this.label10);
            this.panelSID.Location = new System.Drawing.Point(3, 571);
            this.panelSID.Name = "panelSID";
            this.panelSID.Size = new System.Drawing.Size(1008, 120);
            this.panelSID.TabIndex = 17;
            this.panelSID.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(332, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 12);
            this.label21.TabIndex = 85;
            this.label21.Text = "99 : 記載なし";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label11.Location = new System.Drawing.Point(169, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(155, 60);
            this.label11.TabIndex = 84;
            this.label11.Text = "6 : コミナティ(2価BA.1)\r\n7 : スパイクバックス(2価BA.1)\r\n8 : コミナティ(2価BA.4/5)\r\n9 : コミナティ(6か月-4歳)\r\n" +
    "10: スパイクバックス(2価BA.4/5)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label20.Location = new System.Drawing.Point(58, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 60);
            this.label20.TabIndex = 83;
            this.label20.Text = "1 : ファイザー\r\n2 : アストラゼネカ\r\n3 : モデルナ／武田\r\n4 : ファイザー(5-11歳）\r\n5 : ノババックス";
            // 
            // verifyBoxSesshuD
            // 
            this.verifyBoxSesshuD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSesshuD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSesshuD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSesshuD.Location = new System.Drawing.Point(948, 90);
            this.verifyBoxSesshuD.MaxLength = 2;
            this.verifyBoxSesshuD.Name = "verifyBoxSesshuD";
            this.verifyBoxSesshuD.NewLine = false;
            this.verifyBoxSesshuD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxSesshuD.TabIndex = 55;
            this.verifyBoxSesshuD.TextV = "";
            this.verifyBoxSesshuD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSesshuY
            // 
            this.verifyBoxSesshuY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSesshuY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSesshuY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSesshuY.Location = new System.Drawing.Point(818, 90);
            this.verifyBoxSesshuY.MaxLength = 4;
            this.verifyBoxSesshuY.Name = "verifyBoxSesshuY";
            this.verifyBoxSesshuY.NewLine = false;
            this.verifyBoxSesshuY.Size = new System.Drawing.Size(52, 23);
            this.verifyBoxSesshuY.TabIndex = 50;
            this.verifyBoxSesshuY.Text = "2022";
            this.verifyBoxSesshuY.TextV = "";
            this.verifyBoxSesshuY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSesshuM
            // 
            this.verifyBoxSesshuM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSesshuM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSesshuM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSesshuM.Location = new System.Drawing.Point(893, 90);
            this.verifyBoxSesshuM.MaxLength = 2;
            this.verifyBoxSesshuM.Name = "verifyBoxSesshuM";
            this.verifyBoxSesshuM.NewLine = false;
            this.verifyBoxSesshuM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxSesshuM.TabIndex = 52;
            this.verifyBoxSesshuM.TextV = "";
            this.verifyBoxSesshuM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(872, 96);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 26;
            this.label37.Text = "年";
            // 
            // verifyBoxHos
            // 
            this.verifyBoxHos.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHos.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHos.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHos.Location = new System.Drawing.Point(845, 60);
            this.verifyBoxHos.MaxLength = 10;
            this.verifyBoxHos.Name = "verifyBoxHos";
            this.verifyBoxHos.NewLine = false;
            this.verifyBoxHos.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxHos.TabIndex = 48;
            this.verifyBoxHos.Text = "1234567890";
            this.verifyBoxHos.TextV = "";
            this.verifyBoxHos.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHos.Validated += new System.EventHandler(this.verifyBoxHos_Validated);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(927, 96);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 28;
            this.label38.Text = "月";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(982, 96);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 30;
            this.label39.Text = "日";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(750, 65);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "医療機関等コード";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(750, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "接種年月日";
            // 
            // verifyBoxDr
            // 
            this.verifyBoxDr.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDr.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDr.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDr.Location = new System.Drawing.Point(510, 68);
            this.verifyBoxDr.Name = "verifyBoxDr";
            this.verifyBoxDr.NewLine = false;
            this.verifyBoxDr.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxDr.TabIndex = 45;
            this.verifyBoxDr.TextV = "";
            this.verifyBoxDr.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxLot
            // 
            this.verifyBoxLot.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxLot.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxLot.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.verifyBoxLot.Location = new System.Drawing.Point(20, 89);
            this.verifyBoxLot.Name = "verifyBoxLot";
            this.verifyBoxLot.NewLine = false;
            this.verifyBoxLot.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxLot.TabIndex = 39;
            this.verifyBoxLot.TextV = "";
            this.verifyBoxLot.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxPlace
            // 
            this.verifyBoxPlace.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPlace.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPlace.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPlace.Location = new System.Drawing.Point(510, 7);
            this.verifyBoxPlace.MaxLength = 255;
            this.verifyBoxPlace.Multiline = true;
            this.verifyBoxPlace.Name = "verifyBoxPlace";
            this.verifyBoxPlace.NewLine = false;
            this.verifyBoxPlace.Size = new System.Drawing.Size(450, 45);
            this.verifyBoxPlace.TabIndex = 41;
            this.verifyBoxPlace.TextV = "";
            this.verifyBoxPlace.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxMaker
            // 
            this.verifyBoxMaker.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMaker.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxMaker.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxMaker.Location = new System.Drawing.Point(20, 25);
            this.verifyBoxMaker.Name = "verifyBoxMaker";
            this.verifyBoxMaker.NewLine = false;
            this.verifyBoxMaker.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxMaker.TabIndex = 36;
            this.verifyBoxMaker.TextV = "";
            this.verifyBoxMaker.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label62.Location = new System.Drawing.Point(466, 104);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(218, 12);
            this.label62.TabIndex = 0;
            this.label62.Text = "※不明瞭な場合は「不明」で入力してください";
            this.label62.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(466, 73);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "医師名";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(454, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "実施場所";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "ロットNo.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "メーカー";
            // 
            // panelHeader
            // 
            this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelHeader.Controls.Add(this.verifyBoxY6DK);
            this.panelHeader.Controls.Add(this.verifyBoxS6DK);
            this.panelHeader.Controls.Add(this.verifyBoxY6D);
            this.panelHeader.Controls.Add(this.verifyBoxY6UK);
            this.panelHeader.Controls.Add(this.verifyBoxS6D);
            this.panelHeader.Controls.Add(this.verifyBoxS6UK);
            this.panelHeader.Controls.Add(this.verifyBoxY6U);
            this.panelHeader.Controls.Add(this.verifyBoxS6U);
            this.panelHeader.Controls.Add(this.label5);
            this.panelHeader.Controls.Add(this.label41);
            this.panelHeader.Controls.Add(this.label31);
            this.panelHeader.Controls.Add(this.label43);
            this.panelHeader.Controls.Add(this.label33);
            this.panelHeader.Controls.Add(this.label42);
            this.panelHeader.Controls.Add(this.label7);
            this.panelHeader.Location = new System.Drawing.Point(393, 521);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(350, 166);
            this.panelHeader.TabIndex = 30;
            this.panelHeader.Visible = false;
            // 
            // verifyBoxY6DK
            // 
            this.verifyBoxY6DK.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY6DK.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY6DK.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY6DK.Location = new System.Drawing.Point(197, 123);
            this.verifyBoxY6DK.MaxLength = 16;
            this.verifyBoxY6DK.Name = "verifyBoxY6DK";
            this.verifyBoxY6DK.NewLine = false;
            this.verifyBoxY6DK.Size = new System.Drawing.Size(70, 23);
            this.verifyBoxY6DK.TabIndex = 20;
            this.verifyBoxY6DK.TextV = "";
            this.verifyBoxY6DK.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxS6DK
            // 
            this.verifyBoxS6DK.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxS6DK.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxS6DK.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxS6DK.Location = new System.Drawing.Point(197, 59);
            this.verifyBoxS6DK.MaxLength = 16;
            this.verifyBoxS6DK.Name = "verifyBoxS6DK";
            this.verifyBoxS6DK.NewLine = false;
            this.verifyBoxS6DK.Size = new System.Drawing.Size(70, 23);
            this.verifyBoxS6DK.TabIndex = 10;
            this.verifyBoxS6DK.TextV = "";
            this.verifyBoxS6DK.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxY6D
            // 
            this.verifyBoxY6D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY6D.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY6D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY6D.Location = new System.Drawing.Point(113, 123);
            this.verifyBoxY6D.MaxLength = 16;
            this.verifyBoxY6D.Name = "verifyBoxY6D";
            this.verifyBoxY6D.NewLine = false;
            this.verifyBoxY6D.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxY6D.TabIndex = 16;
            this.verifyBoxY6D.TextV = "";
            this.verifyBoxY6D.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxY6UK
            // 
            this.verifyBoxY6UK.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY6UK.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY6UK.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY6UK.Location = new System.Drawing.Point(197, 90);
            this.verifyBoxY6UK.MaxLength = 16;
            this.verifyBoxY6UK.Name = "verifyBoxY6UK";
            this.verifyBoxY6UK.NewLine = false;
            this.verifyBoxY6UK.Size = new System.Drawing.Size(70, 23);
            this.verifyBoxY6UK.TabIndex = 15;
            this.verifyBoxY6UK.TextV = "";
            this.verifyBoxY6UK.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxS6D
            // 
            this.verifyBoxS6D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxS6D.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxS6D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxS6D.Location = new System.Drawing.Point(113, 59);
            this.verifyBoxS6D.MaxLength = 16;
            this.verifyBoxS6D.Name = "verifyBoxS6D";
            this.verifyBoxS6D.NewLine = false;
            this.verifyBoxS6D.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxS6D.TabIndex = 8;
            this.verifyBoxS6D.TextV = "";
            this.verifyBoxS6D.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxS6UK
            // 
            this.verifyBoxS6UK.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxS6UK.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxS6UK.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxS6UK.Location = new System.Drawing.Point(197, 26);
            this.verifyBoxS6UK.MaxLength = 16;
            this.verifyBoxS6UK.Name = "verifyBoxS6UK";
            this.verifyBoxS6UK.NewLine = false;
            this.verifyBoxS6UK.Size = new System.Drawing.Size(70, 23);
            this.verifyBoxS6UK.TabIndex = 7;
            this.verifyBoxS6UK.TextV = "";
            this.verifyBoxS6UK.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxY6U
            // 
            this.verifyBoxY6U.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY6U.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY6U.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY6U.Location = new System.Drawing.Point(113, 90);
            this.verifyBoxY6U.MaxLength = 16;
            this.verifyBoxY6U.Name = "verifyBoxY6U";
            this.verifyBoxY6U.NewLine = false;
            this.verifyBoxY6U.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxY6U.TabIndex = 13;
            this.verifyBoxY6U.TextV = "";
            this.verifyBoxY6U.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxS6U
            // 
            this.verifyBoxS6U.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxS6U.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxS6U.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxS6U.Location = new System.Drawing.Point(113, 26);
            this.verifyBoxS6U.MaxLength = 16;
            this.verifyBoxS6U.Name = "verifyBoxS6U";
            this.verifyBoxS6U.NewLine = false;
            this.verifyBoxS6U.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxS6U.TabIndex = 5;
            this.verifyBoxS6U.TextV = "";
            this.verifyBoxS6U.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(6, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "※0件、0円は入力不要";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 128);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(92, 12);
            this.label41.TabIndex = 9;
            this.label41.Text = "予診のみ6歳未満";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(34, 65);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(71, 12);
            this.label31.TabIndex = 9;
            this.label31.Text = "接種6歳未満";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(218, 11);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 12);
            this.label43.TabIndex = 9;
            this.label43.Text = "金額";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(12, 96);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(92, 12);
            this.label33.TabIndex = 9;
            this.label33.Text = "予診のみ6歳以上";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(130, 11);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 9;
            this.label42.Text = "人数";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "接種6歳以上";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(920, 20);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelocr
            // 
            this.labelocr.AutoSize = true;
            this.labelocr.Location = new System.Drawing.Point(923, 46);
            this.labelocr.Name = "labelocr";
            this.labelocr.Size = new System.Drawing.Size(77, 12);
            this.labelocr.TabIndex = 259;
            this.labelocr.Text = "OCR自動入力";
            this.labelocr.Visible = false;
            // 
            // verifyBoxKind
            // 
            this.verifyBoxKind.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKind.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKind.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKind.Location = new System.Drawing.Point(128, 25);
            this.verifyBoxKind.MaxLength = 3;
            this.verifyBoxKind.Name = "verifyBoxKind";
            this.verifyBoxKind.NewLine = false;
            this.verifyBoxKind.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxKind.TabIndex = 2;
            this.verifyBoxKind.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKind.TextV = "";
            this.verifyBoxKind.TextChanged += new System.EventHandler(this.verifyBoxKind_TextChanged);
            this.verifyBoxKind.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxKind.Leave += new System.EventHandler(this.verifyBoxKind_Leave);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(5, 649);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 43);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 150;
            this.dgv.TabStop = false;
            this.dgv.Visible = false;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label16.Location = new System.Drawing.Point(3, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 83);
            this.label16.TabIndex = 257;
            this.label16.Text = "ヘッダ  : \"..\"\r\n不要    : \"++\"\r\n予診票  : 空欄";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelym.ResumeLayout(false);
            this.panelym.PerformLayout();
            this.panelBarcode.ResumeLayout(false);
            this.panelBarcode.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelSID.ResumeLayout(false);
            this.panelSID.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxKind;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxKenshu;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panelym;
        private VerifyBox verifyBoxSesshuD;
        private VerifyBox verifyBoxSesshuM;
        private VerifyBox verifyBoxSesshuY;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panelSID;
        private System.Windows.Forms.Label labelocr;
        private System.Windows.Forms.Panel panelBarcode;
        private VerifyBox verifyBoxBarCode;
        private VerifyBox verifyBoxTicket;
        private VerifyBox verifyBoxInsNum;
        private VerifyBox verifyBoxTimes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelInsNum;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private VerifyBox verifyBoxHos;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label9;
        private VerifyBox verifyBoxDr;
        private VerifyBox verifyBoxLot;
        private VerifyBox verifyBoxKibou;
        private VerifyBox verifyBoxZikangai;
        private System.Windows.Forms.Label label18;
        private VerifyBox verifyBoxPlace;
        private VerifyBox verifyBoxMaker;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelHeader;
        private VerifyBox verifyBoxY6DK;
        private VerifyBox verifyBoxS6DK;
        private VerifyBox verifyBoxY6D;
        private VerifyBox verifyBoxY6UK;
        private VerifyBox verifyBoxS6D;
        private VerifyBox verifyBoxS6UK;
        private VerifyBox verifyBoxY6U;
        private VerifyBox verifyBoxS6U;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxHosNum;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label48;
        private VerifyBox verifyBoxZikan;
        private System.Windows.Forms.Label label47;
        private VerifyBox verifyBoxY;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private VerifyBox verifyBoxTaion;
        private System.Windows.Forms.Label label59;
        private VerifyBox verifyBox2w;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label46;
        private VerifyBox verifyBox6down;
        private VerifyBox verifyBoxDateSign;
        private VerifyBox verifyBoxSign;
        private VerifyBox verifyBoxDrSign;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label55;
        private VerifyBox verifyBoxPossible;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label45;
        private VerifyBox verifyBoxNOB;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private VerifyBox verifyBoxClinicName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxBatchNo;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxDrHD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonAdd;
        private VerifyBox verifyBoxAID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label21;
    }
}