﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Toyonakashi_covid19_shigai

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export
    {

        #region テーブル構造    
        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int exportid { get;set; }=0; 

        public string f000insurercode { get; set; } = string.Empty;             //6桁の固定長。「272035」固定;
        public string f001number { get; set; } = string.Empty;                  //お渡しするデータのNo.1：番号を10桁の固定長に（ゼロ埋めあり）;
        public string f002vacc_entrydate1 { get; set; } = string.Empty;         //1回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f003vacc_date1 { get; set; } = string.Empty;              //1回目接種日 YYYYMMDD（Yは西暦）;
        public string f004vacc_ticketnum1 { get; set; } = string.Empty;         //1回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f005vacc_insurercode1 { get; set; } = string.Empty;       //1回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f006vacc_place1 { get; set; } = string.Empty;             //1回目接種会場名 ;
        public string f007vacc_doctor1 { get; set; } = string.Empty;            //1回目接種医師名 ;
        public string f008vacc_maker1 { get; set; } = string.Empty;             //1回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f009vacc_lot1 { get; set; } = string.Empty;               //1回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f010vacc_entrydate2 { get; set; } = string.Empty;         //2回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f011vacc_date2 { get; set; } = string.Empty;              //2回目接種日 YYYYMMDD（Yは西暦）;
        public string f012vacc_ticketnum2 { get; set; } = string.Empty;         //2回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f013vacc_insurercode2 { get; set; } = string.Empty;       //2回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f014vacc_place2 { get; set; } = string.Empty;             //2回目接種会場名 ;
        public string f015vacc_doctor2 { get; set; } = string.Empty;            //2回目接種医師名 ;
        public string f016vacc_maker2 { get; set; } = string.Empty;             //2回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f017vacc_lot2 { get; set; } = string.Empty;               //2回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f018vacc_entrydate3 { get; set; } = string.Empty;         //3回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f019vacc_date3 { get; set; } = string.Empty;              //3回目接種日 YYYYMMDD（Yは西暦）;
        public string f020vacc_ticketnum3 { get; set; } = string.Empty;         //3回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f021vacc_insurercode3 { get; set; } = string.Empty;       //3回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f022vacc_place3 { get; set; } = string.Empty;             //3回目接種会場名 ;
        public string f023vacc_doctor3 { get; set; } = string.Empty;            //3回目接種医師名 ;
        public string f024vacc_maker3 { get; set; } = string.Empty;             //3回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f025vacc_lot3 { get; set; } = string.Empty;               //3回目ワクチンロット番号 数値はゼロ埋めなし;
        
        //20220816_1 ito st /////
        public string f026vacc_entrydate4 { get; set; } = string.Empty;       //4回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f027vacc_date4 { get; set; } = string.Empty;            //4回目接種日 YYYYMMDD（Yは西暦）;
        public string f028vacc_ticketnum4 { get; set; } = string.Empty;       //4回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f029vacc_insurercode4 { get; set; } = string.Empty;     //4回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f030vacc_place4 { get; set; } = string.Empty;           //4回目接種会場名 ;
        public string f031vacc_doctor4 { get; set; } = string.Empty;          //4回目接種医師名 ;
        public string f032vacc_maker4 { get; set; } = string.Empty;           //4回目ワクチンメーカー;
        public string f033vacc_lot4 { get; set; } = string.Empty;             //4回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f034vacc_entrydate5 { get; set; } = string.Empty;       //5回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f035vacc_date5 { get; set; } = string.Empty;            //5回目接種日 YYYYMMDD（Yは西暦）;
        public string f036vacc_ticketnum5 { get; set; } = string.Empty;       //5回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f037vacc_insurercode5 { get; set; } = string.Empty;     //5回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f038vacc_place5 { get; set; } = string.Empty;           //5回目接種会場名 ;
        public string f039vacc_doctor5 { get; set; } = string.Empty;          //5回目接種医師名 ;
        public string f040vacc_maker5 { get; set; } = string.Empty;           //5回目ワクチンメーカー;
        public string f041vacc_lot5 { get; set; } = string.Empty;             //5回目ワクチンロット番号 数値はゼロ埋めなし;
        //20220816_1 ito ed /////

        public int cym { get; set; } = 0;                                       //メホール請求年月;
        public string sid { get; set; } = string.Empty;                         //医療機関;




        #endregion

        /// <summary>
        /// 画像出力先リスト
        /// </summary>
        public static List<string> lstDestImageFileName = new List<string>();


        // 予診票用出力データリスト                  
        public static List<string> lstExport = new List<string>();

        public static string strFiser = "ファイザー";
        public static string strAstra = "アストラゼネカ";
        public static string strTakeda = "武田／モデルナ";
        public static string strFiser5 = "ファイザー（５から１１歳用）"; //20220608100321 furukawa st //メーカー名追加
        public static string strNova = "ノババックス"; //20220608100321 furukawa st //メーカー名追加
        public static string strComi = "コミナティ（２価：ＢＡ．１）"; //20220913_2 ito //メーカー名追加 Comirnaty
        public static string strSpike = "スパイクバックス（２価：ＢＡ．１）"; //20220913_2 ito //メーカー名追加 Spikevax
        public static string strCom4 = "コミナティ（２価：ＢＡ．４／５）";    //20221014_1 ito //メーカー名追加 Comirnaty
        public static string strCom0 = "コミナティ（６か月から４歳用）";           //20221206_1 ito //メーカー名追加 Comirnaty
        public static string strSpike4 = "スパイクバックス（2価：ＢＡ．４／５）";  //20221206_1 ito //メーカー名追加 Comirnaty
        //ワクチンメーカー追加①
        //メーカー名追加の際に変更すべき箇所：「ワクチンメーカー追加」で検索

        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            
            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {

                wf.LogPrint("出力テーブル作成");

                //dataexport作成
                if (!CreateExportTable1(cym, wf)) return false;

                //更新レコードをdataexport2テーブルに登録
                if (!CreateDataExport2(cym, wf)) return false;

                //dataexport2に対して更新
                if (!UpdateDataExport2(cym, wf)) return false;

                //出力データ用テーブルに登録
               // if (!InsertExportTable1(cym, wf)) return false;
                
                //データインポート時に既に登録しているので不要
                //更新レコードをdataexport2テーブルに登録
                //if (!InsertExportTable2(cym, wf)) return false;

                //市外は請求しない
                //請求一覧表を出力テーブルにコピー
               // if (!InsertExportTable_chargelist(cym, wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);

                //画像出力
                if (!ExportImage(cym, strNohinDir, wf)) return false;

                //検索用ブック
                if (!CreateExcelBook(cym, strNohinDir, wf)) return false;

                //dataexportテーブルの、１，２，３回目の各項目に抜けがあればdataexportから削除し、NGリストに記載
                if (!CheckBlankField(cym, strNohinDir,wf)) return false;

                //dataexport2テーブルの、１，２，３回目の各項目に抜けがあればdataexport2から削除し、NGリストに記載
                if (!CheckBlankField2(cym, strNohinDir, wf)) return false;

                //市外は請求しない
                //請求一覧csvファイル
                //if (!CreateChargeCSV(cym, strNohinDir, wf)) return false;

                //予診票CSVファイル作成
                if (!CreateCSVFile1(strNohinDir, cym, wf)) return false;

                //更新分レコードCSVファイル作成（dataexport2テーブルより）
                if (!CreateCSVFile2(strNohinDir, cym, wf)) return false;



                ////画像出力
                //if (!ExportImage(cym, strNohinDir, wf)) return false;

                ////検索用ブック
                //if (!CreateExcelBook(cym, strNohinDir, wf)) return false;

                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }

        
        private static List<export> select (int cym)
        {
            List<export> lst = new List<export>();
            //20220816_1 ito st /////
            DB.Command cmd = new DB.Command(DB.Main, $"select exportid, f001number, " +
                $"f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1," +
                $"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2," +
                $"f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3," +
                $"f026vacc_entrydate4,f027vacc_date4,f028vacc_ticketnum4,f029vacc_insurercode4,f030vacc_place4,f031vacc_doctor4,f032vacc_maker4,f033vacc_lot4," +
                $"f034vacc_entrydate5,f035vacc_date5,f036vacc_ticketnum5,f037vacc_insurercode5,f038vacc_place5,f039vacc_doctor5,f040vacc_maker5,f041vacc_lot5" +
                $" from dataexport where cym={cym} order by exportid");
            //DB.Command cmd = new DB.Command(DB.Main, $"select * from dataexport where cym={cym} order by exportid");
            //20220816_1 ito st /////
            var l =cmd.TryExecuteReaderList();
            
            foreach(var item in l)
            {
                export e = new export();
                e.exportid = int.Parse(item[0].ToString());
                e.f000insurercode = item[1].ToString();
                e.f001number = item[2].ToString();
                e.f002vacc_entrydate1 = item[3].ToString();
                e.f003vacc_date1 = item[4].ToString();
                e.f004vacc_ticketnum1 = item[5].ToString();
                e.f005vacc_insurercode1 = item[6].ToString();
                e.f006vacc_place1 = item[7].ToString();
                e.f007vacc_doctor1 = item[8].ToString();
                e.f008vacc_maker1 = item[9].ToString();
                e.f009vacc_lot1 = item[10].ToString();
                e.f010vacc_entrydate2 = item[11].ToString();
                e.f011vacc_date2 = item[12].ToString();
                e.f012vacc_ticketnum2 = item[13].ToString();
                e.f013vacc_insurercode2 = item[14].ToString();
                e.f014vacc_place2 = item[15].ToString();
                e.f015vacc_doctor2 = item[16].ToString();
                e.f016vacc_maker2 = item[17].ToString();
                e.f017vacc_lot2 = item[18].ToString();
                e.f018vacc_entrydate3 = item[19].ToString();
                e.f019vacc_date3 = item[20].ToString();
                e.f020vacc_ticketnum3 = item[21].ToString();
                e.f021vacc_insurercode3 = item[22].ToString();
                e.f022vacc_place3 = item[23].ToString();
                e.f023vacc_doctor3 = item[24].ToString();
                e.f024vacc_maker3 = item[25].ToString();
                e.f025vacc_lot3 = item[26].ToString();

                //20220816_1 ito st /////
                e.f026vacc_entrydate4 = item[26].ToString();
                e.f027vacc_date4 = item[27].ToString();
                e.f028vacc_ticketnum4 = item[28].ToString();
                e.f029vacc_insurercode4 = item[29].ToString();
                e.f030vacc_place4 = item[30].ToString();
                e.f031vacc_doctor4 = item[31].ToString();
                e.f032vacc_maker4 = item[32].ToString();
                e.f033vacc_lot4 = item[33].ToString();
                e.f034vacc_entrydate5 = item[34].ToString();
                e.f035vacc_date5 = item[35].ToString();
                e.f036vacc_ticketnum5 = item[36].ToString();
                e.f037vacc_insurercode5 = item[37].ToString();
                e.f038vacc_place5 = item[38].ToString();
                e.f039vacc_doctor5 = item[39].ToString();
                e.f040vacc_maker5 = item[40].ToString();
                e.f041vacc_lot5 = item[41].ToString();
                //20220816_1 ito ed /////

                lst.Add(e);
            }
            return lst;
        }


        private static bool CreateChargeCSV(int cym,string strFolder,WaitForm wf)
        {

            StringBuilder sbsql = new StringBuilder();
            sbsql.Append(" SELECT         ");
            sbsql.Append("  exportid,     ");
            sbsql.Append("  no,           ");
            sbsql.Append("  hosnum,       ");
            sbsql.Append("  nob,          ");
            sbsql.Append("  hosname,      ");
            sbsql.Append("  ty6d,         ");
            sbsql.Append("  ty6u,         ");
            sbsql.Append("  ts6d,         ");
            sbsql.Append("  ts6u,         ");
            sbsql.Append("  ty6dk,        ");
            sbsql.Append("  ty6uk,        ");
            sbsql.Append("  ts6dk,        ");
            sbsql.Append("  ts6uk,        ");
            sbsql.Append("  tcharge,      ");
            sbsql.Append("  gy6d,         ");
            sbsql.Append("  gy6u,         ");
            sbsql.Append("  gs6d,         ");
            sbsql.Append("  gs6u,         ");
            sbsql.Append("  gy6dk,        ");
            sbsql.Append("  gy6uk,        ");
            sbsql.Append("  gs6dk,        ");
            sbsql.Append("  gs6uk,        ");
            sbsql.Append("  gcharge,      ");
            sbsql.Append("  ky6d,         ");
            sbsql.Append("  ky6u,         ");
            sbsql.Append("  ks6d,         ");
            sbsql.Append("  ks6u,         ");
            sbsql.Append("  ky6dk,        ");
            sbsql.Append("  ky6uk,        ");
            sbsql.Append("  ks6dk,        ");
            sbsql.Append("  ks6uk,        ");
            sbsql.Append("  kcharge,      ");
            sbsql.Append("  fy6d,         ");
            sbsql.Append("  fy6u,         ");
            sbsql.Append("  fs6d,         ");
            sbsql.Append("  fs6u,         ");
            sbsql.Append("  fy6dk,        ");
            sbsql.Append("  fy6uk,        ");
            sbsql.Append("  fs6dk,        ");
            sbsql.Append("  fs6uk,        ");
            sbsql.Append("  fcharge,      ");
            sbsql.Append("  chargetotal ");
  
            sbsql.Append("  FROM chargelistexport ");
            sbsql.AppendLine($" where cym={cym} ");
            sbsql.AppendLine(" order by exportid ");



            DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString());
            try
            {
                lstExport.Clear();
                wf.InvokeValue = 0;
                //テーブルからロード
                wf.LogPrint("chargelistexportテーブルよりロード");
                var list = cmd.TryExecuteReaderList();
                foreach (var item in list)
                {
                    export e = new export();
                    StringBuilder sb = new StringBuilder();
                    for (int c = 0; c < item.Length; c++)
                    {
                        sb.Append($"{item[c]},");
                    }
                    sb = sb.Remove(sb.ToString().Length - 1, 1);
                    lstExport.Add(sb.ToString());
                }


                wf.LogPrint("chargelistexportテーブルの内容をCSVファイルに出力");

              
                //ファイル名
                string strOutPutFileName = $"{strFolder}\\{DateTime.Now.ToString("yyMMddHHmmss")}_請求一覧.csv";
                //1番目のファイル
                System.IO.StreamWriter sw =
                    new System.IO.StreamWriter(strOutPutFileName, false, System.Text.Encoding.GetEncoding("utf-8"));
                
                wf.SetMax(lstExport.Count);

                //ヘッダ
                sw.WriteLine("exportid,記載順序,医療機関等コード,豊中市独自の番号,医療機関名," +
                    "【通常】予診のみ６歳未満,【通常】予診のみ６歳以上,【通常】接種６歳未満,【通常】接種６歳以上,【通常】予診のみ６歳未満金額,【通常】予診のみ６歳以上金額,【通常】接種６歳未満金額,【通常】接種６歳以上金額,【通常】請求金額," +
                    "【時間外】予診のみ６歳未満,【時間外】予診のみ６歳以上,【時間外】接種６歳未満,【時間外】接種６歳以上,【時間外】予診のみ６歳未満金額,【時間外】予診のみ６歳以上金額,【時間外】接種６歳未満金額,【時間外】接種６歳以上金額,【時間外】請求金額," +
                    "【休日】予診のみ６歳未満,【休日】予診のみ６歳以上,【休日】接種６歳未満,【休日】接種６歳以上,【休日】予診のみ６歳未満金額,【休日】予診のみ６歳以上金額,【休日】接種６歳未満金額,【休日】接種６歳以上金額,【休日】請求金額," +
                    "【不明】予診のみ６歳未満メディ独自,【不明】予診のみ６歳以上メディ独自,【不明】接種６歳未満メディ独自,【不明】接種６歳以上メディ独自,【不明】予診のみ６歳未満金額メディ独自,【不明】予診のみ６歳以上金額メディ独自,【不明】接種６歳未満金額メディ独自,【不明】接種６歳以上金額メディ独自,【不明】請求金額メディ独自,請求金額合計");

                foreach (string s in lstExport)
                {
                    sw.WriteLine(s);
                    
                    wf.InvokeValue++;
                }
                sw.Close();
                wf.LogPrint("請求一覧CSVファイル作成完了");


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

                cmd.Dispose();
            }
        }


        /// <summary>
        /// dataexportテーブルを全て調査し不完全レコードがある場合は削除し納品しない
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strFolder"></param>
        /// <returns></returns>
        private static bool CheckBlankField(int cym,string strFolder,WaitForm wf)
        {
            List<export> lst = new List<export>();
            lst = export.select(cym);

            string strFileName = $"{strFolder}\\{DateTime.Now.ToString("yyyy-MM-dd")}_不完全レコードリスト.csv";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
            wf.LogPrint("不完全レコードチェック");
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;

            try 
            {

                //dataexportテーブルを全て調査
                foreach (export e in lst)
                {
                    //20220816_1 ito st /////
                    bool flgNG1 = false, flgNG2 = false, flgNG3 = false, flgNG4 = false, flgNG5 = false;
                    //bool flgNG1 = false, flgNG2 = false, flgNG3 = false;
                    //20220816_1 ito ed /////

                    if (
                        (e.f002vacc_entrydate1 == string.Empty &&
                        e.f003vacc_date1 == string.Empty &&
                        e.f004vacc_ticketnum1 == string.Empty &&
                        e.f005vacc_insurercode1 == string.Empty &&
                        e.f006vacc_place1 == string.Empty &&
                        e.f007vacc_doctor1 == string.Empty &&
                        e.f008vacc_maker1 == string.Empty &&
                        e.f009vacc_lot1 == string.Empty
                        ) ||
                        (e.f002vacc_entrydate1 != string.Empty &&
                        e.f003vacc_date1 != string.Empty &&
                        e.f004vacc_ticketnum1 != string.Empty &&
                        e.f005vacc_insurercode1 != string.Empty &&
                        e.f006vacc_place1 != string.Empty &&
                        e.f007vacc_doctor1 != string.Empty &&
                        e.f008vacc_maker1 != string.Empty &&
                        e.f009vacc_lot1 != string.Empty
                        )
                      )
                    {
                        flgNG1 = false;
                    }
                    else flgNG1 = true;


                    if (
                       (e.f010vacc_entrydate2 == string.Empty &&
                       e.f011vacc_date2 == string.Empty &&
                       e.f012vacc_ticketnum2 == string.Empty &&
                       e.f013vacc_insurercode2 == string.Empty &&
                       e.f014vacc_place2 == string.Empty &&
                       e.f015vacc_doctor2 == string.Empty &&
                       e.f016vacc_maker2 == string.Empty &&
                       e.f017vacc_lot2 == string.Empty
                       ) ||
                        (e.f010vacc_entrydate2 != string.Empty &&
                       e.f011vacc_date2 != string.Empty &&
                       e.f012vacc_ticketnum2 != string.Empty &&
                       e.f013vacc_insurercode2 != string.Empty &&
                       e.f014vacc_place2 != string.Empty &&
                       e.f015vacc_doctor2 != string.Empty &&
                       e.f016vacc_maker2 != string.Empty &&
                       e.f017vacc_lot2 != string.Empty
                       )
                     )
                    {
                        flgNG2 = false;
                    }
                    else flgNG2 = true;

                    if (
                    (e.f018vacc_entrydate3 == string.Empty &&
                    e.f019vacc_date3 == string.Empty &&
                    e.f020vacc_ticketnum3 == string.Empty &&
                    e.f021vacc_insurercode3 == string.Empty &&
                    e.f022vacc_place3 == string.Empty &&
                    e.f023vacc_doctor3 == string.Empty &&
                    e.f024vacc_maker3 == string.Empty &&
                    e.f025vacc_lot3 == string.Empty
                    ) ||
                    (e.f018vacc_entrydate3 != string.Empty &&
                    e.f019vacc_date3 != string.Empty &&
                    e.f020vacc_ticketnum3 != string.Empty &&
                    e.f021vacc_insurercode3 != string.Empty &&
                    e.f022vacc_place3 != string.Empty &&
                    e.f023vacc_doctor3 != string.Empty &&
                    e.f024vacc_maker3 != string.Empty &&
                    e.f025vacc_lot3 != string.Empty
                    )
                  )
                    {
                        flgNG3 = false;
                    }
                    else flgNG3 = true;

                    //20220816_1 ito st /////
                    if (
                        (e.f026vacc_entrydate4 == string.Empty &&
                        e.f027vacc_date4 == string.Empty &&
                        e.f028vacc_ticketnum4 == string.Empty &&
                        e.f029vacc_insurercode4 == string.Empty &&
                        e.f030vacc_place4 == string.Empty &&
                        e.f031vacc_doctor4 == string.Empty &&
                        e.f032vacc_maker4 == string.Empty &&
                        e.f033vacc_lot4 == string.Empty
                        ) ||
                        (e.f026vacc_entrydate4 != string.Empty &&
                        e.f027vacc_date4 != string.Empty &&
                        e.f028vacc_ticketnum4 != string.Empty &&
                        e.f029vacc_insurercode4 != string.Empty &&
                        e.f030vacc_place4 != string.Empty &&
                        e.f031vacc_doctor4 != string.Empty &&
                        e.f032vacc_maker4 != string.Empty &&
                        e.f033vacc_lot4 != string.Empty
                        )
                    )
                    {
                        flgNG4 = false;
                    }
                    else flgNG4 = true;

                    if (
                        (e.f034vacc_entrydate5 == string.Empty &&
                        e.f035vacc_date5 == string.Empty &&
                        e.f036vacc_ticketnum5 == string.Empty &&
                        e.f037vacc_insurercode5 == string.Empty &&
                        e.f038vacc_place5 == string.Empty &&
                        e.f039vacc_doctor5 == string.Empty &&
                        e.f040vacc_maker5 == string.Empty &&
                        e.f041vacc_lot5 == string.Empty
                        ) ||
                        (e.f034vacc_entrydate5 != string.Empty &&
                        e.f035vacc_date5 != string.Empty &&
                        e.f036vacc_ticketnum5 != string.Empty &&
                        e.f037vacc_insurercode5 != string.Empty &&
                        e.f038vacc_place5 != string.Empty &&
                        e.f039vacc_doctor5 != string.Empty &&
                        e.f040vacc_maker5 != string.Empty &&
                        e.f041vacc_lot5 != string.Empty
                        )
                    )
                    {
                        flgNG5 = false;
                    }
                    else flgNG5 = true;

                    //どれかに抜けがあった場合
                    if (flgNG1 || flgNG2 || flgNG3 || flgNG4 || flgNG5)
                    //    if (flgNG1 || flgNG2 || flgNG3)
                    //20220816_1 ito ed /////
                    {
                        //不完全リストに記載
                        StringBuilder sb = new StringBuilder();
                        sb.Append($"{e.f001number           },");
                        sb.Append($"{e.f002vacc_entrydate1  },");
                        sb.Append($"{e.f003vacc_date1       },");
                        sb.Append($"{e.f004vacc_ticketnum1  },");
                        sb.Append($"{e.f005vacc_insurercode1},");
                        sb.Append($"{e.f006vacc_place1      },");
                        sb.Append($"{e.f007vacc_doctor1     },");
                        sb.Append($"{e.f008vacc_maker1      },");
                        sb.Append($"{e.f009vacc_lot1        },");
                        sb.Append($"{e.f010vacc_entrydate2  },");
                        sb.Append($"{e.f011vacc_date2       },");
                        sb.Append($"{e.f012vacc_ticketnum2  },");
                        sb.Append($"{e.f013vacc_insurercode2},");
                        sb.Append($"{e.f014vacc_place2      },");
                        sb.Append($"{e.f015vacc_doctor2     },");
                        sb.Append($"{e.f016vacc_maker2      },");
                        sb.Append($"{e.f017vacc_lot2        },");
                        sb.Append($"{e.f018vacc_entrydate3  },");
                        sb.Append($"{e.f019vacc_date3       },");
                        sb.Append($"{e.f020vacc_ticketnum3  },");
                        sb.Append($"{e.f021vacc_insurercode3},");
                        sb.Append($"{e.f022vacc_place3      },");
                        sb.Append($"{e.f023vacc_doctor3     },");
                        sb.Append($"{e.f024vacc_maker3      },");
                        //20220816_1 ito st /////
                        //sb.Append($"{e.f025vacc_lot3        }");
                        sb.Append($"{e.f025vacc_lot3        },");

                        sb.Append($"{e.f026vacc_entrydate4  },");
                        sb.Append($"{e.f027vacc_date4       },");
                        sb.Append($"{e.f028vacc_ticketnum4  },");
                        sb.Append($"{e.f029vacc_insurercode4},");
                        sb.Append($"{e.f030vacc_place4      },");
                        sb.Append($"{e.f031vacc_doctor4     },");
                        sb.Append($"{e.f032vacc_maker4      },");
                        sb.Append($"{e.f033vacc_lot4        },");
                        sb.Append($"{e.f034vacc_entrydate5  },");
                        sb.Append($"{e.f035vacc_date5       },");
                        sb.Append($"{e.f036vacc_ticketnum5  },");
                        sb.Append($"{e.f037vacc_insurercode5},");
                        sb.Append($"{e.f038vacc_place5      },");
                        sb.Append($"{e.f039vacc_doctor5     },");
                        sb.Append($"{e.f040vacc_maker5      },");
                        sb.Append($"{e.f041vacc_lot5        }");
                        //20220816_1 ito ed /////

                        sw.WriteLine(sb.ToString());

                        //出力テーブルから削除し納品しない
                        using (DB.Command cmd = DB.Main.CreateCmd($"delete from dataexport where f001number='{e.f001number}' "))
                        {
                            cmd.TryExecuteNonQuery();
                        }
                        wf.LogPrint($"券番号 {e.f001number}削除");
                    }
                    wf.InvokeValue++;
                }
                return true;
               
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);                
                return false;
            }
            finally
            {
                sw.Close();
            }
        }


        /// <summary>
        /// dataexport2テーブルを全て調査し不完全レコードがある場合は削除し納品しない
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strFolder"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CheckBlankField2(int cym, string strFolder, WaitForm wf)
        {
            List<dataexport2> lst = new List<dataexport2>();
            lst = dataexport2.select(cym);

            string strFileName = $"{strFolder}\\{DateTime.Now.ToString("yyyy-MM-dd")}_不完全レコードリスト2.csv";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
            wf.LogPrint("不完全レコードチェック2");
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;

            try
            {

                //dataexportテーブルを全て調査
                foreach (dataexport2 e in lst)
                {
                    //20220816_1 ito st /////
                    bool flgNG1 = false, flgNG2 = false, flgNG3 = false, flgNG4 = false, flgNG5 = false;
                    //bool flgNG1 = false, flgNG2 = false, flgNG3 = false;
                    //20220816_1 ito ed /////

                    if (
                        (e.f002vacc_entrydate1 == string.Empty &&
                        e.f003vacc_date1 == string.Empty &&
                        e.f004vacc_ticketnum1 == string.Empty &&
                        e.f005vacc_insurercode1 == string.Empty &&
                        e.f006vacc_place1 == string.Empty &&
                        e.f007vacc_doctor1 == string.Empty &&
                        e.f008vacc_maker1 == string.Empty &&
                        e.f009vacc_lot1 == string.Empty
                        ) ||
                        (e.f002vacc_entrydate1 != string.Empty &&
                        e.f003vacc_date1 != string.Empty &&
                        e.f004vacc_ticketnum1 != string.Empty &&
                        e.f005vacc_insurercode1 != string.Empty &&
                        e.f006vacc_place1 != string.Empty &&
                        e.f007vacc_doctor1 != string.Empty &&
                        e.f008vacc_maker1 != string.Empty &&
                        e.f009vacc_lot1 != string.Empty
                        )
                      )
                    {
                        flgNG1 = false;
                    }
                    else flgNG1 = true;


                    if (
                       (e.f010vacc_entrydate2 == string.Empty &&
                       e.f011vacc_date2 == string.Empty &&
                       e.f012vacc_ticketnum2 == string.Empty &&
                       e.f013vacc_insurercode2 == string.Empty &&
                       e.f014vacc_place2 == string.Empty &&
                       e.f015vacc_doctor2 == string.Empty &&
                       e.f016vacc_maker2 == string.Empty &&
                       e.f017vacc_lot2 == string.Empty
                       ) ||
                        (e.f010vacc_entrydate2 != string.Empty &&
                       e.f011vacc_date2 != string.Empty &&
                       e.f012vacc_ticketnum2 != string.Empty &&
                       e.f013vacc_insurercode2 != string.Empty &&
                       e.f014vacc_place2 != string.Empty &&
                       e.f015vacc_doctor2 != string.Empty &&
                       e.f016vacc_maker2 != string.Empty &&
                       e.f017vacc_lot2 != string.Empty
                       )
                     )
                    {
                        flgNG2 = false;
                    }
                    else flgNG2 = true;

                    if (
                    (e.f018vacc_entrydate3 == string.Empty &&
                    e.f019vacc_date3 == string.Empty &&
                    e.f020vacc_ticketnum3 == string.Empty &&
                    e.f021vacc_insurercode3 == string.Empty &&
                    e.f022vacc_place3 == string.Empty &&
                    e.f023vacc_doctor3 == string.Empty &&
                    e.f024vacc_maker3 == string.Empty &&
                    e.f025vacc_lot3 == string.Empty
                    ) ||
                    (e.f018vacc_entrydate3 != string.Empty &&
                    e.f019vacc_date3 != string.Empty &&
                    e.f020vacc_ticketnum3 != string.Empty &&
                    e.f021vacc_insurercode3 != string.Empty &&
                    e.f022vacc_place3 != string.Empty &&
                    e.f023vacc_doctor3 != string.Empty &&
                    e.f024vacc_maker3 != string.Empty &&
                    e.f025vacc_lot3 != string.Empty
                    )
                  )
                    {
                        flgNG3 = false;
                    }
                    else flgNG3 = true;


                    //20220816_1 ito st /////
                    if (
                        (e.f026vacc_entrydate4 == string.Empty &&
                        e.f027vacc_date4 == string.Empty &&
                        e.f028vacc_ticketnum4 == string.Empty &&
                        e.f029vacc_insurercode4 == string.Empty &&
                        e.f030vacc_place4 == string.Empty &&
                        e.f031vacc_doctor4 == string.Empty &&
                        e.f032vacc_maker4 == string.Empty &&
                        e.f033vacc_lot4 == string.Empty
                        ) ||
                        (e.f026vacc_entrydate4 != string.Empty &&
                        e.f027vacc_date4 != string.Empty &&
                        e.f028vacc_ticketnum4 != string.Empty &&
                        e.f029vacc_insurercode4 != string.Empty &&
                        e.f030vacc_place4 != string.Empty &&
                        e.f031vacc_doctor4 != string.Empty &&
                        e.f032vacc_maker4 != string.Empty &&
                        e.f033vacc_lot4 != string.Empty
                        )
                    )
                    {
                        flgNG4 = false;
                    }
                    else flgNG4 = true;

                    if (
                        (e.f034vacc_entrydate5 == string.Empty &&
                        e.f035vacc_date5 == string.Empty &&
                        e.f036vacc_ticketnum5 == string.Empty &&
                        e.f037vacc_insurercode5 == string.Empty &&
                        e.f038vacc_place5 == string.Empty &&
                        e.f039vacc_doctor5 == string.Empty &&
                        e.f040vacc_maker5 == string.Empty &&
                        e.f041vacc_lot5 == string.Empty
                        ) ||
                        (e.f034vacc_entrydate5 != string.Empty &&
                        e.f035vacc_date5 != string.Empty &&
                        e.f036vacc_ticketnum5 != string.Empty &&
                        e.f037vacc_insurercode5 != string.Empty &&
                        e.f038vacc_place5 != string.Empty &&
                        e.f039vacc_doctor5 != string.Empty &&
                        e.f040vacc_maker5 != string.Empty &&
                        e.f041vacc_lot5 != string.Empty
                        )
                    )
                    {
                        flgNG5 = false;
                    }
                    else flgNG5 = true;


                    //どれかに抜けがあった場合
                    if (flgNG1 || flgNG2 || flgNG3 || flgNG4 || flgNG5)
                    //    if (flgNG1 || flgNG2 || flgNG3)
                    //20220816_1 ito ed /////
                    {
                        //不完全リストに記載
                        StringBuilder sb = new StringBuilder();
                        sb.Append($"{e.f001number           },");
                        sb.Append($"{e.f002vacc_entrydate1  },");
                        sb.Append($"{e.f003vacc_date1       },");
                        sb.Append($"{e.f004vacc_ticketnum1  },");
                        sb.Append($"{e.f005vacc_insurercode1},");
                        sb.Append($"{e.f006vacc_place1      },");
                        sb.Append($"{e.f007vacc_doctor1     },");
                        sb.Append($"{e.f008vacc_maker1      },");
                        sb.Append($"{e.f009vacc_lot1        },");
                        sb.Append($"{e.f010vacc_entrydate2  },");
                        sb.Append($"{e.f011vacc_date2       },");
                        sb.Append($"{e.f012vacc_ticketnum2  },");
                        sb.Append($"{e.f013vacc_insurercode2},");
                        sb.Append($"{e.f014vacc_place2      },");
                        sb.Append($"{e.f015vacc_doctor2     },");
                        sb.Append($"{e.f016vacc_maker2      },");
                        sb.Append($"{e.f017vacc_lot2        },");
                        sb.Append($"{e.f018vacc_entrydate3  },");
                        sb.Append($"{e.f019vacc_date3       },");
                        sb.Append($"{e.f020vacc_ticketnum3  },");
                        sb.Append($"{e.f021vacc_insurercode3},");
                        sb.Append($"{e.f022vacc_place3      },");
                        sb.Append($"{e.f023vacc_doctor3     },");
                        sb.Append($"{e.f024vacc_maker3      },");
                        //20220816_1 ito st /////
                        //sb.Append($"{e.f025vacc_lot3        }");
                        sb.Append($"{e.f025vacc_lot3        },");

                        sb.Append($"{e.f026vacc_entrydate4  },");
                        sb.Append($"{e.f027vacc_date4       },");
                        sb.Append($"{e.f028vacc_ticketnum4  },");
                        sb.Append($"{e.f029vacc_insurercode4},");
                        sb.Append($"{e.f030vacc_place4      },");
                        sb.Append($"{e.f031vacc_doctor4     },");
                        sb.Append($"{e.f032vacc_maker4      },");
                        sb.Append($"{e.f033vacc_lot4        },");
                        sb.Append($"{e.f034vacc_entrydate5  },");
                        sb.Append($"{e.f035vacc_date5       },");
                        sb.Append($"{e.f036vacc_ticketnum5  },");
                        sb.Append($"{e.f037vacc_insurercode5},");
                        sb.Append($"{e.f038vacc_place5      },");
                        sb.Append($"{e.f039vacc_doctor5     },");
                        sb.Append($"{e.f040vacc_maker5      },");
                        sb.Append($"{e.f041vacc_lot5        }");
                        //20220816_1 ito ed /////

                        sw.WriteLine(sb.ToString());

                        //出力テーブルから削除し納品しない
                        using (DB.Command cmd = DB.Main.CreateCmd($"delete from dataexport2 where f001number='{e.f001number}' "))
                        {
                            cmd.TryExecuteNonQuery();
                        }
                        wf.LogPrint($"券番号 {e.f001number}削除");
                    }
                    wf.InvokeValue++;
                }
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
            }
        }


        /// <summary>
        /// dataexpoertに出力する
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CreateExportTable1(int cym, WaitForm wf)
        {
            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from dataexport where cym='{cym}'", tran);


            try
            {
                cmddel.TryExecuteNonQuery();


                StringBuilder sbsql = new StringBuilder();

                StringBuilder vaccMaker = new StringBuilder();
                vaccMaker.AppendLine($"        when '1' then '{strFiser}'  ");
                vaccMaker.AppendLine($"        when '2' then '{strAstra}'  ");
                vaccMaker.AppendLine($"        when '3' then '{strTakeda}' ");
                vaccMaker.AppendLine($"        when '4' then '{strFiser5}' ");//20220608100605 furukawa st //メーカー名追加
                vaccMaker.AppendLine($"        when '5' then '{strNova}'   ");//20220608100605 furukawa ed //メーカー名追加
                vaccMaker.AppendLine($"        when '6' then '{strComi}'   ");//202209113_2 ito //メーカー名追加
                vaccMaker.AppendLine($"        when '7' then '{strSpike}'  ");//202209113_2 ito //メーカー名追加
                vaccMaker.AppendLine($"        when '8' then '{strCom4}'   ");//20221014_1 ito //メーカー名追加
                vaccMaker.AppendLine($"        when '9' then '{strCom0}'   ");//20221201_1 ito //メーカー名追加
                vaccMaker.AppendLine($"        when '10' then '{strSpike4}'   ");//20221201_1 ito //メーカー名追加
                //ワクチンメーカー追加②

                #region 1.dataexportbeforeにapplicationからコピーする
                wf.LogPrint("1.dataexportbeforeにapplicationからコピーする");


                sbsql.Clear();
                sbsql.AppendLine($" truncate table dataexportbefore ;");

                using (DB.Command updcmd1 = DB.Main.CreateCmd(sbsql.ToString(), tran))
                {
                    updcmd1.TryExecuteNonQuery();
                }


                sbsql.Clear();

                sbsql.AppendLine($" insert into dataexportbefore (");

                sbsql.AppendLine($" insurercode, ");
                sbsql.AppendLine($" kind, ");
                sbsql.AppendLine($" times, ");
                sbsql.AppendLine($" barcode, ");
                sbsql.AppendLine($" number, ");
                sbsql.AppendLine($" vacc_entrydate, ");
                sbsql.AppendLine($" vacc_date, ");
                sbsql.AppendLine($" vacc_ticketnum, ");
                sbsql.AppendLine($" vacc_insurercode, ");
                sbsql.AppendLine($" vacc_place, ");
                sbsql.AppendLine($" vacc_doctor, ");
                sbsql.AppendLine($" vacc_maker, ");
                sbsql.AppendLine($" vacc_lot, ");
                sbsql.AppendLine($" cym, ");
                sbsql.AppendLine($" sid, ");
                sbsql.AppendLine($" importdate, ");
                //sbsql.AppendLine($" updatedate, ");
                sbsql.AppendLine($" aid )");

                sbsql.AppendLine($" select ");
                sbsql.AppendLine($"  '270353'");
                sbsql.AppendLine($"  ,publcexpense ");
                sbsql.AppendLine($"  ,idays2");
                sbsql.AppendLine($"  ,hname");
                sbsql.AppendLine($"  ,numbering");
                sbsql.AppendLine($"  ,iname1");
                sbsql.AppendLine($"  ,iname2");
                sbsql.AppendLine($"  ,pname");
                sbsql.AppendLine($"  ,inum");
                sbsql.AppendLine($"  ,substring(sname,1,60)");
                sbsql.AppendLine($"  ,substring(sdoctor,1,20)");

                //数字からメーカー文字列に変換必要
                sbsql.AppendLine($"  ,case iname3 ");
                //sbsql.AppendLine($"   when '1' then '{strFiser}' ");
                //sbsql.AppendLine($"   when '2' then '{strAstra}' ");
                //sbsql.AppendLine($"   when '3' then '{strTakeda}' ");
                //sbsql.AppendLine($"   when '4' then '{strFiser5}' "); //20220608100512 furukawa st //メーカー名追加
                //sbsql.AppendLine($"   when '5' then '{strNova}' ");   //20220608100512 furukawa st //メーカー名追加
                //sbsql.AppendLine($"   when '6' then '{strComi}' ");   //20220913_2 ito //メーカー名追加
                //sbsql.AppendLine($"   when '7' then '{strSpike}' ");  //20220913_2 ito //メーカー名追加
                //sbsql.AppendLine($"   when '8' then '{strCom4}' ");   //20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"   else '' end ");
                sbsql.AppendLine($"  ,substring(iname4,1,20)");
                sbsql.AppendLine($"  ,cym");
                sbsql.AppendLine($"  ,sid");
                sbsql.AppendLine($"  ,(select importdate from dataimport where application.numbering=dataimport.f001number and application.cym=dataimport.cym)");
                sbsql.AppendLine($"  ,aid");

                sbsql.AppendLine($"  from application");
                sbsql.AppendLine($" where application.cym={cym} ");
                sbsql.AppendLine($"   and application.aapptype=6 ;");

                #endregion

                #region 2.dataexportにdataexportbeforeからnumberをコピー
                wf.LogPrint("2.dataexportにdataexportbeforeからnumberをコピー");
                sbsql.AppendLine($" delete from dataexport where cym={cym};");

                sbsql.AppendLine($" insert into dataexport (f000insurercode,f001number,cym) ");
                sbsql.AppendLine($" select '272035',number,cym from dataexportbefore group by number,cym;");

                #endregion

                #region 3.dataexportbeforeからdataexportにバーコード値,aidをコピー
                wf.LogPrint("3.dataexportbeforeからdataexportにバーコード値,aidをコピー");
                //予診は外す
                sbsql.AppendLine($" update dataexport set barcode1= barcode from dataexportbefore where times='1' and f001number=number and kind<>'1';");
                sbsql.AppendLine($" update dataexport set barcode2= barcode from dataexportbefore where times='2' and f001number=number and kind<>'1';");
                sbsql.AppendLine($" update dataexport set barcode3= barcode from dataexportbefore where times='3' and f001number=number and kind<>'1';");
                //バーコードと回数からaidを入れる
                sbsql.AppendLine($" update dataexport set aid1= a.aid from application a where a.hname=barcode1 and a.idays2='1' ; ");
                sbsql.AppendLine($" update dataexport set aid2= a.aid from application a where a.hname=barcode2 and a.idays2='2' ; ");
                sbsql.AppendLine($" update dataexport set aid3= a.aid from application a where a.hname=barcode3 and a.idays2='3' ; ");
                //20220816_1 ito st /////
                sbsql.AppendLine($" update dataexport set barcode4= barcode from dataexportbefore where times='4' and f001number=number and kind<>'1';");
                sbsql.AppendLine($" update dataexport set barcode5= barcode from dataexportbefore where times='5' and f001number=number and kind<>'1';");
                sbsql.AppendLine($" update dataexport set aid4= a.aid from application a where a.hname=barcode4 and a.idays2='4' ; ");
                sbsql.AppendLine($" update dataexport set aid5= a.aid from application a where a.hname=barcode5 and a.idays2='5' ; ");
                //20220816_1 ito end /////

                #endregion

                #region 4.dataexportのバーコード値1,2,3に対して納品データ用の値を入れる
                wf.LogPrint("4.dataexportのバーコード値1,2,3に対して納品データ用の値を入れる");
                //sbsql.Clear();
                sbsql.AppendLine($"  update dataexport set 										");
                sbsql.AppendLine($"         f002vacc_entrydate1     =a.iname1                   ");
                sbsql.AppendLine($"       , f003vacc_date1          =a.iname2                   ");
                sbsql.AppendLine($"       , f004vacc_ticketnum1     =a.pname                    ");
                sbsql.AppendLine($"       , f005vacc_insurercode1   =a.inum                     ");
                sbsql.AppendLine($"       , f006vacc_place1         =substring(a.sname,1,20)    ");
                sbsql.AppendLine($"       , f007vacc_doctor1        =substring(a.sdoctor,1,20)  ");
                sbsql.AppendLine($"       , f008vacc_maker1         =                           ");
                sbsql.AppendLine($"        case a.iname3                                        ");
                //sbsql.AppendLine($"        when '1' then '{strFiser}'                           ");
                //sbsql.AppendLine($"        when '2' then '{strAstra}'                           ");
                //sbsql.AppendLine($"        when '3' then '{strTakeda}'                          ");
                //sbsql.AppendLine($"        when '4' then '{strFiser5}'                          ");//20220608100605 furukawa st //メーカー名追加
                //sbsql.AppendLine($"        when '5' then '{strNova}'                            ");//20220608100605 furukawa ed //メーカー名追加
                //sbsql.AppendLine($"        when '6' then '{strComi}'                            ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '7' then '{strSpike}'                           ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '8' then '{strCom4}'                            ");//20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"        else ''                                              ");
                sbsql.AppendLine($"        end                                                  ");
                sbsql.AppendLine($"       , f009vacc_lot1      =a.iname4                        ");
                sbsql.AppendLine($"  from application a                                         ");
                sbsql.AppendLine($"  where a.hname=barcode1 and a.idays2='1' and a.cym={cym};  ");

                sbsql.AppendLine($"  update dataexport set 										");
                sbsql.AppendLine($"         f010vacc_entrydate2     =a.iname1                   ");
                sbsql.AppendLine($"       , f011vacc_date2          =a.iname2                   ");
                sbsql.AppendLine($"       , f012vacc_ticketnum2     =a.pname                    ");
                sbsql.AppendLine($"       , f013vacc_insurercode2   =a.inum                     ");
                sbsql.AppendLine($"       , f014vacc_place2         =substring(a.sname,1,20)    ");
                sbsql.AppendLine($"       , f015vacc_doctor2        =substring(a.sdoctor,1,20)  ");
                sbsql.AppendLine($"       , f016vacc_maker2         =                           ");
                sbsql.AppendLine($"        case a.iname3                                        ");
                //sbsql.AppendLine($"        when '1' then '{strFiser}'                           ");
                //sbsql.AppendLine($"        when '2' then '{strAstra}'                           ");
                //sbsql.AppendLine($"        when '3' then '{strTakeda}'                          ");
                //sbsql.AppendLine($"        when '4' then '{strFiser5}'                          ");//20220608100605 furukawa st //メーカー名追加
                //sbsql.AppendLine($"        when '5' then '{strNova}'                            ");//20220608100605 furukawa ed //メーカー名追加
                //sbsql.AppendLine($"        when '6' then '{strComi}'                            ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '7' then '{strSpike}'                           ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '8' then '{strCom4}'                            ");//20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"        else ''                                              ");
                sbsql.AppendLine($"        end                                                  ");
                sbsql.AppendLine($"       , f017vacc_lot2      =a.iname4                        ");
                sbsql.AppendLine($"  from application a                                         ");
                sbsql.AppendLine($"  where a.hname=barcode2 and a.idays2='2' and a.cym={cym};  ");

                sbsql.AppendLine($"  update dataexport set 										");
                sbsql.AppendLine($"         f018vacc_entrydate3     =a.iname1                   ");
                sbsql.AppendLine($"       , f019vacc_date3          =a.iname2                   ");
                sbsql.AppendLine($"       , f020vacc_ticketnum3     =a.pname                    ");
                sbsql.AppendLine($"       , f021vacc_insurercode3   =a.inum                     ");
                sbsql.AppendLine($"       , f022vacc_place3         =substring(a.sname,1,20)    ");
                sbsql.AppendLine($"       , f023vacc_doctor3        =substring(a.sdoctor,1,20)  ");
                sbsql.AppendLine($"       , f024vacc_maker3         =                           ");
                sbsql.AppendLine($"        case a.iname3                                        ");
                //sbsql.AppendLine($"        when '1' then '{strFiser}'                           ");
                //sbsql.AppendLine($"        when '2' then '{strAstra}'                           ");
                //sbsql.AppendLine($"        when '3' then '{strTakeda}'                          ");
                //sbsql.AppendLine($"        when '4' then '{strFiser5}'                          ");//20220608100605 furukawa st //メーカー名追加
                //sbsql.AppendLine($"        when '5' then '{strNova}'                            ");//20220608100605 furukawa ed //メーカー名追加
                //sbsql.AppendLine($"        when '6' then '{strComi}'                            ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '7' then '{strSpike}'                           ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '8' then '{strCom4}'                            ");//20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"        else ''                                              ");
                sbsql.AppendLine($"        end                                                  ");
                sbsql.AppendLine($"       , f025vacc_lot3      =a.iname4                        ");
                sbsql.AppendLine($"  from application a                                         ");
                sbsql.AppendLine($"  where a.hname=barcode3 and a.idays2='3' and a.cym={cym};  ");

                //20220816_1 ito st /////
                //4回目接種の場合
                sbsql.AppendLine($"  update dataexport set 										");
                sbsql.AppendLine($"         f026vacc_entrydate4     =a.iname1                   ");
                sbsql.AppendLine($"       , f027vacc_date4          =a.iname2                   ");
                sbsql.AppendLine($"       , f028vacc_ticketnum4     =a.pname                    ");
                sbsql.AppendLine($"       , f029vacc_insurercode4   =a.inum                     ");
                sbsql.AppendLine($"       , f030vacc_place4         =substring(a.sname,1,20)    ");
                sbsql.AppendLine($"       , f031vacc_doctor4        =substring(a.sdoctor,1,20)  ");
                sbsql.AppendLine($"       , f032vacc_maker4         =                           ");
                sbsql.AppendLine($"        case a.iname3                                        ");
                //sbsql.AppendLine($"        when '1' then '{strFiser}'                           ");
                //sbsql.AppendLine($"        when '2' then '{strAstra}'                           ");
                //sbsql.AppendLine($"        when '3' then '{strTakeda}'                          ");
                //sbsql.AppendLine($"        when '4' then '{strFiser5}'                          ");//20220608100605 furukawa st //メーカー名追加
                //sbsql.AppendLine($"        when '5' then '{strNova}'                            ");//20220608100605 furukawa ed //メーカー名追加
                //sbsql.AppendLine($"        when '6' then '{strComi}'                            ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '7' then '{strSpike}'                           ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '8' then '{strCom4}'                            ");//20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"        else ''                                              ");
                sbsql.AppendLine($"        end                                                  ");
                sbsql.AppendLine($"       , f033vacc_lot4      =a.iname4                        ");
                sbsql.AppendLine($"  from application a                                         ");
                sbsql.AppendLine($"  where a.hname=barcode4 and a.idays2='4' and a.cym={cym};  ");

                //5回目接種の場合
                sbsql.AppendLine($"  update dataexport set 										");
                sbsql.AppendLine($"         f034vacc_entrydate5     =a.iname1                   ");
                sbsql.AppendLine($"       , f035vacc_date5          =a.iname2                   ");
                sbsql.AppendLine($"       , f036vacc_ticketnum5     =a.pname                    ");
                sbsql.AppendLine($"       , f037vacc_insurercode5   =a.inum                     ");
                sbsql.AppendLine($"       , f038vacc_place5         =substring(a.sname,1,20)    ");
                sbsql.AppendLine($"       , f039vacc_doctor5        =substring(a.sdoctor,1,20)  ");
                sbsql.AppendLine($"       , f040vacc_maker5         =                           ");
                sbsql.AppendLine($"        case a.iname3                                        ");
                //sbsql.AppendLine($"        when '1' then '{strFiser}'                           ");
                //sbsql.AppendLine($"        when '2' then '{strAstra}'                           ");
                //sbsql.AppendLine($"        when '3' then '{strTakeda}'                          ");
                //sbsql.AppendLine($"        when '4' then '{strFiser5}'                          ");//20220608100605 furukawa st //メーカー名追加
                //sbsql.AppendLine($"        when '5' then '{strNova}'                            ");//20220608100605 furukawa ed //メーカー名追加
                //sbsql.AppendLine($"        when '6' then '{strComi}'                            ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '7' then '{strSpike}'                           ");//202209113_2 ito //メーカー名追加
                //sbsql.AppendLine($"        when '8' then '{strCom4}'                            ");//20221014_1 ito //メーカー名追加
                sbsql.AppendLine(vaccMaker.ToString());
                sbsql.AppendLine($"        else ''                                              ");
                sbsql.AppendLine($"        end                                                  ");
                sbsql.AppendLine($"       , f041vacc_lot5      =a.iname4                        ");
                sbsql.AppendLine($"  from application a                                         ");
                sbsql.AppendLine($"  where a.hname=barcode5 and a.idays2='5' and a.cym={cym};  ");
                //20220816_1 ito ed /////

                using (DB.Command updcmd1 = DB.Main.CreateCmd(sbsql.ToString(), tran))
                {
                    if (!updcmd1.TryExecuteNonQuery()) System.Windows.Forms.MessageBox.Show("実行失敗");
                }

                #endregion

                tran.Commit();
                wf.LogPrint($"出力テーブル登録完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmddel.Dispose();
            }
        }

        /// <summary>
        /// 更新レコードに該当する券番号のレコードをdataexportからコピーしてくる(更新レコードのみデータ)
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CreateDataExport2(int cym, WaitForm wf)
        {

            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from dataexport2 where cym='{cym}'", tran);

            try
            {
                cmddel.TryExecuteNonQuery();

                StringBuilder sbsql = new StringBuilder();

                //券番号、メーカー、ロットが無いレコードの番号をdataimport（提供データから）入れる（変わるのはこれぐらいしかなさそう（森安さん2022/03/16）
                //20220816_1 ito st /////
                sbsql.AppendLine($" insert into dataexport2 (f001number,cym,aid1,aid2,aid3,aid4,aid5,barcode1,barcode2,barcode3,barcode4,barcode5)  ");
                sbsql.AppendLine($" select e.f001number,e.cym,e.aid1,aid2,aid3,aid4,aid5,barcode1,barcode2,barcode3,barcode4,barcode5 ");
                //sbsql.AppendLine($" insert into dataexport2 (f001number,cym,aid1,aid2,aid3,barcode1,barcode2,barcode3)  ");
                //sbsql.AppendLine($" select e.f001number,e.cym,e.aid1,aid2,aid3,barcode1,barcode2,barcode3 ");
                //20220816_1 ito ed /////
                sbsql.AppendLine($" from dataimport i inner join dataexport e on ");
                sbsql.AppendLine($" i.f001number=e.f001number  ");
                sbsql.AppendLine($" and i.cym=e.cym ");
                sbsql.AppendLine($" where i.cym={cym} ");
                sbsql.AppendLine($" and  ");
                sbsql.AppendLine($" ( ");
                sbsql.AppendLine($" (i.f004vacc_ticketnum1 is null and i.f008vacc_maker1 is null and i.f009vacc_lot1 is null) ");
                //20220816_1 ito st /////なんで1回目だけnot null条件？
                //sbsql.AppendLine($" and (e.f004vacc_ticketnum1 is not null and e.f008vacc_maker1 is not null and e.f009vacc_lot1 is not null) ");
                sbsql.AppendLine($" and (e.f004vacc_ticketnum1 <> '' and e.f008vacc_maker1 <> '' and e.f009vacc_lot1 <> '') "); sbsql.AppendLine($" )or( ");
                //20220816_1 ito ed /////
                sbsql.AppendLine($" (i.f012vacc_ticketnum2 is null and i.f016vacc_maker2 is null and i.f017vacc_lot2 is null) ");
                sbsql.AppendLine($" and (e.f012vacc_ticketnum2 <> '' and e.f016vacc_maker2 <> '' and e.f017vacc_lot2 <> '') ");
                sbsql.AppendLine($" )or( ");
                sbsql.AppendLine($" (i.f020vacc_ticketnum3 is null and i.f024vacc_maker3 is null and i.f025vacc_lot3 is null) ");
                sbsql.AppendLine($" and (e.f020vacc_ticketnum3 <> '' and e.f024vacc_maker3 <> '' and e.f025vacc_lot3 <> '') ");
                //20220816_1 ito st /////
                //sbsql.AppendLine($" ) ");
                //sbsql.AppendLine($" group by e.f001number,e.cym,e.aid1,aid2,aid3,barcode1,barcode2,barcode3 ;");
                sbsql.AppendLine($" )or( ");
                sbsql.AppendLine($" (i.f028vacc_ticketnum4 is null and i.f032vacc_maker4 is null and i.f033vacc_lot4 is null) ");
                sbsql.AppendLine($" and (e.f028vacc_ticketnum4 <> '' and e.f032vacc_maker4 <> '' and e.f033vacc_lot4 <> '') ");
                sbsql.AppendLine($" )or( ");
                sbsql.AppendLine($" (i.f036vacc_ticketnum5 is null and i.f040vacc_maker5 is null and i.f041vacc_lot5 is null) ");
                sbsql.AppendLine($" and (e.f036vacc_ticketnum5 <> '' and e.f040vacc_maker5 <> '' and e.f041vacc_lot5 <> '') ");
                sbsql.AppendLine($" ) ");
                sbsql.AppendLine($" group by e.f001number,e.cym,e.aid1,aid2,aid3,aid4,aid5,barcode1,barcode2,barcode3,barcode4,barcode5 ;");
                //20220816_1 ito ed /////

                using (DB.Command cmd = new DB.Command(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmddel.Dispose();
            }
        }


        /// <summary>
        /// dataexport2の更新
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool UpdateDataExport2(int cym, WaitForm wf)
        {
            bool flgInsert = false;//新規追加時フラグ

            StringBuilder sbsql = new StringBuilder();

            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            try
            {
                //入力したapplicationをnumberingで抽出し、新規登録・更新する
                wf.LogPrint("予診票取得");

                List<App> list = new List<App>();
                list = App.GetApps(cym);
                list.Sort((x, y) => x.HihoName.CompareTo(y.HihoName));

                wf.InvokeValue = 0;
                wf.SetMax(list.Count);


                foreach (App app in list)
                {
                    if (app.AppType != APP_TYPE.柔整) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //券番号、cymで合致するレコードがあれば更新する。なければ放置（dataexportの中からdataimportに合致するレコードだけなので）
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($"select * from dataexport2 where f001number='{app.Numbering}' and cym={cym}");

                    int cntRecord = 0;
                    using (DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString()))
                    {
                        cntRecord = cmd.TryExecuteReaderList().Count;
                    }

                    if (cntRecord == 0) flgInsert = true;
                    else flgInsert = false;

                    //新規の予診票入力対応

                    if (flgInsert)
                    {
                        //ここではdataexportの中からdataimportの更新レコードだけを作成するのでレコード自体の新規追加はない
                        continue;

                        wf.LogPrint($"更新データへの新規追加 バーコード値: {app.HihoName}");

                    }
                    wf.InvokeValue++;
                }

                //不要な行（券番号がない）を削除
                sbsql.Clear();
                //20220816_1 ito st /////
                sbsql.AppendLine($"delete from dataexport2 where cym={cym} and f004vacc_ticketnum1 = '' and f012vacc_ticketnum2= '' and f020vacc_ticketnum3= '' " +
                    $" and f028vacc_ticketnum4= '' and f036vacc_ticketnum5= '';");
                //sbsql.AppendLine($"delete from dataexport2 where cym={cym} and f004vacc_ticketnum1 ='' and f012vacc_ticketnum2='' and f020vacc_ticketnum3='';");
                //20220816_1 ito ed /////
                using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
        }


        /*
        //20221206_1 参照がゼロだったためコメントアウト

        /// <summary>
        /// 出力用テーブルに登録(予診票全件データ)
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// {<param name="wf">waitform</param>
        /// <returns></returns>
        private static bool InsertExportTable1(int cym ,WaitForm wf)
        {
            
            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();
                        
            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from dataexport where cym='{cym}'", tran);
            
            try
            {
                cmddel.TryExecuteNonQuery();
            
                //1.dataimportから券番号を先に登録
                //2.appから1回目の券番号を以て1を更新
                //3.appから2回目の券番号を以て1を更新
                //4.appから3回目の券番号を以て1を更新
                
                StringBuilder sbsql = new StringBuilder();

                wf.LogPrint("1.dataimportから券番号を先に登録");

                sbsql.AppendLine(" insert into dataexport (");
                sbsql.AppendLine(" f000insurercode,f001number,cym ");
                sbsql.AppendLine(" ) ");
                sbsql.AppendLine(" select  ");
                sbsql.AppendLine($" '272035',d.f001number,'{cym}'  ");//保険者番号は固定
                sbsql.AppendLine("  from dataimport d inner join application a on  ");
                sbsql.AppendLine("  d.f001number=a.numbering ");

                //20220214172548 furukawa st ////////////////////////
                //cym追加
                
                sbsql.AppendLine("  and d.cym=a.cym ");
                //20220214172548 furukawa ed ////////////////////////

                sbsql.AppendLine($" where a.cym={cym}  ");

                //20220301162558 furukawa st ////////////////////////
                //dataexportはApplicationと違いAID管理でなくf001Number管理で、1つのnumberにaidが複数該当し、
                //行数が違うのでグループしないと重複が出るように見える
                
                sbsql.AppendLine($" group by   ");
                sbsql.AppendLine($" d.f001number  ");
                //20220301162558 furukawa ed ////////////////////////


                using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }


                wf.LogPrint("appから1回目の券番号を以て1を更新");
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine("  update dataexport set ");
                sbsql.AppendLine("  f002vacc_entrydate1     =a.iname1 ");
                sbsql.AppendLine(", f003vacc_date1          =a.iname2 ");
                sbsql.AppendLine(", f004vacc_ticketnum1     =a.pname ");
                sbsql.AppendLine(", f005vacc_insurercode1   =a.inum ");

                //20220225102009 furukawa st ////////////////////////
                //文字数は20まで
                
                sbsql.AppendLine(", f006vacc_place1         =substring(a.sname,0,21) ");
                sbsql.AppendLine(", f007vacc_doctor1        =substring(a.sdoctor,0,21) ");
                //sbsql.AppendLine(", f006vacc_place1         =a.sname ");
                //sbsql.AppendLine(", f007vacc_doctor1        =a.sdoctor ");
                //20220225102009 furukawa ed ////////////////////////


                sbsql.AppendLine(", f008vacc_maker1         = ");
                sbsql.AppendLine(" case a.iname3  ");
                sbsql.AppendLine($" when '1' then '{strFiser}' ");
                sbsql.AppendLine($" when '2' then '{strAstra}' ");
                sbsql.AppendLine($" when '3' then '{strTakeda}' ");
                //20220608103238 furukawa st ////////////////////////
                //メーカー名追加
                
                sbsql.AppendLine($" when '4' then '{strFiser5}' ");
                sbsql.AppendLine($" when '5' then '{strNova}' ");
                //20220608103238 furukawa ed ////////////////////////
                sbsql.AppendLine($" else '' ");
                sbsql.AppendLine($" end ");
                

                sbsql.AppendLine(", f009vacc_lot1           =a.iname4 ");
                sbsql.AppendLine(", aid1           =a.aid ");
                sbsql.AppendLine(" from application a  ");
                sbsql.AppendLine(" where ");
                sbsql.AppendLine(" f001number				= a.numbering ");//券番号
                sbsql.AppendLine(" and  ");
                sbsql.AppendLine(" a.idays2=1 ");//1回目

                //20220214170743 furukawa st ////////////////////////
                //cym追加
                
                sbsql.AppendLine($" and a.cym={cym} ");
                //20220214170743 furukawa ed ////////////////////////


                using (DB.Command updcmd1 = DB.Main.CreateCmd(sbsql.ToString(),tran)) {
                    updcmd1.TryExecuteNonQuery();
                }

                wf.LogPrint("appから2回目の券番号を以て1を更新");

                sbsql.Remove(0,sbsql.ToString().Length);
                sbsql.AppendLine("  update dataexport set ");
                sbsql.AppendLine("  f010vacc_entrydate2     =a.iname1 ");
                sbsql.AppendLine(", f011vacc_date2          =a.iname2 ");
                sbsql.AppendLine(", f012vacc_ticketnum2     =a.pname ");
                sbsql.AppendLine(", f013vacc_insurercode2   =a.inum ");

                //20220225102028 furukawa st ////////////////////////
                //文字数は20まで
                
                sbsql.AppendLine(", f014vacc_place2         =substring(a.sname,0,21) ");
                sbsql.AppendLine(", f015vacc_doctor2        =substring(a.sdoctor,0,21) ");
                //sbsql.AppendLine(", f014vacc_place2         =a.sname ");
                //sbsql.AppendLine(", f015vacc_doctor2        =a.sdoctor ");
                //20220225102028 furukawa ed ////////////////////////


                sbsql.AppendLine(", f016vacc_maker2         = ");
                sbsql.AppendLine(" case a.iname3  ");
                sbsql.AppendLine($" when '1' then '{strFiser}' ");
                sbsql.AppendLine($" when '2' then '{strAstra}' ");
                sbsql.AppendLine($" when '3' then '{strTakeda}' ");
                //20220608103329 furukawa st ////////////////////////
                //メーカー名追加
                
                sbsql.AppendLine($" when '4' then '{strFiser5}' ");
                sbsql.AppendLine($" when '5' then '{strNova}' ");
                //20220608103329 furukawa ed ////////////////////////

                sbsql.AppendLine($" else '' ");
                sbsql.AppendLine($" end ");

                sbsql.AppendLine(", f017vacc_lot2           =a.iname4 ");
                sbsql.AppendLine(", aid2           =a.aid ");
                sbsql.AppendLine(" from application a  ");
                sbsql.AppendLine(" where ");
                sbsql.AppendLine(" f001number				= a.numbering ");//券番号
                sbsql.AppendLine(" and  ");
                sbsql.AppendLine(" a.idays2=2 ");//2回目

                //20220214170719 furukawa st ////////////////////////
                //cym追加
                
                sbsql.AppendLine($" and a.cym={cym} ");
                //20220214170719 furukawa ed ////////////////////////

                using (DB.Command updcmd2 = DB.Main.CreateCmd(sbsql.ToString(),tran))
                {
                    updcmd2.TryExecuteNonQuery();
                }

                wf.LogPrint("appから3回目の券番号を以て1を更新");

                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine("  update dataexport set ");
                sbsql.AppendLine("  f018vacc_entrydate3     = a.iname1 ");
                sbsql.AppendLine(", f019vacc_date3          = a.iname2 ");
                sbsql.AppendLine(", f020vacc_ticketnum3     = a.pname ");
                sbsql.AppendLine(", f021vacc_insurercode3   = a.inum ");

                //20220225102042 furukawa st ////////////////////////
                //文字数は20まで
                
                sbsql.AppendLine(", f022vacc_place3         = substring(a.sname,0,21) ");
                sbsql.AppendLine(", f023vacc_doctor3        = substring(a.sdoctor,0,21) ");
                //sbsql.AppendLine(", f022vacc_place3         = a.sname ");
                //sbsql.AppendLine(", f023vacc_doctor3        = a.sdoctor ");
                //20220225102042 furukawa ed ////////////////////////


                sbsql.AppendLine(", f024vacc_maker3         = ");
                sbsql.AppendLine(" case a.iname3  ");
                sbsql.AppendLine($" when '1' then '{strFiser}' ");
                sbsql.AppendLine($" when '2' then '{strAstra}' ");
                sbsql.AppendLine($" when '3' then '{strTakeda}' ");
                //20220608103403 furukawa st ////////////////////////
                //メーカー名追加
                
                sbsql.AppendLine($" when '4' then '{strFiser5}' ");
                sbsql.AppendLine($" when '5' then '{strNova}' ");
                //20220608103403 furukawa ed ////////////////////////
                sbsql.AppendLine($" else '' ");
                sbsql.AppendLine($" end ");

                sbsql.AppendLine(", f025vacc_lot3           = a.iname4 ");
                sbsql.AppendLine(", aid3           =a.aid ");
                sbsql.AppendLine(" from application a  ");


                sbsql.AppendLine(" where ");
                sbsql.AppendLine(" f001number				= a.numbering ");//券番号
                sbsql.AppendLine(" and  ");
                sbsql.AppendLine(" a.idays2=3 ");//3回目

                //20220214170634 furukawa st ////////////////////////
                //cym追加
                
                sbsql.AppendLine($" and a.cym={cym} ");
                //20220214170634 furukawa ed ////////////////////////


                using (DB.Command updcmd3 = DB.Main.CreateCmd(sbsql.ToString(), tran))
                {
                    updcmd3.TryExecuteNonQuery();
                }

                tran.Commit();
                wf.LogPrint($"出力テーブル登録完了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
              
                cmddel.Dispose();
            }

               
        }

        /// <summary>
        /// 更新レコードに該当する券番号のレコードをimportdataからコピーしてくる(更新レコードのみデータ)
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool InsertExportTable2(int cym, WaitForm wf)
        {
            //1.更新レコードに該当する券番号のレコードをimportdataからコピーしてくる
              
            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from dataexport2 where cym='{cym}'", tran);
            

            try
            {
                cmddel.TryExecuteNonQuery();

                //1.更新レコードに該当する券番号のレコードをimportdataからコピーしてくる
                StringBuilder sbsql = new StringBuilder();
                sbsql.AppendLine($"insert into dataexport2 (");
                sbsql.AppendLine($" exportid,f001number,f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,");
                sbsql.AppendLine($" f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1,");
                sbsql.AppendLine($" f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,");
                sbsql.AppendLine($" f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2,f018vacc_entrydate3,f019vacc_date3,");
                sbsql.AppendLine($" f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,");
                sbsql.AppendLine($" f025vacc_lot3,cym,sid,importdate,updatedate ");
                sbsql.AppendLine($" ) select * from dataimport where updatedate <>'0001-01-01' and cym={cym}");
                using (DB.Command cmd = new DB.Command(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmddel.Dispose();
            }
        }

        /// <summary>
        /// 請求一覧テーブルを出力/保管用にこちらにコピー
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool InsertExportTable_chargelist(int cym, WaitForm wf)
        {
            //1.更新レコードに該当する券番号のレコードをimportdataからコピーしてくる

            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from chargelistexport where cym='{cym}'", tran);
            
            try
            {
                cmddel.TryExecuteNonQuery();

                //請求一覧テーブルを出力/保管用にこちらにコピー
                using (DB.Command cmd = new DB.Command(
                    //20220214174048 furukawa st ////////////////////////
                    //cym追加
                    
                    $"insert into chargelistexport select * from chargelist where cym='{cym}';", tran))
                //"insert into chargelistexport select * from chargelist;", tran))
                //20220214174048 furukawa ed ////////////////////////
                {
                    cmd.TryExecuteNonQuery();
                }
                //cym更新
                using (DB.Command cmd = new DB.Command(
                    $"update chargelistexport set cym={cym} where cym =0",tran))
                {
                    cmd.TryExecuteNonQuery();
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmddel.Dispose();
            }
        }
        
        */


        /// <summary>
        /// 予診票全件CSVファイル出力
        /// </summary>
        /// <returns></returns>
        private static bool CreateCSVFile1(string strFolder,int cym,WaitForm wf)
        {
                  
            StringBuilder sbsql = new StringBuilder();
            sbsql.AppendLine("select ");
            
            //20220221170405 furukawa st ////////////////////////
			//仕様間違いで予診票全件データには保険者番号不要　2022/02/18
            sbsql.Append(" f001number");
               
            //		sbsql.Append("  f000insurercode");
            //		sbsql.Append(" ,f001number");
			//20220221170405 furukawa ed ////////////////////////

            sbsql.Append(" ,f002vacc_entrydate1");
            sbsql.Append(" ,f003vacc_date1");
            sbsql.Append(" ,f004vacc_ticketnum1");
            sbsql.Append(" ,f005vacc_insurercode1");
            sbsql.Append(" ,f006vacc_place1");
            sbsql.Append(" ,f007vacc_doctor1");
            sbsql.Append(" ,f008vacc_maker1");
            sbsql.Append(" ,f009vacc_lot1");
            sbsql.Append(" ,f010vacc_entrydate2");
            sbsql.Append(" ,f011vacc_date2");
            sbsql.Append(" ,f012vacc_ticketnum2");
            sbsql.Append(" ,f013vacc_insurercode2");
            sbsql.Append(" ,f014vacc_place2");
            sbsql.Append(" ,f015vacc_doctor2");
            sbsql.Append(" ,f016vacc_maker2");
            sbsql.Append(" ,f017vacc_lot2");
            sbsql.Append(" ,f018vacc_entrydate3");
            sbsql.Append(" ,f019vacc_date3");
            sbsql.Append(" ,f020vacc_ticketnum3");
            sbsql.Append(" ,f021vacc_insurercode3");
            sbsql.Append(" ,f022vacc_place3");
            sbsql.Append(" ,f023vacc_doctor3");
            sbsql.Append(" ,f024vacc_maker3");
            sbsql.Append(" ,f025vacc_lot3");

            //20220816_1 ito st /////
            sbsql.Append(" ,f026vacc_entrydate4");
            sbsql.Append(" ,f027vacc_date4");
            sbsql.Append(" ,f028vacc_ticketnum4");
            sbsql.Append(" ,f029vacc_insurercode4");
            sbsql.Append(" ,f030vacc_place4");
            sbsql.Append(" ,f031vacc_doctor4");
            sbsql.Append(" ,f032vacc_maker4");
            sbsql.Append(" ,f033vacc_lot4");
            sbsql.Append(" ,f034vacc_entrydate5");
            sbsql.Append(" ,f035vacc_date5");
            sbsql.Append(" ,f036vacc_ticketnum5");
            sbsql.Append(" ,f037vacc_insurercode5");
            sbsql.Append(" ,f038vacc_place5");
            sbsql.Append(" ,f039vacc_doctor5");
            sbsql.Append(" ,f040vacc_maker5");
            sbsql.Append(" ,f041vacc_lot5");
            //20220816_1 ito ed /////

            sbsql.AppendLine(" from dataexport ");
            sbsql.AppendLine($" where cym={cym} ");

            //20220319192644 furukawa st ////////////////////////
            //券番号順
            
            sbsql.AppendLine(" order by f001number ");
            //sbsql.AppendLine(" order by exportid ");
            //20220319192644 furukawa ed ////////////////////////


            DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString());
            try
            {
                lstExport.Clear();
                wf.InvokeValue = 0;
                //テーブルからロード
                wf.LogPrint("dataexportテーブルよりロード");
                var list=cmd.TryExecuteReaderList();
                foreach(var item in list)
                {
                    export e = new export();                    
                    StringBuilder sb = new StringBuilder();
                    for (int c = 0; c < item.Length; c++)
                    {
                        sb.Append($"{item[c]},");
                    }
                    sb = sb.Remove(sb.ToString().Length - 1, 1);
                    lstExport.Add(sb.ToString());
                }

                wf.LogPrint("dataexportテーブルの内容をCSVファイルに出力");

                //総ファイル数＝テーブル行数/10万　の切り上げ
                decimal allfiles = System.Math.Ceiling(decimal.Parse(lstExport.Count.ToString()) / decimal.Parse("100000"));
                
                int cntFile = 1;
                
                //ファイル名
                string strOutPutFileName = $"{strFolder}\\272035_{DateTime.Now.ToString("yyMMddHHmmss")}_{cntFile}of{allfiles}_予診票全件.csv";

                //1番目のファイル

                //20220301162715 furukawa st ////////////////////////
                //UTF8BOMなし
                System.Text.UTF8Encoding enc = new UTF8Encoding(false);
                System.IO.StreamWriter sw =
                    new System.IO.StreamWriter(strOutPutFileName, false, enc);
                //System.IO.StreamWriter sw = 
                //    new System.IO.StreamWriter(strOutPutFileName, false, System.Text.Encoding.GetEncoding("utf-8"));
                //20220301162715 furukawa ed ////////////////////////

                int cntRow = 0;
                wf.SetMax(lstExport.Count);

                foreach(string s in lstExport)
                {
                    sw.WriteLine(s);
                    cntRow++;
                    wf.InvokeValue++;

                    //10万行で1ファイルとする仕様
                    if (cntRow > 100000)
                    {
                        sw.Close();
                        cntFile++;
                        cntRow = 0;
                        strOutPutFileName = $"272035_{DateTime.Now.ToString("yyMMddHHmmss")}_{cntFile}of{allfiles}_予診票全件.csv";

                        //20220301162744 furukawa st ////////////////////////
                        //UTF8BOMなし
                        sw = new System.IO.StreamWriter(strOutPutFileName, false, enc);
                        //sw = new System.IO.StreamWriter(strOutPutFileName, false, System.Text.Encoding.GetEncoding("utf-8"));
                        //20220301162744 furukawa ed ////////////////////////

                        wf.LogPrint(strOutPutFileName);
                    }
                }
                sw.Close();
                wf.LogPrint("予診票全件CSVファイル作成完了");

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// dataimportで更新したレコードだけをCSVファイル出力
        /// </summary>
        /// <param name="strFolder"></param>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CreateCSVFile2(string strFolder, int cym, WaitForm wf)
        {
            List<string> lstExport = new List<string>();
            StringBuilder sbsql = new StringBuilder();
            sbsql.AppendLine("select ");            
			sbsql.Append(" '272035' ,");    //20220221170446 furukawa st //仕様間違いで更新データに保険者番号必要　2022/02/18
            sbsql.Append("  lpad(f001number,10,'0')");  //20220310090642 furukawa st //2列めの券番号は10桁ゼロ埋め
            sbsql.Append(" ,f002vacc_entrydate1");
            sbsql.Append(" ,f003vacc_date1");
            sbsql.Append(" ,f004vacc_ticketnum1");
            sbsql.Append(" ,f005vacc_insurercode1");
            sbsql.Append(" ,f006vacc_place1");
            sbsql.Append(" ,f007vacc_doctor1");
            sbsql.Append(" ,f008vacc_maker1");
            sbsql.Append(" ,f009vacc_lot1");
            sbsql.Append(" ,f010vacc_entrydate2");
            sbsql.Append(" ,f011vacc_date2");
            sbsql.Append(" ,f012vacc_ticketnum2");
            sbsql.Append(" ,f013vacc_insurercode2");
            sbsql.Append(" ,f014vacc_place2");
            sbsql.Append(" ,f015vacc_doctor2");
            sbsql.Append(" ,f016vacc_maker2");
            sbsql.Append(" ,f017vacc_lot2");
            sbsql.Append(" ,f018vacc_entrydate3");
            sbsql.Append(" ,f019vacc_date3");
            sbsql.Append(" ,f020vacc_ticketnum3");
            sbsql.Append(" ,f021vacc_insurercode3");
            sbsql.Append(" ,f022vacc_place3");
            sbsql.Append(" ,f023vacc_doctor3");
            sbsql.Append(" ,f024vacc_maker3");
            sbsql.Append(" ,f025vacc_lot3");

            //20220816_1 ito st /////
            sbsql.Append(" ,f026vacc_entrydate4");
            sbsql.Append(" ,f027vacc_date4");
            sbsql.Append(" ,f028vacc_ticketnum4");
            sbsql.Append(" ,f029vacc_insurercode4");
            sbsql.Append(" ,f030vacc_place4");
            sbsql.Append(" ,f031vacc_doctor4");
            sbsql.Append(" ,f032vacc_maker4");
            sbsql.Append(" ,f033vacc_lot4");
            sbsql.Append(" ,f034vacc_entrydate5");
            sbsql.Append(" ,f035vacc_date5");
            sbsql.Append(" ,f036vacc_ticketnum5");
            sbsql.Append(" ,f037vacc_insurercode5");
            sbsql.Append(" ,f038vacc_place5");
            sbsql.Append(" ,f039vacc_doctor5");
            sbsql.Append(" ,f040vacc_maker5");
            sbsql.Append(" ,f041vacc_lot5");
            //20220816_1 ito ed /////

            sbsql.AppendLine(" from dataexport2 ");
            sbsql.AppendLine($" where cym={cym} ");
            sbsql.AppendLine($" and updatedate<>'0001-01-01' ");            
            sbsql.AppendLine(" order by lpad(f001number,10,'0') "); //20220319213610 furukawa st //券番号順

            DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString());
            try
            {
                //テーブルからロード
                wf.LogPrint("dataexport2テーブルよりロード");
                var list = cmd.TryExecuteReaderList();
                foreach (var item in list)
                {
                    export e = new export();
                    StringBuilder sb = new StringBuilder();
                    for (int c = 0; c < item.Length; c++)
                    {
                        sb.Append($"{item[c]},");
                    }
                    sb = sb.Remove(sb.ToString().Length - 1, 1);
                    lstExport.Add(sb.ToString());
                }

                wf.LogPrint("dataexport2テーブルの内容をCSVファイルに出力");

                //総ファイル数＝テーブル行数/10万　の切り上げ
                decimal allfiles = System.Math.Ceiling(decimal.Parse(lstExport.Count.ToString()) / decimal.Parse("100000"));
                int cntFile = 1;

                //ファイル名
                string strOutPutFileName = $"{strFolder}\\272035_{DateTime.Now.ToString("yyMMddHHmmss")}_{cntFile}of{allfiles}_更新分のみ.csv";
                //1番目のファイル

                //20220301162821 furukawa st ////////////////////////
                //UTF8BOMなし
                System.Text.UTF8Encoding enc = new UTF8Encoding(false);
                System.IO.StreamWriter sw =
                    new System.IO.StreamWriter(strOutPutFileName, false, enc);
                //System.IO.StreamWriter sw =
                //    new System.IO.StreamWriter(strOutPutFileName, false, System.Text.Encoding.GetEncoding("utf-8"));
                //20220301162821 furukawa ed ////////////////////////

                int cntRow = 0;
                wf.SetMax(lstExport.Count);
                wf.InvokeValue = 0;

                foreach (string s in lstExport)
                {
                    sw.WriteLine(s);
                    cntRow++;

                    wf.InvokeValue++;
                    //10万行で1ファイルとする仕様
                    if (cntRow > 100000)
                    {
                        sw.Close();
                        cntFile++;
                        cntRow = 0;
                        strOutPutFileName = $"272035_{DateTime.Now.ToString("yyMMddHHmmss")}_{cntFile}of{allfiles}_更新分のみ.csv";

                        //20220301162851 furukawa st ////////////////////////
                        //UTF8BOMなし
                        
                        sw = new System.IO.StreamWriter(strOutPutFileName, false, enc);
                        //sw = new System.IO.StreamWriter(strOutPutFileName, false, System.Text.Encoding.GetEncoding("utf-8"));
                        //20220301162851 furukawa ed ////////////////////////


                        wf.LogPrint(strOutPutFileName);
                    }
                    
                }
                sw.Close();
                wf.LogPrint("更新レコード分CSVファイル作成完了");


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

                cmd.Dispose();
            }
        }



        /// <summary>
        /// 予診票画像出力
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(int cym, string strDir, WaitForm wf)
        {
            
            string strImageDir = $"{strDir}\\{cym}\\Images";           //tifコピー先フォルダ

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            

            try
            {
                List <App> lstApp=App.GetApps(cym);
                wf.SetMax(lstApp.Count);

                foreach (App a in lstApp)
                {
                    //20220214170951 furukawa st ////////////////////////
                    //中断処理
                    
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        wf.LogPrint("画像出力中止");
                        return false;
                    }
                    //20220214170951 furukawa ed ////////////////////////

                    if (a.AppType ==APP_TYPE.長期) continue;

                    string strDestFolderName = string.Empty;
                    string strDestFileName = string.Empty;
                    
                    //フォルダはyyyy-MM-dd出力\cym\Images\NoB or 医療機関等コード
                    strDestFolderName = a.HihoNum!=string.Empty ? $"{strImageDir}\\{a.HihoNum}" : $"{strImageDir}\\{a.ClinicNum}";
                    //ファイル名はバーコード番号
                    strDestFileName=$"{strDestFolderName}\\{a.HihoName}.tif";

                    //リストに持たせてエクセル作成時使う                    
                    lstDestImageFileName.Add(strDestFileName);

                    if (!System.IO.Directory.Exists(strDestFolderName)) System.IO.Directory.CreateDirectory(strDestFolderName);

                    fc.FileCopy(a.GetImageFullPath(), strDestFileName);
                    wf.LogPrint($"AID:{a.Aid} 元ファイル名：{a.GetImageFullPath()} 新ファイル名:{strDestFileName}");
                    wf.InvokeValue++;
                

                }

                wf.LogPrint("画像出力完了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        /// <summary>
        /// 検索ブックテーブル作成
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool CreateDataExportFindTable(WaitForm wf, int cym)
        {
            wf.LogPrint("検索ブックテーブル作成");

            StringBuilder sbsql = new StringBuilder();
            DB.Transaction tran = DB.Main.CreateTransaction();

            try
            {
                sbsql.AppendLine("truncate table dataexportfind");
                using (DB.Command cmd = new DB.Command(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }

                #region applicationからdataexportfindにコピー
                //納品データに関係なく、画像データはすべて検索できないといけないので、Applicationから取得するしか無い

                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($" INSERT INTO dataexportfind( ");
                sbsql.AppendLine($"  insurercode");
                sbsql.AppendLine($" ,kind");
                sbsql.AppendLine($" ,times");
                sbsql.AppendLine($" ,barcode");
                sbsql.AppendLine($" ,number");
                sbsql.AppendLine($" ,vacc_entrydate");
                sbsql.AppendLine($" ,vacc_date");
                sbsql.AppendLine($" ,vacc_ticketnum");
                sbsql.AppendLine($" ,vacc_insurercode");
                sbsql.AppendLine($" ,vacc_place");
                sbsql.AppendLine($" ,vacc_doctor");
                sbsql.AppendLine($" ,vacc_maker");
                sbsql.AppendLine($" ,vacc_lot");
                sbsql.AppendLine($" ,tif");
                sbsql.AppendLine($" ,cym");
                sbsql.AppendLine($" ,sid");
                sbsql.AppendLine($" ,aid");
                sbsql.AppendLine($" ,box_no) ");    //202208151720 ito st //箱番号追加

                sbsql.AppendLine($" select ");
                sbsql.AppendLine($"  '270353'");
                sbsql.AppendLine($"  ,publcexpense ");
                sbsql.AppendLine($"  ,idays2");
                sbsql.AppendLine($"  ,hname");
                sbsql.AppendLine($"  ,numbering");
                sbsql.AppendLine($"  ,iname1");
                sbsql.AppendLine($"  ,iname2");
                sbsql.AppendLine($"  ,pname");
                sbsql.AppendLine($"  ,inum");
                sbsql.AppendLine($"  ,substring(sname,1,60)");
                sbsql.AppendLine($"  ,substring(sdoctor,1,20)");

                //数字からメーカー文字列に変換必要
                sbsql.AppendLine($"  ,case iname3 ");
                sbsql.AppendLine($"   when '1' then '{strFiser}' ");
                sbsql.AppendLine($"   when '2' then '{strAstra}' ");
                sbsql.AppendLine($"   when '3' then '{strTakeda}' ");
                sbsql.AppendLine($"   when '4' then '{strFiser5}' "); //20220608101245 furukawa st //メーカー名追加 
                sbsql.AppendLine($"   when '5' then '{strNova}' ");   //20220608101245 furukawa st //メーカー名追加 
                sbsql.AppendLine($"   when '6' then '{strComi}' ");   //20220913_2 ito //メーカー名追加 
                sbsql.AppendLine($"   when '7' then '{strSpike}' ");  //20220913_2 ito //メーカー名追加 
                sbsql.AppendLine($"   when '8' then '{strCom4}' ");   //20221014_1 ito //メーカー名追加
                sbsql.AppendLine($"   when '9' then '{strCom0}'   ");   //20221201_1 ito //メーカー名追加
                sbsql.AppendLine($"   when '10' then '{strSpike4}'   ");//20221201_1 ito //メーカー名追加
                //ワクチンメーカー追加③
                sbsql.AppendLine($"   else '' end ");

                sbsql.AppendLine($"  ,substring(iname4,1,20)");

                sbsql.AppendLine($" ,(select ");
                sbsql.AppendLine($" case ");
                sbsql.AppendLine($" when a.hnum<>'' then a.hnum || '/' || a.hname || '.tif'");
                sbsql.AppendLine($" when a.hnum='' then a.sid || '/' || a.hname || '.tif'");
                sbsql.AppendLine($" end ");
                sbsql.AppendLine($" from application a");
                sbsql.AppendLine($" where a.aid=application.aid)");
                sbsql.AppendLine($"  ,cym");
                sbsql.AppendLine($"  ,sid");
                sbsql.AppendLine($"  ,aid");
                sbsql.AppendLine($"  ,sregnumber"); //202208151720 ito st //箱番号追加
                sbsql.AppendLine($"  from application");
                sbsql.AppendLine($" where application.cym={cym} ");
                sbsql.AppendLine($"   and application.aapptype=6 ");

                #endregion

                using (DB.Command cmd = new DB.Command(sbsql.ToString(), tran))
                {
                    cmd.TryExecuteNonQuery();
                }

                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }


        /// <summary>
        /// dataexportfindテーブル取得
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static List<object[]> getList(WaitForm wf, int cym)
        {

            StringBuilder sbsql = new StringBuilder();

            wf.LogPrint("dataexportfind取得");
            List<object[]> list = new List<object[]>();

            try
            {
                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"select ");
                sbsql.AppendLine($"  insurercode");
                sbsql.AppendLine($" ,kind");
                sbsql.AppendLine($" ,times");
                sbsql.AppendLine($" ,barcode");
                sbsql.AppendLine($" ,number");
                sbsql.AppendLine($" ,tif");//検索ブックでこの辺にしないとスクロールしないといけなくなる
                sbsql.AppendLine($" ,vacc_entrydate");
                sbsql.AppendLine($" ,vacc_date");
                sbsql.AppendLine($" ,vacc_ticketnum");
                sbsql.AppendLine($" ,vacc_insurercode");
                sbsql.AppendLine($" ,vacc_place");
                sbsql.AppendLine($" ,vacc_doctor");
                sbsql.AppendLine($" ,vacc_maker");
                sbsql.AppendLine($" ,vacc_lot");

                //202208151720 ito st //箱番号追加 
                //sbsql.AppendLine($" ,cym");
                //sbsql.AppendLine($" ,sid");
                //sbsql.AppendLine($" ,aid ");
                sbsql.AppendLine($" ,box_no");
                //202208151720 ito ed //箱番号追加 

                sbsql.AppendLine($"  from dataexportfind where cym={cym} ");
                sbsql.AppendLine($"order by number,kind,times,barcode");

                using (DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString()))
                {
                    list = cmd.TryExecuteReaderList();
                }
                return list;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 検索シート作成
        /// </summary>
        /// <param name="list"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CreateSheet(List<object[]> list, string strDir, WaitForm wf)
        {
            NPOI.XSSF.UserModel.XSSFWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet("検索");
            NPOI.SS.UserModel.ICellStyle cs = wb.CreateCellStyle();
            NPOI.SS.UserModel.IFont font = wb.CreateFont();

            try
            {
                font.FontName = "ＭＳ　ゴシック";
                font.FontHeightInPoints = 11;
                cs.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                cs.BorderBottom = NPOI.SS.UserModel.BorderStyle.Hair;
                cs.SetFont(font);

                //データ行
                int rowcnt = 1;

                wf.LogPrint("検索ブック作成");
                wf.SetMax(list.Count);
                wf.InvokeValue = 0;

                foreach (object[] obj in list)
                {
                    NPOI.SS.UserModel.IRow row = ws.CreateRow(rowcnt);

                    for (int cellcnt = 0; cellcnt < obj.Length; cellcnt++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(cellcnt);
                        cell.CellStyle = cs;
                        if (cellcnt == 0) cell.SetCellValue(obj[cellcnt].ToString());       //insurercode
                        if (cellcnt == 1) cell.SetCellValue(obj[cellcnt].ToString());       //kind
                        if (cellcnt == 2) cell.SetCellValue(obj[cellcnt].ToString());       //times
                        if (cellcnt == 3) cell.SetCellValue(obj[cellcnt].ToString());       //barcode
                        if (cellcnt == 4) cell.SetCellValue(obj[cellcnt].ToString());       //number

                        //tif
                        if (cellcnt == 5)
                        {
                            //20220205142434 furukawa st ////////////////////////
                            //画像リンクを相対パスにしないと納品先でロードできない                            
                            string strAddress = $"Images\\{obj[5].ToString()}";
                            //      string strAddress = $"{strDir}\\Images\\{obj[5].ToString()}";
                            //20220205142434 furukawa ed ////////////////////////

                            strAddress = strAddress.Replace("\\", "/");
                            NPOI.XSSF.UserModel.XSSFHyperlink link = new NPOI.XSSF.UserModel.XSSFHyperlink(NPOI.SS.UserModel.HyperlinkType.Unknown);
                            NPOI.XSSF.UserModel.XSSFCreationHelper helper = new NPOI.XSSF.UserModel.XSSFCreationHelper(wb);
                            link = (NPOI.XSSF.UserModel.XSSFHyperlink)helper.CreateHyperlink(NPOI.SS.UserModel.HyperlinkType.Unknown);
                            cell.SetCellValue(strAddress);
                            link.Address = strAddress;

                            cell.Hyperlink = link;
                            cell.SetCellValue($"{obj[5]}");

                        }

                        if (cellcnt == 6) cell.SetCellValue(obj[cellcnt].ToString());       //vacc_entrydate
                        if (cellcnt == 7) cell.SetCellValue(obj[cellcnt].ToString());       //vacc_date
                        if (cellcnt == 8) cell.SetCellValue(obj[cellcnt].ToString());       //vacc_ticketnum
                        if (cellcnt == 9) cell.SetCellValue(obj[cellcnt].ToString());       //vacc_insurercode
                        if (cellcnt == 10) cell.SetCellValue(obj[cellcnt].ToString());       //vacc_place
                        if (cellcnt == 11) cell.SetCellValue(obj[cellcnt].ToString());      //vacc_doctor
                        if (cellcnt == 12) cell.SetCellValue(obj[cellcnt].ToString());      //vacc_maker
                        if (cellcnt == 13) cell.SetCellValue(obj[cellcnt].ToString());      //vacc_lot
                        if (cellcnt == 14) cell.SetCellValue(obj[cellcnt].ToString());      //box_no    //202208151720 ito st //箱番号追加
                    }

                    wf.LogPrint($"バーコード番号 {obj[3]}");
                    wf.InvokeValue++;
                    rowcnt++;
                }

                //ヘッダ行
                NPOI.SS.UserModel.IRow rowHD = ws.CreateRow(0);

                for (int c = 0; c <= 14; c++)   // c <= 13; c++)   //202208151720 ito st //箱番号追加
                {
                    NPOI.SS.UserModel.ICell cellHD = rowHD.CreateCell(c);
                    cellHD.CellStyle = cs;
                    if (c == 0) rowHD.Cells[c].SetCellValue("保険者番号");
                    if (c == 1) rowHD.Cells[c].SetCellValue("券種");
                    if (c == 2) rowHD.Cells[c].SetCellValue("回数");
                    if (c == 3) rowHD.Cells[c].SetCellValue("バーコード");
                    if (c == 4) rowHD.Cells[c].SetCellValue("番号");
                    if (c == 5) rowHD.Cells[c].SetCellValue("画像");
                    if (c == 6) rowHD.Cells[c].SetCellValue("接種履歴登録日時");
                    if (c == 7) rowHD.Cells[c].SetCellValue("接種日");
                    if (c == 8) rowHD.Cells[c].SetCellValue("券番号");
                    if (c == 9) rowHD.Cells[c].SetCellValue("自治体コード");
                    if (c == 10) rowHD.Cells[c].SetCellValue("会場名");
                    if (c == 11) rowHD.Cells[c].SetCellValue("医師名");
                    if (c == 12) rowHD.Cells[c].SetCellValue("メーカー");
                    if (c == 13) rowHD.Cells[c].SetCellValue("ロット");
                    if (c == 14) rowHD.Cells[c].SetCellValue("箱番号");    //202208151720 ito st //箱番号追加

                    ws.AutoSizeColumn(c);
                }

                System.IO.FileStream fs = new System.IO.FileStream($"{strDir}\\検索用.xlsx", System.IO.FileMode.Create);
                wb.Write(fs);
                wf.LogPrint("検索ブック作成完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wb.Close();
            }
        }


        /// <summary>
        /// 検索用ブックの作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CreateExcelBook(int cym,string strDir,WaitForm wf)
        {
            strDir = $"{strDir}\\{cym}";
            
            NPOI.XSSF.UserModel.XSSFWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            
            try
            {
                wf.LogPrint("検索ブックテーブル作成");

                StringBuilder sbsql = new StringBuilder();
                DB.Transaction tran = DB.Main.CreateTransaction();

                //20220313150416 furukawa st ////////////////////////
                //同じ券番号で違う画像が出現したので、根本的にApplicationから取得するしか方法がなくなった
                if (!CreateDataExportFindTable(wf, cym)) return false;

                List<object[]> list = getList(wf, cym);

                if (!CreateSheet(list, strDir, wf)) return false;

                return true;
            }
            catch( Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wb.Close();
            }
        }

    }
}
