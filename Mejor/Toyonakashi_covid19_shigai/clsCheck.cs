﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Toyonakashi_covid19_shigai
{
    class clsCheck
    {
        public static List<App> lstCheck = new List<App>();
        public static List<App> lst = new List<App>();
        public static int cym = 0;

        public static void CheckMain(int _cym)
        {
            cym = _cym;
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            if (!getApp(wf))
            {
                System.Windows.Forms.MessageBox.Show("チェックすべき予診票がありません");
                wf.Dispose();
                return;
            }


            if (!UpdateAppByHeader(wf))
            {
                System.Windows.Forms.MessageBox.Show("予診票更新失敗");
                wf.Dispose();
                return;
            }

            //市外はどちらも不要
            //if (!chkZikangai(wf))
            //{
            //    System.Windows.Forms.MessageBox.Show("時間外チェック失敗");
            //    wf.Dispose();
            //    return;
            //}

            //if (!chkPageCount())
            //{
            //    System.Windows.Forms.MessageBox.Show("ページエラーチェック失敗");
            //    wf.Dispose();
            //    return;
            //}

            wf.Dispose();
        }

    

        /// <summary>
        /// appのリスト取得
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool getApp(WaitForm wf)
        {
            wf.LogPrint("予診票取得");
            lst = App.GetApps(cym);
            lst.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            wf.LogPrint($"{lst.Count}件");

            if (lst.Count == 0) return false;
            return true;
        }

        /// <summary>
        /// ヘッダの値を予診票に適用
        /// </summary>
        private static bool UpdateAppByHeader(WaitForm wf)
        {
            wf.LogPrint("予診票取得");
            lst = App.GetApps(cym);

            if (lst.Count == 0) return false;

            wf.LogPrint($"{lst.Count}件");
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            wf.LogPrint("予診票をヘッダ情報で更新");

            string strHdZikangai = string.Empty;
            string strHdClinicNum = string.Empty;
            string strHdNoB = string.Empty;
            string strHdBatchNo = string.Empty;
            string strClinicName = string.Empty;
            string strDrName = string.Empty;
            int intY = 0;
            int intM = 0;
                
            DB.Transaction tran = DB.Main.CreateTransaction();

            #region バッチ付け
            try
            {
               
                
                //まずバッチ番号が入ってないのでAID昇順で回す
                lst = lst.OrderBy(item => item.Aid).
                        ToList<App>();
                
                
                //AIDで昇順したリストを上から回す。ヘッダが最初に来る前提

                foreach (App a in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    if (a.AppType == APP_TYPE.長期)
                    {//ヘッダシート
                        //strHdZikangai = a.Family.ToString();
                        //strHdClinicNum = a.ClinicNum;
                        //strHdNoB = a.HihoNum;
                        strHdBatchNo = a.HihoZip;
                        //strClinicName = a.ClinicName;
                        //strDrName = a.DrName;
                        //intY = a.MediYear;
                        //intM = a.MediMonth;
                    }
                    else
                    {//予診票
                        bool flgChange = false;
                        StringBuilder sbsql = new StringBuilder();
                        sbsql.AppendLine(" update application set ");

                    

                        //メディ独自
                        //バッチ番号がなく、バーコード番号が入っている場合（バッチの付いてない予診票）にヘッダのをそのまま入れる
                        if (a.HihoZip == string.Empty && a.HihoName!=string.Empty)
                        {
                            sbsql.AppendLine($" hzip='{strHdBatchNo}',");
                            
                            flgChange = true;
                        }

                                                                      
                        sbsql = sbsql.Remove(sbsql.ToString().Trim().Length, 1);

                        if (flgChange)
                        {                                                       
                            sbsql.AppendLine($" where aid={a.Aid} ");
                                                       
                            using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                            {
                                cmd.TryExecuteNonQuery();
                            }
                            
                            wf.LogPrint($"バッチ付け　AID:{a.Aid} Batch:{strHdBatchNo}");
                        }

                    }

                    wf.InvokeValue++;

                }
                
                tran.Commit();
                
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }

            #endregion


            #region ヘッダ情報で更新
            try
            {
                lst.Clear();
                lst = App.GetApps(cym);

                //バッチ番号、AIDでソートしないと画像追加した場合に意図したバッチシートのデータが入らない     

                //まずバッチ番号が入ってないのでAID昇順で回す
                lst = lst.OrderBy(item => item.Aid).
                        ToList<App>();

                //lst = lst.OrderBy(item => item.HihoZip).
                //       ThenBy(item => item.Aid).
                //       ToList<App>();
                //バッチ番号、AIDで昇順したリストを上から回す。ヘッダが最初に来る前提
                tran = DB.Main.CreateTransaction();

                wf.InvokeValue = 0;
                foreach (App a in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    if (a.AppType == APP_TYPE.長期)
                    {//ヘッダシート
                        //strHdZikangai = a.Family.ToString();
                        //strHdClinicNum = a.ClinicNum;
                        //strHdNoB = a.HihoNum;
                        //strHdBatchNo = a.HihoZip;
                        //strClinicName = a.ClinicName;
                        //strDrName = a.DrName;
                        intY = a.MediYear;
                        intM = a.MediMonth;
                    }
                    else
                    {//予診票
                        bool flgChange = false;
                        StringBuilder sbsql = new StringBuilder();
                        sbsql.AppendLine(" update application set ");

                        //市外は無い

                        ////時間外の欄がない場合のみヘッダと合わせる
                        //if (a.Family == 9)
                        //{
                        //    sbsql.AppendLine($" afamily={strHdZikangai},");

                        //    flgChange = true;
                        //}

                        ////NoB類が無い場合にヘッダのをそのまま入れる
                        //if (a.HihoNum == string.Empty)
                        //{
                        //    sbsql.AppendLine($" hnum='{strHdNoB}',");

                        //    flgChange = true;
                        //}

                        //メディ独自
                        //バッチ番号がない場合にヘッダのをそのまま入れる
                        //if (a.HihoZip == string.Empty)
                        //{
                        //    sbsql.AppendLine($" hzip='{strHdBatchNo}',");

                        //    flgChange = true;
                        //}

                        //市外無し
                        ////接種会場には医療機関名をそのまま入れる
                        //if (a.ClinicName == string.Empty)
                        //{
                        //    sbsql.AppendLine($" sname='{strClinicName}',");

                        //    flgChange = true;
                        //}

                        //市外無し
                        ////医師名
                        //if (a.DrName == string.Empty)
                        //{
                        //    sbsql.AppendLine($" sdoctor='{strDrName}',");

                        //    flgChange = true;
                        //}

                        //年月
                        if (a.MediYear == 0 && a.MediMonth == 0 && a.HihoName != string.Empty)
                        {
                            sbsql.AppendLine($" ayear={intY},");
                            sbsql.AppendLine($" amonth={intM},");
                            int ym = DateTimeEx.GetAdYearFromHs(intY * 100 + intM) * 100 + intM;
                            sbsql.AppendLine($" ym={ym},");
                            flgChange = true;
                        }

                        sbsql = sbsql.Remove(sbsql.ToString().Trim().Length, 1);

                        if (flgChange)
                        {

                            //バッチ番号、AIDで合致させないと意図したバッチ番号じゃなくなる                            
                            sbsql.AppendLine($" where aid={a.Aid} and hzip='{a.HihoZip}'");

                            using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                            {
                                cmd.TryExecuteNonQuery();
                            }

                            //バッチも表記                            
                            wf.LogPrint($"ヘッダ情報で更新　AID:{a.Aid} Batch:{a.HihoZip}");
                        }

                    }

                    wf.InvokeValue++;

                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
            #endregion

        }

        /// <summary>
        /// 時間外のヘッダとの一致度チェック
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool chkZikangai(WaitForm wf)
        {
            string strfilename = string.Empty;
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.FileName = $"{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}_時間外等エラーチェックリスト.csv";

            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            strfilename = sfd.FileName;

            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strfilename}", false, System.Text.Encoding.GetEncoding("utf-8"));


            wf.LogPrint("予診票取得");
            lst = App.GetApps(cym);
            lst.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            if (lst.Count == 0) return false;

            wf.LogPrint($"{lst.Count}件");           
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;

            string strHdZikangai = string.Empty;
            string strHdClinicNum = string.Empty;
            

            try
            {
                foreach (App a in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf)) return false;


                    if (a.AppType == APP_TYPE.長期)
                    {
                        strHdZikangai = a.Family.ToString();
                        strHdClinicNum = a.ClinicNum;

                    }
                    else
                    {
                        //医療機関等コードのチェックは不要
                        //if (a.ClinicNum != strHdClinicNum)
                        //{
                        //    a.Memo += "医療機関等コードがヘッダシートと異なります。";
                        //    wf.LogPrint($"AID:{a.Aid} 医療機関等コードがヘッダシートと異なります。");
                        //}

                        if (a.Family != int.Parse(strHdZikangai))
                        {
                            a.Memo += "時間外がヘッダシートと異なります。";
                            wf.LogPrint($"AID:{a.Aid} 時間外がヘッダシートと異なります。");
                        }
                    }

                  

                    wf.InvokeValue++;
                    lstCheck.Add(a);

                }


                string strHeader = $"AID,バッチ番号,Memo,時間外等";
                sw.WriteLine(strHeader);

                foreach (App a in lstCheck)
                {
                    string strErr = $"{a.Aid},{a.HihoZip},{a.Memo},{a.Family.ToString()}";
                    sw.WriteLine(strErr);
                }


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                
                return false;
            }
            finally
            {
                sw.Close();
            }
        }


        /// <summary>
        /// ページエラーリスト
        /// </summary>
        private static bool chkPageCount()
        {
            string strfilename = string.Empty;
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Title = "件数チェックリスト保存先";
            sfd.FileName = $"{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}_ページエラーリスト.csv";
            
            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            strfilename = sfd.FileName;

            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strfilename}", false, System.Text.Encoding.GetEncoding("utf-8"));

            string strLine = string.Empty;
          

            //ヘッダシートのAID、医療機関等コード、バッチ番号、NoB、接種人数、予診人数取得
            StringBuilder sb = new StringBuilder();

            //20220308123557 furukawa st ////////////////////////
            //カンマとピリオド間違った
            
            sb.AppendLine("select a.aid,a.aapptype,a.hzip,a.hnum,a.idays1,a.icourse1,a.idays3,a.icourse3," +            
                "a.idays1+a.icourse1+a.idays3+a.icourse3 total from application a ");    //totalは合計値

            //sb.AppendLine("select a,aid,a.aapptype,a.hzip,a.hnum,a.idays1,a.icourse1,a.idays3,a.icourse3," +
            //"a.idays1+a.icourse1+a.idays3+a.icourse3 total from application a ");    //totalは合計値
            //20220308123557 furukawa ed ////////////////////////

            sb.AppendLine($"where a.aapptype={(int)APP_TYPE.長期} and cym={cym}");
            sb.AppendLine("order by a.aid ");

            //datatableとかに取得して、バッチの人数とappの数をカウントで取得して間違ってたらエラーリスト
            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                for (int c = 0; c <= 8; c++) { dt.Columns.Add(); }
                
                using (DB.Command cmd = new DB.Command(DB.Main, sb.ToString())) {
                    var list = cmd.TryExecuteReaderList();
                    foreach (var item in list)
                    {

                        System.Data.DataRow r = dt.NewRow();
                        //20220308131456 furukawa st ////////////////////////
                        //添え字間違い
                        
                        r[0] = item[0];     //aid
                        r[1] = item[1];     //apptpye
                        r[2] = item[2];     //hzip=バッチ
                        r[3] = item[3];     //hnum=NoB
                        r[4] = item[4];     //idays1
                        r[5] = item[5];     //icourse1
                        r[6] = item[6];     //idays3
                        r[7] = item[7];     //icourse3
                        r[8] = item[8];     //total

                        //r[0] = item[1];     //aid
                        //r[1] = item[2];     //apptpye
                        //r[2] = item[3];     //hzip=バッチ
                        //r[3] = item[4];     //hnum=NoB
                        //r[4] = item[5];     //idays1
                        //r[5] = item[6];     //icourse1
                        //r[6] = item[7];     //idays3
                        //r[7] = item[8];     //icourse3
                        //r[8] = item[9];     //total
                        //20220308131456 furukawa ed ////////////////////////

                        dt.Rows.Add(r);
                    }
                }



                //バッチ番号ごとにカウントを取り、ヘッダの枚数と違ってたらエラーリストに入れる
                strLine = $"AID,バッチ番号,NoB類,合計枚数,入力枚数";
                sw.WriteLine(strLine);

                foreach (System.Data.DataRow r in dt.Rows)
                {
                    string strBatch = r[2].ToString();
                    using(DB.Command cmd=new DB.Command(DB.Main,
                        $"select count(*) from application " +
                        $"where cym={cym} and aapptype<>{(int)APP_TYPE.長期} and hzip='{strBatch}'"))
                    {
                        if (!int.TryParse(cmd.TryExecuteScalar().ToString(), out int cnt)) cnt = 0;
                        
                        if (cnt != int.Parse(r[8].ToString()))
                        {
                            strLine = $"{r[0]},{r[2]},{r[3]},{r[8]},{cnt}";
                            sw.WriteLine(strLine);
                        
                        }
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
