﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Kashiwashi

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName = frm.strFileBaseData;
            string strFileName2 = frm.strFile2;
            string strFileName3 = frm.strFile3;

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName != string.Empty)
                {
                    wf.LogPrint("データインポート");
                    if (!importdata1.importdata1_main(_cym, wf, strFileName))
                    {
                        wf.LogPrint("データインポート異常終了");
                        System.Windows.Forms.MessageBox.Show("データインポート異常終了","",
                            System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    
                    wf.LogPrint("データインポート終了");
                }
                else
                {
                    wf.LogPrint("データのファイルが指定されていないため処理しない");
                }

                if (strFileName2 != string.Empty)
                {
                    wf.LogPrint("施術所名データインポート");
                    if (!importdata2.importdat2_main(_cym, wf, strFileName2))
                    {
                        wf.LogPrint("施術所名データインポート異常終了");
                        return;
                    }

                    wf.LogPrint("施術所名データインポート終了");
                }
                else
                {
                    wf.LogPrint("施術所名データのファイルが指定されていないため処理しない");
                }

                if (strFileName3 != string.Empty)
                {
                    wf.LogPrint("受療者名データインポート");
                    if (!importdata3.importdata3_main(_cym, wf, strFileName3))
                    {
                        wf.LogPrint("受療者名データインポート終了");
                        return;
                    }


                    wf.LogPrint("受療者名データインポート終了");
                }
                else
                {
                    wf.LogPrint("受療者名データのファイルが指定されていないため処理しない");
                }

                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    /// <summary>
    /// 柔整データインポート
    /// </summary>
    partial class importdata1
    {

        #region テーブル構造
        
        public int cym { get;set; }=0;                                         //メホール請求年月管理用;
        public string f001insnum { get; set; } = string.Empty;                 //保険者番号;

        [DB.DbAttribute.PrimaryKey]
        public string f002shinsaym { get; set; } = string.Empty;               //審査年月;
        public string f003comnum { get; set; } = string.Empty;                 //レセプト全国共通キー;

        public string f004rezenum { get; set; } = string.Empty;                //国保連レセプト番号;
        public string f005sejutsuym { get; set; } = string.Empty;              //施術年月;
        public string f006 { get; set; } = string.Empty;                       //保険種別1;
        public string f007 { get; set; } = string.Empty;                       //保険種別2;
        public string f008family { get; set; } = string.Empty;                 //本人家族入外区分;
        public string f009hihomark { get; set; } = string.Empty;               //被保険者証記号;
        public string f010hihonum { get; set; } = string.Empty;                //証番号;
        public string f011pgender { get; set; } = string.Empty;                //性別;
        public string f012pbirthday { get; set; } = string.Empty;              //生年月日;
        public string f013ratio { get; set; } = string.Empty;                  //給付割合;
        public string f014 { get; set; } = string.Empty;                       //特記事項1;
        public string f015 { get; set; } = string.Empty;                       //特記事項2;
        public string f016 { get; set; } = string.Empty;                       //特記事項3;
        public string f017 { get; set; } = string.Empty;                       //特記事項4;
        public string f018 { get; set; } = string.Empty;                       //特記事項5;
        public string f019 { get; set; } = string.Empty;                       //算定区分1;
        public string f020 { get; set; } = string.Empty;                       //算定区分2;
        public string f021 { get; set; } = string.Empty;                       //算定区分3;
        public string f022clinicnum { get; set; } = string.Empty;              //施術所コード;
        public string f023 { get; set; } = string.Empty;                       //柔整団体コード;
        public string f024 { get; set; } = string.Empty;                       //回数;
        public string f025total { get; set; } = string.Empty;                  //決定金額;
        public string f026partial { get; set; } = string.Empty;                //決定一部負担金;
        public string f027 { get; set; } = string.Empty;                       //費用額;
        public string f028charge { get; set; } = string.Empty;                 //保険者負担額;
        public string f029 { get; set; } = string.Empty;                       //高額療養費;
        public string f030 { get; set; } = string.Empty;                       //患者負担額;
        public string f031 { get; set; } = string.Empty;                       //国保優先公費;
        public string f033firstdate1 { get; set; } = string.Empty;             //初検年月日;
        public string f034 { get; set; } = string.Empty;                       //負傷名数;
        public string f035 { get; set; } = string.Empty;                       //転帰;
        public string f036 { get; set; } = string.Empty;                       //転帰レコード区分;
        public string f037 { get; set; } = string.Empty;                       //転帰グループ番号;
        public string f038 { get; set; } = string.Empty;                       //整形審査年月_1;
        public string f039 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_1;
        public string f040 { get; set; } = string.Empty;                       //整形医療機関コード_1;
        public string f041 { get; set; } = string.Empty;                       //整形医療機関名_1;
        public string f042 { get; set; } = string.Empty;                       //整形診療開始日1_1;
        public string f043 { get; set; } = string.Empty;                       //整形診療開始日2_1;
        public string f044 { get; set; } = string.Empty;                       //整形診療開始日3_1;
        public string f045 { get; set; } = string.Empty;                       //整形疾病コード1_1;
        public string f046 { get; set; } = string.Empty;                       //整形疾病コード2_1;
        public string f047 { get; set; } = string.Empty;                       //整形疾病コード3_1;
        public string f048 { get; set; } = string.Empty;                       //整形疾病コード4_1;
        public string f049 { get; set; } = string.Empty;                       //整形疾病コード5_1;
        public string f050 { get; set; } = string.Empty;                       //整形診療日数_1;
        public string f051 { get; set; } = string.Empty;                       //整形費用額_1;
        public string f052 { get; set; } = string.Empty;                       //整形審査年月_2;
        public string f053 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_2;
        public string f054 { get; set; } = string.Empty;                       //整形医療機関コード_2;
        public string f055 { get; set; } = string.Empty;                       //整形医療機関名_2;
        public string f056 { get; set; } = string.Empty;                       //整形診療開始日1_2;
        public string f057 { get; set; } = string.Empty;                       //整形診療開始日2_2;
        public string f058 { get; set; } = string.Empty;                       //整形診療開始日3_2;
        public string f059 { get; set; } = string.Empty;                       //整形疾病コード1_2;
        public string f060 { get; set; } = string.Empty;                       //整形疾病コード2_2;
        public string f061 { get; set; } = string.Empty;                       //整形疾病コード3_2;
        public string f062 { get; set; } = string.Empty;                       //整形疾病コード4_2;
        public string f063 { get; set; } = string.Empty;                       //整形疾病コード5_2;
        public string f064 { get; set; } = string.Empty;                       //整形診診療日数_2;
        public string f065 { get; set; } = string.Empty;                       //整形費用額_2;
        public string f066 { get; set; } = string.Empty;                       //整形審査年月_3;
        public string f067 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_3;
        public string f068 { get; set; } = string.Empty;                       //整形医療機関コード_3;
        public string f069 { get; set; } = string.Empty;                       //整形医療機関名_3;
        public string f070 { get; set; } = string.Empty;                       //整形診療開始日1_3;
        public string f071 { get; set; } = string.Empty;                       //整形診療開始日2_3;
        public string f072 { get; set; } = string.Empty;                       //整形診療開始日3_3;
        public string f073 { get; set; } = string.Empty;                       //整形疾病コード1_3;
        public string f074 { get; set; } = string.Empty;                       //整形疾病コード2_3;
        public string f075 { get; set; } = string.Empty;                       //整形疾病コード3_3;
        public string f076 { get; set; } = string.Empty;                       //整形疾病コード4_3;
        public string f077 { get; set; } = string.Empty;                       //整形疾病コード5_3;
        public string f078 { get; set; } = string.Empty;                       //整形診療日数_3;
        public string f079 { get; set; } = string.Empty;                       //整形費用額_3;
        public string f080 { get; set; } = string.Empty;                       //宛名番号;
  

        //Mejor用項目
        public int f100shinsaYMAD { get; set; } = 0;                           //審査年月西暦;
        public int f101sejutsuYMAD { get; set; } = 0;                          //施術年月西暦;
        public DateTime f102birthAD { get; set; } = DateTime.MinValue;         //生年月日西暦;
        public DateTime f103firstdateAD { get; set; } = DateTime.MinValue;                        //初検年月日西暦;
        public int f104seikeishisnaYMAD1 { get; set; } = 0;                    //整形審査年月_1西暦;
        public int f105seikeishisnaYMAD2 { get; set; } = 0;                    //整形審査年月_2西暦;
        public int f106seikeishisnaYMAD3 { get; set; } = 0;                    //整形審査年月_3西暦;
        public string f107hihomark_narrow { get; set; } = string.Empty;        //被保険者証記号半角;
        public string f108hihonum_narrow { get; set; } = string.Empty;         //証番号半角;


        #endregion


        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        static int intcym = 0;

        List<importdata1> lstImp = new List<importdata1>();

        /// <summary>
        /// データインポート
        /// </summary>
        /// <returns></returns>
        public static bool importdata1_main(int _cym,WaitForm wf,string strFileName)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");
                
                //npoiで開きデータ取りだし
                //importdata1に登録
                if(!EntryCSVData(strFileName, wf)) return false;

                
                wf.LogPrint("Appテーブル作成");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
                if(!UpdateApp(intcym)) return false;

          
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
           

        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine("update application set ");
            sb.AppendLine("inum			                = b.f001insnum");
            sb.AppendLine(",comnum		                = b.f003comnum" );                                         //レセプト全国共通キー
            sb.AppendLine(",afamily		                = case cast(b.f008family as int) when 2 then 2 else 6 end " );        //本人家族入外区分                 
            sb.AppendLine(",hnum		                = b.f009hihomark || '-' || b.f010hihonum" );               //被保険者証記号番号

            //",hnum				          = b.hihomark || b.hihonum" );                          //被保険者証記号番号

            sb.AppendLine(",psex		                	= cast(b.f011pgender as int) " );                          //性別
            sb.AppendLine(",aratio		                	= cast(b.f013ratio as int) " );                            //給付割合
            sb.AppendLine(",sid			                	= b.f022clinicnum" );                                      //施術所コード
            sb.AppendLine(",acounteddays                	= cast((case when b.f024        ='' then '0' else b.f024        end ) as decimal) " );         //回数（実日数）
            sb.AppendLine(",atotal		                	= cast((case when b.f025total   ='' then '0' else b.f025total   end ) as decimal) " );         //決定金額
            sb.AppendLine(",acharge		                	= cast((case when b.f028charge  ='' then '0' else b.f028charge  end ) as decimal)" );          //保険者負担額
            sb.AppendLine(",apartial	                	= cast((case when b.f030 ='' then '0' else b.f030 end ) as decimal)" );          //患者負担額
            sb.AppendLine(",taggeddatas	          " );
            sb.AppendLine("       = 'count:\"' || case when b.f034 ='' then '0' else b.f034 end  || '\"|' || 'GeneralString1:\"' || b.f080 || '\"' ");//負傷数、宛名番号

            //20210414103231 furukawa st ////////////////////////
            //入力画面がないため、app更新時にymをayear,amonthから取得するので、ここで入れないとym=0となり、診療年月が0になってしまう                
            sb.AppendLine(",ayear               = cast(substr(b.f005sejutsuym,2,2) as int)" );       //施術年和暦
            sb.AppendLine(",amonth              = cast(substr(b.f005sejutsuym,4,2) as int)" );       //施術月和暦            
            //20210414103231 furukawa ed ////////////////////////

            sb.AppendLine(",ym				    = cast(b.f101sejutsuYMAD as int) " );             //診療年月
            sb.AppendLine(",pbirthday			= cast(b.f102birthad as date) " );                //受療者生年月日
            sb.AppendLine(",ifirstdate1			= cast(b.f103firstdatead as date)  " );           //初検日1
            sb.AppendLine(",fchargetype=  " );
            sb.AppendLine("	case when cast(b.f101sejutsuYMAD as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end " );//新規継続
            sb.AppendLine(",aapptype = 6 " ); //申請書種類は6柔整だろう
            sb.AppendLine(" from importdata1 b " );
            sb.AppendLine(" where " );
            //画像ファイル名-拡張子＝提供データの国保連レセプト番号
            sb.AppendLine($" replace(application.emptytext3,'.tif','')=b.f004rezenum and application.cym={cym};");



            string strsql = sb.ToString();
            

            DB.Command cmd = new DB.Command(DB.Main,strsql,true);
           

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データCSVを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData(string strFileName ,WaitForm wf)
        {
       
            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd=new DB.Command($"delete from importdata1 where cym={intcym}",tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {


                    try
                    {
                        int.Parse(tmp[11].ToString().Replace(",", ""));
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    importdata1 cls=new importdata1();

                    cls.f001insnum = tmp[0];            //保険者番号
                    cls.f002shinsaym = tmp[1];          //審査年月
                    cls.f003comnum = tmp[2];            //レセプト全国共通キー
                    cls.f004rezenum = tmp[3];           //国保連レセプト番号
                    cls.f005sejutsuym = tmp[4];         //施術年月
                    cls.f006 = tmp[5];                  //保険種別1
                    cls.f007 = tmp[6];                  //保険種別2
                    cls.f008family = tmp[7];            //本人家族入外区分
                    cls.f009hihomark = tmp[8];          //被保険者証記号
                    cls.f010hihonum = tmp[9];           //被保険者証番号
                    cls.f011pgender = tmp[10];          //性別
                    cls.f012pbirthday = tmp[11];        //生年月日
                    cls.f013ratio = tmp[12];            //給付割合
                    cls.f014 = tmp[13];                 //特記事項1
                    cls.f015 = tmp[14];                 //特記事項2
                    cls.f016 = tmp[15];                 //特記事項3
                    cls.f017 = tmp[16];                 //特記事項4
                    cls.f018 = tmp[17];                 //特記事項5
                    cls.f019 = tmp[18];                 //算定区分1
                    cls.f020 = tmp[19];                 //算定区分2
                    cls.f021 = tmp[20];                 //算定区分3
                    cls.f022clinicnum = tmp[21];        //施術所コード
                    cls.f023 = tmp[22];                 //柔整団体コード
                    cls.f024 = tmp[23];                 //回数
                    cls.f025total = tmp[24];            //決定金額
                    cls.f026partial = tmp[25];          //決定一部負担金
                    cls.f027 = tmp[26];                 //費用額
                    cls.f028charge = tmp[27];           //保険者負担額
                    cls.f029 = tmp[28];                 //高額療養費
                    cls.f030 = tmp[29];                 //患者負担額
                    cls.f031 = tmp[30];                 //国保優先公費
                    cls.f033firstdate1 = tmp[31];       //初検年月日
                    cls.f034 = tmp[32];                 //負傷名数
                    cls.f035 = tmp[33];                 //転帰
                    cls.f036 = tmp[34];                 //転帰レコード区分
                    cls.f037 = tmp[35];                 //転帰グループ番号
                    cls.f038 = tmp[36];                 //整形審査年月(1)
                    cls.f039 = tmp[37];                 //整形レセプト全国共通キー(1)
                    cls.f040 = tmp[38];                 //整形医療機関コード(1)
                    cls.f041 = tmp[39];                 //整形医療機関名(1)
                    cls.f042 = tmp[40];                 //整形診療開始日1(1)
                    cls.f043 = tmp[41];                 //整形診療開始日2(1)
                    cls.f044 = tmp[42];                 //整形診療開始日3(1)
                    cls.f045 = tmp[43];                 //整形疾病コード1(1)
                    cls.f046 = tmp[44];                 //整形疾病コード2(1)
                    cls.f047 = tmp[45];                 //整形疾病コード3(1)
                    cls.f048 = tmp[46];                 //整形疾病コード4(1)
                    cls.f049 = tmp[47];                 //整形疾病コード5(1)
                    cls.f050 = tmp[48];                 //整形診療日数(1)
                    cls.f051 = tmp[49];                 //整形費用額(1)
                    cls.f052 = tmp[50];                 //整形審査年月(2)
                    cls.f053 = tmp[51];                 //整形レセプト全国共通キー(2)
                    cls.f054 = tmp[52];                 //整形医療機関コード(2)
                    cls.f055 = tmp[53];                 //整形医療機関名(2)
                    cls.f056 = tmp[54];                 //整形診療開始日1(2)
                    cls.f057 = tmp[55];                 //整形診療開始日2(2)
                    cls.f058 = tmp[56];                 //整形診療開始日3(2)
                    cls.f059 = tmp[57];                 //整形疾病コード1(2)
                    cls.f060 = tmp[58];                 //整形疾病コード2(2)
                    cls.f061 = tmp[59];                 //整形疾病コード3(2)
                    cls.f062 = tmp[60];                 //整形疾病コード4(2)
                    cls.f063 = tmp[61];                 //整形疾病コード5(2)
                    cls.f064 = tmp[62];                 //整形診診療日数(2)
                    cls.f065 = tmp[63];                 //整形費用額(2)
                    cls.f066 = tmp[64];                 //整形審査年月(3)
                    cls.f067 = tmp[65];                 //整形レセプト全国共通キー(3)
                    cls.f068 = tmp[66];                 //整形医療機関コード(3)
                    cls.f069 = tmp[67];                 //整形医療機関名(3)
                    cls.f070 = tmp[68];                 //整形診療開始日1(3)
                    cls.f071 = tmp[69];                 //整形診療開始日2(3)
                    cls.f072 = tmp[70];                 //整形診療開始日3(3)
                    cls.f073 = tmp[71];                 //整形疾病コード1(3)
                    cls.f074 = tmp[72];                 //整形疾病コード2(3)
                    cls.f075 = tmp[73];                 //整形疾病コード3(3)
                    cls.f076 = tmp[74];                 //整形疾病コード4(3)
                    cls.f077 = tmp[75];                 //整形疾病コード5(3)
                    cls.f078 = tmp[76];                 //整形診療日数(3)
                    cls.f079 = tmp[77];                 //整形費用額(3)
                    cls.f080 = tmp[78];                 //宛名番号
                                                        
                    //管理項目
                    string strErr = string.Empty;
                    cls.cym = intcym;//メホール請求年月管理用;

                    //審査年月西暦;
                    if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f002shinsaym)) == 0) strErr += "審査年月 変換に失敗しました";
                    cls.f100shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f002shinsaym));
                    cls.f101sejutsuYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f005sejutsuym));         //施術年月西暦;                            
                    cls.f102birthAD = DateTimeEx.GetDateFromJstr7(cls.f012pbirthday);                               //生年月日西暦;                            
                    cls.f103firstdateAD = DateTimeEx.GetDateFromJstr7(cls.f033firstdate1);//初検年月日西暦

                    if (!int.TryParse(cls.f038 == string.Empty ? "0" : cls.f038, out int tmpf038)) strErr += "\r\n 整形審査年月_1西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf038);                          //整形審査年月_1西暦;

                    if (!int.TryParse(cls.f052 == string.Empty ? "0" : cls.f052, out int tmpf052)) strErr += "\r\n 整形審査年月_2西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf052);                          //整形審査年月_2西暦;

                    if (!int.TryParse(cls.f066 == string.Empty ? "0" : cls.f066, out int tmpf066)) strErr += "\r\n 整形審査年月_3西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf066);                          //整形審査年月_3西暦;

                    //被保険者証記号半角;                                                                                                
                    //記号は漢字の可能性が                                                                          
                    try
                    {
                        cls.f107hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);
                    }
                    catch
                    {
                        cls.f107hihomark_narrow = cls.f010hihonum;
                    }

                    cls.f108hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;

                    if (strErr != string.Empty)
                    {
                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{strErr}");
                        tran.Rollback();
                        return false;
                    }

                    //20210525153647 furukawa st ////////////////////////
                    //トランザクションにしないと遅すぎ
                    
                    if (!DB.Main.Insert<importdata1>(cls,tran)) return false;
                    //if (!DB.Main.Insert<importdata1>(cls)) return false;
                    //20210525153647 furukawa ed ////////////////////////


                    wf.LogPrint($"レセプトデータ  レセプト全国共通キー:{cls.f003comnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");



                return true;
            }         
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }           
            finally
            {
                tran.Commit();                
            }
            
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch(cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }
           
            
        }

    }

    /// <summary>
    /// 施術所名データインポート
    /// </summary>
    partial class importdata2
    {
        #region テーブル構造
        public int cym { get;set; }=0;                                          //メホール請求年月管理用;
        public string f001 { get; set; } = string.Empty;                                         //保険者番号;
        public string f002 { get; set; } = string.Empty;                                         //証記号;
        public string f003 { get; set; } = string.Empty;                                         //証番号;
        public string f004 { get; set; } = string.Empty;                                         //氏名;
        public string f005 { get; set; } = string.Empty;                                         //診療年月;
        public string f006 { get; set; } = string.Empty;                                         //決定点数;
        public string f007clinicnum { get; set; } = string.Empty;                                         //機関コード;
        public string f008clinicname { get; set; } = string.Empty;                                         //医療機関名;
        public string f009 { get; set; } = string.Empty;                                         //処方機関名;
        public string f010 { get; set; } = string.Empty;                                         //最新履歴;
        public string f011 { get; set; } = string.Empty;                                         //世帯番号;
        public string f012 { get; set; } = string.Empty;                                         //宛名番号;
        public string f013 { get; set; } = string.Empty;                                         //生年月日;
        public string f014 { get; set; } = string.Empty;                                         //性別;
        public string f015 { get; set; } = string.Empty;                                         //種別１;
        public string f016 { get; set; } = string.Empty;                                         //種別２;
        public string f017 { get; set; } = string.Empty;                                         //入外;
        public string f018 { get; set; } = string.Empty;                                         //本家;
        public string f019 { get; set; } = string.Empty;                                         //給付割合;
        public string f020 { get; set; } = string.Empty;                                         //診療・療養費別;
        public string f021 { get; set; } = string.Empty;                                         //点数表;
        public string f022 { get; set; } = string.Empty;                                         //療養費種別;
        public string f023 { get; set; } = string.Empty;                                         //審査年月;
        public string f024counteddays { get; set; } = string.Empty;                                         //実日数;
        public string f025 { get; set; } = string.Empty;                                         //食事基準額;
        public string f026comnum { get; set; } = string.Empty;                                         //レセプト全国共通キー;
        public string f027 { get; set; } = string.Empty;                                         //処方機関コード;
        public string f028 { get; set; } = string.Empty;                                         //クリ;
        public string f029 { get; set; } = string.Empty;                                         //ケア;
        public string f030 { get; set; } = string.Empty;                                         //容認;
        public string f031 { get; set; } = string.Empty;                                         //不当;
        public string f032 { get; set; } = string.Empty;                                         //第三者;
        public string f033 { get; set; } = string.Empty;                                         //給付制限;
        public string f034 { get; set; } = string.Empty;                                         //高額;
        public string f035 { get; set; } = string.Empty;                                         //予約;
        public string f036 { get; set; } = string.Empty;                                         //処理;
        public string f037 { get; set; } = string.Empty;                                         //疑義種別;
        public string f038 { get; set; } = string.Empty;                                         //参考;
        public string f039 { get; set; } = string.Empty;                                         //状態;
        public string f040 { get; set; } = string.Empty;                                         //コード情報;
        public string f041 { get; set; } = string.Empty;                                         //原本区分;
        public string f042 { get; set; } = string.Empty;                                         //原本所在;
        public string f043 { get; set; } = string.Empty;                                         //県内外;
        public string f044 { get; set; } = string.Empty;                                         //付箋１  ;
        public string f045 { get; set; } = string.Empty;                                         //付箋２;
        public string f046 { get; set; } = string.Empty;                                         //付箋３;
        public string f047 { get; set; } = string.Empty;                                         //付箋４;
        public string f048 { get; set; } = string.Empty;                                         //付箋５;
        public string f049 { get; set; } = string.Empty;                                         //付箋６;
        public string f050 { get; set; } = string.Empty;                                         //付箋７;
        public string f051 { get; set; } = string.Empty;                                         //付箋８;
        public string f052 { get; set; } = string.Empty;                                         //付箋９;
        public string f053 { get; set; } = string.Empty;                                         //付箋１０;
        public string f054 { get; set; } = string.Empty;                                         //付箋１１;
        public string f055 { get; set; } = string.Empty;                                         //付箋１２;
        public string f056 { get; set; } = string.Empty;                                         //付箋１３;
        public string f057 { get; set; } = string.Empty;                                         //付箋１４;


        #endregion

        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 施術所名データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool importdat2_main(int _cym,WaitForm wf,string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("施術所データ取得");

                //npoiで開きデータ取りだし
                //dataimport_jyuseikyufuに登録
                if (!EntryExcelData(strFileName, wf)) return false;


                wf.LogPrint("Appテーブル作成");
                //レセプト全国共通キーで紐づけてアップデートする
                if(!UpdateApp(intcym))return false;
                

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
         
        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            //20210511140638 furukawa st ////////////////////////
            //ソース整頓
            
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine("update application set     ");
            sb.AppendLine("sid                = k.f007clinicnum ");                     //医療機関コード
            sb.AppendLine(",sname		        = k.f008clinicname  ");                 //医療機関名
            sb.AppendLine(",acounteddays        = cast(k.f024counteddays as int)  ");   //実日数
            sb.AppendLine(",taggeddatas	               ");

            //20210525150841 furukawa st ////////////////////////
            //ダブルコーテーション抜け
            
            sb.AppendLine("       = taggeddatas || '|GeneralString2:\"' || k.f023 || '\"'  ");//審査年月
            //sb.AppendLine("       = taggeddatas || '|GeneralString2:\' || k.f023 || '\'  ");//審査年月
            //20210525150841 furukawa ed ////////////////////////


            sb.AppendLine("from importdata2 k  ");
            sb.AppendLine("where  ");
            sb.AppendLine($"application.comnum=k.f026comnum and application.cym={cym}");
            
            DB.Command cmd = new DB.Command(DB.Main, sb.ToString(), true);

            //    string strsql =
            //    "update application set    " +
            //    "sid                = k.f007clinicnum" +     //医療機関コード
            //    ",sname		        = k.f008clinicname " +     //医療機関名
            //    ",acounteddays        = cast(k.f024counteddays as int) " +        //実日数
            //    ",taggeddatas	              " +
            //    "       = taggeddatas || '|GeneralString2:\"' || k.f023 || '\"' " +//審査年月
            //    "from importdata2 k " +
            //    "where " +
            //    $"application.comnum=k.f026comnum and application.cym={cym}";

            //DB.Command cmd = new DB.Command(DB.Main, strsql, true);

            //20210511140638 furukawa ed ////////////////////////

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd = new DB.Command($"delete from importdata2 where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            int c = 0;//インポートファイルの列
            int r = 1;//インポートファイルの行
            try
            {

                wf.InvokeValue = 0;
                wf.SetMax(ws.LastRowNum);

                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;

                for (r = 1; r <= ws.LastRowNum; r++)
                {
                    importdata2 imp = new importdata2();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

                    for (c = 0; c < cellcnt; c++)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        if (c == 0) imp.f001 = getCellValueToString(cell);              //保険者番号
                        if (c == 1) imp.f002 = getCellValueToString(cell);              //証記号
                        if (c == 2) imp.f003 = getCellValueToString(cell);              //証番号
                        if (c == 3) imp.f004 = getCellValueToString(cell);              //氏名
                        if (c == 4) imp.f005 = getCellValueToString(cell);              //診療年月
                        if (c == 5) imp.f006 = getCellValueToString(cell);              //決定点数
                        if (c == 6) imp.f007clinicnum = getCellValueToString(cell);           //機関コード
                        if (c == 7) imp.f008clinicname = getCellValueToString(cell);         //医療機関名
                        if (c == 8) imp.f009 = getCellValueToString(cell);              //処方機関名
                        if (c == 9) imp.f010 = getCellValueToString(cell);              //最新履歴
                        if (c == 10) imp.f011 = getCellValueToString(cell);             //世帯番号
                        if (c == 11) imp.f012 = getCellValueToString(cell);             //宛名番号
                        if (c == 12) imp.f013 = getCellValueToString(cell);             //生年月日
                        if (c == 13) imp.f014 = getCellValueToString(cell);             //性別
                        if (c == 14) imp.f015 = getCellValueToString(cell);             //種別１
                        if (c == 15) imp.f016 = getCellValueToString(cell);             //種別２
                        if (c == 16) imp.f017 = getCellValueToString(cell);             //入外
                        if (c == 17) imp.f018 = getCellValueToString(cell);             //本家
                        if (c == 18) imp.f019 = getCellValueToString(cell);             //給付割合
                        if (c == 19) imp.f020 = getCellValueToString(cell);             //診療・療養費別
                        if (c == 20) imp.f021 = getCellValueToString(cell);             //点数表
                        if (c == 21) imp.f022 = getCellValueToString(cell);             //療養費種別
                        if (c == 22) imp.f023 = getCellValueToString(cell);             //審査年月
                        if (c == 23) imp.f024counteddays = getCellValueToString(cell);             //実日数
                        if (c == 24) imp.f025 = getCellValueToString(cell);             //食事基準額
                        if (c == 25) imp.f026comnum = getCellValueToString(cell);             //レセプト全国共通キー
                        if (c == 26) imp.f027 = getCellValueToString(cell);             //処方機関コード
                        if (c == 27) imp.f028 = getCellValueToString(cell);             //クリ
                        if (c == 28) imp.f029 = getCellValueToString(cell);             //ケア
                        if (c == 29) imp.f030 = getCellValueToString(cell);             //容認
                        if (c == 30) imp.f031 = getCellValueToString(cell);             //不当
                        if (c == 31) imp.f032 = getCellValueToString(cell);             //第三者
                        if (c == 32) imp.f033 = getCellValueToString(cell);             //給付制限
                        if (c == 33) imp.f034 = getCellValueToString(cell);             //高額
                        if (c == 34) imp.f035 = getCellValueToString(cell);             //予約
                        if (c == 35) imp.f036 = getCellValueToString(cell);             //処理
                        if (c == 36) imp.f037 = getCellValueToString(cell);             //疑義種別
                        if (c == 37) imp.f038 = getCellValueToString(cell);             //参考
                        if (c == 38) imp.f039 = getCellValueToString(cell);             //状態
                        if (c == 39) imp.f040 = getCellValueToString(cell);             //コード情報
                        if (c == 40) imp.f041 = getCellValueToString(cell);             //原本区分
                        if (c == 41) imp.f042 = getCellValueToString(cell);             //原本所在
                        if (c == 42) imp.f043 = getCellValueToString(cell);             //県内外
                        if (c == 43) imp.f044 = getCellValueToString(cell);             //付箋１  
                        if (c == 44) imp.f045 = getCellValueToString(cell);             //付箋２
                        if (c == 45) imp.f046 = getCellValueToString(cell);             //付箋３
                        if (c == 46) imp.f047 = getCellValueToString(cell);             //付箋４
                        if (c == 47) imp.f048 = getCellValueToString(cell);             //付箋５
                        if (c == 48) imp.f049 = getCellValueToString(cell);             //付箋６
                        if (c == 49) imp.f050 = getCellValueToString(cell);             //付箋７
                        if (c == 50) imp.f051 = getCellValueToString(cell);             //付箋８
                        if (c == 51) imp.f052 = getCellValueToString(cell);             //付箋９
                        if (c == 52) imp.f053 = getCellValueToString(cell);             //付箋１０
                        if (c == 53) imp.f054 = getCellValueToString(cell);             //付箋１１
                        if (c == 54) imp.f055 = getCellValueToString(cell);             //付箋１２
                        if (c == 55) imp.f056 = getCellValueToString(cell);             //付箋１３
                        if (c == 56) imp.f057 = getCellValueToString(cell);             //付箋１４
                    }


                    wf.InvokeValue++;
                    wf.LogPrint($"施術所データ  レセプト全国共通キー:{imp.f026comnum}");

                    if (!DB.Main.Insert<importdata2>(imp, tran)) return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
                wb.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }




    }


    partial class importdata3
    {
        //受療者データ
        #region テーブル構造
        public int cym { get;set; }=0;                                          //メホール請求年月管理用;
        public string f001shoriym { get; set; } = string.Empty;                                         //処理年月;
        public string f002comnum { get; set; } = string.Empty;                                         //レセプト全国共通キー;
        public string f003 { get; set; } = string.Empty;                                         //履歴番号;
        public string f004rezeptnum { get; set; } = string.Empty;                                         //国保連レセプト番号;
        public string f005 { get; set; } = string.Empty;                                         //事業区分;
        public string f006clinicnum { get; set; } = string.Empty;                                         //医療機関コード;
        public string f007 { get; set; } = string.Empty;                                         //旧総合病院診療科;
        public string f008 { get; set; } = string.Empty;                                         //任意診療科;
        public string f009insnum { get; set; } = string.Empty;                                         //保険者番号;
        public string f010hmark { get; set; } = string.Empty;                                         //被保険者証記号;
        public string f011hnum { get; set; } = string.Empty;                                         //被保険者証番号（全角）;
        public string f012pbirthday { get; set; } = string.Empty;                                         //生年月日;
        public string f013pgender { get; set; } = string.Empty;                                         //性別;
        public string f014pname { get; set; } = string.Empty;                                         //氏名;
        public string f015personalnum { get; set; } = string.Empty;                                         //個人番号;
        public string f016 { get; set; } = string.Empty;                                         //世帯番号;
        public string f017 { get; set; } = string.Empty;                                         //市町村番号;
        public string f018 { get; set; } = string.Empty;                                         //受給者番号;
        public string f019 { get; set; } = string.Empty;                                         //制御・管理情報制御処理区分;
        public string f020 { get; set; } = string.Empty;                                         //制御・管理情報制御DPC区分;
        public string f021 { get; set; } = string.Empty;                                         //制御・管理情報柔整・療養費口座番号;
        public string f022 { get; set; } = string.Empty;                                         //月診療年月;
        public string f023 { get; set; } = string.Empty;                                         //月支給決定年月;
        public string f024 { get; set; } = string.Empty;                                         //月自支給期間;
        public string f025 { get; set; } = string.Empty;                                         //月至支給期間;
        public string f026 { get; set; } = string.Empty;                                         //区分・種別保険制度（保険種別①）;
        public string f027 { get; set; } = string.Empty;                                         //区分・種別保険種別（保険種別②）;
        public string f028 { get; set; } = string.Empty;                                         //区分・種別本人家族入外;
        public string f029 { get; set; } = string.Empty;                                         //区分・種別点数表;
        public string f030 { get; set; } = string.Empty;                                         //区分・種別療養費種別;
        public string f031 { get; set; } = string.Empty;                                         //区分・種別海外療養費区分;
        public string f032 { get; set; } = string.Empty;                                         //給付割合給付割合;
        public string f033 { get; set; } = string.Empty;                                         //給付割合割引;
        public string f034 { get; set; } = string.Empty;                                         //日診療開始日１;
        public string f035 { get; set; } = string.Empty;                                         //日転帰１;
        public string f036 { get; set; } = string.Empty;                                         //日診療開始日２;
        public string f037 { get; set; } = string.Empty;                                         //日転帰２;
        public string f038 { get; set; } = string.Empty;                                         //日診療開始日３;
        public string f039 { get; set; } = string.Empty;                                         //日転帰３;
        public string f040 { get; set; } = string.Empty;                                         //日初診回数;
        public string f041 { get; set; } = string.Empty;                                         //日再診回数;
        public string f042 { get; set; } = string.Empty;                                         //日入院年月日;
        public string f043 { get; set; } = string.Empty;                                         //柔整団体機関;
        public string f044 { get; set; } = string.Empty;                                         //処方箋交付医療機関;
        public string f045 { get; set; } = string.Empty;                                         //特記特記事項１;
        public string f046 { get; set; } = string.Empty;                                         //特記特記事項２;
        public string f047 { get; set; } = string.Empty;                                         //特記特記事項３;
        public string f048 { get; set; } = string.Empty;                                         //特記特記事項４;
        public string f049 { get; set; } = string.Empty;                                         //特記特記事項５;
        public string f050 { get; set; } = string.Empty;                                         //特記原爆区分;
        public string f051 { get; set; } = string.Empty;                                         //特記継続療養費区分;
        public string f052 { get; set; } = string.Empty;                                         //減額減額割合;
        public string f053 { get; set; } = string.Empty;                                         //減額減額・免除・猶予区分;
        public string f054 { get; set; } = string.Empty;                                         //減額減額金額;
        public string f055 { get; set; } = string.Empty;                                         //算定区分１;
        public string f056 { get; set; } = string.Empty;                                         //算定区分２;
        public string f057 { get; set; } = string.Empty;                                         //算定区分３;
        public string f058 { get; set; } = string.Empty;                                         //給付点検初診料有無;
        public string f059 { get; set; } = string.Empty;                                         //給付点検乳幼児加算区分;
        public string f060 { get; set; } = string.Empty;                                         //給付点検調剤技術基本料;
        public string f061 { get; set; } = string.Empty;                                         //給付点検入院基本料（初期加算）;
        public string f062 { get; set; } = string.Empty;                                         //給付点検補綴時診断料;
        public string f063 { get; set; } = string.Empty;                                         //給付点検特定疾患療養指導料;
        public string f064 { get; set; } = string.Empty;                                         //給付点検老人慢性生活指導管理料;
        public string f065 { get; set; } = string.Empty;                                         //給付点検歯周疾患継続指導管理料;
        public string f066 { get; set; } = string.Empty;                                         //給付点検特定薬剤治療管理料;
        public string f067 { get; set; } = string.Empty;                                         //給付点検悪性腫瘍特異物質治療管理料;
        public string f068 { get; set; } = string.Empty;                                         //給付点検小児科療養指導料;
        public string f069 { get; set; } = string.Empty;                                         //給付点検てんかん指導料;
        public string f070 { get; set; } = string.Empty;                                         //給付点検難病外来指導管理料;
        public string f071 { get; set; } = string.Empty;                                         //給付点検皮膚科特定疾患指導管理料;
        public string f072 { get; set; } = string.Empty;                                         //給付点検在宅指導管理料;
        public string f073 { get; set; } = string.Empty;                                         //給付点検歯科補綴関連検査（ChB）;
        public string f074 { get; set; } = string.Empty;                                         //給付点検歯科補綴関連検査（GoA）;
        public string f075 { get; set; } = string.Empty;                                         //給付点検歯科補綴関連検査（PTG）;
        public string f076 { get; set; } = string.Empty;                                         //給付点検寝たきり老人訪問指導管理料;
        public string f077 { get; set; } = string.Empty;                                         //給付点検退院指導料;
        public string f078 { get; set; } = string.Empty;                                         //給付点検薬剤管理指導料;
        public string f079 { get; set; } = string.Empty;                                         //給付点検特定疾患療養指導料査定;
        public string f080 { get; set; } = string.Empty;                                         //給付点検老人慢性生活指導管理料査定;
        public string f081 { get; set; } = string.Empty;                                         //給付点検在宅訪問ﾘﾊﾋﾞﾘﾃｰｼｮﾝ指導管理料（医科）;
        public string f082 { get; set; } = string.Empty;                                         //給付点検在宅患者訪問薬剤管理指導料（医科）;
        public string f083 { get; set; } = string.Empty;                                         //給付点検在宅患者訪問栄養食事指導料（医科）;
        public string f084 { get; set; } = string.Empty;                                         //給付点検老人訪問口腔指導管理料（歯科）;
        public string f085 { get; set; } = string.Empty;                                         //給付点検訪問歯科衛生指導料（歯科）;
        public string f086 { get; set; } = string.Empty;                                         //給付点検在宅患者訪問薬剤管理指導料（歯科）;
        public string f087 { get; set; } = string.Empty;                                         //給付点検在宅患者訪問薬剤管理指導料（調剤）;
        public string f088 { get; set; } = string.Empty;                                         //給付点検基本療養費Ⅰ（訪問看護療養費）;
        public string f089 { get; set; } = string.Empty;                                         //給付点検管理療養費（訪問看護療養費）;
        public string f090 { get; set; } = string.Empty;                                         //給付点検在宅時医学総合管理料算定者;
        public string f091 { get; set; } = string.Empty;                                         //給付点検処方箋料算定有無情報;
        public string f092 { get; set; } = string.Empty;                                         //連合会任意連合会任意項目１;
        public string f093 { get; set; } = string.Empty;                                         //連合会任意連合会任意項目２;
        public string f094 { get; set; } = string.Empty;                                         //連合会任意連合会任意項目３;
        public string f095 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報被保険者氏名;
        public string f096 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報生年月日;
        public string f097 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報被保険者住所コード;
        public string f098 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報被保険者地区コード;
        public string f099 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報世帯管理番号;
        public string f100 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報個人管理番号;
        public string f101 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報被保険者氏名（カナ）;
        public string f102 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報直近情報_国保喪失事由;
        public string f103 { get; set; } = string.Empty;                                         //資格設定情報個人特定情報退職続柄区分;
        public string f104 { get; set; } = string.Empty;                                         //件数論理件数;
        public string f105 { get; set; } = string.Empty;                                         //件数物理件数;
        public string f106 { get; set; } = string.Empty;                                         //給付割合保険給付割合;
        public string f107 { get; set; } = string.Empty;                                         //請求医療機関機関_点数表;
        public string f108 { get; set; } = string.Empty;                                         //請求医療機関機関_都道府県コード;
        public string f109 { get; set; } = string.Empty;                                         //請求医療機関機関_県内外区分;
        public string f110 { get; set; } = string.Empty;                                         //請求医療機関機関_診療科;
        public string f111 { get; set; } = string.Empty;                                         //給付割合公１_給付割合;
        public string f112 { get; set; } = string.Empty;                                         //給付割合公１_任意給付;
        public string f113 { get; set; } = string.Empty;                                         //給付割合公２_給付割合;
        public string f114 { get; set; } = string.Empty;                                         //給付割合公２_任意給付;
        public string f115 { get; set; } = string.Empty;                                         //給付割合公３_給付割合;
        public string f116 { get; set; } = string.Empty;                                         //給付割合公３_任意給付;
        public string f117 { get; set; } = string.Empty;                                         //給付割合公４_給付割合;
        public string f118 { get; set; } = string.Empty;                                         //給付割合公４_任意給付;
        public string f119 { get; set; } = string.Empty;                                         //給付割合公５_給付割合;
        public string f120 { get; set; } = string.Empty;                                         //給付割合公５_任意給付;
        public string f121 { get; set; } = string.Empty;                                         //療養の給付等（保険）保険_診療実日数;
        public string f122 { get; set; } = string.Empty;                                         //療養の給付等（保険）保険_決定点数;
        public string f123 { get; set; } = string.Empty;                                         //療養の給付等（保険）（公費負担金額）;
        public string f124 { get; set; } = string.Empty;                                         //療養の給付等（保険）（公２負担金額）;
        public string f125 { get; set; } = string.Empty;                                         //療養の給付等（保険）（公３負担金額）;
        public string f126 { get; set; } = string.Empty;                                         //療養の給付等（保険）（公４負担金額）;
        public string f127 { get; set; } = string.Empty;                                         //療養の給付等（保険）（公５負担金額）;
        public string f128 { get; set; } = string.Empty;                                         //療養の給付等（保険）保険_一部負担金;
        public string f129 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_負担者番号;
        public string f130 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_受給者番号;
        public string f131 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_診療実日数;
        public string f132 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_決定点数;
        public string f133 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_増減点数;
        public string f134 { get; set; } = string.Empty;                                         //療養の給付等（公費１）公１_一部負担金;
        public string f135 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_負担者番号;
        public string f136 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_受給者番号;
        public string f137 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_診療実日数;
        public string f138 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_決定点数;
        public string f139 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_増減点数;
        public string f140 { get; set; } = string.Empty;                                         //療養の給付等（公費２）公２_一部負担金;
        public string f141 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_負担者番号;
        public string f142 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_受給者番号;
        public string f143 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_診療実日数;
        public string f144 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_決定点数;
        public string f145 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_増減点数;
        public string f146 { get; set; } = string.Empty;                                         //療養の給付等（公費３）公３_一部負担金;
        public string f147 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_負担者番号;
        public string f148 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_受給者番号;
        public string f149 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_診療実日数;
        public string f150 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_決定点数;
        public string f151 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_増減点数;
        public string f152 { get; set; } = string.Empty;                                         //療養の給付等（公費４）公４_一部負担金;
        public string f153 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_負担者番号;
        public string f154 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_受給者番号;
        public string f155 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_診療実日数;
        public string f156 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_決定点数;
        public string f157 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_増減点数;
        public string f158 { get; set; } = string.Empty;                                         //療養の給付等（公費５）公５_一部負担金;
        public string f159 { get; set; } = string.Empty;                                         //食事（保険）保険_食事回数;
        public string f160 { get; set; } = string.Empty;                                         //食事（保険）保険_食事決定基準額;
        public string f161 { get; set; } = string.Empty;                                         //食事（保険）保険_食事標準負担額;
        public string f162 { get; set; } = string.Empty;                                         //食事（公費１）公１_食事回数;
        public string f163 { get; set; } = string.Empty;                                         //食事（公費１）公１_食事決定基準額;
        public string f164 { get; set; } = string.Empty;                                         //食事（公費１）公１_食事増減基準額;
        public string f165 { get; set; } = string.Empty;                                         //食事（公費１）公１_食事標準負担額;
        public string f166 { get; set; } = string.Empty;                                         //食事（公費１）公１_食事増減標準負担額;
        public string f167 { get; set; } = string.Empty;                                         //食事（公費２）公２_食事回数;
        public string f168 { get; set; } = string.Empty;                                         //食事（公費２）公２_食事決定基準額;
        public string f169 { get; set; } = string.Empty;                                         //食事（公費２）公２_食事増減基準額;
        public string f170 { get; set; } = string.Empty;                                         //食事（公費２）公２_食事標準負担額;
        public string f171 { get; set; } = string.Empty;                                         //食事（公費２）公２_食事増減標準負担額;
        public string f172 { get; set; } = string.Empty;                                         //食事（公費３）公３_食事回数;
        public string f173 { get; set; } = string.Empty;                                         //食事（公費３）公３_食事決定基準額;
        public string f174 { get; set; } = string.Empty;                                         //食事（公費３）公３_食事増減基準額;
        public string f175 { get; set; } = string.Empty;                                         //食事（公費３）公３_食事標準負担額;
        public string f176 { get; set; } = string.Empty;                                         //食事（公費３）公３_食事増減標準負担額;
        public string f177 { get; set; } = string.Empty;                                         //食事（公費４）公４_食事回数;
        public string f178 { get; set; } = string.Empty;                                         //食事（公費４）公４_食事決定基準額;
        public string f179 { get; set; } = string.Empty;                                         //食事（公費４）公４_食事増減基準額;
        public string f180 { get; set; } = string.Empty;                                         //食事（公費４）公４_食事標準負担額;
        public string f181 { get; set; } = string.Empty;                                         //食事（公費４）公４_食事増減標準負担額;
        public string f182 { get; set; } = string.Empty;                                         //食事（公費５）公５_食事回数;
        public string f183 { get; set; } = string.Empty;                                         //食事（公費５）公５_食事決定基準額;
        public string f184 { get; set; } = string.Empty;                                         //食事（公費５）公５_食事増減基準額;
        public string f185 { get; set; } = string.Empty;                                         //食事（公費５）公５_食事標準負担額;
        public string f186 { get; set; } = string.Empty;                                         //食事（公費５）公５_食事増減標準負担額;
        public string f187 { get; set; } = string.Empty;                                         //その他所得区分;
        public string f188 { get; set; } = string.Empty;                                         //その他本人家族区分;
        public string f189 { get; set; } = string.Empty;                                         //その他入院外来区分;
        public string f190 { get; set; } = string.Empty;                                         //その他在総診・在医総区分;
        public string f191 { get; set; } = string.Empty;                                         //その他Ⅰ;
        public string f192 { get; set; } = string.Empty;                                         //その他Ⅱ;
        public string f193 { get; set; } = string.Empty;                                         //その他三月超;
        public string f194 { get; set; } = string.Empty;                                         //その他多数該当;
        public string f195 { get; set; } = string.Empty;                                         //その他経過措置区分;
        public string f196 { get; set; } = string.Empty;                                         //その他市町村保険者変更;
        public string f197 { get; set; } = string.Empty;                                         //その他特別療養費;
        public string f198 { get; set; } = string.Empty;                                         //その他マル公区分;
        public string f199 { get; set; } = string.Empty;                                         //その他長期区分;
        public string f200 { get; set; } = string.Empty;                                         //その他限度額適用認定証区分;
        public string f201 { get; set; } = string.Empty;                                         //その他前期該当区分;
        public string f202 { get; set; } = string.Empty;                                         //その他二割徴収者;
        public string f203 { get; set; } = string.Empty;                                         //その他月中該当区分;
        public string f204 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_決定点数;
        public string f205 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_費用額;
        public string f206 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_負担者負担額;
        public string f207 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_高額療養費;
        public string f208 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_長期高額療養費;
        public string f209 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_患者負担額;
        public string f210 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_他法優先公費負担額;
        public string f211 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_国保優先公費負担額;
        public string f212 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_任意給付額;
        public string f213 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_減免猶予額;
        public string f214 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_食事基準額;
        public string f215 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_食事負担者負担額;
        public string f216 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_食事患者負担額;
        public string f217 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_食事他法優先公費負担額;
        public string f218 { get; set; } = string.Empty;                                         //費用算定結果値保険算定_保険_食事国保優先公費負担額;
        public string f219 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_決定点数;
        public string f220 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_費用額;
        public string f221 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_負担者負担金額;
        public string f222 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_高額療養費;
        public string f223 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_長期高額療養費;
        public string f224 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_患者負担額;
        public string f225 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_任意給付額;
        public string f226 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_食事基準額;
        public string f227 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_食事負担者負担額;
        public string f228 { get; set; } = string.Empty;                                         //費用算定結果値公費１算定_公１_食事患者負担額;
        public string f229 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_決定点数;
        public string f230 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_費用額;
        public string f231 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_負担者負担金額;
        public string f232 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_高額療養費;
        public string f233 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_長期高額療養費;
        public string f234 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_患者負担額;
        public string f235 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_任意給付額;
        public string f236 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_食事基準額;
        public string f237 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_食事負担者負担額;
        public string f238 { get; set; } = string.Empty;                                         //費用算定結果値公費２算定_公２_食事患者負担額;
        public string f239 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_決定点数;
        public string f240 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_費用額;
        public string f241 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_負担者負担金額;
        public string f242 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_高額療養費;
        public string f243 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_長期高額療養費;
        public string f244 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_患者負担額;
        public string f245 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_任意給付額;
        public string f246 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_食事基準額;
        public string f247 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_食事負担者負担額;
        public string f248 { get; set; } = string.Empty;                                         //費用算定結果値公費３算定_公３_食事患者負担額;
        public string f249 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_決定点数;
        public string f250 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_費用額;
        public string f251 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_負担者負担金額;
        public string f252 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_高額療養費;
        public string f253 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_長期高額療養費;
        public string f254 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_患者負担額;
        public string f255 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_任意給付額;
        public string f256 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_食事基準額;
        public string f257 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_食事負担者負担額;
        public string f258 { get; set; } = string.Empty;                                         //費用算定結果値公費４算定_公４_食事患者負担額;
        public string f259 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_決定点数;
        public string f260 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_費用額;
        public string f261 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_負担者負担金額;
        public string f262 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_高額療養費;
        public string f263 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_長期高額療養費;
        public string f264 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_患者負担額;
        public string f265 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_任意給付額;
        public string f266 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_食事基準額;
        public string f267 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_食事負担者負担額;
        public string f268 { get; set; } = string.Empty;                                         //費用算定結果値公費５算定_公５_食事患者負担額;
        public string f269 { get; set; } = string.Empty;                                         //エラー制御重複エラー区分;
        public string f270 { get; set; } = string.Empty;                                         //エラー制御資格チェックエラー項目情報;
        public string f271 { get; set; } = string.Empty;                                         //エラー制御資格チェックエラーコード;
        public string f272 { get; set; } = string.Empty;                                         //エラー制御給付チェックエラーコード;
        public string f273 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_医療機関名(漢字);
        public string f274 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_柔整団体機関名(漢字);
        public string f275 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_支給決定年月日;
        public string f276 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_高額療養費;
        public string f277 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_振込銀行コード;
        public string f278 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_振込支店コード;
        public string f279 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_預金種目;
        public string f280 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_口座名義人（カナ）;
        public string f281 { get; set; } = string.Empty;                                         //療養費窓口申請分管理項目領域療_口座名義人（漢字）;
        public string f282 { get; set; } = string.Empty;                                         //過誤再審査情報過誤・再審査データ区分;
        public string f283 { get; set; } = string.Empty;                                         //過誤再審査情報過誤・再審査管理年月;
        public string f284 { get; set; } = string.Empty;                                         //過誤再審査情報過誤・再審査理由番号;
        public string f285 { get; set; } = string.Empty;                                         //過誤再審査情報過誤・再審査審査結果;
        public string f286 { get; set; } = string.Empty;                                         //過誤再審査情報過誤区分;
        public string f287 { get; set; } = string.Empty;                                         //過誤再審査情報過誤・再審査結果年月;

        #endregion

        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 受診者名データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool importdata3_main(int _cym, WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //dataimport_AHKに登録
                if(!EntryExcelData(strFileName, wf)) return false;


                
                wf.LogPrint("Appテーブル作成");
                ////レセプト全国共通キーで紐づけてアップデートする
                if (!UpdateApp(intcym)) return false;


                //20211101150107 furukawa st ////////////////////////
                //auxにレセプト全国共通キーを入れておく
                
                wf.LogPrint("AUXテーブル更新");
                if (!UpdateAUX(intcym)) return false;
                //20211101150107 furukawa ed ////////////////////////


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        //20211101150011 furukawa st ////////////////////////
        //auxにレセプト全国共通キーを入れておく
        
        /// <summary>
        /// AUX更新
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool UpdateAUX(int cym)
        {

            string strsql =
                "update application_aux set    " +
                "  matchingid02     = ip3.f002comnum " +            
                " from importdata3 ip3,application a  " +
                " where " +
                $" a.comnum=ip3.f002comnum " +
                $" and a.cym={cym}" +
                $" and a.aid=application_aux.aid ";

            DB.Command cmd = new DB.Command(DB.Main, strsql, true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        //20211101150011 furukawa ed ////////////////////////

        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +       
                "  pname     = ip3.f095 " +                          //受療者名=>資格設定情報個人特定情報被保険者氏名
                ",taggeddatas	              " +
                "       =taggeddatas || '|GeneralString3:\"' || trim(ip3.f016)|| '\"' || '|GeneralString4:\"' || ip3.f015personalnum || '\"' " +//世帯番号,個人番号

                " from importdata3 ip3 " +
                " where " +
                $" application.comnum=ip3.f002comnum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main, strsql,true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        private partial class names
        {
            public string comnum { get; set; } = string.Empty;
            public string zip { get; set; } = string.Empty;
            public string address { get; set; } = string.Empty;
            public string name { get; set; } = string.Empty;
            public string kana { get; set; } = string.Empty;
        }

        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {

            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd = new DB.Command($"delete from importdata3 where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            int c = 0;//インポートファイルの列
            int r = 1;//インポートファイルの行
            try
            {

                wf.InvokeValue = 0;
                wf.SetMax(ws.LastRowNum);

                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;

                for (r = 1; r <= ws.LastRowNum; r++)
                {
                    importdata3 imp = new importdata3();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    for (c = 0; c < cellcnt; c++)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        if (c == 0) imp.f001shoriym = getCellValueToString(cell);
                        if (c == 1) imp.f002comnum = getCellValueToString(cell);
                        if (c == 2) imp.f003 = getCellValueToString(cell);
                        if (c == 3) imp.f004rezeptnum = getCellValueToString(cell);
                        if (c == 4) imp.f005 = getCellValueToString(cell);
                        if (c == 5) imp.f006clinicnum = getCellValueToString(cell);
                        if (c == 6) imp.f007 = getCellValueToString(cell);
                        if (c == 7) imp.f008 = getCellValueToString(cell);
                        if (c == 8) imp.f009insnum = getCellValueToString(cell);
                        if (c == 9) imp.f010hmark = getCellValueToString(cell);
                        if (c == 10) imp.f011hnum = getCellValueToString(cell);
                        if (c == 11) imp.f012pbirthday = getCellValueToString(cell);
                        if (c == 12) imp.f013pgender = getCellValueToString(cell);
                        if (c == 13) imp.f014pname = getCellValueToString(cell);
                        if (c == 14) imp.f015personalnum = getCellValueToString(cell);
                        if (c == 15) imp.f016 = getCellValueToString(cell);
                        if (c == 16) imp.f017 = getCellValueToString(cell);
                        if (c == 17) imp.f018 = getCellValueToString(cell);
                        if (c == 18) imp.f019 = getCellValueToString(cell);
                        if (c == 19) imp.f020 = getCellValueToString(cell);
                        if (c == 20) imp.f021 = getCellValueToString(cell);
                        if (c == 21) imp.f022 = getCellValueToString(cell);
                        if (c == 22) imp.f023 = getCellValueToString(cell);
                        if (c == 23) imp.f024 = getCellValueToString(cell);
                        if (c == 24) imp.f025 = getCellValueToString(cell);
                        if (c == 25) imp.f026 = getCellValueToString(cell);
                        if (c == 26) imp.f027 = getCellValueToString(cell);
                        if (c == 27) imp.f028 = getCellValueToString(cell);
                        if (c == 28) imp.f029 = getCellValueToString(cell);
                        if (c == 29) imp.f030 = getCellValueToString(cell);
                        if (c == 30) imp.f031 = getCellValueToString(cell);
                        if (c == 31) imp.f032 = getCellValueToString(cell);
                        if (c == 32) imp.f033 = getCellValueToString(cell);
                        if (c == 33) imp.f034 = getCellValueToString(cell);
                        if (c == 34) imp.f035 = getCellValueToString(cell);
                        if (c == 35) imp.f036 = getCellValueToString(cell);
                        if (c == 36) imp.f037 = getCellValueToString(cell);
                        if (c == 37) imp.f038 = getCellValueToString(cell);
                        if (c == 38) imp.f039 = getCellValueToString(cell);
                        if (c == 39) imp.f040 = getCellValueToString(cell);
                        if (c == 40) imp.f041 = getCellValueToString(cell);
                        if (c == 41) imp.f042 = getCellValueToString(cell);
                        if (c == 42) imp.f043 = getCellValueToString(cell);
                        if (c == 43) imp.f044 = getCellValueToString(cell);
                        if (c == 44) imp.f045 = getCellValueToString(cell);
                        if (c == 45) imp.f046 = getCellValueToString(cell);
                        if (c == 46) imp.f047 = getCellValueToString(cell);
                        if (c == 47) imp.f048 = getCellValueToString(cell);
                        if (c == 48) imp.f049 = getCellValueToString(cell);
                        if (c == 49) imp.f050 = getCellValueToString(cell);
                        if (c == 50) imp.f051 = getCellValueToString(cell);
                        if (c == 51) imp.f052 = getCellValueToString(cell);
                        if (c == 52) imp.f053 = getCellValueToString(cell);
                        if (c == 53) imp.f054 = getCellValueToString(cell);
                        if (c == 54) imp.f055 = getCellValueToString(cell);
                        if (c == 55) imp.f056 = getCellValueToString(cell);
                        if (c == 56) imp.f057 = getCellValueToString(cell);
                        if (c == 57) imp.f058 = getCellValueToString(cell);
                        if (c == 58) imp.f059 = getCellValueToString(cell);
                        if (c == 59) imp.f060 = getCellValueToString(cell);
                        if (c == 60) imp.f061 = getCellValueToString(cell);
                        if (c == 61) imp.f062 = getCellValueToString(cell);
                        if (c == 62) imp.f063 = getCellValueToString(cell);
                        if (c == 63) imp.f064 = getCellValueToString(cell);
                        if (c == 64) imp.f065 = getCellValueToString(cell);
                        if (c == 65) imp.f066 = getCellValueToString(cell);
                        if (c == 66) imp.f067 = getCellValueToString(cell);
                        if (c == 67) imp.f068 = getCellValueToString(cell);
                        if (c == 68) imp.f069 = getCellValueToString(cell);
                        if (c == 69) imp.f070 = getCellValueToString(cell);
                        if (c == 70) imp.f071 = getCellValueToString(cell);
                        if (c == 71) imp.f072 = getCellValueToString(cell);
                        if (c == 72) imp.f073 = getCellValueToString(cell);
                        if (c == 73) imp.f074 = getCellValueToString(cell);
                        if (c == 74) imp.f075 = getCellValueToString(cell);
                        if (c == 75) imp.f076 = getCellValueToString(cell);
                        if (c == 76) imp.f077 = getCellValueToString(cell);
                        if (c == 77) imp.f078 = getCellValueToString(cell);
                        if (c == 78) imp.f079 = getCellValueToString(cell);
                        if (c == 79) imp.f080 = getCellValueToString(cell);
                        if (c == 80) imp.f081 = getCellValueToString(cell);
                        if (c == 81) imp.f082 = getCellValueToString(cell);
                        if (c == 82) imp.f083 = getCellValueToString(cell);
                        if (c == 83) imp.f084 = getCellValueToString(cell);
                        if (c == 84) imp.f085 = getCellValueToString(cell);
                        if (c == 85) imp.f086 = getCellValueToString(cell);
                        if (c == 86) imp.f087 = getCellValueToString(cell);
                        if (c == 87) imp.f088 = getCellValueToString(cell);
                        if (c == 88) imp.f089 = getCellValueToString(cell);
                        if (c == 89) imp.f090 = getCellValueToString(cell);
                        if (c == 90) imp.f091 = getCellValueToString(cell);
                        if (c == 91) imp.f092 = getCellValueToString(cell);
                        if (c == 92) imp.f093 = getCellValueToString(cell);
                        if (c == 93) imp.f094 = getCellValueToString(cell);
                        if (c == 94) imp.f095 = getCellValueToString(cell);
                        if (c == 95) imp.f096 = getCellValueToString(cell);
                        if (c == 96) imp.f097 = getCellValueToString(cell);
                        if (c == 97) imp.f098 = getCellValueToString(cell);
                        if (c == 98) imp.f099 = getCellValueToString(cell);
                        if (c == 99) imp.f100 = getCellValueToString(cell);
                        if (c == 100) imp.f101 = getCellValueToString(cell);
                        if (c == 101) imp.f102 = getCellValueToString(cell);
                        if (c == 102) imp.f103 = getCellValueToString(cell);
                        if (c == 103) imp.f104 = getCellValueToString(cell);
                        if (c == 104) imp.f105 = getCellValueToString(cell);
                        if (c == 105) imp.f106 = getCellValueToString(cell);
                        if (c == 106) imp.f107 = getCellValueToString(cell);
                        if (c == 107) imp.f108 = getCellValueToString(cell);
                        if (c == 108) imp.f109 = getCellValueToString(cell);
                        if (c == 109) imp.f110 = getCellValueToString(cell);
                        if (c == 110) imp.f111 = getCellValueToString(cell);
                        if (c == 111) imp.f112 = getCellValueToString(cell);
                        if (c == 112) imp.f113 = getCellValueToString(cell);
                        if (c == 113) imp.f114 = getCellValueToString(cell);
                        if (c == 114) imp.f115 = getCellValueToString(cell);
                        if (c == 115) imp.f116 = getCellValueToString(cell);
                        if (c == 116) imp.f117 = getCellValueToString(cell);
                        if (c == 117) imp.f118 = getCellValueToString(cell);
                        if (c == 118) imp.f119 = getCellValueToString(cell);
                        if (c == 119) imp.f120 = getCellValueToString(cell);
                        if (c == 120) imp.f121 = getCellValueToString(cell);
                        if (c == 121) imp.f122 = getCellValueToString(cell);
                        if (c == 122) imp.f123 = getCellValueToString(cell);
                        if (c == 123) imp.f124 = getCellValueToString(cell);
                        if (c == 124) imp.f125 = getCellValueToString(cell);
                        if (c == 125) imp.f126 = getCellValueToString(cell);
                        if (c == 126) imp.f127 = getCellValueToString(cell);
                        if (c == 127) imp.f128 = getCellValueToString(cell);
                        if (c == 128) imp.f129 = getCellValueToString(cell);
                        if (c == 129) imp.f130 = getCellValueToString(cell);
                        if (c == 130) imp.f131 = getCellValueToString(cell);
                        if (c == 131) imp.f132 = getCellValueToString(cell);
                        if (c == 132) imp.f133 = getCellValueToString(cell);
                        if (c == 133) imp.f134 = getCellValueToString(cell);
                        if (c == 134) imp.f135 = getCellValueToString(cell);
                        if (c == 135) imp.f136 = getCellValueToString(cell);
                        if (c == 136) imp.f137 = getCellValueToString(cell);
                        if (c == 137) imp.f138 = getCellValueToString(cell);
                        if (c == 138) imp.f139 = getCellValueToString(cell);
                        if (c == 139) imp.f140 = getCellValueToString(cell);
                        if (c == 140) imp.f141 = getCellValueToString(cell);
                        if (c == 141) imp.f142 = getCellValueToString(cell);
                        if (c == 142) imp.f143 = getCellValueToString(cell);
                        if (c == 143) imp.f144 = getCellValueToString(cell);
                        if (c == 144) imp.f145 = getCellValueToString(cell);
                        if (c == 145) imp.f146 = getCellValueToString(cell);
                        if (c == 146) imp.f147 = getCellValueToString(cell);
                        if (c == 147) imp.f148 = getCellValueToString(cell);
                        if (c == 148) imp.f149 = getCellValueToString(cell);
                        if (c == 149) imp.f150 = getCellValueToString(cell);
                        if (c == 150) imp.f151 = getCellValueToString(cell);
                        if (c == 151) imp.f152 = getCellValueToString(cell);
                        if (c == 152) imp.f153 = getCellValueToString(cell);
                        if (c == 153) imp.f154 = getCellValueToString(cell);
                        if (c == 154) imp.f155 = getCellValueToString(cell);
                        if (c == 155) imp.f156 = getCellValueToString(cell);
                        if (c == 156) imp.f157 = getCellValueToString(cell);
                        if (c == 157) imp.f158 = getCellValueToString(cell);
                        if (c == 158) imp.f159 = getCellValueToString(cell);
                        if (c == 159) imp.f160 = getCellValueToString(cell);
                        if (c == 160) imp.f161 = getCellValueToString(cell);
                        if (c == 161) imp.f162 = getCellValueToString(cell);
                        if (c == 162) imp.f163 = getCellValueToString(cell);
                        if (c == 163) imp.f164 = getCellValueToString(cell);
                        if (c == 164) imp.f165 = getCellValueToString(cell);
                        if (c == 165) imp.f166 = getCellValueToString(cell);
                        if (c == 166) imp.f167 = getCellValueToString(cell);
                        if (c == 167) imp.f168 = getCellValueToString(cell);
                        if (c == 168) imp.f169 = getCellValueToString(cell);
                        if (c == 169) imp.f170 = getCellValueToString(cell);
                        if (c == 170) imp.f171 = getCellValueToString(cell);
                        if (c == 171) imp.f172 = getCellValueToString(cell);
                        if (c == 172) imp.f173 = getCellValueToString(cell);
                        if (c == 173) imp.f174 = getCellValueToString(cell);
                        if (c == 174) imp.f175 = getCellValueToString(cell);
                        if (c == 175) imp.f176 = getCellValueToString(cell);
                        if (c == 176) imp.f177 = getCellValueToString(cell);
                        if (c == 177) imp.f178 = getCellValueToString(cell);
                        if (c == 178) imp.f179 = getCellValueToString(cell);
                        if (c == 179) imp.f180 = getCellValueToString(cell);
                        if (c == 180) imp.f181 = getCellValueToString(cell);
                        if (c == 181) imp.f182 = getCellValueToString(cell);
                        if (c == 182) imp.f183 = getCellValueToString(cell);
                        if (c == 183) imp.f184 = getCellValueToString(cell);
                        if (c == 184) imp.f185 = getCellValueToString(cell);
                        if (c == 185) imp.f186 = getCellValueToString(cell);
                        if (c == 186) imp.f187 = getCellValueToString(cell);
                        if (c == 187) imp.f188 = getCellValueToString(cell);
                        if (c == 188) imp.f189 = getCellValueToString(cell);
                        if (c == 189) imp.f190 = getCellValueToString(cell);
                        if (c == 190) imp.f191 = getCellValueToString(cell);
                        if (c == 191) imp.f192 = getCellValueToString(cell);
                        if (c == 192) imp.f193 = getCellValueToString(cell);
                        if (c == 193) imp.f194 = getCellValueToString(cell);
                        if (c == 194) imp.f195 = getCellValueToString(cell);
                        if (c == 195) imp.f196 = getCellValueToString(cell);
                        if (c == 196) imp.f197 = getCellValueToString(cell);
                        if (c == 197) imp.f198 = getCellValueToString(cell);
                        if (c == 198) imp.f199 = getCellValueToString(cell);
                        if (c == 199) imp.f200 = getCellValueToString(cell);
                        if (c == 200) imp.f201 = getCellValueToString(cell);
                        if (c == 201) imp.f202 = getCellValueToString(cell);
                        if (c == 202) imp.f203 = getCellValueToString(cell);
                        if (c == 203) imp.f204 = getCellValueToString(cell);
                        if (c == 204) imp.f205 = getCellValueToString(cell);
                        if (c == 205) imp.f206 = getCellValueToString(cell);
                        if (c == 206) imp.f207 = getCellValueToString(cell);
                        if (c == 207) imp.f208 = getCellValueToString(cell);
                        if (c == 208) imp.f209 = getCellValueToString(cell);
                        if (c == 209) imp.f210 = getCellValueToString(cell);
                        if (c == 210) imp.f211 = getCellValueToString(cell);
                        if (c == 211) imp.f212 = getCellValueToString(cell);
                        if (c == 212) imp.f213 = getCellValueToString(cell);
                        if (c == 213) imp.f214 = getCellValueToString(cell);
                        if (c == 214) imp.f215 = getCellValueToString(cell);
                        if (c == 215) imp.f216 = getCellValueToString(cell);
                        if (c == 216) imp.f217 = getCellValueToString(cell);
                        if (c == 217) imp.f218 = getCellValueToString(cell);
                        if (c == 218) imp.f219 = getCellValueToString(cell);
                        if (c == 219) imp.f220 = getCellValueToString(cell);
                        if (c == 220) imp.f221 = getCellValueToString(cell);
                        if (c == 221) imp.f222 = getCellValueToString(cell);
                        if (c == 222) imp.f223 = getCellValueToString(cell);
                        if (c == 223) imp.f224 = getCellValueToString(cell);
                        if (c == 224) imp.f225 = getCellValueToString(cell);
                        if (c == 225) imp.f226 = getCellValueToString(cell);
                        if (c == 226) imp.f227 = getCellValueToString(cell);
                        if (c == 227) imp.f228 = getCellValueToString(cell);
                        if (c == 228) imp.f229 = getCellValueToString(cell);
                        if (c == 229) imp.f230 = getCellValueToString(cell);
                        if (c == 230) imp.f231 = getCellValueToString(cell);
                        if (c == 231) imp.f232 = getCellValueToString(cell);
                        if (c == 232) imp.f233 = getCellValueToString(cell);
                        if (c == 233) imp.f234 = getCellValueToString(cell);
                        if (c == 234) imp.f235 = getCellValueToString(cell);
                        if (c == 235) imp.f236 = getCellValueToString(cell);
                        if (c == 236) imp.f237 = getCellValueToString(cell);
                        if (c == 237) imp.f238 = getCellValueToString(cell);
                        if (c == 238) imp.f239 = getCellValueToString(cell);
                        if (c == 239) imp.f240 = getCellValueToString(cell);
                        if (c == 240) imp.f241 = getCellValueToString(cell);
                        if (c == 241) imp.f242 = getCellValueToString(cell);
                        if (c == 242) imp.f243 = getCellValueToString(cell);
                        if (c == 243) imp.f244 = getCellValueToString(cell);
                        if (c == 244) imp.f245 = getCellValueToString(cell);
                        if (c == 245) imp.f246 = getCellValueToString(cell);
                        if (c == 246) imp.f247 = getCellValueToString(cell);
                        if (c == 247) imp.f248 = getCellValueToString(cell);
                        if (c == 248) imp.f249 = getCellValueToString(cell);
                        if (c == 249) imp.f250 = getCellValueToString(cell);
                        if (c == 250) imp.f251 = getCellValueToString(cell);
                        if (c == 251) imp.f252 = getCellValueToString(cell);
                        if (c == 252) imp.f253 = getCellValueToString(cell);
                        if (c == 253) imp.f254 = getCellValueToString(cell);
                        if (c == 254) imp.f255 = getCellValueToString(cell);
                        if (c == 255) imp.f256 = getCellValueToString(cell);
                        if (c == 256) imp.f257 = getCellValueToString(cell);
                        if (c == 257) imp.f258 = getCellValueToString(cell);
                        if (c == 258) imp.f259 = getCellValueToString(cell);
                        if (c == 259) imp.f260 = getCellValueToString(cell);
                        if (c == 260) imp.f261 = getCellValueToString(cell);
                        if (c == 261) imp.f262 = getCellValueToString(cell);
                        if (c == 262) imp.f263 = getCellValueToString(cell);
                        if (c == 263) imp.f264 = getCellValueToString(cell);
                        if (c == 264) imp.f265 = getCellValueToString(cell);
                        if (c == 265) imp.f266 = getCellValueToString(cell);
                        if (c == 266) imp.f267 = getCellValueToString(cell);
                        if (c == 267) imp.f268 = getCellValueToString(cell);
                        if (c == 268) imp.f269 = getCellValueToString(cell);
                        if (c == 269) imp.f270 = getCellValueToString(cell);
                        if (c == 270) imp.f271 = getCellValueToString(cell);
                        if (c == 271) imp.f272 = getCellValueToString(cell);
                        if (c == 272) imp.f273 = getCellValueToString(cell);
                        if (c == 273) imp.f274 = getCellValueToString(cell);
                        if (c == 274) imp.f275 = getCellValueToString(cell);
                        if (c == 275) imp.f276 = getCellValueToString(cell);
                        if (c == 276) imp.f277 = getCellValueToString(cell);
                        if (c == 277) imp.f278 = getCellValueToString(cell);
                        if (c == 278) imp.f279 = getCellValueToString(cell);
                        if (c == 279) imp.f280 = getCellValueToString(cell);
                        if (c == 280) imp.f281 = getCellValueToString(cell);
                        if (c == 281) imp.f282 = getCellValueToString(cell);
                        if (c == 282) imp.f283 = getCellValueToString(cell);
                        if (c == 283) imp.f284 = getCellValueToString(cell);


                       

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"受療者データ  レセプト全国共通キー:{imp.f002comnum}");

                    if (!DB.Main.Insert<importdata3>(imp, tran)) return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
                wb.Close();
            }
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }



    }


    /// <summary>
    /// 既出テーブルだが使わないかも
    /// (既に出力した人は出さない場合があるので、その人を記録しておくテーブル)
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
