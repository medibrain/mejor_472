﻿namespace Mejor.Kashiwashi
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnFile3 = new System.Windows.Forms.Button();
            this.btnFile2 = new System.Windows.Forms.Button();
            this.btnFile1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_juryo = new System.Windows.Forms.TextBox();
            this.textBox_sejutsu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImpJyusei = new System.Windows.Forms.Button();
            this.gbJ.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxBase
            // 
            this.textBoxBase.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBase.Location = new System.Drawing.Point(180, 29);
            this.textBoxBase.Multiline = true;
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(512, 44);
            this.textBoxBase.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.Controls.Add(this.btnFile3);
            this.gbJ.Controls.Add(this.btnFile2);
            this.gbJ.Controls.Add(this.btnFile1);
            this.gbJ.Controls.Add(this.label4);
            this.gbJ.Controls.Add(this.label2);
            this.gbJ.Controls.Add(this.textBox_juryo);
            this.gbJ.Controls.Add(this.textBox_sejutsu);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxBase);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(43, 12);
            this.gbJ.Name = "gbJ";
            this.gbJ.Size = new System.Drawing.Size(786, 207);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            this.gbJ.Text = "柔整";
            // 
            // btnFile3
            // 
            this.btnFile3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile3.Location = new System.Drawing.Point(698, 145);
            this.btnFile3.Name = "btnFile3";
            this.btnFile3.Size = new System.Drawing.Size(48, 31);
            this.btnFile3.TabIndex = 3;
            this.btnFile3.Text = "...";
            this.btnFile3.UseVisualStyleBackColor = true;
            this.btnFile3.Click += new System.EventHandler(this.btnFile3_Click);
            // 
            // btnFile2
            // 
            this.btnFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile2.Location = new System.Drawing.Point(698, 81);
            this.btnFile2.Name = "btnFile2";
            this.btnFile2.Size = new System.Drawing.Size(48, 35);
            this.btnFile2.TabIndex = 3;
            this.btnFile2.Text = "...";
            this.btnFile2.UseVisualStyleBackColor = true;
            this.btnFile2.Click += new System.EventHandler(this.btnFile2_Click);
            // 
            // btnFile1
            // 
            this.btnFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile1.Location = new System.Drawing.Point(698, 30);
            this.btnFile1.Name = "btnFile1";
            this.btnFile1.Size = new System.Drawing.Size(48, 31);
            this.btnFile1.TabIndex = 3;
            this.btnFile1.Text = "...";
            this.btnFile1.UseVisualStyleBackColor = true;
            this.btnFile1.Click += new System.EventHandler(this.btnFile1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "受療者名データ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "施術所名データ";
            // 
            // textBox_juryo
            // 
            this.textBox_juryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_juryo.Location = new System.Drawing.Point(180, 144);
            this.textBox_juryo.Multiline = true;
            this.textBox_juryo.Name = "textBox_juryo";
            this.textBox_juryo.Size = new System.Drawing.Size(512, 45);
            this.textBox_juryo.TabIndex = 0;
            // 
            // textBox_sejutsu
            // 
            this.textBox_sejutsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_sejutsu.Location = new System.Drawing.Point(180, 81);
            this.textBox_sejutsu.Multiline = true;
            this.textBox_sejutsu.Name = "textBox_sejutsu";
            this.textBox_sejutsu.Size = new System.Drawing.Size(512, 48);
            this.textBox_sejutsu.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "レセプトデータ";
            // 
            // btnImpJyusei
            // 
            this.btnImpJyusei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImpJyusei.Location = new System.Drawing.Point(729, 239);
            this.btnImpJyusei.Name = "btnImpJyusei";
            this.btnImpJyusei.Size = new System.Drawing.Size(115, 34);
            this.btnImpJyusei.TabIndex = 3;
            this.btnImpJyusei.Text = "取込";
            this.btnImpJyusei.UseVisualStyleBackColor = true;
            this.btnImpJyusei.Click += new System.EventHandler(this.btnImpJyusei_Click);
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 294);
            this.Controls.Add(this.btnImpJyusei);
            this.Controls.Add(this.gbJ);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImpJyusei;
        private System.Windows.Forms.Button btnFile2;
        private System.Windows.Forms.Button btnFile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_sejutsu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_juryo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFile3;
    }
}