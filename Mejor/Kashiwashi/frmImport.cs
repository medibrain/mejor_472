﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Kashiwashi
{
    public partial class frmImport : Form
    {

        public  string strFileBaseData { get; set; } = string.Empty;
        public  string strFile2 { get; set; } = string.Empty;
        public  string strFile3 { get; set; } = string.Empty;

        private  static int _cym;

        public frmImport(int cym)
        {
            InitializeComponent();
            _cym = cym;

        }
        
        

        private void btnFile1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "レセデータ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;
            textBoxBase.Text= ofd.FileName;
            
        }

        //柔整だけだろう
        private void btnFile2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "施術所名データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBox_sejutsu.Text = ofd.FileName;
        }

        private void btnFile3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "受診者名データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBox_juryo.Text = ofd.FileName;
        }

        private void btnImpJyusei_Click(object sender, EventArgs e)
        {
            strFileBaseData = textBoxBase.Text.Trim();
            strFile2 = textBox_sejutsu.Text.Trim();
            strFile3 = textBox_juryo.Text.Trim();
                
            this.Close();
        }

       
    }
}
