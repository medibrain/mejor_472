﻿using System;

namespace Mejor
{
    class FileUtility
    {
        ////////////////////////////////////////////////////////////////////
        ////
        //// フォルダ関連
        ////
        ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 指定フォルダが存在するかを確認します。エラーが発生した場合はfalseを返します。
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static bool FolderExist(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(folderPath)) return false;
            try
            {
                return System.IO.Directory.Exists(folderPath);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 指定フォルダパスのフォルダ名を取得します。失敗した場合はnullを返します。
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string GetDirectoryName(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(folderPath)) return null;
            try
            {
                return System.IO.Path.GetFileName(folderPath);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定パス（フォルダ、ファイル）の親フォルダ名を取得します。失敗した場合はnullを返します。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetParentDirectoryName(string path)
        {            
            if (string.IsNullOrWhiteSpace(path)) return null;
            try
            {
                return System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(path));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定したパスの親フォルダパスを取得します。失敗した場合はnullを返します。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetParentDirectoryPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return null;
            try
            {
                return System.IO.Path.GetDirectoryName(path);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定フォルダ内に存在するすべてのフォルダのパスを取得します。
        /// subがtrueの場合はサブフォルダを含む全フォルダのパスを取得します。
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="pattern"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        public static string[] GetFoldersInFolder(
            string folderPath, 
            bool sub,
            string pattern = "*", 
            System.IO.SearchOption searchOption = System.IO.SearchOption.AllDirectories)
        {
            if (string.IsNullOrWhiteSpace(folderPath)) return null;
            try
            {
                if(sub) return System.IO.Directory.GetDirectories(folderPath, pattern, searchOption);
                else return System.IO.Directory.GetDirectories(folderPath, pattern);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定パスの親・サブフォルダを含めてすべて作成します。既に存在する場合でもtrueを返します。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool CreateDirectories(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return false;
            var folderPath = GetParentDirectoryPath(path);
            if (FolderExist(folderPath))
            {
                return true;
            }
            try
            {
                System.IO.Directory.CreateDirectory(folderPath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 指定フォルダをサブフォルダ・ファイル含めてすべて削除します。存在しない場合でもtrueを返します。
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static bool FolderDelete(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(folderPath)) return false;
            if (!FolderExist(folderPath)) return true;
            try
            {
                System.IO.Directory.Delete(folderPath, true);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }
        }

        ////////////////////////////////////////////////////////////////////
        ////
        //// ファイル関連
        ////
        ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 指定ファイルが存在するかを確認します。エラーが発生した場合はfalseを返します。
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool FileExist(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath)) return false;
            try
            {
                return System.IO.File.Exists(filePath);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 指定ファイルパスのファイル名を取得します。失敗した場合はnullを返します。
        /// extensionをtrueにした場合は拡張子有りになります
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileName(string path, bool extension = false)
        {
            if (string.IsNullOrWhiteSpace(path)) return null;
            try
            {
                if (extension) return System.IO.Path.GetFileName(path);
                else return System.IO.Path.GetFileNameWithoutExtension(path);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定ファイルの拡張子を取得します。lower：小文字にします
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="lower"></param>
        /// <returns></returns>
        public static string GetExtension(string filePath, bool lower = true)
        {
            if (string.IsNullOrWhiteSpace(filePath)) return null;
            try
            {
                if (lower) return System.IO.Path.GetExtension(filePath).ToLower();
                else return System.IO.Path.GetExtension(filePath);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定フォルダ内に存在するすべてのファイルのパスを取得します。
        /// subをtrueにした場合はサブを含む全ファイルを取得します。
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="pattern"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        public static string[] GetFilesInFolder(
            string folderPath, 
            bool sub,
            string pattern = "*", 
            System.IO.SearchOption searchOption = System.IO.SearchOption.AllDirectories)
        {
            if (string.IsNullOrWhiteSpace(folderPath)) return null;
            try
            {
                if (sub) return System.IO.Directory.GetFiles(folderPath, pattern, searchOption);
                else return System.IO.Directory.GetFiles(folderPath, pattern);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 指定ファイルパスを新しいパスにコピーします。フォルダが無い場合は自動的に作成されます。
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="newPath"></param>
        /// <returns></returns>
        public static bool FileCopy(string filePath, string newPath)
        {
            if (string.IsNullOrWhiteSpace(filePath)) return false;
            if (string.IsNullOrWhiteSpace(newPath)) return false;
            if (!FileExist(filePath)) return false;
            if (FileExist(newPath)) return false;
            if (!CreateDirectories(newPath)) return false;
            try
            {
                System.IO.File.Copy(filePath, newPath);
                return true;
            }
            catch(Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }
        }
        
    }
}
