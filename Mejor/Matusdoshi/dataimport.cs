﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Matsudoshi

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName = frm.strFileBaseData;
            string strFileName_jyuseikyufu = frm.strFileKyufuData;
            string strFileName_ahk = frm.strFileAHKData;

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName != string.Empty)
                {
                    wf.LogPrint("データインポート");
                    if (!dataImport_jyusei.dataImport_main(_cym, wf, strFileName))
                    {
                        wf.LogPrint("データインポート異常終了");
                        System.Windows.Forms.MessageBox.Show("データインポート異常終了","",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    
                    wf.LogPrint("データインポート終了");
                }
                else
                {
                    wf.LogPrint("データのファイルが指定されていないため処理しない");
                }

                //if (strFileName_jyuseikyufu!=string.Empty)
                //{
                //    wf.LogPrint("給付データインポート");
                //    if (!dataImport_JyuseiKyufu.dataImport_JyuseiKyufu_main(_cym, wf, strFileName_jyuseikyufu))
                //    {
                //        wf.LogPrint("給付データインポート異常終了");
                //        return;
                //    }

                //    wf.LogPrint("給付データインポート終了");
                //}
                //else
                //{
                //    wf.LogPrint("給付データのファイルが指定されていないため処理しない");
                //}

                if (strFileName_ahk != string.Empty)
                {
                    wf.LogPrint("あはき提供データインポート");
                    if (!dataImport_AHK.dataImport_AHK_main(_cym, wf, strFileName_ahk))
                    {
                        wf.LogPrint("あはき提供データインポート終了");
                        return;
                    }


                    wf.LogPrint("あはき提供データインポート終了");
                }
                else
                {
                    wf.LogPrint("あはき提供データのファイルが指定されていないため処理しない");
                }

                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    /// <summary>
    /// 柔整データインポート
    /// </summary>
    partial class dataImport_jyusei
    {

        #region テーブル構造
        
        public int cym { get;set; }=0;                                         //メホール請求年月管理用;

        public string f001insnum { get; set; } = string.Empty;                 //保険者番号;

        [DB.DbAttribute.PrimaryKey]
        public string f002shinsaym { get; set; } = string.Empty;               //審査年月;
        public string f003comnum { get; set; } = string.Empty;                 //レセプト全国共通キー;

        public string f004rezenum { get; set; } = string.Empty;                //国保連レセプト番号;
        public string f005sejutsuym { get; set; } = string.Empty;              //施術年月;
        public string f006 { get; set; } = string.Empty;                       //保険種別1;
        public string f007 { get; set; } = string.Empty;                       //保険種別2;
        public string f008family { get; set; } = string.Empty;                 //本人家族入外区分;
        public string f009hihomark { get; set; } = string.Empty;               //被保険者証記号;
        public string f010hihonum { get; set; } = string.Empty;                //証番号;
        public string f011pgender { get; set; } = string.Empty;                //性別;
        public string f012pbirthday { get; set; } = string.Empty;              //生年月日;
        public string f013ratio { get; set; } = string.Empty;                  //給付割合;
        public string f014 { get; set; } = string.Empty;                       //特記事項1;
        public string f015 { get; set; } = string.Empty;                       //特記事項2;
        public string f016 { get; set; } = string.Empty;                       //特記事項3;
        public string f017 { get; set; } = string.Empty;                       //特記事項4;
        public string f018 { get; set; } = string.Empty;                       //特記事項5;
        public string f019 { get; set; } = string.Empty;                       //算定区分1;
        public string f020 { get; set; } = string.Empty;                       //算定区分2;
        public string f021 { get; set; } = string.Empty;                       //算定区分3;
        public string f022clinicnum { get; set; } = string.Empty;              //施術所コード;
        public string f023 { get; set; } = string.Empty;                       //柔整団体コード;
        public string f024 { get; set; } = string.Empty;                       //回数;
        public string f025total { get; set; } = string.Empty;                  //決定金額;
        public string f026partial { get; set; } = string.Empty;                //決定一部負担金;
        public string f027 { get; set; } = string.Empty;                       //費用額;
        public string f028charge { get; set; } = string.Empty;                 //保険者負担額;
        public string f029 { get; set; } = string.Empty;                       //高額療養費;
        public string f030 { get; set; } = string.Empty;                       //患者負担額;
        public string f031 { get; set; } = string.Empty;                       //国保優先公費;
        public string f032firstdate1 { get; set; } = string.Empty;                                         //初検年月日;
        public string f033 { get; set; } = string.Empty;                                         //負傷名数;
        public string f034 { get; set; } = string.Empty;                                         //転帰;
        public string f035 { get; set; } = string.Empty;                                         //転帰レコード区分;
        public string f036 { get; set; } = string.Empty;                                         //転帰グループ番号;
        public string f037 { get; set; } = string.Empty;                                         //整形審査年月_1;
        public string f038 { get; set; } = string.Empty;                                         //整形レセプト全国共通キー_1;
        public string f039 { get; set; } = string.Empty;                                         //整形医療機関コード_1;
        public string f040 { get; set; } = string.Empty;                                         //整形医療機関名_1;
        public string f041 { get; set; } = string.Empty;                                         //整形診療開始日1_1;
        public string f042 { get; set; } = string.Empty;                                         //整形診療開始日2_1;
        public string f043 { get; set; } = string.Empty;                                         //整形診療開始日3_1;
        public string f044 { get; set; } = string.Empty;                                         //整形疾病コード1_1;
        public string f045 { get; set; } = string.Empty;                                         //整形疾病コード2_1;
        public string f046 { get; set; } = string.Empty;                                         //整形疾病コード3_1;
        public string f047 { get; set; } = string.Empty;                                         //整形疾病コード4_1;
        public string f048 { get; set; } = string.Empty;                                         //整形疾病コード5_1;
        public string f049 { get; set; } = string.Empty;                                         //整形診療日数_1;
        public string f050 { get; set; } = string.Empty;                                         //整形費用額_1;
        public string f051 { get; set; } = string.Empty;                                         //整形審査年月_2;
        public string f052 { get; set; } = string.Empty;                                         //整形レセプト全国共通キー_2;
        public string f053 { get; set; } = string.Empty;                                         //整形医療機関コード_2;
        public string f054 { get; set; } = string.Empty;                                         //整形医療機関名_2;
        public string f055 { get; set; } = string.Empty;                                         //整形診療開始日1_2;
        public string f056 { get; set; } = string.Empty;                                         //整形診療開始日2_2;
        public string f057 { get; set; } = string.Empty;                                         //整形診療開始日3_2;
        public string f058 { get; set; } = string.Empty;                                         //整形疾病コード1_2;
        public string f059 { get; set; } = string.Empty;                                         //整形疾病コード2_2;
        public string f060 { get; set; } = string.Empty;                                         //整形疾病コード3_2;
        public string f061 { get; set; } = string.Empty;                                         //整形疾病コード4_2;
        public string f062 { get; set; } = string.Empty;                                         //整形疾病コード5_2;
        public string f063 { get; set; } = string.Empty;                                         //整形診診療日数_2;
        public string f064 { get; set; } = string.Empty;                                         //整形費用額_2;
        public string f065 { get; set; } = string.Empty;                                         //整形審査年月_3;
        public string f066 { get; set; } = string.Empty;                                         //整形レセプト全国共通キー_3;
        public string f067 { get; set; } = string.Empty;                                         //整形医療機関コード_3;
        public string f068 { get; set; } = string.Empty;                                         //整形医療機関名_3;
        public string f069 { get; set; } = string.Empty;                                         //整形診療開始日1_3;
        public string f070 { get; set; } = string.Empty;                                         //整形診療開始日2_3;
        public string f071 { get; set; } = string.Empty;                                         //整形診療開始日3_3;
        public string f072 { get; set; } = string.Empty;                                         //整形疾病コード1_3;
        public string f073 { get; set; } = string.Empty;                                         //整形疾病コード2_3;
        public string f074 { get; set; } = string.Empty;                                         //整形疾病コード3_3;
        public string f075 { get; set; } = string.Empty;                                         //整形疾病コード4_3;
        public string f076 { get; set; } = string.Empty;                                         //整形疾病コード5_3;
        public string f077 { get; set; } = string.Empty;                                         //整形診療日数_3;
        public string f078 { get; set; } = string.Empty;                                         //整形費用額_3;
        public string f079 { get; set; } = string.Empty;                                         //宛名番号;



        //Mejor用項目
        public int f100shinsaYMAD { get; set; } = 0;                           //審査年月西暦;
        public int f101sejutsuYMAD { get; set; } = 0;                          //施術年月西暦;
        public DateTime f102birthAD { get; set; } = DateTime.MinValue;         //生年月日西暦;
        public DateTime f103firstdateAD { get; set; } = DateTime.MinValue;                        //初検年月日西暦;
        public int f104seikeishisnaYMAD1 { get; set; } = 0;                    //整形審査年月_1西暦;
        public int f105seikeishisnaYMAD2 { get; set; } = 0;                    //整形審査年月_2西暦;
        public int f106seikeishisnaYMAD3 { get; set; } = 0;                    //整形審査年月_3西暦;
        public string f107hihomark_narrow { get; set; } = string.Empty;        //被保険者証記号半角;
        public string f108hihonum_narrow { get; set; } = string.Empty;         //証番号半角;


        #endregion


        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        static int intcym = 0;

        List<dataImport_jyusei> lstImp = new List<dataImport_jyusei>();

        /// <summary>
        /// データインポート
        /// </summary>
        /// <returns></returns>
        public static bool dataImport_main(int _cym,WaitForm wf,string strFileName)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");
                
                //npoiで開きデータ取りだし
                //dataimport_baseに登録
                if(!EntryCSVData(strFileName, wf)) return false;

                
                wf.LogPrint("Appテーブル作成");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
                if(!UpdateApp(intcym)) return false;

                //20211101163045 furukawa st ////////////////////////
                //auxにレセプト全国共通キー、国保連レセプト番号を入れておく
                
                wf.LogPrint("AUXテーブル更新");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
                if (!UpdateAUX(intcym)) return false;
                //20211101163045 furukawa ed ////////////////////////


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
           

        }



        //20211101163125 furukawa st ////////////////////////
        //auxにレセプト全国共通キー、国保連レセプト番号を入れておく
        
        /// <summary>
        /// AUXにレセプト全国共通キー、国保連レセプト番号更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateAUX(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" update application_aux set    ");            
            sb.AppendLine("  matchingid02			       = b.f003comnum");
            sb.AppendLine(" ,matchingid02date			       = now() ");

            sb.AppendLine(" ,matchingid03			       = b.f004rezenum");
            sb.AppendLine(" ,matchingid03date			       = now() ");

            sb.AppendLine(" from dataimport_jyusei b,application a ");
            sb.AppendLine(" where ");
            sb.AppendLine($"replace(a.emptytext3,'.tif','')=b.f004rezenum and a.cym={cym}");
            sb.AppendLine(" and a.aid=application_aux.aid ");

            DB.Command cmd = new DB.Command(DB.Main, sb.ToString(), true);

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }
        //20211101163125 furukawa ed ////////////////////////





        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" update application set    ");
            sb.AppendLine(" inum				   = b.f001insnum");
            sb.AppendLine(" ,comnum			       = b.f003comnum");
            sb.AppendLine(" ,afamily			   = case b.f008family when '2' then 2 else 6 end ");
            sb.AppendLine(" ,hnum				   = b.f009hihomark || '-' || b.f010hihonum");
            sb.AppendLine(" ,psex				   = cast(b.f011pgender as int) ");
            sb.AppendLine(" ,aratio			       = cast(b.f013ratio as int) ");
            sb.AppendLine(" ,sid				   = b.f022clinicnum");
            sb.AppendLine(" ,acounteddays		   = cast((case when b.f024        ='' then '0' else b.f024        end ) as int) ");
            sb.AppendLine(" ,atotal			       = cast((case when b.f025total   ='' then '0' else b.f025total   end ) as int) ");
            sb.AppendLine(" ,acharge			   = cast((case when b.f028charge  ='' then '0' else b.f028charge  end ) as int)");
            sb.AppendLine(" ,apartial			   = cast((case when b.f030 ='' then '0' else b.f030 end ) as int)");
            sb.AppendLine(" ,taggeddatas	              ");
            sb.AppendLine("        = 'count:\"' || case when b.f033 ='' then '0' else b.f033 end  || '\"'  ");

            //20210525150521 furukawa st ////////////////////////
            //汎用文字列5に国保連レセプト番号            
            sb.AppendLine("        '|GeneralString5:\"' || b.f004rezenum  || '\"'  ");
            //20210525150521 furukawa ed ////////////////////////


            sb.AppendLine(" ,ym				        = cast(b.f101sejutsuYMAD as int) ");

            //入力画面がないため、app更新時にymをayear,amonthから取得するので、ここで入れないとym=0となり、診療年月が0になってしまう   
            //0になると、入力画面以外でapp更新時にymを更新する際、ayear,amonthから取得しているので診療年月が消えてしまう
            sb.AppendLine(",ayear               = cast(substr(b.f005sejutsuym,2,2) as int)");       //施術年和暦
            sb.AppendLine(",amonth              = cast(substr(b.f005sejutsuym,4,2) as int)");       //施術月和暦

            sb.AppendLine(" ,pbirthday				= cast(b.f102birthad as date) ");
            sb.AppendLine(" ,ifirstdate1			= cast(b.f103firstdatead as date)  ");
            sb.AppendLine(" ,fchargetype=  ");
            sb.AppendLine(" 	case when cast(b.f101sejutsuYMAD as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end ");
            sb.AppendLine(" ,aapptype=6 ");
            sb.AppendLine(" ,numbering                 = f079 ");
            sb.AppendLine(" from dataimport_jyusei b ");
            sb.AppendLine(" where ");
            sb.AppendLine($"replace(application.emptytext3,'.tif','')=b.f004rezenum and application.cym={cym}");


            //string strsql =
            //    "update application set    " +
            //    "inum				          = b.f001insnum" +                                         //保険者番号
            //    ",comnum			          = b.f003comnum" +                                         //レセプト全国共通キー
            //    ",afamily			          = case b.f008family when '2' then 2 else 6 end " +        //本人家族入外区分                 
            //    ",hnum				          = b.f009hihomark || '-' || b.f010hihonum" +               //被保険者証記号番号
            //                                                                                            //",hnum				          = b.hihomark || b.hihonum" +                          //被保険者証記号番号
            //    ",psex				          = cast(b.f011pgender as int) " +                          //性別
            //    ",aratio			          = cast(b.f013ratio as int) " +                            //給付割合
            //    ",sid				          = b.f022clinicnum" +                                      //施術所コード
            //    ",acounteddays		          = cast((case when b.f024        ='' then '0' else b.f024        end ) as int) " +         //回数（実日数）
            //    ",atotal			          = cast((case when b.f025total   ='' then '0' else b.f025total   end ) as int) " +         //決定金額
            //    ",acharge			          = cast((case when b.f028charge  ='' then '0' else b.f028charge  end ) as int)" +          //保険者負担額
            //    ",apartial			          = cast((case when b.f030 ='' then '0' else b.f030 end ) as int)" +          //患者負担額
            //    ",taggeddatas	              " +
            //    "       = 'count:\"' || case when b.f033 ='' then '0' else b.f033 end  || '\"'  " +//負傷数
            //                                                                                       //"       = taggeddatas || 'count:\"' || b.fushocount || '\"|' || 'GeneralString1:\"' || b.hihokana || '\"' " +//負傷数、被保険者名カナ
            //    //",hzip				          = b.f081zip" +                                     //被保険者郵便番号
            //    //",haddress			          = b.f082address" +                                 //被保険者住所
            //    //",hname				          = b.f083namekanji" +                               //被保険者名
            //    ",ym				            = cast(b.f101sejutsuYMAD as int) " +             //診療年月
            //    ",pbirthday				        = cast(b.f102birthad as date) " +                //受療者生年月日
            //    ",ifirstdate1				    = cast(b.f103firstdatead as date)  " +           //初検日1
            //    ",fchargetype=  " +
            //    "	case when cast(b.f101sejutsuYMAD as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end " +//新規継続
            //    ",aapptype=6 " + //申請書種類は6柔整だろう
            //    ",numbering                 = f079 "+ //宛名番号一応入れとく
            //    "from dataimport_jyusei b " +
            //    "where " +
            //    $"replace(application.emptytext3,'.tif','')=b.f004rezenum and application.cym={cym}";


            
            DB.Command cmd = new DB.Command(DB.Main,sb.ToString(),true);
            //DB.Command cmd = new DB.Command(DB.Main, strsql, true);


            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <returns></returns>
        private static bool EntryCSVData(string strFileName, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from dataimport_jyusei where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {


                    try
                    {
                        int.Parse(tmp[11].ToString().Replace(",", ""));
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataImport_jyusei cls = new dataImport_jyusei();

                    cls.f001insnum = tmp[0];            //保険者番号
                    cls.f002shinsaym = tmp[1];          //審査年月
                    cls.f003comnum = tmp[2];            //レセプト全国共通キー
                    cls.f004rezenum = tmp[3];           //国保連レセプト番号
                    cls.f005sejutsuym = tmp[4];         //施術年月
                    cls.f006 = tmp[5];                  //保険種別1
                    cls.f007 = tmp[6];                  //保険種別2
                    cls.f008family = tmp[7];            //本人家族入外区分
                    cls.f009hihomark = tmp[8];          //被保険者証記号
                    cls.f010hihonum = tmp[9];           //被保険者証番号
                    cls.f011pgender = tmp[10];          //性別
                    cls.f012pbirthday = tmp[11];        //生年月日
                    cls.f013ratio = tmp[12];            //給付割合
                    cls.f014 = tmp[13];                 //特記事項1
                    cls.f015 = tmp[14];                 //特記事項2
                    cls.f016 = tmp[15];                 //特記事項3
                    cls.f017 = tmp[16];                 //特記事項4
                    cls.f018 = tmp[17];                 //特記事項5
                    cls.f019 = tmp[18];                 //算定区分1
                    cls.f020 = tmp[19];                 //算定区分2
                    cls.f021 = tmp[20];                 //算定区分3
                    cls.f022clinicnum = tmp[21];        //施術所コード
                    cls.f023 = tmp[22];                 //柔整団体コード
                    cls.f024 = tmp[23];                 //回数
                    cls.f025total = tmp[24];            //決定金額
                    cls.f026partial = tmp[25];          //決定一部負担金
                    cls.f027 = tmp[26];                 //費用額
                    cls.f028charge = tmp[27];           //保険者負担額
                    cls.f029 = tmp[28];                 //高額療養費
                    cls.f030 = tmp[29];                 //患者負担額
                    cls.f031 = tmp[30];                 //国保優先公費
                    cls.f032firstdate1 = tmp[31];       //初検年月日

                    cls.f033 = tmp[32];                 //負傷名数
                    cls.f034 = tmp[33];                 //転帰
                    cls.f035 = tmp[34];                 //転帰レコード区分
                    cls.f036 = tmp[35];                 //転帰グループ番号
                    cls.f037 = tmp[36];                 //整形審査年月(1)
                    cls.f038 = tmp[37];                 //整形レセプト全国共通キー(1)
                    cls.f049 = tmp[38];                 //整形医療機関コード(1)
                    cls.f040 = tmp[39];                 //整形医療機関名(1)
                    cls.f041 = tmp[40];                 //整形診療開始日1(1)
                    cls.f042 = tmp[41];                 //整形診療開始日2(1)
                    cls.f043 = tmp[42];                 //整形診療開始日3(1)
                    cls.f044 = tmp[43];                 //整形疾病コード1(1)
                    cls.f045 = tmp[44];                 //整形疾病コード2(1)
                    cls.f046 = tmp[45];                 //整形疾病コード3(1)
                    cls.f047 = tmp[46];                 //整形疾病コード4(1)
                    cls.f048 = tmp[47];                 //整形疾病コード5(1)
                    cls.f059 = tmp[48];                 //整形診療日数(1)
                    cls.f050 = tmp[49];                 //整形費用額(1)
                    cls.f051 = tmp[50];                 //整形審査年月(2)
                    cls.f052 = tmp[51];                 //整形レセプト全国共通キー(2)
                    cls.f053 = tmp[52];                 //整形医療機関コード(2)
                    cls.f054 = tmp[53];                 //整形医療機関名(2)
                    cls.f055 = tmp[54];                 //整形診療開始日1(2)
                    cls.f056 = tmp[55];                 //整形診療開始日2(2)
                    cls.f057 = tmp[56];                 //整形診療開始日3(2)
                    cls.f058 = tmp[57];                 //整形疾病コード1(2)
                    cls.f069 = tmp[58];                 //整形疾病コード2(2)
                    cls.f060 = tmp[59];                 //整形疾病コード3(2)
                    cls.f061 = tmp[60];                 //整形疾病コード4(2)
                    cls.f062 = tmp[61];                 //整形疾病コード5(2)
                    cls.f063 = tmp[62];                 //整形診診療日数(2)
                    cls.f064 = tmp[63];                 //整形費用額(2)
                    cls.f065 = tmp[64];                 //整形審査年月(3)
                    cls.f066 = tmp[65];                 //整形レセプト全国共通キー(3)
                    cls.f067 = tmp[66];                 //整形医療機関コード(3)
                    cls.f068 = tmp[67];                 //整形医療機関名(3)
                    cls.f079 = tmp[68];                 //整形診療開始日1(3)
                    cls.f070 = tmp[69];                 //整形診療開始日2(3)
                    cls.f071 = tmp[70];                 //整形診療開始日3(3)
                    cls.f072 = tmp[71];                 //整形疾病コード1(3)
                    cls.f073 = tmp[72];                 //整形疾病コード2(3)
                    cls.f074 = tmp[73];                 //整形疾病コード3(3)
                    cls.f075 = tmp[74];                 //整形疾病コード4(3)
                    cls.f076 = tmp[75];                 //整形疾病コード5(3)
                    cls.f077 = tmp[76];                 //整形診療日数(3)
                    cls.f078 = tmp[77];                 //整形費用額(3)
                    cls.f079 = tmp[78];                 //宛名番号

                    //管理項目
                    string strErr = string.Empty;
                    cls.cym = intcym;//メホール請求年月管理用;

                    //審査年月西暦;
                    if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f002shinsaym)) == 0) strErr += "審査年月 変換に失敗しました";
                    cls.f100shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f002shinsaym));
                    cls.f101sejutsuYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f005sejutsuym));         //施術年月西暦;                            
                    cls.f102birthAD = DateTimeEx.GetDateFromJstr7(cls.f012pbirthday);                               //生年月日西暦;                            
                    cls.f103firstdateAD = DateTimeEx.GetDateFromJstr7(cls.f032firstdate1);//初検年月日西暦

                    if (!int.TryParse(cls.f037 == string.Empty ? "0" : cls.f037, out int tmpf037)) strErr += "\r\n 整形審査年月_1西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf037);                          //整形審査年月_1西暦;

                    if (!int.TryParse(cls.f051 == string.Empty ? "0" : cls.f051, out int tmpf051)) strErr += "\r\n 整形審査年月_2西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf051);                          //整形審査年月_2西暦;

                    if (!int.TryParse(cls.f065 == string.Empty ? "0" : cls.f065, out int tmpf065)) strErr += "\r\n 整形審査年月_3西暦 変換に失敗しました";
                    cls.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf065);                          //整形審査年月_3西暦;

                    //被保険者証記号半角;                                                                                                
                    //記号は漢字の可能性が                                                                          
                    try
                    {
                        cls.f107hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f009hihomark, Microsoft.VisualBasic.VbStrConv.Narrow);
                    }
                    catch
                    {
                        cls.f107hihomark_narrow = cls.f009hihomark;
                    }

                    cls.f108hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;

                    if (strErr != string.Empty)
                    {
                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{strErr}");
                        tran.Rollback();
                        return false;
                    }

                    //20210525153827 furukawa st ////////////////////////
                    //トランザクションにしないと遅すぎ
                    
                    if (!DB.Main.Insert<dataImport_jyusei>(cls, tran)) return false;
                    //if (!DB.Main.Insert<dataImport_jyusei>(cls)) return false;
                    //20210525153827 furukawa ed ////////////////////////


                    wf.LogPrint($"レセプトデータ  レセプト全国共通キー:{cls.f003comnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");



                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
            }

        }


        ///// <summary>
        ///// 提供データExcelを開いてテーブル登録
        ///// </summary>
        ///// <param name="strFileName">ファイル名</param>
        ///// <param name="wf"></param>
        ///// <returns></returns>
        //private static bool EntryExcelData(string strFileName ,WaitForm wf)
        //{
        //    wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
        //    NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

        //    DB.Transaction tran = new DB.Transaction(DB.Main);

        //    DB.Command cmd=new DB.Command($"delete from dataimport_base where cym={intcym}",tran);
        //    cmd.TryExecuteNonQuery();

        //    wf.LogPrint($"{intcym}削除");

        //    int c = 0;//インポートファイルの列
        //    int r = 1;//インポートファイルの行
        //    try
        //    {

        //        wf.InvokeValue = 0;
        //        wf.SetMax(ws.LastRowNum);
                
        //        //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
        //        int cellcnt = ws.GetRow(0).Cells.Count;
                
        //        for (r = 1; r <= ws.LastRowNum; r++)
        //        {
        //            dataImport_jyusei imp = new dataImport_jyusei();

        //            NPOI.SS.UserModel.IRow row = ws.GetRow(r);

        //            imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

        //            for ( c = 0; c < cellcnt; c++)
        //            {
        //                if(CommonTool.WaitFormCancelProcess(wf)) return false;

        //                NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        
        //                if (c == 0)   imp.f001insnum = getCellValueToString(cell);                //保険者番号;
        //                if (c == 1)   imp.f002shinsaym = getCellValueToString(cell);              //審査年月;
        //                if (c == 2)   imp.f003comnum = getCellValueToString(cell);                //レセプト全国共通キー;
        //                if (c == 3)   imp.f004rezenum = getCellValueToString(cell);               //国保連レセプト番号;
        //                if (c == 4)   imp.f005sejutsuym = getCellValueToString(cell);             //施術年月;
        //                if (c == 5)   imp.f006 = getCellValueToString(cell);                      //保険種別1;
        //                if (c == 6)   imp.f007 = getCellValueToString(cell);                      //保険種別2;
        //                if (c == 7)   imp.f008family = getCellValueToString(cell);                //本人家族入外区分;
        //                if (c == 8)   imp.f009hihomark = getCellValueToString(cell);              //被保険者証記号;
        //                if (c == 9)    imp.f010hihonum = getCellValueToString(cell);              //証番号;
        //                if (c == 10)   imp.f011pgender = getCellValueToString(cell);              //性別;
        //                if (c == 11)   imp.f012pbirthday = getCellValueToString(cell);            //生年月日;
        //                if (c == 12)   imp.f013ratio = getCellValueToString(cell);                //給付割合;
        //                if (c == 13)   imp.f014 = getCellValueToString(cell);                     //特記事項1;
        //                if (c == 14)   imp.f015 = getCellValueToString(cell);                     //特記事項2;
        //                if (c == 15)   imp.f016 = getCellValueToString(cell);                     //特記事項3;
        //                if (c == 16)   imp.f017 = getCellValueToString(cell);                     //特記事項4;
        //                if (c == 17)   imp.f018 = getCellValueToString(cell);                     //特記事項5;
        //                if (c == 18)   imp.f019 = getCellValueToString(cell);                     //算定区分1;
        //                if (c == 19)   imp.f020 = getCellValueToString(cell);                     //算定区分2;
        //                if (c == 20)   imp.f021 = getCellValueToString(cell);                     //算定区分3;
        //                if (c == 21)   imp.f022clinicnum = getCellValueToString(cell);            //施術所コード;
        //                if (c == 22)   imp.f023 = getCellValueToString(cell);                     //柔整団体コード;
        //                if (c == 23)   imp.f024 = getCellValueToString(cell);                     //回数;
        //                if (c == 24)   imp.f025total = getCellValueToString(cell);                //決定金額;
        //                if (c == 25)   imp.f026partial = getCellValueToString(cell);              //決定一部負担金;
        //                if (c == 26)   imp.f027 = getCellValueToString(cell);                     //費用額;
        //                if (c == 27)   imp.f028charge = getCellValueToString(cell);               //保険者負担額;
        //                if (c == 28)   imp.f029 = getCellValueToString(cell);                     //高額療養費;
        //                if (c == 29)   imp.f030 = getCellValueToString(cell);                     //患者負担額;
        //                if (c == 30)   imp.f031 = getCellValueToString(cell);                     //国保優先公費;
        //                if (c == 31)   imp.f033firstdate1 = getCellValueToString(cell);           //初検年月日;
        //                if (c == 32)   imp.f034 = getCellValueToString(cell);                     //負傷名数;
        //                if (c == 33)   imp.f035 = getCellValueToString(cell);                     //転帰;
        //                if (c == 34)   imp.f036 = getCellValueToString(cell);                     //転帰レコード区分;
        //                if (c == 35)   imp.f037 = getCellValueToString(cell);                     //転帰グループ番号;
        //                if (c == 36)   imp.f038 = getCellValueToString(cell);                     //整形審査年月_1;
        //                if (c == 37)   imp.f039 = getCellValueToString(cell);                     //整形レセプト全国共通キー_1;
        //                if (c == 38)   imp.f040 = getCellValueToString(cell);                     //整形医療機関コード_1;
        //                if (c == 39)   imp.f041 = getCellValueToString(cell);                     //整形医療機関名_1;
        //                if (c == 40)   imp.f042 = getCellValueToString(cell);                     //整形診療開始日1_1;
        //                if (c == 41)   imp.f043 = getCellValueToString(cell);                     //整形診療開始日2_1;
        //                if (c == 42)   imp.f044 = getCellValueToString(cell);                     //整形診療開始日3_1;
        //                if (c == 43)   imp.f045 = getCellValueToString(cell);                     //整形疾病コード1_1;
        //                if (c == 44)   imp.f046 = getCellValueToString(cell);                     //整形疾病コード2_1;
        //                if (c == 45)   imp.f047 = getCellValueToString(cell);                     //整形疾病コード3_1;
        //                if (c == 46)   imp.f048 = getCellValueToString(cell);                     //整形疾病コード4_1;
        //                if (c == 47)   imp.f049 = getCellValueToString(cell);                     //整形疾病コード5_1;
        //                if (c == 48)   imp.f050 = getCellValueToString(cell);                     //整形診療日数_1;
        //                if (c == 49)   imp.f051 = getCellValueToString(cell);                     //整形費用額_1;
        //                if (c == 50)   imp.f052 = getCellValueToString(cell);                     //整形審査年月_2;
        //                if (c == 51)   imp.f053 = getCellValueToString(cell);                     //整形レセプト全国共通キー_2;
        //                if (c == 52)   imp.f054 = getCellValueToString(cell);                     //整形医療機関コード_2;
        //                if (c == 53)   imp.f055 = getCellValueToString(cell);                     //整形医療機関名_2;
        //                if (c == 54)   imp.f056 = getCellValueToString(cell);                     //整形診療開始日1_2;
        //                if (c == 55)   imp.f057 = getCellValueToString(cell);                     //整形診療開始日2_2;
        //                if (c == 56)   imp.f058 = getCellValueToString(cell);                     //整形診療開始日3_2;
        //                if (c == 57)   imp.f059 = getCellValueToString(cell);                     //整形疾病コード1_2;
        //                if (c == 58)   imp.f060 = getCellValueToString(cell);                     //整形疾病コード2_2;
        //                if (c == 59)   imp.f061 = getCellValueToString(cell);                     //整形疾病コード3_2;
        //                if (c == 60)   imp.f062 = getCellValueToString(cell);                     //整形疾病コード4_2;
        //                if (c == 61)   imp.f063 = getCellValueToString(cell);                     //整形疾病コード5_2;
        //                if (c == 62)   imp.f064 = getCellValueToString(cell);                     //整形診診療日数_2;
        //                if (c == 63)   imp.f065 = getCellValueToString(cell);                     //整形費用額_2;
        //                if (c == 64)   imp.f066 = getCellValueToString(cell);                     //整形審査年月_3;
        //                if (c == 65)   imp.f067 = getCellValueToString(cell);                     //整形レセプト全国共通キー_3;
        //                if (c == 66)   imp.f068 = getCellValueToString(cell);                     //整形医療機関コード_3;
        //                if (c == 67)   imp.f069 = getCellValueToString(cell);                     //整形医療機関名_3;
        //                if (c == 68)   imp.f070 = getCellValueToString(cell);                     //整形診療開始日1_3;
        //                if (c == 69)   imp.f071 = getCellValueToString(cell);                     //整形診療開始日2_3;
        //                if (c == 70)   imp.f072 = getCellValueToString(cell);                     //整形診療開始日3_3;
        //                if (c == 71)   imp.f073 = getCellValueToString(cell);                     //整形疾病コード1_3;
        //                if (c == 72)   imp.f074 = getCellValueToString(cell);                     //整形疾病コード2_3;
        //                if (c == 73)   imp.f075 = getCellValueToString(cell);                     //整形疾病コード3_3;
        //                if (c == 74)   imp.f076 = getCellValueToString(cell);                     //整形疾病コード4_3;
        //                if (c == 75)   imp.f077 = getCellValueToString(cell);                     //整形疾病コード5_3;
        //                if (c == 76)   imp.f078 = getCellValueToString(cell);                     //整形診療日数_3;
        //                if (c == 77)   imp.f079 = getCellValueToString(cell);                     //整形費用額_3;
        //                if (c == 78)   imp.f080 = getCellValueToString(cell);                     //宛名番号;
                        
                        
        //                //管理項目は最終列で処理
        //                if (c == 81)
        //                {
        //                    string strErr = string.Empty;
                            
        //                    imp.cym = intcym;                                           //メホール請求年月管理用;

        //                    //審査年月西暦;
        //                    if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym)) == 0) strErr += "審査年月 変換に失敗しました";
                            
        //                    imp.f100shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym));

        //                    imp.f101sejutsuYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f005sejutsuym));         //施術年月西暦;                            
        //                    imp.f102birthAD = DateTimeEx.GetDateFromJstr7(imp.f012pbirthday);                               //生年月日西暦;                            
        //                    imp.f103firstdateAD = DateTimeEx.GetDateFromJstr7(imp.f033firstdate1);//初検年月日西暦

        //                    if (!int.TryParse(imp.f038==string.Empty ? "0": imp.f038, out int tmpf038)) strErr += "\r\n 整形審査年月_1西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf038);                          //整形審査年月_1西暦;


        //                    if (!int.TryParse(imp.f052 == string.Empty ? "0" : imp.f052, out int tmpf052)) strErr += "\r\n 整形審査年月_2西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf052);                          //整形審査年月_2西暦;


        //                    if (!int.TryParse(imp.f066 == string.Empty ? "0" : imp.f066, out int tmpf066)) strErr += "\r\n 整形審査年月_3西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf066);                          //整形審査年月_3西暦;



        //                    //被保険者証記号半角;                                                                                                
        //                    //記号は漢字の可能性が                                                                          
        //                    try
        //                    {
        //                        imp.f107hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);
        //                    }
        //                    catch
        //                    {
        //                        imp.f107hihomark_narrow = imp.f010hihonum;
        //                    }

        //                    imp.f108hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;



        //                    if (strErr != string.Empty)
        //                    {
        //                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目:{strErr}");
        //                        tran.Rollback();
        //                        return false;
        //                    }
        //                }

        //            }

        //            wf.InvokeValue++;
        //            wf.LogPrint($"レセプト全国共通キー:{imp.f003comnum}");

        //            if (!DB.Main.Insert<dataImport_jyusei>(imp, tran)) return false;
        //        }

        //        return true;
        //    }         
        //    catch(Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目{ex.Message}");
        //        tran.Rollback();
        //        return false;
        //    }           
        //    finally
        //    {
        //        tran.Commit();
        //        wb.Close();
        //    }
            
        //}

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch(cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }
           
            
        }

    }

    /// <summary>
    /// 柔整給付データインポート
    /// </summary>
    partial class dataImport_JyuseiKyufu
    {
        #region テーブル構造
        [DB.DbAttribute.Serial]

        public int importid { get;set; }=0;                            //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                              //メホール上の処理年月 メホール管理用;
        public string comnum { get; set; } = string.Empty;             //レセプト全国共通キー;
        public string clinicnum { get; set; } = string.Empty;          //医療機関番号（施術所コード;
        public string clinicname { get; set; } = string.Empty;         //医療機関名（施術所名;
        #endregion

        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 柔整給付データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_JyuseiKyufu_main(int _cym,WaitForm wf,string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //dataimport_jyuseikyufuに登録
                if (!EntryExcelData(strFileName, wf)) return false;


                wf.LogPrint("Appテーブル作成");
                //レセプト全国共通キーで紐づけてアップデートする
                if(!UpdateApp(intcym))return false;
                

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
         
        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +
                "sid        = k.clinicnum" +     //医療機関コード
                ",sname		= k.clinicname " +     //医療機関名
                "from dataimport_jyuseikyufu k " +
                "where " +
                $"application.comnum=k.comnum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main, strsql, true);

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd = new DB.Command($"delete from dataimport_jyuseikyufu where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            try
            {
                wf.SetMax(ws.LastRowNum);
                //int c = 0;

                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;

                //20200817181434 furukawa st ////////////////////////
                //最終行が拾えてなかった
                
                for (int r = 1; r <= ws.LastRowNum; r++)
                //for (int r = 1; r < ws.LastRowNum; r++)
                //20200817181434 furukawa ed ////////////////////////
                {
                    dataImport_JyuseiKyufu imp = new dataImport_JyuseiKyufu();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

                    for (int c = 0; c < cellcnt; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        
                        if (c == 5) imp.clinicnum = getCellValueToString(cell);    //医療機関番号（施術所番号
                        if (c == 6) imp.clinicname = getCellValueToString(cell);   //医療機関名（施術所
                        if (c == 24) imp.comnum = getCellValueToString(cell);      //レセプト全国共通キー

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"レセプト全国共通キー:{imp.comnum}");

                    if (!DB.Main.Insert<dataImport_JyuseiKyufu>(imp, tran)) return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
                wb.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }




    }



    partial class dataImport_AHK
    {
        #region テーブル構造
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public string f000importid { get; set; } = string.Empty;              //インポートID　管理用;

        public string f001shoriym { get;set; }=string.Empty;                  //処理年月;
        public string f002comnum { get; set; } = string.Empty;                //レセプト全国共通キー;
        public string f003 { get; set; } = string.Empty;                      //転帰請求グループ番号;
        public string f004 { get; set; } = string.Empty;                      //支給申請書受理番号;
        public string f005 { get; set; } = string.Empty;                      //事業区分;
        public string f006sid { get; set; } = string.Empty;                   //医療機関コード;
        public string f007 { get; set; } = string.Empty;                      //医療機関_異動年月日;
        public string f008sname { get; set; } = string.Empty;                 //医療機関名(漢字);
        public string f009 { get; set; } = string.Empty;                      //柔整団体機関コード;
        public string f010 { get; set; } = string.Empty;                      //柔整団体機関_異動年月日;
        public string f011 { get; set; } = string.Empty;                      //柔整団体機関名(漢字);
        public string f012insnum { get; set; } = string.Empty;                //保険者番号;
        public string f013 { get; set; } = string.Empty;                      //調整保険者番号;
        public string f014hihomark { get; set; } = string.Empty;              //被保険者証記号;
        public string f015hihonum { get; set; } = string.Empty;               //被保険者証番号;
        public string f016setainum { get; set; } = string.Empty;              //世帯番号;
        public string f017 { get; set; } = string.Empty;                      //世帯管理番号;
        public string f018hihokana { get; set; } = string.Empty;              //被保険者氏名（カナ）;
        public string f019hihokanji { get; set; } = string.Empty;             //被保険者氏名（漢字）;
        public string f020pgender { get; set; } = string.Empty;               //性別;
        public string f021pnum { get; set; } = string.Empty;                  //個人管理番号;
        public string f022atenanum { get; set; } = string.Empty;              //宛名番号;
        public string f023 { get; set; } = string.Empty;                      //機械整理番号;
        public string f024 { get; set; } = string.Empty;                      //世帯主個人管理番号;
        public string f025 { get; set; } = string.Empty;                      //世帯主宛名番号;
        public string f026 { get; set; } = string.Empty;                      //世帯主被保険者証記号;
        public string f027 { get; set; } = string.Empty;                      //世帯主被保険者証番号;
        public string f028pbirthday { get; set; } = string.Empty;             //生年月日;
        public string f029shinryoym { get; set; } = string.Empty;             //診療年月;
        public string f030 { get; set; } = string.Empty;                      //支給決定年月;
        public string f031startdate1 { get; set; } = string.Empty;            //診療開始日;
        public string f032finishdate1 { get; set; } = string.Empty;           //診療終了日;
        public string f033 { get; set; } = string.Empty;                      //保険_診療実日数;
        public string f034 { get; set; } = string.Empty;                      //申請年月日;
        public string f035 { get; set; } = string.Empty;                      //審査申出年月日;
        public string f036 { get; set; } = string.Empty;                      //審査申出受領年月日;
        public string f037 { get; set; } = string.Empty;                      //支給決定年月日;
        public string f038 { get; set; } = string.Empty;                      //支給年月日;
        public string f039 { get; set; } = string.Empty;                      //支給実績データ作成年月日;
        public string f040 { get; set; } = string.Empty;                      //支給実績データ更新年月日;
        public string f041 { get; set; } = string.Empty;                      //保険制度（保険種別①）;
        public string f042 { get; set; } = string.Empty;                      //療養費データ区分;
        public string f043apptype { get; set; } = string.Empty;               //療養費種別 04:あんま 05:鍼灸(堺市より);
        public string f044 { get; set; } = string.Empty;                      //点数表;
        public string f045ratio { get; set; } = string.Empty;                 //給付割合;
        public string f046family { get; set; } = string.Empty;                //本人家族入外;
        public string f047 { get; set; } = string.Empty;                      //特記事項１;
        public string f048 { get; set; } = string.Empty;                      //特記事項２;
        public string f049 { get; set; } = string.Empty;                      //特記事項３;
        public string f050 { get; set; } = string.Empty;                      //特記事項４;
        public string f051 { get; set; } = string.Empty;                      //特記事項５;
        public string f052 { get; set; } = string.Empty;                      //長期区分;
        public string f053 { get; set; } = string.Empty;                      //算定区分１;
        public string f054 { get; set; } = string.Empty;                      //算定区分２;
        public string f055 { get; set; } = string.Empty;                      //算定区分３;
        public string f056 { get; set; } = string.Empty;                      //所得区分;
        public string f057 { get; set; } = string.Empty;                      //変更前所得区分;
        public string f058 { get; set; } = string.Empty;                      //前期該当区分;
        public string f059 { get; set; } = string.Empty;                      //特別療養費;
        public string f060 { get; set; } = string.Empty;                      //状態区分;
        public string f061 { get; set; } = string.Empty;                      //海外療養費区分;
        public string f062 { get; set; } = string.Empty;                      //初診料有無;
        public string f063 { get; set; } = string.Empty;                      //高額療養費該当区分;
        public string f064 { get; set; } = string.Empty;                      //強制保留区分;
        public string f065 { get; set; } = string.Empty;                      //強制登録区分;
        public string f066 { get; set; } = string.Empty;                      //転帰請求区分;
        public string f067 { get; set; } = string.Empty;                      //申請区分;
        public string f068 { get; set; } = string.Empty;                      //審査結果区分;
        public string f069 { get; set; } = string.Empty;                      //却下区分;
        public string f070 { get; set; } = string.Empty;                      //却下理由;
        public string f071 { get; set; } = string.Empty;                      //通知書印刷済該当区分;
        public string f072 { get; set; } = string.Empty;                      //傷病コード;
        public string f073 { get; set; } = string.Empty;                      //傷病名;
        public string f074 { get; set; } = string.Empty;                      //備考１;
        public string f075 { get; set; } = string.Empty;                      //備考２;
        public string f076 { get; set; } = string.Empty;                      //データ区分;
        public string f077 { get; set; } = string.Empty;                      //食事区分;
        public string f078 { get; set; } = string.Empty;                      //公費区分;
        public string f079 { get; set; } = string.Empty;                      //自支給期間;
        public string f080 { get; set; } = string.Empty;                      //至支給期間;
        public string f081 { get; set; } = string.Empty;                      //過誤再審査コード;
        public string f082 { get; set; } = string.Empty;                      //保険_一部負担金;
        public string f083 { get; set; } = string.Empty;                      //算定_保険_他法優先公費負担額;
        public string f084 { get; set; } = string.Empty;                      //算定_保険_国保優先公費負担額;
        public string f085 { get; set; } = string.Empty;                      //保険_食事回数;
        public string f086 { get; set; } = string.Empty;                      //算定_保険_食事基準額;
        public string f087 { get; set; } = string.Empty;                      //算定_保険_食事患者負担額;
        public string f088 { get; set; } = string.Empty;                      //算定_保険_食事他法優先公費負担額;
        public string f089 { get; set; } = string.Empty;                      //算定_保険_食事国保優先公費負担額;
        public string f090total { get; set; } = string.Empty;                 //費用額;
        public string f091 { get; set; } = string.Empty;                      //支給予定額;
        public string f092charge { get; set; } = string.Empty;                //保険者負担金額;
        public string f093partial { get; set; } = string.Empty;               //患者負担額;
        public string f094 { get; set; } = string.Empty;                      //指定公費額;
        public string f095 { get; set; } = string.Empty;                      //高額療養費;
        public string f096 { get; set; } = string.Empty;                      //振込先口座有無区分;
        public string f097 { get; set; } = string.Empty;                      //振込先区分;
        public string f098 { get; set; } = string.Empty;                      //振込区分;
        public string f099bankcode { get; set; } = string.Empty;              //振込銀行コード;
        public string f100branchcode { get; set; } = string.Empty;            //振込支店コード;
        public string f101 { get; set; } = string.Empty;                      //預金種目;
        public string f102accnumber { get; set; } = string.Empty;             //口座番号;
        public string f103acckana { get; set; } = string.Empty;               //口座名義人（カナ）;
        public string f104acckanji { get; set; } = string.Empty;              //口座名義人（漢字）;
        public string f105 { get; set; } = string.Empty;                      //公１_負担者番号;
        public string f106 { get; set; } = string.Empty;                      //公１_受給者番号;
        public string f107 { get; set; } = string.Empty;                      //公１_診療実日数;
        public string f108 { get; set; } = string.Empty;                      //公１_決定点数;
        public string f109 { get; set; } = string.Empty;                      //公１_負担者負担金額;
        public string f110 { get; set; } = string.Empty;                      //公１_一部負担金;
        public string f111 { get; set; } = string.Empty;                      //公１_患者負担額;
        public string f112 { get; set; } = string.Empty;                      //公２_負担者番号;
        public string f113 { get; set; } = string.Empty;                      //公２_受給者番号;
        public string f114 { get; set; } = string.Empty;                      //公２_診療実日数;
        public string f115 { get; set; } = string.Empty;                      //公２_決定点数;
        public string f116 { get; set; } = string.Empty;                      //公２_負担者負担金額;
        public string f117 { get; set; } = string.Empty;                      //公２_一部負担金;
        public string f118 { get; set; } = string.Empty;                      //公２_患者負担額;
        public string f119 { get; set; } = string.Empty;                      //公３_負担者番号;
        public string f120 { get; set; } = string.Empty;                      //公３_受給者番号;
        public string f121 { get; set; } = string.Empty;                      //公３_診療実日数;
        public string f122 { get; set; } = string.Empty;                      //公３_決定点数;
        public string f123 { get; set; } = string.Empty;                      //公３_負担者負担金額;
        public string f124 { get; set; } = string.Empty;                      //公３_一部負担金;
        public string f125 { get; set; } = string.Empty;                      //公３_患者負担額;
        public string f126 { get; set; } = string.Empty;                      //公４_負担者番号;
        public string f127 { get; set; } = string.Empty;                      //公４_受給者番号;
        public string f128 { get; set; } = string.Empty;                      //公４_診療実日数;
        public string f129 { get; set; } = string.Empty;                      //公４_決定点数;
        public string f130 { get; set; } = string.Empty;                      //公４_負担者負担金額;
        public string f131 { get; set; } = string.Empty;                      //公４_一部負担金;
        public string f132 { get; set; } = string.Empty;                      //公４_患者負担額;
        public string f133 { get; set; } = string.Empty;                      //公１_食事回数;
        public string f134 { get; set; } = string.Empty;                      //公１_食事基準額;
        public string f135 { get; set; } = string.Empty;                      //公１_食事患者負担額;
        public string f136 { get; set; } = string.Empty;                      //公２_食事回数;
        public string f137 { get; set; } = string.Empty;                      //公２_食事基準額;
        public string f138 { get; set; } = string.Empty;                      //公２_食事患者負担額;
        public string f139 { get; set; } = string.Empty;                      //公３_食事回数;
        public string f140 { get; set; } = string.Empty;                      //公３_食事基準額;
        public string f141 { get; set; } = string.Empty;                      //公３_食事患者負担額;
        public string f142 { get; set; } = string.Empty;                      //公４_食事回数;
        public string f143 { get; set; } = string.Empty;                      //公４_食事基準額;
        public string f144 { get; set; } = string.Empty;                      //公４_食事患者負担額;
        public string f145 { get; set; } = string.Empty;                      //再審査区分;
        public string f146 { get; set; } = string.Empty;                      //強制入力区分;
        public string f147 { get; set; } = string.Empty;                      //登録機能ID;
        public string f148 { get; set; } = string.Empty;                      //登録者ID;
        public string f149 { get; set; } = string.Empty;                      //登録日時;
        public string f150 { get; set; } = string.Empty;                      //最終更新機能ID;
        public string f151 { get; set; } = string.Empty;                      //最終更新者ID;
        public string f152 { get; set; } = string.Empty;                      //最終更新日時;
        public string f153 { get; set; } = string.Empty;                      //削除フラグ;
        public int shoriYMAD { get; set; } = 0;                               //処理年月西暦;
        public int shinsaYMAD { get; set; } = 0;                              //審査年月西暦;
        public int shinryoYMAD { get; set; } = 0;                             //診療年月西暦;
        public DateTime pbirthdayAD { get; set; } = DateTime.MinValue;        //生年月日西暦;
        public DateTime firstdatead { get; set; } = DateTime.MinValue;        //初検年月日西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;       //開始日西暦;
        public DateTime finishdate1ad { get; set; } = DateTime.MinValue;      //終了日西暦;
        public string hihomark_narrow { get; set; } = string.Empty;           //被保険者証記号半角;
        public string hihonum_narrow { get; set; } = string.Empty;            //証番号半角;
        public int cym { get; set; } = 0;                                     //メホール請求年月管理用;




        #endregion


        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        
        /// <summary>
        /// 柔整給付データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_main(int _cym, WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //dataimport_AHKに登録
                if(!EntryExcelData(strFileName, wf)) return false;


                
                //wf.LogPrint("Appテーブル作成");
                //レセプト全国共通キーで紐づけてアップデートする→2021/04/30画像はスキャンするので紐づけられない（新宿福田さん）
               // if (!UpdateApp(intcym)) return false;


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine(" update application set ");
            sb.AppendLine("  ayear			= cast(substr(ahk.f029shinryoym,2,2) as int) ");
            sb.AppendLine(" ,amonth			= cast(substr(ahk.f029shinryoym,4,2) as int) ");
            sb.AppendLine(" ,inum			= ahk.f012insnum	");
            sb.AppendLine(" ,hnum			= ahk.hihonum_narrow	");
            sb.AppendLine(" ,hname			= ahk.f019hihokanji	");
            sb.AppendLine(" ,psex			= cast(ahk.f020pgender	as int) ");
            sb.AppendLine(" ,pbirthday		= ahk.pbirthdayad	");
            sb.AppendLine(" ,afamily		= cast(ahk.f046family as int) ");
            sb.AppendLine(" ,aratio			= cast(ahk.f045ratio as int)/10		"); 

            sb.AppendLine(" ,istartdate1	= ahk.startdate1ad ");
            sb.AppendLine(" ,ifinishdate1	= ahk.finishdate1ad ");

            sb.AppendLine(" ,baccname		= ahk.f104acckanji	");
            sb.AppendLine(" ,bkana		    = ahk.f103acckana	");
            sb.AppendLine(" ,baccnumber		= ahk.f102accnumber	");

            sb.AppendLine(" ,atotal			= cast(ahk.f090total as int) ");
            sb.AppendLine(" ,apartial		= cast(ahk.f093partial as int) ");
            sb.AppendLine(" ,acharge		= cast(ahk.f092charge as int) ");

            sb.AppendLine(" ,aapptype		= case ahk.f043apptype ");
            sb.AppendLine($"                     when '04' then {(int)APP_TYPE.あんま} ");
            sb.AppendLine($"                     when '05' then {(int)APP_TYPE.鍼灸} ");
            sb.AppendLine(" end ");

            sb.AppendLine(" ,comnum			= ahk.f002comnum");

            sb.AppendLine(",taggeddatas");
            sb.AppendLine("       = 'GeneralString1:\"' || ahk.f021pnum || '\"' ");
            sb.AppendLine("       || 'GeneralString2:\"' || ahk.f022atenanum || '\"' ");

            sb.AppendLine(" from dataimport_ahk ahk  ");

            sb.AppendLine(" where ");
            sb.AppendLine($" application.comnum=ahk.f002comnum and application.cym={cym}");



            DB.Command cmd = new DB.Command(DB.Main, sb.ToString(),true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        

        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {
            
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from dataimport_ahk where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            int col = 0;

            try
            {
                //20200817180644 furukawa st ////////////////////////
                //最終行を最大値とする
                
                wf.SetMax(ws.LastRowNum);
             
               
                for (int r = 0; r < ws.LastRowNum; r++)
                {
                    dataImport_AHK imp = new dataImport_AHK();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

            
                    for (int c = 0; c < row.LastCellNum; c++)
                    {

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //1列目が数値に変換できない場合は見出し行と見なし飛ばす
                        //20210517144025 furukawa st ////////////////////////
                        //ループを抜けないと行が終わらない                        
                        if (c == 0) if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp)) break;
                        //if (c == 0) if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp)) continue;
                        //20210517144025 furukawa ed ////////////////////////


                        #region 値の取込
                        if (c == 0) imp.f001shoriym = getCellValueToString(cell);
                        if (c == 1) imp.f002comnum = getCellValueToString(cell);
                        if (c == 2) imp.f003 = getCellValueToString(cell);
                        if (c == 3) imp.f004 = getCellValueToString(cell);
                        if (c == 4) imp.f005 = getCellValueToString(cell);
                        if (c == 5) imp.f006sid = getCellValueToString(cell);
                        if (c == 6) imp.f007 = getCellValueToString(cell);
                        if (c == 7) imp.f008sname = getCellValueToString(cell);
                        if (c == 8) imp.f009 = getCellValueToString(cell);
                        if (c == 9) imp.f010 = getCellValueToString(cell);
                        if (c == 10) imp.f011 = getCellValueToString(cell);
                        if (c == 11)  imp.f012insnum = getCellValueToString(cell);
                        if (c == 12)  imp.f013 = getCellValueToString(cell);
                        if (c == 13)  imp.f014hihomark = getCellValueToString(cell);
                        if (c == 14)  imp.f015hihonum = getCellValueToString(cell);
                        if (c == 15)  imp.f016setainum = getCellValueToString(cell);
                        if (c == 16)  imp.f017 = getCellValueToString(cell);
                        if (c == 17)  imp.f018hihokana = getCellValueToString(cell);
                        if (c == 18)  imp.f019hihokanji = getCellValueToString(cell);
                        if (c == 19)  imp.f020pgender = getCellValueToString(cell);
                        if (c == 20)  imp.f021pnum = getCellValueToString(cell);
                        if (c == 21)  imp.f022atenanum = getCellValueToString(cell);
                        if (c == 22)  imp.f023 = getCellValueToString(cell);
                        if (c == 23)  imp.f024 = getCellValueToString(cell);
                        if (c == 24)  imp.f025 = getCellValueToString(cell);
                        if (c == 25)  imp.f026 = getCellValueToString(cell);
                        if (c == 26)  imp.f027 = getCellValueToString(cell);
                        if (c == 27)  imp.f028pbirthday = getCellValueToString(cell);
                        if (c == 28)  imp.f029shinryoym = getCellValueToString(cell);
                        if (c == 29)  imp.f030 = getCellValueToString(cell);
                        if (c == 30)  imp.f031startdate1 = getCellValueToString(cell);
                        if (c == 31)  imp.f032finishdate1 = getCellValueToString(cell);
                        if (c == 32)  imp.f033 = getCellValueToString(cell);
                        if (c == 33)  imp.f034 = getCellValueToString(cell);
                        if (c == 34)  imp.f035 = getCellValueToString(cell);
                        if (c == 35)  imp.f036 = getCellValueToString(cell);
                        if (c == 36)  imp.f037 = getCellValueToString(cell);
                        if (c == 37)  imp.f038 = getCellValueToString(cell);
                        if (c == 38)  imp.f039 = getCellValueToString(cell);
                        if (c == 39)  imp.f040 = getCellValueToString(cell);
                        if (c == 40)  imp.f041 = getCellValueToString(cell);
                        if (c == 41)  imp.f042 = getCellValueToString(cell);
                        if (c == 42)  imp.f043apptype = getCellValueToString(cell);
                        if (c == 43)  imp.f044 = getCellValueToString(cell);
                        if (c == 44)  imp.f045ratio = getCellValueToString(cell);
                        if (c == 45)  imp.f046family = getCellValueToString(cell);
                        if (c == 46)  imp.f047 = getCellValueToString(cell);
                        if (c == 47)  imp.f048 = getCellValueToString(cell);
                        if (c == 48)  imp.f049 = getCellValueToString(cell);                        
                        if (c == 49)  imp.f050 = getCellValueToString(cell);
                        if (c == 50)  imp.f051 = getCellValueToString(cell);
                        if (c == 51)  imp.f052 = getCellValueToString(cell);
                        if (c == 52)  imp.f053 = getCellValueToString(cell);
                        if (c == 53)  imp.f054 = getCellValueToString(cell);
                        if (c == 54)  imp.f055 = getCellValueToString(cell);
                        if (c == 55)  imp.f056 = getCellValueToString(cell);
                        if (c == 56)  imp.f057 = getCellValueToString(cell);
                        if (c == 57)  imp.f058 = getCellValueToString(cell);
                        if (c == 58)  imp.f059 = getCellValueToString(cell);
                        if (c == 59)  imp.f060 = getCellValueToString(cell);
                        if (c == 60)  imp.f061 = getCellValueToString(cell);
                        if (c == 61)  imp.f062 = getCellValueToString(cell);
                        if (c == 62)  imp.f063 = getCellValueToString(cell);
                        if (c == 63)  imp.f064 = getCellValueToString(cell);
                        if (c == 64)  imp.f065 = getCellValueToString(cell);
                        if (c == 65)  imp.f066 = getCellValueToString(cell);
                        if (c == 66)  imp.f067 = getCellValueToString(cell);
                        if (c == 67)  imp.f068 = getCellValueToString(cell);
                        if (c == 68)  imp.f069 = getCellValueToString(cell);
                        if (c == 69)  imp.f070 = getCellValueToString(cell);
                        if (c == 70)  imp.f071 = getCellValueToString(cell);
                        if (c == 71)  imp.f072 = getCellValueToString(cell);
                        if (c == 72)  imp.f073 = getCellValueToString(cell);
                        if (c == 73)  imp.f074 = getCellValueToString(cell);
                        if (c == 74)  imp.f075 = getCellValueToString(cell);
                        if (c == 75)  imp.f076 = getCellValueToString(cell);
                        if (c == 76)  imp.f077 = getCellValueToString(cell);
                        if (c == 77)  imp.f078 = getCellValueToString(cell);
                        if (c == 78)  imp.f079 = getCellValueToString(cell);
                        if (c == 79)  imp.f080 = getCellValueToString(cell);
                        if (c == 80)  imp.f081 = getCellValueToString(cell);
                        if (c == 81)  imp.f082 = getCellValueToString(cell);
                        if (c == 82)  imp.f083 = getCellValueToString(cell);
                        if (c == 83)  imp.f084 = getCellValueToString(cell);
                        if (c == 84)  imp.f085 = getCellValueToString(cell);
                        if (c == 85)  imp.f086 = getCellValueToString(cell);
                        if (c == 86)  imp.f087 = getCellValueToString(cell);
                        if (c == 87)  imp.f088 = getCellValueToString(cell);
                        if (c == 88)  imp.f089 = getCellValueToString(cell);
                        if (c == 89)  imp.f090total = getCellValueToString(cell);
                        if (c == 90)  imp.f091 = getCellValueToString(cell);
                        if (c == 91)  imp.f092charge = getCellValueToString(cell);
                        if (c == 92)  imp.f093partial = getCellValueToString(cell);
                        if (c == 93)  imp.f094 = getCellValueToString(cell);
                        if (c == 94)  imp.f095 = getCellValueToString(cell);
                        if (c == 95)  imp.f096 = getCellValueToString(cell);
                        if (c == 96)  imp.f097 = getCellValueToString(cell);
                        if (c == 97)  imp.f098 = getCellValueToString(cell);
                        if (c == 98)  imp.f099bankcode = getCellValueToString(cell);
                        if (c == 99)   imp.f100branchcode = getCellValueToString(cell);                     
                        if (c == 100)  imp.f101 = getCellValueToString(cell);
                        if (c == 101)  imp.f102accnumber = getCellValueToString(cell);
                        if (c == 102)  imp.f103acckana = getCellValueToString(cell);
                        if (c == 103)  imp.f104acckanji = getCellValueToString(cell);
                        if (c == 104)  imp.f105 = getCellValueToString(cell);
                        if (c == 105)  imp.f106 = getCellValueToString(cell);
                        if (c == 106)  imp.f107 = getCellValueToString(cell);
                        if (c == 107)  imp.f108 = getCellValueToString(cell);
                        if (c == 108)  imp.f109 = getCellValueToString(cell);
                        if (c == 109)  imp.f110 = getCellValueToString(cell);
                        if (c == 110)  imp.f111 = getCellValueToString(cell);
                        if (c == 111)  imp.f112 = getCellValueToString(cell);
                        if (c == 112)  imp.f113 = getCellValueToString(cell);
                        if (c == 113)  imp.f114 = getCellValueToString(cell);
                        if (c == 114)  imp.f115 = getCellValueToString(cell);
                        if (c == 115)  imp.f116 = getCellValueToString(cell);
                        if (c == 116)  imp.f117 = getCellValueToString(cell);
                        if (c == 117)  imp.f118 = getCellValueToString(cell);
                        if (c == 118)  imp.f119 = getCellValueToString(cell);
                        if (c == 119)  imp.f120 = getCellValueToString(cell);
                        if (c == 120)  imp.f121 = getCellValueToString(cell);
                        if (c == 121)  imp.f122 = getCellValueToString(cell);
                        if (c == 122)  imp.f123 = getCellValueToString(cell);
                        if (c == 123)  imp.f124 = getCellValueToString(cell);
                        if (c == 124)  imp.f125 = getCellValueToString(cell);
                        if (c == 125)  imp.f126 = getCellValueToString(cell);
                        if (c == 126)  imp.f127 = getCellValueToString(cell);
                        if (c == 127)  imp.f128 = getCellValueToString(cell);
                        if (c == 128)  imp.f129 = getCellValueToString(cell);
                        if (c == 129)  imp.f130 = getCellValueToString(cell);
                        if (c == 130)  imp.f131 = getCellValueToString(cell);
                        if (c == 131)  imp.f132 = getCellValueToString(cell);
                        if (c == 132)  imp.f133 = getCellValueToString(cell);
                        if (c == 133)  imp.f134 = getCellValueToString(cell);
                        if (c == 134)  imp.f135 = getCellValueToString(cell);
                        if (c == 135)  imp.f136 = getCellValueToString(cell);
                        if (c == 136)  imp.f137 = getCellValueToString(cell);
                        if (c == 137)  imp.f138 = getCellValueToString(cell);
                        if (c == 138)  imp.f139 = getCellValueToString(cell);
                        if (c == 139)  imp.f140 = getCellValueToString(cell);
                        if (c == 140)  imp.f141 = getCellValueToString(cell);
                        if (c == 141)  imp.f142 = getCellValueToString(cell);
                        if (c == 142)  imp.f143 = getCellValueToString(cell);
                        if (c == 143)  imp.f144 = getCellValueToString(cell);
                        if (c == 144)  imp.f145 = getCellValueToString(cell);
                        if (c == 145)  imp.f146 = getCellValueToString(cell);
                        if (c == 146)  imp.f147 = getCellValueToString(cell);
                        if (c == 147)  imp.f148 = getCellValueToString(cell);
                        if (c == 148)  imp.f149 = getCellValueToString(cell);
                        if (c == 149)  imp.f150 = getCellValueToString(cell);
                        if (c == 150)  imp.f151 = getCellValueToString(cell);
                        if (c == 151)  imp.f152 = getCellValueToString(cell);
                        if (c == 152)  imp.f153 = getCellValueToString(cell);


                        #endregion
                        if (c == 152)
                        {

                            imp.shoriYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f001shoriym));

                            //20210517143135 furukawa st ////////////////////////
                            //代入間違い
                            
                            imp.shinryoYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f029shinryoym));
                            //imp.shinryoYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f001shoriym));
                            //20210517143135 furukawa ed ////////////////////////


                            imp.pbirthdayAD = DateTimeEx.GetDateFromJstr7(imp.f028pbirthday);
                            imp.startdate1ad = DateTimeEx.GetDateFromJstr7(imp.f031startdate1);
                            imp.finishdate1ad = DateTimeEx.GetDateFromJstr7(imp.f032finishdate1);

                            //20210517143019 furukawa st ////////////////////////
                            //エクセル項目には前にスペースがあった
                            
                            imp.hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f014hihomark, Microsoft.VisualBasic.VbStrConv.Narrow).Trim();
                            imp.hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f015hihonum, Microsoft.VisualBasic.VbStrConv.Narrow).Trim();

                            //imp.hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f014hihomark, Microsoft.VisualBasic.VbStrConv.Narrow);
                            //imp.hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f015hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);
                            //20210517143019 furukawa ed ////////////////////////
                        }
                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"あはき提供データ レセプト全国共通キー:{imp.f002comnum}");

                    if (!DB.Main.Insert<dataImport_AHK>(imp, tran)) return false;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col +"\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
                wb.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }


        /// <summary>
        /// あはき提供データの取得
        /// </summary>
        /// <returns></returns>
        public static List<dataImport_AHK> SelectAll(int _cym)
        {
            List<dataImport_AHK> lst = new List<dataImport_AHK>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport_ahk where cym={_cym}");
            var l=cmd.TryExecuteReaderList();
            for (int r = 0; r < l.Count; r++)
            {
                int c = 0;
                dataImport_AHK d = new dataImport_AHK();


                d.f000importid = l[r][0].ToString();//インポートID
                d.f001shoriym = l[r][1].ToString();         	                                //処理年月
                d.f002comnum = 					 l[r][2].ToString();                                      //レセプト全国共通キー
                d.f003 = 						 l[r][3].ToString();                                        //転帰請求グループ番号
                d.f004 = 						 l[r][4].ToString();                                        //支給申請書受理番号
                d.f005 = 							 l[r][5].ToString();                                    //事業区分
                d.f006sid = 					 l[r][6].ToString();                                         //医療機関コード
                d.f007 = 							 l[r][7].ToString();                                    //医療機関_異動年月日
                d.f008sname = 					 l[r][8].ToString();                                       //医療機関名(漢字)
                d.f009 = 						 l[r][9].ToString();                                        //柔整団体機関コード
                d.f010 = 						 l[r][10].ToString();                                       //柔整団体機関_異動年月日
                d.f011 = 							 l[r][11].ToString();                                   //柔整団体機関名(漢字)
                d.f012insnum = 					 l[r][12].ToString();                                     //保険者番号
                d.f013 = 								 l[r][13].ToString();                               //調整保険者番号
                d.f014hihomark = 						 l[r][14].ToString();                               //被保険者証記号
                d.f015hihonum = 						 l[r][15].ToString();                                //被保険者証番号
                d.f016setainum = 				 l[r][16].ToString();                                       //世帯番号
                d.f017 = 								 l[r][17].ToString();                               //世帯管理番号
                d.f018hihokana = 						 l[r][18].ToString();                               //被保険者氏名（カナ）
                d.f019hihokanji = 						 l[r][19].ToString();                              //被保険者氏名（漢字）
                d.f020pgender = 					 l[r][20].ToString();                                    //性別
                d.f021pnum = 							 l[r][21].ToString();                               //個人管理番号
                d.f022atenanum = 				 l[r][22].ToString();                                       //宛名番号
                d.f023 = 						 l[r][23].ToString();                                       //機械整理番号
                d.f024 = 						 l[r][24].ToString();                                       //世帯主個人管理番号
                d.f025 = 						 l[r][25].ToString();                                       //世帯主宛名番号
                d.f026 = 						 l[r][26].ToString();                                       //世帯主被保険者証記号
                d.f027 = 								 l[r][27].ToString();                               //世帯主被保険者証番号
                d.f028pbirthday = 						 l[r][28].ToString();                              //生年月日
                d.f029shinryoym = 				 l[r][29].ToString();                                      //診療年月
                d.f030 = 								 l[r][30].ToString();                               //支給決定年月
                d.f031startdate1 = 							 l[r][31].ToString();                         //診療開始日
                d.f032finishdate1 = 			 l[r][32].ToString();                                        //診療終了日
                d.f033 = 						 l[r][33].ToString();                                       //保険_診療実日数
                d.f034 = 						 l[r][34].ToString();                                       //申請年月日
                d.f035 = 						 l[r][35].ToString();                                       //審査申出年月日
                d.f036 = 						 l[r][36].ToString();                                       //審査申出受領年月日
                d.f037 = 						 l[r][37].ToString();                                       //支給決定年月日
                d.f038 = 						 l[r][38].ToString();                                       //支給年月日
                d.f039 = 						 l[r][39].ToString();                                       //支給実績データ作成年月日
                d.f040 = 						 l[r][40].ToString();                                       //支給実績データ更新年月日
                d.f041 = 						 l[r][41].ToString();                                       //保険制度（保険種別①）
                d.f042 = 								 l[r][42].ToString();                               //療養費データ区分
                d.f043apptype = 				 l[r][43].ToString();                                        //療養費種別 04:あんま 05:鍼灸(堺市より)
                d.f044 = 							 l[r][44].ToString();                                   //点数表
                d.f045ratio = 						 l[r][45].ToString();                                  //給付割合
                d.f046family = 					 l[r][46].ToString();                                     //本人家族入外
                d.f047 = 						 l[r][47].ToString();                                       //特記事項１
                d.f048 = 						 l[r][48].ToString();                                       //特記事項２
                d.f049 = 						 l[r][49].ToString();                                       //特記事項３
                d.f050 = 						 l[r][50].ToString();                                       //特記事項４
                d.f051 = 						 l[r][51].ToString();                                       //特記事項５
                d.f052 = 						 l[r][52].ToString();                                       //長期区分
                d.f053 = 						 l[r][53].ToString();                                       //算定区分１
                d.f054 = 						 l[r][54].ToString();                                       //算定区分２
                d.f055 = 						 l[r][55].ToString();                                       //算定区分３
                d.f056 = 						 l[r][56].ToString();                                       //所得区分
                d.f057 = 						 l[r][57].ToString();                                       //変更前所得区分
                d.f058 = 						 l[r][58].ToString();                                       //前期該当区分
                d.f059 = 						 l[r][59].ToString();                                       //特別療養費
                d.f060 = 						 l[r][60].ToString();                                       //状態区分
                d.f061 = 						 l[r][61].ToString();                                       //海外療養費区分
                d.f062 = 						 l[r][62].ToString();                                       //初診料有無
                d.f063 = 						 l[r][63].ToString();                                       //高額療養費該当区分
                d.f064 = 						 l[r][64].ToString();                                       //強制保留区分
                d.f065 = 						 l[r][65].ToString();                                       //強制登録区分
                d.f066 = 						 l[r][66].ToString();                                       //転帰請求区分
                d.f067 = 						 l[r][67].ToString();                                       //申請区分
                d.f068 = 						 l[r][68].ToString();                                       //審査結果区分
                d.f069 = 						 l[r][69].ToString();                                       //却下区分
                d.f070 = 						 l[r][70].ToString();                                       //却下理由
                d.f071 = 						 l[r][71].ToString();                                       //通知書印刷済該当区分
                d.f072 = 						 l[r][72].ToString();                                       //傷病コード
                d.f073 = 						 l[r][73].ToString();                                       //傷病名
                d.f074 = 						 l[r][74].ToString();                                       //備考１
                d.f075 = 						 l[r][75].ToString();                                       //備考２
                d.f076 = 						 l[r][76].ToString();                                       //データ区分
                d.f077 = 						 l[r][77].ToString();                                       //食事区分
                d.f078 = 						 l[r][78].ToString();                                       //公費区分
                d.f079 = 						 l[r][79].ToString();                                       //自支給期間
                d.f080 = 						 l[r][80].ToString();                                       //至支給期間
                d.f081 = 						 l[r][81].ToString();                                       //過誤再審査コード
                d.f082 = 						 l[r][82].ToString();                                       //保険_一部負担金
                d.f083 = 						 l[r][83].ToString();                                       //算定_保険_他法優先公費負担額
                d.f084 = 						 l[r][84].ToString();                                       //算定_保険_国保優先公費負担額
                d.f085 = 						 l[r][85].ToString();                                       //保険_食事回数
                d.f086 = 						 l[r][86].ToString();                                       //算定_保険_食事基準額
                d.f087 = 						 l[r][87].ToString();                                       //算定_保険_食事患者負担額
                d.f088 = 						 l[r][88].ToString();                                       //算定_保険_食事他法優先公費負担額
                d.f089 = 							 l[r][89].ToString();                                   //算定_保険_食事国保優先公費負担額
                d.f090total = 					 l[r][90].ToString();                                      //費用額
                d.f091 = 							 l[r][91].ToString();                                   //支給予定額
                d.f092charge = 							 l[r][92].ToString();                             //保険者負担金額
                d.f093partial = 				 l[r][93].ToString();                                        //患者負担額
                d.f094 = 						 l[r][94].ToString();                                       //指定公費額
                d.f095 = 						 l[r][95].ToString();                                       //高額療養費
                d.f096 = 						 l[r][96].ToString();                                       //振込先口座有無区分
                d.f097 = 						 l[r][97].ToString();                                       //振込先区分
                d.f098 = 								 l[r][98].ToString();                               //振込区分
                d.f099bankcode = 						 l[r][99].ToString();                               //振込銀行コード
                d.f100branchcode = 				 l[r][100].ToString();                                    //振込支店コード
                d.f101 = 								 l[r][101].ToString();                              //預金種目
                d.f102accnumber = 						 l[r][102].ToString();                             //口座番号
                d.f103acckana = 						 l[r][103].ToString();                               //口座名義人（カナ）
                d.f104acckanji = 				 l[r][104].ToString();                                      //口座名義人（漢字）
                d.f105 = 						 l[r][105].ToString();                                      //公１_負担者番号
                d.f106 = 						 l[r][106].ToString();                                      //公１_受給者番号
                d.f107 = 						 l[r][107].ToString();                                      //公１_診療実日数
                d.f108 = 						 l[r][108].ToString();                                      //公１_決定点数
                d.f109 = 						 l[r][109].ToString();                                      //公１_負担者負担金額
                d.f110 = 						 l[r][110].ToString();                                      //公１_一部負担金
                d.f111 = 						 l[r][111].ToString();                                      //公１_患者負担額
                d.f112 = 						 l[r][112].ToString();                                      //公２_負担者番号
                d.f113 = 						 l[r][113].ToString();                                      //公２_受給者番号
                d.f114 = 						 l[r][114].ToString();                                      //公２_診療実日数
                d.f115 = 						 l[r][115].ToString();                                      //公２_決定点数
                d.f116 = 						 l[r][116].ToString();                                      //公２_負担者負担金額
                d.f117 = 						 l[r][117].ToString();                                      //公２_一部負担金
                d.f118 = 						 l[r][118].ToString();                                      //公２_患者負担額
                d.f119 = 						 l[r][119].ToString();                                      //公３_負担者番号
                d.f120 = 						 l[r][120].ToString();                                      //公３_受給者番号
                d.f121 = 						 l[r][121].ToString();                                      //公３_診療実日数
                d.f122 = 						 l[r][122].ToString();                                      //公３_決定点数
                d.f123 = 						 l[r][123].ToString();                                      //公３_負担者負担金額
                d.f124 = 						 l[r][124].ToString();                                      //公３_一部負担金
                d.f125 = 						 l[r][125].ToString();                                      //公３_患者負担額
                d.f126 = 						 l[r][126].ToString();                                      //公４_負担者番号
                d.f127 = 						 l[r][127].ToString();                                      //公４_受給者番号
                d.f128 = 						 l[r][128].ToString();                                      //公４_診療実日数
                d.f129 = 						 l[r][129].ToString();                                      //公４_決定点数
                d.f130 = 						 l[r][130].ToString();                                      //公４_負担者負担金額
                d.f131 = 						 l[r][131].ToString();                                      //公４_一部負担金
                d.f132 = 						 l[r][132].ToString();                                      //公４_患者負担額
                d.f133 = 						 l[r][133].ToString();                                      //公１_食事回数
                d.f134 = 						 l[r][134].ToString();                                      //公１_食事基準額
                d.f135 = 						 l[r][135].ToString();                                      //公１_食事患者負担額
                d.f136 = 						 l[r][136].ToString();                                      //公２_食事回数
                d.f137 = 						 l[r][137].ToString();                                      //公２_食事基準額
                d.f138 = 						 l[r][138].ToString();                                      //公２_食事患者負担額
                d.f139 = 						 l[r][139].ToString();                                      //公３_食事回数
                d.f140 = 						 l[r][140].ToString();                                      //公３_食事基準額
                d.f141 = 						 l[r][141].ToString();                                      //公３_食事患者負担額
                d.f142 = 						 l[r][142].ToString();                                      //公４_食事回数
                d.f143 = 						 l[r][143].ToString();                                      //公４_食事基準額
                d.f144 = 						 l[r][144].ToString();                                      //公４_食事患者負担額
                d.f145 = 						 l[r][145].ToString();                                      //再審査区分
                d.f146 = 						 l[r][146].ToString();                                      //強制入力区分
                d.f147 = 						 l[r][147].ToString();                                      //登録機能ID
                d.f148 = 						 l[r][148].ToString();                                      //登録者ID
                d.f149 = 						 l[r][149].ToString();                                      //登録日時
                d.f150 = 						 l[r][150].ToString();                                      //最終更新機能ID
                d.f151 = 						 l[r][151].ToString();                                      //最終更新者ID
                d.f152 = 						 l[r][152].ToString();                                      //最終更新日時
                d.f153 = l[r][153].ToString();                      //削除フラグ


                d.shoriYMAD =  int.Parse(l[r][154].ToString());// DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(d.f001shoriym));//処理年月西暦
                d.shinsaYMAD = int.Parse(l[r][155].ToString());                        //審査年月西暦 変換元項目なし
                d.shinryoYMAD = int.Parse(l[r][156].ToString());// DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(d.f029shinryoym));//診療年月西暦
                d.pbirthdayAD = DateTime.Parse(l[r][157].ToString());// DateTimeEx.GetDateFromJstr7(d.f028pbirthday);       //生年月日西暦
                d.firstdatead = DateTime.Parse(l[r][158].ToString());//DateTimeEx.GetDateFromJstr7(d.f032finishdate1);     //初検年月日西暦
                d.startdate1ad = DateTime.Parse(l[r][159].ToString());//DateTimeEx.GetDateFromJstr7(d.f031startdate1);     //開始日西暦
                d.finishdate1ad = DateTime.Parse(l[r][160].ToString());// DateTimeEx.GetDateFromJstr7(d.f032finishdate1);   //終了日西暦
                d.hihomark_narrow = l[r][161].ToString();//被保険者証記号半角
                d.hihonum_narrow = l[r][162].ToString();// Microsoft.VisualBasic.Strings.StrConv(d.f015hihonum,Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角

                d.cym = int.Parse(l[r][163].ToString());                         //メホール請求年月管理用


                lst.Add(d);
            }

            return lst;

        }


    }


    /// <summary>
    /// 既出テーブルだが使わないかも
    /// (既に出力した人は出さない場合があるので、その人を記録しておくテーブル)
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
