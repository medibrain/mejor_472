﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Matsudoshi

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export_ahk
    {
        //

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int exportid { get;set; }=0;                                      //プライマリキー メホール管理用;

        public int cym { get; set; } = 0;                                        //メホール上の処理年月 メホール管理用;
        public int ym { get; set; } = 0;                                         //診療年月 メホール管理用;
        public int aid { get; set; } = 0;                                        //aidメホール管理用;

        public string comnum { get; set; } = string.Empty;                       //全国レセプト共通KEY;
        public string shinsaym { get; set; } = string.Empty;                     //審査月;
        public string bunsho_no { get; set; } = string.Empty;                    //文書番号;
        public string shoriym { get; set; } = string.Empty;                      //処理月;
        public string sejutuym { get; set; } = string.Empty;                     //施術月;
        public string insnum { get; set; } = string.Empty;                       //保険者番号;
        public string hnum { get; set; } = string.Empty;                         //被保険者番号;
        public string atenanum { get; set; } = string.Empty;                     //宛名番号;
        public string pbirthday { get; set; } = string.Empty;                    //生年月日;
        public string pname { get; set; } = string.Empty;                        //受療者名;
        public int total { get; set; } = 0;                                      //合計;
        public int partial { get; set; } = 0;                                    //一部負担;
        public int charge { get; set; } = 0;                                     //保険請求;
        public string clinicname { get; set; } = string.Empty;                   //施術所名;
        public string imagefilename { get; set; } = string.Empty;                //画像ファイル名;
        public string tenken { get; set; } = string.Empty;                       //事務・往療料点検_返戻事由;
        public string kadenhandan { get; set; } = string.Empty;                  //千葉市架電判断（往療料等）;
        public string shoriym2 { get; set; } = string.Empty;                     //処理月;
        public string henreikahi { get; set; } = string.Empty;                   //返戻可否;
        public string shokai_jiyu { get; set; } = string.Empty;                  //患者照会事由;
        public string handan { get; set; } = string.Empty;                       //千葉市発送判断;
        public string henrei_jiyu { get; set; } = string.Empty;                  //患者照会_返戻事由;
        public string kadenhandan2 { get; set; } = string.Empty;                 //千葉市架電判断;
        public string kadendate { get; set; } = string.Empty;                    //架電日付;
        public string outaisha { get; set; } = string.Empty;                     //応対者;
        public string kaden_res { get; set; } = string.Empty;                    //架電結果;



        #endregion


        /// <summary>
        /// マルチTIFF候補リスト（tiffファイルのパスが入る）
        /// </summary>
        public static List<string> lstImageFilePath = new List<string>();



        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            
            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
               
                wf.LogPrint("出力テーブル作成");

                ////出力データ用テーブルに登録
                //if (!InsertExportTable( cym, wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                ////全出力データ取得
                //List<export_ahk> lstExportAHK = DB.Main.Select<export_ahk>($"cym={cym}").ToList();
                //lstExportAHK.Sort((x, y) => x.aid.CompareTo(y.aid));
                List<App> lstExportAHK = App.GetAppsWithWhere($" where a.cym={cym} and a.aapptype <>6");

                ////csv出力
                //if (!Export(lstExportAHK, strNohinDir, wf)) return false;

                //画像出力
                if (!ExportImage(lstExportAHK, strNohinDir, wf)) return false;

                ////20210604164354 furukawa st ////////////////////////
                ////マッチング無し画像リスト出力               
                //if (!NotMathingList(cym, strNohinDir, wf)) return false;
                ////20210604164354 furukawa ed ////////////////////////


                ////20210604172352 furukawa st ////////////////////////
                ////提供データ有り画像無しリスト出力                
                //if (!NoneApplicationList(cym, strNohinDir, wf)) return false;                
                ////20210604172352 furukawa ed ////////////////////////
                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// {<param name="wf">waitform</param>
        /// <returns></returns>
        private static bool InsertExportTable(int cym ,WaitForm wf)
            //private static bool InsertExportTable(List<App> lstApp, int cym, WaitForm wf)
        {

            //20210604150816 furukawa st ////////////////////////
            //AUXにIDがあるレコードのみ出力
            
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" select  ");
            sb.AppendLine("  a.cym  		");
            sb.AppendLine(" ,a.ym          	");
            sb.AppendLine(" ,a.aid 			");
            sb.AppendLine(" ,a.comnum 		");
            sb.AppendLine(" ,d.shinsaym 	");
            sb.AppendLine(" ,a.inum 		");
            sb.AppendLine(" ,a.hnum 		");
            sb.AppendLine(" ,d.atenanum 	");
            sb.AppendLine(" ,a.pbirthday 	");
            sb.AppendLine(" ,a.pname 		");
            sb.AppendLine(" ,a.atotal 		");
            sb.AppendLine(" ,a.apartial 	");
            sb.AppendLine(" ,a.acharge 		");
            sb.AppendLine(" ,a.sname 		");
            sb.AppendLine(" ,a.aimagefile   ");
            sb.AppendLine(" ,a.aapptype     ");

            //auxに存在し、matchingid01が存在するレコードのみ出力
            sb.AppendLine(" from ");
            sb.AppendLine(" application_aux x inner join ");
            sb.AppendLine(" application a on ");
            sb.AppendLine(" x.aid=a.aid ");
            sb.AppendLine(" left join dataimport_ahk d on  ");
            sb.AppendLine(" a.comnum=d.comnum and   ");
            sb.AppendLine(" a.cym=d.cym  ");


            sb.AppendLine(" where  ");

            //20210616130158 furukawa st ////////////////////////
            //続紙には紐付いてないが、続紙も出力対象            
            //sb.AppendLine(" x.matchingID01 <> '' and x.matchingID01 <> '0' and ");
            //20210616130158 furukawa ed ////////////////////////


            sb.AppendLine($" a.cym={cym} ");

            //柔整以外　続紙含め全部
            sb.AppendLine(" and a.aapptype<>6 ");

            //20210616130326 furukawa st ////////////////////////
            //入力したもの全て出す            
            sb.AppendLine(" and a.ym<>0 ");
            //20210616130326 furukawa ed ////////////////////////


            string strsql = sb.ToString();

            #region old
            //"select " +
            //"a.cym" + 			       //0
            //",a.ym" +         	       //1
            //",a.aid" +			       //2
            //",a.comnum" +		       //3
            //",d.shinsaym" +		       //4
            //",a.inum" +			       //5
            //",a.hnum" +			       //6
            //",d.atenanum" +		       //7
            //",a.pbirthday" +		       //8
            //",a.pname" +			       //9
            //",a.atotal" +		       //10
            //",a.apartial" +		       //11
            //",a.acharge" +		       //12
            //",a.sname" +			       //13
            //",a.aimagefile " +          //14
            //",a.aapptype " +          //15　申請書種別判別のため

            ////申請書だけでなく全部必要
            //"from application a left join dataimport_ahk d on " +
            // //"from application a inner join dataimport_ahk d on " +



            //"a.comnum=d.comnum and  " +
            //"a.cym=d.cym " +


            //$"where  a.cym={cym}" +
            //" and a.aapptype<>6 ";//柔整はいらない

            //申請書だけでなく全部必要
            //$"and a.aapptype in (7,8)";//あはきのみ
            #endregion


            //20210604150816 furukawa ed ////////////////////////

            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            DB.Command cmd = new DB.Command(strsql,tran);
            var lst = cmd.TryExecuteReaderList();


            wf.SetMax(lst.Count);
            //wf.SetMax(lstApp.Count);

            
            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from export_ahk where cym='{cym}'", tran);

            try
            {
                cmddel.TryExecuteNonQuery();
                int c = 0;
                string tmpShisaym = string.Empty;//続紙用
                for (int r = 0; r < lst.Count; r++)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        tran.Rollback();
                        return false;
                    }

                    export_ahk exp = new export_ahk();
                    
                    exp.cym = int.Parse(lst[r][0].ToString());          //メホール上の処理年月 メホール管理用
                    exp.ym = int.Parse(lst[r][1].ToString());		    //診療年月 メホール管理用
                    exp.aid = int.Parse(lst[r][2].ToString());          //aidメホール管理用

                    //20210601142748 furukawa st ////////////////////////
                    //マッチング無しが分かるように
                    string strDispLog = $",aid:{exp.aid}";
                    if (exp.ym > 0) strDispLog += ",申請書";
                    else strDispLog += ",申請書以外";
                    
                    if (lst[r][3].ToString() == string.Empty) strDispLog += ",レセプト全国共通キーなし";                    
                    else strDispLog += ",レセプト全国共通キーあり";

                    if (lst[r][4].ToString() == string.Empty) strDispLog += ",マッチング無し";

                    wf.LogPrint(strDispLog);
                    //20210601142748 furukawa ed ////////////////////////

                    if (exp.ym > 0)
                    {
                        exp.comnum = lst[r][3].ToString();                  //全国レセプト共通KEY
                    }
                    else
                    {
                        //続紙の場合区別する項目がないのでここに設定
                        exp.comnum = $"{lst[r][15]}";
                        //exp.comnum = $"申請書以外_{r}";
                    }

                    if (exp.ym > 0)
                    {
                        //審査年月が無いものは飛ばす
                        if (lst[r][4].ToString() != string.Empty)
                        {
                            exp.shinsaym = DateTimeEx.GetGyymmFromAdYM(int.Parse(lst[r][4].ToString())).ToString();                //審査月 50201型
                            tmpShisaym = exp.shinsaym;
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        exp.shinsaym = tmpShisaym;
                    }
                    exp.bunsho_no = string.Empty;                       //文書番号　空欄でいい2020/08/18伸作さんより



                    exp.shoriym = string.Empty;//2020/08/19空欄でいい伸作さんより
                    //DateTime shoriym = DateTimeEx.ToDateTime6(lst[r][4].ToString()).AddMonths(1);                    
                    // exp.shoriym = DateTimeEx.GetGyymmFromAdYM(shoriym.Year*100+shoriym.Month).ToString();//処理月 =審査月+1月？ 50201型


                    if(exp.ym>0) exp.sejutuym = DateTimeEx.GetGyymmFromAdYM(exp.ym).ToString();		                 //施術月 50201型

                    exp.insnum = lst[r][5].ToString(); 				  //保険者番号
                    exp.hnum = lst[r][6].ToString();				  //被保険者番号
                    exp.atenanum = lst[r][7].ToString();			  //宛名番号

                    if (exp.ym > 0) exp.pbirthday = DateTimeEx.GetIntJpDateWithEraNumber(DateTime.Parse(lst[r][8].ToString())).ToString();  			    //生年月日
                    exp.pname = lst[r][9].ToString();				        //受療者名
                    exp.total = int.Parse(lst[r][10].ToString());	        //合計
                    exp.partial = int.Parse(lst[r][11].ToString());		    //一部負担
                    exp.charge = int.Parse(lst[r][12].ToString());          //保険請求
                    exp.clinicname = lst[r][13].ToString();				    //施術所名


                    if (exp.ym > 0) exp.imagefilename = $"{exp.shinsaym}-{lst[r][2].ToString()}.tif"; 	            //画像ファイル名 審査月-画像ファイル名 拡張子は固定にした
                    else exp.imagefilename = $"{tmpShisaym}-{lst[r][2].ToString()}.tif"; 	            //画像ファイル名 審査月-画像ファイル名 拡張子は固定にした

                    exp.tenken = string.Empty;                             //事務・往療料点検_返戻事由
                    exp.kadenhandan = string.Empty;                        //千葉市架電判断（往療料等）
                    exp.shoriym2 = string.Empty; 						   //処理月
                    exp.henreikahi = string.Empty; 						   //返戻可否
                    exp.shokai_jiyu = string.Empty; 				       //患者照会事由
                    exp.handan = string.Empty; 							   //千葉市発送判断
                    exp.henrei_jiyu = string.Empty; 				       //患者照会_返戻事由
                    exp.kadenhandan2 = string.Empty; 					   //千葉市架電判断
                    exp.kadendate = string.Empty; 						   //架電日付
                    exp.outaisha = string.Empty; 						   //応対者
                    exp.kaden_res = string.Empty;                          //架電結果


                    //同ヒホバン、生年月日、性別の人は出さない
                    //既出テーブルを使用してもいいが、2度手間になりそう…

                    //DB.Command cmddup = new DB.Command(
                    //    $"select * from export_ahk where hnum='{exp.hnum}' " +
                    //    $"and pbirthday='{exp.pbirthday.ToString()}' " +
                    //    $"and total={exp.total} limit 1", tran);


                    //if (cmddup.TryExecuteScalar() != null)
                    //{
                    //    wf.LogPrint($"{exp.hnum}既に出しました");
                    //    wf.InvokeValue++;
                    //    continue;
                    //}


                    if (!DB.Main.Insert<export_ahk>(exp, tran)) return false;

                    //20210601143400 furukawa st ////////////////////////
                    //20210601142748で先にAIDを出して不具合時に分かるように
                    //wf.LogPrint($"aid:{exp.aid}");
                    //20210601143400 furukawa ed ////////////////////////

                    wf.InvokeValue++;
                    
                }


                tran.Commit();
                wf.LogPrint($",出力テーブル登録完了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                //20210601143217 furukawa st ////////////////////////
                //ログファイル                
                wf.LogSave($"{System.Windows.Forms.Application.StartupPath}\\Info\\{Insurer.CurrrentInsurer.dbName}_{DateTime.Now.ToString("yyyy年MM月dd日")}出力.csv");
                //20210601143217 furukawa ed ////////////////////////

                cmd.Dispose();
                cmddel.Dispose();
            }

               
        }


        /// <summary>
        /// 不要以外画像出力
        /// </summary>
        /// <param name="lstExportAHK">出力するリスト</param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(List<App> lstExportAHK , string strDir, WaitForm wf)
        //private static bool ExportImage(List<export_ahk> lstExportAHK , string strDir, WaitForm wf)
        {
            string imageName = string.Empty;       //tifコピー先の名前
            string strImageDir = $"{strDir}";      //tifコピー先フォルダ

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            wf.SetMax(lstExportAHK.Count);

            try
            {
                //出力先パス
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";

                string strCYMWareki =
                    DateTimeEx.GetEraNumberYearFromYYYYMM(int.Parse(lstExportAHK[0].CYM.ToString())).ToString() +
                    lstExportAHK[0].CYM.ToString().Substring(4, 2);

                //0番目のファイルパス                
                //imageName = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[0].Aid}.tif";
                string strFileNameApp = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[0].Aid}.tif";

                for (int r = 0; r < lstExportAHK.Count(); r++)
                {
                    imageName = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[r].Aid}.tif";
                    //imageName = $"{strImageDir}\\{lstExportAHK[0].GetImageFullPath()}";

                    //中断
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        System.Windows.Forms.MessageBox.Show("コピー中のデータは手で削除してください");
                        return false;
                    }

                    if((lstExportAHK[r].MediYear> 0) && (lstImageFilePath.Count != 0))
                    //レセプト全国共通キーが20桁の場合、申請書と見なす
                    //if ((lstExportAHK[r].comnum.Length == 20) && (lstImageFilePath.Count != 0))
                    //{
                    //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                    //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                    //その上で、この申請書をマルチTIFF候補リストに追加

                    {

                        wf.LogPrint($"申請書出力中:{imageName}");

                        if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, strFileNameApp))
                        //if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                        {
                            System.Windows.Forms.MessageBox.Show(
                                System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                            return false;
                        }

                        lstImageFilePath.Clear();
                        lstImageFilePath.Add(App.GetApp(lstExportAHK[r].Aid).GetImageFullPath());
                        imageName = $"{strImageDir}\\{imageName}";
                        strFileNameApp = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[r].Aid}.tif";
                    }


                    else if ((lstExportAHK[r].MediYear>0) && (lstImageFilePath.Count == 0))
                    //else if ((lstExportAHK[r].comnum.Length == 20) && (lstImageFilePath.Count == 0))
                    {
                        imageName = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[r].Aid}.tif";
                        strFileNameApp = $"{strImageDir}\\{strCYMWareki}-{lstExportAHK[r].Aid}.tif";

                        //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
                        lstImageFilePath.Add(App.GetApp(lstExportAHK[r].Aid).GetImageFullPath());
                        wf.LogPrint($"申請書出力中:{imageName}");
                    }

                    ////20210601172729 furukawa st ////////////////////////
                    ////マッチング無し画像も出す

                    ////マッチング無しデータの場合、前に付くはずの審査年月がないので頭ハイフンで探す
                    //else if (lstExportAHK[r].imagefilename.Substring(0, 1) == "-")
                    //{
                    //    //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                    //    //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                    //    //その上で、この申請書をマルチTIFF候補リストに追加
                    //    wf.LogPrint($"マッチング無画像出力中:{imageName}");


                    //    if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                    //    {
                    //        System.Windows.Forms.MessageBox.Show(
                    //            System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                    //        return false;
                    //    }

                    //    lstImageFilePath.Clear();


                    //    lstImageFilePath.Add(App.GetApp(lstExportAHK[r].aid).GetImageFullPath());
                    //    imageName = $"{strImageDir}\\NoMatching{lstExportAHK[r].imagefilename}";
                    //}
                    ////20210601172729 furukawa ed ////////////////////////


                    else if (lstExportAHK[r].AppType<0 && lstExportAHK[r].AppType!=APP_TYPE.不要)
                        //else if (new string[] { "-1", "-3", "-5", "-6", "-9", "-11", "-13", "-14" }.Contains(lstExportAHK[r].comnum))
                            {


                        //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
                        lstImageFilePath.Add(App.GetApp(lstExportAHK[r].Aid).GetImageFullPath());
                        wf.LogPrint($"申請書以外:{imageName}");
                    }


                    wf.InvokeValue++;

                }

                //20210729223209 furukawa st ////////////////////////
                //最後が続紙の場合ファイル名を申請書に合わせる
                
                //最終画像出力
                //ループの最後の画像を出力
                if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(strFileNameApp))
                    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, strFileNameApp);

                //if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                //    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);
                //20210729223209 furukawa ed ////////////////////////

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                
            }
            
        }


        /// <summary>
        /// DBからデータを取得してCSV出力まで
        /// <paramref name="lstExportAHK"/>出力リスト</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        private static bool Export(List<export_ahk> lstExportAHK ,string strDir,WaitForm wf)            
        {
            //出力csv名
            string strFileName = $"{strDir}\\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_千葉市国保あはき.csv";


            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ

      
            System.IO.Directory.CreateDirectory(strDir);            //納品フォルダ
            System.IO.Directory.CreateDirectory(strImageDir);       //納品フォルダ\image                       

            wf.LogPrint("ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lstExportAHK.Count<export_ahk>());

            #region NPOIコード
            //ベースのexcelを開いてデータだけ出す方がらくちん　と思ったが、NPOIが面倒なのでCsvにした
            //strFileName += $"{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_千葉市国保あはき.xlsx";
            //DB.Command cmd = new DB.Command(DB.Main, strSQL);
            //var l=cmd.TryExecuteReaderList();
            //wf.SetMax(l.Count);
            //string strBaseExcelFileName = "千葉市国保_あはき申請書一覧.xlsx";            
            //System.IO.File.Copy(Settings.BaseExcelDir + "\\" + strBaseExcelFileName,strFileName);
            //System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.OpenOrCreate);
            //NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook(fs);
            //NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            //NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);
            #endregion

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            //出力csv
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName,false,System.Text.Encoding.GetEncoding("shift-jis"));
            
            try
            {
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                

                for (int r=0;r<lstExportAHK.Count();r++)                
                {
                    string strres = string.Empty;
                    imageName = $"{strImageDir}\\{lstExportAHK[r].imagefilename}";

                    //csvは申請書レコードのみ必要
                    if (lstExportAHK[r].comnum.Length==20)
                    {

                        strres += $"{lstExportAHK[r].comnum},";
                        strres += $"{lstExportAHK[r].shinsaym},";
                        strres += $"{lstExportAHK[r].bunsho_no},";
                        strres += $"{lstExportAHK[r].shoriym},";
                        strres += $"{lstExportAHK[r].sejutuym},";
                        strres += $"{lstExportAHK[r].insnum},";
                        strres += $"{lstExportAHK[r].hnum},";
                        strres += $"{lstExportAHK[r].atenanum},";
                        strres += $"{lstExportAHK[r].pbirthday},";
                        strres += $"{lstExportAHK[r].pname},";
                        strres += $"{lstExportAHK[r].total},";
                        strres += $"{lstExportAHK[r].partial},";
                        strres += $"{lstExportAHK[r].charge},";
                        strres += $"{lstExportAHK[r].clinicname},";
                        strres += $"{lstExportAHK[r].imagefilename},";
                        strres += $"{lstExportAHK[r].tenken},";
                        strres += $"{lstExportAHK[r].kadenhandan},";
                        strres += $"{lstExportAHK[r].shoriym2},";
                        strres += $"{lstExportAHK[r].henreikahi},";
                        strres += $"{lstExportAHK[r].shokai_jiyu},";
                        strres += $"{lstExportAHK[r].handan},";
                        strres += $"{lstExportAHK[r].henrei_jiyu},";
                        strres += $"{lstExportAHK[r].kadenhandan2},";
                        strres += $"{lstExportAHK[r].kadendate},";
                        strres += $"{lstExportAHK[r].outaisha},";
                        strres += $"{lstExportAHK[r].kaden_res}";

                        sw.WriteLine(strres);


                        #region npoiのコード
                        //int rownumber = 2;

                        //foreach (export_ahk e in l)
                        // //for (int colcnt = 0; colcnt < row.Cells.Count; colcnt++)
                        //  {
                        //    NPOI.SS.UserModel.ICell cell = row.GetCell(colcnt);
                        //NPOI.SS.UserModel.ICell cell = row.CreateCell(colcnt);
                        //if (colcnt == 0) cell.SetCellValue(l[rownumber].comnum);
                        //if (colcnt == 1) cell.SetCellValue(l[rownumber].shinsaym);
                        //if (colcnt == 2) cell.SetCellValue(l[rownumber].bunsho_no);
                        //if (colcnt == 3) cell.SetCellValue(l[rownumber].shoriym);
                        //if (colcnt == 4) cell.SetCellValue(l[rownumber].sejutuym);
                        //if (colcnt == 5) cell.SetCellValue(l[rownumber].insnum);
                        //if (colcnt == 6) cell.SetCellValue(l[rownumber].hnum);
                        //if (colcnt == 7) cell.SetCellValue(l[rownumber].atenanum);
                        //if (colcnt == 8) cell.SetCellValue(l[rownumber].pbirthday);
                        //if (colcnt == 9) cell.SetCellValue(l[rownumber].pname);
                        //if (colcnt == 10) cell.SetCellValue(l[rownumber].total);
                        //if (colcnt == 11) cell.SetCellValue(l[rownumber].partial);
                        //if (colcnt == 12) cell.SetCellValue(l[rownumber].charge);
                        //if (colcnt == 13) cell.SetCellValue(l[rownumber].clinicname);
                        //if (colcnt == 14) cell.SetCellValue(l[rownumber].imagefilename);
                        //if (colcnt == 15) cell.SetCellValue(l[rownumber].tenken);
                        //if (colcnt == 16) cell.SetCellValue(l[rownumber].kadenhandan);
                        //if (colcnt == 17) cell.SetCellValue(l[rownumber].shoriym2);
                        //if (colcnt == 18) cell.SetCellValue(l[rownumber].henreikahi);
                        //if (colcnt == 19) cell.SetCellValue(l[rownumber].shokai_jiyu);
                        //if (colcnt == 20) cell.SetCellValue(l[rownumber].handan);
                        //if (colcnt == 21) cell.SetCellValue(l[rownumber].henrei_jiyu);
                        //if (colcnt == 22) cell.SetCellValue(l[rownumber].kadenhandan2);
                        //if (colcnt == 23) cell.SetCellValue(l[rownumber].kadendate);
                        //if (colcnt == 24) cell.SetCellValue(l[rownumber].outaisha);
                        //if (colcnt == 25) cell.SetCellValue(l[rownumber].kaden_res);
                        //row.Cells.Add(cell);
                        //  }
                        #endregion

                    }

                    
                    wf.InvokeValue++;
                }
                sw.Close();

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
                return false;
            }
            finally
            {
                #region npoiコード
                //wb.Close();
                //fs.Close();
                //cmd.Dispose();
                //sw.Close();            
                #endregion

            }

        }

        //20210604164140 furukawa st ////////////////////////
        //マッチング無し画像リスト出力。何かの時に役立てば
        
        /// <summary>
        /// マッチング無しリスト表示
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool NotMathingList(int cym, string strDir, WaitForm wf)
        {

            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" select  ");
            sb.AppendLine("  a.cym  		");
            sb.AppendLine(" ,a.ym          	");
            sb.AppendLine(" ,a.aid 			");
            sb.AppendLine(" ,a.comnum 		");
            sb.AppendLine(" ,d.shinsaym 	");
            sb.AppendLine(" ,a.inum 		");
            sb.AppendLine(" ,a.hnum 		");
            sb.AppendLine(" ,d.atenanum 	");
            sb.AppendLine(" ,a.pbirthday 	");
            sb.AppendLine(" ,a.pname 		");
            sb.AppendLine(" ,a.atotal 		");
            sb.AppendLine(" ,a.apartial 	");
            sb.AppendLine(" ,a.acharge 		");
            sb.AppendLine(" ,a.sname 		");
            sb.AppendLine(" ,a.aimagefile   ");
            sb.AppendLine(" ,a.aapptype     ");

            //auxに存在し、matchingid01が存在するレコードのみ出力
            sb.AppendLine(" from ");
            sb.AppendLine(" application_aux x inner join ");
            sb.AppendLine(" application a on ");
            sb.AppendLine(" x.aid=a.aid ");

            sb.AppendLine(" left join dataimport_ahk d on  ");
            sb.AppendLine(" a.comnum=d.comnum and   ");
            sb.AppendLine(" a.cym=d.cym  ");


            sb.AppendLine(" where  ");
            //マッチングしない場合はIDが無いはず
            sb.AppendLine(" x.matchingID01 = '' or x.matchingID01 = '0' ");

            sb.AppendLine($"and a.cym={cym} ");

            //柔整、はり、あんま、続紙を除いた何もされていない申請書レコード
            sb.AppendLine(" and a.aapptype=0 ");

            string strsql = sb.ToString();


            DB.Command cmd = new DB.Command(DB.Main,strsql);
            var lst = cmd.TryExecuteReaderList();
            
            //無い場合は出さない            
            if (lst.Count == 0) return true;


            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;

            StringBuilder sbNotMatch = new StringBuilder();
            foreach(var item in lst)
            {
                string strNotMatch =
                    $"メホール請求年月:{cym},AID:{item[2].ToString()},画像ファイル名:{item[14].ToString()}," +
                    $"受療者名:{item[9].ToString()},レセプト全国共通キー:{item[3].ToString()}";
               
                sbNotMatch.AppendLine(strNotMatch);
                wf.InvokeValue++;
            }

            string strListFilename = $"{strDir}\\メディブレーン確認用マッチングデータ無し申請書画像リスト{cym}.csv";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strListFilename);
            try
            {
                sw.WriteLine("以下はマッチングデータ無し申請書画像です。各保険者処理にて使用ください");
                sw.Write(sbNotMatch.ToString());
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = strListFilename;
                System.Diagnostics.Process.Start(si);
            }

        }



        //20210604172050 furukawa st ////////////////////////
        //提供データ有り画像無しリスト出力
        
        /// <summary>
        /// 画像無しリスト
        /// 提供データは存在するが、国保連レセプトキーで紐付かない申請書を出力
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool NoneApplicationList(int cym, string strDir, WaitForm wf)
        {

            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" select d.* ");

            sb.AppendLine(" from dataimport_ahk d");
            sb.AppendLine(" left join application a on ");
            sb.AppendLine(" d.comnum=a.comnum");

            sb.AppendLine($"where d.cym={cym} ");
            sb.AppendLine(" and a.aid is null ");


            string strsql = sb.ToString();


            DB.Command cmd = new DB.Command(DB.Main, strsql);
            var lst = cmd.TryExecuteReaderList();

            //無い場合は出さない            
            if (lst.Count == 0) return true;


            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;

            StringBuilder sbNoneApp = new StringBuilder();
            foreach (var item in lst)
            {
                string strNoneApp = string.Empty;
                for (int c = 0; c < item.Length; c++)
                {
                    strNoneApp += $"{item[c].ToString()},";
                }
                
                sbNoneApp.AppendLine(strNoneApp);
                wf.InvokeValue++;
            }

            string strListFilename = $"{strDir}\\メディブレーン確認用申請書画像なしリスト{cym}.csv";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strListFilename);
            try
            {
                sw.WriteLine("以下は提供データあり、申請書画像なしのリストです。各保険者処理にて使用ください");
                sw.WriteLine("プライマリキー","メホール上の処理年月 メホール管理用","保険者番号","証記号","証番号","受療者名","診療年月","機関コード","医療機関名","宛名番号","生年月日","性別","種別２","給付割合","審査年月","実日数","決定点数（金額）","請求金額","患者負担金","レセプト全国共通キー","郵便番号","住所","審査年月西暦","診療年月西暦","受療者生年月日西暦");
                sw.Write(sbNoneApp.ToString());
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = strListFilename;
                System.Diagnostics.Process.Start(si);
            }
            //20210604172050 furukawa ed ////////////////////////
        }

    }
}
