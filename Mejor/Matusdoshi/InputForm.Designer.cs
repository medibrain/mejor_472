﻿namespace Mejor.Matsudoshi
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.labelF1 = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelApptype = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.phnum = new System.Windows.Forms.Panel();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pDoui = new System.Windows.Forms.Panel();
            this.vbDouiG = new Mejor.VerifyBox();
            this.vbDouiD = new Mejor.VerifyBox();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDoui = new System.Windows.Forms.Label();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.label17 = new System.Windows.Forms.Label();
            this.verifyBoxHmark = new Mejor.VerifyBox();
            this.labelFusyoCnt = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.verifyBoxFushoCount = new Mejor.VerifyBox();
            this.cbZenkai = new Mejor.VerifyCheckBox();
            this.pZenkai = new System.Windows.Forms.Panel();
            this.verifyBoxZG = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxZM = new Mejor.VerifyBox();
            this.verifyBoxZY = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.phnum.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.pZenkai.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 753);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(209, 33);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(19, 13);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(140, 20);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(198, 18);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(43, 13);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(136, 33);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(31, 13);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 782);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 753);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 753);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-408, 753);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 753);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 782);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelApptype);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.phnum);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 782);
            this.panelRight.TabIndex = 0;
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.labelF1);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstM);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.label5);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstY);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.label12);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstE);
            this.panelTotal.Location = new System.Drawing.Point(7, 534);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(749, 104);
            this.panelTotal.TabIndex = 20;
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(61, 14);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF1.TabIndex = 30;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(12, 21);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(49, 13);
            this.labelF1.TabIndex = 258;
            this.labelF1.Text = "負傷名1";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(582, 58);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 65;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxTotal.Validated += new System.EventHandler(this.verifyBoxTotal_Validated);
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(434, 58);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisitKasan.TabIndex = 56;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(485, 14);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 46;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(545, 66);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 37;
            this.labelTotal.Text = "合計";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(321, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "初検日";
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(435, 14);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 44;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(340, 58);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisit.TabIndex = 55;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(368, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 36);
            this.label1.TabIndex = 40;
            this.label1.Text = "3:昭\r\n4:平\r\n5:令";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(463, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "年";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(514, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "月";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(397, 14);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstE.TabIndex = 40;
            this.verifyBoxF1FirstE.TextV = "";
            this.verifyBoxF1FirstE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(170, 21);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(920, 22);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(86, 13);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(5, 642);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 87);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 150;
            this.dgv.TabStop = false;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(5, 80);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 450);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelApptype
            // 
            this.labelApptype.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelApptype.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelApptype.Location = new System.Drawing.Point(3, 3);
            this.labelApptype.Name = "labelApptype";
            this.labelApptype.Size = new System.Drawing.Size(119, 80);
            this.labelApptype.TabIndex = 257;
            this.labelApptype.Text = "続紙    : \"--\"\r\n不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  : \"911\"\r\n状態記入書  " +
    ": \"921\"";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // phnum
            // 
            this.phnum.Controls.Add(this.verifyBoxHnum);
            this.phnum.Controls.Add(this.verifyBoxM);
            this.phnum.Controls.Add(this.labelHnum);
            this.phnum.Controls.Add(this.labelM);
            this.phnum.Location = new System.Drawing.Point(146, 11);
            this.phnum.Name = "phnum";
            this.phnum.Size = new System.Drawing.Size(848, 70);
            this.phnum.TabIndex = 5;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(253, 10);
            this.verifyBoxHnum.MaxLength = 16;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(105, 10);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pDoui);
            this.panel1.Controls.Add(this.verifyBoxFamily);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.verifyBoxHmark);
            this.panel1.Controls.Add(this.labelFusyoCnt);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.verifyBoxFushoCount);
            this.panel1.Controls.Add(this.cbZenkai);
            this.panel1.Controls.Add(this.pZenkai);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(94, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(900, 212);
            this.panel1.TabIndex = 258;
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Controls.Add(this.label8);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Location = new System.Drawing.Point(172, 159);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(300, 50);
            this.pDoui.TabIndex = 170;
            this.pDoui.Enter += new System.EventHandler(this.item_Enter);
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(100, 5);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(247, 5);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(195, 5);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(141, 5);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(55, 5);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 26);
            this.label8.TabIndex = 84;
            this.label8.Text = "4:平成\r\n5:令和";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Enabled = false;
            this.lblDoui.Location = new System.Drawing.Point(4, 5);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(43, 26);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(225, 15);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(19, 13);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(278, 15);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(19, 13);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(173, 15);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(19, 13);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(609, 74);
            this.verifyBoxFamily.MaxLength = 1;
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 12;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Visible = false;
            this.verifyBoxFamily.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label17.Location = new System.Drawing.Point(35, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(493, 13);
            this.label17.TabIndex = 268;
            this.label17.Text = "鍼灸時：　　1.神経痛　2.リウマチ　3.頚腕症候群　4.五十肩　5.腰痛症　6.頸椎捻挫後遺症　7.その他";
            // 
            // verifyBoxHmark
            // 
            this.verifyBoxHmark.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHmark.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHmark.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHmark.Location = new System.Drawing.Point(149, 78);
            this.verifyBoxHmark.MaxLength = 8;
            this.verifyBoxHmark.Name = "verifyBoxHmark";
            this.verifyBoxHmark.NewLine = false;
            this.verifyBoxHmark.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxHmark.TabIndex = 10;
            this.verifyBoxHmark.TextV = "";
            this.verifyBoxHmark.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelFusyoCnt
            // 
            this.labelFusyoCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFusyoCnt.AutoSize = true;
            this.labelFusyoCnt.Location = new System.Drawing.Point(560, 119);
            this.labelFusyoCnt.Name = "labelFusyoCnt";
            this.labelFusyoCnt.Size = new System.Drawing.Size(43, 13);
            this.labelFusyoCnt.TabIndex = 73;
            this.labelFusyoCnt.Text = "負傷数";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(544, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 74;
            this.label7.Text = "本人/家族";
            this.label7.Visible = false;
            // 
            // verifyBoxFushoCount
            // 
            this.verifyBoxFushoCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFushoCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFushoCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFushoCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFushoCount.Location = new System.Drawing.Point(609, 111);
            this.verifyBoxFushoCount.MaxLength = 2;
            this.verifyBoxFushoCount.Name = "verifyBoxFushoCount";
            this.verifyBoxFushoCount.NewLine = false;
            this.verifyBoxFushoCount.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxFushoCount.TabIndex = 32;
            this.verifyBoxFushoCount.TextV = "";
            this.verifyBoxFushoCount.Enter += new System.EventHandler(this.item_Enter);
            // 
            // cbZenkai
            // 
            this.cbZenkai.BackColor = System.Drawing.SystemColors.Info;
            this.cbZenkai.CheckedV = false;
            this.cbZenkai.Location = new System.Drawing.Point(377, 61);
            this.cbZenkai.Name = "cbZenkai";
            this.cbZenkai.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.cbZenkai.Size = new System.Drawing.Size(95, 27);
            this.cbZenkai.TabIndex = 84;
            this.cbZenkai.Text = "交付料あり";
            this.cbZenkai.UseVisualStyleBackColor = false;
            this.cbZenkai.CheckedChanged += new System.EventHandler(this.cbZenkai_CheckedChanged);
            this.cbZenkai.Enter += new System.EventHandler(this.item_Enter);
            // 
            // pZenkai
            // 
            this.pZenkai.Controls.Add(this.verifyBoxZG);
            this.pZenkai.Controls.Add(this.label23);
            this.pZenkai.Controls.Add(this.label24);
            this.pZenkai.Controls.Add(this.label25);
            this.pZenkai.Controls.Add(this.label26);
            this.pZenkai.Controls.Add(this.verifyBoxZM);
            this.pZenkai.Controls.Add(this.verifyBoxZY);
            this.pZenkai.Location = new System.Drawing.Point(230, 94);
            this.pZenkai.Name = "pZenkai";
            this.pZenkai.Size = new System.Drawing.Size(270, 50);
            this.pZenkai.TabIndex = 85;
            this.pZenkai.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxZG
            // 
            this.verifyBoxZG.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZG.Location = new System.Drawing.Point(107, 7);
            this.verifyBoxZG.MaxLength = 2;
            this.verifyBoxZG.Name = "verifyBoxZG";
            this.verifyBoxZG.NewLine = false;
            this.verifyBoxZG.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZG.TabIndex = 51;
            this.verifyBoxZG.TextV = "";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label23.Location = new System.Drawing.Point(65, 5);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 26);
            this.label23.TabIndex = 83;
            this.label23.Text = "4:平成\r\n5:令和";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Enabled = false;
            this.label24.Location = new System.Drawing.Point(4, 12);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 13);
            this.label24.TabIndex = 39;
            this.label24.Text = "前回支給";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(232, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "月";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(178, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(19, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "年";
            // 
            // verifyBoxZM
            // 
            this.verifyBoxZM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZM.Location = new System.Drawing.Point(201, 7);
            this.verifyBoxZM.MaxLength = 2;
            this.verifyBoxZM.Name = "verifyBoxZM";
            this.verifyBoxZM.NewLine = false;
            this.verifyBoxZM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZM.TabIndex = 53;
            this.verifyBoxZM.TextV = "";
            // 
            // verifyBoxZY
            // 
            this.verifyBoxZY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZY.Location = new System.Drawing.Point(150, 7);
            this.verifyBoxZY.MaxLength = 2;
            this.verifyBoxZY.Name = "verifyBoxZY";
            this.verifyBoxZY.NewLine = false;
            this.verifyBoxZY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZY.TabIndex = 52;
            this.verifyBoxZY.TextV = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(636, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 26);
            this.label2.TabIndex = 76;
            this.label2.Text = "本人:2\r\n家族:6";
            this.label2.Visible = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 782);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 782);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 820);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.phnum.ResumeLayout(false);
            this.phnum.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.pZenkai.ResumeLayout(false);
            this.pZenkai.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
        private System.Windows.Forms.Panel pDoui;
        private System.Windows.Forms.Label label8;
        private VerifyBox vbDouiG;
        private System.Windows.Forms.Label lblDoui;
        private VerifyBox vbDouiD;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private VerifyCheckBox cbZenkai;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelApptype;
        private System.Windows.Forms.Label labelF1;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label label17;
        private VerifyBox verifyBoxHmark;
        private System.Windows.Forms.Panel phnum;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pZenkai;
        private VerifyBox verifyBoxZG;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxZM;
        private VerifyBox verifyBoxZY;
        private System.Windows.Forms.Label labelFusyoCnt;
        private VerifyBox verifyBoxFushoCount;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelTotal;
    }
}