﻿namespace Mejor
{
    partial class ExportMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.buttonSubmitExport = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(227, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(183, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "---";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonSubmitExport
            // 
            this.buttonSubmitExport.BackColor = System.Drawing.Color.Tomato;
            this.buttonSubmitExport.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonSubmitExport.Location = new System.Drawing.Point(38, 18);
            this.buttonSubmitExport.Name = "buttonSubmitExport";
            this.buttonSubmitExport.Size = new System.Drawing.Size(183, 44);
            this.buttonSubmitExport.TabIndex = 0;
            this.buttonSubmitExport.Text = "出力";
            this.buttonSubmitExport.UseVisualStyleBackColor = false;
            this.buttonSubmitExport.Click += new System.EventHandler(this.buttonSubmitExport_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(227, 168);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(183, 44);
            this.button8.TabIndex = 7;
            this.button8.Text = "----";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(227, 68);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(183, 44);
            this.button4.TabIndex = 3;
            this.button4.Text = "----";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(227, 118);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(183, 44);
            this.button6.TabIndex = 5;
            this.button6.Text = "----";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(38, 168);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(183, 44);
            this.button7.TabIndex = 6;
            this.button7.Text = "----";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(38, 118);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(183, 44);
            this.button5.TabIndex = 4;
            this.button5.Text = "----";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(38, 68);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(183, 44);
            this.button3.TabIndex = 2;
            this.button3.Text = "---";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ExportMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 231);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonSubmitExport);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Name = "ExportMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "提出データ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonSubmitExport;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
    }
}