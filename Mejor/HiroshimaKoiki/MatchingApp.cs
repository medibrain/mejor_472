﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.HiroshimaKoiki
{
    class MatchingApp
    {
        public static List<App> GetNotMatchApp(int cym)
        {
            var where = "FROM application AS a " +
                "LEFT OUTER JOIN refrece AS k ON a.aid = k.aid " +
                "WHERE k.aid IS NULL " +
                $"AND a.cym={cym} " +
                "AND (a.ayear>0 OR a.ayear=-999)";

            return App.InspectSelect(where);
        }

        public static List<App> GetOverlapApp(int cym)
        {
            var where = "FROM application AS a " +
                "WHERE a.rrid IN( " +
                    "SELECT a2.rrid FROM application AS a2 " +
                    $"WHERE a2.cym={cym} " +
                    "AND a2.ayear>0 " +
                    "GROUP BY a2.rrid HAVING COUNT(a2.rrid) > 1) ";

            return App.InspectSelect(where);
        }
    }
}
