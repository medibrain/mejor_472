﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.HiroshimaKoiki
{
    class RefRece : RefReceCore
    {
        public string Kana { get; private set; } = string.Empty;
        public string Zip { get; private set; } = string.Empty;
        public string Add { get; private set; } = string.Empty;
        public string DestName { get; private set; } = string.Empty;
        public string DestZip { get; private set; } = string.Empty;
        public string DestAdd { get; private set; } = string.Empty;
        public string ClinicZip { get; private set; } = string.Empty;
        public string ClinicAdd { get; private set; } = string.Empty;

        public static bool Import(string fileName, APP_TYPE appType)
        {
            var wf = new WaitForm();
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
            wf.ShowDialogOtherTask();
            wf.LogPrint("インポートを開始します");

            wf.LogPrint("CSVファイルを読み込み中です…");
            var l = CommonTool.CsvImportUTF8(fileName);

            wf.LogPrint("データベースへ書き込み中です…");
            wf.SetMax(l.Count - 1);

            //0.被保険者番号,1.被保険者氏名,2.被保険者氏名（カナ）,3.郵便番号,4.住所,
            //5.送付先郵便番号,6.送付先住所,7.送付先氏名,8.医療機関番号,9.医療機関名（漢字）,
            //10.代表者名（漢字）,11.診療年月,12.請求年月,13.診療実日数,14.費用額,
            //15.電算管理番号,16.医療機関郵便番号,17.医療機関住所

            var ip = GetNextImport();
            using (var tran = DB.Main.CreateTransaction())
            {
                int.TryParse(l[1][12], out int ipcym);
                ipcym = DateTimeEx.Int6YmAddMonth(ipcym, -1);
                ip.AppType = appType;
                ip.ReceCount = l.Count - 1;
                ip.CYM = ipcym;
                ip.FileName = fileName;
                ip.Insert(tran);

                try
                {
                    //取込開始 1行目はヘッダ
                    for (int i = 1; i < l.Count; i++)
                    {
                        wf.InvokeValue = i;
                        if (l[i].Length < 17) continue;

                        int.TryParse(l[i][11], out int mym);
                        int.TryParse(l[i][12], out int cym);
                        int.TryParse(l[i][13], out int days);
                        int.TryParse(l[i][14], out int total);
                        cym = DateTimeEx.Int6YmAddMonth(cym, -1);

                        var rr = new RefRece();
                        rr.ImportID = ip.ImportID;
                        rr.AppType = appType;
                        rr.Num = l[i][0];
                        rr.Name = l[i][1];
                        rr.Kana = l[i][2];
                        rr.Zip = l[i][3];
                        rr.Add = l[i][4];
                        rr.DestZip = l[i][5];
                        rr.DestAdd = l[i][6];
                        rr.DestName = l[i][7];
                        rr.ClinicNum = l[i][8];
                        rr.ClinicName = l[i][9];
                        rr.DrName = l[i][10];
                        rr.MYM = mym;
                        rr.CYM = cym;
                        rr.Days = days;
                        rr.Total = total;
                        rr.SearchNum = l[i][15];
                        rr.ClinicZip = l[i][16];
                        rr.ClinicAdd = l[i][17];
                        if (!DB.Main.Insert(rr, tran))
                        {
                            System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                    return false;
                }
                finally
                {
                    wf.Dispose();
                }

                tran.Commit();
            }

            System.Windows.Forms.MessageBox.Show("インポートが完了しました");
            return true;
        }

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="mym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cym, int mym, string num, int total) =>
            DB.Main.Select<RefRece>(new { num, mym, total, cym }).ToList();

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cym, string num, int total) =>
            DB.Main.Select<RefRece>(new { cym, num, total }).ToList();

        public static List<RefRece> SelectAll(int cym) =>
            DB.Main.Select<RefRece>(new { cym }).ToList();

        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cym) =>
            DB.Main.Select<RefRece>(new { cym = cym, aid = 0 }).ToList();

        public static RefRece Select(int rrid) =>
            DB.Main.Select<RefRece>(new { rrid }).FirstOrDefault();

        public static bool AfterAddRefData(int cym)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書情報の取得中です");

                var apps = App.GetApps(cym);

                wf.LogPrint("参照情報の取得中です");
                var rrs = RefRece.SelectAll(cym);
                var rrsDic = new Dictionary<int, RefRece>();
                rrs.ForEach(r => rrsDic.Add(r.RrID, r));

                wf.LogPrint("申請書情報の更新中です");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(apps.Count);

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var app in apps)
                    {
                        wf.InvokeValue++;
                        if (!rrsDic.ContainsKey(app.RrID)) continue;
                        var rr = rrsDic[app.RrID];

                        app.PersonName = rr.Name;
                        app.ClinicName = rr.ClinicName;
                        app.ClinicNum = rr.ClinicNum;
                        app.CountedDays = rr.Days;
                        app.DrName = rr.DrName;
                        app.ClinicAdd = rr.ClinicAdd;
                        app.ClinicZip = rr.ClinicZip;
                        app.HihoZip = rr.DestZip == "" ? rr.Zip : rr.DestZip;
                        app.HihoAdd = rr.DestAdd == "" ? rr.Add : rr.DestAdd;
                        app.ComNum = rr.SearchNum;
                        app.AppType = rr.AppType;
                        //app.RrID = rr.RrID;
                        app.TaggedDatas.DestName = rr.DestName;
                        app.TaggedDatas.Kana = rr.Kana;

                        if (!app.Update(0, App.UPDATE_TYPE.Null, tran)) return false;
                    }
                    tran.Commit();
                }
            }
            return true;
            
        }
    }
}
