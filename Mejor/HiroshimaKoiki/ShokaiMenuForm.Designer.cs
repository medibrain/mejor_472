﻿namespace Mejor.HiroshimaKoiki
{
    partial class ShokaiMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShokaiCSV = new System.Windows.Forms.Button();
            this.buttonShokaiImage = new System.Windows.Forms.Button();
            this.buttonInput = new System.Windows.Forms.Button();
            this.buttonMihenshin = new System.Windows.Forms.Button();
            this.buttonRelation = new System.Windows.Forms.Button();
            this.buttonBarcode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonShokaiCSV
            // 
            this.buttonShokaiCSV.Location = new System.Drawing.Point(37, 21);
            this.buttonShokaiCSV.Name = "buttonShokaiCSV";
            this.buttonShokaiCSV.Size = new System.Drawing.Size(183, 44);
            this.buttonShokaiCSV.TabIndex = 0;
            this.buttonShokaiCSV.Text = "文書照会情報CSV取り込み";
            this.buttonShokaiCSV.UseVisualStyleBackColor = true;
            this.buttonShokaiCSV.Click += new System.EventHandler(this.buttonShokaiCSV_Click);
            // 
            // buttonShokaiImage
            // 
            this.buttonShokaiImage.Location = new System.Drawing.Point(37, 71);
            this.buttonShokaiImage.Name = "buttonShokaiImage";
            this.buttonShokaiImage.Size = new System.Drawing.Size(183, 44);
            this.buttonShokaiImage.TabIndex = 1;
            this.buttonShokaiImage.Text = "照会文書画像取り込み";
            this.buttonShokaiImage.UseVisualStyleBackColor = true;
            this.buttonShokaiImage.Click += new System.EventHandler(this.buttonShokaiImage_Click);
            // 
            // buttonInput
            // 
            this.buttonInput.Location = new System.Drawing.Point(226, 71);
            this.buttonInput.Name = "buttonInput";
            this.buttonInput.Size = new System.Drawing.Size(183, 44);
            this.buttonInput.TabIndex = 4;
            this.buttonInput.Text = "照会結果入力";
            this.buttonInput.UseVisualStyleBackColor = true;
            this.buttonInput.Click += new System.EventHandler(this.buttonInput_Click);
            // 
            // buttonMihenshin
            // 
            this.buttonMihenshin.Location = new System.Drawing.Point(226, 121);
            this.buttonMihenshin.Name = "buttonMihenshin";
            this.buttonMihenshin.Size = new System.Drawing.Size(183, 44);
            this.buttonMihenshin.TabIndex = 5;
            this.buttonMihenshin.Text = "未返信確定処理";
            this.buttonMihenshin.UseVisualStyleBackColor = true;
            this.buttonMihenshin.Click += new System.EventHandler(this.buttonMihenshin_Click);
            // 
            // buttonRelation
            // 
            this.buttonRelation.Location = new System.Drawing.Point(226, 21);
            this.buttonRelation.Name = "buttonRelation";
            this.buttonRelation.Size = new System.Drawing.Size(183, 44);
            this.buttonRelation.TabIndex = 3;
            this.buttonRelation.Text = "申請書との関連付け";
            this.buttonRelation.UseVisualStyleBackColor = true;
            this.buttonRelation.Click += new System.EventHandler(this.buttonRelation_Click);
            // 
            // buttonBarcode
            // 
            this.buttonBarcode.Location = new System.Drawing.Point(37, 121);
            this.buttonBarcode.Name = "buttonBarcode";
            this.buttonBarcode.Size = new System.Drawing.Size(183, 44);
            this.buttonBarcode.TabIndex = 2;
            this.buttonBarcode.Text = "バーコード読み取り確認";
            this.buttonBarcode.UseVisualStyleBackColor = true;
            this.buttonBarcode.Click += new System.EventHandler(this.buttonBarcode_Click);
            // 
            // ShokaiMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 186);
            this.Controls.Add(this.buttonMihenshin);
            this.Controls.Add(this.buttonRelation);
            this.Controls.Add(this.buttonInput);
            this.Controls.Add(this.buttonBarcode);
            this.Controls.Add(this.buttonShokaiImage);
            this.Controls.Add(this.buttonShokaiCSV);
            this.Name = "ShokaiMenuForm";
            this.Text = "OptionsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonShokaiCSV;
        private System.Windows.Forms.Button buttonShokaiImage;
        private System.Windows.Forms.Button buttonInput;
        private System.Windows.Forms.Button buttonMihenshin;
        private System.Windows.Forms.Button buttonRelation;
        private System.Windows.Forms.Button buttonBarcode;
    }
}