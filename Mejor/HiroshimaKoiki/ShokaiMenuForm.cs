﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HiroshimaKoiki
{
    public partial class ShokaiMenuForm : Form
    {
        int cym;

        public ShokaiMenuForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.Text = "オプション機能" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年{ cym % 100}月分";
            //this.Text = "オプション機能" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100)}年{ cym % 100}月分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        }

        private void buttonShokaiCSV_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("広島県広域連合の文書照会情報を読み込みます。よろしいですか？", "文書照会情報取得",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK) return;

            //広島広域　通知対象者の読み込み機能、暫定処置
            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                if (Shokai.ShokaiData.Import(f.FileName))
                {
                    MessageBox.Show("照会情報を読み込みました");
                }
                else
                {
                    MessageBox.Show("照会情報の読み込みに失敗しました", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void buttonShokaiImage_Click(object sender, EventArgs e)
        {
            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return;
                var rect = new Rectangle(1900, 2400, 500, 1050);
                Shokai.ShokaiImage.Import(f.SelectedPath, false, Shokai.SHOKAI_IMAGE_STATUS.NULL, rect, ZXing.BarcodeFormat.QR_CODE);
            }
        }

        private void buttonInput_Click(object sender, EventArgs e)
        {
            using (var f = new Shokai.ShokaiResultForm(cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonMihenshin_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("現在返信データがない照会を未返信として登録します。よろしいですか？", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == System.Windows.Forms.DialogResult.OK) return;

            if (Shokai.ShokaiData.SetNothingImage(cym))
            {
                MessageBox.Show("未返信の確定を行ないました");
            }
            else
            {
                MessageBox.Show("未返信の確定に失敗しました");
            }
        }

        private void buttonBarcode_Click(object sender, EventArgs e)
        {
            var l = Shokai.ShokaiImage.GetNotReadBarcodeImages();
            if (l.Count == 0)
            {
                MessageBox.Show("バーコードが確定していない画像はありません。");
                return;
            }

            var res = MessageBox.Show("バーコードが確定していない画像が" + l.Count.ToString() +
                "枚見つかりました。確定処理に移りますか？", "バーコード確定処理",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (res != DialogResult.OK) return;
            using (var f = new Shokai.ShokaiBarcodeInputForm())
            {
                f.ShowDialog();
            }
        }

        private void buttonRelation_Click(object sender, EventArgs e)
        {
            if (Shokai.ShokaiImage.UpdateAID())
            {
                MessageBox.Show("AIDの確定が終了しました。");
            }
            else
            {
                MessageBox.Show("AIDの確定に失敗しました。");
            }
        }
    }
}
