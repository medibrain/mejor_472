﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mejor.HiroshimaKoiki
{
    class Export
    {
        public static bool ExportViewerData(int cym)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(fileName);
            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                var dic = new Dictionary<int, RefRece>();
                rrs.ForEach(rr => { if (rr.AID != 0) dic.Add(rr.AID, rr); });

                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];
                        if (!dic.ContainsKey(app.Aid))
                        {
                            log += $"突合データがない申請書があります AID:{app.Aid}\r\n";
                            for (i++; i < apps.Count; i++) if (apps[i].YM > 0) break;
                            continue;
                        }

                        var vd = ViewerData.CreateViewerData(app);

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.同意書 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.往療内訳 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.長期)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                if (!ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return false;

                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nビューアデータ出力数   :{receCount}";
                log += lastMsg;
                log += $"{DateTime.Now}\r\n更新データの出力を終了しました。";

                //更新データ
                wf.LogPrint("更新データの作成中です");
                log += "\r\n\r\n";

                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6),cym,dir, wf))
                {
                    log += $"\r\n\r\n{DateTime.Now}\r\n更新データの出力に失敗しました。";
                    return false;
                }

                log += $"{DateTime.Now}\r\n更新データの出力を終了しました。";
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }
    }
}
