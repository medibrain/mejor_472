﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.HiroshimaKoiki
{
    class ExportOld
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,照会ID,記号番号,被保険者名,施術年,施術月,実日数,合計,施術所番号,施術所名,宛名氏名,郵便番号,住所,電算番号";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    string insNoStr = ((int)InsurerID.HIROSHIMA_KOIKI).ToString();
                    foreach (var item in list)
                    {
                        var kd = RefRece.Select(int.Parse(item.Numbering));
                        if (kd == null)
                        {
                            throw new Exception("広域からのデータがない、またはOCR確認が済んでいない申請書が照会対象として指定されています" +
                            "\r\nID:" + item.Aid.ToString());
                        }

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(insNoStr + ymStr + no.ToString("0000"));
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(kd.Name);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(kd.ClinicName);
                        ss.Add(kd.DestName == string.Empty ? kd.Name : kd.DestName);
                        ss.Add(kd.DestAdd == string.Empty ? kd.Zip : kd.DestZip);
                        ss.Add(kd.DestAdd == string.Empty ? kd.Add : kd.DestAdd);
                        ss.Add(kd.SearchNum);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
