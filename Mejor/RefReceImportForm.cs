﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
    public partial class RefReceImportForm : Form
    {
        int cym;
        BindingSource bs = new BindingSource();
        public RefReceImportForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            getList();

            dataGridView1.DataSource = bs;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ImportDate)].DefaultCellStyle.Format = "yyyy/MM/dd";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.CYM)].DefaultCellStyle.Format = "0000/00";

            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.AppType)].HeaderText = "種類";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.CYM)].HeaderText = "処理月";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ImportDate)].HeaderText = "作業日";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ImportID)].HeaderText = "ID";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ReceCount)].HeaderText = "件数";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.UserID)].Visible = false;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.UserName)].HeaderText = "作業ユーザー";
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.FileName)].HeaderText = "ファイル名";

            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.AppType)].Width = 60;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.CYM)].Width = 70;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ImportDate)].Width = 80;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ImportID)].Width = 50;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.ReceCount)].Width = 60;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.UserName)].Width = 120;
            dataGridView1.Columns[nameof(RefReceCore.RefReceImport.FileName)].Width = 340;
        }

        private void getList()
        {
            var l = RefReceCore.GetImportList();
            l.Sort((x, y) => y.ImportID.CompareTo(x.ImportID));
            bs.DataSource = l;
            bs.ResetBindings(false);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            var ins = Insurer.CurrrentInsurer.EnumInsID;
            switch (ins)
            {
                case InsurerID.HIROSHIMA_KOIKI:
                    using (var f = new HiroshimaKoiki.ImportForm()) f.ShowDialog();
                    break;
                case InsurerID.OSAKA_KOIKI:
                    using (var f = new OsakaKoiki.ImportForm(cym)) f.ShowDialog();
                    break;
                case InsurerID.HIGASHIOSAKA_KOKUHO:
                    HigashiOsakaKokuho.RefRece.Import();
                    break;

                //20190406121055 furukawa st ////////////////////////
                //兵庫県後期広域追加（あはきもあるので大阪広域と同じにしておく）
                case InsurerID.HYOGO_KOIKI:

                    //20220325143639 furukawa st ////////////////////////
                    //cym追加
                    
                    using (var f = new HyogoKoiki.ImportForm2(cym)) f.ShowDialog();
                    //      using (var f = new HyogoKoiki.ImportForm2()) f.ShowDialog();
                    //20220325143639 furukawa ed ////////////////////////

                    //using (var f = new HyogoKoiki.ImportForm(cym)) f.ShowDialog();
                    break;
                //20190406121055 furukawa ed ////////////////////////

                //20220312132718 furukawa st ////////////////////////
                //広島広域2022追加                
                case InsurerID.HIROSHIMA_KOIKI2022:
                    using (var f = new HiroshimaKoiki2022.ImportForm()) f.ShowDialog();                    
                    break;
                //20220312132718 furukawa ed ////////////////////////


                default:
                    MessageBox.Show("対応していない保険者です");
                    break;

            }
            getList();
        }

        private void 選択中のインポートを削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rri = (RefReceCore.RefReceImport)bs.Current;
            if (rri == null) return;
            var res = MessageBox.Show("この削除ができるのは、自動マッチング、および対象処理月の入力を開始していない場合に限られます！" +
                "\r\n\r\n" +
                $"選択中のインポート\r\n" +
                $"ID:{rri.ImportID}  処理月:{rri.CYM.ToString("0000/00")}\r\n" +
                $"のデータをすべて削除ます。よろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (res != DialogResult.OK) return;

            res = MessageBox.Show($"ID:{rri.ImportID}  処理月:{rri.CYM.ToString("0000/00")}\r\n" +
                $"のデータをすべて削除ます。元には戻せませんが本当によろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (res != DialogResult.OK) return;

            if (RefReceCore.DeleteImport(rri.ImportID))
            {
                MessageBox.Show("削除しました");
                getList();
            }
            else
            {
                MessageBox.Show("削除に失敗しました");
            }
        }
    }
}
