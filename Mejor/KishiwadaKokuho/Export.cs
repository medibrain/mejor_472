﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;

namespace Mejor.KishiwadaKokuho
{
    class Export
    {
        public static bool ExportViewerData(int cym)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(fileName);
            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                wf.LogPrint("参照データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                rrs = rrs.FindAll(r => r.AppType == APP_TYPE.鍼灸 || r.AppType == APP_TYPE.あんま);
                var hs = new HashSet<int>();
                var rrsDic = new Dictionary<int, RefRece>();
                rrs.ForEach(rr => rrsDic.Add(rr.RrID, rr));

                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();

                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];

                        if (app.AppType != APP_TYPE.あんま && app.AppType != APP_TYPE.鍼灸)
                        {
                            i++;
                            continue;
                        }

                        var v = ViewerData.CreateViewerData(app);
                        sw.WriteLine(v.CreateCsvLine());
                        hs.Add(app.RrID);

                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                if (!ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return false;

                //マッチングなしデータ作成
                wf.LogPrint("マッチングなしデータを作成しています");
                var notMatchCsv = dir + "\\" + cym.ToString() + "マッチなしデータ.csv";
                using (var sw = new StreamWriter(notMatchCsv, false, Encoding.UTF8))
                {
                    sw.WriteLine(RefRece.Header);
                    foreach (var item in rrs)
                    {
                        if (hs.Contains(item.RrID)) continue;
                        sw.WriteLine(item.CsvLine);
                    }
                }

                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\nビューアデータ出力数   :{receCount}";
                log += lastMsg;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }
    }
}
