﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NpgsqlTypes;

namespace Mejor.KishiwadaKokuho
{
    class RefRece
    {
        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int RrID { get; set; }
        public string HihoNum { get; set; } = string.Empty;
        public int CYM { get; set; }
        public int MYM { get; set; }
        public SEX Sex { get; set; } = SEX.Null;
        public DateTime Birth { get; set; } = DateTimeEx.DateTimeNull;
        public string ClinicNum { get; set; } = string.Empty;
        public int Total { get; set; }
        public int Charge { get; set; }
        public int Days { get; set; }
        public string SickCode { get; set; } = string.Empty;
        public APP_TYPE AppType { get; set; } = APP_TYPE.NULL;
        public int AID { get; set; }

        [DB.DbAttribute.Ignore]
        public string BirthJ => DateTimeEx.GetShortJpDateStr(Birth);
        
        [DB.DbAttribute.Ignore]
        public static string Header =>
            "保険証番号,診療年月,性別名称,生年月日,診療実日数," +
            "医療機関コード,支払額,傷病コード,療養費種別";

        [DB.DbAttribute.Ignore]
        public string CsvLine =>
            $"{HihoNum}," +
            $"{CYM.ToString()}," +
            $"{Sex.ToString()}," +
            $"{Birth.ToString("yyyyMMdd")}," +
            $"{Days}," +
            $"{ClinicNum}," +
            $"{Charge}," +
            $"{SickCode}," +
            $"{AppType.ToString()}";

        //public static Clinic GetClinic(string clinicNum) =>
        //   DB.Main.Select<Clinic>(new { clinicNum }).FirstOrDefault();

        public static bool Import(int cym)
        {
            string fn;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "Excelファイル|*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fn = f.FileName;
            }

            using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open))
            {
                if (System.IO.Path.GetExtension(fn) == ".xlsx")
                {
                    var xls = new XSSFWorkbook(fs);

                    var sheet = xls.GetSheetAt(0);
                    var rowCount = sheet.LastRowNum + 1;
                    var header = sheet.GetRow(0);

                    //柔整・あはきの判断
                    var c = header.GetCell(1);
                    if (c ==null || c.CellType != CellType.String)
                    {
                        MessageBox.Show("ファイル内容が判別できませんでした。");
                        return false;
                    }
                    else if(c.StringCellValue == "施術年月")
                    {
                        //柔整
                        if (MessageBox.Show($"岸和田市国保のデータを\r\n{fn}\r\n" +
                            $"より「柔整」{cym.ToString("0000年00月")}分として読み込みます。よろしいですか？",
                            "取り込み確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                            != DialogResult.OK) return true;

                        using (var tran = DB.Main.CreateTransaction())
                        {
                            for (int i = 1; i < rowCount; i++)
                            {
                                try
                                {
                                    var row = sheet.GetRow(i);
                                    if (row.GetCell(1).CellType == CellType.Blank)
                                        continue;

                                    var rr = new RefRece();
                                    var sex = row.GetCell(2).StringCellValue.Trim();
                                    var g = int.Parse(row.GetCell(3).StringCellValue.Trim());
                                    var y = int.Parse(row.GetCell(4).StringCellValue.Trim());
                                    var m = int.Parse(row.GetCell(5).StringCellValue.Trim());
                                    var d = int.Parse(row.GetCell(6).StringCellValue.Trim());
                                    rr.HihoNum = row.GetCell(0).StringCellValue.Trim().PadLeft(7, '0');
                                    rr.MYM = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(row.GetCell(1).StringCellValue.Trim()));
                                    rr.Sex = sex == "1" ? SEX.男 : sex == "2" ? SEX.女 : SEX.Null;
                                    rr.Birth = DateTimeEx.GetDateFromJInt7(g * 1000000 + y * 10000 + m * 100 + d);
                                    rr.ClinicNum = row.GetCell(7).StringCellValue.Trim();
                                    rr.Total = (int)row.GetCell(8).NumericCellValue;
                                    rr.Charge = 0;
                                    rr.CYM = cym;
                                    rr.Days = 0;
                                    rr.SickCode = string.Empty;
                                    rr.AppType = APP_TYPE.柔整;
                                    DB.Main.Insert(rr, tran);
                                }
                                catch (Exception ex)
                                {
                                    Log.ErrorWriteWithMsg(ex);
                                    MessageBox.Show("インポートに失敗しました");
                                    return false;
                                }
                            }
                            tran.Commit();
                        }
                    }
                    else if (c.StringCellValue == "診療年月")
                    {
                        //あはき
                        if (MessageBox.Show($"岸和田市国保のデータを\r\n{fn}\r\n" +
                            $"より「あはき」{cym.ToString("0000年00月")}分として読み込みます。よろしいですか？",
                            "取り込み確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                            != DialogResult.OK) return true;

                        using (var tran = DB.Main.CreateTransaction())
                        {
                            for (int i = 1; i < rowCount; i++)
                            {
                                try
                                {
                                    var row = sheet.GetRow(i);
                                    if (row.GetCell(1).CellType == CellType.Blank)
                                        continue;

                                    var rr = new RefRece();
                                    var sex = row.GetCell(2).StringCellValue.Trim();
                                    var birth = (int)row.GetCell(3).NumericCellValue;
                                    var atype = row.GetCell(10).CellType == CellType.String ?
                                        row.GetCell(10).StringCellValue : string.Empty;
                                    rr.HihoNum = ((int)row.GetCell(0).NumericCellValue).ToString().PadLeft(7, '0');
                                    rr.MYM = (int)row.GetCell(1).NumericCellValue;
                                    rr.Sex = sex == "男" ? SEX.男 : sex == "女" ? SEX.女 : SEX.Null;
                                    rr.Birth = new DateTime(birth / 10000, birth / 100 % 100, birth % 100);
                                    rr.Days = (int)row.GetCell(4).NumericCellValue;
                                    rr.ClinicNum = ((int)row.GetCell(5).NumericCellValue).ToString("00") +
                                         ((int)row.GetCell(7).NumericCellValue).ToString().PadLeft(7, '0');
                                    rr.Total = 0;
                                    rr.Charge = (int)row.GetCell(8).NumericCellValue;
                                    rr.SickCode = row.GetCell(9) != null && row.GetCell(9).CellType == CellType.Numeric ?
                                        ((int)row.GetCell(9).NumericCellValue).ToString() : "";
                                    rr.AppType = atype == "マッサージ" ? APP_TYPE.あんま :
                                        atype == "鍼灸" ? APP_TYPE.鍼灸 : APP_TYPE.NULL;
                                    rr.CYM = cym;
                                    DB.Main.Insert(rr, tran);
                            }
                                catch (Exception ex)
                            {
                                Log.ErrorWriteWithMsg(ex);
                                MessageBox.Show("インポートに失敗しました");
                                return false;
                            }
                        }
                            tran.Commit();
                        }
                    }
                    else
                    {
                        MessageBox.Show("ファイル内容が判別できませんでした。");
                        return false;
                    }

                }
                else
                {
                    MessageBox.Show(
                        "対応しているのはExcel2007ファイル形式(.xlsx)です", "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    return false;
                }

                MessageBox.Show("インポートが完了しました");
                return true;
            }

        }

        public static List<RefRece> SelectAll(int cym)
        {
            var l = DB.Main.Select<RefRece>($"cym={cym}");
            return l.ToList();
        }


        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInputTotal(int cym, int mym, string hihoNum, int total)
        {
            var l = DB.Main.Select<RefRece>(new { cym, mym, hihoNum, total });
            return l.ToList();
        }

        public static List<RefRece> SerchByInputCharge(int cym, int mym, string hihoNum, int charge)
        {
            var l = DB.Main.Select<RefRece>(new { cym, mym, hihoNum, charge });
            return l.ToList();
        }

        
        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cym, string hihoNum, int totalOrCharge)
        {
            var l = DB.Main.Select<RefRece>(
                $"cym={cym} AND hihonum='{hihoNum}' AND (total={totalOrCharge} OR charge={totalOrCharge})");
            return l.ToList();
        }

        public static List<RefRece> SerchForMatchingCharge(int cym, string hihoNum, int charege)
        {
            var l = DB.Main.Select<RefRece>(new { cym, hihoNum, charege });
            return l.ToList();
        }

        /// <summary>
        /// koikidataテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int rrid, int aid, DB.Transaction tran = null)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery()) return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }
    }
}
