﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.KishiwadaKokuho
{
    public partial class InputFormOld : InputFormCore
    {
        private BindingSource bsApp;
        private bool firstTime;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 60);
        Point posPerson = new Point(60, 540);
        Point posTotal = new Point(800, 2060);
        Point posBuiName = new Point(120, 780);
        Point posBuiDate = new Point(950, 780);
        Point posNewCont = new Point(950, 900);
        Point posVisit = new Point(120, 700);

        Control[] ymConts, hnumConts, personConts, totalConts, buiNameConts, buiDateConts, newContConts, visitConts;

        public InputFormOld(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymConts = new[] { verifyBoxY, verifyBoxM };
            hnumConts = new[] { verifyBoxNum, verifyBoxNum };
            personConts = new[] { verifyBoxName };
            totalConts = new[] { verifyBoxTotal, verifyBoxSeikyu };
            buiNameConts = new[] { verifyBoxF1 };
            buiDateConts = new[] { verifyBoxDays, };
            newContConts = new[] { verifyBoxNewCont };
            visitConts = new[] { verifyCheckBoxVisit, };

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        //フォーム表示時
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            
            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);


            /*
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            */
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //被保険者番号
            int num = verifyBoxNum.GetIntValue();
            setStatus(verifyBoxNum, num < 1);
            string hnum = verifyBoxNum.Text.Trim();

            //氏名
            var name = verifyBoxName.Text.Trim().Replace(" ","　");
            setStatus(verifyBoxName, string.IsNullOrWhiteSpace(name) || !name.Contains("　"));

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || total > 200000);

            //請求
            int seikyu = verifyBoxSeikyu.GetIntValue();
            setStatus(verifyBoxSeikyu, seikyu < 100 || total < seikyu);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //負傷
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //合計金額、請求金額のチェック　金額でのエラーがあればいったん登録中断
            var chargeCandidates = new int[4];
            chargeCandidates[0] = total * 7 / 10;
            chargeCandidates[1] = total * 8 / 10;
            chargeCandidates[2] = total * 9 / 10;
            chargeCandidates[3] = total;

            if (!chargeCandidates.Contains(seikyu))
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxSeikyu.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = hnum;
            app.PersonName = name;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = seikyu;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.Distance = verifyCheckBoxVisit.Checked ? 999 : 0;

            //申請書種別
            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
            app.AppType = appsg.note2 == "7" ? APP_TYPE.鍼灸 : appsg.note2 == "8" ? APP_TYPE.あんま : APP_TYPE.柔整;

            //負傷
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力者情報
            labelUser.Text = $"入力1：{User.GetUserName(app.Ufirst)}\r\n入力2：{User.GetUserName(app.Usecond)}";

            //App_Flagのチェック
            if (!firstTime)
            {
                setVerify(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setInputedApp(app);
                }
                else
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (appsg.AppType == APP_TYPE.柔整 && !string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        //OCRデータがあれば、部位のみ挿入
                        var ocr = app.OcrData.Split(',');
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        if (verifyBoxF2.Enabled) verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        if (verifyBoxF3.Enabled) verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        if (verifyBoxF4.Enabled) verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        if (verifyBoxF5.Enabled) verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }
                }
            }

            //画像の表示
            setImaage(app);
            changedReset(app);
        }

        private void setVerify(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                setVerifyVal(verifyBoxY, "**", nv);
            }
            else
            {
                setVerifyVal(verifyBoxY, app.MediYear, nv);

                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                setVerifyVal(verifyBoxNum, app.HihoNum, nv);
                setVerifyVal(verifyBoxName, app.PersonName, nv);

                //往療
                setVerifyVal(verifyCheckBoxVisit, app.Distance == 999, nv);

                //申請情報
                setVerifyVal(verifyBoxDays, app.CountedDays, nv);
                setVerifyVal(verifyBoxTotal, app.Total, nv);
                setVerifyVal(verifyBoxSeikyu, app.Charge, nv);
                setVerifyVal(verifyBoxNewCont, app.NewContType == NEW_CONT.Null ? "" : ((int)app.NewContType).ToString(), nv);

                //負傷
                setVerifyVal(verifyBoxF1, app.FushoName1, nv);
                setVerifyVal(verifyBoxF2, app.FushoName2, nv);
                setVerifyVal(verifyBoxF3, app.FushoName3, nv);
                setVerifyVal(verifyBoxF4, app.FushoName4, nv);
                setVerifyVal(verifyBoxF5, app.FushoName5, nv);
            }
            missCounterReset();
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImaage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else if(app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();

                //被保険者情報
                verifyBoxNum.Text = app.HihoNum;
                verifyBoxName.Text = app.PersonName;

                //往療
                verifyCheckBoxVisit.Checked = app.Distance == 999;

                //申請情報
                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxTotal.Text = app.Total.ToString();
                verifyBoxSeikyu.Text = app.Charge.ToString();
                verifyBoxNewCont.Text = app.NewContType == 0 ? string.Empty : ((int)app.NewContType).ToString();

                //負傷
                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;
            }
        }


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Action<Control, bool> act = null;

            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelUser, };

            act = new Action<Control, bool>((c, b) =>
            {
                foreach (Control item in c.Controls) act(item, b);
                if ((c is IVerifiable == false) && (c is Label == false)) return;
                if (ignoreControls.Contains(c)) return;
                c.Visible = b;
                if (c is IVerifiable == false) return;
                c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            });
            
            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                act(panelRight, false);
            }
            else
            {
                //申請書の場合
                act(panelRight, true);
                buiTabStopAdjust();
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImaage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImaage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImaage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (buiNameConts.Contains(sender)) p = posBuiName;
            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (newContConts.Contains(sender)) p = posNewCont;
            else if (visitConts.Contains(sender)) p = posVisit;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == "Aid")
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiNameConts.Any(c => c.Focused)) posBuiName = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (newContConts.Any(c => c.Focused)) posNewCont = pos;
            else if (visitConts.Any(c => c.Focused)) posVisit = pos;
        }
    }      
}
