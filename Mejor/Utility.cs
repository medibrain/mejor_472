﻿using System;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;

namespace Mejor
{
    public class Utility
    {
        // 円周率
        private const double PI = 3.14159265358979;

        // 扁平率の逆数
        private const double E_FLATTENING = 298.257222101;

        // 地球長半径
        private const double E_RADIUS_LNG = 6378137;

        public static double CalcDistance(
            double srcIdo,      // IN:第一緯度
            double srcKeido,    // IN:第一経度
            double dstIdo,      // IN:第二緯度
            double dstKeido)    // IN:第二経度
        {
            double sRadIdo, sRadKeido, dRadIdo, dRadKeido, diffIdo, diffKeido, avgIdo;
            double w;
            double firstRisinrituSq;    // 第一離心率
            double shigosenRadius;      // 子午線曲率半径
            double boyusenRadius;       // 卯酉線曲率半径
            double result;

            sRadIdo = Radians(srcIdo);
            sRadKeido = Radians(srcKeido);
            dRadIdo = Radians(dstIdo);
            dRadKeido = Radians(dstKeido);
            diffIdo = sRadIdo - dRadIdo;
            diffKeido = sRadKeido - dRadKeido;
            avgIdo = (sRadIdo + dRadIdo) / 2;

            firstRisinrituSq = Math.Pow((Math.Sqrt(2 * E_FLATTENING - 1) / E_FLATTENING), 2);
            w = Math.Sqrt(1 - firstRisinrituSq * Math.Sin(avgIdo));
            shigosenRadius = E_RADIUS_LNG * (1 - firstRisinrituSq) / Math.Pow(w, 3);
            boyusenRadius = E_RADIUS_LNG / w;

            result = Math.Sqrt(Math.Pow(diffIdo * shigosenRadius, 2) +
                                Math.Pow(diffKeido * boyusenRadius * Math.Cos(avgIdo), 2));

            return Sishagonyu(result, 0);
        }

        private static double Radians(double degrees)
        {
            return (PI * degrees / 180);
        }

        // 四捨五入
        private static double Sishagonyu(double dValue, int iDigits)
        {
            double dCoef = Math.Pow(10, iDigits);

            return dValue > 0 ? Math.Floor((dValue * dCoef) + 0.5) / dCoef :
                                Math.Ceiling((dValue * dCoef) - 0.5) / dCoef;
        }

        /// <summary>
        /// 数字のみを半角にします
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string NumberToHalf(string s)
        {
            Regex re = new Regex("[０-９]+");
            return re.Replace(s, m => Strings.StrConv(m.Value, VbStrConv.Narrow, 0));
        }

        /// <summary>
        /// 英数字のみを半角にします
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string Abc123ToHalf(string s)
        {
            Regex re = new Regex("[０-９Ａ-Ｚａ-ｚ]+");
            return re.Replace(s, m => Strings.StrConv(m.Value, VbStrConv.Narrow, 0));
        }

        /// <summary>
        /// 英数字とハイフン―－かっこ（）｛｝＜＞、全角スペースを半角にします
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string Abc123BracketToHalf(string s)
        {
            Regex re = new Regex("[０-９Ａ-Ｚａ-ｚ（）｛｝＜＞　]+ ");
            return re.Replace(s, m => Strings.StrConv(m.Value, VbStrConv.Narrow, 0));
        }

        /// <summary>
        /// カタカナ以外で半角にできるものをすべて半角にします
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToHalfWithoutKatakana(string s)
        {
            Regex re = new Regex(@"[^\u30A0-\u30FF]+");
            return re.Replace(s, m => Strings.StrConv(m.Value, VbStrConv.Narrow, 0));
        }

        /// <summary>
        /// カタカナをすべて全角にします
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToNarrowKatakana(string s)
        {
            //var match = Strings.StrConv(m.Value, Microsoft.VisualBasic.VbStrConv.Wide, 0);
            //Regex.Replace(src, "[\uFF61-\uFF9F+]", match);

            Regex re = new Regex(@"[\uFF61-\uFF9F]+");
            return re.Replace(s, m => Strings.StrConv(m.Value, VbStrConv.Wide, 0));
        }

        /// <summary>
        /// 指定した少数桁数で切り捨てます
        /// </summary>
        /// <param name="dValue"></param>
        /// <param name="iDigits"></param>
        /// <returns></returns>
        public static double ToRoundDown(double dValue, int iDigits)
        {
            double dCoef = System.Math.Pow(10, iDigits);

            return dValue > 0 ? System.Math.Floor(dValue * dCoef) / dCoef :
                                System.Math.Ceiling(dValue * dCoef) / dCoef;
        }

        /// <summary>
        /// データグリッドビューの表示データをそのままCSVにします
        /// </summary>
        /// <param name="view"></param>
        /// <param name="fileName"></param>
        public static void ViewToCsv(DataGridView view, string fileName)
        {
            var l = new List<string>();
            using (var sw = new System.IO.StreamWriter(fileName, false, System.Text.Encoding.UTF8))
            {
                foreach (DataGridViewColumn item in view.Columns)
                {
                    if (!item.Visible) continue;
                    l.Add(item.HeaderText);
                }

                sw.WriteLine(string.Join(",", l));
                l.Clear();

                for (int r = 0; r < view.RowCount; r++)
                {
                    for (int c = 0; c < view.ColumnCount; c++)
                    {
                        if (!view[c, r].Visible) continue;
                        l.Add(view[c, r].Value?.ToString() ?? string.Empty);
                    }
                    sw.WriteLine(string.Join(",", l));
                    l.Clear();
                }
            }
        }

        /// <summary>
        /// 指定されたstringデータ列をCSV文字列に変換します
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static string CreateCsvLine(IEnumerable<string> datas)
        {
            var l = new List<string>();
            foreach (var item in datas) l.Add($"\"{item.Replace("\"", "\"\"")}\"");
            return string.Join(",", l);
        }

        public static Color GridSelectColor => Color.FromArgb(183, 219, 255);

        /// <summary>
        /// ファイルのバイト数が100以上かチェックします
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool CheckFileByte(string fileName)
        {
            try
            {
                var info = new System.IO.FileInfo(fileName);
                return info.Length >= 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 実行ファイルのパスを取得します 最後に\が付属します
        /// </summary>
        public static string StartupPath =>
            AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// 現在実行されているexeファイルのフルパスを取得します
        /// </summary>
        public static string ExeFullPath =>
            System.Reflection.Assembly.GetEntryAssembly().Location;


        //異字体セレクタ(IVS)関連
        //日本の漢字の異字体セレクタ Variation Selector Supplementの範囲
        const char VariationSelectorSupplementHi = '\uDB40';
        const char VariationSelectorSupplementLoStart = '\uDD00';
        const char VariationSelectorSupplementLoEnd = '\uDDEF';
        //それ以外だと以下が使われている可能性がある
        const char VariationSelectorStart = '\uFE00';
        const char VariationSelectorEnd = '\uFE0F';
        //モンゴル文字用
        const char MongolianFreeVariationSelectorStart = '\u180B';
        const char MongolianFreeVariationSelectorEnd = '\u180d';

        /// <summary>
        /// 文字列中の指定位置の文字がIVSのセレクタ文字列か判断する。
        /// </summary>
        /// <param name="s">文字列</param>
        /// <param name="index">s 内の評価する文字の位置。</param>
        /// <returns>s の index の位置にある文字がセレクタ文字列の場合は true。それ以外の場合は false。</returns>
        public static bool IsVariationSelector(string s, int index)
        {
            return CheckSelector(s[index]);
        }

        public static string GetVariationString(string s)
        {
            var l = new List<string>();
            for (int i = 1; i < s.Length; i++)
            {
                if (s[i] == VariationSelectorSupplementHi) l.Add(s.Substring(i - 1, 3));
            }
            return string.Join(", ", l);
        }

        public static bool ContainsVariationString(string s)
        {
            for (int i = 1; i < s.Length; i++)
            {
                if (IsVariationSelector(s[i])) return true;
            }
            return false;
        }


        /// <summary>
        /// 文字がIVSのセレクタ文字列か判断する。
        /// </summary>
        /// <param name="c">評価するUNICODE文字</param>
        /// <returns>文字がセレクタ文字列の場合は true。それ以外の場合は false。</returns>
        public static bool IsVariationSelector(char c)
        {
            return CheckSelector(c);
        }

        /// <summary>
        ///     文字がIVSのセレクタ文字列か判断する。
        /// </summary>
        /// <param name="c">評価するUNICODE文字</param>
        /// <returns>文字がセレクタ文字列の場合は true。それ以外の場合は false。</returns>
        private static bool CheckSelector(char c)
        {
            return c == VariationSelectorSupplementHi ||
                (VariationSelectorSupplementLoStart <= c && c <= VariationSelectorSupplementLoEnd) ||
                (VariationSelectorStart <= c && c <= VariationSelectorEnd) ||
                (MongolianFreeVariationSelectorStart <= c && c <= MongolianFreeVariationSelectorEnd);
        }
    }
}
