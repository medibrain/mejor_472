﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.NagoyashiKokuho

{
    public partial class InputForm_fusho : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);        
        Point posFusho = new Point(200, 500);
        Point posCost = new Point(0, 0);
        Point poshnum = new Point(0, 0);

        Control[] ymControls, fushoControls, costControls, hnumControls;

        public InputForm_fusho(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] {
                verifyBoxY, verifyBoxM,
            };


            //上段
            hnumControls = new Control[]
            {
                verifyBoxHnum,verifyBoxHnumM,
                verifyBoxBD,verifyBoxBE,verifyBoxBM,verifyBoxBY,
                verifyBoxSex,verifyBoxRatio,verifyBoxFamily,verifyBoxClinicCode,verifyBoxPref,verifyBoxAppType,
            };


            //20200617100348 furukawa st ////////////////////////
            //負傷名年月日入力、年号削除
            

            //負傷名箇所
            fushoControls = new Control[] {
                verifyBoxF1 ,verifyBoxF1Days ,verifyBoxF1edD ,verifyBoxF1fsD ,verifyBoxF1fsM ,verifyBoxF1fsY ,verifyBoxF1skD ,verifyBoxF1skM  ,verifyBoxF1skY ,verifyBoxF1stD ,
                verifyBoxF2 ,verifyBoxF2Days ,verifyBoxF2edD ,verifyBoxF2fsD ,verifyBoxF2fsM ,verifyBoxF2fsY ,verifyBoxF2skD ,verifyBoxF2skM  ,verifyBoxF2skY ,verifyBoxF2stD ,
                verifyBoxF3 ,verifyBoxF3Days ,verifyBoxF3edD ,verifyBoxF3fsD ,verifyBoxF3fsM ,verifyBoxF3fsY ,verifyBoxF3skD ,verifyBoxF3skM  ,verifyBoxF3skY ,verifyBoxF3stD ,
                verifyBoxF4 ,verifyBoxF4Days ,verifyBoxF4edD ,verifyBoxF4fsD ,verifyBoxF4fsM ,verifyBoxF4fsY ,verifyBoxF4skD ,verifyBoxF4skM  ,verifyBoxF4skY ,verifyBoxF4stD ,
                verifyBoxF5 ,verifyBoxF5Days ,verifyBoxF5edD ,verifyBoxF5fsD ,verifyBoxF5fsM ,verifyBoxF5fsY ,verifyBoxF5skD ,verifyBoxF5skM  ,verifyBoxF5skY ,verifyBoxF5stD ,
            };

            //負傷名箇所
            //fushoControls = new Control[] {
            //    verifyBoxF1 ,verifyBoxF1Days ,verifyBoxF1edD ,verifyBoxF1fsD ,verifyBoxF1fsM ,verifyBoxF1fsNengo ,verifyBoxF1fsY ,verifyBoxF1skD ,verifyBoxF1skM ,verifyBoxF1skNengo ,verifyBoxF1skY ,verifyBoxF1stD ,//verifyBoxF1Tenki ,
            //    verifyBoxF2 ,verifyBoxF2Days ,verifyBoxF2edD ,verifyBoxF2fsD ,verifyBoxF2fsM ,verifyBoxF2fsNengo ,verifyBoxF2fsY ,verifyBoxF2skD ,verifyBoxF2skM ,verifyBoxF2skNengo ,verifyBoxF2skY ,verifyBoxF2stD ,//verifyBoxF2Tenki ,
            //    verifyBoxF3 ,verifyBoxF3Days ,verifyBoxF3edD ,verifyBoxF3fsD ,verifyBoxF3fsM ,verifyBoxF3fsNengo ,verifyBoxF3fsY ,verifyBoxF3skD ,verifyBoxF3skM ,verifyBoxF3skNengo ,verifyBoxF3skY ,verifyBoxF3stD ,//verifyBoxF3Tenki ,
            //    verifyBoxF4 ,verifyBoxF4Days ,verifyBoxF4edD ,verifyBoxF4fsD ,verifyBoxF4fsM ,verifyBoxF4fsNengo ,verifyBoxF4fsY ,verifyBoxF4skD ,verifyBoxF4skM ,verifyBoxF4skNengo ,verifyBoxF4skY ,verifyBoxF4stD ,//verifyBoxF4Tenki ,
            //    verifyBoxF5 ,verifyBoxF5Days ,verifyBoxF5edD ,verifyBoxF5fsD ,verifyBoxF5fsM ,verifyBoxF5fsNengo ,verifyBoxF5fsY ,verifyBoxF5skD ,verifyBoxF5skM ,verifyBoxF5skNengo ,verifyBoxF5skY ,verifyBoxF5stD ,//verifyBoxF5Tenki ,
            //};

            //20200617100348 furukawa ed ////////////////////////


            //合計金額箇所
            costControls = new Control[]
            {
                verifyBoxSum,verifyBoxCost,verifyBoxDrCode,verifyBoxDrName,verifyBoxHosName,
            };

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;            
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";

            //拡張入力扱いなので、InputStatusExを参照する
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].DisplayIndex = 1;

            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";


            panelHnum.Visible = false;
            panelFusho.Visible = false;
            panelTotal.Visible = false;
            dataGridRefRece.Visible = false;
           

            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion


        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month > 12);


            //負傷ごとの新規継続チェック＋appへの反映
            if(!hasError)chkNewContinue(app,out app);


            //負傷ごとのチェック＋appへの反映
            if (!hasError) if (!chkFusho(app,out app)) hasError = true;

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //db登録
            app.MediYear = year;
            app.MediMonth = month;



            return true;
        }

        /// <summary>
        /// 新規継続
        /// </summary>
        /// <param name="app">Application</param>
        /// <param name="outapp">Application</param>
        private void chkNewContinue(App app,out App outapp)
        {
            bool flgNew = false;//新規の場合True
            DateTime sejutuYM = DateTimeEx.ToDateTime(int.Parse(app.YM + "01"));

            //施術年月より初検日が大きい場合は新規と見なす
            //一つでも継続があれば継続
            if ((app.FushoFirstDate1!=DateTime.MinValue && app.FushoFirstDate1 < sejutuYM) ||
                (app.FushoFirstDate2!=DateTime.MinValue && app.FushoFirstDate2 < sejutuYM) ||
                (app.FushoFirstDate3!=DateTime.MinValue && app.FushoFirstDate3 < sejutuYM) ||
                (app.FushoFirstDate4!=DateTime.MinValue && app.FushoFirstDate4 < sejutuYM) ||
                (app.FushoFirstDate5 != DateTime.MinValue && app.FushoFirstDate5 < sejutuYM)  ) flgNew = false;
            else
                flgNew = true;
            

            app.NewContType = flgNew==true ? NEW_CONT.新規 : NEW_CONT.継続;
            outapp = app;
        }


        /// <summary>
        /// 負傷ごとのチェック
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool chkFusho(App app,out App outapp)
        {
            //施術年月を西暦年に変換
            string sejutuYM = string.Empty;
            sejutuYM = verifyBoxY.GetIntValue().ToString("00") + verifyBoxM.GetIntValue().ToString("00");
            int ADYear = DateTimeEx.GetAdYearFromHs(int.Parse(sejutuYM));

            #region 負傷1
            //負傷名1チェック
            fusho1Check(verifyBoxF1);
            int days1 = 0;
            int tenki1 = 0;
            

            if (verifyBoxF1.Text != string.Empty)
            {

                //20200617100558 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い引数変更
                
                //負傷日
                app.FushoDate1 = chkDate( verifyBoxF1fsY, verifyBoxF1fsM, verifyBoxF1fsD);
                //初検日
                app.FushoFirstDate1 = chkDate( verifyBoxF1skY, verifyBoxF1skM, verifyBoxF1skD);
                ////負傷日
                //app.FushoDate1 = chkDate(verifyBoxF1fsNengo, verifyBoxF1fsY, verifyBoxF1fsM, verifyBoxF1fsD);
                ////初検日
                //app.FushoFirstDate1 = chkDate(verifyBoxF1skNengo, verifyBoxF1skY, verifyBoxF1skM, verifyBoxF1skD);
                //20200617100558 furukawa ed ////////////////////////


                //開始日
                app.FushoStartDate1 = chkDate(ADYear, verifyBoxM, verifyBoxF1stD);
                //終了日
                app.FushoFinishDate1 = chkDate(ADYear, verifyBoxM, verifyBoxF1edD);

                //実日数1
                days1 = verifyBoxF1Days.GetIntValue();
                setStatus(verifyBoxF1Days, verifyBoxF1.Text != string.Empty && days1 < 1 || 31 < days1);

                ////転帰1  治癒1 中止2 転医3 継続:空白
                //tenki1 = verifyBoxF1Tenki.GetIntValue();
                //setStatus(verifyBoxF1Tenki, verifyBoxF1Tenki.Text != string.Empty && (tenki1 < 1 || 3 < tenki1));
            }
            #endregion

            #region 負傷2
            //負傷名2チェック
            fushoCheck(verifyBoxF2);
            int days2 = 0;
            int tenki2 = 0;
            if (verifyBoxF2.Text != string.Empty)
            {
                //20200617100801 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い引数変更
                
                //負傷日
                app.FushoDate2 = chkDate( verifyBoxF2fsY, verifyBoxF2fsM, verifyBoxF2fsD);
                //初検日
                app.FushoFirstDate2 = chkDate( verifyBoxF2skY, verifyBoxF2skM, verifyBoxF2skD);
                //負傷日
                //app.FushoDate2 = chkDate(verifyBoxF2fsNengo, verifyBoxF2fsY, verifyBoxF2fsM, verifyBoxF2fsD);
                //初検日
                //app.FushoFirstDate2 = chkDate(verifyBoxF2skNengo, verifyBoxF2skY, verifyBoxF2skM, verifyBoxF2skD);
                //20200617100801 furukawa ed ////////////////////////


                //開始日
                app.FushoStartDate2 = chkDate(ADYear, verifyBoxM, verifyBoxF2stD);
                //終了日
                app.FushoFinishDate2 = chkDate(ADYear, verifyBoxM, verifyBoxF2edD);


                //実日数2
                days2 = verifyBoxF2Days.GetIntValue();
                setStatus(verifyBoxF2Days, verifyBoxF2.Text != string.Empty && days2 < 1 || 31 < days2);
                //転帰2
                //tenki2 = verifyBoxF2Tenki.GetIntValue();
                //setStatus(verifyBoxF2Tenki, verifyBoxF2Tenki.Text != string.Empty && (tenki2 < 1 || 3 < tenki2));
            }
            #endregion

            #region 負傷3
            //負傷名3チェック
            fushoCheck(verifyBoxF3);
            int days3 = 0;
            int tenki3 = 0;
            if (verifyBoxF3.Text != string.Empty)
            {
                //20200617101222 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い引数変更
                
                //負傷日
                app.FushoDate3 = chkDate( verifyBoxF3fsY, verifyBoxF3fsM, verifyBoxF3fsD);
                //初検日
                app.FushoFirstDate3 = chkDate( verifyBoxF3skY, verifyBoxF3skM, verifyBoxF3skD);

                //負傷日
                //app.FushoDate3 = chkDate(verifyBoxF3fsNengo, verifyBoxF3fsY, verifyBoxF3fsM, verifyBoxF3fsD);
                //初検日
                //app.FushoFirstDate3 = chkDate(verifyBoxF3skNengo, verifyBoxF3skY, verifyBoxF3skM, verifyBoxF3skD);
                //20200617101222 furukawa ed ////////////////////////

                //開始日
                app.FushoStartDate3 = chkDate(ADYear, verifyBoxM, verifyBoxF3stD);
                //終了日
                app.FushoFinishDate3 = chkDate(ADYear, verifyBoxM, verifyBoxF3edD);


                //実日数3
                days3 = verifyBoxF3Days.GetIntValue();
                setStatus(verifyBoxF3Days, verifyBoxF3.Text != string.Empty && days3 < 1 || 31 < days3);
                //転帰3
                //tenki3 = verifyBoxF3Tenki.GetIntValue();
                //setStatus(verifyBoxF3Tenki, verifyBoxF3Tenki.Text != string.Empty && (tenki3 < 1 || 3 < tenki3));
            }
            #endregion

            #region 負傷4
            //負傷名4チェック
            fushoCheck(verifyBoxF4);
            int days4 = 0;
            int tenki4 = 0;
            if (verifyBoxF4.Text != string.Empty)
            {

                //20200617101257 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い引数変更
                
                //負傷日
                app.FushoDate4 = chkDate(verifyBoxF4fsY, verifyBoxF4fsM, verifyBoxF4fsD);
                //初検日
                app.FushoFirstDate4 = chkDate(verifyBoxF4skY, verifyBoxF4skM, verifyBoxF4skD);

                ////負傷日
                //app.FushoDate4 = chkDate(verifyBoxF4fsNengo, verifyBoxF4fsY, verifyBoxF4fsM, verifyBoxF4fsD);
                ////初検日
                //app.FushoFirstDate4 = chkDate(verifyBoxF4skNengo, verifyBoxF4skY, verifyBoxF4skM, verifyBoxF4skD);
                //20200617101257 furukawa ed ////////////////////////

                //開始日
                app.FushoStartDate4 = chkDate(ADYear, verifyBoxM, verifyBoxF4stD);
                //終了日
                app.FushoFinishDate4 = chkDate(ADYear, verifyBoxM, verifyBoxF4edD);


                //実日数4
                days4 = verifyBoxF4Days.GetIntValue();
                setStatus(verifyBoxF4Days, verifyBoxF4.Text != string.Empty && days4 < 1 || 31 < days4);
                ////転帰4
                //tenki4 = verifyBoxF4Tenki.GetIntValue();
                //setStatus(verifyBoxF4Tenki, verifyBoxF4Tenki.Text != string.Empty && (tenki4 < 1 || 3 < tenki4));

            }
            #endregion

            #region 負傷5
            //負傷名5チェック
            fushoCheck(verifyBoxF5);
            int days5 = 0;
            int tenki5 = 0;
            if (verifyBoxF5.Text != string.Empty)
            {

                //20200617101328 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い引数変更
                
                //負傷日
                app.FushoDate5 = chkDate(verifyBoxF5fsY, verifyBoxF5fsM, verifyBoxF5fsD);
                //初検日
                app.FushoFirstDate5 = chkDate( verifyBoxF5skY, verifyBoxF5skM, verifyBoxF5skD);
                ////負傷日
                //app.FushoDate5 = chkDate(verifyBoxF5fsNengo, verifyBoxF5fsY, verifyBoxF5fsM, verifyBoxF5fsD);
                ////初検日
                //app.FushoFirstDate5 = chkDate(verifyBoxF5skNengo, verifyBoxF5skY, verifyBoxF5skM, verifyBoxF5skD);
                //20200617101328 furukawa ed ////////////////////////


                //開始日
                app.FushoStartDate5 = chkDate(ADYear, verifyBoxM, verifyBoxF5stD);
                //終了日
                app.FushoFinishDate5 = chkDate(ADYear, verifyBoxM, verifyBoxF5edD);


                //実日数5
                days5 = verifyBoxF5Days.GetIntValue();
                setStatus(verifyBoxF5Days, verifyBoxF5.Text != string.Empty && days5 < 1 || 31 < days5);
                ////転帰5
                //tenki5 = verifyBoxF5Tenki.GetIntValue();
                //setStatus(verifyBoxF5Tenki, verifyBoxF5Tenki.Text != string.Empty && (tenki5 < 1 || 3 < tenki5));
            }
            #endregion

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                outapp = app;
                return false;                
            }


            #region Appに適用

            //負傷名
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //負傷ごとの実日数
            app.FushoDays1 = days1;
            app.FushoDays2 = days2;
            app.FushoDays3 = days3;
            app.FushoDays4 = days4;
            app.FushoDays5 = days5;

            //負傷ごとの転帰
            app.FushoCourse1 = tenki1;
            app.FushoCourse2 = tenki2;
            app.FushoCourse3 = tenki3;
            app.FushoCourse4 = tenki4;
            app.FushoCourse5 = tenki5;

            //20200529162644 furukawa st ////////////////////////
            //通常画面入力での実日数＝施術日数とし、負傷名ごとの実日数と分ける

            //開始日の最小
            //int[] arrdays = { days1, days2, days3, days4, days5 };
            //int maxdays = arrdays.Max();

            //List<DateTime> startdate = new List<DateTime>();
            //if (app.FushoStartDate1 != DateTime.MinValue) startdate.Add(app.FushoStartDate1);
            //if (app.FushoStartDate2 != DateTime.MinValue) startdate.Add(app.FushoStartDate2);
            //if (app.FushoStartDate3 != DateTime.MinValue) startdate.Add(app.FushoStartDate3);
            //if (app.FushoStartDate4 != DateTime.MinValue) startdate.Add(app.FushoStartDate4);
            //if (app.FushoStartDate5 != DateTime.MinValue) startdate.Add(app.FushoStartDate5);
            //startdate.OrderBy(rec => rec.Date);
            //DateTime minStartDay = startdate[0].Date;

            //終了日の最大
            //List<DateTime> finishdate = new List<DateTime>();
            //if (app.FushoFinishDate1 != DateTime.MinValue) finishdate.Add(app.FushoFinishDate1);
            //if (app.FushoFinishDate2 != DateTime.MinValue) finishdate.Add(app.FushoFinishDate2);
            //if (app.FushoFinishDate3 != DateTime.MinValue) finishdate.Add(app.FushoFinishDate3);
            //if (app.FushoFinishDate4 != DateTime.MinValue) finishdate.Add(app.FushoFinishDate4);
            //if (app.FushoFinishDate5 != DateTime.MinValue) finishdate.Add(app.FushoFinishDate5);
            //finishdate.Sort((x, y) => y.Date.CompareTo(x.Date));
            //DateTime maxfinishdate = finishdate[0].Date;

            //app.CountedDays = maxdays;
            //初検日の最小を入れてもいいが、項目が煩雑になるのでリスト作成時に取得でもいいか
            //app.TaggedDatas.Dates = minStartDay.ToString("yyyy/MM/dd");
            
            
            //20200529162644 furukawa ed ////////////////////////



            //負傷数
            int intFushoCnt = 0;
            if (verifyBoxF1.Text.Trim() != string.Empty) intFushoCnt++;
            if (verifyBoxF2.Text.Trim() != string.Empty) intFushoCnt++;
            if (verifyBoxF3.Text.Trim() != string.Empty) intFushoCnt++;
            if (verifyBoxF4.Text.Trim() != string.Empty) intFushoCnt++;
            if (verifyBoxF5.Text.Trim() != string.Empty) intFushoCnt++;
            app.TaggedDatas.count= intFushoCnt;
            app.Bui = intFushoCnt;

            outapp = app;

            #endregion

            return true;
        }

        /// <summary>
        /// 負傷ごとの日付チェック
        /// </summary>
        /// <param name="vbNengo">年号のVerifybox</param>
        /// <param name="vbY">和暦年のVerifybox</param>
        /// <param name="vbM">月のVerifybox</param>
        /// <param name="vbD">日のVerifybox</param>    
        /// <returns>datetime日付</returns>
        private DateTime chkDate(VerifyBox vbNengo,VerifyBox vbY,VerifyBox vbM,VerifyBox vbD)
        {

            //年号
            int sG = vbNengo.GetIntValue();
            setStatus(vbNengo, sG < 1 || 5 < sG);

            //初検年月
            int sY = vbY.GetIntValue();
            int sM = vbM.GetIntValue();
            int sD = vbD.GetIntValue();

            DateTime dtTmp = DateTime.MinValue;

            //西暦変換
            
            int.TryParse(sG.ToString() + sY.ToString("00") + sM.ToString("00"), out int inttmp);
            if (inttmp > 0) sY = DateTimeEx.GetAdYearMonthFromJyymm(inttmp) / 100;

            if (!DateTimeEx.IsDate(sY, sM, sD))
            {
                setStatus(vbY, true);
                setStatus(vbM, true);
                setStatus(vbD, true);
            }
            else
            {
                setStatus(vbY, false);
                setStatus(vbM, false);
                setStatus(vbD, false);
                dtTmp = new DateTime(sY, sM, sD);
            }
            return dtTmp;
        }


        //20200617103610 furukawa st ////////////////////////
        //負傷名年月日入力、年号削除に伴い引数変更
        
        /// <summary>
        /// 年号非入力版
        /// </summary>
        /// <param name="vbY">年テキストボックス</param>
        /// <param name="vbM">月テキストボックス</param>
        /// <param name="vbD">日テキストボックス</param>
        /// <returns></returns>
        private DateTime chkDate(VerifyBox vbY, VerifyBox vbM, VerifyBox vbD)
        {
            DateTime dtTmp = DateTime.MinValue;

            //初検年月
            int sY = vbY.GetIntValue();
            int sM = vbM.GetIntValue();
            int sD = vbD.GetIntValue();

            if (sY <= 0 || sM <= 0 ||sD<=0) return dtTmp;
            
            int intADYear = DateTimeEx.GetAdYearFromHs(int.Parse(sY.ToString("00") + sM.ToString("00")));
            

            if (!DateTimeEx.IsDate(intADYear, sM, sD))
            {
                setStatus(vbY, true);
                setStatus(vbM, true);
                setStatus(vbD, true);
            }
            else
            {
                setStatus(vbY, false);
                setStatus(vbM, false);
                setStatus(vbD, false);
                dtTmp = new DateTime(intADYear, sM, sD);
            }
            return dtTmp;
        }
        //20200617103610 furukawa ed ////////////////////////




        /// <summary>
        /// 負傷ごとの日付チェック
        /// </summary>
        /// <param name="ADYear">西暦年</param>
        /// <param name="vbM">施術年月の月のVerifybox</param>
        /// <param name="vbD">日のVerifybox</param>
        /// <returns>datetime日付</returns>
        private DateTime chkDate(int ADYear, VerifyBox vbM, VerifyBox vbD)
        {
            //月
            int sM = vbM.GetIntValue();
            //日
            int sD = vbD.GetIntValue();

            DateTime dttmp = DateTime.MinValue;

            if (!DateTimeEx.IsDate(ADYear, sM, sD))
            {
                setStatus(vbD, true);                
            }
            else
            {
                setStatus(vbD, false);                
                dttmp = new DateTime(ADYear, sM, sD);
            }
            return dttmp;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.拡張ベリ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;
            

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //拡張入力扱いとする
                var ut = firstTime ? App.UPDATE_TYPE.FirstInputEx : App.UPDATE_TYPE.SecondInputEx;
            
                if (firstTime && app.UfirstEx == 0)
                {
                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.ExFirst, 0, DateTime.Now-dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.ExFirst, 0, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }
                else if (!firstTime && app.UsecondEx == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;

                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.ExSecond, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.ExSecond, secondMissCount, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {

            //一旦負傷欄を全てTabStopFalseにする
            TabStopFushoControls();

            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示(拡張入力)
            labelInputerName.Text = "拡張入力1:  " + User.GetUserName(app.UfirstEx) +
                "\r\n拡張入力2:  " + User.GetUserName(app.UsecondEx);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.拡張入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.5f;
                    //scrollPictureControl1.Ratio = 0.6f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.拡張ベリ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //負傷名だけなので不要
                //被保険者番号
                //var hn = app.HihoNum.Split('-');
                //if (hn.Length == 2)
                //{
                //    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                //    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                //}
                //else
                //{
                //    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                //    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                //}

                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);


                //負傷ごとの値をセット
                setFusho(app,nv);

            }
        }

        /// <summary>
        /// 負傷ごとのデータを表示
        /// </summary>
        /// <param name="app"></param>
        private void setFusho(App app,bool nv)
        {
            //一旦負傷欄を全てTabStopFalseにする
            TabStopFushoControls();


            #region 負傷1
            //負傷名1
            setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
            if (app.FushoName1 != string.Empty)
            {

                //負傷年月日1

                //20200617101558 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要

                //setValue(verifyBoxF1fsNengo, DateTimeEx.GetEraNumber(app.FushoDate1).ToString(), firstTime, nv);

                //20200617101558 furukawa ed ////////////////////////

                setValue(verifyBoxF1fsY, DateTimeEx.GetJpYear(app.FushoDate1).ToString(), firstTime, nv);
                setValue(verifyBoxF1fsM, app.FushoDate1.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF1fsD, app.FushoDate1.Day.ToString(), firstTime, nv);

                //初検年月1

                //20200617101640 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF1skNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate1).ToString(), firstTime, nv);
                //20200617101640 furukawa ed ////////////////////////

                setValue(verifyBoxF1skY, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, nv);
                setValue(verifyBoxF1skM, app.FushoFirstDate1.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF1skD, app.FushoFirstDate1.Day.ToString(), firstTime, nv);

                //開始日1
                setValue(verifyBoxF1stD, app.FushoStartDate1.Day.ToString(), firstTime, nv);

                //終了日1
                setValue(verifyBoxF1edD, app.FushoFinishDate1.Day.ToString(), firstTime, nv);

                //実日数1
                setValue(verifyBoxF1Days, app.FushoDays1, firstTime, nv);

                //転帰1  -9の場合は空白にする
                //setValue(verifyBoxF1Tenki, app.FushoCourse1 == -9 ? string.Empty : app.FushoCourse1.ToString(), firstTime, nv);
            }
            else
            {
                //負傷年月日1

                //20200617101718 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF1fsNengo, string.Empty, firstTime, nv);
                //20200617101718 furukawa ed ////////////////////////

                setValue(verifyBoxF1fsY, string.Empty, firstTime, nv);
                setValue(verifyBoxF1fsM, string.Empty, firstTime, nv);
                setValue(verifyBoxF1fsD, string.Empty, firstTime, nv);


                //初検日1

                //20200617101741 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF1skNengo, string.Empty, firstTime, nv);

                //20200617101741 furukawa ed ////////////////////////

                setValue(verifyBoxF1skY, string.Empty, firstTime, nv);
                setValue(verifyBoxF1skM, string.Empty, firstTime, nv);
                setValue(verifyBoxF1skD, string.Empty, firstTime, nv);

                //開始日1
                setValue(verifyBoxF1stD, string.Empty, firstTime, nv);

                //終了日1
                setValue(verifyBoxF1edD, string.Empty, firstTime, nv);


                //実日数1
                setValue(verifyBoxF1Days, string.Empty, firstTime, nv);
                //転帰1
                //setValue(verifyBoxF1Tenki, string.Empty, firstTime, nv);
            }
            #endregion

            #region 負傷2
            //負傷名2
            setValue(verifyBoxF2, app.FushoName2, firstTime, nv);
            if (app.FushoName2 != string.Empty)
            {

                //負傷年月日2

                //20200617101812 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF2fsNengo, DateTimeEx.GetEraNumber(app.FushoDate2).ToString(), firstTime, nv);
                //20200617101812 furukawa ed ////////////////////////

                setValue(verifyBoxF2fsY, DateTimeEx.GetJpYear(app.FushoDate2).ToString(), firstTime, nv);
                setValue(verifyBoxF2fsM, app.FushoDate2.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF2fsD, app.FushoDate2.Day.ToString(), firstTime, nv);

                //初検年月2

                //20200617101837 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF2skNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate2).ToString(), firstTime, nv);
                //20200617101837 furukawa ed ////////////////////////

                setValue(verifyBoxF2skY, DateTimeEx.GetJpYear(app.FushoFirstDate2).ToString(), firstTime, nv);
                setValue(verifyBoxF2skM, app.FushoFirstDate2.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF2skD, app.FushoFirstDate2.Day.ToString(), firstTime, nv);

                //開始日2
                setValue(verifyBoxF2stD, app.FushoStartDate2.Day.ToString(), firstTime, nv);

                //終了日2
                setValue(verifyBoxF2edD, app.FushoFinishDate2.Day.ToString(), firstTime, nv);

                //実日数2
                setValue(verifyBoxF2Days, app.FushoDays2, firstTime, nv);

                //転帰2  -9の場合は空白にする
                //setValue(verifyBoxF2Tenki, app.FushoCourse2 == -9 ? string.Empty : app.FushoCourse2.ToString(), firstTime, nv);
            }
            else
            {
                //20200617101915 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //負傷年月日2
                //setValue(verifyBoxF2fsNengo, string.Empty, firstTime, nv);
                //20200617101915 furukawa ed ////////////////////////
                setValue(verifyBoxF2fsY, string.Empty, firstTime, nv);
                setValue(verifyBoxF2fsM, string.Empty, firstTime, nv);
                setValue(verifyBoxF2fsD, string.Empty, firstTime, nv);


                //初検日2     
                //20200617102428 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF2skNengo, string.Empty, firstTime, nv);
                //20200617102428 furukawa ed ////////////////////////

                setValue(verifyBoxF2skY, string.Empty, firstTime, nv);
                setValue(verifyBoxF2skM, string.Empty, firstTime, nv);
                setValue(verifyBoxF2skD, string.Empty, firstTime, nv);

                //開始日2
                setValue(verifyBoxF2stD, string.Empty, firstTime, nv);

                //終了日2
                setValue(verifyBoxF2edD, string.Empty, firstTime, nv);


                //実日数2
                setValue(verifyBoxF2Days, string.Empty, firstTime, nv);
                //転帰2
                //setValue(verifyBoxF2Tenki, string.Empty, firstTime, nv);
            }
            #endregion

            #region 負傷3
            //負傷名3
            setValue(verifyBoxF3, app.FushoName3, firstTime, nv);
            if (app.FushoName3 != string.Empty)
            {

                //負傷年月日3
                //20200617102448 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF3fsNengo, DateTimeEx.GetEraNumber(app.FushoDate3).ToString(), firstTime, nv);
                //20200617102448 furukawa ed ////////////////////////

                setValue(verifyBoxF3fsY, DateTimeEx.GetJpYear(app.FushoDate3).ToString(), firstTime, nv);
                setValue(verifyBoxF3fsM, app.FushoDate3.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF3fsD, app.FushoDate3.Day.ToString(), firstTime, nv);

                //初検年月3
                //20200617102507 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF3skNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate3).ToString(), firstTime, nv);
                //20200617102507 furukawa ed ////////////////////////

                setValue(verifyBoxF3skY, DateTimeEx.GetJpYear(app.FushoFirstDate3).ToString(), firstTime, nv);
                setValue(verifyBoxF3skM, app.FushoFirstDate3.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF3skD, app.FushoFirstDate3.Day.ToString(), firstTime, nv);

                //開始日3
                setValue(verifyBoxF3stD, app.FushoStartDate3.Day.ToString(), firstTime, nv);

                //終了日3
                setValue(verifyBoxF3edD, app.FushoFinishDate3.Day.ToString(), firstTime, nv);

                //実日数3
                setValue(verifyBoxF3Days, app.FushoDays3, firstTime, nv);

                //転帰3  -9の場合は空白にする
                //setValue(verifyBoxF3Tenki, app.FushoCourse3 == -9 ? string.Empty : app.FushoCourse3.ToString(), firstTime, nv);
            }
            else
            {
                //負傷年月日3
                //20200617102523 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要


                //setValue(verifyBoxF3fsNengo, string.Empty, firstTime, nv);
                //20200617102523 furukawa ed ////////////////////////

                setValue(verifyBoxF3fsY, string.Empty, firstTime, nv);
                setValue(verifyBoxF3fsM, string.Empty, firstTime, nv);
                setValue(verifyBoxF3fsD, string.Empty, firstTime, nv);


                //初検日3  
                //20200617102555 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF3skNengo, string.Empty, firstTime, nv);
                //20200617102555 furukawa ed ////////////////////////

                setValue(verifyBoxF3skY, string.Empty, firstTime, nv);
                setValue(verifyBoxF3skM, string.Empty, firstTime, nv);
                setValue(verifyBoxF3skD, string.Empty, firstTime, nv);

                //開始日3
                setValue(verifyBoxF3stD, string.Empty, firstTime, nv);

                //終了日3
                setValue(verifyBoxF3edD, string.Empty, firstTime, nv);


                //実日数3
                setValue(verifyBoxF3Days, string.Empty, firstTime, nv);
                //転帰3
                //setValue(verifyBoxF3Tenki, string.Empty, firstTime, nv);
            }
            #endregion

            #region 負傷4

            //負傷名4
            setValue(verifyBoxF4, app.FushoName4, firstTime, nv);
            if (app.FushoName4 != string.Empty)
            {

                //負傷年月日4

                //20200617102618 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF4fsNengo, DateTimeEx.GetEraNumber(app.FushoDate4).ToString(), firstTime, nv);
                //20200617102618 furukawa ed ////////////////////////
                setValue(verifyBoxF4fsY, DateTimeEx.GetJpYear(app.FushoDate4).ToString(), firstTime, nv);
                setValue(verifyBoxF4fsM, app.FushoDate4.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF4fsD, app.FushoDate4.Day.ToString(), firstTime, nv);

                //初検年月4

                //20200617102624 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF4skNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate4).ToString(), firstTime, nv);
                //20200617102624 furukawa ed ////////////////////////
                setValue(verifyBoxF4skY, DateTimeEx.GetJpYear(app.FushoFirstDate4).ToString(), firstTime, nv);
                setValue(verifyBoxF4skM, app.FushoFirstDate4.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF4skD, app.FushoFirstDate4.Day.ToString(), firstTime, nv);

                //開始日4
                setValue(verifyBoxF4stD, app.FushoStartDate4.Day.ToString(), firstTime, nv);

                //終了日4
                setValue(verifyBoxF4edD, app.FushoFinishDate4.Day.ToString(), firstTime, nv);

                //実日数4
                setValue(verifyBoxF4Days, app.FushoDays4, firstTime, nv);

                //転帰4  -9の場合は空白にする
                //setValue(verifyBoxF4Tenki, app.FushoCourse4 == -9 ? string.Empty : app.FushoCourse4.ToString(), firstTime, nv);
            }
            else
            {
                //負傷年月日4

                //20200617102729 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF4fsNengo, string.Empty, firstTime, nv);
                //20200617102729 furukawa ed ////////////////////////
                setValue(verifyBoxF4fsY, string.Empty, firstTime, nv);
                setValue(verifyBoxF4fsM, string.Empty, firstTime, nv);
                setValue(verifyBoxF4fsD, string.Empty, firstTime, nv);


                //初検日4  
                
                //20200617102735 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF4skNengo, string.Empty, firstTime, nv);
                //20200617102735 furukawa ed ////////////////////////
                setValue(verifyBoxF4skY, string.Empty, firstTime, nv);
                setValue(verifyBoxF4skM, string.Empty, firstTime, nv);
                setValue(verifyBoxF4skD, string.Empty, firstTime, nv);

                //開始日4
                setValue(verifyBoxF4stD, string.Empty, firstTime, nv);

                //終了日4
                setValue(verifyBoxF4edD, string.Empty, firstTime, nv);


                //実日数4
                setValue(verifyBoxF4Days, string.Empty, firstTime, nv);
                //転帰4
                //setValue(verifyBoxF4Tenki, string.Empty, firstTime, nv);
            }

            #endregion
            
            #region 負傷5
            //負傷名5
            setValue(verifyBoxF5, app.FushoName5, firstTime, nv);
            if (app.FushoName5 != string.Empty)
            {

                //負傷年月日5
                //20200617102825 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF5fsNengo, DateTimeEx.GetEraNumber(app.FushoDate5).ToString(), firstTime, nv);
                //20200617102825 furukawa ed ////////////////////////
                setValue(verifyBoxF5fsY, DateTimeEx.GetJpYear(app.FushoDate5).ToString(), firstTime, nv);
                setValue(verifyBoxF5fsM, app.FushoDate5.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF5fsD, app.FushoDate5.Day.ToString(), firstTime, nv);

                //初検年月5
                //20200617102831 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF5skNengo, DateTimeEx.GetEraNumber(app.FushoFirstDate5).ToString(), firstTime, nv);
                //20200617102831 furukawa ed ////////////////////////
                setValue(verifyBoxF5skY, DateTimeEx.GetJpYear(app.FushoFirstDate5).ToString(), firstTime, nv);
                setValue(verifyBoxF5skM, app.FushoFirstDate5.Month.ToString(), firstTime, nv);
                setValue(verifyBoxF5skD, app.FushoFirstDate5.Day.ToString(), firstTime, nv);

                //開始日5
                setValue(verifyBoxF5stD, app.FushoStartDate5.Day.ToString(), firstTime, nv);

                //終了日5
                setValue(verifyBoxF5edD, app.FushoFinishDate5.Day.ToString(), firstTime, nv);

                //実日数5
                setValue(verifyBoxF5Days, app.FushoDays5, firstTime, nv);

                //転帰5  -9の場合は空白にする
                //setValue(verifyBoxF5Tenki, app.FushoCourse5 == -9 ? string.Empty : app.FushoCourse5.ToString(), firstTime, nv);
            }
            else
            {
                //負傷年月日5
                //20200617102858 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF5fsNengo, string.Empty, firstTime, nv);
                //20200617102858 furukawa ed ////////////////////////
                setValue(verifyBoxF5fsY, string.Empty, firstTime, nv);
                setValue(verifyBoxF5fsM, string.Empty, firstTime, nv);
                setValue(verifyBoxF5fsD, string.Empty, firstTime, nv);


                //初検日5 
                //20200617102903 furukawa st ////////////////////////
                //負傷名年月日入力、年号削除に伴い不要
                
                //setValue(verifyBoxF5skNengo, string.Empty, firstTime, nv);
                //20200617102903 furukawa ed ////////////////////////
                setValue(verifyBoxF5skY, string.Empty, firstTime, nv);
                setValue(verifyBoxF5skM, string.Empty, firstTime, nv);
                setValue(verifyBoxF5skD, string.Empty, firstTime, nv);

                //開始日5
                setValue(verifyBoxF5stD, string.Empty, firstTime, nv);

                //終了日5
                setValue(verifyBoxF5edD, string.Empty, firstTime, nv);


                //実日数5
                setValue(verifyBoxF5Days, string.Empty, firstTime, nv);
                //転帰5
                //setValue(verifyBoxF5Tenki, string.Empty, firstTime, nv);
            }

            #endregion
            
        }


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelFusho.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合 負傷名欄のみにする
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                panelFusho.Visible = true;

                //施術月
                verifyBoxM.Enabled = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;

            else if (fushoControls.Contains(t)) posFusho = pos;


        }
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }



        /// <summary>
        /// 負傷名がない場合は、その行にTABSTOPさせない
        /// </summary>
        /// <param name="sender"></param>
        private void TabStopFushoControls(Object sender = null)
        {

            #region 引数指定しない場合全てオフ
            if (sender == null)
            {
                verifyBoxF2.TabStop = false;
                verifyBoxF3.TabStop = false;
                verifyBoxF4.TabStop = false;
                verifyBoxF5.TabStop = false;


                verifyBoxF1fsY.TabStop = false;
                verifyBoxF1fsM.TabStop = false;
                verifyBoxF1fsD.TabStop = false;

                verifyBoxF1skY.TabStop = false;
                verifyBoxF1skM.TabStop = false;
                verifyBoxF1skD.TabStop = false;

                verifyBoxF1stD.TabStop = false;
                verifyBoxF1edD.TabStop = false;
                verifyBoxF1Days.TabStop = false;
                



                verifyBoxF2fsY.TabStop = false;
                verifyBoxF2fsM.TabStop = false;
                verifyBoxF2fsD.TabStop = false;

                verifyBoxF2skY.TabStop = false;
                verifyBoxF2skM.TabStop = false;
                verifyBoxF2skD.TabStop = false;

                verifyBoxF2stD.TabStop = false;
                verifyBoxF2edD.TabStop = false;
                verifyBoxF2Days.TabStop = false;
                


                verifyBoxF3fsY.TabStop = false;
                verifyBoxF3fsM.TabStop = false;
                verifyBoxF3fsD.TabStop = false;

                verifyBoxF3skY.TabStop = false;
                verifyBoxF3skM.TabStop = false;
                verifyBoxF3skD.TabStop = false;

                verifyBoxF3stD.TabStop = false;
                verifyBoxF3edD.TabStop = false;
                verifyBoxF3Days.TabStop = false;
                


                verifyBoxF4fsY.TabStop = false;
                verifyBoxF4fsM.TabStop = false;
                verifyBoxF4fsD.TabStop = false;

                verifyBoxF4skY.TabStop = false;
                verifyBoxF4skM.TabStop = false;
                verifyBoxF4skD.TabStop = false;

                verifyBoxF4stD.TabStop = false;
                verifyBoxF4edD.TabStop = false;
                verifyBoxF4Days.TabStop = false;


                verifyBoxF5fsY.TabStop = false;
                verifyBoxF5fsM.TabStop = false;
                verifyBoxF5fsD.TabStop = false;

                verifyBoxF5skY.TabStop = false;
                verifyBoxF5skM.TabStop = false;
                verifyBoxF5skD.TabStop = false;

                verifyBoxF5stD.TabStop = false;
                verifyBoxF5edD.TabStop = false;
                verifyBoxF5Days.TabStop = false;
                

                return;
            }
            #endregion

            #region 負傷名に入ったらTABSTOPをONにする
            VerifyBox v = (VerifyBox)sender;
            switch (v.Name)
            {
                case "verifyBoxF1":

                    verifyBoxF1fsY.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1fsM.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1fsD.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);

                    verifyBoxF1skY.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1skM.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1skD.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);

                    verifyBoxF1stD.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1edD.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    verifyBoxF1Days.TabStop = !(verifyBoxF1.Text.Trim() == string.Empty);
                    

                    verifyBoxF2.TabStop = true;

                    break;
                case "verifyBoxF2":


                    verifyBoxF2fsY.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2fsM.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2fsD.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);

                    verifyBoxF2skY.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2skM.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2skD.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);

                    verifyBoxF2stD.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2edD.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    verifyBoxF2Days.TabStop = !(verifyBoxF2.Text.Trim() == string.Empty);
                    

                    verifyBoxF3.TabStop = true;

                    break;

                case "verifyBoxF3":

                    verifyBoxF3fsY.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3fsM.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3fsD.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);

                    verifyBoxF3skY.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3skM.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3skD.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);

                    verifyBoxF3stD.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3edD.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    verifyBoxF3Days.TabStop = !(verifyBoxF3.Text.Trim() == string.Empty);
                    

                    verifyBoxF4.TabStop = true;
                    break;


                case "verifyBoxF4":

                    verifyBoxF4fsY.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4fsM.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4fsD.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);

                    verifyBoxF4skY.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4skM.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4skD.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);

                    verifyBoxF4stD.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4edD.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    verifyBoxF4Days.TabStop = !(verifyBoxF4.Text.Trim() == string.Empty);
                    

                    verifyBoxF5.TabStop = true;
                    break;

                case "verifyBoxF5":


                    verifyBoxF5fsY.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5fsM.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5fsD.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);

                    verifyBoxF5skY.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5skM.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5skD.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);

                    verifyBoxF5stD.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5edD.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    verifyBoxF5Days.TabStop = !(verifyBoxF5.Text.Trim() == string.Empty);
                    
                    break;

                default:

                    break;

            }
            #endregion

        }


        private void verifyBoxF_TextChanged(object sender, EventArgs e)
        {
            //一旦負傷欄を全てTabStopFalseにする
            TabStopFushoControls(sender);

        }

        private void verifyBoxY_Leave(object sender, EventArgs e)
        {
            if (verifyBoxY.Text == string.Empty)
            {
                //申請書の場合 負傷名欄のみにする
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                panelFusho.Visible = true;

                //施術月
                verifyBoxM.Visible = false;
                labelM.Visible = false;
                verifyBoxM.Enabled = false;
                
            }
        }

    

        private void verifyBoxAppType_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxAppType.Text != "7" && firstTime)
            {
                verifyBoxF1.Enabled = true;
                verifyBoxF2.Enabled = true;
                verifyBoxF3.Enabled = true;
                verifyBoxF4.Enabled = true;
                verifyBoxF5.Enabled = true;
            }
            else
            {
                verifyBoxF1.Enabled = false;
                verifyBoxF2.Enabled = false;
                verifyBoxF3.Enabled = false;
                verifyBoxF4.Enabled = false;
                verifyBoxF5.Enabled = false;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
