﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.NagoyashiKokuho
{
    class fusho_reason
    {
        //[DB.DbAttribute.PrimaryKey]
        public int memberdataid { get; set; }             //点検対象一覧のIDを保持し、どのレコードを持ってきたか分かるようにする
        public int cym { get; set; }                      //メホール処理年月
        public string reason { get; set; }                //負傷理由の文章

        public int inputuser1 { get; set; }
        public int inputuser2 { get; set; }

        [DB.DbAttribute.PrimaryKey]
        public int aid { get; set; }                      //どのappに紐付くか
        [DB.DbAttribute.PrimaryKey]
        public int reason_no { get; set; }                //1-5

        //public string reason { get; set; }                //負傷理由の文章



        //DBからロード
        public static List<fusho_reason> SelectFromAid(int aid)
        {
            List<fusho_reason> lst = new List<fusho_reason>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from fusho_reason where aid={aid} order by reason_no");
            var lstrdr=cmd.TryExecuteReaderList();

            for (int cnt = 0; cnt < lstrdr.Count; cnt++)
            {
                fusho_reason cls = new fusho_reason();
                cls.memberdataid = int.Parse(lstrdr[cnt][0].ToString());
                cls.cym = int.Parse(lstrdr[cnt][1].ToString());
                cls.aid = int.Parse(lstrdr[cnt][2].ToString());
                cls.reason_no = int.Parse(lstrdr[cnt][3].ToString());
                cls.reason = lstrdr[cnt][4].ToString().Trim();

                if (int.TryParse(lstrdr[cnt][5].ToString(), out int tmp1)) cls.inputuser1 = tmp1;
                if (int.TryParse(lstrdr[cnt][6].ToString(), out int tmp2)) cls.inputuser2 = tmp2;


                lst.Add(cls);

            }

            //20200529153259 furukawa st ////////////////////////
            //コマンドを解放していなかったのでコネクションを限界数まで使ってた
            
            cmd.Dispose();
            //20200529153259 furukawa ed ////////////////////////
            return lst;

        }



        //20201013112529 furukawa st ////////////////////////
        //元々入っているユーザを取得しておかないとNewしたときに０で登録してしまう
        
        /// <summary>
        /// ユーザ名取得
        /// </summary>
        /// <param name="cls">fusho_reason(memberdataid,reason_no入りのやつ)</param>
        public static void GetInputUser(fusho_reason cls)
        {
            List<fusho_reason> lst =
            DB.Main.Select<fusho_reason>($"memberdataid={cls.memberdataid} and reason_no={cls.reason_no}").ToList();
            if (lst.Count == 0)
            {
                cls.inputuser1 = User.CurrentUser.UserID;
                cls.inputuser2 = 0;

            }
            else
            {
                cls.inputuser1 = lst[0].inputuser1 != 0 ? lst[0].inputuser1 : User.CurrentUser.UserID;
                cls.inputuser2 = lst[0].inputuser2 != 0 ? lst[0].inputuser1 : User.CurrentUser.UserID;
            }
        
        }
        //20201013112529 furukawa ed ////////////////////////



        public static void Update()
        {

        }
        
    }
}
