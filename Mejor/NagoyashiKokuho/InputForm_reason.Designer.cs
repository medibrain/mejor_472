﻿namespace Mejor.NagoyashiKokuho
{
    partial class InputForm_reason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.lblwareki = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.btnNextTiff = new System.Windows.Forms.Button();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelFusho = new System.Windows.Forms.Panel();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.lblfushoname5 = new System.Windows.Forms.Label();
            this.lblfushoname4 = new System.Windows.Forms.Label();
            this.lblfushoname3 = new System.Windows.Forms.Label();
            this.lblfushoname2 = new System.Windows.Forms.Label();
            this.lblfushoname1 = new System.Windows.Forms.Label();
            this.btncpy4 = new System.Windows.Forms.Button();
            this.btncpy3 = new System.Windows.Forms.Button();
            this.btncpy2 = new System.Windows.Forms.Button();
            this.btncpy1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridRefRece = new System.Windows.Forms.DataGridView();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelOCR = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.verifyBoxHnumM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxClinicCode = new Mejor.VerifyBox();
            this.verifyBoxPref = new Mejor.VerifyBox();
            this.verifyBoxAppType = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelFamily = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelPref = new System.Windows.Forms.Label();
            this.lblwarning = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelFusho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).BeginInit();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 250;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 22);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(190, 22);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // lblwareki
            // 
            this.lblwareki.AutoSize = true;
            this.lblwareki.Location = new System.Drawing.Point(67, 22);
            this.lblwareki.Name = "lblwareki";
            this.lblwareki.Size = new System.Drawing.Size(29, 12);
            this.lblwareki.TabIndex = 1;
            this.lblwareki.Text = "和暦";
            this.lblwareki.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.btnNextTiff);
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // btnNextTiff
            // 
            this.btnNextTiff.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnNextTiff.Location = new System.Drawing.Point(6, 3);
            this.btnNextTiff.Name = "btnNextTiff";
            this.btnNextTiff.Size = new System.Drawing.Size(100, 30);
            this.btnNextTiff.TabIndex = 13;
            this.btnNextTiff.Text = "次の画像";
            this.btnNextTiff.UseVisualStyleBackColor = true;
            this.btnNextTiff.Click += new System.EventHandler(this.btnNextTiff_Click);
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-196, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelFusho);
            this.panelRight.Controls.Add(this.dataGridRefRece);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.lblwareki);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.lblwarning);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // panelFusho
            // 
            this.panelFusho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelFusho.Controls.Add(this.verifyBoxF5);
            this.panelFusho.Controls.Add(this.verifyBoxF4);
            this.panelFusho.Controls.Add(this.verifyBoxF3);
            this.panelFusho.Controls.Add(this.verifyBoxF2);
            this.panelFusho.Controls.Add(this.verifyBoxF1);
            this.panelFusho.Controls.Add(this.lblfushoname5);
            this.panelFusho.Controls.Add(this.lblfushoname4);
            this.panelFusho.Controls.Add(this.lblfushoname3);
            this.panelFusho.Controls.Add(this.lblfushoname2);
            this.panelFusho.Controls.Add(this.lblfushoname1);
            this.panelFusho.Controls.Add(this.btncpy4);
            this.panelFusho.Controls.Add(this.btncpy3);
            this.panelFusho.Controls.Add(this.btncpy2);
            this.panelFusho.Controls.Add(this.btncpy1);
            this.panelFusho.Controls.Add(this.label10);
            this.panelFusho.Controls.Add(this.label8);
            this.panelFusho.Controls.Add(this.label7);
            this.panelFusho.Controls.Add(this.label2);
            this.panelFusho.Controls.Add(this.label1);
            this.panelFusho.Controls.Add(this.label9);
            this.panelFusho.Location = new System.Drawing.Point(3, 308);
            this.panelFusho.Name = "panelFusho";
            this.panelFusho.Size = new System.Drawing.Size(1000, 380);
            this.panelFusho.TabIndex = 40;
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(124, 265);
            this.verifyBoxF5.MaxLength = 1000;
            this.verifyBoxF5.Multiline = true;
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = true;
            this.verifyBoxF5.Size = new System.Drawing.Size(740, 60);
            this.verifyBoxF5.TabIndex = 20;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(124, 201);
            this.verifyBoxF4.MaxLength = 1000;
            this.verifyBoxF4.Multiline = true;
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = true;
            this.verifyBoxF4.Size = new System.Drawing.Size(740, 60);
            this.verifyBoxF4.TabIndex = 15;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(124, 135);
            this.verifyBoxF3.MaxLength = 1000;
            this.verifyBoxF3.Multiline = true;
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = true;
            this.verifyBoxF3.Size = new System.Drawing.Size(740, 60);
            this.verifyBoxF3.TabIndex = 10;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(124, 69);
            this.verifyBoxF2.MaxLength = 1000;
            this.verifyBoxF2.Multiline = true;
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = true;
            this.verifyBoxF2.Size = new System.Drawing.Size(740, 60);
            this.verifyBoxF2.TabIndex = 5;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(124, 3);
            this.verifyBoxF1.MaxLength = 1000;
            this.verifyBoxF1.Multiline = true;
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = true;
            this.verifyBoxF1.Size = new System.Drawing.Size(740, 60);
            this.verifyBoxF1.TabIndex = 0;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // lblfushoname5
            // 
            this.lblfushoname5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblfushoname5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfushoname5.Location = new System.Drawing.Point(3, 285);
            this.lblfushoname5.Name = "lblfushoname5";
            this.lblfushoname5.Size = new System.Drawing.Size(110, 37);
            this.lblfushoname5.TabIndex = 21;
            this.lblfushoname5.Text = "負傷名1";
            // 
            // lblfushoname4
            // 
            this.lblfushoname4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblfushoname4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfushoname4.Location = new System.Drawing.Point(3, 224);
            this.lblfushoname4.Name = "lblfushoname4";
            this.lblfushoname4.Size = new System.Drawing.Size(110, 37);
            this.lblfushoname4.TabIndex = 21;
            this.lblfushoname4.Text = "負傷名1";
            // 
            // lblfushoname3
            // 
            this.lblfushoname3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblfushoname3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfushoname3.Location = new System.Drawing.Point(3, 158);
            this.lblfushoname3.Name = "lblfushoname3";
            this.lblfushoname3.Size = new System.Drawing.Size(110, 37);
            this.lblfushoname3.TabIndex = 21;
            this.lblfushoname3.Text = "負傷名1";
            // 
            // lblfushoname2
            // 
            this.lblfushoname2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblfushoname2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfushoname2.Location = new System.Drawing.Point(3, 92);
            this.lblfushoname2.Name = "lblfushoname2";
            this.lblfushoname2.Size = new System.Drawing.Size(110, 37);
            this.lblfushoname2.TabIndex = 21;
            this.lblfushoname2.Text = "負傷名1";
            // 
            // lblfushoname1
            // 
            this.lblfushoname1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblfushoname1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfushoname1.Location = new System.Drawing.Point(3, 23);
            this.lblfushoname1.Name = "lblfushoname1";
            this.lblfushoname1.Size = new System.Drawing.Size(110, 37);
            this.lblfushoname1.TabIndex = 21;
            this.lblfushoname1.Text = "負傷名1";
            // 
            // btncpy4
            // 
            this.btncpy4.Location = new System.Drawing.Point(877, 218);
            this.btncpy4.Name = "btncpy4";
            this.btncpy4.Size = new System.Drawing.Size(68, 30);
            this.btncpy4.TabIndex = 16;
            this.btncpy4.TabStop = false;
            this.btncpy4.Text = "下にコピー";
            this.btncpy4.UseVisualStyleBackColor = true;
            this.btncpy4.Click += new System.EventHandler(this.copy_reason);
            // 
            // btncpy3
            // 
            this.btncpy3.Location = new System.Drawing.Point(877, 151);
            this.btncpy3.Name = "btncpy3";
            this.btncpy3.Size = new System.Drawing.Size(68, 30);
            this.btncpy3.TabIndex = 11;
            this.btncpy3.TabStop = false;
            this.btncpy3.Text = "下にコピー";
            this.btncpy3.UseVisualStyleBackColor = true;
            this.btncpy3.Click += new System.EventHandler(this.copy_reason);
            // 
            // btncpy2
            // 
            this.btncpy2.Location = new System.Drawing.Point(877, 84);
            this.btncpy2.Name = "btncpy2";
            this.btncpy2.Size = new System.Drawing.Size(68, 30);
            this.btncpy2.TabIndex = 7;
            this.btncpy2.TabStop = false;
            this.btncpy2.Text = "下にコピー";
            this.btncpy2.UseVisualStyleBackColor = true;
            this.btncpy2.Click += new System.EventHandler(this.copy_reason);
            // 
            // btncpy1
            // 
            this.btncpy1.Location = new System.Drawing.Point(877, 21);
            this.btncpy1.Name = "btncpy1";
            this.btncpy1.Size = new System.Drawing.Size(68, 30);
            this.btncpy1.TabIndex = 3;
            this.btncpy1.TabStop = false;
            this.btncpy1.Text = "下にコピー";
            this.btncpy1.UseVisualStyleBackColor = true;
            this.btncpy1.Click += new System.EventHandler(this.copy_reason);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(870, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "最大1000文字";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "負傷原因5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "負傷原因4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "負傷原因3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "負傷原因2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷原因1";
            // 
            // dataGridRefRece
            // 
            this.dataGridRefRece.AllowUserToAddRows = false;
            this.dataGridRefRece.AllowUserToDeleteRows = false;
            this.dataGridRefRece.AllowUserToResizeRows = false;
            this.dataGridRefRece.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridRefRece.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridRefRece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridRefRece.Location = new System.Drawing.Point(8, 679);
            this.dataGridRefRece.Name = "dataGridRefRece";
            this.dataGridRefRece.ReadOnly = true;
            this.dataGridRefRece.RowHeadersVisible = false;
            this.dataGridRefRece.RowTemplate.Height = 21;
            this.dataGridRefRece.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRefRece.Size = new System.Drawing.Size(674, 10);
            this.dataGridRefRece.StandardTab = true;
            this.dataGridRefRece.TabIndex = 60;
            this.dataGridRefRece.TabStop = false;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 11);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Leave += new System.EventHandler(this.verifyBoxY_Leave);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(155, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 100);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 200);
            this.scrollPictureControl1.TabIndex = 300;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 695);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(29, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.label30);
            this.panelHnum.Controls.Add(this.verifyBoxRatio);
            this.panelHnum.Controls.Add(this.verifyBoxHnumM);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxClinicCode);
            this.panelHnum.Controls.Add(this.verifyBoxPref);
            this.panelHnum.Controls.Add(this.verifyBoxAppType);
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelFamily);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label6);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Controls.Add(this.label23);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.label24);
            this.panelHnum.Controls.Add(this.labelPref);
            this.panelHnum.Location = new System.Drawing.Point(98, 0);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(850, 100);
            this.panelHnum.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(528, 11);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 24);
            this.label30.TabIndex = 26;
            this.label30.Text = "給付\r\n割合";
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(557, 9);
            this.verifyBoxRatio.MaxLength = 1;
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxRatio.TabIndex = 12;
            this.verifyBoxRatio.TextV = "";
            // 
            // verifyBoxHnumM
            // 
            this.verifyBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumM.Location = new System.Drawing.Point(8, 63);
            this.verifyBoxHnumM.Name = "verifyBoxHnumM";
            this.verifyBoxHnumM.NewLine = false;
            this.verifyBoxHnumM.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnumM.TabIndex = 15;
            this.verifyBoxHnumM.TextV = "";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(109, 63);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(111, 23);
            this.verifyBoxHnum.TabIndex = 16;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxClinicCode
            // 
            this.verifyBoxClinicCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicCode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicCode.Location = new System.Drawing.Point(414, 11);
            this.verifyBoxClinicCode.Name = "verifyBoxClinicCode";
            this.verifyBoxClinicCode.NewLine = false;
            this.verifyBoxClinicCode.Size = new System.Drawing.Size(96, 23);
            this.verifyBoxClinicCode.TabIndex = 8;
            this.verifyBoxClinicCode.TextV = "";
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPref.Location = new System.Drawing.Point(299, 11);
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.NewLine = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxPref.TabIndex = 6;
            this.verifyBoxPref.TextV = "";
            // 
            // verifyBoxAppType
            // 
            this.verifyBoxAppType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAppType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAppType.Location = new System.Drawing.Point(194, 11);
            this.verifyBoxAppType.Name = "verifyBoxAppType";
            this.verifyBoxAppType.NewLine = false;
            this.verifyBoxAppType.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxAppType.TabIndex = 4;
            this.verifyBoxAppType.TextV = "";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(226, 63);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 20;
            this.verifyBoxFamily.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(557, 63);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 29;
            this.verifyBoxBD.TextV = "";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(514, 63);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 28;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(471, 63);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 27;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(420, 63);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 26;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(345, 63);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 22;
            this.verifyBoxSex.TextV = "";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(6, 48);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(224, 44);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(59, 12);
            this.labelFamily.TabIndex = 12;
            this.labelFamily.Text = "本人/家族";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(417, 49);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 18;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(343, 48);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 15;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(446, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "昭:3\r\n平:4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(372, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(583, 73);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 25;
            this.label16.Text = "日";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(254, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 36);
            this.label6.TabIndex = 14;
            this.label6.Text = "本人:2 六歳:4\r\n家族:6 高齢:8\r\n高７:0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(540, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 23;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(497, 73);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 21;
            this.label18.Text = "年";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(141, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "申請区分";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(141, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 3;
            this.label19.Text = "針灸は[7]\r\n";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(361, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 24);
            this.label24.TabIndex = 7;
            this.label24.Text = "医療機関\r\nコード";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPref
            // 
            this.labelPref.AutoSize = true;
            this.labelPref.Location = new System.Drawing.Point(246, 13);
            this.labelPref.Name = "labelPref";
            this.labelPref.Size = new System.Drawing.Size(53, 24);
            this.labelPref.TabIndex = 5;
            this.labelPref.Text = "都道府県\r\n番号";
            this.labelPref.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblwarning
            // 
            this.lblwarning.AutoSize = true;
            this.lblwarning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblwarning.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblwarning.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblwarning.Location = new System.Drawing.Point(60, 43);
            this.lblwarning.Name = "lblwarning";
            this.lblwarning.Size = new System.Drawing.Size(628, 21);
            this.lblwarning.TabIndex = 301;
            this.lblwarning.Text = "点検対象者IDが申請書と紐付いていません。先に通常入力してください";
            this.lblwarning.Visible = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm_reason
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm_reason";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelFusho.ResumeLayout(false);
            this.panelFusho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).EndInit();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label lblwareki;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelFusho;
        private System.Windows.Forms.Label label9;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.DataGridView dataGridRefRece;
        private System.Windows.Forms.Panel panelHnum;
        private System.Windows.Forms.Label label30;
        private VerifyBox verifyBoxRatio;
        private VerifyBox verifyBoxHnumM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxClinicCode;
        private VerifyBox verifyBoxPref;
        private VerifyBox verifyBoxAppType;
        private VerifyBox verifyBoxFamily;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelPref;
        private System.Windows.Forms.Button btncpy4;
        private System.Windows.Forms.Button btncpy3;
        private System.Windows.Forms.Button btncpy2;
        private System.Windows.Forms.Button btncpy1;
        private VerifyBox verifyBoxF5;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxF4;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxF3;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxF2;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label lblfushoname5;
        private System.Windows.Forms.Label lblfushoname4;
        private System.Windows.Forms.Label lblfushoname3;
        private System.Windows.Forms.Label lblfushoname2;
        private System.Windows.Forms.Label lblfushoname1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblwarning;
        private System.Windows.Forms.Button btnNextTiff;
    }
}