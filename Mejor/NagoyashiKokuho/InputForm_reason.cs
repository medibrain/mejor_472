﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.NagoyashiKokuho

{
    public partial class InputForm_reason : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);        
        Point posFushoReason = new Point(1000, 400);

        Control[] ymControls, fushoControls, costControls, hnumControls;

        public InputForm_reason(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] {
                verifyBoxY, verifyBoxM,
            };


            //上段
            hnumControls = new Control[]
            {
                verifyBoxHnum,verifyBoxHnumM,
                verifyBoxBD,verifyBoxBE,verifyBoxBM,verifyBoxBY,
                verifyBoxSex,verifyBoxRatio,verifyBoxFamily,verifyBoxClinicCode,verifyBoxPref,verifyBoxAppType,
            };

                   
            //負傷名箇所
            fushoControls = new Control[] {
               verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5,
            };

        

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;            
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";

            //保険者独自扱いなので、InputStatusSP
            dataGridViewPlist.Columns[nameof(App.InputStatusSP)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusSP)].DisplayIndex = 1;

            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            panelHnum.Visible = false;//使わないので隠す
            panelFusho.Visible = false;            
            dataGridRefRece.Visible = false;//使わないので隠す


            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            focusBack(false);

        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFushoReason;
            
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkInput(App app)
        {
            hasError = false;
            List<fusho_reason> lstFusho = new List<fusho_reason>();
                        
            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month > 12);



            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //db登録
            app.MediYear = year;
            app.MediMonth = month;

            return true;

        }



        /// <summary>
        /// 負傷理由登録
        /// </summary>
        /// <param name="app"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        private bool updateFusho_reason(App app,DB.Transaction tran)
        {
            List<fusho_reason> lstFusho = new List<fusho_reason>();

            //20201013100606 furukawa st ////////////////////////
            //空欄でも登録可能とする・ユーザ登録


            //負傷理由1

            fusho_reason cls_fusho1 = new fusho_reason();
            cls_fusho1.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            cls_fusho1.cym = app.CYM;
            cls_fusho1.aid = app.Aid;
            cls_fusho1.reason_no = 1;
            cls_fusho1.reason = verifyBoxF1.Text.Trim();

            //20201013113025 furukawa st ////////////////////////
            //ユーザを取得            
            fusho_reason.GetInputUser(cls_fusho1);
            //20201013113025 furukawa ed ////////////////////////

            lstFusho.Add(cls_fusho1);

            //負傷理由2

            fusho_reason cls_fusho2 = new fusho_reason();
            cls_fusho2.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            cls_fusho2.cym = app.CYM;
            cls_fusho2.aid = app.Aid;
            cls_fusho2.reason_no = 2;
            cls_fusho2.reason = verifyBoxF2.Text.Trim();


            //20201013113110 furukawa st ////////////////////////
            //ユーザを取得   
            fusho_reason.GetInputUser(cls_fusho2);
            //20201013113110 furukawa ed ////////////////////////

            lstFusho.Add(cls_fusho2);
            


            //負傷理由3

            fusho_reason cls_fusho3 = new fusho_reason();
            cls_fusho3.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            cls_fusho3.cym = app.CYM;
            cls_fusho3.aid = app.Aid;
            cls_fusho3.reason_no = 3;
            cls_fusho3.reason = verifyBoxF3.Text.Trim();

            //20201013113126 furukawa st ////////////////////////
            //ユーザを取得
            fusho_reason.GetInputUser(cls_fusho3);
            //20201013113126 furukawa ed ////////////////////////

            lstFusho.Add(cls_fusho3);
            
            

            //負傷理由4

            fusho_reason cls_fusho4 = new fusho_reason();
            cls_fusho4.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            cls_fusho4.cym = app.CYM;
            cls_fusho4.aid = app.Aid;
            cls_fusho4.reason_no = 4;
            cls_fusho4.reason = verifyBoxF4.Text.Trim();

            //20201013113206 furukawa st ////////////////////////
            //ユーザを取得
            fusho_reason.GetInputUser(cls_fusho4);
            //20201013113206 furukawa ed ////////////////////////

            lstFusho.Add(cls_fusho4);


            //負傷理由5

            fusho_reason cls_fusho5 = new fusho_reason();
            cls_fusho5.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            cls_fusho5.cym = app.CYM;
            cls_fusho5.aid = app.Aid;
            cls_fusho5.reason_no = 5;
            cls_fusho5.reason = verifyBoxF5.Text.Trim();

            //20201013113221 furukawa st ////////////////////////
            //ユーザを取得
            fusho_reason.GetInputUser(cls_fusho5);
            //20201013113221 furukawa ed ////////////////////////

            lstFusho.Add(cls_fusho5);

            #region old


            ////負傷理由1
            //if (verifyBoxF1.Text.Trim() != string.Empty && app.TaggedDatas.GeneralString1 != string.Empty)
            //{

            //    fusho_reason cls_fusho = new fusho_reason();
            //    cls_fusho.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            //    cls_fusho.cym = app.CYM;
            //    cls_fusho.aid = app.Aid;
            //    cls_fusho.reason_no = 1;
            //    cls_fusho.reason = verifyBoxF1.Text.Trim();
            //    lstFusho.Add(cls_fusho);
            //}
            ////負傷理由2
            //if (verifyBoxF2.Text.Trim() != string.Empty && app.TaggedDatas.GeneralString1 != string.Empty)
            //{
            //    fusho_reason cls_fusho = new fusho_reason();
            //    cls_fusho.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            //    cls_fusho.cym = app.CYM;
            //    cls_fusho.aid = app.Aid;
            //    cls_fusho.reason_no = 2;
            //    cls_fusho.reason = verifyBoxF2.Text.Trim();
            //    lstFusho.Add(cls_fusho);
            //}
            ////負傷理由3
            //if (verifyBoxF3.Text.Trim() != string.Empty && app.TaggedDatas.GeneralString1 != string.Empty)
            //{
            //    fusho_reason cls_fusho = new fusho_reason();
            //    cls_fusho.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            //    cls_fusho.cym = app.CYM;
            //    cls_fusho.aid = app.Aid;
            //    cls_fusho.reason_no = 3;
            //    cls_fusho.reason = verifyBoxF3.Text.Trim();
            //    lstFusho.Add(cls_fusho);
            //}
            ////負傷理由4
            //if (verifyBoxF4.Text.Trim() != string.Empty && app.TaggedDatas.GeneralString1 != string.Empty)
            //{
            //    fusho_reason cls_fusho = new fusho_reason();
            //    cls_fusho.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            //    cls_fusho.cym = app.CYM;
            //    cls_fusho.aid = app.Aid;
            //    cls_fusho.reason_no = 4;
            //    cls_fusho.reason = verifyBoxF4.Text.Trim();
            //    lstFusho.Add(cls_fusho);
            //}
            ////負傷理由5
            //if (verifyBoxF5.Text.Trim() != string.Empty && app.TaggedDatas.GeneralString1 != string.Empty)
            //{
            //    fusho_reason cls_fusho = new fusho_reason();
            //    cls_fusho.memberdataid = int.Parse(app.TaggedDatas.GeneralString1);
            //    cls_fusho.cym = app.CYM;
            //    cls_fusho.aid = app.Aid;
            //    cls_fusho.reason_no = 5;
            //    cls_fusho.reason = verifyBoxF5.Text.Trim();
            //    lstFusho.Add(cls_fusho);
            //}

            #endregion

            //20201013100606 furukawa ed ////////////////////////




            //負傷理由登録
            //DB.Transaction tran;
            //tran = DB.Main.CreateTransaction();

            try
            {
                for (int cnt = 0; cnt < lstFusho.Count; cnt++)
                {
                    //あったら更新、無ければ登録
                    IEnumerable<fusho_reason> lst= DB.Main.Select<fusho_reason>($"aid={lstFusho[cnt].aid} and reason_no={lstFusho[cnt].reason_no}");
                    if (lst.Count<fusho_reason>()== 0)
                    {
                        if (!DB.Main.Insert<fusho_reason>(lstFusho[cnt], tran)) return false;
                    }
                    else
                    {
                        if (!DB.Main.Update<fusho_reason>(lstFusho[cnt], tran)) return false;
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

            return true;

        }

        /// <summary>
        /// 新規継続
        /// </summary>
        /// <param name="app">Application</param>
        /// <param name="outapp">Application</param>
        private void chkNewContinue(App app,out App outapp)
        {
            bool flgNew = false;//新規の場合True
            DateTime sejutuYM = DateTimeEx.ToDateTime(int.Parse(app.YM + "01"));

            //施術年月より初検日が大きい場合は新規と見なす
            //一つでも継続があれば継続
            if ((app.FushoFirstDate1!=DateTime.MinValue && app.FushoFirstDate1 < sejutuYM) ||
                (app.FushoFirstDate2!=DateTime.MinValue && app.FushoFirstDate2 < sejutuYM) ||
                (app.FushoFirstDate3!=DateTime.MinValue && app.FushoFirstDate3 < sejutuYM) ||
                (app.FushoFirstDate4!=DateTime.MinValue && app.FushoFirstDate4 < sejutuYM) ||
                (app.FushoFirstDate5 != DateTime.MinValue && app.FushoFirstDate5 < sejutuYM)  ) flgNew = false;
            else
                flgNew = true;
            

            app.NewContType = flgNew==true ? NEW_CONT.新規 : NEW_CONT.継続;
            outapp = app;
        }


        /// <summary>
        /// 負傷ごとの日付チェック
        /// </summary>
        /// <param name="vbNengo">年号のVerifybox</param>
        /// <param name="vbY">和暦年のVerifybox</param>
        /// <param name="vbM">月のVerifybox</param>
        /// <param name="vbD">日のVerifybox</param>    
        /// <returns>datetime日付</returns>
        private DateTime chkDate(VerifyBox vbNengo,VerifyBox vbY,VerifyBox vbM,VerifyBox vbD)
        {

            //年号
            int sG = vbNengo.GetIntValue();
            setStatus(vbNengo, sG < 1 || 5 < sG);

            //初検年月
            int sY = vbY.GetIntValue();
            int sM = vbM.GetIntValue();
            int sD = vbD.GetIntValue();

            DateTime dtTmp = DateTime.MinValue;

            //西暦変換
            
            int.TryParse(sG.ToString() + sY.ToString("00") + sM.ToString("00"), out int inttmp);
            if (inttmp > 0) sY = DateTimeEx.GetAdYearMonthFromJyymm(inttmp) / 100;

            if (!DateTimeEx.IsDate(sY, sM, sD))
            {
                setStatus(vbY, true);
                setStatus(vbM, true);
                setStatus(vbD, true);
            }
            else
            {
                setStatus(vbY, false);
                setStatus(vbM, false);
                setStatus(vbD, false);
                dtTmp = new DateTime(sY, sM, sD);
            }
            return dtTmp;
        }



        /// <summary>
        /// 負傷ごとの日付チェック
        /// </summary>
        /// <param name="ADYear">西暦年</param>
        /// <param name="vbM">施術年月の月のVerifybox</param>
        /// <param name="vbD">日のVerifybox</param>
        /// <returns>datetime日付</returns>
        private DateTime chkDate(int ADYear, VerifyBox vbM, VerifyBox vbD)
        {
            //月
            int sM = vbM.GetIntValue();
            //日
            int sD = vbD.GetIntValue();

            DateTime dttmp = DateTime.MinValue;

            if (!DateTimeEx.IsDate(ADYear, sM, sD))
            {
                setStatus(vbD, true);                
            }
            else
            {
                setStatus(vbD, false);                
                dttmp = new DateTime(ADYear, sM, sD);
            }
            return dttmp;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.独自処理2)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkInput(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;
            

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //独自扱いとする
                var ut = firstTime ? App.UPDATE_TYPE.Sp1 : App.UPDATE_TYPE.Sp2;
            
                if (firstTime && app.UfirstEx == 0)
                {
                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Extra1, 0,DateTime.Now-dtstart_core ,jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.Extra1, 0, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////

                }
                else if (!firstTime && app.UsecondEx == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;

                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換

                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Extra2, secondMissCount,DateTime.Now-dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.Extra2, secondMissCount, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;


                //負傷理由更新
                if (!updateFusho_reason(app,tran)) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //20200529120900 furukawa st ////////////////////////
            //点検対象者IDが申請書と紐付いていない場合、後から紐づけるのが面倒なので先に通常入力させる
            
            
            if (app.TaggedDatas.GeneralString1 == string.Empty)
            {            
                panelRight.Enabled = false;
                lblwarning.Visible = true;
            }
            else
            {                
                panelRight.Enabled = true;
                lblwarning.Visible = false;
            }
            //20200529120900 furukawa ed ////////////////////////


            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //負傷名リセット
            lblfushoname1.Text = string.Empty;
            lblfushoname2.Text = string.Empty;
            lblfushoname3.Text = string.Empty;
            lblfushoname4.Text = string.Empty;
            lblfushoname5.Text = string.Empty;

            //負傷名
            lblfushoname1.Text = app.FushoName1 == string.Empty ? string.Empty : app.FushoName1.Trim();
            lblfushoname2.Text = app.FushoName2 == string.Empty ? string.Empty : app.FushoName2.Trim();
            lblfushoname3.Text = app.FushoName3 == string.Empty ? string.Empty : app.FushoName3.Trim();
            lblfushoname4.Text = app.FushoName4 == string.Empty ? string.Empty : app.FushoName4.Trim();
            lblfushoname5.Text = app.FushoName5 == string.Empty ? string.Empty : app.FushoName5.Trim();


            //20201013114611 furukawa st ////////////////////////
            //独自１入力表示は空欄、2回目は表示

            ////入力ユーザー表示(拡張入力)
            //labelInputerName.Text = "独自入力1:  " + User.GetUserName(app.UfirstEx) +
            //    "\r\n独自入力2:  " + User.GetUserName(app.UsecondEx);

            //20201013114611 furukawa ed ////////////////////////


            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.独自処理1))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
                //20201013114611 furukawa st ////////////////////////
                //独自１入力表示は空欄、2回目は表示
                
                //入力ユーザー表示
                labelInputerName.Text = "独自入力1:  " +
                    "\r\n独自入力2:  " ;
                //20201013114611 furukawa ed ////////////////////////

            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.7f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.独自処理2);
            
            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
              
                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);


                //負傷理由ロード箇所

                List<fusho_reason> lst=fusho_reason.SelectFromAid(app.Aid);
                //理由
                if (lst.Count > 0) setValue(verifyBoxF1, lst[0].reason.Trim(), firstTime, nv);
                if (lst.Count > 1) setValue(verifyBoxF2, lst[1].reason.Trim(), firstTime, nv);
                if (lst.Count > 2) setValue(verifyBoxF3, lst[2].reason.Trim(), firstTime, nv);
                if (lst.Count > 3) setValue(verifyBoxF4, lst[3].reason.Trim(), firstTime, nv);
                if (lst.Count > 4) setValue(verifyBoxF5, lst[4].reason.Trim(), firstTime, nv);


                labelInputerName.Text = "独自入力1:  " + User.GetUserName(lst[0].inputuser1) +
                    "\r\n独自入力2:  " + User.GetUserName(lst[0].inputuser2);
            }
        }


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelFusho.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合 負傷名欄のみにする
                panelHnum.Visible = false;
             //   panelTotal.Visible = false;
                panelFusho.Visible = true;

                //施術月
                verifyBoxM.Enabled = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;

            else if (fushoControls.Contains(t)) posFushoReason = pos;


        }
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
         

            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            //verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            //verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            //verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }


        //20200529124708 furukawa st ////////////////////////
        //理由が裏に書かれている場合もあるので画像だけ見せる
        
        private void btnNextTiff_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null) return;
            NextImageForm f = new NextImageForm(app);

            f.TopMost = true;
            f.Show();
            
        }
        //20200529124708 furukawa ed ////////////////////////




        /// <summary>
        /// 文章のコピーボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void copy_reason(object sender, EventArgs e)
        {
            if (sender == btncpy1) verifyBoxF2.Text = verifyBoxF1.Text.Trim();
            if (sender == btncpy2) verifyBoxF3.Text = verifyBoxF2.Text.Trim();
            if (sender == btncpy3) verifyBoxF4.Text = verifyBoxF3.Text.Trim();
            if (sender == btncpy4) verifyBoxF5.Text = verifyBoxF4.Text.Trim();

        }

        private void verifyBoxY_Leave(object sender, EventArgs e)
        {
            if (verifyBoxY.Text == string.Empty)
            {
                //申請書の場合 負傷名欄のみにする
                panelHnum.Visible = false;
                //panelTotal.Visible = false;
                panelFusho.Visible = true;

                //施術月
                verifyBoxM.Visible = false;
                labelM.Visible = false;
                verifyBoxM.Enabled = false;
                
            }
        }

    
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
