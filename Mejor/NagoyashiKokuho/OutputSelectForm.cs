﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.NagoyashiKokuho
{
    //出力リストの番号を選ぶだけ
        
    public partial class OutputSelectForm : Form
    {
        public int ListNo = 0;
   

        public OutputSelectForm()
        {
            InitializeComponent();
        }
        

        private void btn1_Click(object sender, EventArgs e)
        {
            ListNo = 1;
            this.Close();
        }

 
        private void btn3_Click(object sender, EventArgs e)
        {
            ListNo = 2;
            this.Close();
        }

       
        private void btn5_Click(object sender, EventArgs e)
        {
            ListNo = 3;
            this.Close();
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            ListNo = 4;
            this.Close();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            ListNo = 9;
            this.Close();
        }
    }
}
