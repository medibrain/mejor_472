﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20201216124249 furukawa 名古屋市国保用負傷理由入力候補リスト



namespace Mejor.NagoyashiKokuho
{

    public partial class fusho_counter : Form
    {
        DataTable dtInputList = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        List<App> lstApp=new List<App>();
        int cym = 0;

        DataTable dtDisp = new DataTable();

        public fusho_counter(int _cym)
        {
            InitializeComponent();
            cym = _cym;

            WaitFormSimple wf = new WaitFormSimple();
            wf.StartPosition = FormStartPosition.CenterScreen;
            System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());


            LoadData();

            wf.InvokeCloseDispose();

            labelcym.Text = cym.ToString().Substring(0, 4) + '/' + cym.ToString().Substring(4);


        }

        

        /// <summary>
        /// 3部位以上の申請書の独自入力進捗表示
        /// </summary>
        public void LoadData()
        {

            lstApp = App.GetAppsWithWhere($" where cym={cym} and bui>=3").ToList();

            //20201222105428 furukawa st ////////////////////////
            //0件の時は返す            
            if (lstApp.Count == 0) return;
            //20201222105428 furukawa ed ////////////////////////


            lstApp.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            dtDisp.Columns.Add("groupid");
            dtDisp.Columns.Add("app");
            dtDisp.Columns.Add("input1");
            dtDisp.Columns.Add("input2");
            dtDisp.Columns.Add("prog");

            int gid = lstApp[0].GroupID;
            int cntapptotal = 0, cntapp = 0, cnt1 = 0, cnt2 = 0;
            float prog = 0.0f;
            string err = string.Empty;
            App a = lstApp[0];
            int r = 0;

            do
            {
                //最終レコードの場合は飛ばす
                if (lstApp.Count > r) a = lstApp[r];                


                
                if ((gid != a.GroupID) || (lstApp.Count == r))
                {
                    prog = (float)cnt2 / (float)cntapptotal;
                    if (float.IsNaN(prog)) prog = 0.0f;

                    DataRow dr = dtDisp.NewRow();
                    dr["groupid"] = gid;
                    dr["app"] = cntapptotal;//3部位以上全ての申請書数
                    dr["input1"] = cnt1;
                    dr["input2"] = cnt2;
                    dr["prog"] = (prog * 100.0).ToString("0.0");

                    dtDisp.Rows.Add(dr);


                    cntapptotal = 0;
                    cntapp = 0;
                    cnt1 = 0;
                    cnt2 = 0;
                    prog = 0.0f;

                    //最終レコードが来たら終わる
                    if (lstApp.Count == r) break;
                }


                if (a.StatusFlagCheck(StatusFlag.独自処理1)) cnt1++;
                if (a.StatusFlagCheck(StatusFlag.独自処理2)) cnt2++;
                cntapptotal++;
                r++;



                gid = a.GroupID;
               

            } while (1==1);

            dgv.DataSource = dtDisp;

            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.DefaultCellStyle = cs;


            dgv.Columns["groupid"].Width = 80;
            dgv.Columns["app"].Width = 130;
            dgv.Columns["input1"].Width = 80;
            dgv.Columns["input2"].Width = 80;
            dgv.Columns["prog"].Width = 130;


            //gid、入力候補、入力済、ベリ済、ベリ済/入力候補(%)

            dgv.Columns["groupid"].HeaderText = "グループID";
            dgv.Columns["app"].HeaderText = "入力候補\r\n(3部位以上全部)";
            dgv.Columns["input1"].HeaderText = "入力済";
            dgv.Columns["input2"].HeaderText = "ベリ済";
            dgv.Columns["prog"].HeaderText = "進捗率％\r\nベリ済/入力候補";



        }
    
    }
}
