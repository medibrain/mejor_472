﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.NagoyashiKokuho
{
    /// <summary>
    /// 名古屋市柔整納品データ（全件データ）
    /// </summary>
    class Export
    {
        #region テーブル構造
        [DB.DbAttribute.PrimaryKey]
        public int memberdataid { get; set; } = 0;                              //点検対象一覧のIDを保持し、どのレコードを持ってきたか分かるようにする
        public int cym { get; set; } = 0;                                       //メホール処理年月
        public string renban { get; set; } = string.Empty;                      //連番 18 該当月の「点検対象者一覧」のＡ列（連番）を転記
        public int aid1 { get; set; } = 0;                                      //No.  貴社内で整理番号が必要であれば記載
        public string bunsho_no { get; set; } = string.Empty;                   //文書番号 XXXXXXXX 照会文書・調査票を送った場合に、文書番号を記載
        public string kaitou { get; set; } = string.Empty;                      //回答有無 有 照会文書・調査票を送った場合に、回答があれば「有」を記載
        public string henrei { get; set; } = string.Empty;                      //返戻有無 有 照会文書・調査票を送って回答があった場合に、照合の結果返戻対象であれば「有」を記載もしくは、申請書から返戻と判断した場合に「有」を記載
        public string shoriym { get; set; } = string.Empty;                     //処理年月 02XX 
        public string insnum { get; set; } = string.Empty;                      //保険者番号 00234XXX 該当月の「点検対象者一覧」のＢ列（保険者番号）を転記
        public string detailnum { get; set; } = string.Empty;                   //明細番号 905XXXXXXXXXX 該当月の「点検対象者一覧」のＹ列（国保連レセプト番号）を転記
        public string shinryoym { get; set; } = string.Empty;                   //診療年月 3104 該当月の「点検対象者一覧」のＮ列（診療年月）を転記
        public string hnum { get; set; } = string.Empty;                        //記号番号 XXXXXXXX 該当月の「点検対象者一覧」のＤ列（被保険者証番号月）を転記
        public string pnum { get; set; } = string.Empty;                        //個人番号 XXXXXXXXXX 該当月の「点検対象者一覧」のＧ列（個人番号2）を転記
        public string nusi { get; set; } = string.Empty;                        //世帯主名 〇〇　〇〇 該当月の「点検対象者一覧」のＡＥ列（世帯主氏名）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認
        public string pname { get; set; } = string.Empty;                       //受療者名 〇〇　〇〇 該当月の「点検対象者一覧」のＡＤ列（被保険者氏名）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認
        public string psex { get; set; } = string.Empty;                        //性別 2 申請書の性別欄から性別を記載
        public string pbirthday { get; set; } = string.Empty;                   //生年月日 3211119 該当月の「点検対象者一覧」のＡＦ列（生年月日）を転記
        public string bui { get; set; } = string.Empty;                         //部位数 2 申請書の施術の内容欄を確認し、1か月を通じて施術を受けた部位数を記載
        public string counteddays { get; set; } = string.Empty;                 //実日数 2 申請書の施術の施術日欄を確認し、1か月を通じて施術を受けた実日数を記載
        public string fushoname1 { get; set; } = string.Empty;                  //負傷名1 右膝関節捻挫 申請書の施術の内容欄から、負傷名（１）を記載
        public string fushodate1 { get; set; } = string.Empty;                  //負傷年月日1 310403 申請書の施術の内容欄から、負傷年月日（１）を記載
        public string firstdate1 { get; set; } = string.Empty;                  //初険年月日1 310408 申請書の施術の内容欄から、初険年月日（１）を記載
        public string startdate1 { get; set; } = string.Empty;                  //施術開始年月日1 310408 申請書の施術の内容欄から、施術開始年月日（１）を記載
        public string finishdate1 { get; set; } = string.Empty;                 //施術終了年月日1 310409 申請書の施術の内容欄から、施術終了年月日（１）を記載
        public string days1 { get; set; } = string.Empty;                       //日数1 2 申請書の施術の内容欄から、実日数（１）を記載
        public string fushoname2 { get; set; } = string.Empty;                  //負傷名2 左膝関節捻挫 申請書の施術の内容欄から、負傷名（２）を記載
        public string fushodate2 { get; set; } = string.Empty;                  //負傷年月日2 310407 申請書の施術の内容欄から、負傷年月日（２）を記載
        public string firstdate2 { get; set; } = string.Empty;                  //初険年月日2 310408 申請書の施術の内容欄から、初険年月日（２）を記載
        public string startdate2 { get; set; } = string.Empty;                  //施術開始年月日2 310408 申請書の施術の内容欄から、施術開始年月日（２）を記載
        public string finishdate2 { get; set; } = string.Empty;                 //施術終了年月日2 310409 申請書の施術の内容欄から、施術終了年月日（２）を記載
        public string days2 { get; set; } = string.Empty;                       //日数2 2 申請書の施術の内容欄から、実日数（２）を記載
        public string fushoname3 { get; set; } = string.Empty;                  //負傷名3  申請書の施術の内容欄から、負傷名（３）を記載
        public string fushodate3 { get; set; } = string.Empty;                  //負傷年月日3  申請書の施術の内容欄から、負傷年月日（３）を記載
        public string firstdate3 { get; set; } = string.Empty;                  //初検年月日3  申請書の施術の内容欄から、初険年月日（３）を記載
        public string startdate3 { get; set; } = string.Empty;                  //施術開始年月日3  申請書の施術の内容欄から、施術開始年月日（３）を記載
        public string finishdate3 { get; set; } = string.Empty;                 //施術終了年月日3  申請書の施術の内容欄から、施術終了年月日（３）を記載
        public string days3 { get; set; } = string.Empty;                       //日数3  申請書の施術の内容欄から、実日数（３）を記載
        public string fushoname4 { get; set; } = string.Empty;                  //負傷名4  申請書の施術の内容欄から、負傷名（４）を記載
        public string fushodate4 { get; set; } = string.Empty;                  //負傷年月日4  申請書の施術の内容欄から、負傷年月日（４）を記載
        public string firstdate4 { get; set; } = string.Empty;                  //初検年月日4  申請書の施術の内容欄から、初険年月日（４）を記載
        public string startdate4 { get; set; } = string.Empty;                  //施術開始年月日4  申請書の施術の内容欄から、施術開始年月日（４）を記載
        public string finishdate4 { get; set; } = string.Empty;                 //施術終了年月日4  申請書の施術の内容欄から、施術終了年月日（４）を記載
        public string days4 { get; set; } = string.Empty;                       //日数4  申請書の施術の内容欄から、実日数（４）を記載
        public string fushoname5 { get; set; } = string.Empty;                  //負傷名5  申請書の施術の内容欄から、負傷名（５）を記載
        public string fushodate5 { get; set; } = string.Empty;                  //負傷年月日5  申請書の施術の内容欄から、負傷年月日（５）を記載
        public string firstdate5 { get; set; } = string.Empty;                  //初検年月日5  申請書の施術の内容欄から、初険年月日（５）を記載
        public string startdate5 { get; set; } = string.Empty;                  //施術開始年月日5  申請書の施術の内容欄から、施術開始年月日（５）を記載
        public string finishdate5 { get; set; } = string.Empty;                 //施術終了年月日5  申請書の施術の内容欄から、施術終了年月日（５）を記載
        public string days5 { get; set; } = string.Empty;                       //日数5  申請書の施術の内容欄から、実日数（５）を記載
        public int aid2 { get; set; } = 0;                        //No. XXXXXXXXXX 貴社内で整理番号が必要であれば記載→本来不要
        public string fushoreason1 { get; set; } = string.Empty;                //負傷の原因1  申請書の負傷の原因欄から、負傷の原因（１）を記載
        public string fushoreason2 { get; set; } = string.Empty;                //負傷の原因2  申請書の負傷の原因欄から、負傷の原因（２）を記載
        public string fushoreason3 { get; set; } = string.Empty;                //負傷の原因3  申請書の負傷の原因欄から、負傷の原因（３）を記載
        public string fushoreason4 { get; set; } = string.Empty;                //負傷の原因4  申請書の負傷の原因欄から、負傷の原因（４）を記載
        public string fushoreason5 { get; set; } = string.Empty;                //負傷の原因5  申請書の負傷の原因欄から、負傷の原因（５）を記載
        public string total { get; set; } = string.Empty;                       //合計金額 4630 該当月の「点検対象者一覧」のＴ列（療養の給付等－決定点数）を転記
        public string partial { get; set; } = string.Empty;                     //一部負担額 926 申請書の一部負担金額欄から、一部負担金額を記載
        public string charge { get; set; } = string.Empty;                      //請求金額 3704 申請書の請求金額欄から、請求金額を記載
        public string clinicnum { get; set; } = string.Empty;                   //施術所コード 2358003XXX 該当月の「点検対象者一覧」のＫ列（医療機関コード）を転記
        public string clinicname { get; set; } = string.Empty;                  //施術所名 〇〇接骨院 該当月の「点検対象者一覧」のＬ列（医療機関名）を転記
        public string drname { get; set; } = string.Empty;                      //柔整師氏名 〇〇　〇〇 申請書の施術証明欄を確認し、柔道整復師の氏名を記載
        public string biko { get; set; } = string.Empty;                        //備考  該当月の「点検対象者一覧」のＡＭ列（記事）を転記※この欄に「調査対象外」とある場合は実態調査は送らない。
        public string bunrui { get; set; } = string.Empty;                      //分類 A 該当月の「点検対象者一覧」のＡＮ列（分類）を転記
        public string condition { get; set; } = string.Empty;                   //13日以上、3部位以上、4ヶ月以上、25,000円以上、8日以上のどれか　※伸作さん許可済み
        public string zip { get; set; } = string.Empty;                         //郵便番号 453-XXXX 該当月の「点検対象者一覧」のＡＨ・ＡＩ列（郵便番号１と２）を転記
        public string address { get; set; } = string.Empty;                     //住所 中村区△△町１丁目２３番地△△住宅１棟〇〇号 該当月の「点検対象者一覧」のＡＪ・ＡＫ列（住所と住所方書）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認

        #endregion

        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool ExportData(int cym,List<App> lstApp)
        {
            int OutputListNo = 0;

            //リストの選択画面
            OutputSelectForm frmOut = new OutputSelectForm();
            frmOut.ShowDialog();
            OutputListNo = frmOut.ListNo;
            if (OutputListNo == 0) return false;
            
            //保存場所
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //申請書取得
                //List<App> lstApp = App.GetApps(cym);

                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable(lstApp, cym, wf)) return false;


                wf.LogPrint("データ取得");

                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);

                string strDir = strNohinDir;


                //exportテーブルを取得 npoiでブック作成、保存
                switch (OutputListNo)
                {
                    case 1:
                        if (!DataExport(1, cym, strDir)) return false;
                        break;

                    //case 2:
                    //    if (!DataExport(2, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    break;
                    //case 3:
                    //    if (!DataExport(3, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    break;
                    //case 4:
                    //    if (!DataExport(4, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    break;
                    //case 9:
                    //    if (!DataExport(1, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    if (!DataExport(2, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    if (!DataExport(3, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    if (!DataExport(4, cym, strDir, strSortNoInsName,item.posix)) return false;
                    //    break;

                    default: break;

                }

                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        private static void chkLength(App item, List<memberdata> lstMemData,List<fusho_reason> lstFusho_reason)
        {
            System.Diagnostics.Debug.Print("exp.memberdataid = " +item.TaggedDatas.GeneralString1);
            System.Diagnostics.Debug.Print("exp.cym = " + item.CYM.ToString().Length);
            System.Diagnostics.Debug.Print("exp.renban = " + lstMemData[0].renban.Length);
            System.Diagnostics.Debug.Print("exp.aid1 = " + item.Aid.ToString().Length);
            System.Diagnostics.Debug.Print("exp.bunsho_no = " + string.Empty.Length);
            System.Diagnostics.Debug.Print("exp.kaitou = " + string.Empty.Length);
            System.Diagnostics.Debug.Print("exp.henrei = " + string.Empty.Length);
            System.Diagnostics.Debug.Print("exp.shoriym = " + lstMemData[0].shoriym.Length);
            System.Diagnostics.Debug.Print("exp.insnum = " + item.InsNum.Length);
            System.Diagnostics.Debug.Print("exp.detailnum = " + lstMemData[0].kokuhoreznum.Length);
            System.Diagnostics.Debug.Print("exp.shinryoym = " + lstMemData[0].shinryoym.Length);
            System.Diagnostics.Debug.Print("exp.hnum = " + lstMemData[0].hihoNum1.Length);
            System.Diagnostics.Debug.Print("exp.pnum = " + lstMemData[0].pnum1.Length);
            System.Diagnostics.Debug.Print("exp.nusi = " + lstMemData[0].setainame.Length);
            System.Diagnostics.Debug.Print("exp.pname = " + lstMemData[0].hihoname.Length);
            System.Diagnostics.Debug.Print("exp.psex = " + item.Sex.ToString().Length);
            System.Diagnostics.Debug.Print("exp.pbirthday = " + lstMemData[0].hihobirthday.ToString().Length);
            System.Diagnostics.Debug.Print("exp.bui = " + item.Bui.ToString().Length);
            System.Diagnostics.Debug.Print("exp.counteddays = " + item.CountedDays.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoname1 = " + item.FushoName1.Length);
            System.Diagnostics.Debug.Print("exp.fushodate1 = " + item.FushoDate1.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.firstdate1 = " + item.FushoFirstDate1.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.startdate1 = " + item.FushoStartDate1.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.finishdate1 = " + item.FushoFinishDate1.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.days1 = " + item.FushoDays1.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoname2 = " + item.FushoName2.Length);
            System.Diagnostics.Debug.Print("exp.fushodate2 = " + item.FushoDate2.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.firstdate2 = " + item.FushoFirstDate2.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.startdate2 = " + item.FushoStartDate2.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.finishdate2 = " + item.FushoFinishDate2.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.days2 = " + item.FushoDays2.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoname3 = " + item.FushoName3.Length);
            System.Diagnostics.Debug.Print("exp.fushodate3 = " + item.FushoDate3.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.firstdate3 = " + item.FushoFirstDate3.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.startdate3 = " + item.FushoStartDate3.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.finishdate3 = " + item.FushoFinishDate3.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.days3 = " + item.FushoDays3.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoname4 = " + item.FushoName4.Length);
            System.Diagnostics.Debug.Print("exp.fushodate4 = " + item.FushoDate4.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.firstdate4 = " + item.FushoFirstDate4.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.startdate4 = " + item.FushoStartDate4.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.finishdate4 = " + item.FushoFinishDate4.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.days4 = " + item.FushoDays4.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoname5 = " + item.FushoName5.Length);
            System.Diagnostics.Debug.Print("exp.fushodate5 = " + item.FushoDate5.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.firstdate5 = " + item.FushoFirstDate5.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.startdate5 = " + item.FushoStartDate5.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.finishdate5 = " + item.FushoFinishDate5.ToString("yyyy-MM-dd").Length);
            System.Diagnostics.Debug.Print("exp.days5 = " + item.FushoDays5.ToString().Length);
            System.Diagnostics.Debug.Print("exp.aid2 = " + item.Aid.ToString().Length);
            System.Diagnostics.Debug.Print("exp.fushoreason1 = " + (lstFusho_reason.Count>0 ? lstFusho_reason[0].reason.Length : 0));
            System.Diagnostics.Debug.Print("exp.fushoreason2 = " + (lstFusho_reason.Count>1 ? lstFusho_reason[1].reason.Length : 0));
            System.Diagnostics.Debug.Print("exp.fushoreason3 = " + (lstFusho_reason.Count>2 ? lstFusho_reason[2].reason.Length : 0));
            System.Diagnostics.Debug.Print("exp.fushoreason4 = " + (lstFusho_reason.Count>3 ? lstFusho_reason[3].reason.Length : 0));
            System.Diagnostics.Debug.Print("exp.fushoreason5 = " + (lstFusho_reason.Count>4 ? lstFusho_reason[4].reason.Length : 0));
            System.Diagnostics.Debug.Print("exp.total = " + lstMemData[0].total.ToString().Length);
            System.Diagnostics.Debug.Print("exp.partial = " + item.Partial.ToString().Length);
            System.Diagnostics.Debug.Print("exp.charge = " + item.Charge.ToString().Length);
            System.Diagnostics.Debug.Print("exp.clinicnum = " + lstMemData[0].clinicnum.Length);
            System.Diagnostics.Debug.Print("exp.clinicname = " + lstMemData[0].clinicname.Length);
            System.Diagnostics.Debug.Print("exp.drname = " + item.DrName.Length);
            System.Diagnostics.Debug.Print("exp.biko = " + lstMemData[0].biko.Length);
            System.Diagnostics.Debug.Print("exp.bunrui = " + lstMemData[0].bunrui.Length);
            System.Diagnostics.Debug.Print("exp.condition = " + chkCondition(item, lstMemData[0].bunrui).Length);
            System.Diagnostics.Debug.Print("exp.zip = " + $"{lstMemData[0].zip1}-{lstMemData[0].zip2.PadLeft(4, '0')}".Length);
            System.Diagnostics.Debug.Print("exp.address = " + $"{lstMemData[0].add1}{lstMemData[0].add2}".Length);

        }

        /// <summary>
        /// exportクラスにデータを入れる
        /// </summary>
        /// <param name="item">app</param>
        /// <param name="lstMemData">memberdata</param>
        /// <param name="lstFusho_reason">fusho_reason</param>
        /// <param name="exp">export</param>
        /// <param name="outexp">export</param>
        /// <returns></returns>
        private static bool entryToExport(App item, List<memberdata> lstMemData,
            List<fusho_reason> lstFusho_reason,Export exp,out Export outexp)
        {
            outexp = null;
            try
            {
         //       if (item.TaggedDatas.GeneralString1 == "138") System.Windows.Forms.MessageBox.Show("");

                exp.memberdataid = int.Parse(item.TaggedDatas.GeneralString1);  //点検対象一覧のIDを保持し、どのレコードを持ってきたか分かるようにする
                exp.cym = item.CYM;                                             //メホール処理年月
                exp.renban = lstMemData[0].renban;                              //連番 18 該当月の「点検対象者一覧」のＡ列（連番）を転記
                exp.aid1 = item.Aid;                                            //No.  貴社内で整理番号が必要であれば記載
                exp.bunsho_no = string.Empty;//不明                             //文書番号 XXXXXXXX 照会文書・調査票を送った場合に、文書番号を記載
                exp.kaitou = string.Empty;//不明                                //回答有無 有 照会文書・調査票を送った場合に、回答があれば「有」を記載
                exp.henrei = string.Empty;//不明                                //返戻有無 有 照会文書・調査票を送って回答があった場合に、照合の結果返戻対象であれば「有」を記載もしくは、申請書から返戻と判断した場合に「有」を記載
                exp.shoriym = lstMemData[0].shoriym;                            //処理年月 02XX 

                //20200528093401 furukawa st ////////////////////////
                //保険者番号は提供データから取得
                                                                                
                exp.insnum = lstMemData[0].insnum;                              //保険者番号 00234XXX 該当月の「点検対象者一覧」のＢ列（保険者番号）を転記
                //exp.insnum = item.InsNum;                                     //保険者番号 00234XXX 該当月の「点検対象者一覧」のＢ列（保険者番号）を転記
                //20200528093401 furukawa ed ////////////////////////


                exp.detailnum = lstMemData[0].kokuhoreznum;                      //明細番号 905XXXXXXXXXX 該当月の「点検対象者一覧」のＹ列（国保連レセプト番号）を転記
                exp.shinryoym = lstMemData[0].shinryoym;                         //診療年月 3104 該当月の「点検対象者一覧」のＮ列（診療年月）を転記
                exp.hnum = lstMemData[0].hihoNum1;                               //記号番号 XXXXXXXX 該当月の「点検対象者一覧」のＤ列（被保険者証番号月）を転記
                exp.pnum = lstMemData[0].pnum1;                                  //個人番号 XXXXXXXXXX 該当月の「点検対象者一覧」のＧ列（個人番号2）を転記
                exp.nusi = lstMemData[0].setainame;                              //世帯主名 〇〇　〇〇 該当月の「点検対象者一覧」のＡＥ列（世帯主氏名）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認
                exp.pname = lstMemData[0].hihoname;                              //受療者名 〇〇　〇〇 該当月の「点検対象者一覧」のＡＤ列（被保険者氏名）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認
                exp.psex = item.Sex.ToString();                                  //性別 2 申請書の性別欄から性別を記載
                exp.pbirthday = lstMemData[0].hihobirthday.ToString();           //生年月日 3211119 該当月の「点検対象者一覧」のＡＦ列（生年月日）を転記
                exp.bui = item.Bui.ToString();                                   //部位数 2 申請書の施術の内容欄を確認し、1か月を通じて施術を受けた部位数を記載
                exp.counteddays = item.CountedDays.ToString();                   //実日数 2 申請書の施術の施術日欄を確認し、1か月を通じて施術を受けた実日数を記載

                exp.fushoname1 = item.FushoName1;                                //負傷名1 右膝関節捻挫 申請書の施術の内容欄から、負傷名（１）を記載


                //20200601154921 furukawa st ////////////////////////
                //負傷年月日～施術終了日をeyymmddに
                
                exp.fushodate1 = item.FushoDate1 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoDate1.ToString("yyyyMM"))) + item.FushoDate1.ToString("dd").PadLeft(2, '0') : string.Empty;                 //負傷年月日1 310403 申請書の施術の内容欄から、負傷年月日（１）を記載
                exp.firstdate1 = item.FushoFirstDate1 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFirstDate1.ToString("yyyyMM")))+item.FushoFirstDate1.ToString("dd").PadLeft(2,'0') : string.Empty;          //初険年月日1 310408 申請書の施術の内容欄から、初険年月日（１）を記載
                exp.startdate1 = item.FushoStartDate1 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoStartDate1.ToString("yyyyMM")))+item.FushoStartDate1.ToString("dd").PadLeft(2, '0') : string.Empty;         //施術開始年月日1 310408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                exp.finishdate1 = item.FushoFinishDate1 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFinishDate1.ToString("yyyyMM")))+item.FushoFinishDate1.ToString("dd").PadLeft(2, '0') : string.Empty;      //施術終了年月日1 310409 申請書の施術の内容欄から、施術終了年月日（１）を記載                
                            //exp.fushodate1 = item.FushoDate1.ToString("yyyy-MM-dd");         //負傷年月日1 310403 申請書の施術の内容欄から、負傷年月日（１）を記載
                            //exp.firstdate1 = item.FushoFirstDate1.ToString("yyyy-MM-dd");    //初険年月日1 310408 申請書の施術の内容欄から、初険年月日（１）を記載
                            //exp.startdate1 = item.FushoStartDate1.ToString("yyyy-MM-dd");    //施術開始年月日1 310408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                            //exp.finishdate1 = item.FushoFinishDate1.ToString("yyyy-MM-dd");  //施術終了年月日1 310409 申請書の施術の内容欄から、施術終了年月日（１）を記載
                //20200601154921 furukawa ed ////////////////////////

                exp.days1 = item.FushoDays1.ToString();                          //日数1 2 申請書の施術の内容欄から、実日数（１）を記載
                exp.fushoname2 = item.FushoName2;                                //負傷名2 左膝関節捻挫 申請書の施術の内容欄から、負傷名（２）を記載


                //20200601155021 furukawa st ////////////////////////
                //負傷年月日～施術終了日をeyymmddに
                
                exp.fushodate2 = item.FushoDate2 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoDate2.ToString("yyyyMM"))) + item.FushoDate2.ToString("dd").PadLeft(2, '0') : string.Empty;                 //負傷年月日2 320403 申請書の施術の内容欄から、負傷年月日（１）を記載
                exp.firstdate2 = item.FushoFirstDate2 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFirstDate2.ToString("yyyyMM"))) + item.FushoFirstDate2.ToString("dd").PadLeft(2, '0') : string.Empty;          //初険年月日2 320408 申請書の施術の内容欄から、初険年月日（１）を記載
                exp.startdate2 = item.FushoStartDate2 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoStartDate2.ToString("yyyyMM"))) + item.FushoStartDate2.ToString("dd").PadLeft(2, '0') : string.Empty;         //施術開始年月日2 320408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                exp.finishdate2 = item.FushoFinishDate2 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFinishDate2.ToString("yyyyMM"))) + item.FushoFinishDate2.ToString("dd").PadLeft(2, '0') : string.Empty;      //施術終了年月日2 320409 申請書の施術の内容欄から、施術終了年月日（１）を記載
                            //exp.fushodate2 = item.FushoDate2.ToString("yyyy-MM-dd");         //負傷年月日2 310407 申請書の施術の内容欄から、負傷年月日（２）を記載
                            //exp.firstdate2 = item.FushoFirstDate2.ToString("yyyy-MM-dd");    //初険年月日2 310408 申請書の施術の内容欄から、初険年月日（２）を記載
                            //exp.startdate2 = item.FushoStartDate2.ToString("yyyy-MM-dd");    //施術開始年月日2 310408 申請書の施術の内容欄から、施術開始年月日（２）を記載
                            //exp.finishdate2 = item.FushoFinishDate2.ToString("yyyy-MM-dd");  //施術終了年月日2 310409 申請書の施術の内容欄から、施術終了年月日（２）を記載
                //20200601155021 furukawa ed ////////////////////////

                exp.days2 = item.FushoDays2.ToString();                          //日数2 2 申請書の施術の内容欄から、実日数（２）を記載
                exp.fushoname3 = item.FushoName3;                                //負傷名3  申請書の施術の内容欄から、負傷名（３）を記載

                //20200601155050 furukawa st ////////////////////////
                //負傷年月日～施術終了日をeyymmddに
                
                exp.fushodate3 = item.FushoDate3 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoDate3.ToString("yyyyMM"))) + item.FushoDate3.ToString("dd").PadLeft(2, '0') : string.Empty;                 //負傷年月日3 330403 申請書の施術の内容欄から、負傷年月日（１）を記載
                exp.firstdate3 = item.FushoFirstDate3 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFirstDate3.ToString("yyyyMM"))) + item.FushoFirstDate3.ToString("dd").PadLeft(2, '0') : string.Empty;          //初険年月日3 330408 申請書の施術の内容欄から、初険年月日（１）を記載
                exp.startdate3 = item.FushoStartDate3 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoStartDate3.ToString("yyyyMM"))) + item.FushoStartDate3.ToString("dd").PadLeft(2, '0') : string.Empty;         //施術開始年月日3 330408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                exp.finishdate3 = item.FushoFinishDate3 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFinishDate3.ToString("yyyyMM"))) + item.FushoFinishDate3.ToString("dd").PadLeft(2, '0') : string.Empty;      //施術終了年月日3 330409 申請書の施術の内容欄から、施術終了年月日（１）を記載
                            //exp.fushodate3 = item.FushoDate3.ToString("yyyy-MM-dd");         //負傷年月日3  申請書の施術の内容欄から、負傷年月日（３）を記載
                            //exp.firstdate3 = item.FushoFirstDate3.ToString("yyyy-MM-dd");    //初検年月日3  申請書の施術の内容欄から、初険年月日（３）を記載
                            //exp.startdate3 = item.FushoStartDate3.ToString("yyyy-MM-dd");    //施術開始年月日3  申請書の施術の内容欄から、施術開始年月日（３）を記載
                            //exp.finishdate3 = item.FushoFinishDate3.ToString("yyyy-MM-dd");  //施術終了年月日3  申請書の施術の内容欄から、施術終了年月日（３）を記載
                //20200601155050 furukawa ed ////////////////////////

                exp.days3 = item.FushoDays3.ToString();                          //日数3  申請書の施術の内容欄から、実日数（３）を記載
                exp.fushoname4 = item.FushoName4;                                //負傷名4  申請書の施術の内容欄から、負傷名（４）を記載

                //20200601155108 furukawa st ////////////////////////
                //負傷年月日～施術終了日をeyymmddに
                
                exp.fushodate4 = item.FushoDate4 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoDate4.ToString("yyyyMM"))) + item.FushoDate4.ToString("dd").PadLeft(2, '0') : string.Empty;                 //負傷年月日4 340403 申請書の施術の内容欄から、負傷年月日（１）を記載
                exp.firstdate4 = item.FushoFirstDate4 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFirstDate4.ToString("yyyyMM"))) + item.FushoFirstDate4.ToString("dd").PadLeft(2, '0') : string.Empty;          //初険年月日4 340408 申請書の施術の内容欄から、初険年月日（１）を記載
                exp.startdate4 = item.FushoStartDate4 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoStartDate4.ToString("yyyyMM"))) + item.FushoStartDate4.ToString("dd").PadLeft(2, '0') : string.Empty;         //施術開始年月日4 340408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                exp.finishdate4 = item.FushoFinishDate4 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFinishDate4.ToString("yyyyMM"))) + item.FushoFinishDate4.ToString("dd").PadLeft(2, '0') : string.Empty;      //施術終了年月日4 340409 申請書の施術の内容欄から、施術終了年月日（１）を記載
                            //exp.fushodate4 = item.FushoDate4.ToString("yyyy-MM-dd");         //負傷年月日4  申請書の施術の内容欄から、負傷年月日（４）を記載
                            //exp.firstdate4 = item.FushoFirstDate4.ToString("yyyy-MM-dd");    //初検年月日4  申請書の施術の内容欄から、初険年月日（４）を記載
                            //exp.startdate4 = item.FushoStartDate4.ToString("yyyy-MM-dd");    //施術開始年月日4  申請書の施術の内容欄から、施術開始年月日（４）を記載
                            //exp.finishdate4 = item.FushoFinishDate4.ToString("yyyy-MM-dd");  //施術終了年月日4  申請書の施術の内容欄から、施術終了年月日（４）を記載
                //20200601155108 furukawa ed ////////////////////////

                exp.days4 = item.FushoDays4.ToString();                          //日数4  申請書の施術の内容欄から、実日数（４）を記載
                exp.fushoname5 = item.FushoName5;                                //負傷名5  申請書の施術の内容欄から、負傷名（５）を記載


                //20200601155127 furukawa st ////////////////////////
                //負傷年月日～施術終了日をeyymmddに
                
                exp.fushodate5 = item.FushoDate5 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoDate5.ToString("yyyyMM"))) + item.FushoDate5.ToString("dd").PadLeft(2, '0') : string.Empty;                 //負傷年月日5 350403 申請書の施術の内容欄から、負傷年月日（１）を記載
                exp.firstdate5 = item.FushoFirstDate5 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFirstDate5.ToString("yyyyMM"))) + item.FushoFirstDate5.ToString("dd").PadLeft(2, '0') : string.Empty;          //初険年月日5 350408 申請書の施術の内容欄から、初険年月日（１）を記載
                exp.startdate5 = item.FushoStartDate5 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoStartDate5.ToString("yyyyMM"))) + item.FushoStartDate5.ToString("dd").PadLeft(2, '0') : string.Empty;         //施術開始年月日5 350408 申請書の施術の内容欄から、施術開始年月日（１）を記載
                exp.finishdate5 = item.FushoFinishDate5 != DateTime.MinValue ? DateTimeEx.GetGyymmFromAdYM(int.Parse(item.FushoFinishDate5.ToString("yyyyMM"))) + item.FushoFinishDate5.ToString("dd").PadLeft(2, '0') : string.Empty;      //施術終了年月日5 350409 申請書の施術の内容欄から、施術終了年月日（１）を記載
                            //exp.fushodate5 = item.FushoDate5.ToString("yyyy-MM-dd");         //負傷年月日5  申請書の施術の内容欄から、負傷年月日（５）を記載
                            //exp.firstdate5 = item.FushoFirstDate5.ToString("yyyy-MM-dd");    //初検年月日5  申請書の施術の内容欄から、初険年月日（５）を記載
                            //exp.startdate5 = item.FushoStartDate5.ToString("yyyy-MM-dd");    //施術開始年月日5  申請書の施術の内容欄から、施術開始年月日（５）を記載
                            //exp.finishdate5 = item.FushoFinishDate5.ToString("yyyy-MM-dd");  //施術終了年月日5  申請書の施術の内容欄から、施術終了年月日（５）を記載
                //20200601155127 furukawa ed ////////////////////////
                exp.days5 = item.FushoDays5.ToString();                          //日数5  申請書の施術の内容欄から、実日数（５）を記載


                exp.aid2 = item.Aid;                                                 //No. XXXXXXXXXX 貴社内で整理番号が必要であれば記載→本来不要
                exp.fushoreason1 = lstFusho_reason.Count > 0 ? lstFusho_reason[0].reason : string.Empty;    //負傷の原因1  申請書の負傷の原因欄から、負傷の原因（１）を記載
                exp.fushoreason2 = lstFusho_reason.Count > 1 ? lstFusho_reason[1].reason : string.Empty;    //負傷の原因2  申請書の負傷の原因欄から、負傷の原因（２）を記載
                exp.fushoreason3 = lstFusho_reason.Count > 2 ? lstFusho_reason[2].reason : string.Empty;    //負傷の原因3  申請書の負傷の原因欄から、負傷の原因（３）を記載
                exp.fushoreason4 = lstFusho_reason.Count > 3 ? lstFusho_reason[3].reason : string.Empty;    //負傷の原因4  申請書の負傷の原因欄から、負傷の原因（４）を記載
                exp.fushoreason5 = lstFusho_reason.Count > 4 ? lstFusho_reason[4].reason : string.Empty;    //負傷の原因5  申請書の負傷の原因欄から、負傷の原因（５）を記載
                exp.total = lstMemData[0].total.ToString();                                    //合計金額 4630 該当月の「点検対象者一覧」のＴ列（療養の給付等－決定点数）を転記
                exp.partial = item.Partial.ToString();                                         //一部負担額 926 申請書の一部負担金額欄から、一部負担金額を記載
                exp.charge = item.Charge.ToString();                                           //請求金額 3704 申請書の請求金額欄から、請求金額を記載
                exp.clinicnum = lstMemData[0].clinicnum;                                       //施術所コード 2358003XXX 該当月の「点検対象者一覧」のＫ列（医療機関コード）を転記
                exp.clinicname = lstMemData[0].clinicname;                                     //施術所名 〇〇接骨院 該当月の「点検対象者一覧」のＬ列（医療機関名）を転記
                exp.drname = item.DrName;                                                      //柔整師氏名 〇〇　〇〇 申請書の施術証明欄を確認し、柔道整復師の氏名を記載
                exp.biko = lstMemData[0].biko;                                                 //備考  該当月の「点検対象者一覧」のＡＭ列（記事）を転記※この欄に「調査対象外」とある場合は実態調査は送らない。
                exp.bunrui = lstMemData[0].bunrui;                                             //分類 A 該当月の「点検対象者一覧」のＡＮ列（分類）を転記

                exp.condition = chkCondition(item, lstMemData[0].bunrui);                                            //13日以上、3部位以上、4ヶ月以上、25,000円以上、8日以上のどれか　※伸作さん許可済み
                //exp.condition = chkCondition(item);                                            //13日以上、3部位以上、4ヶ月以上、25,000円以上、8日以上のどれか　※伸作さん許可済み

                exp.zip =
                lstMemData[0].zip1 == string.Empty ? string.Empty :
                    $"{lstMemData[0].zip1}-{lstMemData[0].zip2.PadLeft(4, '0')}";              //郵便番号 453-XXXX 該当月の「点検対象者一覧」のＡＨ・ＡＩ列（郵便番号１と２）を転記

                exp.address = $"{lstMemData[0].add1}{lstMemData[0].add2}";                     //住所 中村区△△町１丁目２３番地△△住宅１棟〇〇号 該当月の「点検対象者一覧」のＡＪ・ＡＫ列（住所と住所方書）を転記※◆がある場合は、外字のため「点検対象者一覧」の別シートの画像で確認

                outexp = exp;
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <returns></returns>
        private static bool InsertExportTable(List<App> lstApp,int cym ,WaitForm wf)
        {
            wf.SetMax(lstApp.Count);

            DB.Transaction tran = new DB.Transaction(DB.Main);
            


            try
            {
                //今月分を削除
                DB.Command cmd = new DB.Command($"delete from export where cym={ cym }", tran);
                cmd.TryExecuteNonQuery();

           
                //int cnt = 0;

                foreach (App item in lstApp)
                {

              
                    Export exp = new Export();

                    if (item.TaggedDatas.GeneralString1 == string.Empty) continue;

                    List<memberdata> lstMemData=memberdata.select($" memberdataid={item.TaggedDatas.GeneralString1}");


                    //20200928141005 furukawa st ////////////////////////
                    //Appに記録された提供データIDが無い場合エラーで止める

                    if (lstMemData.Count == 0)
                    {
                        if (MessageBox.Show($"提供データID{item.TaggedDatas.GeneralString1}が見つかりません。続行しますか？",
                            Application.ProductName, 
                            MessageBoxButtons.YesNo, 
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
                        continue;
                    }
                    //20200928141005 furukawa ed ////////////////////////


                    List<fusho_reason> lstFusho_reason = fusho_reason.SelectFromAid(item.Aid);
                    lstFusho_reason.Sort((x, y) => x.reason_no.CompareTo(y.reason_no));

#if DEBUG 
                    //chkLength(item, lstMemData, lstFusho_reason);
#endif

                    entryToExport(item, lstMemData, lstFusho_reason, exp,out exp);


                    //if (cmddup.TryExecuteScalar() != null)
                    //{
                    //    wf.LogPrint($"{item.HihoNum}既に出しました");                        
                    //    wf.InvokeValue++;
                    //    continue;
                    //}



                    if(!DB.Main.Insert<Export>(exp, tran)) return false;

                    wf.LogPrint(exp.memberdataid.ToString());
                    wf.InvokeValue++;
                }
                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                tran.Rollback();
                return false;
            }
          
               
        }

        /// <summary>
        /// 条件チェック
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private static string chkCondition(App app,string strBunrui)
        {


            //13日以上、3部位以上、4ヶ月以上、25,000円以上、8日以上のどれか　※伸作さん許可済み

            string strCondition = string.Empty;


            //20200621115709 furukawa st ////////////////////////
            //区分ABで別々の判定
            

            //　　多部位　　　長期　　　　回数　　　　金額
            //A　3部位以上かつ4ヶ月超　月13回以上　　合計金額25,000超 →仕様書では左のようになっているが、保険者側の処理の都合上、多部位と長期はAB区別無く同じ処理とする　伸作さん
            //B　3部位以上　　4ヶ月超　月8回以上　　　合計金額25,000超

            switch (strBunrui)
            {
                case "A":

                    //20200831134745 furukawa st ////////////////////////
                    //保険者側の処理の都合上、多部位と長期はAB区別無く同じ処理とする　2020/8/20伸作さん
                    
                    if (app.Bui >= 3) strCondition += "3部位以上|";
                    //終了日-初検年月日
                    if (
                        (app.FushoFinishDate1 - app.FushoFirstDate1 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate2 - app.FushoFirstDate2 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate3 - app.FushoFirstDate3 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate4 - app.FushoFirstDate4 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate5 - app.FushoFirstDate5 > new TimeSpan(120, 0, 0, 0, 0))
                        )
                    {
                        //どれかの負傷が120日超なので該当とする
                        strCondition += "4ヶ月超|";
                    }

                    ////終了日-初検年月日
                    //if ((
                    //    (app.FushoFinishDate1 - app.FushoFirstDate1 > new TimeSpan(120, 0, 0, 0, 0)) ||
                    //    (app.FushoFinishDate2 - app.FushoFirstDate2 > new TimeSpan(120, 0, 0, 0, 0)) ||
                    //    (app.FushoFinishDate3 - app.FushoFirstDate3 > new TimeSpan(120, 0, 0, 0, 0)) ||
                    //    (app.FushoFinishDate4 - app.FushoFirstDate4 > new TimeSpan(120, 0, 0, 0, 0)) ||
                    //    (app.FushoFinishDate5 - app.FushoFirstDate5 > new TimeSpan(120, 0, 0, 0, 0))
                    //    ) && 
                    //    (app.Bui >= 3))
                    //{
                    //    //どれかの負傷が120日超なので該当とする
                    //    strCondition += "3部位以上かつ4ヶ月超|";
                    //}

                    //20200831134745 furukawa ed ////////////////////////

                    if (app.CountedDays >= 13) strCondition += "13日以上|";
                    if (app.Total > 25000) strCondition += "25,000超|";

                    break;
                case "B":
                    if (app.Bui >= 3) strCondition += "3部位以上|";
                    if (app.CountedDays >= 8) strCondition += "8日以上|";
                    if (app.Total > 25000) strCondition += "25,000超|";

                    //終了日-初検年月日
                    if (
                        (app.FushoFinishDate1 - app.FushoFirstDate1 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate2 - app.FushoFirstDate2 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate3 - app.FushoFirstDate3 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate4 - app.FushoFirstDate4 > new TimeSpan(120, 0, 0, 0, 0)) ||
                        (app.FushoFinishDate5 - app.FushoFirstDate5 > new TimeSpan(120, 0, 0, 0, 0))
                        )
                    {
                        //どれかの負傷が120日超なので該当とする
                        strCondition += "4ヶ月超|";
                    }
                    break;
                default:
                    break;
            }



            #region old

            //if (app.CountedDays >= 13) strCondition += "13日以上|";
            //if (app.Bui >= 3) strCondition += "3部位以上|";

            //if (app.Total >= 25000) strCondition += "25,000以上|";
            //if (app.CountedDays >= 8) strCondition += "8日以上|";




            // --差分が120日超の人のSQL
            //これでaidを抽出し、該当のaid
            //            select a.aid--a.hnum,a.pbirthday,a.psex

            //,a.ifinishdate1 - a.ifirstdate1 as f1diffdays
            //,a.ifinishdate2 - a.ifirstdate2 as f2diffdays
            //,a.ifinishdate3 - a.ifirstdate3 as f3diffdays
            //,a.ifinishdate4 - a.ifirstdate4 as f4diffdays
            //,a.ifinishdate5 - a.ifirstdate5 as f5diffdays
            //from application a

            // where
            // --差分が120日超の人
            // (a.ifinishdate1 - a.ifirstdate1 > 120
            // or a.ifinishdate2 - a.ifirstdate2 > 120
            // or a.ifinishdate3 - a.ifirstdate3 > 120
            // or a.ifinishdate4 - a.ifirstdate4 > 120
            // or a.ifinishdate5 - a.ifirstdate5 > 120)
            // and
            // a.ifinishdate1 >= a.istartdate1


            //--a.aid,a.hnum,a.psex,a.pbirthday,a.sid,a.ym
            //order by aid
            //, a.ifirstdate1,a.ifinishdate1

            //終了日-初検年月日
            //if(
            //    (app.FushoFinishDate1 - app.FushoFirstDate1 > new TimeSpan(120,0,0,0,0)) ||
            //    (app.FushoFinishDate2 - app.FushoFirstDate2 > new TimeSpan(120, 0, 0, 0, 0))||
            //    (app.FushoFinishDate3 - app.FushoFirstDate3 > new TimeSpan(120, 0, 0, 0, 0))||
            //    (app.FushoFinishDate4 - app.FushoFirstDate4 > new TimeSpan(120, 0, 0, 0, 0))||
            //    (app.FushoFinishDate5 - app.FushoFirstDate5 > new TimeSpan(120, 0, 0, 0, 0))
            //    )
            //{
            //    //どれかの負傷が120日以上なので該当とする？
            //    strCondition += "4ヶ月以上|";
            //}

            //string strsql = $"select aid from application a where " +
            //$"a.hnum='{app.HihoNum}' " +
            // $" and a.psex={app.Sex} " +
            // $" and a.pbirthday='{app.Birthday.ToString("yyyy-MM-dd")}' " +
            // $" and a.sid='{app.ClinicNum}'" +
            // $" and a.ym between {DateTimeEx.Int6YmAddMonth(app.YM,-4)} and {app.YM}" +

            // $" group by a.aid,a.hnum,a.psex,a.pbirthday,a.sid,a.ym";

            //DB.Command cmd = new DB.Command(DB.Main, strsql);
            //var l=cmd.TryExecuteReaderList();
            //if (l.Count > 4)
            //{
            //    strCondition+= "4ヶ月以上|";
            //}
            //cmd.Dispose();

            #endregion



            //20200621115709 furukawa ed ////////////////////////


            return strCondition;

        }


        /// <summary>
        /// DBからデータを取得してExcel出力まで
        /// <param name="listNo">出力リスト番号</param>
        /// <param name="cym">処理年月</param>
        /// <param name="strDir">保険者ごとの出力フォルダ名</param>
                
        /// </summary>
        /// <returns></returns>
        private static bool DataExport(int listNo,int cym,string strDir)
        {
            
            string strFileName = strDir + "\\";
            string strHeader = string.Empty;
            string strSQL = string.Empty;

            if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);

            switch (listNo)
            {
                case 1:
                    strFileName += $"全件データ.xlsx";
                    #region ヘッダ

                    strHeader =
                    "連番," +
                    "No.," +
                    "文書番号," +
                    "回答有無," +
                    "返戻有無," +
                    "処理年月," +
                    "保険者番号," +
                    "明細番号," +
                    "診療年月," +
                    "記号番号," +
                    "個人番号," +
                    "世帯主名," +
                    "受療者名," +
                    "性別," +
                    "生年月日," +
                    "部位数," +
                    "実日数," +
                    "負傷名1," +
                    "負傷年月日1," +
                    "初険年月日1," +
                    "施術開始年月日1," +
                    "施術終了年月日1," +
                    "日数1," +
                    "負傷名2," +
                    "負傷年月日2," +
                    "初険年月日2," +
                    "施術開始年月日2," +
                    "施術終了年月日2," +
                    "日数2," +
                    "負傷名3," +
                    "負傷年月日3," +
                    "初検年月日3," +
                    "施術開始年月日3," +
                    "施術終了年月日3," +
                    "日数3," +
                    "負傷名4," +
                    "負傷年月日4," +
                    "初検年月日4," +
                    "施術開始年月日4," +
                    "施術終了年月日4," +
                    "日数4," +
                    "負傷名5," +
                    "負傷年月日5," +
                    "初検年月日5," +
                    "施術開始年月日5," +
                    "施術終了年月日5," +
                    "日数5," +
                    "No.," +
                    "負傷の原因1," +
                    "負傷の原因2," +
                    "負傷の原因3," +
                    "負傷の原因4," +
                    "負傷の原因5," +
                    "合計金額," +
                    "一部負担額," +
                    "請求金額," +
                    "施術所コード," +
                    "施術所名," +
                    "柔整師氏名," +
                    "備考," +
                    "分類," +
                    "13日以上、3部位以上、4ヶ月以上、25000円以上、8日以上のどれか," +
                    "郵便番号," +
                    "住所";

                    #endregion

                    #region sql
                    strSQL =
                        "SELECT " +
                    "renban," +
                    "aid1," +
                    "bunsho_no," +
                    "kaitou," +
                    "henrei," +
                    "shoriym," +
                    "insnum," +
                    "detailnum," +
                    "shinryoym," +
                    "hnum," +
                    "pnum," +
                    "nusi," +
                    "pname," +
                    "psex," +
                    "pbirthday," +
                    "bui," +
                    "counteddays," +
                    "fushoname1," +
                    "fushodate1," +
                    "firstdate1," +
                    "startdate1," +
                    "finishdate1," +
                    "days1," +
                    "fushoname2," +
                    "fushodate2," +
                    "firstdate2," +
                    "startdate2," +
                    "finishdate2," +
                    "days2," +
                    "fushoname3," +
                    "fushodate3," +
                    "firstdate3," +
                    "startdate3," +
                    "finishdate3," +
                    "days3," +
                    "fushoname4," +
                    "fushodate4," +
                    "firstdate4," +
                    "startdate4," +
                    "finishdate4," +
                    "days4," +
                    "fushoname5," +
                    "fushodate5," +
                    "firstdate5," +
                    "startdate5," +
                    "finishdate5," +
                    "days5," +
                    "aid2," +
                    "fushoreason1," +
                    "fushoreason2," +
                    "fushoreason3," +
                    "fushoreason4," +
                    "fushoreason5," +
                    "total," +
                    "partial," +
                    "charge," +
                    "clinicnum," +
                    "clinicname," +
                    "drname," +
                    "biko," +
                    "bunrui," +
                    "condition," +
                    "zip," +
                    "address " +
                        $"FROM export where cym ={cym} " +
                        $"order by cast(renban as int)";

                    #endregion

                    break;

                #region 不要
                //case 2:
                //    strFileName += $"{cym}_{strSortNoInsname}_02_初検被保険者リスト.xlsx";
                //    #region ヘッダ

                //    strHeader =
                //       "レセプト全国共通キー," +
                //        "診療年月," +
                //        "記号," +
                //        "番号," +
                //        "性別," +
                //        "生年月日," +
                //        "受療者名," +
                //        "被保険者名," +
                //        "負傷名1," +
                //        "合計金額," +
                //        "柔道整復師名," +
                //        "施術所名," +
                //        "修正有無," +
                //        "被保険者住所," +
                //        "本人家族区分," +
                //        "実日数1," +
                //        "負傷部位数," +
                //        "初検年月日1," +
                //        "請求額";
                //        //"施術開始年月日1," +
                //        //"施術所郵便番号," +
                //        //"施術所住所," +
                //        //"給付割合," +
                //        //"照会結果," +
                //        //"返戻理由";


                //    #endregion

                //    #region sql
                //    strSQL =
                //        "SELECT " +
                //        "comnum," +
                //        "ym," +
                //        "hMark," +
                //        "hNum," +
                //        "psex," +
                //        "pbirthday," +
                //        "pname," +
                //        "hname," +
                //        "fushoname1," +
                //        "total," +
                //        "drname," +
                //        "clinicname," +
                //        "upd," +
                //        "haddress," +
                //        "family," +
                //        "fushodays1," +
                //        "fushocount," +
                //        "shoken1," +
                //        "charge " +
                //        //"startdate1," +
                //        //"cliniczip," +
                //        //"clinicaddress," +
                //        //"ratio," +
                //        //"shokaireason," +
                //        //"henreireason " +
                //        $"FROM export where cym ={cym}";

                //    if (strPosix != string.Empty) strSQL += $" and hMark {strPosix}";
                //    #endregion


                //    break;
                //case 3:
                //    strFileName += $"{cym}_{strSortNoInsname}_03_照会対象候補施術所等リスト.xlsx";
                //    #region ヘッダ

                //    strHeader =
                //        "レセプト全国共通キー," +
                //        "診療年月," +
                //        "記号," +
                //        "番号," +
                //        "性別," +
                //        "生年月日," +
                //        "受療者名," +
                //        "被保険者名," +
                //        "負傷名1," +
                //        "合計金額," +
                //        "柔道整復師名," +
                //        "施術所名," +
                //        "修正有無," +
                //        //"被保険者住所," +
                //        //"本人家族区分," +
                //        //"実日数1," +
                //        //"負傷部位数," +
                //        //"初検年月日1," +
                //        "請求額," +
                //        "施術開始年月日1," +
                //        "施術所郵便番号," +
                //        "施術所住所," +
                //        "給付割合," +
                //        "照会結果," +
                //        "返戻理由";


                //    #endregion

                //    #region sql
                //    strSQL =
                //        "SELECT " +
                //        "comnum," +
                //        "ym," +
                //        "hMark," +
                //        "hNum," +
                //        "psex," +
                //        "pbirthday," +
                //        "pname," +
                //        "hname," +
                //        "fushoname1," +
                //        "total," +
                //        "drname," +
                //        "clinicname," +
                //        "upd," +
                //        //"haddress," +
                //        //"family," +
                //        //"fushodays1," +
                //        //"fushocount," +
                //        //"shoken1," +
                //        "charge," +
                //        "startdate1," +
                //        "cliniczip," +
                //        "clinicaddress," +
                //        "ratio," +
                //        "shokaireason," +
                //        "henreireason " +
                //        $"FROM export where cym ={cym}";
                //    if (strPosix != string.Empty) strSQL += $" and hMark {strPosix}";
                //    #endregion


                //    break;
                //case 4:
                //    strFileName += $"{cym}_{strSortNoInsname}_04_返戻対象候補者リスト.xlsx";
                //    #region ヘッダ

                //    strHeader =
                //     "レセプト全国共通キー," +
                //    "診療年月," +
                //    "記号," +
                //    "番号," +
                //    "性別," +
                //    //"生年月日," +
                //    "受療者名," +
                //    //"被保険者名," +
                //    "負傷名1," +
                //    "合計金額," +
                //    //"柔道整復師名," +
                //    //"施術所名," +
                //    //"修正有無," +
                //    //"被保険者住所," +
                //    "本人家族区分," +
                //    //"実日数1," +
                //    //"負傷部位数," +
                //    //"初検年月日1," +
                //    //"請求額," +
                //    //"施術開始年月日1," +
                //    //"施術所郵便番号," +
                //    //"施術所住所," +
                //    //"給付割合," +
                //    "照会結果," +
                //    "返戻理由";



                //    #endregion

                //    #region sql
                //    strSQL =
                //        "SELECT " +
                //        "comnum," +
                //        "ym," +
                //        "hMark," +
                //        "hNum," +
                //        "psex," +
                //        //"pbirthday," +
                //        "pname," +
                //        //"hname," +
                //        "fushoname1," +
                //        "total," +
                //        //"drname," +
                //        //"clinicname," +
                //        //"upd," +
                //        //"haddress," +
                //        "family," +
                //        //"fushodays1," +
                //        //"fushocount," +
                //        //"shoken1," +
                //        //"charge," +
                //        //"startdate1," +
                //        //"cliniczip," +
                //        //"clinicaddress," +
                //        //"ratio," +
                //        "shokaireason," +
                //        "henreireason " +
                //        $"FROM export where cym ={cym}";
                //    if (strPosix != string.Empty) strSQL += $" and hMark {strPosix}";

                //    #endregion


                //    break;

                #endregion

                default:
                    break;

            }




            wb=new NPOI.XSSF.UserModel.XSSFWorkbook();
            wb.CreateSheet("sheet1");

            NPOI.SS.UserModel.ISheet sh = wb.GetSheetAt(0);

            NPOI.SS.UserModel.IRow r;

            using (var cmd = DB.Main.CreateCmd(strSQL))
            {
                try
                {         
                    var res = cmd.TryExecuteReaderList();
                    if (res.Count == 0) return true;


                    #region データ
                    for (int cntRes = 0; cntRes < res.Count; cntRes++)
                    {
                        //ヘッダを飛ばして2行から
                        r = sh.CreateRow(cntRes + 1);

                        if (res == null || res.Count == 0) continue;

                        for (int c = 0; c < res[cntRes].Length; c++)
                        {

                            NPOI.SS.UserModel.ICell cell = r.CreateCell(c);
                            //cell.CellStyle = csData;

                            cell.SetCellValue(res[cntRes][c].ToString());
                           
                        }

                    }

                 
                    #endregion

                    #region セルスタイル
                    NPOI.SS.UserModel.IFont font = wb.CreateFont();
                    font.FontName = "ＭＳ ゴシック";
                    font.FontHeightInPoints = 9;

                    NPOI.SS.UserModel.ICellStyle csHeader=wb.CreateCellStyle();
                    csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                    csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                    csHeader.SetFont(font);
                
                    
                    NPOI.SS.UserModel.ICellStyle csData = wb.CreateCellStyle();
                    csData.FillPattern = NPOI.SS.UserModel.FillPattern.NoFill;
                    csData.SetFont(font);
                    #endregion

                    #region ヘッダ行
                    r = sh.CreateRow(0);

                    var hs = strHeader.Split(',');
                    for (int cnt = 0; cnt < hs.Length; cnt++){
                        NPOI.SS.UserModel.ICell hdcell = r.CreateCell(cnt);
                        hdcell.CellStyle = csHeader;
                        hdcell.SetCellValue(hs[cnt]);
                    
                    }
                    #endregion

                    //オートフィルタ
                    NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(0, sh.LastRowNum,0, hs.Length-1);
                    sh.SetAutoFilter(rng);

                    for (int c = 0; c < res[0].Length; c++)
                    {
                        sh.AutoSizeColumn(c);
                    }


                    System.IO.FileStream fs =
                        new System.IO.FileStream(strFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    wb.Write(fs);
                    wb.Close();

                    return true;
                }
                catch(Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
            }



        }



    }

    /// <summary>
    /// 既出テーブルだが使わないかも
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
