﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.NagoyashiKokuho
{
    /// <summary>
    /// 保険者から提供される　点検対象者一覧データテーブルクラス
    /// </summary>
    class memberdata
    {
        #region テーブルクラス
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int memberdataid { get; set; }             //メディ管理用 →nextvalで取得するのでここでは無視する
        public int cym { get; set; }                      //メホール処理年月

        public string renban { get; set; }               //連番 18 　　 
        public string insnum { get; set; }               //保険者番号 00234XXX  
        public string insname { get; set; }              //保険者名 名古屋市中村区  
        public string hihoNum1 { get; set; }             //被保険者証番号 XXXXXXXX ８けた 
        public string hihoNum2 { get; set; }             //被保険者証番号2 XXXXXXXX ８けた 
        public string pnum1 { get; set; }                //個人番号 XXXXXXXXXX １０けた 
        public string pnum2 { get; set; }                //個人番号2 XXXXXXXXXX １０けた 
        public string hihoname_no_use { get; set; }      //被保険者氏名 〇〇　〇〇  この氏名は使わない。（外字未対応のため）
        public string ratio { get; set; }                //給付割合 08  
        public string family { get; set; }               //退職本被 前期  
        public string clinicnum { get; set; }            //医療機関コード 2358003XXX  
        public string clinicname { get; set; }           //医療機関名 〇〇接骨院  
        public string ka { get; set; }                   //診療科   
        public string shinryoym { get; set; }            //診療年月 31.04  
        public string startdate1 { get; set; }           //診療開始日 31.03.06  この診療年月日は使わない。１部位目の診療年月日をひろっているだけのため
        public string nyugai { get; set; }               //入外 外  
        public string publicexpance { get; set; }        //公費負担者番号   
        public string jukyunum { get; set; }             //受給者番号   
        public string counteddays { get; set; }          //療養の給付等－日数 4  この日数は使わない。１部位目の施術日数をひろっているだけのため
        public string total { get; set; }                //療養の給付等－決定点数 4880  
        public string partial { get; set; }              //療養の給付等－一部負担金   
        public string mealdays { get; set; }             //食事－日数   
        public string mealcost { get; set; }             //食事－決定基準額   
        public string shoriym { get; set; }              //処理年月 01.05  
        public string kokuhoreznum { get; set; }         //国保連レセプト番号 905140202XXX  
        public string comnum { get; set; }               //レセプト全国共通キー 2350105073000002XXXX  
        public string sikaku { get; set; }               //資格   
        public string biko { get; set; }                 //備考   
        public string tukinaka { get; set; }             //月中   
        public string hihoname { get; set; }             //被保険者氏名 〇〇　〇〇  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
        public string setainame { get; set; }            //世帯主氏名 〇〇　△△  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
        public string hihobirthday { get; set; }         //生年月日 3221206  
        public string finishdate1 { get; set; }          //終了日   
        public string zip1 { get; set; }                 //郵便番号１ 453  全件データに転記し、実態調査に使用。　
        public string zip2 { get; set; }                 //郵便番号２ XXXX  全件データに転記し、実態調査に使用。　
        public string add1 { get; set; }                 //住所 中村区△△町１丁目２３番地  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
        public string add2 { get; set; }                 //住所方書 △△住宅１棟〇〇号  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
        public string chk { get; set; }                  //チェック   
        public string kiji { get; set; }                 //記事   この欄に「調査対象外」とある場合は実態調査は送らない。
        public string bunrui { get; set; }               //分類 ＡかB  A・Bで条件が違います。
        #endregion

        
        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int _cym)
        {

            //ファイル選択
            //excel開く
            //インポート
            //db登録

                        
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx|すべてのファイル|*.*";
            ofd.FilterIndex = 0;
            ofd.Title = "名古屋市国保 点検対象者一覧Excel";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;


            if (!chkAlreadyImport(_cym)) return false;

            if (!ImportExcel(fileName, _cym)) return false;

            System.Windows.Forms.MessageBox.Show("完了");

            return true ;


        }
        static string strerr = string.Empty;

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static string GetCellValuesAuto(NPOI.SS.UserModel.ICell cell, string strFldName,int lineNo)
        {

            if (cell == null) return string.Empty;

            string res = string.Empty;

            //NPOIは値の型によってプロパティ名違う
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    res = cell.StringCellValue;
                    break;

                case NPOI.SS.UserModel.CellType.Numeric:
                    res = cell.NumericCellValue.ToString();
                    break;

                default:
                    break;

            }


            if(!chkLength(res, strFldName)) return "x";
          

            return res;

        }


        /// <summary>
        /// 既に存在するかチェック
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool chkAlreadyImport(int cym)
        {
            DB.Command cmd = new DB.Command(DB.Main, $"select * from memberdata where cym={cym} limit 1");
            var tmp=cmd.TryExecuteScalar();
            if (tmp != null)
            {
                if(System.Windows.Forms.MessageBox.Show($"{cym}は既に取り込んでいます。削除しますか？","",
                    System.Windows.Forms.MessageBoxButtons.YesNo,
                    System.Windows.Forms.MessageBoxIcon.Question,
                    System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    cmd = new DB.Command(DB.Main, $"delete from memberdata where cym={cym}");
                    cmd.TryExecuteNonQuery();
                    System.Windows.Forms.MessageBox.Show("削除しました");
                    return true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("取込を中止します");
                    return false;
                }
            }

            return true;

        }

        /// <summary>
        /// 提供データをインポート
        /// </summary>
        /// <param name="strFilePath">ファイル名</param>
        /// <param name="cym">メホールのchargeYM</param>
        /// <returns></returns>
        private static bool ImportExcel(string strFilePath,int cym)
        {
            NPOI.SS.UserModel.IWorkbook wb=null;
            NPOI.SS.UserModel.ISheet ws;

            DB.Transaction tran=DB.Main.CreateTransaction();
            

            WaitForm wf = new WaitForm();

            //テーブルmemberdataの属性情報取得
            if (!dbSchema.getSchema("memberdata"))
            {
                System.Windows.Forms.MessageBox.Show("スキーマ取得に失敗しました");
                return false;
            }


            try
            {
                wb= NPOI.SS.UserModel.WorkbookFactory.Create(strFilePath);
                ws = wb.GetSheetAt(0);

                wf.ShowDialogOtherTask();
                wf.SetMax(ws.LastRowNum );


                for (int r = 0; r <= ws.LastRowNum; r++)
                {
                    if (r == 0) continue;

                    int c = 0;
                    memberdata cls = new memberdata();
                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);
                    NPOI.SS.UserModel.ICell nullcell=null;




                    cls.cym = cym;          //メホール上の処理年月（請求年月）
                    cls.renban = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "renban",r);                        //連番 18                     
                    cls.insnum = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "insnum",r);                        //保険者番号 00234XXX  
                    cls.insname = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "insname",r);                      //保険者名 名古屋市中村区  
                    cls.hihoNum1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "hihonum1",r);                    //被保険者証番号 XXXXXXXX ８けた 
                    cls.hihoNum2 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "hihonum2",r);                    //被保険者証番号2 XXXXXXXX ８けた 
                    cls.pnum1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "pnum1",r);                          //個人番号 XXXXXXXXXX １０けた 
                    cls.pnum2 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "pnum2",r);                          //個人番号2 XXXXXXXXXX １０けた 
                    cls.hihoname_no_use = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "hihoname_no_use",r);      //被保険者氏名 〇〇　〇〇  この氏名は使わない。（外字未対応のため）
                    cls.ratio = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "ratio",r);                          //給付割合 08  
                    cls.family = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "family",r);                        //退職本被 前期  
                    cls.clinicnum = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "clinicnum",r);                  //医療機関コード 2358003XXX  
                    cls.clinicname = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "clinicname",r);                //医療機関名 〇〇接骨院  
                    cls.ka = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "ka",r);                                //診療科   
                    cls.shinryoym = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "shinryoym",r);                  //診療年月 31.04  
                    cls.startdate1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "startdate1",r);                //診療開始日 31.03.06  この診療年月日は使わない。１部位目の診療年月日をひろっているだけのため
                    cls.nyugai = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "nyugai",r);                        //入外 外  
                    cls.publicexpance = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "publicexpance",r);          //公費負担者番号   
                    cls.jukyunum = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "jukyunum",r);                    //受給者番号   
                    cls.counteddays = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "counteddays",r);              //療養の給付等－日数 4  この日数は使わない。１部位目の施術日数をひろっているだけのため
                    cls.total = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "total",r);                          //療養の給付等－決定点数 4880  
                    cls.partial = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "partial",r);                      //療養の給付等－一部負担金   
                    cls.mealdays = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "mealdays",r);                    //食事－日数   
                    cls.mealcost = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "mealcost",r);                    //食事－決定基準額   
                    cls.shoriym = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "shoriym",r);                      //処理年月 01.05  
                    cls.kokuhoreznum = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "kokuhoreznum",r);            //国保連レセプト番号 905140202XXX  
                    cls.comnum = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "comnum",r);                        //レセプト全国共通キー 2350105073000002XXXX  
                    cls.sikaku = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "sikaku",r);                        //資格   
                    cls.biko = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "biko",r);                            //備考   
                    cls.tukinaka = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "tukinaka",r);                    //月中   
                    cls.hihoname = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "hihoname",r);                    //被保険者氏名 〇〇　〇〇  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
                    cls.setainame = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "setainame",r);                  //世帯主氏名 〇〇　△△  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
                    cls.hihobirthday = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "hihobirthday",r);            //生年月日 3221206  
                    cls.finishdate1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "finishdate1",r);              //終了日   
                    cls.zip1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "zip1",r);                            //郵便番号１ 453  全件データに転記し、実態調査に使用。　
                    cls.zip2 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "zip2",r);                            //郵便番号２ XXXX  全件データに転記し、実態調査に使用。　
                    cls.add1 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "add1",r);                            //住所 中村区△△町１丁目２３番地  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
                    cls.add2 = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "add2",r);                            //住所方書 △△住宅１棟〇〇号  全件データに転記し、実態調査に使用。　◆がある場合は、外字のため別シートの画像を参照
                    cls.chk = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "chk",r);                              //チェック   
                    cls.kiji = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "kiji",r);                            //記事   この欄に「調査対象外」とある場合は実態調査は送らない。
                    cls.bunrui = GetCellValuesAuto(row.GetCell(c++) ?? nullcell, "bunrui",r);                        //分類 ＡかB  A・Bで条件が違います。


                    if (strerr != string.Empty)
                    {
                        //エラーメッセージがある場合警告とエラーログ表示、ロールバック
                        Log.ErrorWriteWithMsg($"{r}行目 {strerr}");
                        Log.OpenErrorFileByTextEditor();
                        strerr = string.Empty;
                        tran.Rollback();
                        return false;
                    }

                    wf.LogPrint($"連番:{cls.renban} 個人番号1:{cls.pnum1}");
                    wf.InvokeValue++;
                    
                    if (!DB.Main.Insert<memberdata>(cls, tran))
                    {
                        //20200621124812 furukawa st ////////////////////////
                        //登録エラー時メッセージ
                        
                        System.Windows.Forms.MessageBox.Show(
                            System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n個人番号1:" +
                            cls.pnum1);
                        //20200621124812 furukawa ed ////////////////////////

                        return false;
                    }
                }


                tran.Commit();

                return true;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(
                   System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);

                return false;
            }
            finally
            {
                if(wb!=null)wb.Close();
                wf.Dispose();
            }
        }


        public static List<memberdata> select(string strwhere)
        {
            List<memberdata> lst = new List<memberdata>();
            var l=DB.Main.Select<memberdata>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }


        /// <summary>
        /// 各列長さチェック 長すぎる場合、strerrにメッセージ追加
        /// </summary>
        /// <param name="strValue"></param>
        /// <param name="strFldName"></param>
        /// <returns></returns>
        private static bool chkLength(string strValue, string strFldName)
        {
            

            switch (strFldName)
            {
                case "renban": if (dbSchema.dic["renban"].fldlen < strValue.Length) { strerr+= dbSchema.dic["renban"].column_name+"桁オーバー"; } break;
                case "insnum": if (dbSchema.dic["insnum"].fldlen < strValue.Length) { strerr+= dbSchema.dic["insnum"].column_name+"桁オーバー"; } break;
                case "insname": if (dbSchema.dic["insname"].fldlen < strValue.Length) { strerr+= dbSchema.dic["insname"].column_name+"桁オーバー"; } break;
                case "hihonum1": if (dbSchema.dic["hihonum1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["hihoNum1"].column_name+"桁オーバー"; } break;
                case "hihonum2": if (dbSchema.dic["hihonum2"].fldlen < strValue.Length) { strerr+= dbSchema.dic["hihoNum2"].column_name+"桁オーバー"; } break;
                case "pnum1": if (dbSchema.dic["pnum1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["pnum1"].column_name+"桁オーバー"; } break;
                case "pnum2": if (dbSchema.dic["pnum2"].fldlen < strValue.Length) { strerr+= dbSchema.dic["pnum2"].column_name+"桁オーバー"; } break;
                case "hihoname_no_use": if (dbSchema.dic["hihoname_no_use"].fldlen < strValue.Length) { strerr+= dbSchema.dic["hihoname_no_use"].column_name+"桁オーバー"; } break;
                case "ratio": if (dbSchema.dic["ratio"].fldlen < strValue.Length) { strerr+= dbSchema.dic["ratio"].column_name+"桁オーバー"; } break;
                case "family": if (dbSchema.dic["family"].fldlen < strValue.Length) { strerr+= dbSchema.dic["family"].column_name+"桁オーバー"; } break;
                case "clinicnum": if (dbSchema.dic["clinicnum"].fldlen < strValue.Length) { strerr+= dbSchema.dic["clinicnum"].column_name+"桁オーバー"; } break;
                case "clinicname": if (dbSchema.dic["clinicname"].fldlen < strValue.Length) { strerr+= dbSchema.dic["clinicname"].column_name+"桁オーバー"; } break;
                case "ka": if (dbSchema.dic["ka"].fldlen < strValue.Length) { strerr+= dbSchema.dic["ka"].column_name+"桁オーバー"; } break;
                case "shinryoym": if (dbSchema.dic["shinryoym"].fldlen < strValue.Length) { strerr+= dbSchema.dic["shinryoym"].column_name+"桁オーバー"; } break;
                case "startdate1": if (dbSchema.dic["startdate1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["startdate1"].column_name+"桁オーバー"; } break;
                case "nyugai": if (dbSchema.dic["nyugai"].fldlen < strValue.Length) { strerr+= dbSchema.dic["nyugai"].column_name+"桁オーバー"; } break;
                case "publicexpance": if (dbSchema.dic["publicexpance"].fldlen < strValue.Length) { strerr+= dbSchema.dic["publicexpance"].column_name+"桁オーバー"; } break;
                case "jukyunum": if (dbSchema.dic["jukyunum"].fldlen < strValue.Length) { strerr+= dbSchema.dic["jukyunum"].column_name+"桁オーバー"; } break;
                case "counteddays": if (dbSchema.dic["counteddays"].fldlen < strValue.Length) { strerr+= dbSchema.dic["counteddays"].column_name+"桁オーバー"; } break;
                case "total": if (dbSchema.dic["total"].fldlen < strValue.Length) { strerr+= dbSchema.dic["total"].column_name+"桁オーバー"; } break;
                case "partial": if (dbSchema.dic["partial"].fldlen < strValue.Length) { strerr+= dbSchema.dic["partial"].column_name+"桁オーバー"; } break;
                case "mealdays": if (dbSchema.dic["mealdays"].fldlen < strValue.Length) { strerr+= dbSchema.dic["mealdays"].column_name+"桁オーバー"; } break;
                case "mealcost": if (dbSchema.dic["mealcost"].fldlen < strValue.Length) { strerr+= dbSchema.dic["mealcost"].column_name+"桁オーバー"; } break;
                case "shoriym": if (dbSchema.dic["shoriym"].fldlen < strValue.Length) { strerr+= dbSchema.dic["shoriym"].column_name+"桁オーバー"; } break;
                case "kokuhoreznum": if (dbSchema.dic["kokuhoreznum"].fldlen < strValue.Length) { strerr+= dbSchema.dic["kokuhoreznum"].column_name+"桁オーバー"; } break;
                case "comnum": if (dbSchema.dic["comnum"].fldlen < strValue.Length) { strerr+= dbSchema.dic["comnum"].column_name+"桁オーバー"; } break;
                case "sikaku": if (dbSchema.dic["sikaku"].fldlen < strValue.Length) { strerr+= dbSchema.dic["sikaku"].column_name+"桁オーバー"; } break;
                case "biko": if (dbSchema.dic["biko"].fldlen < strValue.Length) { strerr+= dbSchema.dic["biko"].column_name+"桁オーバー"; } break;
                case "tukinaka": if (dbSchema.dic["tukinaka"].fldlen < strValue.Length) { strerr+= dbSchema.dic["tukinaka"].column_name+"桁オーバー"; } break;
                case "hihoname": if (dbSchema.dic["hihoname"].fldlen < strValue.Length) { strerr+= dbSchema.dic["hihoname"].column_name+"桁オーバー"; } break;
                case "setainame": if (dbSchema.dic["setainame"].fldlen < strValue.Length) { strerr+= dbSchema.dic["setainame"].column_name+"桁オーバー"; } break;
                case "hihobirthday": if (dbSchema.dic["hihobirthday"].fldlen < strValue.Length) { strerr+= dbSchema.dic["hihobirthday"].column_name+"桁オーバー"; } break;
                case "finishdate1": if (dbSchema.dic["finishdate1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["finishdate1"].column_name+"桁オーバー"; } break;
                case "zip1": if (dbSchema.dic["zip1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["zip1"].column_name+"桁オーバー"; } break;
                case "zip2": if (dbSchema.dic["zip2"].fldlen < strValue.Length) { strerr+= dbSchema.dic["zip2"].column_name+"桁オーバー"; } break;
                case "add1": if (dbSchema.dic["add1"].fldlen < strValue.Length) { strerr+= dbSchema.dic["add1"].column_name+"桁オーバー"; } break;
                case "add2": if (dbSchema.dic["add2"].fldlen < strValue.Length) { strerr+= dbSchema.dic["add2"].column_name+"桁オーバー"; } break;
                case "chk": if (dbSchema.dic["chk"].fldlen < strValue.Length) { strerr+= dbSchema.dic["chk"].column_name+"桁オーバー"; } break;
                case "kiji": if (dbSchema.dic["kiji"].fldlen < strValue.Length) { strerr+= dbSchema.dic["kiji"].column_name+"桁オーバー"; } break;
                case "bunrui": if (dbSchema.dic["bunrui"].fldlen < strValue.Length) { strerr+= dbSchema.dic["bunrui"].column_name+"桁オーバー"; } break;

            }

            if (strerr != string.Empty) return false;            

            return true;
        }

      

    }
    /// <summary>
    /// テーブルの列の長さを取得
    /// </summary>
    partial class dbSchema
    {
        public string table_catalog { get; set; } = string.Empty;
        public string table_name { get; set; } = string.Empty;
        public int ordinal_position { get; set; } = 0;
        public string column_name { get; set; } = string.Empty;
        public string data_type { get; set; } = string.Empty;
        public int fldlen { get; set; } = 0;

        /// <summary>
        /// テーブル列の長さを格納
        /// </summary>
        public static Dictionary<string, dbSchema> dic = new Dictionary<string, dbSchema>();


        /// <summary>
        /// テーブルスキーマ取得
        /// </summary>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public static bool getSchema(string strTableName)
        {

            string strsql =
               " select " +
               " table_catalog," +
               " table_name," +
               " ordinal_position," +
               " column_name," +
               " data_type," +

               " case data_type" +
               " 	when 'text' then character_maximum_length" +
               " 	when 'character varying' then character_maximum_length" +
               " 	when 'bit' then character_maximum_length" +
               " 	else numeric_precision" +
               " end as fldlen" +

               " from information_schema.columns" +
               $" where table_name='{strTableName}'" +
               " order by ordinal_position";

            DB.Command cmd = new DB.Command(DB.Main, strsql);

            var l = cmd.TryExecuteReaderList();
            dic.Clear();

            for (int r = 0; r < l.Count; r++)
            {
                dbSchema clsscm = new dbSchema();
                clsscm.table_catalog = l[r][0].ToString();
                clsscm.table_name = l[r][1].ToString();
                clsscm.ordinal_position = int.Parse(l[r][2].ToString());
                clsscm.column_name = l[r][3].ToString();
                clsscm.data_type = l[r][4].ToString();
                clsscm.fldlen = int.Parse(l[r][5].ToString());

                dic.Add(l[r][3].ToString(), clsscm);

            }

            return true;
        }

    }

}
