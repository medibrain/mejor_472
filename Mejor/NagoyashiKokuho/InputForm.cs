﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.NagoyashiKokuho

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;
        private string rrid = string.Empty;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(100, 0);                
        Point posCost = new Point(800, 1800);
        Point poshnum = new Point(2000, 0);

        Point posDr = new Point(10, 2500);
        Point posPersonalData = new Point(100, 0);

        //20200527155146 furukawa st ////////////////////////
        //実日数の画像座標を個人情報のコントロールと同じにする
        
        Point posCountedDays = new Point(100, 0);
        //Point posCountedDays = new Point(100, 50);
        //Point posCountedDays = new Point(100, 1000);
        //20200527155146 furukawa ed ////////////////////////



        Control[] ymControls, costControls, hnumControls, drControls, personalControls,daysControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] {
                verifyBoxY, verifyBoxM,
            };


            //上段
            hnumControls = new Control[]
            {
                verifyBoxHnum,verifyBoxHnumM,
                verifyBoxClinicCode,
                verifyBoxRatio,verifyBoxFamily,
                
            };

            //個人情報
            personalControls = new Control[]
            {
                verifyBoxBD,verifyBoxBE,verifyBoxBM,verifyBoxBY,verifyBoxSex,
            };

            //実日数
            daysControls = new Control[]
            {
                verifyBoxCountedDays,
            };

            //合計金額箇所
            costControls = new Control[]
            {
                verifyBoxTotal,verifyBoxCharge,verifyBoxHosName,verifyBoxPartial,
            };

            drControls = new Control[] { verifyBoxDrName, };

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            panelHnum.Visible = false;


            #region 提供データリスト
            var l = new List<memberdata>();
            l = memberdata.select("1=1");

            //dgv.DataSource = l;

            setMemberDataGridViewFormat(l);
            #endregion


            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            
            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);            
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            //point(横位置,縦位置);
            //Point posYM = new Point(100, 0);
            //Point posCost = new Point(800, 1800);
            //Point poshnum = new Point(2000, 0);
            //Point posDr = new Point(10, 2500);
            //Point posPersonalData = new Point(100, 0);            
            //Point posCountedDays = new Point(100, 1000);

            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = poshnum;
            else if (drControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDr;
            else if (personalControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPersonalData;
            else if (daysControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCountedDays;
            
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// 登録ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }
        #endregion


        #region 提供データグリッド初期化
        /// <summary>
        /// 提供データグリッド初期化
        /// </summary>
        /// <param name="k"></param>
        private void setMemberDataGridViewFormat(List<memberdata> k)
        {
            dgv.DataSource = k;

            dgv.Columns[nameof(memberdata.memberdataid)].HeaderText = "id";
            dgv.Columns[nameof(memberdata.cym)].HeaderText = "cym";
            dgv.Columns[nameof(memberdata.renban)].HeaderText = "連番";
            dgv.Columns[nameof(memberdata.insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(memberdata.insname)].HeaderText = "保険者名";
            dgv.Columns[nameof(memberdata.hihoNum1)].HeaderText = "被保険者証番号";
            dgv.Columns[nameof(memberdata.hihoNum2)].HeaderText = "被保険者証番号2";
            dgv.Columns[nameof(memberdata.pnum1)].HeaderText = "個人番号";
            dgv.Columns[nameof(memberdata.pnum2)].HeaderText = "個人番号2";
            dgv.Columns[nameof(memberdata.hihoname_no_use)].HeaderText = "被保険者氏名";
            dgv.Columns[nameof(memberdata.ratio)].HeaderText = "給付割合";
            dgv.Columns[nameof(memberdata.family)].HeaderText = "退職本被";
            dgv.Columns[nameof(memberdata.clinicnum)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(memberdata.clinicname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(memberdata.ka)].HeaderText = "診療科";
            dgv.Columns[nameof(memberdata.shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(memberdata.startdate1)].HeaderText = "診療開始日";
            dgv.Columns[nameof(memberdata.nyugai)].HeaderText = "入外";
            dgv.Columns[nameof(memberdata.publicexpance)].HeaderText = "公費負担者番号";
            dgv.Columns[nameof(memberdata.jukyunum)].HeaderText = "受給者番号";
            dgv.Columns[nameof(memberdata.counteddays)].HeaderText = "日数";
            dgv.Columns[nameof(memberdata.total)].HeaderText = "決定点数";
            dgv.Columns[nameof(memberdata.partial)].HeaderText = "一部負担金";
            dgv.Columns[nameof(memberdata.mealdays)].HeaderText = "食事－日数";
            dgv.Columns[nameof(memberdata.mealcost)].HeaderText = "食事－決定基準額";
            dgv.Columns[nameof(memberdata.shoriym)].HeaderText = "処理年月";
            dgv.Columns[nameof(memberdata.kokuhoreznum)].HeaderText = "国保連レセプト番号";
            dgv.Columns[nameof(memberdata.comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(memberdata.sikaku)].HeaderText = "資格";
            dgv.Columns[nameof(memberdata.biko)].HeaderText = "備考";
            dgv.Columns[nameof(memberdata.tukinaka)].HeaderText = "月中";
            dgv.Columns[nameof(memberdata.hihoname)].HeaderText = "被保険者氏名";
            dgv.Columns[nameof(memberdata.setainame)].HeaderText = "世帯主氏名";
            dgv.Columns[nameof(memberdata.hihobirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(memberdata.finishdate1)].HeaderText = "終了日";
            dgv.Columns[nameof(memberdata.zip1)].HeaderText = "郵便番号１";
            dgv.Columns[nameof(memberdata.zip2)].HeaderText = "郵便番号２";
            dgv.Columns[nameof(memberdata.add1)].HeaderText = "住所";
            dgv.Columns[nameof(memberdata.add2)].HeaderText = "住所方書";
            dgv.Columns[nameof(memberdata.chk)].HeaderText = "チェック";
            dgv.Columns[nameof(memberdata.kiji)].HeaderText = "記事";
            dgv.Columns[nameof(memberdata.bunrui)].HeaderText = "分類";

            dgv.Columns[nameof(memberdata.memberdataid)].Visible = true;
            dgv.Columns[nameof(memberdata.cym)].Visible = false;
            dgv.Columns[nameof(memberdata.renban)].Visible = false;
            dgv.Columns[nameof(memberdata.insnum)].Visible = false;
            dgv.Columns[nameof(memberdata.insname)].Visible = false;
            dgv.Columns[nameof(memberdata.hihoNum1)].Visible = true;
            dgv.Columns[nameof(memberdata.hihoNum2)].Visible = false;
            dgv.Columns[nameof(memberdata.pnum1)].Visible = false;
            dgv.Columns[nameof(memberdata.pnum2)].Visible = false;
            dgv.Columns[nameof(memberdata.hihoname_no_use)].Visible = true;
            dgv.Columns[nameof(memberdata.ratio)].Visible = true;
            dgv.Columns[nameof(memberdata.family)].Visible = true;
            dgv.Columns[nameof(memberdata.clinicnum)].Visible = true;
            dgv.Columns[nameof(memberdata.clinicname)].Visible = true;
            dgv.Columns[nameof(memberdata.ka)].Visible = false;
            dgv.Columns[nameof(memberdata.shinryoym)].Visible = true;
            dgv.Columns[nameof(memberdata.startdate1)].Visible = true;
            dgv.Columns[nameof(memberdata.nyugai)].Visible = false;
            dgv.Columns[nameof(memberdata.publicexpance)].Visible = true;
            dgv.Columns[nameof(memberdata.jukyunum)].Visible = true;
            dgv.Columns[nameof(memberdata.counteddays)].Visible = true;
            dgv.Columns[nameof(memberdata.total)].Visible = true;
            dgv.Columns[nameof(memberdata.partial)].Visible = true;
            dgv.Columns[nameof(memberdata.mealdays)].Visible = false;
            dgv.Columns[nameof(memberdata.mealcost)].Visible = false;
            dgv.Columns[nameof(memberdata.shoriym)].Visible = false;
            dgv.Columns[nameof(memberdata.kokuhoreznum)].Visible = true;
            dgv.Columns[nameof(memberdata.comnum)].Visible = true;
            dgv.Columns[nameof(memberdata.sikaku)].Visible = true;
            dgv.Columns[nameof(memberdata.biko)].Visible = false;
            dgv.Columns[nameof(memberdata.tukinaka)].Visible = false;
            dgv.Columns[nameof(memberdata.hihoname)].Visible = false;
            dgv.Columns[nameof(memberdata.setainame)].Visible = false;
            dgv.Columns[nameof(memberdata.hihobirthday)].Visible = false;
            dgv.Columns[nameof(memberdata.finishdate1)].Visible = false;
            dgv.Columns[nameof(memberdata.zip1)].Visible = false;
            dgv.Columns[nameof(memberdata.zip2)].Visible = false;
            dgv.Columns[nameof(memberdata.add1)].Visible = false;
            dgv.Columns[nameof(memberdata.add2)].Visible = false;
            dgv.Columns[nameof(memberdata.chk)].Visible = false;
            dgv.Columns[nameof(memberdata.kiji)].Visible = false;
            dgv.Columns[nameof(memberdata.bunrui)].Visible = false;


            dgv.Columns[nameof(memberdata.memberdataid)].Width = 100;
            dgv.Columns[nameof(memberdata.cym)].Width = 50;
            dgv.Columns[nameof(memberdata.renban)].Width = 50;
            dgv.Columns[nameof(memberdata.insnum)].Width = 50;
            dgv.Columns[nameof(memberdata.insname)].Width = 50;
            dgv.Columns[nameof(memberdata.hihoNum1)].Width = 80;
            dgv.Columns[nameof(memberdata.hihoNum2)].Width = 50;
            dgv.Columns[nameof(memberdata.pnum1)].Width = 50;
            dgv.Columns[nameof(memberdata.pnum2)].Width = 50;
            dgv.Columns[nameof(memberdata.hihoname_no_use)].Width = 100;
            dgv.Columns[nameof(memberdata.ratio)].Width = 30;
            dgv.Columns[nameof(memberdata.family)].Width = 30;
            dgv.Columns[nameof(memberdata.clinicnum)].Width = 100;
            dgv.Columns[nameof(memberdata.clinicname)].Width = 150;
            dgv.Columns[nameof(memberdata.ka)].Width = 50;
            dgv.Columns[nameof(memberdata.shinryoym)].Width = 50;
            dgv.Columns[nameof(memberdata.startdate1)].Width = 50;
            dgv.Columns[nameof(memberdata.nyugai)].Width = 50;
            dgv.Columns[nameof(memberdata.publicexpance)].Width = 50;
            dgv.Columns[nameof(memberdata.jukyunum)].Width = 50;
            dgv.Columns[nameof(memberdata.counteddays)].Width = 50;
            dgv.Columns[nameof(memberdata.total)].Width = 50;
            dgv.Columns[nameof(memberdata.partial)].Width = 50;
            dgv.Columns[nameof(memberdata.mealdays)].Width = 50;
            dgv.Columns[nameof(memberdata.mealcost)].Width = 50;
            dgv.Columns[nameof(memberdata.shoriym)].Width = 50;
            dgv.Columns[nameof(memberdata.kokuhoreznum)].Width = 150;
            dgv.Columns[nameof(memberdata.comnum)].Width = 150;
            dgv.Columns[nameof(memberdata.sikaku)].Width = 50;
            dgv.Columns[nameof(memberdata.biko)].Width = 50;
            dgv.Columns[nameof(memberdata.tukinaka)].Width = 50;
            dgv.Columns[nameof(memberdata.hihoname)].Width = 50;
            dgv.Columns[nameof(memberdata.setainame)].Width = 50;
            dgv.Columns[nameof(memberdata.hihobirthday)].Width = 50;
            dgv.Columns[nameof(memberdata.finishdate1)].Width = 50;
            dgv.Columns[nameof(memberdata.zip1)].Width = 50;
            dgv.Columns[nameof(memberdata.zip2)].Width = 50;
            dgv.Columns[nameof(memberdata.add1)].Width = 50;
            dgv.Columns[nameof(memberdata.add2)].Width = 50;
            dgv.Columns[nameof(memberdata.chk)].Width = 50;
            dgv.Columns[nameof(memberdata.kiji)].Width = 50;
            dgv.Columns[nameof(memberdata.bunrui)].Width = 50;




        }

        #endregion

        #region 更新
        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="app">applicationクラス</app>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力値チェック
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1 );
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month>12);

          
            //被保番
            string hnum = verifyBoxHnum.Text.Trim();
            setStatus(verifyBoxHnum, hnum == string.Empty);
            //string hnum = $"{verifyBoxHnumM.Text.Trim()}-{verifyBoxHnum.Text.Trim()}";

            //本家区分
            int honkeKubun = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !new[] { 0, 2, 4, 6, 8 }.Contains(honkeKubun));
            
            //本人家族区別
            int honnin = verifyBoxHon.GetIntValue();
            setStatus(verifyBoxHon, !new[] { 2, 6 }.Contains(honnin));

            //割合
            int ratio = verifyBoxRatio.GetIntValue();
            setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

         
            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //施術師名
            setStatus(verifyBoxDrName, verifyBoxDrName.Text.Length < 3);

            //施術所名
            //setStatus(verifyBoxHosName, verifyBoxHosName.Text.Length < 3);

            //施術所コード
            setStatus(verifyBoxClinicCode, verifyBoxClinicCode.Text.Length < 9);

            //実日数
            int counteddays = verifyBoxCountedDays.GetIntValue();
            setStatus(verifyBoxCountedDays, counteddays <= 0);
           
            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //一部負担金
            int partial = verifyBoxPartial.GetIntValue();
            setStatus(verifyBoxPartial, partial < 100 | 200000 < partial);

            //金額割合チェック
            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 10 | total < charge);

            //請求金額が合計金額の7～10割の金額と合致すればOK
            bool payError = false;

            //if ((int)(total * 70 / 100) != charge &&
            //    (int)(total * 80 / 100) != charge &&
            //    (int)(total * 90 / 100) != charge &&
            //    (int)(total * 100 / 100) != charge)
            //{
            //    payError = true;
            //}

            //請求＜一時負担金の場合注意
            if (charge < partial)
            {
                payError = true;
            }
            else
            {
                payError = false;
            }


            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;           //合計
                verifyBoxCharge.BackColor = Color.GreenYellow;          //請求金額
                verifyBoxPartial.BackColor = Color.GreenYellow;         //一部負担金
                var res = MessageBox.Show("合計金額・請求金額・一部負担金のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }
            #endregion

            #region 値の反映
            //値の反映

            //施術年
            app.MediYear = year;

            //施術月
            app.MediMonth = month;

            
            //保険者番号
           // app.InsNum = insnum;

            //ヒホバン
            app.HihoNum = hnum;

            //本家区分
            //200 202 204 206 208 600 602 604 606 608

            //本家区分
            //0：高七　
            //2：本人
            //4：六歳　
            //6：家族
            //8：高一　

            //本人区別
            //2：本人　
            //6：家族　
            app.Family = honnin * 100 + honkeKubun;
            
            //割合
            app.Ratio = ratio;
            //性別
            app.Sex = sex;
            //生年月日
            app.Birthday = birthDt;


            //合計金額
            app.Total = total;
            //請求金額
            app.Charge = charge;

            //一部負担金
            app.Partial = partial;

            //施術師コード
            //app.DrNum = verifyBoxClinicCode.Text.Trim();


            //施術師名
            app.DrName = verifyBoxDrName.Text.Trim();
            //施術所名
            app.ClinicName = verifyBoxHosName.Text.Trim();

            //医療機関コード（施術所コード
            app.ClinicNum = verifyBoxClinicCode.Text.Trim();

            //実日数
            app.CountedDays = counteddays;

            //20200527181254 furukawa st ////////////////////////
            //申請書種別の登録            
            app.AppType = scan.AppType;
            //20200527181254 furukawa ed ////////////////////////


            //点検対象者（提供データ）のidを紐づける
            if (dgv.CurrentRow != null)
            {
                app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells["memberdataid"].Value.ToString();

                //レセプト全国共通キー
                if (dgv.CurrentRow.Cells["comnum"].Value.ToString() != string.Empty)
                    app.ComNum = dgv.CurrentRow.Cells["comnum"].Value.ToString();
            }
            #endregion

            return true;
        }

         
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        #endregion


        #region ロード
        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //マッチング表示初期化
            labelMacthCheck.BackColor = Color.Pink;
            labelMacthCheck.Text = "マッチング無し";
            labelMacthCheck.Visible = true;

            dgv.DataSource = null;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
        
            changedReset(app);
        }

    
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            //このへんにグリッド初期化処理

            dgv.DataSource = null;


            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号
                setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);

                //var hn = app.HihoNum.Split('-');
                //if (hn.Length == 2)
                //{
                //    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                // setValue(verifyBoxHnum, hn[1], firstTime, nv);
                //}
                //else
                //{
                //    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                //    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                //}


                //申請書
                //施術年月
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);

         
                //給付割合
                setValue(verifyBoxRatio, app.Ratio, firstTime, nv);

           
                //本家区分と本人区別をわけてそれぞれの入力欄にいれる
                if (app.Family.ToString().Length == 3) setValue(verifyBoxFamily, app.Family.ToString().Substring(2, 1), firstTime, nv);
                setValue(verifyBoxHon, app.Family.ToString().Substring(0, 1), firstTime, nv);


                //性別
                setValue(verifyBoxSex, app.Sex, firstTime, nv);


                //生年月日
                int era = DateTimeEx.GetEraNumber(app.Birthday);
                int WarekiYear = DateTimeEx.GetJpYear(app.Birthday);

                setValue(verifyBoxBE, era, firstTime, nv);
                setValue(verifyBoxBY, WarekiYear, firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);

                //合計
                setValue(verifyBoxTotal, app.Total, firstTime, nv);

                //請求
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);

                //一部負担金
                setValue(verifyBoxPartial, app.Partial, firstTime,nv);

                //実日数
                setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                //施術師コード
                setValue(verifyBoxClinicCode, app.DrNum, firstTime, nv);

                //施術所名
                setValue(verifyBoxHosName, app.ClinicName, firstTime, nv);

                //施術師名
                setValue(verifyBoxDrName, app.DrName, firstTime, nv);

                //施術所コード（医療機関コード
                setValue(verifyBoxClinicCode, app.ClinicNum, firstTime, nv);

                //点検対象者一覧データ

                //string strbirth = verifyBoxBE.Text + verifyBoxBY.Text.PadLeft(2, '0') + verifyBoxBM.Text.PadLeft(2, '0') + verifyBoxBD.Text.PadLeft(2, '0');
                string strMediYM = $"{verifyBoxY.Text.PadLeft(2, '0')}.{verifyBoxM.Text.PadLeft(2,'0')}";
                dispMemberData(app,
                    verifyBoxHnum.Text.Trim(), strMediYM, verifyBoxClinicCode.Text, verifyBoxTotal.Text);


            }
        }

        #endregion


        /// <summary>
        /// 点検対象者一覧をグリッドに出す
        /// </summary>
        /// <param name="app">app</param>
        /// <param name="hnum">被保険者証番号</param>
        /// <param name="strMediYM">診療年月</param>       
        /// <param name="clinicnum">医療機関コード</param>
        /// <param name="total">合計金額</param>        
        /// <returns></returns>
        private bool dispMemberData(App app, string hnum, 
            string strMediYM,string clinicnum,
            string total)
        {
            //メホールの請求年月、被保険者証番号、医療機関コード、診療年月
            int cym = app.CYM;
            try
            {

                //レコード抽出 
                List<memberdata> lstMemberData= new List<memberdata>();
                string strsql = $" cym={cym} " +
                $" and hihonum1='{hnum}'" +                
                $" and shinryoym='{strMediYM}'" +
                $" and clinicnum='{clinicnum}'" + 
                $" and total='{total}'";


                //dgv.DataSource = lstMemberData.select(strsql);

                var l = memberdata.select(strsql);
                //グリッドデータ設定
                setMemberDataGridViewFormat(l);


                #region マッチング判定
                if (l == null || l.Count == 0)
                {
                    labelMacthCheck.BackColor = Color.Pink;
                    labelMacthCheck.Text = "マッチング無し";
                    labelMacthCheck.Visible = true;
                }
                else if (l.Count == 1)
                {
                    labelMacthCheck.BackColor = Color.Cyan;
                    labelMacthCheck.Text = "マッチングOK";
                    labelMacthCheck.Visible = true;
                }

                else
                {
                    for (int i = 0; i < dgv.RowCount; i++)
                    {
                        //20200529112158 furukawa st ////////////////////////
                        //memberdataに複数候補があった場合のマッチング結果
                        
                        var md =dgv[nameof(memberdata.memberdataid), i].Value;

                        if (app.TaggedDatas.GeneralString1.Trim() == md.ToString())
                        {
                            //既に一意の広域データとマッチング済みの場合
                            labelMacthCheck.BackColor = Color.Cyan;
                            labelMacthCheck.Text = "マッチングOK";
                            labelMacthCheck.Visible = true;

                            //グリッドの現在のセルが不可視の場合選択しない（例外発生する）
                            if (dgv[0, i].Visible) dgv.CurrentCell = dgv[0, i];

                            return true;
                        }
                        //20200529112158 furukawa ed ////////////////////////

                    }

                    labelMacthCheck.BackColor = Color.Yellow;
                    labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                    labelMacthCheck.Visible = true;
                    dgv.CurrentCell = null;

                }
                #endregion

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

      


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                //panelFusho.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelHnum.Visible = true;
            //    panelFusho.Visible = true;
                
                //施術月
                verifyBoxM.Visible = true;
                labelM.Visible = true;
             

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }


        /// <summary>
        /// 変更した座標を記憶
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (hnumControls.Contains(t)) poshnum = pos;
            else if (drControls.Contains(t)) posDr = pos;
            else if (personalControls.Contains(t)) posPersonalData = pos;
            else if (daysControls.Contains(t)) posCountedDays = pos;
            
        }



        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    //20200527144532 furukawa st ////////////////////////
                    //拡大率を画像の端を基準にする
                    
                    scrollPictureControl1.Ratio = 0.4f;
                    //scrollPictureControl1.Ratio = 0.6f;
                    //20200527144532 furukawa ed ////////////////////////


                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }




        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            searchMemberData();
        }

        private void verifyBoxCharge_Validated(object sender, EventArgs e)
        {            
            //合計-請求=一部負担金
            if (!int.TryParse(verifyBoxTotal.Text.Trim(), out int total)) return;
            if (!int.TryParse(verifyBoxCharge.Text.Trim(), out int charge)) return;            
            setValue(verifyBoxPartial, total - charge < 0 ? string.Empty : (total - charge).ToString(),true,true);
        }

        private void verifyBoxFamily_TextChanged(object sender, EventArgs e)
        {
            //受診者区分が2.本人の場合は本家区分も2.本人とする
            if (verifyBoxFamily.Text.ToString() == "2")
            {
                verifyBoxHon.Text = "2";
            }
            //受診者区分が6.家族の場合は本家区分も6.家族とする
            else if (verifyBoxFamily.Text.ToString() == "6")
            {
                verifyBoxHon.Text = "6";
            }
        }



        /// <summary>
        /// 照会対象者データ検索
        /// </summary>
        private void searchMemberData()
        {

            if (
                verifyBoxHnum.Text.Trim() == string.Empty ||
                verifyBoxClinicCode.Text.Trim() == string.Empty ||
                verifyBoxTotal.Text.Trim() == string.Empty
                ) return;

            //被保険者記号、番号、医療機関コード、合計金額
            //if (verifyBoxHnumM.Text.Trim() == string.Empty ||
            //    verifyBoxHnum.Text.Trim() == string.Empty ||
            //    verifyBoxClinicCode.Text.Trim()==string.Empty ||
            //    verifyBoxFamily.Text.Trim() == string.Empty ||
            //    verifyBoxTotal.Text.Trim()==string.Empty
            //    ) return;

            var app = (App)bsApp.Current;

            //string strbirth = verifyBoxBE.Text + verifyBoxBY.Text.PadLeft(2, '0')
            //    + verifyBoxBM.Text.PadLeft(2, '0') + verifyBoxBD.Text.PadLeft(2, '0');

            string strSinryoYM = $"{verifyBoxY.Text.PadLeft(2, '0')}.{verifyBoxM.Text.PadLeft(2, '0')}";

            dispMemberData(app,
                verifyBoxHnum.Text.Trim(),
                strSinryoYM,
                verifyBoxClinicCode.Text.Trim(),
                verifyBoxTotal.Text.Trim()
                );

            //dispKyufuData(app, verifyBoxHnumM.Text.Trim(), verifyBoxHnum.Text.Trim(), verifyBoxSex.Text.Trim(), strbirth);
        }

        private void verifyBoxClinicCode_Leave(object sender, EventArgs e)
        {
            searchMemberData();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }       
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

    
    }
}
