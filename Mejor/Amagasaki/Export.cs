﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.Amagasaki
{
    public class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,審査年,審査月,束番号,子番号,保険者番号,被保険者番号,受療者氏名,生年号,生年,生月,生日,性別," +
                        "郵便番号,住所,施術年,施術月,実日数,施術機関番号,施術機関名,決定金額,公費負担者番号,受給者番号," +
                        "電算処理番号,部位数,照会回数,照会理由,過誤理由,再審査理由,点検メモ";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    foreach (var item in list)
                    {
                        var kd = RefRece.Select(item.RrID);
                        if (kd == null)
                        {
                            throw new Exception("広域からのデータがない、またはOCR確認が済んでいない申請書が照会対象として指定されています" +
                            "\r\nID:" + item.Aid.ToString());
                        }

                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString("00"));
                        ss.Add(item.ChargeMonth.ToString("00"));
                        ss.Add(kd.SheafNum.ToString());
                        ss.Add(kd.ChildNum.ToString());
                        ss.Add(kd.InsurerNum);
                        ss.Add(item.HihoNum);
                        ss.Add(item.PersonName);
                        ss.Add(DateTimeEx.GetEraShort(item.Birthday));
                        ss.Add(DateTimeEx.GetJpYear(item.Birthday).ToString("00"));
                        ss.Add(item.Birthday.Month.ToString("00"));
                        ss.Add(item.Birthday.Day.ToString("00"));
                        ss.Add(item.Sex == 1 ? "男" : "女");
                        ss.Add(item.HihoZip);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(kd.ClinicNum);
                        ss.Add(kd.ClinicName);
                        ss.Add(item.Total.ToString());
                        ss.Add(kd.KouhiNum);
                        ss.Add(kd.JyukyuNum);
                        ss.Add(kd.Numbering);
                        ss.Add(item.Bui.ToString());
                        ss.Add(string.Empty);//照会回数
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");
                        ss.Add("\"" + item.KagoReasonStr + "\"");
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add("\"" + item.MemoInspect + "\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool ImageExport(int cym)
        {
            string fn, dir;
            using (var f = new SaveFileDialog())
            {
                f.FileName = $"ExportLog{DateTime.Today.ToString("yyyyMMdd")}.log";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fn = f.FileName;
                dir = System.IO.Path.GetDirectoryName(fn);
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            var fc = new TiffUtility.FastCopy();
            int exportYM = DateTimeEx.Int6YmAddMonth(cym, -1);
            var expotrDt = new DateTime(exportYM / 100, exportYM % 100, 1);
            string y = DateTimeEx.GetJpYear(expotrDt).ToString("00");
            string m = (exportYM % 100).ToString("00");

            try
            {
                wf.LogPrint("スキャンデータを取得しています");
                var scans = Scan.GetScanListYM(cym);
                var dic = new Dictionary<int, Scan>();
                scans.ForEach(x => dic.Add(x.SID, x));

                wf.LogPrint("申請書データを取得しています");
                var apps = App.GetApps(cym);
                
                wf.LogPrint("参照データを取得しています");
                var refs = new Dictionary<int, RefRece>();
                RefRece.SelectAllCym(cym).ForEach(x => refs.Add(x.RrID, x));

                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                string imgName = string.Empty;
                int page = 0;

                var appGroups = apps.GroupBy(x => x.ScanID);

                foreach (var groupApps in appGroups)
                {
                    int appCount = 0;
                    int imageCount = 0;
                    var scan = dic[groupApps.First().ScanID];
                    wf.LogPrint($"束番号[{scan.Note1}]の画像データを保存しています");
                    foreach (var item in groupApps)
                    {
                        //20210623141949 furukawa st ////////////////////////
                        //中断処理
                        
                        if (CommonTool.WaitFormCancelProcess(wf)) return false;
                        //20210623141949 furukawa ed ////////////////////////


                        wf.InvokeValue++;

                        if (item.AppType > 0)
                        {
                            var rr = RefRece.Select(item.RrID);
                            var imgDir = $"{dir}\\{y}-{m}-{scan.Note1}";
                            Directory.CreateDirectory(imgDir);

                            imgName = imgDir + "\\" +
                                $"{y}-{m}-{rr.SheafNum}-{rr.ChildNum.ToString().PadLeft(3, '0')}-" +
                                $"{rr.PersonName}";

                            //20210623152558 furukawa st ////////////////////////
                            //コピーエラーの場合IO例外
                            
                            if (!fc.FileCopy(item.GetImageFullPath(), imgName + ".tif"))
                            {
                                throw new IOException();
                            }
                            //fc.FileCopy(item.GetImageFullPath(), imgName + ".tif");
                            //20210623152558 furukawa ed ////////////////////////

                            imageCount++;
                            appCount++;
                            page = 0;
                        }
                        else if (item.AppType == APP_TYPE.続紙 && imgName != string.Empty)
                        {
                            imageCount++;
                            page++;
                            fc.FileCopy(item.GetImageFullPath(), imgName + $"★{page}.tif");
                        }
                    }
                    wf.LogPrint($"{appCount}件の申請書({imageCount}件の画像データ)を保存しました");
                }
                wf.LogPrint("画像出力が正常に完了しました");
            }
            catch (Exception ex)
            {
                wf.LogPrint(ex.Message);
                wf.LogPrint("エラーが発生しています ログをご確認下さい");
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.LogSave(fn);
                wf.Dispose();
            }
            return true;
        }

    }
}
