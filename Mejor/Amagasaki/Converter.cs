﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Amagasaki
{
    class Converter
    {
        [DB.DbAttribute.DifferentTableName("application")]
        public class AdjustApp
        {
            [DB.DbAttribute.PrimaryKey]
            [DB.DbAttribute.UpdateIgnore]
            public int AID { get; set; }
            [DB.DbAttribute.UpdateIgnore]
            public int YM { get; set; }
            public DateTime iDate1 { get; set; }
            public DateTime iFirstDate1 { get; set; }
            public DateTime iStartDate1 { get; set; }
            public NEW_CONT fChargeType { get; set; }
        }

        public static List<AdjustApp> Select(int cym)
        {
            var l = DB.Main.Select<AdjustApp>($"cym={cym} AND ym>0")?.ToList() ?? new List<AdjustApp>();
            l.Sort((x, y) => x.AID.CompareTo(y.AID));
            return l;
        }

        public static bool Convert(int cym)
        {
            var l = Select(cym);

            using (var t = DB.Main.CreateTransaction())
            {
                foreach (var item in l)
                {
                    item.iStartDate1 = item.iFirstDate1;
                    item.iFirstDate1 = item.iDate1;
                    item.fChargeType =
                        item.YM == (item.iFirstDate1.Year * 100 + item.iFirstDate1.Month) ?
                        NEW_CONT.新規 : NEW_CONT.継続;
                    if (!DB.Main.Update(item, t)) return false;
                }
                t.Commit();
                return true;
            }
        }
    }
}
