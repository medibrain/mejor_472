﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
//using NpgsqlTypes;

namespace Mejor.Amagasaki
{
    public partial class old_InputForm : InputFormCore
    {
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsRefRece = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(2000, 3000);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 800);
        Point posDays = new Point(600, 300);
        Point posNewCont = new Point(800, 1000);
        Point posOryo = new Point(400, 300);
        Point posReason = new Point(1350, 650);

        //サイン欄
        Point posName = new Point(390, 400);
        Point posSign = new Point(1450, 2800);

        Control[] ymControls, hihoNumControls, personControls, dayControls, FushoReasonControls,
            fushoControls, newContControls, reasonControls;

        public old_InputForm(ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxChild };
            hihoNumControls = new Control[] { verifyBoxNum };
            personControls = new Control[] { };
            dayControls = new Control[] { };
           // FushoReasonControls = new Control[] { verifyCheckBoxFushoReason };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            newContControls = new Control[] { };
            reasonControls = new Control[] { verifyCheckBoxFushoReason };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            var dgp = dataGridViewPlist;
            for (int j = 1; j < dgp.ColumnCount; j++) dgp.Columns[j].Visible = false;
            dgp.Columns[nameof(App.Aid)].Visible = true;
            dgp.Columns[nameof(App.Aid)].Width = 50;
            dgp.Columns[nameof(App.Aid)].HeaderText = "ID";
            dgp.Columns[nameof(App.HihoNum)].Visible = true;
            dgp.Columns[nameof(App.HihoNum)].Width = 70;
            dgp.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dgp.Columns[nameof(App.InputStatus)].Visible = true;
            dgp.Columns[nameof(App.InputStatus)].Width = 70;
            dgp.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dgp.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dgp.Columns[nameof(App.Numbering)].Visible = true;
            dgp.Columns[nameof(App.Numbering)].Width = 70;
            dgp.Columns[nameof(App.Numbering)].HeaderText = "子番号";
            dgp.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            bsRefRece.DataSource = new List<RefRece>();
            dataGridRefRece.DataSource = bsRefRece;

            var dgrr = dataGridRefRece;
            for (int i = 1; i < dgrr.ColumnCount; i++) dgrr.Columns[i].Visible = false;
            dgrr.Columns[nameof(RefRece.RrID)].Visible = true;
            dgrr.Columns[nameof(RefRece.RrID)].Width = 50;
            dgrr.Columns[nameof(RefRece.RrID)].HeaderText = "ID";
            dgrr.Columns[nameof(RefRece.SheafNum)].Visible = true;
            dgrr.Columns[nameof(RefRece.SheafNum)].Width = 70;
            dgrr.Columns[nameof(RefRece.SheafNum)].HeaderText = "束番号";
            dgrr.Columns[nameof(RefRece.ChildNum)].Visible = true;
            dgrr.Columns[nameof(RefRece.ChildNum)].Width = 70;
            dgrr.Columns[nameof(RefRece.ChildNum)].HeaderText = "子番号";
            dgrr.Columns[nameof(RefRece.HihoNum)].Visible = true;
            dgrr.Columns[nameof(RefRece.HihoNum)].Width = 70;
            dgrr.Columns[nameof(RefRece.HihoNum)].HeaderText = "被保番";
            dgrr.Columns[nameof(RefRece.MediYM)].Visible = true;
            dgrr.Columns[nameof(RefRece.MediYM)].Width = 70;
            dgrr.Columns[nameof(RefRece.MediYM)].HeaderText = "診療月";
            dgrr.Columns[nameof(RefRece.PersonName)].Visible = true;
            dgrr.Columns[nameof(RefRece.PersonName)].Width = 100;
            dgrr.Columns[nameof(RefRece.PersonName)].HeaderText = "受療者名";
            dgrr.Columns[nameof(RefRece.ClinicName)].Visible = true;
            dgrr.Columns[nameof(RefRece.ClinicName)].Width = 100;
            dgrr.Columns[nameof(RefRece.ClinicName)].HeaderText = "診療所名";

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (Control)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (reasonControls.Contains(t)) scrollPictureControl1.ScrollPosition = posReason;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //子番号
            int childNum = verifyBoxChild.GetIntValue();

            //被保険者番号チェック 1文字以上かつ数字に直せること
            int num = verifyBoxNum.GetIntValue();
            setStatus(verifyBoxNum, num < 0);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            
            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //データ突合
            var rr = (RefRece)bsRefRece.Current;
            if (dataGridRefRece.CurrentCell == null || rr == null)
            {
                MessageBox.Show("関連付ける情報を選択してください",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                return false;
            }

            //値の反映

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //app.MediYear = DateTimeEx.GetHsYearFromAd(rr.MediYM / 100);
            app.MediMonth = rr.MediYM % 100;
            app.MediYear = DateTimeEx.GetHsYearFromAd(rr.MediYM / 100, app.MediMonth);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.HihoNum = verifyBoxNum.Text.Trim();
            app.Sex = rr.Sex;
            app.Birthday = rr.Birth;
            app.CountedDays = rr.Days;
            app.Total = rr.Total;
            app.NewContType = rr.MediYM == (rr.FirstDate.Year * 100+ rr.FirstDate.Month) ? NEW_CONT.新規 : NEW_CONT.継続;
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            app.FushoStartDate1 = rr.StartDate;
            app.FushoFirstDate1 = rr.FirstDate;
            app.PersonName = rr.PersonName;
            app.RrID = rr.RrID;
            app.AppType = scan.AppType;
            app.Numbering = childNum.ToString();
            app.ClinicNum = rr.ClinicNum;
            app.ClinicName = rr.ClinicName;
            app.InsNum = rr.InsurerNum;
            app.ComNum = rr.Numbering;
            
            app.TaggedDatas.KouhiNum = rr.KouhiNum;
            app.TaggedDatas.JukyuNum = rr.JyukyuNum;
            app.GroupNum = rr.SheafNum.ToString();
            

            //点検情報の付与
            if (verifyCheckBoxSign.Checked || verifyCheckBoxFushoReason.Checked)
            {
                app.StatusFlagSet(StatusFlag.点検対象);
            }

            app.KagoReasonsRemove(KagoReasons_.署名違い | KagoReasons_.原因なし);
            if (verifyCheckBoxSign.Checked) app.KagoReasonsSet(KagoReasons_.署名違い);
            if (verifyCheckBoxFushoReason.Checked) app.KagoReasonsSet(KagoReasons_.原因なし);

            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (verifyBoxChild.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxChild.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換

                if (!app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput, tran)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        
        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全ボックスクリア
            iVerifiableAllClear(panelRight);
            bsRefRece.Clear();

            //入力ユーザー表示
            labelInputerName.Text = "入力：  " + User.GetUserName(app.Ufirst);
            
            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                //OCRデータがあれば、部位のみ挿入
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }

            //簡易点検
            verifyCheckBoxSign.Checked = app.KagoReasonsCheck(KagoReasons_.署名違い);
            verifyCheckBoxFushoReason.Checked = app.KagoReasonsCheck(KagoReasons_.原因なし);

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;

                    scrollPictureControlName.SetImage(img, fn);
                    scrollPictureControlSign.SetImage(img, fn);
                }
                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App app)
        {
            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxChild.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxChild.Text = "++";
            }
            else
            {
                //申請書
                verifyBoxChild.Text = app.Numbering;
                verifyBoxNum.Text = app.HihoNum;

                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;

                selecrRefRece(app);
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxChild, labelYearInfo, labelH,
                labelInputerName};

            void visible(bool b)
            {
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is IVerifiable || item is Label)) continue;
                    if (cs.Contains(item)) continue;
                    item.Visible = b;
                }
            }

            if (verifyBoxChild.Text.Length == 2 && 
                (verifyBoxChild.Text == "--" || verifyBoxChild.Text == "++"))
            {
                //続紙その他
                visible(false);
            }
            else
            {
                //申請書の場合
                visible(true);
            }
            bsRefRece.Clear();
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void verifyCheckBoxSign_Enter(object sender, EventArgs e)
        {
            SuspendLayout();
            scrollPictureControlName.Visible = true;
            scrollPictureControlSign.Visible = true;
            scrollPictureControlName.ScrollPosition = posName;
            scrollPictureControlSign.ScrollPosition = posSign;
            scrollPictureControl1.Height = scrollPictureControl1.Height - scrollPictureControlName.Height;
            ResumeLayout();
        }

        private void verifyCheckBoxSign_Leave(object sender, EventArgs e)
        {
            scrollPictureControlName.Visible = false;
            scrollPictureControlSign.Visible = false;
            scrollPictureControl1.Height = scrollPictureControl1.Height + scrollPictureControlName.Height;
        }

        private void verifyCheckBoxFushoReason_Enter(object sender, EventArgs e)
        {
            SuspendLayout();
            scrollPictureControlName.Width = scrollPictureControl1.Width;
            scrollPictureControlName.Visible = true;
            scrollPictureControlName.Ratio = 0.6f;
            scrollPictureControlName.ScrollPosition = posReason;

            scrollPictureControl1.Height = scrollPictureControl1.Height - scrollPictureControlName.Height;
            scrollPictureControl1.ScrollPosition = posFusho;
            ResumeLayout();
        }

        private void verifyCheckBoxFushoReason_Leave(object sender, EventArgs e)
        {
            scrollPictureControlName.Visible = false;
            scrollPictureControlName.Width = 340;
            scrollPictureControlName.Ratio = 0.4f;
            scrollPictureControl1.Height = scrollPictureControl1.Height + scrollPictureControlName.Height;
        }

        private void verifyBoxNum_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selecrRefRece(app);
        }

        private void selecrRefRece(App app)
        {
            var child = verifyBoxChild.GetIntValue();
            int.TryParse(scan.Note1, out int sheafNum);

            var l = RefRece.Select(app.CYM, child, verifyBoxNum.Text.Trim());
            if (100 < sheafNum && sheafNum < 900) l = l.FindAll(x => x.SheafNum == sheafNum);
            bsRefRece.DataSource = l;
            bsRefRece.ResetBindings(false);

            if (l.Count != 1)
            {
                var index = l.FindIndex(x => x.RrID == app.RrID);
                if (index == -1)
                {
                    dataGridRefRece.CurrentCell = null;
                }
                else
                {
                    bsRefRece.Position = index;
                }
            }
        }

        private void verifyBoxNum_TextChanged(object sender, EventArgs e)
        {
            bsRefRece.Clear();
        }

        private void scrollPictureControlName_ImageScrolled(object sender, EventArgs e)
        {
            var t = (Control)ActiveControl;
            if (t == verifyCheckBoxSign) posName = scrollPictureControlName.ScrollPosition;
            else posReason = scrollPictureControlName.ScrollPosition;
        }
        
        private void scrollPictureControlSign_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControlSign.ScrollPosition;
            posSign = pos;
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is IVerifiable)) return;

            var t = (Control)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (reasonControls.Contains(t)) posReason = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        
        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxChild.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
