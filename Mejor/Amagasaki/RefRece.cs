﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;

namespace Mejor.Amagasaki
{
    class RefRece
    {
        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int RrID { get; set; }
        public int ChargeYM { get; set; }
        public int SheafNum { get; set; }
        public int ChildNum { get; set; }
        public int MediYM { get; set; }
        public string InsurerNum { get; set; } = string.Empty;
        public string HihoNum { get; set; } = string.Empty;
        public string KouhiNum { get; set; } = string.Empty;
        public string JyukyuNum { get; set; } = string.Empty;
        public string PersonName { get; set; } = string.Empty;
        public int Sex { get; set; }
        public DateTime Birth { get; set; } = DateTime.MinValue;
        public DateTime FirstDate { get; set; } = DateTime.MinValue;
        public DateTime StartDate { get; set; } = DateTime.MinValue;
        public int Days { get; set; }
        public int Total { get; set; }
        public string ClinicNum { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public string Numbering { get; set; } = string.Empty;

        public static bool Import(string csvFilePath)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                var l = new List<RefRece>();
                wf.LogPrint("CSVファイルを読み込んでいます");


                #region クラスに入れる
                try
                {
                    var csv = CommonTool.CsvImportShiftJis(csvFilePath);
                    foreach (var ss in csv)
                    {
                        int cy, cm, my, mm, days, total, by, bm, bd;

                        //20190901122212 furukawa st ////////////////////////
                        //変換時に取得するので不要

                        //var startDtStr = "4" + ss[11].Trim().Replace(".", "");
                        //var firstDtStr = "4" + ss[28].Trim().Replace(".", "");
                        //20190901122212 furukawa ed ////////////////////////


                                                                                                        //支払データのヘッダより
                        if (!int.TryParse(ss[2], out cy)) continue;                                     //審査年		
                        if (!int.TryParse(ss[3], out cm)) continue;                                     //審査月
                        if (!int.TryParse(ss[16], out my)) continue;                                    //施療年
                        if (!int.TryParse(ss[17], out mm)) continue;                                    //施療月
                        if (!int.TryParse(ss[30], out days)) continue;                                  //実日数
                        if (!int.TryParse(ss[13].Replace(",", ""), out total)) continue;                //決定金額
                        if (!int.TryParse(ss[21], out by)) continue;                                    //生年月日年
                        if (!int.TryParse(ss[22], out bm)) continue;                                    //生年月日月
                        if (!int.TryParse(ss[23], out bd)) continue;                                    //生年月日日


                        //20190901121859 furukawa st ////////////////////////
                        //西暦変換改修

                        //if (!int.TryParse(startDtStr, out int startDtInt)) continue;
                        //var startDt = DateTimeEx.GetDateFromJInt7(startDtInt);
                        //if (startDt == DateTimeEx.DateTimeNull) continue;
                        //if (!int.TryParse(firstDtStr, out int firstDtInt)) continue;
                        //var firstDt = DateTimeEx.GetDateFromJInt7(firstDtInt);
                        //if (firstDt == DateTimeEx.DateTimeNull) continue;


                        DateTime startDt = changeToAD(ss[11].Trim().Replace(".", ""));     //施術開始年月日
                        if (startDt == DateTimeEx.DateTimeNull) continue;

                        DateTime firstDt = changeToAD(ss[28].Trim().Replace(".", ""));      //初検年月日
                        if (firstDt == DateTimeEx.DateTimeNull) continue;
                        //20190901121859 furukawa ed ////////////////////////



                        var r = new RefRece();


                        //生年月日年号

                        //20190407095959 furukawa st ////////////////////////
                        //新元号対応
                        // var era = ss[20] == "明" ? 1 : ss[20] == "大" ? 2 :
                        //   ss[20] == "昭" ? 3 : ss[20] == "平" ? 4 : 0;

                        var era = ss[20] == "明" ? 1 : ss[20] == "大" ? 2 :
                            ss[20] == "昭" ? 3 : ss[20] == "平" ? 4 : 
                            ss[20] == "令" ? 5 : 0 ;
                        //20190407095959 furukawa ed ////////////////////////


                        var birth = DateTimeEx.GetDateFromJInt7(era * 1000000 +
                            by * 10000 + bm * 100 + bd);


                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応


                        //20190515100844 furukawa st ////////////////////////
                        //YMフィールドのMが抜けていた

                        r.ChargeYM = DateTimeEx.GetAdYearFromHs(cy * 100 + cm) * 100 + cm;
                        r.MediYM =DateTimeEx.GetAdYearFromHs(my * 100 + mm) * 100 + mm;
                            //r.ChargeYM = DateTimeEx.GetAdYearFromHs(cy * 100 + cm);
                            //r.MediYM = DateTimeEx.GetAdYearFromHs(my * 100 + mm);

                        //20190515100844 furukawa ed ////////////////////////


                        //r.ChargeYM = DateTimeEx.GetAdYearFromHs(cy) * 100 + cm;
                        //r.MediYM = DateTimeEx.GetAdYearFromHs(my) * 100 + mm;


                        r.InsurerNum = ss[5].Trim();
                        r.HihoNum = ss[19].Trim();
                        r.KouhiNum = ss[9].Trim();
                        r.JyukyuNum = ss[26].Trim();
                        r.PersonName = ss[8].Trim();
                        r.Sex = ss[25].Trim() == "男" ? 1 : 2;
                        r.Birth = birth;
                        r.StartDate = startDt;
                        r.FirstDate = firstDt;
                        r.Days = days;
                        r.Total = total;
                        r.ClinicNum = ss[7].Trim();
                        r.ClinicName = ss[18].Trim();
                        r.Numbering = ss[33].Trim();//レセプト全国共通キー
                        l.Add(r);
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }

                #endregion

                #region 束番号と子番号
                //束番号801から開始、390枚ごとにリセット
                //また保険者番号が67始まりになった時、束番号850とする
                wf.LogPrint("束番号と子番号を付与しています");
                int taba = 801;
                int cnum = 1;
                foreach (var item in l)
                {
                    if (item.InsurerNum.Remove(2) == "67" && taba < 850)
                    {
                        taba = 850;
                        cnum = 1;
                    }

                    if (cnum == 391)
                    {
                        taba++;
                        cnum = 1;
                    }

                    item.SheafNum = taba;
                    item.ChildNum = cnum;
                    cnum++;
                }

                #endregion

                #region refreceに登録

                wf.LogPrint("データベースに登録しています");
                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                try
                {
                    
                    using (var tran = DB.Main.CreateTransaction())
                    {
                        var t = new List<RefRece>();

                        foreach (var item in l)
                        {
                            t.Add(item);
                            if (t.Count == 100)
                            {
                                if (!DB.Main.Inserts(t, tran)) return false;
                                wf.InvokeValue += 100;
                                t.Clear();
                            }
                        }

                        if (t.Count != 0 && !DB.Main.Inserts(t, tran)) return false;
                        tran.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
                #endregion


            }
        }

        /// <summary>
        /// refreceが存在するか判断し、削除するか聞く
        /// </summary>
        /// <param name="cym">請求年月</param>
        /// <returns>true=レコード存在,false=レコードなし</returns>
        public static bool chkDataExist(int cym)
        {
            DB.Command cmd;
            cmd = new DB.Command(DB.Main, $"select count(*) from refrece where chargeym={cym}");

            if (!int.TryParse(cmd.TryExecuteScalar().ToString(), out int count)) return false;
            if (count > 0)
            {
                if (System.Windows.Forms.MessageBox.Show($"請求年月{cym}がすでにインポート済みです。再取り込みしますか？", "",
                    System.Windows.Forms.MessageBoxButtons.YesNoCancel,
                    System.Windows.Forms.MessageBoxIcon.Question,
                    System.Windows.Forms.MessageBoxDefaultButton.Button3) == System.Windows.Forms.DialogResult.Yes)
                {
                    cmd = new DB.Command(DB.Main, $"delete from refrece where chargeym={cym}");
                    cmd.TryExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            //20200518174820 furukawa st ////////////////////////
            //戻り値を間違ってた
            
            else return true;
            //else return false;
            //20200518174820 furukawa ed ////////////////////////
        }



        //20200412151815 furukawa st ////////////////////////
        //元画像ファイル名を元に、提供データをApplicationに適用する

        /// <summary>
        /// 提供データをappに適用
        /// </summary>
        /// <param name="cym"></param>
        public static void UpdateApp(int cym)
        {
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();            
            wf.LogPrint("提供データをAppに適用しています");

            //20210623111444 furukawa st ////////////////////////
            //使ってない

            //var lstRefrece = RefRece.SelectAllCym(cym);
            //20210623111444 furukawa ed ////////////////////////


            string strsql = string.Empty;
            try
            {

                DB.Transaction tran = DB.Main.CreateTransaction();
                DB.Command cmd;

                strsql = "update application set " +
                    " ym                = refrece.mediym" +                 //診療年月西暦
                    ",inum 			    = refrece.insurernum " +            //保険者番号
                    ",hnum				= refrece.hihonum " +               //被保険者証番号                
                    ",pbirthday 		= refrece.birth " +                 //生年月日
                    ",psex 			    = refrece.sex " +                   //性別
                    ",sid       		= refrece.clinicnum " +             //医療機関コード                                                
                    ",ifirstdate1 		= refrece.firstdate " +             //初検日
                    ",istartdate1 		= refrece.startdate " +             //施術開始日
                    ",acounteddays   	= refrece.days " +                  //実日数
                    ",atotal 			= refrece.total " +                 //合計金額

                    ",pname 			= refrece.personname " +            //受療者名
                    ",rrid 			    = refrece.rrid " +                  //ファイル名で紐付くが一応

                    //20200418185334 furukawa st ////////////////////////
                    //医療機関名、電算管理番号追加（旧入力画面では画面から入力してたので漏れてた）                    
                    ",comnum 		    = refrece.numbering " +             //電算管理番号（旧入力画面参考）
                    ",sname 		    = refrece.ClinicName " +            //医療機関名
                                                                            //20200418185334 furukawa ed ////////////////////////


                    //新規継続　開始年月＝診療年月の場合新規、それ以外継続
                    ",fchargetype = case when " +
                    "			    cast(" +
                    "                   (cast " +



                    //20200418164456 furukawa st ////////////////////////
                    //新規継続判定基準を初検日に修正

                    "	                (date_part('year',firstdate) as varchar) || lpad(cast(date_part('month',firstdate)as varchar),2,'0')) " +
                    //(date_part('year', startdate) as varchar) || lpad(cast(date_part('month', startdate) as varchar), 2, '0')) " +
                    //20200418164456 furukawa ed ////////////////////////


                    "	            as int)=mediym then " +
                    "               1 " +
                    "           else " +
                    "               2 " +
                    "           end  " +

                    ",groupnum          = refrece.SheafNum " +


                    //公費負担者番号、公費受給者番号
                    //存在する番号のみ入れる、なければそのまま更新
                    ",taggeddatas= " +
                    "case when refrece.kouhinum<>'' and refrece.jyukyunum <>''  then   " +
                    "   taggeddatas || '|kouhinum:|' || '\"' || refrece.kouhinum  || '\"' || '|Jyukyunum:|'  || '\"' ||  refrece.jyukyunum  || '\"'  " +
                    "when refrece.kouhinum='' and refrece.jyukyunum <>''then  " +
                    "   taggeddatas || '|Jyukyunum:|'  || '\"' ||  refrece.jyukyunum  || '\"'  " +
                    "when refrece.kouhinum<>'' and refrece.jyukyunum =''then  " +
                    "   taggeddatas || '|kouhinum:|' || '\"' || refrece.kouhinum  || '\"' " +
                    "else " +
                    "   taggeddatas  " +
                    "end " +


                    //20210623111301 furukawa st ////////////////////////
                    //AUXのファイル名を介して照合
                    

                    " from refrece , application_aux " +
                    " where " +
                    " application_aux.aid=application.aid and " +
                    " regexp_replace(application_aux.origfile,'~*\\.[a-z]{3,5}','') =refrece.numbering " +

                    //" from refrece " +
                    //" where " +
                    //" regexp_replace(emptytext3,'~*\\.[a-z]{3,5}','') =refrece.numbering " +
                    //      "and emptytext3 <>''" +



                    //20210623111301 furukawa ed ////////////////////////


                    "and chargeym=" + cym;


                //" where application.emptytext3=refrece.numbering || '.tif' and emptytext3 <>''";

                cmd = new DB.Command(strsql, tran);
                //System.Diagnostics.Debug.Print(strsql);

                cmd.TryExecuteNonQuery();



                tran.Commit();
                System.Windows.Forms.MessageBox.Show("適用しました");
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

            }
            finally
            {
                wf.Dispose();
            }

        }
        //20200412151815 furukawa ed ////////////////////////


        //20190901122004 furukawa  ////////////////////////
        //eemmddを西暦に変換して返す
        /// <summary>
        /// eemmddを西暦に変換して返す
        /// </summary>
        /// <param name="strDate">eemmdd</param>
        /// <returns>datetime</returns>
        public static DateTime changeToAD(string strDate)
        {
            DateTime outDt = new DateTime(
                          DateTimeEx.GetAdYearFromHs(int.Parse(strDate.ToString().Substring(0, 4))),
                          int.Parse(strDate.ToString().Substring(2, 2)),
                          int.Parse(strDate.ToString().Substring(4, 2)));

            return outDt;
        }


        public static List<RefRece> Select(int cym, int childNum, string hinoNum)
        {
            return DB.Main.Select<RefRece>(
                new { ChargeYM = cym, childNum = childNum, HihoNum = hinoNum }).ToList();
        }

        public static RefRece Select(int rrid)
        {
            return DB.Main.Select<RefRece>(
                new { rrid = rrid }).SingleOrDefault();
        }


        public static List<RefRece> SelectAllCym(int cym)
        {
            return DB.Main.Select<RefRece>(new { ChargeYM = cym }).ToList();
        }

        public static List<RefRece> SelectAll()
        {
            return DB.Main.SelectAll<RefRece>().ToList();
        }

        public static RefRece Select(string searchID)
        {
            return DB.Main.Select<RefRece>(
                new { Numbering = searchID }).SingleOrDefault();
        }
    }
}
