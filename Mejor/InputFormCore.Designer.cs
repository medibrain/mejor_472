﻿namespace Mejor
{
    partial class InputFormCore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelList = new System.Windows.Forms.Panel();
            this._dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this._panelMatchCheckInfo = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.複数月計算ツールToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.次画像参照ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.前画像参照ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.oCR情報ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._radioButtonNotMatch = new System.Windows.Forms.RadioButton();
            this._radioButtonOverlap = new System.Windows.Forms.RadioButton();
            this._labelInfo = new System.Windows.Forms.Label();
            this._panelInfo = new System.Windows.Forms.Panel();
            this._labelNote = new System.Windows.Forms.Label();
            this._labelGroupID = new System.Windows.Forms.Label();
            this._labelScanID = new System.Windows.Forms.Label();
            this._labelSeikyu = new System.Windows.Forms.Label();
            this.inputMemoControl1 = new Mejor.InputMemoControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridViewPlist)).BeginInit();
            this._panelMatchCheckInfo.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this._panelInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelList
            // 
            this.panelList.Controls.Add(this._dataGridViewPlist);
            this.panelList.Controls.Add(this._panelMatchCheckInfo);
            this.panelList.Controls.Add(this._panelInfo);
            this.panelList.Controls.Add(this.inputMemoControl1);
            this.panelList.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelList.Location = new System.Drawing.Point(0, 0);
            this.panelList.Name = "panelList";
            this.panelList.Size = new System.Drawing.Size(214, 536);
            this.panelList.TabIndex = 0;
            // 
            // _dataGridViewPlist
            // 
            this._dataGridViewPlist.AllowUserToAddRows = false;
            this._dataGridViewPlist.AllowUserToDeleteRows = false;
            this._dataGridViewPlist.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this._dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridViewPlist.Location = new System.Drawing.Point(0, 80);
            this._dataGridViewPlist.MultiSelect = false;
            this._dataGridViewPlist.Name = "_dataGridViewPlist";
            this._dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._dataGridViewPlist.RowHeadersVisible = false;
            this._dataGridViewPlist.RowTemplate.Height = 21;
            this._dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dataGridViewPlist.Size = new System.Drawing.Size(214, 366);
            this._dataGridViewPlist.TabIndex = 4;
            this._dataGridViewPlist.TabStop = false;
            this._dataGridViewPlist.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this._dataGridViewPlist_ColumnHeaderMouseClick);
            // 
            // _panelMatchCheckInfo
            // 
            this._panelMatchCheckInfo.BackColor = System.Drawing.Color.Aquamarine;
            this._panelMatchCheckInfo.ContextMenuStrip = this.contextMenuStrip1;
            this._panelMatchCheckInfo.Controls.Add(this._radioButtonNotMatch);
            this._panelMatchCheckInfo.Controls.Add(this._radioButtonOverlap);
            this._panelMatchCheckInfo.Controls.Add(this._labelInfo);
            this._panelMatchCheckInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this._panelMatchCheckInfo.Location = new System.Drawing.Point(0, 35);
            this._panelMatchCheckInfo.Name = "_panelMatchCheckInfo";
            this._panelMatchCheckInfo.Size = new System.Drawing.Size(214, 45);
            this._panelMatchCheckInfo.TabIndex = 12;
            this._panelMatchCheckInfo.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.複数月計算ツールToolStripMenuItem,
            this.次画像参照ToolStripMenuItem,
            this.前画像参照ToolStripMenuItem,
            this.toolStripSeparator1,
            this.oCR情報ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 98);
            // 
            // 複数月計算ツールToolStripMenuItem
            // 
            this.複数月計算ツールToolStripMenuItem.Name = "複数月計算ツールToolStripMenuItem";
            this.複数月計算ツールToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.複数月計算ツールToolStripMenuItem.Text = "複数月計算ツール";
            this.複数月計算ツールToolStripMenuItem.Click += new System.EventHandler(this.複数月計算ツールToolStripMenuItem_Click);
            // 
            // 次画像参照ToolStripMenuItem
            // 
            this.次画像参照ToolStripMenuItem.Name = "次画像参照ToolStripMenuItem";
            this.次画像参照ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.次画像参照ToolStripMenuItem.Text = "次画像参照";
            this.次画像参照ToolStripMenuItem.Click += new System.EventHandler(this.次画像参照ToolStripMenuItem_Click);
            // 
            // 前画像参照ToolStripMenuItem
            // 
            this.前画像参照ToolStripMenuItem.Name = "前画像参照ToolStripMenuItem";
            this.前画像参照ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.前画像参照ToolStripMenuItem.Text = "前画像参照";
            this.前画像参照ToolStripMenuItem.Click += new System.EventHandler(this.前画像参照ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(158, 6);
            // 
            // oCR情報ToolStripMenuItem
            // 
            this.oCR情報ToolStripMenuItem.Name = "oCR情報ToolStripMenuItem";
            this.oCR情報ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.oCR情報ToolStripMenuItem.Text = "OCR情報";
            this.oCR情報ToolStripMenuItem.Click += new System.EventHandler(this.oCR情報ToolStripMenuItem_Click);
            // 
            // _radioButtonNotMatch
            // 
            this._radioButtonNotMatch.AutoSize = true;
            this._radioButtonNotMatch.Location = new System.Drawing.Point(113, 24);
            this._radioButtonNotMatch.Name = "_radioButtonNotMatch";
            this._radioButtonNotMatch.Size = new System.Drawing.Size(98, 16);
            this._radioButtonNotMatch.TabIndex = 42;
            this._radioButtonNotMatch.Text = "マッチなしチェック";
            this._radioButtonNotMatch.UseVisualStyleBackColor = true;
            // 
            // _radioButtonOverlap
            // 
            this._radioButtonOverlap.AutoSize = true;
            this._radioButtonOverlap.Location = new System.Drawing.Point(29, 24);
            this._radioButtonOverlap.Name = "_radioButtonOverlap";
            this._radioButtonOverlap.Size = new System.Drawing.Size(78, 16);
            this._radioButtonOverlap.TabIndex = 43;
            this._radioButtonOverlap.Text = "重複チェック";
            this._radioButtonOverlap.UseVisualStyleBackColor = true;
            // 
            // _labelInfo
            // 
            this._labelInfo.AutoSize = true;
            this._labelInfo.Location = new System.Drawing.Point(5, 5);
            this._labelInfo.Name = "_labelInfo";
            this._labelInfo.Size = new System.Drawing.Size(35, 12);
            this._labelInfo.TabIndex = 44;
            this._labelInfo.Text = "label1";
            // 
            // _panelInfo
            // 
            this._panelInfo.BackColor = System.Drawing.Color.GreenYellow;
            this._panelInfo.ContextMenuStrip = this.contextMenuStrip1;
            this._panelInfo.Controls.Add(this._labelNote);
            this._panelInfo.Controls.Add(this._labelGroupID);
            this._panelInfo.Controls.Add(this._labelScanID);
            this._panelInfo.Controls.Add(this._labelSeikyu);
            this._panelInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this._panelInfo.Location = new System.Drawing.Point(0, 0);
            this._panelInfo.Name = "_panelInfo";
            this._panelInfo.Size = new System.Drawing.Size(214, 35);
            this._panelInfo.TabIndex = 3;
            // 
            // _labelNote
            // 
            this._labelNote.AutoSize = true;
            this._labelNote.Location = new System.Drawing.Point(2, 20);
            this._labelNote.Name = "_labelNote";
            this._labelNote.Size = new System.Drawing.Size(31, 12);
            this._labelNote.TabIndex = 8;
            this._labelNote.Text = "Note:";
            // 
            // _labelGroupID
            // 
            this._labelGroupID.AutoSize = true;
            this._labelGroupID.Location = new System.Drawing.Point(109, 20);
            this._labelGroupID.Name = "_labelGroupID";
            this._labelGroupID.Size = new System.Drawing.Size(48, 12);
            this._labelGroupID.TabIndex = 6;
            this._labelGroupID.Text = "GroupID:";
            // 
            // _labelScanID
            // 
            this._labelScanID.AutoSize = true;
            this._labelScanID.Location = new System.Drawing.Point(109, 3);
            this._labelScanID.Name = "_labelScanID";
            this._labelScanID.Size = new System.Drawing.Size(43, 12);
            this._labelScanID.TabIndex = 0;
            this._labelScanID.Text = "ScanID:";
            // 
            // _labelSeikyu
            // 
            this._labelSeikyu.AutoSize = true;
            this._labelSeikyu.Location = new System.Drawing.Point(2, 3);
            this._labelSeikyu.Name = "_labelSeikyu";
            this._labelSeikyu.Size = new System.Drawing.Size(31, 12);
            this._labelSeikyu.TabIndex = 5;
            this._labelSeikyu.Text = "請求:";
            // 
            // inputMemoControl1
            // 
            this.inputMemoControl1.BackColor = System.Drawing.Color.LightCyan;
            this.inputMemoControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.inputMemoControl1.Location = new System.Drawing.Point(0, 446);
            this.inputMemoControl1.Name = "inputMemoControl1";
            this.inputMemoControl1.Size = new System.Drawing.Size(214, 90);
            this.inputMemoControl1.TabIndex = 13;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(214, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 536);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // InputFormCore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 536);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelList);
            this.Name = "InputFormCore";
            this.Text = "InputFormCore";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridViewPlist)).EndInit();
            this._panelMatchCheckInfo.ResumeLayout(false);
            this._panelMatchCheckInfo.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this._panelInfo.ResumeLayout(false);
            this._panelInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelList;
        private System.Windows.Forms.Panel _panelInfo;
        private System.Windows.Forms.Label _labelNote;
        private System.Windows.Forms.Label _labelGroupID;
        private System.Windows.Forms.Label _labelScanID;
        private System.Windows.Forms.Label _labelSeikyu;
        private System.Windows.Forms.DataGridView _dataGridViewPlist;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel _panelMatchCheckInfo;
        private System.Windows.Forms.RadioButton _radioButtonNotMatch;
        private System.Windows.Forms.RadioButton _radioButtonOverlap;
        private System.Windows.Forms.Label _labelInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 複数月計算ツールToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 次画像参照ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 前画像参照ToolStripMenuItem;
        private InputMemoControl inputMemoControl1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem oCR情報ToolStripMenuItem;
    }
}