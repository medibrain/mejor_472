﻿namespace Mejor
{
    partial class MemoErrorListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonCSV = new System.Windows.Forms.Button();
            this.checkBoxErrorOn = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess1On = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess2On = new System.Windows.Forms.CheckBox();
            this.checkBoxErrorOff = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess1Off = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess2Off = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(980, 369);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(917, 408);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 8;
            this.buttonClose.Text = "閉じる";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCSV.Location = new System.Drawing.Point(836, 408);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(75, 23);
            this.buttonCSV.TabIndex = 7;
            this.buttonCSV.Text = "CSV";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // checkBoxErrorOn
            // 
            this.checkBoxErrorOn.AutoSize = true;
            this.checkBoxErrorOn.Checked = true;
            this.checkBoxErrorOn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxErrorOn.Location = new System.Drawing.Point(12, 11);
            this.checkBoxErrorOn.Name = "checkBoxErrorOn";
            this.checkBoxErrorOn.Size = new System.Drawing.Size(69, 16);
            this.checkBoxErrorOn.TabIndex = 0;
            this.checkBoxErrorOn.Text = "エラーあり";
            this.checkBoxErrorOn.UseVisualStyleBackColor = true;
            this.checkBoxErrorOn.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxProcess1On
            // 
            this.checkBoxProcess1On.AutoSize = true;
            this.checkBoxProcess1On.Checked = true;
            this.checkBoxProcess1On.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxProcess1On.Location = new System.Drawing.Point(167, 11);
            this.checkBoxProcess1On.Name = "checkBoxProcess1On";
            this.checkBoxProcess1On.Size = new System.Drawing.Size(72, 16);
            this.checkBoxProcess1On.TabIndex = 2;
            this.checkBoxProcess1On.Text = "処理1あり";
            this.checkBoxProcess1On.UseVisualStyleBackColor = true;
            this.checkBoxProcess1On.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxProcess2On
            // 
            this.checkBoxProcess2On.AutoSize = true;
            this.checkBoxProcess2On.Checked = true;
            this.checkBoxProcess2On.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxProcess2On.Location = new System.Drawing.Point(328, 11);
            this.checkBoxProcess2On.Name = "checkBoxProcess2On";
            this.checkBoxProcess2On.Size = new System.Drawing.Size(72, 16);
            this.checkBoxProcess2On.TabIndex = 4;
            this.checkBoxProcess2On.Text = "処理2あり";
            this.checkBoxProcess2On.UseVisualStyleBackColor = true;
            this.checkBoxProcess2On.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxErrorOff
            // 
            this.checkBoxErrorOff.AutoSize = true;
            this.checkBoxErrorOff.Checked = true;
            this.checkBoxErrorOff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxErrorOff.Location = new System.Drawing.Point(89, 11);
            this.checkBoxErrorOff.Name = "checkBoxErrorOff";
            this.checkBoxErrorOff.Size = new System.Drawing.Size(70, 16);
            this.checkBoxErrorOff.TabIndex = 1;
            this.checkBoxErrorOff.Text = "エラーなし";
            this.checkBoxErrorOff.UseVisualStyleBackColor = true;
            this.checkBoxErrorOff.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxProcess1Off
            // 
            this.checkBoxProcess1Off.AutoSize = true;
            this.checkBoxProcess1Off.Checked = true;
            this.checkBoxProcess1Off.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxProcess1Off.Location = new System.Drawing.Point(247, 11);
            this.checkBoxProcess1Off.Name = "checkBoxProcess1Off";
            this.checkBoxProcess1Off.Size = new System.Drawing.Size(73, 16);
            this.checkBoxProcess1Off.TabIndex = 3;
            this.checkBoxProcess1Off.Text = "処理1なし";
            this.checkBoxProcess1Off.UseVisualStyleBackColor = true;
            this.checkBoxProcess1Off.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxProcess2Off
            // 
            this.checkBoxProcess2Off.AutoSize = true;
            this.checkBoxProcess2Off.Checked = true;
            this.checkBoxProcess2Off.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxProcess2Off.Location = new System.Drawing.Point(408, 11);
            this.checkBoxProcess2Off.Name = "checkBoxProcess2Off";
            this.checkBoxProcess2Off.Size = new System.Drawing.Size(73, 16);
            this.checkBoxProcess2Off.TabIndex = 5;
            this.checkBoxProcess2Off.Text = "処理2なし";
            this.checkBoxProcess2Off.UseVisualStyleBackColor = true;
            this.checkBoxProcess2Off.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // MemoErrorListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 438);
            this.Controls.Add(this.checkBoxProcess2Off);
            this.Controls.Add(this.checkBoxProcess2On);
            this.Controls.Add(this.checkBoxProcess1Off);
            this.Controls.Add(this.checkBoxProcess1On);
            this.Controls.Add(this.checkBoxErrorOff);
            this.Controls.Add(this.checkBoxErrorOn);
            this.Controls.Add(this.buttonCSV);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.dataGridView1);
            this.Name = "MemoErrorListForm";
            this.Text = "入力時エラー/メモ一覧";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonCSV;
        private System.Windows.Forms.CheckBox checkBoxErrorOn;
        private System.Windows.Forms.CheckBox checkBoxProcess1On;
        private System.Windows.Forms.CheckBox checkBoxProcess2On;
        private System.Windows.Forms.CheckBox checkBoxErrorOff;
        private System.Windows.Forms.CheckBox checkBoxProcess1Off;
        private System.Windows.Forms.CheckBox checkBoxProcess2Off;
    }
}