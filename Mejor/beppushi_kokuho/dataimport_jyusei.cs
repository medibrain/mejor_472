﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.beppushi_kokuho
{
    /// <summary>
    /// 保険者から提供される　給付データテーブルクラス
    /// </summary>
    class dataimport_jyusei
    {

        #region テーブル構造

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
    
        public int importid { get;set; }=0;                                   //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                                     //メホール上の処理年月 メホール管理用;
        public string shinsaym { get; set; } = string.Empty;                  //審査年月;
        public string shubetsu { get; set; } = string.Empty;                  //療養費種別;
        public string gigi { get; set; } = string.Empty;                      //疑義種別;
        public string insnum { get; set; } = string.Empty;                    //保険者番号;
        public string hihomark { get; set; } = string.Empty;                  //証記号;
        public string hihonum { get; set; } = string.Empty;                   //証番号;
        public string pname { get; set; } = string.Empty;                     //受療者氏名;
        public string pbirthday { get; set; } = string.Empty;                 //受療者生年月日;
        public string pgender { get; set; } = string.Empty;                   //受療者性別;
        public string honke { get; set; } = string.Empty;                     //本家;
        public int ratio { get; set; } = 0;                                   //給付割合;
        public string shinryoym { get; set; } = string.Empty;                 //診療年月;
        public int counteddays { get; set; } = 0;                             //実日数;
        public int total { get; set; } = 0;                                   //決定点数;
        public string clinicnum { get; set; } = string.Empty;                 //機関コード;
        public string clinicname { get; set; } = string.Empty;                //医療機関名;
        public string comnum { get; set; } = string.Empty;                    //レセプト全国共通キー;
        public string shinsaymad { get; set; } = string.Empty;                //審査年月西暦;
        public string ymad { get; set; } = string.Empty;                      //施術年月西暦;
        public DateTime pbirthad { get; set; } = DateTime.MinValue;           //受療者生年月日西暦;
        public string hihonum_nr { get; set; } = string.Empty;                //被保険者証番号半角;



        #endregion 



        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int _cym)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv|すべてのファイル|*.*";
            ofd.FilterIndex = 0;
            ofd.Title = "別府市柔整 提供データ";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;

            var l = CommonTool.CsvImportMultiCode(fileName);

            
            if (l == null) return false;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(l.Count-1);

            try
            {

                using (var tran = DB.Main.CreateTransaction())
                {
                    wf.LogPrint("取込中です");

                    foreach (var item in l)
                    {
                        //ヘッダの場合は飛ばす（保険者番号が数字かどうかで判断）
                        if (!int.TryParse(item[3], out int tmp)) continue;

                  
                        var d = new dataimport_jyusei();

                        d.cym = _cym;                  //メホール処理年月                      
                        
                        d.shinsaym = item[0];         //審査年月;
                        d.shubetsu = item[1];         //療養費種別;
                        d.gigi = item[2];             //疑義種別;
                        d.insnum = item[3];           //保険者番号;
                        d.hihomark = item[4];         //証記号;
                        d.hihonum = item[5];          //証番号;
                        d.pname = item[6];            //受療者氏名;
                        d.pbirthday = item[7];         //生年月日;
                        d.pgender = item[8];           //性別;
                        d.honke = item[9];            //本家;

                        d.ratio = int.TryParse(item[10], out int tmpratio) == false ? 0 : tmpratio;           //給付割合;

                        d.shinryoym = item[11];       //診療年月;

                        d.counteddays = int.TryParse(item[12], out int tmpcd) == false ? 0 : tmpcd;     //実日数;
                        d.total = int.TryParse(item[13].ToString().Replace(",",""), out int tmptotal) == false ? 0 : tmptotal;           //決定点数;

                        d.clinicnum = item[14];       //機関コード;
                        d.clinicname = item[15];      //医療機関名;
                        d.comnum = item[16];          //レセプト全国共通キー;


                        //審査年月西暦;                       
                        if (System.Text.RegularExpressions.Regex.IsMatch(d.shinsaym, "(令和|平成)[0-9]{2}年[0-9]{2}月"))
                        {
                            string strshinsaymY = d.shinsaym.Substring(0, 4);//令和02年07月→令和02
                            string strshinsaymM = d.shinsaym.Substring(5, 2);//令和02年07月→07
                            d.shinsaymad = DateTimeEx.GetAdYear(strshinsaymY).ToString() + strshinsaymM;
                        }
                        else
                        {
                            d.shinsaymad = "999999";
                        }

                        //施術年月西暦;
                        if (System.Text.RegularExpressions.Regex.IsMatch(d.shinryoym, "(令和|平成)[0-9]{2}年[0-9]{2}月"))
                        {
                            string strshinryoY = d.shinryoym.Substring(0, 4);//令和02年07月→令和02
                            string strshinryoM = d.shinryoym.Substring(5, 2);//令和02年07月→07
                            d.ymad = DateTimeEx.GetAdYear(strshinryoY).ToString() + strshinryoM;
                        }
                        else
                        {
                            d.ymad = "999999";
                        }


                        //受療者生年月日西暦(数字部分が前0ない場合があるので{1,2}にしてる);
                        if (System.Text.RegularExpressions.Regex.IsMatch(d.pbirthday, ".+[0-9]{1,2}年[0-9]{1,2}月[0-9]{1,2}日"))
                        {
                            string strPattern = @"(.+)([0-9]{1,2})年([0-9]{1,2})月([0-9]{1,2})日";
                            string strAdYear = System.Text.RegularExpressions.Regex.Replace(d.pbirthday, strPattern, @"$1$2").ToString();
                            int yearad = DateTimeEx.GetAdYear(strAdYear);

                            d.pbirthad =new DateTime( yearad,
                                int.Parse(System.Text.RegularExpressions.Regex.Replace(d.pbirthday,strPattern, @"$3")),
                                int.Parse(System.Text.RegularExpressions.Regex.Replace(d.pbirthday,strPattern, @"$4")));

                        }                        
                        else
                        {
                            d.pbirthad = DateTime.MinValue;
                        }

                        //被保険者証番号半角
                        d.hihonum_nr = Microsoft.VisualBasic.Strings.StrConv(d.hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);


                        wf.InvokeValue++;

                        if (!DB.Main.Insert(d, tran)) return false;
                        
                    }

                    tran.Commit();

                }

           

                System.Windows.Forms.MessageBox.Show("終了しました");
                return true;
            }
          
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(
                    System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;

            }
            finally
            {
                wf.Dispose();
            }
        }


      
        /// <summary>
        /// 給付データ抽出
        /// </summary>
        /// <param name="strwhere">抽出条件(whereいらんsql)</param>
        /// <returns></returns>
        public static List<dataimport_jyusei> select(string strwhere)
        {
            List<dataimport_jyusei> lst = new List<dataimport_jyusei>();
            var l=DB.Main.Select<dataimport_jyusei>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }



    }
}
