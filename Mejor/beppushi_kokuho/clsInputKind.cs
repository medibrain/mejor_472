﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.beppushi_kokuho
{
    //続紙入力種類の定義
    
    public static class clsInputKind
    {
        public const string 続紙 = "--";
        public const string 不要 = "++";
        public const string エラー = "**";
        public const string 長期 = "..";
        public const string 施術同意書 = "901";
        public const string 施術同意書裏 = "902";
        public const string 施術報告書 = "911";
        public const string 状態記入書 = "921";

    }
}
