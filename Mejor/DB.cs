﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using Dapper;
using System.Reflection;

namespace Mejor
{
    public class DB
    {
        //20200131105925 furukawa 各保険者別の接続先を保持        
        /// <summary>
        /// db情報クラス
        /// </summary>
        public class clsDBServer
        {
            /// <summary>
            /// 保険者IDメホール用
            /// </summary>
            public int insurerID = 0;

            /// <summary>
            /// 保険者名
            /// </summary>
            public string strInsurerName = string.Empty;

            /// <summary>
            /// DBサーバIP
            /// </summary>
            public string strDBHostIP = string.Empty;

            /// <summary>
            /// DB名
            /// </summary>
            public string strDBName = string.Empty;

            /// <summary>
            /// DBポート
            /// </summary>
            public int intDBPort = 5432;//postrgresデフォルトポート

            /// <summary>
            /// スキャン画像フォルダ
            /// </summary>
            public string strScanPath = string.Empty;

            //20200910194307 furukawa st ////////////////////////
            //insurerテーブルから接続先を取得する（AWS段階的移行対応
            
            /// <summary>
            /// dbユーザ名
            /// </summary>
            public string strDBUser = string.Empty;
            /// <summary>
            /// dbパスワード
            /// </summary>
            public string strDBPass = string.Empty;

            //20200910194307 furukawa ed ////////////////////////

        }

        public static DB Main { get; private set; }

        string dbAddress = string.Empty;
        string portNo = string.Empty;
        string dbName = string.Empty;
        string userName = string.Empty;
        string password = string.Empty;
        string timeout = string.Empty;
        string longTimeout = string.Empty;
        string connectionStr = string.Empty;
        string longConnectionStr = string.Empty;

        string dbPort = string.Empty;

     
        //20200131110053 furukawa st ////////////////////////
        //これに各保険者の接続先を保持させておく             
        public static Dictionary<string, clsDBServer> dicDB = new Dictionary<string, clsDBServer>();
        //20200131110053 furukawa ed ////////////////////////


        //20200131143907 furukawa 現在の保険者の接続文字列
        public static string strConn=String.Empty;

        //20200131143940 furukawa jyuseiDBの接続文字列
        public static string strConnJyusei = String.Empty;
                                                          

        static DB()
        {
            SetMainDBName("jyusei");
        }

        public Npgsql.NpgsqlConnection getConn()
        {
            return new Npgsql.NpgsqlConnection(connectionStr);
        }


        //20200131143202 furukawa   現在の保険者の接続文字列を返す        
        /// <summary>
        /// 現在の保険者とjyuseiDBの接続文字列を返す
        /// </summary>
        /// <returns></returns>
        public static string getJyuseiDBConnection()
        {             
            return strConn + "\n" + strConnJyusei;
        }
      

        public DB(string database)
        {

            dbName = database;


            //20200131125640 furukawa st ////////////////////////
            //各保険者接続先リストから接続先を探す

            if (dicDB.ContainsKey(database))
            {
                clsDBServer db = dicDB[database];
                dbAddress = db.strDBHostIP;
                portNo = db.intDBPort.ToString();

                //20200910194528 furukawa st ////////////////////////
                //insurerテーブルから接続先を取得する（AWS段階的移行対応
                
                userName = db.strDBUser;
                password = db.strDBPass;

                //20200910194528 furukawa ed ////////////////////////

            }        
            else
            {

                dbAddress = Settings.DataBaseHost;

                //20191128091239 furukawa st ////////////////////////
                //目的でポートを分けたいのでsettings.xmlから取得

                portNo = Settings.dbPort;
                //portNo = "5432";
                //20191128091239 furukawa ed ////////////////////////                


                //20200131144016 furukawa st ////////////////////////
                //jyuseiDBの接続文字列作成                
                strConnJyusei = $"Server={dbAddress};Port={portNo};Database={dbName};";
                //20200131144016 furukawa ed ////////////////////////


                //20200910194734 furukawa st ////////////////////////
                //DB名が無い場合は仕方ないのでSettings.xmlから取得
                
                userName = Settings.JyuseiDBUser;
                password = Settings.JyuseiDBPassword;
                //20200910194734 furukawa ed ////////////////////////

            }

            //20200131125640 furukawa ed ////////////////////////





            //20200313183726 furukawa st ////////////////////////
            //ユーザ、パスワードをSettings.xmlから取得


                //20200910194821 furukawa st ////////////////////////
                //insurerテーブルから接続先を取得する（AWS段階的移行対応

                    //userName = Settings.JyuseiDBUser;
                    //password = Settings.JyuseiDBPassword;
                //20200910194821 furukawa ed ////////////////////////


                                //userName = "postgres";
                                //password = "pass";
            //20200313183726 furukawa ed ////////////////////////






            timeout = Settings.CommandTimeout;
            longTimeout = "3600";




            //20200317162603 furukawa st ////////////////////////
            //接続文字列にSSLを追加



            //20200910194910 furukawa st ////////////////////////
            //社内とAWSでSSL方式が異なるため分岐


                    //20220214133057 furukawa st ////////////////////////
                    //NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
            
                    if (dbAddress.Contains("aws"))
                            {
                                connectionStr =
                                    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
                                    $"Password={password};CommandTimeout={timeout};sslmode=1;convert infinity datetime=true";

                                longConnectionStr =
                                    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
                                    $"Password={password};CommandTimeout={longTimeout};sslmode=1;convert infinity datetime=true";
                            }
                            else
                            {


                                connectionStr =
                                    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
                                    $"Password={password};CommandTimeout={timeout};SSLMode=0;convert infinity datetime=true";

                                longConnectionStr =
                                    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
                                    $"Password={password};CommandTimeout={longTimeout};SSLMode=0;convert infinity datetime=true";
                            }
                    //20220214133057 furukawa ed ////////////////////////

            //connectionStr =
            //    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
            //    $"Password={password};CommandTimeout={timeout};";

            //longConnectionStr =
            //    $"Server={dbAddress};Port={portNo};Database={dbName};User Id={userName};" +
            //    $"Password={password};CommandTimeout={longTimeout};";
            //20200317162603 furukawa ed ////////////////////////

            //20200910194910 furukawa ed ////////////////////////






            //20200131143055 furukawa st ////////////////////////
            //現在の保険者の接続文字列を返す

            strConn = $"Server={dbAddress};Port={portNo};Database={dbName}";
            //20200131143055 furukawa ed ////////////////////////
        }


        //20200131142954 furukawa 各保険者接続先リストの作成
        /// <summary>
        /// 各保険者接続先リスト作成
        /// </summary>
        /// <returns></returns>
        public static bool CreateDBList()
        {
            Command cmd=new Command(DB.Main,
                "select " +
                "insurerid," +
                "insurername," +
                "dbserver," +
                "dbname," +
                "dbport," +
                "imagefolderpath " +

                //20200910195102 furukawa st ////////////////////////
                //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応
                
                ",dbuser," +
                "dbpass "+
                //20200910195102 furukawa ed ////////////////////////


                "from insurer order by insurerid");

            try
            {
                
                List<object[]> lst = cmd.TryExecuteReaderList();
                
                foreach (var db in lst)
                {
                    int col = 0;
                    clsDBServer clsDB = new clsDBServer();
                    clsDB.insurerID=int.Parse(db[col++].ToString());
                    clsDB.strInsurerName= db[col++].ToString();
                    clsDB.strDBHostIP = db[col++].ToString();
                    clsDB.strDBName = db[col++].ToString();
                    clsDB.intDBPort = int.Parse(db[col++].ToString());
                    clsDB.strScanPath = db[col++].ToString();


                    //20200910195248 furukawa st ////////////////////////
                    //Insurerテーブルにdbuser　dbpass追加に伴う修正（AWS段階的移行対応
                    
                    
                    clsDB.strDBUser = db[col++].ToString();
                    clsDB.strDBPass = db[col++].ToString();
                    //20200910195248 furukawa ed ////////////////////////


                    dicDB.Add(clsDB.strDBName, clsDB);
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("各保険者接続先リスト作成失敗" + "\n" + ex.Message);
                return false;
            }
        }



        public static string getDBHost()
        {
            return Settings.DataBaseHost;
        }

        /// <summary>
        /// 設定をロードします
        /// </summary>
        /// <returns></returns>
        public static bool SetMainDBName(string dbname)
        {
            try
            {
                NpgsqlConnection.ClearAllPools();
                Main = new DB(dbname);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string GetMainDBName()
        {
            return Main.dbName;
        }


        /// <summary>
        /// トランザクション
        /// </summary>
        public class Transaction : IDisposable
        {
            internal NpgsqlConnection conn { get; private set; }
            internal NpgsqlTransaction tran { get; private set; }

            internal Transaction(DB db)
            {
                open(db);
                tran = conn.BeginTransaction();
                
            }

            private bool open(DB db)
            {
                conn = new NpgsqlConnection(db.connectionStr);
#if DEBUG
                conn.Open();
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            internal void Commit()
            {
                tran.Commit();
            }

            internal void Rollback()
            {
                tran.Rollback();
            }

            public void Dispose()
            {
                tran.Dispose();
                conn.Dispose();
            }
        }

        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public Transaction CreateTransaction()
        {
            return new Transaction(this);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public Command CreateCmd(string commandText, Transaction tran)
        {
            return new Command(commandText, tran);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public Command CreateCmd(string commandText)
        {
            return new Command(this, commandText);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public Command CreateLongTimeoutCmd(string commandText)
        {
            return new Command(this, commandText, true);
        }

        public class Command : IDisposable
        {
            DB db;
            NpgsqlConnection conn;
            NpgsqlCommand cmd;
            bool longTimeout = false;
            System.Timers.Timer timer;
            CommandCancelForm ccf;
            DateTime startTime;

            private bool open()
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                conn = new NpgsqlConnection(longTimeout ? db.longConnectionStr : db.connectionStr);
#if DEBUG
                conn.Open();
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    //Log.ErrorWrite(ex);
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            public NpgsqlParameterCollection Parameters
            {
                get { return cmd.Parameters; }
                set { value = cmd.Parameters; }
            }

            internal Command(DB db, string commandText)
            {
                this.db = db;
                open();
                cmd = new NpgsqlCommand(commandText, conn);
            }

            internal Command(DB db, string commandText, bool longTimeout)
            {
                this.db = db;
                this.longTimeout = longTimeout;
                startTime = DateTime.Now;
                open();
                cmd = new NpgsqlCommand(commandText, conn);
            }

            internal Command(string commandText, Transaction tran)
            {
                cmd = new NpgsqlCommand(commandText, tran.conn, tran.tran);
            }

            private bool reOpen()
            {
                if (!open()) return false;
                cmd.Connection = conn;
                return true;
            }

            /// <summary>
            /// トランザクションを使用しているかをチェックし、使用中ならプログラムを強制終了します。
            /// </summary>
            private void tranCheck()
            {
                if (cmd.Transaction != null) Environment.Exit(0);
            }

            private void systemExit()
            {
                Environment.Exit(0);
            }

            /// <summary>
            /// このコマンドによって使用されているすべてのリソースを解放します。
            /// </summary>
            public void Dispose()
            {
                if (cmd.Transaction == null && conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                cmd.Dispose();
            }

            /// <summary>
            /// Tryを含んだExcuteReaderを発行し、リストで返します。失敗した場合 null が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public List<object[]> TryExecuteReaderList()
            {
                var lst = new List<object[]>();

                //タイムアウト通知用タイマー
                if (longTimeout)
                {
                    timer = new System.Timers.Timer(20000);
                    timer.Elapsed += t_Elapsed;
                    timer.Start();
                }
#if DEBUG
                try
                {
                    using (var dr = cmd.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (NpgsqlException ex)
                {
                    //ユーザーによるキャンセル 57014
                    if (ex.ErrorCode == 57014) return lst;
                    //if (ex.Code == "57014") return lst;
                    throw ex;
                }
                finally
                {
                    if (timer != null)
                    {
                        timer.Stop();
                        timer.Dispose();
                    }

                    if (ccf != null) ccf.InvokeDispose();
                }

#else
                try
                {
                    try
                    {
                        using (var dr = cmd.ExecuteReader())
                        {
                            var c = dr.FieldCount;
                            while (dr.Read())
                            {
                                var ojs = new object[c];
                                dr.GetValues(ojs);
                                lst.Add(ojs);
                            }
                        }
                        return lst;
                    }
                    catch (NpgsqlException ex)
                    {
                        //ユーザーによるキャンセル 57014
                        if (ex.ErrorCode == 57014) return lst;
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        if (cmd.Transaction != null) throw ex;
                        if (!reOpen()) throw ex;
                    }

                    try
                    {
                        using (var dr = cmd.ExecuteReader())
                        {
                            var c = dr.FieldCount;
                            while (dr.Read())
                            {
                                var ojs = new object[c];
                                dr.GetValues(ojs);
                                lst.Add(ojs);
                            }
                        }
                        return lst;
                    }
                    catch (NpgsqlException ex)
                    {
                        //ユーザーによるキャンセル 57014
                        if (ex.ErrorCode == 57014) return lst;
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                        var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                        ex.Data.Add("MethodName", data);
                        throw ex;
                    }
                }
                finally
                {
                    if (timer != null)
                    {
                        timer.Stop();
                        timer.Dispose();
                    }

                    if (ccf != null) ccf.InvokeDispose();
                }
#endif
            }

            void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                ccf = new CommandCancelForm(startTime);
                var t = (System.Timers.Timer)sender;
                t.Stop();
                t.Dispose();

                var res = ccf.ShowDialog();
                if (res == System.Windows.Forms.DialogResult.Cancel) cmd.Cancel();
            }

            /// <summary>
            /// Tryを含んだExecuteScalarを発行します。失敗した場合 DBNull.Value が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public object TryExecuteScalar()
            {
#if DEBUG
                return cmd.ExecuteScalar();
#else
                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }

            /// <summary>
            /// Tryを含んだExecuteNonQueryを発行します。失敗した場合 false が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public bool TryExecuteNonQuery()
            {
#if DEBUG
                cmd.ExecuteNonQuery();
                return true;
#else
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }

            public static implicit operator NpgsqlCommand(Command v)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Dapperコマンド用オープン済みコネクション
        /// </summary>
        /// <returns></returns>
        private NpgsqlConnection getOpenConn()
        {
            NpgsqlConnection con = null;

            try
            {
                con = new NpgsqlConnection(connectionStr);
                con.Open();
            }
            catch
            {
                try
                {
                    if (con != null) con.Dispose();
                    con = null;
                    con = new NpgsqlConnection(connectionStr);
                    con.Open();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    throw ex;
                }
            }
            return con;
        }

        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public IEnumerable<Type> Query<Type>(string sql, object parameters = null)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    var res = con.Query<Type>(sql, parameters);
                    return res;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        public Type QueryFirstOrDefault<Type>(string sql, object parameters = null)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    var res = con.QueryFirstOrDefault<Type>(sql, parameters);
                    return res;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return default(Type);
            }
        }


        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public IEnumerable<Type> Query<Type>(string sql, object parameters, Transaction tran)
        {
            try
            {
                var res = tran.conn.Query<Type>(sql, parameters, tran.tran);
                return res;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public IEnumerable<dynamic> Query(string sql, object parameters = null, Transaction tran = null)
        {
            try
            {
                var res = tran.conn.Query(sql, parameters, tran.tran);
                return res;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }


        /// <summary>
        /// Dapperによる自動マッピングExcute
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Excute(string sql, object obj = null)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    con.Execute(sql, obj);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Dapperによる自動マッピングExcute
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Excute(string sql, object obj, Transaction tran)
        {
            try
            {
                tran.conn.Execute(sql, obj, tran.tran);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }


        /// <summary>
        /// WHERE句を直接指定して、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where">WHERE句 "WHERE"は不要</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(string where)
        {
            string sql = SqlBuilder.CreateSelectSql<T>(where);
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql);
            }
        }

        /// <summary>
        /// 匿名クラスを利用し、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereClass">条件指定クラス　exsample: new { id = id, name = "test" }</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(object whereClass)
        {
            string sql = SqlBuilder.CreateSelectSql<T>(whereClass);
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql, whereClass);
            }
        }

        /// <summary>
        /// 匿名クラスを利用し、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereClass">条件指定クラス　exsample: new { id = id, name = "test" }</param>
        /// <returns></returns>
        public IEnumerable<T> SelectAll<T>()
        {
            string sql = SqlBuilder.CreateSelectSql<T>();
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql);
            }
        }

        /// <summary>
        /// データをInsertします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert<T>(T obj, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, obj);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

   

        /// <summary>
        /// Serial属性が指定されたプロパティをDynamicで返すInsertを発行します。失敗した場合、Nullが帰ります。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> InsertReturning<T>(T obj, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateInsertReturningSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                return con.Query(sql, obj);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// データ群をINSERTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objs"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Inserts<T>(IEnumerable<T> objs, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, objs);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータをUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Update<T>(T obj, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, obj);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータ群をUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Updates<T>(IEnumerable<T> objs, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, objs);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// データベースのカラムに関連した属性を管理します
        /// </summary>
        public class DbAttribute
        {
            /// <summary>
            /// データベース上のテーブルでキーとなるプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class PrimaryKey : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルでserialが設定されているプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Serial : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルでUPDATE時に更新しないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class UpdateIgnore : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルにないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Ignore : System.Attribute { }

            /// <summary>
            /// クラス名以外のテーブルに関連付けする場合に指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
            public class DifferentTableName : System.Attribute
            {
                public readonly string TableName;
                public DifferentTableName(string tableName) { TableName = tableName; }
            }

            /// <summary>
            /// データベース上のテーブルにないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Index : System.Attribute { }
        }

        /// <summary>
        /// SQL文をクラスから自動生成します
        /// </summary>
        class SqlBuilder
        {
            /// <summary>
            /// クラス情報をリフレクションにより取得し、キャッシュします
            /// </summary>
            class ClassCache
            {
                static Dictionary<Type, ClassCache> classes = new Dictionary<Type, ClassCache>();

                public string TableName { get; private set; }
                List<PropertyInfo> properties = new List<PropertyInfo>();

                public string SelectSql { get; private set; } = string.Empty;

                public string UpdateSql { get; private set; } = string.Empty;

                public string InsertSql { get; private set; } = string.Empty;

                public string InsertRetuningSql { get; private set; } = string.Empty;

                ClassCache(Type t)
                {
                    var ta = Array.Find(System.Attribute.GetCustomAttributes(t), x => x is DbAttribute.DifferentTableName);
                    TableName = ((DbAttribute.DifferentTableName)ta)?.TableName ?? t.Name;

                    void addProperties(Type t2)
                    {
                        if (t2 == typeof(object)) return;
                        addProperties(t2.BaseType);
                        var ps = t2.GetProperties();
                        properties.AddRange(Array.FindAll(ps, p => p.DeclaringType.Name == t2.Name));
                    }
                    addProperties(t);

                    var selects = new List<string>();
                    var inserts = new List<string>();
                    var updates = new List<string>();
                    var keys = new List<string>();
                    var serials = new List<string>();

                    foreach (var item in properties)
                    {
                        //無視をすべてのコマンドから対象外
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Ignore)) continue;

                        //SELECT対象 すべて
                        selects.Add(item.Name);

                        //INSERT対象 serial/Readonly以外
                        if (!Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Serial))
                            inserts.Add(item.Name);

                        //UPDATE対象 Key/Readonly以外
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.PrimaryKey))
                            keys.Add(item.Name);
                        else if (!(Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.UpdateIgnore)))
                            updates.Add(item.Name);

                        //RETURNING対象 serial属性が指定されたもの
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Serial))
                            serials.Add(item.Name);
                    }

                    //各SQL文を作成
                    //Select
                    SelectSql = $"SELECT {string.Join(",", selects)} FROM {TableName}";

                    //Update
                    if (keys.Count > 0) UpdateSql = $"UPDATE {TableName} " +
                        $"SET {string.Join(",", updates.Select(p => $"{p}=@{p}"))} " +
                        $"WHERE {string.Join(" AND ", keys.Select(p => $"{p}=@{p}"))};";

                    //Insert
                    InsertSql = $"INSERT INTO {TableName}({string.Join(",", inserts)})" +
                        $"VALUES(@{string.Join(",@", inserts)})";

                    //InsertRetuning
                    InsertRetuningSql = $"INSERT INTO {TableName}({string.Join(",",　inserts)})" +
                        $"VALUES(@{string.Join(",@", inserts)}) " +
                        $"RETURNING {string.Join(",", serials)}";

                    classes.Add(t, this);
                }

                public static ClassCache GetCashe(Type t)
                {
                    return classes.ContainsKey(t) ?
                        classes[t] : new ClassCache(t);
                }

                public string CreateTableSql<T>()
                {
                    var cols = new List<string>();
                    var keys = new List<string>();

                    foreach (var item in properties)
                    {
                        //無視
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Ignore)) continue;

                        //各カラム
                        string colType;
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Serial)) colType = "serial NOT NULL";
                        else if (item.PropertyType == typeof(int)) colType = "INTEGER NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(long)) colType = "BIGINT NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(string)) colType = "TEXT NOT NULL DEFAULT ''";
                        else if (item.PropertyType == typeof(DateTime)) colType = "DATE NOT NULL DEFAULT '0001-01-01'";
                        else if (item.PropertyType == typeof(float)) colType = "REAL NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(double)) colType = "REAL NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(decimal)) colType = "NUMERIC NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(Boolean)) colType = "BOOLEAN NOT NULL DEFAULT FALSE";
                        else if (item.PropertyType.IsEnum) colType = "INTEGER NOT NULL DEFAULT 0";
                        else throw new Exception("自動テーブル作成に対応していないデータ型です");
                        cols.Add(item.Name + " " + colType);

                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.PrimaryKey))
                            keys.Add(item.Name);
                    }

                    if (keys.Count == 0)
                    {
                        throw new Exception("PrimaryKey属性が指定されていません");
                    }

                    return "CREATE TABLE " + TableName + "(\r\n" + string.Join(",\r\n", cols) +
                        " ,\r\nPRIMARY KEY (" + string.Join(", ", keys) + "));";
                }
            }

            public static string CreateSelectSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"{c.SelectSql};";
            }

            public static string CreateSelectSql<T>(string where)
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"{c.SelectSql} WHERE {where};";
            }

            public static string CreateSelectSql<T>(object obj)
            {
                var c = ClassCache.GetCashe(typeof(T));

                var t = obj.GetType();
                if (!t.IsClass) throw new Exception("指定されたオブジェクトはクラスではありません");
                var ps = t.GetProperties();

                return $"{c.SelectSql} WHERE {string.Join(" AND ", ps.Select(p => $"{p.Name}=@{p.Name}"))};";
            }

            public static string CreateInsertSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"{c.InsertSql};";
            }

            /// <summary>
            /// Serial属性が指定されたカラムを返すInsert文を作成します
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <returns></returns>
            public static string CreateInsertReturningSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"{c.InsertRetuningSql};";
            }

            public static string CreateUpdateSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                if (c.UpdateSql == string.Empty) throw new Exception("キー属性が指定されていません");
                return $"{c.UpdateSql};";
            }

            public static string CreateTableSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                return c.CreateTableSql<T>();
            }
        }

        public bool CreateTable<T>(Transaction tran)
        {
            var sql = SqlBuilder.CreateTableSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }
    }
}