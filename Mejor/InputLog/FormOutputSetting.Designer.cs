﻿namespace Mejor.InputLogOutput
{
    partial class OutputSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpSt = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEd = new System.Windows.Forms.DateTimePicker();
            this.listBoxIns = new System.Windows.Forms.ListBox();
            this.buttonExcelOutput = new System.Windows.Forms.Button();
            this.dgvuser = new System.Windows.Forms.DataGridView();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvuser)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpSt
            // 
            this.dtpSt.Location = new System.Drawing.Point(66, 33);
            this.dtpSt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSt.Name = "dtpSt";
            this.dtpSt.Size = new System.Drawing.Size(157, 24);
            this.dtpSt.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpEd);
            this.groupBox1.Controls.Add(this.dtpSt);
            this.groupBox1.Location = new System.Drawing.Point(590, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(249, 133);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力範囲";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "終了";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "開始";
            // 
            // dtpEd
            // 
            this.dtpEd.Location = new System.Drawing.Point(66, 77);
            this.dtpEd.Margin = new System.Windows.Forms.Padding(4);
            this.dtpEd.Name = "dtpEd";
            this.dtpEd.Size = new System.Drawing.Size(157, 24);
            this.dtpEd.TabIndex = 0;
            // 
            // listBoxIns
            // 
            this.listBoxIns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxIns.FormattingEnabled = true;
            this.listBoxIns.ItemHeight = 18;
            this.listBoxIns.Location = new System.Drawing.Point(13, 36);
            this.listBoxIns.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxIns.Name = "listBoxIns";
            this.listBoxIns.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxIns.Size = new System.Drawing.Size(200, 454);
            this.listBoxIns.TabIndex = 2;
            this.listBoxIns.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxIns_MouseClick);
            this.listBoxIns.SelectedIndexChanged += new System.EventHandler(this.listBoxIns_SelectedIndexChanged);
            // 
            // buttonExcelOutput
            // 
            this.buttonExcelOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExcelOutput.Location = new System.Drawing.Point(626, 155);
            this.buttonExcelOutput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExcelOutput.Name = "buttonExcelOutput";
            this.buttonExcelOutput.Size = new System.Drawing.Size(171, 68);
            this.buttonExcelOutput.TabIndex = 3;
            this.buttonExcelOutput.Text = "Excel出力";
            this.buttonExcelOutput.UseVisualStyleBackColor = true;
            this.buttonExcelOutput.Click += new System.EventHandler(this.buttonExcelOutput_Click);
            // 
            // dgvuser
            // 
            this.dgvuser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvuser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvuser.Location = new System.Drawing.Point(220, 36);
            this.dgvuser.Name = "dgvuser";
            this.dgvuser.RowTemplate.Height = 21;
            this.dgvuser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvuser.Size = new System.Drawing.Size(354, 454);
            this.dgvuser.TabIndex = 4;
            this.dgvuser.SelectionChanged += new System.EventHandler(this.dgvuser_SelectionChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClear.Location = new System.Drawing.Point(69, 525);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(88, 38);
            this.buttonClear.TabIndex = 5;
            this.buttonClear.Text = "選択クリア";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ctrl+クリックで複数選択";
            // 
            // OutputSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 575);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.dgvuser);
            this.Controls.Add(this.buttonExcelOutput);
            this.Controls.Add(this.listBoxIns);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OutputSetting";
            this.Text = "出力設定";
            this.Load += new System.EventHandler(this.OutputSetting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvuser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpSt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpEd;
        private System.Windows.Forms.ListBox listBoxIns;
        private System.Windows.Forms.Button buttonExcelOutput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvuser;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label label3;
    }
}