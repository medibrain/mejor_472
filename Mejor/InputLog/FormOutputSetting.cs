﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.InputLogOutput
{
    public partial class OutputSetting : Form
    {
        string strIns = string.Empty;//選択した保険者をSQL条件文字列として格納
        string strUsr = string.Empty;//選択したユーザをSQL条件文字列として格納

        public OutputSetting()
        {
            InitializeComponent();
        }

        private void OutputSetting_Load(object sender, EventArgs e)
        {
            getInsurerData();
            getUserName();
            listBoxIns.ClearSelected();
            dgvuser.ClearSelection();
            strUsr = string.Empty;
            strIns = string.Empty;
            dtpSt.Value = DateTime.Now.AddMonths(-6);//開始は半年前ぐらいで
        }

        private void getUserName()
        {
            List<User> lst = new List<User>();
            lst = Mejor.User.GetList();            
            lst.Sort((x, y) => x.UserID.CompareTo(y.UserID));


            DataTable dt = new DataTable();
            try
            {
                //20210127135148 furukawa st ////////////////////////
                //ユーザIDを数値扱いにしないとソートしづらい
                
                dt.Columns.Add("userid",typeof(Int32));
                //      dt.Columns.Add("userid");
                //20210127135148 furukawa ed ////////////////////////


                dt.Columns.Add("username");

                for (int r = 0; r < lst.Count; r++)
                {
                    if (lst[r].Enable == false) continue;
                    DataRow dr = dt.NewRow();
                    dr["userid"] = lst[r].UserID.ToString();
                    dr["username"] = lst[r].Name;
                    dt.Rows.Add(dr);
                }

                dgvuser.DataSource = dt;
                dgvuser.AllowUserToAddRows = false;
                dgvuser.MultiSelect = true;
                dgvuser.RowHeadersVisible = false;
                dgvuser.Columns["userid"].Width = 40;
                dgvuser.Columns["username"].Width = 140;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void getInsurerData()
        {
            //保険者一覧を取得            
            Insurer.GetInsurers(true);
            listBoxIns.DisplayMember = nameof(Insurer.InsurerName);
            showInsurers();
        }

        private void showInsurers()
        {
            listBoxIns.SelectedIndex = -1;
            var l = Insurer.GetInsurers();
            l.Sort((x, y) => x.InsurerType == y.InsurerType ?
            x.ViewIndex.CompareTo(y.ViewIndex) : x.InsurerType.CompareTo(y.InsurerType));

            listBoxIns.DataSource = l;
        }


        /// <summary>
        /// inputlog から inputlog_output　にコピー
        /// </summary>
        /// <returns></returns>
        private bool getdata(WaitForm wf)
        {

            if (strIns == string.Empty)
            {
                MessageBox.Show("保険者が指定されていません"
                    ,Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return false;
            }
            if(strUsr== string.Empty)
            {
                MessageBox.Show("ユーザが指定されていません"
                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            string strsql =
                $"select distinct " +
                $"l.aid," +
                $"l.insurerid," +
                $"i.insurername," +

                $"case l.apptype " +
                $"when {(int)APP_TYPE.柔整} then '柔整' " +
                $"when {(int)APP_TYPE.鍼灸} then '鍼灸' " +
                $"when {(int)APP_TYPE.あんま} then 'あんま' " +
                $"end apptype ," +

                $"l.uid," +
                $"u.name as username," +
                $"l.inputtype," +
                $"l.inputdt," +
                $"l.timespan, " +
                $"l.misscount " +

                $"from inputlog l " +
                $"left join users u on " +
                $"l.uid = u.userid " +
                $"left join insurer i on " +
                $"i.insurerid = l.insurerid ";

            strsql +=
                $"where l.insurerid in ({strIns}) " +
                $"and l.uid in ({strUsr}) ";


            if (dtpSt.Value != DateTime.MinValue)
                strsql += $"and l.inputdt between '{dtpSt.Value.ToString("yyyy-MM-dd 00:00:00")}' and '{dtpEd.Value.ToString("yyyy-MM-dd 23:59:59")}' ";


            strsql +=
                $"group by " +
                $"l.aid," +
                $"l.insurerid," +
                $"i.insurername," +
                $"l.apptype, " +
                $"l.uid," +
                $"u.name ," +
                $"l.inputtype," +
                $"l.inputdt," +
                $"l.timespan, " +
                $"l.misscount ";

            wf.LogPrint("一時テーブル削除");

            DB.SetMainDBName("jyusei");
            DB.Transaction tran = DB.Main.CreateTransaction();

            DB.Command cmd = new DB.Command("truncate table inputlog_output", tran);
            cmd.TryExecuteNonQuery();

            cmd = new DB.Command(strsql, tran);

            try
            {
                var l = cmd.TryExecuteReaderList();
                wf.LogPrint("一時テーブル作成");
                wf.InvokeValue = 0;
                wf.SetMax(l.Count);

                int c = 0;
                for (int r = 0; r < l.Count; r++)
                {
                    c = 0;
                    inputlog_output ol = new inputlog_output();
                    ol.aid = int.Parse(l[r][c++].ToString());
                    ol.insurerid = int.Parse(l[r][c++].ToString());
                    ol.insurername = l[r][c++].ToString();
                    ol.apptype = l[r][c++].ToString();
                    ol.uid = int.Parse(l[r][c++].ToString());
                    ol.username = l[r][c++].ToString();
                    ol.inputtype = int.Parse(l[r][c++].ToString());
                    ol.inputdt = DateTime.Parse(l[r][c++].ToString());

                    TimeSpan.TryParse(l[r][c++].ToString(),out TimeSpan ts);
                    ol.timespan = ts == null ? TimeSpan.MinValue : ts; 

                    ol.misscount = int.Parse(l[r][c++].ToString());


                    DB.Main.Insert<inputlog_output>(ol, tran);
                    wf.InvokeValue++;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }

        /// <summary>
        /// excel出力
        /// </summary>
        /// <returns></returns>
        private bool CreateOutputFile(WaitForm wf)
        {
            System.Windows.Forms.SaveFileDialog dlg = new SaveFileDialog();
            string strFileName = $"inputlog_output{DateTime.Now.ToString("yyyyMMdd_hhmmss")}.xlsx";
            dlg.FileName = strFileName;
            dlg.Filter = "excel|*.xlsx";
            dlg.FilterIndex = 0;

            if (dlg.ShowDialog() != DialogResult.OK) return false;

            System.IO.FileStream fs = new System.IO.FileStream(dlg.FileName, System.IO.FileMode.CreateNew);
            NPOI.XSSF.UserModel.XSSFWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet();

            wf.LogPrint("excel出力中");

            DB.Command cmd = new DB.Command(DB.Main,"select * from inputlog_output order by uid,insurerid,aid");
            

            try
            {
                var l=cmd.TryExecuteReaderList();
                wf.InvokeValue = 0;
                wf.SetMax(l.Count);

                for (int r = 0; r < l.Count; r++)
                {
                    NPOI.SS.UserModel.IRow row = ws.CreateRow(r);
                    

                    for (int c = 0; c < l[r].Length; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(c);
                        if (r == 0)
                        {
                            //ヘッダ
                            if (c == 0) cell.SetCellValue("aid"); 
                            if (c == 1) cell.SetCellValue("保険者id");
                            if (c == 2) cell.SetCellValue("保険者名");
                            if (c == 3) cell.SetCellValue("申請書種類"); 
                            if (c == 4) cell.SetCellValue("ユーザID"); 
                            if (c == 5) cell.SetCellValue("ユーザ"); 
                            if (c == 6) cell.SetCellValue("入力回"); 
                            if (c == 7) cell.SetCellValue("登録時刻");

                            //20210127135400 furukawa st ////////////////////////
                            //単位付けないと何の値か分からない
                            
                            if (c == 8) cell.SetCellValue("入力時間(秒)");
                            //if (c == 8) cell.SetCellValue("入力時間");
                            //20210127135400 furukawa ed ////////////////////////


                            if (c == 9) cell.SetCellValue("ミス回数");
                        }
                        else
                        {
                            //20210127124521 furukawa st ////////////////////////
                            //入力時間を秒にしないとExcelで集計しづらい
                            
                            if (c == 8)
                            {

                                //見づらいので秒に直す
                                int sec = 0;
                                sec = DateTime.Parse(l[r][c].ToString()).Hour * 360 +
                                    DateTime.Parse(l[r][c].ToString()).Minute * 60 +
                                    DateTime.Parse(l[r][c].ToString()).Second;
                                cell.SetCellValue(sec);

                            }
                            else
                            {
                                cell.SetCellValue(l[r][c].ToString());
                            }

                            //      cell.SetCellValue(l[r][c].ToString());
                            //20210127124521 furukawa ed ////////////////////////


                            if (r==2)ws.AutoSizeColumn(c);
                        }
                        row.Cells.Add(cell);
                    }
                    wf.InvokeValue++;
                }
                
                
                wb.Write(fs);
               
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;

            }
            finally
            {
                wb.Close();
                fs.Close();                
            }

        }

        private void buttonExcelOutput_Click(object sender, EventArgs e)
        {
            
            using (WaitForm wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                if (!getdata(wf)) return;

                if (!CreateOutputFile(wf)) return;
                
            }

            MessageBox.Show("正常終了しました");
        }



        private void dgvuser_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvuser.Rows.Count == 0) return;
            if (dgvuser.SelectedRows.Count == 0) return;
            strUsr = string.Empty;
            foreach (DataGridViewRow r in dgvuser.SelectedRows)
            {
                strUsr += r.Cells["userid"].Value.ToString() + ',';
            }

            strUsr = strUsr.Substring(0, strUsr.Length - 1);

        }

        /// <summary>
        /// クリアボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            listBoxIns.ClearSelected();
            dgvuser.ClearSelection();
            strUsr = string.Empty;
            strIns = string.Empty;
        }

        /// <summary>
        /// 保険者リストクリック時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxIns_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBoxIns.SelectedItems.Count == 0) return;
            strIns = string.Empty;
            foreach (Insurer s in listBoxIns.SelectedItems) strIns += s.InsurerID.ToString() + ',';

            strIns = strIns.Substring(0, strIns.Length - 1);
        }

        private void listBoxIns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxIns.SelectedItems.Count == 0) return;
            strIns = string.Empty;
            foreach (Insurer s in listBoxIns.SelectedItems) strIns += s.InsurerID.ToString() + ',';

            strIns = strIns.Substring(0, strIns.Length - 1);
        }
    }


    /// <summary>
    /// 入力ログ出力用テーブル
    /// </summary>
    public partial class inputlog_output
    {
        public int aid { get;set; }=0;                                    //aid;
        public int insurerid { get; set; } = 0;                           //メホール管理保険者ID;
        public string insurername { get; set; } = string.Empty;           //保険者名;
        public string apptype { get; set; } = string.Empty;               //申請書種類;
        public int uid { get; set; } = 0;                                 //ユーザID;
        public string username { get; set; } = string.Empty;              //ユーザ名;
        public int inputtype { get; set; } = 0;                           //初回入力、ベリファイ入力;
        public DateTime inputdt { get;set; }=DateTime.MinValue;                  //登録時刻;
        public TimeSpan timespan { get;set; }=TimeSpan.MinValue;                 //入力時間;
        public int misscount { get; set; } = 0;                           //ミスカウンタ;
        
    }
}