﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    //20210603143431 furukawa 申請書に紐付くデータのIDを一元管理する
    /// <summary>
    /// 申請書に紐付くデータのIDを一元管理する
    /// </summary>
    public class Application_AUX
    {
        //20210709091425 furukawa st ////////////////////////
        //テーブル変更
        
        [DB.DbAttribute.PrimaryKey]
        public int aid { get;set; }=0;                                                   //aid;
        public int cym { get; set; } = 0;                                                //メホール請求年月;
        public int scanid { get; set; } = 0;                                             //スキャンID;
        public int groupid { get; set; } = 0;                                            //グループID;
        public string aimagefile { get; set; } = string.Empty;                           //変換後画像ファイル名;
        public string origfile { get; set; } = string.Empty;                             //元画像ファイル名;
        public int multitiff_pageno { get; set; } = 0;                                   //マルチtiff画像ページ番号;
        public int aapptype { get; set; } = 0;                                           //AIDのaapptype;
        public int parentaid { get; set; } = 0;                                          //続紙の親申請書AID;
        public int batchaid { get; set; } = 0;                                           //所属バッチAID;
        public string matchingID01 { get; set; } = string.Empty;                         //マッチングしたデータのID1;
        public string matchingID02 { get; set; } = string.Empty;                         //マッチングしたデータのID2;
        public string matchingID03 { get; set; } = string.Empty;                         //マッチングしたデータのID3;
        public string matchingID04 { get; set; } = string.Empty;                         //マッチングしたデータのID4;
        public string matchingID05 { get; set; } = string.Empty;                         //マッチングしたデータのID5;
        public DateTime matchingID01Date { get; set; } = DateTime.MinValue;              //マッチング時刻1;
        public DateTime matchingID02Date { get; set; } = DateTime.MinValue;              //マッチング時刻2;
        public DateTime matchingID03Date { get; set; } = DateTime.MinValue;              //マッチング時刻3;
        public DateTime matchingID04Date { get; set; } = DateTime.MinValue;              //マッチング時刻4;
        public DateTime matchingID05Date { get; set; } = DateTime.MinValue;              //マッチング時刻5;


        #region old
        //public int aid { get;set; }=0;                                        //aid;
        //public int cym { get; set; } = 0;                                     //メホール請求年月;
        //public int scanid { get; set; } = 0;                                  //スキャンID;
        //public int groupid { get; set; } = 0;                                 //グループID;
        //public string aimagefile { get; set; } = string.Empty;                //変換後画像ファイル名;
        //public string origfile { get; set; } = string.Empty;                  //元画像ファイル名;
        //public int multitiff_pageno { get; set; } = 0;                        //マルチtiff画像ページ番号;        
        //public string matchingID01 { get; set; } = string.Empty;              //マッチングしたデータのID1;
        //public string matchingID02 { get; set; } = string.Empty;              //マッチングしたデータのID2;
        //public string matchingID03 { get; set; } = string.Empty;              //マッチングしたデータのID3;
        //public string matchingID04 { get; set; } = string.Empty;              //マッチングしたデータのID4;
        //public string matchingID05 { get; set; } = string.Empty;              //マッチングしたデータのID5;
        //public DateTime matchingID01Date { get; set; } = DateTime.MinValue;   //マッチング時刻1;
        //public DateTime matchingID02Date { get; set; } = DateTime.MinValue;   //マッチング時刻2;
        //public DateTime matchingID03Date { get; set; } = DateTime.MinValue;   //マッチング時刻3;
        //public DateTime matchingID04Date { get; set; } = DateTime.MinValue;   //マッチング時刻4;
        //public DateTime matchingID05Date { get; set; } = DateTime.MinValue;   //マッチング時刻5;
        //public int parentaid { get; set; } = 0;                                 //続紙の申請書aid

        #endregion


        //20210709091425 furukawa ed ////////////////////////

        /// <summary>
        /// whereでリスト取得
        /// </summary>
        /// <param name="strWhere">where句(where抜き)</param>
        /// <returns>条件に合うAUX List</returns>
        public static List<Application_AUX> Select(string strWhere)
        {
            
            List<Application_AUX> lst=DB.Main.Select<Application_AUX>(strWhere).ToList<Application_AUX>();
            return lst;

        }

        //20210716101250 furukawa st ////////////////////////
        //該当AIDのAUX取得
        
        /// <summary>
        /// 該当AIDのAUX取得
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static Application_AUX Select(int aid)
        {
            List<Application_AUX> lstaux = Select($" aid={aid}");                        
            return lstaux[0];
        }
        //20210716101250 furukawa ed ////////////////////////



        //20211007142228 furukawa st ////////////////////////
        //ScanIDでAUXを削除
        
        public static void DeleteByScanID(int scanid)
        {
            DB.Command cmd = new DB.Command(DB.Main,$"delete from application_aux where scanid={scanid}");
            cmd.TryExecuteNonQuery();
            cmd.Dispose();
        }
        //20211007142228 furukawa ed ////////////////////////


        /// <summary>
        /// ParentAIDからApplicationレコードのリストを取得(aid昇順)
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static List<App> GetAppsFromParentAID(int aid)
        {
            List<Application_AUX> lstaux = Select($" parentaid={aid}");
            List<App> lstApp = new List<App>();
            foreach(Application_AUX aux in lstaux)
            {
                lstApp.Add(App.GetApp(aux.aid));
            }
            lstApp.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            return lstApp;
        }


        /// <summary>
        /// バッチ一覧取得
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static List<App> GetBatchApp(int aid)
        {
            List<Application_AUX> lstaux = Select($" aid={aid}");
            List<App> lstBatchApp = new List<App>();
            foreach (Application_AUX aux in lstaux)
            {
                lstBatchApp.Add(App.GetApp(aux.batchaid));
            }
            return lstBatchApp;

        }

        /// <summary>
        /// 初期登録
        /// </summary>
        /// <param name="_aid"></param>
        /// <param name="_cym"></param>
        /// <param name="_scanid"></param>
        /// <param name="_groupid"></param>
        /// <returns>成功=true,失敗=false</returns>
        public static bool Insert(int _aid,int _cym,int _scanid,int _groupid,string _aimagefile,string _origfile,int _multitiff_pageno)
        {
            Application_AUX aux = new Application_AUX();
            aux.aid = _aid;
            aux.cym = _cym;
            aux.scanid = _scanid;
            aux.groupid = _groupid;
            aux.aimagefile = _aimagefile;
            aux.origfile = _origfile;
            aux.multitiff_pageno = _multitiff_pageno;

            if (!DB.Main.Insert<Application_AUX>(aux)) return false;

            return true;

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="_aid"></param>
        /// <param name="apptype">apptype</param>//20211011130508 furukawa apptype追加
        /// <param name="tran">transaction</param>//20210723115437 furukawa トランザクション追加
        /// <param name="id01">提供データのID</param>
        /// <param name="id02">提供データのID</param>
        /// <param name="id03">提供データのID</param>
        /// <param name="id04">提供データのID</param>
        /// <param name="id05">提供データのID</param>
        /// <returns>成功=true,失敗=false</returns>
        public static bool Update(int _aid, APP_TYPE apptype,DB.Transaction tran = null,string id01="",string id02="",string id03="",string id04="",string id05="")
        {
            Application_AUX aux = new Application_AUX();
            List<Application_AUX> lst=Application_AUX.Select($"aid={_aid}");
            if(lst.Count!=1)
            {
                System.Windows.Forms.MessageBox.Show("AIDが重複しています。管理者に確認してください");
                return false;
            }

            aux = lst[0];

            aux.aapptype = (int)apptype;

            if (id01 != string.Empty)
            {
                aux.matchingID01 = id01;
                aux.matchingID01Date = DateTime.Now;
            }
            if (id02 != string.Empty)
            {
                aux.matchingID02 = id02;
                aux.matchingID02Date = DateTime.Now;
            }
            if (id03 != string.Empty)
            {
                aux.matchingID03 = id03;
                aux.matchingID03Date = DateTime.Now;
            }
            if (id04 != string.Empty)
            {
                aux.matchingID04 = id04;
                aux.matchingID04Date = DateTime.Now;
            }
            if (id05 != string.Empty)
            {
                aux.matchingID05 = id05;
                aux.matchingID05Date = DateTime.Now;

            }

            if (!DB.Main.Update<Application_AUX>(aux,tran)) return false;

            return true;

        }
    }
}
