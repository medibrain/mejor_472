﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;


namespace Mejor
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if DEBUG

#else
            // ThreadExceptionイベント・ハンドラを登録する
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            // UnhandledExceptionイベント・ハンドラを登録する
            Thread.GetDomain().UnhandledException += new UnhandledExceptionEventHandler(Application_UnhandledException);
#endif


            Settings.getServerSettings();
            Settings.VersionFolder=Settings.dsSettings.Tables[Settings.CurrentServer].Rows[0]["VersionFolder"].ToString();

            

            //20200313181605 furukawa st ////////////////////////
            //DBサーバを判定し、柔整サーバかテストサーバかのIPを選択する




            //Settings.VersionFolder = DB.getDBHost() == Settings.GetProductionIP ?
            //        $@"\\{Settings.GetInternalDBIP}\public\JuseiSystem\MejorVersions" :
            //        $@"\\{Settings.GetTestDBIP}\public\JuseiSystem\MejorVersions";

                    //20200312103647 furukawa st ////////////////////////
                    //テストサーバのIP変更

                    //Settings.VersionFolder = DB.getDBHost() == "192.168.200.100" ? 
                    //        @"\\192.168.200.100\public\JuseiSystem\MejorVersions" :
                    //        @"\\192.168.110.4\public\JuseiSystem\MejorVersions";

                                //20191003110013 furukawa st ////////////////////////
                                //試験サーバの場合、バージョンフォルダも試験サーバに移す

                                //Settings.VersionFolder = DB.getDBHost() == "192.168.200.100" ?
                                //        @"\\192.168.200.100\public\JuseiSystem\MejorVersions" :
                                //        @"\\192.168.206.97\public\JuseiSystem\MejorVersions";

                                                    // if (Settings.VersionFolder == string.Empty)
                                                    //Settings.VersionFolder = @"\\192.168.200.100\public\JuseiSystem\MejorVersions";
                                //20191003110013 furukawa ed ////////////////////////
                    //20200312103647 furukawa ed ////////////////////////



            //20200313181605 furukawa ed ////////////////////////



            Application.Run(new MainForm());
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        // （Windowsアプリケーション用）
        public static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            try
            {
                Log.ErrorWrite(e.Exception);
            }
            finally
            {
                Environment.Exit(0);
            }
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        // （主にコンソール・アプリケーション用）
        public static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            try
            {
                if (ex != null) Log.ErrorWrite(ex);
            }
            finally
            {
                Environment.Exit(0);
            }
        }
    }
}
