﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OsakaGakko
{
    public class SubmitData
    {
        public class BatchInfo
        {
            public List<SubmitData> apps = new List<SubmitData>();
            public string BatchNo { get; set; }         //バッチ番号
            public string AccountNum { get; set; }      //赤バッチ・振込先口座番号
            public string KyogikaiNum { get; set; }     //赤バッジ・協議会番号
            public string Pref { get; set; }            //県番号
            public int Total { get; set; }              //総括表・請求金額合計
        }

        App app;
        BatchInfo bi;
        string batch => bi.BatchNo;
        string numbering => app.Numbering;
        string sinryoYM => DateTimeEx.GetEraNumber(DateTime.Today).ToString() +
            app.MediYear.ToString("00") + app.MediMonth.ToString("00");
        string kubun => "0";                        //仕様書で0固定
        string honke => app.Family.ToString();
        string pref => bi.Pref.ToString();
        string insurerNo => Insurer.CurrrentInsurer.InsNumber;
        string kumiaiNo => app.HihoNum;
        string sex => app.Sex == 1 ? "1" : "2";    //stringに変換する意味合いもあり
        string birth => DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday).ToString().Remove(5);
        string days => app.CountedDays.ToString();
        string totalCost => app.Total.ToString();
        string seikyu => app.Charge.ToString();
        string yuyo => "0";                         //仕様書で0固定
        string kigoubango => app.ClinicNum;         //記号番号は？
        string redKyogikaiNo => bi.KyogikaiNum;
        string redFurikomiNo => bi.AccountNum;
        string soFurikomiNo => bi.AccountNum;
        string soTotal => bi.Total.ToString();

        public SubmitData(App app, BatchInfo bi)
        {
            this.app = app;
            this.bi = bi;
        }

        public string CreateRes(string writeYM)
        {
            var rex = new List<string>();
            rex.Add(writeYM);
            rex.Add(batch);
            rex.Add(numbering);
            rex.Add(sinryoYM);
            rex.Add(kubun);
            rex.Add(honke);
            rex.Add(pref);
            rex.Add(insurerNo);
            rex.Add(kumiaiNo);
            rex.Add(sex);
            rex.Add(birth);
            rex.Add(days);
            rex.Add(totalCost);
            rex.Add(seikyu);
            rex.Add(yuyo);
            rex.Add(redKyogikaiNo);
            rex.Add(redFurikomiNo);
            rex.Add(soFurikomiNo);
            rex.Add(soTotal);

            return string.Join(",", rex);
        }

        public string CreateRex(string writeYM)
        {
            var res = new List<string>();
            res.Add(writeYM);
            res.Add(batch);
            res.Add(numbering);
            res.Add(sinryoYM);
            res.Add(kubun);
            res.Add(honke);
            res.Add(pref);
            res.Add(insurerNo);
            res.Add(kumiaiNo);
            res.Add(sex);
            res.Add(birth);
            res.Add(days);
            res.Add(totalCost);
            res.Add(seikyu);
            res.Add(yuyo);
            res.Add(redKyogikaiNo);
            res.Add(redFurikomiNo);

            return string.Join(",", res);
        }

        public static bool Export(int cym, string folderPath, bool inBatxhFolder = true)
        {
            string batch = string.Empty, kyogikai = string.Empty,
                account = string.Empty, pref = string.Empty;
            int total = 0;

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                wf.LogPrint("申請書の取得中です");

                var l = App.GetApps(cym);
                var dic = new Dictionary<string, BatchInfo>();
                var lastAid = 0;
                var withImages = new Dictionary<int, List<string>>();

                wf.LogPrint("バッチごとに整理しています");

                //バッチごと整理
                foreach (var item in l)
                {
                    if (item.AppType == APP_TYPE.バッチ)
                    {
                        batch = item.Numbering;
                        kyogikai = string.Empty;
                        pref = string.Empty;
                        total = 0;
                        continue;
                    }

                    if (item.AppType == APP_TYPE.バッチ2)
                    {
                        kyogikai = item.Numbering;
                        account = item.AccountNumber;
                        pref = item.HihoPref.ToString("00");
                        continue;
                    }

                    if (item.AppType == APP_TYPE.総括票)
                    {
                        total = item.Total;
                        continue;
                    }

                    if (item.AppType == APP_TYPE.続紙)
                    {
                        if (!withImages.ContainsKey(lastAid))
                        {
                            withImages.Add(lastAid, new List<string>());
                        }
                        withImages[lastAid].Add(item.GetImageFullPath());
                        continue;
                    }

                    if (item.YM < 0) continue;

                    if (kyogikai == string.Empty)
                        throw new Exception("協議会番号(赤バッチ)が見つかりません");

                    if (total == 0)
                        new Exception("総括票が見つかりません");

                    var key = batch + "-" + kyogikai;
                    if (!dic.ContainsKey(key))
                    {
                        var bi = new BatchInfo();
                        bi.BatchNo = batch;
                        bi.KyogikaiNum = kyogikai;
                        bi.AccountNum = account;
                        bi.Pref = pref;
                        bi.Total = total;
                        dic.Add(key, bi);
                    }

                    var b = dic[key];
                    var s = new SubmitData(item, b);
                    dic[key].apps.Add(s);
                    lastAid = item.Aid;
                }

                //並び替え
                var bs = dic.Values.ToList();
                bs.Sort((x, y) => x.BatchNo != y.BatchNo ?
                        x.BatchNo.CompareTo(y.BatchNo) :
                        x.KyogikaiNum.CompareTo(y.KyogikaiNum));

                wf.LogPrint("データを出力しています");
                //出力
                var dt = new DateTime(cym / 100, cym % 100, 1);
                var e = DateTimeEx.GetEraNumberYear(dt);
                var jy = DateTimeEx.GetJpYear(dt);
                string writeYM = e.ToString() + (cym % 100).ToString("00");
                System.IO.Directory.CreateDirectory(folderPath);

                var fileREX = folderPath + "\\REX" + DateTime.Today.ToString("yyMMdd") + ".csv";
                var fileRES = folderPath + "\\RES" + DateTime.Today.ToString("yyMMdd") + ".csv";
                var fc = new TiffUtility.FastCopy();

                wf.SetMax(bs.Sum(b => b.apps.Count));
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                var sds = new List<SubmitData>();
                foreach (var item in bs) sds.AddRange(item.apps);
                sds.Sort((x, y) => x.batch == y.batch ?
                    x.numbering.CompareTo(y.numbering) : x.batch.CompareTo(y.batch));

                using (var swRex = new System.IO.StreamWriter(fileREX, true, Encoding.GetEncoding("Shift_JIS")))
                using (var swRes = new System.IO.StreamWriter(fileRES, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (var sd in sds)
                    {
                        var imgDir = folderPath + "\\" + sd.batch;
                        System.IO.Directory.CreateDirectory(imgDir);

                        wf.Value++;
                        //Rex & Res
                        swRex.WriteLine(sd.CreateRes(writeYM));
                        swRes.WriteLine(sd.CreateRex(writeYM));

                        //画像出力
                        var fn = imgDir + "\\" + (cym % 10000).ToString("0000") +
                            sd.batch + sd.numbering + "01.tif";
                        fc.FileCopy(sd.app.GetImageFullPath(), fn);

                        //続紙出力
                        if (withImages.ContainsKey(sd.app.Aid))
                        {
                            int page = 1;
                            foreach (var wi in withImages[sd.app.Aid])
                            {
                                page++;
                                fn = imgDir + "\\" + (cym % 10000).ToString("0000") +
                                    sd.batch + sd.numbering + page.ToString("00") + ".tif";
                                fc.FileCopy(wi, fn);
                            }
                        }
                    }
                }

                wf.LogPrint("正常に終了しました");
            }
            catch(Exception ex)
            {
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
            }
            finally
            {
                wf.LogSave($"{folderPath}\\Export{DateTime.Now.ToString("yyyyMMddHHmmss")}.log");
                wf.Dispose();
            }

            return true;
        }
    }
}
