﻿namespace Mejor.OsakaGakko
{
    partial class OCRCheckForm_Old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBoxIN = new System.Windows.Forms.PictureBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.textBoxInum = new System.Windows.Forms.TextBox();
            this.textBoxHnum = new System.Windows.Forms.TextBox();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelInum = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.panelIN = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPref = new System.Windows.Forms.TextBox();
            this.labelPref = new System.Windows.Forms.Label();
            this.textBoxFamily = new System.Windows.Forms.TextBox();
            this.textBoxHnumM = new System.Windows.Forms.TextBox();
            this.labelFamily = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageInsert = new System.Windows.Forms.Button();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.panelUP = new System.Windows.Forms.Panel();
            this.textDispNote1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textDispSID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textDispM = new System.Windows.Forms.TextBox();
            this.textDispY = new System.Windows.Forms.TextBox();
            this.textDispGID = new System.Windows.Forms.TextBox();
            this.panelRight = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxNumbering2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxNumbering = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxPref2 = new System.Windows.Forms.TextBox();
            this.textBoxBD2 = new System.Windows.Forms.TextBox();
            this.textBoxBD = new System.Windows.Forms.TextBox();
            this.textBoxBM2 = new System.Windows.Forms.TextBox();
            this.textBoxFamily2 = new System.Windows.Forms.TextBox();
            this.textBoxBM = new System.Windows.Forms.TextBox();
            this.textBoxBY2 = new System.Windows.Forms.TextBox();
            this.textBoxBY = new System.Windows.Forms.TextBox();
            this.textBoxSex2 = new System.Windows.Forms.TextBox();
            this.textBoxSex = new System.Windows.Forms.TextBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.textBoxBirthday2 = new System.Windows.Forms.TextBox();
            this.textBoxHnumM2 = new System.Windows.Forms.TextBox();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.textBoxHnum2 = new System.Windows.Forms.TextBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.textBoxInum2 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.panelCharge = new System.Windows.Forms.Panel();
            this.pictureBoxCharge = new System.Windows.Forms.PictureBox();
            this.textBoxDays2 = new System.Windows.Forms.TextBox();
            this.textBoxDays = new System.Windows.Forms.TextBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.textBoxY2 = new System.Windows.Forms.TextBox();
            this.textBoxM2 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxCharge2 = new System.Windows.Forms.TextBox();
            this.textBoxCharge = new System.Windows.Forms.TextBox();
            this.labelCharge = new System.Windows.Forms.Label();
            this.textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.pictureBoxTotal = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.buttonBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelIN.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            this.panelUP.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panelCharge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCharge)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxIN
            // 
            this.pictureBoxIN.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxIN.Name = "pictureBoxIN";
            this.pictureBoxIN.Size = new System.Drawing.Size(589, 198);
            this.pictureBoxIN.TabIndex = 2;
            this.pictureBoxIN.TabStop = false;
            // 
            // textBoxY
            // 
            this.textBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY.Location = new System.Drawing.Point(59, 7);
            this.textBoxY.MaxLength = 2;
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(33, 26);
            this.textBoxY.TabIndex = 2;
            this.textBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY.TextChanged += new System.EventHandler(this.textBoxY_TextChanged);
            this.textBoxY.Enter += new System.EventHandler(this.textBoxY_Enter);
            // 
            // textBoxM
            // 
            this.textBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM.Location = new System.Drawing.Point(109, 7);
            this.textBoxM.MaxLength = 2;
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(33, 26);
            this.textBoxM.TabIndex = 5;
            this.textBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM.Enter += new System.EventHandler(this.textBoxM_Enter);
            // 
            // textBoxInum
            // 
            this.textBoxInum.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxInum.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxInum.Location = new System.Drawing.Point(59, 69);
            this.textBoxInum.Name = "textBoxInum";
            this.textBoxInum.Size = new System.Drawing.Size(108, 26);
            this.textBoxInum.TabIndex = 19;
            this.textBoxInum.Enter += new System.EventHandler(this.textBoxInum_Enter);
            // 
            // textBoxHnum
            // 
            this.textBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum.Location = new System.Drawing.Point(290, 69);
            this.textBoxHnum.MaxLength = 6;
            this.textBoxHnum.Name = "textBoxHnum";
            this.textBoxHnum.Size = new System.Drawing.Size(93, 26);
            this.textBoxHnum.TabIndex = 23;
            this.textBoxHnum.Enter += new System.EventHandler(this.textBoxHnum_Enter);
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRegist.Location = new System.Drawing.Point(494, 691);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 50;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 50);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(250, 671);
            this.dataGridViewPlist.TabIndex = 0;
            this.dataGridViewPlist.CurrentCellChanged += new System.EventHandler(this.dataGridViewPlist_CurrentCellChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "ScanID:";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(92, 21);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 4;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(142, 21);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 7;
            this.labelM.Text = "月分";
            // 
            // labelInum
            // 
            this.labelInum.AutoSize = true;
            this.labelInum.Location = new System.Drawing.Point(18, 71);
            this.labelInum.Name = "labelInum";
            this.labelInum.Size = new System.Drawing.Size(41, 24);
            this.labelInum.TabIndex = 18;
            this.labelInum.Text = "保険者\r\n番号";
            this.labelInum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(181, 71);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(53, 24);
            this.labelHnum.TabIndex = 21;
            this.labelHnum.Text = "被保険者\r\n番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelIN
            // 
            this.panelIN.AutoScroll = true;
            this.panelIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelIN.Controls.Add(this.pictureBoxIN);
            this.panelIN.Location = new System.Drawing.Point(0, 128);
            this.panelIN.Name = "panelIN";
            this.panelIN.Size = new System.Drawing.Size(591, 200);
            this.panelIN.TabIndex = 30;
            this.panelIN.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panelIN_Scroll);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(499, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 24);
            this.label6.TabIndex = 29;
            this.label6.Text = "本人: 2　六歳: 4\r\n家族: 6　高齢: 8";
            // 
            // textBoxPref
            // 
            this.textBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxPref.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPref.Location = new System.Drawing.Point(355, 7);
            this.textBoxPref.MaxLength = 2;
            this.textBoxPref.Name = "textBoxPref";
            this.textBoxPref.Size = new System.Drawing.Size(44, 26);
            this.textBoxPref.TabIndex = 13;
            this.textBoxPref.Enter += new System.EventHandler(this.textBoxPref_Enter);
            // 
            // labelPref
            // 
            this.labelPref.AutoSize = true;
            this.labelPref.Location = new System.Drawing.Point(302, 9);
            this.labelPref.Name = "labelPref";
            this.labelPref.Size = new System.Drawing.Size(53, 24);
            this.labelPref.TabIndex = 12;
            this.labelPref.Text = "都道府県\r\n番号";
            this.labelPref.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxFamily
            // 
            this.textBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFamily.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFamily.Location = new System.Drawing.Point(465, 69);
            this.textBoxFamily.MaxLength = 1;
            this.textBoxFamily.Name = "textBoxFamily";
            this.textBoxFamily.Size = new System.Drawing.Size(34, 26);
            this.textBoxFamily.TabIndex = 27;
            this.textBoxFamily.Enter += new System.EventHandler(this.textBoxFamily_Enter);
            // 
            // textBoxHnumM
            // 
            this.textBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnumM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnumM.Location = new System.Drawing.Point(234, 69);
            this.textBoxHnumM.MaxLength = 4;
            this.textBoxHnumM.Name = "textBoxHnumM";
            this.textBoxHnumM.Size = new System.Drawing.Size(56, 26);
            this.textBoxHnumM.TabIndex = 22;
            this.textBoxHnumM.TextChanged += new System.EventHandler(this.textBoxHnumM_TextChanged);
            this.textBoxHnumM.Enter += new System.EventHandler(this.textBoxHnum_Enter);
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(406, 83);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(59, 12);
            this.labelFamily.TabIndex = 26;
            this.labelFamily.Text = "本人/家族";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 685);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "年";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(188, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "月 請求分";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Controls.Add(this.splitter1);
            this.panelLeft.Controls.Add(this.panelDatalist);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(750, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageInsert);
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(253, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(497, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageInsert
            // 
            this.buttonImageInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageInsert.Location = new System.Drawing.Point(120, 680);
            this.buttonImageInsert.Name = "buttonImageInsert";
            this.buttonImageInsert.Size = new System.Drawing.Size(40, 23);
            this.buttonImageInsert.TabIndex = 13;
            this.buttonImageInsert.Text = "挿入";
            this.buttonImageInsert.UseVisualStyleBackColor = true;
            this.buttonImageInsert.Click += new System.EventHandler(this.buttonImageInsert_Click);
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 680);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 680);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 680);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(414, 680);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(497, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(250, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 721);
            this.splitter1.TabIndex = 40;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(250, 721);
            this.panelDatalist.TabIndex = 1;
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.GreenYellow;
            this.panelUP.Controls.Add(this.textDispNote1);
            this.panelUP.Controls.Add(this.label11);
            this.panelUP.Controls.Add(this.textDispSID);
            this.panelUP.Controls.Add(this.label9);
            this.panelUP.Controls.Add(this.textDispM);
            this.panelUP.Controls.Add(this.textDispY);
            this.panelUP.Controls.Add(this.textDispGID);
            this.panelUP.Controls.Add(this.label2);
            this.panelUP.Controls.Add(this.label10);
            this.panelUP.Controls.Add(this.label1);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(250, 50);
            this.panelUP.TabIndex = 0;
            // 
            // textDispNote1
            // 
            this.textDispNote1.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispNote1.Location = new System.Drawing.Point(160, 26);
            this.textDispNote1.Name = "textDispNote1";
            this.textDispNote1.ReadOnly = true;
            this.textDispNote1.Size = new System.Drawing.Size(80, 19);
            this.textDispNote1.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(120, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 12);
            this.label11.TabIndex = 8;
            this.label11.Text = "Note1:";
            // 
            // textDispSID
            // 
            this.textDispSID.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispSID.Location = new System.Drawing.Point(56, 4);
            this.textDispSID.Name = "textDispSID";
            this.textDispSID.ReadOnly = true;
            this.textDispSID.Size = new System.Drawing.Size(50, 19);
            this.textDispSID.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "GroupID:";
            // 
            // textDispM
            // 
            this.textDispM.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispM.Location = new System.Drawing.Point(164, 4);
            this.textDispM.Name = "textDispM";
            this.textDispM.ReadOnly = true;
            this.textDispM.Size = new System.Drawing.Size(25, 19);
            this.textDispM.TabIndex = 4;
            // 
            // textDispY
            // 
            this.textDispY.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispY.Location = new System.Drawing.Point(119, 4);
            this.textDispY.Name = "textDispY";
            this.textDispY.ReadOnly = true;
            this.textDispY.Size = new System.Drawing.Size(25, 19);
            this.textDispY.TabIndex = 2;
            // 
            // textDispGID
            // 
            this.textDispGID.BackColor = System.Drawing.SystemColors.Menu;
            this.textDispGID.Location = new System.Drawing.Point(56, 26);
            this.textDispGID.Name = "textDispGID";
            this.textDispGID.ReadOnly = true;
            this.textDispGID.Size = new System.Drawing.Size(50, 19);
            this.textDispGID.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.labelInfo);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.panelIN);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.label18);
            this.panelRight.Controls.Add(this.textBoxNumbering2);
            this.panelRight.Controls.Add(this.label17);
            this.panelRight.Controls.Add(this.textBoxNumbering);
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.textBoxPref2);
            this.panelRight.Controls.Add(this.textBoxBD2);
            this.panelRight.Controls.Add(this.textBoxBD);
            this.panelRight.Controls.Add(this.textBoxBM2);
            this.panelRight.Controls.Add(this.textBoxFamily2);
            this.panelRight.Controls.Add(this.textBoxBM);
            this.panelRight.Controls.Add(this.textBoxBY2);
            this.panelRight.Controls.Add(this.textBoxFamily);
            this.panelRight.Controls.Add(this.textBoxBY);
            this.panelRight.Controls.Add(this.textBoxSex2);
            this.panelRight.Controls.Add(this.textBoxPref);
            this.panelRight.Controls.Add(this.textBoxSex);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.textBoxBirthday2);
            this.panelRight.Controls.Add(this.textBoxHnumM2);
            this.panelRight.Controls.Add(this.textBoxBirthday);
            this.panelRight.Controls.Add(this.textBoxHnumM);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.labelPref);
            this.panelRight.Controls.Add(this.textBoxHnum2);
            this.panelRight.Controls.Add(this.textBoxHnum);
            this.panelRight.Controls.Add(this.labelFamily);
            this.panelRight.Controls.Add(this.labelNumbering);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.textBoxInum2);
            this.panelRight.Controls.Add(this.textBoxInum);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelInum);
            this.panelRight.Controls.Add(this.groupBox10);
            this.panelRight.Controls.Add(this.textBoxY2);
            this.panelRight.Controls.Add(this.textBoxY);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.textBoxM2);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.textBoxM);
            this.panelRight.Controls.Add(this.groupBox4);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(753, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(591, 721);
            this.panelRight.TabIndex = 2;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(411, 673);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(174, 15);
            this.labelInputerName.TabIndex = 55;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(177, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 48);
            this.label3.TabIndex = 54;
            this.label3.Text = "赤バッジ: **\r\n総括表: //\r\n続紙: --\r\n不要: ++";
            // 
            // labelInfo
            // 
            this.labelInfo.Location = new System.Drawing.Point(11, 674);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(376, 38);
            this.labelInfo.TabIndex = 53;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(309, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 24);
            this.label5.TabIndex = 38;
            this.label5.Text = "昭 : 3\r\n平 : 4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(103, 333);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 24);
            this.label4.TabIndex = 34;
            this.label4.Text = "男 : 1\r\n女 : 2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(391, 345);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 41;
            this.label18.Text = "年";
            // 
            // textBoxNumbering2
            // 
            this.textBoxNumbering2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxNumbering2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxNumbering2.Location = new System.Drawing.Point(480, 32);
            this.textBoxNumbering2.MaxLength = 6;
            this.textBoxNumbering2.Name = "textBoxNumbering2";
            this.textBoxNumbering2.Size = new System.Drawing.Size(90, 26);
            this.textBoxNumbering2.TabIndex = 17;
            this.textBoxNumbering2.Visible = false;
            this.textBoxNumbering2.Enter += new System.EventHandler(this.textBoxNumbering_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(451, 345);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 44;
            this.label17.Text = "月";
            // 
            // textBoxNumbering
            // 
            this.textBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxNumbering.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxNumbering.Location = new System.Drawing.Point(480, 7);
            this.textBoxNumbering.MaxLength = 6;
            this.textBoxNumbering.Name = "textBoxNumbering";
            this.textBoxNumbering.Size = new System.Drawing.Size(90, 26);
            this.textBoxNumbering.TabIndex = 16;
            this.textBoxNumbering.Enter += new System.EventHandler(this.textBoxNumbering_Enter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(511, 345);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 47;
            this.label16.Text = "日";
            // 
            // textBoxPref2
            // 
            this.textBoxPref2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxPref2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPref2.Location = new System.Drawing.Point(355, 32);
            this.textBoxPref2.MaxLength = 2;
            this.textBoxPref2.Name = "textBoxPref2";
            this.textBoxPref2.Size = new System.Drawing.Size(44, 26);
            this.textBoxPref2.TabIndex = 14;
            this.textBoxPref2.Visible = false;
            this.textBoxPref2.Enter += new System.EventHandler(this.textBoxPref_Enter);
            // 
            // textBoxBD2
            // 
            this.textBoxBD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD2.Location = new System.Drawing.Point(471, 356);
            this.textBoxBD2.MaxLength = 2;
            this.textBoxBD2.Name = "textBoxBD2";
            this.textBoxBD2.Size = new System.Drawing.Size(40, 26);
            this.textBoxBD2.TabIndex = 46;
            this.textBoxBD2.Visible = false;
            this.textBoxBD2.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxBD
            // 
            this.textBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD.Location = new System.Drawing.Point(471, 331);
            this.textBoxBD.MaxLength = 2;
            this.textBoxBD.Name = "textBoxBD";
            this.textBoxBD.Size = new System.Drawing.Size(40, 26);
            this.textBoxBD.TabIndex = 45;
            this.textBoxBD.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxBM2
            // 
            this.textBoxBM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM2.Location = new System.Drawing.Point(411, 356);
            this.textBoxBM2.MaxLength = 2;
            this.textBoxBM2.Name = "textBoxBM2";
            this.textBoxBM2.Size = new System.Drawing.Size(40, 26);
            this.textBoxBM2.TabIndex = 43;
            this.textBoxBM2.Visible = false;
            this.textBoxBM2.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxFamily2
            // 
            this.textBoxFamily2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFamily2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFamily2.Location = new System.Drawing.Point(465, 94);
            this.textBoxFamily2.MaxLength = 1;
            this.textBoxFamily2.Name = "textBoxFamily2";
            this.textBoxFamily2.Size = new System.Drawing.Size(34, 26);
            this.textBoxFamily2.TabIndex = 28;
            this.textBoxFamily2.Visible = false;
            this.textBoxFamily2.Enter += new System.EventHandler(this.textBoxFamily_Enter);
            // 
            // textBoxBM
            // 
            this.textBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM.Location = new System.Drawing.Point(411, 331);
            this.textBoxBM.MaxLength = 2;
            this.textBoxBM.Name = "textBoxBM";
            this.textBoxBM.Size = new System.Drawing.Size(40, 26);
            this.textBoxBM.TabIndex = 42;
            this.textBoxBM.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxBY2
            // 
            this.textBoxBY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY2.Location = new System.Drawing.Point(351, 356);
            this.textBoxBY2.MaxLength = 2;
            this.textBoxBY2.Name = "textBoxBY2";
            this.textBoxBY2.Size = new System.Drawing.Size(40, 26);
            this.textBoxBY2.TabIndex = 40;
            this.textBoxBY2.Visible = false;
            this.textBoxBY2.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxBY
            // 
            this.textBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY.Location = new System.Drawing.Point(351, 331);
            this.textBoxBY.MaxLength = 2;
            this.textBoxBY.Name = "textBoxBY";
            this.textBoxBY.Size = new System.Drawing.Size(40, 26);
            this.textBoxBY.TabIndex = 39;
            this.textBoxBY.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxSex2
            // 
            this.textBoxSex2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex2.Location = new System.Drawing.Point(61, 356);
            this.textBoxSex2.MaxLength = 1;
            this.textBoxSex2.Name = "textBoxSex2";
            this.textBoxSex2.Size = new System.Drawing.Size(40, 26);
            this.textBoxSex2.TabIndex = 33;
            this.textBoxSex2.Visible = false;
            this.textBoxSex2.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxSex
            // 
            this.textBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex.Location = new System.Drawing.Point(61, 331);
            this.textBoxSex.MaxLength = 1;
            this.textBoxSex.Name = "textBoxSex";
            this.textBoxSex.Size = new System.Drawing.Size(40, 26);
            this.textBoxSex.TabIndex = 32;
            this.textBoxSex.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(28, 345);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 31;
            this.labelSex.Text = "性別";
            // 
            // textBoxBirthday2
            // 
            this.textBoxBirthday2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday2.Location = new System.Drawing.Point(267, 356);
            this.textBoxBirthday2.Name = "textBoxBirthday2";
            this.textBoxBirthday2.Size = new System.Drawing.Size(40, 26);
            this.textBoxBirthday2.TabIndex = 37;
            this.textBoxBirthday2.Visible = false;
            this.textBoxBirthday2.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // textBoxHnumM2
            // 
            this.textBoxHnumM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnumM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnumM2.Location = new System.Drawing.Point(234, 94);
            this.textBoxHnumM2.MaxLength = 4;
            this.textBoxHnumM2.Name = "textBoxHnumM2";
            this.textBoxHnumM2.Size = new System.Drawing.Size(56, 26);
            this.textBoxHnumM2.TabIndex = 24;
            this.textBoxHnumM2.Visible = false;
            this.textBoxHnumM2.Enter += new System.EventHandler(this.textBoxHnum_Enter);
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday.Location = new System.Drawing.Point(267, 331);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(40, 26);
            this.textBoxBirthday.TabIndex = 36;
            this.textBoxBirthday.Enter += new System.EventHandler(this.textBoxSex_Enter);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(213, 345);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 35;
            this.labelBirthday.Text = "生年月日";
            // 
            // textBoxHnum2
            // 
            this.textBoxHnum2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum2.Location = new System.Drawing.Point(290, 94);
            this.textBoxHnum2.MaxLength = 6;
            this.textBoxHnum2.Name = "textBoxHnum2";
            this.textBoxHnum2.Size = new System.Drawing.Size(93, 26);
            this.textBoxHnum2.TabIndex = 25;
            this.textBoxHnum2.Visible = false;
            this.textBoxHnum2.Enter += new System.EventHandler(this.textBoxHnum_Enter);
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(421, 9);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(59, 12);
            this.labelNumbering.TabIndex = 15;
            this.labelNumbering.Text = "ナンバリング";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxInum2
            // 
            this.textBoxInum2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxInum2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxInum2.Location = new System.Drawing.Point(59, 94);
            this.textBoxInum2.Name = "textBoxInum2";
            this.textBoxInum2.Size = new System.Drawing.Size(108, 26);
            this.textBoxInum2.TabIndex = 20;
            this.textBoxInum2.Visible = false;
            this.textBoxInum2.Enter += new System.EventHandler(this.textBoxInum_Enter);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.panelCharge);
            this.groupBox10.Controls.Add(this.textBoxDays2);
            this.groupBox10.Controls.Add(this.textBoxDays);
            this.groupBox10.Controls.Add(this.labelDays);
            this.groupBox10.Location = new System.Drawing.Point(457, 384);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(130, 286);
            this.groupBox10.TabIndex = 49;
            this.groupBox10.TabStop = false;
            // 
            // panelCharge
            // 
            this.panelCharge.AutoScroll = true;
            this.panelCharge.Controls.Add(this.pictureBoxCharge);
            this.panelCharge.Location = new System.Drawing.Point(8, 12);
            this.panelCharge.Name = "panelCharge";
            this.panelCharge.Size = new System.Drawing.Size(120, 208);
            this.panelCharge.TabIndex = 0;
            this.panelCharge.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panelCharge_Scroll);
            // 
            // pictureBoxCharge
            // 
            this.pictureBoxCharge.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCharge.Name = "pictureBoxCharge";
            this.pictureBoxCharge.Size = new System.Drawing.Size(120, 208);
            this.pictureBoxCharge.TabIndex = 3;
            this.pictureBoxCharge.TabStop = false;
            // 
            // textBoxDays2
            // 
            this.textBoxDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays2.Location = new System.Drawing.Point(72, 251);
            this.textBoxDays2.MaxLength = 2;
            this.textBoxDays2.Name = "textBoxDays2";
            this.textBoxDays2.Size = new System.Drawing.Size(50, 26);
            this.textBoxDays2.TabIndex = 3;
            this.textBoxDays2.Visible = false;
            // 
            // textBoxDays
            // 
            this.textBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays.Location = new System.Drawing.Point(72, 226);
            this.textBoxDays.MaxLength = 2;
            this.textBoxDays.Name = "textBoxDays";
            this.textBoxDays.Size = new System.Drawing.Size(50, 26);
            this.textBoxDays.TabIndex = 2;
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(14, 240);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(53, 12);
            this.labelDays.TabIndex = 1;
            this.labelDays.Text = "診療日数";
            // 
            // textBoxY2
            // 
            this.textBoxY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY2.Location = new System.Drawing.Point(59, 32);
            this.textBoxY2.MaxLength = 2;
            this.textBoxY2.Name = "textBoxY2";
            this.textBoxY2.Size = new System.Drawing.Size(33, 26);
            this.textBoxY2.TabIndex = 3;
            this.textBoxY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY2.Visible = false;
            this.textBoxY2.TextChanged += new System.EventHandler(this.textBoxY_TextChanged);
            this.textBoxY2.Enter += new System.EventHandler(this.textBoxY_Enter);
            // 
            // textBoxM2
            // 
            this.textBoxM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM2.Location = new System.Drawing.Point(109, 32);
            this.textBoxM2.MaxLength = 2;
            this.textBoxM2.Name = "textBoxM2";
            this.textBoxM2.Size = new System.Drawing.Size(33, 26);
            this.textBoxM2.TabIndex = 6;
            this.textBoxM2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM2.Visible = false;
            this.textBoxM2.Enter += new System.EventHandler(this.textBoxM_Enter);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.textBoxCharge2);
            this.groupBox4.Controls.Add(this.textBoxCharge);
            this.groupBox4.Controls.Add(this.labelCharge);
            this.groupBox4.Controls.Add(this.textBoxTotal2);
            this.groupBox4.Controls.Add(this.textBoxTotal);
            this.groupBox4.Controls.Add(this.labelTotal);
            this.groupBox4.Controls.Add(this.panelTotal);
            this.groupBox4.Location = new System.Drawing.Point(12, 384);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(440, 286);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            // 
            // textBoxCharge2
            // 
            this.textBoxCharge2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge2.Location = new System.Drawing.Point(332, 206);
            this.textBoxCharge2.Name = "textBoxCharge2";
            this.textBoxCharge2.Size = new System.Drawing.Size(102, 26);
            this.textBoxCharge2.TabIndex = 6;
            this.textBoxCharge2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCharge2.Visible = false;
            // 
            // textBoxCharge
            // 
            this.textBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge.Location = new System.Drawing.Point(332, 180);
            this.textBoxCharge.Name = "textBoxCharge";
            this.textBoxCharge.Size = new System.Drawing.Size(102, 26);
            this.textBoxCharge.TabIndex = 5;
            this.textBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(330, 165);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(53, 12);
            this.labelCharge.TabIndex = 4;
            this.labelCharge.Text = "請求金額";
            // 
            // textBoxTotal2
            // 
            this.textBoxTotal2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal2.Location = new System.Drawing.Point(333, 56);
            this.textBoxTotal2.Name = "textBoxTotal2";
            this.textBoxTotal2.Size = new System.Drawing.Size(102, 26);
            this.textBoxTotal2.TabIndex = 3;
            this.textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal2.Visible = false;
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal.Location = new System.Drawing.Point(333, 31);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(102, 26);
            this.textBoxTotal.TabIndex = 2;
            this.textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal.Enter += new System.EventHandler(this.textBoxTotal_Enter);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(330, 16);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(53, 12);
            this.labelTotal.TabIndex = 1;
            this.labelTotal.Text = "合計金額";
            // 
            // panelTotal
            // 
            this.panelTotal.AutoScroll = true;
            this.panelTotal.Controls.Add(this.pictureBoxTotal);
            this.panelTotal.Location = new System.Drawing.Point(6, 12);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(321, 267);
            this.panelTotal.TabIndex = 0;
            this.panelTotal.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panelTotal_Scroll);
            // 
            // pictureBoxTotal
            // 
            this.pictureBoxTotal.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxTotal.Name = "pictureBoxTotal";
            this.pictureBoxTotal.Size = new System.Drawing.Size(321, 267);
            this.pictureBoxTotal.TabIndex = 3;
            this.pictureBoxTotal.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Location = new System.Drawing.Point(750, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(398, 691);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 56;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // FormOCRCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Name = "FormOCRCheck";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_OsakaGakko3_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_OsakaGakko_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelIN.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelDatalist.ResumeLayout(false);
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panelCharge.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCharge)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIN;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.TextBox textBoxInum;
        private System.Windows.Forms.TextBox textBoxHnum;
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelInum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelIN;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TextBox textBoxPref;
        private System.Windows.Forms.Label labelPref;
        private System.Windows.Forms.TextBox textBoxSex;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.TextBox textBoxFamily;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.TextBox textBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Panel panelCharge;
        private System.Windows.Forms.PictureBox pictureBoxCharge;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.PictureBox pictureBoxTotal;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelNumbering;
        private System.Windows.Forms.TextBox textDispM;
        private System.Windows.Forms.TextBox textDispY;
        private System.Windows.Forms.TextBox textDispGID;
        private System.Windows.Forms.TextBox textBoxNumbering;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxBD;
        private System.Windows.Forms.TextBox textBoxBM;
        private System.Windows.Forms.TextBox textBoxBY;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.TextBox textBoxDays;
        private System.Windows.Forms.GroupBox groupBox10;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textDispNote1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textDispSID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageInsert;
        private System.Windows.Forms.TextBox textBoxHnumM;
        private System.Windows.Forms.TextBox textBoxNumbering2;
        private System.Windows.Forms.TextBox textBoxPref2;
        private System.Windows.Forms.TextBox textBoxFamily2;
        private System.Windows.Forms.TextBox textBoxHnumM2;
        private System.Windows.Forms.TextBox textBoxHnum2;
        private System.Windows.Forms.TextBox textBoxInum2;
        private System.Windows.Forms.TextBox textBoxY2;
        private System.Windows.Forms.TextBox textBoxM2;
        private System.Windows.Forms.TextBox textBoxBD2;
        private System.Windows.Forms.TextBox textBoxBM2;
        private System.Windows.Forms.TextBox textBoxBY2;
        private System.Windows.Forms.TextBox textBoxSex2;
        private System.Windows.Forms.TextBox textBoxBirthday2;
        private System.Windows.Forms.TextBox textBoxCharge2;
        private System.Windows.Forms.TextBox textBoxTotal2;
        private System.Windows.Forms.TextBox textBoxDays2;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonBack;
    }
}