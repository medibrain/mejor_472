﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OsakaGakko.check
{
    interface Columns
    {
        int Row { get; set; }
        Property C_GEEMM { get; set; }
        Property Batch { get; set; }
        Property Numbering { get; set; }
    }
}
