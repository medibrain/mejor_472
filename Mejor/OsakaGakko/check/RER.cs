﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OsakaGakko.check
{
    class RER : Columns
    {
        private const int REQUIRED_COLUMN_COUNT = 43;
        private bool lostColumn;
        private bool overColumn;
        public bool Error;
        public int Row { get; set; }
        public string 入力年月 => C_GEEMM.Val;
        public string ﾊﾞｯﾁ => Batch.Val;
        public string ﾅﾝﾊﾞﾘﾝｸﾞ => Numbering.Val;
        public string 診療年月 => V_GEEMM.Val;
        public string 明細区分 => Meisai.Val;
        public string 一般老健 => IppanRouken.Val;
        public string 本人家族 => HonninKazoku.Val;
        public string 社保公費 => SyahoKouhi.Val;
        public string 都道府県番号 => PrefNum.Val;
        public string 医療機関ｺｰﾄﾞ => HospitalCode.Val;
        public string 予備1 => Yobi1.Val;
        public string 公費1ｺｰﾄﾞ => Kouhi1Code.Val;
        public string 保険者番号 => HokensyaNum.Val;
        public string 組合員番号 => KumiaiinNum.Val;
        public string 性別 => SexNum.Val;
        public string 和暦生年月 => B_GEEMM.Val;
        public string 特記 => Tokki.Val;
        public string 通知区分 => Tuuchikubun.Val;
        public string 予備2 => Yobi2.Val;
        public string 処方箋 => Syohosen.Val;
        public string 医保日数 => IhoNissu.Val;
        public string 医保請求点数 => IhoSeikyuTensu.Val;
        public string 医保負担金額 => IhoFutanKingaku.Val;
        public string 公費1日数 => Kouhi1Nissu.Val;
        public string 公費1請求点数 => Kouhi1SeikyuTensu.Val;
        public string 公費1負担金額 => Kouhi1FutanKingaku.Val;
        public string 公費2日数 => Kouhi2Nissu.Val;
        public string 公費2請求点数 => Kouhi2SeikyuTensu.Val;
        public string 公費2負担金額 => Kouhi2FutanKingaku.Val;
        public string 食療医日数 => SyokujiRyoyoIhoNissu.Val;
        public string 食療医療養金額 => SyokujiRyoyoIhoRyoyoKingaku.Val;
        public string 食療医負担金額 => SyokujiRyoyoIhoFutanKingaku.Val;
        public string 食療公1日数 => SyokujiRyoyoKouhi1Nissu.Val;
        public string 食療公1療養金額 => SyokujiRyoyoKouhi1RyoyoKingaku.Val;
        public string 食療公1負担金額 => SyokujiRyoyoKouhi1FutanKingaku.Val;
        public string 食療公2日数 => SyokujiRyoyoKouhi2Nissu.Val;
        public string 食療公2療養金額 => SyokujiRyoyoKouhi2RyoyoKingaku.Val;
        public string 食療公2負担金額 => SyokujiRyoyoKouhi2FutanKingaku.Val;
        public string 高額療養費 => KougakuRyoyohi.Val;
        public string 再審査再入力 => SaisinsaSainyuryoku.Val;
        public string DRG区分 => DrgType.Val;
        public string 診療年月日＿自 => DrgDay1.Val;
        public string 診療年月日＿至 => DrgDay2.Val;
        public Property C_GEEMM { get; set; }
        public Property Batch { get; set; }
        public Property Numbering { get; set; }
        private Property V_GEEMM;
        private Property Meisai;
        private Property IppanRouken;
        private Property SyahoKouhi;
        private Property HonninKazoku;
        private Property PrefNum;
        private Property HospitalCode;
        private Property Yobi1;
        private Property Kouhi1Code;
        private Property HokensyaNum;
        private Property KumiaiinNum;
        private Property SexNum;
        private Property B_GEEMM;
        private Property Tokki;
        private Property Tuuchikubun;
        private Property Yobi2;
        private Property Syohosen;
        private Property IhoNissu;
        private Property IhoSeikyuTensu;
        private Property IhoFutanKingaku;
        private Property Kouhi1Nissu;
        private Property Kouhi1SeikyuTensu;
        private Property Kouhi1FutanKingaku;
        private Property Kouhi2Nissu;
        private Property Kouhi2SeikyuTensu;
        private Property Kouhi2FutanKingaku;
        private Property SyokujiRyoyoIhoNissu;
        private Property SyokujiRyoyoIhoRyoyoKingaku;
        private Property SyokujiRyoyoIhoFutanKingaku;
        private Property SyokujiRyoyoKouhi1Nissu;
        private Property SyokujiRyoyoKouhi1RyoyoKingaku;
        private Property SyokujiRyoyoKouhi1FutanKingaku;
        private Property SyokujiRyoyoKouhi2Nissu;
        private Property SyokujiRyoyoKouhi2RyoyoKingaku;
        private Property SyokujiRyoyoKouhi2FutanKingaku;
        private Property KougakuRyoyohi;
        private Property SaisinsaSainyuryoku;
        private Property DrgType;
        private Property DrgDay1;
        private Property DrgDay2;
        public RER(int row, string[] record)
        {
            this.Row = row;

            lostColumn = record.Length < REQUIRED_COLUMN_COUNT;
            overColumn = record.Length > REQUIRED_COLUMN_COUNT;
            if (lostColumn || overColumn)
            {
                Error = true;
                var p = new Property();
                this.C_GEEMM = p;
                this.Batch = p;
                this.Numbering = p;
                this.V_GEEMM = p;
                this.Meisai = p;
                this.IppanRouken = p;
                this.SyahoKouhi = p;
                this.HonninKazoku = p;
                this.PrefNum = p;
                this.HospitalCode = p;
                this.Yobi1 = p;
                this.Kouhi1Code = p;
                this.HokensyaNum = p;
                this.KumiaiinNum = p;
                this.SexNum = p;
                this.B_GEEMM = p;
                this.Tokki = p;
                this.Tuuchikubun = p;
                this.Yobi2 = p;
                this.Syohosen = p;
                this.IhoNissu = p;
                this.IhoSeikyuTensu = p;
                this.IhoFutanKingaku = p;
                this.Kouhi1Nissu = p;
                this.Kouhi1SeikyuTensu = p;
                this.Kouhi1FutanKingaku = p;
                this.Kouhi2Nissu = p;
                this.Kouhi2SeikyuTensu = p;
                this.Kouhi2FutanKingaku = p;
                this.SyokujiRyoyoIhoNissu = p;
                this.SyokujiRyoyoIhoRyoyoKingaku = p;
                this.SyokujiRyoyoIhoFutanKingaku = p;
                this.SyokujiRyoyoKouhi1Nissu = p;
                this.SyokujiRyoyoKouhi1RyoyoKingaku = p;
                this.SyokujiRyoyoKouhi1FutanKingaku = p;
                this.SyokujiRyoyoKouhi2Nissu = p;
                this.SyokujiRyoyoKouhi2RyoyoKingaku = p;
                this.SyokujiRyoyoKouhi2FutanKingaku = p;
                this.KougakuRyoyohi = p;
                this.SaisinsaSainyuryoku = p;
                this.DrgType = p;
                this.DrgDay1 = p;
                this.DrgDay2 = p;
                return;
            }

            this.C_GEEMM = new Property(nameof(RER.入力年月), record[0], 5, Property.TYPE.GEEMM);
            this.Batch = new Property(nameof(RER.ﾊﾞｯﾁ), record[1], 4, Property.TYPE.NUMERIC);
            this.Numbering = new Property(nameof(RER.ﾅﾝﾊﾞﾘﾝｸﾞ), record[2], 6, Property.TYPE.NUMERIC);
            this.V_GEEMM = new Property(nameof(RER.診療年月), record[3], 5, Property.TYPE.GEEMM);
            this.Meisai = new Property(nameof(RER.明細区分), record[4], new string[] { "1", "3", "4", "5", "6" });
            this.IppanRouken = new Property(nameof(RER.一般老健), record[5], new string[] { "1", "3" });
            this.SyahoKouhi = new Property(nameof(RER.社保公費), record[6], new string[] { "1", "2", "3" });
            this.HonninKazoku = new Property(nameof(RER.本人家族), record[7], 9, true);
            this.PrefNum = new Property(nameof(RER.都道府県番号), record[8], 2, 47);
            this.HospitalCode = new Property(nameof(RER.医療機関ｺｰﾄﾞ), record[9], 7, Property.TYPE.NUMERIC);
            this.Yobi1 = new Property(nameof(RER.予備1), record[10], new string[] { "" });
            this.Kouhi1Code = new Property(nameof(RER.公費1ｺｰﾄﾞ), record[11], -1, 2, -1, false, Property.TYPE.NUMERIC);
            this.HokensyaNum = new Property(nameof(RER.保険者番号), record[12], 8, Property.TYPE.NUMERIC);
            this.KumiaiinNum = new Property(nameof(RER.組合員番号), record[13], 10, true, Property.TYPE.KUMIAIINBANGO);
            this.SexNum = new Property(nameof(RER.性別), record[14], new string[] { "1", "2" });
            this.B_GEEMM = new Property(nameof(RER.和暦生年月), record[15], 5, Property.TYPE.GEEMM);
            this.Tokki = new Property(nameof(RER.特記), record[16], new string[]
            {
                    "",
                    "01", "02", "03",
                    "10", "11", "16",
                    "20",
                    "96", "97",
            });
            this.Tuuchikubun = new Property(nameof(RER.通知区分), record[17], new string[] { "0", "1" });
            this.Yobi2 = new Property(nameof(RER.予備2), record[18], new string[] { "" });
            this.Syohosen = new Property(nameof(RER.処方箋), record[19], new string[] { "" });
            this.IhoNissu = new Property(nameof(RER.医保日数), record[20], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.IhoSeikyuTensu = new Property(nameof(RER.医保請求点数), record[21], 8, false, Property.TYPE.NUMERIC);
            this.IhoFutanKingaku = new Property(nameof(RER.医保負担金額), record[22], 8, false, Property.TYPE.NUMERIC);
            this.Kouhi1Nissu = new Property(nameof(RER.公費1日数), record[23], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.Kouhi1SeikyuTensu = new Property(nameof(RER.公費1請求点数), record[24], 8, false, Property.TYPE.NUMERIC);
            this.Kouhi1FutanKingaku = new Property(nameof(RER.公費1負担金額), record[25], 8, false, Property.TYPE.NUMERIC);
            this.Kouhi2Nissu = new Property(nameof(RER.公費2日数), record[26], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.Kouhi2SeikyuTensu = new Property(nameof(RER.公費2請求点数), record[27], 8, false, Property.TYPE.NUMERIC);
            this.Kouhi2FutanKingaku = new Property(nameof(RER.公費2負担金額), record[28], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoIhoNissu = new Property(nameof(RER.食療医日数), record[29], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoIhoRyoyoKingaku = new Property(nameof(RER.食療医療養金額), record[30], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoIhoFutanKingaku = new Property(nameof(RER.食療医負担金額), record[31], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi1Nissu = new Property(nameof(RER.食療公1日数), record[32], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi1RyoyoKingaku = new Property(nameof(RER.食療公1療養金額), record[33], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi1FutanKingaku = new Property(nameof(RER.食療公1負担金額), record[34], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi2Nissu = new Property(nameof(RER.食療公2日数), record[35], -1, 2, 31, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi2RyoyoKingaku = new Property(nameof(RER.食療公2療養金額), record[36], 8, false, Property.TYPE.NUMERIC);
            this.SyokujiRyoyoKouhi2FutanKingaku = new Property(nameof(RER.食療公2負担金額), record[37], 8, false, Property.TYPE.NUMERIC);
            this.KougakuRyoyohi = new Property(nameof(RER.高額療養費), record[38], 8, false, Property.TYPE.NUMERIC);
            this.SaisinsaSainyuryoku = new Property(nameof(RER.再審査再入力), record[39], new string[] { "0", "1" });
            this.DrgType = new Property(nameof(RER.DRG区分), record[40], new string[] { "", "0", "1", "2" });
            this.DrgDay1 = new Property(nameof(RER.診療年月日＿自), record[41], false, Property.TYPE.GEEMMDD);
            this.DrgDay2 = new Property(nameof(RER.診療年月日＿至), record[42], false, Property.TYPE.GEEMMDD);
        }
        public bool AllValueCheck(out string message)
        {
            message = "";

            if (lostColumn)
            {
                message += $"({Row}行目) 列数が{REQUIRED_COLUMN_COUNT}ではありません\r\n";
                return false;
            }
            else if (overColumn)
            {
                message += $"({Row}行目)[] 列数が{REQUIRED_COLUMN_COUNT}を超過しています\r\n";
                return false;
            }

            string m;
            if (!C_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Batch.ValueCheck(Row, out m)) message += m;
            if (!Numbering.ValueCheck(Row, out m)) message += m;
            if (!V_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Meisai.ValueCheck(Row, out m)) message += m;
            if (!IppanRouken.ValueCheck(Row, out m)) message += m;
            if (!SyahoKouhi.ValueCheck(Row, out m)) message += m;
            if (!HonninKazoku.ValueCheck(Row, out m)) message += m;
            if (!PrefNum.ValueCheck(Row, out m)) message += m;
            if (!HospitalCode.ValueCheck(Row, out m)) message += m;
            if (!Yobi1.ValueCheck(Row, out m)) message += m;
            if (!Kouhi1Code.ValueCheck(Row, out m)) message += m;
            if (!HokensyaNum.ValueCheck(Row, out m)) message += m;
            if (!KumiaiinNum.ValueCheck(Row, out m)) message += m;
            if (!SexNum.ValueCheck(Row, out m)) message += m;
            if (!B_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Tokki.ValueCheck(Row, out m)) message += m;
            if (!Tuuchikubun.ValueCheck(Row, out m)) message += m;
            if (!Yobi2.ValueCheck(Row, out m)) message += m;
            if (!Syohosen.ValueCheck(Row, out m)) message += m;
            if (!IhoNissu.ValueCheck(Row, out m)) message += m;
            if (!IhoSeikyuTensu.ValueCheck(Row, out m)) message += m;
            if (!IhoFutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!Kouhi1Nissu.ValueCheck(Row, out m)) message += m;
            if (!Kouhi1SeikyuTensu.ValueCheck(Row, out m)) message += m;
            if (!Kouhi1FutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!Kouhi2Nissu.ValueCheck(Row, out m)) message += m;
            if (!Kouhi2SeikyuTensu.ValueCheck(Row, out m)) message += m;
            if (!Kouhi2FutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoIhoNissu.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoIhoRyoyoKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoIhoFutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi1Nissu.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi1RyoyoKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi1FutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi2Nissu.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi2RyoyoKingaku.ValueCheck(Row, out m)) message += m;
            if (!SyokujiRyoyoKouhi2FutanKingaku.ValueCheck(Row, out m)) message += m;
            if (!KougakuRyoyohi.ValueCheck(Row, out m)) message += m;
            if (!SaisinsaSainyuryoku.ValueCheck(Row, out m)) message += m;
            if (!drgCheck(Row, out m)) message += m;

            if (message != "") message = message.Substring(0, message.LastIndexOf("\r\n"));
            return message == "";
        }

        private bool drgCheck(int row, out string message)
        {
            message = "";
            string m;
            if(DrgType.Val != "")
            {
                DrgDay1.Required = true;
                DrgDay2.Required = true;
            }
            if (!DrgType.ValueCheck(Row, out m)) message += m;
            if (!DrgDay1.ValueCheck(Row, out m)) message += m;
            if (!DrgDay2.ValueCheck(Row, out m)) message += m;

            return message == "";
        }
    }
}
