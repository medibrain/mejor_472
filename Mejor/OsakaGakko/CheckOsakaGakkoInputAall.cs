﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor.OsakaGakko
{
            
    class CheckOsakaGakkoInputAall
    {
        static Regex re = new Regex(@"[^0-9]");

        public App app { get; private set; }
        public bool ErrInum { get; private set; }
        public bool ErrHnum { get; private set; }
        public bool ErrHpref { get; private set; }
        public bool ErrAtotal { get; private set; }
        public bool ErrAcharge { get; private set; }
        public bool ErrAyear { get; private set; }
        public bool ErrAmonth { get; private set; }
        public bool ErrAcounteddays { get; private set; }
        public bool ErrSex { get; private set; }
        public bool ErrEra { get; private set; }
        public bool ErrBY { get; private set; }
        public bool ErrBM { get; private set; }
        public bool ErrBD { get; private set; }

        public bool ErrBirth
        {
            get
            {
                return ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public bool HasError
        {
            get
            {
                return ErrInum || ErrHnum || ErrHpref || ErrHpref ||
                    ErrAtotal || ErrAcharge || ErrAyear || ErrAmonth ||
                    ErrAcounteddays || ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public CheckOsakaGakkoInputAall(App app, System.Windows.Forms.Label label)
        {
            this.app = app;
            var ocr = app.OcrData.Split(',');
            if (ocr.Length < 200)
            {
                ErrAyear = true;
                ErrAmonth = true;
                ErrInum = true;
                ErrHnum = true;
                ErrHpref = true;
                ErrAtotal = true;
                ErrAcharge = true;
                ErrAcounteddays = true;
                ErrSex = true;
                ErrEra = true;
                ErrBY = true;
                ErrBM = true;
                ErrBD = true;
                label.Text = "OCRデータがありません";
                return;
            }

            ErrAyear = false;
            ErrAmonth = false;
            ErrInum = false;
            ErrHnum = false;
            ErrHpref = false;
            ErrAtotal = false;
            ErrAcharge = false;
            ErrAcounteddays = false;
            ErrSex = false;
            ErrEra = false;
            ErrBY = false;
            ErrBM = false;
            ErrBD = false;

            string labelText = "Aall   ";
            int temp = 0;

            //診療年
            labelText += "AY:" + ocr[3] + "   ";
            int.TryParse(re.Replace(ocr[3], ""), out temp);
            if (temp == 0 || app.MediYear != temp)
                ErrAyear = true;

            //診療月
            labelText += "AM:" + ocr[4] + "   ";
            int.TryParse(re.Replace(ocr[4], ""), out temp);
            if (temp == 0 || app.MediMonth != temp)
                ErrAmonth = true;

            //保険者番号
            if (app.InsNum != "34270017")
                ErrInum = true;

            //都道府県
            labelText += "pref:" + ocr[5] + "   ";
            int.TryParse(re.Replace(ocr[5], ""), out temp);
            if (temp == 0 || app.HihoPref != temp)
                ErrHpref = true;

            //被保険者番号
            var ahn = re.Replace(ocr[7], "");
            if (ahn.Length > 10) ahn = ahn.Substring(ahn.Length - 10);
            labelText += "hnum:" + ahn + "   ";
            var ihn = re.Replace(app.HihoNum, "");
            if (ahn != ihn || ihn.Length != 10)
                ErrHnum = true;

            //性別
            string sexStr = "";
            if (ocr[30] != "0") { sexStr = "1"; }
            if (ocr[31] != "0") { sexStr = sexStr + "2"; }
            if (sexStr.Length != 1 || app.Sex.ToString() != sexStr)
                ErrSex = true;
            labelText += "sex:" + sexStr + "   ";

            //生年月日 年号
            string era = "";
            if (ocr[32] != "0") era = "明治";
            if (ocr[33] != "0") era += "大正";
            if (ocr[34] != "0") era += "昭和";
            if (ocr[35] != "0") era += "平成";
            if (DateTimeEx.GetEra(app.Birthday) != era) 
                ErrEra = true;
            labelText += "era:" + era + "   ";

            //生年月日 年
            var y = ocr[36];
            var yi = y.IndexOf('年');
            if (yi > 0)
                y = y.Remove(yi);
            labelText += "Y:" + y + "   ";
            int.TryParse(re.Replace(y, ""), out temp);
            if (temp == 0 || DateTimeEx.GetJpYear(app.Birthday) != temp)
                ErrBY = true;

            //生年月日 月
            var m = ocr[37];
            var mi = m.IndexOf('月');
            if (mi > 0)
                m = m.Remove(mi);
            labelText += "M:" + m + "   ";
            int.TryParse(re.Replace(m, ""), out temp);
            if (temp == 0 || app.Birthday.Month != temp)
                ErrBM = true;

            //生年月日 日
            var d = ocr[38];
            var di = d.IndexOf('日');
            if (di > 0)
                d = d.Remove(di);
            labelText += "D:" + d + "   ";
            int.TryParse(re.Replace(d, ""), out temp);
            if (temp == 0 || app.Birthday.Day != temp)
                ErrBD = true;
            
            //合計
            labelText += "total:" + ocr[193] + "   ";
            int.TryParse(re.Replace(ocr[193], ""), out temp);
            if (temp == 0 || app.Total != temp)
                ErrAtotal = true;

            //請求
            labelText += "charge:" + ocr[195] + "   ";
            int.TryParse(re.Replace(ocr[195], ""), out temp);
            if (temp == 0 || app.Charge != temp) 
                ErrAcharge = true;

            //合計と請求は計算が合い、どちらか一方があっていればOK
            bool payError = false;
            if ((app.Family == 2 || app.Family == 6) && (int)(app.Total * 70 / 100) != app.Charge)
                payError = true;
            else if (app.Family == 8 && (int)(app.Total * 80 / 100) != app.Charge && (int)(app.Total * 90 / 100) != app.Charge)
                payError = true;
            else if (app.Family == 4 && (int)(app.Total * 80 / 100) != app.Charge)
                payError = true;

            if (!payError && (!ErrAtotal || !ErrAcharge))
            {
                ErrAtotal = false;
                ErrAcharge = false;
            }

            //実日数
            labelText += "days:" + ocr[53] + "," + ocr[70] + "," + ocr[87] + "," + ocr[104] + "   ";
            var idays = new List<int>();
            int iday;
            string sday;
            sday = re.Replace(ocr[53], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[70], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[87], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[104], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[121], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            iday = idays.Max();
            if (app.CountedDays != iday) 
                ErrAcounteddays = true;

            label.Text = labelText;
        }

    }
}
