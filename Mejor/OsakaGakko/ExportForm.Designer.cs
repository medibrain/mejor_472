﻿namespace Mejor.OsakaGakko
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonNohin = new System.Windows.Forms.RadioButton();
            this.radioButtonBunseki = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSubmitAllCheck = new System.Windows.Forms.Button();
            this.checkBoxSubmitRER = new System.Windows.Forms.CheckBox();
            this.checkBoxSubmitHES = new System.Windows.Forms.CheckBox();
            this.checkBoxSubmitRESREX = new System.Windows.Forms.CheckBox();
            this.radioButtonMediMatching = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonNohin
            // 
            this.radioButtonNohin.AutoSize = true;
            this.radioButtonNohin.Checked = true;
            this.radioButtonNohin.Location = new System.Drawing.Point(35, 24);
            this.radioButtonNohin.Name = "radioButtonNohin";
            this.radioButtonNohin.Size = new System.Drawing.Size(155, 16);
            this.radioButtonNohin.TabIndex = 0;
            this.radioButtonNohin.TabStop = true;
            this.radioButtonNohin.Text = "納品データ　（出力先固定）";
            this.radioButtonNohin.UseVisualStyleBackColor = true;
            // 
            // radioButtonBunseki
            // 
            this.radioButtonBunseki.AutoSize = true;
            this.radioButtonBunseki.Location = new System.Drawing.Point(35, 209);
            this.radioButtonBunseki.Name = "radioButtonBunseki";
            this.radioButtonBunseki.Size = new System.Drawing.Size(87, 16);
            this.radioButtonBunseki.TabIndex = 1;
            this.radioButtonBunseki.Text = "分析用データ";
            this.radioButtonBunseki.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Tomato;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(341, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonSubmitAllCheck);
            this.groupBox1.Controls.Add(this.checkBoxSubmitRER);
            this.groupBox1.Controls.Add(this.checkBoxSubmitHES);
            this.groupBox1.Controls.Add(this.checkBoxSubmitRESREX);
            this.groupBox1.Location = new System.Drawing.Point(59, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 143);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力データを選択";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(208, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "※ファイル名と同じシートが対象";
            // 
            // buttonSubmitAllCheck
            // 
            this.buttonSubmitAllCheck.Location = new System.Drawing.Point(29, 105);
            this.buttonSubmitAllCheck.Name = "buttonSubmitAllCheck";
            this.buttonSubmitAllCheck.Size = new System.Drawing.Size(49, 23);
            this.buttonSubmitAllCheck.TabIndex = 3;
            this.buttonSubmitAllCheck.Text = "全選択";
            this.buttonSubmitAllCheck.UseVisualStyleBackColor = true;
            this.buttonSubmitAllCheck.Click += new System.EventHandler(this.buttonSubmitAllCheck_Click);
            // 
            // checkBoxSubmitRER
            // 
            this.checkBoxSubmitRER.AutoSize = true;
            this.checkBoxSubmitRER.Checked = true;
            this.checkBoxSubmitRER.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitRER.Location = new System.Drawing.Point(29, 80);
            this.checkBoxSubmitRER.Name = "checkBoxSubmitRER";
            this.checkBoxSubmitRER.Size = new System.Drawing.Size(230, 16);
            this.checkBoxSubmitRER.TabIndex = 2;
            this.checkBoxSubmitRER.Text = "RER データ　（医科レセ全件データ）＋画像";
            this.checkBoxSubmitRER.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubmitHES
            // 
            this.checkBoxSubmitHES.AutoSize = true;
            this.checkBoxSubmitHES.Checked = true;
            this.checkBoxSubmitHES.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitHES.Location = new System.Drawing.Point(29, 58);
            this.checkBoxSubmitHES.Name = "checkBoxSubmitHES";
            this.checkBoxSubmitHES.Size = new System.Drawing.Size(182, 16);
            this.checkBoxSubmitHES.TabIndex = 1;
            this.checkBoxSubmitHES.Text = "HES データ　（返戻対象者一覧）";
            this.checkBoxSubmitHES.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubmitRESREX
            // 
            this.checkBoxSubmitRESREX.AutoSize = true;
            this.checkBoxSubmitRESREX.Checked = true;
            this.checkBoxSubmitRESREX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitRESREX.Location = new System.Drawing.Point(29, 25);
            this.checkBoxSubmitRESREX.Name = "checkBoxSubmitRESREX";
            this.checkBoxSubmitRESREX.Size = new System.Drawing.Size(238, 28);
            this.checkBoxSubmitRESREX.TabIndex = 0;
            this.checkBoxSubmitRESREX.Text = "RESデータ　（柔整鍼灸全件データ）＋画像\r\nREXデータ　（柔整鍼灸全件データ 大阪ver）";
            this.checkBoxSubmitRESREX.UseVisualStyleBackColor = true;
            // 
            // radioButtonMediMatching
            // 
            this.radioButtonMediMatching.AutoSize = true;
            this.radioButtonMediMatching.Location = new System.Drawing.Point(35, 244);
            this.radioButtonMediMatching.Name = "radioButtonMediMatching";
            this.radioButtonMediMatching.Size = new System.Drawing.Size(111, 16);
            this.radioButtonMediMatching.TabIndex = 4;
            this.radioButtonMediMatching.Text = "医科突合用データ";
            this.radioButtonMediMatching.UseVisualStyleBackColor = true;
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 322);
            this.Controls.Add(this.radioButtonMediMatching);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.radioButtonBunseki);
            this.Controls.Add(this.radioButtonNohin);
            this.Name = "ExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonNohin;
        private System.Windows.Forms.RadioButton radioButtonBunseki;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonSubmitAllCheck;
        private System.Windows.Forms.CheckBox checkBoxSubmitRER;
        private System.Windows.Forms.CheckBox checkBoxSubmitHES;
        private System.Windows.Forms.CheckBox checkBoxSubmitRESREX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonMediMatching;
    }
}