﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.OsakaGakko
{
    class ExportSubmitData
    {
        private Insurer ins;
        private string defaultFolderPath;
        private string folderPath;
        private bool resrex;
        private bool hes;
        private bool rer;

        private Pref pref;
        private int cym;

        private IWorkbook dataBK;
        private ISheet dataSH;

        private int hesCount;

        public ExportSubmitData(Insurer ins, string folderPath, int cym, bool resrex, bool hes, bool rer)
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            var fPath = folderPath;
            if (fPath.Contains("{ee}"))
            {
                var ee = month < 4 ? jYear - 1 : jYear;
                fPath = fPath.Replace("{ee}", ee.ToString("00"));
            }
            if (fPath.Contains("{eemm}"))
            {
                fPath = fPath.Replace("{eemm}", jYear.ToString("00") + month.ToString("00"));
            }
            var pid = Insurer.CurrrentInsurer.ViewIndex;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            var yyyyMM = $"{DateTimeEx.GetAdYearFromHs(jYear * 100 + month).ToString("00")}{month.ToString("00")}";
            //var yyyyMM = $"{DateTimeEx.GetAdYearFromHs(jYear).ToString("00")}{month.ToString("00")}";
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            this.folderPath = $"{fPath}\\{yyyyMM}";
            defaultFolderPath = fPath;

            this.ins = ins;
            this.resrex = resrex;
            this.hes = hes;
            this.rer = rer;

            this.pref = Pref.GetPref(Insurer.CurrrentInsurer.ViewIndex);
            this.cym = cym;
        }

        /// <summary>
        /// 選択項目を出力します。
        /// </summary>
        /// <returns></returns>
        public bool Exportes()
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            hesCount = 0;
            try
            {
                if (!fileExistCheck()) return false;

                if (resrex && !dataExistCheck_RESREX()) return false;
                if (hes && !dataExistCheckAndLoad_HES()) return false;
                if (rer && !dataExistCheck_RER()) return false;

                var agree = MessageBox.Show(
                    $"【{Insurer.CurrrentInsurer.InsurerName}】{jYear * 100 + month} 分\r\n" +
                    (resrex ? "　・RES ファイル＋画像\r\n" : "") +
                    (resrex ? "　・REX ファイル\r\n" : "") +
                    (hes ? $"　・HES ファイル（{hesCount}件）\r\n" : "") +
                    (rer ? "　・RER ファイル＋画像\r\n" : "") +
                    $"\r\n以上の出力を開始してもよろしいですか？\r\n\r\n\r\n保存先：\r\n{folderPath}",
                    "確認", MessageBoxButtons.OKCancel);
                if (agree != DialogResult.OK) return false;

                if (resrex && !exportRESREX())
                {
                    MessageBox.Show("RES, REXファイルの出力に失敗しました。", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                if (hes && !exportHES_Excel())
                {
                    MessageBox.Show("HESファイルの出力に失敗しました。", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                if (rer && !exportRER())
                {
                    MessageBox.Show("RERファイルの出力に失敗しました。", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                var rootFolderPath = System.IO.Path.GetDirectoryName(defaultFolderPath);    //１階層上がルートフォルダ（eemm）
                var excelFolderPath = $"{rootFolderPath}\\各種一覧表";
                if (!System.IO.Directory.Exists(excelFolderPath))
                {
                    System.IO.Directory.CreateDirectory(excelFolderPath);   //Excelファイル格納フォルダの作成
                }

                return true;
            }
            finally
            {
                if (dataBK != null)
                {
                    dataBK.Close();
                    dataBK = null;
                    dataSH = null;
                }
            }            
        }

        private bool fileExistCheck()
        {
            if (System.IO.Directory.Exists(folderPath))
            {
                foreach (var f in System.IO.Directory.GetFiles(folderPath))
                {
                    var fn = System.IO.Path.GetFileName(f);

                    if (resrex && fn.StartsWith("RES"))
                    {
                        MessageBox.Show($"RESファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if (resrex && fn.StartsWith("REX"))
                    {
                        MessageBox.Show($"REXファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if (hes && fn.StartsWith("HES"))
                    {
                        MessageBox.Show($"HESファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if (rer && fn.StartsWith("RER"))
                    {
                        MessageBox.Show($"RERファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }

            return true;            
        }

        /// <summary>
        /// RESデータと画像を出力します
        /// </summary>
        /// <returns></returns>
        private bool exportRESREX()
        {
            return SubmitData.Export(cym, folderPath);
            //return Export.DoExport(ins, cym, folderPath);
        }

        /// <summary>
        /// HESデータを出力します
        /// </summary>
        /// <returns></returns>
        private bool exportHES_Excel()
        {
            var wf = new WaitForm();
            try
            {
                wf.Max = dataSH.LastRowNum;
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.LogPrint("HESファイルを出力しています…");

                if (!System.IO.Directory.Exists(folderPath)) System.IO.Directory.CreateDirectory(folderPath);

                var outputHESPath = $"{folderPath}\\HES{DateTime.Today.ToString("yyMMdd")}.csv";
                using (var sw = new System.IO.StreamWriter(outputHESPath, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    var csv = new List<string>();

                    Func<ICell, string> getVal = cell =>
                    {
                        if (cell == null) return string.Empty;
                        if (cell.CellType == CellType.String) return cell.StringCellValue;
                        else if (cell.CellType == CellType.Numeric) return cell.NumericCellValue.ToString();
                        return string.Empty;
                    };
                    Func<string, string> toInsertVal = val =>
                    {
                        if (string.IsNullOrWhiteSpace(val)) return "";
                        return val;
                    };
                    Func<string, string, string, string> toInsertVals = (val1, val2, val3) =>
                    {
                        var result = "";
                        if (!string.IsNullOrWhiteSpace(val1)) result += val1;
                        if (!string.IsNullOrWhiteSpace(val2)) result += "：" + val2;
                        if (!string.IsNullOrWhiteSpace(val3)) result += "：" + val3;
                        return result;
                    };

                    for (var i = 1; i <= dataSH.LastRowNum; i++)//ヘッダは除く
                    {
                        var row = dataSH.GetRow(i);
                        if (row == null)
                        {
                            wf.InvokeValue++;
                            continue;//パス
                        }
                        var valEE = getVal(row.GetCell(3));
                        var valMM = getVal(row.GetCell(4));
                        var valBatch = getVal(row.GetCell(5));
                        var valNumbering = getVal(row.GetCell(6));
                        if(string.IsNullOrWhiteSpace(valEE)
                            && string.IsNullOrWhiteSpace(valMM)
                            && string.IsNullOrWhiteSpace(valBatch)
                            && string.IsNullOrWhiteSpace(valNumbering))
                        {
                            wf.InvokeValue++;
                            continue;//空白行はパス（書式だけ残ってる可能性大）
                        }
                        var cellHenrei1 = row.GetCell(25);
                        var valHenrei1 = "";
                        var valHenrei2 = "";
                        var valHenrei3 = "";
                        if (cellHenrei1.IsMergedCell)//結合セル
                        {
                            valHenrei1 = getVal(row.GetCell(25));
                        }
                        else
                        {
                            valHenrei1 = getVal(row.GetCell(25));
                            valHenrei2 = getVal(row.GetCell(26));
                            valHenrei3 = getVal(row.GetCell(27));
                        }

                        csv.Add($"4{valEE.PadLeft(2, '0')}{valMM.PadLeft(2, '0')}");//平成＝４
                        csv.Add(toInsertVal(valBatch));
                        csv.Add(toInsertVal(valNumbering.PadLeft(6, '0')));//6桁合わせ
                        csv.Add(toInsertVals(valHenrei1, valHenrei2, valHenrei3));

                        sw.WriteLine(string.Join(",", csv));
                        csv.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }

        /// <summary>
        /// RERデータと画像を出力します
        /// </summary>
        /// <returns></returns>
        private bool exportRER()
        {
            if (pref == null) return false;
            var dbname = "zenkoku_gakko";
            var currDBName = DB.GetMainDBName();
            var change = currDBName != dbname;
            try
            {
                if (change) DB.SetMainDBName(dbname);
                return ExportMediApp.DoExport(cym, pref, folderPath, true);
            }
            finally
            {
                if (change) DB.SetMainDBName(currDBName);
            }            
        }

        private bool dataExistCheck_RESREX()
        {
            var list = Scan.GetScanListYM(cym);
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("RES, REXファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool dataExistCheckAndLoad_HES()
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            int yyMM = jYear * 100 + month;

            //出力先から4階層戻る
            var hesPath = this.folderPath;
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            //識別子取得
            var tmps = FileUtility.GetDirectoryName(hesPath).Replace("支部", string.Empty).Split(' ');
            if (tmps.Count() != 3) return false;
            var identifier = $"{tmps[0]} {tmps[1]}{tmps[2]}";
            //合体
            hesPath += $"\\{identifier} 作業フォルダ\\{identifier} {yyMM}作業フォルダ\\提出用\\{pref.ID.ToString("00")}{pref.Name2}_{yyMM}返戻対象者一覧表.xlsx";

            if (!System.IO.File.Exists(hesPath))
            {
                MessageBox.Show($"HESファイルの作成元Excelファイルがありません。\r\n\r\n{hesPath}", "エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!readExcel(hesPath))
            {
                MessageBox.Show("HESファイルの作成元Excelファイルの取得に失敗しました。\r\n" +
                    "該当のExcelファイルを開いている場合は閉じてください。\r\n" +
                    "また対象となるシート名はファイル名と同じである必要があります。", 
                    "エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dataSH.LastRowNum == 0)//ヘッダのみ
            {
                MessageBox.Show("HESファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            //件数の取得
            Func<ICell, string> getVal = cell =>
            {
                if (cell == null) return string.Empty;
                if (cell.CellType == CellType.String) return cell.StringCellValue;
                else if (cell.CellType == CellType.Numeric) return cell.NumericCellValue.ToString();
                return string.Empty;
            };
            var count = 0;
            for (var i = 1; i <= dataSH.LastRowNum; i++)//ヘッダは除く
            {
                var row = dataSH.GetRow(i);
                if (row == null)
                {
                    continue;//パス
                }
                var valEE = getVal(row.GetCell(3));
                var valMM = getVal(row.GetCell(4));
                var valBatch = getVal(row.GetCell(5));
                var valNumbering = getVal(row.GetCell(6));
                if (string.IsNullOrWhiteSpace(valEE)
                    && string.IsNullOrWhiteSpace(valMM)
                    && string.IsNullOrWhiteSpace(valBatch)
                    && string.IsNullOrWhiteSpace(valNumbering))
                {
                    continue;//空白行はパス（書式だけ残ってる可能性大）
                }
                count++;
            }
            hesCount = count;
            return true;
        }

        private bool dataExistCheck_RER()
        {
            var list = MediApp.GetMediAppsByPIDIYM(cym, pref.ID);
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("RERファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// エクセルファイルを取得します。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool readExcel(string path)
        {
            try
            {
                //ブック読み込み
                dataBK = WorkbookFactory.Create(path);
                //シート取得（ファイル名と同じシート名のもの）
                dataSH = dataBK.GetSheet(System.IO.Path.GetFileNameWithoutExtension(path));
                if (dataSH == null) return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
