﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.OsakaGakko
{
    public partial class ExportForm : Form
    {
        Insurer ins;
        int cym;
        string exportFolderPath;
        public ExportForm(Insurer ins, int cym)
        {
            this.ins = ins;
            this.cym = cym;
            exportFolderPath = ins.OutputPath;
            InitializeComponent();

            this.Text = ins.InsurerName + " " + cym.ToString("0000年00月");
        }

        private void buttonSubmitAllCheck_Click(object sender, EventArgs e)
        {
            checkBoxSubmitRESREX.Checked = true;
            checkBoxSubmitHES.Checked = true;
            checkBoxSubmitRER.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButtonNohin.Checked)
            {
                if(!checkBoxSubmitRESREX.Checked && !checkBoxSubmitHES.Checked && !checkBoxSubmitRER.Checked)
                {
                    MessageBox.Show("出力するデータを選択してください。", "エラー");
                    return;
                }

                var esd = new ExportSubmitData(ins, exportFolderPath, cym,
                    checkBoxSubmitRESREX.Checked, checkBoxSubmitHES.Checked, checkBoxSubmitRER.Checked);
                var result = esd.Exportes();
                if (result)
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
            }
            else if (radioButtonBunseki.Checked)
            {
                if (Export.AnalysisExport(Insurer.CurrrentInsurer, cym))
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
                else
                {
                    MessageBox.Show("エラーが発生しました。", "失敗");
                }
            }
            else if (radioButtonMediMatching.Checked)
            {
                if (Export.MediMatchingExportWithRESAndImage(Insurer.CurrrentInsurer, cym))
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
                else
                {
                    MessageBox.Show("エラーが発生しました。", "失敗");
                }
            }
        }
    }
}
