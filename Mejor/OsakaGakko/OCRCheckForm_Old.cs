﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;


namespace Mejor.OsakaGakko
{
    public partial class OCRCheckForm_Old : Form
    {
        private Dictionary<int, App> dic;
        private BindingSource bs;

        private ScanGroup sg;
        int currentAID = -1;
        private bool firstTime;

        //愛知都市共済用
        private int cYear;
        private int cMonth;
        const string iNumber = "34270017";
        const string iPref = "27";


        // --------------------------------------------------------------------------------
        //
        // クラス定義部
        //
        // --------------------------------------------------------------------------------

        public enum a2tp { 未チェック = 0, First = 1, Other = 2 };

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHosCode = new Point(500, 0);
        Point posHnum = new Point(550, 60);
        Point posPref = new Point(550, 60);
        Point posFamily = new Point(550, 60);
        Point posSex = new Point(150, 200);
        Point posBirthday = new Point(250, 200);
        Point posTotal = new Point(700, 800);
        Point posDays = new Point(770, 325);
        Point posRedAN = new Point(420, 490);
        Point posRedKN = new Point(100, 1060);

        // --------------------------------------------------------------------------------
        //
        // メイン部
        //
        // --------------------------------------------------------------------------------

        public OCRCheckForm_Old(ScanGroup sGroup, bool firstTime)
        {
            InitializeComponent();

            this.sg = sGroup;
            cYear = sg.cyear;
            cMonth = sg.cmonth;
            this.firstTime = firstTime;

            //キー入力を監視
            this.KeyPreview = true;

            //請求年月で検索
            //var list = App.GetApplications(cyear, cmonth);

            //GIDで検索
            int gid = sg.GroupID;
            var list = App.GetAppsGID(gid);

            //Dictionaryを作成
            dic = new Dictionary<int, App>();
            foreach (var item in list) dic.Add(item.Aid, item);

            //データリストを作成
            bs = new BindingSource();

            bs.DataSource = list;
            dataGridViewPlist.DataSource = bs;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            textDispSID.Text = sg.ScanID.ToString();
            textDispGID.Text = sg.GroupID.ToString();
            textDispY.Text = sg.cyear.ToString();
            textDispM.Text = sg.cmonth.ToString();
            textDispNote1.Text = sg.note1.ToString();

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
            textBoxInum.Enabled = false;
        }

        private void regist()
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (!appDBupdate(ri)) return;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri + 1];
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        //EnterキーでTABキーの動作をさせる
        private void FormOCRCheck_OsakaGakko_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        // --------------------------------------------------------------------------------
        //
        // 入力チェック部(文字種チェック→値チェック→一回目のみOCR結果との比較)
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 指定されたエラーに合わせて、テキストボックスの表示を調整します
        /// </summary>
        /// <param name="t"></param>
        /// <param name="isError"></param>
        /// <returns>エラー処理が必要か</returns>
        private bool setStatus(TextBox t, bool isError)
        {
            if (!t.Enabled) return false;
            if (isError)
            {
                t.BackColor = Color.Pink;
                return true;
            }
            else
            {
                t.BackColor = SystemColors.Info;
                return false;
            }
        }


        /// <summary>
        /// 入力チェック：総括表
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private App CheckSokatsu(int rowIndex)
        {
            var aid = (int)dataGridViewPlist[0, rowIndex].Value;
            var r = dic[aid];
            bool err = false;

            //総括表で必要なのは振込先口座番号と請求金額

            //A1.総括表では合計金額欄に口座番号を入力
            int anum;
            if (!int.TryParse(textBoxTotal.Text, out anum)) err |= setStatus(textBoxTotal, true);
            else if (textBoxTotal.TextLength != 7) err |= setStatus(textBoxTotal, true);
            else setStatus(textBoxTotal, false);

            //A2.請求金額
            int charge;
            if (!int.TryParse(textBoxCharge.Text, out charge)) setStatus(textBoxCharge, true);
            else if (charge < 100) setStatus(textBoxCharge, true);
            else setStatus(textBoxCharge, false);

            //チェックでエラーが検出されたらnullを返す
            if (err) return null;

            //A3.赤バッジの口座番号と比較。
            //前が赤バッチか確認し、赤であれば口座番号比較
            var bankNo = textBoxTotal.Text.PadLeft(7, '0');
            if (rowIndex > 0)
            {
                var preID = (int)dataGridViewPlist[0, rowIndex - 1].Value;
                var preApp = dic[preID];
                if (preApp.MediYear == -2 && preApp.AccountNumber != bankNo)
                {
                    var res = MessageBox.Show("直前の赤バッチと口座番号が一致しません。" +
                        "このまま登録してもよろしいですか？", "口座番号確認",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);
                    if (res != System.Windows.Forms.DialogResult.OK)
                    {
                        setStatus(textBoxTotal, true);
                        return null;
                    }
                }
            }

            r.AccountNumber = bankNo;
            r.Charge = charge;
            r.MediYear = -1;

            r.MediMonth = 0;
            r.HihoNum = "";
            r.HihoPref = 0;
            r.Family = 0;
            r.Sex = 0;
            r.Birthday = DateTime.MinValue;
            r.Total = 0;
            r.CountedDays = 0;
            r.Numbering = "";
            r.AppType = APP_TYPE.NULL;

            return r;
        }

        /// <summary>
        /// 入力チェック：赤バッジ
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private App CheckRed(int rowIndex)
        {
            var aid = (int)dataGridViewPlist[0, rowIndex].Value;
            var r = dic[aid];
            bool err = false;

            //赤バッジで必要なのは振込先口座番号と協議会番号

            //B1.赤バッジでは合計金額欄に口座番号を入力
            int anum;
            if (!int.TryParse(textBoxTotal.Text, out anum)) err |= setStatus(textBoxTotal, true);
            else if (textBoxTotal.TextLength != 7) err |= setStatus(textBoxTotal, true);
            else setStatus(textBoxTotal, false);

            //B2.赤バッジでは保険者番号欄に協議会番号を入力
            int inum;
            if (!int.TryParse(textBoxInum.Text, out inum)) err |= setStatus(textBoxInum, true);
            else if (textBoxInum.TextLength < 1 || textBoxInum.TextLength > 6) err |= setStatus(textBoxInum, true);
            else setStatus(textBoxInum, false);

            //チェックでエラーが検出されたらnullを返す
            if (err) return null;

            r.AccountNumber = textBoxTotal.Text;
            r.InsNum = textBoxInum.Text;
            r.MediYear = -2;

            r.MediMonth = 0;
            r.HihoNum = "";
            r.HihoPref = 0;
            r.Family = 0;
            r.Sex = 0;
            r.Birthday = DateTime.MinValue;
            r.Total = 0;
            r.Charge = 0;
            r.CountedDays = 0;
            r.Numbering = "";
            r.AppType = APP_TYPE.NULL;

            return r;
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private App CheckApp(int rowIndex)
        {
            var aid = (int)dataGridViewPlist[0, rowIndex].Value;
            var r = dic[aid];
            bool err = false;

            //01.年のチェック
            int year;
            if (!int.TryParse(textBoxY.Text, out year)) err |= setStatus(textBoxY, true);
            else if (textBoxY.Text.Length != 2 || year < r.ChargeYear - 5) err |= setStatus(textBoxY, true);
            else setStatus(textBoxY, false);

            //02.月のチェック
            int month;
            if (!int.TryParse(textBoxM.Text, out month)) err |= setStatus(textBoxM, true);
            else if (month < 0 || month > 12) err |= setStatus(textBoxM, true);
            else setStatus(textBoxM, false);

            //03.保険者番号のチェック
            int inum;
            if (!int.TryParse(textBoxInum.Text, out inum)) err |= setStatus(textBoxInum, true);
            else if (textBoxInum.TextLength != 8) err |= setStatus(textBoxInum, true);
            else setStatus(textBoxInum, false);

            //04.被保険者番号のチェック
            long hnumTemp;
            var hnum = textBoxHnumM.Text.Trim() + textBoxHnum.Text.Trim();
            if (textBoxHnumM.Text.Trim().Length != 4 || textBoxHnum.Text.Trim().Length != 6)
            {
                err |= setStatus(textBoxHnumM, true);
                err |= setStatus(textBoxHnum, true);
            }
            else if (!long.TryParse(hnum, out hnumTemp))
            {
                err |= setStatus(textBoxHnumM, true);
                err |= setStatus(textBoxHnum, true);
            }
            else
            {
                setStatus(textBoxHnumM, false);
                setStatus(textBoxHnum, false);
            }

            //05.都道府県番号のチェック
            int pref;
            if (!int.TryParse(textBoxPref.Text, out pref)) err |= setStatus(textBoxPref, true);
            else if (pref < 1 || pref > 50) err |= setStatus(textBoxPref, true);
            else setStatus(textBoxPref, false);

            //06.本家区分のチェック
            int family;
            if (!int.TryParse(textBoxFamily.Text, out family)) err |= setStatus(textBoxFamily, true);
            else
            {
                if (family % 2 == 1 || textBoxFamily.TextLength > 1) err |= setStatus(textBoxFamily, true);
                else setStatus(textBoxFamily, false);
            }

            //07.性別のチェック
            int sex;
            if (!int.TryParse(textBoxSex.Text, out sex)) err |= setStatus(textBoxSex, true);
            else
            {
                if (sex < 1 || sex > 2) err |= setStatus(textBoxSex, true);
                else setStatus(textBoxSex, false);
            }

            //08.生年月日のチェック
            int bG;
            if (!int.TryParse(textBoxBirthday.Text, out bG)) err |= setStatus(textBoxBirthday, true);
            else
            {
                if (bG < 1 || bG > 4) err |= setStatus(textBoxBirthday, true);
                else setStatus(textBoxBirthday, false);
            }

            int bY;
            if (!int.TryParse(textBoxBY.Text, out bY)) err |= setStatus(textBoxBY, true);
            else
            {
                if (bY < 1 || bY > 65) err |= setStatus(textBoxBY, true);
                else setStatus(textBoxBY, false);
            }

            int bM;
            if (!int.TryParse(textBoxBM.Text, out bM)) err |= setStatus(textBoxBM, true);
            else
            {
                if (bM < 1 || bM > 12) err |= setStatus(textBoxBM, true);
                else setStatus(textBoxBM, false);
            }

            int bD;
            if (!int.TryParse(textBoxBD.Text, out bD)) err |= setStatus(textBoxBD, true);
            else
            {
                if (bD < 1 || bD > 31) err |= setStatus(textBoxBD, true);
                else setStatus(textBoxBD, false);
            }

            //09.合計金額のチェック
            int total;
            if (!int.TryParse(textBoxTotal.Text, out total)) err |= setStatus(textBoxTotal, true);
            else if (total < 100 || total > 100000) err |= setStatus(textBoxTotal, true);
            else setStatus(textBoxTotal, false);
            
            //10.請求金額のチェック
            int charge;
            if (!int.TryParse(textBoxCharge.Text, out charge)) err |= setStatus(textBoxCharge, true);
            else if (charge > total) err |= setStatus(textBoxCharge, true);
            else setStatus(textBoxCharge, false);

            //10.1 合計金額：請求金額：本家区分のトリプルチェック

            bool payError = false;
            if ((family == 2 || family == 6) && (int)(total * 70 /100) != charge)
                payError=true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //11.診療日数のチェック
            int days;
            if (!int.TryParse(textBoxDays.Text, out days)) err |= setStatus(textBoxDays, true);
            else if (days < 1 || days > 31) err |= setStatus(textBoxDays, true);
            else setStatus(textBoxDays, false);

            //12.ナンバリングのチェック
            //大阪学校共済のナンバリングは6桁以内
            int numbering;
            if (!int.TryParse(textBoxNumbering.Text, out numbering)) err |= setStatus(textBoxNumbering, true);
            else if (textBoxNumbering.TextLength < 1 || textBoxNumbering.TextLength > 6) err |= setStatus(textBoxNumbering, true);
            else setStatus(textBoxNumbering, false);

            //チェックでエラーが検出されたらnullを返す
            if (err)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return null;
            }

            //被保険者番号のチェック
            if (textBoxHnumM.Text[3] != '0')
            {
                textBoxHnumM.BackColor = Color.GreenYellow;
                textBoxHnum.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("被保険者番号に入力ミスがある可能性があります。"+
                    "このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return null;
            }
            if (textBoxHnumM.Text.Trim().Remove(3) != textBoxHnum.Text.Trim().Remove(3))
            {
                textBoxHnumM.BackColor = Color.GreenYellow;
                textBoxHnum.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("被保険者番号に入力ミスがある可能性があります。"+
                    "このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return null;
            }

            //金額でのエラーがあればいったん登録中断
            if (payError)
            {
                textBoxFamily.BackColor = Color.GreenYellow;
                textBoxTotal.BackColor = Color.GreenYellow;
                textBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return null;
            }

            //12.1 大阪学校共済用、ナンバリング抜けチェック
            int preNum = 0;
            for (int i = rowIndex - 1; i >= 0; i--)
            {
                var preID = (int)dataGridViewPlist[0, i].Value;
                var preApp = dic[preID];
                if (preApp.MediYear > 0)
                {
                    if (!int.TryParse(preApp.Numbering, out preNum)) continue;
                    break;
                }
            }

            if (preNum != 0 && preNum + 1 != numbering)
            {
                textBoxNumbering.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("直前のナンバリングと連番になっていません。" +
                    "このまま登録してもよろしいですか？", "ナンバリング確認",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return null;
            }

            //12.2 大阪学校共済用、請求金額の合計と総括表記載の金額との比較
            //2015.6.17 松本 総括表との比較削除

            //値の反映
            r.MediYear = year;
            r.MediMonth = month;
            r.HihoNum = hnum;
            r.InsNum = textBoxInum.Text; //textBoxInum.Text.Trim();
            r.HihoPref = pref;
            r.Family = family;
            r.Sex = sex;
            r.AppType = APP_TYPE.柔整;

            string gengo = "";
            if (textBoxBirthday.Text == "1") { gengo = "明治"; }
            else if (textBoxBirthday.Text == "2") { gengo = "大正"; }
            else if (textBoxBirthday.Text == "3") { gengo = "昭和"; }
            else if (textBoxBirthday.Text == "4") { gengo = "平成"; }
            gengo = gengo + textBoxBY.Text; //gengo + string.Format("{0:D2}", textBoxBY.Text);
            gengo = DateTimeEx.GetAdYear(gengo).ToString() + "/" + textBoxBM.Text + "/" + textBoxBD.Text;
            r.Birthday = DateTime.Parse(gengo);

            r.Total = total;
            r.Charge = charge;
            r.CountedDays = days;
            r.Numbering = numbering.ToString("000000");

            return r;
        }



        // --------------------------------------------------------------------------------
        //
        // Application操作部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 現在の行のApplicationを表示します
        /// </summary>
        private void currentRowAppRead()
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri < 0) return;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            //次のApplicationを表示
            displayDataReflesh(r);

            //textBoxYにフォーカスを移動
            focusBack(false);
        }


        /// <summary>
        /// Applicationの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(int rowIndex)
        {
            if (rowIndex == -1) return false;
            var aid = (int)dataGridViewPlist["Aid", rowIndex].Value;

            if (currentAID != aid)
            {
                MessageBox.Show("更新しようとしているデータと現在表示中のデータが一致しません。"+
                    "ご迷惑をおかけしますが、システムエラーを回避するため、この画面を閉じます。"+
                    "再度このグループを開いて作業を行ってください。", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return false;
            }

            App updateApp;
            //ベリファイチェック時
            if (!firstTime)
            {
                updateApp = dic[aid];
                if (updateApp.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

                if (!checkVerify())
                {
                    MessageBox.Show("データが一致していません。" +
                        "赤で示されたデータをご確認ください。登録できません。",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    focusBack(true);
                    return false;
                }
            }

            if (textBoxY.Text == "//")
            {
                //総括表の場合
                updateApp = CheckSokatsu(rowIndex);
                if (updateApp == null)
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (textBoxY.Text == "**")
            {
                //赤バッジの場合
                updateApp = CheckRed(rowIndex);
                if (updateApp == null)
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (textBoxY.Text == "--")
            {
                //続紙の場合
                var zoku = (int)dataGridViewPlist[0, rowIndex].Value;
                updateApp = dic[zoku];
                updateApp.MediYear = -3;
            }
            else if (textBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                var white = (int)dataGridViewPlist[0, rowIndex].Value;
                updateApp = dic[white];
                updateApp.MediYear = -4;
            }
            else
            {
                //データベース登録用のAppインスタンスを作成
                updateApp = CheckApp(rowIndex);
                if (updateApp == null)
                {
                    focusBack(true);
                    return false;
                }
            }

            //データベースへ反映
            if (firstTime)
            {
                return updateApp.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput);
            }
            else
            {
                return updateApp.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.SecondInput);
            }
        }


        /// <summary>
        /// 次のApplicationを表示します
        /// </summary>
        /// <param name="r"></param>
        private void displayDataReflesh(App r)
        {
            currentAID = r.Aid;

            var clearTB = new Action<TextBox>((tb) =>
            {
                tb.Clear();
                tb.BackColor = SystemColors.Info;
            });

            clearTB(textBoxY);
            clearTB(textBoxM);
            clearTB(textBoxInum);
            clearTB(textBoxHnumM);
            clearTB(textBoxHnum);
            clearTB(textBoxPref);
            clearTB(textBoxFamily);
            clearTB(textBoxSex);
            clearTB(textBoxBirthday);
            clearTB(textBoxBY);
            clearTB(textBoxBM);
            clearTB(textBoxBD);
            clearTB(textBoxTotal);
            clearTB(textBoxCharge);
            clearTB(textBoxDays);
            clearTB(textBoxNumbering);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(r.Ufirst) +
                "  2:" + User.GetUserName(r.Usecond);

            if (!firstTime)
            {
                //ベリファイ入力時
                var cai = new CheckOsakaGakkoInputAall(dic[r.Aid], labelInfo);
                setVerify(cai);
            }
            else
            {
                labelInfo.Text = "";
                //App_Flagのチェック
                if (r.StatusFlagCheck(StatusFlag.入力済))
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    displayDataRefleshFromRcd(r);
                }
            }

            //画像の表示
            setImage(r);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                //メイン画像の表示
                var img = System.Drawing.Image.FromFile(fn);

                labelImageName.Text = fn + " )";

                //中さん方式表示
                userControlImage1.SetPictureFile(fn);
                userControlImage1.SetPictureBoxFill();

                //通常表示
                float ratio2 = 0.4f;// 0.3f;
                var act = new Action<PictureBox>((pb) =>
                {
                    pb.Size = new Size((int)(img.Width * ratio2), (int)(img.Height * ratio2));
                    pb.SizeMode = PictureBoxSizeMode.Zoom;
                    pb.Image = img;
                });
                act(pictureBoxIN);
                panelIN.AutoScrollPosition = 
                    textBoxY.Text != "**" ? posYM : posRedKN; //new Point(550, 60);
                act(pictureBoxTotal);
                panelTotal.AutoScrollPosition = 
                    textBoxY.Text != "**" ? posTotal : posRedAN; //new Point(700, 800);
                act(pictureBoxCharge);
                panelCharge.AutoScrollPosition = posDays; //new Point(770, 325);
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="a"></param>
        private void displayDataRefleshFromRcd(App a)
        {
            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(a.Ufirst) +
                "  2:" + User.GetUserName(a.Usecond);

            //OCRチェックが済んだ画像の場合
            if (a.MediYear == -1)
            {
                //総括表
                textBoxY.Text = "//";
                textBoxTotal.Text = a.AccountNumber;
                textBoxCharge.Text = a.Charge.ToString();
            }
            else if (a.MediYear == -2)
            {
                //赤バッジ
                textBoxY.Text = "**";
                textBoxInum.Text = a.InsNum;
                textBoxTotal.Text = a.AccountNumber;
            }
            else if (a.MediYear == -3)
            {
                //続紙
                textBoxY.Text = "--";
            }
            else if (a.MediYear == -4)
            {
                //白バッジ、その他
                textBoxY.Text = "++";
            }
            else
            {
                //申請書
                textBoxY.Text = a.MediYear.ToString();
                textBoxM.Text = a.MediMonth.ToString();
                textBoxInum.Text = a.InsNum;
                
                if (a.HihoNum.Length > 4)
                {
                    textBoxHnumM.Text = a.HihoNum.Remove(4);
                    textBoxHnum.Text = a.HihoNum.Substring(4);
                }
                else
                {
                    textBoxHnumM.Text = a.HihoNum;
                    textBoxHnum.Clear();
                }

                textBoxPref.Text = a.HihoPref.ToString();
                textBoxFamily.Text = a.Family.ToString();
                textBoxSex.Text = a.Sex.ToString();
                int jj = DateTimeEx.GetEraNumber(a.Birthday);
                textBoxBirthday.Text = jj.ToString();
                textBoxBY.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM.Text = a.Birthday.Month.ToString();
                textBoxBD.Text = a.Birthday.Day.ToString();
                textBoxTotal.Text = a.Total.ToString();
                textBoxCharge.Text = a.Charge.ToString();
                textBoxDays.Text = a.CountedDays.ToString();
                textBoxNumbering.Text = a.Numbering;
            }
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            if (textBoxY.TextLength != 2) return;

            var setCanInput = new Action<TextBox, bool>((t, b) =>
            {
                t.ReadOnly = !b;
                t.TabStop = b;
                if (!b)
                    t.BackColor = SystemColors.Menu;
                else if (!t.Enabled)
                    t.BackColor = SystemColors.Control;
                else
                    t.BackColor = SystemColors.Info;
            });

            if (textBoxY.Text == "//")
            {
                //総括表の場合
                textBoxInum.Enabled = false;
                setCanInput(textBoxM, false);
                setCanInput(textBoxInum, false);
                labelInum.ForeColor = SystemColors.ControlText;
                if (textBoxInum.Text == iNumber) textBoxInum.Clear();
                setCanInput(textBoxHnumM, false);
                setCanInput(textBoxHnum, false);
                setCanInput(textBoxPref, false);
                setCanInput(textBoxSex, false);
                setCanInput(textBoxBirthday, false);
                setCanInput(textBoxBY, false);
                setCanInput(textBoxBM, false);
                setCanInput(textBoxBD, false);
                setCanInput(textBoxFamily, false);
                setCanInput(textBoxTotal, true);
                labelTotal.Text = "振込先口座番号";
                labelTotal.ForeColor = SystemColors.MenuHighlight;
                setCanInput(textBoxCharge, true);
                labelCharge.ForeColor = SystemColors.MenuHighlight;
                setCanInput(textBoxNumbering, false);
                setCanInput(textBoxDays, false);
            }
            else if (textBoxY.Text == "**")
            {
                //赤バッジの場合
                textBoxInum.Enabled = true;
                setCanInput(textBoxM, false);
                setCanInput(textBoxInum, true);
                labelInum.Text = "協議会\r\n番号";
                labelInum.ForeColor = SystemColors.MenuHighlight;
                if (textBoxInum.Text == iNumber) textBoxInum.Clear();
                setCanInput(textBoxHnumM, false);
                setCanInput(textBoxHnum, false);
                setCanInput(textBoxPref, false);
                setCanInput(textBoxSex, false);
                setCanInput(textBoxBirthday, false);
                setCanInput(textBoxBY, false);
                setCanInput(textBoxBM, false);
                setCanInput(textBoxBD, false);
                setCanInput(textBoxFamily, false);
                setCanInput(textBoxTotal, true);
                labelTotal.Text = "振込先口座番号";
                labelTotal.ForeColor = SystemColors.MenuHighlight;
                setCanInput(textBoxCharge, false);
                labelCharge.ForeColor = SystemColors.ControlText;
                setCanInput(textBoxNumbering, false);
                setCanInput(textBoxDays, false);

                panelIN.AutoScrollPosition = posRedKN;
                panelTotal.AutoScrollPosition = posRedAN;
            }
            else if (textBoxY.Text == "--" || textBoxY.Text == "++")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                textBoxInum.Enabled = false;
                setCanInput(textBoxM, false);
                setCanInput(textBoxInum, false);
                labelInum.Text = "保険者\r\n番号";
                labelInum.ForeColor = SystemColors.ControlText;
                if (textBoxInum.Text == iNumber) textBoxInum.Clear();
                setCanInput(textBoxHnumM, false);
                setCanInput(textBoxHnum, false);
                setCanInput(textBoxPref, false);
                setCanInput(textBoxSex, false);
                setCanInput(textBoxBirthday, false);
                setCanInput(textBoxBY, false);
                setCanInput(textBoxBM, false);
                setCanInput(textBoxBD, false);
                setCanInput(textBoxFamily, false);
                setCanInput(textBoxTotal, false);
                labelTotal.Text = "合計金額";
                labelTotal.ForeColor = SystemColors.ControlText;
                setCanInput(textBoxCharge, false);
                labelCharge.ForeColor = SystemColors.ControlText;
                setCanInput(textBoxNumbering, false);
                setCanInput(textBoxDays, false);
            }
            else
            {
                //申請書の場合
                textBoxInum.Enabled = false;
                setCanInput(textBoxM, true);
                setCanInput(textBoxInum, true);
                labelInum.Text = "保険者\r\n番号";
                labelInum.ForeColor = SystemColors.ControlText;
                textBoxInum.Text = iNumber;
                setCanInput(textBoxHnumM, true);
                setCanInput(textBoxHnum, true);
                setCanInput(textBoxPref, true);
                setCanInput(textBoxSex, true);
                setCanInput(textBoxBirthday, true);
                setCanInput(textBoxBY, true);
                setCanInput(textBoxBM, true);
                setCanInput(textBoxBD, true);
                setCanInput(textBoxFamily, true);
                setCanInput(textBoxTotal, true);
                labelTotal.Text = "合計金額";
                labelTotal.ForeColor = SystemColors.ControlText;
                setCanInput(textBoxCharge, true);
                labelCharge.ForeColor = SystemColors.ControlText;
                setCanInput(textBoxNumbering, true);
                setCanInput(textBoxDays, true);
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            var apl = imageDispose();
            userControlImage1.ImageRotate(true);
            setImage(apl);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            var apl = imageDispose();
            userControlImage1.ImageRotate(false);
            setImage(apl);
        }

        private void buttonImageInsert_Click(object sender, EventArgs e)
        {
            //var apl = imageDispose();
            //AppEdit.afterInsert(apl.Aid, dic);

            ////GIDで検索
            //var list = App.GetAppsGID(apl.GroupID);

            ////Dictionaryを更新
            //dic = new Dictionary<int, App>();
            //foreach (var item in list)
            //{
            //    dic.Add(item.Aid, item);
            //}

            ////データリストを更新
            //bs = new BindingSource();
            //bs.DataSource = list;
            //dataGridViewPlist.DataSource = bs;

            //setImage(apl);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var apl = imageDispose();
            string fn = apl.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(apl);
        }

        private App imageDispose()
        {
            if (pictureBoxIN.Image != null) pictureBoxIN.Image.Dispose();
            if (pictureBoxTotal.Image != null) pictureBoxTotal.Image.Dispose();
            if (pictureBoxCharge.Image != null) pictureBoxCharge.Image.Dispose();

            //aIdで一意のAppインスタンスを選択
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            return r;
        }

        private void textBoxY_Enter(object sender, EventArgs e)
        {
            if (textBoxY.Text == "**")
                panelIN.AutoScrollPosition = posRedKN;
            else
                panelIN.AutoScrollPosition = posYM;
            //ここのせいで赤バッジ時、panelINがうまく移動しないかも
        }

        private void textBoxM_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posYM;//new Point(30, 30);
        }

        private void textBoxNumbering_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posHosCode;//new Point(600, 150);
        }

        private void textBoxInum_Enter(object sender, EventArgs e)
        {
            if (textBoxY.Text == "**")
                panelIN.AutoScrollPosition = posRedKN;
            else
                panelIN.AutoScrollPosition = posHnum;//new Point(550, 60);
        }

        private void textBoxHnum_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posHnum;//new Point(550, 60);
        }

        private void textBoxFamily_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posFamily;//new Point(550, 60);
        }

        private void textBoxPref_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posPref;//new Point(550, 60);
        }

        private void textBoxSex_Enter(object sender, EventArgs e)
        {
            panelIN.AutoScrollPosition = posSex;//new Point(200, 200);
        }

        private void textBoxTotal_Enter(object sender, EventArgs e)
        {
            if (textBoxY.Text == "**")
                panelTotal.AutoScrollPosition = posRedAN;
            else
                panelTotal.AutoScrollPosition = posTotal;
        }
        
        private void focusBack(bool infoColorFocus)
        {
            Control[] controls = { textBoxY, textBoxM, textBoxPref, textBoxNumbering, 
                    textBoxInum, textBoxHnumM, textBoxHnum, textBoxFamily, textBoxSex, 
                    textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, textBoxTotal,
                    textBoxCharge, textBoxDays};

            if (infoColorFocus)
            {
                foreach (var item in controls)
                {
                    if (!item.Enabled) continue;
                    if (item.BackColor == SystemColors.Info) continue;
                    item.Focus();
                    return;
                }
            }

            foreach (var item in controls)
            {
                if (!item.Enabled) continue;
                item.Focus();
                return;
            }

            buttonRegist.Focus();
        }

        private void panelIN_Scroll(object sender, ScrollEventArgs e)
        {
            var pos = new Point(-panelIN.AutoScrollPosition.X, -panelIN.AutoScrollPosition.Y);

            if (textBoxY.Text == "**")
            {
                posRedKN = pos;
                return;
            }

            if (textBoxY.Focused == true) posYM = pos;
            else if (textBoxM.Focused == true) posYM = pos;
            else if (textBoxNumbering.Focused == true) posHosCode = pos;
            else if (textBoxHnumM.Focused == true) posHnum = pos;
            else if (textBoxHnum.Focused == true) posHnum = pos;
            else if (textBoxInum.Focused == true) posHnum = pos;
            else if (textBoxPref.Focused == true) posPref = pos;
            else if (textBoxFamily.Focused == true) posFamily = pos;
            else if (textBoxSex.Focused == true) posSex = pos;
            else if (textBoxBirthday.Focused == true) posBirthday = pos;
            else if (textBoxBY.Focused == true) posBirthday = pos;
            else if (textBoxBM.Focused == true) posBirthday = pos;
            else if (textBoxBD.Focused == true) posBirthday = pos;
        }

        private void panelTotal_Scroll(object sender, ScrollEventArgs e)
        {
            var pos = new Point(-panelTotal.AutoScrollPosition.X, -panelTotal.AutoScrollPosition.Y);

            if (textBoxY.Text == "**")
            {
                posRedAN = pos;
                return;
            }
            else
            {
                posTotal = pos;
                return;
            }
        }

        private void panelCharge_Scroll(object sender, ScrollEventArgs e)
        {
            posDays = new Point(-panelCharge.AutoScrollPosition.X, -panelCharge.AutoScrollPosition.Y);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(CheckOsakaGakkoInputAall ca)
        {
            //まず、すべて非表示
            var clear = new Action<TextBox>(t =>
                {
                    t.Visible = false;
                    t.Clear();
                });
            clear(textBoxY2);
            clear(textBoxM2);
            clear(textBoxPref2);
            clear(textBoxNumbering2);
            clear(textBoxInum2);
            clear(textBoxHnumM2);
            clear(textBoxHnum2);
            clear(textBoxFamily2);
            clear(textBoxSex2);
            clear(textBoxBirthday2);
            clear(textBoxBY2);
            clear(textBoxBM2);
            clear(textBoxBD2);
            clear(textBoxTotal2);
            clear(textBoxCharge2);
            clear(textBoxDays2);

            var app = ca.app;

            //1回目入力の値を挿入
            if (app.MediYear == -1)
            {
                textBoxY2.Text = "//";
                textBoxTotal2.Text = app.AccountNumber;
                textBoxCharge2.Text = app.Charge.ToString();
            }
            else if (app.MediYear == -2)
            {
                textBoxY2.Text = "**";
                textBoxInum2.Text = app.InsNum;
                textBoxTotal2.Text = app.AccountNumber;
            }
            else if (app.MediYear == -3)
            {
                textBoxY2.Text = "--";
            }
            else if (app.MediYear == -4)
            {
                textBoxY2.Text = "++";
            }
            else
            {
                textBoxY2.Text = app.MediYear.ToString();
                textBoxM2.Text = app.MediMonth.ToString();
                textBoxPref2.Text = app.HihoPref.ToString();
                textBoxNumbering2.Text = app.Numbering; ;
                textBoxInum2.Text = app.InsNum;

                if (app.HihoNum.Length > 4)
                {
                    textBoxHnumM2.Text = app.HihoNum.Remove(4);
                    textBoxHnum2.Text = app.HihoNum.Substring(4);
                }
                else
                {
                    textBoxHnumM2.Text = app.HihoNum;
                    textBoxHnum2.Clear();
                }

                textBoxFamily2.Text = app.Family.ToString();
                textBoxSex2.Text = app.Sex.ToString();
                textBoxBirthday2.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                textBoxBY2.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                textBoxBM2.Text = app.Birthday.Month.ToString();
                textBoxBD2.Text = app.Birthday.Day.ToString();
                textBoxTotal2.Text = app.Total.ToString();
                textBoxCharge2.Text = app.Charge.ToString();
                textBoxDays2.Text = app.CountedDays.ToString();
            }

            //エラーに応じてテキストボックスを有効化、値設定
            var act = new Action<TextBox, bool, string>((t, b, s) =>
                {
                    if (app.StatusFlagCheck(StatusFlag.ベリファイ済))
                    {
                        t.Enabled = false;
                        t.BackColor = SystemColors.Control;
                        t.Text = s;
                    }
                    else
                    {
                        t.Enabled = b;
                        t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                        t.Text = b ? string.Empty : s;
                    }
                });

            //この3項目はかならず入力、ベリファイ済みの時のみ表示
            if (app.StatusFlagCheck(StatusFlag.ベリファイ済))
            {
                act(textBoxNumbering, false, app.Numbering);
                act(textBoxSex, false, app.Sex.ToString());
                act(textBoxFamily, false, app.Family.ToString());
            }
            else
            {
                act(textBoxNumbering, true, app.Numbering);
                act(textBoxSex, true, app.Sex.ToString());
                act(textBoxFamily, true, app.Family.ToString());
            }

            if (app.MediYear > 0)
            {
                act(textBoxY, ca.ErrAyear, app.MediYear.ToString());
                act(textBoxM, ca.ErrAmonth, app.MediMonth.ToString());
                act(textBoxPref, ca.ErrHpref, app.HihoPref.ToString());
                act(textBoxInum, ca.ErrInum, app.InsNum);
                act(textBoxBirthday, ca.ErrBirth, DateTimeEx.GetEraNumber(app.Birthday).ToString());
                act(textBoxBY, ca.ErrBirth, DateTimeEx.GetJpYear(app.Birthday).ToString());
                act(textBoxBM, ca.ErrBirth, app.Birthday.Month.ToString());
                act(textBoxBD, ca.ErrBirth, app.Birthday.Day.ToString());
                act(textBoxTotal, ca.ErrAtotal, app.Total.ToString());
                act(textBoxCharge, ca.ErrAcharge, app.Charge.ToString());
                act(textBoxDays, ca.ErrAcounteddays, app.CountedDays.ToString());

                if (app.HihoNum.Length > 4)
                {
                    act(textBoxHnumM, ca.ErrHnum, app.HihoNum.Remove(4));
                    act(textBoxHnum, ca.ErrHnum, app.HihoNum.Substring(4));
                }
                else
                {
                    act(textBoxHnumM, ca.ErrHnum, app.HihoNum);
                    act(textBoxHnum, ca.ErrHnum, "");
                }
            }
            else
            {
                if (app.MediYear == -1)
                {
                    act(textBoxY, !app.StatusFlagCheck(StatusFlag.ベリファイ済), "//");
                    act(textBoxInum, false, "");
                    act(textBoxTotal, !app.StatusFlagCheck(StatusFlag.ベリファイ済), app.AccountNumber);
                    act(textBoxCharge, !app.StatusFlagCheck(StatusFlag.ベリファイ済), app.Charge.ToString());
                }
                else if (app.MediYear == -2)
                {
                    act(textBoxY, !app.StatusFlagCheck(StatusFlag.ベリファイ済), "**");
                    act(textBoxInum, !app.StatusFlagCheck(StatusFlag.ベリファイ済), app.InsNum);
                    act(textBoxTotal, !app.StatusFlagCheck(StatusFlag.ベリファイ済), app.AccountNumber);
                    act(textBoxCharge, false, "");
                }
                else if (app.MediYear == -3)
                {
                    act(textBoxY, !app.StatusFlagCheck(StatusFlag.ベリファイ済), "--");
                    act(textBoxInum, false, "");
                    act(textBoxTotal, false, "");
                    act(textBoxCharge, false, "");
                }
                else if (app.MediYear == -4)
                {
                    act(textBoxY, !app.StatusFlagCheck(StatusFlag.ベリファイ済), "++");
                    act(textBoxInum, false, "");
                    act(textBoxTotal, false, "");
                    act(textBoxCharge, false, "");
                }

                act(textBoxM, false, "");
                act(textBoxPref, false, "");
                act(textBoxBirthday, false, "");
                act(textBoxBY, false, "");
                act(textBoxBM, false, "");
                act(textBoxBD, false, "");
                act(textBoxDays, false, "");
                act(textBoxHnumM, false, "");
                act(textBoxHnum, false, "");
                act(textBoxNumbering, false, "");
                act(textBoxSex, false, "");
                act(textBoxFamily, false, "");
            }
        }

        /// <summary>
        /// ベリファイ入力時、1回目と数値が一致するかチェックします
        /// </summary>
        /// <returns></returns>
        private bool checkVerify()
        {
            bool error = false;
            var act = new Action<TextBox, TextBox>((t1, t2) =>
                {
                    if (!t1.Enabled) return;
                    if (t1.Text == t2.Text)
                    {
                        t1.BackColor = SystemColors.Info;
                        t2.BackColor = SystemColors.Info;
                        t2.Visible = false;
                    }
                    else
                    {
                        t1.BackColor = Color.Pink;
                        t2.BackColor = Color.Pink;
                        t2.Visible = true;
                        error = true;
                    }
                });

            act(textBoxY, textBoxY2);
            act(textBoxM, textBoxM2);
            act(textBoxPref, textBoxPref2);
            act(textBoxInum, textBoxInum2);
            act(textBoxHnumM, textBoxHnumM2);
            act(textBoxHnum, textBoxHnum2);
            act(textBoxFamily, textBoxFamily2);
            act(textBoxSex, textBoxSex2);
            act(textBoxBirthday, textBoxBirthday2);
            act(textBoxBY, textBoxBY2);
            act(textBoxBM, textBoxBM2);
            act(textBoxBD, textBoxBD2);
            act(textBoxTotal, textBoxTotal2);
            act(textBoxCharge, textBoxCharge2);
            act(textBoxDays, textBoxDays2);

            if (textBoxNumbering.Enabled)
            {
                if (textBoxNumbering.Text.PadLeft(6, '0') == textBoxNumbering2.Text.PadLeft(6, '0'))
                {
                    textBoxNumbering.BackColor = SystemColors.Info;
                    textBoxNumbering2.BackColor = SystemColors.Info;
                    textBoxNumbering2.Visible = false;
                }
                else
                {
                    textBoxNumbering.BackColor = Color.Pink;
                    textBoxNumbering2.BackColor = Color.Pink;
                    textBoxNumbering2.Visible = true;
                    error = true;
                }
            }

            return !error;
        }

        int prevIndex = -1;
        private void dataGridViewPlist_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void FormOCRCheck_OsakaGakko3_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount != 0)
            {
                dataGridViewPlist.CurrentCell = null;
                dataGridViewPlist.CurrentCell = dataGridViewPlist["Aid", 0];
            }
        }

        private void textBoxHnumM_TextChanged(object sender, EventArgs e)
        {
            if (!textBoxHnumM.Focused) return;
            if (textBoxHnumM.Text.Length != 4) return;
            textBoxHnum.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri <= 0) return;
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri - 1];
        }
    }
}
