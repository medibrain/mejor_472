﻿using System.Collections.Generic;
using System.Linq;

namespace Mejor.OsakaGakko
{
    public class MediAppInfo
    {
        /// <summary>
        /// MAID [MediAppInfo ID]
        /// </summary>
        public int MAID { get; set; }
        /// <summary>
        /// 審査年月 [Input Year Month](yyyyMM)
        /// </summary>
        public int IYM { get; set; }
        /// <summary>
        /// 支部番号 [Prefectures Number]
        /// </summary>
        public int PID { get; set; }
        /// <summary>
        /// (Not In Table) 審査年 [Input Year]
        /// </summary>
        /// //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        /// //未使用？
        //public int IY => DateTimeEx.GetHsYearFromAd(IYM / 100);
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        /// <summary>
        /// (Not In Table) 審査月 [Input Month]
        /// </summary>
        public int IM => IYM - ((IYM / 100) * 100);
        /// <summary>
        /// (Not In Table) 件数 [MediAppInfo Count]
        /// </summary>
        public int MAICount { get; set; }


        ///// <summary>
        ///// 審査年月別　登録件数を取得します（降順）
        ///// </summary>
        ///// <returns></returns>
        //public static List<MediAppInfo> GetCountList()
        //{
        //    var sql = $"SELECT iym, COUNT(maid) AS maicount FROM mediappinfo GROUP BY iym ORDER BY iym DESC;";
        //    var db = new CommonDB("zenkoku_gakko");
        //    var l = db.Query<MediAppInfo>(sql);
        //    if (l == null)
        //    {
        //        return null;
        //    }
        //    var maiList = new List<MediAppInfo>();
        //    l.ToList().ForEach(mai => maiList.Add(mai));
        //    return maiList;
        //}

        ///// <summary>
        ///// 都道府県別　指定年月登録件数を取得します（昇順）
        ///// </summary>
        ///// <param name="yyyyMM"></param>
        ///// <returns></returns>
        //public static List<MediAppInfo> GetCountListByIYM(int yyyyMM)
        //{
        //    var sql = $"SELECT iym, pid, COUNT(maid) AS maicount FROM mediappinfo WHERE iym={yyyyMM} GROUP BY pid ORDER BY pid ASC;";
        //    var db = new CommonDB("zenkoku_gakko");
        //    var l = db.Query<MediAppInfo>(sql);
        //    if (l == null)
        //    {
        //        return null;
        //    }
        //    var maiList = new List<MediAppInfo>();
        //    l.ToList().ForEach(mai => maiList.Add(mai));
        //    return maiList;
        //}

    }
}
