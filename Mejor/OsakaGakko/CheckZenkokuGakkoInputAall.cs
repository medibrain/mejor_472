﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor.OsakaGakko
{
            
    class CheckZenkokuGakkoInputAall
    {
        static Regex re = new Regex(@"[^0-9]");

        public App app { get; private set; }
        public bool ErrTotal { get; private set; }
        public bool ErrCharge { get; private set; }
        public bool ErrFutan { get; private set; }
        public bool ErrYear { get; private set; }
        public bool ErrMonth { get; private set; }
        public bool ErrCountedDays { get; private set; }
        public bool ErrSex { get; private set; }
        public bool ErrEra { get; private set; }
        public bool ErrBY { get; private set; }
        public bool ErrBM { get; private set; }
        public bool ErrBD { get; private set; }
        public bool ErrDrCode { get; private set; }

        public bool ErrBirth
        {
            get
            {
                return ErrSex || ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public bool HasError
        {
            get
            {
                return ErrTotal || ErrCharge || ErrFutan || ErrYear ||
                    ErrMonth ||ErrCountedDays || ErrEra ||
                    ErrBY || ErrBM || ErrBD || ErrDrCode;
            }
        }

        public CheckZenkokuGakkoInputAall(App app)
        {
            this.app = app;
            var ocr = app.OcrData.Split(',');
            if (ocr.Length < 200)
            {
                ErrYear = true;
                ErrMonth = true;
                ErrTotal = true;
                ErrCharge = true;
                ErrFutan = true;
                ErrCountedDays = true;
                ErrSex = true;
                ErrEra = true;
                ErrBY = true;
                ErrBM = true;
                ErrBD = true;
                ErrDrCode = true;
                return;
            }

            ErrYear = false;
            ErrMonth = false;
            ErrTotal = false;
            ErrCharge = false;
            ErrFutan = false;
            ErrCountedDays = false;
            ErrSex = false;
            ErrEra = false;
            ErrBY = false;
            ErrBM = false;
            ErrBD = false;
            
            int temp = 0;

            //診療年
            int.TryParse(re.Replace(ocr[3], ""), out temp);
            if (temp == 0 || app.MediYear != temp)
                ErrYear = true;

            //診療月
            int.TryParse(re.Replace(ocr[4], ""), out temp);
            if (temp == 0 || app.MediMonth != temp)
                ErrMonth = true;

            //性別
            string sexStr = "";
            if (ocr[30] != "0") { sexStr = "1"; }
            if (ocr[31] != "0") { sexStr = sexStr + "2"; }
            if (sexStr.Length != 1 || app.Sex.ToString() != sexStr)
                ErrSex = true;

            //生年月日 年号
            string era = "";
            if (ocr[32] != "0") era = "明治";
            if (ocr[33] != "0") era += "大正";
            if (ocr[34] != "0") era += "昭和";
            if (ocr[35] != "0") era += "平成";
            if (DateTimeEx.GetEra(app.Birthday) != era) 
                ErrEra = true;

            //生年月日 年
            var y = ocr[36];
            var yi = y.IndexOf('年');
            if (yi > 0)
                y = y.Remove(yi);
            int.TryParse(re.Replace(y, ""), out temp);
            if (temp == 0 || DateTimeEx.GetJpYear(app.Birthday) != temp)
                ErrBY = true;

            //生年月日 月
            var m = ocr[37];
            var mi = m.IndexOf('月');
            if (mi > 0)
                m = m.Remove(mi);
            int.TryParse(re.Replace(m, ""), out temp);
            if (temp == 0 || app.Birthday.Month != temp)
                ErrBM = true;

            //生年月日 日
            var d = ocr[38];
            var di = d.IndexOf('日');
            if (di > 0)
                d = d.Remove(di);
            int.TryParse(re.Replace(d, ""), out temp);
            if (temp == 0 || app.Birthday.Day != temp)
                ErrBD = true;
            
            //合計
            int.TryParse(re.Replace(ocr[193], ""), out temp);
            if (temp == 0 || app.Total != temp)
                ErrTotal = true;

            //請求
            int.TryParse(re.Replace(ocr[195], ""), out temp);
            if (temp == 0 || app.Charge != temp) 
                ErrCharge = true;

            //合計と請求は計算が合い、どちらか一方があっていればOK
            bool payError = false;
            if ((app.Family == 2 || app.Family == 6) && (int)(app.Total * 70 / 100) != app.Charge)
                payError = true;
            else if (app.Family == 8 && (int)(app.Total * 80 / 100) != app.Charge && (int)(app.Total * 90 / 100) != app.Charge)
                payError = true;
            else if (app.Family == 4 && (int)(app.Total * 80 / 100) != app.Charge)
                payError = true;

            if (!payError && (!ErrTotal || !ErrCharge))
            {
                ErrTotal = false;
                ErrCharge = false;
            }

            //実日数
            var idays = new List<int>();
            int iday;
            string sday;
            sday = re.Replace(ocr[53], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[70], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[87], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[104], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[121], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            iday = idays.Max();
            if (app.CountedDays != iday) 
                ErrCountedDays = true;

            //医療機関コード
            var drCode = getDrNumber(ocr[214]);
            ErrDrCode = app.DrNum != drCode;
        }

        /// <summary>
        /// OCRデータの施術師番号から施術師番号を取り出します
        /// 頭に、「協」には0、「契」には1がつきます
        /// </summary>
        /// <param name="drNumberColumn"></param>
        /// <returns></returns>
        private string getDrNumber(string drNumberColumn)
        {
            //施術師番号整理
            var dn = drNumberColumn.Trim();
            var kyoIndex = dn.IndexOf('協');
            var keiIndex = dn.IndexOf('契');

            dn = dn.Replace("--", "-");
            int index = -1;
            if (dn.Length > 10)
            {
                for (int i = 7; i < dn.Length - 2; i++)
                {
                    if (dn[i] == '-' && "0123456789".Contains(dn[i + 1]) && dn[i + 2] == '-')
                    {
                        index = i;
                        break;
                    }
                }

                if (index != -1)
                {
                    dn = dn.Substring(index - 7);
                    dn = dn.Replace("-", "");
                    if (dn.Length > 9) dn = dn.Remove(9);
                }
            }

            if (kyoIndex != -1) dn = "0" + dn;
            if (keiIndex != -1) dn = "1" + dn;
                
            return dn;
        }

    }
}
