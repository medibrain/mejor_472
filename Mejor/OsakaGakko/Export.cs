﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.OsakaGakko
{
    class Export
    {
        static string logText;
        static int sumCheck = 0;

        public static bool DoExport(Insurer ins, int cym, string folderpath, bool inBatchFolder = true)
        {
            var sd = new scanD();

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100);
            //DateTimeEx.GetHsYearFromAd(cym / 100);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            sd.cm = cym % 100;
            sd.soS = 0;

            var wf = new WaitForm();
            try
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();

                //特定の月のスキャンID一覧を取得
                List<int> sids = new List<int>();
                using (var cmd = DB.Main.CreateCmd("SELECT sid FROM scan " +
                    "WHERE cym=:cym " +
                    "ORDER BY note1, sid"))
                {
                    cmd.Parameters.Add("cy", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                    var res = cmd.TryExecuteReaderList();
                    if (res == null)
                    {
                        Log.ErrorWriteWithMsg(cym.ToString("0000年00月") + "分のスキャンIDを取得できませんでした");
                        return false;
                    }

                    foreach (var item in res)
                    {
                        sids.Add((int)item[0]);
                    }
                }

                wf.SetMax(sids.Count);
                int scanCount = 0;

                try
                {
                    foreach (var item in sids)
                    {
                        wf.LogPrint("SID:" + item.ToString() + "の出力を開始します");
                        wf.InvokeValue = scanCount;
                        scanCount++;
                        var scan = Scan.Select(item);
                        var appList = App.GetAppsSID(item);
                        if (!ExportbySID(appList, folderpath, scan, sd, true, inBatchFolder, wf)) return false;
                        wf.LogPrint("SID:" + item.ToString() + "の出力が終わりました");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                }

                //データチェック結果表示
                if (logText != string.Empty)
                {
                    Log.ErrorWriteWithMsg(logText);
                }
            }
            finally
            {
                wf.Dispose();
            }            

            return true;
        }

        private static bool ExportbySID(List<App> appList, string exportFolder, Scan scan, scanD sd, bool imgOutput, bool inBatchFolder, WaitForm wf)
        {
            int numCheck = 0;

            string saveFolder;
            if (inBatchFolder)
            {
                saveFolder = exportFolder + "\\" + scan.Note1;
                System.IO.Directory.CreateDirectory(saveFolder);
            }
            else
            {
                saveFolder = exportFolder;
            }

            int rowcount = 0;
            int notchecked = 0;            

            appList.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            var fileREX = exportFolder + "\\REX" + DateTime.Today.ToString("yyMMdd") + ".csv";
            var fileRES = exportFolder + "\\RES" + DateTime.Today.ToString("yyMMdd") + ".csv";
            string writeYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString()
                + scan.Cyear.ToString("00") + scan.Cmonth.ToString("00");

            using (var swRex = new System.IO.StreamWriter(fileREX, true, Encoding.GetEncoding("Shift_JIS")))
            using (var swRes = new System.IO.StreamWriter(fileRES, true, Encoding.GetEncoding("Shift_JIS")))
            {
                string appNumbering = "";
                int pageNumber = 1;

                var maegami = new List<string>();

                DateTime inputD;
                string inputDat = DateTimeEx.GetEraNumber(DateTime.Today) + scan.Cyear.ToString("00")
                    + scan.Cmonth.ToString("00") + "01";
                DateTimeEx.TryGetYearMonthTimeFromJdate(inputDat, out inputD);
                var inputYYMM = inputD.ToString("yyMM");

                foreach (var app in appList)
                {
                    if (wf.Cancel) return false;

                    if (!app.StatusFlagCheck(StatusFlag.入力済))
                    {
                        //Applicationの点検済みフラグが立っていない場合の処理は必要ないか？

                        notchecked++;
                        continue;
                    }

                    string originalImgName = app.GetImageFullPath();
                    string baseFileName = inputYYMM + scan.Note1;

                    if (app.MediYear == -1)
                    {
                        //総括書　画像化は不要

                        //総括表の合計金額と申請書の請求合計とのチェック
                        if (sumCheck > sd.soS && numCheck != 0)
                        {
                            int s = sumCheck - sd.soS;
                            logText = logText + scan.Note1 + ": Numbering=" + numCheck +
                                " 付近で金額不整合。総括表金額: \\" + sd.soS +
                                " , 申請書合計金額: \\" + sumCheck +
                                " => 超過金額: \\" + s +
                                "\r\n";
                        }
                        else if (sumCheck < sd.soS && numCheck != 0)
                        {
                            int s = sd.soS - sumCheck;
                            logText = logText + scan.Note1 + ": Numbering=" + numCheck +
                                " 付近で金額不整合。総括表金額: \\" + sd.soS +
                                " , 申請書合計金額: \\" + sumCheck +
                                " => 不足金額: -\\" + s +
                                "\r\n";
                        }

                        sd.soF = app.AccountNumber.ToString();
                        sd.soS = app.Charge;

                        //口座番号マッチング
                        sumCheck = 0;
                        if (sd.soF != sd.redF)
                        {
                            logText = logText + scan.Note1 + ": " +
                                " 赤バッジと総括表の口座番号が一致しません。\r\n";
                        }

                        continue;
                    }
                    else if (app.MediYear == -2)
                    {
                        //赤バッチの際は保険者番号に協議会番号を記載
                        sd.redF = app.AccountNumber.ToString();
                        sd.redK = app.InsNum;
                        continue;
                    }
                    else if (app.MediYear == -3)
                    {
                        //続紙
                        //画像化+ページナンバリング
                        pageNumber++;
                        string zokushiFilename = baseFileName + appNumbering + pageNumber.ToString("00") + ".tif";

                        if (imgOutput == true)
                        {
                            if (!ImageUtility.SaveOne(originalImgName, saveFolder + "\\" + zokushiFilename))
                            {
                                Log.ErrorWriteWithMsg("\r\n\r\nナンバリング " +
                                    appNumbering + " 番が重複している可能性があります");
                                return false;
                            }
                        }

                        continue;
                    }
                    else if (app.MediYear == -4 || app.MediYear == 0)
                    {
                        //白バッジ、その他の場合、やることは無い
                        continue;
                    }
                    else if (app.MediYear == -5)
                    {
                        //緊急避難処置、申請書の前に続紙があった場合
                        //画像化+ページナンバリング
                        maegami.Add(originalImgName);
                        continue;
                    }


                    //以下、申請書の場合
                    appNumbering = app.Numbering.PadLeft(6, '0');
                    pageNumber = 1;
                    string saveFilename = baseFileName + appNumbering + pageNumber.ToString("00") + ".tif";

                    if (imgOutput == true)
                    {
                        if (!ImageUtility.SaveOne(originalImgName, saveFolder + "\\" + saveFilename))
                        {
                            Log.ErrorWriteWithMsg("\r\n\r\nナンバリング " +
                                appNumbering + " 番が重複している可能性があります");
                            return false;
                        }
                    }

                    //ナンバリング連番チェック
                    int n;
                    if (int.TryParse(app.Numbering, out n))
                    {
                        if (numCheck == 0)
                        {
                            numCheck = n;
                        }
                        else if (n == 1)
                        {
                            numCheck = 1;
                        }
                        else
                        {
                            numCheck++;
                            if (n != numCheck)
                            {
                                logText = logText + scan.Note1 + ": Numbering " + n +
                                    " でナンバリング不連続が検出されました。\r\n";
                            }
                        }
                    }

                    //総括表の合計金額と申請書の請求合計とのチェック
                    sumCheck = sumCheck + app.Charge;

                    //緊急避難処置で前紙があった場合
                    foreach (var mae in maegami)
                    {
                        pageNumber++;
                        string maeFilename = baseFileName + appNumbering + pageNumber.ToString("00") + ".tif";

                        if (imgOutput == true)
                        {
                            if (!ImageUtility.SaveOne(mae, saveFolder + "\\" + maeFilename))
                            {
                                Log.ErrorWriteWithMsg("\r\n\r\nID= " +
                                    app.Aid + " の前紙の処理に失敗しました");
                                return false;
                            }
                        }
                    }

                    maegami.Clear();

                    string bd = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                    bd += DateTimeEx.GetJpYear(app.Birthday).ToString("00");
                    bd += app.Birthday.ToString("MM");//. ToString("MMdd");

                    var batch = scan.Note1;
                    var num = app.Numbering;
                    var sinryoYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString() +
                        app.MediYear.ToString("00") + app.MediMonth.ToString("00");
                    var kubun = "0";                        //仕様書で0固定
                    var honke = app.Family.ToString();
                    var pref = app.HihoPref.ToString();
                    var insurerNo = app.InsNum;
                    var kumiaiNo = app.HihoNum;
                    var sex = app.Sex == 1 ? "1" : "2";    //stringに変換する意味合いもあり
                    var birth = bd;
                    var days = app.CountedDays.ToString();
                    var totalCost = app.Total.ToString();
                    var seikyu = app.Charge.ToString();
                    var yuyo = "0";                         //仕様書で0固定
                    var kigoubango = app.ClinicNum;               //記号番号は？
                    var redKyogikaiNo = sd.redK;//redKnum;
                    var redFurikomiNo = sd.redF;// redFnum;
                    var soFurikomiNo = sd.soF;// soFnum;
                    var soTotal = sd.soS.ToString();// soSum.ToString();

                    //
                    var rex = new string[19];
                    rex[0] = writeYM;
                    rex[1] = batch;
                    rex[2] = num;
                    rex[3] = sinryoYM;
                    rex[4] = kubun;
                    rex[5] = honke;
                    rex[6] = pref;
                    rex[7] = insurerNo;
                    rex[8] = kumiaiNo;
                    rex[9] = sex;
                    rex[10] = birth;
                    rex[11] = days;
                    rex[12] = totalCost;
                    rex[13] = seikyu;
                    rex[14] = yuyo;
                    rex[15] = redKyogikaiNo;
                    rex[16] = redFurikomiNo;
                    rex[17] = soFurikomiNo;
                    rex[18] = soTotal;
                    try
                    {
                        swRex.WriteLine(string.Join(",", rex));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                        return false;
                    }

                    var res = new string[17];
                    res[0] = writeYM;
                    res[1] = batch;
                    res[2] = num;
                    res[3] = sinryoYM;
                    res[4] = kubun;
                    res[5] = honke;
                    res[6] = pref;
                    res[7] = insurerNo;
                    res[8] = kumiaiNo;
                    res[9] = sex;
                    res[10] = birth;
                    res[11] = days;
                    res[12] = totalCost;
                    res[13] = seikyu;
                    res[14] = yuyo;
                    res[15] = redKyogikaiNo;
                    res[16] = redFurikomiNo;
                    try
                    {
                        swRes.WriteLine(string.Join(",", res));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nRES CSVデータの書き込みに失敗しました");
                        return false;
                    }
                    rowcount++;
                }
            }

            sd.rc += rowcount;
            sd.nc += notchecked;
            return true;
        }

        //白バッジが複数のScanIDにまたがった場合のため。
        class scanD
        {
            public int cy { get; set; }
            public int cm { get; set; }
            public string redK { get; set; }    //赤バッジ・協議会番号
            public string redF { get; set; }    //赤バッジ・振込先口座番号
            public string soF { get; set; }     //総括表・振込先口座番号
            public int soS { get; set; }        //総括表・請求金額合計
            public int rc { get; set; }
            public int nc { get; set; }
        }

        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            //バッチ,バッチ２シート一覧を作成
            var batchList = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ}");
            batchList.Sort((x, y) => y.Aid.CompareTo(x.Aid));
            var batch2List = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ2}");
            batch2List.Sort((x, y) => y.Aid.CompareTo(x.Aid));

            //バッチ,バッチ２取得ラムダ
            var getBatch = new Func<int, App>(aid =>
            {
                foreach (var b in batchList)
                {
                    if (b.Aid < aid) return b;
                }
                return null;
            });
            var getBatch2 = new Func<int, App>(aid =>
            {
                foreach (var b in batch2List)
                {
                    if (b.Aid < aid) return b;
                }
                return null;
            });

            //団体名取得ラムダ
            var accountDic = new Dictionary<string, string>();
            var getDantaiName = new Func<string, string>(code =>
            {
                if (string.IsNullOrEmpty(code)) return string.Empty;
                if (!accountDic.ContainsKey(code))
                {
                    var account = Account.GetAccount(code);
                    if (account != null)
                    {
                        accountDic.Add(code, account.Name);
                        return account.Name;
                    }
                    else return string.Empty;
                }
                return accountDic[code];
            });

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(list.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;
            wf.LogPrint("出力を開始します");
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,通知番号,保険者番号,処理年,処理月,バッチ,ナンバリング," +
                        "組合員番号,組合員氏名,受療者氏名,性別,年号,生年,月,日,郵便番号,送付先住所1,送付先住所2," +
                        "診療年,診療月,実日数,合計金額,請求金額,施術所番号,施術所名," +
                        "照会理由,過誤理由,再審査理由,返戻理由,詳細,団体名,口座番号,照会結果,メモ");
                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100,int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    var notifyNumber = $"{jcy}{jcm}-{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<string, string[]> toAddresses = address =>
                    {
                        var result = new string[] { "", "" };
                        if (!address.Contains("　"))
                        {
                            result[0] = address;
                        }
                        else
                        {
                            var index = address.IndexOf("　");
                            result[0] = address.Substring(0, index);
                            result[1] = address.Substring(index + 1);
                        }
                        return result;
                    };

                    foreach (var item in list)
                    {
                        var adds = toAddresses(item.HihoAdd);
                        var batch = getBatch(item.Aid);
                        var batch2 = getBatch2(item.Aid);
                        var accountNumber = batch2?.AccountNumber;

                        wf.LogPrint($"バッチ[{batch.Numbering}]－バッチ２[{batch2.Numbering}]－ナンバリング[{item.Numbering}]の出力中です…");

                        ss.Add(item.Aid.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(Insurer.CurrrentInsurer.InsNumber.ToString());
                        ss.Add(jcy);
                        ss.Add(jcm);
                        ss.Add(batch?.Numbering);
                        ss.Add(item.Numbering.ToString());
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(DateTimeEx.GetEra(item.Birthday));
                        ss.Add(toJYear(DateTimeEx.GetEraNumberYear(item.Birthday)));
                        ss.Add(item.Birthday.Month.ToString());
                        ss.Add(item.Birthday.Day.ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(adds[0]);
                        ss.Add(adds[1]);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add(item.HenreiReasonStr);
                        ss.Add("\"" + item.MemoInspect + "\"");
                        ss.Add(getDantaiName(accountNumber));
                        ss.Add(accountNumber);
                        ss.Add(item.ShokaiResultStr);
                        ss.Add("\"" + item.Memo + "\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool AnalysisExport(Insurer ins, int cym)
        {
            string fn;
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv";
                f.FileName = ins.InsurerName + " 分析用" + cym.ToString("00") + ".csv";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fn = f.FileName;
            }

            var wfs = new WaitFormSimple();
            Task.Factory.StartNew(() => wfs.ShowDialog());

            var list = App.GetApps(cym);

            try
            {
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.UTF8))
                {
                    sw.WriteLine("処理年,処理月,診療年,診療月,被保番,家族区分,災害支払猶予,性別,生年月日," +
                        "実日数,合計,請求,負担,新規継続,柔整師登録番号,ナンバリング," +
                        "初検年,初検月,負傷1,負傷2,負傷3,負傷4,負傷5,部位数");
                    var l = new List<string>();
                    foreach (var a in list)
                    {
                        if (a.MediYear < 0) continue;
                        l.Add(a.ChargeYear.ToString());
                        l.Add(a.ChargeMonth.ToString());
                        l.Add(a.MediYear.ToString());
                        l.Add(a.MediMonth.ToString());
                        l.Add(a.HihoNum);
                        l.Add(a.Family.ToString());
                        l.Add(a.HihoType.ToString());
                        l.Add(a.Sex.ToString());
                        l.Add(a.Birthday.ToString("yyyy/MM/dd"));
                        l.Add(a.CountedDays.ToString());
                        l.Add(a.Total.ToString());
                        l.Add(a.Charge.ToString());
                        l.Add(a.Partial.ToString());
                        l.Add(a.NewContType.ToString());
                        l.Add(a.DrNum);
                        l.Add(a.Numbering);

                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        l.Add(DateTimeEx.GetHsYearFromAd(a.FushoFirstDate1.Year, a.FushoFirstDate1.Month).ToString());
                        //l.Add(DateTimeEx.GetHsYearFromAd(a.FushoFirstDate1.Year).ToString());
                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        l.Add(a.FushoFirstDate1.Month.ToString());
                        l.Add(a.FushoName1);
                        l.Add(a.FushoName2);
                        l.Add(a.FushoName3);
                        l.Add(a.FushoName4);
                        l.Add(a.FushoName5);
                        int b = 0;
                        if (!string.IsNullOrWhiteSpace(a.FushoName1)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName2)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName3)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName4)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName5)) b++;
                        l.Add(b.ToString());
                        sw.WriteLine(string.Join(",", l));
                        l.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wfs.InvokeCloseDispose();
            }

            return true;
        }

        public static bool MediMatchingExportWithRESAndImage(Insurer ins, int cym)
        {
            string saveFolder = "";
            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
                saveFolder = f.SelectedPath;
            }

            return DoExport(ins, cym, saveFolder, false);
        }
    }
}
