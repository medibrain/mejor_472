﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.OsakaGakko
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHosCode = new Point(400, 1900);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 300);
        Point posCost = new Point(400, 1900);
        Point posDays = new Point(400, 300);
        Point posNumbering = new Point(300, 0);

        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls,
            fushoControls, hosControls, numberControls, batchControls, 
            batchCountControls, batchDrCodeControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hnumControls = new Control[] { verifyBoxPref, verifyBoxHnum, verifyBoxFamily, verifyBoxFamily };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF1Y, verifyBoxF1M, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            hosControls = new Control[] { verifyBoxDrCode, };
            numberControls = new Control[] { verifyBoxNumbering, };
            batchControls = new Control[] { verifyBoxBank, };
            batchCountControls = new Control[] { verifyBoxAllTotal, };
            batchDrCodeControls = new Control[] { };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            var list = App.GetAppsGID(this.scanGroup.GroupID);
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //先頭データの際、shownイベントより先にtextchangedイベントが発生するためこちらへ移動
            panelBatch.Visible = false;
            panelHnum.Visible = false;
            panelTotal.Visible = false;
            verifyBoxM.Visible = false;
            labelM.Visible = false;

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (hosControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHosCode;
            else if (numberControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
            else if (batchControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatch;
            else if (batchCountControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatchCount;
            else if (batchDrCodeControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatchDrCode;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }
        
        private bool checkWhiteBatch(App app)
        {
            hasError = false;

            //バッチ番号
            setStatus(verifyBoxBatch, verifyBoxBatch.Text.Trim().Length != 4);

            if(hasError)
            {
                showInputErrorMessage();
                return false;
            }

            resetInputData(app);
            app.Numbering = verifyBoxBatch.Text.Trim();
            app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
            app.AppType = APP_TYPE.バッチ;
            return true;
        }

        private bool checkRedBatch(App app)
        {
            hasError = false;
            
            setStatus(verifyBoxBank, verifyBoxBank.Text.Trim().Length != 7);
            setStatus(verifyBoxBatch, verifyBoxBatch.Text.Trim().Length < 1);
            int pref = verifyBoxPref.GetIntValue();
            setStatus(verifyBoxPref, pref < 1 || 47 < pref);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            resetInputData(app);
            app.HihoPref = pref;
            app.AccountNumber = verifyBoxBank.Text.Trim();
            app.Numbering = verifyBoxBatch.Text.Trim();
            app.MediYear = (int)APP_SPECIAL_CODE.バッチ2;
            app.AppType = APP_TYPE.バッチ2;
            return true;
        }

        private bool checkSokatsu(App app)
        {
            hasError = false;

            //合計請求額
            int total = verifyBoxAllTotal.GetIntValue();
            setStatus(verifyBoxAllTotal, total < 100);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            resetInputData(app);
            app.Total = total;
            app.MediYear = (int)APP_SPECIAL_CODE.総括票;
            app.AppType = APP_TYPE.総括票;
            return true;
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 10文字かつ数字に直せること
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length != 10 || 
                !long.TryParse(verifyBoxHnum.Text, out long hnumTemp));

            //本家区分
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !(new int[] { 0, 2, 4, 6, 8 }).Contains(family));

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //合計
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            if ((family == 2 || family == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //初検年月チェック
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue(); ;
            var shoken = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);
                //sY = DateTimeEx.GetAdYearFromHs(sY);
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                if (!DateTimeEx.IsDate(sY, sM, 1))
                {
                    hasError = true;
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                }
                else
                {
                    shoken = new DateTime(sY, sM, 1);
                }
            }

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //ナンバリングチェック
            //大阪学校共済のナンバリングは6桁以内
            int numbering = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, numbering < 1 || 1000000 < numbering);

            //柔整師番号
            if (verifyBoxDrCode.Text.Trim().Length != 0)
            {
                var t = verifyBoxDrCode.Text.Trim();
                if (t.Length == 10 && (t[0] != '0' && t[0] != '1'))
                {
                    //1文字目は0か1で指定されている
                    setStatus(verifyBoxDrCode, true);
                }
                else
                {
                    setStatus(verifyBoxDrCode, false);
                }
            }
            else
            {
                setStatus(verifyBoxDrCode, false);
            }

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //柔整で柔整登録番号がなければ確認
            if (scan.Note2 == string.Empty && verifyBoxDrCode.Text.Trim().Length == 0)
            {
                verifyBoxDrCode.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("柔整師登録番号が指定されていません。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //ナンバリング抜けチェック
            var index = bsApp.Position;
            var l = (List<App>)bsApp.DataSource;
            int preNum = 0;
            for (int i = index - 1; i >= 0; i--)
            {
                var preApp = l[i];
                if (preApp.MediYear > 0)
                {
                    if (!int.TryParse(preApp.Numbering, out preNum)) continue;
                    break;
                }
            }

            if (numbering != 1 && preNum != 0 && preNum + 1 != numbering)
            {
                verifyBoxNumbering.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("直前のナンバリングと連番になっていません。" +
                    "このまま登録してもよろしいですか？", "ナンバリング確認",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text.Trim();
            app.Family = family;
            app.Sex = sex;
            app.Birthday = birthDt;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            app.NewContType = DateTimeEx.GetAdYearFromHs(year * 100 + month) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = DateTimeEx.GetAdYearFromHs(year) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.AccountNumber = string.Empty;
            app.DrNum = verifyBoxDrCode.Text.Trim();

            app.AppType = scan.AppType;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            app.FushoFirstDate1 = shoken;
            app.HihoType = verifyCheckBoxSai.Checked ? (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予 : 0;

            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "**")
            {
                //赤バッジ
                if (!checkRedBatch(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (verifyBoxY.Text == "00")
            {
                //白バッジ
                if (!checkWhiteBatch(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (verifyBoxY.Text == "//")
            {
                //総括票
                if (!checkSokatsu(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(6, '0');

                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全テキストボックスクリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }

            void setEnable(TextBox t, bool b)
            {
                t.Enabled = b;
                t.BackColor = b ? SystemColors.Info : SystemColors.Control;
            }

            //鍼灸時はバッチシートで柔整師番号を入力しているため、グレーアウト
            if (scan.AppType != APP_TYPE.柔整)
            {
                setEnable(verifyBoxF1, false);
                setEnable(verifyBoxF1Y, false);
                setEnable(verifyBoxF1M, false);
                setEnable(verifyBoxF2, false);
                setEnable(verifyBoxF3, false);
                setEnable(verifyBoxF4, false);
                setEnable(verifyBoxF5, false);
                setEnable(verifyBoxDrCode, false);
            }
            
            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        private void verifyBoxFusho_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }


        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            var ca = new CheckZenkokuGakkoInputAall(app);
            var nv = !ca.app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {
                //白バッチ
                setValue(verifyBoxY, "00", firstTime, nv);
                setValue(verifyBoxBatch, app.Numbering, firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ2)
            {
                //赤バッチ
                setValue(verifyBoxY, "**", firstTime, nv);
                setValue(verifyBoxBank, app.AccountNumber, firstTime, nv);
                setValue(verifyBoxPref, app.HihoPref.ToString("00"), firstTime, nv);
                setValue(verifyBoxBatch, app.Numbering, firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.総括票)
            {
                setValue(verifyBoxY, "//", firstTime, nv);
                setValue(verifyBoxAllTotal, app.Total.ToString(), firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyCheckBoxSai, app.HihoType == (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予, firstTime, nv);
                setValue(verifyBoxNumbering, app.Numbering, firstTime, nv);
                setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                setValue(verifyBoxFamily, app.Family, firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1), firstTime, false);
                setValue(verifyBoxF1M, app.FushoFirstDate1.Month, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);
                setValue(verifyBoxTotal, app.Total, firstTime, nv && ca.ErrTotal);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv && ca.ErrCharge);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxDrCode, app.DrNum, firstTime, nv && app.AppType == APP_TYPE.柔整);
            }

            missCounterReset();
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            SuspendLayout();
            var spss = new string[] { "**", "00", "//", "--", "++" };
            if (spss.Contains(verifyBoxY.Text))
            {
                panelBatch.Visible = true;
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;

                if (verifyBoxY.Text == "00")
                {
                    labelBatch.Text = "バッチ番号";
                    verifyBoxBank.Enabled = false;
                    verifyBoxPref.Enabled = false;
                    verifyBoxBatch.Enabled = true;
                    verifyBoxAllTotal.Enabled = false;
                }
                else if (verifyBoxY.Text == "**")
                {
                    labelBatch.Text = "協議会番号";
                    verifyBoxBank.Enabled = true;
                    verifyBoxPref.Enabled = true;
                    verifyBoxBatch.Enabled = true;
                    verifyBoxAllTotal.Enabled = false;
                }
                else if (verifyBoxY.Text == "//")
                {
                    labelBatch.Text = "バッチ番号";
                    verifyBoxBank.Enabled = false;
                    verifyBoxPref.Enabled = false;
                    verifyBoxBatch.Enabled = false;
                    verifyBoxAllTotal.Enabled = true;
                }
                else
                {
                    labelBatch.Text = "バッチ番号";
                    verifyBoxBank.Enabled = false;
                    verifyBoxPref.Enabled = false;
                    verifyBoxBatch.Enabled = false;
                    verifyBoxAllTotal.Enabled = false;
                }
            }
            else
            {
                panelBatch.Visible = false;
                panelHnum.Visible = true;
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
            }
            ResumeLayout();
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (batchControls.Contains(t)) posBatch = pos;
            else if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (hosControls.Contains(t)) posHosCode = pos;
            else if (numberControls.Contains(t)) posNumbering = pos;
            else if (batchCountControls.Contains(t)) posBatchCount = pos;
            else if (batchDrCodeControls.Contains(t)) posBatchDrCode = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
