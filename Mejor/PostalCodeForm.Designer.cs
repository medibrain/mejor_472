﻿namespace Mejor
{
    partial class PostalCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxZipcode = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxChoiki = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSikutyoson = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTodofuken = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxShowZipcode = new System.Windows.Forms.TextBox();
            this.textBoxShowAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewHistory = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.row = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHistory)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(343, 16);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(91, 31);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.TabStop = false;
            this.buttonCancel.Text = "閉じる（F5）";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.SystemColors.HighlightText;
            this.buttonOK.Font = new System.Drawing.Font("MS UI Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonOK.ForeColor = System.Drawing.Color.RoyalBlue;
            this.buttonOK.Location = new System.Drawing.Point(437, 16);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(67, 31);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "確定";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "〒（7桁）";
            // 
            // textBoxZipcode
            // 
            this.textBoxZipcode.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxZipcode.Location = new System.Drawing.Point(9, 20);
            this.textBoxZipcode.MaxLength = 7;
            this.textBoxZipcode.Name = "textBoxZipcode";
            this.textBoxZipcode.Size = new System.Drawing.Size(53, 19);
            this.textBoxZipcode.TabIndex = 1;
            this.textBoxZipcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxZipcode_KeyPress);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(5, 5);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(506, 196);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.LightSkyBlue;
            this.buttonSearch.Location = new System.Drawing.Point(453, 5);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(58, 36);
            this.buttonSearch.TabIndex = 8;
            this.buttonSearch.Text = "検索";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(286, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "町域";
            // 
            // textBoxChoiki
            // 
            this.textBoxChoiki.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxChoiki.Location = new System.Drawing.Point(288, 20);
            this.textBoxChoiki.Name = "textBoxChoiki";
            this.textBoxChoiki.Size = new System.Drawing.Size(159, 19);
            this.textBoxChoiki.TabIndex = 7;
            this.textBoxChoiki.Enter += new System.EventHandler(this.textBoxChoiki_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "市区町村";
            // 
            // textBoxSikutyoson
            // 
            this.textBoxSikutyoson.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxSikutyoson.Location = new System.Drawing.Point(131, 20);
            this.textBoxSikutyoson.Name = "textBoxSikutyoson";
            this.textBoxSikutyoson.Size = new System.Drawing.Size(154, 19);
            this.textBoxSikutyoson.TabIndex = 5;
            this.textBoxSikutyoson.Enter += new System.EventHandler(this.textBoxSikutyoson_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "都道府県";
            // 
            // textBoxTodofuken
            // 
            this.textBoxTodofuken.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.textBoxTodofuken.Location = new System.Drawing.Point(65, 20);
            this.textBoxTodofuken.Name = "textBoxTodofuken";
            this.textBoxTodofuken.Size = new System.Drawing.Size(63, 19);
            this.textBoxTodofuken.TabIndex = 3;
            this.textBoxTodofuken.Enter += new System.EventHandler(this.textBoxTodofuken_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "〒";
            // 
            // textBoxShowZipcode
            // 
            this.textBoxShowZipcode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxShowZipcode.Location = new System.Drawing.Point(36, 6);
            this.textBoxShowZipcode.Name = "textBoxShowZipcode";
            this.textBoxShowZipcode.ReadOnly = true;
            this.textBoxShowZipcode.Size = new System.Drawing.Size(53, 12);
            this.textBoxShowZipcode.TabIndex = 1;
            this.textBoxShowZipcode.TabStop = false;
            this.textBoxShowZipcode.Text = "-------";
            this.textBoxShowZipcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxShowZipcode.DoubleClick += new System.EventHandler(this.textBoxShowZipcode_DoubleClick);
            // 
            // textBoxShowAddress
            // 
            this.textBoxShowAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxShowAddress.Location = new System.Drawing.Point(95, 6);
            this.textBoxShowAddress.Multiline = true;
            this.textBoxShowAddress.Name = "textBoxShowAddress";
            this.textBoxShowAddress.ReadOnly = true;
            this.textBoxShowAddress.Size = new System.Drawing.Size(241, 30);
            this.textBoxShowAddress.TabIndex = 2;
            this.textBoxShowAddress.TabStop = false;
            this.textBoxShowAddress.Text = "-------";
            this.textBoxShowAddress.DoubleClick += new System.EventHandler(this.textBoxShowAddress_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label6.Location = new System.Drawing.Point(13, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 22);
            this.label6.TabIndex = 3;
            this.label6.Text = "ダブルクリックで\r\n　コピーできます";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label7.Location = new System.Drawing.Point(358, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 11);
            this.label7.TabIndex = 4;
            this.label7.Text = "↑　Tabキーで確定ボタンへ　↓";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxZipcode);
            this.panel1.Controls.Add(this.textBoxTodofuken);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxSikutyoson);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxChoiki);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(516, 45);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewHistory);
            this.panel2.Controls.Add(this.buttonCancel);
            this.panel2.Controls.Add(this.buttonOK);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.textBoxShowZipcode);
            this.panel2.Controls.Add(this.textBoxShowAddress);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 251);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(516, 300);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewHistory
            // 
            this.dataGridViewHistory.AllowUserToAddRows = false;
            this.dataGridViewHistory.AllowUserToDeleteRows = false;
            this.dataGridViewHistory.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridViewHistory.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewHistory.ColumnHeadersHeight = 20;
            this.dataGridViewHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.row,
            this.zip,
            this.address});
            this.dataGridViewHistory.Location = new System.Drawing.Point(12, 53);
            this.dataGridViewHistory.MultiSelect = false;
            this.dataGridViewHistory.Name = "dataGridViewHistory";
            this.dataGridViewHistory.ReadOnly = true;
            this.dataGridViewHistory.RowHeadersVisible = false;
            this.dataGridViewHistory.RowTemplate.Height = 21;
            this.dataGridViewHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewHistory.Size = new System.Drawing.Size(492, 235);
            this.dataGridViewHistory.TabIndex = 7;
            this.dataGridViewHistory.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHistory_CellDoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 45);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(516, 206);
            this.panel3.TabIndex = 1;
            // 
            // address
            // 
            this.address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.address.HeaderText = "住所";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // zip
            // 
            this.zip.HeaderText = "郵便番号";
            this.zip.Name = "zip";
            this.zip.ReadOnly = true;
            this.zip.Width = 80;
            // 
            // row
            // 
            this.row.HeaderText = "履歴";
            this.row.Name = "row";
            this.row.ReadOnly = true;
            this.row.Width = 40;
            // 
            // PostalCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Turquoise;
            this.ClientSize = new System.Drawing.Size(516, 551);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "PostalCodeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "住所検索";
            this.Shown += new System.EventHandler(this.PostalCodeForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PostalCodeForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PostalCodeForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHistory)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxZipcode;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxChoiki;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSikutyoson;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTodofuken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxShowZipcode;
        private System.Windows.Forms.TextBox textBoxShowAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridViewHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn row;
        private System.Windows.Forms.DataGridViewTextBoxColumn zip;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
    }
}