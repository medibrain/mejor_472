﻿
partial class TiffControl
{
    /// <summary> 
    /// 必要なデザイナー変数です。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// 使用中のリソースをすべてクリーンアップします。
    /// </summary>
    /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region コンポーネント デザイナーで生成されたコード

    /// <summary> 
    /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
    /// コード エディターで変更しないでください。
    /// </summary>
    private void InitializeComponent()
    {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonTiffPrev = new System.Windows.Forms.Button();
            this.buttonTiffNext = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(370, 271);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // buttonTiffPrev
            // 
            this.buttonTiffPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTiffPrev.Location = new System.Drawing.Point(3, 3);
            this.buttonTiffPrev.Name = "buttonTiffPrev";
            this.buttonTiffPrev.Size = new System.Drawing.Size(24, 22);
            this.buttonTiffPrev.TabIndex = 7;
            this.buttonTiffPrev.TabStop = false;
            this.buttonTiffPrev.Text = "<";
            this.buttonTiffPrev.UseVisualStyleBackColor = true;
            this.buttonTiffPrev.Visible = false;
            this.buttonTiffPrev.Click += new System.EventHandler(this.buttonTiffPrev_Click);
            // 
            // buttonTiffNext
            // 
            this.buttonTiffNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTiffNext.Location = new System.Drawing.Point(29, 3);
            this.buttonTiffNext.Name = "buttonTiffNext";
            this.buttonTiffNext.Size = new System.Drawing.Size(24, 22);
            this.buttonTiffNext.TabIndex = 8;
            this.buttonTiffNext.TabStop = false;
            this.buttonTiffNext.Text = ">";
            this.buttonTiffNext.UseVisualStyleBackColor = true;
            this.buttonTiffNext.Visible = false;
            this.buttonTiffNext.Click += new System.EventHandler(this.buttonTiffNext_Click);
            // 
            // TiffControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.buttonTiffPrev);
            this.Controls.Add(this.buttonTiffNext);
            this.Controls.Add(this.pictureBox);
            this.Name = "TiffControl";
            this.Size = new System.Drawing.Size(370, 271);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pictureBox;
    private System.Windows.Forms.Button buttonTiffPrev;
    private System.Windows.Forms.Button buttonTiffNext;
}
