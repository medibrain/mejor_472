﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

static class DateTimeEx
{
    public static DateTime DateTimeNull = DateTime.MinValue;
    public static string ErrorMessage = string.Empty;
    public static string EraString { get; private set; }
    static List<EraJ> EraList = new List<EraJ>();
    public static System.Globalization.CultureInfo culture;

    static DateTimeEx()
    {
        culture = new System.Globalization.CultureInfo("ja-JP", true);
        culture.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
        GetEraList();
    }

    internal class EraJ
    {
        internal DateTime SDate = DateTime.MaxValue;
        internal DateTime EDate = DateTime.MaxValue;
        internal string Name = string.Empty;
        internal string ShortName = string.Empty;
        internal string Initial = string.Empty;
        internal int Number = 0;
        internal int Difference = 0;
        internal int Max = 0;
    }

    /// <summary>
    /// 和暦年号一覧を指定します
    /// </summary>
    /// <returns></returns>
    private static void GetEraList()
    {
        var m = new EraJ();
        m.SDate = new DateTime(1868, 9, 8);
        m.EDate = new DateTime(1912, 7, 29);
        m.Name = "明治";
        m.ShortName = "明";
        m.Number = 1;
        m.Initial = "M";
        m.Difference = 1867;
        m.Max = 45;
        EraList.Add(m);

        var t = new EraJ();
        t.SDate = new DateTime(1912, 7, 30);
        t.EDate = new DateTime(1926, 12, 24);
        t.Name = "大正";
        t.ShortName = "大";
        t.Number = 2;
        t.Initial = "T";
        t.Difference = 1911;
        t.Max = 15;
        EraList.Add(t);

        var s = new EraJ();
        s.SDate = new DateTime(1926, 12, 25);
        s.EDate = new DateTime(1989, 1, 7);
        s.Name = "昭和";
        s.ShortName = "昭";
        s.Number = 3;
        s.Initial = "S";
        s.Difference = 1925;
        s.Max = 64;
        EraList.Add(s);

        var h = new EraJ();
        h.SDate = new DateTime(1989, 1, 8);


        //20190401134048 furukawa st ////////////////////////
        //平成31年4月30日で終了
        h.EDate = new DateTime(2019, 4, 30);
        //h.EDate = DateTime.MaxValue;
        //20190401134048 furukawa ed ////////////////////////

        h.Name = "平成";
        h.ShortName = "平";
        h.Number = 4;
        h.Initial = "H";
        h.Difference = 1988;

        //20190401131332 furukawa st ////////////////////////
        //平成31年で最大
        h.Max = 31;
        //h.Max = 99;
        //20190401131332 furukawa ed ////////////////////////


        EraList.Add(h);


        //20190401131232 furukawa st ////////////////////////
        //令和追加
        var r = new EraJ();
        r.SDate = new DateTime(2019, 5, 1);
        r.EDate = DateTime.MaxValue;
        r.Name = "令和";
        r.ShortName = "令";
        r.Number = 5;
        r.Initial = "R";
        r.Difference = 2018;
        r.Max = 99;
        EraList.Add(r);
        //20190401131232 furukawa ed ////////////////////////


        //年号を表す文字列を整備
        foreach (var item in EraList)
        {
            EraString += item.ShortName;
            EraString += item.Initial;
            EraString += item.Initial.ToLower();
        }
    }


    /// <summary>
    /// 年号の略(漢字１文字)を取得します。なかった場合、空白が返ります。
    /// </summary>
    /// <param name="dt">日付</param>
    /// <returns></returns>
    public static string GetEraShort(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].ShortName;
        }

        return string.Empty;
    }

    /// <summary>
    /// 年号の略(アルファベット１文字)を取得します。なかった場合、空白が返ります。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetInitialEra(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial;
        }

        return string.Empty;
    }

    /// <summary>
    /// 年号を漢字正式名称で取得します
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetEra(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name;
        }

        return string.Empty;
    }

    /// <summary>
    /// 年号を表す数字(昭和=3、平成=4)を取得します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="dt">西暦</param>
    /// <returns></returns>
    public static int GetEraNumber(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Number;
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年を返します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="dt">西暦</param>
    /// <returns></returns>
    public static int GetJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return dt.Year - EraList[i].Difference;
        }

        return 0;
    }
    /// <summary>
    /// 対応する和暦の年を返します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="yyyyMM">yyyyMM</param>
    /// <returns></returns>
    public static int GetJpYearFromYM(int yyyyMM)
    {
        var dt = new DateTime(yyyyMM / 100, yyyyMM % 100, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return dt.Year - EraList[i].Difference;
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年月を正式年号付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetEraJpYearMonth(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00年") + dt.ToString("MM月");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年月を正式元号付きで返します。なかった場合、0が返ります。
    /// </summary>
    /// <returns>令和yy年mm月</returns>
    public static string GetEraJpYearMonth(int yyyymm)
    {
        var dt = new DateTime(yyyymm / 100, yyyymm % 100, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00年") + dt.ToString("MM月");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年月をイニシャル元号付きで返します。なかった場合、0が返ります。
    /// </summary>
    /// <returns>Gyy/mm</returns>
    public static string GetInitialEraJpYearMonth(int yyyymm)
    {
        var y = yyyymm / 100;
        var m = yyyymm % 100;
        if (!IsDate(y, m, 1)) return string.Empty;

        var dt = new DateTime(y, m, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00") + "/" + dt.ToString("MM");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年を正式年号付きで返します。なかった場合、0が返ります。
    /// </summary>
    /// <returns>令和yy</returns>
    public static string GetEraJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }


    /// <summary>
    /// 対応する和暦の年を英頭文字付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetInitialEraJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年を英頭文字付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetInitialEraJpYear(int yyyymm)
    {
        var dt = new DateTime(yyyymm / 100, yyyymm % 100, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }


    /// <summary>
    /// 対応する和暦の年を昭和=3とするJYY形式のint型で返します。
    /// 失敗した場合0が返ります。
    /// </summary>
    public static int GetEraNumberYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Number * 100 + (dt.Year - EraList[i].Difference);
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年を昭和=3とするJYY形式のint型で返します。
    /// 失敗した場合0が返ります。
    /// </summary>
    public static int GetEraNumberYearFromYYYYMM(int yyyyMM)
    {
        if (!DateTimeEx.IsDate(yyyyMM / 100, yyyyMM % 100, 1)) return 0;
        var dt = new DateTime(yyyyMM / 100, yyyyMM % 100, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Number * 100 + (dt.Year - EraList[i].Difference);
        }

        return 0;
    }


    /// <summary>
    /// 年号(1or2文字)から昭和=3とする番号を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="era"></param>
    /// <returns></returns>
    public static int GetEraNumber(string era)
    {
        int number = 0;
        if (era == null || era.Length < 1 || era.Length > 3) return number;
        foreach(var item in EraList)
        {
            if(era.Length == 1)
            {
                if (item.ShortName == era)
                {
                    number = item.Number;
                    break;
                }
            }
            else
            {
                if (item.Name == era)
                {
                    number = item.Number;
                    break;
                }
            }
        }
        return number;
    }


    /// <summary>
    /// 年号(2文字)つきの和暦年から、西暦年を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyear">平成30</param>
    /// <returns>2018</returns>
    public static int GetAdYear(string jyear)
    {
        int year = 0;
        if (jyear.Length < 3) return 0;
        string era = jyear.Remove(2);
        if (!int.TryParse(jyear.Substring(2), out year)) return 0;

        foreach (var item in EraList)
        {
            if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
            {
                if (item.Max < year) return 0;
                return year + item.Difference;
            }
        }
        return 0;
    }


    //20220509094134 furukawa st ////////////////////////
    //平30型を西暦に変換
    
    /// <summary>
    /// 年号(1文字)つきの和暦年から、西暦年を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyear">平30</param>
    /// <returns>2018</returns>
    public static int GetAdYearFromInitial1(string jyear)
    {
        int year = 0;
        if (jyear.Length < 3) return 0;
        string era = jyear.Remove(1);
        if (!int.TryParse(jyear.Substring(1), out year)) return 0;

        foreach (var item in EraList)
        {
            if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
            {
                if (item.Max < year) return 0;
                return year + item.Difference;
            }
        }
        return 0;
    }
    //20220509094134 furukawa ed ////////////////////////


    //20190816112519 furukawa st ////////////////////////
    //月がないため判断できないので関数を削除


    /// <summary>
    /// 昭和=3とした和暦番号と和暦年から、西暦年を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyear">JJyy</param>
    /// <returns></returns>
    //public static int GetAdYearFromEraYear(int eraNumber, int jYear)
    //{

    //20190816110741 furukawa st ////////////////////////
    //平成年は２０～３１、令和１～１９とした

    //    if (jYear >= 20 && jYear < 31) eraNumber = 4;
    //    if (jYear >= 1 && jYear < 20) eraNumber = 5;
    //20190816110741 furukawa ed ////////////////////////


    //    foreach (var item in EraList)
    //    {
    //        if (item.Number == eraNumber)
    //        {
    //            if (item.Max < jYear) return 0;
    //            return jYear + item.Difference;
    //        }
    //    }
    //    return 0;
    //}


    //20190816112519 furukawa ed ////////////////////////



    /// <summary>
    /// 昭和=3とした年号つきの和暦年月から、西暦年月を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyymm">43104型</param>
    /// <returns>201901</returns>
    public static int GetAdYearMonthFromJyymm(int jyymm)
    {
        //20190627094906 furukawa st ////////////////////////
        //令和対応

        if (jyymm <= 10000 || 60000 <= jyymm) return 0;
        //if (jyymm <= 10000 || 50000 <= jyymm) return 0;
        //20190627094906 furukawa ed ////////////////////////


        int era = jyymm / 10000;
        int yymm = jyymm % 10000;

        //20190914215452 furukawa st ////////////////////////
        //明治～昭和、平成・令和で処理分け
        
        //明治～昭和
        if (era>=1 && era <= 3)
        {
            int diff = 0;
            foreach (var item in EraList)
            {
                if (item.Number == era)
                {
                    diff = item.Difference;
                    break;
                }
            }

            if (diff == 0) return 0;
            return yymm += diff * 100;
        }


        //平成、令和
        //単純にEra番号だけでは令和1年5月を判別できないので上とは違う方法にする
        if (era == 4 || era==5)
        {

            //20190924115509 furukawa st ////////////////////////
            //GetAdYearFromHsをやめて独自判断
            

            int diff = 0;
            int y = yymm / 100;
            int m = yymm % 100;
            foreach (var item in EraList)
            {
                if (item.Number == era)
                {
                    switch (era)
                    {
                        case 4:
                            //20191001120311 furukawa st ////////////////////////
                            //平成で来た場合はすべて平成の差を足す
                            
                            diff = EraList[era - 1].Difference;
                            continue;

                        //if (y==31 && m<=4)
                        //{
                        //    //平成で4月以前
                        //    diff = EraList[era-1].Difference;
                        //    continue;                                
                        //}
                        //else if(y==31 && m>=5)
                        //{
                        //    //3106->201906変換するには平成の差を足す
                        //    diff = EraList[era -1].Difference;
                        //    continue;
                        //}
                        //else
                        //{
                        //    diff = EraList[era - 1].Difference;
                        //    continue;
                        //}

                        //20191001120311 furukawa ed ////////////////////////

                        case 5:
                            if (y==1 && m >=5)
                            { 
                                //令和で5月以降
                                diff = EraList[era-1].Difference;
                                continue;
                            }
                            else if (y==1 && m<=4)
                            {
                                //0104->201904 変換するには令和の差を足す
                                diff = EraList[era -1].Difference;
                                continue;
                            }
                            else if (y >= 31)
                            {
                                //多分年号間違いと判断する

                                //20191001113340 furukawa st ////////////////////////
                                //令和31年以降対応
                             
                                //しつこく平成31年入力の場合、平成なので平成の差を足す
                                diff = EraList[era - 2].Difference;
                                continue;
                                //20191001113340 furukawa ed ////////////////////////
                            }
                            else
                            {
                                diff = EraList[era - 1].Difference;
                                continue;
                            }
                    }                   
                }
            }
            if (diff == 0) return 0;
            return yymm += diff * 100;

            

                                //int y = yymm / 100;
                                //int m = yymm % 100;
                                //int ady = GetAdYearFromHs(yymm);
                                //return ady * 100 + m;


            //20190924115509 furukawa ed ////////////////////////

        }

        else return 0;



                    //int diff = 0;
                    //foreach (var item in EraList)
                    //{
                    //    if (item.Number == era)
                    //    {
                    //        diff = item.Difference;
                    //        break;
                    //    }
                    //}

                    //if (diff == 0) return 0;
                    //return yymm += diff * 100;


        //20190914215452 furukawa ed ////////////////////////


    }


    /// <summary>
    /// 西暦年4ケタの文字列を和暦年に変換します。
    /// </summary>
    /// <param name="yearStr4"></param>
    /// <returns></returns>
    public static string GetJpYearStr(string yearStr4)
    {
        int year;
        int.TryParse(yearStr4, out year);
        if (year == 0) return string.Empty;
        var dt = new DateTime(year, 1, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }
        return string.Empty;
    }

    /// <summary>
    /// 西暦年4ケタの文字列を和暦年に変換します。
    /// </summary>
    /// <param name="yearStr4"></param>
    /// <returns></returns>
    public static string GetJpYearStr(int year)
    {
        if (year == 0) return string.Empty;
        var dt = new DateTime(year, 1, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }
        return string.Empty;
    }

    /// <summary>
    /// 年号つきの和暦年から、西暦年を取得します。
    /// </summary>
    /// <param name="jyear">JJyy</param>
    /// <returns></returns>
    public static bool TryGetYear(string jyear, out int year)
    {
        year = 0;
        if (jyear.Length < 3) return false;
        string era = jyear.Remove(2);
        if (!int.TryParse(jyear.Substring(2), out year)) return false;

        int diff = 0;
        foreach (var item in EraList)
        {
            if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
            {
                diff = item.Difference;
                break;
            }
        }
        if (diff == 0)
        {
            year = 0;
            return false;
        }

        year += diff;
        return true;
    }

    /// <summary>
    /// 文字列を日付に変換します。
    /// </summary>
    /// <param name="jDate"></param>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool TryGetYearMonthTimeFromJdate(string jDate, out DateTime dt)
    {
        dt = DateTimeNull;

        //アンダーラインはスペースに
        jDate = jDate.Replace('_', ' ');

        //年月日区切りをスラッシュに
        StringBuilder sb = new StringBuilder();
        const string separator = "年月・.";
        foreach (var c in jDate)
        {
            if (separator.Contains(c)) sb.Append('/');
            else sb.Append(c);
        }
        jDate = sb.ToString();
        sb.Clear();

        //半角に置き換え
        jDate = KanaToHalf(jDate);

        var dateStr = jDate.Split('/');

        //区切りなしの場合は調整
        if (dateStr.Length == 1)
        {
            dateStr = new string[2];
            if (jDate.Length == 6)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(2);
                dateStr[1] = jDate.Substring(2, 2);
            }
            else if (jDate.Length == 7)
            {
                //和暦として扱う
                dateStr[0] = jDate.Remove(3);
                dateStr[1] = jDate.Substring(3, 2);
            }
            else if (jDate.Length == 8)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(4);
                dateStr[1] = jDate.Substring(4, 2);
            }
            else return false;
        }

        if (dateStr.Length < 2) return false;

        //年の数字だけ取出し
        const string numStr = "0123456789";
        foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        string yearNumStr = sb.ToString();

        //数字部分が3ケタ時、1文字目を年号に変換
        if (yearNumStr.Length == 3)
        {
            int eraNum;
            if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
            foreach (var item in EraList)
            {
                if (item.Number == eraNum)
                {
                    dateStr[0] = item.Initial + yearNumStr.Substring(1);
                    yearNumStr = yearNumStr.Substring(1);
                    break;
                }
            }
        }

        int year, month;
        if (!int.TryParse(yearNumStr, out year)) return false;
        if (!int.TryParse(dateStr[1], out month)) return false;

        foreach (var item in EraList)
        {
            if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
            {
                year += item.Difference;
                break;
            }
        }

        if (!IsDate(year, month, 1)) return false;
        dt = new DateTime(year, month, 1);
        return true;
    }

    /// <summary>
    /// 昭和=3をはじめとする7ケタの数字文字列から日付を取得します
    /// 失敗した場合、DateTime.MinValueが返ります
    /// </summary>
    /// <param name="jdateInt7">ex:4310123</param>
    /// <returns>西暦</returns>
    public static DateTime GetDateFromJstr7(string jdate7)
    {
        int jdateInt7;
        if (!int.TryParse(jdate7, out jdateInt7)) return DateTime.MinValue;
        return GetDateFromJInt7(jdateInt7);
    }


    /// <summary>
    /// 昭和=3をはじめとする7ケタの数字から日付を取得します
    /// 失敗した場合、DateTime.MinValueが返ります
    /// </summary>
    /// <param name="jdateInt7">例：4310310</param>
    /// <returns>西暦</returns>
    public static DateTime GetDateFromJInt7(int jdateInt7)
    {
        var eraNum = jdateInt7 / 1000000;
        var y = jdateInt7 / 10000 % 100;

        //20190407165944 furukawa st ////////////////////////
        //43105以上の場合は強制的に５にする
        
        
        //20190627150540 furukawa st ////////////////////////
        //53105対応
        
        if (jdateInt7 / 100 >= 43105 && jdateInt7.ToString().Substring(0,1)!="5")
            //if (jdateInt7 / 100 >= 43105)
        //20190627150540 furukawa ed ////////////////////////


        {
            eraNum = 5;
            y -= 30;
        }

        //20190407165944 furukawa ed ////////////////////////


        var era = EraList.FirstOrDefault(e => e.Number == eraNum);
        if (era == null) return DateTime.MinValue;

        //20190407171355 furukawa st ////////////////////////
        //上で宣言した
        //var y = jdateInt7 / 10000 % 100;
        //20190407171355 furukawa ed ////////////////////////

        y = y + era.Difference;

        var m = jdateInt7 / 100 % 100;
        var d = jdateInt7 % 100;
        
        return IsDate(y, m, d) ? new DateTime(y, m, d) : DateTime.MinValue;
    }

    /// <summary>
    /// 文字列を日付に変換します。
    /// </summary>
    /// <param name="jDate"></param>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool TryGetDateTimeFromJdate(string jDate, out DateTime dt)
    {
        dt = DateTimeNull;

        //アンダーラインはスペースに
        jDate = jDate.Replace('_', ' ');

        //年月日区切りをスラッシュに
        StringBuilder sb = new StringBuilder();
        const string separator = "年月日・.";
        foreach (var c in jDate)
        {
            if (separator.Contains(c)) sb.Append('/');
            else sb.Append(c);
        }
        jDate = sb.ToString();
        sb.Clear();

        //半角に置き換え
        jDate = KanaToHalf(jDate);

        var dateStr = jDate.Split('/');

        //区切りなしの場合は調整
        if (dateStr.Length == 1)
        {
            dateStr = new string[3];
            if (jDate.Length == 6)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(2);
                dateStr[1] = jDate.Substring(2, 2);
                dateStr[2] = jDate.Substring(4, 2);
            }
            else if (jDate.Length == 7)
            {
                //和暦として扱う
                dateStr[0] = jDate.Remove(3);
                dateStr[1] = jDate.Substring(3, 2);
                dateStr[2] = jDate.Substring(5, 2);
            }
            else if (jDate.Length == 8)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(4);
                dateStr[1] = jDate.Substring(4, 2);
                dateStr[2] = jDate.Substring(6, 2);
            }
            else return false;
        }

        if (dateStr.Length < 3) return false;

        //年の数字だけ取出し
        const string numStr = "0123456789";
        foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        string yearNumStr = sb.ToString();

        //数字部分が3ケタ時、1文字目を年号に変換
        if (yearNumStr.Length == 3)
        {
            int eraNum;
            if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
            foreach (var item in EraList)
            {
                if (item.Number == eraNum)
                {
                    dateStr[0] = item.Initial + yearNumStr.Substring(1);
                    yearNumStr = yearNumStr.Substring(1);
                    break;
                }
            }
        }

        int year, month, day;
        if (!int.TryParse(yearNumStr, out year)) return false;
        if (!int.TryParse(dateStr[1], out month)) return false;
        if (!int.TryParse(dateStr[2], out day)) return false;

        foreach (var item in EraList)
        {
            if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
            {
                year += item.Difference;
                break;
            }
        }

        if (!IsDate(year, month, day)) return false;
        dt = new DateTime(year, month, day);
        return true;
    }

    /// <summary>
    /// 正しい日付かどうかチェックします。
    /// </summary>
    public static bool IsDate(int iYear, int iMonth, int iDay)
    {
        if ((DateTime.MinValue.Year > iYear) || (iYear > DateTime.MaxValue.Year))
        {
            return false;
        }

        if ((DateTime.MinValue.Month > iMonth) || (iMonth > DateTime.MaxValue.Month))
        {
            return false;
        }

        int iLastDay = DateTime.DaysInMonth(iYear, iMonth);

        if ((DateTime.MinValue.Day > iDay) || (iDay > iLastDay))
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// 和暦正式文字で表示される日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetJpDateStr(DateTime dt)
    {
        var year = GetEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "年" + dt.Month.ToString("00") + "月" + dt.Day.ToString("00") + "日";
    }

    /// <summary>
    /// 和暦頭文字で表示される日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns>R01/01/01</returns>
    public static string GetShortJpDateStr(DateTime dt)
    {
        var year = GetInitialEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
    }

    /// <summary>
    /// 昭和=3としたJYYMMDD形式のInt型で日付を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns>5021012</returns>
    public static int GetIntJpDateWithEraNumber(DateTime dt)
    {
        var year = GetEraNumberYear(dt);
        if (year == 0) return 0;
        return year * 10000 + dt.Month * 100 + dt.Day;
    }


    /// <summary>
    /// コードまたは番号から、年号を返します。なかった場合空白が返ります。
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string GetEraName(string codeStr)
    {
        codeStr = codeStr.ToUpper();
        int code;
        foreach (var item in EraList)
        {
            if (codeStr.Contains(item.Initial)) return item.Name;
            if (int.TryParse(codeStr, out code)) if (code == item.Number) return item.Name;
        }
        return string.Empty;
    }

    /// <summary>
    /// 4月から始まる、西暦の「年度」を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetTheYear(this DateTime dt)
    {
        int year = dt.Year;
        if (dt.Month < 4) year--;
        return year;
    }

    /// <summary>
    /// 4月から始まる、西暦の「年度」の下2桁を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetTheYear2(this DateTime dt)
    {
        int year = dt.Year;
        if (dt.Month < 4) year--;
        year %= 100;
        return year;
    }

    /// <summary>
    /// 数字8ケタからなる西暦をDateTimeに変換します。
    /// </summary>
    /// <param name="dateInt8">8桁のint</param>
    /// <returns></returns>
    public static DateTime ToDateTime(this int dateInt8)
    {
        int year = dateInt8 / 10000;
        int month = (dateInt8 % 10000) / 100;
        int day = dateInt8 % 100;
        if (!IsDate(year, month, day)) return DateTimeNull;
        return new DateTime(year, month, day);
    }

    /// <summary>
    /// 数字6ケタからなる西暦年月(yyyyMM)を指定された月の1日のDateTimeに変換します。
    /// </summary>
    /// <param name="dateInt6">6桁のint</param>
    /// <returns></returns>
    public static DateTime ToDateTime6(this int dateInt6)
    {
        int year = dateInt6 / 100;
        int month = dateInt6 % 100;
        int day = 1;
        if (!IsDate(year, month, day)) return DateTimeNull;
        return new DateTime(year, month, day);
    }

    /// <summary>
    /// 数字8文字からなる西暦をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
    /// </summary>
    /// <param name="dateStr">8桁int yyyyMMdd</param>
    /// <returns></returns>
    public static DateTime ToDateTime(this string dateStr)
    {
        int dateInt8;
        if (dateStr == null) return DateTimeNull;
        if (!int.TryParse(dateStr, out dateInt8)) return DateTimeNull;

        return ToDateTime(dateInt8);
    }

    /// <summary>
    /// 数字6文字からなる西暦(yyyymm)をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
    /// </summary>
    /// <param name="dateStr6">6桁string yyyyMM</param>
    /// <returns>datetime 1日を自動付与 yyyy/MM/01</returns>
    public static DateTime ToDateTime6(this string dateStr6)
    {
        int dateInt6;
        if (dateStr6 == null) return DateTimeNull;
        if (!int.TryParse(dateStr6, out dateInt6)) return DateTimeNull;

        return ToDateTime6(dateInt6);
    }

    /// <summary>
    /// 数字8ケタのIntに変換します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int ToInt(this DateTime dt)
    {
        int di = dt.Day;
        di = di + dt.Month * 100;
        di = di + dt.Year * 10000;
        return di;
    }

    public static int TointWithNullDate(this DateTime dt)
    {
        if (dt == DateTimeNull) return 0;
        int di = dt.Day;
        di = di + dt.Month * 100;
        di = di + dt.Year * 10000;
        return di;
    }

    /// <summary>
    /// 現在挿入されている日付がNULLかどうかを返します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool IsNullDate(this DateTime dt)
    {
        return dt == DateTimeNull;
    }

    public static string ToJDateShortStr(this DateTime dt)
    {
        return GetShortJpDateStr(dt);
    }

    public static string ToJDateStr(this DateTime dt)
    {
        return GetJpDateStr(dt);
    }

    /// <summary>
    /// 和暦頭文字で表示される年月文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns>H31/04</returns>
    public static string ToJyearMonthShortStr(this DateTime dt)
    {
        var year = GetInitialEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00");
    }

    /// <summary>
    /// 和暦頭文字で表示される年の文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns>H31</returns>
    public static string ToJyearShortStr(this DateTime dt)
    {
        var year = GetInitialEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year;
    }

    /// <summary>
    /// 和暦で表示される年月文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns>平成31年04月</returns>
    public static string ToJyearMonthStr(this DateTime dt)
    {
        var year = GetEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "年" + dt.Month.ToString("00") + "月";
    }

    /// <summary>
    /// 指定された月の1日の日付を返します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static DateTime GetMonthFirstDate(this DateTime dt)
    {
        return new DateTime(dt.Year, dt.Month, 1);
    }

    /// <summary>
    /// 半角にコンバートします。
    /// </summary>
    private static string KanaToHalf(string str)
    {
        return Strings.StrConv(str, VbStrConv.Narrow);
    }



    /// <summary>
    /// 平成年から西暦年を取得します
    /// 失敗した場合、0が返ります
    /// <paramref name="eemm"/>和暦年月(3104,105etc)
    /// </summary>
    /// //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
    /// 31だけでは判別不能なので3104のように月をつける
    public static int GetAdYearFromHs(int eemm)
    {

        //var h = CngWareki(geemm);
        //if (h == null) return 0;

        int y = eemm / 100;
        //return y += h.Difference;
        
        //一カ所差を足すだけでは無理なのですべて計算して戻す
        GetADYearFromWareki(eemm, out y);

        return y;

        //return geemm += h.Difference;
    }

    /*
    public static int GetAdYearFromHs(int hensei)
    {
        if (hensei < 1 || 70 < hensei) return 0;
        var h = EraList.First(e => e.Initial == "H");
        return hensei += h.Difference;
    }
    */

    /// <summary>
    /// 令和対応
    /// </summary>
    /// <param name="eemm">和暦年月</param>
    /// <returns>西暦年</returns>
    private static void GetADYearFromWareki(int eemm,out int cng_y)
    //private static EraJ CngWareki(int geemm,out int cng_y)
    {
        string strgeemm = eemm.ToString().PadLeft(4, '0');

        int y = int.Parse(strgeemm.Substring(0, 2));
        int m = int.Parse(strgeemm.Substring(2, 2));
        cng_y = y;
        EraJ clsEraJ = new EraJ();


        //  int y = int.Parse(geemm.ToString().Substring(0, 2));
        //int m = int.Parse(geemm.ToString().Substring(2, 2));

        //20190725120109 furukawa st ////////////////////////
        //平成年は２０～３１とする               
        if (y >= 20 && y < 31)
                //if (y >= 10 && y < 31)//年が10～30の間は平成
        //20190725120109 furukawa ed ////////////////////////

        {
            //return EraList.First(e => e.Initial == "H");
            clsEraJ = EraList.First(e => e.Initial == "H");
            cng_y = y + clsEraJ.Difference;
            return;
        }

        //年=31、月が4以下は平成
        if (y == 31 && m <= 4)
        {
            // return EraList.First(e => e.Initial == "H");
            clsEraJ = EraList.First(e => e.Initial == "H");
            cng_y = y + clsEraJ.Difference;
            return;
        }

        //年=31、月が５以上は令和
        if (y == 31 && m >= 5)
        {
            //return EraList.First(e => e.Initial == "R")
            clsEraJ = EraList.First(e => e.Initial == "H");            
            y -= (clsEraJ.Max-1);
            EraJ  diff=EraList.First(e=>e.Initial=="R");
            cng_y = y + diff.Difference;
            return;
        }

        //年=1、月が4以下は平成
        if (y == 1 && m <= 4)
        {
            // return EraList.First(e => e.Initial == "H");
            clsEraJ = EraList.First(e => e.Initial == "H");
            y += (clsEraJ.Max - 1);
            EraJ diff = EraList.First(e => e.Initial == "H");
            cng_y = y + clsEraJ.Difference;
            return;
        }

        //年=1、月が5以上は令和 20190422110822 furukawa
        if (y == 1 && m >= 5)
        {
            //return EraList.First(e => e.Initial == "R");
            clsEraJ = EraList.First(e => e.Initial == "R");
            cng_y = y + clsEraJ.Difference;
            return;
        }

        //年>=32以上は令和
        if (y >= 32)
        {
            //return EraList.First(e => e.Initial == "R");

            //20190725160004 furukawa st ////////////////////////
            //32以上はないが、入力するとしたら平成年としてなので、平成の西暦差を足す


            //clsEraJ = EraList.First(e => e.Initial == "R");
            clsEraJ = EraList.First(e => e.Initial == "H");

            //20190725160004 furukawa ed ////////////////////////
            cng_y = y + clsEraJ.Difference;
            return;
        }

        //年が2以上、月が1以上は令和
        if (y > 1 && m >= 1)
        {
            //return EraList.First(e => e.Initial == "R");
            clsEraJ = EraList.First(e => e.Initial == "R");
            cng_y = y + clsEraJ.Difference;
            return;
        }


        //return null;

    }
    //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

    /// <summary>
    /// 西暦年から平成年を取得します
    /// <paramref name="adYear"/>年
    /// <param name="mm">月</param>
    /// </summary>
    public static int GetHsYearFromAd(int adYear,int mm)
    {


        return GetJPYaerFromAD(adYear, mm);


        //var h = EraList.First(e => e.Initial == "H");
        //return adYear -= h.Difference;

    }

    /// <summary>
    /// 西暦年月より和暦年を返す
    /// </summary>
    /// <param name="adYear">西暦年</param>
    /// <param name="mm">月</param>
    /// <returns>和暦年</returns>
    private static int GetJPYaerFromAD(int adYear,int mm)
    {
        
        EraJ era=new EraJ();

        if (adYear == 2019)
        {
            if(mm < 5) era = EraList.First(e => e.Initial == "H");
            if(mm >= 5) era = EraList.First(e => e.Initial == "R");
        }

        //20190725133500 furukawa st ////////////////////////
        //2019年以外は通常判定
        
        //else if (adYear < 2019)
        //{
        //    era = EraList.Last(e => e.Difference < adYear);

        //}
        else
        {
            era = EraList.Last(e => e.Difference < adYear);
        }
        //20190725133500 furukawa ed ////////////////////////
        return adYear -= era.Difference;

        
    }

    /// <summary>
    /// 西暦年月から昭和=3とする年号つき和暦年月を取得します
    /// </summary>
    public static int GetGyymmFromAdYM(int adYearMonth)
    {
        var y = adYearMonth / 100;
        var m = adYearMonth % 100;
        var dt = new DateTime(y, m, 1);
        var g = GetEraNumber(dt);
        var jy = GetJpYear(dt);

        return g * 10000 + jy * 100 + m;
    }

    /// <summary>
    /// 年齢を取得します。
    /// birthには誕生日を、dateには誕生日以降の日付を指定してください。
    /// dateがbirthよりも下回っている場合は-1を返します。
    /// </summary>
    /// <param name="birth"></param>
    /// <param name="date"></param>
    /// <returns></returns>
    public static int GetAge(DateTime birth, DateTime date)
    {
        if (date < birth) return -1;
        int age = date.Year - birth.Year;
        if (date.Month < birth.Month ||
            (date.Month == birth.Month &&
            date.Day < birth.Day))
        {
            age--;//誕生日がまだ来ていなければ、1引く
        }

        return age;
    }
    public static int GetAge(DateTime birth, int ym)
    {
        int y = ym / 100;
        int m = ym % 100;
        if (!IsDate(y, m, 1)) return -1;

        var dt = new DateTime(y, m, 1);
        if (dt < birth) return -1;

        int age = dt.Year - birth.Year;
        if (dt.Month < birth.Month ||
            (dt.Month == birth.Month &&
            dt.Day < birth.Day))
        {
            age--;//誕生日がまだ来ていなければ、1引く
        }

        return age;
    }

    /// <summary>
    /// 指定年齢の日付を返します。０未満はDateTimeNullを返します。
    /// </summary>
    /// <param name="age"></param>
    /// <returns></returns>
    public static DateTime GetDateFromAge(int age)
    {
        if (age < 0) return DateTimeNull;
        var now = DateTime.Today;
        var year = now.Year;
        var targetYear = year - age;
        var result = new DateTime(targetYear, now.Month, now.Day);
        return result;
    }

    /// <summary>
    /// 西暦yyyymm形式(201601)のintに指定された月を追加します
    /// </summary>
    /// <param name="yyyymm"></param>
    /// <param name="addmonth"></param>
    /// <returns></returns>
    public static int Int6YmAddMonth(int yyyymm, int addmonth)
    {
        int m = yyyymm % 100;
        int addy = addmonth / 12;
        int addm = addmonth % 12;

        yyyymm += addy * 100;
        yyyymm += addm;
        if (12 < m + addm) yyyymm += 88;
        if (m + addm < 1) yyyymm -= 88;

        return yyyymm;
    }

    /// <summary>
    /// 当月における前月(eemm)を返します。※平成→令和 対応版
    /// </summary>
    /// <param name="e"></param>
    /// <param name="m"></param>
    /// <returns></returns>
    public static string GetPrevEEMM_ChangeGengo(int e, int m)
    {
        // (令和) 0105 => (平成) 3104
        if (e == 1 && m == 5)
        {
            return "3104";
        }

        //それ以外なら普通にマイナス一月
        var eemm2 = "";
        if (m == 1) eemm2 = ((e - 1) * 100 + 12).ToString("0000");
        else eemm2 = (e * 100 + (m - 1)).ToString("0000");
        return eemm2;
    }
}
