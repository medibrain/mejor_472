﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Mejor
{
    public enum InputMode { Input, Verify, MatchCheck }

    static class CommonTool
    {

        /// <summary>
        /// //20220215171017 furukawa CSVをBeginTextImportで一気に取り込む
        /// </summary>
        /// <param name="strFileName">csvファイル名</param>
        /// <param name="strCopyFromCommand">COPYFROMコマンド</param>
        /// <returns></returns>
        public static bool CsvDirectImportToTable(string strFileName,string strCopyFromCommand,string strEncode="utf-8")
        {
            Npgsql.NpgsqlConnection cn = DB.Main.getConn();
            System.IO.TextWriter writer=null;
            System.IO.StreamReader reader=null;
            try
            {
                cn.Open();
                writer = cn.BeginTextImport(strCopyFromCommand);
                reader= new System.IO.StreamReader(strFileName, System.Text.Encoding.GetEncoding(strEncode));
                writer.Write(reader.ReadToEnd());
                
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                writer.Close();
                reader.Close();

                cn.Close();
            }
        }

        /// <summary>
        /// 20220217092004 furukawa テーブルからcsvファイルに直接出力        
        /// </summary>
        /// <param name="strFileName">csvファイル名</param>
        /// <param name="strCopyToCommand">COPYTOコマンド</param>
        /// <param name="strEnc">エンコード</param>
        /// <returns></returns>
        
        
        //20220708160157 furukawa st ////////////////////////
        //文字コード引数追加        
        public static bool CsvDirectExportFromTable(string strFileName, string strCopyToCommand,string strEnc="utf-8")
        //      public static bool CsvDirectExportFromTable(string strFileName, string strCopyToCommand)
        //20220708160157 furukawa ed ////////////////////////
        {
            Npgsql.NpgsqlConnection cn = DB.Main.getConn();
            System.IO.TextReader reader = null;
            System.IO.StreamWriter writer = null;

            try
            {
                cn.Open();
                reader = cn.BeginTextExport(strCopyToCommand);

                //20220708161136 furukawa st ////////////////////////
                //文字コードは引数から取得
                writer = new System.IO.StreamWriter(strFileName, false,System.Text.Encoding.GetEncoding(strEnc));
                //  writer = new System.IO.StreamWriter(strFileName, false,System.Text.Encoding.GetEncoding("utf-8"));
                //20220708161136 furukawa ed ////////////////////////

                writer.Write(reader.ReadToEnd());

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                if(writer!=null)writer.Close();
                if(reader!=null)reader.Close();

                cn.Close();
            }
        }



        #region CSVインポート
        public static List<string[]> CsvImportShiftJis(string fileName)
        {
            //Shift JISで読み込む
            using (var tfp = new TextFieldParser(fileName, Encoding.GetEncoding(932)))
            {
                //フィールドが文字で区切られているとする。
                tfp.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                //区切り文字を,とする
                tfp.Delimiters = new string[] { "," };
                //フィールドを"で囲み、改行文字、区切り文字を含めることができるか
                tfp.HasFieldsEnclosedInQuotes = true;
                //フィールドの前後からスペースを削除する。
                tfp.TrimWhiteSpace = true;
                var l = new List<string[]>();

                while (!tfp.EndOfData)
                {
                    //フィールドを読み込む
                    l.Add(tfp.ReadFields());
                }

                return l;
            }
        }

        public static List<string[]> CsvImportShiftJisAllRecordByRecordSplitCRLFAndValueSplitComma(string fileName)
        {
            var list = new List<string[]>();
            try
            {
                var csv = System.IO.File.ReadAllText(fileName, Encoding.GetEncoding("Shift_JIS"));
                var rows = csv.Replace("\r", "").Split('\n');
                string[] fields;
                foreach (var record in rows)
                {
                    fields = record.Split(',');
                    list.Add(fields);
                }
            }
            catch (System.Exception e)
            {
                // ファイルを開くのに失敗したとき
                Log.ErrorWriteWithMsg("CSV読込に失敗しました");
                Log.ErrorWrite(e);
                return null;
            }
            return list;
        }

        public static List<string[]> CsvImportUTF8(string fileName)
        {
            //UTF8で読み込む
            using (var tfp = new TextFieldParser(fileName, Encoding.UTF8))
            {
                //フィールドが文字で区切られているとする。
                tfp.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                //区切り文字を,とする
                tfp.Delimiters = new string[] { "," };
                //フィールドを"で囲み、改行文字、区切り文字を含めることができるか
                tfp.HasFieldsEnclosedInQuotes = true;
                //フィールドの前後からスペースを削除する。
                tfp.TrimWhiteSpace = true;
                var l = new List<string[]>();

                while (!tfp.EndOfData)
                {
                    //フィールドを読み込む
                    l.Add(tfp.ReadFields());
                }

                return l;
            }
        }

        /// <summary>
        /// CSVインポート 文字コード自動判定
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        /// <returns></returns>
        public static List<string[]> CsvImportMultiCode(string fileName)
        {

            //20191107130603 furukawa st ////////////////////////
            //メモリに入り切らないCSVは、上から100行で判断する

            byte[] bs = new byte[] { };
            try
            {
                bs = System.IO.File.ReadAllBytes(fileName);
            }
            catch
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                string tmpStr = string.Empty;

                for (int r = 0; r < 100; r++)
                {
                    tmpStr += sr.ReadLine();
                }
                bs = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(tmpStr);
                sr.Close();
            }
            //byte[] bs = System.IO.File.ReadAllBytes(fileName);

            //20191107130603 furukawa ed ////////////////////////



            System.Text.Encoding enc = GetCode(bs);



            //取得した文字コードで読み込む
            using (var tfp = new TextFieldParser(fileName, enc))
            {
                //フィールドが文字で区切られているとする。
                tfp.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                //区切り文字を,とする
                tfp.Delimiters = new string[] { "," };
                //フィールドを"で囲み、改行文字、区切り文字を含めることができるか
                tfp.HasFieldsEnclosedInQuotes = true;
                //フィールドの前後からスペースを削除する。
                tfp.TrimWhiteSpace = true;
                var l = new List<string[]>();

                while (!tfp.EndOfData)
                {
                    //フィールドを読み込む
                    l.Add(tfp.ReadFields());
                }

                return l;
            }
        }
        #endregion


        /// <summary>
        /// //20220411093659 furukawa ファイルの文字コード調査        
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <returns></returns>
        public static System.Text.Encoding CSVEncodingCheck(string strFileName)
        {

            byte[] bs = new byte[] { };
            try
            {
                bs = System.IO.File.ReadAllBytes(strFileName);
            }
            catch
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(strFileName);
                string tmpStr = string.Empty;

                for (int r = 0; r < 100; r++)
                {
                    tmpStr += sr.ReadLine();
                }
                bs = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(tmpStr);
                sr.Close();
            }
            System.Text.Encoding enc = GetCode(bs);
            return enc;
        }

        /// <summary>
        /// 20220208103843 furukawa CSVインポート 文字コード自動判定列数制限        
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="lst"></param>
        /// <param name="FieldCount"></param>
        /// <returns></returns>
        public static bool CsvImportMultiCodeLimitCols(string fileName, out List<string[]> lst, int FieldCount =0)
        {

            //メモリに入り切らないCSVは、上から100行で判断する

            byte[] bs = new byte[] { };
            try
            {
                bs = System.IO.File.ReadAllBytes(fileName);
            }
            catch
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                string tmpStr = string.Empty;

                for (int r = 0; r < 100; r++)
                {
                    tmpStr += sr.ReadLine();
                }
                bs = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(tmpStr);
                sr.Close();
            }
            System.Text.Encoding enc = GetCode(bs);

            //取得した文字コードで読み込む
            using (var tfp = new TextFieldParser(fileName, enc))
            {
                //フィールドが文字で区切られているとする。
                tfp.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                //区切り文字を,とする
                tfp.Delimiters = new string[] { "," };
                //フィールドを"で囲み、改行文字、区切り文字を含めることができるか
                tfp.HasFieldsEnclosedInQuotes = true;
                //フィールドの前後からスペースを削除する。
                tfp.TrimWhiteSpace = true;
                var l = new List<string[]>();

                while (!tfp.EndOfData)
                {
                    //フィールドを読み込む
                    l.Add(tfp.ReadFields());
                }

                //列数確認
                if (FieldCount > 0)
                {
                    if(l[1].Length!=FieldCount)
                    {
                        MessageBox.Show($"インポートするファイルの列数が{FieldCount}ではありません。ファイルの中身を確認してください",
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        lst = null;
                        return false;
                    }
                }

                lst = l;
                return true;
            }
        }



        //20210209165050 furukawa st ////////////////////////
        /// <summary>
        /// 請求金額計算（合計×割合/10　AND　小数点以下切り捨て）
        /// 合計×給付割合で請求金額を算出する場合、計算はDecimal型でやらないと丸め誤差出るのでかなり注意
        /// </summary>
        /// <param name="total">合計</param>
        /// <param name="ratio">割合7～10 or 70～100</param>
        /// <returns></returns>
        public static int CalcCharge(int total,int ratio)
        {
            //20210303104319 furukawa st ////////////////////////
            //給付割合が７０％表記でも割合に直す
            
            int input_ratio = ratio > 10 ? ratio / 10 : ratio;

            decimal decCharge =decimal.Parse(total.ToString()) * decimal.Parse(input_ratio.ToString()) / 10;
            //      decimal decCharge = decimal.Parse(total.ToString()) * decimal.Parse(ratio.ToString()) / 10;
            
            
            //20210303104319 furukawa ed ////////////////////////


            //小数点以下切り捨て
            decimal tmpdecCharge = Math.Truncate(decCharge);

            int retCharge=int.Parse(tmpdecCharge.ToString());
            return retCharge;
        }
        //20210209165050 furukawa ed ////////////////////////


        #region 20190227132029 furukawa 文字コードを判別する関数webからコピー

        /// <summary>
        /// 文字コードを判別する
        /// </summary>
        /// <remarks>
        /// Jcode.pmのgetcodeメソッドを移植したものです。
        /// Jcode.pm(http://openlab.ring.gr.jp/Jcode/index-j.html)
        /// Jcode.pmの著作権情報
        /// Copyright 1999-2005 Dan Kogai <dankogai@dan.co.jp>
        /// This library is free software; you can redistribute it and/or modify it
        ///  under the same terms as Perl itself.
        /// </remarks>
        /// <param name="bytes">文字コードを調べるデータ</param>
        /// <returns>適当と思われるEncodingオブジェクト。
        /// 判断できなかった時はnull。</returns>
        public static System.Text.Encoding GetCode(byte[] bytes)
        {
            const byte bEscape = 0x1B;
            const byte bAt = 0x40;
            const byte bDollar = 0x24;
            const byte bAnd = 0x26;
            const byte bOpen = 0x28;    //'('
            const byte bB = 0x42;
            const byte bD = 0x44;
            const byte bJ = 0x4A;
            const byte bI = 0x49;

            int len = bytes.Length;
            byte b1, b2, b3, b4;

            //Encode::is_utf8 は無視

            bool isBinary = false;
            for (int i = 0; i < len; i++)
            {
                b1 = bytes[i];
                if (b1 <= 0x06 || b1 == 0x7F || b1 == 0xFF)
                {
                    //'binary'
                    isBinary = true;
                    if (b1 == 0x00 && i < len - 1 && bytes[i + 1] <= 0x7F)
                    {
                        //smells like raw unicode
                        return System.Text.Encoding.Unicode;
                    }
                }
            }
            if (isBinary)
            {
                return null;
            }

            //not Japanese
            bool notJapanese = true;
            for (int i = 0; i < len; i++)
            {
                b1 = bytes[i];
                if (b1 == bEscape || 0x80 <= b1)
                {
                    notJapanese = false;
                    break;
                }
            }
            if (notJapanese)
            {
                return System.Text.Encoding.ASCII;
            }

            for (int i = 0; i < len - 2; i++)
            {
                b1 = bytes[i];
                b2 = bytes[i + 1];
                b3 = bytes[i + 2];

                if (b1 == bEscape)
                {
                    if (b2 == bDollar && b3 == bAt)
                    {
                        //JIS_0208 1978
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                    else if (b2 == bDollar && b3 == bB)
                    {
                        //JIS_0208 1983
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                    else if (b2 == bOpen && (b3 == bB || b3 == bJ))
                    {
                        //JIS_ASC
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                    else if (b2 == bOpen && b3 == bI)
                    {
                        //JIS_KANA
                        //JIS
                        return System.Text.Encoding.GetEncoding(50220);
                    }
                    if (i < len - 3)
                    {
                        b4 = bytes[i + 3];
                        if (b2 == bDollar && b3 == bOpen && b4 == bD)
                        {
                            //JIS_0212
                            //JIS
                            return System.Text.Encoding.GetEncoding(50220);
                        }
                        if (i < len - 5 &&
                            b2 == bAnd && b3 == bAt && b4 == bEscape &&
                            bytes[i + 4] == bDollar && bytes[i + 5] == bB)
                        {
                            //JIS_0208 1990
                            //JIS
                            return System.Text.Encoding.GetEncoding(50220);
                        }
                    }
                }
            }

            //should be euc|sjis|utf8
            //use of (?:) by Hiroki Ohzaki <ohzaki@iod.ricoh.co.jp>
            int sjis = 0;
            int euc = 0;
            int utf8 = 0;
            for (int i = 0; i < len - 1; i++)
            {
                b1 = bytes[i];
                b2 = bytes[i + 1];
                if (((0x81 <= b1 && b1 <= 0x9F) || (0xE0 <= b1 && b1 <= 0xFC)) &&
                    ((0x40 <= b2 && b2 <= 0x7E) || (0x80 <= b2 && b2 <= 0xFC)))
                {
                    //SJIS_C
                    sjis += 2;
                    i++;
                }
            }
            for (int i = 0; i < len - 1; i++)
            {
                b1 = bytes[i];
                b2 = bytes[i + 1];
                if (((0xA1 <= b1 && b1 <= 0xFE) && (0xA1 <= b2 && b2 <= 0xFE)) ||
                    (b1 == 0x8E && (0xA1 <= b2 && b2 <= 0xDF)))
                {
                    //EUC_C
                    //EUC_KANA
                    euc += 2;
                    i++;
                }
                else if (i < len - 2)
                {
                    b3 = bytes[i + 2];
                    if (b1 == 0x8F && (0xA1 <= b2 && b2 <= 0xFE) &&
                        (0xA1 <= b3 && b3 <= 0xFE))
                    {
                        //EUC_0212
                        euc += 3;
                        i += 2;
                    }
                }
            }
            for (int i = 0; i < len - 1; i++)
            {
                b1 = bytes[i];
                b2 = bytes[i + 1];
                if ((0xC0 <= b1 && b1 <= 0xDF) && (0x80 <= b2 && b2 <= 0xBF))
                {
                    //UTF8
                    utf8 += 2;
                    i++;
                }
                else if (i < len - 2)
                {
                    b3 = bytes[i + 2];
                    if ((0xE0 <= b1 && b1 <= 0xEF) && (0x80 <= b2 && b2 <= 0xBF) &&
                        (0x80 <= b3 && b3 <= 0xBF))
                    {
                        //UTF8
                        utf8 += 3;
                        i += 2;
                    }
                }
            }
            //M. Takahashi's suggestion
            //utf8 += utf8 / 2;

            System.Diagnostics.Debug.WriteLine(
                string.Format("sjis = {0}, euc = {1}, utf8 = {2}", sjis, euc, utf8));
            if (euc > sjis && euc > utf8)
            {
                //EUC
                return System.Text.Encoding.GetEncoding(51932);
            }
            else if (sjis > euc && sjis > utf8)
            {
                //SJIS
                return System.Text.Encoding.GetEncoding(932);
            }
            else if (utf8 > euc && utf8 > sjis)
            {
                //UTF8
                return System.Text.Encoding.UTF8;
            }

            return null;
        }

        #endregion



        /// <summary>
        /// ShiftJISでのバイト数を取得します
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int GetByteLength(this string str)
        {
            return System.Text.Encoding.GetEncoding("Shift_JIS").GetByteCount(str);
        }


        #region 本番サーバでない場合、フォーム背景色をThistleにする 20190424092509 furukawa 
        /// <summary>
        /// 本番サーバでない場合、フォーム背景色を変更
        /// </summary>
        /// <param name="frm">フォーム</param>
        public static void setFormColor(Form frm)
        {

            //20200531135143 furukawa st ////////////////////////
            //サーバごとに定義を持たせたのでそこから取る

            string strcolor = Settings.dsSettings.Tables[Settings.CurrentServer].Rows[0]["color"].ToString();
            System.Drawing.Color c = new System.Drawing.Color();
            c = strcolor == string.Empty ?
                System.Drawing.SystemColors.Control : System.Drawing.ColorTranslator.FromHtml(strcolor);
            System.Drawing.Color clr = c;


            //20200313173324 furukawa st ////////////////////////
            //サーバIPをSettings.xmlからロードし判定する
            //System.Drawing.Color clr = Settings.IsEnvironment(DB.getDBHost()) != Settings.mejor_enviroment.Production ? 
            //    clr = System.Drawing.Color.Thistle : System.Drawing.SystemColors.Control;


            //string strcolor= Settings.dsSettings.Tables[DB.getDBHost()].Rows[0]["color"].ToString();


            ////３パターンになったので書き直し
            //switch (Settings.IsEnvironment(DB.getDBHost()))
            //{
            //    case Settings.mejor_enviroment.Production:
            //        clr = System.Drawing.Color.Honeydew;

            //        clr = System.Drawing.ColorTranslator.FromHtml("#FFF0FFF0");


            //        break;
            //    case Settings.mejor_enviroment.InternalServer:
            //        clr = System.Drawing.SystemColors.Control;
            //        break;
            //    case Settings.mejor_enviroment.Test:
            //        clr = System.Drawing.Color.Thistle;
            //        break;
            //    default:
            //        break;
            //}

            //20200131141424 furukawa st ////////////////////////
            //サーバIPは保険者によって変わるので、connectionstringから取得に変更


            //System.Drawing.Color clr =
            //    !DB.getDBConnection().ToString().Contains("192.168.200.100") ? System.Drawing.Color.Thistle : System.Drawing.SystemColors.Control;

            //本番サーバでない場合、フォーム背景色をThistleにする
            //System.Drawing.Color clr = 
            //    DB.getDBHost() != "192.168.200.100" ? System.Drawing.Color.Thistle : System.Drawing.SystemColors.Control;

            //20200131141424 furukawa ed ////////////////////////

            //20200313173324 furukawa ed ////////////////////////


            //20200531135143 furukawa ed ////////////////////////

            frm.BackColor = clr;
        }
        #endregion


        /// <summary>
        /// 20201221160211 furukawa WaitFormのキャンセルボタン押下時の処理        
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="tran"></param>
        /// <returns>キャンセル＝True,キャンセルしない（継続）=false</returns>
        public static bool WaitFormCancelProcess(WaitForm wf,DB.Transaction tran = null)
        {
            if (wf.Cancel)
            {
                if (System.Windows.Forms.MessageBox.Show("中止しますか？", "Mejor",
                   System.Windows.Forms.MessageBoxButtons.YesNo,
                   System.Windows.Forms.MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    if (tran != null) tran.Rollback();
                    return true;
                }
                else
                {
                    wf.Cancel = false;
                    return false;
                }

            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 20210303091314 furukawa 受診者区分から請求金額をチェックする
        /// </summary>
        /// <param name="input_total">入力画面の合計金額</param>
        /// <param name="input_JyushinshaKbn">入力画面の受診者区分</param>
        /// <param name="input_charge">入力画面の請求金額</param>
        /// <returns>計算値と入力画面の請求が一致=true</returns>
        public static bool CheckChargeFromKbn(int input_total,int input_JyushinshaKbn, int input_charge)
        {
            int ratio =-1;

            //シャープ健保より

            //本家区分	給付割合
            //2:本人		7
            //4:六歳		8
            //6:家族		7
            //8:高一		8
            //0:高7			7
            //受診者区分
            switch (input_JyushinshaKbn)
            {
                case 2:
                case 6:
                case 0:
                    ratio = 7;
                    break;

                case 4:
                case 8:
                    ratio = 8;
                    break;


                default:
                    break;
            }

            int calc_charge = 0;
            calc_charge=CalcCharge(input_total, ratio);

            if (input_charge == calc_charge) return true;
            else return false;

        }



        /// <summary>
        /// 20210303091439 furukawa 給付割合から請求金額をチェックする
        /// </summary>
        /// <param name="input_total">入力画面の合計金額</param>
        /// <param name="input_JyushinshaKbn">入力画面の受診者区分</param>
        /// <param name="input_charge">入力画面の請求金額</param>
        /// <returns>計算値と入力画面の請求が一致=true</returns>
        public static bool CheckChargeFromRatio(int input_total, int input_Ratio, int input_charge)
        {
            int ratio = input_Ratio;            

            decimal calc_charge = 0;
            calc_charge = CalcCharge(input_total, ratio);

            if (input_charge == calc_charge) return true;
            else return false;

        }


        //20210721180850 furukawa st ////////////////////////
        //バイト単位で文字列を切り出す
        
        /// <summary>
        /// バイト単位で文字列を切り出します
        /// </summary>
        /// <param name="strOrig">元の文字列</param>
        /// <param name="limitLength">切り出すバイト数</param>
        /// <param name="e">エンコード</param>
        /// <returns></returns>
        public static string SubStringByBytes(string strOrig,int limitLength,System.Text.Encoding e)
        {
            string strRet = string.Empty;
            byte[] b = e.GetBytes(strOrig);
            byte[] newb = new byte[limitLength];
            if (b.Length > limitLength)
            {
                for (int bytecnt = 0; bytecnt < limitLength; bytecnt++)
                {
                    newb[bytecnt] = b[bytecnt];
                }

                //最後のバイトが文字にならない場合切る
                if (!char.TryParse(e.GetString(b).Substring(limitLength, 1), out char tmpchar))
                {
                    Array.Resize(ref newb, limitLength - 1);                    
                }

              
                return System.Text.Encoding.GetEncoding("shift-jis").GetString(newb);
            }
            else
            {
                return strOrig;
            }
        }
        //20210721180850 furukawa ed ////////////////////////


        /// <summary>
        /// メホール再起動 //20210128101110 furukawa 再起動しないと変更が適用されない場合に使う
        /// </summary>
        public static void Restart_Mejor()
        {
            MessageBox.Show("Mejorを再起動します");
            //プログラム終了
            try
            {
                Application.Exit();
            }
            catch
            {
                try
                {
                    //強制終了
                    Environment.Exit(0);
                }
                catch
                {
                    //握りつぶす
                }
            }

            //新プログラム起動
            System.Diagnostics.Process.Start(Application.ExecutablePath);
        }

        //20210607150934 furukawa st ////////////////////////
        //拗音があった場合半角カナに置換後を返す。無かった場合はそのまま返す
        
        /// <summary>
        /// 拗音置換　拗音があった場合半角カナに置換後を返す。無かった場合はそのまま返す
        /// </summary>
        /// <param name="strBeforeReplace"></param>
        /// <returns></returns>
        public static string ReplaceContract(string strBeforeReplace)
        {

            string[] arrBefore = { "ｧ", "ｨ", "ｩ", "ｪ", "ｫ", "ｬ", "ｭ", "ｮ", "ｯ","　" };            //20210618183341 furukawa 全角スペースを半角に追加            
            string[] arrAfter = { "ｱ", "ｲ", "ｳ", "ｴ", "ｵ", "ﾔ", "ﾕ", "ﾖ", "ﾂ"," " };

            //拗音があった場合置換後を返す
            if (System.Text.RegularExpressions.Regex.IsMatch(strBeforeReplace, "[ｧ-ｯ　]"))
            {
                string strReplace = strBeforeReplace;
                for (int c = 0; c < strBeforeReplace.Length; c++)
                {
                    for (int repCnt = 0; repCnt < arrBefore.Length; repCnt++)
                    {
                        if (strBeforeReplace.Substring(c, 1) == arrBefore[repCnt])
                        {
                            strReplace = strReplace.Replace(arrBefore[repCnt], arrAfter[repCnt]);
                        }
                    }

                }

                return strReplace;
            }
            else
            {
                return strBeforeReplace;
            }
        }
        //20210607150934 furukawa ed ////////////////////////


        //20210610140113 furukawa st ////////////////////////
        //DPIに応じた拡大率を返す        
        /// <summary>
        /// DPIに応じた拡大率を返す        
        /// </summary>
        /// <param name="strFileName">画像ファイル名</param>
        /// <returns>250DPI以上0.4f,200-249 0.6f,100-199 0.8f それ以下1.0f</returns>
        public static float AutoAdjustScreen(System.Drawing.Image img)
        {
            float hres = img.HorizontalResolution;


            if (hres > 250)
            {
                return 0.4f;
            }
            else if (hres >= 200 && hres < 250)
            {
                return 0.6f;
            }
            else if (hres >= 100 && hres < 200)
            {
                return 0.8f;
            }
            else
            {
                return 1.0f;
            }
        }
        //20210610140113 furukawa ed ////////////////////////

        //20210713213625 furukawa st ////////////////////////
        //NPOIのセル取得関数を共通で使えるように
        
        /// <summary>
        /// NPOIのセル値を取得string/numericのみ。その他はstring
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }
        //20210713213625 furukawa ed ////////////////////////


        #region 未使用
        //public static void LoadAppTextFromNumber(string strSQL, VerifyBox verifyBox)
        //{
        //    DB.Command cmd = new DB.Command(DB.Main, strSQL);
        //    var res = cmd.TryExecuteScalar();
        //    if (res != null) verifyBox.Text = res.ToString();

        //}
        #endregion



    }

    /// <summary>
    /// フォルダ指定ダイアログ
    /// </summary>
    public class OpenDirectoryDiarog
    {
        /// <summary>
        /// フォルダ名
        /// </summary>
        public string Name { get; private set; } = string.Empty;

        public DialogResult ShowDialog()
        {
            using (var f = new CommonOpenFileDialog())
            {
                // フォルダーを開く設定に
                f.IsFolderPicker = true;

                // 読み取り専用フォルダ/コントロールパネルは開かない
                f.EnsureReadOnly = false;
                f.AllowNonFileSystemItems = false;
                var ptr = Form.ActiveForm?.Handle ?? IntPtr.Zero;
                var res = (ptr == IntPtr.Zero) ? f.ShowDialog() : f.ShowDialog(ownerWindowHandle: ptr);

                if (res == CommonFileDialogResult.Ok)
                {
                    Name = f.FileName;
                    return DialogResult.OK;
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }
    }

    
  

}

