﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.kyokaikenpo

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class exportdata
    {

        static string strPrefID = string.Empty;

        
        #region テーブル構造
        [DB.DbAttribute.PrimaryKey]
        public int aid{ get; set; } = 0;                   //申請書のAID;

        public string F001renban { get;set; }=string.Empty;                   //ファイルごと連番;
        public string F002cym { get; set; } = string.Empty;                   //請求年月;
        public string F003seq { get; set; } = string.Empty;                   //シーケンスNO;
        public string F004fuken { get; set; } = string.Empty;                 //府県コード;
        public string F005jcd { get; set; } = string.Empty;                   //柔整師会コード;
        public string F006hihokigo { get; set; } = string.Empty;              //被保険者証記号;
        public string F007hihonum { get; set; } = string.Empty;               //被保険者証番号;
        public string F008hoken { get; set; } = string.Empty;                 //保険種別;
        public string F009tanhei { get; set; } = string.Empty;                //単併区分;
        public string F010honke { get; set; } = string.Empty;                 //本家区分;
        public string F011ratio { get; set; } = string.Empty;                 //給付割合;
        public string F012pname { get; set; } = string.Empty;                 //受診者氏名;
        public string F013 { get; set; } = string.Empty;                      //郵便番号;
        public string F014 { get; set; } = string.Empty;                      //住所1;
        public string F015 { get; set; } = string.Empty;                      //住所2;
        public string F016 { get; set; } = string.Empty;                      //住所3;
        public string F017 { get; set; } = string.Empty;                      //電話番号;
        public string F018psex { get; set; } = string.Empty;                  //性別;
        public string F019pbirthday { get; set; } = string.Empty;             //受診者生年月日;
        public string F020ym { get; set; } = string.Empty;                    //診療年月;
        public string F021counteddays { get; set; } = string.Empty;           //診療日数;
        public string F022bui { get; set; } = string.Empty;                   //部位数;
        public string F023 { get; set; } = string.Empty;                      //負傷原因記入有無;
        public string F024 { get; set; } = string.Empty;                      //負傷名1;
        public string F025 { get; set; } = string.Empty;                      //負傷年月日1;
        public string F026ifirstdate1 { get; set; } = string.Empty;           //初検日1;
        public string F027istartdate1 { get; set; } = string.Empty;           //開始日1;
        public string F028ifinishdate1 { get; set; } = string.Empty;          //終了日1;
        public string F029idays1 { get; set; } = string.Empty;                //日数1;
        public string F030 { get; set; } = string.Empty;                      //負傷名2;
        public string F031 { get; set; } = string.Empty;                       //負傷年月日2;
        public string F032 { get; set; } = string.Empty;                       //初検日2;
        public string F033 { get; set; } = string.Empty;                       //開始日2;
        public string F034 { get; set; } = string.Empty;                       //終了日2;
        public string F035 { get; set; } = string.Empty;                       //日数2;
        public string F036 { get; set; } = string.Empty;                       //負傷名3;
        public string F037 { get; set; } = string.Empty;                       //負傷年月日3;
        public string F038 { get; set; } = string.Empty;                       //初検日3;
        public string F039 { get; set; } = string.Empty;                       //開始日3;
        public string F040 { get; set; } = string.Empty;                       //終了日3;
        public string F041 { get; set; } = string.Empty;                       //日数3;
        public string F042 { get; set; } = string.Empty;                       //負傷名4;
        public string F043 { get; set; } = string.Empty;                       //負傷年月日4;
        public string F044 { get; set; } = string.Empty;                       //初検日4;
        public string F045 { get; set; } = string.Empty;                       //開始日4;
        public string F046 { get; set; } = string.Empty;                       //終了日4;
        public string F047 { get; set; } = string.Empty;                       //日数4;
        public string F048 { get; set; } = string.Empty;                       //負傷名5;
        public string F049 { get; set; } = string.Empty;                       //負傷年月日5;
        public string F050 { get; set; } = string.Empty;                       //初検日5;
        public string F051 { get; set; } = string.Empty;                       //開始日5;
        public string F052 { get; set; } = string.Empty;                       //終了日5;
        public string F053 { get; set; } = string.Empty;                       //日数5;
        public string F054 { get; set; } = string.Empty;                       //6部位以上有無;
        public string F055 { get; set; } = string.Empty;                       //経過記入有無;
        public string F056 { get; set; } = string.Empty;                       //施術日1;
        public string F057 { get; set; } = string.Empty;                       //施術日2;
        public string F058 { get; set; } = string.Empty;                       //施術日3;
        public string F059 { get; set; } = string.Empty;                       //施術日4;
        public string F060 { get; set; } = string.Empty;                       //施術日5;
        public string F061 { get; set; } = string.Empty;                       //施術日6;
        public string F062 { get; set; } = string.Empty;                       //施術日7;
        public string F063 { get; set; } = string.Empty;                       //施術日8;
        public string F064 { get; set; } = string.Empty;                       //施術日9;
        public string F065 { get; set; } = string.Empty;                       //施術日10;
        public string F066 { get; set; } = string.Empty;                       //施術日11;
        public string F067 { get; set; } = string.Empty;                       //施術日12;
        public string F068 { get; set; } = string.Empty;                       //施術日13;
        public string F069 { get; set; } = string.Empty;                       //施術日14;
        public string F070 { get; set; } = string.Empty;                       //施術日15;
        public string F071 { get; set; } = string.Empty;                       //施術日16;
        public string F072 { get; set; } = string.Empty;                       //施術日17;
        public string F073 { get; set; } = string.Empty;                       //施術日18;
        public string F074 { get; set; } = string.Empty;                       //施術日19;
        public string F075 { get; set; } = string.Empty;                       //施術日20;
        public string F076 { get; set; } = string.Empty;                       //施術日21;
        public string F077 { get; set; } = string.Empty;                       //施術日22;
        public string F078 { get; set; } = string.Empty;                       //施術日23;
        public string F079 { get; set; } = string.Empty;                       //施術日24;
        public string F080 { get; set; } = string.Empty;                       //施術日25;
        public string F081 { get; set; } = string.Empty;                       //施術日26;
        public string F082 { get; set; } = string.Empty;                       //施術日27;
        public string F083 { get; set; } = string.Empty;                       //施術日28;
        public string F084 { get; set; } = string.Empty;                       //施術日29;
        public string F085 { get; set; } = string.Empty;                       //施術日30;
        public string F086 { get; set; } = string.Empty;                       //施術日31;
        public string F087 { get; set; } = string.Empty;                       //初検料;
        public string F088 { get; set; } = string.Empty;                       //初検時相談支援料;
        public string F089 { get; set; } = string.Empty;                       //再検料;
        public string F090 { get; set; } = string.Empty;                       //加算料・休日深夜時間外;
        public string F091 { get; set; } = string.Empty;                       //往療距離;
        public string F092 { get; set; } = string.Empty;                       //往療回数;
        public string F093 { get; set; } = string.Empty;                       //往療料;
        public string F094 { get; set; } = string.Empty;                       //加算料・夜間難路暴風雨雪;
        public string F095 { get; set; } = string.Empty;                       //金属副子等加算料;
        public string F096 { get; set; } = string.Empty;                       //施術情報提供料;
        public string F097 { get; set; } = string.Empty;                       //整復料固定料施術料1;
        public string F098 { get; set; } = string.Empty;                       //整復料固定料施術料2;
        public string F099 { get; set; } = string.Empty;                       //整復料固定料施術料3;
        public string F100 { get; set; } = string.Empty;                       //整復料固定料施術料4;
        public string F101 { get; set; } = string.Empty;                       //整復料固定料施術料5;
        public string F102 { get; set; } = string.Empty;                       //整復料固定料施術料合計;
        public string F103 { get; set; } = string.Empty;                       //部位1後療回数;
        public string F104 { get; set; } = string.Empty;                       //部位1後療料;
        public string F105 { get; set; } = string.Empty;                       //部位1冷あん法回数;
        public string F106 { get; set; } = string.Empty;                       //部位1冷あん法料;
        public string F107 { get; set; } = string.Empty;                       //部位1温あん法回数;
        public string F108 { get; set; } = string.Empty;                       //部位1温あん法料;
        public string F109 { get; set; } = string.Empty;                       //部位1電療回数;
        public string F110 { get; set; } = string.Empty;                       //部位1電療料;
        public string F111 { get; set; } = string.Empty;                       //部位1長期;
        public string F112 { get; set; } = string.Empty;                       //部位1合計金額;
        public string F113 { get; set; } = string.Empty;                       //部位2後療回数;
        public string F114 { get; set; } = string.Empty;                       //部位2後療料;
        public string F115 { get; set; } = string.Empty;                       //部位2冷あん法回数;
        public string F116 { get; set; } = string.Empty;                       //部位2冷あん法料;
        public string F117 { get; set; } = string.Empty;                       //部位2温あん法回数;
        public string F118 { get; set; } = string.Empty;                       //部位2温あん法料;
        public string F119 { get; set; } = string.Empty;                       //部位2電療回数;
        public string F120 { get; set; } = string.Empty;                       //部位2電療料;
        public string F121 { get; set; } = string.Empty;                       //部位2長期;
        public string F122 { get; set; } = string.Empty;                       //部位2合計金額;
        public string F123 { get; set; } = string.Empty;                       //部位3後療回数1;
        public string F124 { get; set; } = string.Empty;                       //部位3後療料1;
        public string F125 { get; set; } = string.Empty;                       //部位3冷あん法回数1;
        public string F126 { get; set; } = string.Empty;                       //部位3冷あん法料1;
        public string F127 { get; set; } = string.Empty;                       //部位3温あん法回数1;
        public string F128 { get; set; } = string.Empty;                       //部位3温あん法料1;
        public string F129 { get; set; } = string.Empty;                       //部位3電療回数1;
        public string F130 { get; set; } = string.Empty;                       //部位3電療料1;
        public string F131 { get; set; } = string.Empty;                       //部位3長期1;
        public string F132 { get; set; } = string.Empty;                       //部位3合計金額1;
        public string F133 { get; set; } = string.Empty;                       //部位3逓減開始月日2;
        public string F134 { get; set; } = string.Empty;                       //部位3後療回数2;
        public string F135 { get; set; } = string.Empty;                       //部位3後療料2;
        public string F136 { get; set; } = string.Empty;                       //部位3冷あん法回数2;
        public string F137 { get; set; } = string.Empty;                       //部位3冷あん法料2;
        public string F138 { get; set; } = string.Empty;                       //部位3温あん法回数2;
        public string F139 { get; set; } = string.Empty;                       //部位3温あん法料2;
        public string F140 { get; set; } = string.Empty;                       //部位3電療回数2;
        public string F141 { get; set; } = string.Empty;                       //部位3電療料2;
        public string F142 { get; set; } = string.Empty;                       //部位3長期2;
        public string F143 { get; set; } = string.Empty;                       //部位3合計金額2;
        public string F144 { get; set; } = string.Empty;                       //部位4逓減1;
        public string F145 { get; set; } = string.Empty;                       //部位4逓減開始月日1;
        public string F146 { get; set; } = string.Empty;                       //部位4後療回数1;
        public string F147 { get; set; } = string.Empty;                       //部位4後療料1;
        public string F148 { get; set; } = string.Empty;                       //部位4冷あん法回数1;
        public string F149 { get; set; } = string.Empty;                       //部位4冷あん法料1;
        public string F150 { get; set; } = string.Empty;                       //部位4温あん法回数1;
        public string F151 { get; set; } = string.Empty;                       //部位4温あん法料1;
        public string F152 { get; set; } = string.Empty;                       //部位4電療回数1;
        public string F153 { get; set; } = string.Empty;                       //部位4電療料1;
        public string F154 { get; set; } = string.Empty;                       //部位4長期1;
        public string F155 { get; set; } = string.Empty;                       //部位4合計金額1;
        public string F156 { get; set; } = string.Empty;                       //部位4逓減2;
        public string F157 { get; set; } = string.Empty;                       //部位4逓減開始月日2;
        public string F158 { get; set; } = string.Empty;                       //部位4後療回数2;
        public string F159 { get; set; } = string.Empty;                       //部位4後療料2;
        public string F160 { get; set; } = string.Empty;                       //部位4冷あん法回数2;
        public string F161 { get; set; } = string.Empty;                       //部位4冷あん法料2;
        public string F162 { get; set; } = string.Empty;                       //部位4温あん法回数2;
        public string F163 { get; set; } = string.Empty;                       //部位4温あん法料2;
        public string F164 { get; set; } = string.Empty;                       //部位4電療回数2;
        public string F165 { get; set; } = string.Empty;                       //部位4電療料2;
        public string F166 { get; set; } = string.Empty;                       //部位4長期2;
        public string F167 { get; set; } = string.Empty;                       //部位4合計金額2;
        public string F168 { get; set; } = string.Empty;                     //摘要欄記入有無;
        public string F169atotal { get; set; } = string.Empty;               //総合計;
        public string F170acharge { get; set; } = string.Empty;              //請求金額;
        public string F171 { get; set; } = string.Empty;                     //保険者使用金額;
        public string F172sregnumber { get; set; } = string.Empty;           //登録記号番号;
        public string F173 { get; set; } = string.Empty;                     //受取代理人記入有無;
        public string aimagefile { get; set; } = string.Empty;              //画像ファイル名　納品処理用



        #endregion



        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {

            //全支部出す？
            //frmExport frmexp = new frmExport();
            //frmexp.ShowDialog();
            //frmexp.flgAll


            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;

            
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                
               
                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable(cym, wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DB.GetMainDBName()}";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                //全出力データ取得
                List<exportdata> lstExport = DB.Main.SelectAll<exportdata>().ToList();
                //List<exportdata> lstExport = DB.Main.Select<exportdata>($"cym={cym}").ToList();
                lstExport.Sort((x, y) => x.F003seq.CompareTo(y.F003seq));


                //データ出力
                if (!Export(lstExport, strNohinDir, wf, cym)) return false;

                //画像出力
            //    if (!ExportImage(lstExport, strNohinDir, wf)) return false;

                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }



        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// {<param name="wf">waitform</param>
        /// <returns></returns>
        private static bool InsertExportTable(int cym ,WaitForm wf)
        {
            int renban = 0;

            strPrefID = clsPrefInfo.getPrefInfo(DB.GetMainDBName()).prefid;//府県コード
            string strsql = string.Empty;

            #region sql

            strsql += $" select ";
            strsql += $" a.aid";        //検索用

            //                                                           項目                         length    description
            strsql += $",0";                                     //1      連番                          6       半角前ゼロ6桁　分割したファイルごとに1から →　ファイル出力時につける
            strsql += $",a.cym";                                 //2      請求年月                      6       受付年月（ヘッダ）をYYYYMM→ヘッダの受付年月を、appに持たせてそこから取る
            strsql += $",a.numbering seq";                       //3      シーケンスNO                  6       打番された数字を半角前ゼロ6桁→入力画面で制御
            strsql += $",{strPrefID.ToString().PadLeft(2,'0')}"; //4      府県コード                    2       db名から取得
            strsql += $",a.baccnumber jcd ";                     //5      柔整師会コード                7       口座番号　半角前ゼロ7桁
            strsql += $",split_part(a.hnum,'-',1) hmark";        //6      被保険者証記号                8       数字のみ　半角前ゼロ8桁　数字以外は半角スペース
            strsql += $",split_part(a.hnum,'-',2) hnum";         //7      被保険者証番号                7              

            strsql += $",a.htype";                               //8      保険種別                      1              


            strsql += $",case when a.asingle =0 then 1 ";       //9      単併区分                      1       //1.単独が選択されている場合及びいずれの選択もない場合1
            strsql += $"else a.asingle end asingle ";                                                           //2.2併が選択されている場合2
                                                                                                                //3.3併が選択されている場合3


            strsql += $",a.afamily";                             //10      本家区分                     1   
            
            strsql += $",case a.aratio when 10 then 1";          //11      給付割合                     1      10が選択されている場合1
            strsql += $" when 9 then 2";                         //11      給付割合                     1      9が選択されている場合2
            strsql += $" when 8 then 3";                         //11      給付割合                     1      8が選択されている場合3
            strsql += $" when 7 then 4";                         //11      給付割合                     1      7が選択されている場合4
            strsql += $" end as ratio ";

            strsql += $",'{string.Empty}'";                      //12      受診者氏名                   26     不要（項目は必要  
            strsql += $",'{string.Empty}'";                      //13      郵便番号                     7     不要（項目は必要   
            strsql += $",'{string.Empty}'";                      //14      住所1                        26     不要（項目は必要  
            strsql += $",'{string.Empty}'";                      //15      住所2                        26     不要（項目は必要  
            strsql += $",'{string.Empty}'";                      //16      住所3                        26     不要（項目は必要  
            strsql += $",'{string.Empty}'";                      //17      電話番号                     12     不要（項目は必要  
                                                                          
            strsql += $",a.psex";                                            //18      性別                         1              
            strsql += $",to_char(a.pbirthday,'YYYYMMDD') birthday";          //19      受診者生年月日               8              YYYYMMDD



            strsql += $",case when date_part('year',istartdate1)='0001' then '' ";         //20      診療年月        6              27開始日の値YYYYMMをコピー。開始日がない場合値無し
            strsql += $" else to_char(a.istartdate1,'YYYYMM') end ym ";
            

            strsql += $",cast(a.acounteddays as varchar) counteddays ";      //21      診療日数                     2               

            strsql += $",replace(split_part(a.taggeddatas,':',2),'\"','') buicount ";     //22      部位数                       1                                                                              


            strsql += $",'{string.Empty}'";                      //23      負傷原因記入有無             1     不要（項目は必要  
            strsql += $",'{string.Empty}'";                      //24      負傷1　負傷名                40     不要（項目は必要 
            strsql += $",'{string.Empty}'";                      //25      　負傷年月日                 8     不要（項目は必要  

            strsql += $",to_char(a.ifirstdate1,'YYYYMMDD')";    //26      　初検日                     8              YYYYMMDD
            strsql += $",to_char(a.istartdate1,'YYYYMMDD')";    //27      　開始日                     8              YYYYMMDD
            strsql += $",to_char(a.ifinishdate1,'YYYYMMDD')";   //28      　終了日                     8              YYYYMMDD
            strsql += $",cast(a.idays1 as varchar)";            //29      　日数                       2                 

            #region 不要（項目は必要部分
            strsql += $",'{string.Empty}'";        //30      負傷2　負傷名                40     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //31      　負傷年月日                 8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //32      　初検日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //33      　開始日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //34      　終了日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //35      　日数                       2     不要（項目は必要    
            strsql += $",'{string.Empty}'";        //36      負傷3　負傷名                40     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //37      　負傷年月日                 8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //38      　初検日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //39      　開始日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //40      　終了日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //41      　日数                       2     不要（項目は必要    
            strsql += $",'{string.Empty}'";        //42      負傷4　負傷名                40     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //43      　負傷年月日                 8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //44      　初検日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //45      　開始日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //46      　終了日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //47      　日数                       2     不要（項目は必要    
            strsql += $",'{string.Empty}'";        //48      負傷5　負傷名                40     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //49      　負傷年月日                 8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //50      　初検日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //51      　開始日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //52      　終了日                     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //53      　日数                       2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //54      6部位以上有無                1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //55      経過記入有無                 1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //56      施術日1                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //57      施術日2                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //58      施術日3                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //59      施術日4                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //60      施術日5                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //61      施術日6                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //62      施術日7                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //63      施術日8                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //64      施術日9                      1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //65      施術日10                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //66      施術日11                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //67      施術日12                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //68      施術日13                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //69      施術日14                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //70      施術日15                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //71      施術日16                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //72      施術日17                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //73      施術日18                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //74      施術日19                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //75      施術日20                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //76      施術日21                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //77      施術日22                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //78      施術日23                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //79      施術日24                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //80      施術日25                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //81      施術日26                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //82      施術日27                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //83      施術日28                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //84      施術日29                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //85      施術日30                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //86      施術日31                     1     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //87      初検料                       8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //88      初検時相談支援料             8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //89      再検料                       8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //90      加算料・休日深夜時間外       8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //91      往療距離                     4     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //92      往療回数                     2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //93      往療料                       8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //94      加算料・夜間難路暴風雨雪     8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //95      金属副子等加算料             8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //96      施術情報提供料               8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //97      整復料固定料施術料1          8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //98      整復料固定料施術料2          8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //99      整復料固定料施術料3          8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //100      整復料固定料施術料4         8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //101      整復料固定料施術料5         8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //102      整復料固定料施術料合計      8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //103      部位1後療回数               2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //104      部位1後療料                 5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //105      部位1冷あん法回数           2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //106      部位1冷あん法料             5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //107      部位1温あん法回数           2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //108      部位1温あん法料             5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //109      部位1電療回数               2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //110      部位1電療料                 5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //111      部位1長期                   3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //112      部位1合計金額               8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //113      部位2後療回数               2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //114      部位2後療料                 5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //115      部位2冷あん法回数           2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //116      部位2冷あん法料             5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //117      部位2温あん法回数           2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //118      部位2温あん法料             5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //119      部位2電療回数               2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //120      部位2電療料                 5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //121      部位2長期                   3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //122      部位2合計金額               8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //123      部位3後療回数1              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //124      部位3後療料1                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //125      部位3冷あん法回数1          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //126      部位3冷あん法料1            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //127      部位3温あん法回数1          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //128      部位3温あん法料1            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //129      部位3電療回数1              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //130      部位3電療料1                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //131      部位3長期1                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //132      部位3合計金額1              8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //133      部位3逓減開始月日2          4     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //134      部位3後療回数2              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //135      部位3後療料2                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //136      部位3冷あん法回数2          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //137      部位3冷あん法料2            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //138      部位3温あん法回数2          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //139      部位3温あん法料2            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //140      部位3電療回数2              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //141      部位3電療料2                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //142      部位3長期2                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //143      部位3合計金額2              8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //144      部位4逓減1                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //145      部位4逓減開始月日1          4     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //146      部位4後療回数1              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //147      部位4後療料1                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //148      部位4冷あん法回数1          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //149      部位4冷あん法料1            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //150      部位4温あん法回数1          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //151      部位4温あん法料1            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //152      部位4電療回数1              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //153      部位4電療料1                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //154      部位4長期1                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //155      部位4合計金額1              8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //156      部位4逓減2                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //157      部位4逓減開始月日2          4     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //158      部位4後療回数2              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //159      部位4後療料2                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //160      部位4冷あん法回数2          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //161      部位4冷あん法料2            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //162      部位4温あん法回数2          2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //163      部位4温あん法料2            5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //164      部位4電療回数2              2     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //165      部位4電療料2                5     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //166      部位4長期2                  3     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //167      部位4合計金額2              8     不要（項目は必要    
            strsql += $",'{string.Empty}'";         //168      摘要欄記入有無              1     不要（項目は必要    

            #endregion

            strsql += $",cast(a.atotal as varchar)";           //169      総合計                      8          
            strsql += $",cast(a.acharge as varchar)";          //170      請求金額                    8          

            strsql += $",'{string.Empty}'";                    //171      保険者使用金額              8     不要（項目は必要     


            strsql += $",case when substring(a.sregnumber,1,1)='0' then '2' || substring(a.sregnumber,2) "; //172      登録記号番号 10    契=1　協=2　＋　ハイフン抜き半角数字　＝＞　契(画面上は1)→1協(画面上は0)→2
            strsql += $" else a.sregnumber end sregnumber ";

            strsql += $",'{string.Empty}'";                    //173      受取代理人記入有無          1     不要（項目は必要     

            strsql += $",a.aimagefile "; //納品画像判別用

            strsql += $" from application a ";
            strsql += $"where a.cym={cym} ";
            
            strsql += $"order by a.numbering ";

            #endregion


            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            wf.LogPrint("整形/取得中");
            DB.Command cmd = new DB.Command(strsql,tran);
            var lst = cmd.TryExecuteReaderList();


            wf.SetMax(lst.Count);
            //wf.SetMax(lstApp.Count);

            
            //今月分を削除
            DB.Command cmddel = new DB.Command($"truncate table exportdata ", tran);
            //DB.Command cmddel = new DB.Command($"delete from exportdata where cym='{cym}'", tran);


            
            try
            {
                wf.LogPrint("出力テーブル削除");
                cmddel.TryExecuteNonQuery();
                
                wf.LogPrint("出力テーブル登録");
                for (int r = 0; r < lst.Count; r++)
                {
                    if (wf.Cancel)
                    {
                        if (System.Windows.Forms.MessageBox.Show(
                            "中止しますか？",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.YesNo,
                            System.Windows.Forms.MessageBoxIcon.Question,
                            System.Windows.Forms.MessageBoxDefaultButton.Button2) 
                            == System.Windows.Forms.DialogResult.Yes) return false;
                    }

                    int c = 0;//列番号

                    exportdata exp = new exportdata();
                    exp.aid = int.Parse(lst[r][c++].ToString());
                    exp.F001renban = lst[r][c++].ToString();// seqPerFile++.ToString().PadLeft(6,'0');                    exp.F001renban = "0";// seqPerFile++.ToString().PadLeft(6,'0');
                    exp.F002cym = lst[r][c++].ToString();
                    exp.F003seq = lst[r][c++].ToString();
                    exp.F004fuken = lst[r][c++].ToString();
                    exp.F005jcd = lst[r][c++].ToString();
                    exp.F006hihokigo = lst[r][c++].ToString();
                    exp.F007hihonum = lst[r][c++].ToString();
                    exp.F008hoken = lst[r][c++].ToString();
                    exp.F009tanhei = lst[r][c++].ToString();
                    exp.F010honke = lst[r][c++].ToString();
                    exp.F011ratio = lst[r][c++].ToString();
                    exp.F012pname = lst[r][c++].ToString();
                    exp.F013 = lst[r][c++].ToString();
                    exp.F014 = lst[r][c++].ToString();
                    exp.F015 = lst[r][c++].ToString();
                    exp.F016 = lst[r][c++].ToString();
                    exp.F017 = lst[r][c++].ToString();
                    exp.F018psex = lst[r][c++].ToString();
                    exp.F019pbirthday = lst[r][c++].ToString();
                    exp.F020ym = lst[r][c++].ToString();
                    exp.F021counteddays = lst[r][c++].ToString();
                    exp.F022bui = lst[r][c++].ToString();
                    exp.F023 = lst[r][c++].ToString();
                    exp.F024 = lst[r][c++].ToString();
                    exp.F025 = lst[r][c++].ToString();
                    exp.F026ifirstdate1 = lst[r][c++].ToString();
                    exp.F027istartdate1 = lst[r][c++].ToString();
                    exp.F028ifinishdate1 = lst[r][c++].ToString();
                    exp.F029idays1 = lst[r][c++].ToString();

                    #region 不要（項目は必要項目
                    exp.F030 = lst[r][c++].ToString();
                    exp.F031 = lst[r][c++].ToString();
                    exp.F032 = lst[r][c++].ToString();
                    exp.F033 = lst[r][c++].ToString();
                    exp.F034 = lst[r][c++].ToString();
                    exp.F035 = lst[r][c++].ToString();
                    exp.F036 = lst[r][c++].ToString();
                    exp.F037 = lst[r][c++].ToString();
                    exp.F038 = lst[r][c++].ToString();
                    exp.F039 = lst[r][c++].ToString();
                    exp.F040 = lst[r][c++].ToString();
                    exp.F041 = lst[r][c++].ToString();
                    exp.F042 = lst[r][c++].ToString();
                    exp.F043 = lst[r][c++].ToString();
                    exp.F044 = lst[r][c++].ToString();
                    exp.F045 = lst[r][c++].ToString();
                    exp.F046 = lst[r][c++].ToString();
                    exp.F047 = lst[r][c++].ToString();
                    exp.F048 = lst[r][c++].ToString();
                    exp.F049 = lst[r][c++].ToString();
                    exp.F050 = lst[r][c++].ToString();
                    exp.F051 = lst[r][c++].ToString();
                    exp.F052 = lst[r][c++].ToString();
                    exp.F053 = lst[r][c++].ToString();
                    exp.F054 = lst[r][c++].ToString();
                    exp.F055 = lst[r][c++].ToString();
                    exp.F056 = lst[r][c++].ToString();
                    exp.F057 = lst[r][c++].ToString();
                    exp.F058 = lst[r][c++].ToString();
                    exp.F059 = lst[r][c++].ToString();
                    exp.F060 = lst[r][c++].ToString();
                    exp.F061 = lst[r][c++].ToString();
                    exp.F062 = lst[r][c++].ToString();
                    exp.F063 = lst[r][c++].ToString();
                    exp.F064 = lst[r][c++].ToString();
                    exp.F065 = lst[r][c++].ToString();
                    exp.F066 = lst[r][c++].ToString();
                    exp.F067 = lst[r][c++].ToString();
                    exp.F068 = lst[r][c++].ToString();
                    exp.F069 = lst[r][c++].ToString();
                    exp.F070 = lst[r][c++].ToString();
                    exp.F071 = lst[r][c++].ToString();
                    exp.F072 = lst[r][c++].ToString();
                    exp.F073 = lst[r][c++].ToString();
                    exp.F074 = lst[r][c++].ToString();
                    exp.F075 = lst[r][c++].ToString();
                    exp.F076 = lst[r][c++].ToString();
                    exp.F077 = lst[r][c++].ToString();
                    exp.F078 = lst[r][c++].ToString();
                    exp.F079 = lst[r][c++].ToString();
                    exp.F080 = lst[r][c++].ToString();
                    exp.F081 = lst[r][c++].ToString();
                    exp.F082 = lst[r][c++].ToString();
                    exp.F083 = lst[r][c++].ToString();
                    exp.F084 = lst[r][c++].ToString();
                    exp.F085 = lst[r][c++].ToString();
                    exp.F086 = lst[r][c++].ToString();
                    exp.F087 = lst[r][c++].ToString();
                    exp.F088 = lst[r][c++].ToString();
                    exp.F089 = lst[r][c++].ToString();
                    exp.F090 = lst[r][c++].ToString();
                    exp.F091 = lst[r][c++].ToString();
                    exp.F092 = lst[r][c++].ToString();
                    exp.F093 = lst[r][c++].ToString();
                    exp.F094 = lst[r][c++].ToString();
                    exp.F095 = lst[r][c++].ToString();
                    exp.F096 = lst[r][c++].ToString();
                    exp.F097 = lst[r][c++].ToString();
                    exp.F098 = lst[r][c++].ToString();
                    exp.F099 = lst[r][c++].ToString();
                    exp.F100 = lst[r][c++].ToString();
                    exp.F101 = lst[r][c++].ToString();
                    exp.F102 = lst[r][c++].ToString();
                    exp.F103 = lst[r][c++].ToString();
                    exp.F104 = lst[r][c++].ToString();
                    exp.F105 = lst[r][c++].ToString();
                    exp.F106 = lst[r][c++].ToString();
                    exp.F107 = lst[r][c++].ToString();
                    exp.F108 = lst[r][c++].ToString();
                    exp.F109 = lst[r][c++].ToString();
                    exp.F110 = lst[r][c++].ToString();
                    exp.F111 = lst[r][c++].ToString();
                    exp.F112 = lst[r][c++].ToString();
                    exp.F113 = lst[r][c++].ToString();
                    exp.F114 = lst[r][c++].ToString();
                    exp.F115 = lst[r][c++].ToString();
                    exp.F116 = lst[r][c++].ToString();
                    exp.F117 = lst[r][c++].ToString();
                    exp.F118 = lst[r][c++].ToString();
                    exp.F119 = lst[r][c++].ToString();
                    exp.F120 = lst[r][c++].ToString();
                    exp.F121 = lst[r][c++].ToString();
                    exp.F122 = lst[r][c++].ToString();
                    exp.F123 = lst[r][c++].ToString();
                    exp.F124 = lst[r][c++].ToString();
                    exp.F125 = lst[r][c++].ToString();
                    exp.F126 = lst[r][c++].ToString();
                    exp.F127 = lst[r][c++].ToString();
                    exp.F128 = lst[r][c++].ToString();
                    exp.F129 = lst[r][c++].ToString();
                    exp.F130 = lst[r][c++].ToString();
                    exp.F131 = lst[r][c++].ToString();
                    exp.F132 = lst[r][c++].ToString();
                    exp.F133 = lst[r][c++].ToString();
                    exp.F134 = lst[r][c++].ToString();
                    exp.F135 = lst[r][c++].ToString();
                    exp.F136 = lst[r][c++].ToString();
                    exp.F137 = lst[r][c++].ToString();
                    exp.F138 = lst[r][c++].ToString();
                    exp.F139 = lst[r][c++].ToString();
                    exp.F140 = lst[r][c++].ToString();
                    exp.F141 = lst[r][c++].ToString();
                    exp.F142 = lst[r][c++].ToString();
                    exp.F143 = lst[r][c++].ToString();
                    exp.F144 = lst[r][c++].ToString();
                    exp.F145 = lst[r][c++].ToString();
                    exp.F146 = lst[r][c++].ToString();
                    exp.F147 = lst[r][c++].ToString();
                    exp.F148 = lst[r][c++].ToString();
                    exp.F149 = lst[r][c++].ToString();
                    exp.F150 = lst[r][c++].ToString();
                    exp.F151 = lst[r][c++].ToString();
                    exp.F152 = lst[r][c++].ToString();
                    exp.F153 = lst[r][c++].ToString();
                    exp.F154 = lst[r][c++].ToString();
                    exp.F155 = lst[r][c++].ToString();
                    exp.F156 = lst[r][c++].ToString();
                    exp.F157 = lst[r][c++].ToString();
                    exp.F158 = lst[r][c++].ToString();
                    exp.F159 = lst[r][c++].ToString();
                    exp.F160 = lst[r][c++].ToString();
                    exp.F161 = lst[r][c++].ToString();
                    exp.F162 = lst[r][c++].ToString();
                    exp.F163 = lst[r][c++].ToString();
                    exp.F164 = lst[r][c++].ToString();
                    exp.F165 = lst[r][c++].ToString();
                    exp.F166 = lst[r][c++].ToString();
                    exp.F167 = lst[r][c++].ToString();
                    exp.F168 = lst[r][c++].ToString();
# endregion
                    
                    exp.F169atotal = lst[r][c++].ToString();
                    exp.F170acharge = lst[r][c++].ToString();
                    exp.F171 = lst[r][c++].ToString();
                    exp.F172sregnumber = lst[r][c++].ToString();
                    exp.F173 = lst[r][c++].ToString();

                    exp.aimagefile= lst[r][c++].ToString();


                    if (!DB.Main.Insert(exp, tran)) return false;


                    wf.LogPrint($"出力テーブル登録 シーケンス:{exp.F003seq} 被保番:{exp.F007hihonum}");

                    wf.InvokeValue++;
                }


                tran.Commit();
                wf.LogPrint($"出力テーブル登録完了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message );
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
                cmddel.Dispose();
            }

               
        }


        /// <summary>
        /// 不要（項目は必要以外画像出力
        /// </summary>
        /// <param name="lstExport">出力するリスト</param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        //private static bool ExportImage(List<export_data> lstExport , string strDir, WaitForm wf)
        //{
        //    string imageName = string.Empty;                    //tifコピー先の名前
        //    string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ
       
        //    TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
        //    wf.InvokeValue = 0;
        //    wf.SetMax(lstExport.Count);

        //    try
        //    {
        //        //出力先パス
        //        string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";


        //        //0番目のファイルパス
        //        imageName = $"{strImageDir}\\{lstExport[0].imagefilename}";

        //        for (int r = 0; r < lstExport.Count(); r++)
        //        {
                    
        //            //レセプト全国共通キーが20桁の場合、申請書と見なす

        //            if ((lstExport[r].)  && (lstImageFilePath.Count != 0))
        //            {
        //                //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
        //                //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
        //                //その上で、この申請書をマルチTIFF候補リストに追加
        //                wf.LogPrint($"申請書出力中:{imageName}");
        //                if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
        //                {
        //                    System.Windows.Forms.MessageBox.Show(
        //                        System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
        //                    return false;
        //                }

        //                lstImageFilePath.Clear();                        
        //                lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
        //                imageName = $"{strImageDir}\\{lstExport[r].imagefilename}";
        //            }                   
        //            else if ((lstExport[r].comnum.Length==20) && (lstImageFilePath.Count == 0))
        //            {
        //                //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
        //                lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
        //                wf.LogPrint($"申請書出力中:{imageName}");
        //            }

        //            else if (new string[] { "-1", "-3", "-5", "-6", "-9", "-11", "-13", "-14"}.Contains(lstExport[r].comnum))
        //            {
        //                //申請書以外の、不要（項目は必要ファイル以外をマルチTIFF候補ととして追加
        //                lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
        //                wf.LogPrint($"申請書以外:{imageName}");
        //            }
               
                    
        //            wf.InvokeValue++;

        //        }


        //        //最終画像出力
        //        //ループの最後の画像を出力
        //        if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
        //            TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
        //        return false;
        //    }
            
        //}


        /// <summary>
        /// DBからデータを取得してCSV出力まで
        /// <param name="lstExport"/>出力リスト</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// <param name="wf">waitform</param>
        /// <param name="cym">メホール請求年月</param>
        /// </summary>
        /// <returns></returns>
        private static bool Export(List<exportdata> lstExport ,string strDir,WaitForm wf,int cym)            
        {
                
            System.IO.Directory.CreateDirectory(strDir);            //納品フォルダ

            wf.LogPrint("ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lstExport.Count<exportdata>());

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            int intFileCount = 1;                   //分割ファイル番号(10MBで分割)
            int intseqPerFile = 1;                  //ファイルごとの連番
            string strFileName = string.Empty;      //datファイル名
            string strres = string.Empty;           //データ用文字列
            string strFukenCode = string.Empty;     //ファイル名用府県コード
            System.IO.StreamWriter sw=null;
            
            try
            {
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                

                for (int r=0;r<lstExport.Count();r++)                
                {

                    strFukenCode = lstExport[r].F004fuken;

                    #region 文字列作成

                    strres += $"{intseqPerFile++.ToString().PadLeft(6,'0')},";          //1連番
                    strres += $"{lstExport[r].F002cym},";                               //2請求年月
                    strres += $"{lstExport[r].F003seq},";                               //3シーケンスNO
                    strres += $"{lstExport[r].F004fuken},";                             //4府県コード
                    strres += $"{lstExport[r].F005jcd},";                               //5柔整師会コード
                    strres += $"{lstExport[r].F006hihokigo},";                          //6被保険者証記号
                    strres += $"{lstExport[r].F007hihonum},";                           //7被保険者証番号
                    strres += $"{lstExport[r].F008hoken},";                             //8保険種別
                    strres += $"{lstExport[r].F009tanhei},";                            //9単併区分
                    strres += $"{lstExport[r].F010honke},";                             //10本家区分
                    strres += $"{lstExport[r].F011ratio},";                             //11給付割合
                    strres += $"{lstExport[r].F012pname},";                             //12受診者氏名

                    strres += $"{lstExport[r].F013},";                                  //13郵便番号
                    strres += $"{lstExport[r].F014},";                                  //14住所1
                    strres += $"{lstExport[r].F015},";                                  //15住所2
                    strres += $"{lstExport[r].F016},";                                  //16住所3
                    strres += $"{lstExport[r].F017},";                                  //17電話番号

                    strres += $"{lstExport[r].F018psex},";                              //18性別
                    strres += $"{lstExport[r].F019pbirthday},";                         //19受診者生年月日
                    strres += $"{lstExport[r].F020ym},";                                //20診療年月
                    strres += $"{lstExport[r].F021counteddays},";                       //21診療日数
                    strres += $"{lstExport[r].F022bui},";                               //22部位数

                    strres += $"{lstExport[r].F023},";                                  //23負傷原因記入有無
                    strres += $"{lstExport[r].F024},";                                  //24負傷1　負傷名
                    strres += $"{lstExport[r].F025},";                                  //25　負傷年月日

                    strres += $"{lstExport[r].F026ifirstdate1},";                       //26　初検日
                    strres += $"{lstExport[r].F027istartdate1},";                       //27　開始日
                    strres += $"{lstExport[r].F028ifinishdate1},";                      //28　終了日
                    strres += $"{lstExport[r].F029idays1},";                            //29　日数

                    strres += $"{lstExport[r].F030},";                                  //30負傷2　負傷名
                    strres += $"{lstExport[r].F031},";                                  //31　負傷年月日
                    strres += $"{lstExport[r].F032},";                                  //32　初検日
                    strres += $"{lstExport[r].F033},";                                  //33　開始日
                    strres += $"{lstExport[r].F034},";                                  //34　終了日
                    strres += $"{lstExport[r].F035},";                                  //35　日数
                    strres += $"{lstExport[r].F036},";                                  //36負傷3　負傷名
                    strres += $"{lstExport[r].F037},";                                  //37　負傷年月日
                    strres += $"{lstExport[r].F038},";                                  //38　初検日
                    strres += $"{lstExport[r].F039},";                                  //39　開始日
                    strres += $"{lstExport[r].F040},";                                  //40　終了日
                    strres += $"{lstExport[r].F041},";                                  //41　日数
                    strres += $"{lstExport[r].F042},";                                  //42負傷4　負傷名
                    strres += $"{lstExport[r].F043},";                                  //43　負傷年月日
                    strres += $"{lstExport[r].F044},";                                  //44　初検日
                    strres += $"{lstExport[r].F045},";                                  //45　開始日
                    strres += $"{lstExport[r].F046},";                                  //46　終了日
                    strres += $"{lstExport[r].F047},";                                  //47　日数
                    strres += $"{lstExport[r].F048},";                                  //48負傷5　負傷名
                    strres += $"{lstExport[r].F049},";                                  //49　負傷年月日
                    strres += $"{lstExport[r].F050},";                                  //50　初検日
                    strres += $"{lstExport[r].F051},";                                  //51　開始日
                    strres += $"{lstExport[r].F052},";                                  //52　終了日
                    strres += $"{lstExport[r].F053},";                                  //53　日数
                    strres += $"{lstExport[r].F054},";                                  //546部位以上有無
                    strres += $"{lstExport[r].F055},";                                  //55経過記入有無
                    strres += $"{lstExport[r].F056},";                                  //56施術日1
                    strres += $"{lstExport[r].F057},";                                  //57施術日2
                    strres += $"{lstExport[r].F058},";                                  //58施術日3
                    strres += $"{lstExport[r].F059},";                                  //59施術日4
                    strres += $"{lstExport[r].F060},";                                  //60施術日5
                    strres += $"{lstExport[r].F061},";                                  //61施術日6
                    strres += $"{lstExport[r].F062},";                                  //62施術日7
                    strres += $"{lstExport[r].F063},";                                  //63施術日8
                    strres += $"{lstExport[r].F064},";                                  //64施術日9
                    strres += $"{lstExport[r].F065},";                                  //65施術日10
                    strres += $"{lstExport[r].F066},";                                  //66施術日11
                    strres += $"{lstExport[r].F067},";                                  //67施術日12
                    strres += $"{lstExport[r].F068},";                                  //68施術日13
                    strres += $"{lstExport[r].F069},";                                  //69施術日14
                    strres += $"{lstExport[r].F070},";                                  //70施術日15
                    strres += $"{lstExport[r].F071},";                                  //71施術日16
                    strres += $"{lstExport[r].F072},";                                  //72施術日17
                    strres += $"{lstExport[r].F073},";                                  //73施術日18
                    strres += $"{lstExport[r].F074},";                                  //74施術日19
                    strres += $"{lstExport[r].F075},";                                  //75施術日20
                    strres += $"{lstExport[r].F076},";                                  //76施術日21
                    strres += $"{lstExport[r].F077},";                                  //77施術日22
                    strres += $"{lstExport[r].F078},";                                  //78施術日23
                    strres += $"{lstExport[r].F079},";                                  //79施術日24
                    strres += $"{lstExport[r].F080},";                                  //80施術日25
                    strres += $"{lstExport[r].F081},";                                  //81施術日26
                    strres += $"{lstExport[r].F082},";                                  //82施術日27
                    strres += $"{lstExport[r].F083},";                                  //83施術日28
                    strres += $"{lstExport[r].F084},";                                  //84施術日29
                    strres += $"{lstExport[r].F085},";                                  //85施術日30
                    strres += $"{lstExport[r].F086},";                                  //86施術日31
                    strres += $"{lstExport[r].F087},";                                  //87初検料
                    strres += $"{lstExport[r].F088},";                                  //88初検時相談支援料
                    strres += $"{lstExport[r].F089},";                                  //89再検料
                    strres += $"{lstExport[r].F090},";                                  //90加算料・休日深夜時間外
                    strres += $"{lstExport[r].F091},";                                  //91往療距離
                    strres += $"{lstExport[r].F092},";                                  //92往療回数
                    strres += $"{lstExport[r].F093},";                                  //93往療料
                    strres += $"{lstExport[r].F094},";                                  //94加算料・夜間難路暴風雨雪
                    strres += $"{lstExport[r].F095},";                                  //95金属副子等加算料
                    strres += $"{lstExport[r].F096},";                                  //96施術情報提供料
                    strres += $"{lstExport[r].F097},";                                  //97整復料固定料施術料1
                    strres += $"{lstExport[r].F098},";                                  //98整復料固定料施術料2
                    strres += $"{lstExport[r].F099},";                                  //99整復料固定料施術料3
                    strres += $"{lstExport[r].F100},";                                  //100整復料固定料施術料4
                    strres += $"{lstExport[r].F101},";                                  //101整復料固定料施術料5
                    strres += $"{lstExport[r].F102},";                                  //102整復料固定料施術料合計
                    strres += $"{lstExport[r].F103},";                                  //103部位1後療回数
                    strres += $"{lstExport[r].F104},";                                  //104部位1後療料
                    strres += $"{lstExport[r].F105},";                                  //105部位1冷あん法回数
                    strres += $"{lstExport[r].F106},";                                  //106部位1冷あん法料
                    strres += $"{lstExport[r].F107},";                                  //107部位1温あん法回数
                    strres += $"{lstExport[r].F108},";                                  //108部位1温あん法料
                    strres += $"{lstExport[r].F109},";                                  //109部位1電療回数
                    strres += $"{lstExport[r].F110},";                                  //110部位1電療料
                    strres += $"{lstExport[r].F111},";                                  //111部位1長期
                    strres += $"{lstExport[r].F112},";                                  //112部位1合計金額
                    strres += $"{lstExport[r].F113},";                                  //113部位2後療回数
                    strres += $"{lstExport[r].F114},";                                  //114部位2後療料
                    strres += $"{lstExport[r].F115},";                                  //115部位2冷あん法回数
                    strres += $"{lstExport[r].F116},";                                  //116部位2冷あん法料
                    strres += $"{lstExport[r].F117},";                                  //117部位2温あん法回数
                    strres += $"{lstExport[r].F118},";                                  //118部位2温あん法料
                    strres += $"{lstExport[r].F119},";                                  //119部位2電療回数
                    strres += $"{lstExport[r].F120},";                                  //120部位2電療料
                    strres += $"{lstExport[r].F121},";                                  //121部位2長期
                    strres += $"{lstExport[r].F122},";                                  //122部位2合計金額
                    strres += $"{lstExport[r].F123},";                                  //123部位3後療回数1
                    strres += $"{lstExport[r].F124},";                                  //124部位3後療料1
                    strres += $"{lstExport[r].F125},";                                  //125部位3冷あん法回数1
                    strres += $"{lstExport[r].F126},";                                  //126部位3冷あん法料1
                    strres += $"{lstExport[r].F127},";                                  //127部位3温あん法回数1
                    strres += $"{lstExport[r].F128},";                                  //128部位3温あん法料1
                    strres += $"{lstExport[r].F129},";                                  //129部位3電療回数1
                    strres += $"{lstExport[r].F130},";                                  //130部位3電療料1
                    strres += $"{lstExport[r].F131},";                                  //131部位3長期1
                    strres += $"{lstExport[r].F132},";                                  //132部位3合計金額1
                    strres += $"{lstExport[r].F133},";                                  //133部位3逓減開始月日2
                    strres += $"{lstExport[r].F134},";                                  //134部位3後療回数2
                    strres += $"{lstExport[r].F135},";                                  //135部位3後療料2
                    strres += $"{lstExport[r].F136},";                                  //136部位3冷あん法回数2
                    strres += $"{lstExport[r].F137},";                                  //137部位3冷あん法料2
                    strres += $"{lstExport[r].F138},";                                  //138部位3温あん法回数2
                    strres += $"{lstExport[r].F139},";                                  //139部位3温あん法料2
                    strres += $"{lstExport[r].F140},";                                  //140部位3電療回数2
                    strres += $"{lstExport[r].F141},";                                  //141部位3電療料2
                    strres += $"{lstExport[r].F142},";                                  //142部位3長期2
                    strres += $"{lstExport[r].F143},";                                  //143部位3合計金額2
                    strres += $"{lstExport[r].F144},";                                  //144部位4逓減1
                    strres += $"{lstExport[r].F145},";                                  //145部位4逓減開始月日1
                    strres += $"{lstExport[r].F146},";                                  //146部位4後療回数1
                    strres += $"{lstExport[r].F147},";                                  //147部位4後療料1
                    strres += $"{lstExport[r].F148},";                                  //148部位4冷あん法回数1
                    strres += $"{lstExport[r].F149},";                                  //149部位4冷あん法料1
                    strres += $"{lstExport[r].F150},";                                  //150部位4温あん法回数1
                    strres += $"{lstExport[r].F151},";                                  //151部位4温あん法料1
                    strres += $"{lstExport[r].F152},";                                  //152部位4電療回数1
                    strres += $"{lstExport[r].F153},";                                  //153部位4電療料1
                    strres += $"{lstExport[r].F154},";                                  //154部位4長期1
                    strres += $"{lstExport[r].F155},";                                  //155部位4合計金額1
                    strres += $"{lstExport[r].F156},";                                  //156部位4逓減2
                    strres += $"{lstExport[r].F157},";                                  //157部位4逓減開始月日2
                    strres += $"{lstExport[r].F158},";                                  //158部位4後療回数2
                    strres += $"{lstExport[r].F159},";                                  //159部位4後療料2
                    strres += $"{lstExport[r].F160},";                                  //160部位4冷あん法回数2
                    strres += $"{lstExport[r].F161},";                                  //161部位4冷あん法料2
                    strres += $"{lstExport[r].F162},";                                  //162部位4温あん法回数2
                    strres += $"{lstExport[r].F163},";                                  //163部位4温あん法料2
                    strres += $"{lstExport[r].F164},";                                  //164部位4電療回数2
                    strres += $"{lstExport[r].F165},";                                  //165部位4電療料2
                    strres += $"{lstExport[r].F166},";                                  //166部位4長期2
                    strres += $"{lstExport[r].F167},";                                  //167部位4合計金額2
                    strres += $"{lstExport[r].F168},";                                  //168摘要欄記入有無


                    strres += $"{lstExport[r].F169atotal},";                            //169総合計
                    strres += $"{lstExport[r].F170acharge},";                           //170請求金額
                    strres += $"{lstExport[r].F171},";                                  //171保険者使用金額
                    strres += $"{lstExport[r].F172sregnumber},";                        //172登録記号番号
                    strres += $"{lstExport[r].F173}";                                   //173受取代理人記入有無
                    strres += "\r\n";

                    #endregion


                    //10MB判定
                    if (System.Text.Encoding.GetEncoding("shift-jis").GetByteCount(strres) > 10000000)
                    {
                        strres = strres.Replace("\r\n\r\n", "");

                        //出力ファイル名
                        strFileName = $"JYUSEI_{strFukenCode}_{DateTime.Now.ToString("yyyyMMdd")}_{intFileCount++.ToString("00")}.dat";
                        sw = new System.IO.StreamWriter($"{strDir}\\{strFileName}", false, System.Text.Encoding.GetEncoding("shift-jis"));

                        sw.Write(strres);
                        
                        strres = string.Empty;

                        sw.Close();
                        intseqPerFile = 0;
                    }

                    
                    wf.InvokeValue++;
                    wf.LogPrint($"ファイル作成中 シーケンス {lstExport[r].F003seq}");

                }

                strres = strres.Replace("\r\n\r\n", "");

                //出力ファイル名 ファイルカウント1以下（分割無し）の場合は番号付けない
                if (intFileCount <=1) strFileName = $"JYUSEI_{strFukenCode}_{DateTime.Now.ToString("yyyyMMdd")}.dat";
                else strFileName = $"JYUSEI_{strFukenCode}_{DateTime.Now.ToString("yyyyMMdd")}_{intFileCount++.ToString("00")}.dat";

                sw = new System.IO.StreamWriter($"{strDir}\\{strFileName}", false, System.Text.Encoding.GetEncoding("shift-jis"));
                sw.Write(strres);
                sw.Close();

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
                return false;
            }
            finally
            {
                System.Diagnostics.Process.Start(strDir);

            }

        }

    }
}
