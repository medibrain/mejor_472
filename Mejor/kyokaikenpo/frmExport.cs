﻿using System;
using System.Windows.Forms;

namespace Mejor.kyokaikenpo

{
    public partial class frmExport : Form
    {
        public bool flgAll { get; set; }

        public frmExport()
        {
            InitializeComponent();
        }

       
        private void btnAll_Click(object sender, EventArgs e)
        {
            flgAll = true;
            this.Close();
        }

        private void btnSelected_Click(object sender, EventArgs e)
        {
            flgAll = false;
            this.Close();
        }
    }
}
