﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.kyokaikenpo
{
    //県情報クラス
    class clsPrefInfo
    {
        [DB.DbAttribute.PrimaryKey]
        public string prefid { get;set; }=string.Empty;          //県番号;
        public string name { get; set; } = string.Empty;         //県名;
        public string fullname { get; set; } = string.Empty;     //都道府県名;
        public string kana { get; set; } = string.Empty;         //カナ;
        public string dbname { get; set; } = string.Empty;       //db名;
        public string block { get; set; } = string.Empty;        //所属ブロック;



        /// <summary>
        /// 県情報
        /// </summary>
        /// <param name="strdbname">db名</param>
        /// <returns></returns>
        public static clsPrefInfo getPrefInfo(string strdbname)
        {
            clsPrefInfo cls = new clsPrefInfo();
            string currentdb = DB.GetMainDBName();

            DB.SetMainDBName("kk_90manage");
            DB.Command cmd = DB.Main.CreateCmd($"select * from pref where dbname='{strdbname}'");
            try
            {
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count==0)
                {
                    cls.prefid = "90";
                    cls.name = "test";
                    cls.fullname = "テスト協会けんぽ";
                    cls.kana = "テストキョウカイケンポ";
                    cls.dbname = "kyokaikenpo";
                    cls.block = "99";
                    return cls;

                }
                int c = 0;
                cls.prefid = res[c++].ToString();
                cls.name = res[c++].ToString();
                cls.fullname = res[c++].ToString();
                cls.kana = res[c++].ToString();
                cls.dbname = res[c++].ToString();
                cls.block = res[c++].ToString();


                return cls;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                DB.SetMainDBName(currentdb);
            }
        }
    }
}
