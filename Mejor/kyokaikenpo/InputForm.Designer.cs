﻿namespace Mejor.kyokaikenpo
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelFusho = new System.Windows.Forms.Panel();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxF1D = new Mejor.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.verifyBox4 = new Mejor.VerifyBox();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelOCR = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.lblDrCode01 = new System.Windows.Forms.Label();
            this.verifyBox5 = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.verifyBoxAccountNum = new Mejor.VerifyBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.label28 = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBox3 = new Mejor.VerifyBox();
            this.verifyBox2 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBox1 = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxKyufuRate = new Mejor.VerifyBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxJuryoType = new Mejor.VerifyBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxHKigo = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxKanriBango1 = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelFusho.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 753);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 13;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(205, 25);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(147, 26);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(361, 26);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(79, 13);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(143, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 759);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(44, 781);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(44, 781);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 754);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 754);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 754);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-39, 754);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(44, 781);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelFusho);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(264, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 781);
            this.panelRight.TabIndex = 2;
            // 
            // panelFusho
            // 
            this.panelFusho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelFusho.Controls.Add(this.verifyBoxF1FirstE);
            this.panelFusho.Controls.Add(this.label24);
            this.panelFusho.Controls.Add(this.verifyBoxF1);
            this.panelFusho.Controls.Add(this.label1);
            this.panelFusho.Controls.Add(this.verifyBoxDays);
            this.panelFusho.Controls.Add(this.labelDays);
            this.panelFusho.Controls.Add(this.verifyBoxF1D);
            this.panelFusho.Controls.Add(this.label27);
            this.panelFusho.Controls.Add(this.verifyBoxF1Start);
            this.panelFusho.Controls.Add(this.label6);
            this.panelFusho.Controls.Add(this.verifyBoxF1Finish);
            this.panelFusho.Controls.Add(this.label15);
            this.panelFusho.Controls.Add(this.label19);
            this.panelFusho.Controls.Add(this.label23);
            this.panelFusho.Controls.Add(this.verifyBoxF1M);
            this.panelFusho.Controls.Add(this.verifyBox4);
            this.panelFusho.Controls.Add(this.verifyBoxF1Y);
            this.panelFusho.Controls.Add(this.label29);
            this.panelFusho.Controls.Add(this.label10);
            this.panelFusho.Controls.Add(this.label2);
            this.panelFusho.Controls.Add(this.label7);
            this.panelFusho.Location = new System.Drawing.Point(19, 579);
            this.panelFusho.Name = "panelFusho";
            this.panelFusho.Size = new System.Drawing.Size(950, 80);
            this.panelFusho.TabIndex = 259;
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(439, 36);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 76;
            this.verifyBoxF1FirstE.TextV = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label24.Location = new System.Drawing.Point(466, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 36);
            this.label24.TabIndex = 77;
            this.label24.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(119, 36);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF1.TabIndex = 74;
            this.verifyBoxF1.TextV = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "負傷名1";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(781, 35);
            this.verifyBoxDays.MaxLength = 3;
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 63;
            this.verifyBoxDays.TextV = "";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(779, 18);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(43, 13);
            this.labelDays.TabIndex = 73;
            this.labelDays.Text = "実日数";
            // 
            // verifyBoxF1D
            // 
            this.verifyBoxF1D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1D.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1D.Location = new System.Drawing.Point(602, 36);
            this.verifyBoxF1D.MaxLength = 2;
            this.verifyBoxF1D.Name = "verifyBoxF1D";
            this.verifyBoxF1D.NewLine = false;
            this.verifyBoxF1D.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1D.TabIndex = 60;
            this.verifyBoxF1D.TextV = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(629, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 13);
            this.label27.TabIndex = 72;
            this.label27.Text = "日";
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(658, 36);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 61;
            this.verifyBoxF1Start.TextV = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(713, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 70;
            this.label6.Text = "終了日";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(717, 36);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 62;
            this.verifyBoxF1Finish.TextV = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(654, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 68;
            this.label15.Text = "開始日";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(746, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 71;
            this.label19.Text = "日";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(687, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 69;
            this.label23.Text = "日";
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(553, 36);
            this.verifyBoxF1M.MaxLength = 2;
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1M.TabIndex = 59;
            this.verifyBoxF1M.TextV = "";
            // 
            // verifyBox4
            // 
            this.verifyBox4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox4.Location = new System.Drawing.Point(25, 36);
            this.verifyBox4.MaxLength = 2;
            this.verifyBox4.Name = "verifyBox4";
            this.verifyBox4.NewLine = false;
            this.verifyBox4.Size = new System.Drawing.Size(27, 23);
            this.verifyBox4.TabIndex = 58;
            this.verifyBox4.TextV = "";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(509, 36);
            this.verifyBoxF1Y.MaxLength = 2;
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1Y.TabIndex = 57;
            this.verifyBoxF1Y.TextV = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(17, 18);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 65;
            this.label29.Text = "部位数";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 64;
            this.label10.Text = "初検日";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(580, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "月";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(537, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "年";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label11.Location = new System.Drawing.Point(7, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 72);
            this.label11.TabIndex = 258;
            this.label11.Text = "続紙    : \"--\" \r\n不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  : \"911\"\r\n状態記入書 " +
    " : \"921\"";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 139);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 137);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 433);
            this.scrollPictureControl1.TabIndex = 19;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 14;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 753);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(30, 13);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(172, 13);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.lblDrCode01);
            this.panelTotal.Controls.Add(this.verifyBox5);
            this.panelTotal.Controls.Add(this.label31);
            this.panelTotal.Controls.Add(this.label30);
            this.panelTotal.Controls.Add(this.verifyBoxAccountNum);
            this.panelTotal.Controls.Add(this.labelNumbering);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.label28);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Location = new System.Drawing.Point(371, 665);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(600, 80);
            this.panelTotal.TabIndex = 10;
            // 
            // lblDrCode01
            // 
            this.lblDrCode01.AutoSize = true;
            this.lblDrCode01.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblDrCode01.Location = new System.Drawing.Point(388, 33);
            this.lblDrCode01.Name = "lblDrCode01";
            this.lblDrCode01.Size = new System.Drawing.Size(37, 26);
            this.lblDrCode01.TabIndex = 147;
            this.lblDrCode01.Text = "[協]: 0\r\n[契]: 1";
            // 
            // verifyBox5
            // 
            this.verifyBox5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox5.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox5.Location = new System.Drawing.Point(7, 33);
            this.verifyBox5.MaxLength = 3;
            this.verifyBox5.Name = "verifyBox5";
            this.verifyBox5.NewLine = false;
            this.verifyBox5.Size = new System.Drawing.Size(40, 23);
            this.verifyBox5.TabIndex = 145;
            this.verifyBox5.TextV = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 13);
            this.label31.TabIndex = 146;
            this.label31.Text = "施術日数";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(261, 17);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(123, 13);
            this.label30.TabIndex = 142;
            this.label30.Text = "口座番号バッチから自動";
            // 
            // verifyBoxAccountNum
            // 
            this.verifyBoxAccountNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccountNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccountNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxAccountNum.Location = new System.Drawing.Point(264, 33);
            this.verifyBoxAccountNum.Name = "verifyBoxAccountNum";
            this.verifyBoxAccountNum.NewLine = false;
            this.verifyBoxAccountNum.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxAccountNum.TabIndex = 144;
            this.verifyBoxAccountNum.Text = "1234567";
            this.verifyBoxAccountNum.TextV = "";
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(432, 16);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(115, 13);
            this.labelNumbering.TabIndex = 141;
            this.labelNumbering.Text = "柔整師登録記号番号";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(431, 33);
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxDrCode.TabIndex = 143;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(163, 33);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxCharge.TabIndex = 34;
            this.verifyBoxCharge.TextV = "";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(160, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "請求金額";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(74, 33);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxTotal.TabIndex = 33;
            this.verifyBoxTotal.TextV = "";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(71, 17);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(55, 13);
            this.labelTotal.TabIndex = 12;
            this.labelTotal.Text = "合計金額";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBox3);
            this.panelHnum.Controls.Add(this.verifyBox2);
            this.panelHnum.Controls.Add(this.label22);
            this.panelHnum.Controls.Add(this.label3);
            this.panelHnum.Controls.Add(this.verifyBox1);
            this.panelHnum.Controls.Add(this.label9);
            this.panelHnum.Controls.Add(this.verifyBoxKyufuRate);
            this.panelHnum.Controls.Add(this.label25);
            this.panelHnum.Controls.Add(this.label26);
            this.panelHnum.Controls.Add(this.verifyBoxJuryoType);
            this.panelHnum.Controls.Add(this.label13);
            this.panelHnum.Controls.Add(this.label21);
            this.panelHnum.Controls.Add(this.label20);
            this.panelHnum.Controls.Add(this.label14);
            this.panelHnum.Controls.Add(this.verifyBoxHKigo);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxKanriBango1);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Controls.Add(this.label12);
            this.panelHnum.Location = new System.Drawing.Point(122, -1);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(900, 138);
            this.panelHnum.TabIndex = 7;
            // 
            // verifyBox3
            // 
            this.verifyBox3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox3.Location = new System.Drawing.Point(768, 18);
            this.verifyBox3.MaxLength = 1;
            this.verifyBox3.Name = "verifyBox3";
            this.verifyBox3.NewLine = false;
            this.verifyBox3.Size = new System.Drawing.Size(26, 23);
            this.verifyBox3.TabIndex = 59;
            this.verifyBox3.TextV = "";
            // 
            // verifyBox2
            // 
            this.verifyBox2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox2.Location = new System.Drawing.Point(76, 86);
            this.verifyBox2.MaxLength = 1;
            this.verifyBox2.Name = "verifyBox2";
            this.verifyBox2.NewLine = false;
            this.verifyBox2.Size = new System.Drawing.Size(26, 23);
            this.verifyBox2.TabIndex = 59;
            this.verifyBox2.TextV = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(688, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 60;
            this.label22.Text = "府県コード自動";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 60;
            this.label3.Text = "保険種別";
            // 
            // verifyBox1
            // 
            this.verifyBox1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox1.Location = new System.Drawing.Point(203, 86);
            this.verifyBox1.MaxLength = 1;
            this.verifyBox1.Name = "verifyBox1";
            this.verifyBox1.NewLine = false;
            this.verifyBox1.Size = new System.Drawing.Size(26, 23);
            this.verifyBox1.TabIndex = 57;
            this.verifyBox1.TextV = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(200, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 58;
            this.label9.Text = "単併区分";
            // 
            // verifyBoxKyufuRate
            // 
            this.verifyBoxKyufuRate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyufuRate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyufuRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyufuRate.Location = new System.Drawing.Point(496, 87);
            this.verifyBoxKyufuRate.Name = "verifyBoxKyufuRate";
            this.verifyBoxKyufuRate.NewLine = false;
            this.verifyBoxKyufuRate.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyufuRate.TabIndex = 18;
            this.verifyBoxKyufuRate.TextV = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(494, 64);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 13);
            this.label25.TabIndex = 53;
            this.label25.Text = "給付割合";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(525, 93);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 12);
            this.label26.TabIndex = 52;
            this.label26.Text = "7～10";
            // 
            // verifyBoxJuryoType
            // 
            this.verifyBoxJuryoType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxJuryoType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxJuryoType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxJuryoType.Location = new System.Drawing.Point(317, 87);
            this.verifyBoxJuryoType.MaxLength = 1;
            this.verifyBoxJuryoType.Name = "verifyBoxJuryoType";
            this.verifyBoxJuryoType.NewLine = false;
            this.verifyBoxJuryoType.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxJuryoType.TabIndex = 13;
            this.verifyBoxJuryoType.TextV = "";
            this.verifyBoxJuryoType.TextChanged += new System.EventHandler(this.verifyBoxJuryoType_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(315, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "本家区分";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(235, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 36);
            this.label21.TabIndex = 41;
            this.label21.Text = "1.単独 or 無\r\n2.２併 \r\n3.３併";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label20.Location = new System.Drawing.Point(108, 86);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 24);
            this.label20.TabIndex = 41;
            this.label20.Text = "1.協 2.組 3.共\r\n4.国 5.退 6.後期";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label14.Location = new System.Drawing.Point(345, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 48);
            this.label14.TabIndex = 41;
            this.label14.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７\r\n２つ○がある場合、8、0を優先";
            // 
            // verifyBoxHKigo
            // 
            this.verifyBoxHKigo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHKigo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHKigo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHKigo.Location = new System.Drawing.Point(447, 14);
            this.verifyBoxHKigo.Name = "verifyBoxHKigo";
            this.verifyBoxHKigo.NewLine = false;
            this.verifyBoxHKigo.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHKigo.TabIndex = 10;
            this.verifyBoxHKigo.TextV = "";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(553, 14);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(111, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxKanriBango1
            // 
            this.verifyBoxKanriBango1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKanriBango1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKanriBango1.Location = new System.Drawing.Point(249, 14);
            this.verifyBoxKanriBango1.MaxLength = 4;
            this.verifyBoxKanriBango1.Name = "verifyBoxKanriBango1";
            this.verifyBoxKanriBango1.NewLine = false;
            this.verifyBoxKanriBango1.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxKanriBango1.TabIndex = 1;
            this.verifyBoxKanriBango1.TextV = "";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(112, 14);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 0;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(768, 87);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 23;
            this.verifyBoxBD.TextV = "";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(725, 87);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 22;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(682, 87);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 21;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(631, 87);
            this.verifyBoxBE.MaxLength = 1;
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 20;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(564, 87);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 19;
            this.verifyBoxSex.TextV = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(627, 64);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 18;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(560, 64);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 15;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(657, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 39);
            this.label5.TabIndex = 13;
            this.label5.Text = "3.昭\r\n4.平\r\n5.令";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(591, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 26);
            this.label4.TabIndex = 17;
            this.label4.Text = "1.男\r\n2.女";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(794, 99);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(751, 99);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(708, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "年";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(184, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "ナンバリング";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(261, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 781);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1284, 781);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelFusho.ResumeLayout(false);
            this.panelFusho.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxHKigo;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private VerifyBox verifyBoxKanriBango1;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxJuryoType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxKyufuRate;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label label28;
        private VerifyBox verifyBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private VerifyBox verifyBox3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panelFusho;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Label labelDays;
        private VerifyBox verifyBoxF1D;
        private System.Windows.Forms.Label label27;
        private VerifyBox verifyBoxF1Start;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxF1M;
        private VerifyBox verifyBox4;
        private VerifyBox verifyBoxF1Y;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label30;
        private VerifyBox verifyBoxAccountNum;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private System.Windows.Forms.Label label24;
        private VerifyBox verifyBox5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblDrCode01;
    }
}