﻿namespace Mejor.Koshigayashi_Kokuho

{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.verifyBoxBuiCount = new Mejor.VerifyBox();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 753);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(137, 48);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(19, 13);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(39, 37);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(198, 8);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(43, 13);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(6, 10);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(59, 36);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++\r\nヘッダ:**";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(64, 48);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(31, 13);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 782);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 753);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 753);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-191, 753);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 753);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 782);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 782);
            this.panelRight.TabIndex = 0;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 92);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 477);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxTotal);
            this.panelHnum.Controls.Add(this.labelTotal);
            this.panelHnum.Controls.Add(this.labelMacthCheck);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.label23);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.verifyBoxNumbering);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Location = new System.Drawing.Point(158, 10);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(850, 76);
            this.panelHnum.TabIndex = 10;
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(352, 26);
            this.verifyBoxTotal.MaxLength = 10;
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxTotal.TabIndex = 30;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxTotal.Validated += new System.EventHandler(this.verifyBoxTotal_Validated);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(352, 9);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(55, 13);
            this.labelTotal.TabIndex = 151;
            this.labelTotal.Text = "合計金額";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(758, 11);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(86, 13);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(4, 27);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(93, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 9;
            this.label23.Text = "ナンバリング";
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(95, 27);
            this.verifyBoxNumbering.MaxLength = 8;
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxNumbering.TabIndex = 8;
            this.verifyBoxNumbering.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxNumbering.Validated += new System.EventHandler(this.verifyBoxNumbering_Validated);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(201, 27);
            this.verifyBoxHnum.MaxLength = 8;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(98, 37);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.dgv);
            this.panelTotal.Controls.Add(this.verifyBoxBuiCount);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstD);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstM);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstY);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.label42);
            this.panelTotal.Controls.Add(this.label12);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstE);
            this.panelTotal.Controls.Add(this.label13);
            this.panelTotal.Controls.Add(this.label5);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Location = new System.Drawing.Point(3, 580);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1000, 170);
            this.panelTotal.TabIndex = 70;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(7, 77);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(950, 90);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 150;
            this.dgv.TabStop = false;
            // 
            // verifyBoxBuiCount
            // 
            this.verifyBoxBuiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuiCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuiCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuiCount.Location = new System.Drawing.Point(19, 22);
            this.verifyBoxBuiCount.MaxLength = 3;
            this.verifyBoxBuiCount.Name = "verifyBoxBuiCount";
            this.verifyBoxBuiCount.NewLine = false;
            this.verifyBoxBuiCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBuiCount.TabIndex = 8;
            this.verifyBoxBuiCount.TextV = "";
            this.verifyBoxBuiCount.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(254, 22);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 48;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(204, 22);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 45;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(154, 22);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 43;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(120, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 36);
            this.label1.TabIndex = 40;
            this.label1.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(13, 4);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "部位数";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(233, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "月";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(93, 22);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 40;
            this.verifyBoxF1FirstE.TextV = "";
            this.verifyBoxF1FirstE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(285, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(152, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "初検日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(182, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "年";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 782);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 782);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 820);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Panel panelTotal;
        private VerifyBox verifyBoxBuiCount;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private System.Windows.Forms.Panel panelHnum;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labelMacthCheck;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxNumbering;
    }
}