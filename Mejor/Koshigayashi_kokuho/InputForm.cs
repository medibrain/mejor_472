﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Koshigayashi_Kokuho

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        private InputMode inputMode;

        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(800, 0);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// 柔整師登録記号番号
        /// </summary>
        Point posDrCode = new Point(1800, 2060);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, buiDateConts, hihoNameConts ,drCodeConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;

        /// <summary>
        /// 保険者提供情報
        /// </summary>
        List<importdata> lstimp = new List<importdata>();

        /// <summary>
        /// 患者（受療者）情報
        /// </summary>
        List<hihoInfo> lstHiho = new List<hihoInfo>();

        public static Dictionary<string, APP_ClinicInfo> dicClinicInfoDrNum = new Dictionary<string, APP_ClinicInfo>();

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxNumbering, };

            //合計、請求、往料、交付
            totalConts = new Control[] { verifyBoxTotal,};

            //初検日年号,負傷名等
            buiDateConts = new Control[] {
                verifyBoxF1FirstE,verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD,              
                verifyBoxBuiCount, };


            #endregion

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            panelTotal.Visible = false;
            panelHnum.Visible = false;

            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                setApp(app);
                //保険者提供情報取得
                if (lstimp.Count == 0) lstimp = importdata.SelectAll(app.CYM);

                //被保険者名リスト取得
                lstHiho = hihoInfo.GetHihoInfo();

                //提供データ選択状態にする
                createGrid(app.RrID);
            }
            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }



        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            panelHnum.Visible = false;
            panelTotal.Visible = false;

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }


            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
                panelHnum.Visible = true;
                panelTotal.Visible = true;

            }
            else
            {
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供データ表示グリッド設定
        /// </summary>
        private void InitGrid()
        {
            
            dgv.ColumnHeadersHeight = 20;
            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Font = new Font("Microsoft Sans Serif", 8);
            dgv.ColumnHeadersDefaultCellStyle = cs;

            dgv.Columns[nameof(importdata.importid)].Width = 50;            //importid=rrid
            dgv.Columns[nameof(importdata.cym)].Width = 50;                 //メホール請求年月            
            dgv.Columns[nameof(importdata.pbirthad)].Width = 50;            //生年月日西暦
            dgv.Columns[nameof(importdata.ymad)].Width = 50;                //診療年月西暦　
            dgv.Columns[nameof(importdata.entrydatetime)].Width = 50;       //登録年月日

            dgv.Columns[nameof(importdata.shoriym)].Width = 80;             //処理年月和暦　1列目;     
            dgv.Columns[nameof(importdata.comnum)].Width = 100;             //レセプト全国共通キー　2列目;
            dgv.Columns[nameof(importdata.clinicnum)].Width = 100;          //医療機関コード　6列目;
            dgv.Columns[nameof(importdata.clinicname)].Width =100;          //医療機関名(漢字)　8列目;
            dgv.Columns[nameof(importdata.insnum)].Width = 80;              //保険者番号　12列目;
            dgv.Columns[nameof(importdata.hihonum)].Width = 80;             //世帯番号＝被保険者証番号　16列目;
            dgv.Columns[nameof(importdata.hihoname)].Width = 80;            //被保険者氏名（漢字）＝受療者名とする　19列目;
            dgv.Columns[nameof(importdata.psex)].Width = 40;                //性別　20列目;     
            dgv.Columns[nameof(importdata.pbirth)].Width = 80;              //生年月日　28列目;     
            dgv.Columns[nameof(importdata.ym)].Width = 50;                  //診療年月　29列目;     
            dgv.Columns[nameof(importdata.days1)].Width = 50;               //保険_診療実日数　33列目;     
            dgv.Columns[nameof(importdata.ratio)].Width = 50;               //給付割合　45列目;     
            dgv.Columns[nameof(importdata.honke)].Width = 50;               //本人家族入外　46列目;     
            dgv.Columns[nameof(importdata.startdate1)].Width = 80;          //自支給期間＝開始日1　79列目;
            dgv.Columns[nameof(importdata.finishdate1)].Width = 80;         //至支給期間＝終了日1　80列目;
            dgv.Columns[nameof(importdata.total)].Width = 80;               //費用額＝合計金額　90列目;     
            dgv.Columns[nameof(importdata.charge)].Width = 80;              //支給予定額＝請求金額　91列目;     
            dgv.Columns[nameof(importdata.partial)].Width = 80;             //患者負担額＝一部負担金　93列目;     

            dgv.Columns[nameof(importdata.startdate1ad)].Width = 80;          //自支給期間西暦
            dgv.Columns[nameof(importdata.finishdate1ad)].Width = 80;         //至支給期間西暦




            dgv.Columns[nameof(importdata.importid)].HeaderText = "importid(rrid)";
            dgv.Columns[nameof(importdata.cym)].HeaderText = "メホール請求年月 ";
            dgv.Columns[nameof(importdata.pbirthad)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(importdata.ymad)].HeaderText = "診療年月西暦";
            dgv.Columns[nameof(importdata.entrydatetime)].HeaderText = "登録年月日";

            dgv.Columns[nameof(importdata.shoriym)].HeaderText = "処理年月和暦";                //処理年月和暦　1列目;     
            dgv.Columns[nameof(importdata.comnum)].HeaderText = "レセプト全国共通キー";         //レセプト全国共通キー　2列目;
            dgv.Columns[nameof(importdata.clinicnum)].HeaderText = "医療機関コード";          //医療機関コード　6列目;
            dgv.Columns[nameof(importdata.clinicname)].HeaderText = "医療機関名";       //医療機関名(漢字)　8列目;
            dgv.Columns[nameof(importdata.insnum)].HeaderText = "保険者番号";                   //保険者番号　12列目;
            dgv.Columns[nameof(importdata.hihonum)].HeaderText = "世帯番号";                    //世帯番号＝被保険者証番号　16列目;
            dgv.Columns[nameof(importdata.hihoname)].HeaderText = "被保険者氏名";               //被保険者氏名（漢字）＝受療者名とする　19列目;
            dgv.Columns[nameof(importdata.psex)].HeaderText = "性別";                           //性別　20列目;     
            dgv.Columns[nameof(importdata.pbirth)].HeaderText = "生年月日";                     //生年月日　28列目;     
            dgv.Columns[nameof(importdata.ym)].HeaderText = "診療年月";                         //診療年月　29列目;     
            dgv.Columns[nameof(importdata.days1)].HeaderText = "保険_診療実日数";               //保険_診療実日数　33列目;     
            dgv.Columns[nameof(importdata.ratio)].HeaderText = "給付割合";                      //給付割合　45列目;     
            dgv.Columns[nameof(importdata.honke)].HeaderText = "本人家族入外";                  //本人家族入外　46列目;     
            dgv.Columns[nameof(importdata.startdate1)].HeaderText = "自支給期間";               //自支給期間＝開始日1　79列目;
            dgv.Columns[nameof(importdata.finishdate1)].HeaderText = "至支給期間";              //至支給期間＝終了日1　80列目;
            dgv.Columns[nameof(importdata.total)].HeaderText = "費用額";                        //費用額＝合計金額　90列目;     
            dgv.Columns[nameof(importdata.charge)].HeaderText = "支給予定額";                   //支給予定額＝請求金額　91列目;     
            dgv.Columns[nameof(importdata.partial)].HeaderText = "患者負担額";                  //患者負担額＝一部負担金　93列目;     

            dgv.Columns[nameof(importdata.startdate1ad)].HeaderText = "自支給期間西暦";
            dgv.Columns[nameof(importdata.finishdate1ad)].HeaderText = "至支給期間西暦";




            dgv.Columns[nameof(importdata.importid)].Visible = true;    //importid=rrid";
            dgv.Columns[nameof(importdata.cym)].Visible = false;        //メホール請求年月 ";
            dgv.Columns[nameof(importdata.pbirthad)].Visible = false;        //生年月日西暦";
            dgv.Columns[nameof(importdata.ymad)].Visible = false;             //診療年月西暦";
            dgv.Columns[nameof(importdata.entrydatetime)].Visible = true;     //登録年月日";


            dgv.Columns[nameof(importdata.shoriym)].Visible = true;           //処理年月和暦　1列目;     
            dgv.Columns[nameof(importdata.comnum)].Visible = true;            //レセプト全国共通キー　2列目;
            dgv.Columns[nameof(importdata.clinicnum)].Visible = true;         //医療機関コード　6列目;
            dgv.Columns[nameof(importdata.clinicname)].Visible = true;        //医療機関名(漢字)　8列目;
            dgv.Columns[nameof(importdata.insnum)].Visible = true;            //保険者番号　12列目;
            dgv.Columns[nameof(importdata.hihonum)].Visible = true;           //世帯番号＝被保険者証番号　16列目;
            dgv.Columns[nameof(importdata.hihoname)].Visible = true;          //被保険者氏名（漢字）＝受療者名とする　19列目;
            dgv.Columns[nameof(importdata.psex)].Visible = true;              //性別　20列目;     
            dgv.Columns[nameof(importdata.pbirth)].Visible = true;            //生年月日　28列目;     
            dgv.Columns[nameof(importdata.ym)].Visible = true;                //診療年月　29列目;     
            dgv.Columns[nameof(importdata.days1)].Visible = true;             //保険_診療実日数　33列目;     
            dgv.Columns[nameof(importdata.ratio)].Visible = true;             //給付割合　45列目;     
            dgv.Columns[nameof(importdata.honke)].Visible = true;             //本人家族入外　46列目;     
            dgv.Columns[nameof(importdata.startdate1)].Visible = true;        //自支給期間＝開始日1　79列目;
            dgv.Columns[nameof(importdata.finishdate1)].Visible = true;       //至支給期間＝終了日1　80列目;
            dgv.Columns[nameof(importdata.total)].Visible = true;             //費用額＝合計金額　90列目;     
            dgv.Columns[nameof(importdata.charge)].Visible = true;            //支給予定額＝請求金額　91列目;     
            dgv.Columns[nameof(importdata.partial)].Visible = true;           //患者負担額＝一部負担金　93列目;     


            dgv.Columns[nameof(importdata.startdate1ad)].Visible = false; //"自支給期間西暦";
            dgv.Columns[nameof(importdata.finishdate1ad)].Visible = false;// "至支給期間西暦";



        }

        /// <summary>
        /// 保険者提供情報表示
        /// </summary>
        /// <param name="shinryoym">診療年月yyyymm</param>
        /// <param name="total">合計金額</param>
        /// <param name="hihonum">被保険者証番号</param>
        private void createGrid(int shinryoym,int total,string hihonum)
        {
            App app = (App)bsApp.Current;

            List<importdata> lst = new List<importdata>();
          
            foreach (importdata item in lstimp)
            {
                if (item.ymad == shinryoym &&
                    int.Parse(item.total)==total &&
                    item.hihonum==hihonum
                    )
                {
                    //診療年月、合計金額、被保険者証番号が合致したらリスト表示
                    lst.Add(item);
                }
            }


            dgv.DataSource = lst;

            
            if (lst == null || lst.Count == 0)
            {
                //合致するレコードがない場合

                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (lst.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                //合致

                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                //合致するレコードが複数

                for (int i = 0; i < dgv.RowCount; i++)
                {
                    
                    var rrid = (int)dgv[nameof(importdata.importid), i].Value;

                    if (app.RrID == rrid )
                    {
                        //既に一意のデータとマッチング済みの場合
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;

                        
                        //グリッドの現在のセルが不可視の場合選択しない（例外発生する）
                        if (dgv[0, i].Visible) dgv.CurrentCell = dgv[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dgv.CurrentCell = null;
            }

            //グリッドのフォーマット設定
            InitGrid();
        }



        /// <summary>
        /// 保険者提供情報グリッド
        /// </summary>
        private void createGrid(int rrid)
        {

            List<importdata> lstai = new List<importdata>();


            foreach (importdata item in lstimp)
            {
                if (item.importid == rrid)
                {
                    lstai.Add(item);
                }
            }
            dgv.DataSource = null;
            dgv.DataSource = lstai;

            #region グリッド初期化
            InitGrid();
            #endregion

            if (rrid !=0)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (r.Cells["importid"].Value.ToString() == rrid.ToString())
                    {
                        r.Selected = true;
                        break;
                    }
                }
            }



            if (lstai == null || lstai.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (lstai.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        #endregion


        #region 各種ロード

       
        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            panelHnum.Visible = false;
            panelTotal.Visible = false;

            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }

       
            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //setvalueしないとステータスが更新されない
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //setvalueしないとステータスが更新されない                
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {

                setValue(verifyBoxY,"**", firstTime, nv);
                
            }
            else
            {
                //申請書

                //診療和暦年
                setValue(verifyBoxY, app.MediYear, firstTime, nv);

                //診療和暦月
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);

                //被保険者番号
                setValue(verifyBoxHnum, app.HihoNum.ToString(), firstTime, nv);

                //初検日1
                setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                //部位数入力
                setValue(verifyBoxBuiCount, app.TaggedDatas.count, firstTime, nv);
                
                //ナンバリング 4桁0うめ
                setValue(verifyBoxNumbering, app.Numbering.ToString(), firstTime, nv);

                //グリッド選択する
                createGrid(app.RrID);

                //合計金額
                setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);


            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外

            if (verifyBoxY.Text == "**"  )
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                
                //申請書タイプをバッチにする                
                app.AppType = APP_TYPE.バッチ;
           

                #endregion                
            }
            else if (verifyBoxY.Text == "--")
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;

                //申請書タイプ
                app.AppType = APP_TYPE.続紙;


                #endregion
            }
            else if (verifyBoxY.Text == "++")
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.不要;

                //申請書タイプ          
                app.AppType = APP_TYPE.不要;


                #endregion
            }
            //申請書
            else
            {
                #region 入力チェック
                //和暦月
                int month = verifyBoxM.GetIntValue();
                setStatus(verifyBoxM, month < 1 || 12 < month);

                //和暦年
                int year = verifyBoxY.GetIntValue();
                setStatus(verifyBoxY, year < 1 || 31 < year);
                int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                //被保険者番号
                string strverifyBoxHnum = verifyBoxHnum.Text.Trim();
                setStatus(verifyBoxHnum, strverifyBoxHnum.Length > 12);

                //初検日1
                DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                //部位数入力
                int buicount = verifyBoxBuiCount.GetIntValue();
                setStatus(verifyBoxBuiCount, buicount<=0);

                //ナンバリング
                int numbering = verifyBoxNumbering.GetIntValue();
                setStatus(verifyBoxNumbering, numbering <= 0 || numbering.ToString().Length>4);

                //合計金額
                int total = verifyBoxTotal.GetIntValue();
                setStatus(verifyBoxTotal, total < 100 || total > 200000);


                //ここまでのチェックで必須エラーが検出されたらfalse
                if (hasError)
                {
                    showInputErrorMessage();
                    return false;
                }

               

                #endregion

                #region Appへの反映
                             
                //柔整師登録記号番号（契・協区別）
                
                
                //ここから値の反映
                app.MediYear = year;                //施術年
                app.MediMonth = month;              //施術月

                //被保険者番号
                app.HihoNum = strverifyBoxHnum;

                app.Total = total;                  //合計金額

                //部位数入力追加
                app.TaggedDatas.count = buicount;//部位数入力

                //ナンバリング4桁0埋め
                app.Numbering = numbering.ToString().PadLeft(4, '0');

                //初検年月
                int start1ym = DateTimeEx.GetAdYearMonthFromJyymm(
                    verifyBoxF1FirstE.GetIntValue() * 10000 +
                    verifyBoxF1FirstY.GetInt100Value() +
                    verifyBoxF1FirstM.GetIntValue());
                //診療年月
                int ym = DateTimeEx.GetAdYearFromHs(year * 100 + month);
                
                //初検年月＝診療年月の場合　新規とみなす
                app.NewContType = start1ym == ym ? NEW_CONT.新規 : NEW_CONT.継続;

                //初検日
                app.FushoFirstDate1 = f1FirstDt;

                //申請書タイプ
                app.AppType = scan.AppType;


                //インポートデータより
                if (dgv.Rows.Count > 0)
                {
                    using (DataGridViewRow dgvr = dgv.CurrentRow)
                    {
                        //給付割合
                        int.TryParse(dgvr.Cells["ratio"].Value.ToString(), out int dgvRatio);


                        //20210209174734 furukawa st ////////////////////////
                        //合計×給付割合で請求金額を算出する場合、計算はDecimal型でやらないと丸め誤差出るので関数を使用する
                        
                        app.Charge = CommonTool.CalcCharge(app.Total, dgvRatio);


                            //      app.Ratio = dgvRatio / 10;

                            //      //請求金額　合計金額×給付割合　1円未満切り捨て   
                            //      double dblcharge = double.Parse(app.Total.ToString()) * double.Parse(app.Ratio.ToString()) / 10;
                            //      decimal tmpdeccharge = Math.Truncate(decimal.Parse(dblcharge.ToString()));
                            //      app.Charge = int.Parse(tmpdeccharge.ToString());
                        //20210209174734 furukawa ed ////////////////////////


                        //一部負担金
                        app.Partial = app.Total - app.Charge;

                        //rrid登録
                        app.RrID = int.Parse(dgvr.Cells["importid"].Value.ToString());

                        //レセプト全国共通キー
                        app.ComNum = dgvr.Cells["comnum"].Value.ToString();
                        //保険者番号
                        app.InsNum = dgvr.Cells["insnum"].Value.ToString();
                      
                        //生年月日
                        app.Birthday = DateTime.Parse(dgvr.Cells["pbirthad"].Value.ToString());
                        
                        //性別
                        string strgender = dgvr.Cells["psex"].Value.ToString();
                        
                        //20201222112534 furukawa st ////////////////////////
                        //不要な性別判定が入ってた                        
                        app.Sex = int.Parse(strgender);
                        //app.Sex = strgender == "男" ? 1 : 2;
                        //20201222112534 furukawa ed ////////////////////////

                        //医療機関番号
                        app.ClinicNum = dgvr.Cells["clinicnum"].Value.ToString();
                        //医療機関名
                        app.ClinicName = dgvr.Cells["clinicname"].Value.ToString();

                        //20201222112719 furukawa st ////////////////////////
                        //被保険者氏名不要

                        //被保険者名
                        //app.HihoName = dgvr.Cells["hihoname"].Value.ToString();
                        //20201222112719 furukawa ed ////////////////////////



                        //20201222120332 furukawa st ////////////////////////
                        //参照先間違った
                        
                        //受療者名=被保険者名とする
                        app.PersonName = dgvr.Cells["hihoname"].Value.ToString();
                        //app.PersonName = app.HihoName;
                        //20201222120332 furukawa ed ////////////////////////


                        //実日数
                        app.CountedDays = !int.TryParse(dgvr.Cells["days1"].Value.ToString(), out int tmpdays1) ? 0 : tmpdays1;

                        //本家
                        app.Family = !int.TryParse(dgvr.Cells["honke"].Value.ToString(), out int tmpfamily) ? 0 : tmpfamily;
                        //開始日1
                        app.FushoStartDate1 = !DateTime .TryParse(dgvr.Cells["startdate1ad"].Value.ToString(), out DateTime tmpstart) ? DateTime.MinValue : tmpstart;
                        //終了日1
                        app.FushoFinishDate1 = !DateTime.TryParse(dgvr.Cells["finishdate1ad"].Value.ToString(), out DateTime tmpfinish) ? DateTime.MinValue : tmpfinish;
                    }
                }

                
             
                #endregion

            }


            return true;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
             
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                
            }
            else if (verifyBoxY.Text == "**")
            {
                //ヘッダ（バッチ扱いとする）
                resetInputData(app);

                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                app.AppType = APP_TYPE.バッチ;

            }
            

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }


            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;


            //20201228143927 furukawa st ////////////////////////
            //マッチング無し確認は申請書のみ
            
            if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
            {
                //マッチングなし確認

                if (dgv.Rows.Count == 0)
                {
                    if (MessageBox.Show("マッチングなしですがよろしいですか？",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            //20201228143927 furukawa ed ////////////////////////

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; 
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        


        /// <summary>
        /// 合計金額入力後マッチング
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            int shinryoym = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue())*100 + verifyBoxM.GetIntValue();
            createGrid(shinryoym, verifyBoxTotal.GetIntValue(), verifyBoxHnum.Text);


        }



        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            //20201222141107 furukawa st ////////////////////////
            //被保険者証番号は前ゼロつき7桁
            verifyBoxHnum.Text = verifyBoxHnum.Text.PadLeft(7, '0');
            //20201222141107 furukawa ed ////////////////////////

        }

        private void verifyBoxNumbering_Validated(object sender, EventArgs e)
        {
            verifyBoxNumbering.Text = verifyBoxNumbering.Text.PadLeft(4, '0');
        }




        #endregion

        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;            
            else if (totalConts.Contains(sender)) p = posTotal;           
            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (hihoNameConts.Contains(sender)) p = posHname;
            else if (drCodeConts.Contains(sender)) p = posDrCode;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (hihoNameConts.Any(c => c.Focused)) posHname = pos;

        }
        #endregion


    
       
    }      
}
