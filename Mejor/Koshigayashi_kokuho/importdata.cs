﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Koshigayashi_Kokuho
{
    class importdata
    {
        #region メンバ
        [DB.DbAttribute.Serial]         
        public  int importid { get;set; }=0;                                         //プライマリキー メホール管理用;
        public  int cym { get; set; } = 0;                                           //処理年月西暦　メホール管理用;
        public  DateTime pbirthad { get; set; } = DateTime.MinValue;                 //受療者生年月日西暦　メホール管理用;
        public  int ymad { get; set; } = 0;                                          //診療年月西暦　メホール管理用;
        public  DateTime entrydatetime { get; set; } = DateTime.MinValue;            //登録年月日時刻　メホール管理用;

        public  string shoriym { get; set; } = string.Empty;                         //処理年月和暦　1列目;

        [DB.DbAttribute.PrimaryKey]
        public  string comnum { get; set; } = string.Empty;                          //レセプト全国共通キー　2列目;
        public  string clinicnum { get; set; } = string.Empty;                       //医療機関コード　6列目;
        public  string clinicname { get; set; } = string.Empty;                      //医療機関名(漢字)　8列目;
        public  string insnum { get; set; } = string.Empty;                          //保険者番号　12列目;
        public  string hihonum { get; set; } = string.Empty;                         //世帯番号＝被保険者証番号　16列目;
        public  string hihoname { get; set; } = string.Empty;                        //被保険者氏名（漢字）＝受療者名とする　19列目;
        public  string psex { get; set; } = string.Empty;                            //性別　20列目;
        public  string pbirth { get; set; } = string.Empty;                          //生年月日　28列目;
        public  string ym { get; set; } = string.Empty;                              //診療年月　29列目;
        public  string days1 { get; set; } = string.Empty;                           //保険_診療実日数　33列目;
        public  string ratio { get; set; } = string.Empty;                           //給付割合　45列目;
        public  string honke { get; set; } = string.Empty;                           //本人家族入外　46列目;
        public  string startdate1 { get; set; } = string.Empty;                      //自支給期間和暦＝開始日1　79列目;
        public  string finishdate1 { get; set; } = string.Empty;                     //至支給期間和暦＝終了日1　80列目;
        public  string total { get; set; } = string.Empty;                           //費用額＝合計金額　90列目;
        public  string charge { get; set; } = string.Empty;                          //支給予定額＝請求金額　91列目;
        public  string partial { get; set; } = string.Empty;                         //患者負担額＝一部負担金　93列目;

        public  DateTime startdate1ad { get; set; } = DateTime.MinValue;             //自支給期間西暦;
        public  DateTime finishdate1ad { get; set; } = DateTime.MinValue;            //至支給期間西暦;



        #endregion


        /// <summary>
        /// 提供データ用リスト
        /// </summary>
        private static List<importdata> lstImp = new List<importdata>();



        /// <summary>
        /// 和暦変換
        /// </summary>
        private static System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-jp");
        

              

        /// <summary>
        /// インポート処理
        /// </summary>
        /// <param name="_cym">メホール処理年月</param>
        public static void Import(int _cym)
        {
            //和暦カレンダー
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //インポートファイル選択
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;
            string strFileName = ofd.FileName;



            WaitForm wf =new WaitForm();
            wf.ShowDialogOtherTask();
            

            wf.LogPrint("csv情報取得");      

            List<string[]> lst=CommonTool.CsvImportMultiCode(strFileName);

            DB.Transaction tran = DB.Main.CreateTransaction();

    
            //wf.LogPrint($"{_cym}削除");
            //DB.Command cmd = new DB.Command($"delete from importdata where cym={_cym}", tran);
            //cmd.TryExecuteNonQuery();


            try
            {                
                wf.LogPrint("情報インポート");
                wf.SetMax(lst.Count);                
                
                foreach (string[] s in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return;
                   
                    //最初のレコードが数字でない場合飛ばす
                    if (!int.TryParse(s[0].ToString(), out int tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }
                    importdata impdata = new importdata();


                    impdata.shoriym = s[0].ToString();                   //処理年月和暦　1列目;
                    impdata.comnum = s[1].ToString();                    //レセプト全国共通キー　2列目;
                    impdata.clinicnum = s[5].ToString();                 //医療機関コード　6列目;
                    impdata.clinicname = s[7].ToString();                //医療機関名(漢字)　8列目;
                    impdata.insnum = s[11].ToString();                   //保険者番号　12列目;


                    //20201222134834 furukawa st ////////////////////////
                    //被保険者証番号は前ゼロつき7桁
                    
                    impdata.hihonum = s[15].ToString().PadLeft(7,'0');                  //世帯番号＝被保険者証番号　16列目;
                    //impdata.hihonum = s[15].ToString();                  //世帯番号＝被保険者証番号　16列目;
                    //20201222134834 furukawa ed ////////////////////////



                    impdata.hihoname = s[18].ToString();                 //被保険者氏名（漢字）＝受療者名とする　19列目;
                    impdata.psex = s[19].ToString();                     //性別　20列目;
                    impdata.pbirth = s[27].ToString();                   //生年月日　28列目;
                    impdata.ym = s[28].ToString();                       //診療年月　29列目;
                    impdata.days1 = s[32].ToString();                    //保険_診療実日数　33列目;
                    impdata.ratio = s[44].ToString();                    //給付割合　45列目;
                    impdata.honke = s[45].ToString();                    //本人家族入外　46列目;
                    impdata.startdate1 = s[78].ToString();               //自支給期間＝開始日1　79列目;
                    impdata.finishdate1 = s[79].ToString();              //至支給期間＝終了日1　80列目;
                    impdata.total = s[89].ToString();                    //費用額＝合計金額　90列目;
                    impdata.charge = s[90].ToString();                   //支給予定額＝請求金額　91列目;
                    impdata.partial = s[92].ToString();			         //患者負担額＝一部負担金　93列目;

                    impdata.cym = _cym;
                    impdata.pbirthad = DateTimeEx.GetDateFromJstr7(impdata.pbirth);
                    impdata.ymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(impdata.ym));
                    impdata.entrydatetime = DateTime.Now;

                    impdata.startdate1ad = DateTimeEx.GetDateFromJstr7(impdata.startdate1);
                    impdata.finishdate1ad = DateTimeEx.GetDateFromJstr7(impdata.finishdate1);

                    //登録やり直しをどうやろう？→nextvalで取っている以上難しい
                    wf.LogPrint($"レセプト全国共通キー:{impdata.comnum}");
                    if (!DB.Main.Insert<importdata>(impdata, tran))
                    {
                        System.Windows.Forms.MessageBox.Show("データを取り込むことができませんでした","Mejor",
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);   
                        return;
                    };
                    wf.InvokeValue++;
                }


                tran.Commit();
                System.Windows.Forms.MessageBox.Show("終了");
                return ;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return ;
            }
            finally
            {
                wf.Dispose();
            }

        }



        /// <summary>
        /// 情報全取得
        /// </summary>
        /// <returns> List<importdata> </returns>
        public static List<importdata> SelectAll(int _cym)
        {
            string strsql = string.Empty;
            strsql += "";

            DB.Command cmd = DB.Main.CreateCmd("select * from importdata " +
                $"where cym={_cym}" +
                "order by importid");

            var list=cmd.TryExecuteReaderList();
            lstImp.Clear();

            foreach (var item in list)
            {

                importdata id = new importdata();
                int col = 0;

                id.importid = int.Parse(item[col++].ToString());                //プライマリキー メホール管理用;
                id.cym = int.Parse(item[col++].ToString());                   //処理年月西暦　メホール管理用;
                id.pbirthad = DateTime.Parse(item[col++].ToString());           //受療者生年月日西暦　メホール管理用;
                id.ymad = int.Parse(item[col++].ToString());                    //診療年月西暦　メホール管理用;
                id.entrydatetime= DateTime.Parse(item[col++].ToString());      //登録年月日時刻　メホール管理用;


                id.shoriym = item[col++].ToString();                                //処理年月和暦　1列目;     
                id.comnum = item[col++].ToString();                                 //レセプト全国共通キー　2列目;
                id.clinicnum = item[col++].ToString();                              //医療機関コード　6列目;
                id.clinicname = item[col++].ToString();                             //医療機関名(漢字)　8列目;
                id.insnum = item[col++].ToString();                                 //保険者番号　12列目;
                id.hihonum = item[col++].ToString();                                //世帯番号＝被保険者証番号　16列目;
                id.hihoname = item[col++].ToString();                               //被保険者氏名（漢字）＝受療者名とする　19列目;
                id.psex = item[col++].ToString();                                   //性別　20列目;     
                id.pbirth = item[col++].ToString();                                 //生年月日　28列目;     
                id.ym = item[col++].ToString();                                     //診療年月　29列目;     
                id.days1 = item[col++].ToString();                                  //保険_診療実日数　33列目;     
                id.ratio = item[col++].ToString();                                  //給付割合　45列目;     
                id.honke = item[col++].ToString();                                  //本人家族入外　46列目;     
                id.startdate1 = item[col++].ToString();                             //自支給期間＝開始日1　79列目;
                id.finishdate1 = item[col++].ToString();                            //至支給期間＝終了日1　80列目;
                id.total = item[col++].ToString();                                  //費用額＝合計金額　90列目;     
                id.charge = item[col++].ToString();                                 //支給予定額＝請求金額　91列目;     
                id.partial = item[col++].ToString();                                //患者負担額＝一部負担金　93列目;     


                id.startdate1ad = DateTime.Parse(item[col++].ToString());           //自支給期間西暦
                id.finishdate1ad = DateTime.Parse(item[col++].ToString());          //至支給期間西暦

                lstImp.Add(id);
            }


            return lstImp;
        }
    }
}
