﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.Futaba
{
    class SubmitData
    {
        App app;

        string mark => app.HihoNum.Split('-')[0];

        string RecordID => "0";
        string DataType => "5";
        string ReceType => "1";
        string Kago => "0";
        string KagoReason => "000";
        string Direct => "1";
        string Toriatsukai => "0";
        string FutanType => app.Family % 100 == 4 ? "1" :
            app.Family % 100 == 8 ? "2" :
            app.Family % 100 == 0 ? "3" : "0";
        string MediYM => ((DateTimeEx.GetGyymmFromAdYM(app.YM) / 10000) * 2 - 1).ToString() +
            (DateTimeEx.GetGyymmFromAdYM(app.YM) % 10000).ToString("0000");
        string Jigyosya => (mark == "11" ? "1" :
            mark == "12" ? "2" :
            mark == "13" ? "3" :
            mark == "14" ? "4" :
            mark == "15" ? "5" :
            mark == "50" ? "88888" : string.Empty).PadLeft(5, '0');
        string HihoNum => app.HihoNum.Split('-')[1].PadLeft(10, '0');
        string MealCount => "000";
        string MealCharge => "0000000";
        string MealKohi => "0000000";
        string MearFutan => "00000";

        //20190123091744 furukawa st ////////////////////////
        //あんまを00から22に変更
        
        string SickCode => app.AppType == APP_TYPE.柔整 ? "0021" :
            app.AppType == APP_TYPE.鍼灸 ? "0023" : "0022";

        //string SickCode => app.AppType == APP_TYPE.柔整 ? "0021" :
        //    app.AppType == APP_TYPE.鍼灸 ? "0023" : "0000";
        //20190123091744 furukawa ed ////////////////////////


        string DrugFutan => "00000";
        string DrugKohi => "00000";
        string Sex => app.Sex.ToString();
        string Birthday => ((DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday) / 1000000) * 2 - 1).ToString() +
            (DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday) % 1000000).ToString("000000");
        string ShinryoType => "2";
        string ShinryoStartDate => ((DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1) / 1000000) * 2 - 1).ToString() +
            (DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1) % 1000000).ToString("000000");
        string Days => app.CountedDays.ToString().PadLeft(3, '0');
        string Score => "0".PadLeft(9, '0');
        string Total => app.Total.ToString().PadLeft(9, '0');
        string Shosin => "0".PadLeft(7, '0');
        string Nyuin => "0".PadLeft(7, '0');
        string KohiCode => "0".PadLeft(2, '0');
        string KohiCost => "0".PadLeft(9, '0');
        string Futan => "0".PadLeft(7, '0');
        string TantoCode => "  ";
        string Tekio1 => new string(' ', 13);
        string Tekio2 => new string(' ', 20);

        //20190123093328 furukawa st ////////////////////////
        //あんまの医療機関コードを3に

        string ClinicCode => (app.AppType == APP_TYPE.柔整 ? "10000" :
            app.AppType == APP_TYPE.鍼灸 ? "20000" : "30000").PadLeft(9, '0');

        //string ClinicCode => (app.AppType == APP_TYPE.柔整 ? "10000" :
        //    app.AppType == APP_TYPE.鍼灸 ? "20000" : "").PadLeft(9, '0');
        //20190123093328 furukawa ed ////////////////////////

        string Pref => app.HihoPref.ToString("00");
        string Family => app.Family % 100 == 8 ? "00" :
            app.Family % 100 == 0 ? "00" :
            (app.Family / 100).ToString().PadLeft(2, '0');
        string Tokki => "00";

        string createLine()
        {
            return RecordID + DataType + ReceType + Kago + KagoReason +
                Direct + Toriatsukai + FutanType + MediYM + Jigyosya +
                HihoNum + MealCount + MealCharge + MealKohi + MearFutan +
                SickCode + DrugFutan + DrugKohi + Sex + Birthday +
                ShinryoType + ShinryoStartDate + Days + Score + Total +
                Shosin + Nyuin + KohiCode + KohiCost + Futan +
                TantoCode + Tekio1 + Tekio2 + ClinicCode + Pref +
                Family + Tokki;
        }

        public SubmitData(App app)
        {
            this.app = app;
        }





        /// <summary>
        /// 20200305104817 furukawa 保険者のシステムベンダーが大和総研に変わったので、名古屋港湾と同じ処理にする
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <returns></returns>
        public static bool DoExport(int cym)
        {
            string dir;
            
            //using (var f = new System.Windows.Forms.FolderBrowserDialog())
            //{

            //    if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            //    dir = f.SelectedPath;
            //}

            //フォルダダイアログはこっちのほうが使いやすい
            Mejor.OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            dir = dlg.Name;


            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("対象の申請書を取得しています");
            var file = dir + "\\" + cym.ToString("00") + ".txt";

            var apps = App.GetApps(cym, InsurerID.FUTABA);

            //保留データを削除
            apps.RemoveAll(x => x.StatusFlagCheck(StatusFlag.支払保留));

            wf.SetMax(apps.Count);
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

            wf.LogPrint("対象の申請書を出力データ形式に変換しています");
            var deList = new List<DaiwaExport>();

            int numbering = 50000000;

            for (int i = 0; i < apps.Count; i++)
            {
                var a = apps[i];

                //保留データをskip
                if (a.StatusFlagCheck(StatusFlag.支払保留))
                {
                    //次の申請書までskip（別続紙添付回避のため）
                    while (i < apps.Count - 1 && apps[i + 1].YM < 0) i++;
                    continue;
                }

                if (a.MediYear > 0) numbering++;
                a.Numbering = numbering.ToString("00000000000");

                
                //2020-03-10仕様より
                //組合コードは下記を設定頂けますでしょうか。
                //組合コード：FG
                //支払先コードは、柔整師会コードなるものを入れる
                deList.Add(DaiwaExport.Create(a, "FG", "01", a.PayCode, true));

            }


            try
            {
                wf.LogPrint("大和総研データを出力しています");
                using (var sw = new System.IO.StreamWriter(file, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (var item in deList)
                    {
                        if (wf.Cancel)
                        {
                            System.Windows.Forms.MessageBox.Show("出力を中止しました。途中までのデータが残されていますのでご注意ください。");
                            return false;
                        }

                        wf.InvokeValue++;
                        if (item.Ayear > 0) sw.WriteLine(item.ToString());
                    }
                }

                wf.LogPrint("基金形式画像データを出力しています");
                DaiwaExport.KikinTypeExport(deList, dir, wf);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);

                //20210511112353 furukawa st ////////////////////////
                //エラー発生場所を記載
                
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name+"\r\n出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                //System.Windows.Forms.MessageBox.Show("出力に失敗しました。エラーデータが残されていますのでご注意ください。");
                //20210511112353 furukawa ed ////////////////////////


                return false;
            }
            finally
            {
                wf.Dispose();
            }

            System.Windows.Forms.MessageBox.Show("出力が完了しました");
            return true;
        }


        /// <summary>
        /// 2020年3月までの出力処理
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool Export(int cym)
        {
            string csvName;
            string dir;

            using (var f = new System.Windows.Forms.SaveFileDialog())
            {
                f.FileName = "RF70.TXT";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;

                csvName = f.FileName;
                dir = System.IO.Path.GetDirectoryName(csvName);
            }

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("対象データを取得しています");
                var l = App.GetApps(cym);

                //保留データを削除
                l.RemoveAll(x => x.StatusFlagCheck(StatusFlag.支払保留));

                var lastNumbering = string.Empty;
                var fn = string.Empty;

                var fc = new TiffUtility.FastCopy();

                wf.LogPrint("出力しています");
                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                using (var sw = new System.IO.StreamWriter(csvName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        wf.InvokeValue++;
                        if ((int)l[i].AppType < 1)
                        {
                            //申請書以外
                            continue;
                        }
                        else
                        {
                            //csv出力
                            var sf = new SubmitData(l[i]);
                            sw.WriteLine(sf.createLine());
                        }
                    }

                    System.Windows.Forms.MessageBox.Show("出力が完了しました");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                System.Windows.Forms.MessageBox.Show("出力に失敗しました","",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }
    }
}
