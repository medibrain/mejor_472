﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Futaba
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 800);
        Point posDays = new Point(400, 800);
        Point posNewCont = new Point(400, 800);
        Point posOryo = new Point(400, 800);
        Point posCost = new Point(800, 1800);
        Point posBank = new Point(800, 2000);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls,newContControls, oryoControls, bankControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hihoNumControls = new Control[] { verifyBoxNumM, verifyBoxNumN, verifyBoxPref, verifyBoxJuryoType };
            personControls = new Control[] { verifyBoxFamily, verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge,  };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, verifyBoxStartDate };
            newContControls = new Control[] { verifyBoxNewCont };
            oryoControls = new Control[] { verifyCheckBoxOryo };
            bankControls = new Control[] {  };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (bankControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBank;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            #region 入力チェック
            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //県
            int pref = verifyBoxPref.GetIntValue();
            setStatus(verifyBoxPref, pref < 1 || 47 < pref);

            //受療者区分
            int juryoType = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new[] { 2, 4, 6, 8, 0 }.Contains(juryoType));

            //本家区分 入力2=本人を1に、入力6=家族を2に変換して記録
            int family = verifyBoxFamily.GetIntValue();
            family = family == 2 ? 1 : family == 6 ? 2 : 0;
            setStatus(verifyBoxFamily, family != 1 && family != 2);

            //被保険者番号 1文字以上かつ数字に直せること
            int numM = verifyBoxNumM.GetIntValue();
            setStatus(verifyBoxNumM, numM < 1);
            int numN = verifyBoxNumN.GetIntValue();
            setStatus(verifyBoxNumN, numN < 1);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birth = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //20200316142735 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い初検日追加
            
            var firstDate1 = dateCheck(verifyBoxF1Nengo, verifyBoxF1Y, verifyBoxF1M, verifyBoxF1D);
            //20200316142735 furukawa ed ////////////////////////


            //開始日
            //20191105165533 furukawa st ////////////////////////
            //開始・終了日は診療年月から取得した方が正確

            var startDt = dateCheck(verifyBoxY, verifyBoxM, verifyBoxStartDate.GetIntValue());
            //var startDt = dateCheck(4, year, month, verifyBoxStartDate);
            //20191105165533 furukawa ed ////////////////////////

            //20200316142508 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い終了日追加

            var finishdt = dateCheck(verifyBoxY, verifyBoxM, verifyBoxFinishDate.GetIntValue());
            //20200316142508 furukawa ed ////////////////////////

            //20200316145450 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い給付割合追加
            
            //給付割合 7～10
            int rate = 0;
            int.TryParse(verifyBoxKyufuRate.Text, out rate);
            setStatus(verifyBoxKyufuRate, rate <= 6 || rate >= 11);
            //20200316145450 furukawa ed ////////////////////////


            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額
            bool payError =
                ((total * 70 / 100) != charge && (total * 80 / 100) != charge && (total * 90 / 100) != charge);

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //負傷名
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);


            //20210511115526 furukawa st ////////////////////////
            //支払先コードが無い場合、9を5桁に変更する（大和総研仕様

            //  20210419120108 furukawa st ////////////////////////
            //  支払先コード（団体コード）の仕様変更？→団体コードが設定されていない場合は9×6を設定するルール2021/04/19伸作さん

            //柔整師登録記号番号
            string strDatntaiCode = "99999";

            //  string strDatntaiCode = "999999";
            //              string strDatntaiCode = verifyBoxDantaiCode.Text;
            //  20210419120108 furukawa ed ////////////////////////

            //20210511115526 furukawa ed ////////////////////////

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }


            #endregion

            #region Appへの反映
            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoPref = pref;
            app.HihoNum = $"{verifyBoxNumM.Text.Trim()}-{verifyBoxNumN.Text.Trim()}";
            app.Family = family * 100 + juryoType;   //受診者区分と本人家族をまとめて記録
            app.Sex = sex;
            app.Birthday = birth;
            app.FushoStartDate1 = startDt;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.Partial = total - charge;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            
            app.Distance = verifyCheckBoxOryo.Checked ? 999 : 0;
            app.AppType = scan.AppType;


            //20190509102845 furukawa st ////////////////////////
            //公費負担番号有無追加
            app.PublcExpense = verifyCheckKohiUmu.Checked ? "1" : string.Empty;

            //20190509102845 furukawa ed ////////////////////////

            //20200316143127 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い初検日追加            
            app.FushoFirstDate1 = firstDate1;
            //20200316143127 furukawa ed ////////////////////////

            //20200316143114 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い終了日追加
            app.FushoFinishDate1 = finishdt;
            //20200316143114 furukawa ed ////////////////////////

            //20200316145425 furukawa st ////////////////////////
            //ベンダー大和総研に変更に伴い給付割合追加            
            app.Ratio = rate;
            //20200316145425 furukawa ed ////////////////////////


            //20200408152102 furukawa st ////////////////////////
            //大和総研へ移行に伴い団体コード（多分支払コード）追加
            app.PayCode = strDatntaiCode;
            //20200408152102 furukawa ed ////////////////////////

            #endregion

            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            verifyBoxPref.Text = verifyBoxPref.Text.Trim().PadLeft(2, '0');

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if(!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                //OCRデータがあれば、部位のみ挿入
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxY, labelYearInfo, labelH, labelY,
                labelInputerName};

            var visible = new Action<bool>(b =>
            {
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is IVerifiable || item is Label)) continue;
                    if (cs.Contains(item)) continue;
                    item.Visible = b;
                }
            });

            if ((verifyBoxY.Text == "--" || verifyBoxY.Text == "++") && verifyBoxY.Text.Length == 2)
            {
                //続紙その他
                visible(false);
            }
            else
            {
                //申請書の場合
                visible(true);
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (bankControls.Contains(t)) posBank = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //申請書

                //家族区分 本人データ1=入力時2 家族データ2=入力時6
                int family = app.Family / 100;
                family = family == 1 ? 2 : 6;

                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyBoxPref, app.HihoPref.ToString("00"), firstTime, nv);
                setValue(verifyBoxNumM, app.HihoNum.Split('-')[0], firstTime, nv);
                setValue(verifyBoxNumN, app.HihoNum.Split('-')[1], firstTime, nv);
                setValue(verifyBoxJuryoType, app.Family % 100, firstTime, nv);
                setValue(verifyBoxFamily, family, firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);
                setValue(verifyBoxStartDate, app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxTotal, app.Total, firstTime, nv);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                setValue(verifyCheckBoxOryo, app.Distance == 999, firstTime, false);
                setValue(verifyBoxNewCont, app.NewContType == NEW_CONT.新規 ? "1" : "2", firstTime, false);

                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);

                //20190509103101 furukawa st ////////////////////////
                //公費負担番号有無追加
                setValue(verifyCheckKohiUmu, app.PublcExpense=="1", firstTime, false);
                //20190509103101 furukawa ed ////////////////////////


                //20200316144928 furukawa st ////////////////////////
                //ベンダー大和総研に変更に伴い初検日、終了日、給付割合追加
                if (app.FushoFinishDate1 != DateTime.MinValue)
                {
                    int fsNengo = DateTimeEx.GetEraNumber(app.FushoFirstDate1);
                    setValue(verifyBoxF1Nengo, fsNengo.ToString(), firstTime, nv);
                    int fsWarekiY = DateTimeEx.GetHsYearFromAd(app.FushoFirstDate1.Year, app.FushoFirstDate1.Month);
                    setValue(verifyBoxF1Y, fsWarekiY, firstTime, nv);
                    setValue(verifyBoxF1M, app.FushoFirstDate1.Month, firstTime, nv);
                    setValue(verifyBoxF1D, app.FushoFirstDate1.Day, firstTime, nv);

                    setValue(verifyBoxFinishDate, app.FushoFinishDate1.Day, firstTime, nv);
                }

                setValue(verifyBoxKyufuRate, app.Ratio, firstTime, nv);
                //20200316144928 furukawa ed ////////////////////////

                //20210419140959 furukawa st ////////////////////////
                //入力不要

                //      20200408153902 furukawa st ////////////////////////
                //      大和総研へ移行に伴い団体コード（多分支払コード）追加       
                //      setValue(verifyBoxDantaiCode, app.PayCode, firstTime, nv);
                //      20200408153902 furukawa ed ////////////////////////
                //20210419140959 furukawa ed ////////////////////////
            }
            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
