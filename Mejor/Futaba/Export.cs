﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Futaba
{
    class Export
    {
        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var sdic = new Dictionary<int, Scan>();

            foreach (var item in scans) sdic.Add(item, Scan.Select(item));

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    //20200408183144 furukawa st ////////////////////////
                    //大和総研へ移行に伴い団体コード（多分支払コード）追加                           
                    sw.WriteLine(
                        "AID,No.,通知番号,処理年,処理月,被保険者番号,被保険者名,受療者名,性別,生年月日,年齢," +
                        "〒,住所1,施術年,施術月,合計金額,請求金額,施術日数,施術所名,ナンバリング,点検,照会理由,過誤理由,再審査理由,負傷数,柔整師会コード");

                    //sw.WriteLine(
                    //    "AID,No.,通知番号,処理年,処理月,被保険者番号,被保険者名,受療者名,性別,生年月日,年齢," +
                    //    "〒,住所1,施術年,施術月,合計金額,請求金額,施術日数,施術所名,ナンバリング,点検,照会理由,過誤理由,再審査理由,負傷数");
                    //20200408183144 furukawa ed ////////////////////////


                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100 , int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    var notifyNumber = $"{jcy}{jcm}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<App, string> getFushoCount = app =>
                    {
                        var c = 0;
                        if (app.FushoName1 != "") c++;
                        if (app.FushoName2 != "") c++;
                        if (app.FushoName3 != "") c++;
                        if (app.FushoName4 != "") c++;
                        if (app.FushoName5 != "") c++;
                        return c.ToString();
                    };
                    foreach (var item in list)
                    {
                        ss.Add(item.Aid.ToString());
                        ss.Add(count.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(jcy);
                        ss.Add(jcm);
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(DateTimeEx.GetAge(item.Birthday, DateTime.Today).ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.Numbering.ToString());
                        ss.Add(item.InspectInfo.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");//区切り文字が「, (半角カンマ)(半角スペース)」なのでエスケープ処理
                        ss.Add("\"" + item.KagoReasonStr + "\"");
                        ss.Add("\"" + item.SaishinsaReasonStr + "\"");
                        ss.Add(getFushoCount(item));

                        //20200408183222 furukawa st ////////////////////////
                        //大和総研へ移行に伴い団体コード（多分支払コード）追加       
                        ss.Add(item.PayCode);
                        //20200408183222 furukawa ed ////////////////////////

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
