﻿using System;

namespace Mejor.Futaba
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxKyufuRate = new Mejor.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panelF1FirstDate = new System.Windows.Forms.Panel();
            this.verifyBoxF1D = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.label13 = new System.Windows.Forms.Label();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1Nengo = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyCheckKohiUmu = new Mejor.VerifyCheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.verifyBoxJuryoType = new Mejor.VerifyBox();
            this.labelFamily = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyCheckBoxOryo = new Mejor.VerifyCheckBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxNumN = new Mejor.VerifyBox();
            this.verifyBoxNumM = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxFinishDate = new Mejor.VerifyBox();
            this.verifyBoxStartDate = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxPref = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelF1FirstDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(665, 753);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 250;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(119, 35);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(172, 35);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(251, 7);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(79, 13);
            this.labelHnum.TabIndex = 8;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(57, 35);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(31, 13);
            this.labelH.TabIndex = 1;
            this.labelH.Text = "和暦";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(120, 759);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 781);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(107, 781);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 754);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 754);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 754);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(24, 754);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 781);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxKyufuRate);
            this.panelRight.Controls.Add(this.label27);
            this.panelRight.Controls.Add(this.label28);
            this.panelRight.Controls.Add(this.panelF1FirstDate);
            this.panelRight.Controls.Add(this.verifyCheckKohiUmu);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.label7);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.verifyBoxJuryoType);
            this.panelRight.Controls.Add(this.labelFamily);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.label15);
            this.panelRight.Controls.Add(this.verifyCheckBoxOryo);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.verifyBoxNumN);
            this.panelRight.Controls.Add(this.verifyBoxNumM);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.verifyBoxBD);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxBM);
            this.panelRight.Controls.Add(this.verifyBoxCharge);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.verifyBoxBY);
            this.panelRight.Controls.Add(this.verifyBoxFinishDate);
            this.panelRight.Controls.Add(this.verifyBoxStartDate);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.verifyBoxBE);
            this.panelRight.Controls.Add(this.verifyBoxNewCont);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxPref);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.label22);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.label21);
            this.panelRight.Controls.Add(this.label20);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.label2);
            this.panelRight.Controls.Add(this.label14);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.label1);
            this.panelRight.Controls.Add(this.label9);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.label24);
            this.panelRight.Controls.Add(this.labelH);
            this.panelRight.Controls.Add(this.label10);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.labelDays);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelCharge);
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.label18);
            this.panelRight.Controls.Add(this.label17);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 781);
            this.panelRight.TabIndex = 0;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(86, 23);
            this.verifyBoxY.MaxLength = 2;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // verifyBoxKyufuRate
            // 
            this.verifyBoxKyufuRate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyufuRate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyufuRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyufuRate.Location = new System.Drawing.Point(559, 25);
            this.verifyBoxKyufuRate.MaxLength = 1;
            this.verifyBoxKyufuRate.Name = "verifyBoxKyufuRate";
            this.verifyBoxKyufuRate.NewLine = false;
            this.verifyBoxKyufuRate.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyufuRate.TabIndex = 16;
            this.verifyBoxKyufuRate.TextV = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(557, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 13);
            this.label27.TabIndex = 61;
            this.label27.Text = "給付割合";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label28.Location = new System.Drawing.Point(588, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 12);
            this.label28.TabIndex = 60;
            this.label28.Text = "7～10";
            // 
            // panelF1FirstDate
            // 
            this.panelF1FirstDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelF1FirstDate.Controls.Add(this.verifyBoxF1D);
            this.panelF1FirstDate.Controls.Add(this.label19);
            this.panelF1FirstDate.Controls.Add(this.label23);
            this.panelF1FirstDate.Controls.Add(this.label25);
            this.panelF1FirstDate.Controls.Add(this.verifyBoxF1M);
            this.panelF1FirstDate.Controls.Add(this.label13);
            this.panelF1FirstDate.Controls.Add(this.verifyBoxF1Y);
            this.panelF1FirstDate.Controls.Add(this.label12);
            this.panelF1FirstDate.Controls.Add(this.verifyBoxF1Nengo);
            this.panelF1FirstDate.Controls.Add(this.label26);
            this.panelF1FirstDate.Location = new System.Drawing.Point(6, 557);
            this.panelF1FirstDate.Name = "panelF1FirstDate";
            this.panelF1FirstDate.Size = new System.Drawing.Size(220, 70);
            this.panelF1FirstDate.TabIndex = 30;
            // 
            // verifyBoxF1D
            // 
            this.verifyBoxF1D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1D.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1D.Location = new System.Drawing.Point(174, 20);
            this.verifyBoxF1D.MaxLength = 2;
            this.verifyBoxF1D.Name = "verifyBoxF1D";
            this.verifyBoxF1D.NewLine = false;
            this.verifyBoxF1D.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1D.TabIndex = 4;
            this.verifyBoxF1D.TextV = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(116, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 64;
            this.label19.Text = "年";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(178, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 67;
            this.label23.Text = "日";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(80, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 63;
            this.label25.Text = "年号";
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(142, 20);
            this.verifyBoxF1M.MaxLength = 2;
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1M.TabIndex = 3;
            this.verifyBoxF1M.TextV = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(146, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 65;
            this.label13.Text = "月";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(111, 20);
            this.verifyBoxF1Y.MaxLength = 2;
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Y.TabIndex = 2;
            this.verifyBoxF1Y.TextV = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(43, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 26);
            this.label12.TabIndex = 62;
            this.label12.Text = "平:4\r\n令:5";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // verifyBoxF1Nengo
            // 
            this.verifyBoxF1Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Nengo.Location = new System.Drawing.Point(79, 20);
            this.verifyBoxF1Nengo.MaxLength = 1;
            this.verifyBoxF1Nengo.Name = "verifyBoxF1Nengo";
            this.verifyBoxF1Nengo.NewLine = false;
            this.verifyBoxF1Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Nengo.TabIndex = 1;
            this.verifyBoxF1Nengo.TextV = "";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 26);
            this.label26.TabIndex = 30;
            this.label26.Text = "初検\r\n年月日";
            // 
            // verifyCheckKohiUmu
            // 
            this.verifyCheckKohiUmu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckKohiUmu.CheckedV = false;
            this.verifyCheckKohiUmu.Location = new System.Drawing.Point(894, 22);
            this.verifyCheckKohiUmu.Name = "verifyCheckKohiUmu";
            this.verifyCheckKohiUmu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckKohiUmu.Size = new System.Drawing.Size(83, 27);
            this.verifyCheckKohiUmu.TabIndex = 28;
            this.verifyCheckKohiUmu.Text = "公費あり";
            this.verifyCheckKohiUmu.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(895, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "公費有無";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(486, 24);
            this.verifyBoxFamily.MaxLength = 1;
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 15;
            this.verifyBoxFamily.TextV = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(486, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "本人/家族";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label8.Location = new System.Drawing.Point(513, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 26);
            this.label8.TabIndex = 16;
            this.label8.Text = "本人:2\r\n家族:6";
            // 
            // verifyBoxJuryoType
            // 
            this.verifyBoxJuryoType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxJuryoType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxJuryoType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxJuryoType.Location = new System.Drawing.Point(380, 23);
            this.verifyBoxJuryoType.MaxLength = 1;
            this.verifyBoxJuryoType.Name = "verifyBoxJuryoType";
            this.verifyBoxJuryoType.NewLine = false;
            this.verifyBoxJuryoType.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxJuryoType.TabIndex = 12;
            this.verifyBoxJuryoType.TextV = "";
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(378, 7);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(67, 13);
            this.labelFamily.TabIndex = 11;
            this.labelFamily.Text = "受療者区分";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(407, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 26);
            this.label6.TabIndex = 13;
            this.label6.Text = "本人:2 六歳:4\r\n家族:6 高齢:8";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(462, 577);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 26);
            this.label15.TabIndex = 36;
            this.label15.Text = "新規：1\r\n継続：2";
            // 
            // verifyCheckBoxOryo
            // 
            this.verifyCheckBoxOryo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxOryo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxOryo.CheckedV = false;
            this.verifyCheckBoxOryo.Location = new System.Drawing.Point(514, 576);
            this.verifyCheckBoxOryo.Name = "verifyCheckBoxOryo";
            this.verifyCheckBoxOryo.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxOryo.Size = new System.Drawing.Size(83, 27);
            this.verifyCheckBoxOryo.TabIndex = 60;
            this.verifyCheckBoxOryo.Text = "往療あり";
            this.verifyCheckBoxOryo.UseVisualStyleBackColor = false;
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(259, 694);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(245, 23);
            this.verifyBoxF5.TabIndex = 94;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(8, 694);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(245, 23);
            this.verifyBoxF4.TabIndex = 92;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxNumN
            // 
            this.verifyBoxNumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumN.Location = new System.Drawing.Point(297, 23);
            this.verifyBoxNumN.Name = "verifyBoxNumN";
            this.verifyBoxNumN.NewLine = false;
            this.verifyBoxNumN.Size = new System.Drawing.Size(69, 23);
            this.verifyBoxNumN.TabIndex = 10;
            this.verifyBoxNumN.TextV = "";
            // 
            // verifyBoxNumM
            // 
            this.verifyBoxNumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumM.Location = new System.Drawing.Point(253, 23);
            this.verifyBoxNumM.Name = "verifyBoxNumM";
            this.verifyBoxNumM.NewLine = false;
            this.verifyBoxNumM.Size = new System.Drawing.Size(44, 23);
            this.verifyBoxNumM.TabIndex = 9;
            this.verifyBoxNumM.TextV = "";
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(510, 647);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(245, 23);
            this.verifyBoxF3.TabIndex = 90;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(829, 24);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 27;
            this.verifyBoxBD.TextV = "";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 87);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 136);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 462);
            this.scrollPictureControl1.TabIndex = 29;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(786, 24);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 25;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(725, 577);
            this.verifyBoxCharge.MaxLength = 10;
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxCharge.TabIndex = 75;
            this.verifyBoxCharge.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(569, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(624, 577);
            this.verifyBoxTotal.MaxLength = 10;
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(87, 23);
            this.verifyBoxTotal.TabIndex = 70;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(743, 24);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 23;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxFinishDate
            // 
            this.verifyBoxFinishDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFinishDate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFinishDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFinishDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFinishDate.Location = new System.Drawing.Point(307, 576);
            this.verifyBoxFinishDate.MaxLength = 2;
            this.verifyBoxFinishDate.Name = "verifyBoxFinishDate";
            this.verifyBoxFinishDate.NewLine = false;
            this.verifyBoxFinishDate.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxFinishDate.TabIndex = 42;
            this.verifyBoxFinishDate.TextV = "";
            // 
            // verifyBoxStartDate
            // 
            this.verifyBoxStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxStartDate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStartDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStartDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStartDate.Location = new System.Drawing.Point(253, 576);
            this.verifyBoxStartDate.MaxLength = 2;
            this.verifyBoxStartDate.Name = "verifyBoxStartDate";
            this.verifyBoxStartDate.NewLine = false;
            this.verifyBoxStartDate.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxStartDate.TabIndex = 41;
            this.verifyBoxStartDate.TextV = "";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(364, 577);
            this.verifyBoxDays.MaxLength = 2;
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 43;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(259, 647);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(245, 23);
            this.verifyBoxF2.TabIndex = 88;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(692, 24);
            this.verifyBoxBE.MaxLength = 1;
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 21;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(434, 577);
            this.verifyBoxNewCont.MaxLength = 1;
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxNewCont.TabIndex = 50;
            this.verifyBoxNewCont.TextV = "";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(138, 23);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPref.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPref.Location = new System.Drawing.Point(205, 23);
            this.verifyBoxPref.MaxLength = 2;
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.NewLine = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxPref.TabIndex = 7;
            this.verifyBoxPref.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(631, 24);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 18;
            this.verifyBoxSex.TextV = "";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(8, 647);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(245, 23);
            this.verifyBoxF1.TabIndex = 86;
            this.verifyBoxF1.TextV = "";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(257, 679);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 53;
            this.label22.Text = "負傷名5";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 679);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 13);
            this.label21.TabIndex = 51;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(508, 631);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 49;
            this.label20.Text = "負傷名3";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 57;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(257, 631);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 47;
            this.label11.Text = "負傷名2";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(690, 7);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 20;
            this.labelBirthday.Text = "生年月日";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(432, 561);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "新規継続";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(515, 561);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "往療有無";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(8, 20);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(49, 26);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(203, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "県コード";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 631);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 45;
            this.label9.Text = "負傷名1";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(629, 7);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 17;
            this.labelSex.Text = "性別";
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(307, 560);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 13);
            this.label24.TabIndex = 30;
            this.label24.Text = "終了日";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(253, 560);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "開始日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(718, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 26);
            this.label5.TabIndex = 22;
            this.label5.Text = "昭:3\r\n平:4";
            // 
            // labelDays
            // 
            this.labelDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(364, 561);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(55, 13);
            this.labelDays.TabIndex = 32;
            this.labelDays.Text = "診療日数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(658, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 26);
            this.label4.TabIndex = 19;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(621, 561);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(55, 13);
            this.labelTotal.TabIndex = 39;
            this.labelTotal.Text = "合計金額";
            // 
            // labelCharge
            // 
            this.labelCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(723, 561);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(55, 13);
            this.labelCharge.TabIndex = 41;
            this.labelCharge.Text = "請求金額";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(855, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "日";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(769, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "年";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(812, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "月";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 781);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 781);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelF1FirstDate.ResumeLayout(false);
            this.panelF1FirstDate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxNumM;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxNumN;
        private VerifyCheckBox verifyCheckBoxOryo;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxNewCont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxJuryoType;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxPref;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxStartDate;
        private System.Windows.Forms.Label label10;
        private VerifyCheckBox verifyCheckKohiUmu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelF1FirstDate;
        private VerifyBox verifyBoxF1D;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxF1M;
        private System.Windows.Forms.Label label13;
        private VerifyBox verifyBoxF1Y;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxF1Nengo;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxFinishDate;
        private System.Windows.Forms.Label label24;
        private VerifyBox verifyBoxKyufuRate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
    }
}