﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Sakaishi

{
    class dataImport
    {
       

        public static int ChangeToADYM(string strtmp)
        {            
            return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
        }
    }



    /// <summary>
    /// 柔整データインポート
    /// </summary>
    public partial class dataImport_jyusei
    {

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int f000importid { get;set; }=0;                                 //importid;
        
        public string f001shinsaYM { get; set; } = string.Empty;               //審査年月;
        public string f002hihomark { get; set; } = string.Empty;               //証記号;
        public string f003hihonum { get; set; } = string.Empty;                //証番号;
        public string f004pname { get; set; } = string.Empty;                  //氏名;
        public string f005pbirthday { get; set; } = string.Empty;              //生年月日;
        public string f006pgender { get; set; } = string.Empty;                //性別;
        public string f007shinryoYM { get; set; } = string.Empty;              //診療年月;
        public string f008counteddays { get; set; } = string.Empty;            //実日数;
        public string f009total { get; set; } = string.Empty;                  //決定点数;
        public string f010clinicnum { get; set; } = string.Empty;              //機関コード;
        public string f011clinicname { get; set; } = string.Empty;             //医療機関名;
        public string f012 { get; set; } = string.Empty;                       //療養費種別;
        public string f013 { get; set; } = string.Empty;                       //疑義種別;
        public string f014comnum { get; set; } = string.Empty;                 //レセプト全国共通キー;
        public int f100shinsaYMAD { get; set; } = 0;                           //審査年月西暦;
        public int f101sejutsuYMAD { get; set; } = 0;                          //診療年月西暦;
        public DateTime f102birthAD { get; set; } = DateTime.MinValue;         //生年月日西暦;
        public DateTime f103firstdateAD { get; set; } = DateTime.MinValue;     //初検日西暦だが未使用;
        public string f104hihomark_narrow { get; set; } = string.Empty;        //被保険者証記号半角;
        public string f105hihonum_narrow { get; set; } = string.Empty;         //証番号半角;
        public int cym { get; set; } = 0;                                      //メホール請求年月;
        
        #endregion


        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        static int intcym = 0;

        List<dataImport_jyusei> lstImp = new List<dataImport_jyusei>();

        /// <summary>
        /// データインポート
        /// </summary>
        /// <returns></returns>
        public static bool dataImport_main(int _cym,string strFileName)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;


            try
            {
                wf.LogPrint("データ取得");
                
                
                //dataimport_baseに登録
                if(!EntryCSVData(strFileName, wf)) return false;

                
                wf.LogPrint("Appテーブル作成");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
            //    if(!UpdateApp(intcym)) return false;

          
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }

        /// <summary>
        /// 入力画面用データ取得
        /// </summary>
        /// <param name="_cym">メホール年月</param>
        /// <returns></returns>
        public static List<dataImport_jyusei> GetDataImport_Jyusei(int _cym)
        {
            List<dataImport_jyusei> lst = new List<dataImport_jyusei>();
            DB.Command cmd = DB.Main.CreateCmd($"select * from dataimport_jyusei where cym={_cym}");
            try
            {
                var l = cmd.TryExecuteReaderList();
                foreach (var tmp in l)
                {
                    dataImport_jyusei cls = new dataImport_jyusei();

                    cls.f000importid = int.Parse(tmp[0].ToString());
                    cls.f001shinsaYM 				= tmp[1].ToString();          //審査年月;
                    cls.f002hihomark 				= tmp[2].ToString();          //証記号;
                    cls.f003hihonum 				= tmp[3].ToString();           //証番号;
                    cls.f004pname 					= tmp[4].ToString();             //氏名;
                    cls.f005pbirthday 				= tmp[5].ToString();         //生年月日;
                    cls.f006pgender 				= tmp[6].ToString();           //性別;
                    cls.f007shinryoYM 				= tmp[7].ToString();         //診療年月;
                    cls.f008counteddays 			= tmp[8].ToString();       //実日数;
                    cls.f009total 					= tmp[9].ToString();             //決定点数;
                    cls.f010clinicnum 				= tmp[10].ToString();         //機関コード;
                    cls.f011clinicname 				= tmp[11].ToString();       //医療機関名;
                    cls.f012 						= tmp[12].ToString();                 //療養費種別;
                    cls.f013 						= tmp[13].ToString();                 //疑義種別;
                    cls.f014comnum 					= tmp[14].ToString();           //レセプト全国共通キー;

                    cls.f100shinsaYMAD              = int.Parse(tmp[15].ToString());
                    cls.f101sejutsuYMAD 			= int.Parse(tmp[16].ToString());
                    cls.f102birthAD 				= DateTime.Parse(tmp[17].ToString());
                    cls.f103firstdateAD 			= DateTime.MinValue; 
                    cls.f104hihomark_narrow 		= tmp[19].ToString();
                    cls.f105hihonum_narrow 			= tmp[20].ToString();
                    cls.cym = int.Parse(tmp[21].ToString());
                    
                    lst.Add(cls);


                }
                return lst;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
            
        }


        /// <summary>
        /// インポートされているレコード数をメホール年月で出す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport_jyusei group by cym order by cym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport_jyusei where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;
            return int.Parse(lst[0].GetValue(0).ToString());

        }


        /// <summary>
        /// 提供データでAppを更新→画面で登録するので未使用
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +
                "inum				          = b.f001insnum" +                                         //保険者番号
                ",comnum			          = b.f003comnum" +                                         //レセプト全国共通キー
                ",afamily			          = case b.f008family when '2' then 2 else 6 end " +        //本人家族入外区分                 
                ",hnum				          = b.f009hihomark || '-' || b.f010hihonum" +               //被保険者証記号番号
                                                                                                        //",hnum				          = b.hihomark || b.hihonum" +                          //被保険者証記号番号
                ",psex				          = cast(b.f011pgender as int) " +                          //性別
                ",aratio			          = cast(b.f013ratio as int) " +                            //給付割合
                ",sid				          = b.f022clinicnum" +                                      //施術所コード
                ",acounteddays		          = cast((case when b.f024        ='' then '0' else b.f024        end ) as int) " +         //回数（実日数）
                ",atotal			          = cast((case when b.f025total   ='' then '0' else b.f025total   end ) as int) " +         //決定金額
                ",acharge			          = cast((case when b.f028charge  ='' then '0' else b.f028charge  end ) as int)" +          //保険者負担額
                ",apartial			          = cast((case when b.f030 ='' then '0' else b.f030 end ) as int)" +          //患者負担額
                ",taggeddatas	              " +
                "       = 'count:\"' || case when b.f033 ='' then '0' else b.f033 end  || '\"'  " +//負傷数
                                                                                                   //"       = taggeddatas || 'count:\"' || b.fushocount || '\"|' || 'GeneralString1:\"' || b.hihokana || '\"' " +//負傷数、被保険者名カナ
                //",hzip				          = b.f081zip" +                                     //被保険者郵便番号
                //",haddress			          = b.f082address" +                                 //被保険者住所
                //",hname				          = b.f083namekanji" +                               //被保険者名
                ",ym				            = cast(b.f101sejutsuYMAD as int) " +             //診療年月
                ",pbirthday				        = cast(b.f102birthad as date) " +                //受療者生年月日
                ",ifirstdate1				    = cast(b.f103firstdatead as date)  " +           //初検日1
                ",fchargetype=  " +
                "	case when cast(b.f101sejutsuYMAD as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end " +//新規継続
                ",aapptype=6 " + //申請書種類は6柔整だろう
                ",numbering                 = f079 "+ //宛名番号一応入れとく
                "from dataimport_jyusei b " +
                "where " +
                $"replace(application.emptytext3,'.tif','')=b.f004rezenum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main,strsql,true);
           

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


       

        /// <summary>
        /// csvをインポート
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData(string strFileName, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from dataimport_jyusei where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                wf.LogPrint("CSVロード");
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                //20220204101845 furukawa st ////////////////////////
                //列数が違っても入ってしまうのでチェック
                
                wf.LogPrint("CSV列数チェック");
                wf.LogPrint($"CSV列数{lst[0].Length}");

                //列数チェック
                int colCnt = 14;
                if (lst[0].Length != colCnt)
                {
                    System.Windows.Forms.MessageBox.Show($"列数が{colCnt}ではありません。ファイルの中身を確認してください","",
                        System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }
                //20220204101845 furukawa ed ////////////////////////


                wf.SetMax(lst.Count);

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {

                    if (CommonTool.WaitFormCancelProcess(wf,tran)) return false;

                    try
                    {
                        int.Parse(tmp[8].ToString().Replace(",", ""));
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataImport_jyusei cls = new dataImport_jyusei();

                    
                    cls.f001shinsaYM = tmp[0];          //審査年月;
                    cls.f002hihomark = tmp[1];          //証記号;
                    cls.f003hihonum = tmp[2];           //証番号;
                    cls.f004pname = tmp[3];             //氏名;
                    cls.f005pbirthday = tmp[4];         //生年月日;
                    cls.f006pgender = tmp[5];           //性別;
                    cls.f007shinryoYM = tmp[6];         //診療年月;
                    cls.f008counteddays = tmp[7];       //実日数;
                    cls.f009total = tmp[8];             //決定点数;
                    cls.f010clinicnum = tmp[9];         //機関コード;
                    cls.f011clinicname = tmp[10];       //医療機関名;
                    cls.f012 = tmp[11];                 //療養費種別;
                    cls.f013 = tmp[12];                 //疑義種別;
                    cls.f014comnum = tmp[13];           //レセプト全国共通キー;



                    //管理項目
                    string strErr = string.Empty;
                    cls.cym = intcym;//メホール請求年月管理用;

                    //審査年月西暦;
                    //if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f001shinsaYM)) == 0) strErr += "審査年月 変換に失敗しました";
                    cls.f100shinsaYMAD = dataImport.ChangeToADYM(cls.f001shinsaYM);//審査年月西暦

                    cls.f101sejutsuYMAD = dataImport.ChangeToADYM(cls.f007shinryoYM);         //施術年月西暦;                            


                    int y = DateTimeEx.GetAdYear(cls.f005pbirthday.Substring(0, 4));
                    int m = int.Parse(cls.f005pbirthday.Substring(5, 2));
                    int d = int.Parse(cls.f005pbirthday.Substring(8, 2));
                    cls.f102birthAD= new DateTime(y, m, d);        //生年月日西暦メホール用;
                    
                    cls.f103firstdateAD = DateTime.MinValue;

                    //被保険者証記号半角;                                                                                                
                    //記号は漢字の可能性が                                                                          
                    try
                    {
                        cls.f104hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f002hihomark, Microsoft.VisualBasic.VbStrConv.Narrow);
                    }
                    catch
                    {
                        cls.f104hihomark_narrow = cls.f002hihomark;
                    }

                    cls.f105hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f003hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;

                    if (strErr != string.Empty)
                    {
                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{strErr}");
                        tran.Rollback();
                        return false;
                    }
                    //20210628161118 furukawa st ////////////////////////
                    //トランザクションぬけてた
                    if (!DB.Main.Insert<dataImport_jyusei>(cls,tran)) return false;
                    //if (!DB.Main.Insert<dataImport_jyusei>(cls)) return false;
                    //20210628161118 furukawa ed ////////////////////////

                    wf.LogPrint($"柔整提供データ  レセプト全国共通キー:{cls.f014comnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");

                //20210628161307 furukawa st ////////////////////////
                //コミット場所try内に修正
                tran.Commit();
                //20210628161307 furukawa ed ////////////////////////

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                //20210628161212 furukawa st ////////////////////////
                //コミット場所try内に修正
                //tran.Commit();
                //20210628161212 furukawa ed ////////////////////////

            }

        }


        ///// <summary>
        ///// 提供データExcelを開いてテーブル登録
        ///// </summary>
        ///// <param name="strFileName">ファイル名</param>
        ///// <param name="wf"></param>
        ///// <returns></returns>
        //private static bool EntryExcelData(string strFileName ,WaitForm wf)
        //{
        //    wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
        //    NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

        //    DB.Transaction tran = new DB.Transaction(DB.Main);

        //    DB.Command cmd=new DB.Command($"delete from dataimport_base where cym={intcym}",tran);
        //    cmd.TryExecuteNonQuery();

        //    wf.LogPrint($"{intcym}削除");

        //    int c = 0;//インポートファイルの列
        //    int r = 1;//インポートファイルの行
        //    try
        //    {

        //        wf.InvokeValue = 0;
        //        wf.SetMax(ws.LastRowNum);
                
        //        //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
        //        int cellcnt = ws.GetRow(0).Cells.Count;
                
        //        for (r = 1; r <= ws.LastRowNum; r++)
        //        {
        //            dataImport_jyusei imp = new dataImport_jyusei();

        //            NPOI.SS.UserModel.IRow row = ws.GetRow(r);

        //            imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

        //            for ( c = 0; c < cellcnt; c++)
        //            {
        //                if(CommonTool.WaitFormCancelProcess(wf)) return false;

        //                NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        
        //                if (c == 0)   imp.f001insnum = getCellValueToString(cell);                //保険者番号;
        //                if (c == 1)   imp.f002shinsaym = getCellValueToString(cell);              //審査年月;
        //                if (c == 2)   imp.f003comnum = getCellValueToString(cell);                //レセプト全国共通キー;
        //                if (c == 3)   imp.f004rezenum = getCellValueToString(cell);               //国保連レセプト番号;
        //                if (c == 4)   imp.f005sejutsuym = getCellValueToString(cell);             //施術年月;
        //                if (c == 5)   imp.f006 = getCellValueToString(cell);                      //保険種別1;
        //                if (c == 6)   imp.f007 = getCellValueToString(cell);                      //保険種別2;
        //                if (c == 7)   imp.f008family = getCellValueToString(cell);                //本人家族入外区分;
        //                if (c == 8)   imp.f009hihomark = getCellValueToString(cell);              //被保険者証記号;
        //                if (c == 9)    imp.f010hihonum = getCellValueToString(cell);              //証番号;
        //                if (c == 10)   imp.f011pgender = getCellValueToString(cell);              //性別;
        //                if (c == 11)   imp.f012pbirthday = getCellValueToString(cell);            //生年月日;
        //                if (c == 12)   imp.f013ratio = getCellValueToString(cell);                //給付割合;
        //                if (c == 13)   imp.f014 = getCellValueToString(cell);                     //特記事項1;
        //                if (c == 14)   imp.f015 = getCellValueToString(cell);                     //特記事項2;
        //                if (c == 15)   imp.f016 = getCellValueToString(cell);                     //特記事項3;
        //                if (c == 16)   imp.f017 = getCellValueToString(cell);                     //特記事項4;
        //                if (c == 17)   imp.f018 = getCellValueToString(cell);                     //特記事項5;
        //                if (c == 18)   imp.f019 = getCellValueToString(cell);                     //算定区分1;
        //                if (c == 19)   imp.f020 = getCellValueToString(cell);                     //算定区分2;
        //                if (c == 20)   imp.f021 = getCellValueToString(cell);                     //算定区分3;
        //                if (c == 21)   imp.f022clinicnum = getCellValueToString(cell);            //施術所コード;
        //                if (c == 22)   imp.f023 = getCellValueToString(cell);                     //柔整団体コード;
        //                if (c == 23)   imp.f024 = getCellValueToString(cell);                     //回数;
        //                if (c == 24)   imp.f025total = getCellValueToString(cell);                //決定金額;
        //                if (c == 25)   imp.f026partial = getCellValueToString(cell);              //決定一部負担金;
        //                if (c == 26)   imp.f027 = getCellValueToString(cell);                     //費用額;
        //                if (c == 27)   imp.f028charge = getCellValueToString(cell);               //保険者負担額;
        //                if (c == 28)   imp.f029 = getCellValueToString(cell);                     //高額療養費;
        //                if (c == 29)   imp.f030 = getCellValueToString(cell);                     //患者負担額;
        //                if (c == 30)   imp.f031 = getCellValueToString(cell);                     //国保優先公費;
        //                if (c == 31)   imp.f033firstdate1 = getCellValueToString(cell);           //初検年月日;
        //                if (c == 32)   imp.f034 = getCellValueToString(cell);                     //負傷名数;
        //                if (c == 33)   imp.f035 = getCellValueToString(cell);                     //転帰;
        //                if (c == 34)   imp.f036 = getCellValueToString(cell);                     //転帰レコード区分;
        //                if (c == 35)   imp.f037 = getCellValueToString(cell);                     //転帰グループ番号;
        //                if (c == 36)   imp.f038 = getCellValueToString(cell);                     //整形審査年月_1;
        //                if (c == 37)   imp.f039 = getCellValueToString(cell);                     //整形レセプト全国共通キー_1;
        //                if (c == 38)   imp.f040 = getCellValueToString(cell);                     //整形医療機関コード_1;
        //                if (c == 39)   imp.f041 = getCellValueToString(cell);                     //整形医療機関名_1;
        //                if (c == 40)   imp.f042 = getCellValueToString(cell);                     //整形診療開始日1_1;
        //                if (c == 41)   imp.f043 = getCellValueToString(cell);                     //整形診療開始日2_1;
        //                if (c == 42)   imp.f044 = getCellValueToString(cell);                     //整形診療開始日3_1;
        //                if (c == 43)   imp.f045 = getCellValueToString(cell);                     //整形疾病コード1_1;
        //                if (c == 44)   imp.f046 = getCellValueToString(cell);                     //整形疾病コード2_1;
        //                if (c == 45)   imp.f047 = getCellValueToString(cell);                     //整形疾病コード3_1;
        //                if (c == 46)   imp.f048 = getCellValueToString(cell);                     //整形疾病コード4_1;
        //                if (c == 47)   imp.f049 = getCellValueToString(cell);                     //整形疾病コード5_1;
        //                if (c == 48)   imp.f050 = getCellValueToString(cell);                     //整形診療日数_1;
        //                if (c == 49)   imp.f051 = getCellValueToString(cell);                     //整形費用額_1;
        //                if (c == 50)   imp.f052 = getCellValueToString(cell);                     //整形審査年月_2;
        //                if (c == 51)   imp.f053 = getCellValueToString(cell);                     //整形レセプト全国共通キー_2;
        //                if (c == 52)   imp.f054 = getCellValueToString(cell);                     //整形医療機関コード_2;
        //                if (c == 53)   imp.f055 = getCellValueToString(cell);                     //整形医療機関名_2;
        //                if (c == 54)   imp.f056 = getCellValueToString(cell);                     //整形診療開始日1_2;
        //                if (c == 55)   imp.f057 = getCellValueToString(cell);                     //整形診療開始日2_2;
        //                if (c == 56)   imp.f058 = getCellValueToString(cell);                     //整形診療開始日3_2;
        //                if (c == 57)   imp.f059 = getCellValueToString(cell);                     //整形疾病コード1_2;
        //                if (c == 58)   imp.f060 = getCellValueToString(cell);                     //整形疾病コード2_2;
        //                if (c == 59)   imp.f061 = getCellValueToString(cell);                     //整形疾病コード3_2;
        //                if (c == 60)   imp.f062 = getCellValueToString(cell);                     //整形疾病コード4_2;
        //                if (c == 61)   imp.f063 = getCellValueToString(cell);                     //整形疾病コード5_2;
        //                if (c == 62)   imp.f064 = getCellValueToString(cell);                     //整形診診療日数_2;
        //                if (c == 63)   imp.f065 = getCellValueToString(cell);                     //整形費用額_2;
        //                if (c == 64)   imp.f066 = getCellValueToString(cell);                     //整形審査年月_3;
        //                if (c == 65)   imp.f067 = getCellValueToString(cell);                     //整形レセプト全国共通キー_3;
        //                if (c == 66)   imp.f068 = getCellValueToString(cell);                     //整形医療機関コード_3;
        //                if (c == 67)   imp.f069 = getCellValueToString(cell);                     //整形医療機関名_3;
        //                if (c == 68)   imp.f070 = getCellValueToString(cell);                     //整形診療開始日1_3;
        //                if (c == 69)   imp.f071 = getCellValueToString(cell);                     //整形診療開始日2_3;
        //                if (c == 70)   imp.f072 = getCellValueToString(cell);                     //整形診療開始日3_3;
        //                if (c == 71)   imp.f073 = getCellValueToString(cell);                     //整形疾病コード1_3;
        //                if (c == 72)   imp.f074 = getCellValueToString(cell);                     //整形疾病コード2_3;
        //                if (c == 73)   imp.f075 = getCellValueToString(cell);                     //整形疾病コード3_3;
        //                if (c == 74)   imp.f076 = getCellValueToString(cell);                     //整形疾病コード4_3;
        //                if (c == 75)   imp.f077 = getCellValueToString(cell);                     //整形疾病コード5_3;
        //                if (c == 76)   imp.f078 = getCellValueToString(cell);                     //整形診療日数_3;
        //                if (c == 77)   imp.f079 = getCellValueToString(cell);                     //整形費用額_3;
        //                if (c == 78)   imp.f080 = getCellValueToString(cell);                     //宛名番号;
                        
                        
        //                //管理項目は最終列で処理
        //                if (c == 81)
        //                {
        //                    string strErr = string.Empty;
                            
        //                    imp.cym = intcym;                                           //メホール請求年月管理用;

        //                    //審査年月西暦;
        //                    if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym)) == 0) strErr += "審査年月 変換に失敗しました";
                            
        //                    imp.f100shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym));

        //                    imp.f101sejutsuYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f005sejutsuym));         //施術年月西暦;                            
        //                    imp.f102birthAD = DateTimeEx.GetDateFromJstr7(imp.f012pbirthday);                               //生年月日西暦;                            
        //                    imp.f103firstdateAD = DateTimeEx.GetDateFromJstr7(imp.f033firstdate1);//初検年月日西暦

        //                    if (!int.TryParse(imp.f038==string.Empty ? "0": imp.f038, out int tmpf038)) strErr += "\r\n 整形審査年月_1西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf038);                          //整形審査年月_1西暦;


        //                    if (!int.TryParse(imp.f052 == string.Empty ? "0" : imp.f052, out int tmpf052)) strErr += "\r\n 整形審査年月_2西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf052);                          //整形審査年月_2西暦;


        //                    if (!int.TryParse(imp.f066 == string.Empty ? "0" : imp.f066, out int tmpf066)) strErr += "\r\n 整形審査年月_3西暦 変換に失敗しました";
        //                    imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf066);                          //整形審査年月_3西暦;



        //                    //被保険者証記号半角;                                                                                                
        //                    //記号は漢字の可能性が                                                                          
        //                    try
        //                    {
        //                        imp.f107hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);
        //                    }
        //                    catch
        //                    {
        //                        imp.f107hihomark_narrow = imp.f010hihonum;
        //                    }

        //                    imp.f108hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;



        //                    if (strErr != string.Empty)
        //                    {
        //                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目:{strErr}");
        //                        tran.Rollback();
        //                        return false;
        //                    }
        //                }

        //            }

        //            wf.InvokeValue++;
        //            wf.LogPrint($"レセプト全国共通キー:{imp.f003comnum}");

        //            if (!DB.Main.Insert<dataImport_jyusei>(imp, tran)) return false;
        //        }

        //        return true;
        //    }         
        //    catch(Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目{ex.Message}");
        //        tran.Rollback();
        //        return false;
        //    }           
        //    finally
        //    {
        //        tran.Commit();
        //        wb.Close();
        //    }
            
        //}

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch(cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }
           
            
        }

    }

  


    /// <summary>
    /// あはきデータインポート
    /// </summary>
    public partial class dataImport_AHK
    {
        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int f000importid { get;set; }=0;                                //importid;
        public string f001shinsaYM { get; set; } = string.Empty;               //審査月;
        public string f002shinryoYM { get; set; } = string.Empty;              //診療月;
        public string f003regnum1 { get; set; } = string.Empty;                //登録記号番号;
        public string f004regnum2 { get; set; } = string.Empty;                //医療機関番号;
        public string f005kind { get; set; } = string.Empty;                   //種別;
        public string f006family { get; set; } = string.Empty;                 //本家区分;
        public string f007insnum { get; set; } = string.Empty;                 //保険者番号;
        public string f008hihonum { get; set; } = string.Empty;                //記号番号;
        public string f009pname { get; set; } = string.Empty;                  //受療者名;
        public string f010pgender { get; set; } = string.Empty;                //性別;
        public string f011pbirthday { get; set; } = string.Empty;              //生年月日;
        public string f012firstdate1 { get; set; } = string.Empty;             //初療年月日;
        public string f013startdate1 { get; set; } = string.Empty;             //施術開始日;
        public string f014finishdate1 { get; set; } = string.Empty;            //施術終了日;
        public string f015counteddays { get; set; } = string.Empty;            //実日数;
        public string f016total { get; set; } = string.Empty;                  //合計金額;
        public string f017partial { get; set; } = string.Empty;                //一部負担金;
        public string f018charge { get; set; } = string.Empty;                 //請求額;
        public string f019 { get; set; } = string.Empty;                       //施術日;
        public string f020 { get; set; } = string.Empty;                       //施術証明欄日付;
        public string f021 { get; set; } = string.Empty;                       //申請欄日付;
        public string f022accnum { get; set; } = string.Empty;                 //口座番号;
        public string f023douiday { get; set; } = string.Empty;                //同意年月日;
        public string f024ininday { get; set; } = string.Empty;                //受療委任欄日付;
        public string f025comnum { get; set; } = string.Empty;                 //全国レセプト共通キー;

        public int shinsaYMAD { get; set; } = 0;                               //審査年月西暦;
        public int shinryoYMAD { get; set; } = 0;                              //診療年月西暦;
        public DateTime birthdayAD { get; set; } = DateTime.MinValue;          //生年月日西暦;
        public DateTime firstdate1AD { get; set; } = DateTime.MinValue;        //初検日西暦;
        public DateTime startdate1AD { get; set; } = DateTime.MinValue;        //開始日1西暦;
        public DateTime finishdate1AD { get; set; } = DateTime.MinValue;       //終了日1西暦;
        public DateTime f020AD { get; set; } = DateTime.MinValue;              //施術証明欄日付西暦;
        public DateTime f021AD { get; set; } = DateTime.MinValue;              //申請欄日付西暦;
        public DateTime douidayAD { get; set; } = DateTime.MinValue;           //同意年月日西暦;
        public DateTime inindayAD { get; set; } = DateTime.MinValue;           //受療委任欄日付西暦;
        public string hihomark_narrow { get; set; } = string.Empty;            //被保険者証記号半角;
        public string hihonum_narrow { get; set; } = string.Empty;             //証番号半角;
        public int cym { get; set; } = 0;                                      //メホール請求年月;




        #endregion


        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 柔整給付データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_main(int _cym,  string strFileName)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                
                //dataimport_AHKに登録
                if(!EntryCSVData(strFileName, wf)) return false;


                
                wf.LogPrint("Appテーブル作成");
                //レセプト全国共通キーで紐づけてアップデートする
                if (!UpdateApp(intcym)) return false;

                //20211102172117 furukawa st ////////////////////////
                //auxにインポートID、レセプト全国共通キーを入れておく
                
                wf.LogPrint("AUXテーブル更新");
                //レセプト全国共通キーで紐づけてアップデートする
                if (!UpdateAUX(intcym)) return false;
                //20211102172117 furukawa ed ////////////////////////



                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        public static void UpdateOnly(int _cym)
        {
            //20211102172045 furukawa st ////////////////////////
            //auxにインポートID、レセプト全国共通キーを入れておく
            
            if (UpdateApp(_cym) && UpdateAUX(_cym))
            //  if (UpdateApp(_cym))
            //20211102172045 furukawa ed ////////////////////////

            {
                System.Windows.Forms.MessageBox.Show("成功しました");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("失敗しました","",
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
            }
        }

        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport_ahk where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;
            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// インポートされているレコード数をメホール年月で出す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport_ahk group by cym order by cym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        //20211102171938 furukawa st ////////////////////////
        //auxにインポートID、レセプト全国共通キーを入れておく
        
        /// <summary>
        /// AUX更新
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool UpdateAUX(int cym)
        {
            
            
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine("update application_aux set    ");
            sb.AppendLine(" matchingid01          = ahk.f000importid "); 
            sb.AppendLine(",matchingid01date      = now() ");

            sb.AppendLine(",matchingid02          = ahk.f025comnum ");
            sb.AppendLine(",matchingid02date      = now() ");

            sb.AppendLine(",aapptype          = a.aapptype ");
            
            sb.AppendLine(" from dataimport_ahk ahk,application a ");
            sb.AppendLine(" where ");
            sb.AppendLine($" a.cym={cym} ");
            sb.AppendLine(" and a.aid=application_aux.aid ");
            sb.AppendLine($"and replace(a.emptytext3,'.tif','')=ahk.f025comnum ");


            DB.Command cmd = new DB.Command(DB.Main,sb.ToString());


            try
            {
                cmd.TryExecuteNonQuery();
                
                return true;

            }
            catch (Exception ex)
            {
                
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();                
            }
        }
        //20211102171938 furukawa ed ////////////////////////


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            //20210726174820 furukawa st ////////////////////////
            //トランザクション追加
            
            DB.Transaction tran = new DB.Transaction(DB.Main);
            //20210726174820 furukawa ed ////////////////////////


            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine("update application set    ");
            sb.AppendLine(" ayear         = cast(substr(ahk.f002shinryoYM,2,2) as int) ");     //                 
            sb.AppendLine(",amonth        = cast(substr(ahk.f002shinryoYM,4,2) as int) ");     //                 

            sb.AppendLine(",inum         = ahk.f007insnum");                            //保険者番号                 
            sb.AppendLine(",hnum		 = ahk.hihonum_narrow ");                       //証番号  半角                 
            sb.AppendLine(",ym           = ahk.shinryoYMAD ");                          //診療年月西暦
            sb.AppendLine(",sid          = ahk.f003regnum1");                           //機関コード
            
            //sb.AppendLine(",sname        = ahk.clinicname" );                         //医療機関名
            
            sb.AppendLine(",pbirthday    = ahk.birthdayad");                            //受療者生年月日西暦
            sb.AppendLine(",psex         = cast(ahk.f010pgender as int) ");             //性別

            //最初の列04→あんま 登録記号番号の3桁目(9)　　 AppType あんま = 8,
            //最初の列05→鍼灸 登録記号番号の3桁目(8)       AppType 鍼灸 = 7
            sb.AppendLine(",aapptype     = case substr(f004regnum2,3,1) when '9' then 8 ");
            sb.AppendLine("                                             when '8' then 7 end   ");    //種別２ あんま・鍼灸区別するため3桁目を取得
            
            //sb.AppendLine(",aratio       = cast(ahk.ratio as int) " );                //給付割合                
            
            sb.AppendLine(",acounteddays = cast(ahk.f015counteddays as int) ");  //実日数
            sb.AppendLine(",atotal       = cast(ahk.f016total as int) ");        //決定点数
            sb.AppendLine(",comnum       = ahk.f025comnum");                     //レセプト全国共通キー
            
            //sb.AppendLine(",hzip         = ahk.hihozip" );                     //郵便番号
            //sb.AppendLine(",haddress     = ahk.hihoadd" );                     //住所
            //sb.AppendLine(",hname     = ahk.hihoname" );                       //氏名

            sb.AppendLine(",pname           = ahk.f009pname ");                  //受療者名
            sb.AppendLine(",afamily			= cast(ahk.f006family as int)");     //本家区分
            sb.AppendLine(",ifirstdate1		= ahk.firstdate1AD ");               //初検日
            sb.AppendLine(",istartdate1		= ahk.startdate1AD ");               //開始日
            sb.AppendLine(",ifinishdate1	= ahk.finishdate1AD ");              //終了日
            sb.AppendLine(",apartial 		= cast(ahk.f017partial as int)");    //一部負担金
            sb.AppendLine(",acharge 		= cast(ahk.f018charge  as int)");    //請求金額
            sb.AppendLine(",baccnumber 		= ahk.f022accnum ");                 //レセプト全国共通キー   

            //新規継続　初検日と診療年月が同じ年月は新規、それ以外は継続
            sb.AppendLine(",fchargetype = ");
            sb.AppendLine("     case when replace(substr(cast(firstdate1AD as varchar),1,7),'-','')" +
                "                               =cast(shinryoYMAD as varchar)  then 1 ");
            sb.AppendLine("     else 2 end ");

            //入力済みにしておく
            sb.AppendLine(",statusflags=1");

            //登録したユーザ
            sb.AppendLine($",ufirst={User.CurrentUser.UserID} ");
            sb.AppendLine(",taggeddatas	              ");
            sb.AppendLine("       = 'GeneralString1:\"' || ahk.f001shinsaYM || '\"' ");//審査年月
            sb.AppendLine("'|GeneralString2:\"' || ahk.shinsaYMAD || '\"' ");//審査年月西暦

            sb.AppendLine(" from dataimport_ahk ahk ");
            sb.AppendLine(" where ");
            sb.AppendLine($" replace(application.emptytext3,'.tif','')=ahk.f025comnum and application.cym={cym}");

            //申請書は空欄　または　マルチtiffの場合0
            sb.AppendLine($" and (emptytext2='0' or emptytext2='')");
            //sb.AppendLine($" and emptytext2='0' or emptytext2=''");



            //20210726174907 furukawa st ////////////////////////
            //トランザクション追加

            DB.Command cmd = new DB.Command(sb.ToString(),tran);
            //DB.Command cmd = new DB.Command(DB.Main, sb.ToString(), true);
            //20210726174907 furukawa ed ////////////////////////


            //20210726174926 furukawa st ////////////////////////            
            //あはきは入力無しなので、入力登録時に申請書タイプを入力できない。
            //そこで提供データと紐付かないレコードは全て続紙として登録しないと、続紙認識ができず点検画面で続紙が出てこない
            System.Text.StringBuilder sbZokushi = new StringBuilder();
            sbZokushi.AppendLine("update application set ");
            sbZokushi.AppendLine($" ayear        = {(int)APP_TYPE.続紙} ");
            sbZokushi.AppendLine($",aapptype     = {(int)APP_TYPE.続紙} ");
            sbZokushi.AppendLine(" where ");
            sbZokushi.AppendLine(" aapptype=0 ");
            sbZokushi.AppendLine($" and cym={cym} ");



            DB.Command cmdZokushi = new DB.Command(sbZokushi.ToString(),tran);

            //20210726174926 furukawa ed ////////////////////////

            try
            {
                cmd.TryExecuteNonQuery();

                cmdZokushi.TryExecuteNonQuery();

                //20210726174851 furukawa st ////////////////////////
                //トランザクション追加
                
                tran.Commit();
                //20210726174851 furukawa ed ////////////////////////

                return true;

            }
            catch (Exception ex)
            {
                tran.Rollback();

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                cmdZokushi.Dispose();
            }
        }

        private partial class names
        {
            public string comnum { get; set; } = string.Empty;
            public string zip { get; set; } = string.Empty;
            public string address { get; set; } = string.Empty;
            public string name { get; set; } = string.Empty;
            public string kana { get; set; } = string.Empty;
        }

        /// <summary>
        /// csvをインポート
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData(string strFileName, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from dataimport_ahk where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {

                wf.LogPrint("CSVロード");
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                //20220204111525 furukawa st ////////////////////////
                //列数が違っても入ってしまうのでチェック
                
                wf.LogPrint("CSV列数チェック");
                wf.LogPrint($"CSV列数{lst[0].Length}");

                //列数チェック
                int colCnt = 68;
                if (lst[0].Length != colCnt)
                {
                    System.Windows.Forms.MessageBox.Show($"列数が{colCnt}ではありません。ファイルの中身を確認してください", "",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }
                //20220204111525 furukawa ed ////////////////////////


                wf.SetMax(lst.Count);

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                    
                    //合計金額でヘッダかどうか判断
                    try
                    {
                        int.Parse(tmp[27].ToString().Replace(",", ""));
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataImport_AHK cls = new dataImport_AHK();

                    cls.f001shinsaYM = tmp[1];          //審査月
                    cls.f002shinryoYM = tmp[2];         //診療月
                    cls.f003regnum1 = tmp[3];           //登録記号番号
                    cls.f004regnum2 = tmp[4];           //登録記号番号2
                    cls.f005kind = tmp[5];              //種別
                    cls.f006family = tmp[6];            //本家区分
                    cls.f007insnum = tmp[7];            //保険者番号
                    cls.f008hihonum = tmp[8];           //記号番号
                    cls.f009pname = tmp[11];            //受療者名
                    cls.f010pgender = tmp[12];          //性別
                    cls.f011pbirthday = tmp[13];        //生年月日
                    cls.f012firstdate1 = tmp[16];       //初療年月日
                    cls.f013startdate1 = tmp[17];       //施術開始日
                    cls.f014finishdate1 = tmp[18];      //施術終了日
                    cls.f015counteddays = tmp[19];      //実日数
                    cls.f016total = tmp[25];            //合計金額
                    cls.f017partial = tmp[26];          //一部負担金
                    cls.f018charge = tmp[27];           //請求額
                    cls.f019 = tmp[30];                 //施術日
                    cls.f020 = tmp[31];                 //施術証明欄日付
                    cls.f021 = tmp[33];                 //申請欄日付
                    cls.f022accnum = tmp[34];           //口座番号
                    cls.f023douiday = tmp[35];          //同意年月日
                    cls.f024ininday = tmp[36];          //受療委任欄日付
                    cls.f025comnum = tmp[39];           //全国レセプト共通キー


                    //管理項目
                    string strErr = string.Empty;
                    cls.cym = intcym;//メホール請求年月管理用;

                    //審査年月西暦;                    
                    cls.shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f001shinsaYM));       //審査年月西暦
                    cls.shinryoYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f002shinryoYM));      //施術年月西暦
                    cls.birthdayAD = DateTimeEx.GetDateFromJstr7(cls.f011pbirthday);;                       //生年月日西暦
                    cls.firstdate1AD = DateTimeEx.GetDateFromJstr7(cls.f012firstdate1);
                    cls.startdate1AD = DateTimeEx.GetDateFromJstr7(cls.f013startdate1);
                    cls.finishdate1AD = DateTimeEx.GetDateFromJstr7(cls.f014finishdate1);
                    cls.f020AD = DateTimeEx.GetDateFromJstr7(cls.f020);
                    cls.f021AD = DateTimeEx.GetDateFromJstr7(cls.f021);
                    cls.douidayAD= DateTimeEx.GetDateFromJstr7(cls.f023douiday);
                    cls.inindayAD = DateTimeEx.GetDateFromJstr7(cls.f024ininday);

                    //被保険者証記号半角;                                                                                                
                    ////記号は漢字の可能性が                                                                          
                    //try
                    //{
                    //    cls.hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f002hihomark, Microsoft.VisualBasic.VbStrConv.Narrow);
                    //}
                    //catch
                    //{
                    //    cls.hihomark_narrow = cls.f002hihomark;
                    //}

                    cls.hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(cls.f008hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;

                    if (strErr != string.Empty)
                    {
                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{strErr}");
                        tran.Rollback();
                        return false;
                    }

                    if (!DB.Main.Insert<dataImport_AHK>(cls)) return false;
                    wf.LogPrint($"あはき提供データ  レセプト全国共通キー:{cls.f025comnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");



                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
            }

        }


        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }


        ///// <summary>
        ///// あはき提供データの取得
        ///// </summary>
        ///// <returns></returns>
        //public static List<dataImport_AHK> SelectAll(int _cym)
        //{
        //    List<dataImport_AHK> lst = new List<dataImport_AHK>();
        //    DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport_ahk where cym={_cym}");
        //    var l=cmd.TryExecuteReaderList();
        //    for (int r = 0; r < l.Count; r++)
        //    {
        //        int c = 0;
        //        dataImport_AHK d = new dataImport_AHK();

        //        d.importid = int.Parse(l[r][c++].ToString());
        //        d.cym = int.Parse(l[r][c++].ToString());
        //        d.insnum = l[r][c++].ToString();
        //        d.hihomark = l[r][c++].ToString();
        //        d.hihonum = l[r][c++].ToString();
        //        d.pname = l[r][c++].ToString();//20200818190302 furukawa受療者名納品データに必要なので入れとく
        //        d.shinryoym = l[r][c++].ToString();
        //        d.clinicnum = l[r][c++].ToString();
        //        d.clinicname = l[r][c++].ToString();

        //        d.atenanum = l[r][c++].ToString();//20200818162856 furukawa宛名番号追加

        //        d.pbirthday = l[r][c++].ToString();
        //        d.psex = l[r][c++].ToString();
        //        d.kind = l[r][c++].ToString();
        //        d.ratio = int.Parse(l[r][c++].ToString());
        //        d.shinsaym = l[r][c++].ToString();
        //        d.counteddays = int.Parse(l[r][c++].ToString());
        //        d.total = int.Parse(l[r][c++].ToString());
        //        d.charge = int.Parse(l[r][c++].ToString());
        //        d.partial = int.Parse(l[r][c++].ToString());
        //        d.comnum = l[r][c++].ToString();
        //        d.hihozip = l[r][c++].ToString();
        //        d.hihoadd = l[r][c++].ToString();
        //        //d.hihoname = l[r][c++].ToString();//20200904110809 furukawa 被保険者名はないので削除
        //        d.shinsaymad = l[r][c++].ToString();
        //        d.ymad = l[r][c++].ToString();
        //        d.pbirthad = DateTime.Parse(l[r][c++].ToString());

        //        lst.Add(d);
        //    }

        //    return lst;

        //}


    }


    /// <summary>
    /// 既出テーブルだが使わないかも
    /// (既に出力した人は出さない場合があるので、その人を記録しておくテーブル)
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
