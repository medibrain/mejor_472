﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Sakaishi
{
    public partial class FormImport : Form
    {

        int cym=0;
        DataGridViewButtonColumn bc = new DataGridViewButtonColumn();

        public FormImport(int _cym)
        {
            InitializeComponent();
            cym = _cym;
            dispAHK();
            dispSupplyData();

            buttonUpdateOnly.Visible = User.CurrentUser.Developer;
        }

        private void buttonAHKCSV_Click(object sender, EventArgs e)
        {
            if (dataImport_AHK.GetCountCYM(cym) > 0)
            {
                if (MessageBox.Show("既にデータが存在します。上書きしますか？", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "csv|*.csv";
            if (dlg.ShowDialog() != DialogResult.OK) return;

            string strAHKFileName= dlg.FileName;

            dataImport_AHK.dataImport_AHK_main(cym, strAHKFileName);

            dispAHK();
            dispSupplyData();
        }

        private void buttonSupplyData_Click(object sender, EventArgs e)
        {
            if (dataImport_jyusei.GetCountCYM(cym) > 0)
            {
                if (MessageBox.Show("既にデータが存在します。上書きしますか？", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            }

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "csv|*.csv";
            if (dlg.ShowDialog() != DialogResult.OK) return;
            
            string strSupplyFileName = dlg.FileName;

            dataImport_jyusei.dataImport_main(cym,strSupplyFileName);
            dispAHK();
            dispSupplyData();
        }

        private void dispAHK()
        {
            dataGridViewAHK.DataSource=dataImport_AHK.GetDispCount();
     

            //bc.UseColumnTextForButtonValue = true;
            //bc.Text = "削除";
            //bc.Width = 50;
            //bc.HeaderText = "";

            //bc.Name = "btnDeleteColumn";

            //if (!dataGridViewAHK.Columns.Contains(bc)) dataGridViewAHK.Columns.Insert(0, bc);


            dataGridViewAHK.Columns[0].HeaderText = "メホール請求年月";
            dataGridViewAHK.Columns[1].HeaderText = "行数";
            dataGridViewAHK.Columns[0].Width = 150;
        }


     

        //private void dataGridViewKokuho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (e.ColumnIndex != 0) return;
        //    int delcym = 0;
        //    int.TryParse(dataGridViewAHK.Rows[e.RowIndex].Cells["cym"].Value.ToString(), out delcym);
        //    dataImport_AHK.DeleteRecords(delcym);

        //    dispAHK();
        //}


        private void dispSupplyData()
        {
            dataGridViewSupplyData.DataSource = dataImport_jyusei.GetDispCount();
            dataGridViewSupplyData.Columns[0].HeaderText = "メホール請求年月";
            dataGridViewSupplyData.Columns[1].HeaderText = "行数";
            dataGridViewSupplyData.Columns[0].Width = 150;
        }

        private void buttonUpdateOnly_Click(object sender, EventArgs e)
        {
            dataImport_AHK.UpdateOnly(cym);
        }
    }
}
