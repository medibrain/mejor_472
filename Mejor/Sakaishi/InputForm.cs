﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Sakaishi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);


        //20210616135811 furukawa st ////////////////////////
        /// <summary>
        /// ナンバリング座標は最下段
        /// </summary>
        Point posNumbering = new Point(1800, 3000);
        //20210616135811 furukawa ed ////////////////////////



        //20210610115453 furukawa st ////////////////////////
        //あはき用調整

        Point posTotalAHK = new Point(1000, 800);
        //      Point posTotalAHK = new Point(1000, 1000);
        //20210610115453 furukawa ed ////////////////////////

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;
      
        /// <summary>
        /// 国保データ柔整あはき両方
        /// </summary>
        List<dataImport_jyusei> lstdataImport_jyusei = new List<dataImport_jyusei>();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            //20210517111027 furukawa st ////////////////////////
            //柔整のみ入力

            if (sGroup.AppType != APP_TYPE.柔整)
            {
                MessageBox.Show("柔整のみ入力可能です");
                return;
            }
            //20210517111027 furukawa ed ////////////////////////


            InitializeComponent();

            

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount, verifyBoxFamily, pBirthday,verifyBoxSex};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge, };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 };

           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //国保データのリスト（柔整）                
                lstdataImport_jyusei = dataImport_jyusei.GetDataImport_Jyusei(app.CYM);
                
                //負傷数は柔整のみ
                verifyBoxFushoCount.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;

               

                setApp(app);
            }

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);

            //20210427105530 furukawa st ////////////////////////
            //フォーム表示後にグリッド選択させないと、グリッド選択がフォーム表示時に反映されない                       
            App app = (App)bsApp.Current;
            createGrid(app);
            //20210427105530 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;
            dgv.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelunder.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelunder.Visible = false;
                    pDoui.Visible = true;
                    break;
                    
                default:
                    panelunder.Visible = true;
                    phnum.Visible = true;
                    dgv.Visible = true;
                    //pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            //提供データ
            dataImport_jyusei impdata = lstdataImport_jyusei[0];

            dgv.Columns[nameof(impdata.f000importid)].Width = 50;
            dgv.Columns[nameof(impdata.f001shinsaYM)].Width = 80;
            dgv.Columns[nameof(impdata.f002hihomark)].Width = 50;
            dgv.Columns[nameof(impdata.f003hihonum)].Width = 100;
            dgv.Columns[nameof(impdata.f004pname)].Width = 100;
            dgv.Columns[nameof(impdata.f005pbirthday)].Width = 120;
            dgv.Columns[nameof(impdata.f006pgender)].Width = 50;
            dgv.Columns[nameof(impdata.f007shinryoYM)].Width = 80;
            dgv.Columns[nameof(impdata.f008counteddays)].Width = 50;
            dgv.Columns[nameof(impdata.f009total)].Width = 50;
            dgv.Columns[nameof(impdata.f010clinicnum)].Width = 100;
            dgv.Columns[nameof(impdata.f011clinicname)].Width = 100;
            dgv.Columns[nameof(impdata.f012)].Width = 50;
            dgv.Columns[nameof(impdata.f013)].Width = 50;
            dgv.Columns[nameof(impdata.f014comnum)].Width = 100;
            dgv.Columns[nameof(impdata.f100shinsaYMAD)].Width = 50;
            dgv.Columns[nameof(impdata.f101sejutsuYMAD)].Width = 50;
            dgv.Columns[nameof(impdata.f102birthAD)].Width = 100;
            dgv.Columns[nameof(impdata.f103firstdateAD)].Width = 50;
            dgv.Columns[nameof(impdata.f104hihomark_narrow)].Width = 50;
            dgv.Columns[nameof(impdata.f105hihonum_narrow)].Width = 80;
            dgv.Columns[nameof(impdata.cym)].Width = 50;


            dgv.Columns[nameof(impdata.f000importid)].HeaderText = "importid";
            dgv.Columns[nameof(impdata.f001shinsaYM)].HeaderText = "審査年月";
            dgv.Columns[nameof(impdata.f002hihomark)].HeaderText = "証記号";
            dgv.Columns[nameof(impdata.f003hihonum)].HeaderText = "証番号";
            dgv.Columns[nameof(impdata.f004pname)].HeaderText = "氏名";
            dgv.Columns[nameof(impdata.f005pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(impdata.f006pgender)].HeaderText = "性別";
            dgv.Columns[nameof(impdata.f007shinryoYM)].HeaderText = "診療年月";
            dgv.Columns[nameof(impdata.f008counteddays)].HeaderText = "実日数";
            dgv.Columns[nameof(impdata.f009total)].HeaderText = "決定点数";
            dgv.Columns[nameof(impdata.f010clinicnum)].HeaderText = "機関コード";
            dgv.Columns[nameof(impdata.f011clinicname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(impdata.f012)].HeaderText = "療養費種別";
            dgv.Columns[nameof(impdata.f013)].HeaderText = "疑義種別";
            dgv.Columns[nameof(impdata.f014comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(impdata.f100shinsaYMAD)].HeaderText = "審査年月西暦";
            dgv.Columns[nameof(impdata.f101sejutsuYMAD)].HeaderText = "診療年月西暦";
            dgv.Columns[nameof(impdata.f102birthAD)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(impdata.f103firstdateAD)].HeaderText = "初検日西暦だが未使用";
            dgv.Columns[nameof(impdata.f104hihomark_narrow)].HeaderText = "被保険者証記号半角";
            dgv.Columns[nameof(impdata.f105hihonum_narrow)].HeaderText = "証番号半角";
            dgv.Columns[nameof(impdata.cym)].HeaderText = "メホール請求年月";


            dgv.Columns[nameof(impdata.f000importid)].Visible = true;
            dgv.Columns[nameof(impdata.f001shinsaYM)].Visible = true;
            dgv.Columns[nameof(impdata.f002hihomark)].Visible = false;
            dgv.Columns[nameof(impdata.f003hihonum)].Visible = true;
            dgv.Columns[nameof(impdata.f004pname)].Visible = true;
            dgv.Columns[nameof(impdata.f005pbirthday)].Visible = true;
            dgv.Columns[nameof(impdata.f006pgender)].Visible = true;
            dgv.Columns[nameof(impdata.f007shinryoYM)].Visible = true;
            dgv.Columns[nameof(impdata.f008counteddays)].Visible = true;
            dgv.Columns[nameof(impdata.f009total)].Visible = true;
            dgv.Columns[nameof(impdata.f010clinicnum)].Visible = true;
            dgv.Columns[nameof(impdata.f011clinicname)].Visible = true;
            dgv.Columns[nameof(impdata.f012)].Visible = false;
            dgv.Columns[nameof(impdata.f013)].Visible =false;
            dgv.Columns[nameof(impdata.f014comnum)].Visible = true;
            dgv.Columns[nameof(impdata.f100shinsaYMAD)].Visible = true;
            dgv.Columns[nameof(impdata.f101sejutsuYMAD)].Visible = true;
            dgv.Columns[nameof(impdata.f102birthAD)].Visible = true;
            dgv.Columns[nameof(impdata.f103firstdateAD)].Visible = false;//もともとデータに入ってない
            dgv.Columns[nameof(impdata.f104hihomark_narrow)].Visible = false;
            dgv.Columns[nameof(impdata.f105hihonum_narrow)].Visible = true;
            dgv.Columns[nameof(impdata.cym)].Visible = true;


        }

        /// <summary>
        /// 提供データグリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid(App app = null)
        {
            List<dataImport_jyusei> lstimp = new List<dataImport_jyusei>();

            string strhnum = verifyBoxHnum.Text.Trim();
            
            //入力してない時のShown時は生年月日入っているはずがないので、それを基準に下のチェックを通さない。
            //入力後のShown時は生年月日入っているはずなのでdatecheckが通っても、正常であれば赤くならない
            //ヒホバンを判定に使用したのは、ヒホバンがないのはあり得ないから
            if (strhnum == string.Empty) return;

            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            DateTime dtbirth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);
            string strGender = verifyBoxSex.GetIntValue() == 1 ? "男" : "女";
            
            //20210517140607 furukawa st ////////////////////////
            //ヒホバンに前ゼロを付けたので提供データと合わなくなったため、前ゼロを取る            
            string strHihonum = verifyBoxHnum.GetIntValue().ToString();
            //20210517140607 furukawa ed ////////////////////////


            foreach (dataImport_jyusei item in lstdataImport_jyusei)
            {
                //被保番半角、メホール請求年月、合計金額、生年月日、性別で探す
                if (item.f105hihonum_narrow == strHihonum &&
                    item.cym == scan.CYM &&
                    item.f009total.Replace(",", "") == verifyBoxTotal.Text.ToString().Trim() &&
                    item.f102birthAD == dtbirth &&
                    item.f006pgender == strGender)

                    //if (item.f105hihonum_narrow == verifyBoxHnum.Text.Trim() &&
                    //item.cym == scan.CYM &&
                    //item.f009total.Replace(",", "") == verifyBoxTotal.Text.ToString().Trim() &&
                    //item.f102birthAD == dtbirth &&
                    //item.f006pgender == strGender)

                    {
                        lstimp.Add(item);
                }
            }


            dgv.DataSource = null;

            //0件の場合グリッド作らない
            if (lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
                return;
            }

            dgv.DataSource = lstimp;



            InitGrid();

            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();



            if (app != null && app.RrID.ToString() != string.Empty)//&& app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {

                    int intRRid = int.Parse(r.Cells["f000importid"].Value.ToString());

                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells["f014comnum"].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (r.Cells["f000importid"].Value.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {
                        //レセプト全国共通キーがある場合はそれで一致させる
                        //20220127165129 furukawa st ////////////////////////
                        //レセプト全国共通キーは空の場合は比較させない
                        
                        if (app.ComNum != string.Empty && strcomnum == app.ComNum)
                        //  if (strcomnum == app.ComNum)
                        //20220127165129 furukawa ed ////////////////////////

                        {
                            r.Selected = true;
                            break;
                        }


                        //20220127165220 furukawa st ////////////////////////
                        //比較対象に施術年月追加
                        
                        else if (app.ComNum == string.Empty &&
                            intTotal == int.Parse(verifyBoxTotal.Text.Trim()) &&
                            strhnum == verifyBoxHnum.Text.Trim() &&
                            intymad == int.Parse(r.Cells["f101sejutsuYMAD"].Value.ToString()))

                        //      else if (app.ComNum == string.Empty &&
                        //      intTotal == int.Parse(verifyBoxTotal.Text.Trim()) &&
                        //      strhnum == verifyBoxHnum.Text.Trim())
                        //20220127165220 furukawa ed ////////////////////////
                        {
                            //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;

                        }
                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            
            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)           

            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        private void verifyBoxHnum_Validating(object sender, CancelEventArgs e)
        {
            //20210517112840 furukawa st ////////////////////////
            //前ゼロを付けてあはきと合わせないと、リスト作成画面でゼロ付きゼロ無しを区別するのが面倒

            if (verifyBoxHnum.Text != string.Empty) verifyBoxHnum.Text = verifyBoxHnum.Text.PadLeft(8, '0');
            //if (verifyBoxHnum.Text != string.Empty) verifyBoxHnum.Text = verifyBoxHnum.GetIntValue().ToString();
            //20210517112840 furukawa ed ////////////////////////
        }

        private void verifyBoxNumbering_Validated(object sender, EventArgs e)
        {
            //20210614171534 furukawa st ////////////////////////
            //ナンバリングに前ゼロ

            if (verifyBoxNumbering.Text.Trim() != string.Empty) verifyBoxNumbering.Text = verifyBoxNumbering.Text.PadLeft(4, '0');
            //20210614171534 furukawa ed ////////////////////////
        }

        //20220127161625 furukawa st ////////////////////////
        //PageUpを押すと登録と同時にグリッドの位置がずれて，ずれたまま登録される
        
        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) e.Handled = true;
        }
        //20220127161625 furukawa ed ////////////////////////

        #endregion


        #region 各種ロード

        /// <summary>
        /// scanタイプでコントロール使用可否制御
        /// </summary>
        private void VisibleControlByScanType()
        {
            //負傷数は柔整のみ
            verifyBoxFushoCount.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;
            labelFushoCount.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;

        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            //scanのapptypeでコントロール使用可否制御
            VisibleControlByScanType();
            
            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();


                    //20210610115549 furukawa st ////////////////////////
                    //柔整300/あはき200でDPI違う
                    scrollPictureControl1.Ratio = CommonTool.AutoAdjustScreen(img);
                    //      scrollPictureControl1.Ratio = 0.4f;
                    //20210610115549 furukawa ed ////////////////////////


                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum,VbStrConv.Narrow), firstTime, nv);

                    //受療者生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                    //性別
                    setValue(verifyBoxSex, app.Sex.ToString(), firstTime, nv);

                    //負傷カウント                   
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);  

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //請求金額
                    setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //20210614165715 furukawa st ////////////////////////
                    //ナンバリング追加 
                    setValue(verifyBoxNumbering, app.Numbering.ToString(), firstTime, nv);
                    //20210614165715 furukawa ed ////////////////////////
                    


                    //グリッド選択する                    
                    createGrid(app);
                   
                    
                    break;
            }
        }


        #endregion

        #region 各種更新


        private bool CheckNumbering(int intNumbering)
        {
            if (intPrevNumbering == 0) return true;
            if (intPrevNumbering > intNumbering) return true;//戻ったときの対策
            if (Math.Abs(intNumbering - intPrevNumbering) == 1) return true;
            else return false;
            
        }

        /// <summary>
        /// 提供データをAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistSupplyData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    dataImport_jyusei k = lstdataImport_jyusei[0];
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(k.f000importid)].Value.ToString()); //ID


                    //入力を優先させる
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                                //被保険者証番号
                    app.PersonName = dgv.CurrentRow.Cells[nameof(k.f004pname)].Value.ToString();                      //受療者名
                    //app.InsNum = dgv.CurrentRow.Cells[nameof(k.f021_insnum)].Value.ToString();                         //保険者番号
                    app.Sex = dgv.CurrentRow.Cells[nameof(k.f006pgender)].Value.ToString() == "男" ? 1 : 2;           //受療者性別                    
                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.f102birthAD)].Value.ToString());        //受療者生年月日
                    app.ClinicName = dgv.CurrentRow.Cells[nameof(k.f011clinicname)].Value.ToString();                      //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f010clinicnum)].Value.ToString();                         //医療機関コード
                    app.ComNum = dgv.CurrentRow.Cells[nameof(k.f014comnum)].Value.ToString();                         //レセプト全国共通キー
                    app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(k.f008counteddays)].Value.ToString());  //施術日数
                    app.AppType = APP_TYPE.柔整;          //種別

                    app.TaggedDatas.GeneralString2 = dgv.CurrentRow.Cells[nameof(k.f001shinsaYM)].Value.ToString();  //審査年月
                    app.TaggedDatas.GeneralString3 = dgv.CurrentRow.Cells[nameof(k.f100shinsaYMAD)].Value.ToString();  //審査年月和暦

                    //int.TryParse(dgv.CurrentRow.Cells[nameof(k.)].Value.ToString(), out int tmpRatio);
                    //app.Ratio = tmpRatio;  //給付割合

                    //string strfamily = dgv.CurrentRow.Cells[nameof(k.f014_honke)].Value.ToString() == "本人" ? "2" : "6";
                    //app.Family = int.Parse(strfamily);

                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(k.f101sejutsuYMAD)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(k.f101sejutsuYMAD)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


    
        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //生年月日
                    DateTime dtbirth = DateTimeEx.DateTimeNull;
                    dtbirth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //性別
                    int intsex = verifyBoxSex.GetIntValue();
                    setStatus(verifyBoxSex, intsex < 0 || intsex>2) ;
                                    
                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);

                 
                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //請求金額
                    int intcharge = verifyBoxCharge.GetIntValue();
                    setStatus(verifyBoxCharge, intcharge >= total　|| intcharge < 100 || intcharge > 200000);

                    //一部負担金　画面に出さないが算出。何かのリストで必要（2021/04/14　神崎さん）
                    int intPartial = total - intcharge;

                    //負傷数
                    int fushoCount = verifyBoxFushoCount.GetIntValue();
                    if(scan.AppType==APP_TYPE.柔整)setStatus(verifyBoxFushoCount, fushoCount <=0);


                    //20210614165544 furukawa st ////////////////////////
                    //ナンバリング追加

                    string strNumbering = verifyBoxNumbering.Text.Trim();
                    if (scan.AppType == APP_TYPE.柔整) setStatus(verifyBoxNumbering, strNumbering == string.Empty || !int.TryParse(strNumbering,out int tmp));
                    //20210614165544 furukawa ed ////////////////////////


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    //20210427115638 furukawa st ////////////////////////
                    //合計金額：請求金額：7，8,9割固定割合のチェック
                    
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = true;

                    if (ratioError) ratioError = !CommonTool.CheckChargeFromRatio(total, 7, intcharge);
                    if (ratioError) ratioError = !CommonTool.CheckChargeFromRatio(total, 8, intcharge);
                    if (ratioError) ratioError = !CommonTool.CheckChargeFromRatio(total, 9, intcharge);                    
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    //20210427115638 furukawa ed ////////////////////////

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月

                    app.HihoNum =hnumN;                //被保険者証番号                                       

                    app.Sex = intsex;//性別
                    app.Birthday = dtbirth;//生年月日


                    //申請書種別
                    app.AppType = scan.AppType;

                    //初検日
                    app.FushoFirstDate1 = f1FirstDt;

                    //負傷数                    
                    app.TaggedDatas.count = fushoCount;

                  
                    //合計
                    app.Total = total;
                    
                    //請求金額
                    app.Charge = intcharge;

                    //一部負担金
                    app.Partial = intPartial;

                    //20210614165627 furukawa st ////////////////////////
                    //ナンバリング追加
                    app.Numbering = strNumbering.ToString();
                    //20210614165627 furukawa ed ////////////////////////




                    //当初あはきはExcelの予定だったが、国保CSVにまとまったらしい
                    RegistSupplyData(app);


                    //マッチング無し確認は申請書のみ
                    
                    if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    {
                        if (dgv.Rows.Count == 0 || dgv.SelectedRows.Count==0)
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
               
                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            createGrid(app);
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }

            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

       

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            else if (sender==verifyBoxNumbering) p = posNumbering;//20210616135931 furukawa ナンバリング座標は最下段
                                                                  

            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
