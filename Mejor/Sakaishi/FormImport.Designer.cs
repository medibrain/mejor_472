﻿namespace Mejor.Sakaishi
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewSupplyData = new System.Windows.Forms.DataGridView();
            this.dataGridViewAHK = new System.Windows.Forms.DataGridView();
            this.buttonSupplyData = new System.Windows.Forms.Button();
            this.buttonCSV = new System.Windows.Forms.Button();
            this.buttonUpdateOnly = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSupplyData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAHK)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewSupplyData
            // 
            this.dataGridViewSupplyData.AllowUserToAddRows = false;
            this.dataGridViewSupplyData.AllowUserToDeleteRows = false;
            this.dataGridViewSupplyData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSupplyData.Location = new System.Drawing.Point(52, 13);
            this.dataGridViewSupplyData.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewSupplyData.Name = "dataGridViewSupplyData";
            this.dataGridViewSupplyData.Size = new System.Drawing.Size(363, 142);
            this.dataGridViewSupplyData.TabIndex = 0;
            // 
            // dataGridViewAHK
            // 
            this.dataGridViewAHK.AllowUserToAddRows = false;
            this.dataGridViewAHK.AllowUserToDeleteRows = false;
            this.dataGridViewAHK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAHK.Location = new System.Drawing.Point(52, 201);
            this.dataGridViewAHK.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewAHK.Name = "dataGridViewAHK";
            this.dataGridViewAHK.Size = new System.Drawing.Size(363, 131);
            this.dataGridViewAHK.TabIndex = 0;
            // 
            // buttonSupplyData
            // 
            this.buttonSupplyData.Location = new System.Drawing.Point(457, 45);
            this.buttonSupplyData.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSupplyData.Name = "buttonSupplyData";
            this.buttonSupplyData.Size = new System.Drawing.Size(156, 70);
            this.buttonSupplyData.TabIndex = 1;
            this.buttonSupplyData.Text = "柔整提供データ\r\nインポート";
            this.buttonSupplyData.UseVisualStyleBackColor = true;
            this.buttonSupplyData.Click += new System.EventHandler(this.buttonSupplyData_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Location = new System.Drawing.Point(457, 225);
            this.buttonCSV.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(156, 67);
            this.buttonCSV.TabIndex = 1;
            this.buttonCSV.Text = "あはきCSV\r\nインポート";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonAHKCSV_Click);
            // 
            // buttonUpdateOnly
            // 
            this.buttonUpdateOnly.Location = new System.Drawing.Point(457, 299);
            this.buttonUpdateOnly.Name = "buttonUpdateOnly";
            this.buttonUpdateOnly.Size = new System.Drawing.Size(156, 39);
            this.buttonUpdateOnly.TabIndex = 2;
            this.buttonUpdateOnly.Text = "あはき更新のみ";
            this.buttonUpdateOnly.UseVisualStyleBackColor = true;
            this.buttonUpdateOnly.Click += new System.EventHandler(this.buttonUpdateOnly_Click);
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 377);
            this.Controls.Add(this.buttonUpdateOnly);
            this.Controls.Add(this.buttonCSV);
            this.Controls.Add(this.buttonSupplyData);
            this.Controls.Add(this.dataGridViewAHK);
            this.Controls.Add(this.dataGridViewSupplyData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormImport";
            this.Text = "FormImport";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSupplyData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAHK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewSupplyData;
        private System.Windows.Forms.DataGridView dataGridViewAHK;
        private System.Windows.Forms.Button buttonSupplyData;
        private System.Windows.Forms.Button buttonCSV;
        private System.Windows.Forms.Button buttonUpdateOnly;
    }
}