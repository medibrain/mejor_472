﻿namespace Mejor
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonInspectCheck = new System.Windows.Forms.Button();
            this.listBoxInsurer = new System.Windows.Forms.ListBox();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewMonth = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonHusen = new System.Windows.Forms.Button();
            this.gbSagyo = new System.Windows.Forms.GroupBox();
            this.panelDown = new System.Windows.Forms.Panel();
            this.buttonHenreiPrint = new System.Windows.Forms.Button();
            this.buttonGaibu = new System.Windows.Forms.Button();
            this.buttonDataExport = new System.Windows.Forms.Button();
            this.buttonOther = new System.Windows.Forms.Button();
            this.buttonShokai = new System.Windows.Forms.Button();
            this.buttonAidSearch = new System.Windows.Forms.Button();
            this.panelUpper = new System.Windows.Forms.Panel();
            this.buttonScan = new System.Windows.Forms.Button();
            this.buttonListInput = new System.Windows.Forms.Button();
            this.buttonQuery = new System.Windows.Forms.Button();
            this.buttonMatching = new System.Windows.Forms.Button();
            this.buttonOCRCheck = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButtonKokuho = new System.Windows.Forms.RadioButton();
            this.radioButtonKoiki = new System.Windows.Forms.RadioButton();
            this.radioButtonKenpo = new System.Windows.Forms.RadioButton();
            this.radioButtonGakko = new System.Windows.Forms.RadioButton();
            this.radioButtonOther = new System.Windows.Forms.RadioButton();
            this.buttonVersion = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.checkBoxAllVisible = new System.Windows.Forms.CheckBox();
            this.lblconn = new System.Windows.Forms.Label();
            this.checkBoxPrevious = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonth)).BeginInit();
            this.gbSagyo.SuspendLayout();
            this.panelDown.SuspendLayout();
            this.panelUpper.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExit.Location = new System.Drawing.Point(44, 647);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(100, 35);
            this.buttonExit.TabIndex = 14;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonInspectCheck
            // 
            this.buttonInspectCheck.Location = new System.Drawing.Point(149, 141);
            this.buttonInspectCheck.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInspectCheck.Name = "buttonInspectCheck";
            this.buttonInspectCheck.Size = new System.Drawing.Size(130, 64);
            this.buttonInspectCheck.TabIndex = 4;
            this.buttonInspectCheck.Text = "点検作業";
            this.buttonInspectCheck.UseVisualStyleBackColor = true;
            this.buttonInspectCheck.Click += new System.EventHandler(this.buttonInspect_Click);
            // 
            // listBoxInsurer
            // 
            this.listBoxInsurer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxInsurer.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.listBoxInsurer.FormattingEnabled = true;
            this.listBoxInsurer.ItemHeight = 24;
            this.listBoxInsurer.Location = new System.Drawing.Point(228, 41);
            this.listBoxInsurer.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxInsurer.Name = "listBoxInsurer";
            this.listBoxInsurer.Size = new System.Drawing.Size(298, 532);
            this.listBoxInsurer.TabIndex = 8;
            this.listBoxInsurer.SelectedIndexChanged += new System.EventHandler(this.listBoxInsurer_SelectedIndexChanged);
            // 
            // buttonSettings
            // 
            this.buttonSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSettings.Location = new System.Drawing.Point(153, 647);
            this.buttonSettings.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(100, 35);
            this.buttonSettings.TabIndex = 15;
            this.buttonSettings.Text = "設定";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(225, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "保険者：";
            // 
            // dataGridViewMonth
            // 
            this.dataGridViewMonth.AllowUserToAddRows = false;
            this.dataGridViewMonth.AllowUserToDeleteRows = false;
            this.dataGridViewMonth.AllowUserToResizeColumns = false;
            this.dataGridViewMonth.AllowUserToResizeRows = false;
            this.dataGridViewMonth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewMonth.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewMonth.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMonth.Location = new System.Drawing.Point(568, 41);
            this.dataGridViewMonth.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewMonth.MultiSelect = false;
            this.dataGridViewMonth.Name = "dataGridViewMonth";
            this.dataGridViewMonth.ReadOnly = true;
            this.dataGridViewMonth.RowHeadersVisible = false;
            this.dataGridViewMonth.RowTemplate.Height = 21;
            this.dataGridViewMonth.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMonth.Size = new System.Drawing.Size(252, 532);
            this.dataGridViewMonth.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(576, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "請求月：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(538, 239);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "→";
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(8, 4);
            this.buttonImport.Margin = new System.Windows.Forms.Padding(4);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(130, 64);
            this.buttonImport.TabIndex = 5;
            this.buttonImport.Text = "データ\r\nインポート";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonHusen
            // 
            this.buttonHusen.Location = new System.Drawing.Point(149, 4);
            this.buttonHusen.Margin = new System.Windows.Forms.Padding(4);
            this.buttonHusen.Name = "buttonHusen";
            this.buttonHusen.Size = new System.Drawing.Size(130, 64);
            this.buttonHusen.TabIndex = 6;
            this.buttonHusen.Text = "付箋作成";
            this.buttonHusen.UseVisualStyleBackColor = true;
            this.buttonHusen.Visible = false;
            this.buttonHusen.Click += new System.EventHandler(this.buttonHusen_Click);
            // 
            // gbSagyo
            // 
            this.gbSagyo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbSagyo.Controls.Add(this.panelDown);
            this.gbSagyo.Controls.Add(this.panelUpper);
            this.gbSagyo.Controls.Add(this.buttonOCRCheck);
            this.gbSagyo.Location = new System.Drawing.Point(863, 13);
            this.gbSagyo.Margin = new System.Windows.Forms.Padding(4);
            this.gbSagyo.Name = "gbSagyo";
            this.gbSagyo.Padding = new System.Windows.Forms.Padding(4);
            this.gbSagyo.Size = new System.Drawing.Size(306, 612);
            this.gbSagyo.TabIndex = 13;
            this.gbSagyo.TabStop = false;
            this.gbSagyo.Text = "作業：";
            // 
            // panelDown
            // 
            this.panelDown.Controls.Add(this.buttonImport);
            this.panelDown.Controls.Add(this.buttonHusen);
            this.panelDown.Controls.Add(this.buttonHenreiPrint);
            this.panelDown.Controls.Add(this.buttonGaibu);
            this.panelDown.Controls.Add(this.buttonDataExport);
            this.panelDown.Controls.Add(this.buttonOther);
            this.panelDown.Controls.Add(this.buttonShokai);
            this.panelDown.Controls.Add(this.buttonAidSearch);
            this.panelDown.Location = new System.Drawing.Point(7, 321);
            this.panelDown.Name = "panelDown";
            this.panelDown.Size = new System.Drawing.Size(292, 287);
            this.panelDown.TabIndex = 13;
            this.panelDown.Visible = false;
            // 
            // buttonHenreiPrint
            // 
            this.buttonHenreiPrint.Location = new System.Drawing.Point(149, 138);
            this.buttonHenreiPrint.Margin = new System.Windows.Forms.Padding(4);
            this.buttonHenreiPrint.Name = "buttonHenreiPrint";
            this.buttonHenreiPrint.Size = new System.Drawing.Size(130, 64);
            this.buttonHenreiPrint.TabIndex = 10;
            this.buttonHenreiPrint.Text = "返戻付箋";
            this.buttonHenreiPrint.UseVisualStyleBackColor = true;
            this.buttonHenreiPrint.Click += new System.EventHandler(this.buttonHenreiPrint_Click);
            // 
            // buttonGaibu
            // 
            this.buttonGaibu.Location = new System.Drawing.Point(8, 138);
            this.buttonGaibu.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGaibu.Name = "buttonGaibu";
            this.buttonGaibu.Size = new System.Drawing.Size(130, 64);
            this.buttonGaibu.TabIndex = 7;
            this.buttonGaibu.Text = "外部業者用";
            this.buttonGaibu.UseVisualStyleBackColor = true;
            this.buttonGaibu.Click += new System.EventHandler(this.buttonGaibu_Click);
            // 
            // buttonDataExport
            // 
            this.buttonDataExport.Location = new System.Drawing.Point(8, 71);
            this.buttonDataExport.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDataExport.Name = "buttonDataExport";
            this.buttonDataExport.Size = new System.Drawing.Size(130, 64);
            this.buttonDataExport.TabIndex = 7;
            this.buttonDataExport.Text = "データ出力";
            this.buttonDataExport.UseVisualStyleBackColor = true;
            this.buttonDataExport.Click += new System.EventHandler(this.buttonDataExport_Click);
            // 
            // buttonOther
            // 
            this.buttonOther.Location = new System.Drawing.Point(149, 206);
            this.buttonOther.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOther.Name = "buttonOther";
            this.buttonOther.Size = new System.Drawing.Size(130, 64);
            this.buttonOther.TabIndex = 11;
            this.buttonOther.Text = "その他";
            this.buttonOther.UseVisualStyleBackColor = true;
            this.buttonOther.Click += new System.EventHandler(this.buttonOther_Click);
            // 
            // buttonShokai
            // 
            this.buttonShokai.Location = new System.Drawing.Point(149, 71);
            this.buttonShokai.Margin = new System.Windows.Forms.Padding(4);
            this.buttonShokai.Name = "buttonShokai";
            this.buttonShokai.Size = new System.Drawing.Size(130, 64);
            this.buttonShokai.TabIndex = 8;
            this.buttonShokai.Text = "照会";
            this.buttonShokai.UseVisualStyleBackColor = true;
            this.buttonShokai.Click += new System.EventHandler(this.buttonShokai_Click);
            // 
            // buttonAidSearch
            // 
            this.buttonAidSearch.Location = new System.Drawing.Point(8, 206);
            this.buttonAidSearch.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAidSearch.Name = "buttonAidSearch";
            this.buttonAidSearch.Size = new System.Drawing.Size(130, 64);
            this.buttonAidSearch.TabIndex = 9;
            this.buttonAidSearch.Text = "AID検索";
            this.buttonAidSearch.UseVisualStyleBackColor = true;
            this.buttonAidSearch.Click += new System.EventHandler(this.buttonAidSearch_Click);
            // 
            // panelUpper
            // 
            this.panelUpper.Controls.Add(this.buttonScan);
            this.panelUpper.Controls.Add(this.buttonInspectCheck);
            this.panelUpper.Controls.Add(this.buttonListInput);
            this.panelUpper.Controls.Add(this.buttonQuery);
            this.panelUpper.Controls.Add(this.buttonMatching);
            this.panelUpper.Location = new System.Drawing.Point(7, 24);
            this.panelUpper.Name = "panelUpper";
            this.panelUpper.Size = new System.Drawing.Size(292, 216);
            this.panelUpper.TabIndex = 12;
            this.panelUpper.Visible = false;
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(8, 5);
            this.buttonScan.Margin = new System.Windows.Forms.Padding(4);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(130, 64);
            this.buttonScan.TabIndex = 0;
            this.buttonScan.Text = "画像管理";
            this.buttonScan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // buttonListInput
            // 
            this.buttonListInput.Location = new System.Drawing.Point(8, 73);
            this.buttonListInput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonListInput.Name = "buttonListInput";
            this.buttonListInput.Size = new System.Drawing.Size(130, 64);
            this.buttonListInput.TabIndex = 3;
            this.buttonListInput.Text = "リスト抽出入力";
            this.buttonListInput.UseVisualStyleBackColor = true;
            this.buttonListInput.Click += new System.EventHandler(this.buttonListInput_Click);
            // 
            // buttonQuery
            // 
            this.buttonQuery.Location = new System.Drawing.Point(149, 73);
            this.buttonQuery.Margin = new System.Windows.Forms.Padding(4);
            this.buttonQuery.Name = "buttonQuery";
            this.buttonQuery.Size = new System.Drawing.Size(130, 64);
            this.buttonQuery.TabIndex = 2;
            this.buttonQuery.Text = "リスト作成";
            this.buttonQuery.UseVisualStyleBackColor = true;
            this.buttonQuery.Click += new System.EventHandler(this.buttonQuery_Click);
            // 
            // buttonMatching
            // 
            this.buttonMatching.Location = new System.Drawing.Point(149, 5);
            this.buttonMatching.Margin = new System.Windows.Forms.Padding(4);
            this.buttonMatching.Name = "buttonMatching";
            this.buttonMatching.Size = new System.Drawing.Size(130, 64);
            this.buttonMatching.TabIndex = 1;
            this.buttonMatching.Text = "マッチング\r\n確認";
            this.buttonMatching.UseVisualStyleBackColor = true;
            this.buttonMatching.Click += new System.EventHandler(this.buttonMatching_Click);
            // 
            // buttonOCRCheck
            // 
            this.buttonOCRCheck.Location = new System.Drawing.Point(15, 247);
            this.buttonOCRCheck.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOCRCheck.Name = "buttonOCRCheck";
            this.buttonOCRCheck.Size = new System.Drawing.Size(130, 64);
            this.buttonOCRCheck.TabIndex = 3;
            this.buttonOCRCheck.Text = "データ入力";
            this.buttonOCRCheck.UseVisualStyleBackColor = true;
            this.buttonOCRCheck.Click += new System.EventHandler(this.buttonOCRCheck_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(833, 239);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "→";
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(41, 616);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(46, 18);
            this.labelVersion.TabIndex = 16;
            this.labelVersion.Text = "label5";
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Checked = true;
            this.radioButtonAll.Location = new System.Drawing.Point(44, 54);
            this.radioButtonAll.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(63, 22);
            this.radioButtonAll.TabIndex = 1;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "すべて";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 13);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "保険者タイプ：";
            // 
            // radioButtonKokuho
            // 
            this.radioButtonKokuho.AutoSize = true;
            this.radioButtonKokuho.Location = new System.Drawing.Point(44, 86);
            this.radioButtonKokuho.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonKokuho.Name = "radioButtonKokuho";
            this.radioButtonKokuho.Size = new System.Drawing.Size(116, 22);
            this.radioButtonKokuho.TabIndex = 2;
            this.radioButtonKokuho.Text = "国民健康保険";
            this.radioButtonKokuho.UseVisualStyleBackColor = true;
            this.radioButtonKokuho.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonKoiki
            // 
            this.radioButtonKoiki.AutoSize = true;
            this.radioButtonKoiki.Location = new System.Drawing.Point(44, 118);
            this.radioButtonKoiki.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonKoiki.Name = "radioButtonKoiki";
            this.radioButtonKoiki.Size = new System.Drawing.Size(101, 22);
            this.radioButtonKoiki.TabIndex = 3;
            this.radioButtonKoiki.Text = "後期高齢者";
            this.radioButtonKoiki.UseVisualStyleBackColor = true;
            this.radioButtonKoiki.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonKenpo
            // 
            this.radioButtonKenpo.AutoSize = true;
            this.radioButtonKenpo.Location = new System.Drawing.Point(44, 150);
            this.radioButtonKenpo.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonKenpo.Name = "radioButtonKenpo";
            this.radioButtonKenpo.Size = new System.Drawing.Size(116, 22);
            this.radioButtonKenpo.TabIndex = 4;
            this.radioButtonKenpo.Text = "健康保険組合";
            this.radioButtonKenpo.UseVisualStyleBackColor = true;
            this.radioButtonKenpo.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonGakko
            // 
            this.radioButtonGakko.AutoSize = true;
            this.radioButtonGakko.Location = new System.Drawing.Point(44, 182);
            this.radioButtonGakko.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonGakko.Name = "radioButtonGakko";
            this.radioButtonGakko.Size = new System.Drawing.Size(86, 22);
            this.radioButtonGakko.TabIndex = 5;
            this.radioButtonGakko.Text = "学校共済";
            this.radioButtonGakko.UseVisualStyleBackColor = true;
            this.radioButtonGakko.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonOther
            // 
            this.radioButtonOther.AutoSize = true;
            this.radioButtonOther.Location = new System.Drawing.Point(44, 214);
            this.radioButtonOther.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonOther.Name = "radioButtonOther";
            this.radioButtonOther.Size = new System.Drawing.Size(64, 22);
            this.radioButtonOther.TabIndex = 6;
            this.radioButtonOther.Text = "その他";
            this.radioButtonOther.UseVisualStyleBackColor = true;
            this.radioButtonOther.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // buttonVersion
            // 
            this.buttonVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonVersion.Location = new System.Drawing.Point(269, 647);
            this.buttonVersion.Margin = new System.Windows.Forms.Padding(4);
            this.buttonVersion.Name = "buttonVersion";
            this.buttonVersion.Size = new System.Drawing.Size(145, 35);
            this.buttonVersion.TabIndex = 11;
            this.buttonVersion.Text = "バージョン管理";
            this.buttonVersion.UseVisualStyleBackColor = true;
            this.buttonVersion.Click += new System.EventHandler(this.buttonVersion_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(44, 555);
            this.buttonTest.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(101, 35);
            this.buttonTest.TabIndex = 11;
            this.buttonTest.Text = "機能テスト用";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Visible = false;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // checkBoxAllVisible
            // 
            this.checkBoxAllVisible.AutoSize = true;
            this.checkBoxAllVisible.Location = new System.Drawing.Point(44, 445);
            this.checkBoxAllVisible.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxAllVisible.Name = "checkBoxAllVisible";
            this.checkBoxAllVisible.Size = new System.Drawing.Size(87, 22);
            this.checkBoxAllVisible.TabIndex = 17;
            this.checkBoxAllVisible.Text = "無効表示";
            this.checkBoxAllVisible.UseVisualStyleBackColor = true;
            this.checkBoxAllVisible.Visible = false;
            this.checkBoxAllVisible.CheckedChanged += new System.EventHandler(this.checkBoxAllVisible_CheckedChanged);
            // 
            // lblconn
            // 
            this.lblconn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblconn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblconn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblconn.Location = new System.Drawing.Point(437, 630);
            this.lblconn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblconn.Name = "lblconn";
            this.lblconn.Size = new System.Drawing.Size(732, 52);
            this.lblconn.TabIndex = 18;
            this.lblconn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblconn.Visible = false;
            // 
            // checkBoxPrevious
            // 
            this.checkBoxPrevious.AutoSize = true;
            this.checkBoxPrevious.Location = new System.Drawing.Point(638, 582);
            this.checkBoxPrevious.Name = "checkBoxPrevious";
            this.checkBoxPrevious.Size = new System.Drawing.Size(106, 22);
            this.checkBoxPrevious.TabIndex = 19;
            this.checkBoxPrevious.Text = "旧データ表示";
            this.checkBoxPrevious.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 691);
            this.Controls.Add(this.checkBoxPrevious);
            this.Controls.Add(this.lblconn);
            this.Controls.Add(this.checkBoxAllVisible);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.buttonVersion);
            this.Controls.Add(this.radioButtonOther);
            this.Controls.Add(this.radioButtonGakko);
            this.Controls.Add(this.radioButtonKenpo);
            this.Controls.Add(this.radioButtonKoiki);
            this.Controls.Add(this.radioButtonKokuho);
            this.Controls.Add(this.radioButtonAll);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbSagyo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridViewMonth);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.listBoxInsurer);
            this.Controls.Add(this.buttonExit);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mejor メイン画面";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonth)).EndInit();
            this.gbSagyo.ResumeLayout(false);
            this.panelDown.ResumeLayout(false);
            this.panelUpper.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonInspectCheck;
        private System.Windows.Forms.ListBox listBoxInsurer;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewMonth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonHusen;
        private System.Windows.Forms.GroupBox gbSagyo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.Button buttonOCRCheck;
        private System.Windows.Forms.Button buttonDataExport;
        private System.Windows.Forms.Button buttonQuery;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Button buttonShokai;
        private System.Windows.Forms.Button buttonMatching;
        private System.Windows.Forms.Button buttonAidSearch;
        private System.Windows.Forms.Button buttonOther;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButtonKokuho;
        private System.Windows.Forms.RadioButton radioButtonKoiki;
        private System.Windows.Forms.RadioButton radioButtonKenpo;
        private System.Windows.Forms.RadioButton radioButtonGakko;
        private System.Windows.Forms.RadioButton radioButtonOther;
        private System.Windows.Forms.Button buttonVersion;
        private System.Windows.Forms.Button buttonHenreiPrint;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.CheckBox checkBoxAllVisible;
        public System.Windows.Forms.Label lblconn;
        private System.Windows.Forms.Button buttonListInput;
        private System.Windows.Forms.Panel panelDown;
        private System.Windows.Forms.Panel panelUpper;
        private System.Windows.Forms.Button buttonGaibu;
        public System.Windows.Forms.CheckBox checkBoxPrevious;
    }
}

