﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Mejor
{
    public class BorderCheckBox : CheckBox
    {
        public BorderCheckBox()
        {
            this.Paint += BorderCheckBox_Paint;
            this.Enter += BorderCheckBox_Enter;
            this.Leave += BorderCheckBox_Leave;
            this.KeyPress += BorderCheckBox_KeyPress;
            this.EnabledChanged += BorderCheckBox_EnabledChanged;
            this.Padding = new Padding(7, 3, 0, 0);
            this.BackColor = SystemColors.Info;
        }

        private void BorderCheckBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ('0' <= e.KeyChar && e.KeyChar <= '9') Checked = !Checked;
        }

        private void BorderCheckBox_Leave(object sender, EventArgs e)
        {
            if (this.BackColor == Color.LightCyan) this.BackColor = SystemColors.Info;
            else if (this.BackColor == Color.HotPink) this.BackColor = Color.Pink;
        }

        private void BorderCheckBox_Enter(object sender, EventArgs e)
        {
            if (this.BackColor == SystemColors.Info) this.BackColor = Color.LightCyan;
            else if (this.BackColor == Color.Pink) this.BackColor = Color.HotPink;
        }

        private void BorderCheckBox_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            g.DrawRectangle(Pens.DarkGray, 0, 0, Width - 1, Height - 1);
        }

        private void BorderCheckBox_EnabledChanged(object sender, EventArgs e)
        {
            this.BackColor = Enabled ? 
                Focused ? this.BackColor = Color.LightCyan : SystemColors.Info :
                SystemColors.Menu;
        }
    }
}
