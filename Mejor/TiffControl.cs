﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

public partial class TiffControl : UserControl
{
    public TiffControl()
    {
        InitializeComponent();
        Disposed += TiffControl_Disposed;
    }

    private string imageFileName = "";
    private Image currentImage = null;
    private MemoryStream currentStream = null;
    private int nowPageIndex = 0;

    public void SetBlank()
    {
        //imageFileName = string.Empty;
        pictureBox.Image = null;
    }

    public void AllDispose()
    {
        if (pictureBox.Image != null) pictureBox.Image.Dispose();
        if (currentImage != null) currentImage.Dispose();
        if (currentStream != null) currentStream.Dispose();

    }

    public void SetTiff(string fileName)
    {
        imageFileName = fileName;
        System.Threading.Tasks.Task.Factory.StartNew(() =>
        {
            var preMs = GetMemStream(fileName);
            if (preMs == null || imageFileName != fileName)
            {
                SetBlank();
                return;
            }

            var preImg = Image.FromStream(preMs);

            int pageCount = 0;
            try
            {
                var gfd = new FrameDimension(preImg.FrameDimensionsList[0]);
                pageCount = preImg.GetFrameCount(gfd);
            }
            catch
            {
                preMs.Dispose();
                preImg.Dispose();
                SetBlank();
                return;
            }

            //次のファイルを読み込みにかかっていないかチェック
            if (imageFileName != fileName)
            {
                preMs.Dispose();
                preImg.Dispose();
                SetBlank();
                return;
            }

            Invoke(new Action(() =>
            {
                if (pageCount == 1)
                {
                    buttonTiffPrev.Visible = false;
                    buttonTiffNext.Visible = false;
                }
                else
                {
                    buttonTiffPrev.Visible = true;
                    buttonTiffNext.Visible = true;
                }
            }));

            if (currentImage != null) currentImage.Dispose();
            currentImage = preImg;
            if (currentStream != null) currentStream.Dispose();
            currentStream = preMs;

            pictureBox.Image = currentImage;
            nowPageIndex = 0;
        });
    }

    /// <summary>
    /// 複数ページのTiffを切り替えます
    /// </summary>
    /// <param name="pageIndex"></param>
    private void selectPage(int pageIndex)
    {
        if (currentImage == null | currentStream == null) return;
        var gfd = new FrameDimension(currentImage.FrameDimensionsList[0]);
        int pageCount = currentImage.GetFrameCount(gfd);

        if (pageCount > pageIndex && pageIndex >= 0)
        {
            currentImage.SelectActiveFrame(gfd, pageIndex);
            pictureBox.Refresh();
            nowPageIndex = pageIndex;
        }
    }

    /// <summary>
    /// ファイルをメモリー上に転送します
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private MemoryStream GetMemStream(string fileName)
    {
        if (!System.IO.File.Exists(fileName)) return null;
        var readms = new MemoryStream();

        using (var tifFS = new FileStream(fileName, FileMode.Open, FileAccess.Read))
        {
            try
            {
                tifFS.CopyTo(readms);
                return readms;
            }
            catch
            {
                readms.Dispose();
                return null;
            }
        }
    }

    void TiffControl_Disposed(object sender, EventArgs e)
    {
        if (pictureBox.Image != null) pictureBox.Image.Dispose();
        if (currentImage != null) currentImage.Dispose();
        if (currentStream != null) currentStream.Dispose();
    }

    public class InputWindowSize
    {
        public static int SplitWidth;
        public static int SplitHeight;

        public Point PictureBoxPos;
        public Size PictureBoxSize;
    }

    /// <summary> System.Windows.Forms.PictureBox に表示するイメージの再描画</summary>
    public void UpdateView()
    {
        SetTiff(imageFileName);
    }


    public void InitPictureBoxLocation()
    {
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    /// <summary> 画像のサイズ・スクロール位置の更新 </summary>
    /// <param name="pictureSize">画像のサイズ</param>
    /// <param name="scroll">スクロール位置</param>
    public void SetPictureBoxLocation(Size pictureSize, Point scroll)
    {
        if (pictureSize.Height <= this.Height && pictureSize.Width <= this.Width)
        {
            // 画像のサイズ ＜ 枠のサイズの時 → 完全に枠の大きさに合わせる！
            this.AutoScrollPosition = new Point(0, 0);
            pictureBox.Size = this.Size;
            pictureBox.Dock = DockStyle.Fill;
        }
        else
        {
            pictureBox.Dock = DockStyle.None;
            pictureBox.Size = pictureSize;
            this.AutoScrollPosition = scroll;
        }
    }

    public void SetPictureBoxFill()
    {
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    public void PictureDispose()
    {
        pictureBox.Image.Dispose();
        pictureBox.Image = null;
    }

    public void GetPictureBoxLocation(out Size pictureSize, out Point scroll)
    {
        if (pictureBox.Dock == DockStyle.Fill)
        {
            pictureSize = this.Size;
            scroll = new Point(0, 0);
        }
        else
        {
            pictureSize = pictureBox.Size;
            scroll = new Point(-this.AutoScrollPosition.X, -this.AutoScrollPosition.Y);
        }
    }

    private void pictureBox_MouseClick(object sender, MouseEventArgs e)
    {
        if (System.Math.Abs(dx) > 10 && System.Math.Abs(dy) > 10) return;
        
        if (e.Button == System.Windows.Forms.MouseButtons.Left || e.Button == System.Windows.Forms.MouseButtons.Right)
        {
            // 画像サイズ ・ スクロール位置 の取得
            Size pictureSize;       // 画像サイズ
            Point scroll;           // スクロール位置
            GetPictureBoxLocation(out pictureSize, out scroll);

            // 拡大率 （左クリック 1.3倍ズーム、 右 1/1.3 縮小）
            double rate = (e.Button == System.Windows.Forms.MouseButtons.Left ? 1.5 : 1 / 1.5);

            // 画像サイズ ・ スクロール位置 の更新
            pictureSize.Width = (int)(rate * pictureSize.Width);
            pictureSize.Height = (int)(rate * pictureSize.Height);
            scroll.X = (int)(e.Location.X);
            scroll.Y = (int)(e.Location.Y);
            SetPictureBoxLocation(pictureSize, scroll);
        }
    }

    private bool is_mouse_down_ = false;
    private Point origin_;
    int dx = 0;
    int dy = 0;

    private void pictureBox_MouseDown(object sender, MouseEventArgs e)
    {
        origin_ = pictureBox.Parent.PointToScreen(e.Location);
        Cursor.Current = Cursors.Hand; // マウスカーソルの見た目を変更
        is_mouse_down_ = true;
        
        dx = 0;
        dy = 0;
    }

    private void pictureBox_MouseMove(object sender, MouseEventArgs e)
    {
        if (is_mouse_down_)
        {
            var current = pictureBox.PointToScreen(e.Location);
            int x = current.X - origin_.X;
            int y = current.Y - origin_.Y;
            //panel1.AutoScrollPosition = new Point(-x, -y);
            this.AutoScrollPosition = new Point(-x, -y);
            dx = x;
            dy = y;
        }
    }

    private void pictureBox_MouseUp(object sender, MouseEventArgs e)
    {
        is_mouse_down_ = false;
        Cursor.Current = Cursors.Default;
    }

    private void buttonTiffPrev_Click(object sender, EventArgs e)
    {
        selectPage(nowPageIndex - 1);
    }

    private void buttonTiffNext_Click(object sender, EventArgs e)
    {
        selectPage(nowPageIndex + 1);
    }

}
