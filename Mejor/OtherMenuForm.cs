﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class OtherMenuForm : Form
    {
        int cym;
        List<PostalCode> selectedPostalCodeHistory;

        public OtherMenuForm(int cym, List<PostalCode> selectedPostalCodeHistory)
        {
            InitializeComponent();

            //20210312160023 furukawa st ////////////////////////
            //フォーム色
            
            CommonTool.setFormColor(this);
            //20210312160023 furukawa ed ////////////////////////

            this.cym = cym;
            this.selectedPostalCodeHistory = selectedPostalCodeHistory;

            this.Text += "  " + Insurer.CurrrentInsurer.InsurerName + " " + cym.ToString("0000/00");
            buttonOcr.Text = $"OCR過去データ突合\r\n({cym.ToString("0000/00")})";
            //buttonOcr.Enabled = Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済;

            buttonDistance.Enabled = Insurer.CurrrentInsurer.EnumInsID == InsurerID.OSAKA_KOIKI;


            //20190524114143 furukawa st ////////////////////////
            //兵庫広域も追加

            buttonOryoFlag.Enabled = 
                Insurer.CurrrentInsurer.EnumInsID == InsurerID.OSAKA_KOIKI ||
                Insurer.CurrrentInsurer.EnumInsID == InsurerID.HYOGO_KOIKI;


            //      buttonOryoFlag.Enabled = Insurer.CurrrentInsurer.EnumInsID == InsurerID.OSAKA_KOIKI;
            //20190524114143 furukawa ed ////////////////////////


            //20210210113819 furukawa st ////////////////////////
            //button7が大阪広域のみ使用可能になっていたが、処理が書かれていないので削除
            //      button7.Enabled = Insurer.CurrrentInsurer.EnumInsID == InsurerID.OSAKA_KOIKI;
            //20210210113819 furukawa ed ////////////////////////


            //20201112235224 furukawa st////////////////////////
            //アーカイブ用ボタン

            button8.Enabled = true;
            //20201112235224 furukawa ed ////////////////////////

            //20210312155940 furukawa st ////////////////////////
            //学校共済分析用ボタン            
            button9.Enabled = Insurer.CurrrentInsurer.InsurerType==INSURER_TYPE.学校共済;
            //20210312155940 furukawa ed ////////////////////////


            buttonSFMng.Enabled = User.CurrentUser.Developer;
        }

        /**
         * 住所検索
         */
        private void buttonPostalCode_Click(object sender, EventArgs e)
        {
            try
            {
                Visible = false;
                using (var f = new PostalCodeForm(null, selectedPostalCodeHistory))
                {
                    f.ShowDialog();
                    if (f.SelectedPostalCode != null)
                    {
                        selectedPostalCodeHistory.Add(f.SelectedPostalCode);
                    }
                }
            }
            finally
            {
                Visible = true;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Visible = false;
                using (var f = new TotalCalcForm()) f.ShowDialog();
            }
            finally
            {
                Visible = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var f = new MemoErrorListForm(cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonOcr_Click(object sender, EventArgs e)
        {
            Ocr.OcrHelper.Test(cym);
        }

        private void buttonDistance_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show($"{cym / 100}年{cym % 100}月処理分の往療距離一覧をCSVで出力します。", "",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res != DialogResult.OK) return;
            OsakaKoiki.CalcDistance.AllAppCalcDistanceCsv(cym);
        }

        private void buttonOryoFlag_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show($"{cym / 100}年{cym % 100}月処理分の往療点検フラグを追加します。" +
                $"すべての入力が完了していることを確認後にこの作業を実行して下さい。\r\n\r\n" +
                $"処理を開始してよろしいですか？", "往療点検フラグ追加確認",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res != DialogResult.OK) return;



            //20190524121901 furukawa st ////////////////////////
            //兵庫広域も追加

            bool flg = false;
            switch (Insurer.CurrrentInsurer.EnumInsID)
            {
                case InsurerID.OSAKA_KOIKI:

                    flg = OsakaKoiki.CalcDistance.AllAppCalcDistance(cym);                 
                    break;

                case InsurerID.HYOGO_KOIKI:
                    flg = HyogoKoiki.CalcDistance.AllAppCalcDistance(cym);                   
                    break;

            }


            if (flg)
            {
                MessageBox.Show("往療点検フラグの追加が完了しました");
            }
            else
            {
                MessageBox.Show("往療点検フラグの追加に失敗しました");
            }
        
                //if (OsakaKoiki.CalcDistance.AllAppCalcDistance(cym))
                //{
                //    MessageBox.Show("往療点検フラグの追加が完了しました");
                //}
                //else
                //{
                //    MessageBox.Show("往療点検フラグの追加に失敗しました");
                //}

            //20190524121901 furukawa ed ////////////////////////


        }

        private void button8_Click(object sender, EventArgs e)
        {
            Archive.CymListForm frm = new Archive.CymListForm();
            frm.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //20210210114029 furukawa st ////////////////////////
            //入力された請求金額と計算結果で差異がないかチェックする
            
            clsChargeCheck cls = new clsChargeCheck();
            cls.chkMain(cym);
            //20210210114029 furukawa ed ////////////////////////
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ZenkokuGakko.ExprotForAnalyze frm = new ZenkokuGakko.ExprotForAnalyze(cym);
            frm.ShowDialog();
        }

        private void buttonSFMng_Click(object sender, EventArgs e)
        {  
            StatusFlagsMng f = new StatusFlagsMng(int.Parse(cym.ToString()));
            f.ShowDialog();
            
        }
    }
}
