﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.HyogoKoiki
{
    class Clinic
    {
        [DB.DbAttribute.PrimaryKey]
        public string ClinicNum { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public double Ido { get; set; }
        public double Keido { get; set; }

        private int branchNo = 0;

        public static bool Import()
        {
            string fileName;
            
            using (var f = new System.Windows.Forms.OpenFileDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;
                fileName = f.FileName;
            }
            
            //var csv = CommonTool.CsvImportShiftJis(fileName);
            var csv = CommonTool.CsvImportMultiCode(fileName);
            var dic = new Dictionary<string, Clinic>();

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(csv.Count);

                for (int i = 1; i < csv.Count; i++)
                {
                    wf.InvokeValue++;
                    var line = csv[i];
                    if (line.Length < 4) continue;

                    var c = new Clinic();
                    c.ClinicNum = line[0].Trim();//line[0].Trim() + line[1].Trim() + line[2].Trim() + line[3].Trim();
                    //int.TryParse(line[4].Trim(), out c.branchNo);
                    c.Name = line[1].Trim(); //line[8].Trim();
                    c.Zip = line[2].Trim(); //line[9].Trim();
                    c.Address = line[3].Trim();// line[10].Trim();                    

                    var g = GeoData.GetGeoData(c.Address);
                    if (g != null)
                    {
                        c.Ido = g.Ido;
                        c.Keido = g.Keido;
                    }
                    else
                    {
                        //wf.LogPrint(c.Address);
                    }

                    if (dic.ContainsKey(c.ClinicNum))
                    {
                        if (dic[c.ClinicNum].branchNo > c.branchNo) continue;
                        dic[c.ClinicNum] = c;
                    }
                    else
                    {
                        dic.Add(c.ClinicNum, c);
                    }
                }
                wf.LogSave("not geodata addresses.txt");
            }

            using (var tran = DB.Main.CreateTransaction())
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(dic.Count);
                foreach (var item in dic)
                {
                    wf.InvokeValue++;
                    if (!DB.Main.Insert(item.Value, tran)) return false;
                }
                tran.Commit();
            }

            return true;
        }

        public static Clinic GetClinic(string clinicNum) =>
            DB.Main.Select<Clinic>(new { clinicNum }).FirstOrDefault();
    }
}
