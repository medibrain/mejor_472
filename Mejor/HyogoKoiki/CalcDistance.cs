﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.HyogoKoiki
{
    public enum DistanceRank
    {
        Null = 0,
        Rank2Under = 1,
        Rank2to4 = 2,
        Rank4to6 = 3,
        Rank6to8 = 4,
        Rank8to10 = 5,
        Rank10to12 = 6,
        Rank12to14 = 7,
        Rank14to16 = 8,
        Rank16Over = 9,
    }

    class CalcDistance
    {
        public static DistanceRank GetDistanceRank(double dist)
        {
            if (dist <= 2) return DistanceRank.Rank2Under;
            if (dist > 2 && dist <= 4) return DistanceRank.Rank2to4;
            if (dist > 4 && dist <= 6) return DistanceRank.Rank4to6;
            if (dist > 6 && dist <= 8) return DistanceRank.Rank6to8;
            if (dist > 8 && dist <= 10) return DistanceRank.Rank8to10;
            if (dist > 10 && dist <= 12) return DistanceRank.Rank10to12;
            if (dist > 12 && dist <= 14) return DistanceRank.Rank12to14;
            if (dist > 14 && dist <= 16) return DistanceRank.Rank14to16;
            return DistanceRank.Rank16Over;
        }

        public static DistanceRank GetDistanceRank(App app)
        {
            var dist = 2 + app.VisitAdd / 1000d;
            if (dist <= 2) return DistanceRank.Rank2Under;
            if (dist > 2 && dist <= 4) return DistanceRank.Rank2to4;
            if (dist > 4 && dist <= 6) return DistanceRank.Rank4to6;
            if (dist > 6 && dist <= 8) return DistanceRank.Rank6to8;
            if (dist > 8 && dist <= 10) return DistanceRank.Rank8to10;
            if (dist > 10 && dist <= 12) return DistanceRank.Rank10to12;
            if (dist > 12 && dist <= 14) return DistanceRank.Rank12to14;
            if (dist > 14 && dist <= 16) return DistanceRank.Rank14to16;
            return DistanceRank.Rank16Over;
        }

        public static double GetCalcDistance(App app)
        {
            var c = Clinic.GetClinic(app.ClinicNum);
            if (c == null) return 0;
            if (c.Ido == 0) return 0;

            var g = GeoData.GetGeoData(app.HihoAdd);
            if (g == null) return 0;

            return g.GetDistance(c.Ido, c.Keido);
        }


        public static DistanceRank GetCalcDistanceRank(App app)
        {
            var c = Clinic.GetClinic(app.ClinicNum);
            if (c == null) return DistanceRank.Null;
            if (c.Ido == 0) return DistanceRank.Null;

            var g = GeoData.GetGeoData(app.HihoAdd);
            if (g == null) return DistanceRank.Null;

            var dist = g.GetDistance(c.Ido, c.Keido);
            return GetDistanceRank(dist);
        }

        public static string CheckAppDistance(App app)
        {
            string clinic;
            string patient;
            string input;
            string calc;

            var c = Clinic.GetClinic(app.ClinicNum);
            if (c == null) clinic = "医療機関情報が取得できませんでした。";
            else if (c.Ido == 0) clinic = "医療機関の位置情報が取得できませんでした。";
            else clinic = $"緯度:{c.Ido}  経度{c.Keido}";

            var g = GeoData.GetGeoData(app.HihoAdd);
            if (g == null) patient = "被保険者の位置情報が取得できませんでした。";
            else patient = $"緯度:{g.Ido}  経度{g.Keido}";

            var ir = GetDistanceRank(app);
            var cr = GetCalcDistanceRank(app);

            var inputDist = (app.Distance > 0 ? 2 : 0) + app.VisitAdd / 1000d;
            input = $"入力距離：{inputDist}　　ランク：{ir}";
            var calcDist = g?.GetDistance(c.Ido, c.Keido) ?? 0;
            calc = $"計算距離：{calcDist}　　ランク：{cr}";

            return "医療機関:" + clinic + "\r\n" +
                "被保険者:" + patient + "\r\n" +
                input + "\r\n" +
                calc;
        }

        /// <summary>
        /// 結果CSVのラインで出力します
        /// AID,医療機関番号,医療機関住所,受診者住所,
        /// 入力距離,入力往療料,入力加算料,入力ランク,計算距離,計算ランク,判定
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static string CheckAppDistanceToCsvLine(App app)
        {
            var ss = new string[11];
            var c = Clinic.GetClinic(app.ClinicNum);
            var g = GeoData.GetGeoData(app.HihoAdd);
            var calcDist = (c == null || g == null) ? 0 : g.GetDistance(c.Ido, c.Keido);
            var dist = app.VisitAdd / 1000d + 2;
            var ir = GetDistanceRank(dist);
            var cr = (c == null || c.Ido == 0 || g == null) ?
                DistanceRank.Null : GetDistanceRank(calcDist);

            ss[0] = app.Aid.ToString();
            ss[1] = c?.Name ?? string.Empty;
            ss[2] = c?.Address ?? string.Empty;
            ss[3] = app.HihoAdd;
            ss[4] = dist.ToString();
            ss[5] = app.VisitFee.ToString();
            ss[6] = app.VisitTimes.ToString();
            ss[7] = ir.ToString();
            ss[8] = c == null ? "医療機関不明" :
                c.Ido == 0 ? "医療機関座標不明" :
                g == null ? "受診者座標不明" :
                g.GetDistance(c.Ido, c.Keido).ToString();
            ss[9] = cr.ToString();
            ss[10] = (c == null || c.Ido == 0 || g == null) ? "判定不能" :
                cr < ir ? "要点検" : "適正";

            return string.Join(",", ss);
        }

        public static bool AllAppCalcDistanceCsv(int cym)
        {
            string fn;
            using (var f = new System.Windows.Forms.SaveFileDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    return false;
                fn = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var l = App.GetApps(cym);

                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    //ヘッダ
                    sw.WriteLine("AID,医療機関番号,医療機関住所,受診者住所,入力距離,入力往療料,入力加算料,入力ランク,計算距離,計算ランク,判定");
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        if (item.Distance == 0) continue;
                        sw.WriteLine(CalcDistance.CheckAppDistanceToCsvLine(item));
                    }
                }

                return true;
            }
        }


        public static bool AllAppCalcDistance(int cym)
        {
            int cntTrue = 0;
            int cntNotZero = 0;
            int cntIdo = 0;
            int cntGeo = 0;
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var l = App.GetApps(cym);

                wf.LogPrint("距離を計算し、往療点検フラグを立てています");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(l.Count);

                foreach (var app in l)
                {
                    wf.InvokeValue++;

                    if (app.Distance == 0) continue;
                    cntNotZero++;

                    var c = Clinic.GetClinic(app.ClinicNum);
                    //試験if (c == null || c.Ido == 0) continue;
                    cntIdo++;

                    var g = GeoData.GetGeoData(app.HihoAdd);
                    if (g == null) continue;
                    cntGeo++;

                    //2019-05-24比較するgeoのgeodataは犬を使って座標をリスト化したのかな？

                    var dist = app.VisitAdd / 1000d + 2;
                    //試験var calcDist = g.GetDistance(c.Ido, c.Keido);
                    var ir = GetDistanceRank(dist);
                    //試験var cr = GetDistanceRank(calcDist);

                    //試験if (cr < ir)
                    //試験{
                    app.StatusFlagSet(StatusFlag.往療点検対象);
                        //試験app.TaggedDatas.CalcDistance = calcDist;
                        if (!app.UpdateINQ()) return false;
                    //試験}
                }
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("距離：{0} 緯度:{1} 該当座標あり:{2}", cntNotZero, cntIdo, cntGeo);
                System.Windows.Forms.MessageBox.Show(sb.ToString());
                return true;
            }
        }
    }
}
