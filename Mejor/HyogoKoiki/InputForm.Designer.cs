﻿namespace Mejor.HyogoKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.lblかくす = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelOCR = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.pSejutu = new System.Windows.Forms.Panel();
            this.vbSejutuG = new Mejor.VerifyBox();
            this.lblSejutu = new System.Windows.Forms.Label();
            this.lblSejutuM = new System.Windows.Forms.Label();
            this.lblSejutuY = new System.Windows.Forms.Label();
            this.vbSejutuM = new Mejor.VerifyBox();
            this.vbSejutuY = new Mejor.VerifyBox();
            this.vcSejutu = new Mejor.VerifyCheckBox();
            this.verifyBoxBuisu = new Mejor.VerifyBox();
            this.lblBui = new System.Windows.Forms.Label();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.labelOryoKasan = new System.Windows.Forms.Label();
            this.labelOryoBasic = new System.Windows.Forms.Label();
            this.verifyBoxKasanCost = new Mejor.VerifyBox();
            this.verifyBoxOryoCost = new Mejor.VerifyBox();
            this.labelOuryo2 = new System.Windows.Forms.Label();
            this.labelOuryo = new System.Windows.Forms.Label();
            this.verifyBoxKasanKm = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.lblFirst = new System.Windows.Forms.Label();
            this.lblFirstY = new System.Windows.Forms.Label();
            this.lblFirstM = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxClinicNum = new Mejor.VerifyBox();
            this.lblReg = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.pSejutu.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(672, 736);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 70;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 742);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(0, 764);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(0, 764);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 737);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 737);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 737);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-83, 737);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(4);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(0, 764);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.lblかくす);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(63, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 764);
            this.panelRight.TabIndex = 2;
            // 
            // lblかくす
            // 
            this.lblかくす.Location = new System.Drawing.Point(120, 33);
            this.lblかくす.Name = "lblかくす";
            this.lblかくす.Size = new System.Drawing.Size(90, 12);
            this.lblかくす.TabIndex = 71;
            this.lblかくす.Text = " ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(7, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 96);
            this.label6.TabIndex = 21;
            this.label6.Text = "通常    : 空欄\r\n続紙    : \"--\"     \r\n不要    : \"++\"\r\n長期    : \"..\"\r\n施術同意書  : \"901\"\r\n施術同意書裏" +
    "：\"902\"\r\n施術報告書  : \"911\"\r\n状態記入書  : \"921\"\r\n";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 121);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(4);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 136);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 503);
            this.scrollPictureControl1.TabIndex = 20;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(576, 736);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 19;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 736);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(41, 13);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "label23";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(225, 22);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 1;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.pSejutu);
            this.panelTotal.Controls.Add(this.vcSejutu);
            this.panelTotal.Controls.Add(this.verifyBoxBuisu);
            this.panelTotal.Controls.Add(this.lblBui);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Controls.Add(this.labelOryoKasan);
            this.panelTotal.Controls.Add(this.labelOryoBasic);
            this.panelTotal.Controls.Add(this.verifyBoxKasanCost);
            this.panelTotal.Controls.Add(this.verifyBoxOryoCost);
            this.panelTotal.Controls.Add(this.labelOuryo2);
            this.panelTotal.Controls.Add(this.labelOuryo);
            this.panelTotal.Controls.Add(this.verifyBoxKasanKm);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstY);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstM);
            this.panelTotal.Controls.Add(this.lblFirst);
            this.panelTotal.Controls.Add(this.lblFirstY);
            this.panelTotal.Controls.Add(this.lblFirstM);
            this.panelTotal.Controls.Add(this.label15);
            this.panelTotal.Controls.Add(this.verifyBoxClinicNum);
            this.panelTotal.Controls.Add(this.lblReg);
            this.panelTotal.Location = new System.Drawing.Point(0, 633);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(850, 87);
            this.panelTotal.TabIndex = 20;
            // 
            // pSejutu
            // 
            this.pSejutu.Controls.Add(this.vbSejutuG);
            this.pSejutu.Controls.Add(this.lblSejutu);
            this.pSejutu.Controls.Add(this.lblSejutuM);
            this.pSejutu.Controls.Add(this.lblSejutuY);
            this.pSejutu.Controls.Add(this.vbSejutuM);
            this.pSejutu.Controls.Add(this.vbSejutuY);
            this.pSejutu.Enabled = false;
            this.pSejutu.Location = new System.Drawing.Point(281, 52);
            this.pSejutu.Name = "pSejutu";
            this.pSejutu.Size = new System.Drawing.Size(262, 33);
            this.pSejutu.TabIndex = 72;
            this.pSejutu.Visible = false;
            // 
            // vbSejutuG
            // 
            this.vbSejutuG.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuG.Location = new System.Drawing.Point(74, 3);
            this.vbSejutuG.MaxLength = 2;
            this.vbSejutuG.Name = "vbSejutuG";
            this.vbSejutuG.NewLine = false;
            this.vbSejutuG.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuG.TabIndex = 39;
            this.vbSejutuG.TextV = "";
            // 
            // lblSejutu
            // 
            this.lblSejutu.AutoSize = true;
            this.lblSejutu.Enabled = false;
            this.lblSejutu.Location = new System.Drawing.Point(4, 13);
            this.lblSejutu.Name = "lblSejutu";
            this.lblSejutu.Size = new System.Drawing.Size(55, 13);
            this.lblSejutu.TabIndex = 39;
            this.lblSejutu.Text = "前回支給";
            // 
            // lblSejutuM
            // 
            this.lblSejutuM.AutoSize = true;
            this.lblSejutuM.Enabled = false;
            this.lblSejutuM.Location = new System.Drawing.Point(188, 14);
            this.lblSejutuM.Name = "lblSejutuM";
            this.lblSejutuM.Size = new System.Drawing.Size(19, 13);
            this.lblSejutuM.TabIndex = 41;
            this.lblSejutuM.Text = "月";
            // 
            // lblSejutuY
            // 
            this.lblSejutuY.AutoSize = true;
            this.lblSejutuY.Enabled = false;
            this.lblSejutuY.Location = new System.Drawing.Point(134, 14);
            this.lblSejutuY.Name = "lblSejutuY";
            this.lblSejutuY.Size = new System.Drawing.Size(19, 13);
            this.lblSejutuY.TabIndex = 40;
            this.lblSejutuY.Text = "年";
            // 
            // vbSejutuM
            // 
            this.vbSejutuM.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuM.Location = new System.Drawing.Point(157, 3);
            this.vbSejutuM.MaxLength = 2;
            this.vbSejutuM.Name = "vbSejutuM";
            this.vbSejutuM.NewLine = false;
            this.vbSejutuM.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuM.TabIndex = 42;
            this.vbSejutuM.TextV = "";
            // 
            // vbSejutuY
            // 
            this.vbSejutuY.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuY.Location = new System.Drawing.Point(106, 3);
            this.vbSejutuY.MaxLength = 2;
            this.vbSejutuY.Name = "vbSejutuY";
            this.vbSejutuY.NewLine = false;
            this.vbSejutuY.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuY.TabIndex = 41;
            this.vbSejutuY.TextV = "";
            // 
            // vcSejutu
            // 
            this.vcSejutu.BackColor = System.Drawing.SystemColors.Info;
            this.vcSejutu.CheckedV = false;
            this.vcSejutu.Enabled = false;
            this.vcSejutu.Location = new System.Drawing.Point(178, 57);
            this.vcSejutu.Name = "vcSejutu";
            this.vcSejutu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.vcSejutu.Size = new System.Drawing.Size(95, 27);
            this.vcSejutu.TabIndex = 71;
            this.vcSejutu.Text = "交付料あり";
            this.vcSejutu.UseVisualStyleBackColor = false;
            // 
            // verifyBoxBuisu
            // 
            this.verifyBoxBuisu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuisu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuisu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuisu.Location = new System.Drawing.Point(74, 49);
            this.verifyBoxBuisu.Name = "verifyBoxBuisu";
            this.verifyBoxBuisu.NewLine = false;
            this.verifyBoxBuisu.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxBuisu.TabIndex = 62;
            this.verifyBoxBuisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBuisu.TextV = "";
            // 
            // lblBui
            // 
            this.lblBui.AutoSize = true;
            this.lblBui.Location = new System.Drawing.Point(27, 59);
            this.lblBui.Name = "lblBui";
            this.lblBui.Size = new System.Drawing.Size(43, 13);
            this.lblBui.TabIndex = 70;
            this.lblBui.Text = "部位数";
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(285, 18);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisitKasan.TabIndex = 58;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.CheckedChanged += new System.EventHandler(this.checkBoxVisitKasan_CheckedChanged);
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(178, 18);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisit.TabIndex = 57;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.CheckedChanged += new System.EventHandler(this.checkBoxVisit_CheckedChanged);
            // 
            // labelOryoKasan
            // 
            this.labelOryoKasan.AutoSize = true;
            this.labelOryoKasan.Location = new System.Drawing.Point(628, 18);
            this.labelOryoKasan.Name = "labelOryoKasan";
            this.labelOryoKasan.Size = new System.Drawing.Size(43, 26);
            this.labelOryoKasan.TabIndex = 69;
            this.labelOryoKasan.Text = "加算料\r\n　合計";
            // 
            // labelOryoBasic
            // 
            this.labelOryoBasic.AutoSize = true;
            this.labelOryoBasic.Location = new System.Drawing.Point(514, 18);
            this.labelOryoBasic.Name = "labelOryoBasic";
            this.labelOryoBasic.Size = new System.Drawing.Size(43, 26);
            this.labelOryoBasic.TabIndex = 68;
            this.labelOryoBasic.Text = "往療料\r\n　合計";
            // 
            // verifyBoxKasanCost
            // 
            this.verifyBoxKasanCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKasanCost.Enabled = false;
            this.verifyBoxKasanCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKasanCost.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxKasanCost.Location = new System.Drawing.Point(672, 21);
            this.verifyBoxKasanCost.Name = "verifyBoxKasanCost";
            this.verifyBoxKasanCost.NewLine = false;
            this.verifyBoxKasanCost.Size = new System.Drawing.Size(61, 23);
            this.verifyBoxKasanCost.TabIndex = 61;
            this.verifyBoxKasanCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKasanCost.TextV = "";
            // 
            // verifyBoxOryoCost
            // 
            this.verifyBoxOryoCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxOryoCost.Enabled = false;
            this.verifyBoxOryoCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxOryoCost.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxOryoCost.Location = new System.Drawing.Point(558, 21);
            this.verifyBoxOryoCost.Name = "verifyBoxOryoCost";
            this.verifyBoxOryoCost.NewLine = false;
            this.verifyBoxOryoCost.Size = new System.Drawing.Size(61, 23);
            this.verifyBoxOryoCost.TabIndex = 60;
            this.verifyBoxOryoCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxOryoCost.TextV = "";
            // 
            // labelOuryo2
            // 
            this.labelOuryo2.AutoSize = true;
            this.labelOuryo2.Location = new System.Drawing.Point(385, 31);
            this.labelOuryo2.Name = "labelOuryo2";
            this.labelOuryo2.Size = new System.Drawing.Size(31, 13);
            this.labelOuryo2.TabIndex = 66;
            this.labelOuryo2.Text = "加算";
            // 
            // labelOuryo
            // 
            this.labelOuryo.AutoSize = true;
            this.labelOuryo.Location = new System.Drawing.Point(477, 31);
            this.labelOuryo.Name = "labelOuryo";
            this.labelOuryo.Size = new System.Drawing.Size(21, 13);
            this.labelOuryo.TabIndex = 67;
            this.labelOuryo.Text = "km";
            // 
            // verifyBoxKasanKm
            // 
            this.verifyBoxKasanKm.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKasanKm.Enabled = false;
            this.verifyBoxKasanKm.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKasanKm.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxKasanKm.Location = new System.Drawing.Point(420, 21);
            this.verifyBoxKasanKm.Name = "verifyBoxKasanKm";
            this.verifyBoxKasanKm.NewLine = false;
            this.verifyBoxKasanKm.Size = new System.Drawing.Size(50, 23);
            this.verifyBoxKasanKm.TabIndex = 59;
            this.verifyBoxKasanKm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKasanKm.TextV = "";
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(67, 20);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 55;
            this.verifyBoxF1FirstY.TextV = "";
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(112, 20);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 56;
            this.verifyBoxF1FirstM.TextV = "";
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.Location = new System.Drawing.Point(20, 31);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(43, 13);
            this.lblFirst.TabIndex = 63;
            this.lblFirst.Text = "初検日";
            // 
            // lblFirstY
            // 
            this.lblFirstY.AutoSize = true;
            this.lblFirstY.Location = new System.Drawing.Point(96, 31);
            this.lblFirstY.Name = "lblFirstY";
            this.lblFirstY.Size = new System.Drawing.Size(19, 13);
            this.lblFirstY.TabIndex = 64;
            this.lblFirstY.Text = "年";
            // 
            // lblFirstM
            // 
            this.lblFirstM.AutoSize = true;
            this.lblFirstM.Location = new System.Drawing.Point(141, 31);
            this.lblFirstM.Name = "lblFirstM";
            this.lblFirstM.Size = new System.Drawing.Size(19, 13);
            this.lblFirstM.TabIndex = 65;
            this.lblFirstM.Text = "月";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(755, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "柔整 [協]:0 [契]:1";
            // 
            // verifyBoxClinicNum
            // 
            this.verifyBoxClinicNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.verifyBoxClinicNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxClinicNum.Location = new System.Drawing.Point(745, 29);
            this.verifyBoxClinicNum.Name = "verifyBoxClinicNum";
            this.verifyBoxClinicNum.NewLine = false;
            this.verifyBoxClinicNum.Size = new System.Drawing.Size(103, 23);
            this.verifyBoxClinicNum.TabIndex = 65;
            this.verifyBoxClinicNum.TextV = "";
            // 
            // lblReg
            // 
            this.lblReg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReg.AutoSize = true;
            this.lblReg.Location = new System.Drawing.Point(742, 13);
            this.lblReg.Name = "lblReg";
            this.lblReg.Size = new System.Drawing.Size(91, 13);
            this.lblReg.TabIndex = 35;
            this.lblReg.Text = "柔整師登録番号";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.AutoSize = true;
            this.labelInputerName.Location = new System.Drawing.Point(268, 741);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(34, 13);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "入力:";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(60, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 764);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1083, 764);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.pSejutu.ResumeLayout(false);
            this.pSejutu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelTotal;
        private VerifyBox verifyBoxY;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxClinicNum;
        private System.Windows.Forms.Label lblReg;
        private VerifyBox verifyBoxBuisu;
        private System.Windows.Forms.Label lblBui;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
        private System.Windows.Forms.Label labelOryoKasan;
        private System.Windows.Forms.Label labelOryoBasic;
        private VerifyBox verifyBoxKasanCost;
        private VerifyBox verifyBoxOryoCost;
        private System.Windows.Forms.Label labelOuryo2;
        private System.Windows.Forms.Label labelOuryo;
        private VerifyBox verifyBoxKasanKm;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxF1FirstM;
        private System.Windows.Forms.Label lblFirst;
        private System.Windows.Forms.Label lblFirstY;
        private System.Windows.Forms.Label lblFirstM;
        private System.Windows.Forms.Panel pSejutu;
        private VerifyBox vbSejutuG;
        private System.Windows.Forms.Label lblSejutu;
        private System.Windows.Forms.Label lblSejutuM;
        private System.Windows.Forms.Label lblSejutuY;
        private VerifyBox vbSejutuM;
        private VerifyBox vbSejutuY;
        private VerifyCheckBox vcSejutu;
        private System.Windows.Forms.Label lblかくす;
    }
}