﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki
{
    public partial class ImportForm : Form
    {
        public ImportForm(int cym)
        {
            InitializeComponent();

            //20190423185427 furukawa st ////////////////////////
            //引数に月を追加

            textBoxY.Text = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100).ToString();
            //textBoxY.Text = DateTimeEx.GetHsYearFromAd(cym / 100).ToString();
            //20190423185427 furukawa ed ////////////////////////


            textBoxM.Text = (cym % 100).ToString();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            string dirName = textBox1.Text;
            if (dirName == string.Empty)
            {
                MessageBox.Show("対象フォルダを指定してください");
                return;
            }

            int.TryParse(textBoxY.Text, out int y);
            //だめy = DateTimeEx.GetAdYearFromHs(y);
            if (y < DateTime.Today.Year - 2 || DateTime.Today.Year < y)
            {
                MessageBox.Show("請求年度の指定が不正です");
                return;
            }

            int.TryParse(textBoxM.Text, out int m);
            if (m < 1 || 12 < m)
            {
                MessageBox.Show("月の指定が不正です");
                return;
            }

            int cym = y * 100 + m;
            //if (RefRece.Import(dirName, cym)) Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDir_Click(object sender, EventArgs e)
        {
            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != DialogResult.OK) return;
            textBox1.Text = d.Name;
        }
    }
}
