﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using NpgsqlTypes;

namespace Mejor.HyogoKoiki
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        public string Numbering { get; set; }
        public string InsNumber { get; set; }//保険者番号
        public string Number { get; set; }//被保険者証番号     
        public int Sex { get; set; }
        public DateTime Birth { get; set; }

        public string DrNumber { get; set; }//施術者登録記号番号
        public int CYM { get; set; }//処理年月
        public int YM { get; set; }//診療年月
        public DateTime FirstDay { get; set; }//初検日
        public int Days { get; set; }
        public int Ratio { get; set; }
        public int Total { get; set; }
        public int Charge { get; set; }
        public int Futan { get; set; }
        public string Name { get; set; }//被保険者氏名
        public string Zip { get; set; }
        public string PrefName { get; set; }
        public string CityName { get; set; }
        public string Add { get; set; }
        public string DestName { get; set; }
        //public DateTime StartDate { get; set; }


        //20190414090746 furukawa st ////////////////////////
        //兵庫広域からのレセデータインポート

        public static bool RefRece_Import(string fileName)
        {
            var l = CommonTool.CsvImportMultiCode(fileName);
            //var l = CommonTool.CsvImportUTF8(fileName);

            if (l == null) return false;

            //20220222143415 furukawa st ////////////////////////
            //try句、waitform追加
            
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;

            try
            {
                //20220222143415 furukawa ed ////////////////////////


                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf, tran))
                        {
                            if (System.Windows.Forms.MessageBox.Show("中止しますか？") ==
                                System.Windows.Forms.DialogResult.Yes) return false;                            
                        }

                        if (item.Length < 24) continue;

                        //CM以外飛ばす
                        if (item[0] != "CM") continue;

                        int.TryParse(item[29], out int sex);//性別
                        int.TryParse(item[42], out int days);//診療実日数

                        //20190704135531 furukawa st ////////////////////////
                        //給付割合、合計金額列間違い

                        int.TryParse(item[20], out int ratio);//給付割合
                        int.TryParse(item[55], out int total);//合計金額
                                                              //int.TryParse(item[21], out int ratio);//給付割合                    
                                                              //int.TryParse(item[56], out int total);//合計金額

                        //20190704135531 furukawa ed ////////////////////////


                        var rr = new RefRece();
                        rr.Numbering = item[63];//電算管理番号=レセプト全国共通キー
                        rr.InsNumber = item[17];//保険者番号
                        rr.Number = Microsoft.VisualBasic.Strings.StrConv(item[19], Microsoft.VisualBasic.VbStrConv.Narrow);
                        rr.Sex = sex;
                        rr.Birth = DateTimeEx.GetDateFromJstr7(item[30]);
                        rr.DrNumber = item[5] + "7" + item[10];//item[10].Remove(2) + item[10].Substring(3);//医療機関コード
                        rr.CYM = DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[7])), 1);//処理年月
                        rr.YM = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[3]));//診療年月
                        rr.FirstDay = DateTime.MinValue;// DateTimeEx.GetDateFromJstr7("4" + item[35]);//初検日→画面から取得予定
                        rr.Days = days;
                        rr.Ratio = ratio;
                        rr.Total = total;
                        rr.Charge = int.Parse(item[50]);
                        rr.Futan = 0;
                        rr.Name = string.Empty;
                        rr.Zip = string.Empty;
                        rr.PrefName = string.Empty;
                        rr.CityName = string.Empty;
                        rr.Add = string.Empty;
                        rr.DestName = string.Empty;

                        if (!DB.Main.Insert(rr, tran)) return false;

                        wf.InvokeValue++;
                    }
                    tran.Commit();
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }

        //20190414090746 furukawa ed ////////////////////////


        public static bool RefRece_ImportAHK(string fileName)
        {
            var l = CommonTool.CsvImportMultiCode(fileName);
            //var l = CommonTool.CsvImportUTF8(fileName);

            if (l == null) return false;

            //20220222140855 furukawa st ////////////////////////
            //try句、waitform追加
            
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;

            try
            {
                //20220222140855 furukawa ed ////////////////////////


                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf, tran))
                        {
                            if (System.Windows.Forms.MessageBox.Show("中止しますか？") ==
                                System.Windows.Forms.DialogResult.Yes) return false;
                        }

                        if (item.Length < 19) continue;
                        if (!int.TryParse(item[0], out int tmp)) continue;
                        int.TryParse(item[2], out int sex);//性別
                        int.TryParse(item[14], out int days);//診療実日数
                        int.TryParse(item[13], out int ratio);//給付割合
                        int.TryParse(item[12], out int total);//合計金額

                        var rr = new RefRece();
                        rr.Numbering = item[1].Remove(16); //item[0];//電算管理番号=レセプト全国共通キー あはきの場合は画像ファイル名の方が適切？
                        rr.InsNumber = item[4];     //保険者番号
                        rr.Number = item[5];        //被保険者証番号
                        rr.Sex = sex;
                        rr.Birth = DateTimeEx.GetDateFromJstr7(item[3]);

                        //医療機関番号
                        //都道府県番号＋柔整あはきコード（７～９）＋医療機関コード
                        rr.DrNumber = item[7];// item[5] + item[11] + item[7];//item[10].Remove(2) + item[10].Substring(3);//医療機関コード

                        rr.CYM = int.Parse(item[10]);// DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[10])), 1);//処理年月
                        rr.YM = int.Parse(item[9]);// DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[9]));//診療年月
                        rr.FirstDay = DateTime.MinValue;// DateTimeEx.GetDateFromJstr7("4" + item[35]);//初検日→画面から取得予定
                        rr.Days = days;
                        rr.Ratio = ratio;
                        rr.Total = total;
                        rr.Charge = 0;//請求金額が載っていない
                        rr.Futan = 0;
                        rr.Name = item[6];
                        rr.Zip = string.Empty;
                        rr.PrefName = string.Empty;
                        rr.CityName = string.Empty;
                        rr.Add = string.Empty;
                        rr.DestName = item[0];//string.Empty; あはきの場合は電算処理番号？が短く完全じゃないぽいのでこっちに移す

                        if (!DB.Main.Insert(rr, tran)) return false;

                        wf.InvokeValue++;
                    }
                    tran.Commit();
                    
                }

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }


        #region いる？

        //public static bool BeforeImport(string fileName)
        //{
        //    var l = CommonTool.CsvImportUTF8(fileName);
        //    if (l == null) return false;

        //    using (var tran = DB.Main.CreateTransaction())
        //    {
        //        foreach (var item in l)
        //        {
        //            if (item.Length < 24) continue;

        //            int.TryParse(item[14], out int sex);
        //            int.TryParse(item[47], out int days);
        //            int.TryParse(item[34], out int ratio);
        //            int.TryParse(item[48], out int total);

        //            var rr = new RefRece();
        //            rr.Numbering = item[18];
        //            rr.InsNumber = item[11];
        //            rr.Number = item[12];
        //            rr.Sex = sex;
        //            rr.Birth = DateTimeEx.GetDateFromJstr7(item[13]);
        //            rr.DrNumber = item[6].Remove(2) + item[6].Substring(3);
        //            rr.CYM = DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[22])), 1);
        //            rr.YM = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[23]));
        //            rr.FirstDay = DateTimeEx.GetDateFromJstr7("4" + item[35]);
        //            rr.Days = days;
        //            rr.Ratio = ratio;
        //            rr.Total = total;
        //            rr.Charge = 0;
        //            rr.Futan = 0;
        //            rr.Name = string.Empty;
        //            rr.Zip = string.Empty;
        //            rr.PrefName = string.Empty;
        //            rr.CityName = string.Empty;
        //            rr.Add = string.Empty;
        //            rr.DestName = string.Empty;

        //            if (!DB.Main.Insert(rr, tran)) return false;
        //        }
        //        tran.Commit();
        //    }

        //    return true;
        //}


        //public static bool AfterImport(string fileName)
        //{
        //    var rrs = new Dictionary<string, RefRece>();

        //    var wf = new WaitForm();
        //    wf.ShowDialogOtherTask();

        //    try
        //    {
        //        var csv = CommonTool.CsvImportUTF8(fileName);
        //        if (csv == null) return false;

        //        wf.LogPrint("CSV情報を取得しています");
        //        foreach (var item in csv)
        //        {
        //            if (item.Length < 24) continue;

        //            var rr = new RefRece();
        //            rr.Numbering = item[8].Trim();
        //            rr.InsNumber = item[0].Trim();
        //            rr.Number = item[1].Trim();
        //            rr.Sex = int.Parse(item[2]);
        //            rr.Birth = DateTimeEx.GetDateFromJstr7(item[3]);
        //            rr.DrNumber = item[4].Trim() + item[6].Trim() + item[7].Trim();
        //            rr.CYM = int.Parse(item[9]);
        //            rr.YM = int.Parse(item[10]);
        //            //初検日は先に提供されるパンチデータから取得するように変更
        //            //rr.FirstDay = DateTimeEx.ToDateTime(item[11]);
        //            rr.Days = int.Parse(item[12]);
        //            rr.Ratio = int.Parse(item[13]);
        //            rr.Total = int.Parse(item[14]);
        //            rr.Charge = int.Parse(item[15]);
        //            rr.Futan = int.Parse(item[16]);
        //            rr.Name = item[17].Trim();
        //            rr.Zip = item[19].Trim();
        //            rr.PrefName = item[20].Trim();
        //            rr.CityName = item[21].Trim();
        //            rr.Add = item[22].Trim();
        //            rr.DestName = item[23].Trim();
        //            rrs.Add(rr.Numbering, rr);
        //        }

        //        if (rrs.Count == 0)
        //        {
        //            System.Windows.Forms.MessageBox.Show("読み込むデータがありません。");
        //            return false;
        //        }

        //        int cym = rrs.First().Value.CYM;
        //        wf.LogPrint("すでに読み込まれた参照データを取得しています");
        //        var beforeRrs = RefRece.SelectAll(cym);
        //        foreach (var item in beforeRrs)
        //        {
        //            if (!rrs.ContainsKey(item.Numbering)) continue;
        //            rrs[item.Numbering].FirstDay = item.FirstDay;
        //        }

        //        //初検日のみ、先に提供されるパンチデータから取得する
        //        wf.LogPrint("申請情報を取得しています");
        //        var apps = App.GetApps(cym);
        //        wf.SetMax(apps.Count + rrs.Count);
        //        wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

        //        using (var tran = DB.Main.CreateTransaction())
        //        {
        //            //RefRece更新
        //            wf.LogPrint("参照データを更新しています");
        //            foreach (var item in rrs)
        //            {
        //                wf.InvokeValue++;

        //                if (!DB.Main.Update(item.Value, tran))
        //                {
        //                    wf.LogPrint("参照データの更新に失敗しました");
        //                    return false;
        //                }
        //            }

        //            //app更新
        //            wf.LogPrint("申請書データを更新しています");
        //            foreach (var app in apps)
        //            {
        //                wf.InvokeValue++;

        //                if (!rrs.ContainsKey(app.Numbering))
        //                {
        //                    app.StatusFlagSet(StatusFlag.支払保留);
        //                    if (!app.Update(0, App.UPDATE_TYPE.Null, tran)) return false;
        //                    continue;
        //                }

        //                var rr = rrs[app.Numbering];
        //                app.StatusFlagRemove(StatusFlag.支払保留);

        //                //すでにマッチング済みかどうかを、名前の有無でチェック
        //                if (string.IsNullOrEmpty(app.PersonName))
        //                {
        //                    var ymd = DateTimeEx.ToDateTime(rr.YM * 100 + 1);
        //                    app.MediYear = DateTimeEx.GetJpYear(ymd);
        //                    app.MediMonth = rr.YM % 100;
        //                    app.InsNum = rr.InsNumber;
        //                    app.HihoNum = rr.Number;
        //                    app.Family = 2;
        //                    app.HihoType = 0;
        //                    app.Sex = rr.Sex;
        //                    app.Birthday = rr.Birth;
        //                    app.PersonName = rr.Name;
        //                    app.HihoZip = rr.Zip;
        //                    app.HihoAdd = rr.PrefName + rr.CityName + rr.Add;
        //                    app.CountedDays = rr.Days;
        //                    app.Total = rr.Total;
        //                    app.Charge = rr.Charge;
        //                    app.Partial = rr.Futan;
        //                    app.NewContType = ymd.Year == rr.FirstDay.Year && ymd.Month == rr.FirstDay.Month ? NEW_CONT.新規 : NEW_CONT.継続;
        //                    app.FushoFirstDate1 = rr.FirstDay;
        //                    app.Ratio = rr.Ratio;
        //                    app.DrNum = rr.DrNumber;
        //                    app.ComNum = rr.Numbering;

        //                    app.TaggedDatas.DestName = rr.DestName == rr.Name ? string.Empty : rr.DestName;

        //                }

        //                if (!app.Update(0, App.UPDATE_TYPE.Null, tran)) return false;
        //            }
        //            tran.Commit();
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Log.ErrorWriteWithMsg(ex);
        //    }
        //    finally
        //    {
        //        wf.Dispose();
        //    }
        //    return true;
        //}

        #endregion


        public static RefRece Select(string numbering)
        {
            //インジェクション回避
            //if (!(long.TryParse(numbering, out long temp))) return null;
            return DB.Main.Select<RefRece>($"numbering='{numbering}'").SingleOrDefault();
        }

        public static List<RefRece> SelectAll(int cym)
        {
            return DB.Main.Select<RefRece>($"cym={cym}").ToList();
        }

        public static List<RefRece> SelectAll()
        {
            return DB.Main.SelectAll<RefRece>().ToList();
        }

        /// <summary>
        /// マッチングができなかったデータを取得します
        /// </summary>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cym)
        {
            var sql = "SELECT r.numbering, r.insnumber, r.number, r.sex, r.birth, " +
                "r.drnumber, r.cym, r.ym, r.firstday, r.days, r.ratio, r.total, " +
                "r.charge, r.futan, r.name, r.zip, r.prefname, r.cityname, r.add, r.destname " +
                "FROM RefRece AS r " +
                "LEFT OUTER JOIN application AS a ON r.numbering=a.numbering " +
                "WHERE r.cym=:cym AND a.numbering IS NULL;";

            return DB.Main.Query<RefRece>(sql, new { cym = cym }).ToList();
        }

        public bool CreateNotMatchCsv(System.IO.StreamWriter sw)
        {
            try
            {
                var l = new List<string>();

                l.Add(Numbering);
                l.Add(InsNumber);
                l.Add(Number);

                //l.Add(gender.ToString());
                //l.Add(DateTimeEx.GetIntJpDateWithEraNumber(Birthday).ToString());
                l.Add(Sex.ToString());
                l.Add(DateTimeEx.GetIntJpDateWithEraNumber(Birth).ToString());

                l.Add(DrNumber);
                l.Add(CYM.ToString());
                l.Add(YM.ToString());
                l.Add(FirstDay.ToString("yyyyMMdd"));
                l.Add(Days.ToString());
                l.Add(Ratio.ToString());
                l.Add(Total.ToString());
                l.Add(Charge.ToString());
                l.Add(Futan.ToString());
                l.Add(Name);
                l.Add(Zip);
                l.Add(PrefName);
                l.Add(CityName);
                l.Add(Add);
                l.Add(DestName);

                for (int i = 0; i < l.Count; i++)
                {
                    l[i] = "\"" + l[i] + "\"";
                }
                sw.WriteLine(string.Join(",", l));
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        public static bool AfterMatching(int cym, List<RefRece> refList)
        {
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("提供データを取得しています");
            var rrs = SelectAll(cym);
            var dic = new Dictionary<string, RefRece>();
            rrs.ForEach(x => dic.Add(x.Numbering, x));
            
            wf.LogPrint("申請情報を取得しています");
            var apps = App.GetApps(cym);
            
            wf.LogPrint("関連付けを行なっています");
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
            wf.SetMax(apps.Count);
            int c = 0, nc = 0, oc=0;

            try
            {
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var app in apps)
                    {
                        wf.InvokeValue++;
                        //すでにマッチング済みかどうかを、名前の有無でチェック
                        if (!string.IsNullOrEmpty(app.PersonName))
                        {
                            oc++;
                            continue;
                        }

                        if (!dic.ContainsKey(app.Numbering))
                        {
                            //提供データにない申請書の扱い
                            if (app.YM > 0)
                            {
                                nc++;
                                app.StatusFlagSet(StatusFlag.支払保留);
                            }
                        }
                        else
                        {
                            //データの反映
                            c++;
                            var rr = dic[app.Numbering];
                            var ymd = DateTimeEx.ToDateTime(rr.YM * 100 + 1);
                            app.MediYear = DateTimeEx.GetJpYear(ymd);
                            app.MediMonth = rr.YM % 100;
                            app.InsNum = rr.InsNumber;
                            app.HihoNum = rr.Number;
                            app.Family = 2;
                            app.HihoType = 0;

                            //app.Sex = rr.gender;
                            //app.Birthday = rr.Birthday;
                            app.Sex = rr.Sex;
                            app.Birthday = rr.Birth;

                            app.PersonName = rr.Name;
                            app.HihoZip = rr.Zip;
                            app.HihoAdd = rr.PrefName + rr.CityName + rr.Add;
                            app.CountedDays = rr.Days;
                            app.Total = rr.Total;
                            app.Charge = rr.Charge;
                            app.Partial = rr.Futan;
                            app.NewContType = ymd.Year == rr.FirstDay.Year && ymd.Month == rr.FirstDay.Month ? NEW_CONT.新規 : NEW_CONT.継続;
                            app.FushoFirstDate1 = rr.FirstDay;
                            app.Ratio = rr.Ratio;
                            app.DrNum = rr.DrNumber;
                            app.ComNum = rr.Numbering;

                            app.TaggedDatas.DestName = rr.DestName == rr.Name ? string.Empty : rr.DestName;
                        }

                        if (!app.Update(0, App.UPDATE_TYPE.Null, tran))
                        {
                            System.Windows.Forms.MessageBox.Show("データの更新に失敗しました。処理を中止します");
                            return false;
                        }
                    }
                    tran.Commit();

                    System.Windows.Forms.MessageBox.Show(
                        $"今回マッチングした申請書: {c}枚\r\n" +
                        $"データなし申請書: {nc}枚\r\n" +
                        $"マッチング済み申請書: {oc}枚\r\n\r\n" +
                        "データの更新が完了しました。");
                    return true;
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }
    }


    /*
     * 広域より提供されたデータ順
     * 
        0	保険者番号
        1	被保険者番号
        2	性別
        3	和暦生年月日
        4	4,6,7で施術師番号 県番号
        5	不明
        6	4,6,7で施術師番号
        7	4,6,7で施術師番号
        8	電算番号
        9	審査年月
        10	診療年月
        11	初検年月日
        12	実日数
        13	給付割合？
        14	合計
        15	支給額
        16	一部負担金
        17	被保険者氏名
        18	市町村番号
        19	郵便番号
        20	県名
        21	市町村名
        22	住所
        23	送付先氏名
     */

}
