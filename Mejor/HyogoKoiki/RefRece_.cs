﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NPOI.HSSF;
using NPOI.SS.UserModel;

namespace Mejor.HyogoKoiki
{
    class RefRece_ : RefReceCore
    {
        public string Kana { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Add { get; set; } = string.Empty;
        public DateTime Birthday { get; set; }
        public string DestName { get; set; } = string.Empty;
        public string DestKana { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdd { get; set; } = string.Empty;

        public int Family { get; set; }
        public int Ratio { get; set; }
        public string GroupCode { get; set; } = string.Empty;
        public string InsName { get; set; } = string.Empty;
        public DateTime StartDate { get; set; }
        public bool Merge { get; set; } = false;
        public bool Advance { get; set; } = false;

        public static int cymlocal = 0;
        
        class ImportFiles
        {
            public string dir = string.Empty;
            public string jyu = string.Empty;
            public string jyuMerge = string.Empty;
            public string ahk = string.Empty;
            public string ahkMerge = string.Empty;
            public string bfJyu = string.Empty;
        }

        public static bool Import(string dirName, int cym)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                cymlocal = cym;

                var res = import(dirName, cym, wf);
                if (res)
                {
                    System.Windows.Forms.MessageBox.Show("インポートが完了しました");
                    return true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                    return false;
                }
            }
        }

        private static bool import(string dirName, int cym, WaitForm wf)
        {
            
            var iFiles = new ImportFiles();
            List<RefRece_>jyuBf, jyul, ahkl;
            try
            {
                
                wf.LogPrint("ファイルをチェックしています");
                if (!fileCheck(dirName, wf, iFiles)) return false;
                
                wf.LogPrint("事前柔整申請書を読み込んでいます");
                jyuBf = getRefReceFromExcel(iFiles.bfJyu, cym);

                wf.LogPrint("柔整申請書を読み込んでいます");
                jyul = getRefReceFromCsv(iFiles.jyu, cym);

                wf.LogPrint("柔整申請書をマージしています");
                mergeFromCsv(iFiles.jyuMerge, jyul);

                wf.LogPrint("あはき申請書を読み込んでいます");
                ahkl = getRefReceFromCsv(iFiles.ahk, cym);

                wf.LogPrint("あはき申請書をマージしています");
                mergeFromCsv(iFiles.ahkMerge, ahkl);
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            using (var tran = DB.Main.CreateTransaction())
            {
                wf.LogPrint("インポート情報をDBに登録しています");
                var rri = RefReceImport.GetNextImport();
                rri.FileName = dirName;
                rri.CYM = cym;
                rri.ReceCount = jyuBf.Count + jyul.Count + ahkl.Count;
                rri.AppType = APP_TYPE.複合;
                rri.Insert(tran);

                jyuBf.ForEach(r => r.ImportID = rri.ImportID);
                jyul.ForEach(r => r.ImportID = rri.ImportID);
                ahkl.ForEach(r => r.ImportID = rri.ImportID);

                wf.SetMax(rri.ReceCount);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                wf.LogPrint("事前柔整申請書情報をDBに登録しています");
                foreach (var item in jyuBf)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }

                wf.LogPrint("柔整申請書情報をDBに登録しています");
                foreach (var item in jyul)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }

                wf.LogPrint("あはき申請書情報をDBに登録しています");
                foreach (var item in ahkl)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }                

                wf.LogPrint("照会対象外情報をDBに登録しています");
                var ignoreHihos = new List<string>();
                ignoreHihos.AddRange(jyul.FindAll(r => !r.Merge).Select(r => r.Num));
                ignoreHihos.AddRange(ahkl.FindAll(r => !r.Merge).Select(r => r.Num));
                if (!Shokai.ShokaiExclude.RegisterShokaiExcludeHihoNums(ignoreHihos, tran)) return false;

                tran.Commit();
                return true;
            }
        }

        private static bool fileCheck(string dirName, WaitForm wf, ImportFiles iFiles)
        {
            bool error = false;
            iFiles.dir = dirName;
            var dirs = Directory.GetDirectories(dirName);


            List<String> lst = System.IO.Directory.GetFiles(dirName).ToList();

            //CSVファイル名に含んでいる文字列
            const string clinicFileNamePart = "医療機関";
            const string sendToFileNamePart = "送付先";
            const string personFileNamePart = "被保険者";
            const string jyuFileNamePart = "柔整";
            const string ahkFileNamePart = "あはき";

           
            foreach(string tmp in lst)
            {
                if (tmp.Contains(jyuFileNamePart))
                {
                    getRefReceFromCsv(tmp,cymlocal);
                }
            }


            if (!lst.Contains(jyuFileNamePart))
            {
                wf.LogPrint(jyuFileNamePart + "CSVが特定できませんでした。");
                error = true;
            }
            if (!lst.Contains(ahkFileNamePart))
            {
                wf.LogPrint(ahkFileNamePart + "CSVが特定できませんでした。");
                error = true;
            }
            if (!lst.Contains(clinicFileNamePart))
            {
                wf.LogPrint(clinicFileNamePart + "CSVが特定できませんでした。");
                error = true;
            }
            if (!lst.Contains(sendToFileNamePart))
            {
                wf.LogPrint(sendToFileNamePart + "CSVが特定できませんでした。");
                error = true;
            }
            if (!lst.Contains(personFileNamePart))
            {
                wf.LogPrint(personFileNamePart + "CSVが特定できませんでした。");
                error = true;
            }

            
            var jyu1 = Array.FindAll(dirs, dir => dir.Contains(clinicFileNamePart));
            //var jyu1 = Array.FindAll(dirs, dir => dir.Contains("(8)柔整CSV(入力全件)"));
            if (jyu1.Length == 1)
            {
                var fs = Directory.GetFiles(jyu1[0]);
                var js = Array.FindAll(fs, f => f.Contains(".csv"));

                if (js.Length == 1) iFiles.jyu = js[0];
                else
                {
                    wf.LogPrint(jyuFileNamePart +"CSVが特定できませんでした。");
                    //wf.LogPrint("柔整全件CSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint(jyuFileNamePart + "CSVが特定できませんでした。");
                //wf.LogPrint("柔整全件CSVが特定できませんでした。");
                error = true;
            }

            var jyuM = Array.FindAll(dirs, dir => dir.Contains("(3)柔整CSV"));
            if (jyuM.Length == 1)
            {
                var fs = Directory.GetFiles(jyuM[0]);
                var js = Array.FindAll(fs, f => f.Contains(".csv"));

                if (js.Length == 1) iFiles.jyuMerge = js[0];
                else
                {
                    wf.LogPrint("柔整マージCSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("柔整マージCSVが特定できませんでした。");
                error = true;
            }



            var ahk = Array.FindAll(dirs, dir => dir.Contains(ahkFileNamePart));
            //var ahk = Array.FindAll(dirs, dir => dir.Contains("(7)あんま・はり灸・マッサージCSV(入力全件)"));
            if (ahk.Length == 1)
            {
                var fs = Directory.GetFiles(ahk[0]);
                var js = Array.FindAll(fs, f => f.Contains(".csv"));

                if (js.Length == 1) iFiles.ahk = js[0];
                else
                {
                    wf.LogPrint(ahkFileNamePart+"CSVが特定できませんでした。");
                    //wf.LogPrint("あはき全件CSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint(ahkFileNamePart+"CSVが特定できませんでした。");
                //wf.LogPrint("あはき全件CSVが特定できませんでした。");
                error = true;
            }

            var ahkM = Array.FindAll(dirs, dir => dir.Contains("(5)あんま・はり灸・マッサージCSV(マージあり)"));
            if (ahkM.Length == 1)
            {
                var fs = Directory.GetFiles(ahkM[0]);
                var js = Array.FindAll(fs, f => f.Contains(".csv"));

                if (js.Length == 1) iFiles.ahkMerge = js[0];
                else
                {
                    wf.LogPrint("あはきマージCSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("あはきマージCSVが特定できませんでした。");
                error = true;
            }

            var dirFs = Directory.GetFiles(dirName);
            var bfjyu = Array.FindAll(dirFs, dir => dir.Contains("_柔整点検データ（後期分）"));
            if (bfjyu.Length == 1) iFiles.bfJyu = bfjyu[0];
            else
            {
                wf.LogPrint("柔整事前Excelが特定できませんでした。");
                error = true;
            }
            
            return !error;
        }
        

        private static List<RefRece_> getRefReceFromCsv(string fileName, int cym)
        {
            var rrs = new List<RefRece_>();

            var l = CommonTool.CsvImportMultiCode(fileName);
            //var l2 = CommonTool.CsvImportMultiCode(被保険者csv);
            //柔整データを回して被保険者情報を取り込みながら登録？
            
            foreach (var item in l)
            {


                if (item.Length < 20) continue;
                /*
                int.TryParse(item[4], out int family);
                int.TryParse(item[5], out int ratio);
                int.TryParse(item[6], out int mym);
                int.TryParse(item[21], out int birth);*/

                int.TryParse(item[14], out int family);//本人家族      
                int.TryParse(item[3], out int mym);//たぶん診療年月
                int.TryParse(item[30], out int birth);


                int.TryParse(item[29], out int sex);//性別
                int.TryParse(item[42], out int days);//診療実日数
                int.TryParse(item[21], out int ratio);//給付割合
                int.TryParse(item[56], out int total);//合計金額

                var rr = new RefRece_();
                rr.AppType = item[7] == "8" ? APP_TYPE.鍼灸 :
                    item[7] == "9" ? APP_TYPE.あんま : APP_TYPE.柔整;

                mym = DateTimeEx.GetAdYearFromHs(mym) * 100 + mym % 100;
                //mym = DateTimeEx.GetAdYearFromHs(mym / 100) * 100 + mym % 100;

                rr.CYM = cym;
                rr.InsName = string.Empty;// item[0].Replace("　", " ").Replace(" ", "");
                rr.InsNum = item[17].Trim();
                rr.Num = item[19].Trim();
                rr.Name = string.Empty; item[3].Trim();
                rr.Family = family;
                rr.Ratio = ratio;
                rr.MYM = mym;

                rr.Kana = string.Empty;// item[16].Trim();
                rr.Zip = string.Empty;// item[17].Trim();
                rr.Add = string.Empty;//item[18].Trim() + item[19].Trim() + item[20].Trim();
                rr.Birthday = DateTimeEx.ToDateTime(birth);// item[21].Trim().ToDateTime();
                rr.DestZip = string.Empty;//item[22].Trim();
                rr.DestAdd = string.Empty;//item[23].Trim() + item[24].Trim() + item[25].Trim();
                rr.DestName = string.Empty;//item[26].Trim();
                rr.DestKana = string.Empty;//item[27].Trim();

                /*
                if (rr.AppType == APP_TYPE.柔整)
                {
                    int.TryParse(item[12], out int sDate);
                    int.TryParse(item[13], out int days);
                    int.TryParse(item[14], out int total);
                    int.TryParse(item[15], out int charge);
                    rr.ClinicNum = item[8].Trim() + "7" + item[9].Trim();
                    rr.GroupCode = item[10].Trim();
                    rr.ClinicName = item[11].Trim();
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = charge;
                    int y = DateTimeEx.GetAdYearFromHs(sDate / 10000);
                    int m = sDate / 100 % 100;
                    int d = sDate % 100;
                    rr.StartDate = new DateTime(y, m, d);
                }
                else
                {
                    int.TryParse(item[11], out int sDate);
                    int.TryParse(item[12], out int days);
                    int.TryParse(item[13], out int total);
                    int.TryParse(item[14], out int charge);
                    rr.DrNum = item[9].Trim();
                    rr.DrName = item[10].Trim();
                    rr.SearchNum = item[15].Trim();
                    rr.ClinicNum = rr.DrNum;
                    rr.ClinicName = item[28].Trim();
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = charge;
                    int y = DateTimeEx.GetAdYearFromHs(sDate / 10000);
                    int m = sDate / 100 % 100;
                    int d = sDate % 100;
                    rr.StartDate = new DateTime(y, m, d);
                }
                */
                rrs.Add(rr);
            }
            return rrs;
        }

        private static void mergeFromCsv(string fileName, List<RefRece_> rrs)
        {

            //20190201115309 furukawa st ////////////////////////
            //インポートデータがUTF8に変わるため
            //var l = CommonTool.CsvImportShiftJis(fileName);
            
            //var l = CommonTool.CsvImportUTF8(fileName);
            var l = CommonTool.CsvImportMultiCode(fileName);

            //20190201115309 furukawa ed ////////////////////////

            var dic = new Dictionary<string, List<RefRece_>>();

            foreach (var item in rrs)
            {
                if (!dic.ContainsKey(item.Num)) dic.Add(item.Num, new List<RefRece_>());
                dic[item.Num].Add(item);
            }

            foreach (var item in l)
            {
                if (item.Length < 20) continue;
                int.TryParse(item[6], out int mym);
                mym = DateTimeEx.GetAdYearFromHs(mym / 100) * 100 + mym % 100;

                var num = item[2].Trim();
                var appType = item[7] == "8" ? APP_TYPE.鍼灸 :
                    item[7] == "9" ? APP_TYPE.あんま : APP_TYPE.柔整;
                var clinicNum = appType == APP_TYPE.柔整 ?
                    item[8].Trim() + "7" + item[9].Trim() : item[9].Trim();

                if (!dic.ContainsKey(num)) continue;
                var t = dic[num].FirstOrDefault(r =>
                     r.ClinicNum == clinicNum && r.MYM == mym && !r.Merge);

                if (t != null)
                {
                    t.Merge = true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("NULL");
                }
            }
        }

        public static List<RefRece_> getRefReceFromExcel(string fileName, int cym)
        {
            var rrs = new List<RefRece_>();

            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var wb = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);
                var sheet = wb.GetSheet("後期点検データ");

                var rowCount = sheet.LastRowNum + 2;

                for (int i = 2; i < rowCount; i++)
                {
                    var r = sheet.GetRow(i);
                    if (r == null) continue;
                    var c = r.GetCell(0);
                    if (c == null || c.CellType != CellType.String) continue;

                    var b = r.GetCell(0).StringCellValue;
                    if (b.Length < 1 || b[0] != 'B') continue;

                    int.TryParse(r.GetCell(4).StringCellValue, out int mym);
                    int.TryParse(r.GetCell(10).StringCellValue, out int days);
                    int.TryParse(r.GetCell(16).StringCellValue, out int total);
                    int.TryParse(r.GetCell(17).StringCellValue, out int partial);

                    var rr = new RefRece_();
                    rr.AppType = APP_TYPE.柔整;
                    rr.CYM = cym;
                    rr.MYM = DateTimeEx.GetAdYearMonthFromJyymm(mym);
                    rr.InsName = r.GetCell(2).StringCellValue;
                    rr.ClinicNum = r.GetCell(5).StringCellValue;
                    rr.Num = r.GetCell(6).StringCellValue.PadLeft(8, '0'); ;
                    rr.Birthday = DateTimeEx.GetDateFromJstr7(r.GetCell(7).StringCellValue);
                    rr.Name = r.GetCell(9).StringCellValue;
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = total - partial;
                    rr.Merge = true;
                    rr.Advance = true;

                    rrs.Add(rr);
                }
            }

            return rrs;
        }


        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="mym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece_> SerchByInput(int cym, int mym, string num, int total) =>
            DB.Main.Select<RefRece_>(new { num, mym, total, cym }).ToList();

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece_> SerchForMatching(int cym, string num, int total) =>
            DB.Main.Select<RefRece_>(new { cym, num, total }).ToList();

        public static List<RefRece_> SelectAll(int cym) =>
            DB.Main.Select<RefRece_>(new { cym }).ToList();

        /// <summary>
        /// すでに読み込まれた参照データから、マージ情報を元に
        /// 照会対象外データを登録します
        /// </summary>
        /// <returns></returns>
        public static bool IgnoreHihosInsert()
        {
            var l = DB.Main.SelectAll<RefRece_>();
            var gs = l.GroupBy(rr => rr.ImportID);
            using (var tran = DB.Main.CreateTransaction())
            {
                foreach (var item in gs)
                {
                    var rrs = item.ToList();
                    var ignoreHihos = new List<string>();
                    ignoreHihos.AddRange(rrs.FindAll(r => !r.Merge).Select(r => r.Num));
                    if (!Shokai.ShokaiExclude.RegisterShokaiExcludeHihoNums(ignoreHihos, tran))
                        return false;
                }
                tran.Commit();
            }
            return true;
        }
    }
}