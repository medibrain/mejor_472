﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
//using NpgsqlTypes;


namespace Mejor.HyogoKoiki
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        
        private InputMode inputMode;
        
        int cym;
        private APP_TYPE currentAppType;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        /*
        Point posHosCode = new Point(400, 800);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        */

        Point posFusho = new Point(100, 250);
        Point posFushoAHK = new Point(100, 300);


        /*
        Point posCost = new Point(400, 800);
        Point posDays = new Point(400, 300);
        Point posNumbering = new Point(300, 0);
        */

        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);
        

        Point posOryo = new Point(350, 500);
        Point posOryoAHK = new Point(150, 300);
        Point posClinic = new Point(800, 1300);

        Control[] ymControls, fushoControls, oryoControls, clinicControls;

        public InputForm(ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, };

            // 兵庫県後期広域　負傷名なし　
            //fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            fushoControls = new Control[] { verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxBuisu, };

            oryoControls = new Control[] {checkBoxVisit,checkBoxVisitKasan,
                verifyBoxKasanKm, labelOryoKasan, verifyBoxOryoCost};

            //登録記号番号
            clinicControls = new Control[] { verifyBoxClinicNum, };


            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox || item is CheckBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            //panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            
            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }


        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }


        private void initialize()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

      
        }

        #region 表示レセが変わったとき
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            
            //20190416205234 furukawa st ////////////////////////
            //現在のレセタイプを取得            
            currentAppType = scanGroup.AppType;
            //20190416205234 furukawa ed ////////////////////////            

            setApp(app);
            focusBack(false);
        }
        #endregion


        #region コントロールにカーソルが入ったとき
        void item_Enter(object sender, EventArgs e)
        {
            var t = (Control)sender;
            var app = (App)bsApp.Current;

            //柔整とあはきで見る場所が違う
            if (currentAppType == APP_TYPE.柔整)
            {
                if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
                else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
                else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
                else if (clinicControls.Contains(t)) scrollPictureControl1.ScrollPosition = posClinic;//登録記号番号
            }
            else if (currentAppType != APP_TYPE.柔整)
            {
                if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
                else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryoAHK;
                else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryoAHK;
                else if (clinicControls.Contains(t)) scrollPictureControl1.ScrollPosition = posClinic;//登録記号番号
            }
        }
        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion

        #region 登録ボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }
        #endregion

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        /*
        private void selectRefRece(App app)
        {
            bsRefReceData.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefReceData.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時
                int cym = scan.CYM;
                int mym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
                l = RefRece.SerchByInput(cym, mym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefReceData.DataSource = l;
            bsRefReceData.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridRefRece.RowCount; i++)
                {
                    var rrid = (int)dataGridRefRece[nameof(RefRece.RrID), i].Value;
                    if (app.RrID == rrid)
                    {
                        //既に一意の広域データとマッチング済みの場合。
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridRefRece.CurrentCell = dataGridRefRece[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridRefRece.CurrentCell = null;
            }
        }

        */


        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            #region 入力チェック

            //初検日
            //20190421120221 furukawa st ////////////////////////
            //あんま以外はチェックする

            if (scanGroup.AppType == APP_TYPE.柔整 || scanGroup.AppType==APP_TYPE.鍼灸)
            //    if (scanGroup.AppType == APP_TYPE.柔整)
            //20190421120221 furukawa ed ////////////////////////

            {
                setStatus(verifyBoxF1FirstY, !int.TryParse(verifyBoxF1FirstY.Text, out int resY));
                setStatus(verifyBoxF1FirstM, !int.TryParse(verifyBoxF1FirstM.Text, out int resM));
                var firstDate = dateCheck(verifyBoxF1FirstY, verifyBoxF1FirstM);
                app.FushoFirstDate1 = firstDate;

            }


            #region 往療料のチェック
            //往療料のチェック
            // decimal visitDistance = 0;
            int kihonOryo = 0;
            int kasanOryo = 0;

            //大阪広域よりコピー
            double inputDistance = 0;
            double.TryParse(verifyBoxKasanKm.Text, out inputDistance);
            int.TryParse(verifyBoxOryoCost.Text, out kihonOryo);
            int.TryParse(verifyBoxKasanCost.Text, out kasanOryo);


            // 数字が不要なのでチェックも不要 2019/04/21とか言っていたが結局必要なご様子
            if (checkBoxVisitKasan.Checked)
            {
                if (!string.IsNullOrWhiteSpace(verifyBoxKasanKm.Text) && inputDistance == 0)
                {
                    setStatus(verifyBoxKasanKm, true);
                }
                else
                {
                    // 加算金額より計算
                    if (!string.IsNullOrEmpty(verifyBoxOryoCost.Text) && kihonOryo == 0)
                    {
                        setStatus(verifyBoxOryoCost, true);
                    }

                    if (!string.IsNullOrEmpty(verifyBoxKasanCost.Text) && kasanOryo == 0)
                    {
                        setStatus(verifyBoxKasanCost, true);
                    }


                    // 加算にチェックがあり、距離・金額ともに入力がない場合
                    if (inputDistance == 0 && (kihonOryo == 0 || kasanOryo == 0))
                    {
                        if (currentAppType == APP_TYPE.柔整)
                        {
                            setStatus(verifyBoxOryoCost, true);
                            setStatus(verifyBoxKasanCost, true);
                        }
                    }

                    // 加算距離算出
                    if (!hasError && inputDistance == 0)
                    {
                        int visittimes = kihonOryo / 1800;
                        int incrementKasan = (visittimes > 0) ? kasanOryo / visittimes / 800 : 0;
                        inputDistance = incrementKasan * 2;
                    }
                }
            }

            #endregion

            //柔整師番号
            var clinicCode = verifyBoxClinicNum.Text.Trim();
            int.TryParse(clinicCode, out int drIntCode);

            int bui = 0;//部位数

            if (scanGroup.AppType == APP_TYPE.柔整)
            {
       
                #region 柔整師番号
               if (clinicCode.Length == 0)
                {
                    var res = MessageBox.Show(
                        "施術機関番号が入力されていません。このまま登録してもよろしいですか？", "",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);
                    if (res != DialogResult.OK) setStatus(verifyBoxClinicNum, true);
                    else setStatus(verifyBoxClinicNum, false);
                }
                else if (clinicCode.Length == 10 && (clinicCode[0] == '0' || clinicCode[0] == '1') && 0 < drIntCode)
                {
                    setStatus(verifyBoxClinicNum, false);
                }
                else if (clinicCode.Length != 10)
                {
                    //10文字でなければエラー
                    setStatus(verifyBoxClinicNum,true);
                }
                else
                {
                    setStatus(verifyBoxClinicNum, true);
                }
                #endregion

                //20190415145711 furukawa st ////////////////////////
                //部位数追加            
                setStatus(verifyBoxBuisu, (!int.TryParse(verifyBoxBuisu.Text, out bui) && verifyBoxBuisu.Text != string.Empty) ||
                                            (bui < 1 || bui > 10));

                //20190415145711 furukawa ed ////////////////////////
            }
            
            

    

            /* 兵庫県後期広域　負傷名なし　
            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            */

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
            #endregion

            #region 広域データからデータ取得し、Applicaitionテーブルに反映


            //広域データからデータ取得
            var rr = RefRece.Select(app.Numbering);
            if (rr == null)
            {
                MessageBox.Show("広域提供データから一致する情報を見つけられませんでした。登録できません。\r\n",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //値の反映
            var ymd = DateTimeEx.ToDateTime(rr.YM * 100 + 1);
            app.MediYear = DateTimeEx.GetJpYear(ymd);
            app.MediMonth = rr.YM % 100;
            app.InsNum = rr.InsNumber;
            app.HihoNum = rr.Number;
            app.Family = 2;
            app.HihoType = 0;
            app.Sex = rr.Sex;
            app.Birthday = rr.Birth;


            //20190703151335 furukawa st ////////////////////////
            //提供される被保険者csvから取得する
            
            Person p = Person.GetPerson(app.HihoNum, app.Birthday);
            if (p != null)
            {
                app.PersonName = p.HihoName;
                app.HihoZip = p.Zip;
                app.HihoAdd = p.Address1 + p.Address2 + p.Address3;
            }

            //app.PersonName = rr.Name;
            //app.HihoZip = rr.Zip;
            //app.HihoAdd = rr.PrefName + rr.CityName + rr.Add;

            //20190703151335 furukawa ed ////////////////////////


            app.CountedDays = rr.Days;
            app.Total = rr.Total;
            app.Charge = rr.Charge;
            app.Partial = rr.Futan;


            //20190703121040 furukawa st ////////////////////////
            //初検日で新規継続判定　初検年月＜＞診療年月の場合継続
            if (scanGroup.AppType == APP_TYPE.柔整 || scanGroup.AppType == APP_TYPE.鍼灸)
            {
                app.NewContType = ymd.Year == app.FushoFirstDate1.Year && ymd.Month == app.FushoFirstDate1.Month ? NEW_CONT.新規 : NEW_CONT.継続;
            }
            else
            {
                //あんまは初検日の入力をしないため判別できない
                app.NewContType = NEW_CONT.Null;
            }
            //app.NewContType = ymd.Year == rr.FirstDay.Year && ymd.Month == rr.FirstDay.Month ? NEW_CONT.新規 : NEW_CONT.継続;
            //20190703121040 furukawa ed ////////////////////////



            //app.FushoFirstDate1 = rr.FirstDay;
            app.Ratio = rr.Ratio;
            app.DrNum = clinicCode;// rr.DrNumber;
            app.Distance = checkBoxVisit.Checked ? 999 : 0;

            //20190421102935 furukawa st ////////////////////////
            //往料加算のチェックがない場合、加算距離は0で登録。あはきは距離を入れないので無条件で-1
            if (currentAppType == APP_TYPE.柔整)
            {
                app.VisitAdd = checkBoxVisitKasan.Checked ? Convert.ToInt32(inputDistance * 1000) : 0;
            }
            else
            {
                app.VisitAdd = checkBoxVisitKasan.Checked ? -1 : 0;
            }

            //app.VisitAdd = Convert.ToInt32(inputDistance * 1000);//大阪広域よりコピー
            //20190421102935 furukawa ed ////////////////////////


            app.VisitFee = checkBoxVisit.Checked ? kihonOryo : 0;
            app.VisitTimes = checkBoxVisit.Checked ? kasanOryo : 0;
            app.AppType = scan.AppType;
            app.ComNum = rr.Numbering;


            app.TaggedDatas.DestName = rr.DestName == rr.Name ? string.Empty : rr.DestName;
            
            /*
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            */

            //部位数
            app.Bui = bui;

            //20190703112308 furukawa st ////////////////////////
            //部位数タグデータに持たせ、リスト作成画面で検索できるようにする                      
            app.TaggedDatas.count = bui;
            //20190703112308 furukawa ed ////////////////////////


            //20190422103041 furukawa st ////////////////////////
            //交付料有無フラグ登録
            if (currentAppType == APP_TYPE.あんま || currentAppType == APP_TYPE.鍼灸)
            {
                app.TaggedDatas.flgKofuUmu = vcSejutu.Checked;
            }
            //20190422103041 furukawa ed ////////////////////////

            #endregion

            #region 被保険者情報がある場合はセット
            if (app.HihoNum !="0" && app.Birthday != DateTime.MinValue)
            {
                var psn = Person.GetPerson(app.HihoNum, app.Birthday);
                if (psn != null)
                {
                    app.HihoAdd = psn.Address1.Trim() + psn.Address2.Trim() + psn.Address3.Trim();
                    app.HihoZip = psn.Zip.Trim();
                    app.TaggedDatas.Kana = psn.Kana;
                }
            }
            #endregion


            return true;
        }

        #region 関連AID取得
        /// <summary>
        /// 続紙に紐付く申請書のAID取得
        /// </summary>
        /// <returns></returns>
        private int GetAID()
        {
            //20190426124154 furukawa st ////////////////////////
//タイムアウト調査のため一旦外す

            return 0;
            //20190426124154 furukawa ed ////////////////////////


            var app = (App)bsApp.Current;
            StringBuilder sb = new StringBuilder();

            //自分のAID-1から自分AID-20の間で、AppTypeが0超（申請書だけ）を拾う
            //2019-03-13　申請書の最大枚数は20と想定（伸作さん確認済）
            
            sb.AppendFormat("select max(aid) from application where aid < {0} and aapptype>=6",
                 app.Aid);
            
            var cmd = DB.Main.CreateCmd(sb.ToString());
            string strAID = cmd.TryExecuteScalar().ToString();

            
            if (strAID == string.Empty) return 0;
            return int.Parse(strAID);

        }
        #endregion


        #region db更新
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //20190417170910 furukawa st ////////////////////////
            //numberingを控える           
            string tmpNumbering = app.Numbering;
            //20190417170910 furukawa ed ////////////////////////


            //続紙分類の追加
            switch (verifyBoxY.Text)
            {
                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID(); //20190421125050 furukawa 一応続紙の親を控えておく
                    break;

                case clsInputKind.長期:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.AppType = APP_TYPE.長期;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID(); //20190421125050 furukawa 一応続紙の親を控えておく
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID(); //20190421125050 furukawa 一応続紙の親を控えておく
                    break;

                case clsInputKind.エラー:
                    //エラー
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID(); //20190421125050 furukawa 一応続紙の親を控えておく
                    break;


                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    /*if (!checkAppFor901(app))
                    {
                        focusBack(true);
                        return false;
                    }
                    */
                    break;


                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                case clsInputKind.状態記入書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                default:
                    //申請書の場合
                    
                    
                    /* いる？
                    if (dataGridRefRece.CurrentCell == null)
                    {
                        MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                            "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    */

                    int oldDid;
                    int.TryParse(app.Numbering, out oldDid);
                    var oldDis = app.Distance;

                    if (!checkApp(app))
                    {
                        focusBack(true);
                        return false;
                    }

                    break;

            }

            




            //20190417170934 furukawa st ////////////////////////
            //控えたnumberingを戻す
            app.Numbering = tmpNumbering;
            //20190417170934 furukawa ed ////////////////////////

            #region 旧コード

            /*
            if (verifyBoxY.Text == "--")
            {
                //続紙
                var com = app.Numbering;
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
                app.Numbering = com;
                app.ComNum = com;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                var com = app.Numbering;
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                app.Numbering = com;
                app.ComNum = com;
            }
            else
            {
                //データベース登録用のAppインスタンスを作成
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            */

            #endregion


            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = App.UPDATE_TYPE.FirstInput;

                if (app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        #endregion



        #region 次のレセの表示
        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力:  " + User.GetUserName(app.Ufirst);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    /* 兵庫県後期広域　負傷名なし　
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    */
                }
            }

            //20190416205234 furukawa st ////////////////////////
            //現在のレセタイプを取得            
            currentAppType = scanGroup.AppType;
            //20190416205234 furukawa ed ////////////////////////


            setControlInput();
           

            //画像の表示
            setImage(app);
            changedReset(app);
        }
        #endregion


        #region 入力可能コントロールをAPPTYPEで区別 20190422095716 furukawa 
        /// <summary>
        /// 入力可能コントロールをAPPTYPEで区別
        /// </summary>
        private void setControlInput()
        {
            switch (currentAppType)
            {
                case APP_TYPE.鍼灸:
                    setCanInput(verifyBoxBuisu, false);
                    setCanInput(lblBui, false);
                    setCanInput(verifyBoxClinicNum, false);
                    setCanInput(lblReg, false);

                    setCanInput(vcSejutu, true);


                    break;
                case APP_TYPE.あんま:
                    setCanInput(verifyBoxBuisu, false);
                    setCanInput(lblBui, false);
                    setCanInput(verifyBoxClinicNum, false);
                    setCanInput(lblReg, false);

                    setCanInput(verifyBoxF1FirstY, false);
                    setCanInput(verifyBoxF1FirstM, false);
                    setCanInput(lblFirst, false);
                    setCanInput(lblFirstY, false);
                    setCanInput(lblFirstM, false);

                    setCanInput(vcSejutu, true);
                    break;
            }
        }
        #endregion


        #region 画像表示
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="a"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //20190416213607 furukawa st ////////////////////////
                    //現在のレセタイプで率を変更

                    //拡大表示
                    if (currentAppType == APP_TYPE.柔整)
                    {
                        scrollPictureControl1.Ratio =0.9f;
                    }
                    else if (currentAppType != APP_TYPE.柔整)
                    {
                        scrollPictureControl1.Ratio = 0.7f;
                    }
                    //scrollPictureControl1.Ratio = 1.0f;

                    //20190416213607 furukawa ed ////////////////////////


                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
                scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }
        #endregion

        #region 書面の種類を判定 20190421122625 furukawa 

        private void setInputKind(App app)
        {
            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    verifyBoxY.Text = clsInputKind.続紙;
                    break;
                case (int)APP_SPECIAL_CODE.不要:
                    verifyBoxY.Text = clsInputKind.不要;
                    break;
                case (int)APP_SPECIAL_CODE.エラー:
                    verifyBoxY.Text = clsInputKind.エラー;
                    break;
                case (int)APP_SPECIAL_CODE.長期:
                    verifyBoxY.Text = clsInputKind.長期;
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    verifyBoxY.Text = clsInputKind.施術同意書;

                    break;
                case (int)APP_SPECIAL_CODE.同意書裏:
                    verifyBoxY.Text = clsInputKind.施術同意書裏;
                    break;
                case (int)APP_SPECIAL_CODE.施術報告書:
                    verifyBoxY.Text = clsInputKind.施術報告書;
                    break;
                case (int)APP_SPECIAL_CODE.状態記入書:
                    verifyBoxY.Text = clsInputKind.状態記入書;
                    break;

                default:
                    break;

            }
        }
        #endregion
               
        #region 入力データを画面にロード
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App app)
        {


            //20190421122726 furukawa st ////////////////////////
            //書面判定を関数へ移行
            
            setInputKind(app);

            /*
                        //OCRチェックが済んだ画像の場合
                        if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
                        {
                            verifyBoxY.Text = "--";
                        }
                        else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
                        {
                            verifyBoxY.Text = "++";
                        }
                        else
                        {*/

            //20190421122726 furukawa ed ////////////////////////



            //申請書
            ChangeVisitControls(false);
            ChangeVisitKasanControls(false);

            //往療
            checkBoxVisit.Checked = app.Distance == 999;

            //20190421115934 furukawa st ////////////////////////
            //0以外の場合も発生するので0超の時のみ表示する

            if (app.VisitAdd > 0) verifyBoxKasanKm.Text = (app.VisitAdd / 1000m).ToString();
            //if (app.VisitAdd != 0) verifyBoxKasanKm.Text = (app.VisitAdd / 1000m).ToString();

            //20190421115934 furukawa ed ////////////////////////


            if (app.VisitFee != 0) verifyBoxOryoCost.Text = app.VisitFee.ToString();
            if (app.VisitTimes != 0) verifyBoxKasanCost.Text = app.VisitTimes.ToString();

            //20190421114710 furukawa st ////////////////////////
            //あはきの場合、往料加算チェックが入ると距離を-1としたので、0未満の場合にチェックを入れる            
                checkBoxVisitKasan.Checked =
                    0 > app.VisitAdd + app.VisitFee + app.VisitTimes;
                              
                //  checkBoxVisitKasan.Checked =
                //      0 != app.VisitAdd + app.VisitFee + app.VisitTimes;                                
            //20190421114710 furukawa ed ////////////////////////

            //往療
            if (app.Distance == 999) checkBoxVisit.Checked = true;

            // 加算
            if (app.VisitAdd > 0)
            {
                decimal visitAdd = Convert.ToDecimal(app.VisitAdd) / 1000m;
                checkBoxVisitKasan.Checked = true;
                verifyBoxKasanKm.Text = string.Format("{0:0.#}", visitAdd);               
            }

            // 基本往療料
            if (app.VisitFee > 0)
            {
                checkBoxVisitKasan.Checked = true;
                verifyBoxOryoCost.Text = app.VisitFee.ToString();
            }

            // 加算往療料
            if (app.VisitTimes > 0)
            {
                checkBoxVisitKasan.Checked = true;
                verifyBoxKasanCost.Text = app.VisitTimes.ToString();
            }

            //部位数
            verifyBoxBuisu.Text = app.Bui >0 ? app.Bui.ToString():string.Empty;

            //初検年月
            if (app.FushoFirstDate1 != DateTime.MinValue)
            {
                verifyBoxF1FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString();
                verifyBoxF1FirstM.Text = app.FushoFirstDate1.Month.ToString();
            }

            //柔整師登録記号番号
            string strDrNum = app.DrNum.ToString();
            verifyBoxClinicNum.Text = app.DrNum.ToString();

            //20190422103817 furukawa st ////////////////////////
            //交付料有無フラグのロード

            if (currentAppType==APP_TYPE.鍼灸 || currentAppType==APP_TYPE.あんま)
                vcSejutu.Checked = app.TaggedDatas.flgKofuUmu;
            //20190422103817 furukawa ed ////////////////////////



            /* 兵庫県後期広域　負傷名なし　
            if (app.FushoFirstDate1.IsNullDate())
            {
                var ocr = app.OcrData.Split(',');
                if (ocr.Length > 40 && ocr[40] != null)
                {
                    var f = Fusho.Change(ocr[40]);
                    if (Fusho.BasicWordsCheck(f)) verifyBoxF1.Text = f;
                }
                if (ocr.Length > 57 && ocr[57] != null)
                {
                    var f = Fusho.Change(ocr[57]);
                    if (Fusho.BasicWordsCheck(f)) verifyBoxF2.Text = f;
                }
                if (ocr.Length > 74 && ocr[74] != null)
                {
                    var f = Fusho.Change(ocr[74]);
                    if (Fusho.BasicWordsCheck(f)) verifyBoxF3.Text = f;
                }
                if (ocr.Length > 91 && ocr[91] != null)
                {
                    var f = Fusho.Change(ocr[91]);
                    if (Fusho.BasicWordsCheck(f)) verifyBoxF4.Text = f;
                }
                if (!string.IsNullOrWhiteSpace(app.OcrData) && ocr[108] != null)
                {
                    var f = Fusho.Change(ocr[108]);
                    if (Fusho.BasicWordsCheck(f)) verifyBoxF5.Text = f;
                }
            }
            else
            {
                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;
            }
            */
            //  }
        }

        #endregion

        #region 入力可不可関数
        /// <summary>
        /// 入力可不可関数
        /// </summary>
        /// <param name="t"></param>
        /// <param name="b"></param>
        private void setCanInput(Control t,bool b)
        {
            if (t is TextBox) ((TextBox)t).ReadOnly = !b;
            else t.Enabled = b;

            t.TabStop = b;
            if (!b)
                t.BackColor = SystemColors.Menu;
            else
                t.BackColor = SystemColors.Info;
        }
        #endregion


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
           
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                case clsInputKind.不要:
                case clsInputKind.続紙:
                case clsInputKind.長期:

                case clsInputKind.施術同意書裏:
                case clsInputKind.施術報告書:
                case clsInputKind.状態記入書:
                case clsInputKind.施術同意書:
                    
                    //続紙、白バッジ、その他の場合、入力項目は無い
                    panelTotal.Visible = false;
                    break;

                default:
                    //申請書の場合
                    panelTotal.Visible = true;                 
                   
                    break;
            }
           
        }



        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            /* 兵庫県後期広域　負傷名なし　
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
            */
       }

        #region 往療料関連コントロール
        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisit.Checked;
            ChangeVisitControls(bEnable);
            ChangeVisitKasanControls(checkBoxVisitKasan.Checked && bEnable);
        }

        private void checkBoxVisitKasan_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisitKasan.Checked;
            ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitControls(bool bEnable)
        {
            checkBoxVisitKasan.Enabled = bEnable;
            if (checkBoxVisitKasan.Checked) ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitKasanControls(bool bEnable)
        {
            if (currentAppType != APP_TYPE.柔整) bEnable = false;
            
                labelOuryo.Enabled = bEnable;
                labelOuryo2.Enabled = bEnable;
                verifyBoxKasanKm.Enabled = bEnable;
                ChangeVisitKasanExtraControls(bEnable);
            
        }

        private void ChangeVisitKasanExtraControls(bool bEnable)
        {
            labelOryoBasic.Enabled = bEnable;
            labelOryoKasan.Enabled = bEnable;
            verifyBoxOryoCost.Enabled = bEnable;
            verifyBoxKasanCost.Enabled = bEnable;
        }

        #endregion

        #region 画像関連ボタン
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        #endregion

        #region 画像表示コントロール

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var t = (Control)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
        }
        #endregion

        #region 戻るボタン
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
        #endregion

    }
}
