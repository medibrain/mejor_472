﻿namespace Mejor.NagoyaMokuzai
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.labelF1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.verifyBoxFushoCount = new Mejor.VerifyBox();
            this.label42 = new System.Windows.Forms.Label();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.label41 = new System.Windows.Forms.Label();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.labelFamily = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxBirthE = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxSeikyu = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxHnumN = new Mejor.VerifyBox();
            this.label34 = new System.Windows.Forms.Label();
            this.verifyBoxHnumS = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.verifyBoxKohi = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(692, 695);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 56;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(115, 20);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(165, 20);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(359, 8);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(29, 24);
            this.labelHnum.TabIndex = 11;
            this.labelHnum.Text = "被保\r\n記番";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(6, 9);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(47, 24);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(53, 20);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(29, 12);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(552, 76);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 33;
            this.labelTotal.Text = "合計";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 695);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 695);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(26, 695);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 695);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.labelF1);
            this.panelRight.Controls.Add(this.label17);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.label23);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.verifyBoxF1FirstY);
            this.panelRight.Controls.Add(this.verifyBoxRatio);
            this.panelRight.Controls.Add(this.verifyBoxF1FirstM);
            this.panelRight.Controls.Add(this.verifyBoxFushoCount);
            this.panelRight.Controls.Add(this.label42);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.verifyBoxF1FirstD);
            this.panelRight.Controls.Add(this.label41);
            this.panelRight.Controls.Add(this.verifyBoxF1Start);
            this.panelRight.Controls.Add(this.labelFamily);
            this.panelRight.Controls.Add(this.label9);
            this.panelRight.Controls.Add(this.verifyBoxF1Finish);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.label7);
            this.panelRight.Controls.Add(this.label35);
            this.panelRight.Controls.Add(this.label15);
            this.panelRight.Controls.Add(this.verifyBoxBirthE);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.label14);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.label36);
            this.panelRight.Controls.Add(this.label13);
            this.panelRight.Controls.Add(this.label37);
            this.panelRight.Controls.Add(this.label12);
            this.panelRight.Controls.Add(this.verifyBoxBirthY);
            this.panelRight.Controls.Add(this.label38);
            this.panelRight.Controls.Add(this.verifyBoxBirthM);
            this.panelRight.Controls.Add(this.verifyBoxBirthD);
            this.panelRight.Controls.Add(this.label39);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.verifyBoxSeikyu);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.verifyBoxHnumN);
            this.panelRight.Controls.Add(this.label34);
            this.panelRight.Controls.Add(this.verifyBoxHnumS);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.verifyBoxKohi);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 722);
            this.panelRight.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 650);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 38;
            this.label16.Text = "負傷1";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 58;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // labelF1
            // 
            this.labelF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(38, 629);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(41, 12);
            this.labelF1.TabIndex = 39;
            this.labelF1.Text = "負傷名";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(471, 77);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 31;
            this.label17.Text = "負傷数";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(633, 643);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDays.TabIndex = 55;
            this.verifyBoxDays.TextV = "";
            this.verifyBoxDays.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label23.Location = new System.Drawing.Point(580, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 24);
            this.label23.TabIndex = 16;
            this.label23.Text = "本人:2\r\n家族:6";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(38, 644);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF1.TabIndex = 40;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(287, 643);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 42;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(666, 9);
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRatio.TabIndex = 18;
            this.verifyBoxRatio.TextV = "";
            this.verifyBoxRatio.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(332, 643);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 44;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxFushoCount
            // 
            this.verifyBoxFushoCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFushoCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFushoCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFushoCount.Location = new System.Drawing.Point(512, 66);
            this.verifyBoxFushoCount.MaxLength = 1;
            this.verifyBoxFushoCount.Name = "verifyBoxFushoCount";
            this.verifyBoxFushoCount.NewLine = false;
            this.verifyBoxFushoCount.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxFushoCount.TabIndex = 32;
            this.verifyBoxFushoCount.TextV = "";
            this.verifyBoxFushoCount.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(592, 654);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 12);
            this.label42.TabIndex = 54;
            this.label42.Text = "実日数";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(554, 9);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 15;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(377, 643);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 46;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(637, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 12);
            this.label41.TabIndex = 17;
            this.label41.Text = "割合";
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(455, 643);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 49;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(519, 8);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(35, 24);
            this.labelFamily.TabIndex = 14;
            this.labelFamily.Text = "本人/\r\n家族";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(528, 628);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 51;
            this.label9.Text = "終了日";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(530, 643);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 52;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(106, 76);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 22;
            this.labelBirthday.Text = "生年月日";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(453, 628);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "開始日";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label35.Location = new System.Drawing.Point(185, 64);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 24);
            this.label35.TabIndex = 24;
            this.label35.Text = "昭：3\r\n平：4";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(558, 654);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 53;
            this.label15.Text = "日";
            // 
            // verifyBoxBirthE
            // 
            this.verifyBoxBirthE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthE.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthE.Location = new System.Drawing.Point(159, 65);
            this.verifyBoxBirthE.Name = "verifyBoxBirthE";
            this.verifyBoxBirthE.NewLine = false;
            this.verifyBoxBirthE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBirthE.TabIndex = 23;
            this.verifyBoxBirthE.TextV = "";
            this.verifyBoxBirthE.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 628);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 41;
            this.label5.Text = "初検日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(7, 76);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 19;
            this.labelSex.Text = "性別";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(483, 654);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 50;
            this.label14.Text = "日";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(36, 65);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 20;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(315, 654);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 43;
            this.label11.Text = "年";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label36.Location = new System.Drawing.Point(63, 64);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 24);
            this.label36.TabIndex = 21;
            this.label36.Text = "男: 1\r\n女: 2";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(405, 654);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 47;
            this.label13.Text = "日";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(246, 76);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 26;
            this.label37.Text = "年";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(360, 654);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 45;
            this.label12.Text = "月";
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(214, 65);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthY.TabIndex = 25;
            this.verifyBoxBirthY.TextV = "";
            this.verifyBoxBirthY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(295, 76);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 28;
            this.label38.Text = "月";
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(263, 65);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 27;
            this.verifyBoxBirthM.TextV = "";
            this.verifyBoxBirthM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(312, 65);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 29;
            this.verifyBoxBirthD.TextV = "";
            this.verifyBoxBirthD.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(344, 76);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 30;
            this.label39.Text = "日";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 135);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 483);
            this.scrollPictureControl1.TabIndex = 37;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(596, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 57;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(274, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "市町村:1\r\n老人:2";
            // 
            // verifyBoxSeikyu
            // 
            this.verifyBoxSeikyu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSeikyu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSeikyu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSeikyu.Location = new System.Drawing.Point(702, 65);
            this.verifyBoxSeikyu.Name = "verifyBoxSeikyu";
            this.verifyBoxSeikyu.NewLine = false;
            this.verifyBoxSeikyu.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxSeikyu.TabIndex = 36;
            this.verifyBoxSeikyu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSeikyu.TextV = "";
            this.verifyBoxSeikyu.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(581, 65);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 34;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxHnumN
            // 
            this.verifyBoxHnumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumN.Location = new System.Drawing.Point(440, 9);
            this.verifyBoxHnumN.Name = "verifyBoxHnumN";
            this.verifyBoxHnumN.NewLine = false;
            this.verifyBoxHnumN.Size = new System.Drawing.Size(67, 23);
            this.verifyBoxHnumN.TabIndex = 13;
            this.verifyBoxHnumN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumN.TextV = "";
            this.verifyBoxHnumN.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(673, 76);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 35;
            this.label34.Text = "請求";
            // 
            // verifyBoxHnumS
            // 
            this.verifyBoxHnumS.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumS.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumS.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumS.Location = new System.Drawing.Point(388, 9);
            this.verifyBoxHnumS.Name = "verifyBoxHnumS";
            this.verifyBoxHnumS.NewLine = false;
            this.verifyBoxHnumS.Size = new System.Drawing.Size(52, 23);
            this.verifyBoxHnumS.TabIndex = 12;
            this.verifyBoxHnumS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumS.TextV = "";
            this.verifyBoxHnumS.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "公費\r\n(扶助)";
            // 
            // verifyBoxKohi
            // 
            this.verifyBoxKohi.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKohi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKohi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKohi.Location = new System.Drawing.Point(242, 9);
            this.verifyBoxKohi.MaxLength = 1;
            this.verifyBoxKohi.Name = "verifyBoxKohi";
            this.verifyBoxKohi.NewLine = false;
            this.verifyBoxKohi.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxKohi.TabIndex = 7;
            this.verifyBoxKohi.TextV = "";
            this.verifyBoxKohi.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(82, 9);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(132, 9);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 10;
            this.splitter2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(327, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "請求\r\nコード";
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnumS;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Label labelYearInfo;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private VerifyBox verifyBoxKohi;
        private VerifyBox verifyBoxHnumN;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1Finish;
        private VerifyBox verifyBoxF1Start;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxSeikyu;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label35;
        private VerifyBox verifyBoxBirthE;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label labelFamily;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private VerifyBox verifyBoxFushoCount;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label1;
    }
}