﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Mejor.Pay;

namespace Mejor.NagoyaMokuzai
{
    class PayCodeImporter
    {
        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Title = "";
                f.Filter = "支払先マスター一覧|*支払先マスター一覧.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データベースの情報を取り込んでいます");
                var dic = new Dictionary<string, PayCode>();
                var l = DB.Main.SelectAll<PayCode>();
                foreach (var item in l) dic.Add(item.Key, item);

                wf.LogPrint("Excelファイルを読み込んでいます");

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    var excel = System.IO.Path.GetExtension(fileName);

                    if (excel == ".xlsx")
                    {
                        var xlsx = new XSSFWorkbook(fs);

                        var sheet = xlsx.GetSheetAt(0);
                        var rowCount = sheet.LastRowNum + 2;

                        for (int i = 0; i < rowCount; i++)
                        {
                            try
                            {
                                var row = sheet.GetRow(i);
                                if (row == null) continue;
                                var c = row.GetCell(0);
                                if (c == null) continue;
                                if (c.CellType != CellType.Numeric) continue;

                                var row2 = sheet.GetRow(i + 1);
                                var row3 = sheet.GetRow(i + 2);

                                var p = new PayCode();
                                p.Code = row.GetCell(0).NumericCellValue.ToString();
                                p.UpdateDate = 0;
                                p.BankNo = row.GetCell(5).StringCellValue.Remove(4);
                                p.BankName = row.GetCell(5).StringCellValue.Substring(5).Trim();
                                p.BranchNo = row2.GetCell(5).StringCellValue.Remove(3).Trim();
                                p.BranchName = row2.GetCell(5).StringCellValue.Substring(4).Trim();
                                p.AccountNo = row3.GetCell(4).NumericCellValue.ToString("0000000");
                                p.AccountName = row.GetCell(2).StringCellValue.Trim();
                                p.AccountKana = Utility.ToNarrowKatakana(row.GetCell(7).StringCellValue.Trim());

                                if (dic.ContainsKey(p.Key))
                                {
                                    if (dic[p.Key].UpdateDate > p.UpdateDate) continue;
                                    dic[p.Key] = p;
                                }
                                else
                                {
                                    dic.Add(p.Key, p);
                                }
                                i += 2;
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show(i.ToString() + ": " + ex.Message);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("対応しているのはExcel2007ファイル形式(.xlsx)です。");
                    }
                }

                wf.SetMax(dic.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {
                    if (!DB.Main.Excute("DELETE FROM paycode", null, tran))
                    {
                        MessageBox.Show("インポートに失敗しました");
                        return false;
                    }

                    var g = dic.Values
                        .Select((p, i) => new { Index = i, PayCode = p })
                        .GroupBy(x => x.Index / 10)
                        .Select(gr => gr.Select(x => x.PayCode));

                    foreach (var item in g)
                    {
                        wf.InvokeValue += item.Count();
                        if (!DB.Main.Inserts(item, tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
                MessageBox.Show("インポートが正常に終了しました");
            }

            return true;
        }


        public static List<PayCode> Select(string accountNo)
        {
            if (accountNo.Length < 7) return new List<PayCode>();
            return DB.Main.Select<PayCode>($"accountno='{accountNo}'").ToList();
        }
    }
}
