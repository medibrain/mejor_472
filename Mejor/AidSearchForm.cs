﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class AidSearchForm : Form
    {
        public AidSearchForm()
        {
            InitializeComponent();
            Text = Insurer.CurrrentInsurer.InsurerName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id;
            if (!int.TryParse(textBox1.Text, out id))
            {
                textBox2.Text = "IDが認識できません。";
            }
            else
            {
                var a = App.GetApp(id);
                if (a == null)
                {
                    textBox2.Text = "指定されたIDの情報はありません。";
                }
                else
                {
                    textBox2.Text = "内部ID:\t" + a.Aid.ToString() + "\r\n" +
                        "処理年月:\t" + a.ChargeYear.ToString() + "/" + a.ChargeMonth.ToString() + "\r\n" +
                        "スキャンID:\t" + a.ClinicNum.ToString() + "\r\n" +
                        "グループID:\t" + a.GroupID.ToString() + "\r\n" +
                        "被保記番:\t" + a.HihoNum + "\r\n" +
                        "患者氏名:\t" + a.PersonName.ToString() + "\r\n" +
                        "生年月日:\t" + DateTimeEx.ToJDateShortStr(a.Birthday);
                }

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id;
            if (!int.TryParse(textBox1.Text, out id))
            {
                textBox2.Text = "IDが認識できません。";
            }
            else
            {
                var app = App.GetApp(id);
                if (app == null)
                {
                    textBox2.Text = "指定されたIDの情報はありません。";
                }
                else
                {
                    InputStarter.Start(app.GroupID, INPUT_TYPE.First, app.Aid);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id;
            if (!int.TryParse(textBox1.Text, out id))
            {
                textBox2.Text = "IDが認識できません。";
                return;
            }

            var app = App.GetApp(id);
            if (app == null)
            {
                textBox2.Text = "指定されたIDの情報はありません。";
                return;
            }
            using (var f = new ListCreate.ListCreateForm(app))
            {
                f.ShowDialog();
            }
        }
    }
}
