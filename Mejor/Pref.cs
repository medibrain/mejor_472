﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    public class Pref
    {
        static Dictionary<int, Pref> dic = new Dictionary<int, Pref>();

        public int ID { get; private set; }
        public string Name { get; private set; }
        public string Name2 { get; set; }

        public static bool GetPrefList()
        {
            var sql = "SELECT * FROM pref";
            var db = new DB("common");
            var result = db.Query<Pref>(sql);
            if(result != null)
            {
                result.ToList().ForEach(p => dic.Add(p.ID, p));
                return true;
            }
            return false;
        }

        /// <summary>
        /// 都道府県番号から都道府県情報を取得します。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Pref GetPref(int id)
        {
            if (!dic.ContainsKey(id)) return null;
            return dic[id];
        }
    }
}
