﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Mejor
{
    public partial class UserControlSearchV : UserControl
    {
        public void SetCym(int cym)
        {
            textBoxCymFrom.Text = cym.ToString();
            textBoxCymTo.Text = cym.ToString();
        }

        public UserControlSearchV()
        {
            InitializeComponent();

            groupBoxBank.Enabled = false;
            groupBoxOryoSai.Enabled = false;

            insurerID = Insurer.CurrrentInsurer?.InsurerID ?? 0;
            switch (insurerID)
            {
                case (int)Mejor.InsurerID.OSAKA_KOIKI:
                    //groupBoxMerge.Enabled = true;
                    //checkBoxMerge1.Checked = true;
                    //checkBoxMerge2.Checked = false;
                    groupBoxOryoSai.Enabled = true;
                    break;
                default:
                    if ((Insurer.CurrrentInsurer?.InsurerType ?? INSURER_TYPE.指定なし) == INSURER_TYPE.学校共済)
                    {
                        groupBoxBank.Enabled = true;
                        textBoxBatch.Enabled = true;
                        textBoxBankAccount.Enabled = true;
                        labelBatch.Enabled = true;
                        labelBankAccount.Enabled = true;
                    }
                    break;
            }

            void checkBoxAdjust(GroupBox gb)
            {
                foreach (var item in gb.Controls)
                {
                    if (item is CheckBox == false) continue;
                    ((CheckBox)item).CheckedChanged += GroupCheckBox_CheckedChanged;
                }
            }

            checkBoxAdjust(groupBoxShinki);
            checkBoxAdjust(groupBoxVisit);
            checkBoxAdjust(groupBoxOryoGigi);
            checkBoxAdjust(groupBoxOryoSai);
            checkBoxAdjust(groupBoxAge);
            checkBoxAdjust(groupBoxLastM);
            checkBoxAdjust(groupBoxSaishinsa);
            checkBoxAdjust(groupBoxKago);
            checkBoxAdjust(groupBoxSyokai);
            checkBoxAdjust(groupBoxTenken);
            checkBoxAdjust(groupBoxOryoTenken);
            checkBoxAdjust(groupBoxHenrei);
            checkBoxAdjust(groupBoxPend);
            checkBoxAdjust(groupBoxInputError);
            checkBoxAdjust(groupBoxMemo);
            checkBoxAdjust(groupBoxTel);
            checkBoxAdjust(groupBoxShiharai);
            checkBoxAdjust(groupBoxProcess1);
            checkBoxAdjust(groupBoxProcess2);
            checkBoxAdjust(groupBoxBank);
            checkBoxAdjust(groupBoxBlack);
            checkBoxAdjust(groupBoxShokaiHen);
        }

        /// <summary>
        /// 同じグループボックス内の他のチェックボックスのチェックを外します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GroupCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var ck = (CheckBox)sender;
            if (!ck.Checked) return;
            var gb = ck.Parent;
            if (gb == null) return;

            foreach (Control item in gb.Controls)
            {
                if (item == sender) continue;
                if (item is CheckBox == false) continue;
                ((CheckBox)item).Checked = false;
            }
        }

        public bool ParamOryo => checkBoxOryoGigi1.Checked;

        //20210616190641 furukawa st ////////////////////////
        //神崎さんより要望
        

        //2021/06/16　神崎さんより
        //1140362　は、日本柔道整復師会の前身の団体です。
        //2000709　は、日本柔道整復師会で、昔3カ月まとめた申請書を出していたため、例外案件として除外対象としていたことが分かりました。
        //2021/06/16現在は、この団体も1カ月ごとに申請書を提出しているため、除外対象から外したいと思います。
        //それで、1140362、2000709をリストから除外して頂けるでしょうか。
        //結果、柔整総研のみを「特定口座外」のリストとしたいと思います。
        //柔整総研　柔整口座　1621064
        //柔整総研　あはき口座　3010648

        public string[] PARAM_GAKKO_BANK_NUMBERS = { "1621064", "3010648" };
        //public string[] PARAM_GAKKO_BANK_NUMBERS = { "1140362", "1621064", "2000709" };

        //20210616190641 furukawa ed ////////////////////////


        public bool Jyu
        {
            set => checkBoxJyu.Checked = value;
        }

        public bool Hari
        {
            set => checkBoxHari.Checked = value;
        }

        public bool Massage
        {
            set => checkBoxMassage.Checked = value;
        }

        public bool Lastm
        {
            set => checkBoxLast1.Checked = value;
        }

        public bool Gigi
        {
            set => checkBoxGigi2.Checked = true;
        }

        public bool Kago
        {
            set => checkBoxKago2.Checked = value;
        }

        public string HihoNum
        {
            set => textBoxNum.Text = value;
        }

        private int insurerID;

        /// <summary>
        /// 現在指定されている検索条件から、照会理由に該当する条件を取得します
        /// </summary>
        /// <returns></returns>
        public ShokaiReason GetSearchCondition()
        {
            var flag = ShokaiReason.なし;
            if (!string.IsNullOrWhiteSpace(textBoxBui.Text)) flag |= ShokaiReason.部位数;
            if (!string.IsNullOrWhiteSpace(textBoxDays.Text)) flag |= ShokaiReason.施術日数;
            if (!string.IsNullOrWhiteSpace(textBoxClinicNum.Text)) flag |= ShokaiReason.疑義施術所;
            if (!string.IsNullOrWhiteSpace(textBoxTotalMin.Text)) flag |= ShokaiReason.合計金額;
            if (!string.IsNullOrWhiteSpace(textBoxCont.Text)) flag |= ShokaiReason.施術期間;
            return flag;
        }

        /// <summary>
        /// 指定されている条件の申請書を取得します
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public List<App> GetApps()
        {
            string selectCont, from;
            var where = createSQL(out selectCont, out from);
            var apps = App.InspectSelect(selectCont + from + where);

            if (string.IsNullOrWhiteSpace(where)) return new List<App>();

            //重複削除 最新診療月
            if (checkBoxLastYm1.Checked)
            {
                var l = new List<App>();
                var g = apps.GroupBy(x => new { x.HihoNum, x.Birthday });
                foreach (var item in g)
                {
                    var lastYM = item.Max(x => x.YM);
                    l.AddRange(item.Where(x => x.YM == lastYM));
                }
                apps = l;
            }
            else if (checkBoxLastYm2.Checked)
            {
                var l = new List<App>();
                var g = apps.GroupBy(x => new { x.HihoNum, x.Birthday });
                foreach (var item in g)
                {
                    var lastYM = item.Max(x => x.YM);
                    l.AddRange(item.Where(x => x.YM != lastYM));
                }
                apps = l;
            }

            return apps;
        }

        /// <summary>
        /// 抽出用SQL文を生成します
        /// </summary>
        /// <returns></returns>
        private string createSQL(out string selectCont, out string from)
        {
            //請求年月
            string cymWhere = string.Empty;
            int.TryParse(textBoxCymFrom.Text, out int fromCym);
            int.TryParse(textBoxCymTo.Text, out int toCym);
            if (fromCym < 200001 || toCym < 200001)
            {
                MessageBox.Show("処理年月の範囲を西暦年月数字6桁で正しく指定して下さい", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                selectCont = string.Empty;
                from = string.Empty;
                return string.Empty;
            }

            if (fromCym == toCym)
            {
                cymWhere = $"a.cym={fromCym}";
            }
            else
            {
                cymWhere = $"a.cym BETWEEN {fromCym} AND {toCym}";
            }

            //バッチ/アカウント一覧
            List<App> _batchList = null;
            List<App> getBatchList()
            {
                if (_batchList != null) return _batchList;
                var sql = $" FROM application AS a WHERE {cymWhere} AND ym={(int)APP_SPECIAL_CODE.バッチ}";

                _batchList = App.InspectSelect(sql);
                return _batchList;
            }

            //List<App> _accountList = null;
            //学校共済大阪支部の扱い変更による調整
            //List<App> getAccountList()
            //{
            //    if (insurerID != (int)InsurerID.OSAKA_GAKKO) return getBatchList();
            //    if (_accountList != null) return _accountList;

            //    var sql = $" FROM application AS a WHERE {cymWhere} AND ym={(int)APP_SPECIAL_CODE.バッチ2}";
            //    _accountList = App.InspectSelect(sql);
            //    return _accountList;
            //}
            List<App> getAccountList() => getBatchList();


            //関連申請書に誕生日を検索条件としてつけるか
            //現在広域かどうかで判断
            var birthIgnore = Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.後期高齢;

            var whereList = new List<string>();
            whereList.Add(cymWhere);
            selectCont = "";
            from = " FROM application AS a ";

            if (checkBoxOryoSai1.Checked || checkBoxOryoSai2.Checked)
            {
                selectCont = ", ky.\"statusCode\", ky.\"calcDistance\" ";
                from += ", kyori_data AS ky ";
                whereList.Add("a.aid=ky.aid ");
            }

            //合計金額
            int min = GetIntValue(textBoxTotalMin.Text);
            int max = GetIntValue(textBoxTotalMax.Text);
            if (min != 0 && max != 0)
            {
                whereList.Add("a.atotal BETWEEN " + min.ToString() + " AND " + max.ToString());
            }
            else if (min != 0)
            {
                whereList.Add("a.atotal >= " + min.ToString());
            }
            else if (max != 0)
            {
                whereList.Add("a.atotal <= " + max.ToString());
            }

            //被保番
            //複数ある場合はスペースとタブ（Excelからコピペ用）で区切って入力
            if (textBoxNum.Text.Length > 32700)
                MessageBox.Show("被保番の指定が非常に長いため、" +
                    "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            var temps = Regex.Split(textBoxNum.Text, @"\s");
            var vals = new List<string>();
            foreach (var item in temps)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                vals.Add("'" + item + "'");
            }
            if (vals.Count > 0)
            {
                whereList.Add("a.hnum IN ( " + string.Join(", ", vals) + " )");
            }

            //施術所コード
            if (textBoxClinicNum.Text.Length > 32700)
                MessageBox.Show("施術所コードの指定が非常に長いため、" +
                    "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            temps = Regex.Split(textBoxClinicNum.Text, @"\s");
            vals.Clear();
            foreach (var item in temps)
            {
                if (item == "") break;
                vals.Add("'" + item + "'");
            }
            if (vals.Count > 0)
            {
                whereList.Add("a.sid IN ( " + string.Join(", ", vals) + " )");
            }

            //施術師コード
            if (textBoxDrNum.Text.Length > 32700)
                MessageBox.Show("施術師コードの指定が非常に長いため、" +
                    "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            temps = Regex.Split(textBoxDrNum.Text, @"\s");
            vals.Clear();
            foreach (var item in temps)
            {
                if (item == "") break;
                vals.Add("'" + item + "'");
            }
            if (vals.Count > 0)
            {
                whereList.Add("a.sregnumber IN ( " + string.Join(", ", vals) + " )");
            }

            //種別
            vals.Clear();
            if (checkBoxJyu.Checked) vals.Add(((int)APP_TYPE.柔整).ToString());
            if (checkBoxHari.Checked) vals.Add(((int)APP_TYPE.鍼灸).ToString());
            if (checkBoxMassage.Checked) vals.Add(((int)APP_TYPE.あんま).ToString());

            if (vals.Count != 0)
                whereList.Add("a.aapptype IN ( " + string.Join(", ", vals) + " )");

            //部位数
            if (textBoxBui.Text != "")
            {
                int bui;
                int.TryParse(textBoxBui.Text.Trim(), out bui);



                //20190620140049 furukawa st ////////////////////////
                //入力負傷数にて検索する機能追加

                switch (chkInputCount.Checked)
                {
                    case true:
                        if (bui > 0)
                        {
                            whereList.Add("a.taggedDatas ~'count:.[" + bui + "-5]'");
                        }

                        break;
                    case false:

                        if (bui == 0)
                        {
                            whereList.Add("(a.iname1 = '' OR a.iname1 = ')')");
                        }
                        else if (bui == 1)
                        {
                            whereList.Add("a.iname1 != '' AND a.iname1 != ')'");
                        }
                        else if (bui == 2)
                        {
                            whereList.Add("a.iname2 != '' AND a.iname2 != ')'");
                        }
                        else if (bui == 3)
                        {
                            whereList.Add("a.iname3 != '' AND a.iname3 != ')'");
                        }
                        else if (bui == 4)
                        {
                            whereList.Add("a.iname4 != '' AND a.iname4 != ')'");
                        }
                        else if (bui == 5)
                        {
                            whereList.Add("a.iname5 != '' AND a.iname5 != ')'");
                        }
                        break;
                }

                //if (bui == 0)
                //{
                //    whereList.Add("(a.iname1 = '' OR a.iname1 = ')')");
                //}
                //else if (bui == 1)
                //{
                //    whereList.Add("a.iname1 != '' AND a.iname1 != ')'");
                //}
                //else if (bui == 2)
                //{                   
                //    whereList.Add("a.iname2 != '' AND a.iname2 != ')'");                    
                //}
                //else if (bui == 3)
                //{
                //    whereList.Add("a.iname3 != '' AND a.iname3 != ')'");
                //}
                //else if (bui == 4)
                //{
                //    whereList.Add("a.iname4 != '' AND a.iname4 != ')'");
                //}
                //else if (bui == 5)
                //{
                //    whereList.Add("a.iname5 != '' AND a.iname5 != ')'");
                //}


                //20190620140049 furukawa ed ////////////////////////


            }

            //日数
            int days;
            int.TryParse(textBoxDays.Text.Trim(), out days);
            if (days > 0)
            {
                whereList.Add("a.acounteddays >= " + days.ToString());
            }

            //連続受診
            int.TryParse(textBoxCont.Text, out int contMonth);
            if (contMonth > 1)
            {
                contMonth -= 1;
                for (int i = 1; i < contMonth + 1; i++)
                {
                    string tn = "ac" + i.ToString();
                    string w = $"EXISTS(SELECT aid FROM application AS {tn} " +
                        $"WHERE a.hnum={tn}.hnum " +
                        (birthIgnore ? string.Empty : $"AND a.pbirthday = {tn}.pbirthday ") +
                        $"AND {tn}.ym=addmonth(a.ym, -{i})";
                    if (checkBoxFusho.Checked)
                    {
                        w += " AND (((a.iname1 <> '' AND a.iname1 IN(" + tn + ".iname1, " + tn + ".iname2, " + tn + ".iname3, " + tn + ".iname4, " + tn + ".iname5)) OR " +
                        "(a.iname2 <> '' AND a.iname2 IN(" + tn + ".iname1, " + tn + ".iname2, " + tn + ".iname3, " + tn + ".iname4, " + tn + ".iname5)) OR " +
                        "(a.iname3 <> '' AND a.iname3 IN(" + tn + ".iname1, " + tn + ".iname2, " + tn + ".iname3, " + tn + ".iname4, " + tn + ".iname5)) OR " +
                        "(a.iname4 <> '' AND a.iname4 IN(" + tn + ".iname1, " + tn + ".iname2, " + tn + ".iname3, " + tn + ".iname4, " + tn + ".iname5)) OR " +
                        "(a.iname5 <> '' AND a.iname5 IN(" + tn + ".iname1, " + tn + ".iname2, " + tn + ".iname3, " + tn + ".iname4, " + tn + ".iname5))))";
                    }
                    w += ")";
                    whereList.Add(w);
                }
            }

            //初検年月
            int firstYM;
            int.TryParse(textBoxFirstYM.Text.Trim(), out firstYM);
            if (firstYM != 0)
            {
                //20190701191855 furukawa st ////////////////////////
                //初検年月　西暦に

                if (firstYM.ToString().Length != 6)
                    MessageBox.Show("西暦YYYYMM形式で入力してください", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                var firstYYYYMM = firstYM;
                //var firstYYYYMM = DateTimeEx.GetAdYearMonthFromJyymm(4 * 10000 + firstYM);
                //20190701191855 furukawa ed ////////////////////////


                whereList.Add($"(a.ifirstdate1 != '0001-01-01' AND to_number(to_char(a.ifirstdate1, 'YYYYMM'),'000000') <= {firstYYYYMM})");
            }

            //施術年月
            int mediYM;
            int.TryParse(textBoxMediYM.Text.Trim(), out mediYM);
            if (mediYM != 0)
            {
                //20190701191947 furukawa st ////////////////////////
                //施術年月　西暦に

                if (mediYM.ToString().Length != 6)
                    MessageBox.Show("西暦YYYYMM形式で入力してください", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                var mediYYYYMM = mediYM;

                //var mediYYYYMM = DateTimeEx.GetAdYearMonthFromJyymm(4 * 10000 + mediYM);
                //20190701191947 furukawa ed ////////////////////////
                whereList.Add($"a.ym >= {mediYYYYMM}");
            }

            //ナンバリング
            if (!string.IsNullOrWhiteSpace(textBoxNumbering.Text))
            {
                if (textBoxNumbering.Text.Length > 32700)
                    MessageBox.Show("ナンバリングの指定が非常に長いため、" +
                        "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                temps = Regex.Split(textBoxNumbering.Text, @"\s");
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add("'" + item + "'");
                }
                if (vals.Count > 0)
                {
                    whereList.Add("a.numbering IN ( " + string.Join(", ", vals) + " )");
                }
            }

            //学校共済バッチ番号
            if (!string.IsNullOrWhiteSpace(textBoxBatch.Text))
            {
                if (textBoxBatch.Text.Length > 32700)
                    MessageBox.Show("バッチ番号の指定が非常に長いため、" +
                        "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                temps = Regex.Split(textBoxBatch.Text, @"\s");
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add(item);
                }

                var bl = getBatchList();
                var tempList = new List<string>();

                for (int i = 0; i < bl.Count; i++)
                {
                    //指定バッチ番号の範囲AIDのみ
                    if (vals.Exists(x => x == bl[i].Numbering))
                    {
                        tempList.Add($"a.aid BETWEEN {bl[i].Aid} AND " +
                            $"{(i + 1 == bl.Count ? int.MaxValue : bl[i + 1].Aid - 1)}");
                    }
                }

                if (tempList.Count == 1)
                {
                    whereList.Add(tempList[0]);
                }
                else if (tempList.Count > 1)
                {
                    whereList.Add("(" + string.Join(" OR ", tempList) + ")");
                }
                else if (tempList.Count == 0)
                {
                    whereList.Add("FALSE");
                }
            }

            //学校共済口座番号
            if (!string.IsNullOrWhiteSpace(textBoxBankAccount.Text))
            {
                if (textBoxBankAccount.Text.Length > 32700)
                    MessageBox.Show("口座番号の指定が非常に長いため、" +
                        "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                temps = Regex.Split(textBoxBankAccount.Text, @"\s");
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add(item);
                }

                var al = getAccountList();
                var tempList = new List<string>();

                for (int i = 0; i < al.Count; i++)
                {
                    //指定口座番号の範囲AIDのみ
                    if (vals.Exists(x => x == al[i].AccountNumber))
                    {
                        tempList.Add($"a.aid BETWEEN {al[i].Aid} AND " +
                            $"{(i + 1 == al.Count ? int.MaxValue : al[i + 1].Aid - 1)}");
                    }
                }

                if (tempList.Count == 1)
                {
                    whereList.Add(tempList[0]);
                }
                else if (tempList.Count > 1)
                {
                    whereList.Add("(" + string.Join(" OR ", tempList) + ")");
                }
                else if (tempList.Count == 0)
                {
                    whereList.Add("FALSE");
                }
            }

            // AID
            //複数ある場合はスペースで区切って入力
            if (textBoxAID.Text.Length > 32700)
                MessageBox.Show("AIDの指定が非常に長いため、" +
                                "指定された番号のすべてが検索対象になっていない可能性があります。お気を付けください",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            var aids = new List<string>();
            foreach (var item in Regex.Split(textBoxAID.Text, @"\s"))
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                aids.Add("'" + item + "'");
            }
            if (aids.Count > 0)
            {
                whereList.Add("a.aid IN ( " + string.Join(", ", aids) + " )");
            }

            //新規
            if (checkBoxNew2.Checked)
            {
                whereList.Add("a.fchargetype = 2");
            }
            else if (checkBoxNew1.Checked)
            {
                whereList.Add("a.fchargetype = 1");
            }

            //往診料
            if (checkBoxVisit2.Checked)
            {
                whereList.Add("a.fdistance != 999");
            }
            else if (checkBoxVisit1.Checked)
            {
                whereList.Add("a.fdistance = 999");
            }

            // 加算あり
            if (checkBoxVisitAdd1.Checked)
            {
                // 兵庫広域のあはきの場合は距離が入力されずに-1が入るので0かどうかで判定
                whereList.Add("a.fvisitadd != 0");
            }
            else if (checkBoxVisitAdd2.Checked)
            {
                whereList.Add("a.fvisitadd = 0");
            }

            //再審査
            if (checkBoxGigi1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.再審査})");
            }
            else if (checkBoxGigi2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.再審査})");
            }

            //過誤
            if (checkBoxKago1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.過誤})");
            }
            else if (checkBoxKago2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.過誤})");
            }

            //照会
            if (checkBoxSyokai1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.照会対象})");
            }
            else if (checkBoxSyokai2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.照会対象})");
            }

            //返戻
            if (checkBoxHenrei1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.返戻})");
            }
            else if (checkBoxHenrei2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.返戻})");
            }

            //支払保留
            if (checkBoxPend1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.支払保留})");
            }
            else if (checkBoxPend2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.支払保留})");
            }

            //点検対象
            int tf = (int)StatusFlag.点検対象;
            if (checkBoxTenken1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{tf})");
            }
            else if (checkBoxTenken2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{tf})");
            }

            //往診点検対象
            int of = (int)StatusFlag.往療点検対象;
            if (checkBoxOryoTenken1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{of})");
            }
            else if (checkBoxOryoTenken2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{of})");
            }

            //入力時エラー
            if (checkBoxInputError1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.入力時エラー})");
            }
            else if (checkBoxInputError2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.入力時エラー})");
            }

            //架電
            if (checkBoxTel1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.架電対象})");
            }
            else if (checkBoxTel2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.架電対象})");
            }

            //支払済み
            if (checkBoxShiharai1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.支払済})");
            }
            else if (checkBoxShiharai2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.支払済})");
            }

            //処理1
            if (checkBoxProcess11.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.処理1})");
            }
            else if (checkBoxProcess12.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.処理1})");
            }

            //処理2
            if (checkBoxProcess21.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.処理2})");
            }
            else if (checkBoxProcess22.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.処理2})");
            }

            //処理3
            if (checkBoxProcess31.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.処理3})");
            }
            else if (checkBoxProcess32.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.処理3})");
            }

            //処理4
            if (checkBoxProcess41.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.処理4})");
            }
            else if (checkBoxProcess42.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.処理4})");
            }

            ////処理5
            //if (checkBoxProcess21.Checked)
            //{
            //    whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.処理5})");
            //}
            //else if (checkBoxProcess22.Checked)
            //{
            //    whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.処理5})");
            //}

            //メモ
            if (checkBoxMemo1.Checked)
            {
                whereList.Add($"a.memo<>''");
            }
            else if (checkBoxMemo2.Checked)
            {
                whereList.Add($"a.memo=''");
            }

            // 出力メモ
            if (checkBoxOutMemo1.Checked)
            {
                whereList.Add("a.outmemo<>''");
            }
            else if (checkBoxOutMemo2.Checked)
            {
                whereList.Add("a.outmemo=''");
            }

            //照会除外リスト
            if (checkBoxBlack1.Checked)
            {
                whereList.Add("EXISTS(SELECT aid FROM shokaiexclude " +
                    "WHERE a.hnum = shokaiexclude.hihonum " +
                    "AND shokaiexclude.chargeym  = 999999 " +
                    "AND(shokaiexclude.birth<='0001-1-1' OR shokaiexclude.birth=pbirthday))");
            }
            else if (checkBoxBlack2.Checked)
            {
                whereList.Add("NOT EXISTS(SELECT aid FROM shokaiexclude " +
                    "WHERE a.hnum = shokaiexclude.hihonum " +
                    "AND shokaiexclude.chargeym  = 999999 " +
                    "AND(shokaiexclude.birth<='0001-1-1' OR shokaiexclude.birth=pbirthday))");
            }

            //負傷名・部位
            var buiSerchWords = new List<string>();
            if (checkBoxKubi.Checked) buiSerchWords.Add("頚");
            if (checkBoxKubi.Checked) buiSerchWords.Add("頸");
            if (checkBoxKata.Checked) buiSerchWords.Add("肩");
            if (checkBoxSe.Checked) buiSerchWords.Add("背");
            if (checkBoxKoshi.Checked) buiSerchWords.Add("腰");
            if (checkBoxHiza.Checked) buiSerchWords.Add("膝");
            if (buiSerchWords.Count != 0)
            {
                var likes = new List<string>();
                for (int i = 1; i <= 5; i++)
                {
                    foreach (var item in buiSerchWords)
                    {
                        likes.Add("a.iname" + i.ToString() + " LIKE '%" + item + "%'");
                    }
                }
                whereList.Add("(" + string.Join(" OR ", likes) + ")");
            }

            //年齢
            if (checkBoxAdult.Checked)
            {
                whereList.Add("date_part('year',age(current_date,a.pbirthday)) >= 20");//今日の時点で成年
            }
            else if (checkBoxMinor.Checked)
            {
                whereList.Add("date_part('year',age(current_date,a.pbirthday)) < 20");
            }

            //往療疑義判定用
            if (checkBoxOryoGigi1.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.往療疑義})");
            }
            else if (checkBoxOryoGigi2.Checked)
            {
                whereList.Add($"a.statusflags<>(a.statusflags|{(int)StatusFlag.往療疑義})");
            }

            //照会返信
            if (checkBoxShokaiHen1.Checked)
            {
                whereList.Add($"(a.shokaireason=(a.shokaireason|{(int)ShokaiReason.宛所なし}) OR " +
                    $"a.shokaireason=(a.shokaireason |{ (int)ShokaiReason.相違あり}) OR " +
                    $"a.shokaireason=(a.shokaireason|{(int)ShokaiReason.相違なし}))");
            }
            else if (checkBoxShokaiHen2.Checked)
            {
                whereList.Add($"a.statusflags=(a.statusflags|{(int)StatusFlag.照会対象})");
                whereList.Add($"a.shokaireason<>(a.shokaireason|{(int)ShokaiReason.宛所なし})");
                whereList.Add($"a.shokaireason<>(a.shokaireason|{(int)ShokaiReason.相違あり})");
                whereList.Add($"a.shokaireason<>(a.shokaireason|{(int)ShokaiReason.相違なし})");
            }

            //過去照会
            int.TryParse(textBoxKako.Text, out int kakoShokai);
            if (kakoShokai != 0)
            {
                if (fromCym != toCym)
                {
                    if (textBoxBankAccount.Text.Length > 32700)
                        MessageBox.Show(
                            "処理年月が範囲指定されているため、過去照会が適切に抽出できません。お気を付けください",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                whereList.Add("NOT EXISTS(SELECT aid FROM application AS a2 " +
                    "WHERE a.hnum = a2.hnum " +
                    $"AND a2.cym BETWEEN addmonth(a.cym,{-kakoShokai}) AND addmonth(a.cym,{-1}) " +
                    (birthIgnore ? string.Empty : "AND a.pbirthday = a2.pbirthday ") +
                    $"AND a2.statusflags=(a2.statusflags|{(int)StatusFlag.照会対象}))");

                if (checkBoxOutside.Checked)
                {
                    whereList.Add("NOT EXISTS(SELECT excludeid FROM shokaiexclude " +
                        "WHERE a.hnum = shokaiexclude.hihonum " +
                        $"AND shokaiexclude.chargeym BETWEEN addmonth(a.cym,{-kakoShokai}) AND  addmonth(a.cym,{-1}) " +
                        "AND(shokaiexclude.birth<='0001-1-1' OR shokaiexclude.birth=pbirthday))");
                }
            }

            //先月申請書
            //無しの場合は先頭に NOT
            if (checkBoxLast2.Checked)
            {
                whereList.Add("NOT EXISTS (SELECT aid FROM application AS a3 " +
                    "WHERE a.hnum=a3.hnum " +
                    (birthIgnore ? string.Empty : "AND a.pbirthday = a3.pbirthday ") +
                    "AND a3.ym=addmonth(a.ym, -1))");
            }
            else if (checkBoxLast1.Checked)
            {
                whereList.Add("EXISTS (SELECT aid FROM application AS a3 " +
                    "WHERE a.hnum=a3.hnum " +
                    (birthIgnore ? string.Empty : "AND a.pbirthday = a3.pbirthday ") +
                    "AND a3.ym=addmonth(a.ym, -1))");
            }

            //特定口座番号（学校共済）            
            if (checkBoxBank1.Checked)
            {
                var al = getAccountList();
                var tempList = new List<string>();

                for (int i = 0; i < al.Count; i++)
                {
                    //特定口座番号の範囲AIDのみ
                    if (Array.Exists(PARAM_GAKKO_BANK_NUMBERS, (x => x == al[i].AccountNumber)))
                    {
                        tempList.Add($"a.aid BETWEEN {al[i].Aid} AND " +
                            $"{(i + 1 == al.Count ? int.MaxValue : al[i + 1].Aid - 1)}");
                    }
                }

                if (tempList.Count == 1)
                {
                    whereList.Add(tempList[0]);
                }
                else if (tempList.Count > 1)
                {
                    whereList.Add("(" + string.Join(" OR ", tempList) + ")");
                }
                else if (tempList.Count == 0)
                {
                    whereList.Add("FALSE");
                }
            }
            else if (checkBoxBank2.Checked)
            {
                var al = getAccountList();
                var tempList = new List<string>();

                for (int i = 0; i < al.Count; i++)
                {
                    //特定口座の範囲AIDを除外
                    if (Array.Exists(PARAM_GAKKO_BANK_NUMBERS, x => x == al[i].AccountNumber))
                    {
                        whereList.Add($"a.aid NOT BETWEEN {al[i].Aid} AND " +
                            $"{(i + 1 == al.Count ? int.MaxValue : al[i + 1].Aid - 1)}");
                    }
                }
            }

            if (whereList.Count == 0) return string.Empty;
            return " WHERE " + string.Join(" AND ", whereList) + " ";
        }

        /// <summary>
        /// 失敗した場合、0が返ります。
        /// </summary>
        /// <returns></returns>
        public int GetIntValue(string txt)
        {
            int i;
            int.TryParse(txt, out i);
            return i;
        }
    }
}
