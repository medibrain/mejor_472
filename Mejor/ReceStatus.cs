﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    [Flags]
    public enum ShokaiReason
    {
        なし = 0,
        部位数 = 0x1,
        施術日数 = 0x2,
        施術期間 = 0x4,
        疑義施術所 = 0x8,
        合計金額 = 0x10,
        その他 = 0x1_0000,
        宛所なし = 0x10_0000,
        相違なし = 0x20_0000,
        相違あり = 0x40_0000
    }

    public enum SHOKAI_STATUS
    {
        なし = 0,
        照会中 = 1,
        未返信 = 2,
        相違なし = 3,
        相違あり = 4
    }

    [Flags]
    public enum SHOKAI_RESULT
    {
        なし = 0,
        負傷原因相違 = 0x1,
        施術部位相違 = 0x2,
        負傷年月相違 = 0x4,
        署名欄疑義 = 0x8,
        その他疑義 = 0x10,
        同意なし = 0x20,
        匿名同意 = 0x40
    }

    [Flags]
    public enum KAGO_REASON
    {
        なし = 0,
        署名違い = 0x01, 筆跡違い = 0x02, 家族同一筆跡 = 0x04,
        原因なし = 0x08, /*署名なし = 0x10,*/ 長期理由なし = 0x20, その他 = 0x40
    }

    [Flags]
    public enum SAISHINSA_REASON //再審査
    {
        なし = 0, 初検料疑義 = 0x01,
        その他 = 0x10000000
    }



    [Flags]

    public enum KagoReasons : long//longじゃないと追加分がサイズオーバーになる
                                  //public enum KagoReasons 
    {
        なし = 0,
        署名違い = 0x1, 筆跡違い = 0x2, 家族同一筆跡 = 0x4,
        原因なし = 0x8, 長期理由なし = 0x10, 負傷原因相違 = 0x20,
        その他 = 0x80,
        原因なし1 = 0x100, 原因なし2 = 0x200, 原因なし3 = 0x400, 原因なし4 = 0x800, 原因なし5 = 0x1000,
        長期理由なし1 = 0x2000, 長期理由なし2 = 0x4000, 長期理由なし3 = 0x8000, 長期理由なし4 = 0x10000, 長期理由なし5 = 0x20000,
        負傷原因相違1 = 0x40000, 負傷原因相違2 = 0x80000, 負傷原因相違3 = 0x100000, 負傷原因相違4 = 0x200000, 負傷原因相違5 = 0x400000,

        //20201001150310 furukawa st ////////////////////////
        //追加
        //追加したが、数が多すぎるのと今後のことを考えてxml型に変更を検討する
        療養費請求権の消滅時効 = 0x1_000000, 本家区分誤り = 0x2_000000,
        長期理由相違1 = 0x1_000_0000, 長期理由相違2 = 0x2_000_0000, 長期理由相違3 = 0x4_000_0000, 長期理由相違4 = 0x8_000_0000, 長期理由相違5 = 0x10_000_0000,
        保険者相違 = 0x1_0000_0000,
        往療理由記載なし = 0x2_0000_0000, 往療16km以上 = 0x4_0000_0000,
        同意書添付なし = 0x8_0000_0000, 同意書期限切れ = 0x10_0000_0000, //20210531165929 furukawa 期限切れでなく不備に変更したいが、ここを変更するとDBも変更しないといけないので放置
                                                          
        施術報告書添付なし = 0x20_0000_0000, 施術報告書記載不備 = 0x40_0000_0000,
        施術継続理由状態記入書添付なし = 0x80_0000_0000, 施術継続理由状態記入書記載不備 = 0x100_0000_0000,
        独自1 = 0x200_0000_0000, 独自2 = 0x400_0000_0000, 独自3 = 0x800_0000_0000, 独自4 = 0x1000_0000_0000, 独自5 = 0x2000_0000_0000,

        //20201001150310 furukawa ed ////////////////////////
    }


    //2020/10/05 furukawa
  
    public enum KagoReasons_Member                                  
    {
        初期化,
        なし,
        署名違い, 筆跡違い, 家族同一筆跡, その他,

        原因なし, 原因なし1, 原因なし2, 原因なし3, 原因なし4, 原因なし5,
        負傷原因相違, 負傷原因相違1, 負傷原因相違2, 負傷原因相違3, 負傷原因相違4, 負傷原因相違5,
        長期理由なし, 長期理由なし1, 長期理由なし2, 長期理由なし3, 長期理由なし4, 長期理由なし5,
        長期理由相違, 長期理由相違1, 長期理由相違2, 長期理由相違3, 長期理由相違4, 長期理由相違5,

        療養費請求権の消滅時効,
        本家区分誤り,        
        保険者相違,
        往療理由記載なし,
        往療16km以上,

        同意書, 同意書添付なし, 同意書期限切れ,//同意書期限切れ,　//20210531165615 furukawa 期限切れでなく不備に変更したいが、ここを変更するとDBも変更しないといけないので放置


        施術報告書, 施術報告書添付なし, 施術報告書記載不備,

        施術継続理由状態記入書,施術継続理由状態記入書添付なし, 施術継続理由状態記入書記載不備,

        往療内訳書,往療内訳書添付なし,往療内訳書記載不備,//20210201153923 furukawa 往療内訳書追加                                  

        独自, 独自1, 独自2, 独自3, 独自4, 独自5,
    }

    
    [Flags]
    public enum SaishinsaReasons
    {
        なし = 0,
        初検料 = 0x1, その他 = 0x2,

        負傷1 = 0x10, 負傷2 = 0x20, 負傷3 = 0x40,
        負傷4 = 0x80, 負傷5 = 0x100,

        転帰なし = 0x1000, 中止 = 0x2000,
        同一負傷名 = 0x4000, 別負傷名 = 0x8000,
    }

    [Flags]
    public enum HenreiReasons
    {
        なし = 0,
        負傷部位 = 0x1, 負傷原因 = 0x2, 負傷時期 = 0x4,
        けが外 = 0x8, 受療日数 = 0x10, 勤務中 = 0x20,
        過誤 = 0x40, その他 = 0x80,
        負傷部位1 = 0x100, 負傷部位2 = 0x200, 負傷部位3 = 0x400, 負傷部位4 = 0x800, 負傷部位5 = 0x1000,
        負傷原因1 = 0x2000, 負傷原因2 = 0x4000, 負傷原因3 = 0x8000, 負傷原因4 = 0x1_0000, 負傷原因5 = 0x2_0000,
    }

    [Flags]
    public enum ContactStatus
    {
        Null = 0,
        連絡不要 = 0x1, 団体 = 0x2,
    }


}
