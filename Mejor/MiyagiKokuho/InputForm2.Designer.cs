﻿namespace Mejor.MiyagiKokuho
{
    partial class InputForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelpname = new System.Windows.Forms.Panel();
            this.buttonZipSearch = new System.Windows.Forms.Button();
            this.verifyBoxHihozip = new Mejor.VerifyBox();
            this.verifyBoxHihoname = new Mejor.VerifyBox();
            this.verifyBoxHihoadd = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.panelClinic = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.verifyBoxHosName = new Mejor.VerifyBox();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.label34 = new System.Windows.Forms.Label();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelOCR = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.verifyBoxF5Name = new Mejor.VerifyBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.verifyBoxF1Name = new Mejor.VerifyBox();
            this.verifyBoxF2Name = new Mejor.VerifyBox();
            this.verifyBoxF3Name = new Mejor.VerifyBox();
            this.verifyBoxF4Name = new Mejor.VerifyBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelpname.SuspendLayout();
            this.panelClinic.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 753);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 250;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 759);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 781);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 781);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 754);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 754);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 754);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-196, 754);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 781);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelpname);
            this.panelRight.Controls.Add(this.panelClinic);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.verifyBoxF5Name);
            this.panelRight.Controls.Add(this.label40);
            this.panelRight.Controls.Add(this.label42);
            this.panelRight.Controls.Add(this.label43);
            this.panelRight.Controls.Add(this.label44);
            this.panelRight.Controls.Add(this.label45);
            this.panelRight.Controls.Add(this.verifyBoxF1Name);
            this.panelRight.Controls.Add(this.verifyBoxF2Name);
            this.panelRight.Controls.Add(this.verifyBoxF3Name);
            this.panelRight.Controls.Add(this.verifyBoxF4Name);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 781);
            this.panelRight.TabIndex = 2;
            // 
            // panelpname
            // 
            this.panelpname.Controls.Add(this.buttonZipSearch);
            this.panelpname.Controls.Add(this.verifyBoxHihozip);
            this.panelpname.Controls.Add(this.verifyBoxHihoname);
            this.panelpname.Controls.Add(this.verifyBoxHihoadd);
            this.panelpname.Controls.Add(this.label39);
            this.panelpname.Controls.Add(this.label38);
            this.panelpname.Controls.Add(this.label46);
            this.panelpname.Location = new System.Drawing.Point(3, 6);
            this.panelpname.Name = "panelpname";
            this.panelpname.Size = new System.Drawing.Size(900, 68);
            this.panelpname.TabIndex = 10;
            // 
            // buttonZipSearch
            // 
            this.buttonZipSearch.Location = new System.Drawing.Point(338, 24);
            this.buttonZipSearch.Name = "buttonZipSearch";
            this.buttonZipSearch.Size = new System.Drawing.Size(84, 25);
            this.buttonZipSearch.TabIndex = 26;
            this.buttonZipSearch.TabStop = false;
            this.buttonZipSearch.Text = "住所帳（F5）";
            this.buttonZipSearch.UseVisualStyleBackColor = true;
            this.buttonZipSearch.Click += new System.EventHandler(this.buttonZipSearch_Click);
            // 
            // verifyBoxHihozip
            // 
            this.verifyBoxHihozip.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihozip.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihozip.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHihozip.Location = new System.Drawing.Point(232, 26);
            this.verifyBoxHihozip.Name = "verifyBoxHihozip";
            this.verifyBoxHihozip.NewLine = false;
            this.verifyBoxHihozip.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHihozip.TabIndex = 19;
            this.verifyBoxHihozip.TextV = "";
            this.verifyBoxHihozip.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHihozip.Leave += new System.EventHandler(this.verifyBoxHihozip_Leave);
            // 
            // verifyBoxHihoname
            // 
            this.verifyBoxHihoname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihoname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihoname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHihoname.Location = new System.Drawing.Point(32, 26);
            this.verifyBoxHihoname.Name = "verifyBoxHihoname";
            this.verifyBoxHihoname.NewLine = false;
            this.verifyBoxHihoname.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxHihoname.TabIndex = 15;
            this.verifyBoxHihoname.TextV = "";
            this.verifyBoxHihoname.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxHihoadd
            // 
            this.verifyBoxHihoadd.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihoadd.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihoadd.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHihoadd.Location = new System.Drawing.Point(428, 26);
            this.verifyBoxHihoadd.Name = "verifyBoxHihoadd";
            this.verifyBoxHihoadd.NewLine = false;
            this.verifyBoxHihoadd.Size = new System.Drawing.Size(450, 23);
            this.verifyBoxHihoadd.TabIndex = 20;
            this.verifyBoxHihoadd.TextV = "";
            this.verifyBoxHihoadd.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHihoadd.Leave += new System.EventHandler(this.verifyBoxHihoadd_Leave);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(425, 8);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 13);
            this.label39.TabIndex = 17;
            this.label39.Text = "被保険者住所";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(232, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(103, 13);
            this.label38.TabIndex = 18;
            this.label38.Text = "被保険者郵便番号";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(30, 8);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 13);
            this.label46.TabIndex = 9;
            this.label46.Text = "被保険者名";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelClinic
            // 
            this.panelClinic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelClinic.Controls.Add(this.label32);
            this.panelClinic.Controls.Add(this.verifyBoxHosName);
            this.panelClinic.Controls.Add(this.verifyBoxDrCode);
            this.panelClinic.Controls.Add(this.label34);
            this.panelClinic.Controls.Add(this.labelNumbering);
            this.panelClinic.Controls.Add(this.verifyBoxDrName);
            this.panelClinic.Location = new System.Drawing.Point(9, 674);
            this.panelClinic.Name = "panelClinic";
            this.panelClinic.Size = new System.Drawing.Size(909, 80);
            this.panelClinic.TabIndex = 80;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(200, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 26);
            this.label32.TabIndex = 61;
            this.label32.Text = "施術\r\n所名";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHosName
            // 
            this.verifyBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHosName.Location = new System.Drawing.Point(233, 25);
            this.verifyBoxHosName.Name = "verifyBoxHosName";
            this.verifyBoxHosName.NewLine = false;
            this.verifyBoxHosName.Size = new System.Drawing.Size(238, 23);
            this.verifyBoxHosName.TabIndex = 130;
            this.verifyBoxHosName.TextV = "";
            this.verifyBoxHosName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(63, 25);
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(120, 23);
            this.verifyBoxDrCode.TabIndex = 120;
            this.verifyBoxDrCode.TextV = "";
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxDrCode.Leave += new System.EventHandler(this.verifyBoxDrCode_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(477, 22);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 26);
            this.label34.TabIndex = 59;
            this.label34.Text = "施術\r\n師名";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(62, 7);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(115, 13);
            this.labelNumbering.TabIndex = 57;
            this.labelNumbering.Text = "施術師登録記号番号";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(512, 25);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(160, 23);
            this.verifyBoxDrName.TabIndex = 140;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 80);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 137);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 480);
            this.scrollPictureControl1.TabIndex = 300;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 753);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(30, 13);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // verifyBoxF5Name
            // 
            this.verifyBoxF5Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5Name.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Name.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxF5Name.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5Name.Location = new System.Drawing.Point(72, 646);
            this.verifyBoxF5Name.Name = "verifyBoxF5Name";
            this.verifyBoxF5Name.NewLine = false;
            this.verifyBoxF5Name.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF5Name.TabIndex = 70;
            this.verifyBoxF5Name.TextV = "";
            this.verifyBoxF5Name.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF5Name.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(14, 579);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(49, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "負傷名1";
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(380, 579);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(49, 13);
            this.label42.TabIndex = 2;
            this.label42.Text = "負傷名2";
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(14, 615);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(49, 13);
            this.label43.TabIndex = 4;
            this.label43.Text = "負傷名3";
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(380, 614);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 13);
            this.label44.TabIndex = 6;
            this.label44.Text = "負傷名4";
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(14, 651);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 13);
            this.label45.TabIndex = 8;
            this.label45.Text = "負傷名5";
            // 
            // verifyBoxF1Name
            // 
            this.verifyBoxF1Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1Name.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Name.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxF1Name.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1Name.Location = new System.Drawing.Point(72, 574);
            this.verifyBoxF1Name.Name = "verifyBoxF1Name";
            this.verifyBoxF1Name.NewLine = false;
            this.verifyBoxF1Name.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF1Name.TabIndex = 30;
            this.verifyBoxF1Name.TextV = "";
            this.verifyBoxF1Name.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF1Name.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF2Name
            // 
            this.verifyBoxF2Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2Name.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Name.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxF2Name.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2Name.Location = new System.Drawing.Point(435, 572);
            this.verifyBoxF2Name.Name = "verifyBoxF2Name";
            this.verifyBoxF2Name.NewLine = false;
            this.verifyBoxF2Name.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF2Name.TabIndex = 35;
            this.verifyBoxF2Name.TextV = "";
            this.verifyBoxF2Name.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF2Name.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF3Name
            // 
            this.verifyBoxF3Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3Name.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Name.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxF3Name.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3Name.Location = new System.Drawing.Point(72, 608);
            this.verifyBoxF3Name.Name = "verifyBoxF3Name";
            this.verifyBoxF3Name.NewLine = false;
            this.verifyBoxF3Name.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF3Name.TabIndex = 40;
            this.verifyBoxF3Name.TextV = "";
            this.verifyBoxF3Name.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF3Name.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF4Name
            // 
            this.verifyBoxF4Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4Name.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Name.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verifyBoxF4Name.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4Name.Location = new System.Drawing.Point(435, 606);
            this.verifyBoxF4Name.Name = "verifyBoxF4Name";
            this.verifyBoxF4Name.NewLine = false;
            this.verifyBoxF4Name.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF4Name.TabIndex = 60;
            this.verifyBoxF4Name.TextV = "";
            this.verifyBoxF4Name.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF4Name.Enter += new System.EventHandler(this.item_Enter);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 781);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 781);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm2";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelpname.ResumeLayout(false);
            this.panelpname.PerformLayout();
            this.panelClinic.ResumeLayout(false);
            this.panelClinic.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label32;
        private VerifyBox verifyBoxHosName;
        private System.Windows.Forms.Label label34;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private System.Windows.Forms.Panel panelClinic;
        private VerifyBox verifyBoxF5Name;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private VerifyBox verifyBoxF1Name;
        private VerifyBox verifyBoxF2Name;
        private VerifyBox verifyBoxF3Name;
        private VerifyBox verifyBoxF4Name;
        private System.Windows.Forms.Panel panelpname;
        private VerifyBox verifyBoxHihozip;
        private VerifyBox verifyBoxHihoname;
        private VerifyBox verifyBoxHihoadd;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button buttonZipSearch;
    }
}