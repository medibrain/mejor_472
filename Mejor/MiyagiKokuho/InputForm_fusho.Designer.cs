﻿namespace Mejor.MiyagiKokuho
{
    partial class InputForm_fusho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.lblwareki = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelClinic = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.verifyBoxHosName = new Mejor.VerifyBox();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.label34 = new System.Windows.Forms.Label();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.panelFusho = new System.Windows.Forms.Panel();
            this.verifyBoxF5edD = new Mejor.VerifyBox();
            this.verifyBoxF4edD = new Mejor.VerifyBox();
            this.verifyBoxF3edD = new Mejor.VerifyBox();
            this.verifyBoxF2edD = new Mejor.VerifyBox();
            this.verifyBoxF1edD = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxF5stD = new Mejor.VerifyBox();
            this.verifyBoxF5skD = new Mejor.VerifyBox();
            this.verifyBoxF5fsD = new Mejor.VerifyBox();
            this.verifyBoxF4stD = new Mejor.VerifyBox();
            this.verifyBoxF4skD = new Mejor.VerifyBox();
            this.verifyBoxF4fsD = new Mejor.VerifyBox();
            this.verifyBoxF3stD = new Mejor.VerifyBox();
            this.verifyBoxF3skD = new Mejor.VerifyBox();
            this.verifyBoxF3fsD = new Mejor.VerifyBox();
            this.verifyBoxF2stD = new Mejor.VerifyBox();
            this.verifyBoxF2skD = new Mejor.VerifyBox();
            this.verifyBoxF2fsD = new Mejor.VerifyBox();
            this.verifyBoxF1stD = new Mejor.VerifyBox();
            this.verifyBoxF1skD = new Mejor.VerifyBox();
            this.verifyBoxF1fsD = new Mejor.VerifyBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxF5skM = new Mejor.VerifyBox();
            this.verifyBoxF5fsM = new Mejor.VerifyBox();
            this.verifyBoxF5skY = new Mejor.VerifyBox();
            this.verifyBoxF5fsY = new Mejor.VerifyBox();
            this.verifyBoxF5Tenki = new Mejor.VerifyBox();
            this.verifyBoxF4skM = new Mejor.VerifyBox();
            this.verifyBoxF5Days = new Mejor.VerifyBox();
            this.verifyBoxF4skY = new Mejor.VerifyBox();
            this.verifyBoxF4fsM = new Mejor.VerifyBox();
            this.verifyBoxF4fsY = new Mejor.VerifyBox();
            this.verifyBoxF4Tenki = new Mejor.VerifyBox();
            this.verifyBoxF3skM = new Mejor.VerifyBox();
            this.verifyBoxF4Days = new Mejor.VerifyBox();
            this.verifyBoxF3skY = new Mejor.VerifyBox();
            this.verifyBoxF3fsM = new Mejor.VerifyBox();
            this.verifyBoxF3fsY = new Mejor.VerifyBox();
            this.verifyBoxF3Tenki = new Mejor.VerifyBox();
            this.verifyBoxF2skM = new Mejor.VerifyBox();
            this.verifyBoxF3Days = new Mejor.VerifyBox();
            this.verifyBoxF2skY = new Mejor.VerifyBox();
            this.verifyBoxF2fsM = new Mejor.VerifyBox();
            this.verifyBoxF2fsY = new Mejor.VerifyBox();
            this.verifyBoxF2Tenki = new Mejor.VerifyBox();
            this.verifyBoxF1skM = new Mejor.VerifyBox();
            this.verifyBoxF2Days = new Mejor.VerifyBox();
            this.verifyBoxF1skY = new Mejor.VerifyBox();
            this.verifyBoxF1fsM = new Mejor.VerifyBox();
            this.verifyBoxF1fsY = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxF1Tenki = new Mejor.VerifyBox();
            this.verifyBoxF1Days = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelOCR = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelClinic.SuspendLayout();
            this.panelFusho.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 250;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 22);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(190, 22);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // lblwareki
            // 
            this.lblwareki.AutoSize = true;
            this.lblwareki.Location = new System.Drawing.Point(65, 22);
            this.lblwareki.Name = "lblwareki";
            this.lblwareki.Size = new System.Drawing.Size(29, 12);
            this.lblwareki.TabIndex = 1;
            this.lblwareki.Text = "和暦";
            this.lblwareki.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-196, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelClinic);
            this.panelRight.Controls.Add(this.panelFusho);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.lblwareki);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // panelClinic
            // 
            this.panelClinic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelClinic.Controls.Add(this.label32);
            this.panelClinic.Controls.Add(this.verifyBoxHosName);
            this.panelClinic.Controls.Add(this.verifyBoxDrCode);
            this.panelClinic.Controls.Add(this.label34);
            this.panelClinic.Controls.Add(this.labelNumbering);
            this.panelClinic.Controls.Add(this.verifyBoxDrName);
            this.panelClinic.Location = new System.Drawing.Point(3, 614);
            this.panelClinic.Name = "panelClinic";
            this.panelClinic.Size = new System.Drawing.Size(909, 74);
            this.panelClinic.TabIndex = 50;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(231, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 24);
            this.label32.TabIndex = 61;
            this.label32.Text = "施術\r\n所名";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHosName
            // 
            this.verifyBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHosName.Location = new System.Drawing.Point(260, 18);
            this.verifyBoxHosName.Name = "verifyBoxHosName";
            this.verifyBoxHosName.NewLine = false;
            this.verifyBoxHosName.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxHosName.TabIndex = 130;
            this.verifyBoxHosName.TextV = "";
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(75, 18);
            this.verifyBoxDrCode.MaxLength = 12;
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxDrCode.TabIndex = 120;
            this.verifyBoxDrCode.TextV = "";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(595, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 24);
            this.label34.TabIndex = 59;
            this.label34.Text = "施術\r\n師名";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(75, 5);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(113, 12);
            this.labelNumbering.TabIndex = 57;
            this.labelNumbering.Text = "柔整師登録記号番号";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(624, 18);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxDrName.TabIndex = 140;
            this.verifyBoxDrName.TextV = "";
            // 
            // panelFusho
            // 
            this.panelFusho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelFusho.Controls.Add(this.verifyBoxF5edD);
            this.panelFusho.Controls.Add(this.verifyBoxF4edD);
            this.panelFusho.Controls.Add(this.verifyBoxF3edD);
            this.panelFusho.Controls.Add(this.verifyBoxF2edD);
            this.panelFusho.Controls.Add(this.verifyBoxF1edD);
            this.panelFusho.Controls.Add(this.label31);
            this.panelFusho.Controls.Add(this.verifyBoxF5stD);
            this.panelFusho.Controls.Add(this.verifyBoxF5skD);
            this.panelFusho.Controls.Add(this.verifyBoxF5fsD);
            this.panelFusho.Controls.Add(this.verifyBoxF4stD);
            this.panelFusho.Controls.Add(this.verifyBoxF4skD);
            this.panelFusho.Controls.Add(this.verifyBoxF4fsD);
            this.panelFusho.Controls.Add(this.verifyBoxF3stD);
            this.panelFusho.Controls.Add(this.verifyBoxF3skD);
            this.panelFusho.Controls.Add(this.verifyBoxF3fsD);
            this.panelFusho.Controls.Add(this.verifyBoxF2stD);
            this.panelFusho.Controls.Add(this.verifyBoxF2skD);
            this.panelFusho.Controls.Add(this.verifyBoxF2fsD);
            this.panelFusho.Controls.Add(this.verifyBoxF1stD);
            this.panelFusho.Controls.Add(this.verifyBoxF1skD);
            this.panelFusho.Controls.Add(this.verifyBoxF1fsD);
            this.panelFusho.Controls.Add(this.label29);
            this.panelFusho.Controls.Add(this.label33);
            this.panelFusho.Controls.Add(this.label7);
            this.panelFusho.Controls.Add(this.label28);
            this.panelFusho.Controls.Add(this.label2);
            this.panelFusho.Controls.Add(this.verifyBoxF5skM);
            this.panelFusho.Controls.Add(this.verifyBoxF5fsM);
            this.panelFusho.Controls.Add(this.verifyBoxF5skY);
            this.panelFusho.Controls.Add(this.verifyBoxF5fsY);
            this.panelFusho.Controls.Add(this.verifyBoxF5Tenki);
            this.panelFusho.Controls.Add(this.verifyBoxF4skM);
            this.panelFusho.Controls.Add(this.verifyBoxF5Days);
            this.panelFusho.Controls.Add(this.verifyBoxF4skY);
            this.panelFusho.Controls.Add(this.verifyBoxF4fsM);
            this.panelFusho.Controls.Add(this.verifyBoxF4fsY);
            this.panelFusho.Controls.Add(this.verifyBoxF4Tenki);
            this.panelFusho.Controls.Add(this.verifyBoxF3skM);
            this.panelFusho.Controls.Add(this.verifyBoxF4Days);
            this.panelFusho.Controls.Add(this.verifyBoxF3skY);
            this.panelFusho.Controls.Add(this.verifyBoxF3fsM);
            this.panelFusho.Controls.Add(this.verifyBoxF3fsY);
            this.panelFusho.Controls.Add(this.verifyBoxF3Tenki);
            this.panelFusho.Controls.Add(this.verifyBoxF2skM);
            this.panelFusho.Controls.Add(this.verifyBoxF3Days);
            this.panelFusho.Controls.Add(this.verifyBoxF2skY);
            this.panelFusho.Controls.Add(this.verifyBoxF2fsM);
            this.panelFusho.Controls.Add(this.verifyBoxF2fsY);
            this.panelFusho.Controls.Add(this.verifyBoxF2Tenki);
            this.panelFusho.Controls.Add(this.verifyBoxF1skM);
            this.panelFusho.Controls.Add(this.verifyBoxF2Days);
            this.panelFusho.Controls.Add(this.verifyBoxF1skY);
            this.panelFusho.Controls.Add(this.verifyBoxF1fsM);
            this.panelFusho.Controls.Add(this.verifyBoxF1fsY);
            this.panelFusho.Controls.Add(this.label26);
            this.panelFusho.Controls.Add(this.label27);
            this.panelFusho.Controls.Add(this.label13);
            this.panelFusho.Controls.Add(this.label10);
            this.panelFusho.Controls.Add(this.label14);
            this.panelFusho.Controls.Add(this.verifyBoxF5);
            this.panelFusho.Controls.Add(this.verifyBoxF4);
            this.panelFusho.Controls.Add(this.verifyBoxF3);
            this.panelFusho.Controls.Add(this.verifyBoxF1Tenki);
            this.panelFusho.Controls.Add(this.verifyBoxF1Days);
            this.panelFusho.Controls.Add(this.verifyBoxF2);
            this.panelFusho.Controls.Add(this.verifyBoxF1);
            this.panelFusho.Controls.Add(this.label22);
            this.panelFusho.Controls.Add(this.label21);
            this.panelFusho.Controls.Add(this.label20);
            this.panelFusho.Controls.Add(this.label11);
            this.panelFusho.Controls.Add(this.label15);
            this.panelFusho.Controls.Add(this.label9);
            this.panelFusho.Controls.Add(this.labelDays);
            this.panelFusho.Location = new System.Drawing.Point(6, 406);
            this.panelFusho.Name = "panelFusho";
            this.panelFusho.Size = new System.Drawing.Size(1000, 200);
            this.panelFusho.TabIndex = 40;
            // 
            // verifyBoxF5edD
            // 
            this.verifyBoxF5edD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5edD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5edD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5edD.Location = new System.Drawing.Point(632, 150);
            this.verifyBoxF5edD.MaxLength = 2;
            this.verifyBoxF5edD.Name = "verifyBoxF5edD";
            this.verifyBoxF5edD.NewLine = false;
            this.verifyBoxF5edD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5edD.TabIndex = 90;
            this.verifyBoxF5edD.TextV = "";
            // 
            // verifyBoxF4edD
            // 
            this.verifyBoxF4edD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4edD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4edD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4edD.Location = new System.Drawing.Point(632, 122);
            this.verifyBoxF4edD.MaxLength = 2;
            this.verifyBoxF4edD.Name = "verifyBoxF4edD";
            this.verifyBoxF4edD.NewLine = false;
            this.verifyBoxF4edD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4edD.TabIndex = 70;
            this.verifyBoxF4edD.TextV = "";
            // 
            // verifyBoxF3edD
            // 
            this.verifyBoxF3edD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3edD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3edD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3edD.Location = new System.Drawing.Point(632, 94);
            this.verifyBoxF3edD.MaxLength = 2;
            this.verifyBoxF3edD.Name = "verifyBoxF3edD";
            this.verifyBoxF3edD.NewLine = false;
            this.verifyBoxF3edD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3edD.TabIndex = 50;
            this.verifyBoxF3edD.TextV = "";
            // 
            // verifyBoxF2edD
            // 
            this.verifyBoxF2edD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2edD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2edD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2edD.Location = new System.Drawing.Point(632, 67);
            this.verifyBoxF2edD.MaxLength = 2;
            this.verifyBoxF2edD.Name = "verifyBoxF2edD";
            this.verifyBoxF2edD.NewLine = false;
            this.verifyBoxF2edD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2edD.TabIndex = 30;
            this.verifyBoxF2edD.TextV = "";
            // 
            // verifyBoxF1edD
            // 
            this.verifyBoxF1edD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1edD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1edD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1edD.Location = new System.Drawing.Point(632, 39);
            this.verifyBoxF1edD.MaxLength = 2;
            this.verifyBoxF1edD.Name = "verifyBoxF1edD";
            this.verifyBoxF1edD.NewLine = false;
            this.verifyBoxF1edD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1edD.TabIndex = 10;
            this.verifyBoxF1edD.TextV = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(630, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 12);
            this.label31.TabIndex = 50;
            this.label31.Text = "終了";
            // 
            // verifyBoxF5stD
            // 
            this.verifyBoxF5stD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5stD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5stD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5stD.Location = new System.Drawing.Point(590, 150);
            this.verifyBoxF5stD.MaxLength = 2;
            this.verifyBoxF5stD.Name = "verifyBoxF5stD";
            this.verifyBoxF5stD.NewLine = false;
            this.verifyBoxF5stD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5stD.TabIndex = 89;
            this.verifyBoxF5stD.TextV = "";
            // 
            // verifyBoxF5skD
            // 
            this.verifyBoxF5skD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5skD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5skD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5skD.Location = new System.Drawing.Point(546, 150);
            this.verifyBoxF5skD.MaxLength = 2;
            this.verifyBoxF5skD.Name = "verifyBoxF5skD";
            this.verifyBoxF5skD.NewLine = false;
            this.verifyBoxF5skD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5skD.TabIndex = 88;
            this.verifyBoxF5skD.TextV = "";
            // 
            // verifyBoxF5fsD
            // 
            this.verifyBoxF5fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5fsD.Location = new System.Drawing.Point(440, 150);
            this.verifyBoxF5fsD.MaxLength = 2;
            this.verifyBoxF5fsD.Name = "verifyBoxF5fsD";
            this.verifyBoxF5fsD.NewLine = false;
            this.verifyBoxF5fsD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5fsD.TabIndex = 84;
            this.verifyBoxF5fsD.TextV = "";
            // 
            // verifyBoxF4stD
            // 
            this.verifyBoxF4stD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4stD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4stD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4stD.Location = new System.Drawing.Point(590, 122);
            this.verifyBoxF4stD.MaxLength = 2;
            this.verifyBoxF4stD.Name = "verifyBoxF4stD";
            this.verifyBoxF4stD.NewLine = false;
            this.verifyBoxF4stD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4stD.TabIndex = 69;
            this.verifyBoxF4stD.TextV = "";
            // 
            // verifyBoxF4skD
            // 
            this.verifyBoxF4skD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4skD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4skD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4skD.Location = new System.Drawing.Point(546, 122);
            this.verifyBoxF4skD.MaxLength = 2;
            this.verifyBoxF4skD.Name = "verifyBoxF4skD";
            this.verifyBoxF4skD.NewLine = false;
            this.verifyBoxF4skD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4skD.TabIndex = 68;
            this.verifyBoxF4skD.TextV = "";
            // 
            // verifyBoxF4fsD
            // 
            this.verifyBoxF4fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4fsD.Location = new System.Drawing.Point(440, 122);
            this.verifyBoxF4fsD.MaxLength = 2;
            this.verifyBoxF4fsD.Name = "verifyBoxF4fsD";
            this.verifyBoxF4fsD.NewLine = false;
            this.verifyBoxF4fsD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4fsD.TabIndex = 64;
            this.verifyBoxF4fsD.TextV = "";
            // 
            // verifyBoxF3stD
            // 
            this.verifyBoxF3stD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3stD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3stD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3stD.Location = new System.Drawing.Point(590, 94);
            this.verifyBoxF3stD.MaxLength = 2;
            this.verifyBoxF3stD.Name = "verifyBoxF3stD";
            this.verifyBoxF3stD.NewLine = false;
            this.verifyBoxF3stD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3stD.TabIndex = 49;
            this.verifyBoxF3stD.TextV = "";
            // 
            // verifyBoxF3skD
            // 
            this.verifyBoxF3skD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3skD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3skD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3skD.Location = new System.Drawing.Point(546, 94);
            this.verifyBoxF3skD.MaxLength = 2;
            this.verifyBoxF3skD.Name = "verifyBoxF3skD";
            this.verifyBoxF3skD.NewLine = false;
            this.verifyBoxF3skD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3skD.TabIndex = 48;
            this.verifyBoxF3skD.TextV = "";
            // 
            // verifyBoxF3fsD
            // 
            this.verifyBoxF3fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3fsD.Location = new System.Drawing.Point(440, 94);
            this.verifyBoxF3fsD.MaxLength = 2;
            this.verifyBoxF3fsD.Name = "verifyBoxF3fsD";
            this.verifyBoxF3fsD.NewLine = false;
            this.verifyBoxF3fsD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3fsD.TabIndex = 44;
            this.verifyBoxF3fsD.TextV = "";
            // 
            // verifyBoxF2stD
            // 
            this.verifyBoxF2stD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2stD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2stD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2stD.Location = new System.Drawing.Point(590, 67);
            this.verifyBoxF2stD.MaxLength = 2;
            this.verifyBoxF2stD.Name = "verifyBoxF2stD";
            this.verifyBoxF2stD.NewLine = false;
            this.verifyBoxF2stD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2stD.TabIndex = 29;
            this.verifyBoxF2stD.TextV = "";
            // 
            // verifyBoxF2skD
            // 
            this.verifyBoxF2skD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2skD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2skD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2skD.Location = new System.Drawing.Point(546, 67);
            this.verifyBoxF2skD.MaxLength = 2;
            this.verifyBoxF2skD.Name = "verifyBoxF2skD";
            this.verifyBoxF2skD.NewLine = false;
            this.verifyBoxF2skD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2skD.TabIndex = 28;
            this.verifyBoxF2skD.TextV = "";
            // 
            // verifyBoxF2fsD
            // 
            this.verifyBoxF2fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2fsD.Location = new System.Drawing.Point(440, 67);
            this.verifyBoxF2fsD.MaxLength = 2;
            this.verifyBoxF2fsD.Name = "verifyBoxF2fsD";
            this.verifyBoxF2fsD.NewLine = false;
            this.verifyBoxF2fsD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2fsD.TabIndex = 24;
            this.verifyBoxF2fsD.TextV = "";
            // 
            // verifyBoxF1stD
            // 
            this.verifyBoxF1stD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1stD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1stD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1stD.Location = new System.Drawing.Point(590, 39);
            this.verifyBoxF1stD.MaxLength = 2;
            this.verifyBoxF1stD.Name = "verifyBoxF1stD";
            this.verifyBoxF1stD.NewLine = false;
            this.verifyBoxF1stD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1stD.TabIndex = 9;
            this.verifyBoxF1stD.TextV = "";
            // 
            // verifyBoxF1skD
            // 
            this.verifyBoxF1skD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1skD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1skD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1skD.Location = new System.Drawing.Point(546, 39);
            this.verifyBoxF1skD.MaxLength = 2;
            this.verifyBoxF1skD.Name = "verifyBoxF1skD";
            this.verifyBoxF1skD.NewLine = false;
            this.verifyBoxF1skD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1skD.TabIndex = 8;
            this.verifyBoxF1skD.TextV = "";
            // 
            // verifyBoxF1fsD
            // 
            this.verifyBoxF1fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsD.Location = new System.Drawing.Point(440, 39);
            this.verifyBoxF1fsD.MaxLength = 2;
            this.verifyBoxF1fsD.Name = "verifyBoxF1fsD";
            this.verifyBoxF1fsD.NewLine = false;
            this.verifyBoxF1fsD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1fsD.TabIndex = 4;
            this.verifyBoxF1fsD.TextV = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(550, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 12);
            this.label29.TabIndex = 42;
            this.label29.Text = "日";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(588, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 12);
            this.label33.TabIndex = 37;
            this.label33.Text = "開始";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 42;
            this.label7.Text = "日";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(495, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(65, 12);
            this.label28.TabIndex = 37;
            this.label28.Text = "初検年月日";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(389, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 37;
            this.label2.Text = "負傷年月日";
            // 
            // verifyBoxF5skM
            // 
            this.verifyBoxF5skM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5skM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5skM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5skM.Location = new System.Drawing.Point(514, 150);
            this.verifyBoxF5skM.MaxLength = 2;
            this.verifyBoxF5skM.Name = "verifyBoxF5skM";
            this.verifyBoxF5skM.NewLine = false;
            this.verifyBoxF5skM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5skM.TabIndex = 87;
            this.verifyBoxF5skM.TextV = "";
            // 
            // verifyBoxF5fsM
            // 
            this.verifyBoxF5fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5fsM.Location = new System.Drawing.Point(408, 150);
            this.verifyBoxF5fsM.MaxLength = 2;
            this.verifyBoxF5fsM.Name = "verifyBoxF5fsM";
            this.verifyBoxF5fsM.NewLine = false;
            this.verifyBoxF5fsM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5fsM.TabIndex = 83;
            this.verifyBoxF5fsM.TextV = "";
            // 
            // verifyBoxF5skY
            // 
            this.verifyBoxF5skY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5skY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5skY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5skY.Location = new System.Drawing.Point(483, 150);
            this.verifyBoxF5skY.MaxLength = 2;
            this.verifyBoxF5skY.Name = "verifyBoxF5skY";
            this.verifyBoxF5skY.NewLine = false;
            this.verifyBoxF5skY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5skY.TabIndex = 86;
            this.verifyBoxF5skY.TextV = "";
            // 
            // verifyBoxF5fsY
            // 
            this.verifyBoxF5fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5fsY.Location = new System.Drawing.Point(377, 150);
            this.verifyBoxF5fsY.MaxLength = 2;
            this.verifyBoxF5fsY.Name = "verifyBoxF5fsY";
            this.verifyBoxF5fsY.NewLine = false;
            this.verifyBoxF5fsY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5fsY.TabIndex = 82;
            this.verifyBoxF5fsY.TextV = "";
            // 
            // verifyBoxF5Tenki
            // 
            this.verifyBoxF5Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Tenki.Location = new System.Drawing.Point(726, 150);
            this.verifyBoxF5Tenki.Name = "verifyBoxF5Tenki";
            this.verifyBoxF5Tenki.NewLine = false;
            this.verifyBoxF5Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF5Tenki.TabIndex = 92;
            this.verifyBoxF5Tenki.TextV = "";
            // 
            // verifyBoxF4skM
            // 
            this.verifyBoxF4skM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4skM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4skM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4skM.Location = new System.Drawing.Point(514, 122);
            this.verifyBoxF4skM.MaxLength = 2;
            this.verifyBoxF4skM.Name = "verifyBoxF4skM";
            this.verifyBoxF4skM.NewLine = false;
            this.verifyBoxF4skM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4skM.TabIndex = 67;
            this.verifyBoxF4skM.TextV = "";
            // 
            // verifyBoxF5Days
            // 
            this.verifyBoxF5Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Days.Location = new System.Drawing.Point(680, 150);
            this.verifyBoxF5Days.Name = "verifyBoxF5Days";
            this.verifyBoxF5Days.NewLine = false;
            this.verifyBoxF5Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF5Days.TabIndex = 91;
            this.verifyBoxF5Days.TextV = "";
            // 
            // verifyBoxF4skY
            // 
            this.verifyBoxF4skY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4skY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4skY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4skY.Location = new System.Drawing.Point(483, 122);
            this.verifyBoxF4skY.MaxLength = 2;
            this.verifyBoxF4skY.Name = "verifyBoxF4skY";
            this.verifyBoxF4skY.NewLine = false;
            this.verifyBoxF4skY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4skY.TabIndex = 66;
            this.verifyBoxF4skY.TextV = "";
            // 
            // verifyBoxF4fsM
            // 
            this.verifyBoxF4fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4fsM.Location = new System.Drawing.Point(408, 122);
            this.verifyBoxF4fsM.MaxLength = 2;
            this.verifyBoxF4fsM.Name = "verifyBoxF4fsM";
            this.verifyBoxF4fsM.NewLine = false;
            this.verifyBoxF4fsM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4fsM.TabIndex = 63;
            this.verifyBoxF4fsM.TextV = "";
            // 
            // verifyBoxF4fsY
            // 
            this.verifyBoxF4fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4fsY.Location = new System.Drawing.Point(377, 122);
            this.verifyBoxF4fsY.MaxLength = 2;
            this.verifyBoxF4fsY.Name = "verifyBoxF4fsY";
            this.verifyBoxF4fsY.NewLine = false;
            this.verifyBoxF4fsY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4fsY.TabIndex = 62;
            this.verifyBoxF4fsY.TextV = "";
            // 
            // verifyBoxF4Tenki
            // 
            this.verifyBoxF4Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Tenki.Location = new System.Drawing.Point(726, 122);
            this.verifyBoxF4Tenki.Name = "verifyBoxF4Tenki";
            this.verifyBoxF4Tenki.NewLine = false;
            this.verifyBoxF4Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF4Tenki.TabIndex = 72;
            this.verifyBoxF4Tenki.TextV = "";
            // 
            // verifyBoxF3skM
            // 
            this.verifyBoxF3skM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3skM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3skM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3skM.Location = new System.Drawing.Point(514, 94);
            this.verifyBoxF3skM.MaxLength = 2;
            this.verifyBoxF3skM.Name = "verifyBoxF3skM";
            this.verifyBoxF3skM.NewLine = false;
            this.verifyBoxF3skM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3skM.TabIndex = 47;
            this.verifyBoxF3skM.TextV = "";
            // 
            // verifyBoxF4Days
            // 
            this.verifyBoxF4Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Days.Location = new System.Drawing.Point(680, 122);
            this.verifyBoxF4Days.Name = "verifyBoxF4Days";
            this.verifyBoxF4Days.NewLine = false;
            this.verifyBoxF4Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF4Days.TabIndex = 71;
            this.verifyBoxF4Days.TextV = "";
            // 
            // verifyBoxF3skY
            // 
            this.verifyBoxF3skY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3skY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3skY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3skY.Location = new System.Drawing.Point(483, 94);
            this.verifyBoxF3skY.MaxLength = 2;
            this.verifyBoxF3skY.Name = "verifyBoxF3skY";
            this.verifyBoxF3skY.NewLine = false;
            this.verifyBoxF3skY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3skY.TabIndex = 46;
            this.verifyBoxF3skY.TextV = "";
            // 
            // verifyBoxF3fsM
            // 
            this.verifyBoxF3fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3fsM.Location = new System.Drawing.Point(408, 94);
            this.verifyBoxF3fsM.MaxLength = 2;
            this.verifyBoxF3fsM.Name = "verifyBoxF3fsM";
            this.verifyBoxF3fsM.NewLine = false;
            this.verifyBoxF3fsM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3fsM.TabIndex = 43;
            this.verifyBoxF3fsM.TextV = "";
            // 
            // verifyBoxF3fsY
            // 
            this.verifyBoxF3fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3fsY.Location = new System.Drawing.Point(377, 94);
            this.verifyBoxF3fsY.MaxLength = 2;
            this.verifyBoxF3fsY.Name = "verifyBoxF3fsY";
            this.verifyBoxF3fsY.NewLine = false;
            this.verifyBoxF3fsY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3fsY.TabIndex = 42;
            this.verifyBoxF3fsY.TextV = "";
            // 
            // verifyBoxF3Tenki
            // 
            this.verifyBoxF3Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Tenki.Location = new System.Drawing.Point(726, 94);
            this.verifyBoxF3Tenki.Name = "verifyBoxF3Tenki";
            this.verifyBoxF3Tenki.NewLine = false;
            this.verifyBoxF3Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF3Tenki.TabIndex = 52;
            this.verifyBoxF3Tenki.TextV = "";
            // 
            // verifyBoxF2skM
            // 
            this.verifyBoxF2skM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2skM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2skM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2skM.Location = new System.Drawing.Point(514, 67);
            this.verifyBoxF2skM.MaxLength = 2;
            this.verifyBoxF2skM.Name = "verifyBoxF2skM";
            this.verifyBoxF2skM.NewLine = false;
            this.verifyBoxF2skM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2skM.TabIndex = 27;
            this.verifyBoxF2skM.TextV = "";
            // 
            // verifyBoxF3Days
            // 
            this.verifyBoxF3Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Days.Location = new System.Drawing.Point(680, 94);
            this.verifyBoxF3Days.Name = "verifyBoxF3Days";
            this.verifyBoxF3Days.NewLine = false;
            this.verifyBoxF3Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF3Days.TabIndex = 51;
            this.verifyBoxF3Days.TextV = "";
            // 
            // verifyBoxF2skY
            // 
            this.verifyBoxF2skY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2skY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2skY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2skY.Location = new System.Drawing.Point(483, 67);
            this.verifyBoxF2skY.MaxLength = 2;
            this.verifyBoxF2skY.Name = "verifyBoxF2skY";
            this.verifyBoxF2skY.NewLine = false;
            this.verifyBoxF2skY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2skY.TabIndex = 26;
            this.verifyBoxF2skY.TextV = "";
            // 
            // verifyBoxF2fsM
            // 
            this.verifyBoxF2fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2fsM.Location = new System.Drawing.Point(408, 67);
            this.verifyBoxF2fsM.MaxLength = 2;
            this.verifyBoxF2fsM.Name = "verifyBoxF2fsM";
            this.verifyBoxF2fsM.NewLine = false;
            this.verifyBoxF2fsM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2fsM.TabIndex = 23;
            this.verifyBoxF2fsM.TextV = "";
            // 
            // verifyBoxF2fsY
            // 
            this.verifyBoxF2fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2fsY.Location = new System.Drawing.Point(377, 67);
            this.verifyBoxF2fsY.MaxLength = 2;
            this.verifyBoxF2fsY.Name = "verifyBoxF2fsY";
            this.verifyBoxF2fsY.NewLine = false;
            this.verifyBoxF2fsY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2fsY.TabIndex = 22;
            this.verifyBoxF2fsY.TextV = "";
            // 
            // verifyBoxF2Tenki
            // 
            this.verifyBoxF2Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Tenki.Location = new System.Drawing.Point(726, 67);
            this.verifyBoxF2Tenki.Name = "verifyBoxF2Tenki";
            this.verifyBoxF2Tenki.NewLine = false;
            this.verifyBoxF2Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF2Tenki.TabIndex = 32;
            this.verifyBoxF2Tenki.TextV = "";
            // 
            // verifyBoxF1skM
            // 
            this.verifyBoxF1skM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1skM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1skM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1skM.Location = new System.Drawing.Point(514, 39);
            this.verifyBoxF1skM.MaxLength = 2;
            this.verifyBoxF1skM.Name = "verifyBoxF1skM";
            this.verifyBoxF1skM.NewLine = false;
            this.verifyBoxF1skM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1skM.TabIndex = 7;
            this.verifyBoxF1skM.TextV = "";
            // 
            // verifyBoxF2Days
            // 
            this.verifyBoxF2Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Days.Location = new System.Drawing.Point(680, 67);
            this.verifyBoxF2Days.Name = "verifyBoxF2Days";
            this.verifyBoxF2Days.NewLine = false;
            this.verifyBoxF2Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF2Days.TabIndex = 31;
            this.verifyBoxF2Days.TextV = "";
            // 
            // verifyBoxF1skY
            // 
            this.verifyBoxF1skY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1skY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1skY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1skY.Location = new System.Drawing.Point(483, 39);
            this.verifyBoxF1skY.MaxLength = 2;
            this.verifyBoxF1skY.Name = "verifyBoxF1skY";
            this.verifyBoxF1skY.NewLine = false;
            this.verifyBoxF1skY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1skY.TabIndex = 6;
            this.verifyBoxF1skY.TextV = "";
            // 
            // verifyBoxF1fsM
            // 
            this.verifyBoxF1fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsM.Location = new System.Drawing.Point(408, 39);
            this.verifyBoxF1fsM.MaxLength = 2;
            this.verifyBoxF1fsM.Name = "verifyBoxF1fsM";
            this.verifyBoxF1fsM.NewLine = false;
            this.verifyBoxF1fsM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1fsM.TabIndex = 3;
            this.verifyBoxF1fsM.TextV = "";
            // 
            // verifyBoxF1fsY
            // 
            this.verifyBoxF1fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsY.Location = new System.Drawing.Point(377, 39);
            this.verifyBoxF1fsY.MaxLength = 2;
            this.verifyBoxF1fsY.Name = "verifyBoxF1fsY";
            this.verifyBoxF1fsY.NewLine = false;
            this.verifyBoxF1fsY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1fsY.TabIndex = 2;
            this.verifyBoxF1fsY.TextV = "";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label26.Location = new System.Drawing.Point(772, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 24);
            this.label26.TabIndex = 26;
            this.label26.Text = "治癒：１、中止：２、\r\n転医：３、継続：空白";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(518, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 31;
            this.label27.Text = "月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(412, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 31;
            this.label13.Text = "月";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(488, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 29;
            this.label10.Text = "年";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(382, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 29;
            this.label14.Text = "年";
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(65, 150);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF5.TabIndex = 80;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(65, 122);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF4.TabIndex = 60;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(65, 94);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF3.TabIndex = 40;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF1Tenki
            // 
            this.verifyBoxF1Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Tenki.Location = new System.Drawing.Point(726, 39);
            this.verifyBoxF1Tenki.Name = "verifyBoxF1Tenki";
            this.verifyBoxF1Tenki.NewLine = false;
            this.verifyBoxF1Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF1Tenki.TabIndex = 12;
            this.verifyBoxF1Tenki.TextV = "";
            // 
            // verifyBoxF1Days
            // 
            this.verifyBoxF1Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Days.Location = new System.Drawing.Point(680, 39);
            this.verifyBoxF1Days.Name = "verifyBoxF1Days";
            this.verifyBoxF1Days.NewLine = false;
            this.verifyBoxF1Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF1Days.TabIndex = 11;
            this.verifyBoxF1Days.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(65, 67);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF2.TabIndex = 20;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(65, 39);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF1.TabIndex = 0;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 155);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 8;
            this.label22.Text = "負傷名5";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 129);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 6;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 4;
            this.label20.Text = "負傷名3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 2;
            this.label11.Text = "負傷名2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(730, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 10;
            this.label15.Text = "転帰";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷名1";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(678, 22);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(41, 12);
            this.labelDays.TabIndex = 10;
            this.labelDays.Text = "実日数";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 11);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Leave += new System.EventHandler(this.verifyBoxY_Leave);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(155, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 65);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 323);
            this.scrollPictureControl1.TabIndex = 300;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 695);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(29, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm_fusho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm_fusho";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelClinic.ResumeLayout(false);
            this.panelClinic.PerformLayout();
            this.panelFusho.ResumeLayout(false);
            this.panelFusho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label lblwareki;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelFusho;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF1Days;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private VerifyBox verifyBoxF5fsM;
        private VerifyBox verifyBoxF5fsY;
        private VerifyBox verifyBoxF5Tenki;
        private VerifyBox verifyBoxF5Days;
        private VerifyBox verifyBoxF4fsM;
        private VerifyBox verifyBoxF4fsY;
        private VerifyBox verifyBoxF4Tenki;
        private VerifyBox verifyBoxF4Days;
        private VerifyBox verifyBoxF3fsM;
        private VerifyBox verifyBoxF3fsY;
        private VerifyBox verifyBoxF3Tenki;
        private VerifyBox verifyBoxF3Days;
        private VerifyBox verifyBoxF2fsM;
        private VerifyBox verifyBoxF2fsY;
        private VerifyBox verifyBoxF2Tenki;
        private VerifyBox verifyBoxF2Days;
        private VerifyBox verifyBoxF1fsM;
        private VerifyBox verifyBoxF1fsY;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxF1Tenki;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxF5edD;
        private VerifyBox verifyBoxF4edD;
        private VerifyBox verifyBoxF3edD;
        private VerifyBox verifyBoxF2edD;
        private VerifyBox verifyBoxF1edD;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxF5stD;
        private VerifyBox verifyBoxF5skD;
        private VerifyBox verifyBoxF5fsD;
        private VerifyBox verifyBoxF4stD;
        private VerifyBox verifyBoxF4skD;
        private VerifyBox verifyBoxF4fsD;
        private VerifyBox verifyBoxF3stD;
        private VerifyBox verifyBoxF3skD;
        private VerifyBox verifyBoxF3fsD;
        private VerifyBox verifyBoxF2stD;
        private VerifyBox verifyBoxF2skD;
        private VerifyBox verifyBoxF2fsD;
        private VerifyBox verifyBoxF1stD;
        private VerifyBox verifyBoxF1skD;
        private VerifyBox verifyBoxF1fsD;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxF5skM;
        private VerifyBox verifyBoxF5skY;
        private VerifyBox verifyBoxF4skM;
        private VerifyBox verifyBoxF4skY;
        private VerifyBox verifyBoxF3skM;
        private VerifyBox verifyBoxF3skY;
        private VerifyBox verifyBoxF2skM;
        private VerifyBox verifyBoxF2skY;
        private VerifyBox verifyBoxF1skM;
        private VerifyBox verifyBoxF1skY;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label32;
        private VerifyBox verifyBoxHosName;
        private System.Windows.Forms.Label label34;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private System.Windows.Forms.Panel panelClinic;
    }
}