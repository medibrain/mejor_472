﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.MiyagiKokuho
{
    /// <summary>
    /// 宮城県柔整エクスポートデータ
    /// </summary>
    class Export
    {

        #region テーブル構造
        public int renban { get; set; } = 0;                        //メホール上の処理年月
        public string comnum { get; set; } = string.Empty;          //レセプト全国共通キー
        public string cym { get; set; } = string.Empty;             //審査年月（メホール請求年月）
        public string sendno { get; set; } = string.Empty;          //審査年月-保険者番号-連番
        public string hMark { get; set; } = string.Empty;           //記号
        public string hNum { get; set; } = string.Empty;            //番号


        //20200701135820 furukawa st ////////////////////////
        //順序変更
        
        public string hname { get; set; } = string.Empty;           //被保険者名
        public string pname { get; set; } = string.Empty;           //受療者名
        

        //public string pname { get; set; } = string.Empty;           //受療者名
        //public string hname { get; set; } = string.Empty;           //被保険者名
        //20200701135820 furukawa ed ////////////////////////

        public string psex { get; set; } = string.Empty;            //性別
        public string pbirthday { get; set; } = string.Empty;       //生年月日
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族
        public string hzip { get; set; } = string.Empty;            //郵便番号
        public string haddress1 { get; set; } = string.Empty;       //被保険者住所1
        public string haddress2 { get; set; } = string.Empty;       //被保険者住所2
        public string ym { get; set; } = string.Empty;              //施術年月
        public string firstdate1 { get; set; } = string.Empty;      //初検年月日1
        public string startdate1 { get; set; } = string.Empty;      //施術開始年月日1
        public string fushoname1 { get; set; } = string.Empty;      //負傷名1
        public string fushoname2 { get; set; } = string.Empty;      //負傷名2
        public string fushoname3 { get; set; } = string.Empty;      //負傷名3
        public string fushoname4 { get; set; } = string.Empty;      //負傷名4
        public string fushoname5 { get; set; } = string.Empty;      //負傷名5
        public string fushocount { get; set; } = string.Empty;      //負傷部位数
        public string fushodays1 { get; set; } = string.Empty;      //実日数1
        public string total { get; set; } = string.Empty;           //合計金額
        public string charge { get; set; } = string.Empty;          //請求額
        public string ratio { get; set; } = string.Empty;           //給付割合
        public string drnum { get; set; } = string.Empty;           //柔整師登録記号番号
        public string clinicname { get; set; } = string.Empty;      //施術所名
        public string drname { get; set; } = string.Empty;          //柔道整復師名
        public string cliniczip { get; set; } = string.Empty;       //施術所郵便番号
        public string clinicaddress { get; set; } = string.Empty;   //施術所住所
        public string shokaireason { get; set; } = string.Empty;    //照会理由


        //20200701135702 furukawa st ////////////////////////
        //メモ欄7つに変更（7つ固定）
        

        public string memo1 { get; set; } = string.Empty;         //メモ1
        public string memo2 { get; set; } = string.Empty;         //メモ2
        public string memo3 { get; set; } = string.Empty;         //メモ3
        public string memo4 { get; set; } = string.Empty;         //メモ4
        public string memo5 { get; set; } = string.Empty;         //メモ5
        public string memo6 { get; set; } = string.Empty;         //メモ6
        public string memo7 { get; set; } = string.Empty;         //メモ7



                            //20200622141604 furukawa st ////////////////////////
                            //内容可変メモ追加

                            //public string memo { get; set; } = string.Empty;         //メモ

                            //20200622141604 furukawa ed ////////////////////////



        //20200701135702 furukawa ed ////////////////////////

        public string shokairesult { get; set; } = string.Empty;    //照会結果
        public string henreireason { get; set; } = string.Empty;    //返戻理由
        public string kahi { get; set; } = string.Empty;            //可否
        public string upd { get; set; } = string.Empty;             //修正有無
        public string aid { get; set; } = string.Empty;				//業者整理番号

        #endregion
        #region old

        //#region テーブル構造
        //[DB.DbAttribute.PrimaryKey]
        //public int cym { get; set; } = 0;                           //メホール上の処理年月
        //public string comnum { get; set; } = string.Empty;          //レセプト全国共通キー
        //public string ym { get; set; } = string.Empty;              //診療年月
        //public string hMark { get; set; } = string.Empty;           //記号
        //public string hNum { get; set; } = string.Empty;            //番号
        //public string psex { get; set; } = string.Empty;            //性別
        //public string pbirthday { get; set; } = string.Empty;       //生年月日
        //public string pname { get; set; } = string.Empty;           //受療者名
        //public string hname { get; set; } = string.Empty;           //被保険者名
        //public string fushoname1 { get; set; } = string.Empty;      //負傷名1
        //public string total { get; set; } = string.Empty;           //合計金額
        //public string drname { get; set; } = string.Empty;          //柔道整復師名
        //public string clinicname { get; set; } = string.Empty;      //施術所名
        //public string upd { get; set; } = string.Empty;             //修正有無
        //public string haddress { get; set; } = string.Empty;        //被保険者住所
        //public string family { get; set; } = string.Empty;          //本人家族区分
        //public string fushodays1 { get; set; } = string.Empty;      //実日数1
        //public string fushocount { get; set; } = string.Empty;      //負傷部位数
        //public string shoken1 { get; set; } = string.Empty;         //初検年月日1
        //public string charge { get; set; } = string.Empty;          //請求額
        //public string startdate1 { get; set; } = string.Empty;      //施術開始年月日1
        //public string cliniczip { get; set; } = string.Empty;       //施術所郵便番号
        //public string clinicaddress { get; set; } = string.Empty;   //施術所住所
        //public string ratio { get; set; } = string.Empty;           //給付割合
        //public string shokaireason { get; set; } = string.Empty;    //照会結果
        //public string henreireason { get; set; } = string.Empty;    //返戻理由

        //#endregion
        #endregion

        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        
            //20200730171654 furukawa st ////////////////////////
        //当初CYMを使用して月ごとにエクセルブックを分けていたが、月マタギで一気に出すように仕様変更要望
        
        public static bool ExportData(List<App> lstApp)
        //public static bool ExportData(int cym, List<App> lstApp)
        //20200730171654 furukawa ed ////////////////////////

        {
        

            int OutputListNo = 0;

            //リストの選択画面
            OutputSelectForm frmOut = new OutputSelectForm();
            frmOut.ShowDialog();
            OutputListNo = frmOut.ListNo;
            if (OutputListNo == 0) return false;
            
            //保存場所
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //申請書取得
                //List<App> lstApp = App.GetApps(cym);

                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable(lstApp, wf)) return false;
                //if (!InsertExportTable(lstApp, cym, wf)) return false;


                wf.LogPrint("データ取得");
                //保険者データ取得
                List<hokensha> lsthname = DB.Main.SelectAll<hokensha>().ToList();

                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                //保険者ごとにフォルダとファイルを分けて出力
                foreach (hokensha item in lsthname)
                {
                    
                    wf.LogPrint(item.insname);

                    string strDir = strNohinDir + "\\" + hokensha.select($" hmark='{item.hmark}'")[0].insnum + item.insname;
                    //string strDir = strNohinDir + "\\" + item.sortno.ToString("000") + item.insname;

                    //20200730152952 furukawa st ////////////////////////
                    //ファイル名の頭に保険者番号つける
                    
                    string strSortNoInsName = item.insnum.PadLeft(3,'0') + item.insname;
                    //item.sortno.ToString("000") + item.insname;
                    //20200730152952 furukawa ed ////////////////////////


                    //exportテーブルを取得 npoiでブック作成、保存
                    switch (OutputListNo)
                    {
                        //20200730105651 furukawa st ////////////////////////
                        //当初CYMを使用して月ごとにエクセルブックを分けていたが、月マタギで一気に出すように仕様変更要望
                        
                        case 1:
                            if (!DataExport(1,strDir, strSortNoInsName, item.hmark)) return false;
                            break;
                        case 2:
                            if (!DataExport(2,strDir, strSortNoInsName,item.hmark)) return false;
                            break;
                        case 3:
                            if (!DataExport(3,strDir, strSortNoInsName,item.hmark)) return false;
                            break;
                        case 4:
                            if (!DataExport(4,strDir, strSortNoInsName,item.hmark)) return false;
                            break;
                        case 9:
                            if (!DataExport(1,strDir, strSortNoInsName,item.hmark)) return false;
                            if (!DataExport(2,strDir, strSortNoInsName,item.hmark)) return false;
                            if (!DataExport(3,strDir, strSortNoInsName,item.hmark)) return false;
                            if (!DataExport(4,strDir, strSortNoInsName,item.hmark)) return false;
                            break;
                        //case 1:
                        //    if (!DataExport(1, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    break;
                        //case 2:
                        //    if (!DataExport(2, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    break;
                        //case 3:
                        //    if (!DataExport(3, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    break;
                        //case 4:
                        //    if (!DataExport(4, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    break;
                        //case 9:
                        //    if (!DataExport(1, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    if (!DataExport(2, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    if (!DataExport(3, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    if (!DataExport(4, cym, strDir, strSortNoInsName, item.hmark)) return false;
                        //    break;

                        //20200730105651 furukawa ed ////////////////////////


                        default: break;

                    }
                
                }

                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        //リスト番号１：照会対象候補者リスト　２：初検被保険者リスト ３：照会対象候補施術所等リスト　４：返戻対象候補者リスト

        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <returns></returns>
        private static bool InsertExportTable(List<App> lstApp ,WaitForm wf)
            //private static bool InsertExportTable(List<App> lstApp, int cym, WaitForm wf)
        {
            wf.SetMax(lstApp.Count);

            List<hokensha> lsthokensha =hokensha.selectAll();

            DB.Transaction tran;
            tran=DB.Main.CreateTransaction();
                        
            try
            {




                //20200730122715 furukawa st ////////////////////////
                //cym不要なので全消しにする

                //今月分を削除
                //DB.Command cmd = new DB.Command($"delete from export where cym='{cym}'", tran);
                DB.Command cmd = new DB.Command($"truncate table export;", tran);
                //20200730122715 furukawa ed ////////////////////////

                cmd.TryExecuteNonQuery();
                
                int cnt = 0;

                foreach (App item in lstApp)
                {

                    #region 不要？
                    ////既出テーブルに登録
                    //hihoSent hs = new hihoSent();
                    //if (item.HihoNum != string.Empty && item.HihoNum.Contains('-'))
                    //{
                    //    hs.hMark = item.HihoNum.Split('-')[0].ToString();
                    //    hs.hNum = item.HihoNum.Split('-')[1].ToString();
                    //}
                    //hs.psex = item.Sex.ToString();
                    //hs.pbirthday = item.Birthday.ToString("yyyy-MM-dd");
                    //hs.pname = item.PersonName;
                    //hs.hname = item.HihoName;
                    //hs.family = item.Family.ToString();

                    ////登録できない場合は既出と見なす
                    ////         if (!DB.Main.Insert<hihoSent>(hs, tran))
                    ////        {
                    ////         wf.LogPrint($"{hs.hMark}-{hs.hNum}は過去にリスト作成しましたので除外します");
                    ////         continue;
                    ////         }

                    //if (item.HihoNum == string.Empty || !item.HihoNum.Contains('-')) continue;

                    //    DB.Command cmdhihosent = new DB.Command(DB.Main, $"insert into hihosent (" +
                    //    $"hMark,hNum,psex,pbirthday,pname,hname,family) values( " +
                    //    $"'{item.HihoNum.Split('-')[0].ToString()}'," +
                    //    $"'{item.HihoNum.Split('-')[1].ToString()}'," +
                    //    $"'{item.Sex.ToString()}'," +
                    //    $"'{item.Birthday.ToString("yyyy-MM-dd")}'," +
                    //    $"'{item.PersonName}'," +
                    //    $"'{item.HihoName}'," +
                    //    $"'{item.Family.ToString()}')"
                    //    );

                    //try
                    //{
                    //    cmdhihosent.TryExecuteNonQuery();
                    //}
                    //catch (Npgsql.NpgsqlException nex)
                    //{
                    //    if (nex.Message.Contains("duplicate"))
                    //    {
                    //        wf.LogPrint(item.PersonName + nex.Message);
                    //        continue;
                    //    }
                    //}

                    #endregion


                    Export exp = new Export();

                    exp.renban = 0;// cnt;//連番→保険者ごとに1からにするので、Excel出力時に回す

                    //レセプト全国共通キー
                    exp.comnum = item.ComNum == string.Empty ? $"dummy{cnt.ToString("000")}" : item.ComNum;
                    //exp.comnum = item.TaggedDatas.GeneralString1.ToString().Trim() == string.Empty ?
                    //    $"dummy{cnt.ToString("000")}" : item.TaggedDatas.GeneralString1.ToString().Trim();

                    cnt++;


                    exp.cym = item.CYM.ToString();              //審査年月(メホール請求年月     

                    //通知番号                    

                    //20200622161352 furukawa st ////////////////////////
                    //保険者番号取得高速化
                    
                    string strHokenDokuziNo = string.Empty;
                    
                    foreach (hokensha h in lsthokensha)
                    {
                        if (h.kokuho_insnum == item.InsNum)
                        {
                            strHokenDokuziNo = h.insnum;
                            break;
                        }
                    }
                    //string strHokenDokuziNo = hokensha.select($" kokuho_insnum='{item.InsNum}'")[0].insnum;
                    //20200622161352 furukawa ed ////////////////////////



                    //20200702215150 furukawa st ////////////////////////
                    //通知番号和暦
                    
                    string strCYMWareki =
                        DateTimeEx.GetEraNumberYearFromYYYYMM(int.Parse(exp.cym.ToString())).ToString() + exp.cym.Substring(4, 2);

                    exp.sendno = $"{strCYMWareki}-{strHokenDokuziNo}";

                                //連番は保険者ごとに1からにするので、Excel出力時に回す
                                //exp.sendno = $"{exp.cym}-{strHokenDokuziNo}";//-{exp.renban.ToString().PadLeft(4, '0')}";
                    //20200702215150 furukawa ed ////////////////////////




                    //被保険者証記号・番号
                    //2020年6月14日現在　被保険者証記号と番号はパイプつなぎであるという前提
                    if (item.HihoNum != string.Empty && item.HihoNum.Contains('|'))             
                    {
                        exp.hMark = item.HihoNum.Split('|')[0].ToString();
                        exp.hNum = item.HihoNum.Split('|')[1].ToString();

                    }
                    ////被保険者証記号・番号
                    ////2020年4月21日現在　被保険者証記号と番号はハイフンつなぎであるという前提
                    //if (item.HihoNum != string.Empty && item.HihoNum.Contains('-'))
                    //{
                    //    exp.hMark = item.HihoNum.Split('-')[0].ToString();
                    //    exp.hNum = item.HihoNum.Split('-')[1].ToString();

                    //}
                    exp.pname = item.PersonName;                                                       //受療者名
                    exp.hname = item.HihoName;                                                         //被保険者名
                    exp.psex = item.Sex == 1 ? "男" :"女";                                             //性別                 

                    exp.pbirthday = DateTimeEx.GetIntJpDateWithEraNumber(item.Birthday).ToString(); //生年月日
                    //exp.pbirthday = item.Birthday.ToString("yyyy/MM/dd");                              //生年月日

                    //本家区分(給付データ)x100+本人家族(入力)
                    exp.family = item.Family%100 == 2 ? "本人" : "家族";        //本人家族
                    exp.hzip = item.HihoZip;
                    exp.haddress1 = item.HihoAdd;                                                       //被保険者住所
                    exp.haddress2 = string.Empty;                                                       //被保険者住所

                    exp.ym = item.YM > 0 ? DateTimeEx.GetEraNumberYearFromYYYYMM(item.YM) + item.YM.ToString().Substring(4, 2) : "0";               //診療年月
                    //exp.ym = item.YM.ToString();                //診療年月

                    exp.firstdate1 = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoFirstDate1).ToString();                         //初検年月日1
                    //exp.shoken1 = item.FushoFirstDate1.ToString("yyyy/MM/dd");                         //初検年月日1

                    exp.startdate1 = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();                      //施術開始年月日1
                    //exp.startdate1 = item.FushoStartDate1.ToString("yyyy/MM/dd");                      //施術開始年月日1

                    exp.fushoname1 = item.FushoName1;                       //負傷名1
                    exp.fushoname2 = item.FushoName2;                       //負傷名2
                    exp.fushoname3 = item.FushoName3;                       //負傷名3
                    exp.fushoname4 = item.FushoName4;                       //負傷名4
                    exp.fushoname5 = item.FushoName5;                       //負傷名5
                    exp.fushocount = item.TaggedDatas.count.ToString() ;    //部位数（手入力

                    //20200701184052 furukawa st ////////////////////////
                    //仕様変更
                    
                    exp.fushodays1 = item.CountedDays.ToString();            //診療＿実日数
                    //exp.fushodays1 = item.FushoDays1.ToString();            //実日数1
                    //20200701184052 furukawa ed ////////////////////////



                    exp.total = item.Total.ToString();                      //合計金額

                    //インポート時に本家入外区分から割合を自動判別、合計×割合　小数点以下切り捨て
                    exp.charge = item.Charge.ToString();                    //請求金額 
                   
                    //インポート時に本家入外区分から割合を自動判別
                    exp.ratio = item.Ratio.ToString();                      //給付割合
                    

                    exp.drnum = item.DrNum;                                 //柔整師登録記号番号
                    exp.clinicname = item.ClinicName;                       //施術所名
                    exp.drname = item.DrName;                               //柔道整復師名
                    
                    
                    
                    exp.cliniczip = item.ClinicZip;                         //施術所郵便番号
                    exp.clinicaddress = item.ClinicAdd;                     //施術所住所

                    exp.shokaireason = item.ShokaiReason.ToString();        //照会理由


                    //20200701142118 furukawa st ////////////////////////
                    //メモ仕様変更
                    
                    //出力メモ（パイプつなぎ前提）
                    string strMemo = item.OutMemo;
                    
                    if (strMemo.Contains('|') || strMemo.Contains('｜'))
                    {
                        strMemo = strMemo.Replace('｜', '|');

                        //|が6個ある場合に限る
                        if (System.Text.RegularExpressions.Regex.IsMatch(strMemo, ".+|.+|.+|.+|.+|.+|.+"))
                            //if (strMemo.Replace("|", "").Length == 6)
                            {
                            string[] arrMemo = strMemo.Split('|');

                            exp.memo1 = arrMemo[0];
                            exp.memo2 = arrMemo[1];
                            exp.memo3 = arrMemo[2];
                            exp.memo4 = arrMemo[3];
                            exp.memo5 = arrMemo[4];
                            exp.memo6 = arrMemo[5];
                            exp.memo7 = arrMemo[6];
                        }
                    }
                                    //20200622141925 furukawa st ////////////////////////
                                    //内容可変メモ追加

                                    //exp.memo = item.Memo;
                                    //20200622141925 furukawa ed ////////////////////////


                    //20200701142118 furukawa ed ////////////////////////

                    
                    
                    //20200701143619 furukawa st ////////////////////////
                    //代入先間違い
                    
                    exp.shokairesult = item.ShokaiResultStr.ToString();     //照会結果
                    //exp.shokaireason = item.ShokaiResultStr.ToString();     //照会結果
                    //20200701143619 furukawa ed ////////////////////////



                    exp.henreireason = item.HenreiReasons.ToString();       //返戻理由


                    exp.kahi = string.Empty;
                    exp.upd = "";                                           //修正有無 2020年4月21日　伸作さんより　無視していい
                    exp.aid = item.Aid.ToString();//業者整理番号(aid)

                    

                    //同ヒホバン、生年月日、性別の人は出さない
                    //既出テーブルを使用してもいいが、2度手間になりそう…
                    DB.Command cmddup = new DB.Command( 
                        $"select * from export where hnum='{item.HihoNum}' " +
                        $"and pbirthday='{item.Birthday.ToString("yyyy/MM/dd")}' " +
                        $"and psex='{item.Sex.ToString()}'",tran);


                    if (cmddup.TryExecuteScalar() != null)
                    {
                        wf.LogPrint($"{item.HihoNum}既に出しました");                        
                        wf.InvokeValue++;
                        continue;
                    }



                    if(!DB.Main.Insert<Export>(exp, tran)) return false;

                    wf.LogPrint($"レセプト全国共通キー:{exp.comnum}");
                    wf.InvokeValue++;
                }   

                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }

               
        }




        /// <summary>
        /// DBからデータを取得してExcel出力まで
        /// <param name="listNo">出力リスト番号</param>
        /// <param name="cym">処理年月</param>
        /// <param name="strDir">保険者ごとの出力フォルダ名</param>
        /// <paramref name="insno_insname"/>保険者名
        /// <param name="strPosix">保険者ごとの検索条件</param>
        /// </summary>
        /// <returns></returns>
        /// 

        //20200730105825 furukawa st ////////////////////////
        //当初CYMを使用して月ごとにエクセルブックを分けていたが、月マタギで一気に出すように仕様変更要望
        
        private static bool DataExport(int listNo,string strDir,string strSortNoInsname,string strhMark="")
            //private static bool DataExport(int listNo, int cym, string strDir, string strSortNoInsname, string strhMark = "")
            //private static bool DataExport(int listNo, int cym, string strDir, string strSortNoInsname, string strPosix = "")
        //20200730105825 furukawa ed ////////////////////////
        {

            string strFileName = strDir + "\\";
            string strHeader = string.Empty;
            string strSQL = string.Empty;

            if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);

            switch (listNo)
            {
                case 1:
                    //20200730105859 furukawa st ////////////////////////
                    //cym不要
                    
                    strFileName += $"{strSortNoInsname}_01_照会対象候補者リスト.xlsx";
                    //strFileName += $"{cym}_{strSortNoInsname}_01_照会対象候補者リスト.xlsx";
                    //20200730105859 furukawa ed ////////////////////////

                    #region ヘッダ

                    strHeader =
                        "No.," +                        //renban            ○
                        "レセプト全国共通キー," +       //comnum            ○
                        "審査年月," +                   //cym               ○
                        "通知番号," +                   //sendno            ○
                        "記号," +                       //hMark             ○
                        "番号," +                       //hNum              ○

                        //20200701173838 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        
                        "被保険者名," +                 //hname             ○
                        "受療者名," +                   //pname             ○                   
                                                    //"受療者名," +                   //pname             ○
                                                    //"被保険者名," +                 //hname             ○
                        //20200701173838 furukawa ed ////////////////////////

                        "性別," +                       //psex              ○
                        "生年月日," +                   //pbirthday         ○
                        "本人家族区分," +               //family            ○
                        "郵便番号," +                   //hzip              ○
                        "被保険者住所1," +              //haddress1         ○
                        "被保険者住所2," +              //haddress2         ○
                        "施術年月," +                   //ym                ○
                        "初検年月日1," +                //firstdate1        ○
                        //"施術開始年月日1," +          //startdate1        -
                        "負傷名1," +                    //fushoname1        ○
                        "負傷名2," +                    //fushoname2        ○
                        "負傷名3," +                    //fushoname3        ○
                        "負傷名4," +                    //fushoname4        ○
                        "負傷名5," +                    //fushoname5        ○
                        "負傷部位数," +                 //fushocount        ○
                        "実日数1," +                    //fushodays1        ○
                        "合計金額," +                   //total             ○
                        //"請求額," +                   //charge            -
                        //"給付割合," +                 //ratio             -
                        "柔整師登録記号番号," +         //drnum             ○
                        "施術所名," +                   //clinicname        ○
                        "柔道整復師名," +               //drname            ○
                        //"施術所郵便番号," +           //cliniczip         -
                        //"施術所住所," +               //clinicaddress     -
                        "照会理由," +                   //shokaireason      ○
                        //"メモ," +                     //outmemo           -
                        //"照会結果," +                 //shokairesult      -
                        //"返戻理由," +                 //henreireason      -
                        "可否," +                       //kahi              ○
                        "修正有無," +                   //upd               ○
                        "業者整理番号 ";                //aid               ○



                    #endregion

                    #region sql
                    strSQL =
                        "SELECT " +
                        "renban," +               //renban            ○
                        "comnum," +               //comnum            ○
                        "cym," +                  //cym               ○
                        "sendno," +               //sendno            ○
                        "hMark," +                //hMark             ○
                        "hNum," +                 //hNum              ○

                        //20200701173955 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        
                        "hname," +                //hname             ○
                        "pname," +                //pname             ○                        
                                                  //"pname," +                //pname             ○
                                                  //"hname," +                //hname             ○
                        //20200701173955 furukawa ed ////////////////////////

                        "psex," +                 //psex              ○
                        "pbirthday," +            //pbirthday         ○
                        "family," +               //family            ○
                        "hzip," +                 //hzip              ○
                        "haddress1," +            //haddress1         ○
                        "haddress2," +            //haddress2         ○
                        "ym," +                   //ym                ○
                        "firstdate1," +           //firstdate1        ○
                        //"startdate1," +         //startdate1        -
                        "fushoname1," +           //fushoname1        ○
                        "fushoname2," +           //fushoname2        ○
                        "fushoname3," +           //fushoname3        ○
                        "fushoname4," +           //fushoname4        ○
                        "fushoname5," +           //fushoname5        ○
                        "fushocount," +           //fushocount        ○
                        "fushodays1," +           //fushodays1        ○
                        "total," +                //total             ○
                        //"charge," +             //charge            -
                        //"ratio," +              //ratio             -
                        "drnum," +                //drnum             ○
                        "clinicname," +           //clinicname        ○
                        "drname," +               //drname            ○
                        //"cliniczip," +          //cliniczip         -
                        //"clinicaddress," +      //clinicaddress     -
                        "shokaireason," +         //shokaireason      ○
                        //"memo," +               //outmemo           -
                        //"shokairesult," +       //shokairesult      -
                        //"henreireason," +       //henreireason      -
                        "kahi," +                 //kahi              ○
                        "upd," +                  //upd               ○
                        "aid ";                   //aid               ○
                    #endregion

                    break;
                case 2:
                    //20200730110547 furukawa st ////////////////////////
                    //cym不要
                    
                    strFileName += $"{strSortNoInsname}_02_初検被保険者リスト.xlsx";
                    //strFileName += $"{cym}_{strSortNoInsname}_02_初検被保険者リスト.xlsx";
                    //20200730110547 furukawa ed ////////////////////////

                    #region ヘッダ

                    strHeader =
                        "No.," +                        //renban		○                 
                        "レセプト全国共通キー," +       //comnum          ○
                        "審査年月," +                   //cym             ○
                        "通知番号," +                   //sendno          ○
                        "記号," +                       //hMark           ○
                        "番号," +                       //hNum            ○

                        //20200701175037 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        
                        "被保険者名," +                 //hname           ○
                        "受療者名," +                   //pname           ○
                        

                        //"受療者名," +                   //pname           ○
                        //"被保険者名," +                 //hname           ○
                        //20200701175037 furukawa ed ////////////////////////

                        "性別," +                       //psex            ○
                        "生年月日," +                   //pbirthday       ○
                        "本人家族区分," +               //family          ○
                        "郵便番号," +                   //hzip            ○
                        "被保険者住所1," +              //haddress1       ○
                        "被保険者住所2," +              //haddress2       ○
                        "施術年月," +                   //ym              ○
                        "初検年月日1," +                //firstdate1      ○
                        //"施術開始年月日1," +          //startdate1      -
                        "負傷名1," +                    //fushoname1      ○
                        "負傷名2," +                    //fushoname2      ○
                        "負傷名3," +                    //fushoname3      ○
                        "負傷名4," +                    //fushoname4      ○
                        "負傷名5," +                    //fushoname5      ○
                        "負傷部位数," +                 //fushocount      ○
                        "実日数1," +                    //fushodays1      ○
                        "合計金額," +                   //total           ○
                        "請求額," +                     //charge          ○
                        //"給付割合," +                 //ratio           -
                        "柔整師登録記号番号," +         //drnum           ○
                        "施術所名," +                   //clinicname      ○
                        "柔道整復師名," +               //drname          ○
                        //"施術所郵便番号," +           //cliniczip       -
                        //"施術所住所," +               //clinicaddress   -
                        "照会理由," +                   //shokaireason    ○
                        //"メモ," +                     //memo            -
                        //"照会結果," +                 //shokairesult    -
                        //"返戻理由," +                 //henreireason    -
                        "可否," +                       //kahi            ○
                        "修正有無," +                   //upd             ○
                        "業者整理番号 ";                //aid             ○



                    #endregion

                    #region sql
                    strSQL =
                        "SELECT " +
                        "renban," +                         //renban			○
                        "comnum," +                         //comnum          ○
                        "cym," +                            //cym             ○
                        "sendno," +                         //sendno          ○
                        "hMark," +                          //hMark           ○
                        "hNum," +                           //hNum            ○

                        //20200701175121 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        "hname," +                          //hname           ○
                        "pname," +                          //pname           ○
                        
                        //"pname," +                          //pname           ○
                        //"hname," +                          //hname           ○

                        //20200701175121 furukawa ed ////////////////////////

                        "psex," +                           //psex            ○
                        "pbirthday," +                      //pbirthday       ○
                        "family," +                         //family          ○
                        "hzip," +                           //hzip            ○
                        "haddress1," +                      //haddress1       ○
                        "haddress2," +                      //haddress2       ○
                        "ym," +                             //ym              ○
                        "firstdate1," +                     //firstdate1      ○
                        //"startdate1," +                   //startdate1      -
                        "fushoname1," +                     //fushoname1      ○
                        "fushoname2," +                     //fushoname2      ○
                        "fushoname3," +                     //fushoname3      ○
                        "fushoname4," +                     //fushoname4      ○
                        "fushoname5," +                     //fushoname5      ○
                        "fushocount," +                     //fushocount      ○
                        "fushodays1," +                     //fushodays1      ○
                        "total," +                          //total           ○
                        "charge," +                         //charge          ○
                        //"ratio," +                        //ratio           -
                        "drnum," +                        //drnum           ○
                        "clinicname," +                     //clinicname      ○
                        "drname," +                         //drname          ○
                        //"cliniczip," +                    //cliniczip       -
                        //"clinicaddress," +                //clinicaddress   -
                        "shokaireason," +                   //shokaireason    ○
                        //"memo," +                         //memo            -
                        //"shokairesult," +                 //shokairesult    -
                        //"henreireason," +                 //henreireason    -
                        "kahi," +                           //kahi            ○
                        "upd," +                            //upd             ○
                        "aid ";                             //aid             ○
                    #endregion


                    break;
                case 3:
                    //20200730110612 furukawa st ////////////////////////
                    //cym不要
                    
                    strFileName += $"{strSortNoInsname}_03_照会対象候補施術所等リスト.xlsx";
                    //strFileName += $"{cym}_{strSortNoInsname}_03_照会対象候補施術所等リスト.xlsx";
                    //20200730110612 furukawa ed ////////////////////////
                    #region ヘッダ

                    strHeader =
                        "No.," +                        //renban		○
                        "レセプト全国共通キー," +       //comnum          ○
                        "審査年月," +                   //cym             ○
                        "通知番号," +                   //sendno          ○
                        "記号," +                       //hMark           ○
                        "番号," +                       //hNum            ○

                        //20200701175215 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        "被保険者名," +                 //hname           ○                        
                        "受療者名," +                   //pname           ○

                        //"受療者名," +                   //pname           ○
                        //"被保険者名," +                 //hname           ○
                        //20200701175215 furukawa ed ////////////////////////

                        "性別," +                       //psex            ○
                        "生年月日," +                   //pbirthday       ○
                        //"本人家族区分," +             //family          -
                        //"郵便番号," +                 //hzip            -
                        //"被保険者住所1," +            //haddress1       -
                        //"被保険者住所2," +            //haddress2       -
                        "施術年月," +                   //ym              ○
                        //"初検年月日1," +              //firstdate1      -
                        "施術開始年月日1," +            //startdate1      ○
                        "負傷名1," +                    //fushoname1      ○
                        "負傷名2," +                    //fushoname2      ○
                        "負傷名3," +                    //fushoname3      ○
                        "負傷名4," +                    //fushoname4      ○
                        "負傷名5," +                    //fushoname5      ○
                        "負傷部位数," +                 //fushocount      ○
                        //"実日数1," +                  //fushodays1      -
                        "合計金額," +                   //total           ○
                        "請求額," +                   //charge          ○
                        "給付割合," +                   //ratio           ○
                        "柔整師登録記号番号," +       //drnum           ○
                        "施術所名," +                   //clinicname      ○
                        "柔道整復師名," +               //drname          ○
                        "施術所郵便番号," +             //cliniczip       ○
                        "施術所住所," +                 //clinicaddress   ○
                        "照会理由," +                   //shokaireason    ○


                        //20200701175514 furukawa st ////////////////////////
                        //メモ分割、項目名は固定
                        
                        "①-1," +
                        "①-2," +
                        "①-3," +
                        "①-4," +
                        "②," +
                        "③," +
                        "部位," +

                        //"メモ," +                       //memo            ○
                        //20200701175514 furukawa ed ////////////////////////

                        //20200720114904 furukawa st ////////////////////////
                        //リスト３、４照会結果、返戻理由不要
                        
                        //"照会結果," +                   //shokairesult    ○
                        //"返戻理由," +                   //henreireason    ○
                        //20200720114904 furukawa ed ////////////////////////

                        "可否," +                       //kahi            ○
                        "修正有無," +                   //upd             ○
                        "業者整理番号 ";                //aid             ○



                    #endregion

                    #region sql
                    strSQL =
                        "SELECT " +
                        "renban," +         //renban			○
                        "comnum," +         //comnum          ○
                        "cym," +            //cym             ○
                        "sendno," +         //sendno          ○
                        "hMark," +          //hMark           ○
                        "hNum," +           //hNum            ○

                        //20200701175600 furukawa st ////////////////////////
                        //仕様変更（順序入替）
                        
                        "hname," +          //hname           ○
                        "pname," +          //pname           ○

                        //"pname," +          //pname           ○
                        //"hname," +          //hname           ○
                        //20200701175600 furukawa ed ////////////////////////



                        "psex," +           //psex            ○
                        "pbirthday," +      //pbirthday       ○
                        //"family," +       //family          -
                        //"hzip," +         //hzip            -
                        //"haddress1," +    //haddress1       -
                        //"haddress2," +    //haddress2       -
                        "ym," +             //ym              ○
                        //"firstdate1," +   //firstdate1      -
                        "startdate1," +     //startdate1      ○
                        "fushoname1," +     //fushoname1      ○
                        "fushoname2," +     //fushoname2      ○
                        "fushoname3," +     //fushoname3      ○
                        "fushoname4," +     //fushoname4      ○
                        "fushoname5," +     //fushoname5      ○
                        "fushocount," +     //fushocount      ○
                        //"fushodays1," +    //fushodays1      -
                        "total," +          //total           ○
                        "charge," +         //charge          ○
                        "ratio," +          //ratio           ○
                        "drnum," +          //drnum           ○
                        "clinicname," +     //clinicname      ○
                        "drname," +       //drname          ○
                        "cliniczip," +      //cliniczip       ○
                        "clinicaddress," +  //clinicaddress   ○
                        "shokaireason," +   //shokaireason    ○

                        //20200701175740 furukawa st ////////////////////////
                        //メモ１～７
                        
                        "memo1," +
                        "memo2," +
                        "memo3," +
                        "memo4," +
                        "memo5," +
                        "memo6," +
                        "memo7," +

                        //"memo," +           //memo            ○
                        //20200701175740 furukawa ed ////////////////////////

                        //20200720114935 furukawa st ////////////////////////
                        //リスト３、４照会結果、返戻理由不要
                        
                        //"shokairesult," +   //shokairesult    ○
                        //"henreireason," +   //henreireason    ○
                        //20200720114935 furukawa ed ////////////////////////

                        "kahi," +           //kahi            ○
                        "upd," +            //upd             ○
                        "aid ";             //aid             ○
                    #endregion



                    break;
                case 4:
                    //20200730110640 furukawa st ////////////////////////
                    //cym不要
                    
                    strFileName += $"{strSortNoInsname}_04_返戻対象候補者リスト.xlsx";
                    //strFileName += $"{cym}_{strSortNoInsname}_04_返戻対象候補者リスト.xlsx";
                    //20200730110640 furukawa ed ////////////////////////
                    #region ヘッダ

                    strHeader =
                        "No.," +                        //renban		○
                        "レセプト全国共通キー," +       //comnum        ○
                        "審査年月," +                   //cym           ○
                        "通知番号," +                   //sendno        ○
                        "記号," +                       //hMark         ○
                        "番号," +                       //hNum          ○
                        "受療者名," +                   //pname         -
                        //"被保険者名," +                 //hname         ○
                        "性別," +                       //psex          ○
                        //"生年月日," +                   //pbirthday     -
                        "本人家族区分," +               //family        ○
                        //"郵便番号," +                   //hzip          -
                        //"被保険者住所1," +              //haddress1     -
                        //"被保険者住所2," +              //haddress2     -
                        "施術年月," +                   //ym            ○
                        "初検年月日1," +                //firstdate1    ○

                        //20200702182256 furukawa st ////////////////////////
                        //施術開始年月日新規追加
                        
                        "施術開始年月日1," +            //startdate1    -
                        //20200702182256 furukawa ed ////////////////////////


                        "負傷名1," +                    //fushoname1    ○
                        "負傷名2," +                    //fushoname2    ○
                        "負傷名3," +                    //fushoname3    ○
                        "負傷名4," +                    //fushoname4    ○
                        "負傷名5," +                    //fushoname5    ○
                        "負傷部位数," +                 //fushocount    ○
                        //"実日数1," +                    //fushodays1    -
                        "合計金額," +                   //total         ○
                        "請求額," +                     //charge        ○
                        //"給付割合," +                   //ratio         -
                        //"柔整師登録記号番号," +         //drnum         -
                        //"施術所名," +                   //clinicname    -
                        //"柔道整復師名," +               //drname        -
                        //"施術所郵便番号," +             //cliniczip     -
                        //"施術所住所," +                 //clinicaddress -
                        "照会理由," +                   //shokaireason  ○

                        //20200701175844 furukawa st ////////////////////////
                        //メモ分割、項目名は固定
                        
                        "①-1," +
                        "①-2," +
                        "①-3," +
                        "①-4," +
                        "②," +
                        "③," +
                        "部位," +

                        //"メモ," +                       //memo          ○
                        //20200701175844 furukawa ed ////////////////////////


                        //20200720114649 furukawa st ////////////////////////
                        //リスト３、４照会結果、返戻理由不要
                        
                        //"照会結果," +                   //shokairesult  ○
                        //"返戻理由," +                   //henreireason  ○
                        //20200720114649 furukawa ed ////////////////////////

                        "可否," +                       //kahi          ○
                        //"修正有無," +                   //upd           -
                        "業者整理番号 ";                //aid           ○



                    #endregion

                    #region sql
                    strSQL =
                        "SELECT " +
                        "renban," +             //renban		○
                        "comnum," +             //comnum        ○
                        "cym," +                //cym           ○
                        "sendno," +             //sendno        ○
                        "hMark," +              //hMark         ○
                        "hNum," +               //hNum          ○
                        "pname," +            //pname         -
                        //"hname," +            //hname         ○
                        "psex," +               //psex          ○
                        //"pbirthday," +        //pbirthday     -
                        "family," +           //family        ○
                        //"hzip," +             //hzip          -
                        //"haddress1," +        //haddress1     -
                        //"haddress2," +        //haddress2     -
                        "ym," +                 //ym            ○
                        "firstdate1," +         //firstdate1    ○

                        //20200702182204 furukawa st ////////////////////////
                        //施術開始年月日新規追加
                        
                        "startdate1," +       //startdate1    -
                        //20200702182204 furukawa ed ////////////////////////


                        "fushoname1," +         //fushoname1    ○
                        "fushoname2," +         //fushoname2    ○
                        "fushoname3," +         //fushoname3    ○
                        "fushoname4," +         //fushoname4    ○
                        "fushoname5," +         //fushoname5    ○
                        "fushocount," +         //fushocount    ○
                        //"fushodays1," +       //fushodays1    -
                        "total," +              //total         ○
                        "charge," +           //charge        ○
                        //"ratio," +            //ratio         -
                        //"drnum," +            //drnum         -
                        //"clinicname," +       //clinicname    -
                        //"drname," +           //drname        -
                        //"cliniczip," +        //cliniczip     -
                        //"clinicaddress," +    //clinicaddress -
                        "shokaireason," +       //shokaireason  ○

                        //20200701175924 furukawa st ////////////////////////
                        //メモ１～７
                        
                        "memo1," +
                        "memo2," +
                        "memo3," +
                        "memo4," +
                        "memo5," +
                        "memo6," +
                        "memo7," +
                        //"memo," +                //memo          ○
                        //20200701175924 furukawa ed ////////////////////////

                        //20200720114834 furukawa st ////////////////////////
                        //リスト３、４照会結果、返戻理由不要
                        
                        //"shokairesult," +       //shokairesult  ○
                        //"henreireason," +       //henreireason  ○
                        //20200720114834 furukawa ed ////////////////////////

                        "kahi," +               //kahi          ○
                        //"upd," +              //upd           -
                        "aid ";                 //aid           ○

                    
                    #endregion



                    break;
                default:
                    break;

            }

            //20200730111236 furukawa st ////////////////////////
            //cym不要
            
            strSQL += $"FROM export ";
            if (strhMark != string.Empty) strSQL += $" where hMark ='{strhMark}'";
            strSQL += $" order by sendno,comnum";

            //strSQL += $"FROM export where cym ='{cym}'";
            //if (strhMark != string.Empty) strSQL += $" and hMark ='{strhMark}'";
            //if (strPosix != string.Empty) strSQL += $" and hMark ='[{strPosix}]'";
            //20200730111236 furukawa ed ////////////////////////


            wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            wb.CreateSheet("sheet1");

            NPOI.SS.UserModel.ISheet sh = wb.GetSheetAt(0);

            NPOI.SS.UserModel.IRow r;

            using (var cmd = DB.Main.CreateCmd(strSQL))
            {
                try
                {         
                    var res = cmd.TryExecuteReaderList();
                    if (res.Count == 0) return true;


                    //20200730111911 furukawa st ////////////////////////
                    //cym不要に伴い、cymが変わったときの判定、連番振り直し用
                    
                    int cntRenban = 0;//通知番号用連番（保険者ごとの連番）
                    string strJpCYM = string.Empty;//現在のレコードのCYM
                    string strJpCYM_hikae = strJpCYM;//前のレコードのCYM

                    string strSendno = string.Empty;
                    string strSendno_hikae = strSendno;
                    
                    //20200730111911 furukawa ed ////////////////////////


                    #region データ


                    for (int cntRes = 0; cntRes < res.Count; cntRes++)
                    {
                        //ヘッダを飛ばして2行から
                        r = sh.CreateRow(cntRes + 1);

                        if (res == null || res.Count == 0) continue;

                        //連番インクリメント
                        cntRenban++;
                        

                        for (int c = 0; c < res[cntRes].Length; c++)
                        {

                            NPOI.SS.UserModel.ICell cell = r.CreateCell(c);
                            //cell.CellStyle = csData;
                            if (c == 0)
                            {
                                //連番部分は保険者ごとの連番なのでここで取得
                                cell.SetCellValue((cntRes+1).ToString());
                            }
                            else if (c == 2)
                            {

                                //20200730122809 furukawa st ////////////////////////
                                //cym不要なのでレコードの値を直接見る
                                

                                //和暦にするんだと
                                int intcym = int.Parse(res[cntRes][2].ToString());
                                strJpCYM = (DateTimeEx.GetEraNumberYearFromYYYYMM(intcym) * 100 + intcym % 100).ToString();
                                //string strJpCYM = (DateTimeEx.GetEraNumberYearFromYYYYMM(cym) * 100 + cym % 100).ToString();


                                //20200730122809 furukawa ed ////////////////////////


                                cell.SetCellValue(strJpCYM);                            

                            }
                            else if (c == 3)
                            {

                                //通知連番　連番部分は保険者ごとの連番なのでここで取得
                                //20200617190937 furukawa st ////////////////////////
                                //連番をループカウンタにする

                                //現在のcymと控えが違う場合、カウンタを1にリセットしないと、cymごとの連番が取れない
                                strSendno = res[cntRes][c].ToString();
                                if ((strJpCYM != strJpCYM_hikae)|| (strSendno!=strSendno_hikae)) cntRenban = 0;
                                cell.SetCellValue($"{res[cntRes][c].ToString()}-{(cntRenban + 1).ToString().PadLeft(4, '0')}");

                                //cell.SetCellValue($"{res[cntRes][c].ToString()}-{(cntRes+1).ToString().PadLeft(4,'0')}");
                                //cell.SetCellValue($"{res[cntRes][c].ToString()}-{(int.Parse(res[cntRes][0].ToString()) + 1).ToString().PadLeft(4, '0')}");
                                //20200617190937 furukawa ed ////////////////////////
                            }
                            else
                            {
                                cell.SetCellValue(res[cntRes][c].ToString());
                            }

                        }

                        //20200730112045 furukawa st ////////////////////////
                        //現在のCYM（メホール請求年月）を控える
                        
                        strJpCYM_hikae = strJpCYM;
                        strSendno_hikae = strSendno;
                        //20200730112045 furukawa ed ////////////////////////

                    }


                    #endregion

                    #region セルスタイル
                    NPOI.SS.UserModel.IFont font = wb.CreateFont();
                    font.FontName = "ＭＳ ゴシック";
                    font.FontHeightInPoints = 9;

                    NPOI.SS.UserModel.ICellStyle csHeader=wb.CreateCellStyle();
                    csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                    csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                    csHeader.SetFont(font);
                
                    
                    NPOI.SS.UserModel.ICellStyle csData = wb.CreateCellStyle();
                    csData.FillPattern = NPOI.SS.UserModel.FillPattern.NoFill;
                    csData.SetFont(font);
                    #endregion

                    #region ヘッダ行
                    r = sh.CreateRow(0);

                    var hs = strHeader.Split(',');
                    for (int cnt = 0; cnt < hs.Length; cnt++){
                        NPOI.SS.UserModel.ICell hdcell = r.CreateCell(cnt);
                        hdcell.CellStyle = csHeader;
                        hdcell.SetCellValue(hs[cnt]);
                    
                    }
                    #endregion

                    //オートフィルタ
                    NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(0, sh.LastRowNum,0, hs.Length-1);
                    sh.SetAutoFilter(rng);

                    for (int c = 0; c < res[0].Length; c++)
                    {
                        sh.AutoSizeColumn(c);
                    }


                    System.IO.FileStream fs =
                        new System.IO.FileStream(strFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    wb.Write(fs);
                    wb.Close();

                    return true;
                }
                catch(Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
            }



        }



    }

    ///// <summary>
    ///// 宮城県国保専用保険者テーブル
    ///// </summary>
    //partial class hokensha
    //{
    //    public int sortno { get; set; } = 0;
    //    public string insnum { get; set; } = string.Empty;        
    //    public string insname { get; set; } = string.Empty;
    //    public string hmark { get; set; } = string.Empty;
    //    public string posix { get; set; } = string.Empty;

     
    //}

    /// <summary>
    /// 既出テーブルだが使わないかも
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
