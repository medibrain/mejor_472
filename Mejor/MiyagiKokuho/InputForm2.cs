﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.MiyagiKokuho

{
    public partial class InputForm2 : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;
        private string rrid = string.Empty;

        /// <summary>
        /// 住所検索フォーム用
        /// </summary>
        private PostalCodeForm pcForm;
        private List<PostalCode> selectedHistory = new List<PostalCode>();
        private string prevTodofuken;
        private string prevSikutyoson;




        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        
        Point posFusho = new Point(0, 0);
       // Point posCost = new Point(800, 1800);
        Point posPname = new Point(0, 0);
        Point posClinic = new Point(0, 2200);
        Point posClinicNum = new Point(800, 2200);

        Control[] fushoControls, clinicControls, pnameControls,clinicNumControls;

        public InputForm2(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();
            

            #region コントロールグループ化
         

            //上段　被保険者情報
            pnameControls = new Control[]
            {
                verifyBoxHihoname,verifyBoxHihozip,verifyBoxHihoadd,
            };
 
            //施術師名、施術所名
            clinicControls = new Control[]
            {
                verifyBoxDrName,verifyBoxHosName,
            };

            //柔整師登録記号番号
            clinicNumControls = new Control[] {
                verifyBoxDrCode,
            };

            //負傷名
            fushoControls = new Control[]
            {
                verifyBoxF1Name,
                verifyBoxF2Name,
                verifyBoxF3Name,
                verifyBoxF4Name,
                verifyBoxF5Name,
            };

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion
            
            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            //条件付きでリストロード
            var list = App.GetAppsWithWhere($" where groupid={this.scanGroup.GroupID} and a.statusflags=(a.statusflags|{(int)StatusFlag.照会対象})");
            //var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";

            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].DisplayIndex = 1;

            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";


        
            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //施術所情報取得
            APP_ClinicInfo.CreateClinicInfoList(app.CYM);

            if (app != null) setApp(app);
            #endregion
            
            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);            
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (pnameControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPname;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (clinicControls.Contains(t)) scrollPictureControl1.ScrollPosition = posClinic;
            else if (clinicNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posClinicNum;

            //20200701171017 furukawa st ////////////////////////
            //住所テキストボックスの最後にカーソル
            
            if (t == verifyBoxHihoadd) verifyBoxHihoadd.Select(verifyBoxHihoadd.Text.Length, 0);
            //20200701171017 furukawa ed ////////////////////////
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// 登録ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();//住所検索
        }
        #endregion



        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="app">applicationクラス</app>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力値チェック
            hasError = false;

            //被保険者名
            setStatus(verifyBoxHihoname, false);
            //被保険者郵便番号
            setStatus(verifyBoxHihozip, (verifyBoxHihozip.Text.Length < 0 || verifyBoxHihozip.Text.Length < 7) && verifyBoxHihozip.Text == string.Empty);
            //被保険者住所
            setStatus(verifyBoxHihoadd, false);


            //施術師名
            setStatus(verifyBoxDrName, verifyBoxDrName.Text.Length < 3);

            //施術所名
            setStatus(verifyBoxHosName, verifyBoxHosName.Text.Length < 3);

                  
            //負傷ごとのチェック＋appへの反映
            if (!chkFusho(app,out app)) hasError = true;

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }


            #endregion

            #region 値の反映
            //値の反映

            //被保険者氏名
            app.HihoName = verifyBoxHihoname.Text.Trim();
            //被保険者郵便番号
            app.HihoZip = verifyBoxHihozip.Text.Trim();
            //被保険者住所
            app.HihoAdd = verifyBoxHihoadd.Text.Trim();
          
            //施術師コード
            app.DrNum = verifyBoxDrCode.Text.Trim();
            //施術師名
            app.DrName = verifyBoxDrName.Text.Trim();
            //施術所名
            app.ClinicName = verifyBoxHosName.Text.Trim();

            //20200527181413 furukawa st ////////////////////////
            //申請書種別の登録            
            app.AppType = scan.AppType;
            //20200527181413 furukawa ed ////////////////////////

   
            #endregion

            return true;
        }

     

    
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.独自処理2)) return true;

           
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }
            

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //保険者独自処理とする
                var ut = firstTime ? App.UPDATE_TYPE.ListInput1 : App.UPDATE_TYPE.ListInput2;
                //var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.ListInput1, 0,DateTime.Now-dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.ListInput1, 0, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;

                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.ListInput2, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.ListInput2, secondMissCount, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }

                if (!app.Update_SP(User.CurrentUser.UserID, ut, tran,Insurer.CurrrentInsurer.InsurerID)) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            string strUser1 = string.Empty;
            if(int.TryParse(app.TaggedDatas.GeneralString2, out int u1)) strUser1 = User.GetUserName(u1);
            string strUser2 = string.Empty;
            if (int.TryParse(app.TaggedDatas.GeneralString3, out int u2)) strUser2 = User.GetUserName(u2);

            labelInputerName.Text = "入力1:  " + strUser1 + "\r\n入力2:  " + strUser2;

            //labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
            //    "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.独自処理1))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
      
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1Name.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2Name.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3Name.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4Name.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5Name.Text = Fusho.GetFusho5(ocr);
                }
            }
            changedReset(app);
        }

    
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.独自処理2);

            //被保険者名
            setValue(verifyBoxHihoname, app.HihoName, firstTime, nv);
            //被保険者郵便番号
            setValue(verifyBoxHihozip, app.HihoZip, firstTime, nv);
            //被保険者住所
            setValue(verifyBoxHihoadd, app.HihoAdd, firstTime, nv);


            //施術師コード
            setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);

            //施術所名
            setValue(verifyBoxHosName, app.ClinicName, firstTime, nv);

            //施術師名
            setValue(verifyBoxDrName, app.DrName, firstTime, nv);
            
            //負傷ごとの値をセット
            setFusho(app,nv);
            
        }


        /// <summary>
        /// 負傷ごとのチェック
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool chkFusho(App app,out App outapp)
        {
            

            //負傷名1チェック
            fusho1Check(verifyBoxF1Name);
          
            //負傷名2チェック
            fushoCheck(verifyBoxF2Name);
          
            //負傷名3チェック
            fushoCheck(verifyBoxF3Name);
           
            //負傷名4チェック
            fushoCheck(verifyBoxF4Name);
          
            //負傷名5チェック
            fushoCheck(verifyBoxF5Name);
           
            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                outapp = app;
                return false;                
            }


            //負傷名
            app.FushoName1 = verifyBoxF1Name.Text.Trim();
            app.FushoName2 = verifyBoxF2Name.Text.Trim();
            app.FushoName3 = verifyBoxF3Name.Text.Trim();
            app.FushoName4 = verifyBoxF4Name.Text.Trim();
            app.FushoName5 = verifyBoxF5Name.Text.Trim();

            
            outapp = app;
            return true;
        }


           
        /// <summary>
        /// 負傷ごとの初検日チェック
        /// </summary>
        /// <param name="vbNengo">年号</param>
        /// <param name="vbY">和暦年</param>
        /// <param name="vbM">月</param>
        /// <param name="FushoDate">負傷ごとの初検日</param>
        /// <returns>初検日</returns>
        private DateTime chkFushoDate(VerifyBox vbNengo,VerifyBox vbY,VerifyBox vbM)
        {

            //年号
            int sG = vbNengo.GetIntValue();
            setStatus(vbNengo, sG < 1 || 5 < sG);

            //初検年月
            int sY = vbY.GetIntValue();
            int sM = vbM.GetIntValue();
            
            DateTime shoken = DateTime.MinValue;

            //西暦変換
            
            int.TryParse(sG.ToString() + sY.ToString("00") + sM.ToString("00"), out int inttmp);
            if (inttmp > 0) sY = DateTimeEx.GetAdYearMonthFromJyymm(inttmp) / 100;

            if (!DateTimeEx.IsDate(sY, sM, 1))
            {
                setStatus(vbY, true);
                setStatus(vbM, true);
            }
            else
            {
                setStatus(vbY, false);
                setStatus(vbM, false);
                shoken = new DateTime(sY, sM, 1);
            }
            return shoken;
        }
        


        /// <summary>
        /// 負傷ごとのデータを表示
        /// </summary>
        /// <param name="app"></param>
        private void setFusho(App app,bool nv)
        {

            //負傷名1
            setValue(verifyBoxF1Name, app.FushoName1, firstTime, nv);
       
            //負傷名2
            setValue(verifyBoxF2Name, app.FushoName2, firstTime, nv);
        
            //負傷名3
            setValue(verifyBoxF3Name, app.FushoName3, firstTime, nv);
                                          
            //負傷名4
            setValue(verifyBoxF4Name, app.FushoName4, firstTime, nv);
        
            //負傷名5
            setValue(verifyBoxF5Name, app.FushoName5, firstTime, nv);           

        }
       


        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (pnameControls.Contains(t)) posPname = pos;
            else if (clinicControls.Contains(t)) posClinic = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (clinicNumControls.Contains(t)) posClinicNum = pos;


        }
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.6f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

      
        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                void setPostal(PostalCode pc)
                {
                    if (pc == null) return;
                    verifyBoxHihozip.Text = pc.Postal_Code.ToString("0000000");
                    verifyBoxHihoadd.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    prevTodofuken = pc.Todofuken;
                    prevSikutyoson = pc.Sichokuson;
                    verifyBoxHihoadd.Focus();
                    verifyBoxHihoadd.Select(verifyBoxHihoadd.Text.Length, 0);
                    selectedHistory.Add(pc);
                }

                var x = panelRight.Location.X + panelpname.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = this.Height - 820;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible) pcForm.Show(this);
            else pcForm.MoveDefaultFocus();
        }
        

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
        #endregion

        private void verifyBoxHihozip_Leave(object sender, EventArgs e)
        {

            //郵便番号と住所入力欄があれば自動で設定する
            if (int.TryParse(verifyBoxHihozip.Text, out int zipcode))
            {
                var pc = PostalCode.GetPostalCode(zipcode);
                if (pc != null)
                {
                    var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    var curAdd = verifyBoxHihoadd.Text;
                    if (!curAdd.StartsWith(add)) verifyBoxHihoadd.Text = add;
                }
                else verifyBoxHihoadd.Text = "";
            }
        }

        private void verifyBoxDrCode_Leave(object sender, EventArgs e)
        {
            APP_ClinicInfo c = APP_ClinicInfo.selectFromDrNum(verifyBoxDrCode.Text.Trim());
            if (c == null) return;
            verifyBoxDrName.Text = c.strDrName;
            verifyBoxHosName.Text = c.strClinicName;

        }

        private void verifyBoxHihoadd_Leave(object sender, EventArgs e)
        {
            if (pcForm != null) pcForm.Close();
        }

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        //前のが空欄の場合次のフォーカスを飛ばす
        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3Name.TabStop = verifyBoxF2Name.Text != string.Empty;
            verifyBoxF4Name.TabStop = verifyBoxF3Name.Text != string.Empty;
            verifyBoxF5Name.TabStop = verifyBoxF4Name.Text != string.Empty;
        }

    
    }
}
