﻿namespace Mejor.MiyagiKokuho
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.verifyBoxBuiCount = new Mejor.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.label36 = new System.Windows.Forms.Label();
            this.verifyBoxF1Nengo = new Mejor.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.verifyBoxF1D = new Mejor.VerifyBox();
            this.verifyBoxCountedDays = new Mejor.VerifyBox();
            this.label44 = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelOCR = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.verifyBoxInsNum = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.verifyBoxHnumM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelTotal.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 250;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 22);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(190, 22);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(6, 42);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-196, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(8, 609);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(900, 80);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 100;
            this.dgv.TabStop = false;
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.verifyBoxBuiCount);
            this.panelTotal.Controls.Add(this.label38);
            this.panelTotal.Controls.Add(this.label35);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.label36);
            this.panelTotal.Controls.Add(this.verifyBoxF1Nengo);
            this.panelTotal.Controls.Add(this.label39);
            this.panelTotal.Controls.Add(this.label40);
            this.panelTotal.Controls.Add(this.label41);
            this.panelTotal.Controls.Add(this.verifyBoxF1Y);
            this.panelTotal.Controls.Add(this.verifyBoxF1M);
            this.panelTotal.Controls.Add(this.label42);
            this.panelTotal.Controls.Add(this.label43);
            this.panelTotal.Controls.Add(this.verifyBoxF1D);
            this.panelTotal.Controls.Add(this.verifyBoxCountedDays);
            this.panelTotal.Controls.Add(this.label44);
            this.panelTotal.Location = new System.Drawing.Point(19, 535);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(909, 70);
            this.panelTotal.TabIndex = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(33, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 113;
            this.label1.Text = "平:４、令:５";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // verifyBoxBuiCount
            // 
            this.verifyBoxBuiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuiCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuiCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuiCount.Location = new System.Drawing.Point(287, 20);
            this.verifyBoxBuiCount.MaxLength = 1;
            this.verifyBoxBuiCount.Name = "verifyBoxBuiCount";
            this.verifyBoxBuiCount.NewLine = false;
            this.verifyBoxBuiCount.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBuiCount.TabIndex = 20;
            this.verifyBoxBuiCount.TextV = "";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(254, 31);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 12);
            this.label38.TabIndex = 112;
            this.label38.Text = "部位";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(331, 31);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 12);
            this.label35.TabIndex = 10;
            this.label35.Text = "合計";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(487, 20);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxCharge.TabIndex = 40;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(363, 20);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 30;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(452, 31);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 12);
            this.label36.TabIndex = 10;
            this.label36.Text = "請求";
            // 
            // verifyBoxF1Nengo
            // 
            this.verifyBoxF1Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Nengo.Location = new System.Drawing.Point(99, 20);
            this.verifyBoxF1Nengo.MaxLength = 1;
            this.verifyBoxF1Nengo.Name = "verifyBoxF1Nengo";
            this.verifyBoxF1Nengo.NewLine = false;
            this.verifyBoxF1Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Nengo.TabIndex = 5;
            this.verifyBoxF1Nengo.TextV = "";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(136, 7);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 29;
            this.label39.Text = "年";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(100, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(29, 12);
            this.label40.TabIndex = 29;
            this.label40.Text = "年号";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(166, 7);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 12);
            this.label41.TabIndex = 31;
            this.label41.Text = "月";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(131, 20);
            this.verifyBoxF1Y.MaxLength = 2;
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Y.TabIndex = 6;
            this.verifyBoxF1Y.TextV = "";
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(162, 20);
            this.verifyBoxF1M.MaxLength = 2;
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1M.TabIndex = 7;
            this.verifyBoxF1M.TextV = "";
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(17, 31);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(80, 12);
            this.label42.TabIndex = 37;
            this.label42.Text = "初検年月日1";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(198, 7);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 12);
            this.label43.TabIndex = 42;
            this.label43.Text = "日";
            // 
            // verifyBoxF1D
            // 
            this.verifyBoxF1D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1D.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1D.Location = new System.Drawing.Point(194, 20);
            this.verifyBoxF1D.MaxLength = 2;
            this.verifyBoxF1D.Name = "verifyBoxF1D";
            this.verifyBoxF1D.NewLine = false;
            this.verifyBoxF1D.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1D.TabIndex = 8;
            this.verifyBoxF1D.TextV = "";
            // 
            // verifyBoxCountedDays
            // 
            this.verifyBoxCountedDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCountedDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCountedDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCountedDays.Location = new System.Drawing.Point(598, 20);
            this.verifyBoxCountedDays.Name = "verifyBoxCountedDays";
            this.verifyBoxCountedDays.NewLine = false;
            this.verifyBoxCountedDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxCountedDays.TabIndex = 50;
            this.verifyBoxCountedDays.TextV = "";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(596, 5);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(41, 12);
            this.label44.TabIndex = 10;
            this.label44.Text = "実日数";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 11);
            this.verifyBoxY.MaxLength = 2;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(155, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 100);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 429);
            this.scrollPictureControl1.TabIndex = 300;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 695);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(29, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.labelMacthCheck);
            this.panelHnum.Controls.Add(this.verifyBoxInsNum);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.verifyBoxHnumM);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Location = new System.Drawing.Point(94, 0);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(850, 100);
            this.panelHnum.TabIndex = 5;
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(730, 17);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 48;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // verifyBoxInsNum
            // 
            this.verifyBoxInsNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNum.Location = new System.Drawing.Point(597, 10);
            this.verifyBoxInsNum.Name = "verifyBoxInsNum";
            this.verifyBoxInsNum.NewLine = false;
            this.verifyBoxInsNum.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxInsNum.TabIndex = 10;
            this.verifyBoxInsNum.TextV = "";
            this.verifyBoxInsNum.Validated += new System.EventHandler(this.verifyBoxInsNum_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(531, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 30;
            this.label19.Text = "保険者番号";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHnumM
            // 
            this.verifyBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumM.Location = new System.Drawing.Point(8, 59);
            this.verifyBoxHnumM.MaxLength = 8;
            this.verifyBoxHnumM.Name = "verifyBoxHnumM";
            this.verifyBoxHnumM.NewLine = false;
            this.verifyBoxHnumM.ReadOnly = true;
            this.verifyBoxHnumM.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnumM.TabIndex = 13;
            this.verifyBoxHnumM.TabStop = false;
            this.verifyBoxHnumM.TextV = "";
            this.verifyBoxHnumM.Enter += new System.EventHandler(this.verifyBoxHnumM_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.Location = new System.Drawing.Point(114, 59);
            this.verifyBoxHnum.MaxLength = 10;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxHnum.TabIndex = 20;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(711, 59);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 29;
            this.verifyBoxBD.TextV = "";
            this.verifyBoxBD.Leave += new System.EventHandler(this.verifyBoxBD_Leave);
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(668, 59);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 28;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(625, 59);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 27;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(567, 59);
            this.verifyBoxBE.MaxLength = 1;
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 26;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(492, 59);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 24;
            this.verifyBoxSex.TextV = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(564, 45);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 18;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(490, 44);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 15;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(596, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 36);
            this.label5.TabIndex = 13;
            this.label5.Text = "昭:3\r\n平:4\r\n令:5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(519, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(737, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 25;
            this.label16.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(694, 69);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 23;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(651, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 21;
            this.label18.Text = "年";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxHnumM;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel panelTotal;
        private VerifyBox verifyBoxInsNum;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelMacthCheck;
        private VerifyBox verifyBoxBuiCount;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxF1Nengo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private VerifyBox verifyBoxF1Y;
        private VerifyBox verifyBoxF1M;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private VerifyBox verifyBoxF1D;
        private VerifyBox verifyBoxCountedDays;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label1;
    }
}