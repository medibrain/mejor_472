﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.MiyagiKokuho
{
    /// <summary>
    /// 保険者から提供される　給付データテーブルクラス
    /// </summary>
    class kyufudata
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]//20200521184821 furukawa nextval対策                               
        public int rrid { get; set; }                 //提供データ識別ID    => nextvalで取得するのでここでは設定しない
        public int cym { get; set; }                  //メホール上の処理年月
        public int ym { get; set; }                   //診療年月


        //20200615165943 furukawa st ////////////////////////
        //テーブル仕様変更に伴う変更


        public string shoriym { get; set; } = string.Empty;                        //処理年月
        public string searchid { get; set; } = string.Empty;                       //レセプト全国共通キー
        public string kokuhoren_rezeptno { get; set; } = string.Empty;             //国保連レセプト番号
        public string insnum { get; set; } = string.Empty;                         //保険者番号
        public string hihoMark { get; set; } = string.Empty;                       //被保険者証記号
        public string hihoNumw { get; set; } = string.Empty;                       //被保険者証番号(全角)
        public string hihoNumn { get; set; } = string.Empty;                       //被保険者証番号(半角)
        public string pbirth { get; set; } = string.Empty;                         //生年月日
        public string pgender { get; set; } = string.Empty;                        //性別
        public string pname { get; set; } = string.Empty;                          //受療者氏名
        public string pno { get; set; } = string.Empty;                            //個人番号
        public string publicexpence { get; set; } = string.Empty;                  //受給者番号
        public string imageno { get; set; } = string.Empty;                        //帳票イメージ番号
        public string shinsaym { get; set; } = string.Empty;                       //審査年月
        public string shinryoym { get; set; } = string.Empty;                       //診療年月
        public string honkenyugai { get; set; } = string.Empty;                    //本人家族入外
        public int counteddays { get; set; } = 0;                                  //保険_診療実日数
        public int acharge { get; set; } = 0;                                      //保険_請求点数
        public int atotal { get; set; } = 0;                                       //算定＿保険＿決定点数
        public DateTime pbirthad { get; set; } =DateTime.MinValue;                 //生年月日西暦 メホール管理用

        #region old

        //public string searchID { get; set; }            //レセプト全国共通キー
        //public string  ymwareki { get; set; }           //診療年月和暦
        //public string hihomark { get; set; }            //記号
        //public string hihonum { get; set; }             //番号
        //public int honke { get; set; }                  //本人家族区分
        //public int ratio { get; set; }                  //給付割合
        //public int pgender { get; set; }                //性別
        //public string pbirth { get; set; }              //生年月日
        //public string pname { get; set; }		        //氏名
        //public DateTime pbirthad { get; set; }          //生年月日西暦
        #endregion


        //20200615165943 furukawa ed ////////////////////////



        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int _cym)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv|すべてのファイル|*.*";
            ofd.FilterIndex = 0;
            ofd.Title = "宮城県柔整 給付データ";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;

            var l = CommonTool.CsvImportMultiCode(fileName);

            
            if (l == null) return false;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(l.Count-1);

            try
            {

                using (var tran = DB.Main.CreateTransaction())
                {
                    wf.LogPrint("取込中です");

                    foreach (var item in l)
                    {

                        //20200615171824 furukawa st ////////////////////////
                        //テーブル仕様変更に伴う変更

                        //ヘッダの場合は飛ばす（診療年月が数字かどうかで判断）
                        if (!int.TryParse(item[14], out int tmp)) continue;

                  
                        var kd = new kyufudata();

                        kd.cym = _cym;                                                              //メホール処理年月
                        kd.ym = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[14]));             //診療年月 西暦

                        kd.shoriym	=item[0];                  //処理年月
                        kd.searchid	=item[1];                  //レセプト全国共通キー
                        kd.kokuhoren_rezeptno=item[2];         //国保連レセプト番号
                        kd.insnum	=item[3];                  //保険者番号
                        kd.hihoMark	=item[4];                  //被保険者証記号
                        kd.hihoNumw	=item[5];                  //被保険者証番号(全角)
                        kd.hihoNumn	=item[6];                  //被保険者証番号(半角)
                        kd.pbirth	=item[7];                  //生年月日
                        kd.pgender	=item[8];                  //性別
                        kd.pname	=item[9];                  //受療者氏名
                        kd.pno	=item[10];                       //個人番号
                        kd.publicexpence=item[11];               //受給者番号
                        kd.imageno	=item[12];                  //帳票イメージ番号
                        kd.shinsaym	=item[13];                  //審査年月
                        kd.shinryoym	=item[14];                  //診療年月
                        kd.honkenyugai	=item[15];              //本人家族入外
                        kd.counteddays	=int.Parse(item[16]);  //保険_診療実日数
                        kd.acharge	=int.Parse(item[17]);        //保険_請求点数
                        kd.atotal = int.Parse(item[18]);        //算定＿保険＿決定点数
                        kd.pbirthad = DateTimeEx.GetDateFromJstr7(item[7]);                          //生年月日西暦 メホール管理用


                        #region old
                        //ヘッダの場合は飛ばす（診療年月が数字かどうかで判断）
                        //if (!int.TryParse(item[1], out int tmp)) continue;

                        //int.TryParse(item[4], out int honke);//本家区分
                        //int.TryParse(item[5], out int ratio);//給付割合
                        //int.TryParse(item[6], out int gender);//性別                    

                        //var kd = new kyufudata();

                        //kd.cym = _cym;                                                              //メホール処理年月
                        //kd.ym = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[1]));             //診療年月 西暦


                        //kd.searchID = item[0];                                                      //レセプト全国共通キー

                        //kd.ymwareki = int.Parse(item[1]).ToString();                                //診療年月 50202型の場合

                        //kd.hihomark = item[2];                                                      //記号番号(記号と番号が2フィールドの場合)
                        //kd.hihonum = item[3];                                                       //番号(記号と番号が2フィールドの場合)
                        //kd.honke = honke;                                                           //本家区分
                        //kd.ratio = ratio;                                                           //給付割合
                        //kd.pgender = gender;                                                        //性別
                        //kd.pbirth = item[7];                                                        //生年月日 5020310(文字列
                        //kd.pname = item[8];                                                         //氏名
                        //kd.pbirthad= DateTimeEx.GetDateFromJstr7(item[7]);                          //生年月日 5020310(文字列）=>2020/03/10（日付）

                        #endregion

                        //20200615171824 furukawa ed ////////////////////////

                        
                        wf.InvokeValue++;

                        if (!DB.Main.Insert(kd, tran)) return false;
                        
                    }

                    tran.Commit();



                }

                var tran2 = DB.Main.CreateTransaction();

                //保険者提供データとAppをレセプト全国共通キーで結合し、必要なデータをAppに登録
                MergeData(_cym, tran2, wf);

                tran2.Commit();


                System.Windows.Forms.MessageBox.Show("終了しました");
                return true;
            }
          
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(
                    System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;

            }
            finally
            {
                wf.Dispose();
            }
        }


        /// <summary>
        /// 保険者提供データとAppをレセプト全国共通キーで結合し、必要なデータをAppに登録
        /// </summary>
        /// <param name="cym"></param>        
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool MergeData(int cym,DB.Transaction tran,WaitForm wf)
        {

            List<App>lstapp=App.GetApps(cym);
            List<kyufudata> lstkyufu = kyufudata.select($"cym={cym}");
                                    
            wf.SetMax(lstapp.Count);
            wf.InvokeValue = 0;

            try
            {
                foreach (App a in lstapp)
                {                
                    foreach (kyufudata k in lstkyufu)
                    {
                        
                        if (k.searchid == a.Numbering)
                        {

                            a.ComNum = k.searchid;                                                      //レセプト全国共通キー
                            a.TaggedDatas.GeneralString6 = k.kokuhoren_rezeptno;                        //国保連レセプト番号
                            a.InsNum = k.insnum;                                                        //保険者番号
                            a.HihoNum = $"{k.hihoMark}|{k.hihoNumn}";                                   //被保険者証記号|番号
                            a.Birthday = k.pbirthad;                                                    //受療者生年月日
                            a.Sex = int.Parse(k.pgender);                                               //受療者性別
                            a.PersonName = k.pname;                                                     //受療者氏名                                                                                                        
                            a.PublcExpense = k.publicexpence;                                           //受給者番号1

                            
                            int intym = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(k.shinryoym));
                            a.YM = intym;                                                               //診療年月


                            //20200617131328 furukawa st ////////////////////////
                            //MediYear,MediMonthは診療年月ではなく処理年月が入る
                            
                            a.MediYear = int.Parse(k.shoriym.Substring(1, 2));                        //和暦診療年を入れないとYMが更新できない
                            a.MediMonth = int.Parse(k.shoriym.Substring(3, 2));                       //和暦診療年を入れたので月も入れとく

                            //a.MediYear = int.Parse(k.shinryoym.Substring(1, 2));                        //和暦診療年を入れないとYMが更新できない
                            //a.MediMonth = int.Parse(k.shinryoym.Substring(3, 2));                       //和暦診療年を入れたので月も入れとく
                            //20200617131328 furukawa ed ////////////////////////




                            //20200702150006 furukawa st ////////////////////////
                            //給付割合は本家入外区分で決めうち


                            //02本人＝７割
                            //04六歳＝８割
                            //06家族＝７割
                            //08高一＝８割（国保のため８割確定）
                            //00高７＝７割
                            //無いはずだが、2,4,6,8,0以外は0
                            int intRatio = 0;
                            if (!int.TryParse(k.honkenyugai, out int intHonkeNyugai))
                            {
                                intRatio = 0;
                            }
                            else if (new[]{ 2, 6, 0 }.Contains(intHonkeNyugai)) 
                            {
                                intRatio = 7;
                            }
                            else if (new[] { 4, 8 }.Contains(intHonkeNyugai))
                            {
                                intRatio = 8;
                            }
                            a.Ratio = intRatio;

                            //20200702150006 furukawa ed ////////////////////////


                            a.Family = int.Parse(k.honkenyugai);                                        //本人家族入院外来
                            a.CountedDays = k.counteddays;                                              //施術日数                            
                            a.Total = k.atotal;                                                         //合計金額


                            //20210209165312 furukawa st ////////////////////////
                            //合計×給付割合で請求金額を算出する場合、計算はDecimal型でやらないと丸め誤差出るので関数を使用する

                            a.Charge = CommonTool.CalcCharge(k.atotal, intRatio);

                                        //20200702145852 furukawa st ////////////////////////
                                        //請求金額＝合計×給付割合　小数点以下切り捨ての自動計算にする                      

                                        //      double decCharge = double.Parse(k.atotal.ToString()) * (double.Parse(intRatio.ToString())/10);
                                        //      a.Charge = int.Parse(Math.Truncate(decCharge).ToString());                  //請求金額

                                        //      //System.Diagnostics.Debug.Print($"合計:{a.Total},給付割合:{a.Ratio},請求:{decCharge.ToString()}請求:{a.Charge.ToString()}");

                                        ////      a.Charge = k.acharge;                                                     //請求金額
                                        //20200702145852 furukawa ed ////////////////////////

                            //20210209165312 furukawa ed ////////////////////////

                            a.Update(0, App.UPDATE_TYPE.Null, tran);


                            break;
                        }
                    }
                    wf.LogPrint(a.Numbering);
                    wf.InvokeValue++;


                }
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
         

        }

        /// <summary>
        /// 給付データ抽出
        /// </summary>
        /// <param name="strwhere">抽出条件(whereいらんsql)</param>
        /// <returns></returns>
        public static List<kyufudata> select(string strwhere)
        {
            List<kyufudata> lst = new List<kyufudata>();
            var l=DB.Main.Select<kyufudata>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }

        
    }
}
