﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.MiyagiKokuho

{
    public partial class InputForm1 : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;
        private string rrid = string.Empty;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);                
        Point poshnum = new Point(0, 0);        

        Control[] ymControls,  hnumControls;

        public InputForm1(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] {
                verifyBoxY, verifyBoxM,
            };


            //上段
            hnumControls = new Control[]
            {
                verifyBoxF1Nengo,verifyBoxF1Y,
                verifyBoxF1M,verifyBoxF1D,
                verifyBoxBuiCount,
                verifyBoxFamily,
            };


            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";


            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            
            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);            
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = poshnum;
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// 登録ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }
        #endregion


        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="app">applicationクラス</app>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力値チェック
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month > 12);

            //初検日1
            DateTime dtFirstDate1 = dateCheck(verifyBoxF1Nengo, verifyBoxF1Y, verifyBoxF1M, verifyBoxF1D);


            //20200825174758 furukawa st ////////////////////////
            //新規継続は先行入力で判定

            //診療年月と初検日１を比較し新規継続判定
            bool flgNewContWarning = false;
            DateTime dtYM = dateCheck(verifyBoxY, verifyBoxM);
            if (dtYM > dtFirstDate1) app.NewContType = NEW_CONT.継続;
            else if (dtYM < new DateTime(dtFirstDate1.Year, dtFirstDate1.Month, 1)) flgNewContWarning = true;
            else app.NewContType = NEW_CONT.新規;


            if (flgNewContWarning)
            {
                setStatus(verifyBoxY, true);
                setStatus(verifyBoxM, true);
                setStatus(verifyBoxF1Y, true);
                setStatus(verifyBoxF1M, true);

               // verifyBoxY.BackColor = Color.GreenYellow;
               // verifyBoxM.BackColor = Color.GreenYellow;
               // verifyBoxF1Y.BackColor = Color.GreenYellow;
               // verifyBoxF1M.BackColor = Color.GreenYellow;

               // var res = MessageBox.Show("施術年月・初検日のいずれか、" +
               //"または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
               //MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
               // if (res != System.Windows.Forms.DialogResult.OK) return false;
            }


            //20200825174758 furukawa ed ////////////////////////


            //本人家族
            int intInputFamily = verifyBoxFamily.GetIntValue();
            //本人か、家族のどちらかしかないのでそれ以外はエラー
            setStatus(verifyBoxFamily,intInputFamily!=2 && intInputFamily != 6);

            //部位数(数字入力)
            int buicount = verifyBoxBuiCount.GetIntValue();


          
            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }



            #endregion

            #region 値の反映
            //値の反映

            //施術年
            app.MediYear = year;

            //施術月
            app.MediMonth = month;


            //20200527181413 furukawa st ////////////////////////
            //申請書種別の登録            
            app.AppType = scan.AppType;
            //20200527181413 furukawa ed ////////////////////////

            //初検日1
            app.FushoFirstDate1 = dtFirstDate1;

            //部位数(数字入力)
            app.TaggedDatas.count = buicount;

            


            //20200702094130 furukawa st ////////////////////////
            //そのまま入れると文字列追記になるので最後尾の文字を取得し入力の家族とくっつける
            
            //本家区分(給付データ)x100+本人家族(入力)
            if (app.Family.ToString().Trim() != string.Empty)
            {
                string family_kyufu = app.Family.ToString().Substring(app.Family.ToString().Length-1);
                app.Family = int.Parse(family_kyufu) * 100 + intInputFamily ;
            }
            // app.Family =  app.Family*100+ intFamily ;
            //20200702094130 furukawa ed ////////////////////////
            #endregion

            return true;
        }

         
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
           
            changedReset(app);
        }

    
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
      
                //申請書
                //施術年月
                setValue(verifyBoxY, app.MediYear, firstTime, false);
                setValue(verifyBoxM, app.MediMonth, firstTime, false);

                //初検日1
                if (app.FushoFirstDate1 != DateTime.MinValue)
                {
                    int intEra = 0;
                    int int1stY = 0;
                    int int1stM = 0;
                    int int1stD = 0;

                    intEra = DateTimeEx.GetEraNumber(app.FushoFirstDate1);
                    int1stY = DateTimeEx.GetJpYear(app.FushoFirstDate1);
                    int1stM = app.FushoFirstDate1.Month;
                    int1stD = app.FushoFirstDate1.Day;

                    setValue(verifyBoxF1Nengo, intEra, firstTime, nv);
                    setValue(verifyBoxF1Y, int1stY, firstTime, nv);
                    setValue(verifyBoxF1M, int1stM, firstTime, nv);
                    setValue(verifyBoxF1D, int1stD, firstTime, nv);
                }

                //部位数(数字入力）
                setValue(verifyBoxBuiCount, app.TaggedDatas.count, firstTime, nv);

                //本人家族（本家区分*100＋本人家族で格納されている）
                setValue(verifyBoxFamily, app.Family % 100, firstTime, nv);

            }
        }



        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                
                //施術月
                verifyBoxM.Visible = true;
                labelM.Visible = true;
             

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;

        }
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.6f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

  



        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }       
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

   
    }
}
