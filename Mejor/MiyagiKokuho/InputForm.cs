﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

//2020/06/15仕様変更により未使用

namespace Mejor.MiyagiKokuho

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;
        private string rrid = string.Empty;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);        
        Point posCost = new Point(0, 1200);
        Point poshnum = new Point(0, 0);
        Point posCountedDays = new Point(0, 1000);

        /// <summary>
        /// 保険者番号から被保険者証記号をロード
        /// </summary>
        List<hokensha> lstHokensha = new List<hokensha>();


        Control[] ymControls, costControls, hnumControls,counteddaysControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] {
                verifyBoxY, verifyBoxM,
            };


            //上段
            hnumControls = new Control[]
            {
                verifyBoxHnum,verifyBoxHnumM,//verifyBoxHnumM2,verifyBoxHnumMChar,
                verifyBoxBD,verifyBoxBE,verifyBoxBM,verifyBoxBY,
                verifyBoxSex,verifyBoxInsNum,
                verifyBoxF1Nengo,verifyBoxF1Y,
                verifyBoxF1M,verifyBoxF1D,
                verifyBoxBuiCount,
            };

            //合計金額箇所
            costControls = new Control[]
            {
                verifyBoxTotal,verifyBoxCharge,
            };
            //施術日数
            counteddaysControls = new Control[] { verifyBoxCountedDays };

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            panelHnum.Visible = false;
            
            #region 給付データリスト
            var l = new List<kyufudata>();
            //dgv.DataSource = l;

            setDataGridViewFormat(l);
            #endregion


            //保険者番号、被保険者証記号ロード
            hokensha hokensha = new hokensha();
            lstHokensha=hokensha.selectAll();


            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            
            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);            
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = poshnum;
            else if (counteddaysControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;

        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// 登録ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }
        #endregion

        #region 給付データグリッド初期化
        /// <summary>
        /// 給付データグリッド初期化
        /// </summary>
        /// <param name="k"></param>
        private void setDataGridViewFormat(List<kyufudata> k)
        {
            dgv.DataSource = k;
            dgv.Columns[nameof(kyufudata.cym)].Visible = false;
            dgv.Columns[nameof(kyufudata.cym)].Width = 0;
            dgv.Columns[nameof(kyufudata.cym)].HeaderText = "処理年月";
            dgv.Columns[nameof(kyufudata.ym)].Visible = false;
            dgv.Columns[nameof(kyufudata.ym)].Width = 0;
            dgv.Columns[nameof(kyufudata.ym)].HeaderText = "診療年月西暦";
            dgv.Columns[nameof(kyufudata.searchid)].Visible = true;
            dgv.Columns[nameof(kyufudata.searchid)].Width = 150;
            dgv.Columns[nameof(kyufudata.searchid)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(kyufudata.shinryoym)].Visible = true;
            dgv.Columns[nameof(kyufudata.shinryoym)].Width = 80;
            dgv.Columns[nameof(kyufudata.shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(kyufudata.hihoMark)].Visible = true;
            dgv.Columns[nameof(kyufudata.hihoMark)].Width = 70;
            dgv.Columns[nameof(kyufudata.hihoMark)].HeaderText = "証記号";
            dgv.Columns[nameof(kyufudata.hihoNumn)].Visible = true;
            dgv.Columns[nameof(kyufudata.hihoNumn)].Width = 100;
            dgv.Columns[nameof(kyufudata.hihoNumn)].HeaderText = "証番号";
            dgv.Columns[nameof(kyufudata.honkenyugai)].Visible = true;
            dgv.Columns[nameof(kyufudata.honkenyugai)].Width = 30;
            dgv.Columns[nameof(kyufudata.honkenyugai)].HeaderText = "本家";
            //dgv.Columns[nameof(kyufudata.ratio)].Visible = true;
            //dgv.Columns[nameof(kyufudata.ratio)].Width = 30;
            //dgv.Columns[nameof(kyufudata.ratio)].HeaderText = "割合";
            dgv.Columns[nameof(kyufudata.pgender)].Visible = true;
            dgv.Columns[nameof(kyufudata.pgender)].Width = 30;
            dgv.Columns[nameof(kyufudata.pgender)].HeaderText = "性別";
            dgv.Columns[nameof(kyufudata.pbirth)].Visible = true;
            dgv.Columns[nameof(kyufudata.pbirth)].Width = 80;
            dgv.Columns[nameof(kyufudata.pbirth)].HeaderText = "受療者生年月日";
            dgv.Columns[nameof(kyufudata.pname)].Visible = true;
            dgv.Columns[nameof(kyufudata.pname)].Width = 150;
            dgv.Columns[nameof(kyufudata.pname)].HeaderText = "受療者名";
            dgv.Columns[nameof(kyufudata.pbirthad)].Visible = true;
            dgv.Columns[nameof(kyufudata.pbirthad)].Width = 100;
            dgv.Columns[nameof(kyufudata.pbirthad)].HeaderText = "受療者生年月日西暦";

            dgv.Columns[nameof(kyufudata.rrid)].Visible = true;
            dgv.Columns[nameof(kyufudata.rrid)].Width = 30;
            dgv.Columns[nameof(kyufudata.rrid)].HeaderText = "id";


        }
        #endregion



        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="app">applicationクラス</app>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力値チェック
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1 );
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month>12);

            //保険者番号 10桁以内？
            string insnum = verifyBoxInsNum.Text;
            setStatus(verifyBoxInsNum, insnum.Length < 0 || insnum.Length > 10);

            //20200614130150 furukawa st ////////////////////////
            //被保険者証記号番号を2つに戻した。パイプつなぎで格納
            
            //被保番 宮城県国保独自「み登米123A123456」とか
            //string hnum = $"{verifyBoxHnumM.Text.Trim()}|{verifyBoxHnumM2.Text.Trim()}|{verifyBoxHnumMChar.Text.Trim()}|{verifyBoxHnum.Text.Trim()}";
            //み登米|123A123456
            string hnum = $"{verifyBoxHnumM.Text.Trim()}|{verifyBoxHnum.Text.Trim()}";
            //20200614130150 furukawa ed ////////////////////////



            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //初検日1
            DateTime dtFirstDate1 = dateCheck(verifyBoxF1Nengo, verifyBoxF1Y, verifyBoxF1M, verifyBoxF1D);

            //20200701173313 furukawa st ////////////////////////
            //提供データ（給付データ）に入っている診療＿実日数を使用するので、ここでは入れない

            //施術日数
            //int intcounteddays = verifyBoxCountedDays.GetIntValue();
            //20200701173313 furukawa ed ////////////////////////


            //部位数(数字入力)
            int buicount = verifyBoxBuiCount.GetIntValue();

          
            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }


            //金額割合チェック
            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 10 | total < charge);

            //請求金額が合計金額の7～10割の金額と合致すればOK
            //bool payError = false;

            //if ((int)(total * 70 / 100) != charge &&
            //    (int)(total * 80 / 100) != charge &&
            //    (int)(total * 90 / 100) != charge &&
            //    (int)(total * 100 / 100) != charge)
            //{
            //    payError = true;
            //}

            ////金額でのエラーがあれば確認
            //if (payError)
            //{
            //    verifyBoxTotal.BackColor = Color.GreenYellow;
            //    verifyBoxCharge.BackColor = Color.GreenYellow;
            //    var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
            //        "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
            //        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            //    if (res != DialogResult.OK) return false;
            //}
            #endregion

            #region 値の反映
            //値の反映

            //施術年
            app.MediYear = year;

            //施術月
            app.MediMonth = month;

            
            //保険者番号
            app.InsNum = insnum;

            //ヒホバン
            app.HihoNum = hnum;

       
            //性別
            app.Sex = sex;
            //生年月日
            app.Birthday = birthDt;


            //合計金額
            app.Total = total;
            //請求金額
            app.Charge = charge;
          
       
            //20200527181413 furukawa st ////////////////////////
            //申請書種別の登録            
            app.AppType = scan.AppType;
            //20200527181413 furukawa ed ////////////////////////

            //初検日1
            app.FushoFirstDate1 = dtFirstDate1;

            //20200701173718 furukawa st ////////////////////////
            //提供データ（給付データ）に入っている診療＿実日数を使用するので、ここでは入れない
            
            //施術日数
            //app.CountedDays = intcounteddays;
            //20200701173718 furukawa ed ////////////////////////



            //部位数(数字入力)
            app.TaggedDatas.count = buicount;



            if (dgv.RowCount>0)
            {
                //提供データのrridを紐づける
                if (int.TryParse(dgv.CurrentRow.Cells["rrid"].Value.ToString(), out int tmp)) app.RrID = tmp;
                
                //レセプト全国共通キー
                if (dgv.CurrentRow.Cells["searchid"].Value.ToString() != string.Empty)
                    app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells["searchid"].Value.ToString();

                //20200614184724 furukawa st ////////////////////////
                //給付データより家族、給付、患者名をセット
                
                //患者名
                app.PersonName =dgv.CurrentRow.Cells["pname"].Value.ToString() ?? string.Empty;
                //給付割合
                int.TryParse(dgv.CurrentRow.Cells["ratio"].Value.ToString(),out int tmpratio);
                app.Ratio = tmpratio;
                //家族区分
                int.TryParse(dgv.CurrentRow.Cells["family"].Value.ToString(), out int tmpfamily);
                app.Family = tmpfamily;

                //20200614184724 furukawa ed ////////////////////////
            }
            #endregion

            return true;
        }

         
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWrite(app, INPUT_TYPE.First, 0, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWrite(app, INPUT_TYPE.Second, secondMissCount, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //マッチング表示初期化
            labelMacthCheck.BackColor = Color.Pink;
            labelMacthCheck.Text = "マッチング無し";
            labelMacthCheck.Visible = true;

            dgv.DataSource = null;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
           
            changedReset(app);
        }

    
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            //このへんにグリッド初期化処理
            dgv.DataSource = null;


            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号

                //20200614130038 furukawa st ////////////////////////
                //被保険者証記号番号を2つに戻した。記号等は手入力する
                
                //var hn = app.HihoNum.Split('|');

                //if (hn.Length == 4)
                //{
                //    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                //    setValue(verifyBoxHnumM2, hn[1], firstTime, nv);
                //    setValue(verifyBoxHnumMChar, hn[2], firstTime, nv);
                //    setValue(verifyBoxHnum, hn[3], firstTime, nv);
                //}


                var hn = app.HihoNum.Split('|');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }
                //20200614130038 furukawa ed ////////////////////////



                //申請書
                //施術年月
                setValue(verifyBoxY, app.MediYear, firstTime, false);
                setValue(verifyBoxM, app.MediMonth, firstTime, false);

                //保険者番号　今回は宮城県全域なので必要だろう
                setValue(verifyBoxInsNum, app.InsNum, firstTime, nv);
                         
                //性別
                setValue(verifyBoxSex, app.Sex, firstTime, nv);


                //生年月日
                int era = DateTimeEx.GetEraNumber(app.Birthday);
                int WarekiYear = DateTimeEx.GetJpYear(app.Birthday);

                setValue(verifyBoxBE, era, firstTime, nv);
                setValue(verifyBoxBY, WarekiYear, firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);

                //合計
                setValue(verifyBoxTotal, app.Total, firstTime, nv);

                //請求
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);

                //5010501型生年月日
                string strbirth = verifyBoxBE.Text + verifyBoxBY.Text.PadLeft(2, '0') + 
                    verifyBoxBM.Text.PadLeft(2, '0') + verifyBoxBD.Text.PadLeft(2, '0');


                //初検日1
                if (app.FushoFirstDate1 != DateTime.MinValue)
                {
                    int intEra = 0;
                    int int1stY = 0;
                    int int1stM = 0;
                    int int1stD = 0;

                    intEra = DateTimeEx.GetEraNumber(app.FushoFirstDate1);
                    int1stY = DateTimeEx.GetJpYear(app.FushoFirstDate1);
                    int1stM = app.FushoFirstDate1.Month;
                    int1stD = app.FushoFirstDate1.Day;

                    setValue(verifyBoxF1Nengo, intEra, firstTime, nv);
                    setValue(verifyBoxF1Y, int1stY, firstTime, nv);
                    setValue(verifyBoxF1M, int1stM, firstTime, nv);
                    setValue(verifyBoxF1D, int1stD, firstTime, nv);
                }

                //部位数(数字入力）
                setValue(verifyBoxBuiCount, app.TaggedDatas.count, firstTime, nv);
                //施術日数
                setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                string strhnumM = verifyBoxHnumM.Text.Trim();
                //string strhnumM = verifyBoxHnumM.Text.Trim() + verifyBoxHnumM2.Text.Trim() + verifyBoxHnumMChar.Text.Trim();

                //給付データ表示
                dispKyufuData(app, strhnumM, verifyBoxHnum.Text.Trim(), verifyBoxSex.Text.Trim(), strbirth);

            }
        }



        /// <summary>
        /// 給付データ表示
        /// </summary>
        /// <param name="app">APP</param>
        /// <param name="hnumM">記号</param>
        /// <param name="hnum">番号</param>
        /// <param name="gender">性別</param>
        /// <param name="pbirth">生年月日</param>
        /// <returns></returns>
        private bool dispKyufuData(App app,string hnumM,string hnum,string gender,string pbirth)
        {
            
            int cym = app.CYM;
            try
            {
                //処理年月、被保険者証記号、番号、性別、生年月日
                //レコード抽出
                List<kyufudata> lstKyufudata = new List<kyufudata>();
                string strsql= $" cym={cym} " +
                $" and hihomark='{hnumM}'" +
                $" and hihonum='{hnum}'" +
                $" and pgender='{gender}'" +
                $" and pbirth='{pbirth}'";

                //dgv.DataSource = kyufudata.select(strsql);

                var l = kyufudata.select(strsql);
                //グリッドデータ設定
                setDataGridViewFormat(l);


                #region マッチング判定
                if (l == null || l.Count == 0)
                {
                    labelMacthCheck.BackColor = Color.Pink;
                    labelMacthCheck.Text = "マッチング無し";
                    labelMacthCheck.Visible = true;
                }
                else if (l.Count == 1 )
                {
                    labelMacthCheck.BackColor = Color.Cyan;
                    labelMacthCheck.Text = "マッチングOK";
                    labelMacthCheck.Visible = true;
                }

                else
                {
                    for (int i = 0; i < dgv.RowCount; i++)
                    {                            
                        var rrid = (int)dgv[nameof(kyufudata.rrid), i].Value;

                        if (app.RrID == rrid)
                        {
                            //既に一意の広域データとマッチング済みの場合
                            labelMacthCheck.BackColor = Color.Cyan;
                            labelMacthCheck.Text = "マッチングOK";
                            labelMacthCheck.Visible = true;
                                
                            //グリッドの現在のセルが不可視の場合選択しない（例外発生する）
                            if (dgv[0, i].Visible) dgv.CurrentCell = dgv[0, i];

                            return true;
                        }
                    }

                    labelMacthCheck.BackColor = Color.Yellow;
                    labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                    labelMacthCheck.Visible = true;
                    dgv.CurrentCell = null;
                    
                }
                #endregion

                return true;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }




        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelHnum.Visible = true;
                
                //施術月
                verifyBoxM.Visible = true;
                labelM.Visible = true;
             

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (costControls.Contains(t)) posCost = pos;

        }
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

  

        //生年月日を入力した時点で提供データ検索、表示
        private void verifyBoxBD_Leave(object sender, EventArgs e)
        {
            searchKyufuData();
        }


        /// <summary>
        /// 給付データ検索
        /// </summary>
        private void searchKyufuData()
        {

            //被保険者証記号、番号、性別、生年月日
            if (verifyBoxHnumM.Text.Trim() == string.Empty ||
                verifyBoxHnum.Text.Trim() == string.Empty ||
                verifyBoxSex.Text.Trim() == string.Empty ||
                verifyBoxBE.Text.Trim() == string.Empty ||
                verifyBoxBY.Text.Trim() == string.Empty ||
                verifyBoxBM.Text.Trim() == string.Empty ||
                verifyBoxBD.Text.Trim() == string.Empty
                ) return;

            var app = (App)bsApp.Current;

            //被保番がどんな形で提供されるか分からん
            //被保険者記号が数字の前全部と仮定する
            string strHihomark = verifyBoxHnumM.Text.Trim();// + verifyBoxHnumM2.Text.Trim() + verifyBoxHnumMChar.Text.Trim();


            string strbirth = verifyBoxBE.Text + verifyBoxBY.Text.PadLeft(2, '0')
                + verifyBoxBM.Text.PadLeft(2, '0') + verifyBoxBD.Text.PadLeft(2, '0');

            dispKyufuData(app,strHihomark, verifyBoxHnum.Text.Trim(), verifyBoxSex.Text.Trim(), strbirth);
        }

  

        /// <summary>
        /// 保険者番号入れたら記号入れる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxInsNum_Validated(object sender, EventArgs e)
        {
            foreach(hokensha h in lstHokensha)
            {
                if (h.kokuho_insnum.Length == 0) continue;
                if(h.kokuho_insnum== verifyBoxInsNum.Text)
                {
                    verifyBoxHnumM.Text = h.hmark;
                    //verifyBoxHnumMChar.Text = h.hmarkChar;
                    return;
                }

                if (h.koki_insnum.Length == 0) continue;
                if (h.koki_insnum == verifyBoxInsNum.Text)
                {
                    verifyBoxHnumM.Text = h.hmark;
                    //verifyBoxHnumMChar.Text = h.hmarkChar;
                    return;
                }
            }
        }

        /// <summary>
        /// 被保険者証記号の後ろを編集しやすいように
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxHnumM_Enter(object sender, EventArgs e)
        {
            verifyBoxHnumM.Select(verifyBoxHnumM.Text.Length, 1);
        }
      

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }       
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

   
    }
}
