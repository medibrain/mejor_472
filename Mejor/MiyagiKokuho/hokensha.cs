﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.MiyagiKokuho
{
    /// <summary>
    /// 保険者番号テーブルクラス
    /// </summary>
    class hokensha
    {
        public int sortno { get; set; } = 0;                        //ソート順
        public string insnum { get; set; } = string.Empty;          //保険者のあたまの番号
        public string insname { get; set; } = string.Empty;         //保険者名
        public string hmark { get; set; } = string.Empty;           //保険者番号ルール第1項目
        public string hmarkChar { get; set; } = string.Empty;           //保険者番号ルール第3項目
        public string posix { get; set; } = string.Empty;           //正規表現の検索文
        public string kokuho_insnum { get; set; } = string.Empty;   //国保保険者番号
        public string koki_insnum { get; set; } = string.Empty;     //後期高齢保険者番号



        public static List<hokensha> select(string strwhere)
        {
            List<hokensha> lst = new List<hokensha>();
            var l=DB.Main.Select<hokensha>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }

        /// <summary>
        /// 保険者番号を全てロード
        /// </summary>
        /// <returns></returns>
        public static List<hokensha> selectAll()
        {
            List<hokensha> lst = new List<hokensha>();
            var l = DB.Main.SelectAll<hokensha>();
            foreach (var item in l) lst.Add(item);
            return lst;
        }

    }
}
