﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.MiyagiKokuho

{
    public partial class InputForm3 : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;
        private string rrid = string.Empty;


        /// <summary>
        /// 住所検索フォーム用
        /// </summary>
        private PostalCodeForm pcForm;
        private List<PostalCode> selectedHistory = new List<PostalCode>();
        private string prevTodofuken;
        private string prevSikutyoson;



        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);

        Point posstday1 = new Point(10, 100);
        Point posClinicZip = new Point(10, 2200);

        Control[] addressControls, stDayControls;

        public InputForm3(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            addressControls = new Control[] {
                verifyBoxClinicZip,verifyBoxClinicAdd,
            };


            stDayControls = new Control[]
            {
                verifyBoxStartDay1,
            };

            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            //20200713182822 furukawa st ////////////////////////
            //リスト表示条件変更
            
            var list = App.GetAppsWithWhere($" where groupid={this.scanGroup.GroupID} and (a.statusflags=(a.statusflags|{(int)StatusFlag.処理4}) or a.statusflags=(a.statusflags|{(int)StatusFlag.処理3}) )");
            //var list = App.GetAppsWithWhere($" where groupid={this.scanGroup.GroupID} and a.statusflags=(a.statusflags|{(int)StatusFlag.処理4})");
            //var list = App.GetAppsGID(this.scanGroup.GroupID);
            //20200713182822 furukawa ed ////////////////////////


            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusSP2)].DisplayIndex = 1;


            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            

            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

         
            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);


            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion


            focusBack(false);
        }

        private void InputForm3_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }


        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }
        #endregion

        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            if (addressControls.Contains(t)) scrollPictureControl1.ScrollPosition = posClinicZip;            
            else if (stDayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posstday1;

            //20200904145503 furukawa st ////////////////////////
            //住所欄のカーソル位置を最後に
            
            if (t == verifyBoxClinicAdd) verifyBoxClinicAdd.Select(verifyBoxClinicAdd.Text.Length, 0);
            //20200904145503 furukawa ed ////////////////////////
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        /// <summary>
        /// 登録ショートカットキー
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();//住所検索
        }
        
        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="app">applicationクラス</app>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            #region 入力値チェック
            hasError = false;

            string strClinicZip = verifyBoxClinicZip.Text;
            setStatus(verifyBoxClinicZip, strClinicZip != string.Empty && strClinicZip.Length != 7);

            int intstday1 = verifyBoxStartDay1.GetIntValue();
            setStatus(verifyBoxStartDay1, intstday1 < 0 || intstday1 > 31);


            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }


            #endregion

            #region 値の反映

            app.ClinicAdd = verifyBoxClinicAdd.Text.Trim();
            app.ClinicZip = strClinicZip;
            //開始日＝診療年月＋開始日付
            app.FushoStartDate1 =
                new DateTime(app.YM/100, app.YM%100, verifyBoxStartDay1.GetIntValue());

            #endregion

            return true;
        }




        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.追加ベリ済)) return true;


            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }


            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.Extra1 : App.UPDATE_TYPE.Extra2;

                if (firstTime && app.Ufirst == 0)
                {
                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Extra1, 0,DateTime.Now-dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.Extra1, 0, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;

                    //20200806111633 furukawa st ////////////////////////
                    //一申請書の入力時間計測を正確にするため関数置換
                    
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Extra2, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                    //if (!InputLog.LogWrite(app, INPUT_TYPE.Extra2, secondMissCount, jyuTran)) return false;
                    //20200806111633 furukawa ed ////////////////////////
                }

                if (!app.Update_SP(User.CurrentUser.UserID, ut, tran, Insurer.CurrrentInsurer.InsurerID)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        #endregion

        #region App表示
        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);


            //入力ユーザー表示
            string strUser1 = string.Empty;
            if (int.TryParse(app.TaggedDatas.GeneralString4, out int u1)) strUser1 = User.GetUserName(u1);
            string strUser2 = string.Empty;
            if (int.TryParse(app.TaggedDatas.GeneralString5, out int u2)) strUser2 = User.GetUserName(u2);

            labelInputerName.Text = "入力1:  " + strUser1 + "\r\n入力2:  " + strUser2;

            //labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
            //    "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.追加入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }

            changedReset(app);
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.追加ベリ済);

            //施術所郵便番号
            setValue(verifyBoxClinicZip, app.ClinicZip, firstTime, nv);
            //施術所住所
            setValue(verifyBoxClinicAdd, app.ClinicAdd, firstTime, nv);
            //開始日１
            setValue(verifyBoxStartDay1, app.FushoStartDate1.Day, firstTime, nv);


        }
        #endregion
        
        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (addressControls.Contains(t)) posClinicZip = pos;
            else if (stDayControls.Contains(t)) posstday1 = pos;
            


        }

        
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.6f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }
        #endregion


     

        #region 住所ダイアログ関係
        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                void setPostal(PostalCode pc)
                {
                    if (pc == null) return;
                    verifyBoxClinicZip.Text = pc.Postal_Code.ToString("0000000");
                    verifyBoxClinicAdd.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    prevTodofuken = pc.Todofuken;
                    prevSikutyoson = pc.Sichokuson;
                    verifyBoxClinicAdd.Focus();
                    verifyBoxClinicAdd.Select(verifyBoxClinicAdd.Text.Length, 0);
                    selectedHistory.Add(pc);
                }

                var x = panelRight.Location.X + panelClinic.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = this.Height - 820;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible) pcForm.Show(this);
            else pcForm.MoveDefaultFocus();
        }

        private void verifyBoxClinicZip_Leave(object sender, EventArgs e)
        {
            //郵便番号と住所入力欄があれば自動で設定する
            if (int.TryParse(verifyBoxClinicZip.Text, out int zipcode))
            {
                var pc = PostalCode.GetPostalCode(zipcode);
                if (pc != null)
                {
                    var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    var curAdd = verifyBoxClinicAdd.Text;
                    if (!curAdd.StartsWith(add)) verifyBoxClinicAdd.Text = add;
                }
                else verifyBoxClinicAdd.Text = "";
            }
        }

        private void verifyBoxClinicAdd_Leave(object sender, EventArgs e)
        {
            if (pcForm != null) pcForm.Close();
        }
        #endregion

        #region 一つ前の申請書
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
        #endregion




    }

}
