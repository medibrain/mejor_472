﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
//20190429180440 furukawa st ////////////////////////
//ほかの点検メニュー

    public partial class OtherTenkenMenuForm : Form
    {
        private Insurer ins;
        int cym;

        public OtherTenkenMenuForm(Insurer ins, int cym)
        {
            InitializeComponent();
            this.ins = ins;
            this.cym = cym;

            //ボタン操作
            if (ins.InsurerType == INSURER_TYPE.学校共済)
            {
                /*
                btn2.Enabled = true;
                btn2.Text = "提出データ 事前チェック";
                button3.Enabled = true;
                button3.Text = "金額確認";
                button4.Enabled = true;
                button4.Text = "登録済み施術所(師)一覧";*/
            }
            else if (ins.EnumInsID == InsurerID.CHUO_RADIO)
            {
                btn2.Enabled = true;
                btn2.Text = "並び替え印刷";
            }
            else if (ins.EnumInsID == InsurerID.SHIZUOKA_KOIKI)
            {
                btn2.Enabled = true;
                btn2.Text = "1/10枚 画像抽出";
            }

            if (ins.EnumInsID == InsurerID.HIROSHIMA_KOIKI || ins.EnumInsID == InsurerID.OSAKA_KOIKI)
            {
                button5.Enabled = true;
                button5.Text = "状態記入書画像 一括抽出";
            }

            switch (ins.EnumInsID)
            {
                case InsurerID.SHARP_KENPO:
                    btn1.Text = "資格点検";
                    btn2.Text = "点検作業";
                    btn3.Enabled = false;
                    //btn3.Text = "医科レセ突合用"; 

                    break;
            }
        }
        
        /// <summary>
        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn1_Click(object sender, EventArgs e)
        {
            int iID = ins.InsurerID;
            switch(iID)
            {
                case (int)InsurerID.SHARP_KENPO:
                    using (var f = new SharpKenpo.InputForm_Tekiyo(cym))
                    {
                        f.ShowDialog();
                    }
                    break;
                default:
                    break;
            }

            
            
        }

        /// <summary>
        /// 点検業務
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn2_Click(object sender, EventArgs e)
        {
            using (var f = new Inspect.InspectListForm(cym))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
            
        }

        /// <summary>
        /// 医科突合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int iID = ins.InsurerID;
            switch (iID)
            {
                //case (int)InsurerID.SHARP_KENPO:
                //    using (var f = new SharpKenpo.InputForm_IkaTotu(cym))
                //    {
                //        f.ShowDialog();
                //    }
                //    break;
                //default:
                //    break;
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(button4.Text == "登録済み施術所(師)一覧")
            {
                var fn = Insurer.CurrrentInsurer.InsurerName + "_登録済み施術所(師)一覧.csv";
                using (var f = new SaveFileDialog())
                {
                    f.FileName = fn;
                    if (f.ShowDialog() != DialogResult.OK) return;

                    if(Clinic.CreateListCsv(f.FileName))
                        MessageBox.Show("出力が完了しました");
                    else
                        MessageBox.Show("出力に失敗しました");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return;

            CommonExport.LongImageExport(cym, f.Name);
        }
    }
}
