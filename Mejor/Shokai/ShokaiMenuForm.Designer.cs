﻿namespace Mejor.Shokai
{
    partial class ShokaiMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonShokaiImage = new System.Windows.Forms.Button();
            this.buttonBarcode = new System.Windows.Forms.Button();
            this.buttonResultInput = new System.Windows.Forms.Button();
            this.buttonMedivery = new System.Windows.Forms.Button();
            this.buttonJizenAhakiCSVExport = new System.Windows.Forms.Button();
            this.buttonReadCode = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonShokaiExcludeList = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonShokaiImage
            // 
            this.buttonShokaiImage.Location = new System.Drawing.Point(26, 119);
            this.buttonShokaiImage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonShokaiImage.Name = "buttonShokaiImage";
            this.buttonShokaiImage.Size = new System.Drawing.Size(206, 66);
            this.buttonShokaiImage.TabIndex = 2;
            this.buttonShokaiImage.Text = "照会文書画像取り込み";
            this.buttonShokaiImage.UseVisualStyleBackColor = true;
            this.buttonShokaiImage.Click += new System.EventHandler(this.buttonShokaiImage_Click);
            // 
            // buttonBarcode
            // 
            this.buttonBarcode.Location = new System.Drawing.Point(240, 119);
            this.buttonBarcode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonBarcode.Name = "buttonBarcode";
            this.buttonBarcode.Size = new System.Drawing.Size(206, 66);
            this.buttonBarcode.TabIndex = 2;
            this.buttonBarcode.Text = "バーコード読み取り確認";
            this.buttonBarcode.UseVisualStyleBackColor = true;
            this.buttonBarcode.Click += new System.EventHandler(this.buttonBarcode_Click);
            // 
            // buttonResultInput
            // 
            this.buttonResultInput.Location = new System.Drawing.Point(26, 194);
            this.buttonResultInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonResultInput.Name = "buttonResultInput";
            this.buttonResultInput.Size = new System.Drawing.Size(206, 66);
            this.buttonResultInput.TabIndex = 2;
            this.buttonResultInput.Text = "照会結果入力";
            this.buttonResultInput.UseVisualStyleBackColor = true;
            this.buttonResultInput.Click += new System.EventHandler(this.buttonResultInput_Click);
            // 
            // buttonMedivery
            // 
            this.buttonMedivery.Enabled = false;
            this.buttonMedivery.Location = new System.Drawing.Point(26, 28);
            this.buttonMedivery.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonMedivery.Name = "buttonMedivery";
            this.buttonMedivery.Size = new System.Drawing.Size(206, 66);
            this.buttonMedivery.TabIndex = 3;
            this.buttonMedivery.Text = "追加項目入力\r\n（Medi-very）";
            this.buttonMedivery.UseVisualStyleBackColor = true;
            this.buttonMedivery.Click += new System.EventHandler(this.buttonMedivery_Click);
            // 
            // buttonJizenAhakiCSVExport
            // 
            this.buttonJizenAhakiCSVExport.Enabled = false;
            this.buttonJizenAhakiCSVExport.Location = new System.Drawing.Point(26, 28);
            this.buttonJizenAhakiCSVExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonJizenAhakiCSVExport.Name = "buttonJizenAhakiCSVExport";
            this.buttonJizenAhakiCSVExport.Size = new System.Drawing.Size(236, 66);
            this.buttonJizenAhakiCSVExport.TabIndex = 4;
            this.buttonJizenAhakiCSVExport.Text = "静岡広域\r\n事前あはき照会CSV出力";
            this.buttonJizenAhakiCSVExport.UseVisualStyleBackColor = true;
            this.buttonJizenAhakiCSVExport.Click += new System.EventHandler(this.buttonJizenAhakiCSVExport_Click);
            // 
            // buttonReadCode
            // 
            this.buttonReadCode.Location = new System.Drawing.Point(26, 43);
            this.buttonReadCode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonReadCode.Name = "buttonReadCode";
            this.buttonReadCode.Size = new System.Drawing.Size(206, 66);
            this.buttonReadCode.TabIndex = 3;
            this.buttonReadCode.Text = "照会コード情報読み込み";
            this.buttonReadCode.UseVisualStyleBackColor = true;
            this.buttonReadCode.Click += new System.EventHandler(this.buttonReadCode_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonMedivery);
            this.groupBox1.Location = new System.Drawing.Point(30, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(465, 108);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Medi-Very";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonReadCode);
            this.groupBox2.Controls.Add(this.buttonShokaiImage);
            this.groupBox2.Controls.Add(this.buttonBarcode);
            this.groupBox2.Controls.Add(this.buttonResultInput);
            this.groupBox2.Location = new System.Drawing.Point(30, 130);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(465, 273);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "照会文書管理";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonJizenAhakiCSVExport);
            this.groupBox3.Location = new System.Drawing.Point(30, 529);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(465, 108);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "その他";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonShokaiExcludeList);
            this.groupBox4.Location = new System.Drawing.Point(30, 413);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(465, 108);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "照会対象者除外設定";
            // 
            // buttonShokaiExcludeList
            // 
            this.buttonShokaiExcludeList.Location = new System.Drawing.Point(26, 28);
            this.buttonShokaiExcludeList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonShokaiExcludeList.Name = "buttonShokaiExcludeList";
            this.buttonShokaiExcludeList.Size = new System.Drawing.Size(206, 66);
            this.buttonShokaiExcludeList.TabIndex = 4;
            this.buttonShokaiExcludeList.Text = "Mejor外履歴/除外者リスト";
            this.buttonShokaiExcludeList.UseVisualStyleBackColor = true;
            this.buttonShokaiExcludeList.Click += new System.EventHandler(this.buttonShokaiExcludeList_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(131, 645);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(274, 35);
            this.button1.TabIndex = 7;
            this.button1.Text = "閉じる";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ShokaiMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 691);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ShokaiMenuForm";
            this.Text = "照会メニュー";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonShokaiImage;
        private System.Windows.Forms.Button buttonBarcode;
        private System.Windows.Forms.Button buttonResultInput;
        private System.Windows.Forms.Button buttonMedivery;
        private System.Windows.Forms.Button buttonJizenAhakiCSVExport;
        private System.Windows.Forms.Button buttonReadCode;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonShokaiExcludeList;
        private System.Windows.Forms.Button button1;
    }
}