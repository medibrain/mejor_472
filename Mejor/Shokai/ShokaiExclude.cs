﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    class ShokaiExclude
    {
        [DB.DbAttribute.PrimaryKey]
        public int ExcludeID { get; private set; }
        public int ImportID { get; private set; }
        public int ChargeYM { get; private set; }
        public int MediYM { get; private set; }
        public string HihoNum { get; private set; } = string.Empty;
        public DateTime Birth { get; private set; } = DateTimeEx.DateTimeNull;

        [DB.DbAttribute.Ignore]
        public string ChargeYmStr => ChargeYM == 999999 ? "除外" : ChargeYM.ToString("0000/00");

        [DB.DbAttribute.Ignore]
        public string MediYmStr => MediYM == 999999 ? "" : MediYM.ToString("0000/00");

        [DB.DbAttribute.Ignore]
        public string BirthStr => Birth.IsNullDate() ? "" : Birth.ToString("yyyy/MM/dd");


        public class ExcludeList
        {
            public string ChargeYmStr => ChargeYM == 999999 ? "除外" : ChargeYM.ToString("0000/00");
            public int ChargeYM { get; private set; }
            public int ExcludeCount { get; set; }

            public static List<ExcludeList> GetList()
            {
                var sql = "SELECT chargeym, COUNT(chargeym) AS excludecount " +
                    "FROM shokaiexclude GROUP BY chargeym;";

                var res = DB.Main.Query<ExcludeList>(sql);
                return res.ToList();
            }
        }

        private static int getNextImportID()
        {
            var sql = "SELECT MAX(importid) FROM ShokaiExclude;";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                var res = cmd.TryExecuteScalar();
                var i = res == DBNull.Value ? 0 : (int)res;
                i++;
                return i;
            }
        }

        public static List<ShokaiExclude> GetList(int cym)
        {
            var l = DB.Main.Select<ShokaiExclude>($"chargeym={cym}");
            return l == null ? new List<ShokaiExclude>() : l.ToList();
        }

        public bool Delete()
        {
            var sql = $"DELETE FROM shokaiexclude WHERE excludeid={ExcludeID}; ";
            return DB.Main.Excute(sql);
        }

        public static bool ImportLog()
        {
            MessageBox.Show("処理年月、診療年月、被保険者番号、生年月日の順でCSVを準備して下さい。\r\n" +
                "また年月は西暦6桁、生年月日は西暦数字8桁の数字のみで指定してください。" +
                "文字コードはShift_JIS、1行目はヘッダーとなります。");

            string fn;
            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return true;
                fn = f.FileName;
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.LogPrint("CSVファイルを読み込んでいます");

            try
            {
                var csv = CommonTool.CsvImportShiftJis(fn);
                var l = new List<ShokaiExclude>();
                int iid = getNextImportID();
                int id = 1;

                for (int i = 1; i < csv.Count; i++)
                {
                    try
                    {
                        var item = csv[i];
                        var se = new ShokaiExclude();
                        int.TryParse(item.Length < 3 ? "0" : item[3], out int birth);
                        se.MediYM = int.Parse(item[0]);
                        se.ChargeYM = int.Parse(item[1]);
                        se.HihoNum = item[2];
                        se.Birth = birth.ToDateTime();
                        se.ImportID = iid;
                        se.ExcludeID = iid * 100000 + id;
                        l.Add(se);
                        id++;
                    }
                    catch (Exception ex)
                    {
                        var res = MessageBox.Show($"{i + 1}行目のデータが読み込めませんでした。" +
                            $"\r\n{ex.Message}\r\n処理を続けますか？", "",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                        if (res == DialogResult.OK) continue;

                        return false;
                    }
                }

                wf.LogPrint("データベースに登録しています");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;


                var g = l.Select((s, i) => new { index = i, shokaiExclude = s })
                    .GroupBy(x => x.index / 10)
                    .Select(gr => gr.Select(x => x.shokaiExclude));

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in g)
                    {
                        wf.InvokeValue += item.Count();
                        if (!DB.Main.Inserts(item, tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }

        public static bool ImportBlackList()
        {
            MessageBox.Show("被保険者番号、生年月日の順でCSVを準備して下さい。\r\n" +
                "生年月日は西暦数字8桁の数字のみで指定してください。" +
                "また生年月日が指定されなかった場合は被保険者番号がそのまま除外リストとなります。" +
                "文字コードはShift_JIS、1行目はヘッダーとなります。");

            string fn;
            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return true;
                fn = f.FileName;
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.LogPrint("CSVファイルを読み込んでいます");

            try
            {
                var csv = CommonTool.CsvImportShiftJis(fn);
                var l = new List<ShokaiExclude>();
                int iid = getNextImportID();
                int id = 1;

                for (int i = 1; i < csv.Count; i++)
                {
                    try
                    {
                        var item = csv[i];
                        var se = new ShokaiExclude();
                        int.TryParse(item.Length > 1 ? item[1] : "0", out int birth);

                        se.MediYM = 999999;
                        se.ChargeYM = 999999;
                        se.HihoNum = item[0];
                        se.Birth = birth.ToDateTime();
                        se.ImportID = iid;
                        se.ExcludeID = iid * 100000 + id;
                        l.Add(se);
                        id++;
                    }
                    catch (Exception ex)
                    {
                        var res = MessageBox.Show($"{i + 1}行目のデータが読み込めませんでした。" +
                            $"\r\n{ex.Message}\r\n処理を続けますか？", "",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                        if (res == DialogResult.OK) continue;

                        return false;
                    }
                }

                wf.LogPrint("データベースに登録しています");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        if (!DB.Main.Insert(item))
                        {
                            MessageBox.Show("データベースの更新に失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
            }
            finally
            {
                wf.Dispose();
            }

            return true;
        }

        public static bool RegisterShokaiExcludeHihoNums(List<string> nums, DB.Transaction tran)
        {
            int iid = getNextImportID();
            int id = 0;

            var sql = "SELECT hihonum FROM shokaiexclude " +
                "WHERE mediym=999999 AND chargeym=999999 AND birth='-infinity';";
            var l = DB.Main.Query<string>(sql);
            var hs = new HashSet<string>();
            foreach (var item in l) hs.Add(item);

            var addNums = new List<string>();
            foreach (var item in nums)
            {
                if (!hs.Add(item))
                    continue;
                addNums.Add(item);
            }

            foreach (var item in addNums)
            {
                id++;
                var se = new ShokaiExclude();

                se.MediYM = 999999;
                se.ChargeYM = 999999;
                se.HihoNum = item;
                se.Birth = DateTimeEx.DateTimeNull;
                se.ImportID = iid;
                se.ExcludeID = iid * 100000 + id;

                if (!DB.Main.Insert(se, tran))
                {
                    MessageBox.Show("データベースの更新に失敗しました");
                    return false;
                }
            }

            return true;
        }
    }
}
