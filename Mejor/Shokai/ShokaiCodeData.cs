﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    [DB.DbAttribute.DifferentTableName("application")]
    class ShokaiCodeData
    {
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; set; }
        public string ShokaiCode { get; set; }

        public static bool Regist(string csvFileName)
        {
            var csv = CommonTool.CsvImportShiftJis(csvFileName);
            if (csv.Count < 2)
            {
                MessageBox.Show("読み込むデータがありません。CSVファイル内容をご確認ください。");
                return false;
            }

            var l = new List<ShokaiCodeData>();
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                wf.LogPrint("CSVを読み込み中です");

                for (int i = 1; i < csv.Count; i++)
                {
                    if (csv[i].Length < 2) continue;
                    if (csv[i][0].Trim().Length == 0) continue;
                    if (!int.TryParse(csv[i][0], out int aid))
                    {
                        MessageBox.Show($"{i + 1}行目のAIDが読み込めませんでした。処理を中止します。");
                        return false;
                    }
                    var sd = new ShokaiCodeData();
                    sd.AID = aid;
                    sd.ShokaiCode = csv[i][1].Trim();
                    l.Add(sd);
                }

                wf.LogPrint($"{l.Count}件のデータを検出しました");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録中です");
                
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        //照会コードはApplication.shokaicodeに書く
                        DB.Main.Update(item, tran);
                    }
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                MessageBox.Show("エラーが発生しました。処理を中止します。\r\n\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            
            MessageBox.Show("照会コードの読み込みが完了しました");
            return true;
        }

        public static bool ExistsShokaiCode(string code)
        {
            var l = DB.Main.Select<ShokaiCodeData>($"shokaicode='{code}'");
            return l.Count() > 0;
        }

        /// <summary>
        /// 照会コードに対応する受療者名を取得します
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetNameFromShokaiCode(string code)
        {
            var sql = $"SELECT pname FROM application WHERE shokaicode='{code}'";
            var res = DB.Main.Query<string>(sql);
            if (res == null || res.Count() != 1) return string.Empty;
            return res.First();
        }

        //20191009085852 furukawa ////////////////////////
        //照会コードから被保番と患者名を取得
        /// <summary>
        /// 照会コードに対応する受療者名、ヒホバンを取得します
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static void GetNameHNumFromShokaiCode(string code,out string strPname,out string strhnum)
        {
            strPname = string.Empty;
            strhnum = string.Empty;
            
            var a = App.GetAppsWithWhere($"where a.shokaicode='{code}'");
            if (a.Count==0) return;

            strPname = a[0].PersonName;
            strhnum = a[0].HihoNum;

        }
    }
}
