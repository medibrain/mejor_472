﻿using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiMenuForm : Form
    {
        Insurer ins;
        int cym;

        public ShokaiMenuForm(Insurer ins, int cym)
        {
            InitializeComponent();
            CommonTool.setFormColor(this);

            this.ins = ins;
            this.cym = cym;
            this.Text = "照会メニュー " + $"{ DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年{ cym % 100}月分";
            //this.Text = "照会メニュー " + $"{ DateTimeEx.GetHsYearFromAd(cym / 100)}年{ cym % 100}月分";

            //20210330131902 furukawa st ////////////////////////
            //medivery使用保険者を取得しボタン使用可否に使う
            
            buttonMedivery.Enabled = Medivery.MediveryXML.dicUseMvXML[ins.InsurerID.ToString()].usemedivery;
            //buttonMedivery.Enabled = Medivery.Medivery.GetMediVery(ins.InsurerID) != null;//未登録保険者はDisable
            //20210330131902 furukawa ed ////////////////////////


            switch (ins.InsurerID)
            {
                case (int)InsurerID.SHIZUOKA_KOIKI:
                    buttonJizenAhakiCSVExport.Enabled = true;
                    break;
            }

            //20201110182222 furukawa st ////////////////////////
            //PastDataのロード。個人情報と施術所のみ

            PastData.PersonalData.LoadPersonalData(cym);
            PastData.ClinicData.LoadClinicData(cym);
            //20201110182222 furukawa ed ////////////////////////
        }

        private void buttonMedivery_Click(object sender, EventArgs e)
        {
            using (var f = new Medivery.MediveryFlagSelectForm(ins, cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonBarcode_Click(object sender, EventArgs e)
        {
            var l = Shokai.ShokaiImage.GetNotReadBarcodeImages();
            if (l.Count == 0)
            {
                MessageBox.Show("バーコードが確定していない画像はありません。");
                return;
            }

            var res = MessageBox.Show("バーコードが確定していない画像が" + l.Count.ToString() +
                "枚見つかりました。確定処理に移りますか？", "バーコード確定処理",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (res != DialogResult.OK) return;
            using (var f = new Shokai.ShokaiBarcodeInputForm())
            {
                f.ShowDialog();
            }
        }

        private void buttonResultInput_Click(object sender, EventArgs e)
        {
            using (var f = new ShokaiInputForm(cym))
            {
                f.ShowDialog();
            }

            //var iid = Insurer.CurrrentInsurer.InsurerID;
            //if (iid == (int)InsurerID.MIYAZAKI_KOIKI)
            //{
            //    using (var f = new Shokai.ShokaiResultForm(cym))
            //    {
            //        f.ShowDialog();
            //    }
            //}
            //else
            //{
            //    using (var f = new ShokaiInputForm(cym))
            //    {
            //        f.ShowDialog();
            //    }
            //}
        }

        private void buttonShokaiImage_Click(object sender, EventArgs e)
        {
            using (var f = new ShokaiImageImportForm(cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonJizenAhakiCSVExport_Click(object sender, EventArgs e)
        {
            var cymStr = Interaction.InputBox("審査年月（yyyyMM）を入力してください", "事前あはき照会CSV出力");
            int cym;
            if (cymStr.Length == 6 && int.TryParse(cymStr, out cym))
            {
                var sfd = new SaveFileDialog();
                sfd.Title = $"事前あはき照会リスト（{cym}） - マッチング用CSVの出力";
                sfd.FileName = $"静岡広域{cym}_refrece.csv";
                sfd.Filter = "CSVファイル(*.csv)|*.csv;";
                var r = sfd.ShowDialog();
                if (r != DialogResult.OK) return;
                var exResult = ShizuokaKoiki.Export.RefReceExportByCYM(sfd.FileName, cym);
                if (exResult)
                {
                    MessageBox.Show("出力が完了しました", "成功");
                }
                else
                {
                    MessageBox.Show("出力が失敗しました", "失敗");
                }
            }
            else
            {
                MessageBox.Show("審査年月の値が不正です", "エラー");
            }
        }

        private void buttonReadCode_Click(object sender, EventArgs e)
        {
            MessageBox.Show("照会コードをシステムに読み込みます。" +
                    "1列目をAID、2列目を照会コードとしたCSVを準備して下さい。" +
                    "3行目以降は自由です。" +
                    "また、1行目はヘッダーとして処理され、コードは読み込まれません。",
                    "照会コード読み込み", MessageBoxButtons.OK, MessageBoxIcon.Information);

            string fileName;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "CSVファイル|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                fileName = f.FileName;
            }

            ShokaiCodeData.Regist(fileName);
        }

        private void buttonShokaiExcludeList_Click(object sender, EventArgs e)
        {
            using (var f = new ShokaiExcludeListForm())
            {
                f.ShowDialog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
