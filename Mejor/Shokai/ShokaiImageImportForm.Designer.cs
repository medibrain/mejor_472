﻿namespace Mejor.Shokai
{
    partial class ShokaiImageImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonDiriSelect = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.radioButtonAri = new System.Windows.Forms.RadioButton();
            this.radioButtonNashi = new System.Windows.Forms.RadioButton();
            this.radioButtonMi = new System.Windows.Forms.RadioButton();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.選択中のインポート画像を削除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxQRx = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxQRy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonDiriSelect
            // 
            this.buttonDiriSelect.Location = new System.Drawing.Point(978, 54);
            this.buttonDiriSelect.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDiriSelect.Name = "buttonDiriSelect";
            this.buttonDiriSelect.Size = new System.Drawing.Size(39, 32);
            this.buttonDiriSelect.TabIndex = 0;
            this.buttonDiriSelect.Text = "…";
            this.buttonDiriSelect.UseVisualStyleBackColor = true;
            this.buttonDiriSelect.Click += new System.EventHandler(this.buttonDiriSelect_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(784, 136);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(112, 35);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // radioButtonAri
            // 
            this.radioButtonAri.AutoSize = true;
            this.radioButtonAri.Location = new System.Drawing.Point(380, 136);
            this.radioButtonAri.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonAri.Name = "radioButtonAri";
            this.radioButtonAri.Size = new System.Drawing.Size(77, 22);
            this.radioButtonAri.TabIndex = 2;
            this.radioButtonAri.TabStop = true;
            this.radioButtonAri.Text = "相違あり";
            this.radioButtonAri.UseVisualStyleBackColor = true;
            // 
            // radioButtonNashi
            // 
            this.radioButtonNashi.AutoSize = true;
            this.radioButtonNashi.Location = new System.Drawing.Point(486, 136);
            this.radioButtonNashi.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonNashi.Name = "radioButtonNashi";
            this.radioButtonNashi.Size = new System.Drawing.Size(80, 22);
            this.radioButtonNashi.TabIndex = 3;
            this.radioButtonNashi.TabStop = true;
            this.radioButtonNashi.Text = "相違なし";
            this.radioButtonNashi.UseVisualStyleBackColor = true;
            // 
            // radioButtonMi
            // 
            this.radioButtonMi.AutoSize = true;
            this.radioButtonMi.Location = new System.Drawing.Point(594, 136);
            this.radioButtonMi.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonMi.Name = "radioButtonMi";
            this.radioButtonMi.Size = new System.Drawing.Size(71, 22);
            this.radioButtonMi.TabIndex = 4;
            this.radioButtonMi.TabStop = true;
            this.radioButtonMi.Text = "未指定";
            this.radioButtonMi.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(905, 136);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 35);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(380, 55);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(598, 24);
            this.textBox1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(376, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "対象フォルダ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(376, 109);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "相違有無グループ指定";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(14, 11);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(318, 472);
            this.dataGridView1.TabIndex = 8;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.選択中のインポート画像を削除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(222, 26);
            // 
            // 選択中のインポート画像を削除ToolStripMenuItem
            // 
            this.選択中のインポート画像を削除ToolStripMenuItem.Name = "選択中のインポート画像を削除ToolStripMenuItem";
            this.選択中のインポート画像を削除ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.選択中のインポート画像を削除ToolStripMenuItem.Text = "選択中のインポート画像を削除";
            this.選択中のインポート画像を削除ToolStripMenuItem.Click += new System.EventHandler(this.選択中のインポート画像を削除ToolStripMenuItem_Click);
            // 
            // textBoxQRx
            // 
            this.textBoxQRx.Location = new System.Drawing.Point(267, 38);
            this.textBoxQRx.Name = "textBoxQRx";
            this.textBoxQRx.Size = new System.Drawing.Size(50, 24);
            this.textBoxQRx.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxQRy);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxQRx);
            this.groupBox1.Location = new System.Drawing.Point(380, 216);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(383, 145);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "実測値でQRコードを読み取る場合：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(255, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "用紙の【右端】からQRコードの【左端】まで";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(255, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "用紙の【下端】からQRコードの【上端】まで";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(703, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "mm";
            // 
            // textBoxQRy
            // 
            this.textBoxQRy.Location = new System.Drawing.Point(267, 76);
            this.textBoxQRy.Name = "textBoxQRy";
            this.textBoxQRy.Size = new System.Drawing.Size(50, 24);
            this.textBoxQRy.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(323, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 18);
            this.label6.TabIndex = 13;
            this.label6.Text = "mm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(374, 18);
            this.label7.TabIndex = 14;
            this.label7.Text = "※スキャン解像度は300dpi、QRコードサイズは12×12mm固定";
            // 
            // ShokaiImageImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 493);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.radioButtonMi);
            this.Controls.Add(this.radioButtonNashi);
            this.Controls.Add(this.radioButtonAri);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonDiriSelect);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ShokaiImageImportForm";
            this.Text = "照会画像取り込み";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDiriSelect;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.RadioButton radioButtonAri;
        private System.Windows.Forms.RadioButton radioButtonNashi;
        private System.Windows.Forms.RadioButton radioButtonMi;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 選択中のインポート画像を削除ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxQRx;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxQRy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
    }
}