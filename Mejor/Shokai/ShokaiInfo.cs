﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.Shokai
{
    class ShokaiInfo
    {
        public App app { get; private set; }
        public ShokaiImage ShokaiImage { get; private set; }
        public int Aid => app.Aid;
        public int MediYear => app.MediYear;
        public int MediMonth => app.MediMonth;
        public APP_TYPE AppType => app.AppType;
        public string ShokaiCode => app.ShokaiCode;
        public SHOKAI_IMAGE_STATUS ShokaiImageStatus => ShokaiImage?.ShokaiImageStatus ?? SHOKAI_IMAGE_STATUS.未返信;
        public int ShokaiImageID => ShokaiImage?.ImageID ?? 0;
        public string ShokaiResultStatus => app.ShokaiResultStr;
        public string Overlapped { get; private set; } = string.Empty;
        
     

        ShokaiInfo(App app, ShokaiImage shokaiImage)
        {
            this.app = app;
            this.ShokaiImage = shokaiImage;
        }

        public static List<ShokaiInfo> SelectByCym(int cym)
        {
            var apps = App.GetAppsWithWhere($"WHERE a.cym={cym} AND shokaicode<>''");
            var images = ShokaiImage.SelectByCym(cym);

            //var dic = new Dictionary<string, ShokaiImage>();
            var dic = new Dictionary<string, List<ShokaiImage>>();

            foreach (var item in images)
            {
                if (dic.ContainsKey(item.Code))
                {
                    dic[item.Code].Add(item);
                    //if (item.ImageID > dic[item.Code].ImageID) dic[item.Code] = item;
                }
                else
                {
                    dic[item.Code] = new List<ShokaiImage>();
                    dic[item.Code].Add(item);
                    //dic.Add(item.Code, item);
                }
            }

            var l = new List<ShokaiInfo>();

            foreach (var item in apps)
            {
                //var image = dic.ContainsKey(item.ShokaiCode) ?
                //    dic[item.ShokaiCode] : null;
                //var info = new ShokaiInfo(item, image);
                //l.Add(info);

                if (!dic.ContainsKey(item.ShokaiCode)) continue;
                foreach (var image in dic[item.ShokaiCode])
                {
                    var info = new ShokaiInfo(item, image);
                    if (dic[item.ShokaiCode].Count > 1) info.Overlapped = "○";
                    l.Add(info);
                }
            }

            return l;
        }

        public static List<ShokaiInfo> SelectByApps(IEnumerable<App> apps)
        {
            var l = new List<ShokaiInfo>();

            foreach (var item in apps)
            {
                var i = ShokaiImage.SelectByCode(item.ShokaiCode);
                if (i == null) continue;
                var info = new ShokaiInfo(item, i);
                l.Add(info);
            }

            return l;
        }

    }
}
