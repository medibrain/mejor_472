﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiImageImportForm : Form
    {
        int cym;
        BindingSource bs = new BindingSource();
        class ShokaiImageImportInfo
        {
            public int ImportID { get; set; }
            public SHOKAI_IMAGE_STATUS ShokaiImageStatus { get; set; }
            public int Count { get; set; }

        }

        public ShokaiImageImportForm(int cym)
        {
            InitializeComponent();
            CommonTool.setFormColor(this);

            this.cym = cym;
            showList();
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(ShokaiImageImportInfo.ImportID)].HeaderText = "ID";
            dataGridView1.Columns[nameof(ShokaiImageImportInfo.ImportID)].Width = 60;
            dataGridView1.Columns[nameof(ShokaiImageImportInfo.ShokaiImageStatus)].HeaderText = "取込時";
            dataGridView1.Columns[nameof(ShokaiImageImportInfo.ShokaiImageStatus)].Width = 70;
            dataGridView1.Columns[nameof(ShokaiImageImportInfo.Count)].HeaderText = "件数";
            dataGridView1.Columns[nameof(ShokaiImageImportInfo.Count)].Width = 60;
        }

        private void buttonDiriSelect_Click(object sender, EventArgs e)
        {
            SelectDir();
        }

        private void SelectDir()
        {
            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != DialogResult.OK) return;

            textBox1.Text = d.Name;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(textBox1.Text))
            {
                MessageBox.Show("指定されている対象フォルダが見つかりません。正しく指定してください");
                return;
            }

            SHOKAI_IMAGE_STATUS s;
            if (radioButtonMi.Checked) s = SHOKAI_IMAGE_STATUS.NULL;
            else if (radioButtonAri.Checked) s = SHOKAI_IMAGE_STATUS.相違あり;
            else if (radioButtonNashi.Checked) s = SHOKAI_IMAGE_STATUS.相違なし;
            else
            {
                MessageBox.Show("取り込む画像の相違有無を指定してください");
                return;
            }
            
            var fs = System.IO.Directory.GetFiles(textBox1.Text.Trim(), "*.tif");
            if (fs.Count() == 0)
            {
                MessageBox.Show("tif画像が見つかりません");
                return;
            }

            if (MessageBox.Show($"{s}として照会画像を取り込みます。よろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Information) != DialogResult.OK) return;



            #region バーコードが記載されているであろう範囲を、保険者ごとに設定する(QRコード)
            var id = Insurer.CurrrentInsurer.EnumInsID;
            var rect =
                id == InsurerID.HIROSHIMA_KOIKI ? new Rectangle(2000, 3100, 400, 400) :
                id == InsurerID.OSAKA_KOIKI ? new Rectangle(2000, 3080, 400, 400) :
                id == InsurerID.MIYAZAKI_KOIKI ? new Rectangle(2000, 3080, 400, 400) :
                id == InsurerID.HYOGO_KOIKI ? new Rectangle(1950, 3100, 400, 400) :             //20190729094943 furukawa 兵庫広域のバーコード範囲を追加                                                                                   
                id == InsurerID.SHIMANE_KOIKI ? new Rectangle(1950, 3100, 400, 400) :           //20190730102125 furukawa 仮で島根広域のバーコード範囲を追加
                
                //20220415084649 furukawa st ////////////////////////
                //広島広域2022、兵庫広域2022追加                                                                                                
                id == InsurerID.HIROSHIMA_KOIKI2022 ? new Rectangle(2000, 2900, 400, 400) :
                id == InsurerID.HYOGO_KOIKI2022 ? new Rectangle(2000, 3100, 380, 380) :
                //20220415084649 furukawa ed ////////////////////////

                //20220630145725 furukawa st ////////////////////////
                //鹿児島広域追加                
                id == InsurerID.KAGOSHIMA_KOIKI ? new Rectangle(2000, 3080, 400, 400) :
                //20220630145725 furukawa ed ////////////////////////

                //20220706232204 furukawa st ////////////////////////
                //愛媛広域追加                
                id == InsurerID.EHIME_KOIKI ?new Rectangle(1950, 2500, 400, 400) :
                //20220706232204 furukawa ed ////////////////////////


                new Rectangle(1900, 3000, 400, 400);

            #endregion


            // 20220930_1 ito st /////
            if ((textBoxQRx.Text != "") && (textBoxQRx.Text != "0") && (textBoxQRy.Text != "") && (textBoxQRy.Text != "0"))
            {
                try
                {
                    double QRx = Convert.ToDouble(textBoxQRx.Text);
                    double QRy = Convert.ToDouble(textBoxQRy.Text);
                    double dpi = 300;

                    //Rectangleの大きさが400pixel=33mm、QRコードの大きさ12mmを引いて2で割った10mmが左右のマージンとなる
                    QRx = 210 - QRx - 10;
                    QRy = 296 - QRy - 10;

                    //mmをpixelに変換
                    QRx = (QRx * dpi) / 25.4;
                    QRy = (QRy * dpi) / 25.4;

                    int iQRx = Convert.ToInt32(QRx);
                    int iQRy = Convert.ToInt32(QRy);

                    //ゼロ以下なら例外をスロー
                    if (iQRx < 0 || iQRy < 0) throw new ArgumentOutOfRangeException();

                    rect = new Rectangle(iQRx, iQRy, 400, 400);
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return;
                }
            }
            // 20220930_1 ito end /////


            var res = ShokaiImage.Import(textBox1.Text, false, s, rect, ZXing.BarcodeFormat.QR_CODE);

            if (res)
            {
                MessageBox.Show("取り込みが完了しました");
                textBox1.Clear();
                showList();
            }
            else
            {
                MessageBox.Show("取り込みに失敗しました");
            }
        }

        private void showList()
        {
            var list = new List<ShokaiImageImportInfo>();
            var l = ShokaiImage.SelectByCym(cym);
            var g = l.GroupBy(x => x.ImportID);

            foreach (var item in g)
            {
                var info = new ShokaiImageImportInfo();
                info.ImportID = item.First().ImportID;
                info.ShokaiImageStatus = item.First().ShokaiImageStatus;
                info.Count = item.Count();
                list.Add(info);
            }

            list.Sort((x, y) => x.ImportID.CompareTo(y.ImportID));
            bs.DataSource = list;
            bs.ResetBindings(false);

        }

        private void 選択中のインポート画像を削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bs.Current == null) return;
            var info = (ShokaiImageImportInfo)bs.Current;


            using (var f = new WaitForm())
            {
                f.ShowDialogOtherTask();
                f.LogPrint("照会画像データの確認中です");

                var images = ShokaiImage.SelectByImport(info.ImportID);
                var apps = new List<App>();

                foreach (var item in images)
                {
                    var app = App.GetAppsWithWhere($"WHERE a.shokaicode='{item.Code}'");
                    if (app.Count != 1) continue;
                    apps.Add(app.First());
                }

                var dic = new Dictionary<int, int>();
                foreach (var item in apps)
                {
                    if (!dic.ContainsKey(item.CYM)) dic.Add(item.CYM, 0);
                    dic[item.CYM]++;
                }

                var l = dic.ToList();
                l.Sort((x, y) => x.Key.CompareTo(y.Key));

                string msgList = string.Empty;
                l.ForEach(c => msgList += $"{c.Key.ToString("0000/00")}  {c.Value}件\r\n");
                msgList += $"不明  {images.Count - apps.Count}件\r\n";

                var res = MessageBox.Show($"照会画像インポートID:{info.ImportID}\r\n\r\n" +
                    msgList + "\r\n" +
                    $"合計 {images.Count}件の画像データを削除します。\r\n" +
                    $"元には戻せませんが、本当によろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                if (res != DialogResult.OK) return;

                f.BarStyle = ProgressBarStyle.Continuous;
                f.SetMax(apps.Count);

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in images)
                    {
                        f.InvokeValue++;
                        if(!item.Delete(tran))
                        {
                            MessageBox.Show("削除に失敗しました", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    tran.Commit();
                }
                MessageBox.Show("削除しました", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                showList();
            }
            
        }
    }
}
