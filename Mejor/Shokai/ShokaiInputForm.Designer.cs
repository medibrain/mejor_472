﻿namespace Mejor.Shokai
{
    partial class ShokaiInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.選択中の照会画像を削除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.inspectControl1 = new Mejor.Inspect.InspectControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonOverwrap = new System.Windows.Forms.RadioButton();
            this.radioButtonInputed = new System.Windows.Forms.RadioButton();
            this.radioButtonNotInput = new System.Windows.Forms.RadioButton();
            this.radioButtonInputAll = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonSouiAri = new System.Windows.Forms.RadioButton();
            this.radioButtonMihenshin = new System.Windows.Forms.RadioButton();
            this.radioButtonSouiNashi = new System.Windows.Forms.RadioButton();
            this.radioButtonSouiAll = new System.Windows.Forms.RadioButton();
            this.scrollPictureControlBack = new Mejor.ScrollPictureControl();
            this.scrollPictureControlFront = new Mejor.ScrollPictureControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(333, 177);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.選択中の照会画像を削除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(209, 26);
            // 
            // 選択中の照会画像を削除ToolStripMenuItem
            // 
            this.選択中の照会画像を削除ToolStripMenuItem.Name = "選択中の照会画像を削除ToolStripMenuItem";
            this.選択中の照会画像を削除ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.選択中の照会画像を削除ToolStripMenuItem.Text = "選択中の照会画像を削除";
            this.選択中の照会画像を削除ToolStripMenuItem.Click += new System.EventHandler(this.選択中の照会画像を削除ToolStripMenuItem_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.DimGray;
            this.splitter1.Location = new System.Drawing.Point(711, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 933);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.DimGray;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(822, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 933);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(825, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(300, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(340, 933);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpdate.Location = new System.Drawing.Point(237, 895);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(91, 25);
            this.buttonUpdate.TabIndex = 4;
            this.buttonUpdate.Text = "更新(PgUP)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 56);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.inspectControl1);
            this.splitContainer1.Size = new System.Drawing.Size(333, 832);
            this.splitContainer1.SplitterDistance = 176;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 5;
            // 
            // inspectControl1
            // 
            this.inspectControl1.Enabled = false;
            this.inspectControl1.Location = new System.Drawing.Point(3, 3);
            this.inspectControl1.MinimumSize = new System.Drawing.Size(300, 433);
            this.inspectControl1.Name = "inspectControl1";
            this.inspectControl1.RelationAid = 0;
            this.inspectControl1.RelationButtonVisible = true;
            this.inspectControl1.Size = new System.Drawing.Size(312, 927);
            this.inspectControl1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.radioButtonOverwrap);
            this.panel3.Controls.Add(this.radioButtonInputed);
            this.panel3.Controls.Add(this.radioButtonNotInput);
            this.panel3.Controls.Add(this.radioButtonInputAll);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(340, 25);
            this.panel3.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "結果";
            // 
            // radioButtonOverwrap
            // 
            this.radioButtonOverwrap.AutoSize = true;
            this.radioButtonOverwrap.Location = new System.Drawing.Point(235, 4);
            this.radioButtonOverwrap.Name = "radioButtonOverwrap";
            this.radioButtonOverwrap.Size = new System.Drawing.Size(49, 17);
            this.radioButtonOverwrap.TabIndex = 3;
            this.radioButtonOverwrap.Text = "重複";
            this.radioButtonOverwrap.UseVisualStyleBackColor = true;
            this.radioButtonOverwrap.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonInputed
            // 
            this.radioButtonInputed.AutoSize = true;
            this.radioButtonInputed.Location = new System.Drawing.Point(161, 4);
            this.radioButtonInputed.Name = "radioButtonInputed";
            this.radioButtonInputed.Size = new System.Drawing.Size(72, 17);
            this.radioButtonInputed.TabIndex = 3;
            this.radioButtonInputed.Text = "入力済み";
            this.radioButtonInputed.UseVisualStyleBackColor = true;
            this.radioButtonInputed.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonNotInput
            // 
            this.radioButtonNotInput.AutoSize = true;
            this.radioButtonNotInput.Location = new System.Drawing.Point(96, 4);
            this.radioButtonNotInput.Name = "radioButtonNotInput";
            this.radioButtonNotInput.Size = new System.Drawing.Size(61, 17);
            this.radioButtonNotInput.TabIndex = 2;
            this.radioButtonNotInput.Text = "未入力";
            this.radioButtonNotInput.UseVisualStyleBackColor = true;
            this.radioButtonNotInput.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonInputAll
            // 
            this.radioButtonInputAll.AutoSize = true;
            this.radioButtonInputAll.Checked = true;
            this.radioButtonInputAll.Location = new System.Drawing.Point(38, 4);
            this.radioButtonInputAll.Name = "radioButtonInputAll";
            this.radioButtonInputAll.Size = new System.Drawing.Size(54, 17);
            this.radioButtonInputAll.TabIndex = 1;
            this.radioButtonInputAll.TabStop = true;
            this.radioButtonInputAll.Text = "すべて";
            this.radioButtonInputAll.UseVisualStyleBackColor = true;
            this.radioButtonInputAll.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.radioButtonSouiAri);
            this.panel2.Controls.Add(this.radioButtonMihenshin);
            this.panel2.Controls.Add(this.radioButtonSouiNashi);
            this.panel2.Controls.Add(this.radioButtonSouiAll);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(340, 25);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "取込";
            // 
            // radioButtonSouiAri
            // 
            this.radioButtonSouiAri.AutoSize = true;
            this.radioButtonSouiAri.Location = new System.Drawing.Point(233, 5);
            this.radioButtonSouiAri.Name = "radioButtonSouiAri";
            this.radioButtonSouiAri.Size = new System.Drawing.Size(67, 17);
            this.radioButtonSouiAri.TabIndex = 4;
            this.radioButtonSouiAri.Text = "相違あり";
            this.radioButtonSouiAri.UseVisualStyleBackColor = true;
            this.radioButtonSouiAri.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonMihenshin
            // 
            this.radioButtonMihenshin.AutoSize = true;
            this.radioButtonMihenshin.Location = new System.Drawing.Point(96, 5);
            this.radioButtonMihenshin.Name = "radioButtonMihenshin";
            this.radioButtonMihenshin.Size = new System.Drawing.Size(61, 17);
            this.radioButtonMihenshin.TabIndex = 2;
            this.radioButtonMihenshin.Text = "未返信";
            this.radioButtonMihenshin.UseVisualStyleBackColor = true;
            this.radioButtonMihenshin.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonSouiNashi
            // 
            this.radioButtonSouiNashi.AutoSize = true;
            this.radioButtonSouiNashi.Location = new System.Drawing.Point(161, 5);
            this.radioButtonSouiNashi.Name = "radioButtonSouiNashi";
            this.radioButtonSouiNashi.Size = new System.Drawing.Size(68, 17);
            this.radioButtonSouiNashi.TabIndex = 3;
            this.radioButtonSouiNashi.Text = "相違なし";
            this.radioButtonSouiNashi.UseVisualStyleBackColor = true;
            this.radioButtonSouiNashi.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonSouiAll
            // 
            this.radioButtonSouiAll.AutoSize = true;
            this.radioButtonSouiAll.Checked = true;
            this.radioButtonSouiAll.Location = new System.Drawing.Point(38, 5);
            this.radioButtonSouiAll.Name = "radioButtonSouiAll";
            this.radioButtonSouiAll.Size = new System.Drawing.Size(54, 17);
            this.radioButtonSouiAll.TabIndex = 1;
            this.radioButtonSouiAll.TabStop = true;
            this.radioButtonSouiAll.Text = "すべて";
            this.radioButtonSouiAll.UseVisualStyleBackColor = true;
            this.radioButtonSouiAll.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // scrollPictureControlBack
            // 
            this.scrollPictureControlBack.ButtonsVisible = false;
            this.scrollPictureControlBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControlBack.Location = new System.Drawing.Point(714, 0);
            this.scrollPictureControlBack.MinimumSize = new System.Drawing.Size(100, 108);
            this.scrollPictureControlBack.Name = "scrollPictureControlBack";
            this.scrollPictureControlBack.Ratio = 1F;
            this.scrollPictureControlBack.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControlBack.Size = new System.Drawing.Size(111, 933);
            this.scrollPictureControlBack.TabIndex = 1;
            // 
            // scrollPictureControlFront
            // 
            this.scrollPictureControlFront.ButtonsVisible = false;
            this.scrollPictureControlFront.Dock = System.Windows.Forms.DockStyle.Left;
            this.scrollPictureControlFront.Location = new System.Drawing.Point(0, 0);
            this.scrollPictureControlFront.MinimumSize = new System.Drawing.Size(200, 137);
            this.scrollPictureControlFront.Name = "scrollPictureControlFront";
            this.scrollPictureControlFront.Ratio = 1F;
            this.scrollPictureControlFront.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControlFront.Size = new System.Drawing.Size(711, 933);
            this.scrollPictureControlFront.TabIndex = 0;
            // 
            // ShokaiInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 933);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.scrollPictureControlBack);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.scrollPictureControlFront);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "ShokaiInputForm";
            this.Text = "照会結果入力";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.ShokaiInputForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShokaiInputForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ScrollPictureControl scrollPictureControlFront;
        private ScrollPictureControl scrollPictureControlBack;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButtonInputed;
        private System.Windows.Forms.RadioButton radioButtonNotInput;
        private System.Windows.Forms.RadioButton radioButtonInputAll;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonSouiAri;
        private System.Windows.Forms.RadioButton radioButtonSouiNashi;
        private System.Windows.Forms.RadioButton radioButtonSouiAll;
        private System.Windows.Forms.RadioButton radioButtonMihenshin;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.RadioButton radioButtonOverwrap;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 選択中の照会画像を削除ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Inspect.InspectControl inspectControl1;
    }
}