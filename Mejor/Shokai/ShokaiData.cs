﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;
using System.Drawing;

namespace Mejor.Shokai
{
    class ShokaiData
    {
        public int AID { get; private set; }
        public int ShokaiID { get; private set; }
        public int YM { get; private set; }
        public string Barcode { get; private set; }
        public ShokaiReason Reason { get; set; }
        public SHOKAI_STATUS Status { get; set; }
        public SHOKAI_RESULT Result { get; set; }
        public bool Henrei { get; set; }
        public string Note { get; set; }
        public DateTime UpdateDate { get;private set; }
        public int UpdateUID { get; private set; }

        public static bool Import(string fileName)
        {
            var sql = "INSERT INTO shokai (aid, shokai_id, ym, barcode, " +
                "shokai_reason, shokai_status, shokai_result, henrei, " +
                "note, update_date, update_uid) " +
                "VALUES (:aid, :shokai_id, :ym, :barcode, " +
                ":shokai_reason, :shokai_status, :shokai_result, :henrei, " +
                ":note, :update_date, :update_uid);";

            using (var tran = DB.Main.CreateTransaction())
            using (var cmd = DB.Main.CreateCmd(sql, tran))
            using (var sr = new System.IO.StreamReader(fileName, Encoding.GetEncoding("Shift_JIS")))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer);
                cmd.Parameters.Add("shokai_id", NpgsqlDbType.Integer);
                cmd.Parameters.Add("ym", NpgsqlDbType.Integer);
                cmd.Parameters.Add("barcode", NpgsqlDbType.Text);
                cmd.Parameters.Add("shokai_reason", NpgsqlDbType.Integer);
                cmd.Parameters.Add("shokai_status", NpgsqlDbType.Integer).Value = (int)SHOKAI_STATUS.照会中;
                cmd.Parameters.Add("shokai_result", NpgsqlDbType.Integer).Value = (int)SHOKAI_RESULT.なし;
                cmd.Parameters.Add("henrei", NpgsqlDbType.Boolean).Value = false;
                cmd.Parameters.Add("note", NpgsqlDbType.Text).Value = string.Empty;
                cmd.Parameters.Add("update_date", NpgsqlDbType.Date).Value = DateTime.Today;
                cmd.Parameters.Add("update_uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;

                //ヘッダー読み込み
                var h =sr.ReadLine().Split(',');
                int aidIdx = Array.FindIndex(h, item => item == "AID");
                int sidIdx = Array.FindIndex(h, item => item == "照会ID");
                int yearIdx = Array.FindIndex(h, item => item == "処理年");
                int monthIdx = Array.FindIndex(h, item => item == "処理月");
                int reasonIdx = Array.FindIndex(h, item => item == "照会理由");
                
                while (sr.Peek() > 0)
                {
                    var ss = sr.ReadLine().Split(',');
                    if (ss.Length < 7) continue;

                    int aid, sid, ym, y, m;
                    int.TryParse(ss[aidIdx], out aid);
                    int.TryParse(ss[sidIdx], out sid);
                    if (yearIdx == -1)
                    {
                        ym = sid / 10000 % 10000;
                    }
                    else
                    {
                        int.TryParse(ss[yearIdx], out y);
                        int.TryParse(ss[monthIdx], out m);
                        ym = y * 100 + m;
                    }

                    if (aid == 0 || ym == 0) continue;

                    var reason = ShokaiReason.なし;
                    if (ss[reasonIdx].Contains("日以上")) reason |= ShokaiReason.施術日数;
                    if (ss[reasonIdx].Contains("部位")) reason |= ShokaiReason.部位数;
                    if (ss[reasonIdx].Contains("疑義施術所")) reason |= ShokaiReason.疑義施術所;

                    cmd.Parameters["aid"].Value = aid;
                    cmd.Parameters["shokai_id"].Value = sid;
                    cmd.Parameters["ym"].Value = ym;
                    cmd.Parameters["barcode"].Value = ss[sidIdx];
                    cmd.Parameters["shokai_reason"].Value = (int)reason;

                    if (!cmd.TryExecuteNonQuery())
                    {
                        tran.Rollback();
                        return false;
                    }
                }

                tran.Commit();
                return true;
            }
        }

        public static Dictionary<int, ShokaiData> Select(int cym)
        {
            var l = new Dictionary<int, ShokaiData>();
            var sql = "SELECT s.aid, s.shokai_id, s.ym, s.barcode, " +
                "s.shokai_reason, s.shokai_status, s.shokai_result, " +
                "s.henrei, s.note, s.update_date, s.update_uid " +
                "FROM shokai AS s, application AS a " +
                "WHERE a.cym=:cym " +
                "AND s.aid=a.aid;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                foreach (var item in res)
                {
                    var s = new ShokaiData();
                    s.AID = (int)item[0];
                    s.ShokaiID = (int)item[1];
                    s.YM = (int)item[2];
                    s.Barcode = (string)item[3];
                    s.Reason = (ShokaiReason)(int)item[4];
                    s.Status = (SHOKAI_STATUS)(int)item[5];
                    s.Result = (SHOKAI_RESULT)(int)item[6];
                    s.Henrei = (bool)item[7];
                    s.Note = (string)item[8];
                    s.UpdateDate = (DateTime)item[9];
                    s.UpdateUID = (int)item[10];
                    l.Add(s.AID, s);
                }
                return l;
            }
        }

        /// <summary>
        /// 指定した月の返信された照会文書が登録されていないものを、未返信として扱います。
        /// </summary>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static bool SetNothingImage(int cym)
        {
            var l = new Dictionary<int, ShokaiData>();
            var sql = "UPDATE shokai SET shokai_status=:st, update_date=:dt, update_uid=:uid " +
                "FROM application AS a " +
                "WHERE a.cym=:cym " +
                "AND shokai_status=:stold " +
                "AND shokai.aid=a.aid;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)SHOKAI_STATUS.未返信;
                cmd.Parameters.Add("dt", NpgsqlDbType.Date).Value = DateTime.Today;
                cmd.Parameters.Add("uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                cmd.Parameters.Add("stold", NpgsqlDbType.Integer).Value = (int)SHOKAI_STATUS.照会中;

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 指定した期間内にアップデートした情報を取得します
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static List<ShokaiData> SelectUpdated(DateTime from, DateTime to)
        {
            var l = new List<ShokaiData>();
            var sql = "SELECT s.aid, s.shokai_id, s.ym, s.barcode, " +
                "s.shokai_reason, s.shokai_status, s.shokai_result, " +
                "s.henrei, s.note, s.update_date, s.update_uid " +
                "FROM shokai AS s, application AS a " +
                "WHERE update_date BETWEEN :from AND :to " +
                "AND s.aid=a.aid " +
                "ORDER BY s.aid;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("from", NpgsqlDbType.Date).Value = from;
                cmd.Parameters.Add("to", NpgsqlDbType.Date).Value = to;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                foreach (var item in res)
                {
                    var s = new ShokaiData();
                    s.AID = (int)item[0];
                    s.ShokaiID = (int)item[1];
                    s.YM = (int)item[2];
                    s.Barcode = (string)item[3];
                    s.Reason = (ShokaiReason)(int)item[4];
                    s.Status = (SHOKAI_STATUS)(int)item[5];
                    s.Result = (SHOKAI_RESULT)(int)item[6];
                    s.Henrei = (bool)item[7];
                    s.Note = (string)item[8];
                    s.UpdateDate = (DateTime)item[9];
                    s.UpdateUID = (int)item[10];
                    l.Add(s);
                }
                return l;
            }
        }

        /// <summary>
        /// ステータスと返戻有無、コメントを更新します
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatus()
        {
            var sql = "UPDATE shokai SET " +
                "shokai_reason=:reason, shokai_status=:status, shokai_result=:result, " +
                "henrei=:henrei, note=:note, update_date=:dt, update_uid=uid " +
                "WHERE aid=:aid;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("reason", NpgsqlDbType.Integer).Value = (int)this.Reason;
                cmd.Parameters.Add("status", NpgsqlDbType.Integer).Value = (int)this.Status;
                cmd.Parameters.Add("result", NpgsqlDbType.Integer).Value = (int)this.Result;
                cmd.Parameters.Add("henrei", NpgsqlDbType.Boolean).Value = this.Henrei;
                cmd.Parameters.Add("note", NpgsqlDbType.Text).Value = this.Note;
                cmd.Parameters.Add("dt", NpgsqlDbType.Date).Value = DateTime.Today;
                cmd.Parameters.Add("uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = AID;
                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 指定された照会IDがあるかどうかを返します
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool ExistsShokaiID(int id)
        {
            var sql = "SELECT EXISTS(SELECT shokai_id FROM shokai WHERE shokai_id=:id)";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("id", NpgsqlDbType.Integer).Value = id;
                var res = cmd.TryExecuteScalar();
                if (res == DBNull.Value || !(bool)res) return false;
                return true;
            }
        }
    }
}
