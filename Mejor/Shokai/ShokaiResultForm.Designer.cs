﻿namespace Mejor.Shokai
{
    partial class ShokaiResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.checkBoxHenrei = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymous = new System.Windows.Forms.CheckBox();
            this.checkBoxNot = new System.Windows.Forms.CheckBox();
            this.checkBoxOther = new System.Windows.Forms.CheckBox();
            this.checkBoxSign = new System.Windows.Forms.CheckBox();
            this.checkBoxYM = new System.Windows.Forms.CheckBox();
            this.checkBoxBui = new System.Windows.Forms.CheckBox();
            this.checkBoxCause = new System.Windows.Forms.CheckBox();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.tiffControl1 = new TiffControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(320, 662);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CurrentCellChanged += new System.EventHandler(this.dataGridView1_CurrentCellChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxComment);
            this.panel1.Controls.Add(this.checkBoxHenrei);
            this.panel1.Controls.Add(this.checkBoxAnonymous);
            this.panel1.Controls.Add(this.checkBoxNot);
            this.panel1.Controls.Add(this.checkBoxOther);
            this.panel1.Controls.Add(this.checkBoxSign);
            this.panel1.Controls.Add(this.checkBoxYM);
            this.panel1.Controls.Add(this.checkBoxBui);
            this.panel1.Controls.Add(this.checkBoxCause);
            this.panel1.Controls.Add(this.buttonRegist);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1156, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(183, 662);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "コメント";
            // 
            // textBoxComment
            // 
            this.textBoxComment.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxComment.Location = new System.Drawing.Point(6, 286);
            this.textBoxComment.Multiline = true;
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(171, 86);
            this.textBoxComment.TabIndex = 8;
            // 
            // checkBoxHenrei
            // 
            this.checkBoxHenrei.AutoSize = true;
            this.checkBoxHenrei.Location = new System.Drawing.Point(9, 406);
            this.checkBoxHenrei.Name = "checkBoxHenrei";
            this.checkBoxHenrei.Size = new System.Drawing.Size(74, 16);
            this.checkBoxHenrei.TabIndex = 5;
            this.checkBoxHenrei.Text = "返戻 ( 0 )";
            this.checkBoxHenrei.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnonymous
            // 
            this.checkBoxAnonymous.AutoSize = true;
            this.checkBoxAnonymous.Location = new System.Drawing.Point(9, 216);
            this.checkBoxAnonymous.Name = "checkBoxAnonymous";
            this.checkBoxAnonymous.Size = new System.Drawing.Size(98, 16);
            this.checkBoxAnonymous.TabIndex = 5;
            this.checkBoxAnonymous.Text = "匿名同意 ( 7 )";
            this.checkBoxAnonymous.UseVisualStyleBackColor = true;
            // 
            // checkBoxNot
            // 
            this.checkBoxNot.AutoSize = true;
            this.checkBoxNot.Location = new System.Drawing.Point(9, 191);
            this.checkBoxNot.Name = "checkBoxNot";
            this.checkBoxNot.Size = new System.Drawing.Size(93, 16);
            this.checkBoxNot.TabIndex = 4;
            this.checkBoxNot.Text = "同意なし ( 6 )";
            this.checkBoxNot.UseVisualStyleBackColor = true;
            // 
            // checkBoxOther
            // 
            this.checkBoxOther.AutoSize = true;
            this.checkBoxOther.Location = new System.Drawing.Point(9, 131);
            this.checkBoxOther.Name = "checkBoxOther";
            this.checkBoxOther.Size = new System.Drawing.Size(105, 16);
            this.checkBoxOther.TabIndex = 5;
            this.checkBoxOther.Text = "その他疑義 ( 5 )";
            this.checkBoxOther.UseVisualStyleBackColor = true;
            // 
            // checkBoxSign
            // 
            this.checkBoxSign.AutoSize = true;
            this.checkBoxSign.Location = new System.Drawing.Point(9, 106);
            this.checkBoxSign.Name = "checkBoxSign";
            this.checkBoxSign.Size = new System.Drawing.Size(110, 16);
            this.checkBoxSign.TabIndex = 4;
            this.checkBoxSign.Text = "署名欄疑義 ( 4 )";
            this.checkBoxSign.UseVisualStyleBackColor = true;
            // 
            // checkBoxYM
            // 
            this.checkBoxYM.AutoSize = true;
            this.checkBoxYM.Location = new System.Drawing.Point(9, 81);
            this.checkBoxYM.Name = "checkBoxYM";
            this.checkBoxYM.Size = new System.Drawing.Size(122, 16);
            this.checkBoxYM.TabIndex = 3;
            this.checkBoxYM.Text = "負傷年月相違 ( 3 )";
            this.checkBoxYM.UseVisualStyleBackColor = true;
            // 
            // checkBoxBui
            // 
            this.checkBoxBui.AutoSize = true;
            this.checkBoxBui.Location = new System.Drawing.Point(9, 56);
            this.checkBoxBui.Name = "checkBoxBui";
            this.checkBoxBui.Size = new System.Drawing.Size(122, 16);
            this.checkBoxBui.TabIndex = 2;
            this.checkBoxBui.Text = "施術部位相違 ( 2 )";
            this.checkBoxBui.UseVisualStyleBackColor = true;
            // 
            // checkBoxCause
            // 
            this.checkBoxCause.AutoSize = true;
            this.checkBoxCause.Location = new System.Drawing.Point(9, 31);
            this.checkBoxCause.Name = "checkBoxCause";
            this.checkBoxCause.Size = new System.Drawing.Size(122, 16);
            this.checkBoxCause.TabIndex = 1;
            this.checkBoxCause.Text = "負傷原因相違 ( 1 )";
            this.checkBoxCause.UseVisualStyleBackColor = true;
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(6, 477);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(171, 23);
            this.buttonRegist.TabIndex = 0;
            this.buttonRegist.Text = "登録(PageUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(320, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 662);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(1153, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 662);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // tiffControl1
            // 
            this.tiffControl1.AutoScroll = true;
            this.tiffControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tiffControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tiffControl1.Location = new System.Drawing.Point(323, 0);
            this.tiffControl1.Name = "tiffControl1";
            this.tiffControl1.Size = new System.Drawing.Size(830, 662);
            this.tiffControl1.TabIndex = 0;
            // 
            // ShokaiResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 662);
            this.Controls.Add(this.tiffControl1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ShokaiResultForm";
            this.Text = "照会結果入力";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShokaiResultForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ShokaiResultForm_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TiffControl tiffControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxComment;
        private System.Windows.Forms.CheckBox checkBoxOther;
        private System.Windows.Forms.CheckBox checkBoxSign;
        private System.Windows.Forms.CheckBox checkBoxYM;
        private System.Windows.Forms.CheckBox checkBoxBui;
        private System.Windows.Forms.CheckBox checkBoxCause;
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxHenrei;
        private System.Windows.Forms.CheckBox checkBoxAnonymous;
        private System.Windows.Forms.CheckBox checkBoxNot;
    }
}