﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using NpgsqlTypes;
using System.Drawing.Imaging;

namespace Mejor.Shokai
{
    public enum SHOKAI_IMAGE_STATUS { NULL = 0, 相違あり = 1, 相違なし = 2, 未返信 = 9 }

    public class ShokaiImage
    {
        public int ImageID { get; private set; }
        public int ImportID { get; private set; }
        public string FileName { get; private set; }
        public string Code { get; private set; }
        public int AID { get; private set; }
        [DB.DbAttribute.Ignore]
        public SHOKAI_IMAGE_STATUS ShokaiImageStatus => (SHOKAI_IMAGE_STATUS)(ImageID / 10000 % 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirName">ファイルのあるフォルダフルパス</param>
        /// <param name="barFront">コードが表面の場合=true</param>
        /// <param name="barRect">コード画像範囲Pixcel</param>
        /// <param name="barFormat">コードフォーマット</param>
        /// <returns></returns>
        public static bool Import(string dirName, bool barFront, SHOKAI_IMAGE_STATUS status, Rectangle barRect, ZXing.BarcodeFormat barFormat)
        {
            var f = new WaitForm();
            
            try
            {
                f.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                System.Threading.Tasks.Task.Factory.StartNew(() => f.ShowDialog());
                while (!f.Visible) System.Threading.Thread.Sleep(10);

                //インポートID取得
                int importid = 0;
                using (var cmd = DB.Main.CreateCmd("SELECT MAX(importid) FROM shokaiimageimport;"))
                {
                    var res = cmd.TryExecuteScalar();
                    if (res == DBNull.Value || res == null) importid = 1;
                    else importid = (int)res + 1;
                }

                //画像コピー先確保
                var dir = Settings.ImageFolder +
                    "\\" + DB.GetMainDBName() + "\\s" + importid.ToString();
                System.IO.Directory.CreateDirectory(dir);

                //画像処理・DB登録
                f.LogPrint("画像の取り込みを開始します");
                var fs = System.IO.Directory.GetFiles(dirName, "*.tif");
                f.LogPrint(fs.Length.ToString() + "件の画像を検出しました");

                if(fs.Count()==0)
                {
                    System.Windows.Forms.MessageBox.Show("画像が検出できません。画像を確認してください。");
                    f.Dispose();//20190729115428 furukawa WaitFormを落とす
                    return false;
                }

                if (fs.Count() % 2 != 0)
                {
                    System.Windows.Forms.MessageBox.Show("検出された画像が奇数枚です。" +
                        "表裏を一括処理するため処理を継続できません。画像を確認してください。");
                    f.Dispose();//20190729115505 furukawa WaitFormを落とす                                
                    return false;

                }

                Array.Sort(fs);
                f.SetMax(fs.Length);

                var sql1 = "INSERT INTO shokaiimageimport " +
                    "(importid, importdate, uid) " +
                    "VALUES (:importid, :importdate, :uid);";

                using (var tran = DB.Main.CreateTransaction())
                using (var cmd = DB.Main.CreateCmd(sql1, tran))
                {
                    cmd.Parameters.Add("importid", NpgsqlDbType.Integer).Value = importid;
                    cmd.Parameters.Add("importdate", NpgsqlDbType.Date).Value = DateTime.Today;
                    cmd.Parameters.Add("uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;

                    if (!cmd.TryExecuteNonQuery())
                    {
                        System.Windows.Forms.MessageBox.Show("エラーが発生しました。中断します。");
                        tran.Rollback();
                        return false;
                    }
                    int docCount = 0;

                    try
                    {
                        for (int i = 0; i < fs.Count(); i += 2)
                        {
                            if (f.Cancel)
                            {
                                var res = System.Windows.Forms.MessageBox.Show(
                                    "キャンセルボタンが押されました。キャンセルしてもよろしいですか？",
                                    "キャンセル処理",
                                    System.Windows.Forms.MessageBoxButtons.YesNo,
                                    System.Windows.Forms.MessageBoxIcon.Hand);

                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                    System.IO.Directory.Delete(dir, true);
                                    return false;
                                }

                                f.Cancel = false;
                            }

                            string frontCode = string.Empty;
                            string backCode = string.Empty;
                            string strNoCode = string.Empty;

                            #region 両面画像作成
                            using (var frontBmp = (Bitmap)Image.FromFile(fs[i]))
                            using (var backBmp = (Bitmap)Image.FromFile(fs[i + 1]))
                            {
                                try
                                {
                                    //20220624094546 furukawa st ////////////////////////
                                    //QRコードビットマップフォーマットを32に変更したら兵庫2022、広島2022読めたので2段構えにする
                                    using (var bmp32 = frontBmp.Clone(barRect, PixelFormat.Format32bppRgb))
                                    {
                                        frontCode = BarDecode.Decode(bmp32, barFormat);
                                        if (frontCode == string.Empty)
                                        {
                                            using (var bmp24 = frontBmp.Clone(barRect, PixelFormat.Format24bppRgb))

                                            {
                                                frontCode = BarDecode.Decode(bmp24, barFormat);
                                                if (frontCode == string.Empty) Log.ErrorWrite($"ファイル:{fs[i]}バーコード検出できず");
                                                f.InvokeValue++;
                                            }
                                        }
                                    }

                                    //using (var bmp = frontBmp.Clone(barRect, PixelFormat.Format24bppRgb))                                    
                                    //{
                                    //    frontCode = BarDecode.Decode(bmp, barFormat);
                                    //    if(frontCode==string.Empty)Log.ErrorWrite($"ファイル:{fs[i]}バーコード検出できず");
                                    //    f.InvokeValue++;
                                    //}
                                    //20220624094546 furukawa ed ////////////////////////
                                }
                                catch (Exception ex)
                                {
                                    Log.ErrorWrite($"{ex}:\r\nファイル:{fs[i + 1]}\r\nバーコード判別中エラー 画像範囲超え？");
                                }

                                try
                                {

                                    //20220624092602 furukawa st ////////////////////////
                                    //QRコードビットマップフォーマットを32に変更したら兵庫2022、広島2022読めたので2段構えにする

                                    using (var bmp32 = backBmp.Clone(barRect, PixelFormat.Format32bppRgb))
                                    {
                                        backCode = BarDecode.Decode(bmp32, barFormat);
                                        //bmp32.Save("D:\\projects\\mejor_docs\\11_保険者\\qr.bmp");//QRコード確認用
                                        if (backCode == string.Empty)
                                        {
                                            using (var bmp24 = backBmp.Clone(barRect, PixelFormat.Format24bppRgb))
                                            {
                                                backCode = BarDecode.Decode(bmp24, barFormat);
                                                if (backCode == string.Empty) Log.ErrorWrite($"ファイル:{fs[i + 1]}バーコード検出できず");
                                            }                                            
                                        }
                                        f.InvokeValue++;
                                    }



                                    //using (var bmp = backBmp.Clone(barRect, PixelFormat.Format24bppRgb))

                                    //{
                                    //    backCode = BarDecode.Decode(bmp, barFormat);                                        
                                    //    if (backCode == string.Empty) Log.ErrorWrite($"ファイル:{fs[i + 1]}バーコード検出できず");
                                    //    f.InvokeValue++;
                                    //}

                                    //20220624092602 furukawa ed //////////////////////////////
                                }
                                catch (Exception ex)
                                {
                                    Log.ErrorWrite($"{ex}:\r\nファイル:{fs[i + 1]}\r\nバーコード判別中エラー 画像範囲超え？");
                                }

                             


                                docCount++;
                                //裏表バーコードチェック
                                if ((barFront && !string.IsNullOrEmpty(backCode)) ||
                                    !barFront && !string.IsNullOrEmpty(frontCode))
                                {
                                    var res = System.Windows.Forms.MessageBox.Show(
                                        "バーコードの表裏チェックでエラーが出ました。不要な画像がないか、裏表が逆になっていないかご確認下さい" +
                                        "エラー画像名:" + fs[i], "",
                                        System.Windows.Forms.MessageBoxButtons.OK,
                                        System.Windows.Forms.MessageBoxIcon.Error);
                                    tran.Rollback();
                                    System.IO.Directory.Delete(dir, true);
                                    return false;
                                }

                                //裏表を1ファイルとしてセーブ
                                var fn = $"{dir}\\{importid}{(int)status}{docCount.ToString("0000")}.tif";
                                if (!ImageUtility.Save(new List<Bitmap> { frontBmp, backBmp }, fn))
                                {
                                    tran.Rollback();
                                    System.IO.Directory.Delete(dir, true);
                                    return false;
                                }
                            }
                            #endregion

                            #region 画像ファイル名とimportIDを登録
                            //データベースへ登録
                            var s = new ShokaiImage();
                            s.ImportID = importid;
                            s.ImageID = importid * 100000 + (int)status * 10000 + docCount;
                            s.Code = barFront ? frontCode : backCode;
                            s.AID = 0;
                            s.FileName = s.ImageID.ToString() + ".tif";

                            if (!DB.Main.Insert(s, tran))
                            {
                                System.Windows.Forms.MessageBox.Show(
                                    "データベースへ登録できませんでした。取り込みを中止します。",
                                    "DB登録エラー",
                                    System.Windows.Forms.MessageBoxButtons.OK,
                                    System.Windows.Forms.MessageBoxIcon.Hand);

                                System.IO.Directory.Delete(dir, true);
                                return false;
                            }
                            #endregion

                        }

                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        System.IO.Directory.Delete(dir, true);
                        tran.Rollback();
                        return false;
                    }
                    finally
                    {
                        f.Dispose();
                    }

                    tran.Commit();
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 画像ファイルのフルパスを取得します
        /// </summary>
        /// <returns></returns>
        public string GetFullPath()
        {
            return Settings.ImageFolder +
                "\\" + DB.GetMainDBName() + "\\s" + ImportID.ToString() + "\\" + FileName;
        }

        /// <summary>
        /// まだAIDが確定していないデータに対し、バーコードの情報をもとにAIDを確定します。
        /// </summary>
        /// <returns></returns>
        public static bool UpdateAID()
        {
            var sql = "UPDATE shokaiimage AS si SET aid=s.aid " +
                "FROM (SELECT * FROM shokai) AS s " +
                "WHERE si.aid=0 " +
                "AND si.code=s.code";

            using (var cmd = DB.Main.CreateCmd(sql)) return cmd.TryExecuteNonQuery();
        }

        /// <summary>
        /// バーコード情報が記録されていない画像をすべて取得します
        /// </summary>
        /// <returns></returns>
        public static List<ShokaiImage> GetNotReadBarcodeImages()
        {
            var res = DB.Main.Select<ShokaiImage>("code=''");
            return res.ToList();
        }

        public static List<ShokaiImage> SelectByCym(int cym)
        {
            var sql = "SELECT " +
                "imageid, importid, filename, code, a.aid " +
                "FROM shokaiimage AS s, application AS a " +
                $"WHERE s.code<>'' AND s.code=a.shokaicode AND cym={cym};";
            var l = DB.Main.Query<ShokaiImage>(sql);
            return l == null ? new List<ShokaiImage>() : l.ToList();
        }

        public static List<ShokaiImage> SelectByCym(int fromCym, int toCym)
        {
            var sql = "SELECT " +
                "imageid, importid, filename, code, a.aid " +
                "FROM shokaiimage AS s, application AS a " +
                $"WHERE s.code<>'' AND s.code=a.shokaicode AND cym BETWEEN {fromCym} AND {toCym};";
            var l = DB.Main.Query<ShokaiImage>(sql);
            return l == null ? new List<ShokaiImage>() : l.ToList();
        }

        public static List<ShokaiImage> SelectByImport(int importID)
        {
            var sql = "SELECT " +
                "imageid, importid, filename, code, a.aid " +
                "FROM shokaiimage AS s, application AS a " +
                $"WHERE s.code<>'' AND s.code=a.shokaicode AND importid={importID};";
            var l = DB.Main.Query<ShokaiImage>(sql);
            return l == null ? new List<ShokaiImage>() : l.ToList();
        }

        /// <summary>
        /// バーコード情報を記録します
        /// </summary>
        /// <returns></returns>
        public bool UpdateBarcodeStr(string code)
        {
            var sql = "UPDATE shokaiimage SET code=:bc WHERE imageid=:id;";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                Code = code;
                cmd.Parameters.Add("bc", NpgsqlDbType.Text).Value = code;
                cmd.Parameters.Add("id", NpgsqlDbType.Integer).Value = ImageID;
                return cmd.TryExecuteNonQuery();
            }
        }

        public static ShokaiImage SelectByAid(int aid)
        {
            var l = DB.Main.Select<ShokaiImage>($"aid={aid}");
            return l.SingleOrDefault();
        }

        public static ShokaiImage SelectByCode(string shokaiCode)
        {
            var l = DB.Main.Select<ShokaiImage>($"code='{shokaiCode}'");
            return l.SingleOrDefault();
        }

        public bool Delete()
        {
            var sql = $"DELETE FROM shokaiimage WHERE ImageID={ImageID}";
            return DB.Main.Excute(sql);
        }
        public bool Delete(DB.Transaction tran)
        {
            var sql = $"DELETE FROM shokaiimage WHERE ImageID={ImageID}";
            return DB.Main.Excute(sql, null, tran);
        }
    }
}
