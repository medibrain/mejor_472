﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiInputForm : Form
    {
        BindingSource bs = new BindingSource();
        int cym = 0;

        public ShokaiInputForm(App app)
        {
            InitializeComponent();
            cym = 0;

            panel2.Enabled = false;
            panel3.Enabled = false;
            radioButtonInputAll.Checked = false;
            radioButtonSouiAll.Checked = false;

            var apps = App.GetAppsFamily(app.HihoNum, app.CYM) ?? new List<App>();
            apps.RemoveAll(x => app.YM < x.YM);

            //関連申請書に誕生日を検索条件としてつけるか
            //現在広域かどうかで判断
            var birthIgnore = Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.後期高齢;
            if (!birthIgnore) apps.RemoveAll(x => x.Birthday != app.Birthday);
            
            var l = ShokaiInfo.SelectByApps(apps);
            l.Sort((x, y) => x.ShokaiImageStatus == y.ShokaiImageStatus ?
                x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                x.ShokaiImageStatus.CompareTo(y.ShokaiImageStatus));
            bs.DataSource = l;
            initialize();
        }

        public ShokaiInputForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            var l = ShokaiInfo.SelectByCym(cym);
            l.Sort((x, y) => x.ShokaiImageStatus == y.ShokaiImageStatus ?
                x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                x.ShokaiImageStatus.CompareTo(y.ShokaiImageStatus));
            bs.DataSource = l;
            initialize();
        }

        public void initialize()
        {
            dataGridView1.DataSource = bs;
            bs.CurrentChanged += Bs_CurrentChanged;
            dataGridView1.Columns[nameof(ShokaiInfo.Aid)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.Aid)].Width = 70;
            dataGridView1.Columns[nameof(ShokaiInfo.Aid)].HeaderText = "AID";
            dataGridView1.Columns[nameof(ShokaiInfo.MediYear)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.MediYear)].Width = 25;
            dataGridView1.Columns[nameof(ShokaiInfo.MediYear)].HeaderText = "年";
            dataGridView1.Columns[nameof(ShokaiInfo.MediMonth)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.MediMonth)].Width = 25;
            dataGridView1.Columns[nameof(ShokaiInfo.MediMonth)].HeaderText = "月";
            dataGridView1.Columns[nameof(ShokaiInfo.AppType)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.AppType)].Width = 50;
            dataGridView1.Columns[nameof(ShokaiInfo.AppType)].HeaderText = "種";
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiResultStatus)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiResultStatus)].Width = 60;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiResultStatus)].HeaderText = "結果";
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiResultStatus)].DisplayIndex = 3;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiCode)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiCode)].Width = 80;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiCode)].HeaderText = "照会番号";
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiCode)].DisplayIndex = 2;
            dataGridView1.Columns[nameof(ShokaiInfo.Overlapped)].Visible = true;
            dataGridView1.Columns[nameof(ShokaiInfo.Overlapped)].Width = 30;
            dataGridView1.Columns[nameof(ShokaiInfo.Overlapped)].HeaderText = "重";
            dataGridView1.Columns[nameof(ShokaiInfo.Overlapped)].DisplayIndex = 4;
            dataGridView1.Columns[nameof(ShokaiInfo.Overlapped)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridView1.Columns[nameof(ShokaiInfo.app)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiImage)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiImageID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiInfo.ShokaiImageStatus)].Visible = false;

        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            setImage();            
        }

        private void setImage()
        {
            var s = (ShokaiInfo)bs.Current;
            var app = s?.app;

            if (app == null)
            {
                scrollPictureControlFront.Clear();
                scrollPictureControlBack.Clear();
                inspectControl1.AllClear();
            }
            else
            {            
                scrollPictureControlFront.SetPictureFile(app.GetImageFullPath());
                scrollPictureControlFront.SetPictureBoxFill();
                
                inspectControl1.SetApp(app);

                //var si = ShokaiImage.SelectByCode(app.ShokaiCode);
                var si = s.ShokaiImage;

                if (si == null)
                {
                    scrollPictureControlBack.Clear();
                }
                else
                {
                    using (var fs = new System.IO.FileStream(si.GetFullPath(), System.IO.FileMode.Open))
                    using (var ms = new System.IO.MemoryStream())
                    {
                        fs.CopyTo(ms);
                        scrollPictureControlFront.SetImageMemoryStream(ms, si.GetFullPath());
                        scrollPictureControlBack.SetImageMemoryStream(ms, si.GetFullPath());
                        scrollPictureControlBack.PageSelect(1);
                    }
                    scrollPictureControlFront.SetPictureBoxFill();
                    scrollPictureControlBack.SetPictureBoxFill();
                }
            }
        }

        private void ShokaiInputForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
        }

        private void ShokaiInputForm_Shown(object sender, EventArgs e)
        {
            setImage();

            //20211130163403 furukawa st ////////////////////////
            //リスト部に連番を付けて件数が分かるように
            CreateNoField();
            //20211130163403 furukawa ed ////////////////////////

        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked) return;
            getShokaiImageList();
        }

        /// <summary>
        /// //20211130163139 furukawa リスト部に連番を付けて件数が分かるように
        /// </summary>
        private void CreateNoField()
        {
            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            DataGridViewTextBoxColumn c = new DataGridViewTextBoxColumn();
            
            c.Width = 30;
            c.HeaderText = "行";
            c.Name = "rownum";
            c.Visible = true;
            cs.BackColor = Color.LightGray;
            c.DefaultCellStyle = cs;
            bool flg = false;
            foreach (DataGridViewColumn vc in dataGridView1.Columns)
            {
                if (vc.Name == c.Name) flg = true;
            }

            if (!flg)
            {
                dataGridView1.Columns.Insert(0, c);
            }
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                r.Cells[0].Value = (r.Index + 1).ToString();
            }
            
        }

        private void getShokaiImageList()
        {
            //20201022141430 furukawa st ////////////////////////
            //ロードが長いのでメッセージ
            
            WaitFormSimple wf = new WaitFormSimple();
            wf.StartPosition = FormStartPosition.CenterScreen;
            System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
            //20201022141430 furukawa ed ////////////////////////

            bs.CurrentChanged -= Bs_CurrentChanged;
            var l = ShokaiInfo.SelectByCym(cym);

            if (radioButtonSouiAri.Checked) l = l.FindAll(x => x.ShokaiImageStatus == SHOKAI_IMAGE_STATUS.相違あり);
            if (radioButtonSouiNashi.Checked) l = l.FindAll(x => x.ShokaiImageStatus == SHOKAI_IMAGE_STATUS.相違なし);
            if (radioButtonMihenshin.Checked) l = l.FindAll(x => x.ShokaiImageStatus == SHOKAI_IMAGE_STATUS.未返信);
            if (radioButtonInputed.Checked) l = l.FindAll(x => x.ShokaiResultStatus != "未返信");
            if (radioButtonNotInput.Checked) l = l.FindAll(x => x.ShokaiResultStatus == "未返信");
            if (radioButtonOverwrap.Checked) l = l.FindAll(x => x.Overlapped != string.Empty);

            l.Sort((x, y) => x.ShokaiImageStatus == y.ShokaiImageStatus ?
                x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                x.ShokaiImageStatus.CompareTo(y.ShokaiImageStatus));

            bs.DataSource = l;
            bs.CurrentChanged += Bs_CurrentChanged;

            setImage();

            //20211130163303 furukawa st ////////////////////////
            //リスト部に連番を付けて件数が分かるように            
            CreateNoField();
            //20211130163303 furukawa ed ////////////////////////


            //20201022141509 furukawa st ////////////////////////
            //メッセージクローズ
            wf.InvokeCloseDispose();
            //20201022141509 furukawa ed ////////////////////////
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            var app = ((ShokaiInfo)bs.Current)?.app;
            if (app == null) return;

            if (!inspectControl1.CanUpdate(app, true)) return;
            inspectControl1.DataSetToApp(app);

            app.UpdateINQ();
            bs.MoveNext();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var info = (ShokaiInfo)bs.Current;
            if (info == null) return;

            var app = App.GetApp(info.Aid);
            InputStarter.Start(app.GroupID, INPUT_TYPE.First, app.Aid);
        }

        private void 選択中の照会画像を削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var s = (ShokaiInfo)bs.Current;
            if (s == null) return;

            var res = MessageBox.Show("選択中の紹介画像\r\n" +
                $"照会ID:{s.ShokaiCode}\r\n" +
                $"画像ファイル:{s.ShokaiImage.GetFullPath()}\r\n" +
                $"を削除します。もとには戻せませんが、本当によろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;

            if(s.ShokaiImage.Delete())
            {
                MessageBox.Show("画像を削除しました");
                getShokaiImageList();
            }
            else
            {
                MessageBox.Show("画像の削除に失敗しました");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            //20201020180027 furukawa st ////////////////////////
            //ソート
            WaitFormSimple wf = new WaitFormSimple();
            wf.StartPosition = FormStartPosition.CenterScreen;
            System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());

            var l = ShokaiInfo.SelectByCym(cym);


            switch (dataGridView1.Columns[e.ColumnIndex].Name)

            {
                case nameof(ShokaiInfo.ShokaiCode):
                    l.Sort((x, y) => x.ShokaiCode == y.ShokaiCode ?
                    x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                    x.ShokaiCode.CompareTo(y.ShokaiCode));
                    break;
                case nameof(ShokaiInfo.Aid):
                    l.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                    break;

                case nameof(ShokaiInfo.AppType):
                    l.Sort((x, y) => x.AppType == y.AppType ?
                    x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                    x.AppType.CompareTo(y.AppType));
                    break;

                case nameof(ShokaiInfo.ShokaiResultStatus):
                    l.Sort((x, y) => x.ShokaiResultStatus == y.ShokaiResultStatus ?
                    x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                    x.ShokaiResultStatus.CompareTo(y.ShokaiResultStatus));
                    break;

                default:
                    l.Sort((x, y) => x.ShokaiCode == y.ShokaiCode ?
                    x.ShokaiImageID.CompareTo(y.ShokaiImageID) :
                    x.ShokaiCode.CompareTo(y.ShokaiCode));
                    break;
            }
                
           


            bs.DataSource = l;
            initialize();

            //20211130163333 furukawa st ////////////////////////
            //リスト部に連番を付けて件数が分かるように
            CreateNoField();
            //20211130163333 furukawa ed ////////////////////////


            wf.InvokeCloseDispose();
            
            //20201020180027 furukawa ed ////////////////////////

        }
    }
}
