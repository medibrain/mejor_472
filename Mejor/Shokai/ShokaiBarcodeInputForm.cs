﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiBarcodeInputForm : Form
    {
        BindingSource bs = null;

        public ShokaiBarcodeInputForm()
        {
            InitializeComponent();

            bs = new BindingSource();
            bs.DataSource = ShokaiImage.GetNotReadBarcodeImages();
            dataGridView1.DataSource = bs;
            bs.CurrentItemChanged += Bs_CurrentItemChanged;
            scrollPictureControl1.Ratio = 0.4f;
            setShokaiImage();
        }

        private void Bs_CurrentItemChanged(object sender, EventArgs e)
        {
            setShokaiImage();
        }

        private void setShokaiImage()
        {
            var si = (ShokaiImage)bs.Current;
            textBoxCode.Text = si.Code;
            scrollPictureControl1.SetPictureFile(si.GetFullPath());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            int rowIndex = dataGridView1.CurrentCell.RowIndex;
            if (rowIndex < 0) return;

            if (!codeUpdate())
            {
                MessageBox.Show("バーコードの記録に失敗しました", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int nextRow = rowIndex + 1;
            if (nextRow == dataGridView1.RowCount)
            {
                var res = MessageBox.Show("最後のデータの記録が終了しました。画面を閉じますか？", "終了確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.OK) this.Close();
                return;
            }

            dataGridView1.CurrentCell = dataGridView1[0, nextRow];
            textBoxCode.Focus();
        }

        private void ShokaiBarcodeInputForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                buttonUpdate.PerformClick();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        /// <summary>
        /// 入力値通りにバーコードを記録します
        /// </summary>
        /// <returns></returns>
        private bool codeUpdate()
        {
            var si = (ShokaiImage)dataGridView1.CurrentRow.DataBoundItem;
            var s = textBoxCode.Text.Trim();

            if(s == "---")
            {
                var res = MessageBox.Show("この画像を「無視」として登録してもよろしいですか？", "無視画像確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if(res != DialogResult.OK) return false;
            }


            //20191009164357 furukawa st ////////////////////////
            //被保番と患者名が両方ない場合エラーとする
            

            else if (string.IsNullOrEmpty(textBoxName.Text) && string.IsNullOrEmpty(textBoxHnum.Text))
            {
                MessageBox.Show("指定されたIDはすでに発行された照会IDに存在しないため" +
                    "登録できません。", "ID確認",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


                        //20191008113532 furukawa st ////////////////////////
                        //登録前チェック解除

                        /*
                        else if (string.IsNullOrEmpty(textBoxName.Text))
                        {
                            MessageBox.Show("指定されたIDはすでに発行された照会IDに存在しないため" +
                                "登録できません。", "ID確認",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                        */
                        //20191008113532 furukawa ed ////////////////////////


            //20191009164357 furukawa ed ////////////////////////


            return si.UpdateBarcodeStr(s);
        }

        private void textBoxCode_TextChanged(object sender, EventArgs e)
        {
            textBoxName.Clear();

            //20191009101859 furukawa st ////////////////////////
            //被保番テキストクリア
            
            textBoxHnum.Clear();
            //20191009101859 furukawa ed ////////////////////////
        }

        private void textBoxCode_Leave(object sender, EventArgs e)
        {
            //20191009102000 furukawa st ////////////////////////
            //被保番と患者名を同時に取得、表示
            
            

            string strPname = string.Empty;
            string strhnum = string.Empty;
            ShokaiCodeData.GetNameHNumFromShokaiCode(textBoxCode.Text.Trim(), out strPname, out strhnum);
            textBoxName.Text = strPname;
            textBoxHnum.Text = strhnum;


                        //textBoxName.Text = ShokaiCodeData.GetNameFromShokaiCode(textBoxCode.Text.Trim());


            //20191009102000 furukawa ed ////////////////////////

        }
    }
}
