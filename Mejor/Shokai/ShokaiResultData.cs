﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.Shokai
{
    class ShokaiResultData
    {
        public int AID { get; private set; }
        public int FullShokaiID { get; private set; }
        public int ShokaiID { get { return FullShokaiID % 10000; } }
        public string Hnum { get; private set; }
        public string Pname { get; private set; }
        public ShokaiReason Reason { get; private set; }
        public SHOKAI_STATUS Status { get; set; }
        public SHOKAI_RESULT Result { get; set; }
        public bool Henrei { get; set; }
        public string Note { get; set; }
        public DateTime UpdateDate { get; private set; }
        public int UpdateUID { get; private set; }
        public int ShokaiImportID { get; private set; }
        public string FileName { get; private set; }

        public APP_TYPE AppType { get; set; }

        /// <summary>
        /// 画像ファイルのフルパスを取得します
        /// </summary>
        /// <returns></returns>
        public string GetFullPath()
        {
            return Settings.ImageFolder +
                "\\" + DB.GetMainDBName() + "\\s" + ShokaiImportID.ToString() + "\\" + FileName;
        }

        /// <summary>
        /// 指定された月で返信があり、画像取り込みされた情報を返します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <returns></returns>
        public static List<ShokaiResultData> GetResultsWithImage(int cym)
        {
            var sql = "SELECT a.aid, s.shokai_id, a.hnum, a.pname, " +
                "s.shokai_reason, s.shokai_status, s.shokai_result, s.henrei, s.note, " +
                "s.update_date, s.update_uid, i.import_id, i.filename, a.aapptype " +
                "FROM application AS a, shokai AS s, shokai_image AS i " +
                "WHERE a.aid=s.aid AND a.aid=i.aid " +
                "AND a.cym=:cym AND achargemonth=:cm " +
                "ORDER BY s.shokai_id;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("cym", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<ShokaiResultData>();
                foreach (var item in res)
                {
                    var sr = new ShokaiResultData();
                    sr.AID = (int)item[0];
                    sr.FullShokaiID = (int)item[1];
                    sr.Hnum = (string)item[2];
                    sr.Pname = (string)item[3];
                    sr.Reason = (ShokaiReason)(int)item[4];
                    sr.Status = (SHOKAI_STATUS)(int)item[5];
                    sr.Result = (SHOKAI_RESULT)(int)item[6];
                    sr.Henrei = (bool)item[7];
                    sr.Note = (string)item[8];
                    sr.UpdateDate = (DateTime)item[9];
                    sr.UpdateUID = (int)item[10];
                    sr.ShokaiImportID = (int)item[11];
                    sr.FileName = (string)item[12];
                    sr.AppType = (APP_TYPE)(int)item[13];
                    l.Add(sr);
                }

                return l;
            }
        }

        /// <summary>
        /// ステータスと結果情報を更新します
        /// </summary>
        /// <returns></returns>
        public bool Update( DB.Transaction tran)
        {
            var sql = "UPDATE shokai " +
                "SET shokai_status=:st, shokai_result=:res, henrei=:hr, " +
                "note=:note, update_date=:dt, update_uid=:uid " +
                "WHERE shokai_id=:sid;";

            var dt = DateTime.Today;
            var uid = User.CurrentUser.UserID;

            using (var cmd = DB.Main.CreateCmd(sql, tran))
            {
                cmd.Parameters.Add("st", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)Status;
                cmd.Parameters.Add("res", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)Result;
                cmd.Parameters.Add("hr", NpgsqlTypes.NpgsqlDbType.Boolean).Value = Henrei;
                cmd.Parameters.Add("note", NpgsqlTypes.NpgsqlDbType.Text).Value = Note;
                cmd.Parameters.Add("dt", NpgsqlTypes.NpgsqlDbType.Date).Value = dt;
                cmd.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer).Value = uid;
                cmd.Parameters.Add("sid", NpgsqlTypes.NpgsqlDbType.Integer).Value = FullShokaiID;
                if (!cmd.TryExecuteNonQuery()) return false;
            }

            UpdateDate = dt;
            UpdateUID = uid;
            return true;
        }

        /// <summary>
        /// 指定した期間内にアップデートした情報を取得します
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static List<ShokaiResultData> SelectUpdated(DateTime from, DateTime to)
        {
            var sql = "SELECT a.aid, s.shokai_id, a.hnum, a.pname, " +
                "s.shokai_reason, s.shokai_status, s.shokai_result, s.henrei, s.note, " +
                "s.update_date, s.update_uid, i.import_id, i.filename " +
                "FROM application AS a, shokai AS s " +
                "LEFT OUTER JOIN shokai_image AS i ON s.aid=i.aid " +
                "WHERE a.aid=s.aid " +
                "AND s.update_date BETWEEN :from AND :to " +
                "ORDER BY s.shokai_id;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("from", NpgsqlDbType.Date).Value = from;
                cmd.Parameters.Add("to", NpgsqlDbType.Date).Value = to;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<ShokaiResultData>();
                foreach (var item in res)
                {
                    var sr = new ShokaiResultData();
                    sr.AID = (int)item[0];
                    sr.FullShokaiID = (int)item[1];
                    sr.Hnum = (string)item[2];
                    sr.Pname = (string)item[3];
                    sr.Reason = (ShokaiReason)(int)item[4];
                    sr.Status = (SHOKAI_STATUS)(int)item[5];
                    sr.Result = (SHOKAI_RESULT)(int)item[6];
                    sr.Henrei = (bool)item[7];
                    sr.Note = (string)item[8];
                    sr.UpdateDate = (DateTime)item[9];
                    sr.UpdateUID = (int)item[10];
                    sr.ShokaiImportID = item[11] == DBNull.Value ? 0 : (int)item[11];
                    sr.FileName = item[12] == DBNull.Value ? string.Empty : (string)item[12];
                    l.Add(sr);
                }
                return l;
            }
        }

    }
}
