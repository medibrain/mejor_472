﻿namespace Mejor.Shokai
{
    partial class ShokaiReasonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxBuiCount = new System.Windows.Forms.CheckBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxDayCount = new System.Windows.Forms.CheckBox();
            this.checkBoxPeriod = new System.Windows.Forms.CheckBox();
            this.checkBoxClinic = new System.Windows.Forms.CheckBox();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxOther = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkBoxBuiCount
            // 
            this.checkBoxBuiCount.AutoSize = true;
            this.checkBoxBuiCount.Location = new System.Drawing.Point(39, 76);
            this.checkBoxBuiCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxBuiCount.Name = "checkBoxBuiCount";
            this.checkBoxBuiCount.Size = new System.Drawing.Size(72, 22);
            this.checkBoxBuiCount.TabIndex = 0;
            this.checkBoxBuiCount.Text = "部位数";
            this.checkBoxBuiCount.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(190, 265);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(112, 35);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(312, 265);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 35);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkBoxDayCount
            // 
            this.checkBoxDayCount.AutoSize = true;
            this.checkBoxDayCount.Location = new System.Drawing.Point(39, 126);
            this.checkBoxDayCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxDayCount.Name = "checkBoxDayCount";
            this.checkBoxDayCount.Size = new System.Drawing.Size(87, 22);
            this.checkBoxDayCount.TabIndex = 0;
            this.checkBoxDayCount.Text = "施術日数";
            this.checkBoxDayCount.UseVisualStyleBackColor = true;
            // 
            // checkBoxPeriod
            // 
            this.checkBoxPeriod.AutoSize = true;
            this.checkBoxPeriod.Location = new System.Drawing.Point(39, 185);
            this.checkBoxPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxPeriod.Name = "checkBoxPeriod";
            this.checkBoxPeriod.Size = new System.Drawing.Size(87, 22);
            this.checkBoxPeriod.TabIndex = 0;
            this.checkBoxPeriod.Text = "施術期間";
            this.checkBoxPeriod.UseVisualStyleBackColor = true;
            // 
            // checkBoxClinic
            // 
            this.checkBoxClinic.AutoSize = true;
            this.checkBoxClinic.Location = new System.Drawing.Point(168, 76);
            this.checkBoxClinic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxClinic.Name = "checkBoxClinic";
            this.checkBoxClinic.Size = new System.Drawing.Size(102, 22);
            this.checkBoxClinic.TabIndex = 0;
            this.checkBoxClinic.Text = "疑義施術所";
            this.checkBoxClinic.UseVisualStyleBackColor = true;
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Location = new System.Drawing.Point(168, 126);
            this.checkBoxTotal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(87, 22);
            this.checkBoxTotal.TabIndex = 0;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "照会対象とする理由を選択してください";
            // 
            // checkBoxOther
            // 
            this.checkBoxOther.AutoSize = true;
            this.checkBoxOther.Location = new System.Drawing.Point(294, 185);
            this.checkBoxOther.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxOther.Name = "checkBoxOther";
            this.checkBoxOther.Size = new System.Drawing.Size(65, 22);
            this.checkBoxOther.TabIndex = 0;
            this.checkBoxOther.Text = "その他";
            this.checkBoxOther.UseVisualStyleBackColor = true;
            // 
            // ShokaiReasonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 318);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.checkBoxOther);
            this.Controls.Add(this.checkBoxTotal);
            this.Controls.Add(this.checkBoxClinic);
            this.Controls.Add(this.checkBoxPeriod);
            this.Controls.Add(this.checkBoxDayCount);
            this.Controls.Add(this.checkBoxBuiCount);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ShokaiReasonForm";
            this.Text = "照会理由";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxBuiCount;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox checkBoxDayCount;
        private System.Windows.Forms.CheckBox checkBoxPeriod;
        private System.Windows.Forms.CheckBox checkBoxClinic;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxOther;
    }
}