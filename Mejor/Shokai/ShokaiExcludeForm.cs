﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiExcludeForm : Form
    {
        int cym;
        BindingSource bs = new BindingSource();

        public ShokaiExcludeForm(int cym)
        {
            this.cym = cym;
            InitializeComponent();

            var l = ShokaiExclude.GetList(cym);
            l.Sort((x, y) => x.ExcludeID.CompareTo(y.ExcludeID));
            bs.DataSource = l;

            dataGridView1.DataSource = bs;
            dataGridView1.Columns[nameof(ShokaiExclude.ChargeYM)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiExclude.MediYM)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiExclude.Birth)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeID)].HeaderText = "ID";
            dataGridView1.Columns[nameof(ShokaiExclude.ImportID)].HeaderText = "インポート";
            dataGridView1.Columns[nameof(ShokaiExclude.HihoNum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(ShokaiExclude.BirthStr)].HeaderText = "生年月日";
            dataGridView1.Columns[nameof(ShokaiExclude.ChargeYmStr)].HeaderText = "処理年月";
            dataGridView1.Columns[nameof(ShokaiExclude.MediYmStr)].HeaderText = "診療年月";
            dataGridView1.Columns[nameof(ShokaiExclude.ImportID)].DisplayIndex = 0;
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeID)].DisplayIndex = 1;
            dataGridView1.Columns[nameof(ShokaiExclude.ChargeYmStr)].DisplayIndex = 2;
            dataGridView1.Columns[nameof(ShokaiExclude.MediYmStr)].DisplayIndex = 3;
            dataGridView1.Columns[nameof(ShokaiExclude.HihoNum)].DisplayIndex = 4;
            dataGridView1.Columns[nameof(ShokaiExclude.BirthStr)].DisplayIndex = 5;
            dataGridView1.Columns[nameof(ShokaiExclude.ImportID)].Width = 80;
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeID)].Width = 80;
            dataGridView1.Columns[nameof(ShokaiExclude.ChargeYmStr)].Width = 80;
            dataGridView1.Columns[nameof(ShokaiExclude.MediYmStr)].Width = 80;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var d = (ShokaiExclude)dataGridView1.CurrentRow.DataBoundItem;
            if (d == null) return;

            var res = MessageBox.Show($"選択されているID:[{d.ExcludeID}]のデータを削除してもよろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;


            if (!d.Delete())
            {
                MessageBox.Show("削除に失敗しました", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var l = ShokaiExclude.GetList(cym);
                l.Sort((x, y) => x.ExcludeID.CompareTo(y.ExcludeID));
                bs.DataSource = l;
                bs.ResetBindings(false);
            }
        }
    }
}
