﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiResultForm : Form
    {
        int currentRowIndex = -1;
        //int cy;
        //int cm;
        BindingSource bs = new BindingSource();

        public ShokaiResultForm(int cym)
        {
            InitializeComponent();
            this.KeyPreview = true;
            //this.cy = cy;
            //this.cm = cm;

            var l = Shokai.ShokaiResultData.GetResultsWithImage(cym);
            bs.DataSource = l;
            dataGridView1.DataSource = bs;
            dataGridView1.Columns[nameof(ShokaiResultData.AID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.FullShokaiID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.Note)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.Result)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.Henrei)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.UpdateDate)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.UpdateUID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.ShokaiImportID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.FileName)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.AID)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiResultData.ShokaiID)].HeaderText = "ID";
            dataGridView1.Columns[nameof(ShokaiResultData.ShokaiID)].Width = 40;
            dataGridView1.Columns[nameof(ShokaiResultData.Hnum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(ShokaiResultData.Hnum)].Width = 80;
            dataGridView1.Columns[nameof(ShokaiResultData.Pname)].HeaderText = "患者名";
            dataGridView1.Columns[nameof(ShokaiResultData.Pname)].Width = 100;
            dataGridView1.Columns[nameof(ShokaiResultData.Status)].HeaderText = "ステータス";
            dataGridView1.Columns[nameof(ShokaiResultData.Status)].Width = 80;
        }

        private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            int ri = dataGridView1.CurrentRow.Index;
            if (currentRowIndex == ri) return;
            currentRowIndex = ri;

            var sr = (Shokai.ShokaiResultData)dataGridView1.Rows[ri].DataBoundItem;
            var tiffName = sr.GetFullPath();
            tiffControl1.SetBlank();
            tiffControl1.SetTiff(tiffName);
            SetResult(sr);
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;

            int ri = dataGridView1.CurrentRow.Index;
            var sr = (Shokai.ShokaiResultData)dataGridView1.Rows[ri].DataBoundItem;

            SHOKAI_RESULT result = SHOKAI_RESULT.なし;
            if (checkBoxCause.Checked) result |= SHOKAI_RESULT.負傷原因相違;
            if (checkBoxBui.Checked) result |= SHOKAI_RESULT.施術部位相違;
            if (checkBoxYM.Checked) result |= SHOKAI_RESULT.負傷年月相違;
            if (checkBoxSign.Checked) result |= SHOKAI_RESULT.署名欄疑義;
            if (checkBoxOther.Checked) result |= SHOKAI_RESULT.その他疑義;
            if (checkBoxNot.Checked) result |= SHOKAI_RESULT.同意なし;
            if (checkBoxAnonymous.Checked) result |= SHOKAI_RESULT.匿名同意;
            sr.Result = result;

            sr.Status = (result.HasFlag(SHOKAI_RESULT.負傷原因相違) ||
                result.HasFlag(SHOKAI_RESULT.施術部位相違) ||
                result.HasFlag(SHOKAI_RESULT.負傷年月相違) ||
                result.HasFlag(SHOKAI_RESULT.署名欄疑義) ||
                result.HasFlag(SHOKAI_RESULT.その他疑義)) ?
                SHOKAI_STATUS.相違あり : SHOKAI_STATUS.相違なし;

            sr.Note = textBoxComment.Text.Trim();
            sr.Henrei = checkBoxHenrei.Checked;
            using (var tran = DB.Main.CreateTransaction())
            {
                //Appの返戻フラグ
                int hCode = (int)StatusFlag.返戻;
                var setSql = $"UPDATE application SET statusflags=statusflags|{hCode} WHERE aid=@aid;";
                var rmvSql = $"UPDATE application SET statusflags=statusflags#{hCode} WHERE aid=@aid;";

                if (!DB.Main.Excute(sr.Henrei ? setSql : rmvSql, new { aid = sr.AID }, tran) ||
                    !sr.Update(tran))
                {
                    MessageBox.Show("データベースのアップデートに失敗しました。" +
                    "結果入力画面をいったん閉じます。");
                    this.Close();
                    return;
                }
                tran.Commit();
            }
            bs.ResetBindings(false);

            if (dataGridView1.RowCount - 1 == ri)
            {
                var res = MessageBox.Show("最終行の編集が終わりました。この画面を閉じますか？",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.OK) Close();
                return;
            }

            ri++;
            dataGridView1.CurrentCell = dataGridView1["ShokaiID", ri];
            checkBoxCause.Focus();
        }

        private void SetResult(Shokai.ShokaiResultData sr)
        {
            checkBoxCause.Checked = sr.Result.HasFlag(SHOKAI_RESULT.負傷原因相違);
            checkBoxBui.Checked = sr.Result.HasFlag(SHOKAI_RESULT.施術部位相違);
            checkBoxYM.Checked = sr.Result.HasFlag(SHOKAI_RESULT.負傷年月相違);
            checkBoxSign.Checked = sr.Result.HasFlag(SHOKAI_RESULT.署名欄疑義);
            checkBoxOther.Checked = sr.Result.HasFlag(SHOKAI_RESULT.その他疑義);
            checkBoxNot.Checked = sr.Result.HasFlag(SHOKAI_RESULT.同意なし);
            checkBoxAnonymous.Checked = sr.Result.HasFlag(SHOKAI_RESULT.匿名同意);
            textBoxComment.Text = sr.Note;
            checkBoxHenrei.Checked = sr.Henrei;
        }

        private void ShokaiResultForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                e.Handled = true;
            }
            else if (textBoxComment.Focused) return;
            else if (e.KeyChar == '1') checkBoxCause.Checked = !checkBoxCause.Checked;
            else if (e.KeyChar == '2') checkBoxBui.Checked = !checkBoxBui.Checked;
            else if (e.KeyChar == '3') checkBoxYM.Checked = !checkBoxYM.Checked;
            else if (e.KeyChar == '4') checkBoxSign.Checked = !checkBoxSign.Checked;
            else if (e.KeyChar == '5') checkBoxOther.Checked = !checkBoxOther.Checked;
            else if (e.KeyChar == '6') checkBoxNot.Checked = !checkBoxNot.Checked;
            else if (e.KeyChar == '7') checkBoxAnonymous.Checked = !checkBoxAnonymous.Checked;
            else if (e.KeyChar == '0') checkBoxHenrei.Checked = !checkBoxHenrei.Checked;
        }

        private void ShokaiResultForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
        }
    }
}
