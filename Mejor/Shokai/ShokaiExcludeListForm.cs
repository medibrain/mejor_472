﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiExcludeListForm : Form
    {
        BindingSource bs = new BindingSource();

        public ShokaiExcludeListForm()
        {
            InitializeComponent();

            var l = new List<ShokaiExclude.ExcludeList>();
            bs.DataSource = l;
            dataGridView1.DataSource = bs;
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeList.ChargeYM)].Visible = false;
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeList.ChargeYmStr)].HeaderText = "年月";
            dataGridView1.Columns[nameof(ShokaiExclude.ExcludeList.ExcludeCount)].HeaderText = "件数";

            getList();
        }

        private void getList()
        {
            var l = ShokaiExclude.ExcludeList.GetList();
            l.Sort((x, y) => x.ChargeYM.CompareTo(y.ChargeYM));
            bs.DataSource = l;
            bs.ResetBindings(false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            if (ShokaiExclude.ImportBlackList()) getList();
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            if (ShokaiExclude.ImportLog()) getList();
        }

        private void buttonDetail_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell == null) return;
            if (bs.Current == null) return;

            var d = (ShokaiExclude.ExcludeList)bs.Current;
            using (var f = new ShokaiExcludeForm(d.ChargeYM))f.ShowDialog();

            getList();
        }
    }
}
