﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shokai
{
    public partial class ShokaiReasonForm : Form
    {
        public ShokaiReason Flags
        {
            get
            {
                var f = ShokaiReason.なし;
                if (checkBoxBuiCount.Checked) f |= ShokaiReason.部位数;
                if (checkBoxDayCount.Checked) f |= ShokaiReason.施術日数;
                if (checkBoxPeriod.Checked) f |= ShokaiReason.施術期間;
                if (checkBoxClinic.Checked) f |= ShokaiReason.疑義施術所;
                if (checkBoxTotal.Checked) f |= ShokaiReason.合計金額;
                if (checkBoxOther.Checked) f |= ShokaiReason.その他;
                return f;
            }
            set
            {
                checkBoxBuiCount.Checked = ((value & ShokaiReason.部位数) != 0);
                checkBoxDayCount.Checked = ((value & ShokaiReason.施術日数) != 0);
                checkBoxPeriod.Checked = ((value & ShokaiReason.施術期間) != 0);
                checkBoxClinic.Checked = ((value & ShokaiReason.疑義施術所) != 0);
                checkBoxTotal.Checked = ((value & ShokaiReason.合計金額) != 0);
                checkBoxOther.Checked = ((value & ShokaiReason.その他) != 0);
            }
        }

        public ShokaiReasonForm()
        {
            InitializeComponent();
            CommonTool.setFormColor(this);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (Flags == ShokaiReason.なし)
            {
                var res = MessageBox.Show("照会理由が何も指定されていませんがよろしいですか？",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res != DialogResult.OK) return;
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
