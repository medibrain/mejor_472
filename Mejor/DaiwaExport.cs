﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    /// <summary>
    /// 大和総研型エクスポートデータ
    /// </summary>
    class DaiwaExport
    {
        public enum TOKUTAI_KUBUN { Null = 0, 一般 = 1, 特待 = 2 }
        public enum SEX { Null = 0, 男 = 1, 女 = 2 }
        public enum HONKE_KUBUN { Null = 0, 本人 = 1, 家族 = 2 }
        public enum FUJYO_KUBUN { Null = 0, 市町村助成 = 1, 老人医療費助成 = 2 }


        //現金給付（柔整）登録伝票（大和総研データ仕様書　フタバ産業　2020/04/08）
        public string DataCode { get; } = "UAC01";                                              //データコード						UAC01：固定								
        public string KumiaiCode { get; set; } = string.Empty;                                  //組合コード（３桁）                  ２桁組合コード（左詰）								
        public string ShibuCode { get; set; } = string.Empty;                                   //支部コード                          本支部一括処理組合（支部無し組合も含む）は’０１’固定		
        public TOKUTAI_KUBUN TokutaiKubun { get; set; } = TOKUTAI_KUBUN.Null;                   //特退区分                            １：一般  ２：特退								

        public string Mark { get; set; } = string.Empty;                                        //記号   
        public string Number { get; set; } = string.Empty;                                      //番号                                							
        public SEX Sex { get; set; } = SEX.Null;                                                //性別                                							                             							
        public DateTime Birth { get; set; } = DateTime.MinValue;                                //生年月日元号                        元号＋生年のみの入力でも可								
        public string Zoku { get; set; } = string.Empty;                                        //続柄                                双子の場合は入力しないとエラー
        public HONKE_KUBUN Honke { get; set; } = HONKE_KUBUN.Null;                              //本人家族区分                        １：本人　２：家族			                         						
        public int ShinryoYYMM { get; set; } = 0;                                               //診療年月                            和暦（元号なし）				                         						
        public string ShiharaiCode { get; } = "     ";                                          //支払先コード                                            						
        public int Days { get; set; } = 0;                                                      //日数                           						
        public int Total { get; set; } = 0;                                                     //医療費総額						
        public FUJYO_KUBUN Fujyo { get; set; } = FUJYO_KUBUN.Null;                              //扶助区分								
        public DateTime FushoDate1 { get; set; } = DateTime.MinValue;                           //①負傷年月日						和暦（元号なし）     					
        public DateTime FirstDate1 { get; set; } = DateTime.MinValue;                           //治療開始年月日                      和暦（元号なし）     				
        public DateTime StartDate1 { get; set; } = DateTime.MinValue;                           //治療期間（自）                      和暦（元号なし）
        public DateTime FinishDate1 { get; set; } = DateTime.MinValue;                          //治療期間（至）                      和暦（元号なし）
        public string FushoName1 { get; set; } = string.Empty;                                  //傷病名                              漢字コードはＪＩＳコード（左詰）
        public DateTime FushoDate2 { get; set; } = DateTime.MinValue;                           //②負傷年月日                        和暦（元号なし）
        public DateTime FirstDate2 { get; set; } = DateTime.MinValue;                           //治療開始年月日                      和暦（元号なし）
        public DateTime StartDate2 { get; set; } = DateTime.MinValue;                           //治療期間（自）                      和暦（元号なし）
        public DateTime FinishDate2 { get; set; } = DateTime.MinValue;                          //治療期間（至）                      和暦（元号なし）
        public string FushoName2 { get; set; } = string.Empty;                                  //傷病名                              漢字コードはＪＩＳコード（左詰）
        public DateTime FushoDate3 { get; set; } = DateTime.MinValue;                           //③負傷年月日						和暦（元号なし）
        public DateTime FirstDate3 { get; set; } = DateTime.MinValue;                           //治療開始年月日                      和暦（元号なし）
        public DateTime StartDate3 { get; set; } = DateTime.MinValue;                           //治療期間（自）                      和暦（元号なし）
        public DateTime FinishDate3 { get; set; } = DateTime.MinValue;                          //治療期間（至）                      和暦（元号なし）
        public string FushoName3 { get; set; } = string.Empty;                                  //傷病名                              漢字コードはＪＩＳコード（左詰）
        public string DantaiCode { get; set; } = string.Empty;                                  //柔整師会コード								支払先団体コードを指定（移管時には’９９９９９’等を使用）			
        public string KobetsuCode { get; set; } = string.Empty;                                 //                                                個別コード（１５桁）												
        public string SeiriNo { get; set; } = string.Empty;                                     //整理№                                          記入不可 （エラーリストには出力されます）							
        public string SickCode { get; set; } = string.Empty;                                    //疾病コード（最大３つのコードを管理）            																	
        public DateTime EntryDate { get; set; } = DateTime.MinValue;                            //受付年月日                                      受付日を管理する場合は会社条件の設定が必要						
        public string EntryNo { get; set; } = string.Empty;                                     //受付№                                          受付№を管理する場合は会社条件の設定が必要						
        public int Bill { get; set; } = 0;                                                      //請求金額                                        柔整師会からの請求金額の記述がある場合に使用						
        public string BillType { get; set; } = string.Empty;                                    //請求区分                                        １：46.請求金額を入力している場合									
        public string KogakuType { get; } = " ";                                                //高額療養費計算区分                              未使用															
        public string FukaType { get; } = " ";                                                  //療養附加金計算区分                              未使用															
        public int Ratio { get; set; } = 0;                                                     //給付割合                                        （H14/10月診療以降のみ必須）　07:7割健保負担～10:10割健保負担		
        public string Blank { get; } = "           ";                                           //予備                                            			
        public int ChargeYM { get; set; }                                                       //請求年月-柔整画像連携用                         西暦　例）２０１９年５月　⇒　201905								
        public string Ketsugi { get; } = "             ";                                       //現金給付決議書№                                記入不可															
        public string Minus { get; } = " ";                                                     //マイナス区分                                    未使用															
        public string Kyosei { get; } = " ";                                                    //強制区分                                        未使用															
        public string KeisanYM { get; } = "    ";                                               //計算年月                                        記入不可														



        public int Ayear => app.MediYear;
        public string Numbering => app.Numbering;

        private App app { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(DataCode);
            sb.Append(AddRightSpace(KumiaiCode, 3));
            sb.Append(ShibuCode);
            sb.Append(((int)TokutaiKubun));
            sb.Append(AddLeftSpace(Mark, 4));
            sb.Append(AddLeftSpace(Number, 7));
            sb.Append((int)Sex);
            sb.Append(getJdateWithDaiwaEraNumber(Birth));
            sb.Append(AddLeftSpace(Zoku, 2));
            sb.Append((int)Honke);
            sb.Append(ShinryoYYMM.ToString("0000"));
            sb.Append(ShiharaiCode);
            sb.Append(AddLeftSpace(Days.ToString(), 3));
            sb.Append(AddLeftSpace(Total.ToString(), 10));
            sb.Append(Fujyo == FUJYO_KUBUN.Null ? " " : ((int)Fujyo).ToString());
            sb.Append(toJpDateInt6(FushoDate1));
            sb.Append(toJpDateInt6(FirstDate1));
            sb.Append(toJpDateInt6(StartDate1));
            sb.Append(toJpDateInt6(FinishDate1));
            sb.Append(adjustFusyo(FushoName1));
            sb.Append(toJpDateInt6(FushoDate2));
            sb.Append(toJpDateInt6(FirstDate2));
            sb.Append(toJpDateInt6(StartDate2));
            sb.Append(toJpDateInt6(FinishDate2));
            sb.Append(adjustFusyo(FushoName2));
            sb.Append(toJpDateInt6(FushoDate3));
            sb.Append(toJpDateInt6(FirstDate3));
            sb.Append(toJpDateInt6(StartDate3));
            sb.Append(toJpDateInt6(FinishDate3));
            sb.Append(adjustFusyo(FushoName3));
            sb.Append(AddLeftSpace(DantaiCode, 5));
            sb.Append(AddRightSpace(KobetsuCode, 15));  //なぜか左詰め 大和総研指定
            sb.Append(AddLeftSpace(SeiriNo, 8));
            sb.Append(AddLeftSpace(SickCode, 12));
            sb.Append(toJpDateInt6(EntryDate));
            sb.Append(AddLeftSpace(EntryNo, 11));
            sb.Append(Bill == 0 ? "          " : AddLeftSpace(Bill.ToString(), 10));
            sb.Append(Bill == 0 ? " " : "1");
            sb.Append(KogakuType);
            sb.Append(FukaType);
            sb.Append(Ratio.ToString("00"));


            //20191011143718 furukawa st ////////////////////////
            //診療年月和暦を診療年月西暦に変換してデータに追加、診療年月と言っているが、実際はメホールにおける処理年月（診療年月＋１）       
            //information.csvの処理をコピーした
            int cym = DateTimeEx.GetAdYearFromHs(app.ChargeYear * 100 + app.ChargeMonth) * 100 + app.ChargeMonth;
            cym--;
            if (cym % 100 == 0) cym -= 88;
            sb.Append(cym);
            //sb.Append(Blank);

            //20191011143718 furukawa ed ////////////////////////


            //20200410170303 furukawa st ////////////////////////
            //もともとblankが11桁だったが、前の6桁を請求年月にしたので残りの部分をスペース5桁                       
            sb.Append(string.Empty.PadLeft(5));
            //20200410170303 furukawa ed ////////////////////////

            sb.Append(Ketsugi);
            sb.Append(Minus);
            sb.Append(Kyosei);
            sb.Append(KeisanYM);
            return sb.ToString();
        }

        public string GetImageFullPath()
        {
            return app.GetImageFullPath();
        }

        private string AddRightSpace(string str, int totalByteLength)
        {
            int bc = str.GetByteLength();
            return str + new string(' ', totalByteLength - bc);
        }

        private string AddLeftSpace(string str, int totalByteLength)
        {
            int bc = str.GetByteLength();
            return new string(' ', totalByteLength - bc) + str;
        }

        private string toJpDateInt6(DateTime dt)
        {
            if (dt == DateTime.MinValue) return "      ";
            var s = DateTimeEx.GetJpYear(dt).ToString("00");
            return s += dt.ToString("MMdd");
        }
        
        private string adjustFusyo(string fusho)
        {
            if (fusho == "Exist") return AddRightSpace(string.Empty, 40);

            fusho = Microsoft.VisualBasic.Strings.StrConv(fusho, Microsoft.VisualBasic.VbStrConv.Wide);
            if (fusho.Length > 20) fusho = fusho.Remove(20);

            return AddRightSpace(fusho, 40);
        }

        private string getJdateWithDaiwaEraNumber(DateTime dt)
        {
            var e = DateTimeEx.GetEraNumber(dt);
            if (e == 2) e = 3;
            else if (e == 3) e = 5;
            else if (e == 4) e = 7;

            //20190406181708 furukawa st ////////////////////////
            //新元号対応
            else if (e == 5) e = 9;

            //20190406181708 furukawa ed ////////////////////////


            var y = DateTimeEx.GetJpYear(dt);
            return e.ToString() + y.ToString("00") + dt.ToString("MMdd");
        }

        /// <summary>
        /// 大和総研型レコード作成
        /// </summary>
        /// <param name="app">申請書データ</param>
        /// <param name="kumiaiCode">組合コード（保険者より提供）</param>
        /// <param name="shibuCode">支部コード</param>
        /// <param name="dantaiCode">団体コード</param>
        /// <param name="exportNumbeing">ナンバリング</param>
        /// <returns></returns>
        public static DaiwaExport Create(App app, string kumiaiCode, string shibuCode, string dantaiCode, bool exportNumbeing)
        {
            var de = new DaiwaExport();
            de.app = app;

            //20190503153704 furukawa st ////////////////////////
            //DateTimeEx.GetAdYearFromHsの引数修正
            de.ChargeYM = DateTimeEx.GetAdYearFromHs(app.ChargeYear * 100 + app.ChargeMonth);
            //de.ChargeYM = DateTimeEx.GetAdYearFromHs(app.ChargeYear);
            //20190503153704 furukawa ed ////////////////////////

            de.ChargeYM = de.ChargeYM * 100 + app.ChargeMonth;            
            
            if (de.Ayear <= 0) return de;

            de.KumiaiCode = kumiaiCode;
            de.ShibuCode = shibuCode;
            de.TokutaiKubun = TOKUTAI_KUBUN.一般;
            de.Mark = app.HihoNum.Split('-')[0];
            de.Number = app.HihoNum.Split('-')[1];
            de.Sex = app.Sex == 1 ? SEX.男 : SEX.女;
            de.Birth = app.Birthday;
            de.Zoku = "";


            //20200408140357 furukawa st ////////////////////////
            //受療区分と本人家族をあわせて持っている保険者がいるので分岐

            int insurerid = (int)Insurer.CurrrentInsurer.InsurerID;
            switch (insurerid)
            {
                case (int)InsurerID.FUTABA:
                    //フタバは受療区分と本人家族をあわせて持っているので頭一桁で判断
                    //app.Family = family * 100 + juryoType;
                    int family = 0;
                    int.TryParse(app.Family.ToString().Substring(0, 1), out family);

                    //20200518170535 furukawa st ////////////////////////
                    //入力画面で既に変換しているので値そのままで判定
                    
                    de.Honke = family == 1 ? HONKE_KUBUN.本人 : HONKE_KUBUN.家族;
                    //de.Honke = family == 2 ? HONKE_KUBUN.本人 : HONKE_KUBUN.家族;
                    //20200518170535 furukawa ed ////////////////////////


                    break;

                default:
                    //その他
                    de.Honke = app.Family == 2 ? HONKE_KUBUN.本人 : HONKE_KUBUN.家族;
                    break;
            }

            //de.Honke = app.Family == 2 ? HONKE_KUBUN.本人 : HONKE_KUBUN.家族;

            //20200408140357 furukawa ed ////////////////////////



            de.ShinryoYYMM = app.MediYear * 100 + app.MediMonth;
            de.Days = app.CountedDays;
            de.Total = app.Total;
            de.Fujyo = app.HihoType == 1 ? FUJYO_KUBUN.市町村助成 : app.HihoType == 2 ? FUJYO_KUBUN.老人医療費助成 : FUJYO_KUBUN.Null;
            de.FushoDate1 = app.FushoDate1;
            de.FirstDate1 = app.FushoFirstDate1;
            de.StartDate1 = app.FushoStartDate1;
            de.FinishDate1 = app.FushoFinishDate1;
            de.FushoName1 = app.FushoName1;
            de.FushoDate2 = app.FushoDate2;
            de.FirstDate2 = app.FushoFirstDate2;
            de.StartDate2 = app.FushoStartDate2;
            de.FinishDate2 = app.FushoFinishDate2;
            de.FushoName2 = app.FushoName2;
            de.FushoDate3 = app.FushoDate3;
            de.FirstDate3 = app.FushoFirstDate3;
            de.StartDate3 = app.FushoStartDate3;
            de.FinishDate3 = app.FushoFinishDate3;
            de.FushoName3 = app.FushoName3;
            de.DantaiCode = dantaiCode;
            de.SeiriNo = "";
            de.SickCode = string.Empty;
            de.EntryDate = DateTime.MinValue;
            de.EntryNo = exportNumbeing ? app.Numbering : string.Empty;
            de.Bill = app.Charge;
            de.Ratio = app.Ratio;

            return de;
        }

        public static DaiwaExport CreateTest()
        {
            var de = new DaiwaExport();

            //20190129131502 furukawa st ////////////////////////
            //名古屋港湾用に変更
            de.KumiaiCode = "XL";
            //de.KumiaiCode = "S5";
            //20190129131502 furukawa ed ////////////////////////

            de.ShibuCode = "01";
            de.TokutaiKubun = TOKUTAI_KUBUN.一般;
            de.Mark = "12";
            de.Number = "345678";
            de.Sex = SEX.男;
            de.Birth = new DateTime(1984, 1, 4);
            de.Zoku = "";
            de.Honke = HONKE_KUBUN.本人;
            de.ShinryoYYMM = 2804;
            de.Days = 4;
            de.Total = 8247;
            de.Fujyo = FUJYO_KUBUN.Null;
            de.FushoDate1 = new DateTime(2016, 4, 7);
            de.StartDate1 = new DateTime(2016, 4, 7);
            de.FirstDate1 = new DateTime(2016, 4, 8);
            de.FinishDate1 = new DateTime(2016, 4, 15);
            de.FushoName1 = "左前腕部挫傷";
            de.FushoDate2 = new DateTime(2016, 4, 7);
            de.StartDate2 = new DateTime(2016, 4, 7);
            de.FirstDate2 = new DateTime(2016, 4, 8);
            de.FinishDate2 = new DateTime(2016, 4, 15);
            de.FushoName2 = "腰部捻挫";
            de.FushoDate3 = DateTime.MinValue;
            de.StartDate3 = DateTime.MinValue;
            de.FirstDate3 = DateTime.MinValue;
            de.FinishDate3 = DateTime.MinValue;
            de.FushoName3 = "";
            de.DantaiCode = "4001";
            de.SeiriNo = "";
            de.SickCode = "";
            de.EntryDate = DateTime.MinValue;
            de.EntryNo = "";
            de.Bill = 5772;
            de.BillType = "";
            de.Ratio = 7;

            return de;
        }

        public static bool KikinTypeExport(List<DaiwaExport> list, string dirName, WaitForm wf)
        {
            wf.InvokeValue = 0;

            var info = dirName + "\\0_COMMON001\\00_INFORMATION.CSV";
            var recode = dirName + "\\0_COMMON001\\01_PECULIARTEXTINFO_MED.CSV";
            var img = dirName + "\\1_MEDICAL001\\";

            int numbering = 50000000;

            System.IO.Directory.CreateDirectory(dirName + "\\0_COMMON001");
            System.IO.Directory.CreateDirectory(dirName + "\\1_MEDICAL001");
            var imageNames = new List<string>();

            using (var sw = new System.IO.StreamWriter(recode, true, Encoding.GetEncoding("Shift_JIS")))
            {
                var fc = new TiffUtility.FastCopy();

                int imageNumber = 1;
                for (int i = 0; i < list.Count; i++)
                {
                    //先頭不要等回避
                    for (; i < list.Count && list[i].Ayear < 0; i++) ;

                    var de = list[i];
                    numbering++;

                    //テキストデータ書き出し
                    sw.WriteLine(de.createMN(numbering, imageNumber));
                    sw.WriteLine(de.createSA());
                    sw.WriteLine(de.createSV());
                    sw.WriteLine(de.createGK());
                    sw.WriteLine(de.createRC());
                    imageNames.Add(de.GetImageFullPath());

                    //続紙抽出
                    for (; i < list.Count - 1; i++)
                    {
                        wf.InvokeValue++;
                        if (list[i + 1].Ayear > 0) break;
                        if (list[i + 1].Ayear == (int)APP_SPECIAL_CODE.続紙)
                            imageNames.Add(list[i + 1].GetImageFullPath());
                    }

                    //画像保存
                    int chym = DateTimeEx.GetGyymmFromAdYM(de.ChargeYM);
                    string imageName = dirName + "\\1_MEDICAL001\\" +
                        "011" + chym.ToString("00000") + "6" + imageNumber.ToString("00000000") + ".tif";

                    if (!TiffUtility.MargeOrCopyTiff(fc, imageNames, imageName))
                    {
                        wf.LogPrint($"{imageName}の保存に失敗しました");
                    }
                    imageNames.Clear();
                    imageNumber++;
                }
            }

            int jym = DateTimeEx.GetGyymmFromAdYM(list.Max(item => item.ChargeYM));
            jym--;
            if (jym % 100 == 0) jym -= 88;
            numbering -= 50000000;
            createInfoFile(info, numbering, jym);

            return true;
        }

        private static bool createInfoFile(string fileName, int count, int MediGYYMM)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine($"保険者名,{Insurer.CurrrentInsurer.FormalName}");
                    sw.WriteLine($"保険者番号,{Insurer.CurrrentInsurer.InsNumber}");
                    sw.WriteLine($"診療年月,{MediGYYMM}");
                    sw.WriteLine($"作成年月日,{DateTime.Today.ToString("yyyy/MM/dd")}");
                    sw.WriteLine();
                    sw.WriteLine($"医科 (レセ電/紙レセ/コード/画像) ,0,{count},0,{count}");
                    sw.WriteLine("DPC  (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("歯科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("調剤 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("訪問 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
        
        private string createMN(int numbering, int receNo)
        {
            int chym = DateTimeEx.GetGyymmFromAdYM(ChargeYM);
            var l = new string[15];
            l[0] = "10";
            l[1] = "MN";
            l[2] = numbering.ToString("00000000");


            //20190326155030 furukawa st ////////////////////////
            //新元号対応
            //診療年月は最初から入力されているのでそのままでよいが、頭の元号のみ変える
            //とりあえず平成28年以前の場合は新元号とみなす
            if (ShinryoYYMM<=2800)
            {
                l[3] = "5" + ShinryoYYMM.ToString("0000");
            }
            else
            {
                l[3] = "4" + ShinryoYYMM.ToString("0000");
            }
            //20190326155030 furukawa ed ////////////////////////            


            l[4] = "2";
            l[5] = "01";
            l[6] = "1";
            l[7] = chym.ToString("00000");
            l[8] = "6" + receNo.ToString("00000000");
            l[9] = "011" + chym.ToString("00000") + "6" + receNo.ToString("00000000");
            l[10] = "2";
            l[11] = "0000000";
            l[12] = "";
            l[13] = "";
            l[14] = "";

            return string.Join(",", l);
        }

        private string createSA()
        {
            
            var l = new string[42];
            l[0] = "20";
            l[1] = "SA";
            l[2] = "1";
            l[3] = "1";

            //20200423170301 furukawa st ////////////////////////
            //本人家族を保険者で分岐
            
            switch (Insurer.CurrrentInsurer.InsurerID) {
                case (int)InsurerID.FUTABA:
                    //フタバは206型（本人家族×100+受療者区分0,2,4,6,8)なので変換必要
                    l[4] = app.Family.ToString().Substring(0, 1) == "1" ? "2" : "6";
                    break;
                default:
                    l[4] = app.Family.ToString();
                    break;

            }

            //l[4] = app.Family.ToString();


            //20200423170301 furukawa ed ////////////////////////


            l[5] = "";
            l[6] = "";
            l[7] = Insurer.CurrrentInsurer.InsNumber;
            l[8] = Mark;
            l[9] = Number;
            l[10] = (Ratio * 10).ToString();
            l[11] = "";
            l[12] = "";
            l[13] = "";
            l[14] = "";
            l[15] = "";
            l[16] = "";
            l[17] = "";
            l[18] = "";
            l[19] = "";
            l[20] = "";
            l[21] = "";
            l[22] = ((int)Sex).ToString(); ;
            l[23] = DateTimeEx.GetIntJpDateWithEraNumber(Birth).ToString();
            l[24] = "";
            l[25] = "";
            l[26] = "";
            l[27] = "";
            l[28] = "";
            l[29] = "";
            l[30] = "";
            l[31] = "";
            l[32] = "";
            l[33] = "";
            l[34] = "";
            l[35] = "";
            l[36] = "";
            l[37] = Days.ToString();
            l[38] = "";
            l[39] = "";
            l[40] = "";
            l[41] = "";

            return string.Join(",", l);
        }

        private string createSV()
        {
            int chym = DateTimeEx.GetGyymmFromAdYM(ChargeYM);
            var l = new string[11];
            l[0] = "30";
            l[1] = "SV";
            l[2] = "1";
            l[3] = "0000999";
            l[4] = "";
            l[5] = "";
            l[6] = "1905";
            l[7] = FushoName1;
            l[8] = "";
            l[9] = "";
            l[10] = "1";

            return string.Join(",", l);
        }

        private string createGK()
        {
            int chym = DateTimeEx.GetGyymmFromAdYM(ChargeYM);
            var l = new string[54];
            Array.ForEach(l, item => item = "");
            l[0] = "40";
            l[1] = "GK";
            l[51] = app.Charge.ToString();

            return string.Join(",", l);
        }

        private string createRC()
        {
            int chym = DateTimeEx.GetGyymmFromAdYM(ChargeYM);
            var l = new string[3];
            Array.ForEach(l, item => item = "");
            l[0] = "50";
            l[1] = "RC";
            l[2] = "";

            return string.Join(",", l);
        }

    }
}
