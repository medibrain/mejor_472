﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

public partial class UserControlImage : UserControl
{
    private Form parentForm;

    public UserControlImage()
    {
        InitializeComponent();
        this.Layout += UserControlImage_Layout;
        pictureBox.MouseWheel += ParentForm_MouseWheel;
    }

    private void UserControlImage_Layout(object sender, LayoutEventArgs e)
    {
        if (parentForm != null) return;
        parentForm = FindForm();
        if (parentForm != null) parentForm.MouseWheel += ParentForm_MouseWheel;
    }

    public string ImageLocation { get; private set; }

    /// <summary>
    /// Imageのインスタンスを指定し、画像を表示します
    /// </summary>
    /// <param name="img"></param>
    /// <param name="oldImageDispose">現在表示中のImageを破棄する場合、true</param>
    public void SetImage(Image img, string fileName)
    {
        ImageLocation = fileName;
        var oldImg = pictureBox.Image;
        pictureBox.Image = (Image)img.Clone();
        
        if (oldImg != null) oldImg.Dispose();
    }

    /// <summary>
    /// 画像表示をクリアします
    /// </summary>
    public void Clear()
    {
        ImageLocation = string.Empty;
        var oldImg = pictureBox.Image;
        pictureBox.Image = null;
        if (oldImg != null) oldImg.Dispose();
    }

    /// <summary>
    /// ファイル名を指定し、画像を表示します
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="oldImageDispose"></param>
    public void SetPictureFile(string fileName)
    {
        var oldImg = pictureBox.Image;
        ImageLocation = fileName;
        if (ImageLocation == string.Empty)
        {
            pictureBox.Image = null;
            if (oldImg != null) oldImg.Dispose();
            return;
        }

        using (var fs = new FileStream(
            fileName, FileMode.Open, FileAccess.Read))
        {
            var img = Image.FromStream(fs);
            pictureBox.Image = img;
        }
    }

    /// <summary>表示している画像を９０度回転させる（画像ファイルも回転）</summary>
    /// <param name="clockwise">trueなら時計まわり</param>
    public void ImageRotate(bool clockwise)
    {
        try
        {
            using (var bmp = Image.FromFile(ImageLocation))
            {
                bmp.RotateFlip(clockwise ? RotateFlipType.Rotate90FlipNone : RotateFlipType.Rotate270FlipNone);
                
                var ep = new EncoderParameters(1);
                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                var ici = GetEncoderInfo("image/tiff");
                bmp.Save(ImageLocation, ici, ep);

                SetPictureFile(ImageLocation);
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show("画像の回転に失敗しました\r\n" + ex, "UserControlImage", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
    }

    public void InitPictureBoxLocation()
    {
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    /// <summary> 画像のサイズ・スクロール位置の更新 </summary>
    /// <param name="pictureSize">画像のサイズ</param>
    /// <param name="scroll">スクロール位置</param>
    public void SetPictureBoxLocation(Size pictureSize, Point scroll)
    {
        if (pictureSize.Height <= this.Height && pictureSize.Width <= this.Width)
        {
            // 画像のサイズ ＜ 枠のサイズの時 → 完全に枠の大きさに合わせる！
            this.AutoScrollPosition = new Point(0, 0);
            pictureBox.Size = this.Size;
            pictureBox.Dock = DockStyle.Fill;
        }
        else
        {
            pictureBox.Dock = DockStyle.None;
            pictureBox.Size = pictureSize;
            this.AutoScrollPosition = scroll;
        }
    }

    public void SetPictureBoxFill()
    {
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    public void PictureDispose()
    {
        if (pictureBox.Image == null) return;
        var img = pictureBox.Image;
        pictureBox.Image = null;
        System.Threading.Tasks.Task.Factory.StartNew(() => img.Dispose());
    }

    public void GetPictureBoxLocation(out Size pictureSize, out Point scroll)
    {
        if (pictureBox.Dock == DockStyle.Fill)
        {
            pictureSize = this.Size;
            scroll = new Point(0, 0);
        }
        else
        {
            pictureSize = pictureBox.Size;
            scroll = new Point(-this.AutoScrollPosition.X, -this.AutoScrollPosition.Y);
        }
    }

    private void pictureBox_MouseClick(object sender, MouseEventArgs e)
    {
        if (Math.Abs(dx) > 10 && Math.Abs(dy) > 10) return;
        
        if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
        {
            // 画像サイズ ・ スクロール位置 の取得
            Size pictureSize;       // 画像サイズ
            Point scroll;           // スクロール位置
            GetPictureBoxLocation(out pictureSize, out scroll);

            // 拡大率 （左クリック 1.2倍ズーム、 右 1/1.2 縮小）
            float rate = e.Button == MouseButtons.Left ? 1.2f : 1 / 1.2f;

            // 画像サイズ ・ スクロール位置 の更新
            pictureSize.Width = (int)(rate * pictureSize.Width);
            pictureSize.Height = (int)(rate * pictureSize.Height);
            scroll.X = (int)(e.Location.X);
            scroll.Y = (int)(e.Location.Y);
            SetPictureBoxLocation(pictureSize, scroll);
        }
    }

    private void ParentForm_MouseWheel(object sender, MouseEventArgs e)
    {
        if (pictureBox.Image == null) return;
        if (System.Math.Abs(dx) > 10 && System.Math.Abs(dy) > 10) return;

        //カーソル位置がコントーロール内か
        var mouseClientPos = PointToClient(MousePosition);
        if (!ClientRectangle.Contains(mouseClientPos)) return;
        
        Size pictureSize;       // 画像サイズ
        Point scroll;           // スクロール位置
        GetPictureBoxLocation(out pictureSize, out scroll);
        float rate = e.Delta > 0 ? 1.2f : 1 / 1.2f;

        // 画像サイズ ・ スクロール位置 の更新
        pictureSize.Width = (int)(rate * pictureSize.Width);
        pictureSize.Height = (int)(rate * pictureSize.Height);
        scroll.X = (int)(e.Location.X);
        scroll.Y = (int)(e.Location.Y);
        SetPictureBoxLocation(pictureSize, scroll);
    }


    //MimeTypeで指定されたImageCodecInfoを探して返す
    private static ImageCodecInfo GetEncoderInfo(string mineType)
    {
        //GDI+ に組み込まれたイメージ エンコーダに関する情報をすべて取得
        var encs = ImageCodecInfo.GetImageEncoders();
        //指定されたMimeTypeを探して見つかれば返す
        foreach (ImageCodecInfo enc in encs)
        {
            if (enc.MimeType == mineType) return enc;
        }
        return null;
    }

    //ImageFormatで指定されたImageCodecInfoを探して返す
    private static ImageCodecInfo GetEncoderInfo(ImageFormat f)
    {
        var encs = ImageCodecInfo.GetImageEncoders();
        foreach (ImageCodecInfo enc in encs)
        {
            if (enc.FormatID == f.Guid)
            {
                return enc;
            }
        }
        return null;
    }

    private bool is_mouse_down_ = false;
    private Point origin_;
    int dx = 0;
    int dy = 0;

    private void pictureBox_MouseDown(object sender, MouseEventArgs e)
    {
        origin_ = pictureBox.Parent.PointToScreen(e.Location);
        Cursor.Current = Cursors.Cross; // マウスカーソルの見た目を変更
        is_mouse_down_ = true;
        
        dx = 0;
        dy = 0;
    }

    private void pictureBox_MouseMove(object sender, MouseEventArgs e)
    {
        if (is_mouse_down_)
        {
            var current = pictureBox.PointToScreen(e.Location);
            int x = current.X - origin_.X;
            int y = current.Y - origin_.Y;
            //panel1.AutoScrollPosition = new Point(-x, -y);
            this.AutoScrollPosition = new Point(-x, -y);
            dx = x;
            dy = y;
            Refresh();
        }
    }

    private void pictureBox_MouseUp(object sender, MouseEventArgs e)
    {
        is_mouse_down_ = false;
        Cursor.Current = Cursors.Default;
    }
}
