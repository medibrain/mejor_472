﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Mejor
{
    class GeoData
    {
        private static DB geoDB = new DB("geo");

        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int ID { get; set; }
        public string Pref { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public double Ido { get; set; }
        public double Keido { get; set; }

        public enum ErrorCode
        {
            Error_NoError = -1,
            Error_NoData = -2,
            Error_PatientAddr = -3,
            Error_SejutuAddr = -4,
            Error_BothAddr = -5
        }

        public static bool Import(string fileName)
        {
            var csv = CommonTool.CsvImportShiftJis(fileName);
            var l = new List<GeoData>();

            for (int i = 1; i < csv.Count; i++)
            {
                var line = csv[i];
                if (line.Length < 8) continue;

                var g = new GeoData();
                g.Pref = line[0];
                g.Address = line[1].Trim() + line[2].Trim() + line[3].Trim();
                g.Ido = double.Parse(line[7]);
                g.Keido = double.Parse(line[8]);
                l.Add(g);
            }

            using(var f =new WaitForm())
            using (var tran = geoDB.CreateTransaction())
            {
                f.ShowDialogOtherTask();
                f.SetMax(l.Count);
                f.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                foreach (var item in l)
                {
                    f.InvokeValue++;
                    if (!geoDB.Insert(item, tran)) return false;
                }
                tran.Commit();
            }
            return true;
        }

        public static GeoData GetGeoData(string add)
        {
            var ta = add.Replace("　", "").Replace(" ", "");
            var a = TruncateAddress(ta.Replace("　", ""));
            var g = geoDB.Select<GeoData>(new { Address = a }).FirstOrDefault();
            if (g != null) return g;
            
            var i = ta.IndexOf('－');
            if (i == -1) i = ta.IndexOf('―');
            if (1 < i && i + 1 <= ta.Length)
            {
                a = ta.Remove(i) + "丁目" + ta.Substring(i + 1);
                a = TruncateAddress(a);
                g = geoDB.Select<GeoData>(new { Address = a }).FirstOrDefault();
                if (g != null) return g;

                a = ta.Remove(i) + "丁" + ta.Substring(i + 1);
                a = TruncateAddress(a);
                g = geoDB.Select<GeoData>(new { Address = a }).FirstOrDefault();
            }
            return g;
        }

        public static double CalculateDistance(string clinic, string patient)
        {
            var eCode = ErrorCode.Error_NoError;
            
            //緯度経度情報取得
            var cGeo = GetGeoData(clinic);
            var pGeo = GetGeoData(patient);
            if (cGeo == null) eCode = ErrorCode.Error_SejutuAddr;
            if (pGeo == null) eCode = eCode == ErrorCode.Error_SejutuAddr ? ErrorCode.Error_BothAddr : ErrorCode.Error_PatientAddr;
            if (eCode != ErrorCode.Error_NoError) return (double)eCode;

            // 距離計算
            var distance = Utility.CalcDistance(cGeo.Ido, cGeo.Keido, pGeo.Ido, pGeo.Keido) / 1000;
            Debug.WriteLine("Distance:" + distance);
            return distance;
        }

        public double GetDistance(double ido, double keido)
        {
            // 距離計算
            var distance = Utility.CalcDistance(Ido, Keido, ido, keido) / 1000;
            Debug.WriteLine("Distance:" + distance);
            return distance;

        }


        //20190723191752 furukawa st ////////////////////////
        //往療料チェック時、住所候補出す
        
        public static List<string> GetGeoPref(Insurer _ins)
        {
            
            List<string> retlst = new List<string>();
            StringBuilder sb = new StringBuilder();
            string strPref = string.Empty;

            switch (_ins.InsurerID)
            {
                case (int)InsurerID.OSAKA_KOIKI:
                case (int)InsurerID.KISHIWADA_KOKUHO:

                    strPref = "pref='大阪府'";
                    break;

                //20220516164542 furukawa st ////////////////////////
                //茨城広域追加
                
                case (int)InsurerID.IBARAKI_KOIKI:
                    strPref = "pref in ('茨城県','福島県','千葉県') ";
                    break;
                //20220516164542 furukawa ed ////////////////////////


                default:
                    strPref = "1=1";
                    break;
            }

            sb.AppendFormat("select * from geodata where {0}", strPref);
            var cmd=geoDB.CreateCmd(sb.ToString());
            var lst= cmd.TryExecuteReaderList();

            for(int r=0;r<lst.Count;r++)
            {
                retlst.Add(lst[r][1].ToString()+ lst[r][2].ToString());
            }
            
            return retlst;
        }
        //20190723191752 furukawa ed ////////////////////////



        /// <summary>
        /// 住所を検索用に町番前まで切り詰める
        /// </summary>
        /// <param name="rawAddress"></param>
        /// <returns></returns>
        public static string TruncateAddress(string rawAddress)
        {
            string result = string.Empty;
            const string numeric = "0123456789０１２３４５６７８９";
            const string kanji_numeric = "零一二三四五六七八九零一二三四五六七八九";
            const char cho = '丁';

            if (string.IsNullOrWhiteSpace(rawAddress)) return string.Empty;
            rawAddress = DeletePrefString(rawAddress);

            // 数字＋「丁」の文字を見つけて、その前の数字を漢数字に置換
            for (int i = 3; i < rawAddress.Length - 2; i++)
            {
                if (rawAddress[i] == cho)
                {
                    if (numeric.Contains(rawAddress[i - 1]) && !numeric.Contains(rawAddress[i - 2]))
                    {
                        rawAddress = rawAddress.Remove(i - 1) +
                            kanji_numeric[numeric.IndexOf(rawAddress[i - 1])] +
                            rawAddress.Substring(i);
                        break;
                    }
                }
            }

            //最初の数字部分までを取り出す
            bool exsist = false;
            for (int i = 2; i < rawAddress.Length; i++)
            {
                if (numeric.Contains(rawAddress[i])) exsist = true;
                if (exsist && !numeric.Contains(rawAddress[i]))
                {
                    result = rawAddress.Remove(i);
                    break;
                }
            }

            if (result == string.Empty) result = rawAddress;

            //数字を半角にして返す
            return Utility.NumberToHalf(result);
        }

        public static string DeletePrefString(string Address)
        {
            //20190529171917 furukawa st ////////////////////////
            //長さが足りない場合そのまま
            
            if (Address.Length < 3) return Address;
            //20190529171917 furukawa ed ////////////////////////

            //20190814175022 furukawa st ////////////////////////
            //都道府県が入っていないものはそのまま返す
            
            if (!Regex.IsMatch(Address,"[都道府県]")) return Address;
            //20190814175022 furukawa ed ////////////////////////

            var add3 = Address.Remove(3);

            if (Address.Length <=4) return Address;
            var add4 = Address.Remove(4);

            if (add3 == "大阪府") return Address.Substring(3);
            if (add3 == "京都府") return Address.Substring(3);
            if (add3 == "兵庫県") return Address.Substring(3);
            if (add3 == "奈良県") return Address.Substring(3);
            if (add4 == "和歌山県") return Address.Substring(3);
            if (add3 == "東京都") return Address.Substring(3);
            if (add4 == "神奈川県") return Address.Substring(3);
            return Address;
        }
    }
}
