﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Mejor.YamazakiMazak
{
    class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var rex = new string[21];
                    //先頭行は見出し
                    rex[0] = "No.";
                    rex[1] = "点検月";
                    rex[2] = "通知番号";
                    rex[3] = "記号番号";
                    rex[4] = "被保険者名";
                    rex[5] = "受療者名";
                    rex[6] = "生年月日";
                    rex[7] = "年齢";
                    rex[8] = "郵便番号";
                    rex[9] = "住所";
                    rex[10] = "団体番号";
                    rex[11] = "施術院";
                    rex[12] = "診療年";
                    rex[13] = "診療月";
                    rex[14] = "実日数";
                    rex[15] = "合計";
                    rex[16] = "請求金額";
                    rex[17] = "照会理由";
                    rex[18] = "過誤理由";
                    rex[19] = "再審査理由";
                    rex[20] = "詳細";

                    sw.WriteLine(string.Join(",", rex));

                    var yyMM = 0;
                    if (list != null && list.Count > 0) yyMM = list[0].ChargeYear * 100 + list[0].ChargeMonth;

                    var ss = new List<string>();
                    var count = 1;
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        ss.Add(count.ToString());
                        ss.Add(yyMM.ToString());
                        ss.Add($"{yyMM}-{count.ToString("000")}");
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add($"{DateTimeEx.GetInitialEra(item.Birthday)}{DateTimeEx.GetJpYear(item.Birthday)}.{item.Birthday.ToString("M.d")}");
                        ss.Add(DateTimeEx.GetAge(item.Birthday, DateTime.Today).ToString());
                        ss.Add(item.HihoZip.PadLeft(7, '0').Insert(3, "-"));
                        ss.Add(item.HihoAdd);
                        ss.Add(item.AccountNumber);
                        ss.Add(item.ClinicName);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");
                        ss.Add("\"" + item.KagoReasonStr + "\"");
                        ss.Add("\"" + item.SaishinsaReasonStr + "\"");
                        ss.Add("\"" + item.MemoInspect + "\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
