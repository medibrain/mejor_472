﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Toyonakashi_covid19
{
    class dataexport2
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int exportid { get; set; } = 0;

        public string f001number { get; set; } = string.Empty;                //番号 頭が7で始まる場合は10桁、それ以外は8桁の固定長。（ゼロ埋めあり）;
        public string f002vacc_entrydate1 { get; set; } = string.Empty;       //1回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f003vacc_date1 { get; set; } = string.Empty;            //1回目接種日 YYYYMMDD（Yは西暦）;
        public string f004vacc_ticketnum1 { get; set; } = string.Empty;       //1回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f005vacc_insurercode1 { get; set; } = string.Empty;     //1回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f006vacc_place1 { get; set; } = string.Empty;           //1回目接種会場名 ;
        public string f007vacc_doctor1 { get; set; } = string.Empty;          //1回目接種医師名 ;
        public string f008vacc_maker1 { get; set; } = string.Empty;           //1回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f009vacc_lot1 { get; set; } = string.Empty;             //1回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f010vacc_entrydate2 { get; set; } = string.Empty;       //2回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f011vacc_date2 { get; set; } = string.Empty;            //2回目接種日 YYYYMMDD（Yは西暦）;
        public string f012vacc_ticketnum2 { get; set; } = string.Empty;       //2回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f013vacc_insurercode2 { get; set; } = string.Empty;     //2回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f014vacc_place2 { get; set; } = string.Empty;           //2回目接種会場名 ;
        public string f015vacc_doctor2 { get; set; } = string.Empty;          //2回目接種医師名 ;
        public string f016vacc_maker2 { get; set; } = string.Empty;           //2回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f017vacc_lot2 { get; set; } = string.Empty;             //2回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f018vacc_entrydate3 { get; set; } = string.Empty;       //3回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f019vacc_date3 { get; set; } = string.Empty;            //3回目接種日 YYYYMMDD（Yは西暦）;
        public string f020vacc_ticketnum3 { get; set; } = string.Empty;       //3回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f021vacc_insurercode3 { get; set; } = string.Empty;     //3回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f022vacc_place3 { get; set; } = string.Empty;           //3回目接種会場名 ;
        public string f023vacc_doctor3 { get; set; } = string.Empty;          //3回目接種医師名 ;
        public string f024vacc_maker3 { get; set; } = string.Empty;           //3回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f025vacc_lot3 { get; set; } = string.Empty;             //3回目ワクチンロット番号 数値はゼロ埋めなし;
        //20220816_1 ito st /////
        public string f026vacc_entrydate4 { get; set; } = string.Empty;       //4回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f027vacc_date4 { get; set; } = string.Empty;            //4回目接種日 YYYYMMDD（Yは西暦）;
        public string f028vacc_ticketnum4 { get; set; } = string.Empty;       //4回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f029vacc_insurercode4 { get; set; } = string.Empty;     //4回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f030vacc_place4 { get; set; } = string.Empty;           //4回目接種会場名 ;
        public string f031vacc_doctor4 { get; set; } = string.Empty;          //4回目接種医師名 ;
        public string f032vacc_maker4 { get; set; } = string.Empty;           //4回目ワクチンメーカー;
        public string f033vacc_lot4 { get; set; } = string.Empty;             //4回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f034vacc_entrydate5 { get; set; } = string.Empty;       //5回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f035vacc_date5 { get; set; } = string.Empty;            //5回目接種日 YYYYMMDD（Yは西暦）;
        public string f036vacc_ticketnum5 { get; set; } = string.Empty;       //5回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f037vacc_insurercode5 { get; set; } = string.Empty;     //5回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f038vacc_place5 { get; set; } = string.Empty;           //5回目接種会場名 ;
        public string f039vacc_doctor5 { get; set; } = string.Empty;          //5回目接種医師名 ;
        public string f040vacc_maker5 { get; set; } = string.Empty;           //5回目ワクチンメーカー;
        public string f041vacc_lot5 { get; set; } = string.Empty;             //5回目ワクチンロット番号 数値はゼロ埋めなし;
        //20220816_1 ito ed /////

        public int cym { get; set; } = 0;                                     //メホール請求年月;
        public string sid { get; set; } = string.Empty;                       //医療機関;
        public DateTime importdate { get; set; } = DateTime.MinValue;         //インポート日時;
        public DateTime updatedate { get; set; } = DateTime.MinValue;         //更新日時;
        public int aid1 { get; set; } = 0;                                    //1回目のAID;
        public int aid2 { get; set; } = 0;                                    //2回目のAID;
        public int aid3 { get; set; } = 0;                                    //3回目のAID;
        public int barcode1 { get; set; } = 0;                                //1回目のbarcode;
        public int barcode2 { get; set; } = 0;                                //2回目のbarcode;
        public int barcode3 { get; set; } = 0;                                //3回目のbarcode;
        #endregion


   
        public static List<dataexport2> select (int cym)
        {
            List<dataexport2> lst = new List<dataexport2>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataexport2 where cym={cym} order by exportid");
            //DB.Command cmd = new DB.Command(DB.Main, $"select * from dataexport2 where cym={cym} and updatedate<>'0001-01-01' order by exportid");
            var l = cmd.TryExecuteReaderList();

            foreach (var item in l)
            {
                dataexport2 e = new dataexport2();
                e.exportid = int.Parse(item[0].ToString());

                e.f001number = item[1].ToString();
                e.f002vacc_entrydate1 = item[2].ToString();
                e.f003vacc_date1 = item[3].ToString();
                e.f004vacc_ticketnum1 = item[4].ToString();
                e.f005vacc_insurercode1 = item[5].ToString();
                e.f006vacc_place1 = item[6].ToString();
                e.f007vacc_doctor1 = item[7].ToString();
                e.f008vacc_maker1 = item[8].ToString();
                e.f009vacc_lot1 = item[9].ToString();
                e.f010vacc_entrydate2 = item[10].ToString();
                e.f011vacc_date2 = item[11].ToString();
                e.f012vacc_ticketnum2 = item[12].ToString();
                e.f013vacc_insurercode2 = item[13].ToString();
                e.f014vacc_place2 = item[14].ToString();
                e.f015vacc_doctor2 = item[15].ToString();
                e.f016vacc_maker2 = item[16].ToString();
                e.f017vacc_lot2 = item[17].ToString();
                e.f018vacc_entrydate3 = item[18].ToString();
                e.f019vacc_date3 = item[19].ToString();
                e.f020vacc_ticketnum3 = item[20].ToString();
                e.f021vacc_insurercode3 = item[21].ToString();
                e.f022vacc_place3 = item[22].ToString();
                e.f023vacc_doctor3 = item[23].ToString();
                e.f024vacc_maker3 = item[24].ToString();
                e.f025vacc_lot3 = item[25].ToString();

                //20220816_1 ito st /////
                e.f026vacc_entrydate4 = item[26].ToString();
                e.f027vacc_date4 = item[27].ToString();
                e.f028vacc_ticketnum4 = item[28].ToString();
                e.f029vacc_insurercode4 = item[29].ToString();
                e.f030vacc_place4 = item[30].ToString();
                e.f031vacc_doctor4 = item[31].ToString();
                e.f032vacc_maker4 = item[32].ToString();
                e.f033vacc_lot4 = item[33].ToString();
                e.f034vacc_entrydate5 = item[34].ToString();
                e.f035vacc_date5 = item[35].ToString();
                e.f036vacc_ticketnum5 = item[36].ToString();
                e.f037vacc_insurercode5 = item[37].ToString();
                e.f038vacc_place5 = item[38].ToString();
                e.f039vacc_doctor5 = item[39].ToString();
                e.f040vacc_maker5 = item[40].ToString();
                e.f041vacc_lot5 = item[41].ToString();
                //20220816_1 ito ed /////

                lst.Add(e);
            }
            cmd.Dispose();
            return lst;
        }




    }
}
