﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Toyonakashi_covid19

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        /// <summary>
        /// ヘッダの豊中市かどうか
        /// </summary>
        private bool WithoutToyonakashi = false;

        /// <summary>
        /// ヘッダのバッチ
        /// </summary>
        private string strHeaderBatchNo = string.Empty;


        /// <summary>
        /// ヘッダにしかないNoB
        /// </summary>
        private string strHeaderNoB = string.Empty;

        /// <summary>
        /// ヘッダのAID
        /// </summary>
        private int intHeaederAID = 0;

        /// <summary>
        /// 入力すべき予診票の枚数
        /// </summary>
        private int intHeaderPages = 0;

        /// <summary>
        /// 入力した予診票の枚数
        /// </summary>
        private int intPagesCount = 0;

        /// <summary>
        /// この束の医療機関情報
        /// </summary>
        private chargelist HosInfo = null;

        /// <summary>
        /// 医療機関情報がない場合で医療機関等コードだけがある場合に持たせる
        /// </summary>
        private string strHeaderHosNumber = string.Empty;


        /// <summary>
        /// 一つ前の予診票の医療機関等コード
        /// </summary>
        private string strPrevHosNumber = string.Empty;

        /// <summary>
        /// ヘッダの時間外等
        /// </summary>
        private string strHeaderZikangai = string.Empty;

        /// <summary>
        /// 一つ前の予診票の時間外
        /// </summary>
        private string strPrevZikangai = string.Empty;

        /// <summary>
        /// この束の年月　ヘッダにしかないので変数に持たせる
        /// </summary>
        private int intY = 0;
        private int intM = 0;

        /// <summary>
        /// 時間外
        /// </summary>
        private string Zikan = string.Empty;

        /// <summary>
        /// マッチングした提供データ(予診票)
        /// </summary>
        private dataimport dataimport = null;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100


        Point posTicket = new Point(1800, 0);
        Point pos2w = new Point(1600, 1900);
        Point posLot = new Point(0, 2600);
        Point posPlace = new Point(800, 2600);

        Point posYM = new Point(0, 300);
        Point posHyo = new Point(0, 1200);//ヘッダの表の位置
        Point posNOB = new Point(0, 0);//NoB類の位置

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ticketConts, twoweekConts, lotConts, placeConts,hyoConts,ymConts,NoBConts;

        /// <summary>
        /// 提供データリスト
        /// </summary>
        List<dataimport> lstData = new List<dataimport>();
        List<dataimport_chargelist> lstCharge = new List<dataimport_chargelist>();

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
          
            ticketConts = new Control[] { verifyBoxTicket, verifyBoxKenshu, verifyBoxTimes, verifyBoxInsNum, verifyBoxTaion, };
            twoweekConts = new Control[] { verifyBox2w, verifyBoxPossible, verifyBoxDrSign, verifyBoxZikangai, verifyBox6down, verifyBoxKibou, verifyBoxDateSign, verifyBoxSign, };
            lotConts = new Control[] { verifyBoxMaker, verifyBoxLot, };
            placeConts=new Control[] { verifyBoxPlace, verifyBoxDr, verifyBoxSesshuY, verifyBoxSesshuM, verifyBoxSesshuD,verifyBoxHos };
            ymConts = new Control[] { panelym };
            hyoConts = new Control[] { verifyBoxS6D, verifyBoxS6DK, verifyBoxS6U, verifyBoxS6UK, verifyBoxY6D, verifyBoxY6DK, verifyBoxY6U, verifyBoxY6UK };
            NoBConts = new Control[] { verifyBoxNOB };
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            
            //初回表示時
            if (!InitControl(app, verifyBoxKind)) return;

     
            if (app != null)
            {   
                setApp(app);
            }

            
            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 券番号を入れたら券種＋回数＋保険者番号＋券番号をバーコードに入れる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxTicket_Validated(object sender, EventArgs e)
        {
            if (verifyBoxTicket.Text.Length != 10)
            {
                MessageBox.Show("10桁で入力してください",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                verifyBoxTicket.Focus();
                return;
            }

            if (verifyBoxKenshu.Text == string.Empty) return;
            if (verifyBoxTimes.Text == string.Empty) return;
            if (verifyBoxInsNum.Text == string.Empty) return;
            if (verifyBoxTicket.Text == string.Empty) return;

            //if (verifyBoxBarCode.Text == string.Empty)
            //{
                verifyBoxBarCode.Text =
                verifyBoxKenshu.Text.Trim() +
                verifyBoxTimes.Text.Trim() +
                verifyBoxInsNum.Text.Trim() +
                verifyBoxTicket.Text.Trim();
            // }

            
            var app = (App)bsApp.Current;
            setMatchingValues(verifyBoxBarCode.Text.Trim(),app.CYM);
            //setMatchingValues(verifyBoxBarCode.Text.Trim()); 

            //コントロールが空欄の場合は使えるようにする
            DisableEmptyControl();

            setOCRData(app);
        }



        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);            
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
 
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {      
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 種類ボックスへの入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxKind_TextChanged(object sender, EventArgs e)
        {


            App app = (App)bsApp.Current;

            InitControl(app, verifyBoxKind);

            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxKind, labelInputerName, };
            
            panelym.Visible = false;
            //pDoui.Visible = false;

            panelHeader.Visible = false;
            panelBarcode.Visible = false;

            switch (verifyBoxKind.Text)
            {
                case clsInputKind.長期://ヘッダ

                    //ヘッダ用パネル表示
                    panelym.Visible = true;
                    panelHeader.Visible = true;
                    panelSID.Visible = false;
                    break;

                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelSID.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelSID.Visible = false;                    
                    break;
                    
                    
                default://予診票
                    panelym.Visible = false;//ヘッダから取得するのみ、入力しない
                    panelBarcode.Visible = true;
                    panelSID.Visible = true;
                    panelHeader.Visible = false;
                 
                    break;
            }


        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void verifyBoxKind_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            InitControl(app, verifyBoxKind);
        }

        #endregion


        /// <summary>
        /// センシティブ制御 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="verifyboxKind"></param>
        /// <returns></returns>
        private bool InitControl(App app, VerifyBox verifyboxKind)
        {

            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxKind, labelInputerName, };


            panelym.Visible = false;          
            panelSID.Visible = false;
            panelHeader.Visible = false;
            dgv.Visible = false;
         

            //AppがNull（入力前）のときはscanのを採用
            APP_TYPE type = app.AppType == APP_TYPE.NULL ? scan.AppType : app.AppType;

       
            //未入力・入力後表示前
            if (verifyboxKind.Text.Trim() == string.Empty)
            {
                switch (type)
                {
                    case APP_TYPE.長期:
                        panelym.Visible = true;
                        panelHeader.Visible = true;
                        panelSID.Visible = false;
                        break;
                    case APP_TYPE.続紙:
                    case APP_TYPE.不要:
                    case APP_TYPE.エラー:
                    case APP_TYPE.同意書裏:
                    case APP_TYPE.施術報告書:
                    case APP_TYPE.状態記入書:
                        break;

                    case APP_TYPE.同意書:
                      
                        break;

                    case APP_TYPE.柔整:
                        panelym.Visible = false;
                        panelHeader.Visible = false;
                        panelSID.Visible = true;


                        break;

                    default:
                        break;
                }
            }

            //登録後、再表示時
            else
            {

                switch (verifyboxKind.Text.Trim())
                {
                    case clsInputKind.長期:
                        panelym.Visible = true;
                        panelHeader.Visible = true;
                        break;


                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.状態記入書:
                        break;

                    case clsInputKind.施術同意書:                     
                        break;

                    default:


                        switch (type)
                        {
                            case APP_TYPE.柔整:
                                panelym.Visible = true;
                          
                                panelSID.Visible = true;
                                panelHeader.Visible = false;

                                break;

                       
                            default:
                                break;
                        }

                        break;
                }
            }

            return true;
        }


    
        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            //pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            InitControl();

            
            
            //先にバーコードをセット
            string strBarcode = setBarCode(app);

            //TabStop制御
            TabStopControl(strBarcode);

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {


                //20220127132639 furukawa st ////////////////////////
                //タブストップ判定をベリファイ時にも入れるため上に移動

                ////先にバーコードをセット
                //string strBarcode = setBarCode(app);

                ////TabStop制御
                //TabStopControl(strBarcode);
                //20220127132639 furukawa ed ////////////////////////


                //バーコードからマッチングデータを先に表示
                if (strBarcode != string.Empty)
                {
                    setMatchingValues(strBarcode,app.CYM);
                    //setMatchingValues(strBarcode);

                    //コントロールが空欄の場合は使えるようにする
                    DisableEmptyControl();
                }


                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                       
                        //OCR結果を自動入力
                        setOCRData(app);
                      
                    }
                }
            }

            //202208091450 ito st //////////
            //過去分入力
            if (scanGroup.CYM <= 202112)
            {
                KakoNyuryoku();
                app.DrNum = scan.Note1;
            }
            //医療機関コード不要<森安さん
            verifyBoxHos.Text = "0";
            verifyBoxHos.Enabled = false;
            //金額不要<森安さん
            verifyBoxY6DK.Text = "0";
            verifyBoxY6UK.Text = "0";
            verifyBoxS6DK.Text = "0";
            verifyBoxS6UK.Text = "0";
            verifyBoxY6DK.Enabled = false;
            verifyBoxY6UK.Enabled = false;
            verifyBoxS6DK.Enabled = false;
            verifyBoxS6UK.Enabled = false;
            //202208091450 ito ed //////////

            //画像の表示
            setImage(app);
            changedReset(app);


            //豊中市以外の場合は請求先保険者を変更させない
            verifyBoxInsNum.Visible = !WithoutToyonakashi;
            labelInsNum.Visible = !WithoutToyonakashi;
        }


        //202208091450 ito st //////////
        //過去分入力
        private void KakoNyuryoku()
        {
            verifyBoxTaion.Text = "0";
            verifyBox2w.Text = "0";
            verifyBoxPossible.Text = "0";
            verifyBoxDrSign.Text = "0";
            verifyBoxZikangai.Text = "0";
            verifyBox6down.Text = "0";
            verifyBoxKibou.Text = "0";
            verifyBoxDateSign.Text = "0";
            verifyBoxSign.Text = "0";

            verifyBoxZikan.Text = "1";
            verifyBoxY6D.Text = "0";
            verifyBoxY6U.Text = "0";
            verifyBoxS6D.Text = "0";
            verifyBoxS6U.Text = "0";

            verifyBoxTaion.Enabled      = false;
            verifyBox2w.Enabled         = false;
            verifyBoxPossible.Enabled   = false;
            verifyBoxDrSign.Enabled     = false;
            verifyBoxZikangai.Enabled   = false;
            verifyBox6down.Enabled      = false;
            verifyBoxKibou.Enabled      = false;
            verifyBoxDateSign.Enabled   = false;
            verifyBoxSign.Enabled       = false;
            
            verifyBoxZikan.Enabled = false;
            verifyBoxY6D  .Enabled = false;
            verifyBoxY6U  .Enabled = false;
            verifyBoxS6D  .Enabled = false;
            verifyBoxS6U  .Enabled = false;
        }
        //202208091450 ito ed //////////


        private void InitControl()
        {
            verifyBoxMaker.Enabled 			=true;
            verifyBoxLot.Enabled 			=true;
            verifyBoxPlace.Enabled 			=true;
            verifyBoxDr.Enabled 			=true;
                                            
            verifyBoxSesshuY.Enabled 		=true;
            verifyBoxSesshuM.Enabled 		=true;
            verifyBoxSesshuD.Enabled 		=true;
            verifyBoxTicket.Enabled 		=true;
            verifyBoxInsNum.Enabled         =true;

        }


        /// <summary>
        /// バーコードでタブストップ判定
        /// </summary>
        /// <param name="strBarCode"></param>
        private void TabStopControl(string strBarCode)
        {

            bool flgTabStopTrue = false;

            //バーコードが正しくないと思われる場合、手入力の必要があるのでコントロールを全てTabStopさせる
            //バーコードの値は数字18桁のみ
            if (strBarCode == string.Empty) flgTabStopTrue = true;

            //20220210132626 furukawa st ////////////////////////
            //バーコード値が18桁の場合はタブストップさせない、それ以外はさせる

            if (System.Text.RegularExpressions.Regex.IsMatch(strBarCode, "^[0-9]{18}$")) flgTabStopTrue = false;
            else flgTabStopTrue = true;
            //      if (System.Text.RegularExpressions.Regex.IsMatch(strBarCode, "!^[0-9]{18}$")) flgTabStopTrue = true;
            //20220210132626 furukawa ed ////////////////////////

            verifyBoxSesshuY.TabStop = flgTabStopTrue;
            verifyBoxSesshuM.TabStop = flgTabStopTrue;
            verifyBoxSesshuD.TabStop = flgTabStopTrue;

            verifyBoxTicket.TabStop = flgTabStopTrue;
            verifyBoxInsNum.TabStop = flgTabStopTrue;
            verifyBoxPlace.TabStop = flgTabStopTrue;
            verifyBoxDr.TabStop = flgTabStopTrue;
            verifyBoxLot.TabStop = flgTabStopTrue;
            verifyBoxMaker.TabStop = flgTabStopTrue;
         
            

        }

        private void DisableEmptyControl()
        {

            //データが取れた場合、触れないようにする。2022/01/20森安さん・入力チーム
            //verifyBoxMaker.Enabled = verifyBoxMaker.Text.Trim() == string.Empty;
            //verifyBoxLot.Enabled = verifyBoxLot.Text.Trim() == string.Empty;
            //verifyBoxPlace.Enabled = verifyBoxPlace.Text.Trim() == string.Empty;
            //verifyBoxDr.Enabled = verifyBoxDr.Text.Trim() == string.Empty;

            //verifyBoxSesshuY.Enabled = verifyBoxSesshuY.Text.Trim() == string.Empty;
            //verifyBoxSesshuM.Enabled = verifyBoxSesshuM.Text.Trim() == string.Empty;
            //verifyBoxSesshuD.Enabled = verifyBoxSesshuD.Text.Trim() == string.Empty;
            //verifyBoxTicket.Enabled = verifyBoxTicket.Text.Trim() == string.Empty;
            //verifyBoxInsNum.Enabled = verifyBoxInsNum.Text.Trim() == string.Empty;


            //こっちでいく
            ////データが取れた場合、タブストップ無し、背景グレーにして基本触らないように。必要な際はマウスで選択すれば変更可能
            if (verifyBoxMaker.Text.Trim() != string.Empty) { verifyBoxMaker.TabStop = false; verifyBoxMaker.BackColor = SystemColors.Control; }
            if (verifyBoxLot.Text.Trim() != string.Empty) { verifyBoxLot.TabStop = false; verifyBoxLot.BackColor = SystemColors.Control; }
            if (verifyBoxPlace.Text.Trim() != string.Empty) { verifyBoxPlace.TabStop = false; verifyBoxPlace.BackColor = SystemColors.Control; }
            if (verifyBoxDr.Text.Trim() != string.Empty) { verifyBoxDr.TabStop = false; verifyBoxDr.BackColor = SystemColors.Control; }

            if (verifyBoxSesshuY.Text.Trim() != string.Empty) { verifyBoxSesshuY.TabStop = false; verifyBoxSesshuY.BackColor = SystemColors.Control; }
            if (verifyBoxSesshuM.Text.Trim() != string.Empty) { verifyBoxSesshuM.TabStop = false; verifyBoxSesshuM.BackColor = SystemColors.Control; }
            if (verifyBoxSesshuD.Text.Trim() != string.Empty) { verifyBoxSesshuD.TabStop = false; verifyBoxSesshuD.BackColor = SystemColors.Control; }
            //if (verifyBoxTicket.Text.Trim() != string.Empty) { verifyBoxTicket.TabStop = false; verifyBoxTicket.BackColor = SystemColors.Control; }
            //if (verifyBoxInsNum.Text.Trim() != string.Empty) { verifyBoxInsNum.TabStop = false; verifyBoxInsNum.BackColor = SystemColors.Control; }






        }

        /// <summary>
        /// 提供データにマッチするバーコード番号の予診票をロード
        /// </summary>
        /// <param name="strBarCode"></param>
        private void setMatchingValues(string strBarCode,int cym,string strTicketNum="",string strTimes="")
        //private void setMatchingValues(string strBarCode,string strTicketNum="",string strTimes="") 
        {

            if (strBarCode.Length != 18) return;//間違って読んでいる場合等、券番号が取れない場合

            if (strBarCode != string.Empty && strBarCode.Length==18)
            {
                strTicketNum = strBarCode.Substring(8, 10);
                //strTimes = strBarCode.Substring(1, 1);           
                strTimes = verifyBoxTimes.Text.Trim();
            }
         
            else if(strBarCode==string.Empty && strTicketNum == string.Empty)
            {
                return;
            }
            
            dataimport d = new dataimport();

            //頭が7の場合は10桁で比較
            //提供データの券番号は7桁になっているが、券番号自体はゼロ埋め10桁なので10-7+1(ゼロ開始分）からの文字と比較
            if (strTicketNum.Substring(0, 1) == "7") d = dataimport.select(strTicketNum,cym);
            else d = dataimport.select(strTicketNum.Substring(10 - (7 + 1)),cym);
            //if (strTicketNum.Substring(0, 1) == "7") d = dataimport.select(strTicketNum);
            //else d = dataimport.select(strTicketNum.Substring(10 - (7 + 1)));


            if (d == null) return;
            else
            {

                labelMacthCheck.Visible = true;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.BackColor = Color.Cyan;

            }

            dataimport = d;//この帳票のデータを控えておく

            //20221026_2 ito st ///// 接種回数が増えるたびに記述が冗長になっていくので改造
            //回数で参照フィールドをわける
            string vacc_date = "";
            string vacc_ticketnum = "";
            string vacc_insurercode = "";
            string vacc_place = "";
            string vacc_doctor = "";
            string vacc_lot = "";
            string vacc_maker = "";


            switch (strTimes)
            {
                case "1":
                    vacc_date = d.f003vacc_date1;
                    vacc_ticketnum = d.f004vacc_ticketnum1;
                    vacc_insurercode = d.f005vacc_insurercode1;
                    vacc_place = d.f006vacc_place1;
                    vacc_doctor = d.f007vacc_doctor1;
                    vacc_maker = d.f008vacc_maker1;
                    vacc_lot = d.f009vacc_lot1;
                    break;

                case "2":
                    vacc_date = d.f011vacc_date2;
                    vacc_ticketnum = d.f012vacc_ticketnum2;
                    vacc_insurercode = d.f013vacc_insurercode2;
                    vacc_place = d.f014vacc_place2;
                    vacc_doctor = d.f015vacc_doctor2;
                    vacc_maker = d.f016vacc_maker2;
                    vacc_lot = d.f017vacc_lot2;
                    break;

                case "3":
                    vacc_date = d.f019vacc_date3;
                    vacc_ticketnum = d.f020vacc_ticketnum3;
                    vacc_insurercode = d.f021vacc_insurercode3;
                    vacc_place = d.f022vacc_place3;
                    vacc_doctor = d.f023vacc_doctor3;
                    vacc_maker = d.f024vacc_maker3;
                    vacc_lot = d.f025vacc_lot3;
                    break;

                case "4":
                    vacc_date = d.f027vacc_date4;
                    vacc_ticketnum = d.f028vacc_ticketnum4;
                    vacc_insurercode = d.f029vacc_insurercode4;
                    vacc_place = d.f030vacc_place4;
                    vacc_doctor = d.f031vacc_doctor4;
                    vacc_maker = d.f032vacc_maker4;
                    vacc_lot = d.f033vacc_lot4;
                    break;

                case "5":
                    vacc_date = d.f035vacc_date5;
                    vacc_ticketnum = d.f036vacc_ticketnum5;
                    vacc_insurercode = d.f037vacc_insurercode5;
                    vacc_place = d.f038vacc_place5;
                    vacc_doctor = d.f039vacc_doctor5;
                    vacc_maker = d.f040vacc_maker5;
                    vacc_lot = d.f041vacc_lot5;
                    break;
            }


            if (vacc_date != string.Empty) verifyBoxSesshuY.Text = vacc_date.Substring(0, 4);
            if (vacc_date != string.Empty) verifyBoxSesshuM.Text = vacc_date.Substring(4, 2);
            if (vacc_date != string.Empty) verifyBoxSesshuD.Text = vacc_date.Substring(6, 2);
            if (vacc_ticketnum.Trim() != string.Empty) verifyBoxTicket.Text = vacc_ticketnum.Trim();
            if (vacc_insurercode.Trim() != string.Empty) verifyBoxInsNum.Text = vacc_insurercode.Trim();
            if (vacc_place.Trim() != string.Empty) verifyBoxPlace.Text = vacc_place.Trim();
            if (vacc_doctor.Trim() != string.Empty) verifyBoxDr.Text = vacc_doctor.Trim();
            if (vacc_lot.Trim() != string.Empty && verifyBoxLot.Text == string.Empty) verifyBoxLot.Text = vacc_lot.Trim();

            //メーカーも選択にするため分岐
            if (vacc_maker.Trim() != string.Empty && verifyBoxMaker.Text == string.Empty)
            {
                switch (vacc_maker.Trim())
                {
                    case "ファイザー":
                        verifyBoxMaker.Text = "1";
                        break;
                    case "アストラゼネカ":
                        verifyBoxMaker.Text = "2";
                        break;
                    case "武田／モデルナ":
                        verifyBoxMaker.Text = "3";
                        break;
                    case "ファイザー（５から１１歳用）":
                        verifyBoxMaker.Text = "4";
                        break;
                    case "ノババックス":
                        verifyBoxMaker.Text = "5";
                        break;
                    case "コミナティ（２価：ＢＡ．１）":
                        verifyBoxMaker.Text = "6";
                        break;
                    case "スパイクバックス（２価：ＢＡ．１）":
                        verifyBoxMaker.Text = "7";
                        break;
                    case "コミナティ（２価：ＢＡ．４／５）":
                        verifyBoxMaker.Text = "8";
                        break;
                    case "コミナティ（６か月から４歳用）":
                        verifyBoxMaker.Text = "9";
                        break;
                    case "スパイクバックス（2価：ＢＡ．４／５）":
                        verifyBoxMaker.Text = "10";
                        break;

                }
            }

            //提供データに何も入ってない場合は入力するのでTabStopを有効に
            if (vacc_date != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
            if (vacc_date != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
            if (vacc_date != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
            if (vacc_ticketnum.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
            if (vacc_insurercode.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
            if (vacc_place.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
            if (vacc_doctor.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
            if (vacc_maker.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
            if (vacc_lot.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;

            //20221026_2 ito end /////

            /*
            //回数で参照フィールドをわける
            switch (strTimes)
            {
                case "1":
                    //20220127093704 furukawa st ////////////////////////
                    //不要（南さん

                    //if (d.f004vacc_ticketnum1.Trim() == string.Empty)
                    //{
                    //    MessageBox.Show("回数が間違っている可能性があります");
                    //    return;
                    //}

                    //20220127093704 furukawa ed ////////////////////////


                    //20220127103459 furukawa st ////////////////////////
                    //その回が新規入力の場合、提供データに何も入ってないので空文字が入ってしまうのを防ぐ
                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuY.Text = d.f003vacc_date1.Substring(0, 4);
                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuM.Text = d.f003vacc_date1.Substring(4, 2);
                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuD.Text = d.f003vacc_date1.Substring(6, 2);
                    if (d.f004vacc_ticketnum1.Trim() != string.Empty) verifyBoxTicket.Text = d.f004vacc_ticketnum1.Trim();
                    if (d.f005vacc_insurercode1.Trim() != string.Empty) verifyBoxInsNum.Text = d.f005vacc_insurercode1.Trim();
                    if (d.f006vacc_place1.Trim() != string.Empty) verifyBoxPlace.Text = d.f006vacc_place1.Trim();
                    if (d.f007vacc_doctor1.Trim() != string.Empty) verifyBoxDr.Text = d.f007vacc_doctor1.Trim();
                    if (d.f009vacc_lot1.Trim() != string.Empty) verifyBoxLot.Text = d.f009vacc_lot1.Trim();
                    //      verifyBoxSesshuY.Text = d.f003vacc_date1 != string.Empty ? d.f003vacc_date1.Substring(0, 4) : string.Empty;
                    //      verifyBoxSesshuM.Text = d.f003vacc_date1 != string.Empty ? d.f003vacc_date1.Substring(4, 2) : string.Empty;
                    //      verifyBoxSesshuD.Text = d.f003vacc_date1 != string.Empty ? d.f003vacc_date1.Substring(6, 2) : string.Empty;
                    //      verifyBoxTicket.Text = d.f004vacc_ticketnum1.Trim();
                    //      verifyBoxInsNum.Text = d.f005vacc_insurercode1.Trim();
                    //      verifyBoxPlace.Text = d.f006vacc_place1.Trim();
                    //      verifyBoxDr.Text = d.f007vacc_doctor1.Trim();
                    //      verifyBoxLot.Text = d.f009vacc_lot1.Trim();
                    //20220127103459 furukawa ed ////////////////////////


                    //メーカーも選択にするため分岐
                    switch (d.f008vacc_maker1.Trim()) {
                        case "ファイザー":
                            verifyBoxMaker.Text = "1";
                            break;
                        case "アストラゼネカ":
                            verifyBoxMaker.Text = "2";
                            break;
                        case "武田／モデルナ":
                            verifyBoxMaker.Text = "3";
                            break;
                        //20220823_1 ito st //////////
                        case "ファイザー（５から１１歳用）":
                            verifyBoxMaker.Text = "4";
                            break;
                        case "ノババックス":
                            verifyBoxMaker.Text = "5";
                            break;
                        //20220823_1 ito ed //////////
                        //20220913_2 ito st //////////
                        case "コミナティ（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "6";
                            break;
                        case "スパイクバックス（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "7";
                            break;
                        //20220913_2 ito ed //////////
                        //20221014_1 ito st //////////
                        case "コミナティ（２価：ＢＡ．４／５）":
                            verifyBoxMaker.Text = "8";
                            break;
                        //20221014_1 ito ed //////////
                    }

                    //20220127111204 furukawa st ////////////////////////
                    //提供データに何も入ってない場合は入力するのでTabStopを有効に

                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
                    if (d.f003vacc_date1 != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
                    if (d.f004vacc_ticketnum1.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
                    if (d.f005vacc_insurercode1.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
                    if (d.f006vacc_place1.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
                    if (d.f007vacc_doctor1.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
                    if (d.f009vacc_lot1.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;
                    if (d.f008vacc_maker1.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
                    //20220127111204 furukawa ed ////////////////////////

                    break;
                        
                case "2":
                    //その回が新規入力の場合、提供データに何も入ってないので空文字が入ってしまうのを防ぐ                    
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuY.Text = d.f011vacc_date2.Substring(0, 4);
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuM.Text = d.f011vacc_date2.Substring(4, 2);
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuD.Text = d.f011vacc_date2.Substring(6, 2);
                    if (d.f012vacc_ticketnum2.Trim() != string.Empty) verifyBoxTicket.Text = d.f012vacc_ticketnum2.Trim();
                    if (d.f013vacc_insurercode2.Trim() != string.Empty) verifyBoxInsNum.Text = d.f013vacc_insurercode2.Trim();
                    if (d.f014vacc_place2.Trim() != string.Empty) verifyBoxPlace.Text = d.f014vacc_place2.Trim();
                    if (d.f015vacc_doctor2.Trim() != string.Empty) verifyBoxDr.Text = d.f015vacc_doctor2.Trim();
                    if (d.f017vacc_lot2.Trim() != string.Empty) verifyBoxLot.Text = d.f017vacc_lot2.Trim();

                    switch (d.f016vacc_maker2.Trim())
                    {
                        case "ファイザー":
                            verifyBoxMaker.Text = "1";
                            break;
                        case "アストラゼネカ":
                            verifyBoxMaker.Text = "2";
                            break;
                        case "武田／モデルナ":
                            verifyBoxMaker.Text = "3";
                            break;
                        //20220823_1 ito st //////////
                        case "ファイザー（５から１１歳用）":
                            verifyBoxMaker.Text = "4";
                            break;
                        case "ノババックス":
                            verifyBoxMaker.Text = "5";
                            break;
                        //20220823_1 ito ed //////////
                        //20220913_2 ito st //////////
                        case "コミナティ（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "6";
                            break;
                        case "スパイクバックス（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "7";
                            break;
                        //20220913_2 ito ed //////////
                        //20221014_1 ito st //////////
                        case "コミナティ（２価：ＢＡ．４／５）":
                            verifyBoxMaker.Text = "8";
                            break;
                        //20221014_1 ito ed //////////

                    }

                    //提供データに何も入ってない場合は入力するのでTabStopを有効に
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
                    if (d.f011vacc_date2 != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
                    if (d.f012vacc_ticketnum2.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
                    if (d.f013vacc_insurercode2.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
                    if (d.f014vacc_place2.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
                    if (d.f015vacc_doctor2.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
                    if (d.f016vacc_maker2.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
                    if (d.f017vacc_lot2.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;

                    break;

                case "3":
                    //その回が新規入力の場合、提供データに何も入ってないので空文字が入ってしまうのを防ぐ                    
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuY.Text = d.f019vacc_date3.Substring(0, 4);
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuM.Text = d.f019vacc_date3.Substring(4, 2);
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuD.Text = d.f019vacc_date3.Substring(6, 2);
                    if (d.f020vacc_ticketnum3.Trim() != string.Empty) verifyBoxTicket.Text = d.f020vacc_ticketnum3.Trim();
                    if (d.f021vacc_insurercode3.Trim() != string.Empty) verifyBoxInsNum.Text = d.f021vacc_insurercode3.Trim();
                    if (d.f022vacc_place3.Trim() != string.Empty) verifyBoxPlace.Text = d.f022vacc_place3.Trim();
                    if (d.f023vacc_doctor3.Trim() != string.Empty) verifyBoxDr.Text = d.f023vacc_doctor3.Trim();
                    if (d.f025vacc_lot3.Trim() != string.Empty) verifyBoxLot.Text = d.f025vacc_lot3.Trim();

                    switch (d.f024vacc_maker3.Trim())
                    {
                        case "ファイザー":
                            verifyBoxMaker.Text = "1";
                            break;
                        case "アストラゼネカ":
                            verifyBoxMaker.Text = "2";
                            break;
                        case "武田／モデルナ":
                            verifyBoxMaker.Text = "3";
                            break;
                        //20220823_1 ito st //////////
                        case "ファイザー（５から１１歳用）":
                            verifyBoxMaker.Text = "4";
                            break;
                        case "ノババックス":
                            verifyBoxMaker.Text = "5";
                            break;
                        //20220823_1 ito ed //////////
                        //20220913_2 ito st //////////
                        case "コミナティ（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "6";
                            break;
                        case "スパイクバックス（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "7";
                            break;
                        //20220913_2 ito ed //////////
                        //20221014_1 ito st //////////
                        case "コミナティ（２価：ＢＡ．４／５）":
                            verifyBoxMaker.Text = "8";
                            break;
                        //20221014_1 ito ed //////////

                    }

                    //提供データに何も入ってない場合は入力するのでTabStopを有効に
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
                    if (d.f019vacc_date3 != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
                    if (d.f020vacc_ticketnum3.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
                    if (d.f021vacc_insurercode3.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
                    if (d.f022vacc_place3.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
                    if (d.f023vacc_doctor3.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
                    if (d.f024vacc_maker3.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
                    if (d.f025vacc_lot3.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;

                    break;

                //20220823_1 ito st //////////
                case "4":
                    //その回が新規入力の場合、提供データに何も入ってないので空文字が入ってしまうのを防ぐ                    
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuY.Text = d.f027vacc_date4.Substring(0, 4);
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuM.Text = d.f027vacc_date4.Substring(4, 2);
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuD.Text = d.f027vacc_date4.Substring(6, 2);
                    if (d.f028vacc_ticketnum4.Trim() != string.Empty) verifyBoxTicket.Text = d.f028vacc_ticketnum4.Trim();
                    if (d.f029vacc_insurercode4.Trim() != string.Empty) verifyBoxInsNum.Text = d.f029vacc_insurercode4.Trim();
                    if (d.f030vacc_place4.Trim() != string.Empty) verifyBoxPlace.Text = d.f030vacc_place4.Trim();
                    if (d.f031vacc_doctor4.Trim() != string.Empty) verifyBoxDr.Text = d.f031vacc_doctor4.Trim();
                    if (d.f033vacc_lot4.Trim() != string.Empty) verifyBoxLot.Text = d.f033vacc_lot4.Trim();

                    switch (d.f032vacc_maker4.Trim())
                    {
                        case "ファイザー":
                            verifyBoxMaker.Text = "1";
                            break;
                        case "アストラゼネカ":
                            verifyBoxMaker.Text = "2";
                            break;
                        case "武田／モデルナ":
                            verifyBoxMaker.Text = "3";
                            break;
                        case "ファイザー（５から１１歳用）":
                            verifyBoxMaker.Text = "4";
                            break;
                        case "ノババックス":
                            verifyBoxMaker.Text = "5";
                            break;
                        //20220913_2 ito st //////////
                        case "コミナティ（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "6";
                            break;
                        case "スパイクバックス（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "7";
                            break;
                        //20220913_2 ito ed //////////
                        //20221014_1 ito st //////////
                        case "コミナティ（２価：ＢＡ．４／５）":
                            verifyBoxMaker.Text = "8";
                            break;
                        //20221014_1 ito ed //////////

                    }

                    //提供データに何も入ってない場合は入力するのでTabStopを有効に
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
                    if (d.f027vacc_date4 != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
                    if (d.f028vacc_ticketnum4.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
                    if (d.f029vacc_insurercode4.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
                    if (d.f030vacc_place4.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
                    if (d.f031vacc_doctor4.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
                    if (d.f032vacc_maker4.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
                    if (d.f033vacc_lot4.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;

                    break;

                case "5":
                    //その回が新規入力の場合、提供データに何も入ってないので空文字が入ってしまうのを防ぐ                    
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuY.Text = d.f035vacc_date5.Substring(0, 4);
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuM.Text = d.f035vacc_date5.Substring(4, 2);
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuD.Text = d.f035vacc_date5.Substring(6, 2);
                    if (d.f036vacc_ticketnum5.Trim() != string.Empty) verifyBoxTicket.Text = d.f036vacc_ticketnum5.Trim();
                    if (d.f037vacc_insurercode5.Trim() != string.Empty) verifyBoxInsNum.Text = d.f037vacc_insurercode5.Trim();
                    if (d.f038vacc_place5.Trim() != string.Empty) verifyBoxPlace.Text = d.f038vacc_place5.Trim();
                    if (d.f039vacc_doctor5.Trim() != string.Empty) verifyBoxDr.Text = d.f039vacc_doctor5.Trim();
                    if (d.f041vacc_lot5.Trim() != string.Empty) verifyBoxLot.Text = d.f041vacc_lot5.Trim();

                    switch (d.f040vacc_maker5.Trim())
                    {
                        case "ファイザー":
                            verifyBoxMaker.Text = "1";
                            break;
                        case "アストラゼネカ":
                            verifyBoxMaker.Text = "2";
                            break;
                        case "武田／モデルナ":
                            verifyBoxMaker.Text = "3";
                            break;
                        case "ファイザー（５から１１歳用）":
                            verifyBoxMaker.Text = "4";
                            break;
                        case "ノババックス":
                            verifyBoxMaker.Text = "5";
                            break;
                        //20220913_2 ito st //////////
                        case "コミナティ（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "6";
                            break;
                        case "スパイクバックス（２価：ＢＡ．１）":
                            verifyBoxMaker.Text = "7";
                            break;
                        //20220913_2 ito ed //////////
                        //20221014_1 ito st //////////
                        case "コミナティ（２価：ＢＡ．４／５）":
                            verifyBoxMaker.Text = "8";
                            break;
                        //20221014_1 ito ed //////////

                    }

                    //提供データに何も入ってない場合は入力するのでTabStopを有効に
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuY.TabStop = false; else verifyBoxSesshuY.TabStop = true;
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuM.TabStop = false; else verifyBoxSesshuM.TabStop = true;
                    if (d.f035vacc_date5 != string.Empty) verifyBoxSesshuD.TabStop = false; else verifyBoxSesshuD.TabStop = true;
                    if (d.f036vacc_ticketnum5.Trim() != string.Empty) verifyBoxTicket.TabStop = false; else verifyBoxTicket.TabStop = true;
                    if (d.f037vacc_insurercode5.Trim() != string.Empty) verifyBoxInsNum.TabStop = false; else verifyBoxInsNum.TabStop = true;
                    if (d.f038vacc_place5.Trim() != string.Empty) verifyBoxPlace.TabStop = false; else verifyBoxPlace.TabStop = true;
                    if (d.f039vacc_doctor5.Trim() != string.Empty) verifyBoxDr.TabStop = false; else verifyBoxDr.TabStop = true;
                    if (d.f040vacc_maker5.Trim() != string.Empty) verifyBoxMaker.TabStop = false; else verifyBoxMaker.TabStop = true;
                    if (d.f041vacc_lot5.Trim() != string.Empty) verifyBoxLot.TabStop = false; else verifyBoxLot.TabStop = true;

                    break;
                //20220823_1 ito ed //////////
            }
            */
        }

        /// <summary>
        /// バーコード番号をセット
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private string setBarCode(App app)
        {
            var ocr = app.OcrData.Split(',');

            if (ocr.Length < 27) return string.Empty;

            //バーコード番号は18桁の数字のみ
            string strBarcode = string.Empty;
            if (System.Text.RegularExpressions.Regex.IsMatch(ocr[3].ToString(), "[0-9]{18}"))
            {
                //ベリファイ入力時は自動で入れない
                if (!firstTime) return strBarcode;

                strBarcode = ocr[3].ToString().Trim();
                verifyBoxBarCode.Text = strBarcode;
                verifyBoxKenshu.Text = strBarcode.Substring(0, 1);
                verifyBoxTimes.Text = strBarcode.Substring(1, 1);
                verifyBoxInsNum.Text = strBarcode.Substring(2, 6);
                verifyBoxTicket.Text = strBarcode.Substring(8, 10);
            }
            else verifyBoxBarCode.Text = String.Empty;

            return strBarcode;
        }


        /// <summary>
        /// OCRDATA反映
        /// </summary>
        /// <param name="app"></param>
        private void setOCRData(App app)
        {
            //20220127093409 furukawa st ////////////////////////
            //バーコード番号以外のOCRは不要（南さん
            
            return;
            //20220127093409 furukawa ed ////////////////////////






            var ocr = app.OcrData.Split(',');

            if (ocr.Length < 27) return;



            //数字があれば１：記載有り
            if ((System.Text.RegularExpressions.Regex.IsMatch(ocr[4].ToString(), "[0-9]{2}")) &&
                (System.Text.RegularExpressions.Regex.IsMatch(ocr[5].ToString(), "[0-9]{1}"))) verifyBoxTaion.Text = "1";
            else verifyBoxTaion.Text = "2";

            //2週間以内
            if ((ocr[6].ToString() == "1") && (ocr[7].ToString() == "0")) verifyBox2w.Text = "1";
            else if ((ocr[6].ToString() == "0") && (ocr[7].ToString() == "1")) verifyBox2w.Text = "2";
            else verifyBox2w.Text = "";

            //接種可能
            if ((ocr[8].ToString() == "1") && (ocr[9].ToString() == "0")) verifyBoxPossible.Text = "1";
            else if ((ocr[8].ToString() == "0") && (ocr[9].ToString() == "1")) verifyBoxPossible.Text = "2";
            else verifyBoxPossible.Text = "";

            //数字があれば１：記載有り
            if (ocr[10].ToString().Trim() != string.Empty) verifyBoxDrSign.Text = "1";
            else verifyBoxDrSign.Text = "2";

            //通常、休日、時間外 00=1通常 10=2時間外 01=3休日
            if ((ocr[11].ToString() == "0") && (ocr[12].ToString() == "0")) verifyBoxZikangai.Text = "1";//通常
            else if ((ocr[11].ToString() == "1") && (ocr[12].ToString() == "0")) verifyBoxZikangai.Text = "2";//時間外
            else if ((ocr[11].ToString() == "0") && (ocr[12].ToString() == "1")) verifyBoxZikangai.Text = "3";//休日
            else verifyBoxZikangai.Text = "";


            //6歳未満
            if (ocr[13].ToString().Trim() =="0") verifyBox6down.Text = "1";
            else if (ocr[13].ToString().Trim() == "1") verifyBox6down.Text = "2";
            else verifyBox6down.Text = "";


            //希望
            if ((ocr[16].ToString() == "0") && (ocr[17].ToString() == "0")) verifyBoxKibou.Text = "3";//不明
            else if ((ocr[16].ToString() == "1") && (ocr[17].ToString() == "0")) verifyBoxKibou.Text = "1";//希望
            else if ((ocr[16].ToString() == "0") && (ocr[17].ToString() == "1")) verifyBoxKibou.Text = "2";//希望しない
            else verifyBoxKibou.Text = "";


            //日付署名　年月日を除いて、数字が3つあれば署名ありとする
            string strDateSign = System.Text.RegularExpressions.Regex.Replace(ocr[18].ToString(), "[年|月|日]", "");
            if (System.Text.RegularExpressions.Regex.IsMatch(strDateSign, "[0-9]{3}")) verifyBoxDateSign.Text = "1";
            else verifyBoxDateSign.Text = "2";

            //自筆署名　2文字以上でありとする
            string strSign = ocr[19].ToString().Trim();
            if (strSign.Length>=2) verifyBoxSign.Text = "1";
            else verifyBoxSign.Text = "2";


            //メーカー 大体の予測　提供データから値が入っている時はOCR無視
            if (verifyBoxMaker.Text == string.Empty)
            {
                string strMaker = ocr[20].ToString().Trim();
                //メーカーも選択にする2022/01/21森安さん
                if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[ファイザ|フアイザ|フア|ファイ|フアィ|ﾌｧｲ|ﾌｱｲ]")) verifyBoxMaker.Text = "1";
                else if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[武田|武|モデルナ|ﾓﾃﾞ]")) verifyBoxMaker.Text = "3";
                else if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[アストラ|ｱｽﾄﾗ|アスト|ｱｽﾄ|アス|ｱｽ|ァス|ｧｽ]")) verifyBoxMaker.Text = "2";

                //if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[ファイザ|フアイザ|フア|ファイ|フアィ|ﾌｧｲ|ﾌｱｲ]")) verifyBoxMaker.Text = "ファイザー";
                //else if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[武田|武|モデルナ|ﾓﾃﾞ]")) verifyBoxMaker.Text = "武田／モデルナ";
                //else if (System.Text.RegularExpressions.Regex.IsMatch(strMaker, "[アストラ|ｱｽﾄﾗ|アスト|ｱｽﾄ|アス|ｱｽ|ァス|ｧｽ]")) verifyBoxMaker.Text = "アストラゼネカ";
                else verifyBoxMaker.Text = "";
            }

            //2022/01/21　医療機関等コードOCRしない南さんより
            //医療機関等コード　7-10桁ある場合はそのまま入れる
            //if (System.Text.RegularExpressions.Regex.IsMatch(ocr[22].ToString().Trim(), "[0-9]{7,10}")) verifyBoxHos.Text = ocr[22].ToString().Trim();
            //else if (strPrevHosNumber != string.Empty) verifyBoxHos.Text = strPrevHosNumber;//マッチングが１番、OCRがあれば２番、一つ前の予診票のは３番目
            //else verifyBoxHos.Text = "";


            //接種年月日　提供データから値が入っている時はOCR無視
            if (verifyBoxSesshuY.Text == string.Empty && verifyBoxSesshuM.Text == string.Empty && verifyBoxSesshuD.Text == String.Empty)
            {
                //接種年月日
                if (System.Text.RegularExpressions.Regex.IsMatch(ocr[23].ToString().Trim(), "[0-2]{4}")) verifyBoxSesshuY.Text = ocr[23].ToString().Trim();
                else verifyBoxSesshuY.Text = "";
                if (System.Text.RegularExpressions.Regex.IsMatch(ocr[24].ToString().Trim(), "[0-9]{2}")) verifyBoxSesshuM.Text = ocr[24].ToString().Trim();
                else verifyBoxSesshuM.Text = "";
                if (System.Text.RegularExpressions.Regex.IsMatch(ocr[25].ToString().Trim(), "[0-9]{2}")) verifyBoxSesshuD.Text = ocr[25].ToString().Trim();
                else verifyBoxSesshuD.Text = "";
            }


        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                                        
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            //switch (app.MediYear)
            //{
            //case (int)APP_SPECIAL_CODE.続紙:
            //    setValue(verifyBoxKind, clsInputKind.続紙, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.不要:
            //    setValue(verifyBoxKind, clsInputKind.不要, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.バッチ:
            //    setValue(verifyBoxKind, clsInputKind.エラー, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.同意書:
            //    setValue(verifyBoxKind, clsInputKind.施術同意書, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.同意書裏:
            //    setValue(verifyBoxKind, clsInputKind.施術同意書裏, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.施術報告書:
            //    setValue(verifyBoxKind, clsInputKind.施術報告書, firstTime, nv);
            //    break;

            //case (int)APP_SPECIAL_CODE.状態記入書:
            //    setValue(verifyBoxKind, clsInputKind.状態記入書, firstTime, nv);
            //    break;

            switch (app.AppType) {
                //20220127134650 furukawa st ////////////////////////
                //不要、続紙もあり得る
                
                case APP_TYPE.続紙:
                    setValue(verifyBoxKind, clsInputKind.続紙, firstTime, nv);
                    break;
                case APP_TYPE.不要:
                    setValue(verifyBoxKind, clsInputKind.不要, firstTime, nv);
                    break;
                //20220127134650 furukawa ed ////////////////////////



                case APP_TYPE.長期:// (int)APP_SPECIAL_CODE.長期:
                    setValue(verifyBoxKind, clsInputKind.長期, firstTime, nv);

               

                    setValue(verifyBoxY, app.MediYear, firstTime, nv);              //年
                    setValue(verifyBoxM, app.MediMonth, firstTime, nv);             //月

                    setValue(verifyBoxZikan, app.Family, firstTime, nv);            //時間外等
                    setValue(verifyBoxNOB, app.HihoNum, firstTime, nv);             //NoB類

                    setValue(verifyBoxS6U, app.FushoDays1, firstTime, nv);       //6歳以上人数
                    setValue(verifyBoxS6D, app.FushoCourse1, firstTime, nv);     //6歳未満人数
                    setValue(verifyBoxS6UK, app.FushoDays2, firstTime, nv);      //6歳以上金額
                    setValue(verifyBoxS6DK, app.FushoCourse2, firstTime, nv);    //6歳未満金額
                    setValue(verifyBoxY6U, app.FushoDays3, firstTime, nv);       //6歳以上予診人数
                    setValue(verifyBoxY6D, app.FushoCourse3, firstTime, nv);     //6歳未満予診人数
                    setValue(verifyBoxY6UK, app.FushoDays4, firstTime, nv);      //6歳以上予診金額
                    setValue(verifyBoxY6DK, app.FushoCourse4, firstTime, nv);    //6歳未満予診金額

                    setValue(verifyBoxHosNum, app.ClinicNum, firstTime, nv);    //医療機関等番号
                    setValue(verifyBoxClinicName, app.ClinicName, firstTime, nv);//医療機関名
                    setValue(verifyBoxDrHD, app.DrName, firstTime, nv);//医師名 2022/01/21森安さん

                    verifyCheckBoxToyonakaIgai.Checked = app.HihoAdd == string.Empty ? false: bool.Parse(app.HihoAdd);//豊中以外フラグ
                    

                    setValue(verifyBoxBatchNo, app.HihoZip, firstTime, nv);//バッチ番号
                    break;


                default:

                    //予診票

                    setValue(verifyBoxZikangai, app.Family, firstTime, nv);                 //時間外等
                    setValue(verifyBox6down, app.FushoCourse4, firstTime, nv);              //6歳未満
                    setValue(verifyBoxHos, app.ClinicNum, firstTime, nv);                   //医療機関等コード

                    setValue(verifyBoxKenshu, app.PublcExpense, firstTime, nv);             //券種
                    setValue(verifyBoxTimes, app.FushoDays2, firstTime, nv);                //回数
                    setValue(verifyBoxInsNum, app.InsNum, firstTime, nv);                   //保険者番号
                    setValue(verifyBoxTicket, app.PersonName, firstTime, nv);               //券番号
                    setValue(verifyBoxBarCode, app.HihoName, firstTime, nv);                //バーコード番号                    

                    setValue(verifyBoxTaion, app.FushoDays1, firstTime, false);                //体温
                    setValue(verifyBox2w, app.FushoCourse1, firstTime, false);                 //2週間以内に接種したか
                    setValue(verifyBoxPossible, app.FushoCourse2, firstTime, false);           //接種可能
                    setValue(verifyBoxDrSign, app.FushoDays3, firstTime, false);               //医師署名
                    setValue(verifyBoxKibou, app.Ratio, firstTime, false);                     //接種希望
                    setValue(verifyBoxDateSign, app.FushoCourse3, firstTime, false);           //日付署名
                    setValue(verifyBoxSign, app.FushoDays4, firstTime, false);                 //本人署名
                    
                    setValue(verifyBoxMaker, app.FushoName3, firstTime, nv);                //メーカー
                    setValue(verifyBoxLot, app.FushoName4, firstTime, nv);                  //ロット

                    //2022/01/21森安さん　接種会場/医師名はSRVデータに統一するのでヘッダで入力し、入力完了後一気に更新
                    //setValue(verifyBoxPlace, app.ClinicName, firstTime, nv);                //接種会場
                    //setValue(verifyBoxDr, app.DrName, firstTime, nv);                       //医師名  
                    
            
                    //接種日
                    setValue(verifyBoxSesshuY, app.FushoName2 == string.Empty ? string.Empty : app.FushoName2.Substring(0, 4), firstTime, nv);
                    setValue(verifyBoxSesshuM, app.FushoName2 == string.Empty ? string.Empty : app.FushoName2.Substring(4, 2), firstTime, nv);
                    setValue(verifyBoxSesshuD, app.FushoName2 == string.Empty ? string.Empty : app.FushoName2.Substring(6, 2), firstTime, nv);



                    break;
            }
        }


        
        private void verifyBoxNOB_Validated(object sender, EventArgs e)
        {
            //バッチ番号生成(cym+(NoB or 医療機関等コード)+通常/時間外等)
            //202201-1234-1
            var app = (App)bsApp.Current;
            if (verifyBoxNOB.Text != string.Empty)
            {
                verifyBoxBatchNo.Text = $"{app.CYM.ToString()}-{verifyBoxNOB.Text.Trim()}-{verifyBoxZikan.Text.Trim()}";
            }
            else
            {
                verifyBoxBatchNo.Text = $"{app.CYM.ToString()}-{verifyBoxHosNum.Text.Trim()}-{verifyBoxZikan.Text.Trim()}";
            }

            //この束の医療機関情報
            HosInfo = dataimport_chargelist.select(verifyBoxNOB.Text.Trim());
            if (HosInfo == null) return;

            verifyBoxHosNum.Text = HosInfo.hosnum.Trim();
            verifyBoxClinicName.Text = HosInfo.hosname.Trim();

        }

        private void verifyBoxHosNum_Validated(object sender, EventArgs e)
        {

            //バッチ番号生成(cym+(NoB or 医療機関等コード)+通常/時間外等)
            //202201-1234-1
            var app = (App)bsApp.Current;
            if (verifyBoxNOB.Text != string.Empty)
            {
                verifyBoxBatchNo.Text = $"{app.CYM.ToString()}-{verifyBoxNOB.Text.Trim()}-{verifyBoxZikan.Text.Trim()}";
            }
            else
            {
                verifyBoxBatchNo.Text = $"{app.CYM.ToString()}-{verifyBoxHosNum.Text.Trim()}-{verifyBoxZikan.Text.Trim()}";
            }

            if (HosInfo == null)
            {
                //この束の医療機関情報
                HosInfo = dataimport_chargelist.select_clinicnum(verifyBoxHosNum.Text.Trim());
                if (HosInfo == null) return;
            }

            verifyBoxClinicName.Text = HosInfo.hosname;


        }
        #endregion

        #region 各種更新





        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxKind.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;
                    break;

                case clsInputKind.長期://ヘッダシートになる

                    #region 入力チェック
                    int intverifyboxY = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, intverifyboxY < 0);

                    int intverifyboxM = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, intverifyboxM < 0 || intverifyboxM>12);

                    //NoB類は無い場合がある
                    string strNoB = verifyBoxNOB.Text;
                    if (strNoB == string.Empty)
                    {
                        verifyBoxNOB.BackColor = Color.LightGreen;
                        if (MessageBox.Show("空欄ですが宜しいですか？", "",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
                    }
                    //setStatus(verifyBoxNOB, strNoB ==string.Empty);

                    //GetIntValueは空欄時-9を返すので文字列型にしよう

                    string strZikan = verifyBoxZikan.Text.Trim();
                    setStatus(verifyBoxZikan, !(new string[] { "3", "1", "2","9" }).Contains(strZikan));

                    string strS6D = verifyBoxS6D.Text.Trim();
                    setStatus(verifyBoxS6D, strS6D != string.Empty && int.Parse(strS6D) < 0);
                    string strS6DK = verifyBoxS6DK.Text.Trim();
                    setStatus(verifyBoxS6DK, strS6DK != string.Empty && int.Parse(strS6DK) < 0);

                    string strS6U = verifyBoxS6U.Text.Trim();
                    setStatus(verifyBoxS6U, strS6U != string.Empty && int.Parse(strS6U) < 0);
                    string strS6UK = verifyBoxS6UK.Text.Trim();
                    setStatus(verifyBoxS6UK, strS6UK != string.Empty && int.Parse(strS6UK) < 0);

                    string strY6D = verifyBoxY6D.Text.Trim();
                    setStatus(verifyBoxY6D, strY6D != string.Empty && int.Parse(strY6D) < 0);
                    string strY6DK = verifyBoxY6DK.Text.Trim();
                    setStatus(verifyBoxY6DK, strY6DK != string.Empty && int.Parse(strY6DK) < 0);

                    string strY6U = verifyBoxY6U.Text.Trim();
                    setStatus(verifyBoxY6U, strY6U != string.Empty && int.Parse(strY6U) < 0);
                    string strY6UK = verifyBoxY6UK.Text.Trim();
                    setStatus(verifyBoxY6UK, strY6UK != string.Empty && int.Parse(strY6UK) < 0);

                    string strBatch = verifyBoxBatchNo.Text.Trim();
                    string strMatchPattern1 = "^[0-9]{6}-[0-9]{4}-[0-9]{1}$";
                    string strMatchPattern2 = "^[0-9]{6}-[0-9]{10}-[0-9]{1}$";

                    if(
                        (!System.Text.RegularExpressions.Regex.IsMatch(strBatch,strMatchPattern1)) &&
                        (!System.Text.RegularExpressions.Regex.IsMatch(strBatch, strMatchPattern2)) 
                        )
                    {
                        setStatus(verifyBoxBatchNo, true);
                    }


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }
                    #endregion

                    //年月を取得しておく
                    intY = intverifyboxY;
                    intM = intverifyboxM;

                    #region appに適用
                    //通常の柔整申請書は種類が入るが、今回は年が必要
                    //app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.MediYear = intY;        //年

                    app.MediMonth = intM;           //月 
                    
                    app.AppType = APP_TYPE.長期;      //種類

                    app.HihoNum = strNoB;           //NoB類

                    app.Family = strZikan != string.Empty ? int.Parse(strZikan) : 0;          //時間外等
                    app.FushoDays1 = strS6U != string.Empty ? int.Parse(strS6U) : 0;        //6歳以上人数
                    app.FushoCourse1 = strS6D != string.Empty ? int.Parse(strS6D) : 0;      //6歳未満人数
                    app.FushoDays2 = strS6UK != string.Empty ? int.Parse(strS6UK) : 0;       //6歳以上金額
                    app.FushoCourse2 = strS6DK != string.Empty ? int.Parse(strS6DK) : 0;     //6歳未満金額
                    app.FushoDays3 = strY6U != string.Empty ? int.Parse(strY6U) : 0;        //6歳以上予診人数
                    app.FushoCourse3 = strY6D != string.Empty ? int.Parse(strY6D) : 0;      //6歳未満予診人数
                    app.FushoDays4 = strY6UK != string.Empty ? int.Parse(strY6UK) : 0;       //6歳以上予診金額
                    app.FushoCourse4 = strY6DK != string.Empty ? int.Parse(strY6DK) : 0;     //6歳未満予診金額

                    app.ClinicNum = verifyBoxHosNum.Text.Trim();            //医療機関等コード
                    app.ClinicName = verifyBoxClinicName.Text.Trim();       //医療機関名
                    app.HihoZip = verifyBoxBatchNo.Text.Trim();             //バッチ番号
                    app.DrName = verifyBoxDrHD.Text.Trim();                 //医師名ヘッダ

                    app.HihoAdd = verifyCheckBoxToyonakaIgai.Checked.ToString();//豊中市以外フラグ


                    app.Numbering = dataimport == null ? string.Empty: dataimport.f001number;//メインの券番号

                    //豊中市以外チェックボックスの値を持っておく
                    WithoutToyonakashi = verifyCheckBoxToyonakaIgai.Checked;

                    //予診票に渡すために控える
                    strHeaderBatchNo = verifyBoxBatchNo.Text.Trim();//バッチ番号

                    //医療機関等コード取得
                    strHeaderHosNumber = app.ClinicNum;

                    //入力すべき予診票の枚数取得（接種6歳以上人数+接種6歳未満人数＋予診6歳以上人数＋予診6歳未満人数）
                    //202208091450 ito st //////////
                    //intHeaderPages = strS6U != string.Empty ? int.Parse(strS6U) : 0 +
                    //     strS6D != string.Empty ? int.Parse(strS6D) : 0 +
                    //     strY6U != string.Empty ? int.Parse(strY6U) : 0 +
                    //     strY6D != string.Empty ? int.Parse(strY6D) : 0;
                    //202208091450 ito ed //////////

                    //ヘッダのAID取得
                    intHeaederAID = app.Aid;

                    //ヘッダの時間外等
                    strHeaderZikangai = strZikan;

                    //NoBを控えて予診票に持って行く
                    strHeaderNoB = strNoB;

                    #endregion

                    break;

                default:
                    //予診票

                    #region 入力チェック


                    string strKenshu = verifyBoxKenshu.Text.Trim();
                    setStatus(verifyBoxKenshu, strKenshu == string.Empty);                                      //券種
                    string strTimes = verifyBoxTimes.Text.Trim();                                               //回数
                    setStatus(verifyBoxTimes, strTimes == string.Empty);                                        //
                    string strInsNum = verifyBoxInsNum.Text.Trim();                                             //保険者番号
                    setStatus(verifyBoxInsNum, strInsNum == string.Empty);                                      //
                    string strTicket = verifyBoxTicket.Text.Trim();                                             //券番号
                    setStatus(verifyBoxTicket, strTicket == string.Empty);                                      //
                    string strBarCode = verifyBoxBarCode.Text.Trim();                                           //バーコードの値
                    setStatus(verifyBoxBarCode, strBarCode == string.Empty);                                    //
                                                                                                                
                    string strTaion = verifyBoxTaion.Text.Trim();                                               //体温
                    setStatus(verifyBoxTaion, !(new string[] { "2", "1"}).Contains(strTaion));                 
                    string str2w = verifyBox2w.Text.Trim();                                                     //2週間以内に接種したか
                    setStatus(verifyBox2w, !(new string[] { "2", "1","3" }).Contains(str2w));                  
                                                                                                               
                    string strPossible = verifyBoxPossible.Text.Trim();                                         //接種可能
                    setStatus(verifyBoxPossible, !(new string[] { "2", "1" ,"3"}).Contains(strPossible));      
                    string strDrSign = verifyBoxDrSign.Text.Trim();                                             //医師署名
                    setStatus(verifyBoxDrSign, !(new string[] { "2", "1" }).Contains(strDrSign));              
                    
                    
                    string strZikangai = verifyBoxZikangai.Text.Trim();                                         //時間外
                    setStatus(verifyBoxZikangai,!(new string[] { "9","1","2","3"}).Contains(strZikangai));     

                    //if(strPrevZikangai!=string.Empty && strPrevZikangai != strZikangai)
                    //{
                    //    MessageBox.Show("時間外が前の予診票と異なります。エラー登録してください");
                    //    app.Memo += $"時間外違い 一つ前:{strPrevZikangai} 予診票:{strZikangai}\r\n";
                    //    return false;
                    //}


                    //この予診票の時間外を控える
                    strPrevZikangai = strZikangai;

                    string str6down = verifyBox6down.Text.Trim();                                               //6歳未満
                    setStatus(verifyBox6down, !(new string[] { "2", "1" }).Contains(str6down));                 //
                    string strKibou = verifyBoxKibou.Text.Trim();                                               //接種希望
                    setStatus(verifyBoxKibou, !(new string[] { "2", "1" ,"3"}).Contains(strKibou));             //
                    string strDateSign = verifyBoxDateSign.Text.Trim();                                         //日付署名
                    setStatus(verifyBoxDateSign, !(new string[] { "2", "1" }).Contains(strDateSign));           //
                    string strSign = verifyBoxSign.Text.Trim();                                                 //本人署名
                    setStatus(verifyBoxSign, !(new string[] { "2", "1" }).Contains(strSign));                   //

                    //20220127135453 furukawa st ////////////////////////
                    //接種を見合わせた場合メーカー、ロットが空欄になり得る
                    string strMaker = verifyBoxMaker.Text.Trim();                                                       //メーカー

                    setStatus(verifyBoxMaker, !(new string[] { "99", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }).Contains(strMaker)); //221209_1 ito /////メーカー名追加
                    //setStatus(verifyBoxMaker, !(new string[] { "9", "1", "2", "3", "4", "5", "6", "7", "8" }).Contains(strMaker)); //221026_1 ito /////メーカー名追加
                    //setStatus(verifyBoxMaker, !(new string[] { "9", "1", "2", "3" ,"4","5"}).Contains(strMaker)); //20220608103949 furukawa st /////メーカー名追加
                    //  setStatus(verifyBoxMaker, !(new string[] { "9", "1", "2", "3" }).Contains(strMaker));
                    //  setStatus(verifyBoxMaker, strMaker == string.Empty);                                                //

                    //202208151300 ito st //
                    //string strLot = verifyBoxLot.Text.Trim();                                                           //ロット
                    string strLot = verifyBoxLot.Text.Replace(" ","").Replace("　","").ToUpper();
                    verifyBoxLot.Text = strLot;
                    //202208151300 ito ed //
                    //  setStatus(verifyBoxLot, strLot == string.Empty);                                                    //

                    //20220127135453 furukawa ed ////////////////////////


                    //予診票では入力せずヘッダの入力を，入力完了後に一気に更新
                    //string strPlace = verifyBoxPlace.Text.Trim();                                                       //接種会場
                    //setStatus(verifyBoxPlace, strPlace == string.Empty);                                                
                    //string strDr = verifyBoxDr.Text.Trim();                                                             //医師名
                    //setStatus(verifyBoxDr, strDr == string.Empty);                                                      

                    string strHos = verifyBoxHos.Text.Trim();                                                           //医療機関等コード
                    setStatus(verifyBoxHos, strHos == string.Empty || (strHos!=string.Empty && strHos.Length!=10));//


                    //20220218111437 furukawa st ////////////////////////
                    //不要（森安さん　2022/02/18

                    //      //一つ前の予診票の医療機関等コードとの差異チェック
                    //      if (strPrevHosNumber != string.Empty && strPrevHosNumber != strHos)
                    //      {
                    //          verifyBoxHos.BackColor = Color.LightGreen;
                    //          if (MessageBox.Show(
                    //              "医療機関等コードが前の予診票と異なりますが宜しいですか？\r\n" +
                    //              $"医療機関等コード違い 一つ前:{strPrevHosNumber} 予診票:{strHos}","",
                    //              MessageBoxButtons.YesNo, 
                    //              MessageBoxIcon.Question,
                    //              MessageBoxDefaultButton.Button2) == DialogResult.No) return false;


                    //      }

                    //      //この予診票の医療機関等コードを控えておく
                    //      strPrevHosNumber = strHos;

                    //20220218111437 furukawa ed ////////////////////////

                    string strSesshuY = verifyBoxSesshuY.Text.Trim();                                                   //接種日
                    setStatus(verifyBoxSesshuY, !int.TryParse(strSesshuY,out int tmpY) || tmpY<=2000);                  //
                    string strSesshuM = verifyBoxSesshuM.Text.Trim();                                                   //
                    setStatus(verifyBoxSesshuM, !int.TryParse(strSesshuM, out int tmpM) || tmpM < 0 || tmpM>12);        //
                    string strSesshuD = verifyBoxSesshuD.Text.Trim();                                                   //
                    setStatus(verifyBoxSesshuD, !int.TryParse(strSesshuD, out int tmpD) || tmpD <0|| tmpD>31);          //



                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    #endregion

                    #region Appへの反映

                 

                    //ここから値の反映

                    app.AppType = scan.AppType;        //申請書種別

                    if (intY != 0) app.MediYear = intY;              //施術年
                    if (intM != 0) app.MediMonth = intM;             //施術月

                    //スキャングループの最初がヘッダでない場合、ヘッダは取りようがないので入力終了後にチェック作業時にヘッダの値を取得して更新する
                    //app.HihoNum = strHeaderNoB;         //請求書のNoB類
                    
                    
                    app.PublcExpense = strKenshu;       //券種
                    app.InsNum = strInsNum;         //保険者番号
                    app.HihoName = strBarCode;      //バーコードの値                                                    

                    //スキャングループの最初がヘッダでない場合、ヘッダは取りようがないので入力終了後にチェック作業時にヘッダの値を取得して更新する
                    app.Family = int.Parse(strZikangai);            //時間外

                    app.Ratio = int.Parse(strKibou);               //接種するしない

                    app.FushoDays1 = int.Parse(strTaion);               //体温
                    app.FushoCourse1 = int.Parse(str2w);                //2週間以内に接種したか
                    app.FushoDays2 = int.Parse(strTimes);               //回数
                    app.FushoCourse2 = int.Parse(strPossible);          //接種可能
                    app.FushoDays3 = int.Parse(strDrSign);              //医師署名
                    app.FushoCourse3 = int.Parse(strDateSign);          //日付署名
                    app.FushoDays4 = int.Parse(strSign);                //本人署名
                    app.FushoCourse4 = int.Parse(str6down);             //6歳未満

                    app.ClinicNum = strHos;     //医療機関コード

                    //マッチングがある場合は提供データを引っ張る。無い場合は入力そのままを入れる
                    if (dataimport != null)
                    {
                        app.RrID = dataimport.importid;//インポートIDを持たせておく
                        app.Numbering = dataimport.f001number;//メインの券番号
                    }
                    else
                    {
                        app.RrID = 0;//インポートIDを持たせておく
                        app.Numbering = strTicket;//メインの券番号
                    }


                    //スキャングループの最初がヘッダでない場合、ヘッダ情報が取れないので入力終了後にチェック作業時に追加
                    //ヘッダから持ってきたバッチ番号
                    //app.HihoZip = strHeaderBatchNo;

                    app.PersonName = strTicket;     //券番号
                    app.FushoName1 = DateTime.Now.ToString("yyyyMMddHHmmss"); //接種履歴登録年月日
                    app.FushoName2 = new DateTime(int.Parse(strSesshuY), int.Parse(strSesshuM), int.Parse(strSesshuD)).ToString("yyyyMMdd");//接種日
                    app.FushoName3 = strMaker;      //メーカー
                    app.FushoName4 = strLot;        //ロット
                    

                    //予診票では入力せずヘッダの入力を，入力完了後に一気に更新
                    //app.ClinicName = strPlace;      //会場
                    //app.DrName = strDr;             //医師名
                    
                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString(),app.HihoZip)) return false;

                if (app.AppType == APP_TYPE.長期)
                {//ヘッダシートの場合
                    if (!UpdateChargeList(tran, app)) return false;
                }

                //20220316155507 furukawa st ////////////////////////
                //exprot.csでやる

                //if(app.AppType==APP_TYPE.柔整)
                //{//予診票の場合
                //    if (!UpdateDataExport2(tran, app)) return false;
                //}
                //20220316155507 furukawa ed ////////////////////////


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// ヘッダシートの情報を請求表テーブルに更新
        /// </summary>
        /// <returns></returns>
        private bool UpdateChargeList(DB.Transaction tran, App app)
        {
            DB.Command cmd = DB.Main.CreateCmd($"select * from chargelist where nob='{app.HihoNum}'");
            var list = cmd.TryExecuteReaderList();

            //NoB類で見つかった場合
            if (list.Count > 0)
            {
                #region NoB類で見つかった場合
                StringBuilder sbsql = new StringBuilder();
                sbsql.AppendLine(" update chargelist set ");
                switch (app.Family)
                {
                    case 1://通常
                        sbsql.AppendLine($"  ty6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,ty6u = {app.FushoDays3}");
                        sbsql.AppendLine($" ,ts6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,ts6u = {app.FushoDays1}");
                        sbsql.AppendLine($" ,ty6dk ={ app.FushoCourse4}");
                        sbsql.AppendLine($" ,ty6uk ={ app.FushoDays4}");
                        sbsql.AppendLine($" ,ts6dk ={ app.FushoCourse2}");
                        sbsql.AppendLine($" ,ts6uk ={ app.FushoDays2}");
                        sbsql.AppendLine($" ,tcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                        break;

                    case 2://時間外
                        sbsql.AppendLine($"  gy6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,gy6u = {app.FushoDays3}");
                        sbsql.AppendLine($" ,gs6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,gs6u = {app.FushoDays1}");
                        sbsql.AppendLine($" ,gy6dk ={ app.FushoCourse4}");
                        sbsql.AppendLine($" ,gy6uk ={ app.FushoDays4}");
                        sbsql.AppendLine($" ,gs6dk ={ app.FushoCourse2}");
                        sbsql.AppendLine($" ,gs6uk ={ app.FushoDays2}");
                        sbsql.AppendLine($" ,gcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                        break;

                    case 3://休日
                        sbsql.AppendLine($"  ky6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,ky6u = {app.FushoDays3}");
                        sbsql.AppendLine($" ,ks6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,ks6u = {app.FushoDays1}");
                        sbsql.AppendLine($" ,ky6dk ={ app.FushoCourse4}");
                        sbsql.AppendLine($" ,ky6uk ={ app.FushoDays4}");
                        sbsql.AppendLine($" ,ks6dk ={ app.FushoCourse2}");
                        sbsql.AppendLine($" ,ks6uk ={ app.FushoDays2}");
                        sbsql.AppendLine($" ,kcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                        break;

                    case 9://記載無し
                        sbsql.AppendLine($"  fy6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,fy6u = {app.FushoDays3}");
                        sbsql.AppendLine($" ,fs6d = {app.FushoCourse1}");
                        sbsql.AppendLine($" ,fs6u = {app.FushoDays1}");
                        sbsql.AppendLine($" ,fy6dk ={ app.FushoCourse4}");
                        sbsql.AppendLine($" ,fy6uk ={ app.FushoDays4}");
                        sbsql.AppendLine($" ,fs6dk ={ app.FushoCourse2}");
                        sbsql.AppendLine($" ,fs6uk ={ app.FushoDays2}");
                        sbsql.AppendLine($" ,fcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                        break;



                }

                sbsql.AppendLine($" where nob='{app.HihoNum}'");
                try
                {

                    cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                    cmd.TryExecuteNonQuery();

                    //総合計
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine(" update chargelist set chargetotal=");
                    sbsql.AppendLine(" tcharge+gcharge+kcharge+fcharge ");
                    sbsql.AppendLine($" where nob='{app.HihoNum}'");

                    cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                    cmd.TryExecuteNonQuery();


                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                    return false;
                }
                finally
                {
                    cmd.Dispose();
                }
                #endregion


            }

            //NoB類で見つからない場合は医療機関等コードで再度探す
            else
            {                
                cmd = DB.Main.CreateCmd($"select * from chargelist where hosnum='{app.ClinicNum}'");
                list = cmd.TryExecuteReaderList();

                if (list.Count > 0)
                {
                    #region NoB類で見つからない場合は医療機関等コードで再度探す

                    StringBuilder sbsql = new StringBuilder();
                    sbsql.AppendLine(" update chargelist set ");
                    switch (app.Family)
                    {
                        case 1://通常
                            sbsql.AppendLine($"  ty6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,ty6u = {app.FushoDays3}");
                            sbsql.AppendLine($" ,ts6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,ts6u = {app.FushoDays1}");
                            sbsql.AppendLine($" ,ty6dk ={ app.FushoCourse4}");
                            sbsql.AppendLine($" ,ty6uk ={ app.FushoDays4}");
                            sbsql.AppendLine($" ,ts6dk ={ app.FushoCourse2}");
                            sbsql.AppendLine($" ,ts6uk ={ app.FushoDays2}");
                            sbsql.AppendLine($" ,tcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                            break;

                        case 2://時間外
                            sbsql.AppendLine($"  gy6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,gy6u = {app.FushoDays3}");
                            sbsql.AppendLine($" ,gs6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,gs6u = {app.FushoDays1}");
                            sbsql.AppendLine($" ,gy6dk ={ app.FushoCourse4}");
                            sbsql.AppendLine($" ,gy6uk ={ app.FushoDays4}");
                            sbsql.AppendLine($" ,gs6dk ={ app.FushoCourse2}");
                            sbsql.AppendLine($" ,gs6uk ={ app.FushoDays2}");
                            sbsql.AppendLine($" ,gcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                            break;

                        case 3://休日
                            sbsql.AppendLine($"  ky6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,ky6u = {app.FushoDays3}");
                            sbsql.AppendLine($" ,ks6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,ks6u = {app.FushoDays1}");
                            sbsql.AppendLine($" ,ky6dk ={ app.FushoCourse4}");
                            sbsql.AppendLine($" ,ky6uk ={ app.FushoDays4}");
                            sbsql.AppendLine($" ,ks6dk ={ app.FushoCourse2}");
                            sbsql.AppendLine($" ,ks6uk ={ app.FushoDays2}");
                            sbsql.AppendLine($" ,kcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                            break;

                        case 9://記載無し
                            sbsql.AppendLine($"  fy6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,fy6u = {app.FushoDays3}");
                            sbsql.AppendLine($" ,fs6d = {app.FushoCourse1}");
                            sbsql.AppendLine($" ,fs6u = {app.FushoDays1}");
                            sbsql.AppendLine($" ,fy6dk ={ app.FushoCourse4}");
                            sbsql.AppendLine($" ,fy6uk ={ app.FushoDays4}");
                            sbsql.AppendLine($" ,fs6dk ={ app.FushoCourse2}");
                            sbsql.AppendLine($" ,fs6uk ={ app.FushoDays2}");
                            sbsql.AppendLine($" ,fcharge ={ app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}");
                            break;


                    }

                    sbsql.AppendLine($" where hosnum='{app.ClinicNum}'");
                    try
                    {
                        cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                        cmd.TryExecuteNonQuery();

                        //総合計
                        sbsql.Remove(0, sbsql.ToString().Length);
                        sbsql.AppendLine(" update chargelist set chargetotal=");
                        sbsql.AppendLine(" tcharge+gcharge+kcharge+fcharge ");
                        sbsql.AppendLine($" where hosnum='{app.ClinicNum}'");

                        cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                        cmd.TryExecuteNonQuery();



                        return true;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                    }


                    #endregion
                }


                else
                {
                    //20220205105206 furukawa st ////////////////////////
                    //新規のヘッダシート入力対応

                    //医療機関等コードでも見つからない場合、完全新規追加とする

                    #region 医療機関等コードでも見つからない場合、完全新規追加とする

                    StringBuilder sbsql = new StringBuilder();
                    sbsql.Remove(0, sbsql.ToString().Length);

                    sbsql.AppendLine($"INSERT INTO chargelist(");
                    sbsql.AppendLine($" no,hosnum, nob, hosname,");
                    switch (app.Family)
                    {
                        case 1://通常
                            sbsql.AppendLine($"ty6d, ty6u, ts6d, ts6u, ty6dk, ty6uk, ts6dk, ts6uk, tcharge     ");
                            break;
                        case 2:
                            sbsql.AppendLine($"gy6d, gy6u, gs6d, gs6u, gy6dk, gy6uk, gs6dk, gs6uk, gcharge     ");
                            break;
                        case 3:
                            sbsql.AppendLine($"ky6d, ky6u, ks6d, ks6u, ky6dk, ky6uk, ks6dk, ks6uk, kcharge     ");
                            break;
                        case 9:
                            sbsql.AppendLine($"fy6d, fy6u, fs6d, fs6u, fy6dk, fy6uk, fs6dk, fs6uk, fcharge     ");
                            break;
                    }
                    
                    sbsql.AppendLine($") values (");
                    sbsql.AppendLine($" (select max(no)+1 from chargelist),");
                    sbsql.AppendLine($" '{app.ClinicNum}', '{app.HihoNum}', '{app.ClinicName}' ");

                    sbsql.AppendLine($",{app.FushoCourse1}");
                    sbsql.AppendLine($",{app.FushoDays3}");
                    sbsql.AppendLine($",{app.FushoCourse1}");
                    sbsql.AppendLine($",{app.FushoDays1}");
                    sbsql.AppendLine($",{app.FushoCourse4}");
                    sbsql.AppendLine($",{app.FushoDays4}");
                    sbsql.AppendLine($",{app.FushoCourse2}");
                    sbsql.AppendLine($",{app.FushoDays2}");
                    sbsql.AppendLine($",{app.FushoCourse4 + app.FushoDays4 + app.FushoCourse2 + app.FushoDays2}  )");

                    try
                    {
                        cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                        cmd.TryExecuteNonQuery();

                        //総合計
                        sbsql.Remove(0, sbsql.ToString().Length);
                        sbsql.AppendLine(" update chargelist set chargetotal=");
                        sbsql.AppendLine(" tcharge+gcharge+kcharge+fcharge ");
                        sbsql.AppendLine($" where hosnum='{app.ClinicNum}'");

                        cmd = DB.Main.CreateCmd(sbsql.ToString(), tran);
                        cmd.TryExecuteNonQuery();
                        return true;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                    }

                    #endregion

                    //20220205105206 furukawa ed ////////////////////////

                }
            }

        }

        /// <summary>
        /// データ1の更新     //20220816_1 ito /////参照がゼロだったので、この中は4・5回目およびワクチンメーカー追加は未対応
        /// </summary>
        /// <param name="tran"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool UpdateDataExport2(DB.Transaction tran, App app) 
        { 
            bool flgdiff = false;
            bool flgInsert = false;//新規追加時フラグ

            StringBuilder sbsql = new StringBuilder();

            string strFiser = "ファイザー";
            string strAstra = "アストラゼネカ";
            string strTakeda = "武田／モデルナ";

            //20220608110418 furukawa st ////////////////////////
            //メーカー名追加
            
            string strFiser5 = "ファイザー（５から１１歳用）";
            string strNova = "ノババックス";
            //20220608110418 furukawa ed ////////////////////////
            try
            {


                sbsql.Remove(0, sbsql.ToString().Length);
                sbsql.AppendLine($"select * from dataexport2 where f001number='{app.Numbering}'");
                

                Object ret = null;
                using (DB.Command cmd = new DB.Command(DB.Main, sbsql.ToString()))
                {
                    ret=cmd.TryExecuteScalar();
                }
                if (ret == null) flgInsert = true;

                //20220205105037 furukawa st ////////////////////////
                //新規の予診票入力対応
                
                if (flgInsert)
                {
                    #region 新規の予診票
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($" insert into dataexport2 (");
                    sbsql.AppendLine($" f001number ");

                    switch (app.FushoDays2) {
                        
                        case 1:
                            sbsql.AppendLine($", f002vacc_entrydate1, f003vacc_date1, f004vacc_ticketnum1, f005vacc_insurercode1");
                            sbsql.AppendLine($", f006vacc_place1, f007vacc_doctor1, f008vacc_maker1, f009vacc_lot1, aid1        ");
                            
                            break;
                        case 2:
                            sbsql.AppendLine($", f010vacc_entrydate2, f011vacc_date2, f012vacc_ticketnum2, f013vacc_insurercode2");
                            sbsql.AppendLine($", f014vacc_place2, f015vacc_doctor2, f016vacc_maker2, f017vacc_lot2, aid2        ");
                            
                            break;
                        case 3:
                            sbsql.AppendLine($", f018vacc_entrydate3, f019vacc_date3, f020vacc_ticketnum3, f021vacc_insurercode3");
                            sbsql.AppendLine($", f022vacc_place3, f023vacc_doctor3, f024vacc_maker3, f025vacc_lot3, aid3        ");
                            
                            break;
                    }

                    sbsql.AppendLine($", cym, sid, importdate, updatedate");
                    sbsql.AppendLine($" ) values (   ");

                    //券番号は頭が7の場合は10桁,その他は8桁
                    if (app.Numbering.Substring(0, 1) == "7") sbsql.AppendLine($"'{app.Numbering}'");
                    else sbsql.AppendLine($"'{app.Numbering.Substring(10-(7+1))}'");

                    sbsql.AppendLine($", '{app.FushoName1}', '{app.FushoName2.Trim()}', '{app.PersonName.Trim()}', '{app.InsNum.Trim()}'");
                    sbsql.AppendLine($", '{app.ClinicName.Trim()}', '{app.DrName.Trim()}'   ");

                    switch (app.FushoName3.Trim())
                    {
                        case "1":
                            sbsql.AppendLine($" ,'{strFiser}'");
                            break;
                        case "2":
                            sbsql.AppendLine($" ,'{strAstra}'");
                            break;
                        case "3":
                            sbsql.AppendLine($" ,'{strTakeda}'");
                            break;
                        //20220608110444 furukawa st ////////////////////////
                        //メーカー名追加
                        
                        case "4":
                            sbsql.AppendLine($" ,'{strFiser5}'");
                            break;
                        case "5":
                            sbsql.AppendLine($" ,'{strNova}'");
                            break;
                            //20220608110444 furukawa ed ////////////////////////

                    }
                    sbsql.AppendLine($", '{app.FushoName4.Trim()}','{app.Aid}'  ");
                    sbsql.AppendLine($", {app.CYM},'{app.ClinicNum}','{DateTime.MinValue}','{DateTime.Now.ToString("yyyy-MM-dd")}' ) ");

                    //dataexport2更新処理
                    using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                    {
                        cmd.TryExecuteNonQuery();
                    }
                    #endregion

                }//20220205105037 furukawa ed ////////////////////////
                else
                {
                    #region dataexport2更新処理

                    //更新日付を入れる
                    sbsql.Remove(0, sbsql.ToString().Length);
                    sbsql.AppendLine($" update dataexport2 set updatedate='{DateTime.Now}',sid='{app.ClinicNum}' ");

                    switch (app.FushoDays2)//回数
                    {
                        //各項目を比較し、違っていたらその部分を更新
                        //本来比較はdataexport2としたいが、dataimportと比較してもdataexport2も同じデータのはずなので問題なし
                        case 1:

                            //20220202185642 furukawa st ////////////////////////
                            //提供データの券番号がない場合は新規入力(更新すべきレコード)と見なし、全項目入れる

                            if (dataimport.f004vacc_ticketnum1.Trim() == string.Empty)
                            {
                                flgdiff = true;
                                sbsql.AppendLine($" ,f002vacc_entrydate1 	    ='{app.FushoName1}'");
                                sbsql.AppendLine($" ,f003vacc_date1 		    ='{app.FushoName2.Trim()}'");
                                sbsql.AppendLine($" ,f004vacc_ticketnum1 	    ='{app.PersonName.Trim()}'");
                                sbsql.AppendLine($" ,f005vacc_insurercode1      ='{app.InsNum.Trim()}'");
                                sbsql.AppendLine($" ,f006vacc_place1 		    ='{app.ClinicName.Trim()}'");
                                sbsql.AppendLine($" ,f007vacc_doctor1        	='{app.DrName.Trim()}'");

                                switch (app.FushoName3.Trim())
                                {
                                    case "1":
                                        sbsql.AppendLine($" ,f008vacc_maker1 		='{strFiser}'");
                                        break;
                                    case "2":
                                        sbsql.AppendLine($" ,f008vacc_maker1 		='{strAstra}'");
                                        break;
                                    case "3":
                                        sbsql.AppendLine($" ,f008vacc_maker1 		='{strTakeda}'");
                                        break;
                                    //20220608110516 furukawa st ////////////////////////
                                    //メーカー名追加
                                    
                                    case "4":
                                        sbsql.AppendLine($" ,f008vacc_maker1 		='{strFiser5}'");
                                        break;
                                    case "5":
                                        sbsql.AppendLine($" ,f008vacc_maker1 		='{strNova}'");
                                        break;
                                        //20220608110516 furukawa ed ////////////////////////

                                }

                                sbsql.AppendLine($" ,f009vacc_lot1 		='{app.FushoName4.Trim()}'");
                                sbsql.AppendLine($" ,aid1 		='{app.Aid}'");

                            }


                            #region old



                            ////登録履歴の日付は入力しないので無関係
                            ////if (dataimport.f002vacc_entrydate1 != app.FushoName1) flgdiff = true; sbsql.AppendLine($" ,f002vacc_entrydate1 	='{app.FushoName1}'");
                            //if (dataimport.f003vacc_date1.Trim() != app.FushoName2.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f003vacc_date1 		='{app.FushoName2.Trim()}'"); }
                            //if (dataimport.f004vacc_ticketnum1.Trim() != app.PersonName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f004vacc_ticketnum1 	='{app.PersonName.Trim()}'"); }
                            //if (dataimport.f005vacc_insurercode1.Trim() != app.InsNum.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f005vacc_insurercode1='{app.InsNum.Trim()}'"); }
                            //if (dataimport.f006vacc_place1.Trim() != app.ClinicName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f006vacc_place1 		='{app.ClinicName.Trim()}'"); }
                            //if (dataimport.f007vacc_doctor1.Trim() != app.DrName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f007vacc_doctor1 	='{app.DrName.Trim()}'"); }

                            //2022/01/21　メーカーが選択になったので対応する文字列に戻す
                            //if (dataimport.f008vacc_maker1.Trim() != app.FushoName3.Trim())
                            //{ 
                            //    //flgdiff = true;
                            //    switch (app.FushoName3.Trim())
                            //    {
                            //        case "1":
                            //            if (dataimport.f008vacc_maker1.Trim() != strFiser) flgdiff = true;
                            //            sbsql.AppendLine($" ,f008vacc_maker1 		='{strFiser}'");
                            //            break;
                            //        case "2":
                            //            if (dataimport.f008vacc_maker1.Trim() != strAstra) flgdiff = true;
                            //            sbsql.AppendLine($" ,f008vacc_maker1 		='{strAstra}'");
                            //            break;
                            //        case "3":
                            //            if (dataimport.f008vacc_maker1.Trim() != strTakeda) flgdiff = true;
                            //            sbsql.AppendLine($" ,f008vacc_maker1 		='{strTakeda}'");
                            //            break;

                            //    }
                            //    //sbsql.AppendLine($" ,f008vacc_maker1='{app.FushoName3.Trim()}'");
                            //}
                            ////if (dataimport.f008vacc_maker1.Trim() != app.FushoName3.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f008vacc_maker1 		='{app.FushoName3.Trim()}'"); }

                            //if (dataimport.f009vacc_lot1.Trim() != app.FushoName4.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f009vacc_lot1 		='{app.FushoName4.Trim()}'"); }

                            //sbsql.AppendLine($" ,aid1 		='{app.Aid}'");

                            #endregion


                            //20220202185642 furukawa ed ////////////////////////


                            break;

                        case 2:

                            //20220202185800 furukawa st ////////////////////////
                            //提供データの券番号がない場合は新規入力(更新すべきレコード)と見なし、全項目入れる

                            if (dataimport.f012vacc_ticketnum2.Trim() == string.Empty)
                            {
                                flgdiff = true;
                                sbsql.AppendLine($" ,f010vacc_entrydate2		='{app.FushoName1.Trim()}'");
                                sbsql.AppendLine($" ,f011vacc_date2				='{app.FushoName2.Trim()}'");
                                sbsql.AppendLine($" ,f012vacc_ticketnum2		='{app.PersonName.Trim()}'");
                                sbsql.AppendLine($" ,f013vacc_insurercode2		='{app.InsNum.Trim()}'");
                                sbsql.AppendLine($" ,f014vacc_place2			='{app.ClinicName.Trim()}'");
                                sbsql.AppendLine($" ,f015vacc_doctor2			='{app.DrName.Trim()}'");

                                switch (app.FushoName3.Trim())
                                {
                                    case "1":
                                        sbsql.AppendLine($" ,f016vacc_maker2 		='{strFiser}'");
                                        break;
                                    case "2":
                                        sbsql.AppendLine($" ,f016vacc_maker2 		='{strAstra}'");
                                        break;
                                    case "3":
                                        sbsql.AppendLine($" ,f016vacc_maker2 		='{strTakeda}'");
                                        break;
                                    //20220608110534 furukawa st ////////////////////////
                                    //メーカー名追加
                                    
                                    case "4":
                                        sbsql.AppendLine($" ,f016vacc_maker2 		='{strFiser5}'");
                                        break;
                                    case "5":
                                        sbsql.AppendLine($" ,f016vacc_maker2 		='{strNova}'");
                                        break;
                                        //20220608110534 furukawa ed ////////////////////////
                                }
                                sbsql.AppendLine($" ,f017vacc_lot2='{app.FushoName4.Trim()}'");
                                sbsql.AppendLine($" ,aid2 		='{app.Aid}'");
                            }

                            #region old


                            ////if (dataimport.f010vacc_entrydate2 != app.FushoName1) { flgdiff = true; sbsql.AppendLine($" ,f010vacc_entrydate2='{app.FushoName1.Trim()}'"); }
                            //    if (dataimport.f011vacc_date2.Trim() != app.FushoName2.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f011vacc_date2='{app.FushoName2.Trim()}'"); }
                            //if (dataimport.f012vacc_ticketnum2.Trim() != app.PersonName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f012vacc_ticketnum2='{app.PersonName.Trim()}'"); }
                            //if (dataimport.f013vacc_insurercode2.Trim() != app.InsNum.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f013vacc_insurercode2='{app.InsNum.Trim()}'"); }
                            //if (dataimport.f014vacc_place2.Trim() != app.ClinicName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f014vacc_place2='{app.ClinicName.Trim()}'"); }
                            //if (dataimport.f015vacc_doctor2.Trim() != app.DrName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f015vacc_doctor2='{app.DrName.Trim()}'"); }

                            ////2022/01/21　メーカーが選択になったので対応する文字列に戻す
                            //if (dataimport.f016vacc_maker2.Trim() != app.FushoName3.Trim()) 
                            //{ 
                            //    //flgdiff = true;
                            //    switch (app.FushoName3.Trim())
                            //    {
                            //        case "1":
                            //            if (dataimport.f016vacc_maker2.Trim() != strFiser) flgdiff = true;
                            //            sbsql.AppendLine($" ,f016vacc_maker2 		='{strFiser}'");
                            //            break;
                            //        case "2":
                            //            if (dataimport.f016vacc_maker2.Trim() !=strAstra) flgdiff = true;
                            //            sbsql.AppendLine($" ,f016vacc_maker2 		='{strAstra}'");
                            //            break;
                            //        case "3":
                            //            if (dataimport.f016vacc_maker2.Trim() !=strTakeda) flgdiff = true;
                            //            sbsql.AppendLine($" ,f016vacc_maker2 		='{strTakeda}'");
                            //            break;

                            //    }
                            //    //sbsql.AppendLine($" ,f016vacc_maker2='{app.FushoName3.Trim()}'");
                            //}

                            //if (dataimport.f017vacc_lot2.Trim() != app.FushoName4.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f017vacc_lot2='{app.FushoName4.Trim()}'"); }
                            //sbsql.AppendLine($" ,aid2 		='{app.Aid}'");
                            #endregion

                            //20220202185800 furukawa ed ////////////////////////


                            break;

                        case 3:

                            //20220202185854 furukawa st ////////////////////////
                            //提供データの券番号がない場合は新規入力(更新すべきレコード)と見なし、全項目入れる

                            if (dataimport.f020vacc_ticketnum3.Trim() == string.Empty)
                            {
                                flgdiff = true;

                                sbsql.AppendLine($" ,f018vacc_entrydate3		='{app.FushoName1.Trim()}'");
                                sbsql.AppendLine($" ,f019vacc_date3				='{app.FushoName2.Trim()}'");
                                sbsql.AppendLine($" ,f020vacc_ticketnum3		='{app.PersonName.Trim()}'");
                                sbsql.AppendLine($" ,f021vacc_insurercode3		='{app.InsNum.Trim()}'");
                                sbsql.AppendLine($" ,f022vacc_place3			='{app.ClinicName.Trim()}'");
                                sbsql.AppendLine($" ,f023vacc_doctor3			='{app.DrName.Trim()}'");

                                switch (app.FushoName3.Trim())
                                {
                                    case "1":
                                        sbsql.AppendLine($" ,f024vacc_maker3 		='{strFiser}'");
                                        break;
                                    case "2":
                                        sbsql.AppendLine($" ,f024vacc_maker3 		='{strAstra}'");
                                        break;
                                    case "3":
                                        sbsql.AppendLine($" ,f024vacc_maker3 		='{strTakeda}'");
                                        break;
                                    //20220608110556 furukawa st ////////////////////////
                                    //メーカー名追加
                                    
                                    case "4":
                                        sbsql.AppendLine($" ,f024vacc_maker3 		='{strFiser5}'");
                                        break;
                                    case "5":
                                        sbsql.AppendLine($" ,f024vacc_maker3 		='{strNova}'");
                                        break;
                                        //20220608110556 furukawa ed ////////////////////////
                                }
                                sbsql.AppendLine($" ,f025vacc_lot3='{app.FushoName4.Trim()}'");
                                sbsql.AppendLine($" ,aid3 		='{app.Aid}'");
                            }




                            #region old
                            //if (dataimport.f018vacc_entrydate3 != app.FushoName1) { flgdiff = true; sbsql.AppendLine($" ,f018vacc_entrydate3='{app.FushoName1.Trim()}'"); }
                            //if (dataimport.f019vacc_date3.Trim() != app.FushoName2.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f019vacc_date3='{app.FushoName2.Trim()}'"); }
                            //if (dataimport.f020vacc_ticketnum3.Trim() != app.PersonName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f020vacc_ticketnum3='{app.PersonName.Trim()}'"); }
                            //if (dataimport.f021vacc_insurercode3.Trim() != app.InsNum.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f021vacc_insurercode3='{app.InsNum.Trim()}'"); }
                            //if (dataimport.f022vacc_place3.Trim() != app.ClinicName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f022vacc_place3='{app.ClinicName.Trim()}'"); }
                            //if (dataimport.f023vacc_doctor3.Trim() != app.DrName.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f023vacc_doctor3='{app.DrName.Trim()}'"); }

                            ////2022/01/21　メーカーが選択になったので対応する文字列に戻す
                            //if (dataimport.f024vacc_maker3.Trim() != app.FushoName3.Trim()) 
                            //{
                            //      flgdiff = true;
                            //    switch (app.FushoName3.Trim())
                            //    {
                            //        case "1":
                            //            if(dataimport.f024vacc_maker3.Trim()!= strFiser) flgdiff = true;
                            //            sbsql.AppendLine($" ,f024vacc_maker3 		='{strFiser}'");
                            //            break;
                            //        case "2":
                            //            if (dataimport.f024vacc_maker3.Trim() != strAstra) flgdiff = true;
                            //            sbsql.AppendLine($" ,f024vacc_maker3 		='{strAstra}'");
                            //            break;
                            //        case "3":
                            //            if (dataimport.f024vacc_maker3.Trim() != strTakeda) flgdiff = true;
                            //            sbsql.AppendLine($" ,f024vacc_maker3 		='{strTakeda}'");
                            //            break;

                            //    }
                            //    //    sbsql.AppendLine($" ,f024vacc_maker3='{app.FushoName3.Trim()}'");
                            //}


                            //if (dataimport.f025vacc_lot3.Trim() != app.FushoName4.Trim()) { flgdiff = true; sbsql.AppendLine($" ,f025vacc_lot3='{app.FushoName4.Trim()}'"); }
                            //sbsql.AppendLine($" ,aid3 		='{app.Aid}'"); 
                            #endregion


                            //20220202185854 furukawa ed ////////////////////////

                            break;

                    }

                    if (flgdiff)
                    {
                        //差異フラグtrueの場合に更新処理

                        sbsql.AppendLine($" where f001number='{app.Numbering}' ");

                        switch (verifyBoxTimes.Text)
                        {
                            case "1":
                                sbsql.AppendLine($" and f004vacc_ticketnum1='{verifyBoxTicket.Text.Trim()}'");
                                break;
                            case "2":
                                sbsql.AppendLine($" and f012vacc_ticketnum2='{verifyBoxTicket.Text.Trim()}'");
                                break;
                            case "3":
                                sbsql.AppendLine($" and f020vacc_ticketnum3='{verifyBoxTicket.Text.Trim()}'");
                                break;
                        }


                        //dataexport2更新処理
                        using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(), tran))
                        {
                            cmd.TryExecuteNonQuery();
                        }

                    }

                    #endregion
                }


                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }

        //20220204153524 furukawa st ////////////////////////
        //バッチに対して追加画像を紐付け
        
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (verifyBoxAID.Text.Trim() == string.Empty)
            {
                MessageBox.Show("AIDを指定してください");
                return;
            }

            if (MessageBox.Show("画像をこのバッチに紐付けします。よろしいですか？", 
                Application.ProductName,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                MessageBox.Show("画像紐付を中止します");
                return;
            }


            StringBuilder sb = new StringBuilder();
            sb.AppendLine($" update application set ");
            sb.AppendLine($" memo=memo || " +
                $"' 変更前NoB:' || hnum || " +
                $"' 変更前バッチ:' || hzip || " +
                $"' 変更前医療機関等コード:' || sid || " +
                $"' 変更日:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', ");

            sb.AppendLine($" hnum='{verifyBoxNOB.Text.Trim()}', ");
            sb.AppendLine($" hzip='{verifyBoxBatchNo.Text.Trim()}', ");
            sb.AppendLine($" sid='{verifyBoxHosNum.Text.Trim()}', ");
            sb.AppendLine($" ayear='{verifyBoxY.Text.Trim()}', ");
            sb.AppendLine($" amonth='{verifyBoxM.Text.Trim()}', ");
            
            int jym = int.Parse(verifyBoxY.Text.Trim()) * 100 + int.Parse(verifyBoxM.Text.Trim());
            int ym = DateTimeEx.GetAdYearFromHs(jym)*100+ int.Parse(verifyBoxM.Text.Trim()); 

            sb.AppendLine($" ym={ym} ");

            sb.AppendLine($" where aid={verifyBoxAID.Text.Trim()} ");
            

            using (DB.Command cmd = new DB.Command(DB.Main, sb.ToString()))
            {
                if (!cmd.TryExecuteNonQuery())
                {
                    MessageBox.Show("画像紐付失敗。管理者に連絡してください", 
                        Application.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    MessageBox.Show("画像紐付けしました");
                    return;
                }

            }


        }
        //20220204153524 furukawa ed ////////////////////////




        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }


            bsApp.MoveNext();
        }

      

        #endregion

        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
               
        

    

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

      

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;
            
            if (ticketConts.Contains(sender))
            {
                p = posTicket;
                scrollPictureControl1.Ratio = 0.7f;
            }
            else if (twoweekConts.Contains(sender))
            {
                p = pos2w;
                scrollPictureControl1.Ratio = 0.4f;
            }
            else if (lotConts.Contains(sender))
            {
                p = posLot;
                scrollPictureControl1.Ratio = 0.8f;
            }
            else if (placeConts.Contains(sender))
            {
                p = posPlace;
                scrollPictureControl1.Ratio = 0.6f;
            }
            else if (NoBConts.Contains(sender))
            {
                p = posNOB;
                scrollPictureControl1.Ratio = 0.4f;
            }
            else if (ymConts.Contains(sender))
            {
                p = posYM;
                scrollPictureControl1.Ratio = 0.4f;
            }
            else if (hyoConts.Contains(sender))
            {//ヘッダの表
                p = posHyo;
                scrollPictureControl1.Ratio = 0.4f;
            }
            

            else return;
            
            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;


            if (ticketConts.Any(c => c.Focused)) posTicket = pos;
            else if (twoweekConts.Any(c => c.Focused)) pos2w = pos;
            else if (lotConts.Any(c => c.Focused)) posLot = pos;
            else if (placeConts.Any(c => c.Focused)) posPlace = pos;
            else if (NoBConts.Any(c => c.Focused)) posNOB = pos;
            else if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hyoConts.Any(c => c.Focused)) posHyo = pos;



        }
        #endregion


    
       
    }      
}
