﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Toyonakashi_covid19
{
    public class chargelist
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int importid { get; set; } = 0;

        public int no { get; set; } = 0;                                      //記載順序;
        public string hosnum { get; set; } = string.Empty;                    //医療機関等コード;
        public string nob { get; set; } = string.Empty;                       //豊中市独自の番号;
        public string hosname { get; set; } = string.Empty;                   //医療機関名;
        public int ty6d { get; set; } = 0;                                    //【通常】予診のみ６歳未満;
        public int ty6u { get; set; } = 0;                                    //【通常】予診のみ６歳以上;
        public int ts6d { get; set; } = 0;                                    //【通常】接種６歳未満;
        public int ts6u { get; set; } = 0;                                    //【通常】接種６歳以上;
        public int ty6dk { get; set; } = 0;                                   //【通常】予診のみ６歳未満金額;
        public int ty6uk { get; set; } = 0;                                   //【通常】予診のみ６歳以上金額;
        public int ts6dk { get; set; } = 0;                                   //【通常】接種６歳未満金額;
        public int ts6uk { get; set; } = 0;                                   //【通常】接種６歳以上金額;
        public int tcharge { get; set; } = 0;                                 //【通常】請求金額;
        public int gy6d { get; set; } = 0;                                    //【時間外】予診のみ６歳未満;
        public int gy6u { get; set; } = 0;                                    //【時間外】予診のみ６歳以上;
        public int gs6d { get; set; } = 0;                                    //【時間外】接種６歳未満;
        public int gs6u { get; set; } = 0;                                    //【時間外】接種６歳以上;
        public int gy6dk { get; set; } = 0;                                   //【時間外】予診のみ６歳未満金額;
        public int gy6uk { get; set; } = 0;                                   //【時間外】予診のみ６歳以上金額;
        public int gs6dk { get; set; } = 0;                                   //【時間外】接種６歳未満金額;
        public int gs6uk { get; set; } = 0;                                   //【時間外】接種６歳以上金額;
        public int gcharge { get; set; } = 0;                                 //【時間外】請求金額;
        public int ky6d { get; set; } = 0;                                    //【休日】予診のみ６歳未満;
        public int ky6u { get; set; } = 0;                                    //【休日】予診のみ６歳以上;
        public int ks6d { get; set; } = 0;                                    //【休日】接種６歳未満;
        public int ks6u { get; set; } = 0;                                    //【休日】接種６歳以上;
        public int ky6dk { get; set; } = 0;                                   //【休日】予診のみ６歳未満金額;
        public int ky6uk { get; set; } = 0;                                   //【休日】予診のみ６歳以上金額;
        public int ks6dk { get; set; } = 0;                                   //【休日】接種６歳未満金額;
        public int ks6uk { get; set; } = 0;                                   //【休日】接種６歳以上金額;
        public int kcharge { get; set; } = 0;                                 //【休日】請求金額;
        public int fy6d { get; set; } = 0;                                    //【不明】予診のみ６歳未満メディ独自;
        public int fy6u { get; set; } = 0;                                    //【不明】予診のみ６歳以上メディ独自;
        public int fs6d { get; set; } = 0;                                    //【不明】接種６歳未満メディ独自;
        public int fs6u { get; set; } = 0;                                    //【不明】接種６歳以上メディ独自;
        public int fy6dk { get; set; } = 0;                                   //【不明】予診のみ６歳未満金額メディ独自;
        public int fy6uk { get; set; } = 0;                                   //【不明】予診のみ６歳以上金額メディ独自;
        public int fs6dk { get; set; } = 0;                                   //【不明】接種６歳未満金額メディ独自;
        public int fs6uk { get; set; } = 0;                                   //【不明】接種６歳以上金額メディ独自;
        public int fcharge { get; set; } = 0;                                 //【不明】請求金額メディ独自;
        public int chargetotal { get; set; } = 0;                             //請求金額合計;
        public string hoszip { get; set; } = string.Empty;                    //郵便番号;
        public string hosaddress { get; set; } = string.Empty;                //住所;                                                                                                                                                                                                             
        public int cym { get; set; } = 0;                                     //20220210164126 furukawa cym追加


        #endregion
    }
}
