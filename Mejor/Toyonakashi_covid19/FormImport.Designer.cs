﻿namespace Mejor.Toyonakashi_covid19
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewRefrece = new System.Windows.Forms.DataGridView();
            this.dataGridViewKokuho = new System.Windows.Forms.DataGridView();
            this.buttonExcel = new System.Windows.Forms.Button();
            this.buttonCSV = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonDel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRefrece)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKokuho)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewRefrece
            // 
            this.dataGridViewRefrece.AllowUserToAddRows = false;
            this.dataGridViewRefrece.AllowUserToDeleteRows = false;
            this.dataGridViewRefrece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRefrece.Location = new System.Drawing.Point(52, 13);
            this.dataGridViewRefrece.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewRefrece.Name = "dataGridViewRefrece";
            this.dataGridViewRefrece.Size = new System.Drawing.Size(363, 131);
            this.dataGridViewRefrece.TabIndex = 0;
            this.dataGridViewRefrece.Visible = false;
            // 
            // dataGridViewKokuho
            // 
            this.dataGridViewKokuho.AllowUserToAddRows = false;
            this.dataGridViewKokuho.AllowUserToDeleteRows = false;
            this.dataGridViewKokuho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewKokuho.Location = new System.Drawing.Point(52, 208);
            this.dataGridViewKokuho.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewKokuho.Name = "dataGridViewKokuho";
            this.dataGridViewKokuho.Size = new System.Drawing.Size(363, 131);
            this.dataGridViewKokuho.TabIndex = 0;
            // 
            // buttonExcel
            // 
            this.buttonExcel.Location = new System.Drawing.Point(457, 13);
            this.buttonExcel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(156, 64);
            this.buttonExcel.TabIndex = 1;
            this.buttonExcel.Text = "1.請求書Excel\r\nインポート";
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttonExcel_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Location = new System.Drawing.Point(457, 232);
            this.buttonCSV.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(156, 67);
            this.buttonCSV.TabIndex = 1;
            this.buttonCSV.Text = "3.CSV\r\nインポート";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(457, 105);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 64);
            this.button1.TabIndex = 1;
            this.button1.Text = "2.請求書（職域）Excel\r\nインポート";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonDel
            // 
            this.buttonDel.Location = new System.Drawing.Point(488, 319);
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(95, 40);
            this.buttonDel.TabIndex = 2;
            this.buttonDel.Text = "削除";
            this.buttonDel.UseVisualStyleBackColor = true;
            this.buttonDel.Click += new System.EventHandler(this.buttonDel_Click);
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 378);
            this.Controls.Add(this.buttonDel);
            this.Controls.Add(this.buttonCSV);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonExcel);
            this.Controls.Add(this.dataGridViewKokuho);
            this.Controls.Add(this.dataGridViewRefrece);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormImport";
            this.Text = "FormImport";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRefrece)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKokuho)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewRefrece;
        private System.Windows.Forms.DataGridView dataGridViewKokuho;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.Button buttonCSV;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonDel;
    }
}