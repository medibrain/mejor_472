﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mejor.Toyonakashi_covid19
{
   
    public class dataimport
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int importid { get;set; }=0; 

        public string f001number { get; set; } = string.Empty;                //番号 頭が7で始まる場合は10桁、それ以外は8桁の固定長。（ゼロ埋めあり）;
        public string f002vacc_entrydate1 { get; set; } = string.Empty;       //1回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f003vacc_date1 { get; set; } = string.Empty;            //1回目接種日 YYYYMMDD（Yは西暦）;
        public string f004vacc_ticketnum1 { get; set; } = string.Empty;       //1回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f005vacc_insurercode1 { get; set; } = string.Empty;     //1回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f006vacc_place1 { get; set; } = string.Empty;           //1回目接種会場名 ;
        public string f007vacc_doctor1 { get; set; } = string.Empty;          //1回目接種医師名 ;
        public string f008vacc_maker1 { get; set; } = string.Empty;           //1回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f009vacc_lot1 { get; set; } = string.Empty;             //1回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f010vacc_entrydate2 { get; set; } = string.Empty;       //2回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f011vacc_date2 { get; set; } = string.Empty;            //2回目接種日 YYYYMMDD（Yは西暦）;
        public string f012vacc_ticketnum2 { get; set; } = string.Empty;       //2回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f013vacc_insurercode2 { get; set; } = string.Empty;     //2回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f014vacc_place2 { get; set; } = string.Empty;           //2回目接種会場名 ;
        public string f015vacc_doctor2 { get; set; } = string.Empty;          //2回目接種医師名 ;
        public string f016vacc_maker2 { get; set; } = string.Empty;           //2回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f017vacc_lot2 { get; set; } = string.Empty;             //2回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f018vacc_entrydate3 { get; set; } = string.Empty;       //3回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f019vacc_date3 { get; set; } = string.Empty;            //3回目接種日 YYYYMMDD（Yは西暦）;
        public string f020vacc_ticketnum3 { get; set; } = string.Empty;       //3回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f021vacc_insurercode3 { get; set; } = string.Empty;     //3回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f022vacc_place3 { get; set; } = string.Empty;           //3回目接種会場名 ;
        public string f023vacc_doctor3 { get; set; } = string.Empty;          //3回目接種医師名 ;
        public string f024vacc_maker3 { get; set; } = string.Empty;           //3回目ワクチンメーカー 「ファイザー」「アストラゼネカ」「武田／モデルナ」のみ;
        public string f025vacc_lot3 { get; set; } = string.Empty;             //3回目ワクチンロット番号 数値はゼロ埋めなし;
        //20220816_1 ito st /////
        public string f026vacc_entrydate4 { get; set; } = string.Empty;       //4回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f027vacc_date4 { get; set; } = string.Empty;            //4回目接種日 YYYYMMDD（Yは西暦）;
        public string f028vacc_ticketnum4 { get; set; } = string.Empty;       //4回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f029vacc_insurercode4 { get; set; } = string.Empty;     //4回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f030vacc_place4 { get; set; } = string.Empty;           //4回目接種会場名 ;
        public string f031vacc_doctor4 { get; set; } = string.Empty;          //4回目接種医師名 ;
        public string f032vacc_maker4 { get; set; } = string.Empty;           //4回目ワクチンメーカー ;
        public string f033vacc_lot4 { get; set; } = string.Empty;             //4回目ワクチンロット番号 数値はゼロ埋めなし;
        public string f034vacc_entrydate5 { get; set; } = string.Empty;       //5回目接種履歴登録日時 YYYYMMDDHHMMSS（Yは西暦、HHは24時間表記）;
        public string f035vacc_date5 { get; set; } = string.Empty;            //5回目接種日 YYYYMMDD（Yは西暦）;
        public string f036vacc_ticketnum5 { get; set; } = string.Empty;       //5回目券番号 10桁の固定長。（ゼロ埋めあり）;
        public string f037vacc_insurercode5 { get; set; } = string.Empty;     //5回目自治体コード 6桁の固定長。（ゼロ埋めあり）;
        public string f038vacc_place5 { get; set; } = string.Empty;           //5回目接種会場名 ;
        public string f039vacc_doctor5 { get; set; } = string.Empty;          //5回目接種医師名 ;
        public string f040vacc_maker5 { get; set; } = string.Empty;           //5回目ワクチンメーカー ;
        public string f041vacc_lot5 { get; set; } = string.Empty;             //5回目ワクチンロット番号 数値はゼロ埋めなし;
        //20220816_1 ito ed /////
        public int cym { get; set; } = 0;                                     //メホール請求年月;
        public string sid { get; set; } = string.Empty;                       //医療機関;
        public DateTime importdate { get; set; } = DateTime.MinValue;         //インポート日時;
        public DateTime updatedate { get; set; } = DateTime.MinValue;         //更新日時;


        #endregion

        public static List<dataimport> lstclsData = new List<dataimport>();


        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool ImportMain(int _cym)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            
            try
            {
                if(!ImportCsvFile(wf, _cym)) return false;
                
                System.Windows.Forms.MessageBox.Show("取込完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("取込失敗");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }



        /// <summary>
        /// db登録
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryDB(WaitForm wf, int _cym)
        {

            wf.InvokeValue = 0;
            wf.SetMax(lstclsData.Count);
            DB.Transaction tran = new DB.Transaction(DB.Main);

            try
            {
                wf.LogPrint("DB削除");
                DB.Command cmd = new DB.Command($"delete from dataimport where cym={_cym}", tran);
                cmd.TryExecuteNonQuery();

                wf.LogPrint("DB登録");
                foreach (dataimport cls in lstclsData)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }

                    if (!DB.Main.Insert<dataimport>(cls, tran)) return false;
                    wf.LogPrint($"券番号：{cls.f001number}");
                    wf.InvokeValue++;
                }

                tran.Commit();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
            }
        }

        private static int ChangeToADYM(string strtmp)
        {
            return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
        }

        /// <summary>
        /// 20220216104114 furukawa　BeginTextImportで取り込む        
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private static bool DirectImport(string strFileName)
        {
            //20220816_1 ito st /////
            StreamReader sr = new StreamReader(strFileName);
            string csvstr = sr.ReadLine();
            string[] csvstrAry = csvstr.Split(',');
            int dataType = 3;
            if (csvstrAry.Length == 33) dataType = 4;
            else if (csvstrAry.Length == 41) dataType = 5;
            //20220816_1 ito end /////

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"copy dataimport(");
            sb.AppendLine($"f001number,");
            sb.AppendLine($"f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1,");
            sb.AppendLine($"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2,");
            sb.AppendLine($"f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3");
            //20220816_1 ito st /////
            if (dataType >= 4)
                sb.AppendLine($",f026vacc_entrydate4,f027vacc_date4,f028vacc_ticketnum4,f029vacc_insurercode4,f030vacc_place4,f031vacc_doctor4,f032vacc_maker4,f033vacc_lot4");
            if (dataType == 5)
                sb.AppendLine($",f034vacc_entrydate5,f035vacc_date5,f036vacc_ticketnum5,f037vacc_insurercode5,f038vacc_place5,f039vacc_doctor5,f040vacc_maker5,f041vacc_lot5");
            //20220816_1 ito ed /////
            sb.AppendLine($" ) from stdin with csv encoding 'utf8'");

            if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
            {
                System.Windows.Forms.MessageBox.Show("CSV取込失敗", 
                    System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }

            return true;

        }

        /// <summary>
        /// csvをcopyで取り込む
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="cym"></param>
        private static bool ImportCsvFile(WaitForm wf,int cym)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            
            try
            {
                dlg.Multiselect = true;
                dlg.Filter = "csv|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

                string[] arrstrFileName = dlg.FileNames;

                //20220216103843 furukawa st ////////////////////////
                //waitform表示修正
                
                
                wf.SetMax(arrstrFileName.Length);
                wf.InvokeValue = 0;
                //wf.ShowDialogOtherTask();
                
                //20220216103843 furukawa ed ////////////////////////



                //20220210171304 furukawa st ////////////////////////
                //削除をやめる

                //wf.LogPrint("db削除");
                //using (DB.Command cmd = DB.Main.CreateCmd("truncate table dataimport;"))                
                //{
                //    cmd.TryExecuteNonQuery();
                //}
                //20220210171304 furukawa ed ////////////////////////

                foreach (string strFileName in arrstrFileName)
                {
                    wf.LogPrint($"{strFileName} 取込中...");
                    wf.InvokeValue++;

                    //20220216104033 furukawa st ////////////////////////
                    //CSV取込方法をBeginTextImportに変更

                    if (!DirectImport(strFileName)) return false;
                    //      CreateAndRunBatch(strFileName);
                    //20220216104033 furukawa ed ////////////////////////

                }

                wf.LogPrint("インポート日時、更新日時　更新");
                using (DB.Command cmd = DB.Main.CreateCmd($"update dataimport set " +
                    $"importdate='{DateTime.Now.ToString("yyyy-MM-dd")}'" +
                    $",updatedate='{DateTime.MinValue}'" +
                    //20220210172952 furukawa st ////////////////////////
                    //更新日はcymがないレコードのみ

                    $",cym={cym} " +
                    $" where cym=0;"))
                //      $",cym={cym};"))
                //20220210172952 furukawa ed ////////////////////////
                {
                    cmd.TryExecuteNonQuery();
                }




                //20220316160315 furukawa st ////////////////////////
                //exprot.csでやるので不要


                //      //更新分レコード用にdataexport2に先にデータを入れておく

                //      StringBuilder sbsql = new StringBuilder();

                //      //20220211112817 furukawa st ////////////////////////
                //      //同月分を先に削除

                //      wf.LogPrint("dataexport2削除");
                //      sbsql.AppendLine($"delete from dataexport2 where cym={cym}");

                //      using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString()))
                //      {
                //          cmd.TryExecuteNonQuery();
                //      }
                //      //20220211112817 furukawa ed ////////////////////////


                //      wf.LogPrint("dataexport2登録");
                //      sbsql.Remove(0, sbsql.ToString().Length);
                //      sbsql.AppendLine($"INSERT INTO dataexport2( ");
                //      sbsql.AppendLine($" f001number, ");
                //      sbsql.AppendLine($" f002vacc_entrydate1, ");
                //      sbsql.AppendLine($" f003vacc_date1, ");
                //      sbsql.AppendLine($" f004vacc_ticketnum1, ");
                //      sbsql.AppendLine($" f005vacc_insurercode1, ");
                //      sbsql.AppendLine($" f006vacc_place1, ");
                //      sbsql.AppendLine($" f007vacc_doctor1, ");
                //      sbsql.AppendLine($" f008vacc_maker1, ");
                //      sbsql.AppendLine($" f009vacc_lot1, ");
                //      sbsql.AppendLine($" f010vacc_entrydate2, ");
                //      sbsql.AppendLine($" f011vacc_date2, ");
                //      sbsql.AppendLine($" f012vacc_ticketnum2, ");
                //      sbsql.AppendLine($" f013vacc_insurercode2, ");
                //      sbsql.AppendLine($" f014vacc_place2, ");
                //      sbsql.AppendLine($" f015vacc_doctor2, ");
                //      sbsql.AppendLine($" f016vacc_maker2, ");
                //      sbsql.AppendLine($" f017vacc_lot2, ");
                //      sbsql.AppendLine($" f018vacc_entrydate3, ");
                //      sbsql.AppendLine($" f019vacc_date3, ");
                //      sbsql.AppendLine($" f020vacc_ticketnum3, ");
                //      sbsql.AppendLine($" f021vacc_insurercode3, ");
                //      sbsql.AppendLine($" f022vacc_place3, ");
                //      sbsql.AppendLine($" f023vacc_doctor3, ");
                //      sbsql.AppendLine($" f024vacc_maker3, ");
                //      sbsql.AppendLine($" f025vacc_lot3, ");
                //      sbsql.AppendLine($" cym  ,");
                //      sbsql.AppendLine($" importdate ");
                //      sbsql.AppendLine($" ) ");
                //      sbsql.AppendLine($" select  ");
                //      sbsql.AppendLine($" f001number, ");
                //      sbsql.AppendLine($" f002vacc_entrydate1, ");
                //      sbsql.AppendLine($" f003vacc_date1, ");
                //      sbsql.AppendLine($" f004vacc_ticketnum1, ");
                //      sbsql.AppendLine($" f005vacc_insurercode1, ");
                //      sbsql.AppendLine($" f006vacc_place1, ");
                //      sbsql.AppendLine($" f007vacc_doctor1, ");
                //      sbsql.AppendLine($" f008vacc_maker1, ");
                //      sbsql.AppendLine($" f009vacc_lot1, ");
                //      sbsql.AppendLine($" f010vacc_entrydate2, ");
                //      sbsql.AppendLine($" f011vacc_date2, ");
                //      sbsql.AppendLine($" f012vacc_ticketnum2, ");
                //      sbsql.AppendLine($" f013vacc_insurercode2, ");
                //      sbsql.AppendLine($" f014vacc_place2, ");
                //      sbsql.AppendLine($" f015vacc_doctor2, ");
                //      sbsql.AppendLine($" f016vacc_maker2, ");
                //      sbsql.AppendLine($" f017vacc_lot2, ");
                //      sbsql.AppendLine($" f018vacc_entrydate3, ");
                //      sbsql.AppendLine($" f019vacc_date3, ");
                //      sbsql.AppendLine($" f020vacc_ticketnum3, ");
                //      sbsql.AppendLine($" f021vacc_insurercode3, ");
                //      sbsql.AppendLine($" f022vacc_place3, ");
                //      sbsql.AppendLine($" f023vacc_doctor3, ");
                //      sbsql.AppendLine($" f024vacc_maker3, ");
                //      sbsql.AppendLine($" f025vacc_lot3, ");
                //      sbsql.AppendLine($" cym, ");
                //      sbsql.AppendLine($" importdate ");
                //      sbsql.AppendLine($" from dataimport  ");
                //      sbsql.AppendLine($" where cym={cym} ");

                //      //20220202185528 furukawa st ////////////////////////
                //      //「更新」は券番号が提供データになく、新規で入力したレコードだけなので券番号がないレコードのみをdataexport2に追加

                //      sbsql.AppendLine($" and (f004vacc_ticketnum1 is null ");
                //      sbsql.AppendLine($" or f012vacc_ticketnum2 is null ");
                //      sbsql.AppendLine($" or f020vacc_ticketnum3 is null )");
                //      //20220202185528 furukawa ed ////////////////////////


                //      using (DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString()))
                //      {
                //          cmd.TryExecuteNonQuery();
                //      }


                //20220316160315 furukawa ed ////////////////////////



                wf.LogPrint("終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return false;
            }
            finally
            {
                
            }
        }

        /// <summary>
        /// psqlでcopyするためのバッチを作成/実行
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private static bool CreateAndRunBatch(string strFileName)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("toyonakashi_covid19_batch.bat",false,
                System.Text.Encoding.GetEncoding("shift-jis"));
            try
            {
                //20220816_1 ito st /////
                //string strCmd = $"\\copy dataimport(" +
                //           $"f001number,f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1," +
                //       $"f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1," +
                //       $"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2," +
                //       $"f016vacc_maker2,f017vacc_lot2,f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3," +
                //       $"f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3) from " +
                //       $"'{strFileName.Replace("\\", "\\\\")}' with csv encoding 'utf8'";

                string strCmd = $"\\copy dataimport(" +
                       $"f001number," +
                       $"f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1," +
                       $"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2," +
                       $"f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3," +
                       $"f026vacc_entrydate4,f027vacc_date4,f028vacc_ticketnum4,f029vacc_insurercode4,f030vacc_place4,f031vacc_doctor4,f032vacc_maker4,f033vacc_lot4," +
                       $"f034vacc_entrydate5,f035vacc_date5,f036vacc_ticketnum5,f037vacc_insurercode5,f038vacc_place5,f039vacc_doctor5,f040vacc_maker5,f041vacc_lot5" +
                       $" ) from '{strFileName.Replace("\\", "\\\\")}' with csv encoding 'utf8'";

                //20220816_1 ito end /////

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("@echo on");

                //メホールにpsqlフォルダと必要dll入れたのでそこから

                //sb.AppendLine(" SET PATHPOS1 = c:\\program files\\postgresql\\11\\bin");
                //sb.AppendLine(" SET PATHPOS2 = d:\\program files\\postgresql\\11\\bin");
                //sb.AppendLine(" if exist %PATHPOS1% (cd /d %PATHPOS1%)");
                //sb.AppendLine(" if exist %PATHPOS2% (cd /d %PATHPOS2%)");

                sb.AppendLine(" SET PATHPSQL=%~dp0\\psql ");
                sb.AppendLine(" if exist %PATHPSQL% (cd /d %PATHPSQL%)");

                sb.Append($"psql -h {Settings.DataBaseHost} ");// mejor-test.cbr12l83psj3.ap-northeast-1.rds.amazonaws.com ");
                sb.Append($" -p {Settings.dbPort} -U {Settings.JyuseiDBUser} -d toyonakashi_covid19 --command \"");
                sb.AppendLine(strCmd + "\"");


                sw.WriteLine(sb.ToString());
                sw.Close();
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = "toyonakashi_covid19_batch.bat";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                p.WaitForExit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);

                return false;
            }
            
        }


        /// <summary>
        /// csvをロードする
        /// </summary>
        /// <returns></returns>
        private static bool LoadFile(WaitForm wf, int _cym)
        {

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            
            try
            {
                dlg.Multiselect = true;
                dlg.Filter = "csv|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;
                
                string[] arrstrFileName = dlg.FileNames;
                lstclsData.Clear();


                foreach (string strFileName in arrstrFileName)
                {

                    //CSV内容ロード
                    List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                    wf.LogPrint($"{strFileName} ロード");
                    wf.SetMax(lst.Count);
                    wf.InvokeValue = 0;
                    

                    //クラスのリストに登録
                    foreach (string[] tmp in lst)
                    {

                        dataimport cls = new dataimport();

                        cls.f001number = tmp[0];
                        cls.f002vacc_entrydate1 = tmp[1];
                        cls.f003vacc_date1 = tmp[2];
                        cls.f004vacc_ticketnum1 = tmp[3];
                        cls.f005vacc_insurercode1 = tmp[4];
                        cls.f006vacc_place1 = tmp[5];
                        cls.f007vacc_doctor1 = tmp[6];
                        cls.f008vacc_maker1 = tmp[7];
                        cls.f009vacc_lot1 = tmp[8];
                        cls.f010vacc_entrydate2 = tmp[9];
                        cls.f011vacc_date2 = tmp[10];
                        cls.f012vacc_ticketnum2 = tmp[11];
                        cls.f013vacc_insurercode2 = tmp[12];
                        cls.f014vacc_place2 = tmp[13];
                        cls.f015vacc_doctor2 = tmp[14];
                        cls.f016vacc_maker2 = tmp[15];
                        cls.f017vacc_lot2 = tmp[16];
                        cls.f018vacc_entrydate3 = tmp[17];
                        cls.f019vacc_date3 = tmp[18];
                        cls.f020vacc_ticketnum3 = tmp[19];
                        cls.f021vacc_insurercode3 = tmp[20];
                        cls.f022vacc_place3 = tmp[21];
                        cls.f023vacc_doctor3 = tmp[22];
                        cls.f024vacc_maker3 = tmp[23];
                        cls.f025vacc_lot3 = tmp[24];

                        //20220816_1 ito st /////
                        cls.f026vacc_entrydate4 =   tmp[25];
                        cls.f027vacc_date4 =        tmp[26];
                        cls.f028vacc_ticketnum4 =   tmp[27];
                        cls.f029vacc_insurercode4 = tmp[28];
                        cls.f030vacc_place4 =       tmp[29];
                        cls.f031vacc_doctor4 =      tmp[30];
                        cls.f032vacc_maker4 =       tmp[31];
                        cls.f033vacc_lot4 =         tmp[32];
                        cls.f034vacc_entrydate5 =   tmp[33];
                        cls.f035vacc_date5 =        tmp[34];
                        cls.f036vacc_ticketnum5 =   tmp[35];
                        cls.f037vacc_insurercode5 = tmp[36];
                        cls.f038vacc_place5 =       tmp[37];
                        cls.f039vacc_doctor5 =      tmp[38];
                        cls.f040vacc_maker5 =       tmp[39];
                        cls.f041vacc_lot5 =         tmp[40];
                        //20220816_1 ito ed /////

                        cls.cym = _cym; //メホール請求年月;

                        cls.importdate = DateTime.Now;
                        cls.updatedate = DateTime.MinValue;


                        lstclsData.Add(cls);

                        wf.InvokeValue++;

                    }

                }
                wf.LogPrint("CSVロード終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 20220303151122 furukawa 不要データ削除ボタン用        
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool deleteCYM(int cym)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.Clear();
                sb.AppendLine($" select a.numbering ");
                sb.AppendLine($" from application a inner join dataimport d on ");
                sb.AppendLine($" a.numbering=d.f001number and a.cym=d.cym ");
                sb.AppendLine($" where a.cym={cym} ");

                DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
                var l = cmd.TryExecuteScalar();
                if (l != null)
                {
                    System.Windows.Forms.MessageBox.Show("このデータはすでに予診票に紐付いています。削除するには管理者まで連絡ください",
                        System.Windows.Forms.Application.ProductName,
                        System.Windows.Forms.MessageBoxButtons.OK,
                        System.Windows.Forms.MessageBoxIcon.Exclamation);
                    cmd.Dispose();
                    return false;
                }

                if (System.Windows.Forms.MessageBox.Show("削除します。よろしいですか？",
                    System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.YesNo,
                    System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;


                sb.Clear();
                sb.AppendLine($"delete from dataimport where cym={cym}");
                cmd = DB.Main.CreateCmd(sb.ToString());
                cmd.TryExecuteNonQuery();
                System.Windows.Forms.MessageBox.Show("削除しました");


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

            }
        }


        /// <summary>
        /// 券番号で1レコード呼び出す
        /// </summary>
        /// <param name="strNum"></param>
        /// <returns></returns>
        public static dataimport select (string strNum,int cym)
        //public static dataimport select (string strNum)
        {
            //データ取得
            //20220816_1 ito st /////
            using (DB.Command cmd = new DB.Command(DB.Main, $"select importid, f001number," +
                $"f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1," +
                $"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2," +
                $"f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3," +
                $"f026vacc_entrydate4,f027vacc_date4,f028vacc_ticketnum4,f029vacc_insurercode4,f030vacc_place4,f031vacc_doctor4,f032vacc_maker4,f033vacc_lot4," +
                $"f034vacc_entrydate5,f035vacc_date5,f036vacc_ticketnum5,f037vacc_insurercode5,f038vacc_place5,f039vacc_doctor5,f040vacc_maker5,f041vacc_lot5" +
                $" from dataimport where f001number = '{strNum}' and cym = '{cym}'"))
            //using (DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where f001number='{strNum}' and cym='{cym}'"))
            //20220816_1 ito ed /////
            //using (DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where f001number='{strNum}'"))
            {
                dataimport cls = new dataimport();
                List<object[]> lst = cmd.TryExecuteReaderList();
                if (lst.Count == 0) return null;
                
                object[] tmp = lst[0];

                cls.importid = int.Parse(tmp[0].ToString());
                cls.f001number = tmp[1].ToString();
                cls.f002vacc_entrydate1 = tmp[2].ToString();
                cls.f003vacc_date1 = tmp[3].ToString();
                cls.f004vacc_ticketnum1 = tmp[4].ToString();
                cls.f005vacc_insurercode1 = tmp[5].ToString();
                cls.f006vacc_place1 = tmp[6].ToString();
                cls.f007vacc_doctor1 = tmp[7].ToString();
                cls.f008vacc_maker1 = tmp[8].ToString();
                cls.f009vacc_lot1 = tmp[9].ToString();
                cls.f010vacc_entrydate2 = tmp[10].ToString();
                cls.f011vacc_date2 = tmp[11].ToString();
                cls.f012vacc_ticketnum2 = tmp[12].ToString();
                cls.f013vacc_insurercode2 = tmp[13].ToString();
                cls.f014vacc_place2 = tmp[14].ToString();
                cls.f015vacc_doctor2 = tmp[15].ToString();
                cls.f016vacc_maker2 = tmp[16].ToString();
                cls.f017vacc_lot2 = tmp[17].ToString();
                cls.f018vacc_entrydate3 = tmp[18].ToString();
                cls.f019vacc_date3 = tmp[19].ToString();
                cls.f020vacc_ticketnum3 = tmp[20].ToString();
                cls.f021vacc_insurercode3 = tmp[21].ToString();
                cls.f022vacc_place3 = tmp[22].ToString();
                cls.f023vacc_doctor3 = tmp[23].ToString();
                cls.f024vacc_maker3 = tmp[24].ToString();
                cls.f025vacc_lot3 = tmp[25].ToString();

                //20220816_1 ito st /////
                cls.f026vacc_entrydate4 =   tmp[26].ToString();
                cls.f027vacc_date4 =        tmp[27].ToString();
                cls.f028vacc_ticketnum4 =   tmp[28].ToString();
                cls.f029vacc_insurercode4 = tmp[29].ToString();
                cls.f030vacc_place4 =       tmp[30].ToString();
                cls.f031vacc_doctor4 =      tmp[31].ToString();
                cls.f032vacc_maker4 =       tmp[32].ToString();
                cls.f033vacc_lot4 =         tmp[33].ToString();
                cls.f034vacc_entrydate5 =   tmp[34].ToString();
                cls.f035vacc_date5 =        tmp[35].ToString();
                cls.f036vacc_ticketnum5 =   tmp[36].ToString();
                cls.f037vacc_insurercode5 = tmp[37].ToString();
                cls.f038vacc_place5 =       tmp[38].ToString();
                cls.f039vacc_doctor5 =      tmp[39].ToString();
                cls.f040vacc_maker5 =       tmp[40].ToString();
                cls.f041vacc_lot5 =         tmp[41].ToString();
                //20220816_1 ito ed /////


                return cls;
            }
            
        }


        /// <summary>
        /// 処理年月単位で取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<dataimport> select(int cym)
        {
            lstclsData.Clear();

            //データ取得
            //20220816_1 ito st /////
            DB.Command cmd = new DB.Command(DB.Main, $"select importid, f001number," +
                $"f002vacc_entrydate1,f003vacc_date1,f004vacc_ticketnum1,f005vacc_insurercode1,f006vacc_place1,f007vacc_doctor1,f008vacc_maker1,f009vacc_lot1," +
                $"f010vacc_entrydate2,f011vacc_date2,f012vacc_ticketnum2,f013vacc_insurercode2,f014vacc_place2,f015vacc_doctor2,f016vacc_maker2,f017vacc_lot2," +
                $"f018vacc_entrydate3,f019vacc_date3,f020vacc_ticketnum3,f021vacc_insurercode3,f022vacc_place3,f023vacc_doctor3,f024vacc_maker3,f025vacc_lot3," +
                $"f026vacc_entrydate4,f027vacc_date4,f028vacc_ticketnum4,f029vacc_insurercode4,f030vacc_place4,f031vacc_doctor4,f032vacc_maker4,f033vacc_lot4," +
                $"f034vacc_entrydate5,f035vacc_date5,f036vacc_ticketnum5,f037vacc_insurercode5,f038vacc_place5,f039vacc_doctor5,f040vacc_maker5,f041vacc_lot5" +
                $" from dataimport where cym ={ cym }");
            //DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where cym={cym}");
            //20220816_1 ito ed /////

            List<object[]> lst = cmd.TryExecuteReaderList();

            try
            {
                //クラスのリストに登録
                foreach (object[] tmp in lst)
                {
                    dataimport cls = new dataimport();
                    cls.importid= int.Parse(tmp[0].ToString());
                    cls.f001number = tmp[1].ToString();
                    cls.f002vacc_entrydate1 = tmp[2].ToString();
                    cls.f003vacc_date1 = tmp[3].ToString();
                    cls.f004vacc_ticketnum1 = tmp[4].ToString();
                    cls.f005vacc_insurercode1 = tmp[5].ToString();
                    cls.f006vacc_place1 = tmp[6].ToString();
                    cls.f007vacc_doctor1 = tmp[7].ToString();
                    cls.f008vacc_maker1 = tmp[8].ToString();
                    cls.f009vacc_lot1 = tmp[9].ToString();
                    cls.f010vacc_entrydate2 = tmp[10].ToString();
                    cls.f011vacc_date2 = tmp[11].ToString();
                    cls.f012vacc_ticketnum2 = tmp[12].ToString();
                    cls.f013vacc_insurercode2 = tmp[13].ToString();
                    cls.f014vacc_place2 = tmp[14].ToString();
                    cls.f015vacc_doctor2 = tmp[15].ToString();
                    cls.f016vacc_maker2 = tmp[16].ToString();
                    cls.f017vacc_lot2 = tmp[17].ToString();
                    cls.f018vacc_entrydate3 = tmp[18].ToString();
                    cls.f019vacc_date3 = tmp[19].ToString();
                    cls.f020vacc_ticketnum3 = tmp[20].ToString();
                    cls.f021vacc_insurercode3 = tmp[21].ToString();
                    cls.f022vacc_place3 = tmp[22].ToString();
                    cls.f023vacc_doctor3 = tmp[23].ToString();
                    cls.f024vacc_maker3 = tmp[24].ToString();
                    cls.f025vacc_lot3 = tmp[25].ToString();

                    //20220816_1 ito st /////
                    cls.f026vacc_entrydate4 = tmp[26].ToString();
                    cls.f027vacc_date4 = tmp[27].ToString();
                    cls.f028vacc_ticketnum4 = tmp[28].ToString();
                    cls.f029vacc_insurercode4 = tmp[29].ToString();
                    cls.f030vacc_place4 = tmp[30].ToString();
                    cls.f031vacc_doctor4 = tmp[31].ToString();
                    cls.f032vacc_maker4 = tmp[32].ToString();
                    cls.f033vacc_lot4 = tmp[33].ToString();
                    cls.f034vacc_entrydate5 = tmp[34].ToString();
                    cls.f035vacc_date5 = tmp[35].ToString();
                    cls.f036vacc_ticketnum5 = tmp[36].ToString();
                    cls.f037vacc_insurercode5 = tmp[37].ToString();
                    cls.f038vacc_place5 = tmp[38].ToString();
                    cls.f039vacc_doctor5 = tmp[39].ToString();
                    cls.f040vacc_maker5 = tmp[40].ToString();
                    cls.f041vacc_lot5 = tmp[41].ToString();
                    //20220816_1 ito ed /////

                    cls.cym = cym;      //メホール請求年月;

                    lstclsData.Add(cls);
                }
                return lstclsData;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return lstclsData;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    

        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {
            
            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            //20220303150929 furukawa st ////////////////////////
            //待ち時間用表示
            
            WaitFormSimple wf = new WaitFormSimple();
            wf.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
            //20220303150929 furukawa ed ////////////////////////


            System.Data.DataTable dt = new System.Data.DataTable();
            
            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport group by cym order by cym desc");
            List<object[]> lst=cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
                //20220303150953 furukawa st ////////////////////////
                //待ち時間用表示閉じる
                
                wf.InvokeCloseDispose();
                //20220303150953 furukawa ed ////////////////////////
            }
        }
    }

    public class dataimport_chargelist
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int importid { get;set; }=0;       

        public int no { get; set; } = 0;                                          //記載順序;
        public string hosnum { get; set; } = string.Empty;                                         //医療機関等コード;
        public string nob { get; set; } = string.Empty;                                         //豊中市独自の番号;
        public string hosname { get; set; } = string.Empty;                                         //医療機関名;
        public int ty6d { get; set; } = 0;                                          //【通常】予診のみ６歳未満;
        public int ty6u { get; set; } = 0;                                          //【通常】予診のみ６歳以上;
        public int ts6d { get; set; } = 0;                                          //【通常】接種６歳未満;
        public int ts6u { get; set; } = 0;                                          //【通常】接種６歳以上;
        public int ty6dk { get; set; } = 0;                                          //【通常】予診のみ６歳未満金額;
        public int ty6uk { get; set; } = 0;                                          //【通常】予診のみ６歳以上金額;
        public int ts6dk { get; set; } = 0;                                          //【通常】接種６歳未満金額;
        public int ts6uk { get; set; } = 0;                                          //【通常】接種６歳以上金額;
        public int tcharge { get; set; } = 0;                                          //【通常】請求金額;
        public int gy6d { get; set; } = 0;                                          //【時間外】予診のみ６歳未満;
        public int gy6u { get; set; } = 0;                                          //【時間外】予診のみ６歳以上;
        public int gs6d { get; set; } = 0;                                          //【時間外】接種６歳未満;
        public int gs6u { get; set; } = 0;                                          //【時間外】接種６歳以上;
        public int gy6dk { get; set; } = 0;                                          //【時間外】予診のみ６歳未満金額;
        public int gy6uk { get; set; } = 0;                                          //【時間外】予診のみ６歳以上金額;
        public int gs6dk { get; set; } = 0;                                          //【時間外】接種６歳未満金額;
        public int gs6uk { get; set; } = 0;                                          //【時間外】接種６歳以上金額;
        public int gcharge { get; set; } = 0;                                          //【時間外】請求金額;
        public int ky6d { get; set; } = 0;                                          //【休日】予診のみ６歳未満;
        public int ky6u { get; set; } = 0;                                          //【休日】予診のみ６歳以上;
        public int ks6d { get; set; } = 0;                                          //【休日】接種６歳未満;
        public int ks6u { get; set; } = 0;                                          //【休日】接種６歳以上;
        public int ky6dk { get; set; } = 0;                                          //【休日】予診のみ６歳未満金額;
        public int ky6uk { get; set; } = 0;                                          //【休日】予診のみ６歳以上金額;
        public int ks6dk { get; set; } = 0;                                          //【休日】接種６歳未満金額;
        public int ks6uk { get; set; } = 0;                                          //【休日】接種６歳以上金額;
        public int kcharge { get; set; } = 0;                                          //【休日】請求金額;
        public int fy6d { get; set; } = 0;                                          //【不明】予診のみ６歳未満メディ独自;
        public int fy6u { get; set; } = 0;                                          //【不明】予診のみ６歳以上メディ独自;
        public int fs6d { get; set; } = 0;                                          //【不明】接種６歳未満メディ独自;
        public int fs6u { get; set; } = 0;                                          //【不明】接種６歳以上メディ独自;
        public int fy6dk { get; set; } = 0;                                          //【不明】予診のみ６歳未満金額メディ独自;
        public int fy6uk { get; set; } = 0;                                          //【不明】予診のみ６歳以上金額メディ独自;
        public int fs6dk { get; set; } = 0;                                          //【不明】接種６歳未満金額メディ独自;
        public int fs6uk { get; set; } = 0;                                          //【不明】接種６歳以上金額メディ独自;
        public int fcharge { get; set; } = 0;                                          //【不明】請求金額メディ独自;
        public int chargetotal { get; set; } = 0;                                          //請求金額合計;
        public string hoszip { get; set; } = string.Empty;                                         //郵便番号;
        public string hosaddress { get; set; } = string.Empty;                                         //住所;



        #endregion

        public static List<chargelist> lstclsData = new List<chargelist>();


        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool ImportMain(int _cym,bool flgShokuiki=false)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.InvokeValue = 0;

            try
            {
                if (!EntryExcelFile(wf, _cym,flgShokuiki)) return false;
                //if (!LoadFile(wf, _cym)) return false;

                //db登録
                //if (!EntryDB(wf, _cym)) return false;

                System.Windows.Forms.MessageBox.Show("取込完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("取込失敗");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }



        /// <summary>
        /// db登録
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryDB(WaitForm wf, int _cym)
        {

            wf.InvokeValue = 0;
            wf.SetMax(lstclsData.Count);
            DB.Transaction tran = new DB.Transaction(DB.Main);

            try
            {
                wf.LogPrint("DB削除");
                DB.Command cmd = new DB.Command($"delete from chargelist where cym={_cym}", tran);
                cmd.TryExecuteNonQuery();

                wf.LogPrint("DB登録");
                foreach (chargelist cls in lstclsData)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }

                    if (!DB.Main.Insert<chargelist>(cls, tran)) return false;
                    wf.LogPrint($"NoB：{cls.nob}");
                    wf.InvokeValue++;
                }

                tran.Commit();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }

        private static int ChangeToADYM(string strtmp)
        {
            return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
        }

        
        /// <summary>
        /// エクセルファイルを開き、データを取得してsqlでdbに書き込み
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="_cym"></param>
        /// <param name="flgShokuiki">職域ファイルの場合true</param>
        /// <returns></returns>
        private static bool EntryExcelFile(WaitForm wf , int _cym,bool flgShokuiki)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.Filter = "Excelファイル|*.xls";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

            string strFileName = dlg.FileName;

            //NPOI.XSSF.UserModel.XSSFWorkbook wb;
            //wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            //NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            //請求一覧のファイルはxls形式のようだ
            var fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            NPOI.HSSF.UserModel.HSSFWorkbook wbs = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);            
            NPOI.SS.UserModel.ISheet ws = wbs.GetSheetAt(0);
            


            DB.Transaction tran = DB.Main.CreateTransaction();

            try
            {
                //職域でない場合は削除
                if (!flgShokuiki)
                {
                    wf.LogPrint("DB削除");

                    //20220210165342 furukawa st ////////////////////////
                    //cym追加
                    
                    DB.Command cmd = new DB.Command($"delete from chargelist where cym={_cym}", tran);
                    //      DB.Command cmd = new DB.Command($"truncate table chargelist", tran);
                    //20220210165342 furukawa ed ////////////////////////

                    cmd.TryExecuteNonQuery();
                }

                wf.LogPrint("DB登録");
                wf.SetMax(ws.LastRowNum);
                for (int r = 0; r < ws.LastRowNum; r++)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) 
                        return false;

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);
                    if (row == null) continue;
                    chargelist chg = new chargelist();

                    if (row.GetCell(0) == null) continue;
                    if (!int.TryParse(getCellValueToString(row.GetCell(0)), out int tmp)) continue;//A列が数字でない場合飛ばす



                    chg.no = int.Parse(getCellValueToString(row.GetCell(0)));
                    chg.hosnum = getCellValueToString(row.GetCell(1));
                    
                    //20220127142245 furukawa st ////////////////////////
                    //漢字が入っていると入力時点でロードしないから外す
                    chg.nob = getCellValueToString(row.GetCell(2)).Replace("職域", "");//漢字が入っていると入力時点でロードしないから外す
                    //  chg.nob = getCellValueToString(row.GetCell(2))
                    //20220127142245 furukawa ed ////////////////////////

                    chg.hosname= getCellValueToString(row.GetCell(3));

                    if (!flgShokuiki)
                    {
                        chg.hoszip = getCellValueToString(row.GetCell(34));
                        chg.hosaddress = getCellValueToString(row.GetCell(35));
                    }
                    else
                    {
                        chg.hoszip = getCellValueToString(row.GetCell(15));
                        chg.hosaddress = getCellValueToString(row.GetCell(16));
                    }
                    //20220210164330 furukawa st ////////////////////////
                    //cym追加
                    
                    chg.cym = _cym;
                    //20220210164330 furukawa ed ////////////////////////


                    DB.Main.Insert<chargelist>(chg, tran);
                    wf.LogPrint($"医療機関等コード:{chg.hosnum} NoB:{chg.nob}");
                    wf.InvokeValue++;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex) 
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                wbs.Close();
                //wb.Close();
            }

        }

        /// <summary>
        /// csvをロードする
        /// </summary>
        /// <returns></returns>
        private static bool LoadFile(WaitForm wf, int _cym)
        {

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();

            try
            {
                dlg.Multiselect = true;
                dlg.Filter = "csv|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

                string[] arrstrFileName = dlg.FileNames;
                lstclsData.Clear();


                foreach (string strFileName in arrstrFileName)
                {

                    //CSV内容ロード
                    List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                    wf.LogPrint($"{strFileName} ロード");
                    wf.SetMax(lst.Count);
                    wf.InvokeValue = 0;


                    //クラスのリストに登録
                    foreach (string[] tmp in lst)
                    {

                        chargelist cls = new chargelist();

                        cls.no = int.Parse(tmp[0]);
                        cls.hosnum = tmp[1];
                        cls.nob = tmp[2];
                        cls.hosname = tmp[3];
                        cls.ty6d = int.TryParse(tmp[4], out int tmp4) == false ? 0 : tmp4;
                        cls.ty6u = int.TryParse(tmp[5], out int tmp5) == false ? 0 : tmp5;
                        cls.ts6d = int.TryParse(tmp[6], out int tmp6) == false ? 0 : tmp6;
                        cls.ts6u = int.TryParse(tmp[7], out int tmp7) == false ? 0 : tmp7;
                        cls.ty6dk = int.TryParse(tmp[8], out int tmp8) == false ? 0 : tmp8;
                        cls.ty6uk = int.TryParse(tmp[9], out int tmp9) == false ? 0 : tmp9;
                        cls.ts6dk = int.TryParse(tmp[10], out int tmp10) == false ? 0 : tmp10;
                        cls.ts6uk = int.TryParse(tmp[11], out int tmp11) == false ? 0 : tmp11;
                        cls.tcharge = int.TryParse(tmp[12], out int tmp12) == false ? 0 : tmp12;
                        cls.gy6d = int.TryParse(tmp[13], out int tmp13) == false ? 0 : tmp13;
                        cls.gy6u = int.TryParse(tmp[14], out int tmp14) == false ? 0 : tmp14;
                        cls.gs6d = int.TryParse(tmp[15], out int tmp15) == false ? 0 : tmp15;
                        cls.gs6u = int.TryParse(tmp[16], out int tmp16) == false ? 0 : tmp16;
                        cls.gy6dk = int.TryParse(tmp[17], out int tmp17) == false ? 0 : tmp17;
                        cls.gy6uk = int.TryParse(tmp[18], out int tmp18) == false ? 0 : tmp18;
                        cls.gs6dk = int.TryParse(tmp[19], out int tmp19) == false ? 0 : tmp19;
                        cls.gs6uk = int.TryParse(tmp[20], out int tmp20) == false ? 0 : tmp20;
                        cls.gcharge = int.TryParse(tmp[21], out int tmp21) == false ? 0 : tmp21;
                        cls.ky6d = int.TryParse(tmp[22], out int tmp22) == false ? 0 : tmp22;
                        cls.ky6u = int.TryParse(tmp[23], out int tmp23) == false ? 0 : tmp23;
                        cls.ks6d = int.TryParse(tmp[24], out int tmp24) == false ? 0 : tmp24;
                        cls.ks6u = int.TryParse(tmp[25], out int tmp25) == false ? 0 : tmp25;
                        cls.ky6dk = int.TryParse(tmp[26], out int tmp26) == false ? 0 : tmp26;
                        cls.ky6uk = int.TryParse(tmp[27], out int tmp27) == false ? 0 : tmp27;
                        cls.ks6dk = int.TryParse(tmp[28], out int tmp28) == false ? 0 : tmp28;
                        cls.ks6uk = int.TryParse(tmp[29], out int tmp29) == false ? 0 : tmp29;
                        cls.kcharge = int.TryParse(tmp[30], out int tmp30) == false ? 0 : tmp30;

                        //元々のファイルにないため
                        cls.fy6d = 0;
                        cls.fy6u = 0;
                        cls.fs6d = 0;
                        cls.fs6u = 0;
                        cls.fy6dk =0;
                        cls.fy6uk =0;
                        cls.fs6dk =0;
                        cls.fs6uk = 0;
                        cls.fcharge = 0;

                        //cls.fy6d = int.TryParse(tmp[31].ToString(), out int tmp31) == false ? 0 : tmp31;
                        //cls.fy6u = int.TryParse(tmp[32].ToString(), out int tmp32) == false ? 0 : tmp32;
                        //cls.fs6d = int.TryParse(tmp[33].ToString(), out int tmp33) == false ? 0 : tmp33;
                        //cls.fs6u = int.TryParse(tmp[34].ToString(), out int tmp34) == false ? 0 : tmp34;
                        //cls.fy6dk = int.TryParse(tmp[35].ToString(), out int tmp35) == false ? 0 : tmp35;
                        //cls.fy6uk = int.TryParse(tmp[36].ToString(), out int tmp36) == false ? 0 : tmp36;
                        //cls.fs6dk = int.TryParse(tmp[37].ToString(), out int tmp37) == false ? 0 : tmp37;
                        //cls.fs6uk = int.TryParse(tmp[38].ToString(), out int tmp38) == false ? 0 : tmp38;
                        //cls.fcharge = int.TryParse(tmp[39].ToString(), out int tmp39) == false ? 0 : tmp39;

                        cls.chargetotal = int.TryParse(tmp[40], out int tmp40) == false ? 0 : tmp40;
                        cls.hoszip = tmp[41];
                        cls.hosaddress = tmp[42];



                        lstclsData.Add(cls);

                        wf.InvokeValue++;

                    }

                }
                wf.LogPrint("CSVロード終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 医療機関をロード
        /// </summary>
        /// <param name="strNum">医療機関等番号</param>
        /// <returns></returns>
        public static chargelist select_clinicnum(string strNum)
        {
            using (DB.Command cmd = new DB.Command(DB.Main, $"select * from chargelist where hosnum='{strNum}'"))
            {

                chargelist cls = new chargelist();
                List<object[]> lst = cmd.TryExecuteReaderList();
                if (lst.Count == 0) return null;

                object[] tmp = lst[0];

                cls.importid = int.Parse(tmp[0].ToString());
                cls.no = int.TryParse(tmp[1].ToString(), out int tmp1) == false ? 0 : tmp1;
                cls.hosnum = tmp[2].ToString();
                cls.nob = tmp[3].ToString();
                cls.hosname = tmp[4].ToString();
                cls.ty6d = int.TryParse(tmp[5].ToString(), out int tmp5) == false ? 0 : tmp5;
                cls.ty6u = int.TryParse(tmp[6].ToString(), out int tmp6) == false ? 0 : tmp6;
                cls.ts6d = int.TryParse(tmp[7].ToString(), out int tmp7) == false ? 0 : tmp7;
                cls.ts6u = int.TryParse(tmp[8].ToString(), out int tmp8) == false ? 0 : tmp8;
                cls.ty6dk = int.TryParse(tmp[9].ToString(), out int tmp9) == false ? 0 : tmp9;
                cls.ty6uk = int.TryParse(tmp[10].ToString(), out int tmp10) == false ? 0 : tmp10;
                cls.ts6dk = int.TryParse(tmp[11].ToString(), out int tmp11) == false ? 0 : tmp11;
                cls.ts6uk = int.TryParse(tmp[12].ToString(), out int tmp12) == false ? 0 : tmp12;
                cls.tcharge = int.TryParse(tmp[13].ToString(), out int tmp13) == false ? 0 : tmp13;
                cls.gy6d = int.TryParse(tmp[14].ToString(), out int tmp14) == false ? 0 : tmp14;
                cls.gy6u = int.TryParse(tmp[15].ToString(), out int tmp15) == false ? 0 : tmp15;
                cls.gs6d = int.TryParse(tmp[16].ToString(), out int tmp16) == false ? 0 : tmp16;
                cls.gs6u = int.TryParse(tmp[17].ToString(), out int tmp17) == false ? 0 : tmp17;
                cls.gy6dk = int.TryParse(tmp[18].ToString(), out int tmp18) == false ? 0 : tmp18;
                cls.gy6uk = int.TryParse(tmp[19].ToString(), out int tmp19) == false ? 0 : tmp19;
                cls.gs6dk = int.TryParse(tmp[20].ToString(), out int tmp20) == false ? 0 : tmp20;
                cls.gs6uk = int.TryParse(tmp[21].ToString(), out int tmp21) == false ? 0 : tmp21;
                cls.gcharge = int.TryParse(tmp[22].ToString(), out int tmp22) == false ? 0 : tmp22;
                cls.ky6d = int.TryParse(tmp[23].ToString(), out int tmp23) == false ? 0 : tmp23;
                cls.ky6u = int.TryParse(tmp[24].ToString(), out int tmp24) == false ? 0 : tmp24;
                cls.ks6d = int.TryParse(tmp[25].ToString(), out int tmp25) == false ? 0 : tmp25;
                cls.ks6u = int.TryParse(tmp[26].ToString(), out int tmp26) == false ? 0 : tmp26;
                cls.ky6dk = int.TryParse(tmp[27].ToString(), out int tmp27) == false ? 0 : tmp27;
                cls.ky6uk = int.TryParse(tmp[28].ToString(), out int tmp28) == false ? 0 : tmp28;
                cls.ks6dk = int.TryParse(tmp[29].ToString(), out int tmp29) == false ? 0 : tmp29;
                cls.ks6uk = int.TryParse(tmp[30].ToString(), out int tmp30) == false ? 0 : tmp30;
                cls.kcharge = int.TryParse(tmp[31].ToString(), out int tmp31) == false ? 0 : tmp31;

                cls.fy6d = int.TryParse(tmp[32].ToString(), out int tmp32) == false ? 0 : tmp32;
                cls.fy6u = int.TryParse(tmp[33].ToString(), out int tmp33) == false ? 0 : tmp33;
                cls.fs6d = int.TryParse(tmp[34].ToString(), out int tmp34) == false ? 0 : tmp34;
                cls.fs6u = int.TryParse(tmp[35].ToString(), out int tmp35) == false ? 0 : tmp35;
                cls.fy6dk = int.TryParse(tmp[36].ToString(), out int tmp36) == false ? 0 : tmp36;
                cls.fy6uk = int.TryParse(tmp[37].ToString(), out int tmp37) == false ? 0 : tmp37;
                cls.fs6dk = int.TryParse(tmp[38].ToString(), out int tmp38) == false ? 0 : tmp38;
                cls.fs6uk = int.TryParse(tmp[39].ToString(), out int tmp39) == false ? 0 : tmp39;
                cls.fcharge = int.TryParse(tmp[40].ToString(), out int tmp40) == false ? 0 : tmp40;
                cls.chargetotal = int.TryParse(tmp[41].ToString(), out int tmp41) == false ? 0 : tmp41;
                cls.hoszip = tmp[42].ToString();
                cls.hosaddress = tmp[43].ToString();

                return cls;
            }
        }

        /// <summary>
        /// 医療機関をロード
        /// </summary>
        /// <param name="strNum">NoB類</param>
        /// <returns></returns>
        public static chargelist select(string strNum)
        {
            using (DB.Command cmd = new DB.Command(DB.Main, $"select * from chargelist where nob='{strNum}' and nob<>''"))
            {

                chargelist cls = new chargelist();
                List<object[]> lst = cmd.TryExecuteReaderList();
                if (lst.Count == 0) return null;

                object[] tmp = lst[0];

                cls.importid = int.Parse(tmp[0].ToString());
                cls.no = int.TryParse(tmp[1].ToString(), out int tmp1) == false ? 0 : tmp1;
                cls.hosnum = tmp[2].ToString();
                cls.nob = tmp[3].ToString();
                cls.hosname = tmp[4].ToString();
                cls.ty6d = int.TryParse(tmp[5].ToString(), out int tmp5) == false ? 0 : tmp5;
                cls.ty6u = int.TryParse(tmp[6].ToString(), out int tmp6) == false ? 0 : tmp6;
                cls.ts6d = int.TryParse(tmp[7].ToString(), out int tmp7) == false ? 0 : tmp7;
                cls.ts6u = int.TryParse(tmp[8].ToString(), out int tmp8) == false ? 0 : tmp8;
                cls.ty6dk = int.TryParse(tmp[9].ToString(), out int tmp9) == false ? 0 : tmp9;
                cls.ty6uk = int.TryParse(tmp[10].ToString(), out int tmp10) == false ? 0 : tmp10;
                cls.ts6dk = int.TryParse(tmp[11].ToString(), out int tmp11) == false ? 0 : tmp11;
                cls.ts6uk = int.TryParse(tmp[12].ToString(), out int tmp12) == false ? 0 : tmp12;
                cls.tcharge = int.TryParse(tmp[13].ToString(), out int tmp13) == false ? 0 : tmp13;
                cls.gy6d = int.TryParse(tmp[14].ToString(), out int tmp14) == false ? 0 : tmp14;
                cls.gy6u = int.TryParse(tmp[15].ToString(), out int tmp15) == false ? 0 : tmp15;
                cls.gs6d = int.TryParse(tmp[16].ToString(), out int tmp16) == false ? 0 : tmp16;
                cls.gs6u = int.TryParse(tmp[17].ToString(), out int tmp17) == false ? 0 : tmp17;
                cls.gy6dk = int.TryParse(tmp[18].ToString(), out int tmp18) == false ? 0 : tmp18;
                cls.gy6uk = int.TryParse(tmp[19].ToString(), out int tmp19) == false ? 0 : tmp19;
                cls.gs6dk = int.TryParse(tmp[20].ToString(), out int tmp20) == false ? 0 : tmp20;
                cls.gs6uk = int.TryParse(tmp[21].ToString(), out int tmp21) == false ? 0 : tmp21;
                cls.gcharge = int.TryParse(tmp[22].ToString(), out int tmp22) == false ? 0 : tmp22;
                cls.ky6d = int.TryParse(tmp[23].ToString(), out int tmp23) == false ? 0 : tmp23;
                cls.ky6u = int.TryParse(tmp[24].ToString(), out int tmp24) == false ? 0 : tmp24;
                cls.ks6d = int.TryParse(tmp[25].ToString(), out int tmp25) == false ? 0 : tmp25;
                cls.ks6u = int.TryParse(tmp[26].ToString(), out int tmp26) == false ? 0 : tmp26;
                cls.ky6dk = int.TryParse(tmp[27].ToString(), out int tmp27) == false ? 0 : tmp27;
                cls.ky6uk = int.TryParse(tmp[28].ToString(), out int tmp28) == false ? 0 : tmp28;
                cls.ks6dk = int.TryParse(tmp[29].ToString(), out int tmp29) == false ? 0 : tmp29;
                cls.ks6uk = int.TryParse(tmp[30].ToString(), out int tmp30) == false ? 0 : tmp30;
                cls.kcharge = int.TryParse(tmp[31].ToString(), out int tmp31) == false ? 0 : tmp31;
               
                cls.fy6d = int.TryParse(tmp[32].ToString(), out int tmp32) == false ? 0 : tmp32;
                cls.fy6u = int.TryParse(tmp[33].ToString(), out int tmp33) == false ? 0 : tmp33;
                cls.fs6d = int.TryParse(tmp[34].ToString(), out int tmp34) == false ? 0 : tmp34;
                cls.fs6u = int.TryParse(tmp[35].ToString(), out int tmp35) == false ? 0 : tmp35;
                cls.fy6dk = int.TryParse(tmp[36].ToString(), out int tmp36) == false ? 0 : tmp36;
                cls.fy6uk = int.TryParse(tmp[37].ToString(), out int tmp37) == false ? 0 : tmp37;
                cls.fs6dk = int.TryParse(tmp[38].ToString(), out int tmp38) == false ? 0 : tmp38;
                cls.fs6uk = int.TryParse(tmp[39].ToString(), out int tmp39) == false ? 0 : tmp39;
                cls.fcharge = int.TryParse(tmp[40].ToString(), out int tmp40) == false ? 0 : tmp40;
                cls.chargetotal = int.TryParse(tmp[41].ToString(), out int tmp41) == false ? 0 : tmp41;
                cls.hoszip = tmp[42].ToString();
                cls.hosaddress = tmp[43].ToString();


                return cls;
            }
        }



        /// <summary>
        /// 処理年月単位で取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<chargelist> select(int cym)
        {
            lstclsData.Clear();

            //データ取得
            DB.Command cmd = new DB.Command(DB.Main, $"select * from chargelist ");// where cym={cym}");
            List<object[]> lst = cmd.TryExecuteReaderList();

            try
            {


                //クラスのリストに登録
                foreach (object[] tmp in lst)
                {


                    chargelist cls = new chargelist();
                    
                    cls.no = int.Parse(tmp[0].ToString());
                    cls.hosnum = tmp[1].ToString();
                    cls.nob = tmp[2].ToString();
                    cls.hosname = tmp[3].ToString();
                    cls.ty6d = int.TryParse(tmp[4].ToString(), out int tmp4) == false ? 0 : tmp4;
                    cls.ty6u = int.TryParse(tmp[5].ToString(), out int tmp5) == false ? 0 : tmp5;
                    cls.ts6d = int.TryParse(tmp[6].ToString(), out int tmp6) == false ? 0 : tmp6;
                    cls.ts6u = int.TryParse(tmp[7].ToString(), out int tmp7) == false ? 0 : tmp7;
                    cls.ty6dk = int.TryParse(tmp[8].ToString(), out int tmp8) == false ? 0 : tmp8;
                    cls.ty6uk = int.TryParse(tmp[9].ToString(), out int tmp9) == false ? 0 : tmp9;
                    cls.ts6dk = int.TryParse(tmp[10].ToString(), out int tmp10) == false ? 0 : tmp10;
                    cls.ts6uk = int.TryParse(tmp[11].ToString(), out int tmp11) == false ? 0 : tmp11;
                    cls.tcharge = int.TryParse(tmp[12].ToString(), out int tmp12) == false ? 0 : tmp12;
                    cls.gy6d = int.TryParse(tmp[13].ToString(), out int tmp13) == false ? 0 : tmp13;
                    cls.gy6u = int.TryParse(tmp[14].ToString(), out int tmp14) == false ? 0 : tmp14;
                    cls.gs6d = int.TryParse(tmp[15].ToString(), out int tmp15) == false ? 0 : tmp15;
                    cls.gs6u = int.TryParse(tmp[16].ToString(), out int tmp16) == false ? 0 : tmp16;
                    cls.gy6dk = int.TryParse(tmp[17].ToString(), out int tmp17) == false ? 0 : tmp17;
                    cls.gy6uk = int.TryParse(tmp[18].ToString(), out int tmp18) == false ? 0 : tmp18;
                    cls.gs6dk = int.TryParse(tmp[19].ToString(), out int tmp19) == false ? 0 : tmp19;
                    cls.gs6uk = int.TryParse(tmp[20].ToString(), out int tmp20) == false ? 0 : tmp20;
                    cls.gcharge = int.TryParse(tmp[21].ToString(), out int tmp21) == false ? 0 : tmp21;
                    cls.ky6d = int.TryParse(tmp[22].ToString(), out int tmp22) == false ? 0 : tmp22;
                    cls.ky6u = int.TryParse(tmp[23].ToString(), out int tmp23) == false ? 0 : tmp23;
                    cls.ks6d = int.TryParse(tmp[24].ToString(), out int tmp24) == false ? 0 : tmp24;
                    cls.ks6u = int.TryParse(tmp[25].ToString(), out int tmp25) == false ? 0 : tmp25;
                    cls.ky6dk = int.TryParse(tmp[26].ToString(), out int tmp26) == false ? 0 : tmp26;
                    cls.ky6uk = int.TryParse(tmp[27].ToString(), out int tmp27) == false ? 0 : tmp27;
                    cls.ks6dk = int.TryParse(tmp[28].ToString(), out int tmp28) == false ? 0 : tmp28;
                    cls.ks6uk = int.TryParse(tmp[29].ToString(), out int tmp29) == false ? 0 : tmp29;
                    cls.kcharge = int.TryParse(tmp[30].ToString(), out int tmp30) == false ? 0 : tmp30;
                    cls.fy6d = int.TryParse(tmp[31].ToString(), out int tmp31) == false ? 0 : tmp31;
                    cls.fy6u = int.TryParse(tmp[32].ToString(), out int tmp32) == false ? 0 : tmp32;
                    cls.fs6d = int.TryParse(tmp[33].ToString(), out int tmp33) == false ? 0 : tmp33;
                    cls.fs6u = int.TryParse(tmp[34].ToString(), out int tmp34) == false ? 0 : tmp34;
                    cls.fy6dk = int.TryParse(tmp[35].ToString(), out int tmp35) == false ? 0 : tmp35;
                    cls.fy6uk = int.TryParse(tmp[36].ToString(), out int tmp36) == false ? 0 : tmp36;
                    cls.fs6dk = int.TryParse(tmp[37].ToString(), out int tmp37) == false ? 0 : tmp37;
                    cls.fs6uk = int.TryParse(tmp[38].ToString(), out int tmp38) == false ? 0 : tmp38;
                    cls.fcharge = int.TryParse(tmp[39].ToString(), out int tmp39) == false ? 0 : tmp39;

                    
                    cls.chargetotal = int.TryParse(tmp[40].ToString(), out int tmp40) == false ? 0 : tmp40;
                    cls.hoszip = tmp[41].ToString();
                    cls.hosaddress = tmp[42].ToString();



                    lstclsData.Add(cls);


                }


                return lstclsData;

            }


            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return lstclsData;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from chargelist where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }


        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }
        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport group by cym order by cym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
