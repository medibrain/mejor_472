﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.IrumaKokuho
{
    class Export
    {

        public static bool ImageExport(int cym)
        {
            var logName = string.Empty;
            var dir = string.Empty;

            using (var f = new SaveFileDialog())
            {
                f.Filter = "ログファイル|*.log";
                f.FileName = "ExportLog" + DateTime.Today.ToString("yyyyMMdd") + ".log";
                if (f.ShowDialog() != DialogResult.OK) return true;

                logName = f.FileName;
                dir = System.IO.Path.GetDirectoryName(logName);
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                wf.LogPrint("データを取得しています");
                var l = App.GetApps(cym);

                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                var fc = new TiffUtility.FastCopy();
                var lastNumbering = string.Empty;
                int page = 0;

                wf.LogPrint("画像を保存しています");

                foreach (var item in l)
                {
                    wf.InvokeValue++;

                    if (item.AppType == APP_TYPE.続紙)
                    {
                        if (lastNumbering == string.Empty)
                            throw new Exception($"申請書前に続紙が指定されています　AID:{item.Aid}");

                        page++;
                        fc.FileCopy(item.GetImageFullPath(), dir + "\\" + lastNumbering + "_" + page.ToString("00") + ".tiff");
                    }
                    else if ((int)item.AppType < 1)
                    {
                        continue;
                    }
                    else
                    {
                        lastNumbering = item.Numbering;
                        page = 0;
                        fc.FileCopy(item.GetImageFullPath(), dir + "\\" + lastNumbering + ".tiff");
                    }
                }
                
                wf.LogPrint("画像出力が完了しました");
            }
            catch(Exception ex)
            {
                wf.LogPrint("出力に失敗しました　"+ex.ToString());
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.LogSave(logName);
                wf.Dispose();
            }
            
            return true;
        }
    }
}
