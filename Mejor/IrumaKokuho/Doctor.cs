﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.IrumaKokuho
{
    class Doctor
    {
        [DB.DbAttribute.PrimaryKey]
        public string DrNum { get; set; }
        public string DrName { get; set; }
        public string ClinicName { get; set; }
        public string AccountNum { get; set; }
        public string GroupName { get; set; }
        public bool Verified { get; set; }
        public int LastAid { get; set; }

        public static Doctor GetDoctor(string drNum)
        {
            return DB.Main.Select<Doctor>(new { DrNum = drNum }).SingleOrDefault();
        }

        private bool insert()
        {
            return DB.Main.Insert(this);
        }

        private bool update()
        {
            return DB.Main.Update(this);
        }

        public static bool Upsert(App app)
        {
            var dr = GetDoctor(app.DrNum);
            if (dr == null)
            {
                dr = new Doctor();
                dr.DrNum = app.DrNum;
                dr.DrName = app.DrName;
                dr.ClinicName = app.ClinicName;
                dr.GroupName = app.AccountName;
                dr.AccountNum = app.AccountNumber;
                dr.Verified = false;
                dr.LastAid = app.Aid;
                return dr.insert();
            }
            else
            {
                if (dr.ClinicName == app.ClinicName &&
                    dr.DrName == app.DrName &&
                    dr.GroupName == app.AccountName &&
                    dr.AccountNum == app.AccountNumber)
                {
                    if (dr.Verified) return true;
                    if (dr.LastAid == app.Aid) return true;
                    dr.LastAid = app.Aid;
                    dr.Verified = true;
                    return dr.update();
                }
                else
                {
                    dr.ClinicName = app.ClinicName;
                    dr.GroupName = app.AccountName;
                    dr.AccountNum = app.AccountNumber;
                    dr.Verified = false;
                    dr.LastAid = app.Aid;
                    return dr.update();
                }
            }
        }
    }
}
