﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.IrumaKokuho
{
    class ListData
    {
        int index;
        private App app;
        
        int aid => app.Aid;
        string hihoNum => app.HihoNum;
        string hihoName => app.HihoName;
        string Name => app.PersonName;
        int MediYear => app.MediYear;
        int MediMonth => app.MediMonth;
        int days => app.CountedDays;
        string ClinicName => app.ClinicName;
        string Zip => app.HihoZip;
        string Add => app.HihoAdd;
        string shokaiReason => app.ShokaiReason == ShokaiReason.なし ?
            string.Empty : app.ShokaiReason.ToString();
        string numbering => app.Numbering;
        string sex => app.Sex == 1 ? SEX.男.ToString() : SEX.女.ToString();
        string birthday => app.Birthday.ToString("yyyy/M/d");
        string total => app.Total.ToString();
        string charge => app.Charge.ToString();
        string newContType => app.NewContType.ToString();
        string fusho1Name => app.FushoName1;
        string fusho2Name => app.FushoName2;
        string fusho3Name => app.FushoName3;
        string fusho4Name => app.FushoName4;
        string fusho5Name => app.FushoName5;
        string drNum => app.DrNum;
        string drName => app.DrName;
        string accountNumber => app.AccountNumber;
        string accountName => app.AccountName;

        string getCsvLine()
        {
            var l = new List<string>();
            var mark = hihoNum.Split('-')[0];
            var number = hihoNum.Split('-')[1];

            l.Add(aid.ToString());
            l.Add(index.ToString());
            l.Add(mark);
            l.Add(number);
            l.Add(hihoName);
            l.Add(Name);
            l.Add(MediYear.ToString());
            l.Add(MediMonth.ToString());
            l.Add(days.ToString());
            l.Add(ClinicName);
            l.Add(Zip);
            l.Add(Add);
            l.Add(shokaiReason);
            l.Add(numbering);
            l.Add(sex);
            l.Add(birthday);
            l.Add(total);
            l.Add(charge);
            l.Add(newContType);
            l.Add(fusho1Name);
            l.Add(fusho2Name);
            l.Add(fusho3Name);
            l.Add(fusho4Name);
            l.Add(fusho5Name);
            l.Add(drNum);
            l.Add(drName);
            l.Add(accountNumber);
            l.Add(accountName);

            return Utility.CreateCsvLine(l);
        }

        static string header => Utility.CreateCsvLine(new[] { "AID",
            "文書番号", "記号", "番号", "被保険者名","受診者名",
            "施術年","施術月","実日数","施術所名","郵便番号","住所","照会理由",
            "ナンバリング", "性別", "生年月日", "合計金額", "請求金額", "新規継続",
            "負傷名1", "負傷名2", "負傷名3", "負傷名4", "負傷名5", "柔整師登録番号", "施術師名", "口座番号", "口座名義"});
            
        public ListData(int index, App app)
        {
            this.index = index;
            this.app = app;
        }
        
        public static bool Export(List<App> list, string fileName)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(header);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var ld = new ListData(i + 1, list[i]);
                        sw.WriteLine(ld.getCsvLine());
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");

            return true;
        }
    }
}
