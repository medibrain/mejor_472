﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.IrumaKokuho
{
    class Person
    {
        [DB.DbAttribute.PrimaryKey]
        public string HihoNum { get; set; }
        public string HihoName { get; set; }
        [DB.DbAttribute.PrimaryKey]
        public DateTime Birth { get; set; }
        public string Name { get; set; }
        public bool Verified { get; set; }
        public int LastAid { get; set; }

        private static Person GetPerson(string hinoNum, DateTime birth)
        {
            return DB.Main.Select<Person>
                (new { HihoNum = hinoNum, Birth = birth }).FirstOrDefault();
        }

        public static string GetHihoName(string hinoNum)
        {
            return DB.Main.Select<Person>(new { HihoNum = hinoNum, Verified = true })
                .FirstOrDefault()?.HihoName ?? string.Empty;
        }

        public static string GetPersonName(string hinoNum, DateTime birth)
        {
            return DB.Main.Select<Person>(new { HihoNum = hinoNum, Birth = birth, Verified = true })
                .FirstOrDefault()?.Name ?? string.Empty;
        }

        private bool insert()
        {
            return DB.Main.Insert(this);
        }

        private bool update()
        {
            return DB.Main.Update(this);
        }

        public static bool Upsert(App app)
        {
            var p = GetPerson(app.HihoNum, app.Birthday);
            if (p == null)
            {
                p = new Person();
                p.HihoNum = app.HihoNum;
                p.HihoName = app.HihoName;
                p.Birth = app.Birthday;
                p.Name = app.PersonName;
                p.Verified = false;
                p.LastAid = app.Aid;
                return p.insert();
            }
            else
            {
                if (p.HihoName == app.HihoName &&
                    p.Name == app.PersonName &&
                    p.Birth == app.Birthday)
                {
                    if (p.Verified) return true;
                    if (p.LastAid == app.Aid) return true;
                    p.LastAid = app.Aid;
                    p.Verified = true;
                    return p.update();
                }
                else
                {
                    p.HihoName = app.HihoName;
                    p.Name = app.PersonName;
                    p.Birth = app.Birthday;
                    p.Verified = false;
                    p.LastAid = app.Aid;
                    return p.update();
                }
            }
        }
    }
}
