﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.IrumaKokuho
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHosCode = new Point(400, 1500);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 250);
        Point posCost = new Point(400, 1000);
        Point posDays = new Point(400, 300);
        Point posNewCont = new Point(300, 0);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls,
            fushoControls, hosControls, newContControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hnumControls = new Control[] { verifyBoxHihoMark, verifyBoxHihoNumber, verifyBoxNumbering };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, verifyBoxName };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            hosControls = new Control[] { verifyBoxDrCode, verifyBoxDrName, verifyBoxClinicName, verifyBoxAccountNum, verifyBoxAccountName};
            newContControls = new Control[] { verifyBoxNewCont };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    func(item);
                    if (item is TextBox == false) continue;
                    item.Enter += item_Enter;
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //北村さん指示により、鍼灸時、負傷名関係入力不要
            var setEnable = new Action<TextBox, bool>((t, b) =>
                {
                    t.Enabled = b;
                    t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                });
            if (scan.AppType != APP_TYPE.柔整)
            {
                //鍼灸時はバッチシートで柔整師番号を入力しているため、グレーアウト
                setEnable(verifyBoxF1, false);
                setEnable(verifyBoxF2, false);
                setEnable(verifyBoxF3, false);
                setEnable(verifyBoxF4, false);
                setEnable(verifyBoxF5, false);

                setEnable(verifyBoxDrCode, false);
            }

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (hosControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHosCode;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //ナンバリング
            int numbering = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, numbering < 1 || 10000 < numbering);

            //被保険者名
            setStatus(verifyBoxHihoName, verifyBoxHihoName.Text.Trim().Length < 3);

            //被保険者番号 数字に直せること
            var mark = verifyBoxHihoMark.GetIntValue();
            var number = verifyBoxHihoNumber.GetIntValue();
            var mn = mark < 1 || number < 1;
            setStatus(verifyBoxHihoMark, mn);
            setStatus(verifyBoxHihoNumber, mn);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 10 | total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            var calcs = new int[3] { total * 70 / 100, total * 80 / 100, total * 90 / 100 };
            if(!Array.Exists(calcs, c=> c==charge)) payError = true;

            //診療日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //柔整師番号
            long drCode = verifyBoxDrCode.GetIntValue();
            setStatus(verifyBoxDrCode, verifyBoxDrCode.Text.Trim().Length != 10);

            //施術師名
            int len = verifyBoxDrName.Text.Length;
            setStatus(verifyBoxDrName, len < 3);

            //施術所名
            len = verifyBoxClinicName.Text.Length;
            setStatus(verifyBoxClinicName, len < 3);

            //口座番号
            len = verifyBoxAccountNum.Text.Length;
            setStatus(verifyBoxAccountNum, len != 0 && len < 3);

            //団体名
            len = verifyBoxAccountName.Text.Length;
            setStatus(verifyBoxAccountName, len != 0 && len < 3);

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //柔整で柔整登録番号がなければ確認
            if (scan.Note2 == string.Empty && verifyBoxDrCode.Text.Trim().Length == 0)
            {
                verifyBoxDrCode.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("柔整師登録番号が指定されていません。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;

            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.HihoNum = verifyBoxHihoMark.Text.Trim() + "-" + verifyBoxHihoNumber.Text.Trim();
            app.HihoName = verifyBoxHihoName.Text.Trim();
            app.PersonName = verifyBoxName.Text.Trim();
            app.Sex = sex;
            app.Birthday = birthDt;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.DrNum = verifyBoxDrCode.Text.Trim();
            app.DrName = verifyBoxDrName.Text.Trim();
            app.ClinicName = verifyBoxClinicName.Text.Trim();
            app.AccountNumber = verifyBoxAccountNum.Text.Trim();
            app.AccountName = verifyBoxAccountName.Text.Trim();

            app.AppType = scan.AppType;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            return true;
        }
        

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            verifyBoxHihoMark.Text = verifyBoxHihoMark.Text.ToUpper();

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(4, '0');

                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();

                Doctor.Upsert(app);
                Person.Upsert(app);

                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);
            
            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                setVerify(app);
            }
            else
            {
                //App_Flagのチェック
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    setInputedApp(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
                scrollPictureControl1.AutoScrollPosition = posYM;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else
            {
                //申請書
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxNumbering.Text = app.Numbering;
                var hihonum = app.HihoNum.Split('-');
                verifyBoxHihoMark.Text = hihonum.Length == 2 ? hihonum[0] : string.Empty;
                verifyBoxHihoNumber.Text = hihonum.Length == 2 ? hihonum[1] : string.Empty;
                verifyBoxHihoName.Text = app.HihoName;
                verifyBoxSex.Text = app.Sex.ToString();
                verifyBoxBE.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                verifyBoxBY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                verifyBoxBM.Text = app.Birthday.Month.ToString();
                verifyBoxBD.Text = app.Birthday.Day.ToString();
                verifyBoxName.Text = app.PersonName;
                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxTotal.Text = app.Total.ToString();
                verifyBoxCharge.Text = app.Charge.ToString();
                verifyBoxNewCont.Text = ((int)app.NewContType).ToString();
                
                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;
                
                verifyBoxDrCode.Text = app.DrNum;
                verifyBoxDrName.Text = app.DrName;
                verifyBoxClinicName.Text = app.ClinicName;
                verifyBoxAccountNum.Text = app.AccountNumber;
                verifyBoxAccountName.Text = app.AccountName;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                labelOCR, verifyBoxY, labelInputerName, };

            void act(Control c, bool b)
            {
                foreach (Control item in c.Controls) act(item, b);
                if ((c is IVerifiable == false) && (c is Label == false)) return;
                if (ignoreControls.Contains(c)) return;
                c.Visible = b;
                if (c is IVerifiable == false) return;
                c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            }

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                act(panelRight, false);
            }
            else
            {
                //申請書の場合
                act(panelRight, true);
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void verifyBoxHihoNumber_Leave(object sender, EventArgs e)
        {
            //被保険者名取得
            if (!string.IsNullOrWhiteSpace(verifyBoxHihoName.Text)) return;
            var num = verifyBoxHihoMark.Text.Trim() + "-" + verifyBoxHihoNumber.Text.Trim();

            verifyBoxHihoName.Text = Person.GetHihoName(num);
        }

        private void verifyBoxBD_Leave(object sender, EventArgs e)
        {
            //受療者名取得
            if (!string.IsNullOrWhiteSpace(verifyBoxName.Text)) return;
            var num = verifyBoxHihoMark.Text.Trim() + "-" + verifyBoxHihoNumber.Text.Trim();
            var birth = getDate(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);
            var sex = verifyBoxSex.GetIntValue();

            verifyBoxName.Text = Person.GetPersonName(num, birth);
        }

        private void verifyBoxDrCode_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(verifyBoxDrName.Text)) return;
            var drNum = verifyBoxDrCode.Text.Trim();
            var dr = Doctor.GetDoctor(drNum);
            if (dr == null) return;

            verifyBoxDrName.Text = dr.DrName;
            verifyBoxClinicName.Text = dr.ClinicName;
            verifyBoxAccountNum.Text = dr.AccountNum;
            verifyBoxAccountName.Text = dr.GroupName;
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (hosControls.Contains(t)) posHosCode = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        
        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else
            {
                //申請書
                setVerifyVal(verifyBoxY, app.MediYear, nv);
                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                setVerifyVal(verifyBoxNumbering, app.Numbering, nv);
                var h = app.HihoNum.Split('-');
                setVerifyVal(verifyBoxHihoMark, h.Length == 2 ? h[0] : string.Empty, nv);
                setVerifyVal(verifyBoxHihoNumber, h.Length == 2 ? h[1] : string.Empty, nv);
                setVerifyVal(verifyBoxHihoName, app.HihoName, false);
                setVerifyVal(verifyBoxSex, app.Sex, false);
                setVerifyVal(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), false);
                setVerifyVal(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), false);
                setVerifyVal(verifyBoxBM, app.Birthday.Month, false);
                setVerifyVal(verifyBoxBD, app.Birthday.Day, false);
                setVerifyVal(verifyBoxName, app.PersonName, false);
                setVerifyVal(verifyBoxDays, app.CountedDays, false);
                setVerifyVal(verifyBoxTotal, app.Total, false);
                setVerifyVal(verifyBoxCharge, app.Charge, false);
                setVerifyVal(verifyBoxNewCont, ((int)app.NewContType).ToString(), false);

                setVerifyVal(verifyBoxF1, app.FushoName1, false);
                setVerifyVal(verifyBoxF2, app.FushoName2, false);
                setVerifyVal(verifyBoxF3, app.FushoName3, false);
                setVerifyVal(verifyBoxF4, app.FushoName4, false);
                setVerifyVal(verifyBoxF5, app.FushoName5, false);

                setVerifyVal(verifyBoxDrCode, app.DrNum, false);
                setVerifyVal(verifyBoxDrName, app.DrName, false);
                setVerifyVal(verifyBoxClinicName, app.ClinicName, false);
                setVerifyVal(verifyBoxAccountNum, app.AccountNumber, false);
                setVerifyVal(verifyBoxAccountName, app.AccountName, false);
            }
            missCounterReset();
        }

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
