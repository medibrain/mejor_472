﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class MemoErrorListForm : Form
    {
        int cym;
        List<AppView> appList = new List<AppView>();
        BindingSource bs = new BindingSource();


        class AppView
        {
            public AppView(App app)=> this.app = app;

            App app;
            public int Aid => app.Aid;
            public int ScanID => app.ScanID;
            public int GroupID => app.GroupID;
            public APP_TYPE AppType => app.AppType;
            public string HihoNum => app.HihoNum;
            public string InputError => app.StatusFlagCheck(StatusFlag.入力時エラー) ? "○" : "";
            public string Process1 => app.StatusFlagCheck(StatusFlag.処理1) ? "○" : "";
            public string Process2 => app.StatusFlagCheck(StatusFlag.処理2) ? "○" : "";
            public string Process3 => app.StatusFlagCheck(StatusFlag.処理3) ? "○" : "";
            public string Process4 => app.StatusFlagCheck(StatusFlag.処理4) ? "○" : "";
            public string Process5 => app.StatusFlagCheck(StatusFlag.処理5) ? "○" : "";
            public string Numbering => app.Numbering;
            public string Memo => app.Memo;

        }

        public MemoErrorListForm(int cym)
        {
            InitializeComponent();

            this.cym = cym;
            bs.DataSource = new List<AppView>();
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(AppView.Aid)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Aid)].HeaderText = "AID";
            dataGridView1.Columns[nameof(AppView.Aid)].Width = 70;
            dataGridView1.Columns[nameof(AppView.ScanID)].Visible = true;
            dataGridView1.Columns[nameof(AppView.ScanID)].HeaderText = "ScanID";
            dataGridView1.Columns[nameof(AppView.ScanID)].Width = 60;
            dataGridView1.Columns[nameof(AppView.GroupID)].Visible = true;
            dataGridView1.Columns[nameof(AppView.GroupID)].HeaderText = "GroupID";
            dataGridView1.Columns[nameof(AppView.GroupID)].Width = 60;
            dataGridView1.Columns[nameof(AppView.AppType)].Visible = true;
            dataGridView1.Columns[nameof(AppView.AppType)].HeaderText = "種類";
            dataGridView1.Columns[nameof(AppView.AppType)].Width = 70;
            dataGridView1.Columns[nameof(AppView.HihoNum)].Visible = true;
            dataGridView1.Columns[nameof(AppView.HihoNum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(AppView.HihoNum)].Width = 70;
            dataGridView1.Columns[nameof(AppView.Numbering)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Numbering)].HeaderText = "ナンバリング";
            dataGridView1.Columns[nameof(AppView.Numbering)].Width = 80;
            dataGridView1.Columns[nameof(AppView.InputError)].Visible = true;
            dataGridView1.Columns[nameof(AppView.InputError)].HeaderText = "エラー";
            dataGridView1.Columns[nameof(AppView.InputError)].Width = 50;
            dataGridView1.Columns[nameof(AppView.Process1)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Process1)].HeaderText = "処1";
            dataGridView1.Columns[nameof(AppView.Process1)].Width = 40;
            dataGridView1.Columns[nameof(AppView.Process2)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Process2)].HeaderText = "処2";
            dataGridView1.Columns[nameof(AppView.Process2)].Width = 40;
            dataGridView1.Columns[nameof(AppView.Process3)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Process3)].HeaderText = "処3";
            dataGridView1.Columns[nameof(AppView.Process3)].Width = 40;
            dataGridView1.Columns[nameof(AppView.Process4)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Process4)].HeaderText = "処4";
            dataGridView1.Columns[nameof(AppView.Process4)].Width = 40;
            dataGridView1.Columns[nameof(AppView.Process5)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Process5)].HeaderText = "処5";
            dataGridView1.Columns[nameof(AppView.Process5)].Width = 40;
            dataGridView1.Columns[nameof(AppView.Memo)].Visible = true;
            dataGridView1.Columns[nameof(AppView.Memo)].HeaderText = "メモ";
            dataGridView1.Columns[nameof(AppView.Memo)].Width = 400;

            GetList();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetList()
        {
            var f = new WaitFormSimple();
            System.Threading.Tasks.Task.Factory.StartNew(() => f.ShowDialog());
            appList.Clear();

            try
            {
                var where = ($"WHERE " +
                    $"(a.statusflags=(a.statusflags|{(int)StatusFlag.入力時エラー}) " +
                    $"OR a.memo<>'') " +
                    $"AND a.cym={cym}");
                App.GetAppsWithWhere(where).ForEach(x => appList.Add(new AppView(x)));
                updateList();
            }
            finally
            {
                while (!f.Visible) System.Threading.Thread.Sleep(10);
                f.InvokeCloseDispose();
            }
        }

        private void updateList()
        {
            var l = appList.FindAll(x =>
                (checkBoxErrorOn.Checked ? true : x.InputError != "○") &&
                (checkBoxErrorOff.Checked ? true : x.InputError == "○") &&
                (checkBoxProcess1On.Checked ? true : x.Process1 != "○") &&
                (checkBoxProcess1Off.Checked ? true : x.Process1 == "○") &&
                (checkBoxProcess2On.Checked ? true : x.Process2 != "○") &&
                (checkBoxProcess2Off.Checked ? true : x.Process2 == "○"));
            bs.DataSource = l;
            bs.ResetBindings(false);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var av = (AppView)bs.Current;
            if (av == null) return;

            var firstIndex = dataGridView1.FirstDisplayedScrollingRowIndex;
            InputStarter.Start(av.GroupID, INPUT_TYPE.First, av.Aid);

            SuspendLayout();
            GetList();
            dataGridView1.FirstDisplayedScrollingRowIndex = firstIndex;
            ResumeLayout();
        }

        private void buttonCSV_Click(object sender, EventArgs e)
        {
            using (var f = new SaveFileDialog())
            {
                f.FileName = "メモ＆エラーリスト.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                Utility.ViewToCsv(dataGridView1, f.FileName);
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            updateList();
        }
    }
}
