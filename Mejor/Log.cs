﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Mejor
{
    public static class Log
    {
        static string erdir = System.Windows.Forms.Application.StartupPath +
            "\\" + "\\Error\\";
        
        static string infodir = System.Windows.Forms.Application.StartupPath +
            "\\" + "\\Info\\";


        //20200528153814 furukawa st ////////////////////////
        //エラーログファイルのパスを共通で使用する
        
        static string fileName = erdir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";
        //20200528153814 furukawa ed ////////////////////////

        static Log()
        {
            var func = new Func<string, bool>((p) =>
                {
                    if (System.IO.Directory.Exists(p)) return true;
                    try
                    {
                        System.IO.Directory.CreateDirectory(p);
                        return true;
                    }
                    catch
                    {
                        System.Windows.Forms.MessageBox.Show("ログディレクトリの作成に失敗しました。" +
                            "受信は中断されています。設定を見直し、ソフトを起動しなおしてください。");
                        return false;
                    }
                });

            if (!func(erdir)) return;
            if (!func(infodir)) return;
        }


        //20200528153725 furukawa st ////////////////////////
        //エラーログをテキストエディタで開きユーザに知らせる
        //20200528153725 furukawa ed ////////////////////////
        /// <summary>
        /// エラーログをテキストエディタで開きユーザに知らせる
        /// </summary>
        public static void OpenErrorFileByTextEditor()
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo(fileName);
            p.StartInfo = si;
            p.Start();

        }

        /// <summary>
        /// 任意のエラーメッセージをログファイルに書き込みます
        /// </summary>
        /// <param name="logText"></param>
        /// <returns></returns>
        public static void ErrorWrite(string logText)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    //20200528153905 furukawa st ////////////////////////
                    //エラーログファイルのパスを共通変数にした

                    //string fileName = erdir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";//DateTime.Today.ToString
                    //20200528153905 furukawa ed ////////////////////////

                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + ",\r\n" + logText);
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }
        }

        /// <summary>
        /// 任意のエラーメッセージを表示し、ログファイルに書き込みます
        /// </summary>
        /// <param name="logText"></param>
        /// <returns></returns>
        public static void ErrorWriteWithMsg(string logText)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    //20200528153933 furukawa st ////////////////////////
                    //エラーログファイルのパスを共通変数にした

                    //string fileName = erdir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";//DateTime.Today.ToString
                    //20200528153933 furukawa ed ////////////////////////
                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + ",\r\n" + logText);
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }

            System.Windows.Forms.MessageBox.Show("エラーが発生しました\r\n\r\n" + logText,
                "エラー", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error);
        }

        /// <summary>
        /// システムエラーをログファイルに書き込みます
        /// </summary>
        /// <param name="ex"></param>
        public static void ErrorWrite(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    //20200528153957 furukawa st ////////////////////////
                    //エラーログファイルのパスを共通変数にした

                    //string fileName = erdir + "Error" + DateTime.Now.ToString("yyMMddHHmmss") + ".log";
                    //20200528153957 furukawa ed ////////////////////////
                    if (System.IO.File.Exists(fileName))
                    {
                        System.Threading.Thread.Sleep(10);
                        continue;
                    }

                    using (var sw = new System.IO.StreamWriter(fileName))
                    {
                        sw.Write(ex.ToString());
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }
        }

        /// <summary>
        /// システムエラーを表示し、ログファイルに書き込みます
        /// </summary>
        /// <param name="ex"></param>
        public static void ErrorWriteWithMsg(Exception ex)
        {
            for (int i = 0; i < 20; i++)
            {
                try
                {
                    //20200528154045 furukawa st ////////////////////////
                    //エラーログファイルのパスを共通変数にした

                    //string fileName = erdir + "Error" + DateTime.Now.ToString("yyMMddHHmmss") + ".log";
                    //20200528154045 furukawa ed ////////////////////////

                    if (System.IO.File.Exists(fileName))
                    {
                        System.Threading.Thread.Sleep(10);
                        continue;
                    }

                    using (var sw = new System.IO.StreamWriter(fileName))
                    {
                        sw.Write(ex.ToString());
                    }
                }
                catch
                {
                    System.Threading.Thread.Sleep(100);
                    continue;
                }
                break;
            }

            System.Windows.Forms.MessageBox.Show("エラーが発生しました\r\n\r\n" + ex.Message,
                "エラー", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error);
        }
        
        /// <summary>
        /// インフォメーションを書き込みます
        /// </summary>
        /// <param name="str"></param>
        public static void InfoWrite(string str)
        {
            DateTime dt = DateTime.Now;
            var fileName = infodir + dt.ToString("yyyyMMdd") + ".log";

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(dt.ToString("HH:mm:ss:fff\t") + str);
                }
            }
            catch
            {
                Log.ErrorWrite("インフォログ書き込み失敗 :" + str);
            }
        }
    }
}
