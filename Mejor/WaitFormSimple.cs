﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class WaitFormSimple : Form
    {
        public WaitFormSimple()
        {
            InitializeComponent();
        }

        public void InvokeCloseDispose()
        {
            Invoke(new Action(() =>
            {
                this.Close();
                this.Dispose();
            }));
        }
    }
}
