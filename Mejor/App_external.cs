﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using NPOI.SS.UserModel;

namespace Mejor
{
    /// <summary>
    /// 外部業者からインポートされるApp。通常のAppと同じ
    /// </summary>
    public class App_external
    {
        #region member
        [DB.DbAttribute.PrimaryKey]
        public int Aid { get; set; }
        public int ScanID { get; set; }
        public int GroupID { get; set; }

        public int MediYear { get; set; }
        public int MediMonth { get; set; }
        public string InsNum { get; set; } = string.Empty;

        //被保険者情報
        public string HihoNum { get; set; } = string.Empty;
        public int HihoPref { get; set; }
        public int HihoType { get; set; }
        public string HihoName { get; set; } = string.Empty;
        public string HihoZip { get; set; } = string.Empty;
        public string HihoAdd { get; set; } = string.Empty;
        public string PersonName { get; set; } = string.Empty;
        public int Sex { get; set; }
        public DateTime Birthday { get; set; }

        public string ClinicNum { get; set; } = string.Empty;
        private int Single { get; set; }
        public int Family { get; set; }
        public int Ratio { get; set; }
        public string PublcExpense { get; set; } = string.Empty;    //公費番号

        private string ImageFile { get; set; } = string.Empty;
        public int ChargeYear { get; private set; }//処理年
        public int ChargeMonth { get; private set; }//処理月


        //負傷関連情報
        public string FushoName1 { get; set; } = string.Empty;      //負傷名
        public DateTime FushoDate1 { get; set; }                    //初診日（最初に受診した日付）
        public DateTime FushoFirstDate1 { get; set; }               //初検日（最初にその負傷を認めた日）
        public DateTime FushoStartDate1 { get; set; }               //施術開始年月日（その負傷の治療を開始した日）
        public DateTime FushoFinishDate1 { get; set; }              //施術終了年月日（その負傷の治療を終了した日）
        public int FushoDays1 { get; set; }                         //実日数（診療に要した日数）
        public int FushoCourse1 { get; set; }                       //転帰
        public string FushoName2 { get; set; } = string.Empty;
        public DateTime FushoDate2 { get; set; }
        public DateTime FushoFirstDate2 { get; set; }
        public DateTime FushoStartDate2 { get; set; }
        public DateTime FushoFinishDate2 { get; set; }
        public int FushoDays2 { get; set; }
        public int FushoCourse2 { get; set; }
        public string FushoName3 { get; set; } = string.Empty;
        public DateTime FushoDate3 { get; set; }
        public DateTime FushoFirstDate3 { get; set; }
        public DateTime FushoStartDate3 { get; set; }
        public DateTime FushoFinishDate3 { get; set; }
        public int FushoDays3 { get; set; }
        public int FushoCourse3 { get; set; }
        public string FushoName4 { get; set; } = string.Empty;
        public DateTime FushoDate4 { get; set; }
        public DateTime FushoFirstDate4 { get; set; }
        public DateTime FushoStartDate4 { get; set; }
        public DateTime FushoFinishDate4 { get; set; }
        public int FushoDays4 { get; set; }
        public int FushoCourse4 { get; set; }
        public string FushoName5 { get; set; } = string.Empty;
        public DateTime FushoDate5 { get; set; }
        public DateTime FushoFirstDate5 { get; set; }
        public DateTime FushoStartDate5 { get; set; }
        public DateTime FushoFinishDate5 { get; set; }
        public int FushoDays5 { get; set; }
        public int FushoCourse5 { get; set; }


        //新規継続区分
        public NEW_CONT NewContType { get; set; }

        //往療関連
        public int Distance { get; set; }
        public int VisitTimes { get; set; }
        public int VisitFee { get; set; }
        public int VisitAdd { get; set; }


        //施術し関連情報
        public string DrNum { get; set; } = string.Empty;
        public string ClinicZip { get; set; } = string.Empty;
        public string ClinicAdd { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public string ClinicTel { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string DrKana { get; set; } = string.Empty;
        public int AccountType { get; set; }
        public string BankName { get; set; } = string.Empty;
        public int BankType { get; set; }
        public string BankBranch { get; set; } = string.Empty;
        public int BankBranchType { get; set; }
        public string AccountName { get; set; } = string.Empty;
        public string AccountKana { get; set; } = string.Empty;
        public string AccountNumber { get; set; } = string.Empty;


        //お金関連
        public int Total { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int CountedDays { get; set; }
        public string Numbering { get; set; } = string.Empty;
        public APP_TYPE AppType { get; set; }



        //補足・付随
        public int Ufirst { get; set; }
        public int Usecond { get; set; }
        public int Uinquiry { get; set; }
        public int Bui { get; set; }//あんま部位




        public int CYM { get; set; }
        public int YM { get; set; }
        public StatusFlag StatusFlags { get; private set; }
        public ShokaiReason ShokaiReason { get; private set; }
        public int RrID { get; set; }



        public int AdditionalUid1 { get; set; }
        public int AdditionalUid2 { get; set; }
        public string MemoShokai { get; set; } = string.Empty;
        public string MemoInspect { get; set; } = string.Empty;
        public string Memo { get; set; } = string.Empty;
        public string PayCode { get; set; } = string.Empty;
        public string ShokaiCode { get; set; } = string.Empty;
        public string OcrData { get; set; } = string.Empty;
        public int UfirstEx { get; set; }
        public int UsecondEx { get; set; }

        public KagoReasons KagoReasons { get; set; }//現行


        public SaishinsaReasons SaishinsaReasons { get; set; }
        public HenreiReasons HenreiReasons { get; set; }
        public string ComNum { get; set; }
        public string GroupNum { get; set; }
        public string OutMemo { get; set; } = string.Empty;
        public TaggedDatas TaggedDatas { get; private set; }

        public string strKagoReasonsXML { get; set; }//試験用xml文字列取得



        public string JCYM => DateTimeEx.GetInitialEraJpYearMonth(CYM);
        public string JYM => DateTimeEx.GetInitialEraJpYearMonth(YM);


        //データベース外
        public int ViewIndex { get; set; }
        public bool Select { get; set; } = false;

        public int FushoCount =>
            !string.IsNullOrWhiteSpace(FushoName5) ? 5 :
            !string.IsNullOrWhiteSpace(FushoName4) ? 4 :
            !string.IsNullOrWhiteSpace(FushoName3) ? 3 :
            !string.IsNullOrWhiteSpace(FushoName2) ? 2 :
            !string.IsNullOrWhiteSpace(FushoName1) ? 1 : 0;

   


              
        /// <summary>
        /// KagoReason_xmlの初期化文字列
        /// </summary>
        public string KagoReasons_xml_InitString =
            "<kagoreasons>\r" +
            "<なし>false</なし>\r" +
            "<署名違い>false</署名違い>\r" +
            "<筆跡違い>false</筆跡違い>\r" +
            "<家族同一筆跡>false</家族同一筆跡>\r" +
            "<その他>false</その他>\r" +
            "<原因なし>false</原因なし>\r" +
            "<原因なし1>false</原因なし1>\r" +
            "<原因なし2>false</原因なし2>\r" +
            "<原因なし3>false</原因なし3>\r" +
            "<原因なし4>false</原因なし4>\r" +
            "<原因なし5>false</原因なし5>\r" +
            "<負傷原因相違>false</負傷原因相違>\r" +
            "<負傷原因相違1>false</負傷原因相違1>\r" +
            "<負傷原因相違2>false</負傷原因相違2>\r" +
            "<負傷原因相違3>false</負傷原因相違3>\r" +
            "<負傷原因相違4>false</負傷原因相違4>\r" +
            "<負傷原因相違5>false</負傷原因相違5>\r" +
            "<長期理由なし>false</長期理由なし>\r" +
            "<長期理由なし1>false</長期理由なし1>\r" +
            "<長期理由なし2>false</長期理由なし2>\r" +
            "<長期理由なし3>false</長期理由なし3>\r" +
            "<長期理由なし4>false</長期理由なし4>\r" +
            "<長期理由なし5>false</長期理由なし5>\r" +
            "<長期理由相違>false</長期理由相違>\r" +
            "<長期理由相違1>false</長期理由相違1>\r" +
            "<長期理由相違2>false</長期理由相違2>\r" +
            "<長期理由相違3>false</長期理由相違3>\r" +
            "<長期理由相違4>false</長期理由相違4>\r" +
            "<長期理由相違5>false</長期理由相違5>\r" +
            "<療養費請求権の消滅時効>false</療養費請求権の消滅時効>\r" +
            "<本家区分誤り>false</本家区分誤り>\r" +
            "<保険者相違>false</保険者相違>\r" +
            "<往療理由記載なし>false</往療理由記載なし>\r" +
            "<往療16km以上>false</往療16km以上>\r" +
            "<同意書>false</同意書>\r" +
            "<同意書添付なし>false</同意書添付なし>\r" +
            "<同意書期限切れ>false</同意書期限切れ>\r" +
            "<施術報告書>false</施術報告書>\r" +
            "<施術報告書添付なし>false</施術報告書添付なし>\r" +
            "<施術報告書記載不備>false</施術報告書記載不備>\r" +
            "<施術継続理由状態記入書>false</施術継続理由状態記入書>\r" +
            "<施術継続理由状態記入書添付なし>false</施術継続理由状態記入書添付なし>\r" +
            "<施術継続理由状態記入書記載不備>false</施術継続理由状態記入書記載不備>\r" +

            
            //往療内訳書追加            
            "<往療内訳書>false</往療内訳書>\r" +
            "<往療内訳書添付なし>false</往療内訳書添付なし>\r" +
            "<往療内訳書記載不備>false</往療内訳書記載不備>\r" +
            

            "<独自>false</独自>\r" +
            "<独自1>false</独自1>\r" +
            "<独自2>false</独自2>\r" +
            "<独自3>false</独自3>\r" +
            "<独自4>false</独自4>\r" +
            "<独自5>false</独自5>\r" +
            "</kagoreasons>";




        /// <summary>
        /// 過誤理由設定値
        /// </summary>
        public enum FlgValue {
            add,
            del,
            other,
            check,

        }

        #endregion

     
        /// <summary>
        /// SELECTをかける時に期待するSELECT文はCreateAppFromRecordで使用する
        /// 順番である必要があるので、ここで定義。FROM application as aを忘れずに
        /// </summary>
        private const string SELECT_CLAUSE =
            "a.aid, a.scanid, a.groupid, a.ayear, a.amonth, a.inum, a.hnum, a.hpref, a.htype, a.hname, " +
            "a.hzip, a.haddress, a.pname, a.psex, a.pbirthday, a.sid, a.asingle, a.afamily, a.aratio, " +
            "a.publcexpense, a.aimagefile, a.achargeyear, a.achargemonth, " +
            "a.iname1, a.idate1, a.ifirstdate1, a.istartdate1, a.ifinishdate1, a.idays1, a.icourse1, " +
            "a.iname2, a.idate2, a.ifirstdate2, a.istartdate2, a.ifinishdate2, a.idays2, a.icourse2, " +
            "a.iname3, a.idate3, a.ifirstdate3, a.istartdate3, a.ifinishdate3, a.idays3, a.icourse3, " +
            "a.iname4, a.idate4, a.ifirstdate4, a.istartdate4, a.ifinishdate4, a.idays4, a.icourse4, " +
            "a.iname5, a.idate5, a.ifirstdate5, a.istartdate5, a.ifinishdate5, a.idays5, a.icourse5, " +
            "a.fchargetype, a.fdistance, a.fvisittimes, a.fvisitfee, a.fvisitadd, " +
            "a.sregnumber, a.szip, a.saddress, a.sname, a.stel, a.sdoctor, a.skana, " +
            "a.bacctype, a.bname, a.btype, a.bbranch, a.bbranchtype, a.baccname, a.bkana, a.baccnumber, " +
            "a.atotal, a.apartial, a.acharge, a.acounteddays, a.numbering, a.aapptype, " +
            "a.ufirst, a.usecond, a.uinquiry, a.bui, a.cym, a.ym, a.statusflags, " +
            "a.shokaireason, a.rrid, a.additionaluid1, a.additionaluid2, a.memo_shokai, " +
            "a.memo_inspect, a.memo, a.paycode, a.shokaicode, a.ufirstex, a.usecondex, a.ocrdata, " +
            "a.KagoReasons, " +//試験xml用
            "a.saishinsareasons, a.henreireasons, a.comnum, a.groupnum, a.outmemo, " +
            "a.taggeddatas ," +
            "a.kagoreasons_xml ";//試験xml用



        /// <summary>
        /// aIdで一枚の申請書を抽出
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static App_external GetApp(int aid)
        {
            using (var cmd = DB.Main.CreateCmd(
                "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                return CreateAppFromRecord(res[0]);
            }
        }



        /// <summary>
        /// 請求年月6桁で複数の申請書を抽出
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <param name="insID">保険者(デフォルトは0)</param>
        /// <returns></returns>
        public static List<App_external> GetApps(int cym, InsurerID insID = 0)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE cym=:cym " +
                "ORDER BY ";

            switch (insID)
            {
                case InsurerID.NAGOYA_KOUWAN:
                    sql += "a.paycode,a.afamily";
                    break;

                default:
                    sql += "a.aid";
                    break;
            }


            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
            });
        }




        /// <summary>
        /// WHERE句を指定して複数の申請書を抽出
        /// </summary>
        /// <param name="year"></param>
        /// <param name="where">where句 ※WHEREを含み、各カラムにaをつける</param>
        /// <returns></returns>
        public static List<App_external> GetAppsWithWhere(string where)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                where + " " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, null);
        }

        /// <summary>
        /// WHERE句を指定して複数の申請書を抽出
        /// </summary>
        /// <param name="year"></param>
        /// <param name="where">where句 ※WHEREを含み、各カラムにaをつける</param>
        /// <returns></returns>
        public static List<App_external> GetAppsWithLimit(string where, int limit, bool asc)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                $"{where} ORDER BY a.aid {(asc ? "" : "DESC ")}" +
                $"LIMIT {limit}";

            var l = GetAppFromSql(sql, null);
            l.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            return l;
        }

        /// <summary>
        /// スキャンIDで複数の申請書を抽出
        /// </summary>
        /// <param name="scanID"></param>
        /// <returns></returns>
        public static List<App_external> GetAppsSID(int scanID)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.scanid=:scanid " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
                {
                    cmd.Parameters.Add("scanid", NpgsqlDbType.Integer).Value = scanID;
                });
        }

        /// <summary>
        /// 被保番で過去半年分と次月のApp_external一覧を取得します
        /// </summary>
        /// <param name="hnum"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static List<App_external> GetAppsFamily(string hnum, int cym)
        {
            int sym = DateTimeEx.Int6YmAddMonth(cym, -6);
            int eym = DateTimeEx.Int6YmAddMonth(cym, 1);

            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.hnum=:hnum ";

            sql += "AND a.cym BETWEEN :sym AND :eym " +
                "ORDER BY aid";

            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = hnum;
                cmd.Parameters.Add("sym", NpgsqlDbType.Integer).Value = sym;
                cmd.Parameters.Add("eym", NpgsqlDbType.Integer).Value = eym;
            });
        }

        /// <summary>
        /// グループIDで複数の申請書を抽出
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static List<App_external> GetAppsGID(int groupID)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.groupid=:gid " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
                {
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;
                });
        }

        /// <summary>
        /// SQLが吐き出したobjectを元に、App_externalを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static App_external CreateAppFromRecord(object[] obj)
        {
            int i = 0;
            var a = new App_external();
            a.Aid = (int)obj[i++];
            a.ScanID = (int)obj[i++];
            a.GroupID = (int)obj[i++];
            a.MediYear = (int)obj[i++];
            a.MediMonth = (int)obj[i++];
            a.InsNum = (string)obj[i++];
            a.HihoNum = (string)obj[i++];
            a.HihoPref = (int)obj[i++];
            a.HihoType = (int)obj[i++];
            a.HihoName = (string)obj[i++];
            a.HihoZip = (string)obj[i++];
            a.HihoAdd = (string)obj[i++];
            a.PersonName = (string)obj[i++];
            a.Sex = (int)obj[i++];
            a.Birthday = (DateTime)obj[i++];
            a.ClinicNum = (string)obj[i++];
            a.Single = (int)obj[i++];
            a.Family = (int)obj[i++];
            a.Ratio = (int)obj[i++];
            a.PublcExpense = (string)obj[i++];
            a.ImageFile = (string)obj[i++];
            a.ChargeYear = (int)obj[i++];
            a.ChargeMonth = (int)obj[i++];
            a.FushoName1 = (string)obj[i++];
            a.FushoDate1 = (DateTime)obj[i++];
            a.FushoFirstDate1 = (DateTime)obj[i++];
            a.FushoStartDate1 = (DateTime)obj[i++];
            a.FushoFinishDate1 = (DateTime)obj[i++];
            a.FushoDays1 = (int)obj[i++];
            a.FushoCourse1 = (int)obj[i++];
            a.FushoName2 = (string)obj[i++];
            a.FushoDate2 = (DateTime)obj[i++];
            a.FushoFirstDate2 = (DateTime)obj[i++];
            a.FushoStartDate2 = (DateTime)obj[i++];
            a.FushoFinishDate2 = (DateTime)obj[i++];
            a.FushoDays2 = (int)obj[i++];
            a.FushoCourse2 = (int)obj[i++];
            a.FushoName3 = (string)obj[i++];
            a.FushoDate3 = (DateTime)obj[i++];
            a.FushoFirstDate3 = (DateTime)obj[i++];
            a.FushoStartDate3 = (DateTime)obj[i++];
            a.FushoFinishDate3 = (DateTime)obj[i++];
            a.FushoDays3 = (int)obj[i++];
            a.FushoCourse3 = (int)obj[i++];
            a.FushoName4 = (string)obj[i++];
            a.FushoDate4 = (DateTime)obj[i++];
            a.FushoFirstDate4 = (DateTime)obj[i++];
            a.FushoStartDate4 = (DateTime)obj[i++];
            a.FushoFinishDate4 = (DateTime)obj[i++];
            a.FushoDays4 = (int)obj[i++];
            a.FushoCourse4 = (int)obj[i++];
            a.FushoName5 = (string)obj[i++];
            a.FushoDate5 = (DateTime)obj[i++];
            a.FushoFirstDate5 = (DateTime)obj[i++];
            a.FushoStartDate5 = (DateTime)obj[i++];
            a.FushoFinishDate5 = (DateTime)obj[i++];
            a.FushoDays5 = (int)obj[i++];
            a.FushoCourse5 = (int)obj[i++];
            a.NewContType = (NEW_CONT)(int)obj[i++];
            a.Distance = (int)obj[i++];
            a.VisitTimes = (int)obj[i++];
            a.VisitFee = (int)obj[i++];
            a.VisitAdd = (int)obj[i++];

            a.DrNum = (string)obj[i++];
            a.ClinicZip = (string)obj[i++];
            a.ClinicAdd = (string)obj[i++];
            a.ClinicName = (string)obj[i++];
            a.ClinicTel = (string)obj[i++];
            a.DrName = (string)obj[i++];
            a.DrKana = (string)obj[i++];
            a.AccountType = (int)obj[i++];
            a.BankName = (string)obj[i++];
            a.BankType = (int)obj[i++];
            a.BankBranch = (string)obj[i++];
            a.BankBranchType = (int)obj[i++];
            a.AccountName = (string)obj[i++];
            a.AccountKana = (string)obj[i++];
            a.AccountNumber = (string)obj[i++];
            a.Total = (int)obj[i++];
            a.Partial = (int)obj[i++];
            a.Charge = (int)obj[i++];
            a.CountedDays = (int)obj[i++];
            a.Numbering = (string)obj[i++];
            a.AppType = (APP_TYPE)(int)obj[i++];
            a.Ufirst = (int)obj[i++];
            a.Usecond = (int)obj[i++];
            a.Uinquiry = (int)obj[i++];
            a.Bui = (int)obj[i++];
            a.CYM = (int)obj[i++];
            a.YM = (int)obj[i++];
            a.StatusFlags = (StatusFlag)(int)obj[i++];
            a.ShokaiReason = (ShokaiReason)(int)obj[i++];
            a.RrID = (int)obj[i++];

            a.AdditionalUid1 = (int)obj[i++];
            a.AdditionalUid2 = (int)obj[i++];
            a.MemoShokai = (string)obj[i++];
            a.MemoInspect = (string)obj[i++];
            a.Memo = (string)obj[i++];
            a.PayCode = (string)obj[i++];
            a.ShokaiCode = (string)obj[i++];
            a.UfirstEx = (int)obj[i++];
            a.UsecondEx = (int)obj[i++];
            a.OcrData = (string)obj[i++];

            a.KagoReasons = (KagoReasons)(int)obj[i++];
            //i++;


            a.SaishinsaReasons = (SaishinsaReasons)(int)obj[i++];
            a.HenreiReasons = (HenreiReasons)(int)obj[i++];
            a.ComNum = (string)obj[i++];
            a.GroupNum = (string)obj[i++];
            a.OutMemo = (string)obj[i++];



            a.TaggedDatas = new TaggedDatas((string)obj[i++]);

            a.strKagoReasonsXML = obj[i++].ToString();

            return a;
        }


    
                
        /// <summary>
        /// 続紙と申請書の関連付け
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool CreateRelation(int cym)
        {

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                
                //ストアド用sqlファイルを引数に
                
                //20220604115236 furukawa st ////////////////////////
                //グループIDが飛んでいる対策のストアドに変更
                
                if (!CreateStoredProcedure(wf, "ParentRelation3.sql"))
                //      if (!CreateStoredProcedure(wf, "ParentRelation2.sql"))
                //20220604115236 furukawa ed ////////////////////////
                {
                    System.Windows.Forms.MessageBox.Show($"ストアド作成失敗に失敗しました。処理を中止します");
                    return false;
                }
                if (!UpdateZokushi(cym,wf)) 
                {
                    System.Windows.Forms.MessageBox.Show($"続紙と申請書の関連付けに失敗しました。処理を中止します");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }


        //20210629165737 furukawa st ////////////////////////
        //全DBに作成しておくとメンテが手間なのでここで作成

        /// <summary>
        /// ストアドを現在の保険者のDBに作成
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        /// 


        //20210708182317 furukawa st ////////////////////////
        //ストアド用sqlファイルを引数に
        
        private static bool CreateStoredProcedure(WaitForm wf,string strStored)
        //private static bool CreateStoredProcedure(WaitForm wf)

        //20210708182317 furukawa ed ////////////////////////

        {
            wf.LogPrint("ストアド作成中");
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();

            //20210708182419 furukawa st ////////////////////////
            //バッチに渡す引数にストアドファイル名追加
            
            string strarg = $" {DB.getDBHost()} {Settings.dbPort} {DB.GetMainDBName()} {strStored}";
            //string strarg = $" {DB.getDBHost()} {Settings.dbPort} {DB.GetMainDBName()}";

            //20210708182419 furukawa ed ////////////////////////

            try
            {
                psi.Arguments = strarg;

                psi.FileName = Application.StartupPath + "\\Stored\\create_stored.bat";
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = psi;


                p.Start();
                p.WaitForExit(8000);
                //p.WaitForExit(5000);

                wf.LogPrint("ストアド作成完了");

                return true;

            }
            catch (Exception ex) 
            { 
                
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
           
        }
        //20210629165737 furukawa ed ////////////////////////


        //20210629165816 furukawa st ////////////////////////
        //ストアドを走らせて続紙と申請書の関連付け実行。クライアントからやると遅すぎて終わらない

        /// <summary>
        /// ParentRelationストアドを走らせる
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool UpdateZokushi(int cym,WaitForm wf)
        {
            
            string strdb = string.Empty;
            strdb = DB.GetMainDBName();
            DB.SetMainDBName("common");
            DB.SetMainDBName(strdb);
            
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("続紙と申請書を関連付けています");
        
                DB.Command cmd = new DB.Command(DB.Main, $"select ParentRelation({cym})");
                var cnt = cmd.TryExecuteScalar();
                string tmp = cnt.ToString();

                wf.LogPrint($"{tmp}件　終了");
                
                MessageBox.Show($"{tmp}件　終了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
            }
        }
        //20210629165816 furukawa ed ////////////////////////


        //20210708184917 furukawa st ////////////////////////
        //バッチシートと申請書を紐付け
        /// <summary>
        /// バッチシートと申請書を紐付け
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool RelateBatch(int cym,WaitForm wf)
        {
                       
            try
            {       
                if (!CreateStoredProcedure(wf, "BatchSheetRelation.sql"))         
                {
                    System.Windows.Forms.MessageBox.Show($"ストアド作成失敗に失敗しました。処理を中止します");
                    return false;
                }
                if (!UpdateBatchSheet(cym, wf))
                {
                    System.Windows.Forms.MessageBox.Show($"バッチシートと申請書の関連付けに失敗しました。処理を中止します");
                    return false;
                }

                MessageBox.Show("終了しました");
                
                return true;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
              
            }
        }
        


        /// <summary>
        /// バッチシートと申請書を関連付け
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool UpdateBatchSheet(int cym, WaitForm wf)
        {

            string strdb = string.Empty;
            strdb = DB.GetMainDBName();
            

            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("バッチシートと申請書を関連付けています");

                DB.Command cmd = new DB.Command(DB.Main, $"select BatchSheetRelation({cym})");
                var cnt = cmd.TryExecuteScalar();
                string tmp = cnt.ToString();

                wf.LogPrint($"{tmp}件　終了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
            }
        }



        //20210708184917 furukawa ed ////////////////////////

   

     
        /// <summary>
        /// App_externalインスタンスデータをDBから取得する
        /// </summary>
        /// <param name="sql">applicationテーブルをクエリするデータ</param>
        /// <returns>取得したデータ</returns>
        public static List<App_external> GetAppFromSql(string sql, Action<DB.Command> procParams)
        {
            var l = new List<App_external>();

            using (var cmd = DB.Main.CreateLongTimeoutCmd(sql))
            {
                // パラメータ処理
                procParams?.Invoke(cmd);
                var res = cmd.TryExecuteReaderList();
                if (res != null)
                {
                    foreach (var item in res)
                    {
                        l.Add(CreateAppFromRecord(item));
                    }
                }
            }

            return l;
        }
    }


}