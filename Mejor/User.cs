﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;

namespace Mejor
{
    //20211224095259 furukawa st ////////////////////////
    //ユーザ権限管理に伴うテーブル変更対応
    
    [DB.DbAttribute.DifferentTableName("users_setting")]
    //  [DB.DbAttribute.DifferentTableName("users")]
    //20211224095259 furukawa ed ////////////////////////


    public class User
    {

        //20211224095406 furukawa st ////////////////////////
        //ユーザ権限管理に伴うテーブル変更対応
        [DB.DbAttribute.PrimaryKey]
        public int UserID { get; set; }
        public string Name { get; set; }
        public string LoginName { get; set; }
        public string Pass { get; set; }
        public string belong { get; set; }
        public bool Enable { get; set; }
        public bool Admin { get; set; }
        public bool Developer { get; set; }
        public bool manager { get; set; }
        public bool generaluser { get; set; }
        public bool limiteduser { get; set; }
        public DateTime entrydate { get; set; }
        public DateTime updatedate { get; set; }

        //public int UserID { get; private set; }
        //public string Name { get; set; }
        //public string LoginName { get; set; }
        //public string Pass { get; set; }
        //public bool Enable { get; set; }
        //public bool Admin { get; set; }
        //public bool Developer { get; set; }
        
        
        //20211224095406 furukawa ed ////////////////////////

        public enum UserBelong
        {
            メディブレーン,
            外部業者,
            該当無し,
        }

        private static Dictionary<int, User> userList = new Dictionary<int, User>();
        [DB.DbAttribute.Ignore]
        public static User CurrentUser { get; private set; } = null;

        public User() { }
             
        public User(int id, string name)
        {
            this.UserID = id;           
            this.Name = name;
        }

        public bool Insert()
        {
            var currentDBName = DB.GetMainDBName();
            DB.SetMainDBName("jyusei");

            try
            {
                //20211224095431 furukawa st ////////////////////////
                //テーブル変更に伴うフィールド変更
                
                var sql = "INSERT INTO users " +
                    "(userid, name, loginname, pass, enable, admin,manager,generaluser,limiteduser,entrydate) " +
                    $"VALUES(@userid, @name, @loginname, @pass, @enable, @admin,@manager,@generaluser,@limiteduser,{DateTime.Now.ToString("yyyy-MM-dd")})";


                //var sql = "INSERT INTO users " +
                //    "(userid, name, loginname, pass, enable, admin) " +
                //    "VALUES(@userid, @name, @loginname, @pass, @enable, @admin)";

                //20211224095431 furukawa ed ////////////////////////


                return DB.Main.Excute(sql, this);
            }
            finally
            {
                DB.SetMainDBName(currentDBName);
            }
        }

        public bool Update()
        {
            var currentDBName = DB.GetMainDBName();
            DB.SetMainDBName("jyusei");

            try
            {
                //20211224095501 furukawa st ////////////////////////
                //テーブル変更に伴うフィールド変更
                
                var sql = "UPDATE users SET " +
                    "name=@name, loginname=@loginname, pass=@pass, enable=@enable, admin=@admin ," +
                    $"manager=@manager,generaluser=@generaluser,limiteduser=@limiteduser , updatedate={DateTime.Now.ToString("yyyy-MM-dd")}" +
                    "WHERE userid=@userid;";


                //var sql = "UPDATE users SET " +
                //  "name=@name, loginname=@loginname, pass=@pass, enable=@enable, admin=@admin " +
                //  "WHERE userid=@userid;";

                //20211224095501 furukawa ed ////////////////////////

                return DB.Main.Excute(sql, this);
            }
            finally
            {
                DB.SetMainDBName(currentDBName);
            }
        }


        /// <summary>
        /// 20220623102522 furukawa ユーザがどこに所属するかを判定する        
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static UserBelong checkUserBelong(int userID)
        {
            if (userList.ContainsKey(userID))
            {
                //9000番台は外部業者
                //20220713094753 furukawa st ////////////////////////
                //userid頭9で判定すると90，900もダメになるので修正
                
                if (userID>=9000)
                //if (userID.ToString().Substring(0, 1) == "9")
                //20220713094753 furukawa ed ////////////////////////

                {
                    return UserBelong.外部業者;
                }
                else return UserBelong.メディブレーン;
            }
            return UserBelong.該当無し;
        }

        /// <summary>
        /// 20220623103004 furukawa ユーザがどこに所属するかを文字列で返す        
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static string GetUserBelong(int userID)
        {
            if (userList.ContainsKey(userID))
            {
                return GetUser(userID).belong;                
            }
            return String.Empty;
        }


        public static void GetUsers()
        {
            var currentDBName = DB.GetMainDBName();
            DB.SetMainDBName("jyusei");

            try
            {
                var l = DB.Main.SelectAll<User>();
                userList.Clear();
                l.ToList().ForEach(u => userList.Add(u.UserID, u));
            }
            finally
            {
                DB.SetMainDBName(currentDBName);
            }
        }

        //UserIDに対応するユーザー名を取得
        public static string GetUserName(int userID)
        {
            if (userList.ContainsKey(userID))                
                return userList[userID].Name;
            else
                return string.Empty;
        }

        //UserIDに対応するユーザーを取得
        public static User GetUser(int userID)
        {
            if (userList.ContainsKey(userID))
                return userList[userID];
            else
                return null;
        }

        //ログインしているユーザーを記憶
        public static void SetCurrentUser(int userID)
        {
            var u = GetUser(userID);
            CurrentUser = u;
        }

        public static List<User> GetList()
        {
            return userList.Values.ToList();
        }   
    }
}
