﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Ocr
{
    public partial class OcrViewForm : Form
    {
        BindingSource bs, bs2;

        public App App
        {
            set
            {
                bs.Clear();
                bs2.Clear();

                var ss = value.OcrData.Split(',');
                for (int i = 0; i < ss.Length; i++)
                {
                    bs.Add(new { Index = i, Value = ss[i] });
                }
                dataGridViewRaw.DataSource = bs;

                var d = new OcrDatas(value);
                bs2.Add(new { Name = "診療年", Value = d.MediYear.ToString() });
                bs2.Add(new { Name = "診療月", Value = d.MediMonth.ToString() });
                bs2.Add(new { Name = "被保番", Value = d.HihoNum });
                bs2.Add(new { Name = "生年月日", Value = d.Birth.ToJDateStr() });
                bs2.Add(new { Name = "性別", Value = d.Sex.ToString() });
                bs2.Add(new { Name = "実日数", Value = d.Days.ToString() });
                bs2.Add(new { Name = "合計", Value = d.Total.ToString("#,0") });
                bs2.Add(new { Name = "一部負担", Value = d.Partial.ToString("#,0") });
                bs2.Add(new { Name = "請求", Value = d.Charge.ToString("#,0") });
                bs2.Add(new { Name = "施術師記号", Value = d.DrSign });
                bs2.Add(new { Name = "施術師番号", Value = d.DrCode });
                bs2.Add(new { Name = "負傷名1", Value = d.Fusho1 });
                bs2.Add(new { Name = "負傷名2", Value = d.Fusho2 });
                bs2.Add(new { Name = "負傷名3", Value = d.Fusho3 });
                bs2.Add(new { Name = "負傷名4", Value = d.Fusho4 });
                bs2.Add(new { Name = "負傷名5", Value = d.Fusho5 });

                bs.ResetBindings(false);
                bs2.ResetBindings(false);
            }
        }

        public OcrViewForm(App app)
        {
            InitializeComponent();
            bs = new BindingSource() { new { Index = 0, Value = string.Empty } };
            bs2 = new BindingSource() { new { Name = string.Empty, Value = string.Empty } };
            dataGridViewRaw.DataSource = bs;
            dataGridViewSelect.DataSource = bs2;

            App = app;
            dataGridViewRaw.Columns[0].Width = 50;
            dataGridViewSelect.Columns[1].Width = 200;
            //20201202194658 furukawa st ////////////////////////
            //幅調整
            
            dataGridViewRaw.Columns[1].Width = 200;
            //20201202194658 furukawa ed ////////////////////////
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
