﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Mejor.Ocr
{
    class OcrHelper
    {
        static Regex re = new Regex(@"[^0-9-]");

        class OcrPerson
        {
            public string hnum { get; set; } = string.Empty;
            public DateTime pbirthday { get; set; }
        }

        public class Result
        {
            public bool UsePerson { get; set; } = false;
            public string HihoNum { get; set; } = string.Empty;
            public SEX Sex { get; set; } = SEX.Null;
            public DateTime Birth { get; set; } = DateTimeEx.DateTimeNull;
            public string DrNum { get; set; } = string.Empty;
            public int Total { get; set; }
            public int Charge { get; set; }
        }


        public static Result Check(App app, Ocr.OcrDatas o)
        {
            var r = new Result();

            //金額
            if (o.Total > 1000 && o.Total % 10 != 1 &&
                ((o.Total * 70 / 100) == o.Charge || (o.Total * 80 / 100) == o.Charge))
            {
                r.Total = o.Total;
                r.Charge = o.Charge;
            }

            var st = DateTimeEx.Int6YmAddMonth(app.CYM, -7);
            var fn = DateTimeEx.Int6YmAddMonth(app.CYM, -1);

            //療養者情報
            ExistsPerson(st, fn, app, o, r);
            if (!r.UsePerson) return r;

            //施術師コード
            ExistsDr(st, fn, app, o, r);

            return r;
        }

        private static void ExistsPerson(int sCYM, int fCYM, App app, Ocr.OcrDatas o, Result r)
        {
            if (!o.BirthCandidate || o.Sex == SEX.Null) return;

            var sql = "SELECT hnum, pbirthday FROM application " +
                $"WHERE pbirthday IN " +
                $"('{o.BirthMj.ToString("yyyy-M-d")}'," +
                $"'{o.BirthTs.ToString("yyyy-M-d")}'," +
                $"'{o.BirthSw.ToString("yyyy-M-d")}'," +
                $"'{o.BirthHs.ToString("yyyy-M-d")}') " +
                $"AND psex={(int)o.Sex} " +
                $"AND cym BETWEEN {sCYM} AND {fCYM} " +
                $"GROUP BY hnum, pbirthday " +
                $"HAVING COUNT(hnum)>1;";
            var psRes = DB.Main.Query<OcrPerson>(sql).ToList();
            var ps = new List<OcrPerson>();

            psRes.Sort((x, y) => y.hnum.Length.CompareTo(x.hnum.Length));
            foreach (var item in psRes)
            {
                if (item.hnum == string.Empty) continue;
                if (item.pbirthday.IsNullDate()) continue;
                var temp = re.Replace(item.hnum, string.Empty);
                if (o.ReplacedHihoNum.Length < temp.Length) continue;
                var s = o.ReplacedHihoNum.Substring(o.HihoNum.Length - temp.Length);
                if (s == temp) ps.Add(item);
            }

            var p = ps.FirstOrDefault(op => op.pbirthday == o.Birth);
            if (p != null)
            {
                r.UsePerson = true;
                r.Sex = o.Sex;
                r.Birth = p.pbirthday;
                r.HihoNum = p.hnum;
            }
            else if (ps.Count == 1)
            {
                p = ps.First();
                r.UsePerson = true;
                r.Sex = o.Sex;
                r.Birth = p.pbirthday;
                r.HihoNum = p.hnum;
            }
        }

        private static void ExistsDr(int sCYM, int fCYM, App app, Ocr.OcrDatas o, Result r)
        {
            //施術師コード
            var sql = "SELECT sregnumber FROM application " +
                $"WHERE hnum='{r.HihoNum}' " +
                $"AND cym BETWEEN {sCYM} AND {fCYM};";
            var res = DB.Main.Query<string>(sql);

            var dic = new Dictionary<string, int>();
            foreach (var item in res)
            {
                if (!dic.ContainsKey(item)) dic.Add(item, 0);
                dic[item]++;
            }

            var l = dic.Where(x =>
            {
                if (x.Value < 2) return false;
                var code = x.Key.Length > 8 ? x.Key.Substring(1) : x.Key;
                for (int i = 0; i <= o.DrCode.Length - code.Length; i++)
                {
                    var s = o.DrCode.Substring(i, code.Length);
                    if (s == code) return true;
                }
                return false;
            });

            var c = l.Count();

            if (c == 1)
            {
                r.DrNum = l.First().Key;
            }
            else if (c == 2)
            {
                var c1 = l.ElementAt(0);
                var c2 = l.ElementAt(1);
                if (c1.Value - c2.Value > 1)
                {
                    r.DrNum = c1.Key;
                }
                else if (c2.Value - c1.Value > 1)
                {
                    r.DrNum = c2.Key;
                }
            }
        }

        public static void Test(int cym)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint($"{Insurer.CurrrentInsurer.InsurerName} " +
                    $"{cym.ToString("0000/00")}  申請書を取得しています");
                var l = App.GetApps(cym);

                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(l.Count);

                int appCount = 0;
                int ocrCount = 0;
                int personMatchCount = 0;
                int personMissCount = 0;
                int drMatchCount = 0;
                int drMissCount = 0;
                int priceMatchCount = 0;
                int priceMissCount = 0;

                wf.LogPrint($"OCRと過去データを比較しています");
                foreach (var item in l)
                {
                    wf.InvokeValue++;
                    if (item.MediYear < 1) continue;
                    if (item.AppType != APP_TYPE.柔整) continue;
                    var o = new Ocr.OcrDatas(item);
                    var r = Check(item, o);

                    appCount++;
                    var od = new Ocr.OcrDatas(item);
                    if (od.HihoNum.Length == 0 || !od.BirthCandidate || od.Sex == SEX.Null) continue;
                    ocrCount++;

                    if (r.UsePerson)
                    {
                        personMatchCount++;
                        if (item.HihoNum != r.HihoNum || item.Birthday != r.Birth || item.Sex != (int)r.Sex)
                        {
                            wf.LogPrint($"突合ミス AID:{item.Aid} " +
                                $"OCR被保番:{r.HihoNum} 入力被保番:{item.HihoNum} " +
                                $"OCR生日:{r.Birth.ToString("yyyy/M/d")} 入力生日:{item.Birthday.ToString("yyyy/M/d")} " +
                                $"OCR性別:{(int)od.Sex} 入力性別:{item.Sex}");
                            personMissCount++;
                        }
                    }

                    if (r.DrNum.Length != 0)
                    {
                        drMatchCount++;
                        if (r.DrNum != item.DrNum)
                        {
                            wf.LogPrint($"施術師 AID:{item.Aid} OCR:{r.DrNum} 入力:{item.DrNum}");
                            drMissCount++;
                        }
                    }

                    if(r.Total!=0)
                    {
                        priceMatchCount++;
                        if (r.Total != item.Total || r.Charge != item.Charge)
                        {
                            wf.LogPrint($"金額ミス AID:{item.Aid} OCR合計:{r.Total} 入力合計:{item.Total} " +
                                $"OCR請求:{r.Charge} 入力請求:{item.Charge}");
                            priceMissCount++;
                        }
                    }
                }

                var w =
                    $"柔整枚数:{appCount}\r\n" +
                    $"OCR枚数:{ocrCount}\r\n" +
                    $"受療者マッチ:{personMatchCount}\r\n" +
                    $"受療者エラー:{personMissCount}\r\n" +
                    $"施術師マッチ:{drMatchCount}\r\n" +
                    $"施術師エラー:{drMissCount}\r\n" +
                    $"金額マッチ:{priceMatchCount}\r\n" +
                    $"金額エラー:{priceMissCount}";

                wf.LogPrint("OCRと過去データの突合比較が終了しました\r\n" + w);
                using (var f = new System.Windows.Forms.SaveFileDialog())
                {
                    f.Title = "比較ログ保存";
                    f.RestoreDirectory = true;
                    var d = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                    f.InitialDirectory = d;
                    f.FileName = $"OCR過去データ突合_{Insurer.CurrrentInsurer.InsurerName}_{cym}.txt";

                    if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        wf.LogSave(f.FileName);
                    }
                }
            }
        }
    }
}
