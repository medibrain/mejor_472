﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace Mejor.Ocr
{
    class OcrDatas
    {
        static Regex re = new Regex(@"[^0-9]");

        public int MediYear { get; private set; }
        public int MediMonth { get; private set; }
        public string HihoNum { get; private set; } = string.Empty;
        public string ReplacedHihoNum => re.Replace(HihoNum, "-");
        public SEX Sex { get; private set; }
        public bool BirthCandidate { get; private set; } = false;
        public DateTime Birth { get; private set; }
        public DateTime BirthMj { get; private set; }
        public DateTime BirthTs { get; private set; }
        public DateTime BirthSw { get; private set; }
        public DateTime BirthHs { get; private set; }

        public DateTime BirthRw { get; private set; }//20190722115450 furukawa 令和対応
                                                     
        public int Total { get; private set; }
        public int Partial { get; private set; }
        public int Charge { get; private set; }
        public int Days { get; private set; }
        public string Fusho1 { get; private set; } = string.Empty;
        public string Fusho2 { get; private set; } = string.Empty;
        public string Fusho3 { get; private set; } = string.Empty;
        public string Fusho4 { get; private set; } = string.Empty;
        public string Fusho5 { get; private set; } = string.Empty;
        public DateTime FirstDate { get; private set; }

        /// <summary>
        /// 施術師コードの数字部分
        /// </summary>
        public string DrCode { get; private set; } = string.Empty;

        /// <summary>
        /// 施術師コードの協/契
        /// </summary>
        public string DrSign { get; private set; }

        public OcrDatas(App app)
        {
            var ocr = app.OcrData.Split(',');

            if (ocr.Length > 100)
                getJyuDatas(ocr);
            else if (ocr.Length > 10)
                getAhakiDatas(ocr);
        }

        private void getJyuDatas(string[] ocr)
        {
            if (ocr.Length < 39) return;

            int temp = 0;
            string tStr = string.Empty;

            //診療年
            int.TryParse(re.Replace(ocr[3], string.Empty), out temp);
            MediYear = temp;

            //診療月
            int.TryParse(re.Replace(ocr[4], string.Empty), out temp);
            MediMonth = temp;

            //被保険者番号
            HihoNum = ocr[7];
            //20201206095716 furukawa st ////////////////////////
            //学校共済の被保険者証番号は後ろ6桁の数字のみ取る
            
            if (Insurer.CurrrentInsurer.InsurerName.Contains("GAKKO")) HihoNum = Microsoft.VisualBasic.Strings.Right(ocr[7].ToString(), 6);
            //20201206095716 furukawa ed ////////////////////////


            //性別
            string sexStr = string.Empty;
            Sex = ocr[30] != "0" && ocr[31] == "0" ? SEX.男 :
                ocr[30] == "0" && ocr[31] != "0" ? SEX.女 : SEX.Null;

            //生年月日 年号
            int era = 0;
            if (ocr[32] != "0") era += 11;
            if (ocr[33] != "0") era += 12;
            if (ocr[34] != "0") era += 13;
            if (ocr[35] != "0") era += 14;
            era = era > 20 ? 0 : era - 10;

            //生年月日 年
            tStr = ocr[36];
            var yi = tStr.IndexOf('年');
            if (yi > 0) tStr = tStr.Remove(yi);
            int.TryParse(re.Replace(tStr, string.Empty), out int y);

            //生年月日 月
            tStr = ocr[37];
            var mi = tStr.IndexOf('月');
            if (mi > 0) tStr = tStr.Remove(mi);
            int.TryParse(re.Replace(tStr, string.Empty), out int m);

            //生年月日 日
            tStr = ocr[38];
            var di = tStr.IndexOf('日');
            if (di > 0) tStr = tStr.Remove(di);
            int.TryParse(re.Replace(tStr, string.Empty), out int d);

            Birth = DateTimeEx.GetDateFromJInt7(era * 1000000 + y * 10000 + m * 100 + d);
            BirthMj = DateTimeEx.GetDateFromJInt7(1 * 1000000 + y * 10000 + m * 100 + d);
            BirthTs = DateTimeEx.GetDateFromJInt7(2 * 1000000 + y * 10000 + m * 100 + d);
            BirthSw = DateTimeEx.GetDateFromJInt7(3 * 1000000 + y * 10000 + m * 100 + d);
            BirthHs = DateTimeEx.GetDateFromJInt7(4 * 1000000 + y * 10000 + m * 100 + d);
          
            //20190722115533 furukawa st ////////////////////////
            //令和対応
            BirthRw = DateTimeEx.GetDateFromJInt7(5 * 1000000 + y * 10000 + m * 100 + d);
            //20190722115533 furukawa ed ////////////////////////

            BirthCandidate = !BirthSw.IsNullDate();

            if (ocr.Length < 196) return;

            //合計
            int.TryParse(re.Replace(ocr[193], string.Empty), out temp);
            Total = temp;

            //一部負担
            int.TryParse(re.Replace(ocr[194], string.Empty), out temp);
            Partial = temp;

            //請求
            int.TryParse(re.Replace(ocr[195], string.Empty), out temp);
            Charge = temp;

            //実日数
            var idays = new List<int>();
            int iday;
            int.TryParse(re.Replace(ocr[53], string.Empty), out iday);
            idays.Add(iday);
            int.TryParse(re.Replace(ocr[70], string.Empty), out iday);
            idays.Add(iday);
            int.TryParse(re.Replace(ocr[87], string.Empty), out iday);
            idays.Add(iday);
            int.TryParse(re.Replace(ocr[104], string.Empty), out iday);
            idays.Add(iday);
            int.TryParse(re.Replace(ocr[121], string.Empty), out iday);
            idays.Add(iday);
            Days = idays.Max();

            if (ocr.Length < 215) return;

            ////医療機関コード
            DrCode = re.Replace(ocr[214], string.Empty);
            DrSign =
                ocr[214].Contains("協") ? "協" :
                ocr[214].Contains("契") ? "契" :
                string.Empty;

            //負傷名
            Fusho1 = ocr[40];
            Fusho2 = ocr[57];
            Fusho3 = ocr[74];
            Fusho4 = ocr[91];
            Fusho5 = ocr[108];
        }

        private void getAhakiDatas(string[] ocr)
        {
            if (ocr.Length < 40) return;

            //被保険者番号
            HihoNum = ocr[6];

            int temp = 0;
            string tStr = string.Empty;

            //診療年
            tStr = ocr[3];
            temp = tStr.IndexOf('年');
            if (temp> 2) tStr = tStr.Substring(temp - 2, 2);
            int.TryParse(re.Replace(tStr, string.Empty), out temp);
            MediYear = temp;

            //診療月
            if(int.TryParse(re.Replace(ocr[4], string.Empty), out temp))
            {
                MediMonth = temp;
            }
            else
            {
                tStr = ocr[3];
                temp = tStr.IndexOf('月');
                if (temp > 2)
                {
                    tStr = tStr.Substring(temp - 2, 2);
                    int.TryParse(re.Replace(tStr, string.Empty), out temp);
                    MediMonth = temp;
                }
            }

            //生年月日 年号
            int era = 0;
            if (ocr[11] != "0") era = 1;
            if (ocr[12] != "0") era = era == 0 ? 2 : 0;
            if (ocr[13] != "0") era = era == 0 ? 3 : 0;
            if (ocr[14] != "0") era = era == 0 ? 4 : 0;

            //生年月日 年
            tStr = ocr[15];
            var yi = tStr.IndexOf('年');
            if (yi > 0) tStr = tStr.Remove(yi);
            int.TryParse(re.Replace(tStr, string.Empty), out int y);

            //生年月日 月
            tStr = ocr[16];
            var mi = tStr.IndexOf('月');
            if (mi > 0) tStr = tStr.Remove(mi);
            int.TryParse(re.Replace(tStr, string.Empty), out int m);

            //生年月日 日
            tStr = ocr[17];
            var di = tStr.IndexOf('日');
            if (di > 0) tStr = tStr.Remove(di);
            int.TryParse(re.Replace(tStr, string.Empty), out int d);

            Birth = DateTimeEx.GetDateFromJInt7(era * 1000000 + y * 10000 + m * 100 + d);

            //実日数
            tStr = ocr[20];
            var dsi = tStr.IndexOf('日');
            if (dsi > 0) tStr = tStr.Remove(dsi);
            int.TryParse(re.Replace(tStr, string.Empty), out int days);
            Days = days;

            //合計
            int.TryParse(re.Replace(ocr[36], string.Empty), out temp);
            Total = temp;

            //一部負担
            int.TryParse(re.Replace(ocr[37], string.Empty), out temp);
            Partial = temp;

            //請求
            int.TryParse(re.Replace(ocr[38], string.Empty), out temp);
            Charge = temp;
        }
    }
}
