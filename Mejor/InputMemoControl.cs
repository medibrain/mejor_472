﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class InputMemoControl : UserControl
    {
        App app = null;
        bool appSetting = false;

        //どのコントロールから飛んできたのか控え、戻す先とする
        public Control returnControl;

        public InputMemoControl()
        {
            InitializeComponent();
        }

        public void SetApp(App app)
        {
            appSetting = true;
            labelMemoUpdate.Visible = false;
            labelEditCancel.Visible = false;

            this.app = app;
            if (app == null)
            {
                borderCheckBoxError.Checked = false;
                textBoxMemo.Clear();
            }
            else
            {
                borderCheckBoxError.Checked = app.StatusFlagCheck(StatusFlag.入力時エラー);
                textBoxMemo.Text = app.Memo;
            }
            appSetting = false;
        }

        private void labelMemoUpdate_Click(object sender, EventArgs e)
        {
            //更新
            if (app != null)
            {
                app.Memo = textBoxMemo.Text.Trim();
                if (borderCheckBoxError.Checked) app.StatusFlagSet(StatusFlag.入力時エラー);
                else app.StatusFlagRemove(StatusFlag.入力時エラー);

                if (app.MemoAndFlagsUpdate())
                {
                    labelMemoUpdate.Visible = false;
                    labelEditCancel.Visible = false;
                }
                else
                {
                    MessageBox.Show("メモ情報のアップデートに失敗しました");
                }
            }
        }

        private void labelEditCancel_Click(object sender, EventArgs e)
        {
            if (app == null) return;

            appSetting = true;

            labelMemoUpdate.Visible = false;
            labelEditCancel.Visible = false;

            textBoxMemo.Text = app.Memo;
            borderCheckBoxError.Checked = app.StatusFlagCheck(StatusFlag.入力時エラー);

            appSetting = false;
        }

        
      

        private void borderCheckBoxError_CheckedChanged(object sender, EventArgs e)
        {
            borderCheckBoxError.BackColor = borderCheckBoxError.Checked ?
                Color.MistyRose : SystemColors.Control;

            setLabelVisible();
        }

        private void textBoxMemo_TextChanged(object sender, EventArgs e)
        {
            setLabelVisible();
        }

        private void setLabelVisible()
        {
            if (appSetting) return;
            labelMemoUpdate.Visible = true;
            labelEditCancel.Visible = true;
        }


        
        private void textBoxMemo_KeyDown(object sender, KeyEventArgs e)
        {

            //20200217094656 furukawa st ////////////////////////
            //エラーチェックボックス＋登録をCtrl+Insert
            
            if (e.KeyCode == Keys.Insert && e.Control == true)
            {
                borderCheckBoxError.Checked = !borderCheckBoxError.Checked;
                returnControl.Focus();
            }
            //20200217094656 furukawa ed ////////////////////////

            
            //20200205175528 furukawa st ////////////////////////
            //メモの登録をInsertキーで実行
       
            if (e.KeyCode == Keys.Insert)
            {
                labelMemoUpdate_Click(sender, e);
                returnControl.Focus();
            }
            //20200205175528 furukawa ed ////////////////////////    

        }

    }
}
