﻿namespace Mejor.Shizuokashi_Ahaki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo1 = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.verifyBox1 = new Mejor.VerifyBox();
            this.verifyBox7 = new Mejor.VerifyBox();
            this.verifyBox12 = new Mejor.VerifyBox();
            this.labelFamily = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.verifyBoxHnumS = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.label10 = new System.Windows.Forms.Label();
            this.verifyBoxBui = new Mejor.VerifyBox();
            this.pDoui = new System.Windows.Forms.Panel();
            this.vbDouiG = new Mejor.VerifyBox();
            this.vbDouiD = new Mejor.VerifyBox();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDoui = new System.Windows.Forms.Label();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.labelHnameCopy = new System.Windows.Forms.Label();
            this.pBirthday = new System.Windows.Forms.Panel();
            this.verifyBoxBirthD = new Mejor.VerifyBox();
            this.verifyBoxBirthM = new Mejor.VerifyBox();
            this.verifyBoxBirthY = new Mejor.VerifyBox();
            this.verifyBoxBirthE = new Mejor.VerifyBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.verifyBoxPname = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxHihoname = new Mejor.VerifyBox();
            this.label46 = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxRatio = new Mejor.VerifyBox();
            this.label41 = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.label36 = new System.Windows.Forms.Label();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.btnAccMaintenance = new System.Windows.Forms.Button();
            this.dgvbk = new System.Windows.Forms.DataGridView();
            this.pFusho = new System.Windows.Forms.Panel();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pShinsei = new System.Windows.Forms.Panel();
            this.verifyBoxSG = new Mejor.VerifyBox();
            this.verifyBoxSY = new Mejor.VerifyBox();
            this.verifyBoxSM = new Mejor.VerifyBox();
            this.verifyBoxSD = new Mejor.VerifyBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.verifyBoxAccountNum = new Mejor.VerifyBox();
            this.pZenkai = new System.Windows.Forms.Panel();
            this.verifyBoxZG = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxZM = new Mejor.VerifyBox();
            this.verifyBoxZY = new Mejor.VerifyBox();
            this.cbZenkai = new Mejor.VerifyCheckBox();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.verifyBoxHosName = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.pBirthday.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbk)).BeginInit();
            this.pFusho.SuspendLayout();
            this.pShinsei.SuspendLayout();
            this.pZenkai.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 753);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(196, 25);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(19, 13);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(40, 21);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(96, 21);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(43, 13);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelYearInfo1
            // 
            this.labelYearInfo1.AutoSize = true;
            this.labelYearInfo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYearInfo1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo1.Location = new System.Drawing.Point(565, 181);
            this.labelYearInfo1.Name = "labelYearInfo1";
            this.labelYearInfo1.Size = new System.Drawing.Size(59, 36);
            this.labelYearInfo1.TabIndex = 0;
            this.labelYearInfo1.Text = "続紙: --\r\n不要: ++\r\nヘッダ:**";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(123, 25);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(31, 13);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 782);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 753);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 753);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-191, 753);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 753);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 782);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Controls.Add(this.pDoui);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelYearInfo1);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 782);
            this.panelRight.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.verifyBox1);
            this.panel2.Controls.Add(this.verifyBox7);
            this.panel2.Controls.Add(this.verifyBox12);
            this.panel2.Controls.Add(this.labelFamily);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.verifyBoxHnumS);
            this.panel2.Controls.Add(this.verifyBoxFamily);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.verifyBoxBui);
            this.panel2.Location = new System.Drawing.Point(135, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(725, 180);
            this.panel2.TabIndex = 256;
            this.panel2.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label29.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label29.Location = new System.Drawing.Point(501, 39);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 24);
            this.label29.TabIndex = 66;
            this.label29.Text = "本人:2\r\n家族:6";
            // 
            // verifyBox1
            // 
            this.verifyBox1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox1.Location = new System.Drawing.Point(92, 36);
            this.verifyBox1.MaxLength = 10;
            this.verifyBox1.Name = "verifyBox1";
            this.verifyBox1.NewLine = false;
            this.verifyBox1.Size = new System.Drawing.Size(100, 23);
            this.verifyBox1.TabIndex = 20;
            this.verifyBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBox1.TextV = "";
            this.verifyBox1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBox7
            // 
            this.verifyBox7.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox7.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox7.Location = new System.Drawing.Point(475, 40);
            this.verifyBox7.Name = "verifyBox7";
            this.verifyBox7.NewLine = false;
            this.verifyBox7.Size = new System.Drawing.Size(26, 23);
            this.verifyBox7.TabIndex = 65;
            this.verifyBox7.TextV = "";
            // 
            // verifyBox12
            // 
            this.verifyBox12.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBox12.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBox12.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBox12.Location = new System.Drawing.Point(614, 26);
            this.verifyBox12.MaxLength = 1;
            this.verifyBox12.Name = "verifyBox12";
            this.verifyBox12.NewLine = false;
            this.verifyBox12.Size = new System.Drawing.Size(26, 23);
            this.verifyBox12.TabIndex = 11;
            this.verifyBox12.TextV = "";
            this.verifyBox12.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(245, 46);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(55, 13);
            this.labelFamily.TabIndex = 14;
            this.labelFamily.Text = "本家区分";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(555, 25);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(55, 26);
            this.label45.TabIndex = 45;
            this.label45.Text = "一般退職\r\n区分";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(416, 52);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(60, 13);
            this.label30.TabIndex = 64;
            this.label30.Text = "本人/家族";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "保険者番号";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(73, 117);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "部位数";
            // 
            // verifyBoxHnumS
            // 
            this.verifyBoxHnumS.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumS.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumS.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumS.Location = new System.Drawing.Point(242, 98);
            this.verifyBoxHnumS.Name = "verifyBoxHnumS";
            this.verifyBoxHnumS.NewLine = false;
            this.verifyBoxHnumS.Size = new System.Drawing.Size(52, 23);
            this.verifyBoxHnumS.TabIndex = 10;
            this.verifyBoxHnumS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnumS.TextV = "";
            this.verifyBoxHnumS.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(307, 34);
            this.verifyBoxFamily.MaxLength = 1;
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 15;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label10.Location = new System.Drawing.Point(339, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 36);
            this.label10.TabIndex = 44;
            this.label10.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７";
            // 
            // verifyBoxBui
            // 
            this.verifyBoxBui.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBui.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBui.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBui.Location = new System.Drawing.Point(74, 135);
            this.verifyBoxBui.MaxLength = 2;
            this.verifyBoxBui.Name = "verifyBoxBui";
            this.verifyBoxBui.NewLine = false;
            this.verifyBoxBui.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxBui.TabIndex = 9;
            this.verifyBoxBui.TextV = "";
            this.verifyBoxBui.Enter += new System.EventHandler(this.item_Enter);
            // 
            // pDoui
            // 
            this.pDoui.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Controls.Add(this.label8);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Location = new System.Drawing.Point(692, 588);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(300, 65);
            this.pDoui.TabIndex = 80;
            this.pDoui.Enter += new System.EventHandler(this.item_Enter);
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(100, 10);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(247, 10);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(195, 10);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(141, 10);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(52, 7);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 26);
            this.label8.TabIndex = 84;
            this.label8.Text = "4:平成\r\n5:令和";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Enabled = false;
            this.lblDoui.Location = new System.Drawing.Point(8, 8);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(43, 26);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(225, 20);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(19, 13);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(278, 20);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(19, 13);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(173, 20);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(19, 13);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(3, 7);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(119, 72);
            this.labelYearInfo.TabIndex = 258;
            this.labelYearInfo.Text = "続紙    : \"--\"\r\n不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  : \"911\"\r\n状態記入書  " +
    ": \"921\"";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 111);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1015, 330);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.labelHnameCopy);
            this.panelHnum.Controls.Add(this.pBirthday);
            this.panelHnum.Controls.Add(this.labelMacthCheck);
            this.panelHnum.Controls.Add(this.verifyBoxPname);
            this.panelHnum.Controls.Add(this.label6);
            this.panelHnum.Controls.Add(this.verifyBoxHihoname);
            this.panelHnum.Controls.Add(this.label46);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.verifyBoxRatio);
            this.panelHnum.Controls.Add(this.label41);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.label36);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Location = new System.Drawing.Point(217, 4);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(800, 100);
            this.panelHnum.TabIndex = 10;
            // 
            // labelHnameCopy
            // 
            this.labelHnameCopy.AutoSize = true;
            this.labelHnameCopy.Location = new System.Drawing.Point(614, 50);
            this.labelHnameCopy.Name = "labelHnameCopy";
            this.labelHnameCopy.Size = new System.Drawing.Size(118, 26);
            this.labelHnameCopy.TabIndex = 97;
            this.labelHnameCopy.Text = "Ctrl+Enter \r\n（被保険者氏名 貼付）";
            // 
            // pBirthday
            // 
            this.pBirthday.Controls.Add(this.verifyBoxBirthD);
            this.pBirthday.Controls.Add(this.verifyBoxBirthM);
            this.pBirthday.Controls.Add(this.verifyBoxBirthY);
            this.pBirthday.Controls.Add(this.verifyBoxBirthE);
            this.pBirthday.Controls.Add(this.label35);
            this.pBirthday.Controls.Add(this.label39);
            this.pBirthday.Controls.Add(this.label38);
            this.pBirthday.Controls.Add(this.label37);
            this.pBirthday.Controls.Add(this.labelBirthday);
            this.pBirthday.Location = new System.Drawing.Point(99, 38);
            this.pBirthday.Name = "pBirthday";
            this.pBirthday.Size = new System.Drawing.Size(279, 55);
            this.pBirthday.TabIndex = 48;
            this.pBirthday.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(224, 12);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.NewLine = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthD.TabIndex = 59;
            this.verifyBoxBirthD.TextV = "";
            this.verifyBoxBirthD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(170, 12);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.NewLine = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthM.TabIndex = 55;
            this.verifyBoxBirthM.TextV = "";
            this.verifyBoxBirthM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(118, 12);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.NewLine = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(32, 23);
            this.verifyBoxBirthY.TabIndex = 53;
            this.verifyBoxBirthY.TextV = "";
            this.verifyBoxBirthY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxBirthE
            // 
            this.verifyBoxBirthE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthE.Location = new System.Drawing.Point(63, 12);
            this.verifyBoxBirthE.MaxLength = 1;
            this.verifyBoxBirthE.Name = "verifyBoxBirthE";
            this.verifyBoxBirthE.NewLine = false;
            this.verifyBoxBirthE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBirthE.TabIndex = 50;
            this.verifyBoxBirthE.TextV = "";
            this.verifyBoxBirthE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label35.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label35.Location = new System.Drawing.Point(89, 7);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 36);
            this.label35.TabIndex = 24;
            this.label35.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(259, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "日";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(202, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 13);
            this.label38.TabIndex = 28;
            this.label38.Text = "月";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(150, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 13);
            this.label37.TabIndex = 26;
            this.label37.Text = "年";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(5, 22);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 22;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(706, 11);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(86, 13);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // verifyBoxPname
            // 
            this.verifyBoxPname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPname.Location = new System.Drawing.Point(460, 51);
            this.verifyBoxPname.MaxLength = 25;
            this.verifyBoxPname.Name = "verifyBoxPname";
            this.verifyBoxPname.NewLine = false;
            this.verifyBoxPname.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxPname.TabIndex = 63;
            this.verifyBoxPname.TextV = "";
            this.verifyBoxPname.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxPname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxPname_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(402, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 62;
            this.label6.Text = "受療者名";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHihoname
            // 
            this.verifyBoxHihoname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihoname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihoname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHihoname.Location = new System.Drawing.Point(372, 11);
            this.verifyBoxHihoname.MaxLength = 25;
            this.verifyBoxHihoname.Name = "verifyBoxHihoname";
            this.verifyBoxHihoname.NewLine = false;
            this.verifyBoxHihoname.Size = new System.Drawing.Size(150, 23);
            this.verifyBoxHihoname.TabIndex = 15;
            this.verifyBoxHihoname.TextV = "";
            this.verifyBoxHihoname.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(299, 21);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 13);
            this.label46.TabIndex = 60;
            this.label46.Text = "被保険者名";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(5, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(597, 11);
            this.verifyBoxRatio.MaxLength = 1;
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.NewLine = false;
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxRatio.TabIndex = 18;
            this.verifyBoxRatio.TextV = "";
            this.verifyBoxRatio.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(540, 21);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 17;
            this.label41.Text = "給付割合";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(144, 11);
            this.verifyBoxHnum.MaxLength = 8;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label36.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label36.Location = new System.Drawing.Point(67, 50);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 24);
            this.label36.TabIndex = 21;
            this.label36.Text = "男: 1\r\n女: 2";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(40, 51);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 45;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(7, 61);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 19;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(157, 14);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.btnAccMaintenance);
            this.panelTotal.Controls.Add(this.dgvbk);
            this.panelTotal.Controls.Add(this.pFusho);
            this.panelTotal.Controls.Add(this.pShinsei);
            this.panelTotal.Controls.Add(this.label27);
            this.panelTotal.Controls.Add(this.verifyBoxAccountNum);
            this.panelTotal.Controls.Add(this.pZenkai);
            this.panelTotal.Controls.Add(this.cbZenkai);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Controls.Add(this.label33);
            this.panelTotal.Controls.Add(this.verifyBoxHosName);
            this.panelTotal.Controls.Add(this.label31);
            this.panelTotal.Controls.Add(this.verifyBoxDrName);
            this.panelTotal.Controls.Add(this.labelNumbering);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.label2);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Location = new System.Drawing.Point(3, 440);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1000, 310);
            this.panelTotal.TabIndex = 70;
            // 
            // btnAccMaintenance
            // 
            this.btnAccMaintenance.Location = new System.Drawing.Point(13, 256);
            this.btnAccMaintenance.Name = "btnAccMaintenance";
            this.btnAccMaintenance.Size = new System.Drawing.Size(70, 27);
            this.btnAccMaintenance.TabIndex = 151;
            this.btnAccMaintenance.TabStop = false;
            this.btnAccMaintenance.Text = "口座メンテ";
            this.btnAccMaintenance.UseVisualStyleBackColor = true;
            this.btnAccMaintenance.Click += new System.EventHandler(this.btnAccMaintenance_Click);
            // 
            // dgvbk
            // 
            this.dgvbk.AllowUserToAddRows = false;
            this.dgvbk.AllowUserToDeleteRows = false;
            this.dgvbk.AllowUserToResizeRows = false;
            this.dgvbk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvbk.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvbk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvbk.Location = new System.Drawing.Point(97, 215);
            this.dgvbk.Name = "dgvbk";
            this.dgvbk.ReadOnly = true;
            this.dgvbk.RowHeadersVisible = false;
            this.dgvbk.RowTemplate.Height = 21;
            this.dgvbk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvbk.Size = new System.Drawing.Size(900, 87);
            this.dgvbk.StandardTab = true;
            this.dgvbk.TabIndex = 150;
            this.dgvbk.TabStop = false;
            // 
            // pFusho
            // 
            this.pFusho.Controls.Add(this.verifyBoxF1FirstE);
            this.pFusho.Controls.Add(this.verifyBoxF1Start);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstD);
            this.pFusho.Controls.Add(this.verifyBoxF1);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstM);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstY);
            this.pFusho.Controls.Add(this.label15);
            this.pFusho.Controls.Add(this.verifyBoxDays);
            this.pFusho.Controls.Add(this.label1);
            this.pFusho.Controls.Add(this.label12);
            this.pFusho.Controls.Add(this.verifyBoxF1Finish);
            this.pFusho.Controls.Add(this.label9);
            this.pFusho.Controls.Add(this.label13);
            this.pFusho.Controls.Add(this.label11);
            this.pFusho.Controls.Add(this.label14);
            this.pFusho.Controls.Add(this.label5);
            this.pFusho.Controls.Add(this.label7);
            this.pFusho.Controls.Add(this.label42);
            this.pFusho.Controls.Add(this.label3);
            this.pFusho.Location = new System.Drawing.Point(13, 2);
            this.pFusho.Name = "pFusho";
            this.pFusho.Size = new System.Drawing.Size(939, 65);
            this.pFusho.TabIndex = 65;
            this.pFusho.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(23, 23);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 3;
            this.verifyBoxF1FirstE.TextV = "";
            this.verifyBoxF1FirstE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(252, 23);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 11;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(184, 23);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 8;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(460, 23);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(400, 23);
            this.verifyBoxF1.TabIndex = 43;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(134, 23);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 6;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(84, 23);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 4;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(335, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "日";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(366, 23);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDays.TabIndex = 17;
            this.verifyBoxDays.TextV = "";
            this.verifyBoxDays.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(50, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 36);
            this.label1.TabIndex = 40;
            this.label1.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(163, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "月";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(307, 24);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 14;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(305, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "終了日";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(215, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(112, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "年";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(280, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "初検日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "開始日";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(355, 5);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "実日数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(405, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "負傷名1";
            // 
            // pShinsei
            // 
            this.pShinsei.Controls.Add(this.verifyBoxSG);
            this.pShinsei.Controls.Add(this.verifyBoxSY);
            this.pShinsei.Controls.Add(this.verifyBoxSM);
            this.pShinsei.Controls.Add(this.verifyBoxSD);
            this.pShinsei.Controls.Add(this.label28);
            this.pShinsei.Controls.Add(this.label44);
            this.pShinsei.Controls.Add(this.label34);
            this.pShinsei.Controls.Add(this.label40);
            this.pShinsei.Controls.Add(this.label43);
            this.pShinsei.Location = new System.Drawing.Point(688, 96);
            this.pShinsei.Name = "pShinsei";
            this.pShinsei.Size = new System.Drawing.Size(300, 50);
            this.pShinsei.TabIndex = 100;
            this.pShinsei.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSG
            // 
            this.verifyBoxSG.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSG.Location = new System.Drawing.Point(101, 10);
            this.verifyBoxSG.MaxLength = 1;
            this.verifyBoxSG.Name = "verifyBoxSG";
            this.verifyBoxSG.NewLine = false;
            this.verifyBoxSG.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSG.TabIndex = 50;
            this.verifyBoxSG.TextV = "";
            this.verifyBoxSG.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSY
            // 
            this.verifyBoxSY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSY.Location = new System.Drawing.Point(141, 10);
            this.verifyBoxSY.MaxLength = 2;
            this.verifyBoxSY.Name = "verifyBoxSY";
            this.verifyBoxSY.NewLine = false;
            this.verifyBoxSY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxSY.TabIndex = 53;
            this.verifyBoxSY.TextV = "";
            this.verifyBoxSY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSM
            // 
            this.verifyBoxSM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSM.Location = new System.Drawing.Point(195, 10);
            this.verifyBoxSM.MaxLength = 2;
            this.verifyBoxSM.Name = "verifyBoxSM";
            this.verifyBoxSM.NewLine = false;
            this.verifyBoxSM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxSM.TabIndex = 55;
            this.verifyBoxSM.TextV = "";
            this.verifyBoxSM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxSD
            // 
            this.verifyBoxSD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSD.Location = new System.Drawing.Point(247, 10);
            this.verifyBoxSD.MaxLength = 2;
            this.verifyBoxSD.Name = "verifyBoxSD";
            this.verifyBoxSD.NewLine = false;
            this.verifyBoxSD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxSD.TabIndex = 59;
            this.verifyBoxSD.TextV = "";
            this.verifyBoxSD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label28.Location = new System.Drawing.Point(53, 7);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 26);
            this.label28.TabIndex = 84;
            this.label28.Text = "4:平成\r\n5:令和";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 7);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(43, 26);
            this.label44.TabIndex = 22;
            this.label44.Text = "申請\r\n受付日";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(173, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 26;
            this.label34.Text = "年";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(225, 20);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(19, 13);
            this.label40.TabIndex = 28;
            this.label40.Text = "月";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(278, 20);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(19, 13);
            this.label43.TabIndex = 30;
            this.label43.Text = "日";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(4, 197);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 13);
            this.label27.TabIndex = 88;
            this.label27.Text = "口座番号";
            // 
            // verifyBoxAccountNum
            // 
            this.verifyBoxAccountNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccountNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccountNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxAccountNum.Location = new System.Drawing.Point(7, 213);
            this.verifyBoxAccountNum.Name = "verifyBoxAccountNum";
            this.verifyBoxAccountNum.NewLine = false;
            this.verifyBoxAccountNum.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxAccountNum.TabIndex = 140;
            this.verifyBoxAccountNum.Text = "1234567";
            this.verifyBoxAccountNum.TextV = "";
            this.verifyBoxAccountNum.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxAccountNum.Validated += new System.EventHandler(this.verifyBoxAccountNum_Validated);
            // 
            // pZenkai
            // 
            this.pZenkai.Controls.Add(this.verifyBoxZG);
            this.pZenkai.Controls.Add(this.label23);
            this.pZenkai.Controls.Add(this.label24);
            this.pZenkai.Controls.Add(this.label25);
            this.pZenkai.Controls.Add(this.label26);
            this.pZenkai.Controls.Add(this.verifyBoxZM);
            this.pZenkai.Controls.Add(this.verifyBoxZY);
            this.pZenkai.Location = new System.Drawing.Point(123, 113);
            this.pZenkai.Name = "pZenkai";
            this.pZenkai.Size = new System.Drawing.Size(270, 50);
            this.pZenkai.TabIndex = 85;
            this.pZenkai.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxZG
            // 
            this.verifyBoxZG.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZG.Location = new System.Drawing.Point(107, 8);
            this.verifyBoxZG.MaxLength = 2;
            this.verifyBoxZG.Name = "verifyBoxZG";
            this.verifyBoxZG.NewLine = false;
            this.verifyBoxZG.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZG.TabIndex = 51;
            this.verifyBoxZG.TextV = "";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label23.Location = new System.Drawing.Point(65, 7);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 26);
            this.label23.TabIndex = 83;
            this.label23.Text = "4:平成\r\n5:令和";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Enabled = false;
            this.label24.Location = new System.Drawing.Point(4, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 13);
            this.label24.TabIndex = 39;
            this.label24.Text = "前回支給";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(237, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "月";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(183, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(19, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "年";
            // 
            // verifyBoxZM
            // 
            this.verifyBoxZM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZM.Location = new System.Drawing.Point(206, 8);
            this.verifyBoxZM.MaxLength = 2;
            this.verifyBoxZM.Name = "verifyBoxZM";
            this.verifyBoxZM.NewLine = false;
            this.verifyBoxZM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZM.TabIndex = 53;
            this.verifyBoxZM.TextV = "";
            // 
            // verifyBoxZY
            // 
            this.verifyBoxZY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxZY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxZY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxZY.Location = new System.Drawing.Point(155, 8);
            this.verifyBoxZY.MaxLength = 2;
            this.verifyBoxZY.Name = "verifyBoxZY";
            this.verifyBoxZY.NewLine = false;
            this.verifyBoxZY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxZY.TabIndex = 52;
            this.verifyBoxZY.TextV = "";
            // 
            // cbZenkai
            // 
            this.cbZenkai.BackColor = System.Drawing.SystemColors.Info;
            this.cbZenkai.CheckedV = false;
            this.cbZenkai.Location = new System.Drawing.Point(13, 114);
            this.cbZenkai.Name = "cbZenkai";
            this.cbZenkai.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.cbZenkai.Size = new System.Drawing.Size(95, 27);
            this.cbZenkai.TabIndex = 84;
            this.cbZenkai.Text = "交付料あり";
            this.cbZenkai.UseVisualStyleBackColor = false;
            this.cbZenkai.CheckedChanged += new System.EventHandler(this.cbZenkai_CheckedChanged);
            this.cbZenkai.Enter += new System.EventHandler(this.item_Enter);
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(120, 68);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisitKasan.TabIndex = 74;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.Enter += new System.EventHandler(this.item_Enter);
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(13, 68);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 26);
            this.checkBoxVisit.TabIndex = 73;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(200, 163);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(31, 26);
            this.label33.TabIndex = 49;
            this.label33.Text = "施術\r\n所名";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHosName
            // 
            this.verifyBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHosName.Location = new System.Drawing.Point(235, 165);
            this.verifyBoxHosName.Name = "verifyBoxHosName";
            this.verifyBoxHosName.NewLine = false;
            this.verifyBoxHosName.Size = new System.Drawing.Size(238, 23);
            this.verifyBoxHosName.TabIndex = 115;
            this.verifyBoxHosName.TextV = "";
            this.verifyBoxHosName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(482, 164);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 26);
            this.label31.TabIndex = 47;
            this.label31.Text = "施術\r\n師名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(515, 166);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(160, 23);
            this.verifyBoxDrName.TabIndex = 120;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(4, 163);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(79, 26);
            this.labelNumbering.TabIndex = 45;
            this.labelNumbering.Text = "柔整師\r\n登録記号番号";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(83, 165);
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(110, 23);
            this.verifyBoxDrCode.TabIndex = 110;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxDrCode.Validated += new System.EventHandler(this.verifyBoxDrCode_Validated);
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(564, 121);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxCharge.TabIndex = 95;
            this.verifyBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxCharge.TextV = "";
            this.verifyBoxCharge.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(531, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "請求";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(441, 121);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 90;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(408, 131);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 37;
            this.labelTotal.Text = "合計";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 782);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 782);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 820);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.pBirthday.ResumeLayout(false);
            this.pBirthday.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbk)).EndInit();
            this.pFusho.ResumeLayout(false);
            this.pFusho.PerformLayout();
            this.pShinsei.ResumeLayout(false);
            this.pShinsei.PerformLayout();
            this.pZenkai.ResumeLayout(false);
            this.pZenkai.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelYearInfo1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1Start;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label35;
        private VerifyBox verifyBoxBirthE;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private VerifyBox verifyBoxBirthY;
        private System.Windows.Forms.Label label38;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label33;
        private VerifyBox verifyBoxHosName;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private VerifyBox verifyBoxHihoname;
        private System.Windows.Forms.Label label46;
        private VerifyBox verifyBoxPname;
        private System.Windows.Forms.Label label6;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
        private System.Windows.Forms.Panel pDoui;
        private System.Windows.Forms.Label label8;
        private VerifyBox vbDouiG;
        private System.Windows.Forms.Label lblDoui;
        private VerifyBox vbDouiD;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private System.Windows.Forms.Panel pZenkai;
        private VerifyBox verifyBoxZG;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxZM;
        private VerifyBox verifyBoxZY;
        private VerifyCheckBox cbZenkai;
        private VerifyBox verifyBoxAccountNum;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label43;
        private VerifyBox verifyBoxSD;
        private VerifyBox verifyBoxSM;
        private System.Windows.Forms.Label label40;
        private VerifyBox verifyBoxSY;
        private System.Windows.Forms.Label label34;
        private VerifyBox verifyBoxSG;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel pShinsei;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel pFusho;
        private System.Windows.Forms.Panel pBirthday;
        private System.Windows.Forms.DataGridView dgvbk;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelHnameCopy;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label29;
        private VerifyBox verifyBox1;
        private VerifyBox verifyBox7;
        private VerifyBox verifyBox12;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label4;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private VerifyBox verifyBoxHnumS;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label label10;
        private VerifyBox verifyBoxBui;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button btnAccMaintenance;
    }
}