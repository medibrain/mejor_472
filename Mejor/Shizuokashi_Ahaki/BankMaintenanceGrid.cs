﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Shizuokashi_Ahaki
{

    public partial class BankMaintenanceGrid : Form
    {
        DataTable dtBank = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int dgvCurrentRowIndex = 0;
        

        //20210601113914 furukawa st ////////////////////////
        //入力時直接メンテ用に口座番号を引数に追加
        
        public BankMaintenanceGrid(string _strAccNumber="")
        //      public BankMaintenanceGrid(string _strwhere = "")
        //20210601113914 furukawa ed ////////////////////////
        {
            InitializeComponent();
            
            disp(_strAccNumber);
            
        }



        /// <summary>
        /// 表示
        /// </summary>         
        //20210601114037 furukawa st ////////////////////////
        //入力時直接メンテ用に口座番号を引数に追加
        
        private void disp(string strAccNumber = "")
        //      private void disp(string strAccNumber = "")
        //20210601114037 furukawa ed ////////////////////////

        {

            string strsql = string.Empty;
            strsql += "select * from accountinfo";
            
            //20210601114327 furukawa st ////////////////////////
            //入力時直接メンテ用口座番号があるときは表示条件に追加しフィルタする
            
            if (strAccNumber != string.Empty) strsql += $" where accnumber='{strAccNumber}' ";
            //20210601114327 furukawa ed ////////////////////////

            strsql += " order by ";
            strsql += " bankcd,branchcd,acckindcd,accnumber,flgenable,dateadd desc ";

            
            //やりたくないが、TransactionからConnectionを取る
            DB.Transaction tran = new DB.Transaction(DB.Main);
            cn = tran.conn;

            cmd.CommandText = strsql;
            cmd.Connection = cn;
            
            da.SelectCommand = cmd;            
            da.Fill(dtBank);

            //20210601114408 furukawa st ////////////////////////
            //入力時直接メンテ用口座番号で見つからないときは全レコード表示
            
            if (dtBank.Rows.Count == 0)
            {
                MessageBox.Show("該当口座番号が見つかりません。全データを表示します");
                strsql = "select * from accountinfo";
                strsql += " order by ";
                strsql += " bankcd,branchcd,acckindcd,accnumber,flgenable,dateadd desc ";

                cmd.CommandText = strsql;
                da.SelectCommand = cmd;
                da.Fill(dtBank);                

            }
            //      if (dtBank.Rows.Count == 0) return;
            //20210601114408 furukawa ed ////////////////////////


            dgv.DataSource = dtBank;
            dgv.Columns[nameof(accountinfo.accid)].Width = 50;
            dgv.Columns[nameof(accountinfo.bankcd)].Width = 60;
            dgv.Columns[nameof(accountinfo.bankname)].Width = 170;
            dgv.Columns[nameof(accountinfo.branchcd)].Width = 50;
            dgv.Columns[nameof(accountinfo.branchname)].Width = 170;
            dgv.Columns[nameof(accountinfo.acckindcd)].Width = 50;
            dgv.Columns[nameof(accountinfo.acckind)].Width = 50;
            dgv.Columns[nameof(accountinfo.accnumber)].Width = 80;
            dgv.Columns[nameof(accountinfo.accnamekana)].Width = 250;
            dgv.Columns[nameof(accountinfo.flgenable)].Width = 30;
            dgv.Columns[nameof(accountinfo.dateadd)].Width = 120;
            dgv.Columns[nameof(accountinfo.datedel)].Width = 120;


            dgv.Columns[nameof(accountinfo.accid)].HeaderText = "ID";
            dgv.Columns[nameof(accountinfo.bankcd)].HeaderText = "銀行CD";
            dgv.Columns[nameof(accountinfo.bankname)].HeaderText = "銀行名";
            dgv.Columns[nameof(accountinfo.branchcd)].HeaderText = "支店CD";
            dgv.Columns[nameof(accountinfo.branchname)].HeaderText = "支店名";
            dgv.Columns[nameof(accountinfo.acckindcd)].HeaderText = "口座種別CD";
            dgv.Columns[nameof(accountinfo.acckind)].HeaderText ="口座種別";
            dgv.Columns[nameof(accountinfo.accnumber)].HeaderText = "口座番号";
            dgv.Columns[nameof(accountinfo.accnamekana)].HeaderText = "口座名義カナ";
            dgv.Columns[nameof(accountinfo.flgenable)].HeaderText = "有効";
            dgv.Columns[nameof(accountinfo.dateadd)].HeaderText = "追加日";
            dgv.Columns[nameof(accountinfo.datedel)].HeaderText = "削除日";


            dgv.Columns[nameof(accountinfo.accid)].ReadOnly = true;


            cmbDup.Items.Clear();
            foreach (DataGridViewColumn c in dgv.Columns)
            {
                cmbDup.Items.Add(c.Name);
            }

            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
            tran.Commit();

        }

        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            upd();

        }
        

        private void upd()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            string strAccNumber = string.Empty;

            foreach (DataRow dr in dtBank.Rows)
            {
                
                if (dr.RowState == DataRowState.Added)
                {
                    col =1;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into accountinfo (");
                    
                    sb.Append("bankcd,");
                    sb.Append("bankname,");
                    sb.Append("branchcd,");
                    sb.Append("branchname,");
                    sb.Append("acckindcd,");
                    sb.Append("acckind,");
                    sb.Append("accnumber,");
                    sb.Append("accnamekana,");
                    sb.Append("flgenable,");
                    sb.Append("dateadd,");
                    sb.Append("datedel");

                    sb.AppendLine(")values (");



                    //20211102154216 furukawa st ////////////////////////
                    //銀行名、支店名は全角
                    
                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}'  ",                     
                     dr[col++].ToString(),
                     Microsoft.VisualBasic.Strings.StrConv(dr[col++].ToString(), Microsoft.VisualBasic.VbStrConv.Wide),
                     dr[col++].ToString(),
                     Microsoft.VisualBasic.Strings.StrConv(dr[col++].ToString(), Microsoft.VisualBasic.VbStrConv.Wide),
                     dr[col++].ToString(),                     
                     dr[col++].ToString(), dr[col++].ToString(),
                     limitbyte(dr[col++].ToString(), 30), //拗音置換・30バイト切り
                     dr[col++].ToString(),
                     DateTime.Now.ToString("yyyy-MM-dd"),
                     DateTime.MinValue);



                    //      20210720173333 furukawa st ////////////////////////
                    //      口座名義カナをバイト制限

                    //      sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}'  ",
                    //        dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //        dr[col++].ToString(), dr[col++].ToString(),
                    //        limitbyte(dr[col++].ToString(),30), //拗音置換・30バイト切り
                    //        dr[col++].ToString(),
                    //        DateTime.Now.ToString("yyyy-MM-dd"),
                    //        DateTime.MinValue);


                    //                    //20210618183616 furukawa st ////////////////////////
                    //                    //拗音置換


                    //                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}'  ",
                    //                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //                         dr[col++].ToString(), dr[col++].ToString(),
                    //                         CommonTool.ReplaceContract(dr[col++].ToString()), //拗音置換
                    //                         dr[col++].ToString(),
                    //                         DateTime.Now.ToString("yyyy-MM-dd"),
                    //                         DateTime.MinValue);


                    //                    //          20210601114620 furukawa st ////////////////////////
                    //                    //          追加したときは本日を入れる


                    //                    //          sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}'  ",
                    //                    //               dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //                    //               dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), DateTime.Now.ToString("yyyy-MM-dd"),
                    //                    //               DateTime.MinValue);

                    //                    //                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}'  ",
                    //                    //                        dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //                    //                        dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //                    //                        dr[col++].ToString());
                    //                    //          20210601114620 furukawa ed ////////////////////////


                    //                    //20210618183616 furukawa ed ////////////////////////

                    //      20210720173333 furukawa ed ////////////////////////


                    //20211102154216 furukawa ed ////////////////////////

                    sb.AppendLine(");");

                    lstsql.Add(sb.ToString());

                    //20210721092446 furukawa st ////////////////////////
                    //更新した場合のみ口座番号を指定し、更新後表示でフィルタする
                    
                    //口座番号取得
                    strAccNumber = dr[7].ToString();
                    //20210721092446 furukawa ed ////////////////////////

                }
                else if (dr.RowState == DataRowState.Modified)
                {
                    col = 1;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update accountinfo set ");

                    sb.AppendFormat("	bankcd         ='{0}',", dr["bankcd"].ToString());
                    
                    //20211102154301 furukawa st ////////////////////////
                    //銀行名は全角
                    
                    sb.AppendFormat("	bankname       ='{0}',",Microsoft.VisualBasic.Strings.StrConv(dr["bankname"].ToString(),Microsoft.VisualBasic.VbStrConv.Wide));
                    //  sb.AppendFormat("	bankname       ='{0}',", dr["bankname"].ToString());
                    //20211102154301 furukawa ed ////////////////////////


                    sb.AppendFormat("	branchcd       ='{0}',", dr["branchcd"].ToString());

                    //20211102154321 furukawa st ////////////////////////
                    //支店名は全角
                    
                    sb.AppendFormat("	branchname     ='{0}',", Microsoft.VisualBasic.Strings.StrConv(dr["branchname"].ToString(), Microsoft.VisualBasic.VbStrConv.Wide));
                    //  sb.AppendFormat("	branchname     ='{0}',", dr["branchname"].ToString());
                    //20211102154321 furukawa ed ////////////////////////


                    sb.AppendFormat("	acckindcd      ='{0}',", dr["acckindcd"].ToString());
                    sb.AppendFormat("	acckind        ='{0}',", dr["acckind"].ToString());
                    sb.AppendFormat("	accnumber      ='{0}',", dr["accnumber"].ToString());


                    //20210720173434 furukawa st ////////////////////////
                    //口座名義カナをバイト制限
                    

                    sb.AppendFormat("	accnamekana    ='{0}',", limitbyte(dr["accnamekana"].ToString(),30));

                    //      //20210618183537 furukawa st ////////////////////////
                    //      //拗音置換
                    //      sb.AppendFormat("	accnamekana    ='{0}',", CommonTool.ReplaceContract(dr["accnamekana"].ToString()));
                    //      //sb.AppendFormat("	accnamekana    ='{0}',", dr["accnamekana"].ToString());
                    //      //20210618183537 furukawa ed ////////////////////////

                    //20210720173434 furukawa ed ////////////////////////
                    sb.AppendFormat("	flgenable      ='{0}',", dr["flgenable"].ToString());
                    sb.AppendFormat("	dateadd        ='{0}',", dr["dateadd"].ToString());

                    //削除の場合は削除日を今日にしとく
                    if (dr["flgenable"].ToString() != "True")
                    {
                        sb.AppendFormat("	datedel     ='{0}' ", DateTime.Now.ToString("yyyy-MM-dd"));
                    }
                    else
                    {
                        sb.Append("	datedel     =null ");
                    }
                    
                    sb.AppendFormat("	where accid		={0};", dr[0].ToString());

                    lstsql.Add(sb.ToString());

                    //20210721092411 furukawa st ////////////////////////
                    //更新した場合のみ口座番号を指定し、更新後表示でフィルタする
                    
                    //口座番号取得
                    strAccNumber = dr[7].ToString();
                    //20210721092411 furukawa ed ////////////////////////
                }
                else
                {
                    //20210721092312 furukawa st ////////////////////////
                    //更新していない場合は口座番号入れず、全体をリスト表示
                    
                    strAccNumber = string.Empty;
                    //20210721092312 furukawa ed ////////////////////////
                }

            }

            if (lstsql.Count == 0) return;

            //cmd.CommandText = sb.ToString();
            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {

                foreach (string s in lstsql)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新しました");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtBank.Clear();

                cn.Close();
                disp(strAccNumber);
            }


        }


        //20210720173457 furukawa st ////////////////////////
        //バイト制限して切り落とす
        
        /// <summary>
        /// バイト制限して切り落とす
        /// </summary>
        /// <param name="strOrig"></param>
        /// <returns></returns>
        private static string limitbyte(string strOrig,int limitLength)
        {
            string strRet = CommonTool.ReplaceContract(strOrig);
            strRet = CommonTool.SubStringByBytes(strRet, limitLength, System.Text.Encoding.GetEncoding("shift-jis"));
            return strRet;
        }
        //20210720173457 furukawa ed ////////////////////////


        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine("select max(accid) id from accountinfo");
            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

        /// <summary>
        /// 複製ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btncopy_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index < 0) return;

            DataGridViewRow dgvr = new DataGridViewRow();

            dgvr = dgv.Rows[dgv.CurrentRow.Index];
            DataRow dr;

            dr = dtBank.NewRow();

            int maxid = 0;

            for(int r=1;r<dgvr.Cells.Count;r++)
            {
                //データ型で分けてみる
                switch(dtBank.Columns[r].DataType.Name)
                {
                    case "DateTime":
                        //20210601114733 furukawa st ////////////////////////
                        //datetimeがない場合は最小値
                        
                        if (dgvr.Cells[r].Value.ToString() == string.Empty)
                        {
                            dr[r] = DateTime.MinValue.ToString();
                            break;
                        }
                        else
                        {
                            dr[r] = dgvr.Cells[r].Value.ToString();
                            break;
                        }
                        //      dr[r] = dgvr.Cells[r].Value.ToString();
                        //      break;
                        //20210601114733 furukawa ed ////////////////////////


                    case "Bool":
                    case "Boolean":
                        dr[r ] = "true";
                        break;
                    default:
                        dr[r ] = dgvr.Cells[r].Value.ToString();
                        break;
                }

              

                if (r == 1)
                {
                    //最大のID取得
                    dr[r-1] = getMaxID();
                    maxid = int.Parse(dr[r - 1].ToString());
                }
                
            }

            dtBank.Rows.Add(dr);
            upd();

            //20210601114905 furukawa st ////////////////////////
            //最新ID行の色変えて分かるように
            
            foreach (DataGridViewRow dgvr2 in dgv.Rows)
            {
                if (int.Parse(dgvr2.Cells[0].Value.ToString()) == maxid)
                {
                    dgvr2.DefaultCellStyle.BackColor = Color.Yellow; break;
                }
            }
            //20210601114905 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 削除ボタン列
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //if (e.ColumnIndex != 0) return;

                //セルを触ったら現在行を控える
                dgvCurrentRowIndex = dgv.CurrentRow.Index;

                string id = dgv.Rows[dgv.CurrentRow.Index].Cells[0].Value.ToString();


                if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
                    Application.ProductName, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {


                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" delete from accountinfo where accid=");
                    sb.AppendFormat("'{0}'", id);


                    cmd.CommandText = sb.ToString();
                    try
                    {
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("削除しました");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        dtBank.Clear();

                        cn.Close();
                        disp();
                    }
                }
            }
           
        }

        private void BankMaintenanceGrid_Shown(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0) dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }

        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {
            string fld = "accnumber";//デフォルトだけ決めとく
            fld = cmbDup.Text;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach(DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach(DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString()){
                        
                        cnt++;
                        if (cnt > 1)
                        {
                            
                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }
                  
                }

            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 検索ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            if (textBoxFind.Text.Trim() == string.Empty) return;

            string strFind = textBoxFind.Text.Trim();
            
            dgv.ClearSelection();


            for(int r =0;r<dgv.Rows.Count;r++)
            {
                for(int c = 0; c < dgv.Rows[r].Cells.Count; c++)
                {
                    if (strFind == dgv.Rows[r].Cells[c].Value.ToString())
                    {
                        dgv.Rows[r].Selected = true;
                        dgv.Rows[r].Cells[c].Selected = true;
                        
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// 修正前セル値
        /// </summary>
        string beforeEditString;

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //静岡市あはき　銀行口座名カナは半角30バイト

            if (e.ColumnIndex != 8) return;

            //変更した後のセル値
            string strCell = dgv.CurrentCell.Value.ToString();

            //拗音置換、半角にする、30バイト切り
            string strChanged = CommonTool.ReplaceContract(strCell);
            strChanged= Microsoft.VisualBasic.Strings.StrConv(strChanged, Microsoft.VisualBasic.VbStrConv.Narrow);
            strChanged = CommonTool.SubStringByBytes(strChanged, 30, System.Text.Encoding.GetEncoding("shift-jis"));

            //使える文字かどうか 全角カナ、（）ー－半角カナ、濁点半濁点()中点のばし棒ハイフン
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[ァ-ンｧ-ﾝ（）・ー－()　 ﾞﾟ･ｰ-]");
            bool flgNG = false;
            foreach(char chr in strChanged)
            {
                if (!reg.IsMatch(chr.ToString()))
                {
                    flgNG = true;
                    break;
                }
            }

            if(flgNG)
            {
                MessageBox.Show("入力できる文字は[カタカナ（）ﾞﾟ･ー-]のみ30文字です。元に戻します");
                dgv.CurrentCell.Value = beforeEditString;
                return;
            }

            //変換確認
            if (strChanged != strCell)
            {
                if (MessageBox.Show($"下のように変換しますが宜しいですか？\r\n{strCell}\r\n{strChanged}", "", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    MessageBox.Show("元に戻します");
                    dgv.CurrentCell.Value = beforeEditString;
                }
                else
                {
                    dgv.CurrentCell.Value = strChanged;
                    beforeEditString = strChanged;
                }
            }
            else
            {
                beforeEditString = strChanged;
            }
            

        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                //現在のセルの値を取得
                beforeEditString = dgv.CurrentCell.Value.ToString();
            }
        }
    }
}
