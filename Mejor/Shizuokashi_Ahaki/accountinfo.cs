﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Shizuokashi_Ahaki
{
    class accountinfo
    {
        public string bankcd { get; set; } = string.Empty;              //	金融機関コード		
        public string bankname { get; set; } = string.Empty;            //	金融機関名		
        public string branchcd { get; set; } = string.Empty;            //	支店コード		
        public string branchname { get; set; } = string.Empty;          //	支店名		
        public string acckindcd { get; set; } = string.Empty;           //	科目コード		
        public string acckind { get; set; } = string.Empty;             //	科目名		
        public string accnumber { get; set; } = string.Empty;           //	口座番号		
        public string accnamekana { get; set; } = string.Empty;         //	口座名義（ﾌﾘｶﾞﾅ）		
        public bool flgenable { get; set; } = true;                     //	有効フラグ		
        public DateTime dateadd { get; set; } = DateTime.MinValue;      //	追加日時			
        public DateTime datedel { get; set; } = DateTime.MinValue;		//	削除日時		


        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int accid { get; set; }                              //	プライマリキー メホール管理用		


        private static List<accountinfo> lstAcc = new List<accountinfo>();

        public static bool Import(string strFileName)
        {

            WaitForm wf =new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("口座情報取得");      

            List<string[]> lst=CommonTool.CsvImportMultiCode(strFileName);

            DB.Transaction tran = DB.Main.CreateTransaction();


            try
            {                
                wf.LogPrint("口座情報インポート");
                wf.SetMax(lst.Count);                

                foreach (string[] s in lst)
                {
                    if (s.Length < 7) continue;
                    if (!int.TryParse(s[0].ToString(), out int tmp)) continue;

                    accountinfo acinfo = new accountinfo();
                    int colcnt = 0;

                    acinfo.bankcd = s[colcnt++].ToString();
                    acinfo.bankname = s[colcnt++].ToString();
                    acinfo.branchcd = s[colcnt++].ToString();
                    acinfo.branchname = s[colcnt++].ToString();
                    acinfo.acckindcd = s[colcnt++].ToString();

                    acinfo.acckind = acinfo.acckindcd == "1" ? "普通" : "当座";

                    acinfo.accnumber = s[colcnt++].ToString();

                    //20210618183445 furukawa st ////////////////////////
                    //拗音置換
                    
                    acinfo.accnamekana = CommonTool.ReplaceContract(s[colcnt++].ToString());
                    //acinfo.accnamekana = s[colcnt++].ToString();
                    //20210618183445 furukawa ed ////////////////////////


                    acinfo.flgenable = true;
                    acinfo.dateadd = DateTime.Now;
                    acinfo.datedel = DateTime.MinValue;

                   
                    if (!DB.Main.Insert<accountinfo>(acinfo, tran))
                    {
                        wf.LogPrint($"登録エラー 口座番号：{acinfo.accnumber}");
                        return false;
                    }
                    else
                    {
                        wf.LogPrint($"口座番号：{acinfo.accnumber}");
                    }
                  

                    wf.InvokeValue++;
                }


                tran.Commit();
                System.Windows.Forms.MessageBox.Show("終了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }



        /// <summary>
        /// 口座情報全取得
        /// </summary>
        /// <returns> Dictionary<string,accountinfo></returns>
        public static List<accountinfo> SelectAll()
        {
            
            DB.Command cmd = DB.Main.CreateCmd("select * from accountinfo " +
                "where flgenable=true " +
                "order by bankcd,branchcd,acckindcd,accnumber");

            var list=cmd.TryExecuteReaderList();
            lstAcc.Clear();

            foreach (var item in list)
            {
                string strAccNumber = item[7].ToString();
                accountinfo ai = new accountinfo();

                if (!int.TryParse(item[0].ToString(), out int tmp)) continue;
                ai.accid = tmp;
                
                ai.bankcd = item[1].ToString();
                ai.bankname = item[2].ToString();
                ai.branchcd = item[3].ToString();
                ai.branchname = item[4].ToString();
                ai.acckindcd = item[5].ToString();
                ai.acckind = item[6].ToString();
                ai.accnumber = item[7].ToString();
                ai.accnamekana = item[8].ToString();
                ai.flgenable = bool.Parse(item[9].ToString());


                if (!DateTime.TryParse(item[10].ToString(), out DateTime tmpdtadd)) ai.dateadd = DateTime.MinValue;
                else ai.dateadd = tmpdtadd;


                if (!DateTime.TryParse(item[11].ToString(), out DateTime tmpdtdel)) ai.datedel = DateTime.MinValue;
                else ai.datedel = tmpdtdel;



                lstAcc.Add(ai);
            }


            return lstAcc;
        }
    }
}
