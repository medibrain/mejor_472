﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Shizuokashi_Ahaki
{
    //20200623162804 furukawa st ////////////////////////
    //静岡市あはき用口座情報メンテナンス画面
    

    public partial class BankMaintenance : Form
    {
               

        public BankMaintenance()
        {
            InitializeComponent();
            
            btn1.Enabled = true;
            btn1.Text = "口座情報インポート";
            btn2.Enabled = true;
            btn2.Text = "口座情報メンテナンス";
        
        }
        

        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "口座情報CSVファイル";
            ofd.ShowDialog();


            if (ofd.FileName == string.Empty) return;

            accountinfo.Import(ofd.FileName);
            
        }

    
        private void btn2_Click(object sender, EventArgs e)
        {
            //口座メンテナンス画面
            BankMaintenanceGrid frm = new BankMaintenanceGrid();
            frm.Show();
            
        }

    }
}
