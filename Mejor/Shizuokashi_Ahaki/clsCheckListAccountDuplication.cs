﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Shizuokashi_Ahaki
{
    //今月、先月、先々月のappとaccidを取得
    //全てエクセルで出す
    //変わってるやつは要注意色にする

    public class clsCheckListAccountDuplication
    {
        static List<App> lstCurr = new List<App>();
        static List<App> lst1Prev = new List<App>();
        static List<App> lst2Prev = new List<App>();

        static List<string> lst = new List<string>();
        

        public static bool CheckMain(int cym,string strNohinDir,WaitForm wf)
        {
            //WaitForm wf = new WaitForm();
            //wf.ShowDialogOtherTask();
            try
            {
                //20211102114608 furukawa st ////////////////////////
                //クリア忘れで2回目以降に追加されてた
                lst.Clear();
                //20211102114608 furukawa ed ////////////////////////


                if (!GetApps(cym, wf)) return false;

                if (!CreateExcel(wf, strNohinDir)) return false ;

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
                return false;
            }

            
        }


        /// <summary>
        /// データ取得
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool GetApps(int cym,WaitForm wf)
        {
            int cym1 = DateTimeEx.Int6YmAddMonth(cym, -1);
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" select ");
            sb.AppendLine(" a.aid ");
            sb.AppendLine(" ,a.cym メホール年月 ");
            sb.AppendLine(" ,a.atotal 合計 ");
            sb.AppendLine(" ,a.aapptype 申請書タイプ ");
            sb.AppendLine(" ,a.hnum 被保険者証番号 ");
            sb.AppendLine(" ,a.hname 被保険者名 ");
            sb.AppendLine(" ,a.pname 受療者名 ");
            sb.AppendLine(" ,ac.accid 口座ID ");
            sb.AppendLine(" ,ac.bankcd 銀行コード ");
            sb.AppendLine(" ,ac.bankname 銀行名 ");
            sb.AppendLine(" ,ac.branchcd 支店コード ");
            sb.AppendLine(" ,ac.branchname 支店名 ");
            sb.AppendLine(" ,ac.acckindcd 口座種別コード ");
            sb.AppendLine(" ,ac.acckind 口座種別 ");
            sb.AppendLine(" ,ac.accnumber 口座番号 ");
            sb.AppendLine(" ,ac.accnamekana 口座名カナ ");
            sb.AppendLine(" from application a  ");
            sb.AppendLine(" left join accountinfo ac on  ");
            sb.AppendLine(" replace(regexp_replace(a.taggeddatas,'.+GeneralString5:',''),'\"','')  = cast(ac.accid as varchar)  ");

            sb.AppendLine($" where a.cym in ({cym1},{cym}) ");

            sb.AppendLine(" and a.ym>0 ");

            sb.AppendLine(" group by ");
            sb.AppendLine(" a.aid, ");
            sb.AppendLine(" a.atotal, ");
            sb.AppendLine(" a.aapptype  ");
            sb.AppendLine(" ,a.hnum  ");
            sb.AppendLine(" ,a.hname  ");
            sb.AppendLine(" ,a.pname  ");
            sb.AppendLine(" ,ac.accid  ");
            sb.AppendLine(" ,ac.bankcd  ");
            sb.AppendLine(" ,ac.bankname  ");
            sb.AppendLine(" ,ac.branchcd  ");
            sb.AppendLine(" ,ac.branchname  ");
            sb.AppendLine(" ,ac.acckindcd ");
            sb.AppendLine(" ,ac.acckind  ");
            sb.AppendLine(" ,ac.accnumber  ");
            sb.AppendLine(" ,ac.accnamekana  ");

            sb.AppendLine(" order by a.hnum ");
            sb.AppendLine(" ,a.pname ");
            sb.AppendLine(" ,a.aapptype ");
            sb.AppendLine(" ,a.cym ");

                        
            DB.Command cmd = new DB.Command(DB.Main,sb.ToString());
            var l = cmd.TryExecuteReaderList();
            try
            {
                foreach (var item in l)
                {
                    string s = string.Empty;
                    for (int c = 0; c < item.Length; c++)
                    {
                        s += $"{item[c].ToString()}|";
                    }

                    lst.Add(s.Substring(0,s.Length-1));
                }

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
                return false;
            }
        }




        private static bool CreateExcel(WaitForm wf,string strNohinDir)
        {
            string strFileName = $"{strNohinDir}\\口座チェックリスト{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}.xlsx";

            System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet();

            try
            {
                wf.InvokeValue =0;
                wf.SetMax(lst.Count);
                wf.LogPrint($"重複チェックリスト作成中");

                NPOI.SS.UserModel.IRow row = ws.CreateRow(0);

                NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 16);

                for (int col = 0; col < rng.LastColumn; col++)
                {
                    NPOI.SS.UserModel.ICell cell = row.CreateCell(col);
                    if (col == 0) cell.SetCellValue("aid");
                    if (col == 1) cell.SetCellValue("メホール年月 ");
                    if (col == 2) cell.SetCellValue("合計 ");
                    if (col == 3) cell.SetCellValue("申請書タイプ ");
                    if (col == 4) cell.SetCellValue("被保険者証番号 ");
                    if (col == 5) cell.SetCellValue("被保険者名 ");
                    if (col == 6) cell.SetCellValue("受療者名 ");
                    if (col == 7) cell.SetCellValue("口座");
                    if (col == 8) cell.SetCellValue("銀行コード ");
                    if (col == 9) cell.SetCellValue("銀行名 ");
                    if (col == 10) cell.SetCellValue("支店コード ");
                    if (col == 11) cell.SetCellValue("支店名 ");
                    if (col == 12) cell.SetCellValue("口座種別コード ");
                    if (col == 13) cell.SetCellValue("口座種別 ");
                    if (col == 14) cell.SetCellValue("口座番号 ");
                    if (col == 15) cell.SetCellValue("口座名カナ ");

                }
                
                

                for (int r =1;r<lst.Count;r++) 
                {
                    
                    row = ws.CreateRow(r) ;

                    string[] s = lst[r].ToString().Split('|');

                    for(int c = 0; c < s.Length; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(c);
                        //20211102103510 furukawa st ////////////////////////
                        //銀行名、支店名は全角
                        
                        if (c == 9 || c == 11)
                        {
                            cell.SetCellValue(Microsoft.VisualBasic.Strings.StrConv(s[c].ToString(),Microsoft.VisualBasic.VbStrConv.Wide));
                        }
                        else
                        {
                            cell.SetCellValue(s[c].ToString());
                        }
                        //cell.SetCellValue(s[c].ToString());
                        //20211102103510 furukawa ed ////////////////////////
                    }
                    wf.InvokeValue++;
                    
                    
                }


                NPOI.SS.UserModel.ICellStyle cs = wb.CreateCellStyle();
                cs.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.Orange.Index;
                cs.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;

                NPOI.SS.UserModel.ICellStyle cs2 = wb.CreateCellStyle();
                cs2.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.Lavender.Index;
                cs2.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;

                NPOI.SS.UserModel.ICellStyle cstmp = null;

                string prev = string.Empty;
                for (int r=lst.Count-1;r>0; r--)
                {

                    row = ws.GetRow(r);
                    NPOI.SS.UserModel.ICell undercell = row.GetCell(6);
                    string strunder = row.GetCell(7).StringCellValue;
                    
                                           
                    row = ws.GetRow(r-1);
                    NPOI.SS.UserModel.ICell cell = row.GetCell(6);
                    string str = row.GetCell(7).StringCellValue;

                    //人が同じでaccidが違う場合警告色
                    if (cell.StringCellValue == undercell.StringCellValue)
                    {
                        if (strunder != str)
                        {
                            row = ws.GetRow(r);
                            foreach (NPOI.SS.UserModel.ICell c in row) c.CellStyle = cstmp;

                            row = ws.GetRow(r - 1);
                            foreach (NPOI.SS.UserModel.ICell c in row) c.CellStyle = cstmp;
                            

                            //undercell.CellStyle = cstmp;
                            //cell.CellStyle = cstmp;
                        }
                        else
                        {
                            undercell.CellStyle = null;
                            cell.CellStyle = null;
                        }
                    }
                    else
                    {
                        if (cstmp == cs || cstmp == null) cstmp = cs2; else cstmp = cs;
                        cell.CellStyle = null;
                    }
                   

                }

                //列幅自動
                for (int c = 0; c < rng.LastColumn; c++) ws.AutoSizeColumn(c);


                wb.Write(fs);

                wf.LogPrint($"重複チェックリスト完成");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
                return false;
            }
            finally
            {
                
                wb.Close();
                fs.Close();
            }
        }
    }
}
