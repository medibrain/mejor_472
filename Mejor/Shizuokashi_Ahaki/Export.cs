﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Shizuokashi_Ahaki
{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class Export
    {

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int exportid { get; set; } = 0;                     //プライマリキー メホール管理用

        public int cym { get; set; } = 0;                           //メホール上の処理年月 メホール管理用
        public int ym { get; set; } = 0;                            //診療年月 メホール管理用
        public int aid { get; set; } = 0;                           //aidメホール管理用
        public string numbering { get; set; } = string.Empty;        //ナンバリング 7 1桁目は申請場所の区番号。右6桁は連番。例）1000001・1000002・2000001
        public string shoriym { get; set; } = string.Empty;          //処理年月 5 「5」→令和
        public string shinsei_basho { get; set; } = string.Empty;    //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
        public string apptype { get; set; } = string.Empty;          //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
        public string ippantaishoku { get; set; } = string.Empty;    //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
        public string hnum { get; set; } = string.Empty;             //被保険者証番号 8 8桁の数字とする（前「0」）。
        public string atenanum { get; set; } = string.Empty;         //宛名番号 0 ブランク
        public string mainname { get; set; } = string.Empty;         //世帯主氏名 25 全角
        public string pname { get; set; } = string.Empty;            //療養を受けた者の氏名 25 全角
        public string psex { get; set; } = string.Empty;             //性別 1 「1」→男、「2」→女
        public string pbirth { get; set; } = string.Empty;           //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
        public string startdate1 { get; set; } = string.Empty;       //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和
        public string shinseiymd { get; set; } = string.Empty;       //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
        public string clinicname { get; set; } = string.Empty;       //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
        public string drname { get; set; } = string.Empty;           //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
        public string ratio { get; set; } = string.Empty;            //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
        public string total { get; set; } = string.Empty;            //合計金額（費用額） 9 ゼロ埋めなし。
        public string charge { get; set; } = string.Empty;           //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
        public string partial { get; set; } = string.Empty;          //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
        public string counteddays { get; set; } = string.Empty;      //実日数 2 ゼロ埋めなし。
        public string bcode { get; set; } = string.Empty;            //金融機関コード 4 4桁の数字とする。
        public string bname { get; set; } = string.Empty;            //金融機関名 15 全角
        public string bbcode { get; set; } = string.Empty;           //支店コード 3 3桁の数字とする。
        public string bbname { get; set; } = string.Empty;           //支店名 15 全角
        public string bacctype { get; set; } = string.Empty;         //科目 1 「1」→普通、「2」→当座
        public string baccnumber { get; set; } = string.Empty;       //口座番号 7 ゼロ埋め。
        public string baccname { get; set; } = string.Empty;         //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ

        //20210608150326 furukawa st ////////////////////////
        //申請書等の基本項目データファイル用項目追加
        
        public string result_bunsho { get; set; } = string.Empty;       //申請書等の基本項目データ用　文書照会結果
        public string result_tenken { get; set; } = string.Empty;       //申請書等の基本項目データ用　点検結果
        public string henrei_reason { get; set; } = string.Empty;       //申請書等の基本項目データ用　返戻理由
        public string biko { get; set; } = string.Empty;                //申請書等の基本項目データ用　備考
        //20210608150326 furukawa ed ////////////////////////


        #endregion


        /// <summary>
        /// 申請書等の基本項目データファイルチェックフラグ
        /// </summary>
        static bool flgCheckNG = false;

        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool ExportData(int cym)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(cym);
           
            //保存場所
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
               
                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable(lstApp, cym, wf)) return false;


                //ここでナンバリングaid順
                // if (!CreateRenban(cym,wf)) return false;


                //20210608073244 furukawa st ////////////////////////
                //納品データを取得しリストに入れる                
                List<Export> lstEx = GetExportData(cym, wf);
                if (lstEx.Count == 0) return false;
                //20210608073244 furukawa ed ////////////////////////



                //データ出力フォルダ作成
                //20210608150711 furukawa st ////////////////////////
                //出力フォルダ名を秒出してわかりやすく
                
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}出力";
                //      string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                //20210608150711 furukawa ed ////////////////////////


                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);

                //20210608073324 furukawa st ////////////////////////
                //申請書等の基本項目データ作成
                if (!DataExport_excel(lstEx, strNohinDir, cym, wf)) return false;
                //20210608073324 furukawa ed ////////////////////////

                //口座チェックリスト（前月と比較）出力
                if (!clsCheckListAccountDuplication.CheckMain(cym,strNohinDir,wf)) return false;

                //支払CSV出力




                //20211014092936 furukawa st ////////////////////////
                //申請書等の基本項目データにエラーがあっても支払いcsvを出せるように

                //2021/10/13手作業との比較時に臨時でエラー状態でも出せるようにした
                if (flgCheckNG) {
                    if (System.Windows.Forms.MessageBox.Show("申請書等の基本項目データにエラーがありますが、支払データを出力しますか？", "",
                        System.Windows.Forms.MessageBoxButtons.YesNo,
                        System.Windows.Forms.MessageBoxIcon.Question,
                        System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No) return false;
                    else
                    {
                        if (!DataExport(lstEx, cym, strNohinDir, wf))
                        {
                            System.Windows.Forms.MessageBox.Show("支払データ出力失敗", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            return false;
                        }
                    }

                }
                //if (!flgCheckNG)
                //{
                //20211014092936 furukawa ed ////////////////////////
                else
                {

                    if (!DataExport(lstEx, cym, strNohinDir, wf))
                    {
                        System.Windows.Forms.MessageBox.Show("支払データ出力失敗", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        return false;
                    }
                }
                

                //申請書画像コピー
                if (!flgCheckNG) if (!CopyImage( cym, strNohinDir, wf)) return false;

                
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 保険者ごとに、aid順でソートして保険者ごとに連番
        /// </summary>   
        /// <param name="cym">cym</param>
        /// <returns>失敗=false</returns>
        private static bool CreateRenban(int cym,WaitForm wf)
        {
            wf.LogPrint("連番付与");

            string strsql = "";
            //保険者ごとに、aid順でソートして保険者ごとに連番？要試験
            //https://buralog.jp/postgresql-row-number-function-group-sequence-number/
            strsql = "update export set numbering=insnum || LPAD(cast(row_number() over (partition by insnum order by aid asc) as varchar),6,'0') " +
                $"where cym={cym} and Length(numbering)<2 " +
                $"order by aid;";

            DB.Command cmd = new DB.Command(DB.Main,strsql);

            return cmd.TryExecuteNonQuery();


        }


        
        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <returns></returns>
        private static bool InsertExportTable(List<App> lstApp,int cym ,WaitForm wf)
        {
            wf.SetMax(lstApp.Count);


            DB.Transaction tran;
            tran=DB.Main.CreateTransaction();
                        
            try
            {
                //今月分を削除
                DB.Command cmd = new DB.Command($"delete from export where cym='{cym}'", tran);
                cmd.TryExecuteNonQuery();
                
                int cnt = 0;

                foreach (App item in lstApp)
                {
                    //中断処理
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    wf.LogPrint($"出力テーブル登録 aid:{item.Aid}");

                    //20210608145836 furukawa st ////////////////////////
                    //一旦支払保留もテーブルに出す

                    //      2020/08/04　伸作さんより要望　支払保留は出さない=>一旦出してCSVの時は出さない
                    //      if (item.StatusFlagCheck(StatusFlag.支払保留)) continue;

                    //20210608145836 furukawa ed ////////////////////////


                    Export exp = new Export();

                    cnt++;
                    
                    exp.cym = item.CYM;                                  //メホール上の処理年月 メホール管理用
                    exp.ym = item.YM;                                    //診療年月 メホール管理用
                    exp.aid = item.Aid;                                  //aidメホール管理用
                    
                    //データ取得時にover partitionで区ごとに連番を振るため、dbに入った時点では区番号だけ入ってる
                    exp.numbering = item.InsNum.ToString();              //ナンバリング 7 1桁目は申請場所の区番号。右6桁は連番。例）1000001・1000002・2000001

                    exp.shoriym = (DateTimeEx.GetEraNumberYearFromYYYYMM(item.CYM)*100+item.CYM%100).ToString();   //処理年月 5 「5」→令和
                    exp.shinsei_basho = item.InsNum;                     //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区


                    if (item.AppType == APP_TYPE.あんま) exp.apptype = "17";    //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
                    if (item.AppType == APP_TYPE.鍼灸) exp.apptype = "19";

                    exp.ippantaishoku = item.TaggedDatas.GeneralString4;        //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
                    exp.hnum = item.HihoNum.ToString().PadLeft(8,'0');          //被保険者証番号 8 8桁の数字とする（前「0」）。
                    exp.atenanum = string.Empty;                                //宛名番号 0 ブランク
                    exp.mainname = item.HihoName;                               //世帯主氏名 25 全角
                    exp.pname = item.PersonName;                                //療養を受けた者の氏名 25 全角
                    exp.psex = item.Sex.ToString();                             //性別 1 「1」→男、「2」→女
                    exp.pbirth = DateTimeEx.GetIntJpDateWithEraNumber(item.Birthday).ToString();                //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
                    exp.startdate1 = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();     //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和

                    if(DateTime.TryParse(item.TaggedDatas.GeneralString1, out DateTime dtshinsei))
                        exp.shinseiymd = DateTimeEx.GetIntJpDateWithEraNumber(dtshinsei).ToString();           //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和


                    //20200804142911 furukawa st ////////////////////////
                    //施術所/施術師は全角で
                    
                    exp.clinicname = item.ClinicName.Length > 18 ? 
                        Microsoft.VisualBasic.Strings.StrConv(item.ClinicName.Substring(0, 18),Microsoft.VisualBasic.VbStrConv.Wide) :
                        Microsoft.VisualBasic.Strings.StrConv(item.ClinicName,Microsoft.VisualBasic.VbStrConv.Wide);//施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    
                    exp.drname = Microsoft.VisualBasic.Strings.StrConv(item.DrName,Microsoft.VisualBasic.VbStrConv.Wide);//施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    
                    //exp.clinicname = item.ClinicName;                        //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    //exp.drname = item.DrName;                                //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ

                    //20200804142911 furukawa ed ////////////////////////
                    

                    exp.ratio = item.Ratio.ToString();                       //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
                    exp.total = item.Total.ToString();                       //合計金額（費用額） 9 ゼロ埋めなし。
                    exp.charge = item.Charge.ToString();                     //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                    exp.partial = item.Partial.ToString();                   //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                    exp.counteddays = item.CountedDays.ToString();           //実日数 2 ゼロ埋めなし。
                    exp.bcode = item.TaggedDatas.GeneralString2;             //金融機関コード 4 4桁の数字とする。
                    exp.bname = item.BankName;                               //金融機関名 15 全角
                    exp.bbcode = item.TaggedDatas.GeneralString3;            //支店コード 3 3桁の数字とする。
                    exp.bbname = item.BankBranch;                            //支店名 15 全角
                    exp.bacctype = item.AccountType.ToString();              //科目 1 「1」→普通、「2」→当座
                    exp.baccnumber = item.AccountNumber.PadLeft(7,'0');      //口座番号 7 ゼロ埋め。





                    //20210720101424 furukawa st ////////////////////////
                    //文字数ではなくバイトで切る


                    //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ
                    exp.baccname = CommonTool.SubStringByBytes(item.AccountKana, 30, System.Text.Encoding.GetEncoding("shift-jis"));

                    //          20210422115227 furukawa st ////////////////////////
                    //          文字数オーバーの場合、30文字で切る→バイト配列にしないとちゃんと切れてない
                    //          exp.baccname = item.AccountKana.Length > 30 ? item.AccountKana.Substring(0, 30) : item.AccountKana;//口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ
                    //                exp.baccname = item.AccountKana;                         
                    //          20210422115227 furukawa ed ////////////////////////


                    //20210720101424 furukawa ed ////////////////////////


                    //20210603095337 furukawa st ////////////////////////
                    //拗音置換

                    exp.baccname = CommonTool.ReplaceContract(exp.baccname);


                    //20210603095337 furukawa ed ////////////////////////


                    //20210608145724 furukawa st ////////////////////////
                    //返戻フラグ等あるレコードはテキスト表示
                    
                    //この辺を警告しておけば何とかなる？
                    //返戻 = 0x40, 支払保留 = 0x80,
                    //保留 = 0x200, 過誤 = 0x400, 再審査 = 0x800,
                    //往療点検対象 = 0x1000, 往療疑義 = 0x2000, 

                    if (item.StatusFlagCheck(StatusFlag.返戻)) exp.result_tenken += "返戻,";
                    if (item.StatusFlagCheck(StatusFlag.支払保留)) exp.result_tenken += "支払保留,";
                    if (item.StatusFlagCheck(StatusFlag.保留)) exp.result_tenken += "保留,";
                    if (item.StatusFlagCheck(StatusFlag.過誤)) exp.result_tenken += "過誤,";
                    if (item.StatusFlagCheck(StatusFlag.再審査)) exp.result_tenken += "再審査,";
                    if (item.StatusFlagCheck(StatusFlag.往療点検対象)) exp.result_tenken += "往療点検対象,";
                    if (item.StatusFlagCheck(StatusFlag.往療疑義)) exp.result_tenken += "往療疑義,";
                    //20210608145724 furukawa ed ////////////////////////




                    //同ヒホバン、生年月日、性別の人は出さない
                    //既出テーブルを使用してもいいが、2度手間になりそう…
                    DB.Command cmddup = new DB.Command( 
                        $"select * from export where hnum='{item.HihoNum}' " +
                        $"and pbirth='{item.Birthday.ToString("yyyy/MM/dd")}' " +
                        $"and psex='{item.Sex.ToString()}'",tran);


                    if (cmddup.TryExecuteScalar() != null)
                    {
                        wf.LogPrint($"{item.HihoNum}既に出しました");                        
                        wf.InvokeValue++;
                        continue;
                    }
                    
                    if(!DB.Main.Insert<Export>(exp, tran)) return false;

                    //wf.LogPrint($"aid:{exp.aid}");
                    wf.InvokeValue++;
                }
                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }

               
        }



        //20210608072943 furukawa st ////////////////////////
        //納品データを取得しリストに入れる
        
        /// <summary>
        /// 納品データリスト
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static List<Export> GetExportData(int cym,WaitForm wf)
        {
            string strSQL = string.Empty;

            wf.LogPrint("データ取得");


            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" SELECT ");
            sb.AppendLine(" e.exportid,e.cym,e.ym,e.aid,");

            //ナンバリング 7 1桁目は申請場所の区番号。右6桁は連番。例）1000001・1000002・2000001
            sb.AppendLine(" e.shinsei_basho || lpad(cast(row_number() over (partition by e.shinsei_basho ");


            //修整前のソート
            //"order by shinsei_basho,cast(apptype as int) desc,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +

            //20210617233947 furukawa st ////////////////////////
            //エクセルと同じ日本語並びにするためcollateを使う


            sb.AppendLine(" order by e.shinsei_basho,cast(e.apptype as int) desc,e.ippantaishoku,e.hnum, "+
               "e.ratio,e.pname collate \"ja_JP\" asc,e.startdate1,e.clinicname asc) as varchar),6,'0') numbering ,");

            //sb.AppendLine(" order by e.shinsei_basho,cast(e.apptype as int) desc,e.ippantaishoku,e.hnum,e.pname,e.startdate1,e.clinicname asc) as varchar),6,'0') numbering ,");
            //20210617233947 furukawa ed ////////////////////////


            sb.AppendLine(" e.shoriym,");           //処理年月 5 「5」→令和
            sb.AppendLine(" e.shinsei_basho,");     //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
            sb.AppendLine(" e.apptype,");           //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
            sb.AppendLine(" e.ippantaishoku,");     //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
            sb.AppendLine(" e.hnum,");              //被保険者証番号 8 8桁の数字とする（前「0」）。
            sb.AppendLine(" e.atenanum,");          //宛名番号 0 ブランク
            sb.AppendLine(" e.mainname,");          //世帯主氏名 25 全角
            sb.AppendLine(" e.pname,");             //療養を受けた者の氏名 25 全角
            sb.AppendLine(" e.psex,");              //性別 1 「1」→男、「2」→女
            sb.AppendLine(" e.pbirth,");            //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
            sb.AppendLine(" e.startdate1,");        //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和

            //20210611161746 furukawa st ////////////////////////
            //事前のものは無いはずなので、0を空に置換 2021/06/11長田さん
            
            sb.AppendLine(" case when e.shinseiymd='' then '' else e.shinseiymd end ,");        //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
            //sb.AppendLine(" e.shinseiymd,");        //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
            //20210611161746 furukawa ed ////////////////////////

            //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
            sb.AppendLine(" case when e.clinicname='' then e.drname ");
            sb.AppendLine(" else ");
            sb.AppendLine(" trim(substring(e.clinicname || '　' || e.drname,1,18)) end ,");
            
            sb.AppendLine(" e.ratio,");                 //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
            sb.AppendLine(" e.total,");                 //合計金額（費用額） 9 ゼロ埋めなし。
            sb.AppendLine(" e.charge,");                //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
            sb.AppendLine(" e.partial,");               //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
            sb.AppendLine(" e.counteddays,");           //実日数 2 ゼロ埋めなし。


            //20210720152902 furukawa st ////////////////////////
            //口座情報は整形したexportから取る
            
            sb.AppendLine(" e.bcode,");           //金融機関コード 4 4桁の数字とする。
            sb.AppendLine(" e.bname,");           //金融機関名 15 全角
            sb.AppendLine(" e.bbcode,");          //支店コード 3 3桁の数字とする。
            sb.AppendLine(" e.bbname,");          //支店名 15 全角
            sb.AppendLine(" e.bacctype,");        //口座科目はコードを出す //科目 1 「1」→普通、「2」→当座            
            sb.AppendLine(" e.baccnumber,");      //口座番号 7 ゼロ埋め。
            sb.AppendLine(" e.baccname, ");       //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ



            //      sb.AppendLine(" ac.bankcd,");               //金融機関コード 4 4桁の数字とする。
            //      sb.AppendLine(" ac.bankname,");             //金融機関名 15 全角
            //      sb.AppendLine(" ac.branchcd,");             //支店コード 3 3桁の数字とする。
            //      sb.AppendLine(" ac.branchname,");           //支店名 15 全角

            //      //20210616154148 furukawa st ////////////////////////
            //      //口座科目はコードを出す

            //      sb.AppendLine(" ac.acckindcd,");              //科目 1 「1」→普通、「2」→当座
            //      //      sb.AppendLine(" ac.acckind,");              //科目 1 「1」→普通、「2」→当座
            //      //20210616154148 furukawa ed ////////////////////////


            //      sb.AppendLine(" ac.accnumber,");            //口座番号 7 ゼロ埋め。
            //      sb.AppendLine(" ac.accnamekana, ");         //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ


            //20210720152902 furukawa ed ////////////////////////

            sb.AppendLine(" e.result_bunsho,");      //申請書等の基本項目データ用　文書照会結果
            sb.AppendLine(" e.result_tenken,");      //申請書等の基本項目データ用　点検結果
            sb.AppendLine(" e.henrei_reason,");      //申請書等の基本項目データ用　返戻理由
            sb.AppendLine(" e.biko ");               //申請書等の基本項目データ用　備考

            sb.AppendLine($" FROM export e ");

            sb.AppendLine(" inner join application a on ");
            sb.AppendLine(" e.aid=a.aid ");

            sb.AppendLine(" left join accountinfo ac on ");            
            sb.AppendLine(" replace(regexp_replace(a.taggeddatas,'.+GeneralString5:',''),'\"','')  = cast(ac.accid as varchar) " );



            sb.AppendLine($"where e.cym ='{cym}'");
            sb.AppendLine($" and  e.ym>0 ");

            //ソート順
            //住所区（葵区→駿河区→清水区）=>ナンバリング
            //療養費の種類（はり・きゅう→あん摩・マッサージ）
            //一般退職区分（一般→退職本人→退職扶養）
            //被保険者証番号
            //療養を受けた者（受診者）
            //施術年月
            //施術所

            sb.AppendLine($" order by numbering");

            strSQL = sb.ToString();

            #region oldsql




            //strSQL =
            //    "SELECT " +

            //    //20210608145918 furukawa st ////////////////////////
            //    //管理項目も何かに使えるかもなので持たせておく                
            //    "exportid,cym,ym,aid," +
            //    //20210608145918 furukawa ed ////////////////////////




            //    //"numbering," +

            //    "shinsei_basho || lpad(cast(row_number() over (partition by shinsei_basho " +

            //    //20200804142125 furukawa st ////////////////////////
            //    //idの付け方が間違ってた

            //    "order by shinsei_basho,cast(apptype as int) desc,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +
            //    //"order by shinsei_basho,apptype,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +
            //    //20200804142125 furukawa ed ////////////////////////

            //    "shoriym," +				//処理年月
            //    "shinsei_basho," +          //申請場所
            //    "apptype," +                //療養費の種類
            //    "ippantaishoku," +          //一般退職区分
            //    "hnum," +                   //被保険者証番号
            //    "atenanum," +               //宛名番号
            //    "mainname," +               //世帯主氏名
            //    "pname," +                  //療養を受けた者の氏名
            //    "psex," +                   //性別
            //    "pbirth," +                 //生年月日
            //    "startdate1," +             //施術年月日
            //    "shinseiymd," +             //申請受付年月日


            //    //20200706120217 furukawa st ////////////////////////
            //    //施術所、施術師は両方で18文字なので前から取る
            //    //施術所がない場合は施術師のみ
            //    "case when clinicname='' then drname " +
            //    "else " +
            //    "trim(substring(clinicname || '　' || drname,1,18)) end ," +   //施術機関名、施術師名
            //                                                                  //        "clinicname || ' ' || drname," +   //施術機関名、施術師名

            //    //20200706120217 furukawa ed ////////////////////////


            //    "ratio," +                  //給付割合
            //    "total," +                  //合計金額（費用額）
            //    "charge," +                 //請求額
            //    "partial," +                //一部負担金
            //    "counteddays," +            //実日数

            //    "bcode," +                  //金融機関コード
            //    "bname," +                  //金融機関名
            //    "bbcode," +                 //支店コード
            //    "bbname," +                 //支店名
            //    "bacctype," +               //科目
            //    "baccnumber," +             //口座番号
            //    "baccname " +                //口座名義（ﾌﾘｶﾞﾅ）

            //    //20210608150150 furukawa st ////////////////////////
            //    //申請書等の基本項目データファイル用項目追加

            //    ",result_bunsho" +
            //    ",result_tenken" +
            //    ",henrei_reason" +
            //    ",biko ";
            //    //20210608150150 furukawa ed ////////////////////////




            //strSQL += $"FROM export where cym ='{cym}'";

            ////20200706121703 furukawa st ////////////////////////
            ////申請書だけ出す

            //strSQL += $" and  ym>0 ";
            ////20200706121703 furukawa ed ////////////////////////


            ////ソート順
            ////住所区（葵区→駿河区→清水区）=>ナンバリング
            ////療養費の種類（はり・きゅう→あん摩・マッサージ）
            ////一般退職区分（一般→退職本人→退職扶養）
            ////被保険者証番号
            ////療養を受けた者（受診者）
            ////施術年月
            ////施術所


            //strSQL += $" order by numbering";
            ////strSQL += $" order by  numbering, cast(apptype as int) desc,ippantaishoku,hnum,pname,startdate1,clinicname";
            #endregion



            DB.Command cmd = new DB.Command(DB.Main, strSQL);
            var l = cmd.TryExecuteReaderList();

            List<Export> lst = new List<Export>();
            foreach(var item in l)
            {

                if (CommonTool.WaitFormCancelProcess(wf)) return null;

                

                Export e = new Export();
                e.exportid = int.Parse(item[0].ToString());
                e.cym = int.Parse(item[1].ToString());
                e.ym = int.Parse(item[2].ToString());
                e.aid = int.Parse(item[3].ToString());

                e.numbering 			= item[4].ToString();
                e.shoriym 				= item[5].ToString();
                e.shinsei_basho 		= item[6].ToString();
                e.apptype 				= item[7].ToString();
                e.ippantaishoku 		= item[8].ToString();
                e.hnum 					= item[9].ToString();
                e.atenanum 				= item[10].ToString();
                e.mainname 				= item[11].ToString();
                e.pname 				= item[12].ToString();
                e.psex 					= item[13].ToString();
                e.pbirth 				= item[14].ToString();
                e.startdate1 			= item[15].ToString();
                e.shinseiymd 			= item[16].ToString();
                
                e.clinicname 			= item[17].ToString();                
                //e.drname				=item[17].ToString();施術所名とくっつけてあるので不要

                e.ratio 				= item[18].ToString();
                e.total 				= item[19].ToString();
                e.charge 				= item[20].ToString();
                e.partial 				= item[21].ToString();
                e.counteddays 			= item[22].ToString();
                e.bcode 				= item[23].ToString();
                e.bname 				= item[24].ToString();
                e.bbcode 				= item[25].ToString();
                e.bbname 				= item[26].ToString();
                e.bacctype 				= item[27].ToString();
                e.baccnumber 			= item[28].ToString();
                e.baccname 				= item[29].ToString();


                //20210608150309 furukawa st ////////////////////////
                //申請書等の基本項目データファイル用項目追加
                
                e.result_bunsho = item[30].ToString();
                e.result_tenken = item[31].ToString();
                e.henrei_reason = item[32].ToString();
                e.biko = item[33].ToString();
                //20210608150309 furukawa ed ////////////////////////



                //e.numbering = item[0].ToString();
                //e.shoriym = item[1].ToString();
                //e.shinsei_basho = item[2].ToString();
                //e.apptype = item[3].ToString();
                //e.ippantaishoku = item[4].ToString();
                //e.hnum = item[5].ToString();
                //e.atenanum = item[6].ToString();
                //e.mainname = item[7].ToString();
                //e.pname = item[8].ToString();
                //e.psex = item[9].ToString();
                //e.pbirth = item[10].ToString();
                //e.startdate1 = item[11].ToString();
                //e.shinseiymd = item[12].ToString();
                //e.clinicname = item[13].ToString();
                ////e.drname				=item[14].ToString();
                //e.ratio = item[14].ToString();
                //e.total = item[15].ToString();
                //e.charge = item[16].ToString();
                //e.partial = item[17].ToString();
                //e.counteddays = item[18].ToString();
                //e.bcode = item[19].ToString();
                //e.bname = item[20].ToString();
                //e.bbcode = item[21].ToString();
                //e.bbname = item[22].ToString();
                //e.bacctype = item[23].ToString();
                //e.baccnumber = item[24].ToString();
                //e.baccname = item[25].ToString();



                lst.Add(e);

            }
            return lst;

        }
        //20210608072943 furukawa ed ////////////////////////


        //20210608073107 furukawa st ////////////////////////
        //申請書等の基本項目データ作成
        
        /// <summary>
        /// 申請書等の基本項目データ
        /// </summary>
        /// <param name="lstExport"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool DataExport_excel(List<Export> lstExport, string strDir, int cym,WaitForm wf)
        {

            //出力ファイル名
            string strFileName = $"{strDir}\\申請書等の基本項目データ{lstExport[0].shoriym}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_静岡市あはき.xlsx";
            
            System.IO.Directory.CreateDirectory(strDir);            //納品フォルダ
    

            wf.LogPrint("申請書等の基本項目データ ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lstExport.Count);

            #region NPOIコード
            System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet();
            #endregion
     
            try
            {
                
                #region ヘッダ行
                NPOI.SS.UserModel.IFont font = wb.CreateFont();
                font.FontName = "ＭＳ ゴシック";
                font.FontHeightInPoints = 11;
                NPOI.SS.UserModel.ICellStyle csHeader = wb.CreateCellStyle();
                csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                csHeader.SetFont(font);
                
                NPOI.SS.UserModel.IRow row = ws.CreateRow(0);
                int cntHeaderCol = 30;

                for (int r = 0; r < cntHeaderCol; r++)
                {
                    NPOI.SS.UserModel.ICell hcell = row.CreateCell(r);
                    hcell.CellStyle = csHeader;
                    
                    if (r == 0) hcell.SetCellValue("No.");
                    if (r == 1) hcell.SetCellValue("処理年月");
                    if (r == 2) hcell.SetCellValue("申請場所");
                    if (r == 3) hcell.SetCellValue("種類");
                    if (r == 4) hcell.SetCellValue("一般退職");
                    if (r == 5) hcell.SetCellValue("保険者番号");
                    if (r == 6) hcell.SetCellValue("被保険者番号");
                    if (r == 7) hcell.SetCellValue("世帯主名");
                    if (r == 8) hcell.SetCellValue("受療者名");
                    if (r == 9) hcell.SetCellValue("性別");
                    if (r == 10) hcell.SetCellValue("生年月日");
                    if (r == 11) hcell.SetCellValue("施術年月日");
                    if (r == 12) hcell.SetCellValue("申請受付年月日");
                    if (r == 13) hcell.SetCellValue("施術機関名・施術師名");
                    if (r == 14) hcell.SetCellValue("給付割合");
                    if (r == 15) hcell.SetCellValue("合計金額");
                    if (r == 16) hcell.SetCellValue("請求金額");
                    if (r == 17) hcell.SetCellValue("一部負担金");
                    if (r == 18) hcell.SetCellValue("実日数");
                    if (r == 19) hcell.SetCellValue("金融機関コード");
                    if (r == 20) hcell.SetCellValue("金融機関名");
                    if (r == 21) hcell.SetCellValue("支店コード");
                    if (r == 22) hcell.SetCellValue("支店名");
                    if (r == 23) hcell.SetCellValue("科目");
                    if (r == 24) hcell.SetCellValue("口座番号");
                    if (r == 25) hcell.SetCellValue("口座名義（ﾌﾘｶﾞﾅ）");
                    if (r == 26) hcell.SetCellValue("文書照会結果");
                    if (r == 27) hcell.SetCellValue("点検結果");
                    if (r == 28) hcell.SetCellValue("返戻理由");
                    if (r == 29) hcell.SetCellValue("備考");

                }
                #endregion

                NPOI.SS.UserModel.ICellStyle csWarn = wb.CreateCellStyle();               
                csWarn.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.Orange.Index;
                csWarn.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;                

                for (int r = 0; r < lstExport.Count(); r++)
                {
                    //中断処理                    
                    if (CommonTool.WaitFormCancelProcess(wf)) return false;


                    row = ws.CreateRow(r + 1);

                    string strres = string.Empty;
                   // imageName = $"{strImageDir}\\{lstExport[r].numbering}.tif";

                    for (int colcnt = 0; colcnt < cntHeaderCol; colcnt++)
                        //for (int colcnt = 0; colcnt < 27; colcnt++)
                    {

                        NPOI.SS.UserModel.ICell cell = row.CreateCell(colcnt);

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        //何か存在する場合は注意の意味でオレンジにする
                        if (lstExport[r].result_bunsho != string.Empty ||
                            lstExport[r].result_tenken != string.Empty ||
                            lstExport[r].henrei_reason != string.Empty ||
                            lstExport[r].biko != string.Empty) cell.CellStyle = csWarn;
                        else cell.CellStyle = null;


                        if (colcnt == 0) cell.SetCellValue(r+1);//単なる連番
                        //if (colcnt == 0) cell.SetCellValue(lstExport[r].numbering);

                        if (colcnt == 1) cell.SetCellValue(lstExport[r].shoriym);
                        if (colcnt == 2) cell.SetCellValue(lstExport[r].shinsei_basho);
                        if (colcnt == 3) cell.SetCellValue(lstExport[r].apptype);
                        if (colcnt == 4) cell.SetCellValue(lstExport[r].ippantaishoku);

                        //20210608150407 furukawa st ////////////////////////
                        //いつの間にか保険者番号が出力項目に入ってた
                        
                        if (colcnt == 5)
                        {
                            //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
                            //場所	保険者番号
                            //1	00224014
                            //2	00224022
                            //3	00224030

                            switch (lstExport[r].shinsei_basho)
                            {
                                case "1":
                                    cell.SetCellValue("00224014");
                                    break;
                                case "2":
                                    cell.SetCellValue("00224022");
                                    break;
                                case "3":
                                    cell.SetCellValue("00224030");
                                    break;
                                default:
                                    cell.SetCellValue("");
                                    break;
                            }

                        }
                        //      if (colcnt == 5) cell.SetCellValue(lstExport[r].hnum);

                        //20210608150407 furukawa ed ////////////////////////



                        //20210608150512 furukawa st ////////////////////////
                        //いつの間にか被保険者番号の列番号が入れ変わってた
                        
                        if (colcnt == 6) cell.SetCellValue(lstExport[r].hnum);
                        //      if (colcnt == 6) cell.SetCellValue(lstExport[r].atenanum);
                        //20210608150512 furukawa ed ////////////////////////

                        if (colcnt == 7) cell.SetCellValue(lstExport[r].mainname);      //世帯主氏名 25 全角
                        if (colcnt == 8) cell.SetCellValue(lstExport[r].pname);         //療養を受けた者の氏名 25 全角
                        if (colcnt == 9) cell.SetCellValue(lstExport[r].psex);          //性別 1 「1」→男、「2」→女
                        if (colcnt == 10) cell.SetCellValue(lstExport[r].pbirth);       //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
                        if (colcnt == 11) cell.SetCellValue(lstExport[r].startdate1);   //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和

                        //20210611132740 furukawa st ////////////////////////
                        //事前のものは無いはずなので、0を空に置換 2021/06/11長田さん                        
                        if (colcnt == 12) cell.SetCellValue(lstExport[r].shinseiymd == "0" ? string.Empty : lstExport[r].shinseiymd);   //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
                        //if (colcnt == 12) cell.SetCellValue(lstExport[r].shinseiymd);   //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
                        //20210611132740 furukawa ed ////////////////////////


                        //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                        if (colcnt == 13) cell.SetCellValue(lstExport[r].drname.Trim()!=string.Empty ? $"{lstExport[r].clinicname}　{lstExport[r].drname}": $"{lstExport[r].clinicname}");
                        
                        if (colcnt == 14) cell.SetCellValue(lstExport[r].ratio);            //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割


                        //20211102095039 furukawa st ////////////////////////
                        //exportテーブルは文字列型なので、出力直前で変換

                        if (colcnt == 15) cell.SetCellValue(int.Parse(lstExport[r].total.ToString()));            //合計金額（費用額） 9 ゼロ埋めなし。
                        if (colcnt == 16) cell.SetCellValue(int.Parse(lstExport[r].charge.ToString()));           //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                        if (colcnt == 17) cell.SetCellValue(int.Parse(lstExport[r].partial.ToString()));          //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                        if (colcnt == 18) cell.SetCellValue(int.Parse(lstExport[r].counteddays.ToString()));      //実日数 2 ゼロ埋めなし。

                        //if (colcnt == 15) cell.SetCellValue(lstExport[r].total);            //合計金額（費用額） 9 ゼロ埋めなし。
                        //if (colcnt == 16) cell.SetCellValue(lstExport[r].charge);           //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                        //if (colcnt == 17) cell.SetCellValue(lstExport[r].partial);          //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                        //if (colcnt == 18) cell.SetCellValue(lstExport[r].counteddays);      //実日数 2 ゼロ埋めなし。

                        //20211102095039 furukawa ed ////////////////////////


                        if (colcnt == 19) cell.SetCellValue(lstExport[r].bcode);            //金融機関コード 4 4桁の数字とする。


                        //20211102095156 furukawa st ////////////////////////
                        //すでに登録してある場合も全角変換
                        
                        if (colcnt == 20) cell.SetCellValue(Microsoft.VisualBasic.Strings.StrConv(lstExport[r].bname,Microsoft.VisualBasic.VbStrConv.Wide));            //金融機関名 15 全角
                        //if (colcnt == 20) cell.SetCellValue(lstExport[r].bname);            //金融機関名 15 全角
                        //20211102095156 furukawa ed ////////////////////////


                        if (colcnt == 21) cell.SetCellValue(lstExport[r].bbcode);           //支店コード 3 3桁の数字とする。


                        //20211102100530 furukawa st ////////////////////////
                        //すでに登録してある場合も全角変換
                        
                        if (colcnt == 22) cell.SetCellValue(Microsoft.VisualBasic.Strings.StrConv(lstExport[r].bbname,Microsoft.VisualBasic.VbStrConv.Wide));           //支店名 15 全角
                        //if (colcnt == 22) cell.SetCellValue(lstExport[r].bbname);           //支店名 15 全角
                        //20211102100530 furukawa ed ////////////////////////



                        if (colcnt == 23) cell.SetCellValue(lstExport[r].bacctype);         //科目 1 「1」→普通、「2」→当座
                        if (colcnt == 24) cell.SetCellValue(lstExport[r].baccnumber);       //口座番号 7 ゼロ埋め。
                        if (colcnt == 25) cell.SetCellValue(lstExport[r].baccname);         //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ
                                                                                            
                                                                                            
                        //20210608150556 furukawa st ////////////////////////               
                        //申請書等の基本項目データファイル用項目追加                        
                        
                        if (colcnt == 26) cell.SetCellValue(lstExport[r].result_bunsho);        //申請書等の基本項目データ用　文書照会結果
                        if (colcnt == 27) cell.SetCellValue(lstExport[r].result_tenken);        //申請書等の基本項目データ用　点検結果
                        if (colcnt == 28) cell.SetCellValue(lstExport[r].henrei_reason);        //申請書等の基本項目データ用　返戻理由
                        if (colcnt == 29) cell.SetCellValue(lstExport[r].biko);                 //申請書等の基本項目データ用　備考
                        //20210608150556 furukawa ed ////////////////////////


                        row.Cells.Add(cell);
                    }



                    //App a = App.GetApp(lstExport[r].aid);
                    //string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());
                    

                    ////被保番があるレコードのみ画像をコピー
                    //if (lstExport[r].hnum != string.Empty)
                    //{
                    //    wf.LogPrint($"画像ファイルコピー中:{strImageFileName}");
                    //    fc.FileCopy(strImageFileName, imageName);
                    //}


                    wf.InvokeValue++;
                }
                
                
                //ヘッダ行にフィルタ
                NPOI.SS.Util.CellRangeAddress area = new NPOI.SS.Util.CellRangeAddress(0,ws.LastRowNum,0, cntHeaderCol);
                ws.SetAutoFilter(area);

                //列幅自動
                for (int c = 0; c < cntHeaderCol; c++) ws.AutoSizeColumn(c);



                //申請書等の基本項目データのチェック
                if(!CheckData(ws, wb, cym,wf))return false;
                


                //エクセル書き込み
                wb.Write(fs);

             
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                #region npoiコード
                wb.Close();
                fs.Close();
                //cmd.Dispose();
                //sw.Close();
                if (flgCheckNG)
                {
                    System.Windows.Forms.MessageBox.Show("申請書等の基本項目データファイルにエラー箇所がありました。確認ください");
                    
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = strFileName;
                    p.Start();

                }

                #endregion

            }

        }

        //20210608073107 furukawa ed ////////////////////////


        /// <summary>
        /// 申請書等の基本項目データのチェック
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="csWarn"></param>
        private static bool CheckData(NPOI.SS.UserModel.ISheet ws, NPOI.SS.UserModel.IWorkbook wb,int cym,WaitForm wf)
        {
            NPOI.SS.UserModel.ICellStyle csWarn = wb.CreateCellStyle();
         
            csWarn.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.Pink.Index;
            csWarn.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
            
            

            try
            {
                
                wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                string strCYM = (DateTimeEx.GetEraNumberYearFromYYYYMM(cym) * 100 + cym % 100).ToString();

                for (int r = 1; r <= ws.LastRowNum; r++)
                {

                    wf.LogPrint($"チェック中:{r}行目");

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);
                    bool flgNG = false;

                    int col = 0;
                    List<int> lstErrCol = new List<int>();

                    for (col=0; col <= row.LastCellNum; col++)
                    {
                        flgNG = true;
                        

                        //エクセルでは連番 連番	NULLが入っていないか、ちゃんと最後まで連番がついているか
                        //数字かどうか
                        if (col == 0)
                        {
                            if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                                (!int.TryParse(getCellValueToString(row.GetCell(col)),out int tmp))) lstErrCol.Add(col);

                        }

                        //処理年月 処理年月になっているか
                        //cymと比較
                        if (col == 1)
                        {
                            if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                                (getCellValueToString(row.GetCell(col)) != strCYM)) lstErrCol.Add(col);
                        }

                        //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区 
                        //1(葵区),2(駿河区),3(清水区)以外が入っていないか
                        if (col == 2) if (!new string[] { "1", "2", "3" }.Contains(getCellValueToString(row.GetCell(col)))) lstErrCol.Add(col);

                        //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
                        //療養費の種類  17(あんま),19(鍼灸)以外が入っていないか
                        if (col == 3) if (!new string[] { "17", "19" }.Contains(getCellValueToString(row.GetCell(col)))) lstErrCol.Add(col);

                        //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
                        //一般退職区分	1以外が入っていないか
                        if (col == 4) if (getCellValueToString(row.GetCell(col)) != "1") lstErrCol.Add(col);

                        //保険者番号いつから？？
                        if (col == 5) if (getCellValueToString(row.GetCell(col)).Trim() == String.Empty) lstErrCol.Add(col);


                        //被保険者証番号 8 8桁の数字とする（前「0」）。
                        //被保険者証番号	被保番号8桁で空白行などがないか
                        if (col == 6) if (getCellValueToString(row.GetCell(col)).Length != 8) lstErrCol.Add(col);

                        //世帯主氏名 25 全角
                        //世帯主氏名	空白セルがないか
                        if (col == 7) if (getCellValueToString(row.GetCell(col)) == string.Empty) lstErrCol.Add(col);

                        //療養を受けた者の氏名 25 全角
                        //療養を受けた者の氏名	空白セルがないか
                        if (col == 8) if (getCellValueToString(row.GetCell(col)) == string.Empty) lstErrCol.Add(col);

                        //性別 1 「1」→男、「2」→女
                        //性別	1,2以外が入っていないか
                        if (col == 9)
                        {
                            if (!new string[] { "1", "2" }.Contains(getCellValueToString(row.GetCell(col)))) lstErrCol.Add(col);
                        }

                        //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
                        //生年月日 極端に古い年月(642015等)がないか
                        if (col == 10)
                        {
                            
                            if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                                (!new string[] { "3", "4", "5" }.Contains(getCellValueToString(row.GetCell(col)).Substring(0,1)))) lstErrCol.Add(col);
                        }

                        // 施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和
                        //生年月日 極端に古い年月がないか
                        if (col == 11)
                        {
                            if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                                (!new string[] { "4", "5" }.Contains(getCellValueToString(row.GetCell(col)).Substring(0,1)))) lstErrCol.Add(col);
                        }

                        //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
                        //生年月日 極端に古い年月がないか
                        if (col == 12)
                        {
                            //20210611132113 furukawa st ////////////////////////
                            //事前のものは無いはずなので、無くてもOK 2021/06/11長田さん
                            

                            if ((getCellValueToString(row.GetCell(col)) != string.Empty) &&
                                (!new string[] { "4", "5" }.Contains(getCellValueToString(row.GetCell(col)).Substring(0, 1)))) lstErrCol.Add(col);

                            //if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                            //    (!new string[] { "4", "5" }.Contains(getCellValueToString(row.GetCell(col)).Substring(0,1)))) lstErrCol.Add(col);
                            //20210611132113 furukawa ed ////////////////////////


                        }


                        //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                        //施術機関名、施術師名 空白セルがないか
                        if (col == 13)
                        {                            
                            if ((getCellValueToString(row.GetCell(col)).Trim() == string.Empty) ||                            
                                (getCellValueToString(row.GetCell(col)).Length > 18)) lstErrCol.Add(col);
                        }


                        //   if (c == 14) if (getCellValueToString(row.GetCell(c)) == string.Empty) flgNG=true;                    //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ

                        //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
                        //給付割合  7,8以外が入っていないか
                        if (col == 14) if (!new string[] { "7", "8" }.Contains(getCellValueToString(row.GetCell(col)))) lstErrCol.Add(col);

                        //合計金額（費用額） 9 ゼロ埋めなし。
                        //合計金額	空白セル、0がないか
                        //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                        //請求額	空白セル、0がないか
                        //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                        //一部負担金	空白セル、0がないか

                        if (col == 15 || col == 16 || col == 17)
                        {
                            if ((getCellValueToString(row.GetCell(col)).Trim() == string.Empty) ||
                             (getCellValueToString(row.GetCell(col)).Trim() == "0") ||
                             (!int.TryParse(getCellValueToString(row.GetCell(col)), out int tmp))) lstErrCol.Add(col);
                        }


                        //実日数 2 ゼロ埋めなし。
                        //実日数	空白セル、0、40等がないか
                        if (col == 18)
                        {
                            if ((getCellValueToString(row.GetCell(col)).Trim() == string.Empty) ||
                             (!int.TryParse(getCellValueToString(row.GetCell(col)),out int tmp)) ||
                             (int.Parse(getCellValueToString(row.GetCell(col))) > 31)) lstErrCol.Add(col);
                        }

                        //金融機関コード 4 4桁の数字とする。
                        //空白セルがないか　空白セルがあればメホールで登録
                        if (col == 19)
                        {
                            if ((getCellValueToString(row.GetCell(col)) == string.Empty) ||
                                (getCellValueToString(row.GetCell(col)).Length != 4)) lstErrCol.Add(col);
                        }

                        //金融機関名 15 全角
                        if (col == 20) if (getCellValueToString(row.GetCell(col)) == string.Empty || (getCellValueToString(row.GetCell(col)).Length > 15)) lstErrCol.Add(col);

                        //支店コード 3 3桁の数字とする。
                        if (col == 21) if (getCellValueToString(row.GetCell(col)) == string.Empty || (getCellValueToString(row.GetCell(col)).Length !=3)) lstErrCol.Add(col);

                        //支店名 15 全角
                        if (col == 22) if (getCellValueToString(row.GetCell(col)) == string.Empty || (getCellValueToString(row.GetCell(col)).Length >15 )) lstErrCol.Add(col);

                        //科目 1 「1」→普通、「2」→当座
                        if (col == 23) if (getCellValueToString(row.GetCell(col)) == string.Empty || !new string[] { "1", "2" }.Contains(getCellValueToString(row.GetCell(col)))) lstErrCol.Add(col);

                        //口座番号 7 ゼロ埋め。
                        if (col == 24) if (getCellValueToString(row.GetCell(col)) == string.Empty || (getCellValueToString(row.GetCell(col)).Length != 7)) lstErrCol.Add(col);

                        //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ
                        if (col == 25) if (getCellValueToString(row.GetCell(col)) == string.Empty || (getCellValueToString(row.GetCell(col)).Length > 30)) lstErrCol.Add(col);                    
                                    
                        //flgNG = false;

                    }


                    if (lstErrCol.Count > 0)
                    {
                        flgNG = true;

                        wf.LogPrint($"{r}行目{col + 1}列 NG");
                        for (int lstcnt = 0; lstcnt < lstErrCol.Count; lstcnt++)
                        {
                            NPOI.SS.UserModel.IRow errrow = ws.GetRow(r);
                            NPOI.SS.UserModel.ICell cell = errrow.GetCell(lstErrCol[lstcnt]);
                            cell.CellStyle = csWarn;

                        }

                        //foreach (NPOI.SS.UserModel.ICell cell in row.Cells)
                        //{
                        //    if (col == cell.ColumnIndex) cell.CellStyle = csWarn;
                        //}
                    }


                    else flgNG = false;

                    //{
                    //    foreach (NPOI.SS.UserModel.ICell cell in row.Cells)
                    //    {
                    //        if(cell.CellStyle==null)
                    //        cell.CellStyle = null;
                    //    }

                    //}
                

                    wf.InvokeValue ++;

                    flgCheckNG = flgCheckNG || flgNG;
                }

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            if (cell == null) return string.Empty;

            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }


        //20210608073141 furukawa st ////////////////////////
        //支払CSV作成

        /// <summary>
        /// CSV出力
        /// <param name="cym">処理年月</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        private static bool DataExport(List<Export> lst,int cym, string strDir, WaitForm wf)
        {

            string strFileName = strDir + "\\";
            string strHeader = string.Empty;
            string strSQL = string.Empty;
            bool flgNG = false;

            if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);

          
            strFileName += $"{cym}_療養費振込データ.csv";

            wf.LogPrint($"{strFileName} ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lst.Count);

            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("shift_jis"));

            System.Text.StringBuilder sbErr = new StringBuilder();

            try
            {
                //20210617205724 furukawa st ////////////////////////
                //連番用
                
                string strNumbering = string.Empty;
                int cnt = 0;
                string prev_shinsei_basho = string.Empty;
                //20210617205724 furukawa ed ////////////////////////

                string strCYM = (DateTimeEx.GetEraNumberYearFromYYYYMM(cym) * 100 + cym % 100).ToString();
                
                sbErr.Remove(0, sbErr.ToString().Length);

                foreach (Export e in lst)
                {

                    

                    //20210617234152 furukawa st ////////////////////////
                    //CSV出力時にもチェックを付けた
                    
                    System.Text.StringBuilder sb = new StringBuilder();

                    //20210608150620 furukawa st ////////////////////////
                    //支払データには支払保留出さない

                    //支払保留出さない
                    if (e.result_tenken.Contains("支払保留"))
                    {
                        continue;
                    }
                    else
                    {
                        //20210617205632 furukawa st ////////////////////////
                        //最初から付いてる連番は、途中を飛ばすと抜けてしまうためこっちで作る

                        if (prev_shinsei_basho != e.shinsei_basho) cnt = 1;
                        else cnt++;
                        //20210617205632 furukawa ed ////////////////////////

                    }
                    //20210608150620 furukawa ed ////////////////////////

                    //20210617205744 furukawa st ////////////////////////
                    //連番作成

                    strNumbering = $"{e.shinsei_basho}{cnt.ToString().PadLeft(6, '0')}";
                    //20210617205744 furukawa ed ////////////////////////

                    sb.Append($"{strNumbering},");


                    if ((e.shoriym == string.Empty) || (e.shoriym != strCYM))
                    {                        
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  処理年月が一致しません"); 
                        flgNG=true;
                    }
                    sb.Append($"{e.shoriym},");

                    //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区 
                    //1(葵区),2(駿河区),3(清水区)以外が入っていないか
                    if (!new string[] { "1", "2", "3" }.Contains(e.shinsei_basho))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  区番号が違います");
                        flgNG=true;
                    }
                    sb.Append($"{e.shinsei_basho},");

                    //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
                    //療養費の種類  17(あんま),19(鍼灸)以外が入っていないか
                    if (!new string[] { "17", "19" }.Contains(e.apptype))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  療養費の種類  17(あんま),19(鍼灸)以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.apptype},");

                    //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
                    //一般退職区分	1以外が入っていないか
                    if (e.ippantaishoku != "1")
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  一般退職区分	1以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.ippantaishoku},");

                    //被保険者証番号 8 8桁の数字とする（前「0」）。
                    //被保険者証番号	被保番号8桁で空白行などがないか
                    if (e.hnum.Length != 8)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  被保険者証番号 8桁の数字以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.hnum},");


                    //宛名番号
                    sb.Append($"{e.atenanum},");


                    //世帯主氏名 25 全角
                    //世帯主氏名	空白セルがないか
                    if (e.mainname == string.Empty)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  世帯主氏名	空白");
                        flgNG=true;
                    }
                    sb.Append($"{e.mainname},");


                    //療養を受けた者の氏名 25 全角
                    //療養を受けた者の氏名	空白セルがないか
                    if (e.pname == string.Empty)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  療養を受けた者の氏名	空白");
                        flgNG=true;
                    }
                    sb.Append($"{e.pname},");


                    //性別 1 「1」→男、「2」→女
                    //性別	1,2以外が入っていないか    
                    if (!new string[] { "1", "2" }.Contains(e.psex))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  性別	1,2以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.psex},");


                    //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
                    //生年月日 極端に古い年月(642015等)がないか
                    if ((e.pbirth == string.Empty) || (!new string[] { "3", "4", "5" }.Contains(e.pbirth.Substring(0, 1))))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  生年月日 極端に古い年月");
                        flgNG=true;
                    }
                    sb.Append($"{e.pbirth},");


                    // 施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和
                    //施術年月日 極端に古い年月がないか
                    if ((e.startdate1 == string.Empty) || (!new string[] { "4", "5" }.Contains(e.startdate1.Substring(0, 1))))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  施術年月日 極端に古い年月");
                        flgNG=true;
                    }
                    sb.Append($"{e.startdate1},");


                    //申請年月日が0の場合0埋め
                    if (e.shinseiymd == "0" || e.shinseiymd == string.Empty) sb.Append("0000000,");
                    else if (!new string[] { "4", "5" }.Contains(e.shinseiymd.Substring(0, 1)))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  申請年月日 極端に古い年月");
                        flgNG=true;
                    }
                    else sb.Append($"{e.shinseiymd},");



                    //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    //施術機関名、施術師名 空白セルがないか
                    if ((e.clinicname.Trim() == string.Empty) || (e.clinicname.Length > 18))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  施術機関名、施術師名 空白");
                        flgNG=true;
                    }
                    sb.Append($"{e.clinicname},");


                    //   if (c == 14) if (getCellValueToString(row.GetCell(c)) == string.Empty) flgNG=true;           //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ

                    //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
                    //給付割合  7,8以外が入っていないか
                    if (!new string[] { "7", "8" }.Contains(e.ratio))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  給付割合  7,8以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.ratio},");

                    //合計金額（費用額） 9 ゼロ埋めなし。
                    //合計金額	空白セル、0がないか
                    //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                    //請求額	空白セル、0がないか
                    //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                    //一部負担金	空白セル、0がないか

                    if ((e.total.Trim() == string.Empty) || (e.total.Trim() == "0") || (!int.TryParse(e.total, out int tmp)))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  合計金額	空白、0");
                        flgNG=true;
                    }
                    sb.Append($"{e.total},");

                    if ((e.charge.Trim() == string.Empty) || (e.charge.Trim() == "0") || (!int.TryParse(e.charge, out int tmpcharge)))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  請求金額	空白、0");
                        flgNG=true;
                    }
                    sb.Append($"{e.charge},");

                    if ((e.partial.Trim() == string.Empty) || (e.partial.Trim() == "0") || (!int.TryParse(e.partial, out int tmppartial)))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  一部負担金	空白、0");
                        flgNG=true;
                    }
                    sb.Append($"{e.partial},");



                    //実日数 2 ゼロ埋めなし。
                    //実日数	空白セル、0、40等がないか
                    if ((e.counteddays.Trim() == string.Empty) || (!int.TryParse(e.counteddays, out int tmpcounteddays)) || (int.Parse(e.counteddays) > 31))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  実日数	空白、0");
                        flgNG=true;
                    }
                    sb.Append($"{e.counteddays},");


                    //金融機関コード 4 4桁の数字とする。
                    //空白セルがないか　空白セルがあればメホールで登録
                    if ((e.bcode == string.Empty) || (e.bcode.Length != 4))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  金融機関コード	空白、0");
                        flgNG=true;
                    }
                    
                    sb.Append($"{e.bcode},");


                    //金融機関名 15 全角
                    if (e.bname == string.Empty || e.bname.Length > 15)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  金融機関名 15 全角以外");
                        flgNG=true;
                    }
                    //20211102101230 furukawa st ////////////////////////
                    //すでに登録してある場合も全角変換
                    
                    sb.Append($"{Microsoft.VisualBasic.Strings.StrConv(e.bname,Microsoft.VisualBasic.VbStrConv.Wide)},");
                    //sb.Append($"{e.bname},");
                    //20211102101230 furukawa ed ////////////////////////


                    //支店コード 3 3桁の数字とする。
                    if (e.bbcode == string.Empty || e.bbcode.Length != 3)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  支店コード  3桁の数字以外");
                        flgNG=true;
                    }
                    
                    sb.Append($"{e.bbcode.PadLeft(3,'0')},");

                    //支店名 15 全角
                    if (e.bbname == string.Empty || e.bbname.Length > 15)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  支店名 15 全角以外");
                        flgNG=true;
                    }

                    //20211102102048 furukawa st ////////////////////////
                    //すでに登録してある場合も全角変換
                    
                    sb.Append($"{Microsoft.VisualBasic.Strings.StrConv(e.bbname,Microsoft.VisualBasic.VbStrConv.Wide)},");
                    //sb.Append($"{e.bbname},");
                    //20211102102048 furukawa ed ////////////////////////


                    //科目 1 「1」→普通、「2」→当座
                    if (e.bacctype == string.Empty || !new string[] { "1", "2" }.Contains(e.bacctype))
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  科目 1 「1」→普通、「2」→当座 以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.bacctype},");

                    //口座番号 7 ゼロ埋め。
                    if (e.baccnumber == string.Empty || e.baccnumber.Length != 7)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  口座番号 7 ゼロ埋め 以外");
                        flgNG=true;
                    }
                    sb.Append($"{e.baccnumber.PadLeft(7,'0')},");


                    //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ
                    if (e.baccname == string.Empty || e.baccname.Length > 30)
                    {
                        sbErr.AppendLine($"AID:{e.aid} Numbering:{strNumbering}  口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ 以外");
                        flgNG=true;                   

                    }
                    sb.Append($"{CommonTool.ReplaceContract(e.baccname)}");




                    #region old





                    //string strres = string.Empty;



                    ////20210608150620 furukawa st ////////////////////////
                    ////支払データには支払保留出さない

                    ////支払保留出さない
                    //if (e.result_tenken.Contains("支払保留"))
                    //{
                    //    continue;
                    //}
                    //else
                    //{
                    //    //20210617205632 furukawa st ////////////////////////
                    //    //最初から付いてる連番は、途中を飛ばすと抜けてしまうためこっちで作る

                    //    if (prev_shinsei_basho != e.shinsei_basho) cnt = 1;
                    //    else cnt++;
                    //    //20210617205632 furukawa ed ////////////////////////

                    //}
                    ////20210608150620 furukawa ed ////////////////////////

                    ////20210617205744 furukawa st ////////////////////////
                    ////連番作成

                    //strNumbering = $"{e.shinsei_basho}{cnt.ToString().PadLeft(6, '0')}";
                    ////20210617205744 furukawa ed ////////////////////////




                    //strres +=
                    //    $"{strNumbering}," +
                    //    $"{e.shoriym}," +
                    //    $"{e.shinsei_basho}," +
                    //    $"{e.apptype}," +
                    //    $"{e.ippantaishoku}," +
                    //    $"{e.hnum}," +
                    //    $"{e.atenanum}," +
                    //    $"{e.mainname}," +
                    //    $"{e.pname}," +
                    //    $"{e.psex}," +
                    //    $"{e.pbirth}," +
                    //    $"{e.startdate1},";

                    //    //申請年月日が0の場合0埋め
                    //    if (e.shinseiymd == "0") strres += $"0000000,";
                    //    else strres += $"{e.shinseiymd},";

                    ////$"{e.shinseiymd}," +

                    ////20210617195456 furukawa st ////////////////////////
                    ////すでにくっついている

                    //strres += $"{e.clinicname}," +
                    ////$"{e.clinicname}　{e.drname}," +
                    ////20210617195456 furukawa ed ////////////////////////


                    //    $"{e.ratio}," +
                    //    $"{e.total}," +
                    //    $"{e.charge}," +
                    //    $"{e.partial}," +
                    //    $"{e.counteddays}," +
                    //    $"{e.bcode}," +
                    //    $"{e.bname}," +
                    //    $"{e.bbcode}," +
                    //    $"{e.bbname}," +
                    //    $"{e.bacctype}," +
                    //    $"{e.baccnumber}," +

                    //    //20210617210342 furukawa st ////////////////////////
                    //    //拗音変換

                    //    $"{CommonTool.ReplaceContract(e.baccname)}";
                    //    //$"{e.baccname}";
                    //    //20210617210342 furukawa ed ////////////////////////

                    //sw.WriteLine(strres);


                    #endregion


                    //20210617234152 furukawa ed ////////////////////////


                    sw.WriteLine(sb.ToString());

                    prev_shinsei_basho = e.shinsei_basho;

                    wf.InvokeValue++;
                }

                if (flgNG)
                {
                    if(System.Windows.Forms.MessageBox.Show("エラーがありますが支払CSV作成しますか？","",
                        System.Windows.Forms.MessageBoxButtons.YesNo,
                        System.Windows.Forms.MessageBoxIcon.Question,
                        System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        sw.Close();
                        System.IO.File.Move(strFileName, strFileName.Replace(".csv","_エラーあり.csv"));
                    }
                    else
                    {
                        sw.Close();
                        System.IO.File.Delete(strFileName);
                    }

                    return false;
                }

                sw.Close();
                return true;
            }
            catch (Exception ex)
            {
                //tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                // cmd.Dispose();     
                
                
                if (sbErr.ToString().Length > 0)
                {
                    System.Windows.Forms.MessageBox.Show("エラーログを表示します");
                    CreateLog(strDir, sbErr.ToString());
                }
                
                // tran.Commit();
            }

        }
        //20210608073141 furukawa ed ////////////////////////




        //20210719163545 furukawa st ////////////////////////
        /// <summary>
        /// ログ出力        
        /// </summary>
        /// <param name="strFolder"></param>
        /// <param name="strErr"></param>
        private static void CreateLog(string strFolder,string strErr)
        {
            string strLogName = strFolder + "\\log.txt";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strLogName, false,System.Text.Encoding.GetEncoding("shift-jis")) ;
            try
            {
                sw.WriteLine("支払データエラーチェックログ");
                sw.WriteLine(strErr);
                
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                sw.Close();
                System.Diagnostics.Process.Start(strLogName);
            }
        }
        //20210719163545 furukawa ed ////////////////////////




        //20210608104918 furukawa st ////////////////////////
        //納品用画像コピー


        /// <summary>
        /// AIDで出力テーブルと紐付くApplicationレコードの画像名を取得し、それを続紙含め全てコピーする
        /// 各区ごとに、鍼灸１９、あんま１７の順で出力。ナンバリングが既にそうなっている
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool CopyImage(int cym, string strDir, WaitForm wf)
        {
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
           
            
            //AIDで出力テーブルと紐付くApplicationレコードの画像名を取得し、それを続紙含め全てコピーする
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" select a.aid,a.aimagefile,e.aid,e.shinsei_basho from application a ");
            sb.AppendLine(" inner join export e on ");
            sb.AppendLine(" a.aid=e.aid ");
            sb.AppendLine($" where a.cym={cym} ");
            sb.AppendLine(" order by e.exportid ;");


            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            var l = cmd.TryExecuteReaderList();

            wf.SetMax(l.Count);
            wf.InvokeValue = 0;


          

            try
            {
                foreach (var item in l)
                {

                    if (CommonTool.WaitFormCancelProcess(wf)) return false;

                    string strImageDir = $"{strDir}\\申請書画像データ\\";
                    switch (item[3].ToString())
                    {
                        //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
                        //場所	保険者番号
                        //1	00224014
                        //2	00224022
                        //3	00224030

                        case "1":
                            strImageDir += "葵区";
                            break;

                        case "2":
                            strImageDir += "駿河区";
                            break;

                        case "3":
                            strImageDir += "清水区";
                            break;

                        default:
                            break;
                    }


                    if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

                    App a = App.GetApp(int.Parse(item[0].ToString()));
                    string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());

                    if (!System.IO.File.Exists(strImageFileName)) continue;

                    string imageName = $"{strImageDir}\\{a.Aid}.tif";

                    wf.LogPrint($"画像出力中： AID:{item[0]} -> {imageName}");

                    fc.FileCopy(strImageFileName, $"{imageName}");
                    wf.InvokeValue++;
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }
        //20210608104918 furukawa ed ////////////////////////


        //未使用
        //20210608104918 furukawa st ////////////////////////
        //納品用画像コピー

        /// <summary>
        /// 画像コピー 各区ごとに、鍼灸１９、あんま１７の順で出力。ナンバリングが既にそうなっている
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        //private static bool CopyImage(List<Export> lst, int cym, string strDir, WaitForm wf)
        //{
        //    TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
        //    wf.SetMax(lst.Count);
        //    wf.InvokeValue = 0;

        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine(" select aid,aimagefile from application a ");
        //    sb.AppendLine(" inner join export e on ");
        //    sb.AppendLine(" a.aid=e.aid ");
        //    sb.AppendLine($" where a.cym={cym};");
        //    sb.AppendLine(" order by e.exportid ");


        //    DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
        //    var l = cmd.TryExecuteReaderList();
            
         
        //    try
        //    {
        //        for (int r = 0; r < lst.Count; r++)
        //        {
                    
        //            if (CommonTool.WaitFormCancelProcess(wf)) return false;
                               
        //            string strImageDir = $"{strDir}\\申請書画像データ\\";
        //            switch (lst[r].shinsei_basho)
        //            {
        //                //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
        //                //場所	保険者番号
        //                //1	00224014
        //                //2	00224022
        //                //3	00224030

        //                case "1":
        //                    strImageDir +="葵区";
        //                    break;

        //                case "2":
        //                    strImageDir += "駿河区";
        //                    break;

        //                case "3":
        //                    strImageDir += "清水区";
        //                    break;

        //                default:
        //                    break;
        //            }

                    
        //            if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

        //            App a = App.GetApp(lst[r].aid);
        //            string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());
        //            if (!System.IO.File.Exists(strImageFileName)) continue;

        //            string imageName = $"{strImageDir}\\{lst[r].numbering}.tif";

        //            wf.LogPrint($"画像出力中： AID:{lst[r].aid} -> {lst[r].numbering}.tif");

        //            fc.FileCopy(strImageFileName, $"{imageName}");
        //            wf.InvokeValue++;
        //        }
        //        return true;
        //    }
        //    catch(Exception ex)
        //    {
        //         System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
        //        return false;
        //    }
        //    finally
        //    {

        //    }
            
        //}
        //20210608104918 furukawa ed ////////////////////////


        /// <summary>
        /// DBからデータを取得してCSV出力まで
        /// <param name="cym">処理年月</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        //private static bool DataExport(int cym,string strDir,WaitForm wf)
        //{

        //    string strFileName = strDir + "\\";
        //    string strHeader = string.Empty;
        //    string strSQL = string.Empty;

        //    if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);

        //    wf.LogPrint("データ取得");

        //    strFileName += $"{cym}_療養費振込データ.csv";

        //    #region sql
        //    strSQL =
        //        "SELECT " +
        //          //"numbering," +
        //        "shinsei_basho || lpad(cast(row_number() over (partition by shinsei_basho " +

        //        //20200804142125 furukawa st ////////////////////////
        //        //idの付け方が間違ってた

        //        "order by shinsei_basho,cast(apptype as int) desc,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +
        //        //"order by shinsei_basho,apptype,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +
        //        //20200804142125 furukawa ed ////////////////////////

        //        "shoriym," +				//処理年月
        //        "shinsei_basho," +          //申請場所
        //        "apptype," +                //療養費の種類
        //        "ippantaishoku," +          //一般退職区分
        //        "hnum," +                   //被保険者証番号
        //        "atenanum," +               //宛名番号
        //        "mainname," +               //世帯主氏名
        //        "pname," +                  //療養を受けた者の氏名
        //        "psex," +                   //性別
        //        "pbirth," +                 //生年月日
        //        "startdate1," +             //施術年月日
        //        "shinseiymd," +             //申請受付年月日


        //        //20200706120217 furukawa st ////////////////////////
        //        //施術所、施術師は両方で18文字なので前から取る
        //        //施術所がない場合は施術師のみ
        //        "case when clinicname='' then drname " +
        //        "else " +
        //        "trim(substring(clinicname || '　' || drname,1,18)) end ," +   //施術機関名、施術師名
        //               //"clinicname || ' ' || drname," +   //施術機関名、施術師名

        //        //20200706120217 furukawa ed ////////////////////////


        //        "ratio," +                  //給付割合
        //        "total," +                  //合計金額（費用額）
        //        "charge," +                 //請求額
        //        "partial," +                //一部負担金
        //        "counteddays," +            //実日数
        //        "bcode," +                  //金融機関コード
        //        "bname," +                  //金融機関名
        //        "bbcode," +                 //支店コード
        //        "bbname," +                 //支店名
        //        "bacctype," +               //科目
        //        "baccnumber," +             //口座番号
        //        "baccname ";                //口座名義（ﾌﾘｶﾞﾅ）

        //    #endregion

        //    strSQL += $"FROM export where cym ='{cym}'";

        //    //20200706121703 furukawa st ////////////////////////
        //    //申請書だけ出す

        //    strSQL += $" and  ym>0 ";
        //    //20200706121703 furukawa ed ////////////////////////


        //    //ソート順
        //    //住所区（葵区→駿河区→清水区）=>ナンバリング
        //    //療養費の種類（はり・きゅう→あん摩・マッサージ）
        //    //一般退職区分（一般→退職本人→退職扶養）
        //    //被保険者証番号
        //    //療養を受けた者（受診者）
        //    //施術年月
        //    //施術所


        //    strSQL += $" order by numbering";
        //    //strSQL += $" order by  numbering, cast(apptype as int) desc,ippantaishoku,hnum,pname,startdate1,clinicname";



        //    DB.Command cmd = new DB.Command(DB.Main, strSQL);                      
        //    var l = cmd.TryExecuteReaderList();



        //    wf.LogPrint("ファイル作成中");
        //    wf.InvokeValue = 0;


        //    wf.SetMax(l.Count);

        //    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName,false,System.Text.Encoding.GetEncoding("shift_jis"));

        //    try
        //    {
        //            foreach (var e in l)
        //            {
        //                string strres = string.Empty;
        //                for (int colcnt = 0; colcnt < e.Length; colcnt++)
        //                {
        //                    strres += $"{e[colcnt]},";
        //                }
        //                strres = strres.Substring(0, strres.Length - 1);

        //                sw.WriteLine(strres);

        //                wf.InvokeValue++;
        //            }


        //            return true;
        //    }
        //    catch(Exception ex)
        //    {
        //        //tran.Rollback();
        //        System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //       // cmd.Dispose();
        //        sw.Close();
        //       // tran.Commit();
        //    }

        //}



    }

    /// <summary>
    /// 既出テーブルだが使わないかも
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
