﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Shizuokashi_Ahaki
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);


        //20200803124923 furukawa st ////////////////////////
        //申請受付日の座標を右下にする
        
        /// <summary>
        /// 申請日
        /// </summary>
        /// 
        Point posShinsei = new Point(1800, 2060);
        //Point posShinsei = new Point(1000, 1000);
        //20200803124923 furukawa ed ////////////////////////


        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, personConts, totalConts, buiDateConts, kohiConts, hihoNameConts, shinseiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 口座情報
        /// </summary>
        List<accountinfo> lstAcc = new List<accountinfo>();


        /// <summary>
        /// 被保険者・受療者情報
        /// </summary>
        List<APP_HihoPersonInfo> lstHihoPerson = new List<APP_HihoPersonInfo>();

        /// <summary>
        /// 施術所情報
        /// </summary>
        List<APP_ClinicInfo> lstClinic = new List<APP_ClinicInfo>();
        

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, };


            //受療者性別、生年月日
            personConts = new Control[] { verifyBoxRatio, verifyBoxSex, pBirthday, };// verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD };



            //20200803124512 furukawa st ////////////////////////
            //施術開始日、実日数の座標を合計金額と同じにする
            
            //合計、請求、往料、交付
            totalConts = new Control[] { verifyBoxTotal, verifyBoxCharge,cbZenkai,verifyBoxZG,verifyBoxZM,verifyBoxZY,checkBoxVisit,checkBoxVisitKasan,
                verifyBoxDays,verifyBoxF1Start};

            //初検日年号
            buiDateConts = new Control[] { verifyBoxF1FirstE,verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD,
                verifyBoxF1,//verifyBoxDays,verifyBoxF1Start,
            };

            //20200803124512 furukawa ed ////////////////////////



            //被保険者名、施術所、口座
            hihoNameConts = new Control[] { verifyBoxHihoname, verifyBoxDrCode,verifyBoxDrName,verifyBoxAccountNum,pDoui,};

            //申請日
            shinseiConts = new Control[] { pShinsei, };

            //公費
            kohiConts = new Control[] { };// verifyBoxKONum1, verifyBoxKOJUNum1, verifyBoxKOJUNum2, verifyBoxKONum2, };


            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            panelTotal.Visible = false;
            panelHnum.Visible = false;
            
            #endregion


            //口座情報取得
            if (lstAcc.Count == 0) lstAcc = accountinfo.SelectAll();


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                setApp(app);
                //20200804183217 furukawa st ////////////////////////
                //被保険者・受療者情報取得
                
                lstHihoPerson = APP_HihoPersonInfo.GetHihoPersonInfo();
                //20200804183217 furukawa ed ////////////////////////                                                       
                
                //20200804183111 furukawa st ////////////////////////                
                //施術所情報取得
                lstClinic = APP_ClinicInfo.CreateClinicInfoList();
                //20200804183111 furukawa ed ////////////////////////

            }
            focusBack(false);


          


        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        //20200803183348 furukawa st ////////////////////////
        //コントロール不可視（初期化）する
        
        /// <summary>
        /// コントロール不可視（初期化）
        /// </summary>
        private void UnVisibleControls()
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

         
            foreach (Control c in panelRight.Controls)
            {
                if ((c.GetType()==typeof(Panel) || c.GetType() == typeof(VerifyBox)) && !ignoreControls.Contains(c))
                {
                    c.Visible = false;
                }
            }
           
        }

        //20200803183348 furukawa ed ////////////////////////



        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo1, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            //20200803183508 furukawa st ////////////////////////
            //用紙の種類によって見えるコントロールを制御
            
            UnVisibleControls();

            
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                case clsInputKind.不要:
                case clsInputKind.続紙:
                case clsInputKind.長期:
                case clsInputKind.施術同意書裏:
                case clsInputKind.施術報告書:
                case clsInputKind.状態記入書:
                    
                    break;

                case clsInputKind.施術同意書:
                    
                    pDoui.Visible = true;
                    break;

                default:
                    panelHnum.Visible = true;
                    panelTotal.Visible = true;
                    pDoui.Visible = true;
                    break;

            }

            #region old
            /*
            panelHnum.Visible = false;
            panelTotal.Visible = false;

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }


            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
                panelHnum.Visible = true;
                panelTotal.Visible = true;

            }
            else
            {
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            */
            #endregion



            //20200803183508 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


     
        /// <summary>
        /// 口座情報グリッド
        /// </summary>
        private void createAccGrid(string accid="")
        {

            List<accountinfo> lstai = new List<accountinfo>();

            string straccnum = verifyBoxAccountNum.Text.Trim();

            foreach (accountinfo item in lstAcc)
            {
                if (item.accnumber == straccnum)
                {
                    lstai.Add(item);
                }
            }
            dgvbk.DataSource = null;
            dgvbk.DataSource = lstai;

            #region グリッド初期化
            dgvbk.Columns[nameof(accountinfo.accid)].Width = 40;
            dgvbk.Columns[nameof(accountinfo.bankcd)].Width = 40;
            dgvbk.Columns[nameof(accountinfo.bankname)].Width = 100;
            dgvbk.Columns[nameof(accountinfo.branchcd)].Width = 40;
            dgvbk.Columns[nameof(accountinfo.branchname)].Width = 100;
            dgvbk.Columns[nameof(accountinfo.acckindcd)].Width = 10;
            dgvbk.Columns[nameof(accountinfo.acckind)].Width = 40;
            dgvbk.Columns[nameof(accountinfo.accnumber)].Width = 100;

            //20210531144909 furukawa st ////////////////////////
            //一目で見えるように口座名義幅修正            
            dgvbk.Columns[nameof(accountinfo.accnamekana)].Width = 250;
            //dgvbk.Columns[nameof(accountinfo.accnamekana)].Width = 100;
            //20210531144909 furukawa ed ////////////////////////


            dgvbk.Columns[nameof(accountinfo.flgenable)].Width = 30;
            //20210601111633 furukawa st ////////////////////////
            //一目で見えるように幅修正
            
            dgvbk.Columns[nameof(accountinfo.dateadd)].Width = 70;
            dgvbk.Columns[nameof(accountinfo.datedel)].Width = 70;

            //dgvbk.Columns[nameof(accountinfo.dateadd)].Width = 60;
            //dgvbk.Columns[nameof(accountinfo.datedel)].Width = 60;
            //20210601111633 furukawa ed ////////////////////////


            dgvbk.Columns[nameof(accountinfo.accid)].HeaderText = "ID";
            dgvbk.Columns[nameof(accountinfo.bankcd)].HeaderText = "銀行CD";
            dgvbk.Columns[nameof(accountinfo.bankname)].HeaderText = "銀行名";
            dgvbk.Columns[nameof(accountinfo.branchcd)].HeaderText = "支店CD";
            dgvbk.Columns[nameof(accountinfo.branchname)].HeaderText = "支店名";
            dgvbk.Columns[nameof(accountinfo.acckindcd)].HeaderText = "科目CD";
            dgvbk.Columns[nameof(accountinfo.acckind)].HeaderText = "科目名";
            dgvbk.Columns[nameof(accountinfo.accnumber)].HeaderText = "口座番号";
            dgvbk.Columns[nameof(accountinfo.accnamekana)].HeaderText = "口座名義ｶﾅ";
            dgvbk.Columns[nameof(accountinfo.flgenable)].HeaderText = "有効";
            dgvbk.Columns[nameof(accountinfo.dateadd)].HeaderText = "追加日時";
            dgvbk.Columns[nameof(accountinfo.datedel)].HeaderText = "削除日時";

            #endregion

            //口座ID（口座番号ではない）で検索し該当のレコードを選択する
            if (accid != string.Empty)
            {
                foreach (DataGridViewRow r in dgvbk.Rows)
                {

                    //20210601111804 furukawa st ////////////////////////
                    //口座IDが合致したらそれ以上は見ない。合致しない場合は選択を外す
                    
                    if (r.Cells["accID"].Value.ToString() == accid)
                    {
                        r.Selected = true;
                        break;
                    }
                    else
                    {
                        r.Selected = false;
                    }

                    //      r.Selected = true;
                    //      break;
                    //20210601111804 furukawa ed ////////////////////////
                }
            }



            if (lstai == null || lstai.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            //20210601111935 furukawa st ////////////////////////
            //1行選択できていたらOKと見なす
            
            else if (lstai.Count == 1 || dgvbk.SelectedRows.Count ==1)
            //      else if (lstai.Count == 1)
            //20210601111935 furukawa ed ////////////////////////
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        #endregion



        #region 各種ロード

        /// <summary>
        /// 受療者情報ロード
        /// </summary>
        /// <param name="strhnum"></param>
        /// <returns></returns>
        private string getHihoName(string strhnum)
        {          
            foreach (APP_HihoPersonInfo h in lstHihoPerson)
            {
                if (h.hihoNum == strhnum) return h.hihoName;
            }
            return string.Empty;
        }


        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            panelHnum.Visible = false;
            panelTotal.Visible = false;            
            pZenkai.Enabled = false;

            //20200805132844 furukawa st ////////////////////////
            //グリッド初期化            
            dgvbk.DataSource = null;
            //20200805132844 furukawa ed ////////////////////////


            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                //setVerify(app);
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //setInputedApp(app);
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }

            //20200520155122 furukawa st ////////////////////////
            //ヘッダのgeneralstring1を取得し、その申請書に紐付ける
            
            //ヘッダの情報を取得
            //このaid以下で、同じグループで、ayear=-7(バッチ、ヘッダの意味）
            string strsql = $"where aid <={app.Aid} and groupid = { app.GroupID } and ayear = -7";
            List<App> appheader = App.GetAppsWithWhere(strsql);
            if (appheader.Count == 1) strNumbering = appheader[0].TaggedDatas.GeneralString1;
            //20200520155122 furukawa ed ////////////////////////

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    break;

                default:


                    #region old
                    //        if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
                    //{
                    //    //setvalueしないとステータスが更新されない
                    //    setValue(verifyBoxY, "--", firstTime, nv);
                    //}
                    //else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
                    //{
                    //    //setvalueしないとステータスが更新されない                
                    //    setValue(verifyBoxY, "++", firstTime, nv);
                    //}
                    //else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
                    //{

                    //    setValue(verifyBoxY,"**", firstTime, nv);

                    //}
                    //else
                    //{
                    #endregion



                    //申請書



                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証記号番号
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);

                    //被保険者名
                    setValue(verifyBoxHihoname, app.HihoName, firstTime, nv);
                    //受療者名
                    setValue(verifyBoxPname, app.PersonName, firstTime, nv);

                    //性別
                    setValue(verifyBoxSex, app.Sex.ToString(), firstTime, nv);

                    //生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
              
                    //家族区分いる→scan.note1から取得したので扱わない
                

                    //給付割合
                    setValue(verifyBoxRatio, app.Ratio.ToString(), firstTime, nv);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, nv);

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                    //施術開始日1
                    if (!app.FushoStartDate1.IsNullDate())
                        setValue(verifyBoxF1Start, app.FushoStartDate1.Day.ToString(), firstTime, nv);
                   

                    //20200813092317 furukawa st ////////////////////////
                    //入力ミスが多いので入力リズム調整のために追加2020/08/12伸作さんより
                    
                    //施術終了日1
                    if (!app.FushoFinishDate1.IsNullDate())
                        setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day.ToString(), firstTime, nv);
                    //20200813092317 furukawa ed ////////////////////////


                    //負傷          
                    setValue(verifyBoxF1, app.FushoName1, firstTime, nv);

                    //新規継続 コントロールなし

                    //往療有無
                    setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);

                    //加算有無
                    setValue(checkBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);

                    //施術師コード
                    setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);

                    //医療機関名
                    setValue(verifyBoxHosName, app.ClinicName, firstTime, nv);

                    //施術師名
                    setValue(verifyBoxDrName, app.DrName, firstTime, nv);

                    //金融機関名         　コントロールなし
                    //支店名             コントロールなし
                    //科目口座名義ｶﾅ     コントロールなし

                    //口座番号
                    setValue(verifyBoxAccountNum, app.AccountNumber, firstTime, nv);

                    //グリッド選択する
                    //20210601112421 furukawa st ////////////////////////
                    //口座番号検索は複数になるので、accidで検索する
                    
                    createAccGrid(app.TaggedDatas.GeneralString5.ToString());
                    //      createAccGrid(verifyBoxAccountNum.Text.ToString());
                    //20210601112421 furukawa ed ////////////////////////

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);
               
                    //請求金額　合計金額×給付割合　1円未満切り捨て
                    setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //施術日数
                    setValue(verifyBoxDays, app.CountedDays.ToString(), firstTime, nv);

                    //ナンバリング　申請区番号＋連番6桁ゼロ埋め　　コントロールなし
                    //申請書種別　コントロールなし

                    //GeneralString1：申請受付日 
                    setDateValue(DateTime.Parse(app.TaggedDatas.GeneralString1),firstTime,nv, verifyBoxSY, verifyBoxSM, verifyBoxSG, verifyBoxSD);


                    //GeneralString2:金融機関コード　　コントロールなし
                    //GeneralString3：支店コード 　　コントロールなし
                    //GeneralString4：一般退職区分　　コントロールなし


                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate,  firstTime, nv,  vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    //20200803133306 furukawa st ////////////////////////
                    //交付料有無フラグは前回支給年月とは無関係にする
                
                    //flgKofuUmu 交付料あり
                    setValue(cbZenkai, app.TaggedDatas.flgKofuUmu, firstTime, nv);
                    //20200803133306 furukawa ed ////////////////////////



                    //PastSupplyYM:前回支給

                    if (app.TaggedDatas.PastSupplyYM != null && app.TaggedDatas.PastSupplyYM != string.Empty)
                    {
                        //20200803133207 furukawa st ////////////////////////
                        //交付料有無フラグは前回支給年月とは無関係にする
                        //cbZenkai.Checked = true;
                        //20200803133207 furukawa ed ////////////////////////

                        DateTime tmpps = DateTimeEx.ToDateTime6(app.TaggedDatas.PastSupplyYM);
                        setDateValue(tmpps, firstTime, nv, verifyBoxZY, verifyBoxZM, verifyBoxZG);
                    }

                    break;
            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;


            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region old
                    //        //申請書以外

                    //        if (verifyBoxY.Text == "**"  )
                    //{
                    //    #region Appへの反映

                    //    //施術年
                    //    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;

                    //    //申請書タイプをバッチにする                
                    //    app.AppType = APP_TYPE.バッチ;


                    //    #endregion                
                    //}
                    //else if (verifyBoxY.Text == "--")
                    //{
                    //    #region Appへの反映

                    //    //施術年
                    //    app.MediYear = (int)APP_SPECIAL_CODE.続紙;

                    //    //申請書タイプ
                    //    app.AppType = APP_TYPE.続紙;


                    //    #endregion
                    //}
                    //else if (verifyBoxY.Text == "++")
                    //{
                    //    #region Appへの反映

                    //    //施術年
                    //    app.MediYear = (int)APP_SPECIAL_CODE.不要;

                    //    //申請書タイプ          
                    //    app.AppType = APP_TYPE.不要;


                    //    #endregion
                    //}
                    ////申請書
                    //else
                    //{

                    #endregion


                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証記号番号
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN.Length<6);

                    //被保険者名
                    string strHihoName = verifyBoxHihoname.Text.Trim();
                    setStatus(verifyBoxHihoname, strHihoName.Length < 2);

                    //受療者名
                    string strPname = verifyBoxPname.Text.Trim();
                    setStatus(verifyBoxPname, strPname.Length < 2);

                    //性別
                    int sex = verifyBoxSex.GetIntValue();
                    setStatus(verifyBoxSex, sex < 1 || 2 < sex);

                    //生年月日
                    var birth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //家族区分→note1から appに直接入れる               
                
                    //給付割合
                    int ratio = verifyBoxRatio.GetIntValue();
                    setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

                    //負傷名1 チェックなし

                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                    //施術開始日1
                    DateTime f1Start = DateTimeEx.DateTimeNull;
                    f1Start = DateTimeEx.IsDate(adYear, month, verifyBoxF1Start.GetIntValue()) ?
                        new DateTime(adYear, month, verifyBoxF1Start.GetIntValue()) :
                        DateTimeEx.DateTimeNull;
                    setStatus(verifyBoxF1Start, f1Start.IsNullDate());


                    //20200813092538 furukawa st ////////////////////////
                    //入力リズム調整のためのダミーだが、入力する以上登録する
                    
                    //施術終了日1
                    DateTime f1Finish = DateTimeEx.DateTimeNull;
                    f1Finish = DateTimeEx.IsDate(adYear, month, verifyBoxF1Finish.GetIntValue()) ?
                        new DateTime(adYear, month, verifyBoxF1Finish.GetIntValue()) :
                        DateTimeEx.DateTimeNull;
                    setStatus(verifyBoxF1Finish, f1Finish.IsNullDate());
                    //20200813092538 furukawa ed ////////////////////////


                    //柔整師登録記号番号
                    string strDrCode = verifyBoxDrCode.Text.Trim();
                    setStatus(verifyBoxDrCode, strDrCode != string.Empty && strDrCode.Length<7);

                    //医療機関名
                    string strClinicName = verifyBoxHosName.Text.Trim();

                    //施術師名
                    string strDrName = verifyBoxDrName.Text.Trim();



                    //金融機関名 チェックなし
                    //支店名 チェックなし
                    //科目口座名義ｶﾅ　チェックなし
                

                    //口座番号
                    string strAccNum = verifyBoxAccountNum.Text.Trim();
                    setStatus(verifyBoxAccountNum,strAccNum!=string.Empty && strAccNum.Length != 7);


                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);
                
                    //請求金額　合計金額×給付割合　1円未満切り捨て
                    int seikyu = verifyBoxCharge.GetIntValue();
                    setStatus(verifyBoxCharge, seikyu < 100 || seikyu > 200000);

                    //施術日数
                    int days = verifyBoxDays.GetIntValue();
                    setStatus(verifyBoxDays, days < 1 || 31 < days);

                    //ナンバリング 申請区番号＋連番6桁ゼロ埋め

                    //申請書種別 チェックなし

                    //GeneralString1：申請受付日 

                    //20200803131909 furukawa st ////////////////////////
                    //申請受付日空欄許可フラグTrue
                
                    DateTime dtShinseibi = dateCheck(verifyBoxSG, verifyBoxSY, verifyBoxSM, verifyBoxSD, true);
                            //DateTime dtShinseibi = dateCheck(verifyBoxSG, verifyBoxSY, verifyBoxSM, verifyBoxSD);
                    //20200803131909 furukawa ed ////////////////////////


                    //GeneralString2:金融機関コード チェックなし
                    //GeneralString3：支店コード チェックなし
                    //GeneralString4：一般退職区分 チェックなし


                    //DouiDate:同意年月日               

                    //20200803132056 furukawa st ////////////////////////
                    //同意年月日空欄許可フラグTrue

                    DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD, true);
                            //DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);
                    //20200803132056 furukawa ed ////////////////////////



                    //PastSupplyYM:前回支給

                    //20200803132132 furukawa st ////////////////////////
                    //前回支給空欄許可フラグTrue


                    DateTime dtPastSupply = dateCheck(verifyBoxZG, verifyBoxZY, verifyBoxZM, true);
                            //DateTime dtPastSupply = dateCheck(verifyBoxZG, verifyBoxZY, verifyBoxZM);
                    //20200803132132 furukawa ed ////////////////////////

                    //本家区分
                    //int family = verifyBoxFamily.GetIntValue();
                    //setStatus(verifyBoxFamily, !new[] { 2, 4, 6, 8, 0 }.Contains(family));



                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }



                    //20210303142748 furukawa st ////////////////////////
                    //合計・請求・給付割合の整合性チェック
                    
                    bool ratioError = !CommonTool.CheckChargeFromRatio(total, ratio, seikyu);

                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        verifyBoxRatio.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    //20210303142748 furukawa ed ////////////////////////


                    //チェックロジックをCommonToolに統一するためあえて使ってない
                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/


                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月
                    app.HihoNum = hnumN;                 //被保険者証記号番号

                    //app.Family = family;                //本家区分 →ins_numberでnote1から取得

                    app.Ratio = ratio;                  //給付割合
                    app.Sex = sex;                      //受療者性別
                    app.Birthday = birth;               //受療者生年月日     
                    app.CountedDays = days;             //施術日数

                    app.Total = total;                  //合計金額

                    //請求金額
                    app.Charge = seikyu;

                    //申請書種別
                    app.AppType = scan.AppType;

                    //負傷                
                    app.FushoFirstDate1 = f1FirstDt;    //初検日
                    app.FushoStartDate1 = f1Start;      //開始日


                    //20200813092715 furukawa st ////////////////////////
                    //入力リズム調整のためのダミーだが、入力する以上登録する                    
                    app.FushoFinishDate1 = f1Finish;      //終了日
                    //20200813092715 furukawa ed ////////////////////////


                    //被保険者名
                    app.HihoName = strHihoName;
                
                    //受療者名
                    app.PersonName = strPname;

                    //家族区分 →ins_numberでnote1から取得

                    //負傷名1
                    app.FushoName1 = verifyBoxF1.Text.Trim();

                    //新規継続
                    app.NewContType = f1FirstDt.Year == f1Start.Year && f1FirstDt.Month == f1Start.Month ? NEW_CONT.新規 : NEW_CONT.継続;
            
                    //往療有無
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;

                    //加算有無
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;

                    //施術師コード
                    app.DrNum = strDrCode;
                
                    //医療機関名
                    app.ClinicName = strClinicName;
                
                    //施術師名
                    app.DrName = strDrName;

                    //口座番号
                    app.AccountNumber = strAccNum;

                    //合計
                    app.Total = total;

                    //請求
                    int tmpcharge = app.Total * (app.Ratio / 10);
                    decimal tmpdeccharge = Math.Truncate(decimal.Parse(tmpcharge.ToString()));
                    if (seikyu == 0) app.Charge = int.Parse(tmpdeccharge.ToString());
                    else app.Charge = seikyu;

                    //一部負担金 合計ｰ請求
                    app.Partial = total - seikyu;


                    //施術日数
                    app.CountedDays = days;

                    //numbering 申請区番号＋連番6桁ゼロ埋め→出力テーブル登録時につける

               
                    //GeneralString1：申請受付日 
                    app.TaggedDatas.GeneralString1 = dtShinseibi.ToString("yyyy-MM-dd");

                    
                    
                    if (dgvbk.Rows.Count > 0)
                    {
                        //GeneralString2:金融機関コード
                        app.TaggedDatas.GeneralString2 = dgvbk.CurrentRow.Cells["bankcd"].Value.ToString();

                        //GeneralString3：支店コード
                        app.TaggedDatas.GeneralString3 = dgvbk.CurrentRow.Cells["branchcd"].Value.ToString();

                     
                        //金融機関名
                        app.BankName = dgvbk.CurrentRow.Cells["bankname"].Value.ToString();
                        //支店名
                        app.BankBranch = dgvbk.CurrentRow.Cells["branchname"].Value.ToString();
                        //科目
                        app.AccountType = int.Parse(dgvbk.CurrentRow.Cells["acckindcd"].Value.ToString());
                        //口座名義ｶﾅ
                        app.AccountKana = dgvbk.CurrentRow.Cells["accnamekana"].Value.ToString();

                        //20210601112109 furukawa st ////////////////////////
                        //口座番号のレコードは使用フラグで複数レコードになるので、accidを登録し、以後これで検索する

                        app.TaggedDatas.GeneralString5 = dgvbk.CurrentRow.Cells["accid"].Value.ToString();
                        //20210601112109 furukawa ed ////////////////////////


                    }
                    


                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDoui;

                    //20200803134523 furukawa st ////////////////////////
                    //交付料有無フラグは前回支給年月とは無関係にするので独立して更新          
                
                    //交付料有無
                    app.TaggedDatas.flgKofuUmu = cbZenkai.Checked;
                    //20200803134523 furukawa ed ////////////////////////


                    //PastSupplyYM:前回支給
                    if (cbZenkai.Checked) app.TaggedDatas.PastSupplyYM = dtPastSupply.ToString("yyyyMM");
                    else app.TaggedDatas.PastSupplyYM = string.Empty;

                    #endregion

                    break;
            }

            //GeneralString4：一般退職区分→ins_numberでnote1から取得
            ins_number(app);



            return true;
        }

        /// <summary>
        /// 申請場所と家族区分をフォルダ名（Note1）から判別し設定
        /// </summary>
        /// <param name="app"></param>
        private void ins_number(App app)
        {
            string strInsNumber = string.Empty;
            //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
            if (scan.Note1.Contains("葵区")) strInsNumber = "1";
            if (scan.Note1.Contains("駿河区")) strInsNumber = "2";
            if (scan.Note1.Contains("清水区")) strInsNumber = "3";

            app.InsNum = strInsNumber;

            string strIT = string.Empty;//一般退職
            string strFam = string.Empty;//家族区分

            if (scan.Note1.Contains("一般"))
            {
                strIT = "1";
                strFam = "2";
            }
            if (scan.Note1.Contains("退職本人"))
            {
                strIT = "2";
                strFam = "2";
            }
            if (scan.Note1.Contains("退職家族") || scan.Note1.Contains("退職扶養"))
            {
                strIT = "3";
                strFam = "6";
            }
            

            app.TaggedDatas.GeneralString4 = strIT;
            app.Family = int.Parse(strFam);

        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            /*
            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
             
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                
            }
            else if (verifyBoxY.Text == "**")
            {
                //ヘッダ（バッチ扱いとする）
                resetInputData(app);

                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                app.AppType = APP_TYPE.バッチ;

                //if (!checkApp(app))
                //{
                //    focusBack(true);
                //    return false;
                //}
            }
            else
            {
                //if (!checkApp(app))
                //{
                //    focusBack(true);
                //    return false;
                //}
            }
            */


            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }


            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211101103039 furukawa st ////////////////////////
                //auxに口座番号を入れておく
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString(), "", app.AccountNumber)) return false;

                //      20211012101218 furukawa AUXにApptype登録
                //      if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;
                //20211101103039 furukawa ed ////////////////////////

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

        //20200803125148 furukawa st ////////////////////////
        //Ctrl+Enterで被保険者名と同じ値を受療者に入れる
        
        private void verifyBoxPname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー
            {
                if (verifyBoxHihoname.Text.Trim() != string.Empty)
                {
                    verifyBoxPname.Text = verifyBoxHihoname.Text.Trim();
                    SelectNextControl(verifyBoxPname, true, true, false, false);
                    return;
                }

            }
        }
        //20200803125148 furukawa ed ////////////////////////

        //20200804183355 furukawa st ////////////////////////
        //被保険者番号で被保険者情報ロード
        
        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
         
            //20201109191912 furukawa st ////////////////////////
            //被保番入力で個人情報取得の方法を変更


            var app = (App)bsApp.Current;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //現在のAppが1回目入力の場合のみ自動入力

            //if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return;

            //20201203103806 furukawa st ////////////////////////
            //2回目で入力済みの場合は手入力
            if (!firstTime && app.StatusFlagCheck(StatusFlag.入力済)) return;
            //20201203103806 furukawa ed ////////////////////////

            PastData.PersonalData data = new PastData.PersonalData();
            data = PastData.PersonalData.SelectRecord($"hnum='{verifyBoxHnum.Text.Trim()}'");
            if ((data == null) || (verifyBoxHnum.Text.Trim() == string.Empty))
            {
                //抽出がない場合はクリア
                setValue(verifyBoxPname, string.Empty, firstTime, nv);
                setValue(verifyBoxSex, string.Empty, firstTime, nv);
                setValue(verifyBoxHihoname, string.Empty, firstTime, nv);
                setValue(verifyBoxBirthY, string.Empty, firstTime, nv);
                setValue(verifyBoxBirthM, string.Empty, firstTime, nv);
                setValue(verifyBoxBirthD, string.Empty, firstTime, nv);
                setValue(verifyBoxBirthE, string.Empty, firstTime, nv);
            }
            else
            {
                setValue(verifyBoxPname, data.pname, firstTime, nv);
                setValue(verifyBoxSex, data.psex.ToString(), firstTime, nv);
                setDateValue(data.pbirthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                setValue(verifyBoxHihoname, data.hname, firstTime, nv);
            }

                //下のforeachと同じ処理なので削除
                //foreach (DataRow h in PastData.PersonalData.dtPersonal.Rows)
                ////ListからDataTableに変更
                ////foreach (PastData.PersonalData h in PastData.PersonalData.lstPersonalData)
                //{
                //    if (h[0].ToString() == verifyBoxHnum.Text.Trim())
                //    {
                //        verifyBoxHihoname.Text = h[1].ToString();
                //        verifyBoxPname.Text = h[2].ToString();
                //        verifyBoxSex.Text = h[3].ToString();
                //        setDateValue(DateTime.Parse(h[4].ToString()), firstTime, false, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                //        break;

                //        //ListからDataTableに変更                
                //        //if (h.hnum == verifyBoxHnum.Text.Trim())
                //        //{
                //        //    verifyBoxHihoname.Text = h.hname;
                //        //    verifyBoxPname.Text = h.pname;
                //        //    verifyBoxSex.Text = h.psex.ToString();
                //        //    setDateValue(h.pbirthday, firstTime, false, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                //        //    break;
                //        //}
                //    }
                //}

                //foreach (APP_HihoPersonInfo h in lstHihoPerson)
                //{
                //    if (h.hihoNum == verifyBoxHnum.Text.Trim())
                //    {
                //        verifyBoxHihoname.Text = h.hihoName;
                //        verifyBoxPname.Text = h.pname;

                //        //20200806194426 furukawa st ////////////////////////
                //        //被保険者番号で、AID最新のをロードするので1回目で間違っている場合はそれが出て都合が悪いので削除

                //        //verifyBoxSex.Text = h.psex.ToString();                    
                //        //setDateValue(h.pbirthday, firstTime, false, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                //        //20200806194426 furukawa ed ////////////////////////


                //        break;
                //    }
                //}
                            
            //20201109191912 furukawa ed ////////////////////////

           
        }
        //20200804183355 furukawa ed ////////////////////////




        //20200804183428 furukawa st ////////////////////////
        //柔整師登録記号番号で柔整師情報ロード

        private void verifyBoxDrCode_Validated(object sender, EventArgs e)
        {
            
            foreach (APP_ClinicInfo c in lstClinic)
            {
                if (c.strDrNum == verifyBoxDrCode.Text.Trim())
                {
                    verifyBoxDrName.Text = c.strDrName;
                    verifyBoxHosName.Text = c.strClinicName;
                    break;
                }
            }
        }

        private void btnAccMaintenance_Click(object sender, EventArgs e)
        {
            BankMaintenanceGrid frmBankMaintenance = new BankMaintenanceGrid(verifyBoxAccountNum.Text.Trim());
            frmBankMaintenance.ShowDialog();
            lstAcc = accountinfo.SelectAll();
            App app = (App)bsApp.Current;
            createAccGrid(app.TaggedDatas.GeneralString5.ToString());
            //createAccGrid(verifyBoxAccountNum.Text.ToString());

        }

        //20200804183428 furukawa ed ////////////////////////


        private void verifyBoxAccountNum_Validated(object sender, EventArgs e)
        {
            createAccGrid();
        }

     

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;

            //合計金額の位置が柔整あはきで異なるため分岐
            else if ((scan.AppType == APP_TYPE.柔整) && (totalConts.Contains(sender))) p = posTotal;
            else if ((scan.AppType != APP_TYPE.柔整) && (totalConts.Contains(sender))) p = posTotalAHK;

            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            //else if (kohiConts.Contains(sender)) p = posKohi;
            //else if (headerConts.Contains(sender)) p = posHeader;
            else if (hihoNameConts.Contains(sender)) p = posHname;
            else if (shinseiConts.Contains(sender)) p = posShinsei;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (hihoNameConts.Any(c => c.Focused)) posHname = pos;
            else if (shinseiConts.Any(c => c.Focused)) posShinsei = pos;
        }
        #endregion


    
       
    }      
}
