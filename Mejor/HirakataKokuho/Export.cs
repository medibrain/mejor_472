﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HirakataKokuho
{
    class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var rex = new string[12];
                    //先頭行は見出し
                    rex[0] = "No.";
                    rex[1] = "記号番号";
                    rex[2] = "被保険者名";
                    rex[3] = "受療者名";
                    rex[4] = "診療年";
                    rex[5] = "診療月";
                    rex[6] = "実日数";
                    rex[7] = "窓口支払額";
                    rex[8] = "施術所名";
                    rex[9] = "柔整師名";
                    rex[10] = "郵便番号";
                    rex[11] = "住所";

                    sw.WriteLine(string.Join(",", rex));

                    var ss = new List<string>();
                    var count = 1;
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        ss.Add(count.ToString());
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Partial.ToString());
                        ss.Add(item.ClinicName);
                        ss.Add(item.DrName);
                        ss.Add(item.HihoZip.PadLeft(7, '0').Insert(3, "-"));
                        ss.Add(item.HihoAdd);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
