﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class ExportMenuForm : Form
    {
        private Insurer ins;
        int cym;

        public ExportMenuForm(Insurer ins, int cym)
        {
            InitializeComponent();
            this.ins = ins;
            this.cym = cym;

            //ボタン操作
            if (ins.InsurerType == INSURER_TYPE.学校共済)
            {
                button2.Enabled = true;
                button2.Text = "提出データ 事前チェック";
                button3.Enabled = true;
                button3.Text = "金額確認";
                button4.Enabled = true;
                button4.Text = "登録済み施術所(師)一覧";
            }
            else if (ins.EnumInsID == InsurerID.CHUO_RADIO)
            {
                button2.Enabled = true;
                button2.Text = "並び替え印刷";
            }
            else if (ins.EnumInsID == InsurerID.SHIZUOKA_KOIKI)
            {
                button2.Enabled = true;
                button2.Text = "1/10枚 画像抽出";
            }
            else if (ins.EnumInsID == InsurerID.TOYONAKASHI_COVID19)    //20220119162611 furukawa st //豊中市コロナワクチン接種追加 
            {
                button2.Enabled = true;
                button2.Text = "出力前チェック";
            }
            else if (ins.EnumInsID == InsurerID.TOYONAKASHI_COVID19_SHIGAI) //20220205161550 furukawa st //豊中市コロナワクチン接種市外追加
            {
                button2.Enabled = true;
                button2.Text = "出力前チェック";
            }
            else if (ins.EnumInsID == InsurerID.HYOGO_KOIKI2022)    //20221212 ito //兵庫広域追加
            {
                button2.Enabled = true;
                button2.Text = "MejorViewerあはきのみ";
                button3.Enabled = true;
                button3.Text = "Tiff透かし追加ツール";
            }

            //ボタン5表示
            if (ins.EnumInsID == InsurerID.HIROSHIMA_KOIKI || ins.EnumInsID == InsurerID.OSAKA_KOIKI)
            {
                button5.Enabled = true;
                button5.Text = "状態記入書画像 一括抽出";
            }

        }
        
        /// <summary>
        /// 出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSubmitExport_Click(object sender, EventArgs e)
        {
            int iID = ins.InsurerID;

            if (ins.InsurerType == INSURER_TYPE.学校共済)
            {
                using (var f = new ZenkokuGakko.ExportForm(ins, cym)) f.ShowDialog();
                return;
            }

            //20200419115049 furukawa st //宮城県柔整追加

            //20200423121227 furukawa st ////////////////////////
            //宮城県国保はデータ出力ボタンを使わないことにした
            //else if (iID == (int)InsurerID.MIYAGI_KOKUHO)
            //{
            //    MiyagiKokuho.Export.ExportData(cym);
            //    return;
            //}
            //20200423121227 furukawa ed ////////////////////////

            //20200419115049 furukawa ed ////////////////////////

            else if (iID == (int)InsurerID.KYOKAIKENPO) //20210204104159 furukawa st //協会けんぽ追加
            {
                kyokaikenpo.exportdata.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.SAPPORO_KOKUHO)
            {
                using (var f = new Sapporoshi.ExportForm(cym)) f.ShowDialog();
                return;
            }
            else if (iID == (int)InsurerID.CHIKIIRYO)
            {
                ChikiIryo.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.MITSUBISHI_SEISHI)
            {
                MitsubishiSeishi.Export.DoExport(cym);
                return;
            }

            //20190716142932 furukawa st //島根広域追加            
            else if (iID == (int)InsurerID.SHIMANE_KOIKI)
            {
                //出力先パス　柔整あはき共通
                string strdtkettei = string.Empty;
                string strdtuketuke = string.Empty;
                string strdtsisyutu = string.Empty;//20191107140032 furukawam支給年月日も手入力になったので引数追加
                string strPath = string.Empty;

                ShimaneKoiki.ExportForm f = new ShimaneKoiki.ExportForm();
                f.ShowDialog();
                if(f.ret == DialogResult.Cancel) return;
                strdtkettei = f.dtvalKettei;
                strdtuketuke = f.dtvalUketuke;
                strdtsisyutu = f.dtvalSisyutu;

                if (ShimaneKoiki.ViewerDataExport.ExportViewerData(cym, out strPath))
                {
                    //20191024153542 furukawa st //あはきSAMファイル用処理          

                    //20191107135924 furukawa st //支給年月日も手入力になったので引数追加
                    ShimaneKoiki.ExportAHK.ExportSAMData(cym, strPath,strdtkettei,strdtuketuke,strdtsisyutu);
                            //ShimaneKoiki.ExportAHK.ExportSAMData(cym, strPath, strdtkettei, strdtuketuke);
                    //20191107135924 furukawa ed ////////////////////////

                    //20191024153542 furukawa ed ////////////////////////
                }
                return;
            }
            //20190716142932 furukawa ed ////////////////////////

            else if (iID == (int)InsurerID.NARA_KOIKI)
            {
                NaraKoiki.ViewerDataExport.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.SHIZUOKA_KOIKI)
            {
                ShizuokaKoiki.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.MIYAZAKI_KOIKI)
            {
                MiyazakiKoiki.Export.ExportNewViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.HIROSHIMA_KOIKI)
            {
                HiroshimaKoiki.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.HIROSHIMA_KOIKI2022)//20220320124411 furukawa st //広島広域2022追加
            {                
                HiroshimaKoiki2022.Export.ExportMain(cym);
                //HiroshimaKoiki2022.ExportAHK.CreateAHKFile(cym);
                return;
            }
            else if (iID == (int)InsurerID.HYOGO_KOIKI2022) //20220331145504 furukawa st //兵庫広域2022追加
            {
                HyogoKoiki2022.Export.export_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.KAGOSHIMA_KOIKI) //20220428102752 furukawa st //鹿児島広域追加
            {
                KagoshimaKoiki.Export.ExportMain(cym);
                return;
            }
            else if (iID == (int)InsurerID.EHIME_KOIKI) //20220906_1 ito st //愛媛広域追加
            {
                EhimeKoiki.Export.ExportMain(cym);
                return;
            }
            else if (iID == (int)InsurerID.OSAKA_KOIKI)
            {
                OsakaKoiki.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.KISHIWADA_KOKUHO)
            {
                KishiwadaKokuho.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.OTARU_KOKUHO)
            {
                OtaruKokuho.Export.ExportDeliveryData(cym);
                return;
            }
            else if (iID == (int)InsurerID.KYOTOFU_NOKYO)
            {
                KyotofuNokyo.Export.DoExport(cym);
                KyotofuNokyo.ExportNew.DoExportNew(cym);    //20190122152539 furukawa st //2019年版ファイル作成
                return;
            }
            else if (iID == (int)InsurerID.TOKUSHIMA_KOIKI)
            {
                TokushimaKoiki.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.CHUO_RADIO)
            {
                ChuoRadio.SubmitData.Export(cym);
                return;
            }
            else if (iID == (int)InsurerID.FUTABA)
            {
                //20200305105402 furukawa st //現行と大和総研型エクスポート両方出す
                MessageBox.Show("現行データ形式で出力します");
                Futaba.SubmitData.Export(cym);

                MessageBox.Show("大和総研データ形式で出力します");
                Futaba.SubmitData.DoExport(cym);
                //20200305105402 furukawa ed ////////////////////////
                return;
            }
            else if (iID == (int)InsurerID.AMAGASAKI_KOKUHO)
            {
                Amagasaki.Export.ImageExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.IRUMA_KOKUHO)
            {
                IrumaKokuho.Export.ImageExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.IBARAKISHI_KOKUHO)   //20210408153926 furukawa st //茨木市追加 
            {
                Ibarakishi.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.KYOKAIKENPO_HIROSHIMA)   //20200303113109 furukawa st 協会けんぽ広島支部追加
            {
                kyokaikenpo_hiroshima.KKData.ExportKKData();
                return;
            }
            else if (iID == (int)InsurerID.NAGOYA_KOUWAN)   //20190129173324 furukawa st //名古屋港湾出力用
            {
                NagoyaKouwan.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.NAGOYA_MOKUZAI)  //20190129173324 furukawa ed //
            {
                NagoyaMokuzai.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.ARAKAWAKU)   //20200403171816 furukawa st //荒川区追加
            {
                Arakawaku.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.CHIBASHI_KOKUHO) //20200818115756 furukawa st //千葉市国保はあはきのみ。柔整はリスト作成画面から
            {
                Chibashi_kokuho.export_ahk.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.AKISHIMASHI) //20220405104342 furukawa st //昭島市追加
            {
                Akishimashi.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.NISHINOMIYASHI)  //20220418125803 furukawa st //西宮市国保追加
            {
                Nishinomiyashi.export_ahk.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.IBARAKI_KOIKI)   //20220523141201 furukawa st //茨城広域追加
            {
                IbarakiKoiki.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.SHIROISHI)
            {
                Shiroishi.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.SHIZUOKASHI_AHAKI)
            {
                Shizuokashi_Ahaki.Export.ExportData(cym);
                return;
            }
            else if (iID == (int)InsurerID.KAGOME)
            {
                //20220607161558 furukawa st //カゴメ新納品データ出力(以前のは新処理の中に移動した）
                Kagome.Export2022.DoExport2022(cym);
                //      Kagome.Export.BankAccountExport(cym);
                //20220607161558 furukawa ed ////////////////////////
                return;
            }
            else if (iID == (int)InsurerID.KYOTO_KOIKI)
            {
                using (var f = new KyotoKoiki.ExportForm(cym)) f.ShowDialog();
                return;
            }
            else if (iID == (int)InsurerID.IBARAKI_SEIHO)
            {
                IbarakiSeiho.SubmitData.Export(cym);
                return;
            }
            else if (iID == (int)InsurerID.ASAKURA_KOKUHO)
            {
                AsakuraKokuho.Export.ImageExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.OTSU_KOKUHO)
            {
                OtsuKokuho.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.DENKI_GARASU)
            {
                DenkiGarasu.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.MATSUDOSHI)
            {
                Matsudoshi.export_ahk.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.TOYONAKASHI_COVID19) //20220117095427 furukawa st //豊中市コロナワクチン接種追加
            {
                Toyonakashi_covid19.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.TOYONAKASHI_COVID19_SHIGAI)  //20220205161433 furukawa st  //豊中市コロナワクチン接種市外追加
            {
                Toyonakashi_covid19_shigai.export.dataexport_main(cym);
                return;
            }
            else if (iID == (int)InsurerID.GAIMUSHO_RYOUYOUHI)  //20200414173408 furukawa st //外務省療養費追加
            {
                GaimusyoRyouyouhi.Export.DoExport(cym);
                return;
            }
            else if (iID == (int)InsurerID.NISSHIN_KOKUHO)
            {
                NisshinKokuho.Export.ExportViewerData(cym);
                return;
            }
            else if (iID == (int)InsurerID.MIYAGI_KOIKI ||
                iID ==(int)InsurerID.HANNAN_KOKUHO)
            {
                ViewerData.ExportGeneralViewerData(cym);
                return;
            }

            string saveFolder = "";

            //20210618092110 furukawa st ////使いにくいので変更
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            saveFolder = dlg.Name;
            //using (var f = new FolderBrowserDialog())
            //{
            //    if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            //    saveFolder = f.SelectedPath;
            //}
            //20210618092110 furukawa ed ////////////////////////

            //提出データ作成確認メッセージ
            var res = MessageBox.Show(cym.ToString("0000年00月") + "分の 提出用データを\r\n\r\n" +
                saveFolder + "\r\n\r\nに出力します。よろしいですか？", ins.InsurerName,
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res != System.Windows.Forms.DialogResult.OK) return;

            using (var wf = new WaitForm())
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();

                switch ((InsurerID)iID)
                {
                    case InsurerID.AICHI_TOSHI:                    
                        //ナンバリング
                        wf.LogPrint("ナンバリング処理を開始します");
                        if (!AichiToshi.Export.Numbering(cym))
                        {
                            MessageBox.Show("ナンバリングに失敗しました。");
                            return;
                        }

                        //データ取得
                        wf.LogPrint("出力データを抽出します");
                        var apps = App.GetApps(cym);
                        if (apps == null)
                        {
                            MessageBox.Show("データ取得に失敗しました。");
                            return;
                        }

                        //データ出力
                        wf.LogPrint("出力処理を開始します");
                        if (!AichiToshi.Export.DoExport(saveFolder, apps, wf))
                        {
                            MessageBox.Show("データ出力に失敗しました", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //正常終了メッセージ
                        MessageBox.Show("データ出力は正常に終了しました");
                        break;

                    case InsurerID.SHARP_KENPO :    //20190214131941 furukawa st //シャープ健保用データ処理
                        //ナンバリング
                        wf.LogPrint("ナンバリング処理を開始します");
                        if (!SharpKenpo.Export.Numbering(cym))
                        {
                            MessageBox.Show("ナンバリングに失敗しました。");
                            return;
                        }

                        //データ取得
                        wf.LogPrint("出力データを抽出します");
                        apps = App.GetApps(cym);
                        if (apps == null)
                        {
                            MessageBox.Show("データ取得に失敗しました。");
                            return;
                        }

                        //データ出力
                        wf.LogPrint("出力処理を開始します");
                        if (!SharpKenpo.Export.DoExport(saveFolder, apps, wf))
                        {
                            MessageBox.Show("データ出力に失敗しました", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //正常終了メッセージ
                        MessageBox.Show("データ出力は正常に終了しました");
                        break;
                        
                    case InsurerID.MUSASHINO_KOKUHO:
                        if (!Musashino.Export.Exporting(saveFolder, cym, wf))
                        {
                            MessageBox.Show("データ出力に失敗しました", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// 提出データ事前チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済)   //学校共済 提出データ事前チェック
            {
                using (var f = new ZenkokuGakko.check.SubmitDataCheckForm(Insurer.CurrrentInsurer, cym))
                {
                    f.ShowDialog();
                }
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.CHUO_RADIO)
            {
                using (var f = new ChuoRadio.ImageSortPrintForm(cym)) f.ShowDialog();
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.SHIZUOKA_KOIKI)
            {
                ShizuokaKoiki.Export.ImageSelectExport(cym);
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.TOYONAKASHI_COVID19)    //20220119162822 furukawa st //豊中市コロナワクチン接種追加
            {
                Toyonakashi_covid19.clsCheck.CheckMain(cym);
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.TOYONAKASHI_COVID19_SHIGAI) //20220205161704 furukawa st //豊中市コロナワクチン接種市外追加
            {
                Toyonakashi_covid19_shigai.clsCheck.CheckMain(cym);
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.HYOGO_KOIKI2022)    //20221212 ito 兵庫広域追加
            {
                HyogoKoiki2022.Export.export_ahaki(cym);
            }

            //20220404122120 furukawa st //出力ボタン1個に統合
            //      //20220320161014 furukawa st ////////////////////////
            //      //広島広域2022追加

            //      else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.HIROSHIMA_KOIKI2022)
            //      {
            //          HiroshimaKoiki2022.ExportAHK.CreateAHKFile(cym);
            //      }
            //      //20220320161014 furukawa ed ////////////////////////
            //20220404122120 furukawa ed ////////////////////////

        }

        /// <summary>
        /// 金額確認
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済)   //学校共済 金額確認
            {
                using (var f = new PriceForm(cym))
                {
                    f.ShowDialog();
                }
            }
            else if (Insurer.CurrrentInsurer.EnumInsID == InsurerID.HYOGO_KOIKI2022)    //20221212 ito 兵庫広域追加
            {
                HyogoKoiki2022.Export.export_tiffWMtool(cym);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(button4.Text == "登録済み施術所(師)一覧")
            {
                var fn = Insurer.CurrrentInsurer.InsurerName + "_登録済み施術所(師)一覧.csv";
                using (var f = new SaveFileDialog())
                {
                    f.FileName = fn;
                    if (f.ShowDialog() != DialogResult.OK) return;

                    if(Clinic.CreateListCsv(f.FileName))
                        MessageBox.Show("出力が完了しました");
                    else
                        MessageBox.Show("出力に失敗しました");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return;

            CommonExport.LongImageExport(cym, f.Name);
        }

      
    }
}
