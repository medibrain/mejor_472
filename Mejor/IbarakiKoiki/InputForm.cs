﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.IbarakiKoiki

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 合計金額位置
        /// </summary>        
        Point posTotal = new Point(1000, 1400);


        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts,douiConts;


        //List<imp_ryouyouhi> lstRyouyouhi = new List<imp_ryouyouhi>();


        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();          
            
            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM ,};

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, panelNumbering, panelHnum, verifyBoxNumbering, verifyBoxF1, verifyBoxNumberingHeader,};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal, panelTotal, verifyCheckBoxHenkei, pDoui, verifyBoxDrCode, verifyBoxClinicNum, vcSejutu, verifyBoxF1Finish, };

            
            #endregion

          
            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //初回表示時
            if (!InitControl(app, verifyBoxY)) return;
            
            if (app != null)
            {
                setApp(app);
            }        

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //種類に応じたセンシティブ制御
            App app = (App)bsApp.Current;            
            InitControl(app, verifyBoxY);            
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// センシティブ制御
        /// </summary>
        /// <param name="app"></param>
        /// <param name="verifyboxY"></param>
        /// <returns></returns>
        private bool InitControl(App app,VerifyBox verifyboxY)
        {
          
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            panelHnum.Visible = false;
            panelNumbering.Visible = false;            
            panelTotal.Visible = false;            
            
            verifyBoxF1.Visible = false;
            labelF1.Visible = false;

            vcSejutu.Visible = false;
            
            pDoui.Visible = false;


            labelBui.Visible = false;
            verifyBoxBui.Visible = false;
           
            verifyCheckBoxHenkei.Visible = false;

        
            verifyBoxM.Visible = true;
            labelM.Visible = true;
            
            dgv.Visible = true;
            labelHs.Visible = true;
            labelYear.Visible = true;
            //AppがNull（入力前）のときはscanのを採用
            APP_TYPE type = app.AppType == APP_TYPE.NULL ? scan.AppType : app.AppType;

            //ベリファイ後入力修正する際のセンシティブ
            
            //ベリファイ済みで1回目ボタンの場合＝多分間違いを修正するときだけ
            if (app.StatusFlagCheck(StatusFlag.ベリファイ済) && firstTime)
            {
                
                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:                                  
                    
                        dgv.Visible = false;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        break;

                        case clsInputKind.状態記入書:
                    case clsInputKind.施術同意書:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.往療内訳表:
                    case clsInputKind.その他:
                        panelNumbering.Visible = true;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        verifyBoxM.Visible = false;
                        labelM.Visible = false;
                        dgv.Visible = false;
                        break;
                    
                    default:

                        //scan.apptypeにしとかないと、間違って続紙等登録したとき、
                        //App.apptypeは変わらないので、間違ったままの判定になってしまい入力欄が出ない
                        switch (scan.AppType)
                        {

                            case APP_TYPE.柔整:
                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;
                                panelTotal.Visible = true;

                                labelBui.Visible = true;
                                verifyBoxBui.Visible = true;
                                
                                break;

                            case APP_TYPE.あんま:
                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;                                
                                panelTotal.Visible = true;

                                vcSejutu.Visible = true;
                                
                                pDoui.Visible = true;
                                verifyCheckBoxHenkei.Visible = true;
                                break;

                            case APP_TYPE.鍼灸:

                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;
                                panelTotal.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                vcSejutu.Visible = true;
                                

                                pDoui.Visible = true;

                                break;

                            default:break;
                        }

                        break;
                }

            }
            
            //未入力・入力後表示前
            else if (verifyboxY.Text.Trim() == string.Empty)
            {
                switch (type)
                {
                    case APP_TYPE.長期:
                    case APP_TYPE.続紙:
                    case APP_TYPE.不要:
                    case APP_TYPE.エラー:
                    
                        dgv.Visible = false;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        break;

                    case APP_TYPE.状態記入書:
                    case APP_TYPE.施術報告書:
                    case APP_TYPE.同意書:
                    case APP_TYPE.同意書裏:
                    case APP_TYPE.その他:
                        panelNumbering.Visible = true;
                        verifyBoxM.Visible = false;
                        labelM.Visible = false;
                        dgv.Visible = false;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        break;

                    case APP_TYPE.柔整:
                        panelHnum.Visible = true;
                        panelNumbering.Visible = true;
                        panelTotal.Visible = true;

                        labelBui.Visible = true;
                        verifyBoxBui.Visible = true;

                        vcSejutu.Visible = false;
                        pDoui.Visible = false;
                        
                        break;

                    case APP_TYPE.鍼灸:
                        panelHnum.Visible = true;
                        panelNumbering.Visible = true;
                        panelTotal.Visible = true;

                        verifyBoxF1.Visible = true;
                        labelF1.Visible = true;
                        vcSejutu.Visible = true;
                        
                        pDoui.Visible = true;

                        break;

                    case APP_TYPE.あんま:
                        panelHnum.Visible = true;
                        panelNumbering.Visible =  true;                        
                        panelTotal.Visible = true;
                        vcSejutu.Visible = true;
                        
                        pDoui.Visible = true;
                        verifyCheckBoxHenkei.Visible = true;

                        break;
                    default:
                        break;
                }
            }

            //登録後、再表示時
            else
            {

                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:                   
                        dgv.Visible = false;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        break;

                    case clsInputKind.状態記入書:
                    case clsInputKind.施術同意書:
                    case clsInputKind.施術報告書:
                    case clsInputKind.往療内訳表:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.その他:
                        panelNumbering.Visible = true;
                        verifyBoxM.Visible = false;
                        labelM.Visible = false;
                        dgv.Visible = false;
                        labelHs.Visible = false;
                        labelYear.Visible = false;
                        break;

                    default:

                 
                        switch (type)
                        {

                            case APP_TYPE.柔整:
                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;
                                panelTotal.Visible = true;

                                labelBui.Visible = true;
                                verifyBoxBui.Visible = true;
                                vcSejutu.Visible = false;
                                pDoui.Visible = false;
                                
                                break;

                            case APP_TYPE.あんま:
                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;                                
                                panelTotal.Visible = true;
                                vcSejutu.Visible = true;
                                
                                pDoui.Visible = true;
                                verifyCheckBoxHenkei.Visible = true;
                                break;

                            case APP_TYPE.鍼灸:

                                panelHnum.Visible = true;
                                panelNumbering.Visible = true;
                                panelTotal.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;
                                vcSejutu.Visible = true;
                                
                                pDoui.Visible = true;

                                
                                break;
                            default:
                                
                                
                                break;
                        }

                        break;
                }
            }

            return true;
        }


    

        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            ////表示初期化
            //phnum.Visible = false;
            //panelTotal.Visible = false;            
            //pZenkai.Enabled = false;
            //dgv.Visible = false;

            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = 
                "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //  InitControl(scan.AppType);


            //ナンバリングヘッダ
            verifyBoxNumberingHeader.BackColor = Color.LightGray;
            verifyBoxNumberingHeader.TabStop = false;
            verifyBoxNumberingHeader.Text = $"{scan.CYM}{(scan.AppType == APP_TYPE.柔整 ? "7" : "8")}";

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //ナンバリングのセット
            void setNumbering()
            {
                setValue(verifyBoxNumberingHeader, app.Numbering.Substring(0, 7), firstTime, nv);
                setValue(verifyBoxNumbering, app.Numbering.Substring(7), firstTime, nv);
            }

            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);
                    setNumbering();

                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    setNumbering();
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    setNumbering();
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    setNumbering();
                    break;

                case (int)APP_SPECIAL_CODE.往療内訳:
                    setValue(verifyBoxY, clsInputKind.往療内訳表, firstTime, nv);
                    setNumbering();
                    break;

                case (int)APP_SPECIAL_CODE.その他:
                    setValue(verifyBoxY, clsInputKind.その他, firstTime, nv);
                    setNumbering();
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, false);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, false);

                    
                    //施術終了日1 日
                    setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day.ToString(), firstTime, nv);


                    //柔整師登録記号番号
                    setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);
                    
                    //ナンバリング 
                    setNumbering();

                    //同意日
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    //以下は1回入力にする

                    //被保険者番号
                    setValue(verifyBoxHnum, app.HihoNum.ToString(), firstTime, false);

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, false);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, false);

                    //部位
                    setValue(verifyBoxBui, app.TaggedDatas.count, firstTime, false);
                    
                    //往療あり
                    setValue(checkBoxVisit, app.Distance == 0 ? false : true, firstTime, false);
                    
                    //往療加算
                    setValue(checkBoxVisitKasan, app.VisitAdd == 0 ? false : true, firstTime, false);

                    //変形徒手
                    if(!bool.TryParse(app.TaggedDatas.GeneralString8, out bool tmpHenkei)) tmpHenkei=false;
                    setValue(verifyCheckBoxHenkei,tmpHenkei, firstTime, false);

                    //前回交付フラグ                    
                    setValue(vcSejutu, app.TaggedDatas.flgKofuUmu, firstTime, false);

                    createGrid(app);

                    break;
            }
        }



        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            dgv.Columns[nameof(imp_ryouyouhi.f000_importid)].Width = 60;    		//	インポートID
            dgv.Columns[nameof(imp_ryouyouhi.f001_chargeym)].Width = 60;    		//	1_請求年月
            dgv.Columns[nameof(imp_ryouyouhi.f002_rezenum)].Width = 60; 			//	2_簿冊レセプト番号
            dgv.Columns[nameof(imp_ryouyouhi.f003_hihonum)].Width = 60; 			//	5_被保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f004_kind)].Width = 60;    			//	6_レセプト種類コード
            dgv.Columns[nameof(imp_ryouyouhi.f005_kbn)].Width = 60; 				//	7_給付区分コード
            dgv.Columns[nameof(imp_ryouyouhi.f006_mediym)].Width = 60;  			//	8_診療年月
            dgv.Columns[nameof(imp_ryouyouhi.f007_pref)].Width = 60;    			//	9_都道府県コード
            dgv.Columns[nameof(imp_ryouyouhi.f008_city)].Width = 60;    			//	10_医療機関市区町村コード
            dgv.Columns[nameof(imp_ryouyouhi.f009_clinicnum)].Width = 60;   		//	11_医療機関コード
            dgv.Columns[nameof(imp_ryouyouhi.f010_insnum)].Width = 60;  			//	14_保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f011_gender)].Width = 60;  			//	15_性別コード
            dgv.Columns[nameof(imp_ryouyouhi.f012_pbirthdayw)].Width = 60;  		//	16_生年月日和暦
            dgv.Columns[nameof(imp_ryouyouhi.f013_atenanum)].Width = 60;    		//	17_宛名番号
            dgv.Columns[nameof(imp_ryouyouhi.f014_startdate1)].Width = 60;  		//	24_診療開始年月日１
            dgv.Columns[nameof(imp_ryouyouhi.f015_counteddays)].Width = 60; 		//	30_診療実日数
            dgv.Columns[nameof(imp_ryouyouhi.f016_ratio)].Width = 80;   			//	31_給付割合
            dgv.Columns[nameof(imp_ryouyouhi.f017_total)].Width = 80;   			//	91_費用金額
            dgv.Columns[nameof(imp_ryouyouhi.f018_charge)].Width = 80;  			//	92_保険者負担額
            dgv.Columns[nameof(imp_ryouyouhi.f019_partial)].Width = 60; 			//	97_自己負担額
            dgv.Columns[nameof(imp_ryouyouhi.f020_mngnum)].Width = 120;  			//	121_電算管理番号
            dgv.Columns[nameof(imp_ryouyouhi.cym)].Width = 60;  					//	メホール請求年月
            dgv.Columns[nameof(imp_ryouyouhi.pbirthdayad)].Width = 80;  			//	生年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.startdate1ad)].Width = 80; 			//	開始年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.mediymad)].Width = 60; 				//	診療年月西暦
            dgv.Columns[nameof(imp_ryouyouhi.clinicnum)].Width = 100;    			//	f007_pref+f004_kind+f008_city+f009_clinicnum
            dgv.Columns[nameof(imp_ryouyouhi.filekbn)].Width = 60;					//	td01/td24ファイル区別



            dgv.Columns[nameof(imp_ryouyouhi.f000_importid)].HeaderText = "ID";  				//インポートID
            dgv.Columns[nameof(imp_ryouyouhi.f001_chargeym)].HeaderText = "請求年月";  				//1_請求年月
            dgv.Columns[nameof(imp_ryouyouhi.f002_rezenum)].HeaderText = "簿冊レセプト番号";   		//2_簿冊レセプト番号
            dgv.Columns[nameof(imp_ryouyouhi.f003_hihonum)].HeaderText = "被保険者番号";   			//5_被保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f004_kind)].HeaderText = "レセプト種類コード";  			//6_レセプト種類コード
            dgv.Columns[nameof(imp_ryouyouhi.f005_kbn)].HeaderText = "給付区分コード";   				//7_給付区分コード
            dgv.Columns[nameof(imp_ryouyouhi.f006_mediym)].HeaderText = "診療年月";    				//8_診療年月
            dgv.Columns[nameof(imp_ryouyouhi.f007_pref)].HeaderText = "都道府県コード";  				//9_都道府県コード
            dgv.Columns[nameof(imp_ryouyouhi.f008_city)].HeaderText = "医療機関市区町村コード";  	//10_医療機関市区町村コード
            dgv.Columns[nameof(imp_ryouyouhi.f009_clinicnum)].HeaderText = "医療機関コード"; 		//11_医療機関コード
            dgv.Columns[nameof(imp_ryouyouhi.f010_insnum)].HeaderText = "保険者番号";    			//14_保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f011_gender)].HeaderText = "性別";           			//15_性別コード
            dgv.Columns[nameof(imp_ryouyouhi.f012_pbirthdayw)].HeaderText = "生年月日和暦";    		//16_生年月日和暦
            dgv.Columns[nameof(imp_ryouyouhi.f013_atenanum)].HeaderText = "宛名番号";  				//17_宛名番号
            dgv.Columns[nameof(imp_ryouyouhi.f014_startdate1)].HeaderText = "診療開始年月日１";    	//24_診療開始年月日１
            dgv.Columns[nameof(imp_ryouyouhi.f015_counteddays)].HeaderText = "実日数";   		//30_診療実日数
            dgv.Columns[nameof(imp_ryouyouhi.f016_ratio)].HeaderText = "給付割合"; 					//31_給付割合
            dgv.Columns[nameof(imp_ryouyouhi.f017_total)].HeaderText = "費用金額"; 					//91_費用金額
            dgv.Columns[nameof(imp_ryouyouhi.f018_charge)].HeaderText = "保険者負担額";    			//92_保険者負担額
            dgv.Columns[nameof(imp_ryouyouhi.f019_partial)].HeaderText = "自己負担額";   			//97_自己負担額
            dgv.Columns[nameof(imp_ryouyouhi.f020_mngnum)].HeaderText = "電算管理番号";    			//121_電算管理番号
            dgv.Columns[nameof(imp_ryouyouhi.cym)].HeaderText = "メホール請求年月";    					//メホール請求年月
            dgv.Columns[nameof(imp_ryouyouhi.pbirthdayad)].HeaderText = "生年月日西暦";    				//生年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.startdate1ad)].HeaderText = "開始年月日西暦";   			//開始年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.mediymad)].HeaderText = "診療年月西暦";   					//診療年月西暦
            dgv.Columns[nameof(imp_ryouyouhi.clinicnum)].HeaderText = "医療機関コード";					//f007_pref+f004_kind+f008_city+f009_clinicnum
            dgv.Columns[nameof(imp_ryouyouhi.filekbn)].HeaderText = "td01/td24ファイル区別";			//td01/td24ファイル区別


            dgv.Columns[nameof(imp_ryouyouhi.f000_importid)].Visible = true;                           //	インポートID
            dgv.Columns[nameof(imp_ryouyouhi.f001_chargeym)].Visible = true;                           //	1_請求年月
            dgv.Columns[nameof(imp_ryouyouhi.f002_rezenum)].Visible = false;                            //	2_簿冊レセプト番号
            dgv.Columns[nameof(imp_ryouyouhi.f003_hihonum)].Visible = false;                            //	5_被保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f004_kind)].Visible = false;                           //	6_レセプト種類コード
            dgv.Columns[nameof(imp_ryouyouhi.f005_kbn)].Visible = false;                            //	7_給付区分コード
            dgv.Columns[nameof(imp_ryouyouhi.f006_mediym)].Visible = false;                         //	8_診療年月
            dgv.Columns[nameof(imp_ryouyouhi.f007_pref)].Visible = false;                           //	9_都道府県コード
            dgv.Columns[nameof(imp_ryouyouhi.f008_city)].Visible = false;                           //	10_医療機関市区町村コード
            dgv.Columns[nameof(imp_ryouyouhi.f009_clinicnum)].Visible = false;                          //	11_医療機関コード
            dgv.Columns[nameof(imp_ryouyouhi.f010_insnum)].Visible = false;                         //	14_保険者番号
            dgv.Columns[nameof(imp_ryouyouhi.f011_gender)].Visible = true;                         //	15_性別コード
            dgv.Columns[nameof(imp_ryouyouhi.f012_pbirthdayw)].Visible = false;                         //	16_生年月日和暦
            dgv.Columns[nameof(imp_ryouyouhi.f013_atenanum)].Visible = false;                           //	17_宛名番号
            dgv.Columns[nameof(imp_ryouyouhi.f014_startdate1)].Visible = false;                         //	24_診療開始年月日１
            dgv.Columns[nameof(imp_ryouyouhi.f015_counteddays)].Visible = true;                            //	30_診療実日数
            dgv.Columns[nameof(imp_ryouyouhi.f016_ratio)].Visible = true;                          //	31_給付割合
            dgv.Columns[nameof(imp_ryouyouhi.f017_total)].Visible = true;                          //	91_費用金額
            dgv.Columns[nameof(imp_ryouyouhi.f018_charge)].Visible = true;                         //	92_保険者負担額
            dgv.Columns[nameof(imp_ryouyouhi.f019_partial)].Visible = true;                            //	97_自己負担額
            dgv.Columns[nameof(imp_ryouyouhi.f020_mngnum)].Visible = true;                         //	121_電算管理番号
            dgv.Columns[nameof(imp_ryouyouhi.cym)].Visible = false;                                 //	メホール請求年月
            dgv.Columns[nameof(imp_ryouyouhi.pbirthdayad)].Visible = true;                         //	生年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.startdate1ad)].Visible = true;                            //	開始年月日西暦
            dgv.Columns[nameof(imp_ryouyouhi.mediymad)].Visible = true;                            //	診療年月西暦
            dgv.Columns[nameof(imp_ryouyouhi.clinicnum)].Visible = true;                           //	f007_pref+f004_kind+f008_city+f009_clinicnum
            dgv.Columns[nameof(imp_ryouyouhi.filekbn)].Visible = false;		                        //	td01/td24ファイル区別



        }


        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid(App app = null)
        {
            
            List<imp_ryouyouhi> lstimp = new List<imp_ryouyouhi>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

            List<imp_ryouyouhi> lstRyouyouhi = imp_ryouyouhi.Select(app.CYM, strhnum, intTotal.ToString());

            foreach (imp_ryouyouhi item in lstRyouyouhi)
            {

                if (item.mediymad == intymad &&
                    item.f003_hihonum == strhnum &&
                    item.f017_total == intTotal.ToString())

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;

            //0件の場合グリッド作らない
            if (lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
                return;
            }

            dgv.DataSource = lstimp;

            InitGrid();


            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();


            if (app != null && app.RrID.ToString() != string.Empty && app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {

                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells[nameof(imp_ryouyouhi.f020_mngnum)].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (r.Cells["f000_importid"].Value.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {

                        if (strcomnum == app.ComNum)
                        {
                            //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない                            
                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;
                        }
                        //提供データIDと違う場合の処理抜け                        
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }




            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }


            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }

        #endregion

        #region 各種更新

        private bool RegistImportData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {

                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f000_importid)].Value.ToString()); //ID

                    int tmpRatio = 0;
                    app.Ratio = int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f016_ratio)].Value.ToString(), out tmpRatio) == false ? 0 : tmpRatio/10;  //給付割合                        

                    //入力を優先させる                        
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                      //被保険者証番号

                    app.InsNum = dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f010_insnum)].Value.ToString();                              //保険者番号                                       
                    app.Sex = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f011_gender)].Value.ToString());                      //性別 
                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.pbirthdayad)].Value.ToString());            //生年月日西暦
                    app.ComNum = dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f020_mngnum)].Value.ToString();                              //電算管理番号
                    app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f015_counteddays)].Value.ToString());         //実日数
                    app.FushoFirstDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.startdate1ad)].Value.ToString());    //初検日1(診療開始年月日1でよい）
                    app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f013_atenanum)].Value.ToString();        //宛名番号
                    
                    int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f019_partial)].Value.ToString(), out int intPartial);//一部負担金
                    app.Partial = intPartial;                       
                    int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f018_charge)].Value.ToString(),out int intcharge);//請求金額
                    app.Charge = intcharge;
          
                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.mediymad)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.mediymad)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

                    string strClinicNum = dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.clinicnum)].Value.ToString();
                    List<imp_clinicMaster> lstClinic = imp_clinicMaster.Select(app.CYM, strClinicNum);
                    //複数行ある場合、最初の1行のみ取得で大丈夫です。
                    app.ClinicNum = lstClinic[0].clinicnum;// dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f008_clinicnum)].Value.ToString();                            //医療機関番号
                    app.ClinicName = lstClinic[0].f006_clinicname;//dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f019_clinicname)].Value.ToString();                          //医療機関名                    
                    app.ClinicZip = lstClinic[0].f007_cliniczip;
                    app.ClinicAdd = lstClinic[0].f008_clinicadd;

                    string strHihonum = dgv.CurrentRow.Cells[nameof(imp_ryouyouhi.f003_hihonum)].Value.ToString();

                    List<imp_hihomaster> lstHiho = imp_hihomaster.select(app.CYM, strHihonum);
                    app.HihoName = lstHiho[0].f002_pname;//被保険者名＝受療者名
                    app.HihoZip = lstHiho[0].f004_pzip;
                    app.HihoAdd = lstHiho[0].f005_padd;
                    app.PersonName = lstHiho[0].f002_pname;
                    app.TaggedDatas.DestAdd = lstHiho[0].f007_sendadd;
                    app.TaggedDatas.DestZip = lstHiho[0].f006_sendzip;
                    app.TaggedDatas.Kana = lstHiho[0].f003_pkana;

                    //aux更新
                    if (!Application_AUX.Update(app.Aid, app.AppType, null
                        , string.Empty
                        , lstClinic[0].f000_importid.ToString()
                        , lstHiho[0].f000_importid.ToString())) return false;

                    
                }
                else
                {
                    if (MessageBox.Show($"一致するマッチングデータが見つかりません。このまま登録しますか？",
                        Application.ProductName, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;
            }

        }

        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //ナンバリング
            string numberingHeader = verifyBoxNumberingHeader.Text.Trim();
            string numbering = verifyBoxNumbering.Text.Trim();

            //ナンバリングをチェックしてOKならば登録
            bool numberingCheck()
            {
                setStatus(verifyBoxNumberingHeader, long.Parse(numberingHeader) < 0 || (numberingHeader.Length != 7 && numberingHeader != string.Empty));
                setStatus(verifyBoxNumbering, long.Parse(numbering) < 0 || (numbering.Length != 5 && numbering != string.Empty));

                if(hasError) return false;

                //ナンバリング　後半がない場合はナンバリング無しとする
                if (numbering != string.Empty) app.Numbering = numberingHeader + numbering;

                return true;
            }
            

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    //ナンバリング                    
                    if (verifyBoxNumbering.Text == string.Empty)
                    {
                        if (MessageBox.Show("ナンバリングなしで宜しいですか？"
                            ,Application.ProductName,MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.No) return false;
                    }
                    //ナンバリング
                    app.Numbering = numberingHeader + numbering;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    //ナンバリング
                    if(!numberingCheck())return false;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    //ナンバリング
                    if (!numberingCheck()) return false;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;
                    //ナンバリング
                    if (!numberingCheck()) return false;
                    break;
                case clsInputKind.往療内訳表:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.往療内訳;
                    app.AppType = APP_TYPE.往療内訳;
                    //ナンバリング
                    if (!numberingCheck()) return false;
                    break;

                case clsInputKind.その他:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.その他;
                    app.AppType = APP_TYPE.その他;
                    //ナンバリング
                    if (!numberingCheck()) return false;
                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //ナンバリング
                    setStatus(verifyBoxNumberingHeader, long.Parse(numberingHeader) < 0 || (numberingHeader.Length!=7 && numberingHeader!=string.Empty));
                    setStatus(verifyBoxNumbering, long.Parse(numbering) < 0 || (numbering.Length != 5 && numbering != string.Empty));


                    //被保険者証番号   
                    string strHnum = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, strHnum==string.Empty);

                    //負傷名1=>鍼灸のみ
                    string f1 = verifyBoxF1.Text.Trim();
                    if (scan.AppType == APP_TYPE.鍼灸) setStatus(verifyBoxF1, f1.Trim() == string.Empty);


                    //終了日
                    DateTime f1Finish1 = DateTime.MinValue;
                    f1Finish1 = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1Finish.GetIntValue());


                    //部位
                    int bui = verifyBoxBui.GetIntValue();
                    if (scan.AppType == APP_TYPE.柔整) setStatus(verifyBoxBui, bui < 0 || bui>10);


                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

            
           
                    //柔整師登録記号番号
                    string strDrCode = verifyBoxDrCode.Text.Trim();
                    setStatus(verifyBoxDrCode, strDrCode.Length > 10);

                    //往療 入力チェック無し
                    //往療加算　入力チェック無し


                   

                    DateTime dtDoui = DateTime.MinValue;                    
                    if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                    {
           
                        //同意年月日
                        dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);
                    
                    }

                    string strHenkei = string.Empty;
                    if (scan.AppType == APP_TYPE.あんま)
                    {
                        //変形徒手
                        strHenkei = verifyCheckBoxHenkei.Checked.ToString();
                    }
                                        
                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月


                    //ナンバリング 後半がない場合はナンバリング無しとする
                    if (numbering != string.Empty) app.Numbering = numberingHeader + numbering;
                                        
                    //被保険者証記号番号
                    app.HihoNum =$"{strHnum}";
                    
                    //申請書種別
                    app.AppType = scan.AppType;

                    //終了日
                    if (verifyBoxF1Finish.Visible) app.FushoFinishDate1 = f1Finish1;

                    //負傷名1 
                    if (verifyBoxF1.Visible) app.FushoName1 = verifyBoxF1.Text.Trim();

                    //部位
                    if (scan.AppType == APP_TYPE.柔整) app.TaggedDatas.count = bui;

                    //合計
                    app.Total = total;
                 
                    //柔整師登録記号番号
                    app.DrNum = strDrCode;

                    //往療
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;

                    //往療加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;

                
                    //交付有無
                    app.TaggedDatas.flgKofuUmu = vcSejutu.Checked;
                    
                    //同意年月日
                    app.TaggedDatas.DouiDate = dtDoui;  

                    //変形徒手
                    app.TaggedDatas.GeneralString8 = strHenkei;

                    //提供データ紐付け
                    if(!RegistImportData(app))return false;

                    #endregion


                    break;

            }
          

            return true;
        }


        
        //連番チェック機構。ベリファイでも良いというので未使用だが、良いアイデアなので置いとく
        
        /// <summary>
        /// ナンバリング連番チェック
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="year"></param>
        /// <param name="strNumbering"></param>
        /// <returns></returns>
        private bool CheckNumbering(int aid, int cym, string strNumbering)
        {            
            //１．このグループIDと一つ前のグループIDの申請書だけを取得し、AID降順で並べる。
            //２．取得したリストの0番目（AID最大）と現在の申請書のナンバリングの差が１以外の場合はfalse
            //そうすればグループまたぎも確認できる。グループIDが増えたところで、一つ前のグループIDしか見ないので、最大でも200件となるのでさほど重くない
            using (DB.Command cmd = DB.Main.CreateCmd($"select aid,numbering from application " +
                $"where cym={cym} and " +
                $"groupid in ({this.scanGroup.GroupID},{this.scanGroup.GroupID-1}) and " +
                $"aid<{aid} and aapptype in (6,7,8) order by aid desc"))
            {
                var lst = cmd.TryExecuteReaderList();
                if (lst.Count == 0) return true;
                if (lst[0][1].ToString() == string.Empty) return true;

                int currNumbering = int.Parse(strNumbering);
                App prevApp = new App();
                prevApp.Aid = int.Parse(lst[0][0].ToString());
                prevApp.Numbering = lst[0][1].ToString();
                int prevNumbering = int.Parse(prevApp.Numbering);

                if (currNumbering - prevNumbering != 1) return false;
                else return true;
            }
        }
        

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

    
       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
   

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }



        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotal;
            
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            
        }
        #endregion



        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            verifyBoxHnum.Text = verifyBoxHnum.Text.Trim().PadLeft(8, '0');

            //1ヶ月前データを表示
            if (verifyBoxHnum.Text.Trim() == string.Empty) return;
            PastData.PersonalData p = PastData.PersonalData.SelectRecordOne($"hnum='{verifyBoxHnum.Text.Trim()}'");
            if (p == null) return;
            //setValue(verifyBoxHihoname, p.hname, firstTime,true);
            //setValue(verifyBoxPname, p.pname, firstTime, true);
            //setValue(verifyBoxSex, p.psex, firstTime, true);
            //setDateValue(p.pbirthday, firstTime, true, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

            
        }

  
        private void verifyBoxNumbering_Validated(object sender, EventArgs e)
        {
            verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(5, '0');
        }

    
        private void verifyBoxNumbering_KeyPress(object sender, KeyPressEventArgs e)
        {
            //数字以外入力不可
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar!='\b') e.Handled = true;
        }

        private void verifyBoxF1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //数字以外入力不可
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b') e.Handled = true;
        }

        private void verifyBoxDrCode_Validated(object sender, EventArgs e)
        {
            if (verifyBoxDrCode.Text.Trim() == string.Empty) return;
            PastData.ClinicData c = PastData.ClinicData.SelectRecordOne($"doctornum='{verifyBoxDrCode.Text.Trim()}'");
            if (c == null) return;
            //setValue(verifyBoxDrName, c.doctorname, firstTime, true);
            //setValue(verifyBoxHosName, c.clinicname, firstTime, true);
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            createGrid(app);
        }

    }      
}
