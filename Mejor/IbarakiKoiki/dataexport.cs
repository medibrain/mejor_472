﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.IbarakiKoiki

{

    partial class Juran
    {
        public int aid { get; set; } = 0;
        public int ym { get; set; } = 0;

    }

    
            
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export
    {
        public static bool flgImage = false;
        public static bool flgOther = false;

        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            //・データ出力する際に①全て出す②画像だけ出す③CSVだけ出すと選択できるようにしていただきたいです。
            frmExport frm = new frmExport();
            frm.ShowDialog();

            #region 出力先
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            if (odd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;
            string strOutputPath = odd.Name;

            if (strOutputPath == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("出力先が指定されていません");
                return false;
            }

            strOutputPath = $"{strOutputPath}\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力\\SYS_{cym}";

            if (!System.IO.Directory.Exists(strOutputPath)) System.IO.Directory.CreateDirectory(strOutputPath);

            string strOutputPathJyuseiImage = $"{strOutputPath}\\image\\柔整";
            string strOutputPathAHKImage = $"{strOutputPath}\\image\\あはき";
            string strOutputPathData = $"{strOutputPath}\\data";

            if (!System.IO.Directory.Exists(strOutputPathJyuseiImage)) System.IO.Directory.CreateDirectory(strOutputPathJyuseiImage);
            if (!System.IO.Directory.Exists(strOutputPathAHKImage)) System.IO.Directory.CreateDirectory(strOutputPathAHKImage);
            if (!System.IO.Directory.Exists(strOutputPathData)) System.IO.Directory.CreateDirectory(strOutputPathData);
            #endregion

            //List<App> lst = App.GetApps(cym);

            //バッチ番号(ナンバリング)がある画像のみ抽出
            List<App> lst = App.GetAppsWithWhere($"where trim(a.numbering)<>'' and cym={cym}");
            lst.Sort((x, y) => x.Numbering.CompareTo(y.Numbering));

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                if (flgImage)
                {
                    //納品データ1画像出力＋画像情報テーブル作成
                    if (!ExportImage(lst, strOutputPathAHKImage, strOutputPathJyuseiImage, wf)) return false;

                    //納品データ1画像情報csv作成
                    if (!Create_exp_imagedata_csv(cym, strOutputPathData, wf)) return false;
                }


                if (flgOther)
                {
                    //柔整申請書データ
                    if (!Create_exp_jusei(lst, cym, wf)) return false;
                    if (!Create_exp_jusei_csv(cym, strOutputPathData, wf)) return false;

                    //あはき申請書データ
                    if (!Create_exp_ahk(lst, cym, wf)) return false;
                    if (!Create_exp_ahk_csv(cym, strOutputPathData, wf)) return false;

                    //疑義データ
                    if (!Create_exp_gigi(lst, cym, wf)) return false;
                    if (!Create_exp_ahk_gigi_csv(cym, strOutputPathData, wf)) return false;
                    if (!Create_exp_jusei_gigi_csv(cym, strOutputPathData, wf)) return false;

                    //往療データ
                    if (!Create_exp_ouryo(lst, cym, wf)) return false;
                    if (!Create_exp_ouryo_csv(cym, strOutputPathData, wf)) return false;
                }
                
                System.Windows.Forms.MessageBox.Show("終了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 納品データ1画像出力＋画像情報テーブル作成
        /// </summary>
        /// <param name="lst">appリスト</param>
        /// <param name="strDirAHK">あはきフォルダ</param>
        /// <param name="strDirJusei">柔整フォルダ</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(List<App> lst, string strDirAHK,string strDirJusei, WaitForm wf)
        {
            List<exp_imagedata> lstImageData = new List<exp_imagedata>();
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            DB.Transaction tran = new DB.Transaction(DB.Main);
            
            try
            {
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                //申請書画像パス取得
                //ナンバリングにリネームしフォルダにコピー
                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                   
                    string strImagePath = item.GetImageFullPath();

                    //app.numberingにリネームしフォルダにコピー
                    string strDestPath = string.Empty;
                    if (Scan.Select(item.ScanID).AppType == APP_TYPE.柔整) strDestPath = $"{strDirJusei}\\{item.Numbering}.tif";
                    if (Scan.Select(item.ScanID).AppType == APP_TYPE.あんま || Scan.Select(item.ScanID).AppType == APP_TYPE.鍼灸) strDestPath = $"{strDirAHK}\\{item.Numbering}.tif";

                    string strDestFileName = System.IO.Path.GetFileNameWithoutExtension(strDestPath);
               
                    wf.LogPrint($"納品データ1申請書画像ファイルコピー＋画像情報リスト登録 バッチ番号 {strDestFileName}");
                    fc.FileCopy(strImagePath, strDestPath);


                    //app.numbering（バッチ番号）とapp.aidをリストに入れる
                    exp_imagedata cls = new exp_imagedata();
                    
                    cls.f001_batch = strDestFileName;

                    //20220618154317 furukawa st ////////////////////////
                    //申請書もバッチ番号が必要（その申請書自身のナンバリング）
                    
                    if (item.AppType == APP_TYPE.柔整 || item.AppType == APP_TYPE.あんま || item.AppType == APP_TYPE.鍼灸)
                    {
                        cls.f002_kbn = "1";
                        cls.f003_parentbatch = item.Numbering;
                    }
                    //  if (item.AppType == APP_TYPE.柔整 || item.AppType == APP_TYPE.あんま || item.AppType == APP_TYPE.鍼灸) cls.f002_kbn = "1";
                    //20220618154317 furukawa ed ////////////////////////
                    else
                    {
                        //続紙の場合
                        cls.f002_kbn = "99";
                        //20220618181014 furukawa st ////////////////////////
                        //続紙の親AIDが不明な場合はボタンを押すよう促す
                        
                        try
                        {
                            cls.f003_parentbatch = App.GetApp(Application_AUX.Select(item.Aid).parentaid).Numbering;//item.Numbering;
                        }
                        catch(Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show($"aid:{item.Aid} 続紙関連付けがされていない可能性があります。\r\n" +
                                $"グループ選択画面の「続紙を申請書に関連付け」ボタンを押して、再度実行してください"
                                ,System.Windows.Forms.Application.ProductName
                                ,System.Windows.Forms.MessageBoxButtons.OK
                                ,System.Windows.Forms.MessageBoxIcon.Exclamation);
                            return false; 
                        }
                        //  cls.f003_parentbatch = App.GetApp(Application_AUX.Select(item.Aid).parentaid).Numbering;//item.Numbering;
                        //20220618181014 furukawa ed ////////////////////////
                    }
                    cls.aid = item.Aid;
                    cls.shokaicode = item.ShokaiCode;

                    lstImageData.Add(cls);

                    wf.InvokeValue++;
                }


                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                //回答書画像コピーと画像情報作成
                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    if (item.ShokaiCode == String.Empty) continue;

                    

                    //app.shokaicodeで回答画像パス取得
                    string strKaitoPath = Shokai.ShokaiImage.SelectByCode(item.ShokaiCode).GetFullPath();

                    //K+app.numberingにリネームしフォルダにコピー
                    string strDestPath = string.Empty;
                    if (Scan.Select(item.ScanID).AppType == APP_TYPE.柔整) strDestPath = $"{strDirJusei}\\K{item.Numbering}.tif";
                    if (Scan.Select(item.ScanID).AppType == APP_TYPE.あんま || Scan.Select(item.ScanID).AppType == APP_TYPE.鍼灸) strDestPath = $"{strDirAHK}\\K{item.Numbering}.tif";

                    string strDestFileName = System.IO.Path.GetFileNameWithoutExtension(strDestPath);
                  
                    wf.LogPrint($"納品データ1回答書画像ファイルコピー＋画像情報リスト登録 バッチ番号 {strDestFileName}");

                    fc.FileCopy(strKaitoPath, strDestPath);

                    //K+app.numbering（バッチ番号）とapp.aidをリストに入れる
                    exp_imagedata cls = new exp_imagedata();
                    
                    cls.f001_batch = strDestFileName;
                    cls.f002_kbn = "2";
                    cls.f003_parentbatch = item.Numbering;
                    cls.aid = 0;
                    cls.shokaicode = item.ShokaiCode;

                    lstImageData.Add(cls);
                    wf.InvokeValue++;
                }

                wf.SetMax(lstImageData.Count);
                wf.InvokeValue = 0;
                using (DB.Command cmd = DB.Main.CreateCmd($"truncate table exp_imagedata",tran))
                {
                    wf.LogPrint($"納品データ1画像情報テーブル削除");
                    cmd.TryExecuteNonQuery();

                    //テーブル登録
                    foreach (exp_imagedata item in lstImageData)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                        wf.LogPrint($"納品データ1画像情報テーブル登録 AID {item.aid}");
                        if (!DB.Main.Insert<exp_imagedata>(item, tran)) return false;
                        wf.InvokeValue++;
                    }

                    tran.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

            }

        }

        /// <summary>
        /// 納品データ1画像情報csv作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_imagedata_csv(int cym,string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\IMAGE_{cym}.csv", false, System.Text.Encoding.UTF8);
            DB.Command cmd = null;

            string strHeader = $"バッチ番号,用紙区分,親バッチ番号";
            
            try
            {
                cmd = DB.Main.CreateCmd($"select f001_batch,f002_kbn,f003_parentbatch from exp_imagedata order by f000_id");
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                sw.WriteLine(strHeader);

                foreach(var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }

                    wf.LogPrint($"納品データ1画像情報csv作成 {item[0]}");
                    sw.WriteLine($"{item[0]},{item[1]},{item[2]}");
                    wf.InvokeValue++;
                }
                wf.LogPrint($"納品データ1画像情報csv作成終了");
                return true;
            }
            catch (Exception ex)
            {
                
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                sw.Close();
            }
        }


        /// <summary>
        /// 納品データ2柔整申請書情報CSV作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_jusei_csv(int cym, string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\JUSEI_{cym}.csv", false, System.Text.Encoding.GetEncoding("utf-8"));
            string strHeader = $"バッチ番号,処理年月,施術年月,千住分区分,社団区分,調査照会番号," +
                $"被保険者番号,被保険者氏名,被保険者郵便番号,被保険者住所,初検年月日,施術終了年月日,施術日数," +
                $"部位数,一部負担金,費用額,登録記号番号,施術所名,施術月数,継続月数,第三者行為区分," +
                $"長期施術区分,往療区分,頻回施術区分,多部位施術区分";

            DB.Command cmd = null;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" select ");
                sb.Append(" f001_batch");
                sb.Append(",f002_shoriym");
                sb.Append(",f003_mediym");
                sb.Append(",f004_senju");
                sb.Append(",f005_shadan");
                sb.Append(",f006_shokainum");
                sb.Append(",f007_hihonum");
                sb.Append(",f008_hihoname");
                sb.Append(",f009_hihozip");
                sb.Append(",f010_hihoadd");
                sb.Append(",f011_firstdate");
                sb.Append(",f012_finishdate");
                sb.Append(",f013_counteddays");
                sb.Append(",f014_bui");
                sb.Append(",f015_partial");
                sb.Append(",f016_total");
                sb.Append(",f017_drnum");
                sb.Append(",f018_clinicname");
                sb.Append(",f019_months");
                sb.Append(",f020_continuous");
                sb.Append(",f021_daisansha");
                sb.Append(",f022_choki");
                sb.Append(",f023_ouryo");
                sb.Append(",f024_hinkai");
                sb.Append(",f025_tabui");
                sb.Append(" from exp_jyusei order by f000_id");
                
                cmd = DB.Main.CreateCmd(sb.ToString());
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                sw.WriteLine(strHeader);

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }

                    string strLine = string.Empty;
                    wf.LogPrint($"納品データ2柔整申請書情報csv作成 バッチ番号(ナンバリング): {item[0]}");
                    for (int c = 0; c < item.Length; c++)
                    {
                        strLine += $"{item[c].ToString()},";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }

                wf.LogPrint($"納品データ2柔整申請書情報csv作成完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 納品データ2柔整申請書情報テーブル作成
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_jusei(List<App> lst, int cym, WaitForm wf)
        {
            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd = null;
            try
            {
                cmd = DB.Main.CreateCmd($"truncate table exp_jyusei;", tran);
                wf.LogPrint($"納品データ2柔整申請書情報テーブル削除");
                cmd.TryExecuteNonQuery();

                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //柔整申請書のみ必要
                    if (!new APP_TYPE[] { APP_TYPE.柔整}.Contains(item.AppType)) continue;
                    //if (Scan.Select(item.ScanID).AppType != APP_TYPE.柔整) continue;

                    wf.LogPrint($"納品データ2柔整申請書情報テーブル作成 バッチ番号(ナンバリング): {item.Numbering}");

                    exp_jyusei cls = new exp_jyusei();
                    cls.f001_batch = item.Numbering;
                    cls.f002_shoriym = DateTimeEx.ToDateTime(item.CYM.ToString() + "01").ToString("yyyy/MM/dd");
                    cls.f003_mediym = DateTimeEx.ToDateTime(item.YM.ToString() + "01").ToString("yyyy/MM/dd");
                    cls.f004_senju = Scan.Select(item.ScanID).Note1 == "千住" ? "1" : string.Empty;

                    //・5社団区分は「茨城県」のみです。Noteが「茨城県_社団」となっているもののみです。
                    if (Scan.Select(item.ScanID).Note1=="茨城県_社団") cls.f005_shadan = "1";
                    else cls.f005_shadan = string.Empty;
                    //if (Scan.Select(item.ScanID).Note1.Contains("茨城県_社団")) cls.f005_shadan = string.Empty;
                    //else cls.f005_shadan = "1";

                    cls.f006_shokainum = item.ShokaiCode;
                    cls.f007_hihonum = item.HihoNum;
                    cls.f008_hihoname = item.HihoName;
                    cls.f009_hihozip = item.HihoZip;
                    cls.f010_hihoadd = item.HihoAdd;
                    cls.f011_firstdate = item.FushoFirstDate1.ToString("yyyy/MM/dd");
                    cls.f012_finishdate = item.FushoFinishDate1.ToString("yyyy/MM/dd");
                    cls.f013_counteddays = item.CountedDays.ToString();
                    cls.f014_bui = item.TaggedDatas.count.ToString();
                    cls.f015_partial = item.Partial.ToString();
                    cls.f016_total = item.Total.ToString();

                    //柔整の登録記号番号は協＝0、契＝1で入力しているが、協＝1、契＝2にしないといけない。（あはきは見たまま入力で大丈夫です）
                    if (item.DrNum != string.Empty)
                    {
                        if (item.DrNum.Substring(0, 1) == "0") cls.f017_drnum = $"1{item.DrNum.Substring(1)}";
                        if (item.DrNum.Substring(0, 1) == "1") cls.f017_drnum = $"2{item.DrNum.Substring(1)}";
                    }

                    cls.f018_clinicname = item.ClinicName;

                    //月で計算。年またぎは年数×12を足すことで解消。終了年月日がない場合はあり得ないが、少なくともその月は通院しているので1とする
                    int ts = 0;
                    if (item.FushoFinishDate1 == DateTime.MinValue) ts = 1;// DateTime.Now.Month - item.FushoFirstDate1.Month + (12 * (DateTime.Now.Year - item.FushoFirstDate1.Year))+1;
                    else ts = item.FushoFinishDate1.Month - item.FushoFirstDate1.Month + (12 * (item.FushoFinishDate1.Year - item.FushoFirstDate1.Year))+1;

                    cls.f019_months = ts.ToString();

                    //縦覧
                    cls.f020_continuous = ContinuousMonths(item.HihoNum);


                    cls.f021_daisansha = item.StatusFlagCheck(StatusFlag.処理1) == true ? "1" : string.Empty;//21は点検をしていて見つけた場合に何かしらのフラグ（処理1など）
                    cls.f022_choki = item.StatusFlagCheck(StatusFlag.処理2) == true ? "1" : string.Empty;//22はリスト作成から検索し、何かしらのフラグ（処理2など）を立てる予定でした。


                    cls.f023_ouryo = item.Distance == 999 ? "1" : string.Empty;
                    cls.f024_hinkai = item.CountedDays >= 15 ? "1" : string.Empty;
                    cls.f025_tabui = item.TaggedDatas.count >= 3 ? "1" : string.Empty;



                    if (!DB.Main.Insert<exp_jyusei>(cls,tran)) return false;
                    wf.InvokeValue++;
                }

                wf.LogPrint($"納品データ2柔整申請書情報テーブル作成完了");
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 同じヒホバンが連続月でないか
        /// </summary>
        /// <param name="strHihoNum"></param>
        /// <returns></returns>
        private static string ContinuousMonths(string strHihoNum)
        {
            DB.Command cmd = null;
            List<Juran> lstJuran = new List<Juran>();
            try
            {
                cmd = DB.Main.CreateCmd($"select aid,ym from application where hnum='{strHihoNum}' group by aid,ym order by aid desc");
                var lstapp = cmd.TryExecuteReaderList();

                foreach (var item in lstapp)
                {
                    Juran cls = new Juran();
                    cls.aid = int.Parse(item[0].ToString());
                    cls.ym = int.Parse(item[1].ToString());
                    lstJuran.Add(cls);

                }

                //1件の場合は連続していないので１固定
                if (lstJuran.Count == 1) return "1";
                //ありえないが一応
                if (lstJuran.Count == 0) return string.Empty;

                //2件以上の場合
                //配列の上から差を確認し、1月の差の場合はカウントする
                int prevym = 0;
                int ym = 0;
                int cnt = 0;
                int continuousCnt = 1;

                while (lstJuran.Count > cnt)
                {

                    ym = lstJuran[cnt].ym;
                    if ((cnt + 1) >= lstJuran.Count) break;

                    prevym = lstJuran[cnt + 1].ym;
                    if (DateTimeEx.Int6YmAddMonth(ym, -1) == prevym)
                    {
                        continuousCnt++;
                    }
                    else
                    {
                        break;
                    }
                    cnt++;
                }


                return continuousCnt.ToString();


            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return String.Empty;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 納品データ6往療距離CSV作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_ouryo_csv(int cym, string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\OURYO_{cym}.csv", false, System.Text.Encoding.GetEncoding("utf-8"));
            string strHeader = $"バッチ番号,処理年月,連番,起点住所,施術場所住所,直線距離";
            
            DB.Command cmd = null;
                        
            try
            {
                //20220618155436 furukawa st ////////////////////////
                //f005列名変更
                
                cmd = DB.Main.CreateCmd($"select f001_batch,f002_shoriym,f003_renban,f004_kiten,f005_place,f006_distance from exp_ouryo order by f000_id");
                //cmd = DB.Main.CreateCmd($"select f001_batch,f002_shoriym,f003_renban,f004_kiten,f005_clinic,f006_distance from exp_ouryo order by f000_id");
                //20220618155436 furukawa ed ////////////////////////


                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                sw.WriteLine(strHeader);

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }

                    string strLine = string.Empty;
                    wf.LogPrint($"納品データ6往療距離CSV作成 バッチ番号(ナンバリング): {item[0]}");
                    for (int c = 0; c < item.Length; c++)
                    {
                        strLine += $"{item[c].ToString()},";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }

                wf.LogPrint($"納品データ6往療距離CSV作成完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }
        }



        /// <summary>
        /// 納品データ6往療距離情報
        /// </summary>
        ///<param name="lst"></param>
        ///<param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_ouryo(List<App> lst, int cym, WaitForm wf)
        {
            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd = null;
            
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            try
            {
                cmd = DB.Main.CreateCmd($"truncate table exp_ouryo", tran);
                wf.LogPrint($"納品データ6往療距離テーブル削除");
                cmd.TryExecuteNonQuery();

                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                    
                    //・疑義なし・ありどちらも反映させてほしいです。往療点検フラグ立っているものは全て出力してください。
                    if (!item.StatusFlagCheck(StatusFlag.往療点検対象))
                    {
                        wf.InvokeValue++;
                        continue;
                    }

                    exp_ouryo cls = new exp_ouryo();
                    wf.LogPrint($"納品データ6往療距離テーブル作成 バッチ番号(ナンバリング): {item.Numbering}");

                    cls.f001_batch = item.Numbering;
                    cls.f002_shoriym = DateTimeEx.ToDateTime($"{cym}01").ToString("yyyy/MM/dd");
                    cls.f003_renban = "0";//納品データ6の連番についてですが、基本的に0固定でお願いいたします。


                    //20220618155130 furukawa st ////////////////////////
                    //起点は施術所、往診場所は被保険者住所だった
                    
                    cls.f004_kiten = item.ClinicAdd;
                    cls.f005_place = item.HihoAdd;

                    //      cls.f004_kiten = item.HihoAdd;
                    //      cls.f005_clinic = item.ClinicAdd;
                    //20220618155130 furukawa ed ////////////////////////

                    cls.f006_distance = decimal.Parse(item.TaggedDatas.CalcDistance.ToString()).ToString("F2");

                    if (!DB.Main.Insert<exp_ouryo>(cls,tran)) return false;
                    wf.InvokeValue++;
                }
                tran.Commit();
                wf.LogPrint($"納品データ6往療距離テーブル作成完了");
                return true;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 納品データ5あはき疑義CSV作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_ahk_gigi_csv(int cym, string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\AHAKI_GIGI_{cym}.csv", false, System.Text.Encoding.GetEncoding("utf-8"));
            string strHeader = $"バッチ番号,疑義区分,回答文書受付日,返戻区分,疑義内容";

            DB.Command cmd = null;
            
            try
            {
                cmd = DB.Main.CreateCmd($"select f001_batch,f002_gigi,f003_uketsukebi,f004_henrei,f005_giginaiyo from exp_gigi " +
                    $"where apptype='{(int)APP_TYPE.あんま}' or apptype='{(int)APP_TYPE.鍼灸}' order by f000_id");
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                sw.WriteLine(strHeader);

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }

                    string strLine = string.Empty;
                    wf.LogPrint($"納品データ5あはき疑義CSV作成 バッチ番号(ナンバリング): {item[0]}"); 
                    for (int c = 0; c < item.Length; c++)
                    {
                        strLine += $"{item[c].ToString()},";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }
                wf.LogPrint($"納品データ5あはき疑義CSV作成完了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 納品データ3柔整疑義CSV作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_jusei_gigi_csv(int cym, string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\JUSEI_GIGI_{cym}.csv", false, System.Text.Encoding.GetEncoding("utf-8"));
            string strHeader = $"バッチ番号,疑義区分,回答文書受付日,返戻区分,疑義内容";

            DB.Command cmd = null;
                        
            try
            {
                cmd = DB.Main.CreateCmd($"select f001_batch,f002_gigi,f003_uketsukebi,f004_henrei,f005_giginaiyo from exp_gigi " +
                    $"where apptype='{(int)APP_TYPE.柔整}' order by f000_id");
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                sw.WriteLine(strHeader);

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }

                    string strLine = string.Empty;
                    wf.LogPrint($"納品データ3柔整疑義CSV作成 バッチ番号(ナンバリング): {item[0]}");
                    for (int c = 0; c < item.Length; c++)
                    {
                        strLine += $"{item[c].ToString()},";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }

                wf.LogPrint($"納品データ3柔整疑義CSV作成完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 納品データ3,5疑義テーブル作成(テーブルは柔整あはき兼用)
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_gigi(List<App> lst, int cym, WaitForm wf)
        {
            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd = null;
            try
            {
                cmd = DB.Main.CreateCmd($"truncate table exp_gigi", tran);
                wf.LogPrint($"納品データ3,5疑義テーブル削除");
                cmd.TryExecuteNonQuery();

                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //・こちらのCSVには過誤の情報のみ反映させていただきたいです。照会結果については出力後にExcel上で編集します。

                    if (item.StatusFlagCheck(StatusFlag.過誤)) 
                    //if(item.StatusFlagCheck(StatusFlag.照会対象) || item.StatusFlagCheck(StatusFlag.点検対象))
                    {
                        wf.LogPrint($"納品データ3,5疑義テーブル作成 バッチ番号: {item.Numbering}");

                        exp_gigi cls = new exp_gigi();

                        cls.apptype = Scan.Select(item.ScanID).AppType;
                        cls.f001_batch = item.Numbering;
                        cls.f003_uketsukebi = DateTime.Now.ToString("yyyy/MM/dd");


                        if (!item.HenreiReasonsCheck(HenreiReasons.なし))
                        {
                            cls.f002_gigi = "2";//疑義区分＝２
                            cls.f005_giginaiyo = item.HenreiReasonStr;
                        }

                        //・2疑義区分に過誤になっていないものにも「1」が入っています。
                        else if (!item.KagoReasonsCheck_xml(KagoReasons_Member.なし)&& item.StatusFlagCheck(StatusFlag.過誤))
                        {
                            cls.f002_gigi = "1";//疑義区分＝１

                            //・5疑義内容には過誤理由と点検メモを反映させていただきたいです
                            cls.f005_giginaiyo = $"{item.KagoReasonStr}/{item.MemoInspect}";


                            //cls.f005_giginaiyo = item.KagoReasonStr;
                        }

                        if (item.StatusFlagCheck(StatusFlag.返戻)) cls.f004_henrei = "1";

                        if (!DB.Main.Insert<exp_gigi>(cls, tran)) return false;

                    }

                    wf.InvokeValue++;
                }
                wf.LogPrint($"納品データ3,5疑義テーブル作成完了");
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }



        /// <summary>
        /// 納品データ4あはき申請書情報CSV作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_ahk_csv(int cym, string strDir, WaitForm wf)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strDir}\\AHAKI_{cym}.csv", false, System.Text.Encoding.GetEncoding("utf-8"));
            string strHeader = $"バッチ番号,処理年月,施術年月,データ区分,調査照会番号,被保険者番号," +
                $"被保険者氏名,被保険者郵便番号,被保険者住所,初検年月日,施術日数,同意年月日," +
                $"一部負担金,費用額,登録記号番号,施術所名,施術月数,変形徒手施術区分,長期施術区分,往療区分,頻回施術区分";

            DB.Command cmd = null;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select ");
                sb.Append(" f001_batch");
                sb.Append(",f002_shoriym");
                sb.Append(",f003_mediym");
                sb.Append(",f004_datakbn");
                sb.Append(",f005_shokainum");
                sb.Append(",f006_hihonum");
                sb.Append(",f007_hihoname");
                sb.Append(",f008_hihozip");
                sb.Append(",f009_hihoadd");
                sb.Append(",f010_firstdate");
                sb.Append(",f011_counteddays");
                sb.Append(",f012_doui");
                sb.Append(",f013_partial");
                sb.Append(",f014_total");
                sb.Append(",f015_drnum");
                sb.Append(",f016_clinicname");
                sb.Append(",f017_months");
                sb.Append(",f018_henkei");
                sb.Append(",f019_choki");
                sb.Append(",f020_ouryo");
                sb.Append(",f021_hinkai");
                sb.Append(" from exp_ahk order by f000_id");

                
                cmd = DB.Main.CreateCmd(sb.ToString());
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                sw.WriteLine(strHeader);

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf))
                    {
                        sw.Close();
                        cmd.Dispose();
                        return false;
                    }
                    string strLine = string.Empty;
                    wf.LogPrint($"納品データ4あはき申請書情報CSV作成 バッチ番号: {item[0]}");
                    for (int c = 0; c < item.Length; c++)
                    {
                        strLine += $"{item[c].ToString()},";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }

                wf.LogPrint($"納品データ4あはき申請書情報CSV作成完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 納品データ4あはき申請書情報テーブル作成
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool Create_exp_ahk(List<App> lst, int cym, WaitForm wf)
        {
            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd = null;
            try
            {
                cmd = DB.Main.CreateCmd($"truncate table exp_ahk", tran);
                wf.LogPrint($"納品データ4あはき申請書情報テーブル削除");
                cmd.TryExecuteNonQuery();
                
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                foreach (App item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //あはき申請書のみ必要
                    if (!new APP_TYPE[] { APP_TYPE.あんま, APP_TYPE.鍼灸 }.Contains(item.AppType)) continue;
                    //if (Scan.Select(item.ScanID).AppType != APP_TYPE.あんま && Scan.Select(item.ScanID).AppType != APP_TYPE.鍼灸) continue;

                    wf.LogPrint($"納品データ4あはき申請書情報テーブル作成 バッチ番号(ナンバリング): {item.Numbering}");

                    exp_ahk cls = new exp_ahk();
                    cls.f001_batch = item.Numbering;
                    cls.f002_shoriym = DateTimeEx.ToDateTime(item.CYM.ToString() + "01").ToString("yyyy/MM/dd");
                    cls.f003_mediym = DateTimeEx.ToDateTime(item.YM.ToString() + "01").ToString("yyyy/MM/dd");
                    cls.f004_datakbn = Scan.Select(item.ScanID).AppType == APP_TYPE.あんま ? "9" : "8";

                    cls.f005_shokainum = item.ShokaiCode;
                    cls.f006_hihonum = item.HihoNum;
                    cls.f007_hihoname = item.HihoName;
                    cls.f008_hihozip = item.HihoZip;
                    cls.f009_hihoadd = item.HihoAdd;
                    cls.f010_firstdate = item.FushoFirstDate1.ToString("yyyy/MM/dd");
                    cls.f011_counteddays = item.CountedDays.ToString();
                    cls.f012_doui = item.TaggedDatas.DouiDate.ToString("yyyy/MM/dd");
                    cls.f013_partial = item.Partial.ToString();
                    cls.f014_total = item.Total.ToString();
                    cls.f015_drnum = item.DrNum;
                    cls.f016_clinicname = item.ClinicName;

                    //月で計算。年またぎは年数×12を足すことで解消。終了年月日がない場合はあり得ないが、少なくともその月は通院しているので1とする
                    int ts = 0;
                    if (item.FushoFinishDate1 == DateTime.MinValue) ts = 1;// DateTime.Now.Month - item.FushoFirstDate1.Month + (12 * (DateTime.Now.Year - item.FushoFirstDate1.Year)) + 1;
                    else ts = item.FushoFinishDate1.Month - item.FushoFirstDate1.Month + (12 * (item.FushoFinishDate1.Year - item.FushoFirstDate1.Year)) + 1;

                    cls.f017_months = ts.ToString();

                    //変形徒手
                    //・18変形徒手区分が反映されていないので変形徒手チェックがあるものは「1」が入るようにしていただきたいです。
                    cls.f018_henkei = item.TaggedDatas.GeneralString8 == "True" ? "1":string.Empty;
                    cls.f019_choki=item.StatusFlagCheck(StatusFlag.処理2) == true ? "1" : string.Empty;//22はリスト作成から検索し、何かしらのフラグ（処理2など）を立てる予定でした。

                    
                    cls.f020_ouryo = item.Distance == 999 ? "1" : string.Empty;

                    if (cls.f017_months != string.Empty && cls.f011_counteddays != string.Empty) {

                        //17施術月数が3以上になるもので、11施術日数が15以上のものに頻回フラグを立てていただけないでしょうか？
                        
                        if ((int.Parse(cls.f017_months) >= 3) && (int.Parse(cls.f011_counteddays) >= 15)) cls.f021_hinkai = "1";

                        //こっちは不要
                        //cls.f021_hinkai = checkHinkai(cls.f006_hihonum);
                    }

                    if (!DB.Main.Insert<exp_ahk>(cls,tran)) return false;
                    wf.InvokeValue++;
                }
                tran.Commit();
                wf.LogPrint($"納品データ4あはき申請書情報テーブル作成完了");

                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }



        /// <summary>
        /// 2ヶ月連続で診療日数が15日以上か調査
        /// </summary>
        /// <param name="strHihoNum"></param>
        /// <returns></returns>
        private static string checkHinkai(string strHihoNum)
        {
            DB.Command cmd = null;
            List<App> lsthinkai = new List<App>();
            try
            {
                cmd = DB.Main.CreateCmd($"select aid,acounteddays from application where hnum='{strHihoNum}' " +
                    $"group by aid,acounteddays,ym order by aid desc limit 2");
                var lstapp = cmd.TryExecuteReaderList();

                foreach (var item in lstapp)
                {
                    App cls = new App();
                    cls.Aid = int.Parse(item[0].ToString());
                    cls.CountedDays = int.Parse(item[1].ToString());
                    lsthinkai.Add(cls);

                }

                //1件の場合は連続していないので１固定
                //・21頻回施術区分が15日未満のものもフラグが立っている状態です。
                if (lsthinkai.Count == 1 && lsthinkai[0].CountedDays >= 15)
                {
                    return "1";
                }
                else if(lsthinkai.Count == 1 && lsthinkai[0].CountedDays < 15)
                {
                    return string.Empty;
                }
                

                //ありえないが一応
                if (lsthinkai.Count == 0) return string.Empty;

                //直近2ヶ月の診療日数が両方15以上の場合1
                if(lsthinkai[0].CountedDays>=15 && lsthinkai[1].CountedDays >= 15)
                {
                    return "1";
                }
                else
                {
                    return string.Empty;
                }

               

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return String.Empty;
            }
            finally
            {
                cmd.Dispose();
            }
        }



      
      

        /// <summary>
        /// 不要以外画像出力
        /// </summary>
        /// <param name="lst">出力するリスト</param>
        /// <param name="strDirAHK">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        //private static bool ExportImage(List<App> lst, string strDir, WaitForm wf)
        //{
        //    string imageName = string.Empty;                    //tifコピー先の名前
        //    string strImageDir = $"{strDirAHK}\\Images";           //tifコピー先フォルダ

        //    if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

        //    List<string> lstImageFilePath = new List<string>();

        //    TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
        //    wf.InvokeValue = 0;
        //    wf.SetMax(lst.Count);

        //    try
        //    {
        //        //ファイル名はナンバリング6桁前ゼロ埋め
        //        //0番目のファイルパス
        //        imageName = $"{strImageDir}\\{lst[0].Numbering.PadLeft(6,'0')}.tif";

        //        for (int r = 0; r < lst.Count(); r++)
        //        {

        //            //apptypeが以下の場合、申請書と見なす
        //            if ((new APP_TYPE[]{APP_TYPE.柔整,APP_TYPE.あんま,APP_TYPE.鍼灸 }.Contains(lst[r].AppType)) && (lstImageFilePath.Count != 0))
        //            {
        //                //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
        //                //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
        //                //その上で、この申請書をマルチTIFF候補リストに追加
        //                wf.LogPrint($"申請書出力中:{imageName}");
        //                if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
        //                {
        //                    System.Windows.Forms.MessageBox.Show(
        //                        System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
        //                    return false;
        //                }

        //                lstImageFilePath.Clear();
        //                lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
        //                imageName = $"{strImageDir}\\{lst[r].Numbering.PadLeft(6, '0')}.tif";
        //            }
        //            else if ((new APP_TYPE[] { APP_TYPE.柔整, APP_TYPE.あんま, APP_TYPE.鍼灸 }.Contains(lst[r].AppType)) && (lstImageFilePath.Count == 0))
        //            {
        //                //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
        //                lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
        //                wf.LogPrint($"申請書出力中:{imageName}");
        //            }

        //            else if (new APP_TYPE[] { 
        //                APP_TYPE.総括票, APP_TYPE.続紙,APP_TYPE.同意書,APP_TYPE.往療内訳,
        //                APP_TYPE.長期,APP_TYPE.施術報告書,APP_TYPE.状態記入書,APP_TYPE.同意書裏}.Contains(lst[r].AppType))
        //            {
        //                //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
        //                lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
        //                wf.LogPrint($"申請書以外:{imageName}");
        //            }


        //            wf.InvokeValue++;

        //        }


        //        //最終画像出力
        //        //ループの最後の画像を出力
        //        if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
        //            TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
        //        return false;
        //    }

        //}


        /// <summary>
        /// 納品データ作成
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strOutputPath">出力先</param>
        /// <param name="strKetteibi">支給決定日（GYYMMDD)</param>
        /// <returns></returns>
        //private static bool CreateExportData(List<App> lst,int cym, string strOutputPath,string strKetteibi,WaitForm wf)
        //{

        //    //出力ファイル名            
        //    string strFileName = $"{strOutputPath}\\{DateTime.Now.ToString("yyyyMMdd")}";

           
        //    lst.OrderBy(item => item.PayCode).ThenBy(item => int.Parse(item.Numbering));


        //    System.Text.StringBuilder sb = new StringBuilder();
        //    //ナンバリングの空欄は0扱いにして数値にして比較
        //    sb.AppendLine($" select ");
        //    sb.AppendLine($" min(cast(");
        //    sb.AppendLine($" 	case numbering when '' then 0 else cast(numbering as int) end");
        //    sb.AppendLine($" as int))");
        //    sb.AppendLine($" ,max(cast(");
        //    sb.AppendLine($" 	case numbering when '' then 0 else cast(numbering as int) end ");
        //    sb.AppendLine($" 	as int))");
        //    sb.AppendLine($"  from application");
        //    sb.AppendLine($"  where");
        //    sb.AppendLine($"  cym={cym}");
        //    sb.AppendLine($"  and numbering <>''");

        //    DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
        //    var l = cmd.TryExecuteReaderList();
            
        //    string strfirstnum = l[0][0].ToString();
        //    string strfinishnum = l[0][1].ToString();

        //    //20220420180016 furukawa st ////////////////////////
        //    //ANSI＝シフトJISで作る
            
        //    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("shift-jis"));
        //    //System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("UTF-8"));
        //    //20220420180016 furukawa ed ////////////////////////


        //    wf.SetMax(lst.Count);

        //    try
        //    {

        //        foreach (App item in lst)
        //        {
        //            //申請書以外飛ばす
        //            if ((int)item.AppType < 0) continue;

        //            wf.LogPrint($"出力中 AID {item.Aid}");

        //            clsExport exp = new clsExport();
        //            exp.f001_kbn = "2";
        //            exp.f002_code = item.PayCode.PadLeft(9, '0');
        //            exp.f003_firstnum = strfirstnum.PadLeft(6, '0');
        //            exp.f004_finishnum = strfinishnum.PadLeft(6, '0');
        //            exp.f005_ketteibi = strKetteibi.PadLeft(7, '0');
        //            exp.f006_maisu = lst.Count.ToString().PadLeft(4, '0');
        //            exp.f007_skip = string.Empty.PadLeft(1, ' ');
        //            exp.f008_kbn2 = "7";
        //            exp.f009_skip = string.Empty.PadLeft(1, ' ');


        //            string fushofinishW = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoFinishDate1).ToString();
        //            if (fushofinishW == "0") fushofinishW = "0".PadLeft(7, '0');
        //            string strFushoFinishY = fushofinishW.Substring(1, 2);
        //            string strFushoFinishM = fushofinishW.Substring(3, 2);
        //            string strFushoFinishD = fushofinishW.Substring(5, 2);

        //            exp.f010_year = strFushoFinishY.PadLeft(2, '0');
        //            exp.f011_month = strFushoFinishM.PadLeft(2, '0');
        //            exp.f012_insnum = item.InsNum==string.Empty ? string.Empty.PadLeft(2,'0'):item.InsNum.Substring(0, 2);


        //            exp.f013_hnum = item.HihoNum == string.Empty ? string.Empty.PadLeft(6,'0'):item.HihoNum.Substring(item.HihoNum.Length - 6, 6);
        //            exp.f014_skip3 = string.Empty.PadLeft(5, ' ');
        //            exp.f015_gender = item.Sex.ToString();

        //            string birthdayW = DateTimeEx.GetIntJpDateWithEraNumber(item.Birthday).ToString();
        //            if (birthdayW == "0") birthdayW = "0".PadLeft(7, '0');
        //            string birthNengo = birthdayW.Substring(0, 1);
        //            string birthYearW = birthdayW.Substring(1, 2);
        //            exp.f016_birthnengo = birthNengo;
        //            exp.f017_birthyear = birthYearW;

        //            string fushoStartW = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();
        //            if (fushoStartW == "0") fushoStartW = "0".PadLeft(7, '0');
        //            string strFushoStartY = fushoStartW.Substring(1, 2);
        //            string strFushoStartM = fushoStartW.Substring(3, 2);
        //            string strFushoStartD = fushoStartW.Substring(5, 2);

        //            exp.f018_startyear = strFushoStartY;
        //            exp.f019_startmonth = strFushoStartM;
        //            exp.f020_startday = strFushoStartD;

        //            exp.f021_finishyear = strFushoFinishY;
        //            exp.f022_finishmonth = strFushoFinishM;
        //            exp.f023_finishday = strFushoFinishD;

        //            exp.f024_counteddays = item.CountedDays.ToString().PadLeft(2, '0');
        //            exp.f025_total = item.Total.ToString().PadLeft(6, '0');
        //            exp.f026_chrage = item.Charge.ToString().PadLeft(6, '0');
        //            exp.f027_number = item.Numbering.PadLeft(6, '0');

                               
        //            if (item.AppType == APP_TYPE.柔整)
        //                //20220428112647 furukawa st ////////////////////////
        //                //登録記号番号：柔整＝2桁目から9文字、あはき＝1～8桁＋10桁目の9文字
        //                exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : item.DrNum.Substring(1, 9).PadLeft(9, '0');
        //                //      exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : item.DrNum.Substring(0, 9).PadLeft(9, '0');
        //                //20220428112647 furukawa ed ////////////////////////
        //            else if (item.AppType == APP_TYPE.あんま || item.AppType == APP_TYPE.鍼灸)
        //                exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : (item.DrNum.Substring(0, 8) + item.DrNum.Substring(9, 1)).PadLeft(9, '0');

                    

        //            if (item.TaggedDatas.KouhiNum == string.Empty) exp.f029_fukushi = "00";
        //            else exp.f029_fukushi = item.TaggedDatas.KouhiNum.Substring(0, 2);


        //            sw.WriteLine(CreateLine(exp));

        //        }


        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        sw.Close();
        //        cmd.Dispose();
        //    }

        //}

        /// <summary>
        /// 1行作成
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        //private static string CreateLine(clsExport exp)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    sb.Append(exp.f001_kbn);
        //    sb.Append(exp.f002_code);
        //    sb.Append(exp.f003_firstnum);
        //    sb.Append(exp.f004_finishnum);
        //    sb.Append(exp.f005_ketteibi);
        //    sb.Append(exp.f006_maisu);
        //    sb.Append(exp.f007_skip);
        //    sb.Append(exp.f008_kbn2);
        //    sb.Append(exp.f009_skip);
        //    sb.Append(exp.f010_year);
        //    sb.Append(exp.f011_month);
        //    sb.Append(exp.f012_insnum);
        //    sb.Append(exp.f013_hnum);
        //    sb.Append(exp.f014_skip3);
        //    sb.Append(exp.f015_gender);
        //    sb.Append(exp.f016_birthnengo);
        //    sb.Append(exp.f017_birthyear);
        //    sb.Append(exp.f018_startyear);
        //    sb.Append(exp.f019_startmonth);
        //    sb.Append(exp.f020_startday);
        //    sb.Append(exp.f021_finishyear);
        //    sb.Append(exp.f022_finishmonth);
        //    sb.Append(exp.f023_finishday);
        //    sb.Append(exp.f024_counteddays);
        //    sb.Append(exp.f025_total);
        //    sb.Append(exp.f026_chrage);
        //    sb.Append(exp.f027_number);
        //    sb.Append(exp.f028_sregnumber);
        //    sb.Append(exp.f029_fukushi);

        //    return sb.ToString();
        //}


    }
}
