﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.IbarakiKoiki

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName_TD01 = frm.strFileTD01;
            string strFileName_TD24 = frm.strFileTD24;
            string strFileName_clinicMaster = frm.strFileClinicMaster;
            string strFileName_hihoMaster = frm.strFileHihoMaster;

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                
                if (strFileName_TD01 != string.Empty)
                {
                    wf.LogPrint("提供データ_社団以外TD01インポート");
                    if (!EntryCSVData_TD01(strFileName_TD01,_cym, wf))
                    {
                        wf.LogPrint("提供データ_社団以外TD01インポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_社団以外TD01インポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_社団以外TD01インポート終了");

                }
                else
                {
                    wf.LogPrint("提供データ_社団以外TD01ファイルのファイルが指定されていないため処理しない");
                }



                if (strFileName_TD24 != string.Empty)
                {
                    wf.LogPrint("提供データ_社団以外TD24インポート");
                    if (!EntryCSVData_TD24(strFileName_TD24, _cym, wf))
                    {
                        wf.LogPrint("提供データ_社団以外TD24インポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_社団以外TD24インポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_社団以外TD24インポート終了");

                }
                else
                {
                    wf.LogPrint("提供データ_社団以外TD24ファイルのファイルが指定されていないため処理しない");
                }


                if (strFileName_clinicMaster != string.Empty)
                {
                    wf.LogPrint("提供データ_療養費機関マスタインポート");
                    if (!EntryCSVData_clinicMaster(strFileName_clinicMaster, _cym, wf))
                    {
                        wf.LogPrint("提供データ_療養費機関マスタインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_療養費機関マスタインポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_療養費機関マスタインポート終了");

                }
                else
                {
                    wf.LogPrint("提供データ_療養費機関マスタファイルのファイルが指定されていないため処理しない");
                }


                if (strFileName_hihoMaster != string.Empty)
                {
                    wf.LogPrint("提供データ_被保険者マスタインポート");
                    if (!EntryCSVData_hihoMaster(strFileName_hihoMaster, _cym, wf))
                    {
                        wf.LogPrint("提供データ_被保険者マスタインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_被保険者マスタインポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_被保険者マスタインポート終了");

                }
                else
                {
                    wf.LogPrint("提供データ_被保険者マスタファイルのファイルが指定されていないため処理しない");
                }






                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                return;
            }
            finally
            {
                wf.Dispose();
            }
        }




        /// <summary>
        /// CSV取込
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData_TD01(string strFileName, int intcym,WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_ryouyouhi where cym={intcym} and filekbn='TD01'", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.InvokeValue = 0;
                wf.SetMax(lst.Count);

                if (lst[0].Length != 139)
                {
                    System.Windows.Forms.MessageBox.Show("列数が139ではありません。取り込めません。ファイルの中身を確認してください"
                                            , System.Windows.Forms.Application.ProductName
                                            , System.Windows.Forms.MessageBoxButtons.OK
                                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                    return false;
                }

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {
                    
                    //キャンセルプロセス

                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }
                    
                    try
                    {
                        //被保番を判断材料とする
                        int.Parse(tmp[4].ToString());
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    imp_ryouyouhi cls = new imp_ryouyouhi();

                    cls.f001_chargeym = tmp[0].Trim();
                    cls.f002_rezenum = tmp[1].Trim();
                    cls.f003_hihonum = tmp[4].Trim();
                    cls.f004_kind = tmp[5].Trim();
                    cls.f005_kbn = tmp[6].Trim();
                    cls.f006_mediym = tmp[7].Trim();
                    cls.f007_pref = tmp[8].Trim();
                    cls.f008_city = tmp[9].Trim();
                    cls.f009_clinicnum = tmp[10].Trim();
                    cls.f010_insnum = tmp[13].Trim();
                    cls.f011_gender = tmp[14].Trim();
                    cls.f012_pbirthdayw = tmp[15].Trim();
                    cls.f013_atenanum = tmp[16].Trim();
                    cls.f014_startdate1 = tmp[23].Trim();
                    cls.f015_counteddays = tmp[29].Trim();
                    cls.f016_ratio = tmp[30].Trim();
                    cls.f017_total = tmp[90].Trim();
                    cls.f018_charge = tmp[91].Trim();
                    cls.f019_partial = tmp[96].Trim();
                    cls.f020_mngnum = tmp[120].Trim();

                    cls.cym = intcym;//メホール請求年月管理用;

                    cls.pbirthdayad = DateTimeEx.GetDateFromJstr7(cls.f012_pbirthdayw);
                    cls.startdate1ad = DateTimeEx.ToDateTime(cls.f014_startdate1);
                    cls.mediymad = int.Parse(cls.f006_mediym);
                    cls.clinicnum = cls.f007_pref + cls.f004_kind + cls.f008_city + cls.f009_clinicnum;//医療機関番号
                    cls.filekbn = "TD01";

                  

                    //トランザクションにしないと遅すぎ                    
                    if (!DB.Main.Insert<imp_ryouyouhi>(cls, tran)) return false;

                    wf.LogPrint($"療養費データ 電算管理番号 :{cls.f020_mngnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");


                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// CSV取込
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData_TD24(string strFileName, int intcym, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_ryouyouhi where cym={intcym} and filekbn='TD24'", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.InvokeValue = 0;
                wf.SetMax(lst.Count);

                if (lst[0].Length != 203)
                {
                    System.Windows.Forms.MessageBox.Show("列数が203ではありません。取り込めません。ファイルの中身を確認してください"
                                            , System.Windows.Forms.Application.ProductName
                                            , System.Windows.Forms.MessageBoxButtons.OK
                                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                    return false;
                }

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {

                    //キャンセルプロセス

                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }

                    try
                    {
                        //被保番を判断材料とする
                        int.Parse(tmp[4].ToString());
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.InvokeValue + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    imp_ryouyouhi cls = new imp_ryouyouhi();

                    cls.f001_chargeym = tmp[0].Trim();
                    cls.f002_rezenum = tmp[1].Trim();
                    cls.f003_hihonum = tmp[4].Trim();
                    cls.f004_kind = tmp[5].Trim();
                    cls.f005_kbn = tmp[6].Trim();
                    cls.f006_mediym = tmp[7].Trim();
                    cls.f007_pref = tmp[8].Trim();
                    cls.f008_city = tmp[9].Trim();
                    cls.f009_clinicnum = tmp[10].Trim();
                    cls.f010_insnum = tmp[13].Trim();
                    cls.f011_gender = tmp[14].Trim();
                    cls.f012_pbirthdayw = tmp[15].Trim();
                    cls.f013_atenanum = tmp[16].Trim();
                    cls.f014_startdate1 = tmp[23].Trim();
                    cls.f015_counteddays = tmp[29].Trim();
                    cls.f016_ratio = tmp[30].Trim();
                    cls.f017_total = tmp[90].Trim();
                    cls.f018_charge = tmp[91].Trim();
                    cls.f019_partial = tmp[96].Trim();
                    cls.f020_mngnum = tmp[120].Trim();

                    cls.cym = intcym;//メホール請求年月管理用;

                    cls.pbirthdayad = DateTimeEx.GetDateFromJstr7(cls.f012_pbirthdayw);
                    cls.startdate1ad = DateTimeEx.ToDateTime(cls.f014_startdate1);
                    cls.mediymad = int.Parse(cls.f006_mediym);
                    cls.clinicnum = cls.f007_pref + cls.f004_kind + cls.f008_city + cls.f009_clinicnum;//医療機関番号
                    cls.filekbn = "TD24";



                    //トランザクションにしないと遅すぎ                    
                    if (!DB.Main.Insert<imp_ryouyouhi>(cls, tran)) return false;

                    wf.LogPrint($"療養費データTD24 電算管理番号 :{cls.f020_mngnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");


                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// CSV取込
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData_clinicMaster(string strFileName, int intcym, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_clinicmaster where cym={intcym} ", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.InvokeValue = 0;
                wf.SetMax(lst.Count);

                if (lst[0].Length != 29)
                {
                    System.Windows.Forms.MessageBox.Show("列数が29ではありません。取り込めません。ファイルの中身を確認してください"
                                            , System.Windows.Forms.Application.ProductName
                                            , System.Windows.Forms.MessageBoxButtons.OK
                                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                    return false;
                }

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {

                    //キャンセルプロセス

                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }

                    try
                    {
                        //医療機関コードを判断材料とする
                        int.Parse(tmp[3].ToString());
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.InvokeValue + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    imp_clinicMaster cls = new imp_clinicMaster();

                    cls.f001_pref = tmp[1].ToString().Trim();
                    cls.f002_tensuhyo =       tmp[2].ToString().Trim();
                    cls.f003_clinicnum =      tmp[3].ToString().Trim();
                    cls.f004_clinictel =      tmp[4].ToString().Trim();
                    cls.f005_clinickana =     tmp[5].ToString().Trim();
                    cls.f006_clinicname  =    tmp[6].ToString().Trim();
                    cls.f007_cliniczip =      tmp[7].ToString().Trim();
                    cls.f008_clinicadd = tmp[8].ToString().Trim();


                    cls.cym = intcym;//メホール請求年月管理用;

                  
                    cls.clinicnum = cls.f001_pref + cls.f002_tensuhyo + cls.f003_clinicnum;//医療機関番号
                    



                    //トランザクションにしないと遅すぎ                    
                    if (!DB.Main.Insert<imp_clinicMaster>(cls, tran)) return false;

                    wf.LogPrint($"医療機関マスタ 医療機関コード :{cls.f003_clinicnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");


                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }



        /// <summary>
        /// 被保険者マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool EntryCSVData_hihoMaster(string strFileName,int _cym, WaitForm wf)
        {
            int intcym = _cym;

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);

            StringBuilder sb = new StringBuilder();
            //パーティションテーブル作成
            sb.AppendLine($"create table if not exists imp_hihomaster_{_cym} partition of imp_hihomaster for values in ({_cym}) ;");
            sb.AppendLine($"delete from imp_hihomaster where cym={intcym};");
            DB.Command cmd = new DB.Command(sb.ToString(), tran);
            if (!cmd.TryExecuteNonQuery()) return false;
            tran.Commit();


            tran = new DB.Transaction(DB.Main);

            //本番ファイルを加工用にコピー
            string strFileNameTmp = $"{System.IO.Path.GetDirectoryName(strFileName)}\\{System.IO.Path.GetFileNameWithoutExtension(strFileName)}_tmp.csv";
            System.IO.File.Copy(strFileName, strFileNameTmp, true);

            wf.LogPrint("加工用コピーファイルをロード");
            //加工用コピーファイルをロード
            var lst = CommonTool.CsvImportMultiCode(strFileNameTmp);
            
            try
            {

                wf.LogPrint("被保険者マスタインポート準備");
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileNameTmp, false);

                //加工用ファイルの加工
                for (int r = 0; r < lst.Count; r++)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        sw.Close();
                        return false;
                    }

                    //1行目と最後は削る
                    if (r == 0) continue;
                    if (r == lst.Count - 1) continue;

                    //列数確認 ファイルによって違う
                    int colcnt = 162;
                    if (lst[r].Length != colcnt)
                    {
                        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                        return false;
                    }


                    wf.LogPrint($"被保険者マスタインポート用加工 被保険者証番号: {lst[r][1].ToString()}");


                    string strLine = string.Empty;

                    strLine += $"{lst[r][1].ToString()},";      //被保険者番号
                    strLine += $"{lst[r][2].ToString()},";      //漢字氏名
                    strLine += $"{lst[r][3].ToString()},";      //カナ氏名
                    strLine += $"{lst[r][6].ToString()},";      //郵便番号
                    strLine += $"{lst[r][7].ToString()},";      //住所
                    strLine += $"{lst[r][10].ToString()},";     //発送用郵便番号
                    strLine += $"{lst[r][11].ToString()},";     //発送用住所                    
                    

                    strLine += $"{_cym}";
                    

                    
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }
                sw.Close();


                //必要項目だけに絞った加工用ファイルをテーブルにcopy
                wf.LogPrint($"インポート");
                if (!DirectImport(strFileNameTmp, intcym)) return false;

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

                cmd.Dispose();

            }


        }




        /// <summary>
        /// copyコマンドでインポート
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool DirectImport(string strFileName, int cym)
        {
            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.AppendLine($"copy imp_hihomaster(");
            sb.AppendLine($"f001_hihonum, f002_pname, f003_pkana, f004_pzip, f005_padd, f006_sendzip, f007_sendadd,cym )");
            sb.AppendLine($" from ");
            sb.AppendLine($" stdin with csv NULL AS ' ' encoding 'utf-8' ");

            if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
            {
                System.Windows.Forms.MessageBox.Show("CSV取込失敗",
                    System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }

            return true;

        }


    }


}
