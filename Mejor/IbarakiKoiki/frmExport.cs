﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.IbarakiKoiki
{
    public partial class frmExport : Form
    {
        public bool flgImage = false;
        public bool flgOther = false;

        public frmExport()
        {
            InitializeComponent();
        }

        private void buttonExp_Click(object sender, EventArgs e)
        {
            export.flgImage = checkBoxImage.Checked;
            export.flgOther = checkBoxOther.Checked;
            this.Close();
        }

    }
}
