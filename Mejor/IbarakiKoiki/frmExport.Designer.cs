﻿namespace Mejor.IbarakiKoiki
{
    partial class frmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxImage = new System.Windows.Forms.CheckBox();
            this.checkBoxOther = new System.Windows.Forms.CheckBox();
            this.buttonExp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkBoxImage
            // 
            this.checkBoxImage.AutoSize = true;
            this.checkBoxImage.Checked = true;
            this.checkBoxImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxImage.Location = new System.Drawing.Point(48, 38);
            this.checkBoxImage.Name = "checkBoxImage";
            this.checkBoxImage.Size = new System.Drawing.Size(255, 24);
            this.checkBoxImage.TabIndex = 11;
            this.checkBoxImage.Text = "画像ファイル・画像情報CSVファイル";
            this.checkBoxImage.UseVisualStyleBackColor = true;
            // 
            // checkBoxOther
            // 
            this.checkBoxOther.AutoSize = true;
            this.checkBoxOther.Checked = true;
            this.checkBoxOther.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOther.Location = new System.Drawing.Point(48, 64);
            this.checkBoxOther.Name = "checkBoxOther";
            this.checkBoxOther.Size = new System.Drawing.Size(147, 24);
            this.checkBoxOther.TabIndex = 11;
            this.checkBoxOther.Text = "その他CSVファイル";
            this.checkBoxOther.UseVisualStyleBackColor = true;
            // 
            // buttonExp
            // 
            this.buttonExp.Location = new System.Drawing.Point(86, 109);
            this.buttonExp.Name = "buttonExp";
            this.buttonExp.Size = new System.Drawing.Size(131, 48);
            this.buttonExp.TabIndex = 10;
            this.buttonExp.Text = "出力";
            this.buttonExp.UseVisualStyleBackColor = true;
            this.buttonExp.Click += new System.EventHandler(this.buttonExp_Click);
            // 
            // frmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 182);
            this.Controls.Add(this.checkBoxOther);
            this.Controls.Add(this.checkBoxImage);
            this.Controls.Add(this.buttonExp);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmExport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "データ出力";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxImage;
        private System.Windows.Forms.CheckBox checkBoxOther;
        private System.Windows.Forms.Button buttonExp;
    }
}