﻿namespace Mejor.IbarakiKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.labelBui = new System.Windows.Forms.Label();
            this.verifyBoxBui = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.labelF1 = new System.Windows.Forms.Label();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.labelF1FinishDay = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.labelF1Finish = new System.Windows.Forms.Label();
            this.labelDrCode2 = new System.Windows.Forms.Label();
            this.vcSejutu = new Mejor.VerifyCheckBox();
            this.verifyBoxClinicNum = new Mejor.VerifyBox();
            this.label20 = new System.Windows.Forms.Label();
            this.pDoui = new System.Windows.Forms.Panel();
            this.vbDouiG = new Mejor.VerifyBox();
            this.vbDouiD = new Mejor.VerifyBox();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDoui = new System.Windows.Forms.Label();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.labelDrCode = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyCheckBoxHenkei = new Mejor.VerifyCheckBox();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.panelNumbering = new System.Windows.Forms.Panel();
            this.verifyBoxNumberingHeader = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelHnum.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.panelNumbering.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 695);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(209, 35);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(123, 31);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(7, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(41, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(136, 35);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(29, 12);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 722);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 695);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 695);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-408, 695);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 695);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 722);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.panelNumbering);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 722);
            this.panelRight.TabIndex = 0;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(8, 605);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 80);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 259;
            this.dgv.TabStop = false;
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(931, 8);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 96);
            this.label6.TabIndex = 258;
            this.label6.Text = "続紙    : \"--\"     \r\n不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  : \"911\"\r\n状態" +
    "記入書  : \"921\"\r\n往療内訳表  : \"931\"\r\nその他      : \"999\"";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.labelBui);
            this.panelHnum.Controls.Add(this.verifyBoxBui);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.verifyBoxF1);
            this.panelHnum.Controls.Add(this.labelF1);
            this.panelHnum.Location = new System.Drawing.Point(444, 4);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(440, 65);
            this.panelHnum.TabIndex = 8;
            this.panelHnum.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelBui
            // 
            this.labelBui.AutoSize = true;
            this.labelBui.Location = new System.Drawing.Point(390, 7);
            this.labelBui.Name = "labelBui";
            this.labelBui.Size = new System.Drawing.Size(41, 12);
            this.labelBui.TabIndex = 275;
            this.labelBui.Text = "部位数";
            // 
            // verifyBoxBui
            // 
            this.verifyBoxBui.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBui.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBui.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBui.Location = new System.Drawing.Point(393, 19);
            this.verifyBoxBui.MaxLength = 2;
            this.verifyBoxBui.Name = "verifyBoxBui";
            this.verifyBoxBui.NewLine = false;
            this.verifyBoxBui.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxBui.TabIndex = 60;
            this.verifyBoxBui.TextV = "";
            this.verifyBoxBui.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(7, 20);
            this.verifyBoxHnum.MaxLength = 8;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnum.TabIndex = 22;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1.Location = new System.Drawing.Point(169, 19);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF1.TabIndex = 57;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxF1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxF1_KeyPress);
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(120, 26);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(47, 12);
            this.labelF1.TabIndex = 258;
            this.labelF1.Text = "負傷名1";
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.labelF1FinishDay);
            this.panelTotal.Controls.Add(this.verifyBoxF1Finish);
            this.panelTotal.Controls.Add(this.labelF1Finish);
            this.panelTotal.Controls.Add(this.labelDrCode2);
            this.panelTotal.Controls.Add(this.vcSejutu);
            this.panelTotal.Controls.Add(this.verifyBoxClinicNum);
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.pDoui);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.labelDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.verifyCheckBoxHenkei);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Location = new System.Drawing.Point(5, 484);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1000, 118);
            this.panelTotal.TabIndex = 20;
            this.panelTotal.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelF1FinishDay
            // 
            this.labelF1FinishDay.AutoSize = true;
            this.labelF1FinishDay.Location = new System.Drawing.Point(499, 19);
            this.labelF1FinishDay.Name = "labelF1FinishDay";
            this.labelF1FinishDay.Size = new System.Drawing.Size(17, 12);
            this.labelF1FinishDay.TabIndex = 275;
            this.labelF1FinishDay.Text = "日";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(470, 13);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 63;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelF1Finish
            // 
            this.labelF1Finish.AutoSize = true;
            this.labelF1Finish.Location = new System.Drawing.Point(427, 19);
            this.labelF1Finish.Name = "labelF1Finish";
            this.labelF1Finish.Size = new System.Drawing.Size(41, 12);
            this.labelF1Finish.TabIndex = 274;
            this.labelF1Finish.Text = "終了日";
            // 
            // labelDrCode2
            // 
            this.labelDrCode2.AutoSize = true;
            this.labelDrCode2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDrCode2.Location = new System.Drawing.Point(911, 67);
            this.labelDrCode2.Name = "labelDrCode2";
            this.labelDrCode2.Size = new System.Drawing.Size(37, 24);
            this.labelDrCode2.TabIndex = 270;
            this.labelDrCode2.Text = "[協]: 0\r\n[契]: 1";
            // 
            // vcSejutu
            // 
            this.vcSejutu.BackColor = System.Drawing.SystemColors.Info;
            this.vcSejutu.CheckedV = false;
            this.vcSejutu.Location = new System.Drawing.Point(310, 12);
            this.vcSejutu.Name = "vcSejutu";
            this.vcSejutu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.vcSejutu.Size = new System.Drawing.Size(95, 24);
            this.vcSejutu.TabIndex = 60;
            this.vcSejutu.Text = "交付料あり";
            this.vcSejutu.UseVisualStyleBackColor = false;
            this.vcSejutu.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxClinicNum
            // 
            this.verifyBoxClinicNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicNum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxClinicNum.Location = new System.Drawing.Point(528, 68);
            this.verifyBoxClinicNum.MaxLength = 10;
            this.verifyBoxClinicNum.Name = "verifyBoxClinicNum";
            this.verifyBoxClinicNum.NewLine = false;
            this.verifyBoxClinicNum.Size = new System.Drawing.Size(160, 23);
            this.verifyBoxClinicNum.TabIndex = 75;
            this.verifyBoxClinicNum.TextV = "";
            this.verifyBoxClinicNum.Visible = false;
            this.verifyBoxClinicNum.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(479, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 269;
            this.label20.Text = "施術\r\n所番号";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label20.Visible = false;
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Controls.Add(this.label8);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Location = new System.Drawing.Point(3, 49);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(300, 46);
            this.pDoui.TabIndex = 73;
            this.pDoui.Enter += new System.EventHandler(this.item_Enter);
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(92, 9);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(247, 9);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(195, 9);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(141, 9);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(52, 7);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 24);
            this.label8.TabIndex = 84;
            this.label8.Text = "4:平成\r\n5:令和";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Enabled = false;
            this.lblDoui.Location = new System.Drawing.Point(8, 7);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(41, 24);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(225, 18);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(17, 12);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(278, 18);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(17, 12);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(173, 18);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(17, 12);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrCode.Location = new System.Drawing.Point(787, 68);
            this.verifyBoxDrCode.MaxLength = 10;
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxDrCode.TabIndex = 100;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxDrCode.Validated += new System.EventHandler(this.verifyBoxDrCode_Validated);
            // 
            // labelDrCode
            // 
            this.labelDrCode.AutoSize = true;
            this.labelDrCode.Location = new System.Drawing.Point(726, 68);
            this.labelDrCode.Name = "labelDrCode";
            this.labelDrCode.Size = new System.Drawing.Size(53, 24);
            this.labelDrCode.TabIndex = 260;
            this.labelDrCode.Text = "柔整師\r\n登録番号";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(579, 13);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 65;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxTotal.Validated += new System.EventHandler(this.verifyBoxTotal_Validated);
            // 
            // verifyCheckBoxHenkei
            // 
            this.verifyCheckBoxHenkei.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxHenkei.CheckedV = false;
            this.verifyCheckBoxHenkei.Location = new System.Drawing.Point(11, 12);
            this.verifyCheckBoxHenkei.Name = "verifyCheckBoxHenkei";
            this.verifyCheckBoxHenkei.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxHenkei.Size = new System.Drawing.Size(87, 24);
            this.verifyCheckBoxHenkei.TabIndex = 50;
            this.verifyCheckBoxHenkei.Text = "変形徒手";
            this.verifyCheckBoxHenkei.UseVisualStyleBackColor = false;
            this.verifyCheckBoxHenkei.Enter += new System.EventHandler(this.item_Enter);
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(211, 12);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisitKasan.TabIndex = 56;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(548, 19);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 37;
            this.labelTotal.Text = "合計";
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(112, 12);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisit.TabIndex = 55;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(170, 24);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 104);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 379);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(289, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // panelNumbering
            // 
            this.panelNumbering.Controls.Add(this.verifyBoxNumberingHeader);
            this.panelNumbering.Controls.Add(this.verifyBoxNumbering);
            this.panelNumbering.Controls.Add(this.labelNumbering);
            this.panelNumbering.Controls.Add(this.verifyBoxM);
            this.panelNumbering.Controls.Add(this.labelM);
            this.panelNumbering.Location = new System.Drawing.Point(146, 4);
            this.panelNumbering.Name = "panelNumbering";
            this.panelNumbering.Size = new System.Drawing.Size(290, 60);
            this.panelNumbering.TabIndex = 5;
            // 
            // verifyBoxNumberingHeader
            // 
            this.verifyBoxNumberingHeader.BackColor = System.Drawing.SystemColors.ControlLight;
            this.verifyBoxNumberingHeader.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumberingHeader.Location = new System.Drawing.Point(161, 20);
            this.verifyBoxNumberingHeader.MaxLength = 12;
            this.verifyBoxNumberingHeader.Name = "verifyBoxNumberingHeader";
            this.verifyBoxNumberingHeader.NewLine = false;
            this.verifyBoxNumberingHeader.Size = new System.Drawing.Size(65, 23);
            this.verifyBoxNumberingHeader.TabIndex = 264;
            this.verifyBoxNumberingHeader.TabStop = false;
            this.verifyBoxNumberingHeader.Text = "2022047";
            this.verifyBoxNumberingHeader.TextV = "";
            this.verifyBoxNumberingHeader.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.Location = new System.Drawing.Point(228, 20);
            this.verifyBoxNumbering.MaxLength = 5;
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(50, 23);
            this.verifyBoxNumbering.TabIndex = 8;
            this.verifyBoxNumbering.Text = "12345";
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxNumbering.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxNumbering_KeyPress);
            this.verifyBoxNumbering.Validated += new System.EventHandler(this.verifyBoxNumbering_Validated);
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(156, 5);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(59, 12);
            this.labelNumbering.TabIndex = 263;
            this.labelNumbering.Text = "ナンバリング";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(88, 20);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 722);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(661, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 276;
            this.label1.Text = "円";
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.panelNumbering.ResumeLayout(false);
            this.panelNumbering.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelF1;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Panel panelNumbering;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxDrCode;
        private System.Windows.Forms.Label labelDrCode;
        private System.Windows.Forms.Panel pDoui;
        private VerifyBox vbDouiG;
        private VerifyBox vbDouiD;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDoui;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private System.Windows.Forms.Label labelBui;
        private VerifyBox verifyBoxBui;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxClinicNum;
        private System.Windows.Forms.Label label20;
        private VerifyCheckBox vcSejutu;
        private VerifyCheckBox verifyCheckBoxHenkei;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labelDrCode2;
        private VerifyBox verifyBoxNumberingHeader;
        private System.Windows.Forms.Label labelF1FinishDay;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label labelF1Finish;
        private System.Windows.Forms.Label label1;
    }
}