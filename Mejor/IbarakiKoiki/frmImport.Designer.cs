﻿namespace Mejor.IbarakiKoiki
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnFile2 = new System.Windows.Forms.Button();
            this.btnFile1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxKyufu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImp = new System.Windows.Forms.Button();
            this.textBoxTD01 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFileTD01 = new System.Windows.Forms.Button();
            this.gbA = new System.Windows.Forms.GroupBox();
            this.btnFileClinicMaster = new System.Windows.Forms.Button();
            this.btnFileHihoMaster = new System.Windows.Forms.Button();
            this.btnFileTD24 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHihoMaster = new System.Windows.Forms.TextBox();
            this.textBoxTD24 = new System.Windows.Forms.TextBox();
            this.textBoxClinicMaster = new System.Windows.Forms.TextBox();
            this.gbJ.SuspendLayout();
            this.gbA.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxBase
            // 
            this.textBoxBase.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBase.Location = new System.Drawing.Point(180, 29);
            this.textBoxBase.Multiline = true;
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(512, 44);
            this.textBoxBase.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.Controls.Add(this.btnFile2);
            this.gbJ.Controls.Add(this.btnFile1);
            this.gbJ.Controls.Add(this.label2);
            this.gbJ.Controls.Add(this.textBoxKyufu);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxBase);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(43, 12);
            this.gbJ.Name = "gbJ";
            this.gbJ.Size = new System.Drawing.Size(786, 34);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            this.gbJ.Text = "柔整";
            this.gbJ.Visible = false;
            // 
            // btnFile2
            // 
            this.btnFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile2.Location = new System.Drawing.Point(698, 81);
            this.btnFile2.Name = "btnFile2";
            this.btnFile2.Size = new System.Drawing.Size(48, 15);
            this.btnFile2.TabIndex = 3;
            this.btnFile2.Text = "...";
            this.btnFile2.UseVisualStyleBackColor = true;
            this.btnFile2.Visible = false;
            // 
            // btnFile1
            // 
            this.btnFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile1.Location = new System.Drawing.Point(698, 30);
            this.btnFile1.Name = "btnFile1";
            this.btnFile1.Size = new System.Drawing.Size(48, 31);
            this.btnFile1.TabIndex = 3;
            this.btnFile1.Text = "...";
            this.btnFile1.UseVisualStyleBackColor = true;
            this.btnFile1.Click += new System.EventHandler(this.btnFile1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "給付記録データ";
            this.label2.Visible = false;
            // 
            // textBoxKyufu
            // 
            this.textBoxKyufu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKyufu.Location = new System.Drawing.Point(180, 81);
            this.textBoxKyufu.Multiline = true;
            this.textBoxKyufu.Name = "textBoxKyufu";
            this.textBoxKyufu.Size = new System.Drawing.Size(512, 28);
            this.textBoxKyufu.TabIndex = 0;
            this.textBoxKyufu.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "基本データ";
            // 
            // btnImp
            // 
            this.btnImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImp.Location = new System.Drawing.Point(743, 427);
            this.btnImp.Name = "btnImp";
            this.btnImp.Size = new System.Drawing.Size(115, 34);
            this.btnImp.TabIndex = 3;
            this.btnImp.Text = "取込";
            this.btnImp.UseVisualStyleBackColor = true;
            this.btnImp.Click += new System.EventHandler(this.btnImp_Click);
            // 
            // textBoxTD01
            // 
            this.textBoxTD01.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTD01.Location = new System.Drawing.Point(180, 27);
            this.textBoxTD01.Multiline = true;
            this.textBoxTD01.Name = "textBoxTD01";
            this.textBoxTD01.Size = new System.Drawing.Size(512, 45);
            this.textBoxTD01.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "社団以外TD01\r\n(CSV)";
            // 
            // btnFileTD01
            // 
            this.btnFileTD01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileTD01.Location = new System.Drawing.Point(698, 29);
            this.btnFileTD01.Name = "btnFileTD01";
            this.btnFileTD01.Size = new System.Drawing.Size(48, 31);
            this.btnFileTD01.TabIndex = 3;
            this.btnFileTD01.Text = "...";
            this.btnFileTD01.UseVisualStyleBackColor = true;
            this.btnFileTD01.Click += new System.EventHandler(this.btnFile3_Click);
            // 
            // gbA
            // 
            this.gbA.Controls.Add(this.btnFileClinicMaster);
            this.gbA.Controls.Add(this.btnFileHihoMaster);
            this.gbA.Controls.Add(this.btnFileTD24);
            this.gbA.Controls.Add(this.btnFileTD01);
            this.gbA.Controls.Add(this.label5);
            this.gbA.Controls.Add(this.label6);
            this.gbA.Controls.Add(this.label3);
            this.gbA.Controls.Add(this.label4);
            this.gbA.Controls.Add(this.textBoxHihoMaster);
            this.gbA.Controls.Add(this.textBoxTD24);
            this.gbA.Controls.Add(this.textBoxClinicMaster);
            this.gbA.Controls.Add(this.textBoxTD01);
            this.gbA.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbA.Location = new System.Drawing.Point(43, 59);
            this.gbA.Name = "gbA";
            this.gbA.Size = new System.Drawing.Size(786, 319);
            this.gbA.TabIndex = 1;
            this.gbA.TabStop = false;
            this.gbA.Text = "提供データ";
            // 
            // btnFileClinicMaster
            // 
            this.btnFileClinicMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileClinicMaster.Location = new System.Drawing.Point(698, 238);
            this.btnFileClinicMaster.Name = "btnFileClinicMaster";
            this.btnFileClinicMaster.Size = new System.Drawing.Size(48, 31);
            this.btnFileClinicMaster.TabIndex = 3;
            this.btnFileClinicMaster.Text = "...";
            this.btnFileClinicMaster.UseVisualStyleBackColor = true;
            this.btnFileClinicMaster.Click += new System.EventHandler(this.btnFileClinicMaster_Click);
            // 
            // btnFileHihoMaster
            // 
            this.btnFileHihoMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileHihoMaster.Location = new System.Drawing.Point(698, 168);
            this.btnFileHihoMaster.Name = "btnFileHihoMaster";
            this.btnFileHihoMaster.Size = new System.Drawing.Size(48, 31);
            this.btnFileHihoMaster.TabIndex = 3;
            this.btnFileHihoMaster.Text = "...";
            this.btnFileHihoMaster.UseVisualStyleBackColor = true;
            this.btnFileHihoMaster.Click += new System.EventHandler(this.btnFileHihoMaster_Click);
            // 
            // btnFileTD24
            // 
            this.btnFileTD24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileTD24.Location = new System.Drawing.Point(698, 85);
            this.btnFileTD24.Name = "btnFileTD24";
            this.btnFileTD24.Size = new System.Drawing.Size(48, 31);
            this.btnFileTD24.TabIndex = 3;
            this.btnFileTD24.Text = "...";
            this.btnFileTD24.UseVisualStyleBackColor = true;
            this.btnFileTD24.Click += new System.EventHandler(this.btnFileTD24_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 32);
            this.label5.TabIndex = 2;
            this.label5.Text = "被保険者マスタ\r\n(CSV) ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 32);
            this.label6.TabIndex = 2;
            this.label6.Text = "社団TD24\r\n(CSV)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "療養費機関マスタ\r\n(CSV)";
            // 
            // textBoxHihoMaster
            // 
            this.textBoxHihoMaster.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHihoMaster.Location = new System.Drawing.Point(180, 166);
            this.textBoxHihoMaster.Multiline = true;
            this.textBoxHihoMaster.Name = "textBoxHihoMaster";
            this.textBoxHihoMaster.Size = new System.Drawing.Size(512, 45);
            this.textBoxHihoMaster.TabIndex = 0;
            // 
            // textBoxTD24
            // 
            this.textBoxTD24.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTD24.Location = new System.Drawing.Point(180, 83);
            this.textBoxTD24.Multiline = true;
            this.textBoxTD24.Name = "textBoxTD24";
            this.textBoxTD24.Size = new System.Drawing.Size(512, 45);
            this.textBoxTD24.TabIndex = 0;
            // 
            // textBoxClinicMaster
            // 
            this.textBoxClinicMaster.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClinicMaster.Location = new System.Drawing.Point(180, 236);
            this.textBoxClinicMaster.Multiline = true;
            this.textBoxClinicMaster.Name = "textBoxClinicMaster";
            this.textBoxClinicMaster.Size = new System.Drawing.Size(512, 45);
            this.textBoxClinicMaster.TabIndex = 0;
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 482);
            this.Controls.Add(this.btnImp);
            this.Controls.Add(this.gbA);
            this.Controls.Add(this.gbJ);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.gbA.ResumeLayout(false);
            this.gbA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImp;
        private System.Windows.Forms.Button btnFile2;
        private System.Windows.Forms.Button btnFile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxKyufu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTD01;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFileTD01;
        private System.Windows.Forms.GroupBox gbA;
        private System.Windows.Forms.Button btnFileClinicMaster;
        private System.Windows.Forms.Button btnFileHihoMaster;
        private System.Windows.Forms.Button btnFileTD24;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHihoMaster;
        private System.Windows.Forms.TextBox textBoxTD24;
        private System.Windows.Forms.TextBox textBoxClinicMaster;
    }
}