﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.IbarakiKoiki
{
 
    /// <summary>
    /// 納品データ2柔整申請書情報
    /// </summary>
    public partial class exp_jyusei
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_id { get;set; }=0;                                       //ID;
        public string f001_batch { get; set; } = string.Empty;                   //バッチ番号;
        public string f002_shoriym { get; set; } = string.Empty;                 //処理年月;
        public string f003_mediym { get; set; } = string.Empty;                  //施術年月;
        public string f004_senju { get; set; } = string.Empty;                   //千住分区分;
        public string f005_shadan { get; set; } = string.Empty;                  //社団区分;
        public string f006_shokainum { get; set; } = string.Empty;               //調査照会番号;
        public string f007_hihonum { get; set; } = string.Empty;                 //被保険者番号;
        public string f008_hihoname { get; set; } = string.Empty;                //被保険者氏名;
        public string f009_hihozip { get; set; } = string.Empty;                 //被保険者郵便番号;
        public string f010_hihoadd { get; set; } = string.Empty;                 //被保険者住所;
        public string f011_firstdate { get; set; } = string.Empty;               //初検年月日;
        public string f012_finishdate { get; set; } = string.Empty;              //施術終了年月日;
        public string f013_counteddays { get; set; } = string.Empty;             //施術日数;
        public string f014_bui { get; set; } = string.Empty;                     //部位数;
        public string f015_partial { get; set; } = string.Empty;                 //一部負担金;
        public string f016_total { get; set; } = string.Empty;                   //費用額;
        public string f017_drnum { get; set; } = string.Empty;                   //登録記号番号;
        public string f018_clinicname { get; set; } = string.Empty;              //施術所名;
        public string f019_months { get; set; } = string.Empty;                  //施術月数;
        public string f020_continuous { get; set; } = string.Empty;              //継続月数;
        public string f021_daisansha { get; set; } = string.Empty;               //第三者行為区分;
        public string f022_choki { get; set; } = string.Empty;                   //長期施術区分;
        public string f023_ouryo { get; set; } = string.Empty;                   //往療区分;
        public string f024_hinkai { get; set; } = string.Empty;                  //頻回施術区分;
        public string f025_tabui { get; set; } = string.Empty;                   //多部位施術区分;

    }


    /// <summary>
    /// 納品データ4あはき申請書情報
    /// </summary>
    public partial class exp_ahk
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_id { get;set; }=0;                                        //ID;
        public string f001_batch { get; set; } = string.Empty;                    //バッチ番号;
        public string f002_shoriym { get; set; } = string.Empty;                  //処理年月;
        public string f003_mediym { get; set; } = string.Empty;                   //施術年月;
        public string f004_datakbn { get; set; } = string.Empty;                  //データ区分;
        public string f005_shokainum { get; set; } = string.Empty;                //調査照会番号;
        public string f006_hihonum { get; set; } = string.Empty;                  //被保険者番号;
        public string f007_hihoname { get; set; } = string.Empty;                 //被保険者氏名;
        public string f008_hihozip { get; set; } = string.Empty;                  //被保険者郵便番号;
        public string f009_hihoadd { get; set; } = string.Empty;                  //被保険者住所;
        public string f010_firstdate { get; set; } = string.Empty;                //初検年月日;
        public string f011_counteddays { get; set; } = string.Empty;              //施術日数;
        public string f012_doui { get; set; } = string.Empty;                     //同意年月日;
        public string f013_partial { get; set; } = string.Empty;                  //一部負担金;
        public string f014_total { get; set; } = string.Empty;                    //費用額;
        public string f015_drnum { get; set; } = string.Empty;                    //登録記号番号;
        public string f016_clinicname { get; set; } = string.Empty;               //施術所名;
        public string f017_months { get; set; } = string.Empty;                   //施術月数;
        public string f018_henkei { get; set; } = string.Empty;                   //変形徒手施術区分;
        public string f019_choki { get; set; } = string.Empty;                    //長期施術区分;
        public string f020_ouryo { get; set; } = string.Empty;                    //往療区分;
        public string f021_hinkai { get; set; } = string.Empty;                   //頻回施術区分;
    }

    /// <summary>
    /// 納品データ1申請書回答書紐付けデータ
    /// </summary>
    partial class exp_imagedata
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_id { get;set; }=0;                                   //ID;
        public string f001_batch { get; set; } = string.Empty;               //バッチ番号;
        public string f002_kbn { get; set; } = string.Empty;                 //用紙区分 申請書=1　回答書=2　申請書添付資料=99;
        public string f003_parentbatch { get; set; } = string.Empty;         //親バッチ番号;
        public int aid { get; set; } = 0;                                    //データ生成用;
        public string shokaicode { get; set; } = string.Empty;               //データ生成用;


    }


    /// <summary>
    /// 納品データ3,5柔整あはき疑義情報
    /// </summary>
    partial class exp_gigi
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
     
        public int f000_id { get;set; }=0;                                      //ID;
        public string f001_batch { get; set; } = string.Empty;                  //バッチ番号;
        public string f002_gigi { get; set; } = string.Empty;                   //疑義区分;
        public string f003_uketsukebi { get; set; } = string.Empty;             //回答文書受付日;
        public string f004_henrei { get; set; } = string.Empty;                 //返戻区分;
        public string f005_giginaiyo { get; set; } = string.Empty;              //疑義内容;
        public APP_TYPE apptype { get; set; } = APP_TYPE.NULL;                  //柔整/あはき;


    }

 

    /// <summary>
    /// 納品データ6往療距離
    /// </summary>
    partial class exp_ouryo
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_id { get;set; }=0;                                       //ID;
        public string f001_batch { get; set; } = string.Empty;                   //バッチ番号;
        public string f002_shoriym { get; set; } = string.Empty;                 //処理年月;
        public string f003_renban { get; set; } = string.Empty;                  //連番;
        public string f004_kiten { get; set; } = string.Empty;                   //起点住所;

        //20220618154943 furukawa st ////////////////////////
        //施術所ではなく施術した往診場所だった
        public string f005_place { get; set; } = string.Empty;                  //施術場所住所;
        //public string f005_clinic { get; set; } = string.Empty;                  //施術場所住所;
        //20220618154943 furukawa ed ////////////////////////

        public string f006_distance { get; set; } = string.Empty;                //直線距離;

    }
}
