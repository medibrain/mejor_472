﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.IbarakiKoiki
{
    /// <summary>
    /// 療養費データTD01、TD24
    /// </summary>
    public partial class imp_ryouyouhi
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                                      //インポートID;
        public string f001_chargeym { get; set; } = string.Empty;                     //1_請求年月;
        public string f002_rezenum { get; set; } = string.Empty;                      //2_簿冊レセプト番号;
        public string f003_hihonum { get; set; } = string.Empty;                      //5_被保険者番号;
        public string f004_kind { get; set; } = string.Empty;                         //6_レセプト種類コード;
        public string f005_kbn { get; set; } = string.Empty;                          //7_給付区分コード;
        public string f006_mediym { get; set; } = string.Empty;                       //8_診療年月;
        public string f007_pref { get; set; } = string.Empty;                         //9_都道府県コード;
        public string f008_city { get; set; } = string.Empty;                         //10_医療機関市区町村コード;
        public string f009_clinicnum { get; set; } = string.Empty;                    //11_医療機関コード;
        public string f010_insnum { get; set; } = string.Empty;                       //14_保険者番号;
        public string f011_gender { get; set; } = string.Empty;                       //15_性別コード;
        public string f012_pbirthdayw { get; set; } = string.Empty;                   //16_生年月日和暦;
        public string f013_atenanum { get; set; } = string.Empty;                     //17_宛名番号;
        public string f014_startdate1 { get; set; } = string.Empty;                   //24_診療開始年月日１;
        public string f015_counteddays { get; set; } = string.Empty;                  //30_診療実日数;
        public string f016_ratio { get; set; } = string.Empty;                        //31_給付割合;
        public string f017_total { get; set; } = string.Empty;                        //91_費用金額;
        public string f018_charge { get; set; } = string.Empty;                       //92_保険者負担額;
        public string f019_partial { get; set; } = string.Empty;                      //97_自己負担額;
        public string f020_mngnum { get; set; } = string.Empty;                       //121_電算管理番号;
        public int cym { get; set; } = 0;                                          //メホール請求年月;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;                //生年月日西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;               //開始年月日西暦;
        public int mediymad { get; set; } = 0;                                        //診療年月西暦;
        public string clinicnum { get; set; } = string.Empty;                         //f007_pref+f004_kind+f008_city+f009_clinicnum;
        public string filekbn { get; set; } = string.Empty;                           //td01/td24ファイル区別;

        #endregion

        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_ryouyouhi> SelectAll(int cym)
        {
            List<imp_ryouyouhi> lst = new List<imp_ryouyouhi>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_ryouyouhi where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_ryouyouhi cls = new imp_ryouyouhi();
                    cls.f000_importid = int.Parse(item[0].ToString());

                    cls.f001_chargeym = item[1].ToString().Trim();
                    cls.f002_rezenum = item[2].ToString().Trim();
                    cls.f003_hihonum = item[3].ToString().Trim();
                    cls.f004_kind = item[4].ToString().Trim();
                    cls.f005_kbn = item[5].ToString().Trim();
                    cls.f006_mediym = item[6].ToString().Trim();
                    cls.f007_pref = item[7].ToString().Trim();
                    cls.f008_city = item[8].ToString().Trim();
                    cls.f009_clinicnum = item[9].ToString().Trim();
                    cls.f010_insnum = item[10].ToString().Trim();
                    cls.f011_gender = item[11].ToString().Trim();
                    cls.f012_pbirthdayw = item[12].ToString().Trim();
                    cls.f013_atenanum = item[13].ToString().Trim();
                    cls.f014_startdate1 = item[14].ToString().Trim();
                    cls.f015_counteddays = item[15].ToString().Trim();
                    cls.f016_ratio = item[16].ToString().Trim();
                    cls.f017_total = item[17].ToString().Trim();
                    cls.f018_charge = item[18].ToString().Trim();
                    cls.f019_partial = item[19].ToString().Trim();
                    cls.f020_mngnum = item[20].ToString().Trim();

                    cls.cym = int.Parse(item[21].ToString().Trim());
                    cls.pbirthdayad = DateTime.Parse(item[22].ToString().Trim());
                    cls.startdate1ad = DateTime.Parse(item[23].ToString().Trim());
                    cls.mediymad = int.Parse(item[24].ToString().Trim());
                    cls.clinicnum = item[25].ToString().Trim();
                    cls.filekbn = item[26].ToString().Trim();

                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 被保番・合計指定リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strHihonum">被保番</param>
        /// <param name="strTotal">合計</param>
        /// <returns></returns>
        public static List<imp_ryouyouhi> Select(int cym,string strHihonum,string strTotal)
        {
            List<imp_ryouyouhi> lst = new List<imp_ryouyouhi>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_ryouyouhi " +
                $"where cym={cym} and f003_hihonum='{strHihonum}' and f017_total='{strTotal}' order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_ryouyouhi cls = new imp_ryouyouhi();
                    cls.f000_importid = int.Parse(item[0].ToString());

                    cls.f001_chargeym = item[1].ToString().Trim();
                    cls.f002_rezenum = item[2].ToString().Trim();
                    cls.f003_hihonum = item[3].ToString().Trim();
                    cls.f004_kind = item[4].ToString().Trim();
                    cls.f005_kbn = item[5].ToString().Trim();
                    cls.f006_mediym = item[6].ToString().Trim();
                    cls.f007_pref = item[7].ToString().Trim();
                    cls.f008_city = item[8].ToString().Trim();
                    cls.f009_clinicnum = item[9].ToString().Trim();
                    cls.f010_insnum = item[10].ToString().Trim();
                    cls.f011_gender = item[11].ToString().Trim();
                    cls.f012_pbirthdayw = item[12].ToString().Trim();
                    cls.f013_atenanum = item[13].ToString().Trim();
                    cls.f014_startdate1 = item[14].ToString().Trim();
                    cls.f015_counteddays = item[15].ToString().Trim();
                    cls.f016_ratio = item[16].ToString().Trim();
                    cls.f017_total = item[17].ToString().Trim();
                    cls.f018_charge = item[18].ToString().Trim();
                    cls.f019_partial = item[19].ToString().Trim();
                    cls.f020_mngnum = item[20].ToString().Trim();

                    cls.cym = int.Parse(item[21].ToString().Trim());
                    cls.pbirthdayad = DateTime.Parse(item[22].ToString().Trim());
                    cls.startdate1ad = DateTime.Parse(item[23].ToString().Trim());
                    cls.mediymad = int.Parse(item[24].ToString().Trim());
                    cls.clinicnum = item[25].ToString().Trim();
                    cls.filekbn = item[26].ToString().Trim();

                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }


    /// <summary>
    /// 医療機関マスタ
    /// </summary>
    public partial class imp_clinicMaster
    {

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                              //インポートID;
        public string f001_pref { get; set; } = string.Empty;                 //都道府県;
        public string f002_tensuhyo { get; set; } = string.Empty;             //点数表;
        public string f003_clinicnum { get; set; } = string.Empty;            //医療機関コード;
        public string f004_clinictel { get; set; } = string.Empty;            //医療機関電話;
        public string f005_clinickana { get; set; } = string.Empty;           //医療機関名カナ;
        public string f006_clinicname { get; set; } = string.Empty;           //医療機関名漢字;
        public string f007_cliniczip { get; set; } = string.Empty;            //医療機関郵便番号;
        public string f008_clinicadd { get; set; } = string.Empty;            //医療機関住所;
        public int cym { get; set; } = 0;                                     //メホール請求年月;
        public string clinicnum { get; set; } = string.Empty;                 //f001_pref+f002_tensuhyo+f003_clinicnum;


        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_clinicMaster> SelectAll(int cym)
        {
            List<imp_clinicMaster> lst = new List<imp_clinicMaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_clinicMaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_clinicMaster cls = new imp_clinicMaster();
                    cls.f000_importid = int.Parse(item[0].ToString());

                    cls.f001_pref = item[1].ToString().Trim();
                    cls.f002_tensuhyo = item[2].ToString().Trim();
                    cls.f003_clinicnum = item[3].ToString().Trim();
                    cls.f004_clinictel = item[4].ToString().Trim();
                    cls.f005_clinickana = item[5].ToString().Trim();
                    cls.f006_clinicname = item[6].ToString().Trim();
                    cls.f007_cliniczip = item[7].ToString().Trim();
                    cls.f008_clinicadd = item[8].ToString().Trim();

                    cls.cym = int.Parse(item[9].ToString().Trim());

                    cls.clinicnum = item[10].ToString().Trim();


                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 医療機関コード指定取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strClinicNum">医療機関コード</param>
        /// <returns></returns>
        public static List<imp_clinicMaster> Select(int cym,string strClinicNum)
        {
            List<imp_clinicMaster> lst = new List<imp_clinicMaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_clinicMaster where cym={cym} and clinicnum='{strClinicNum}' order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_clinicMaster cls = new imp_clinicMaster();
                    cls.f000_importid = int.Parse(item[0].ToString());

                    cls.f001_pref = item[1].ToString().Trim();
                    cls.f002_tensuhyo = item[2].ToString().Trim();
                    cls.f003_clinicnum = item[3].ToString().Trim();
                    cls.f004_clinictel = item[4].ToString().Trim();
                    cls.f005_clinickana = item[5].ToString().Trim();
                    cls.f006_clinicname = item[6].ToString().Trim();
                    cls.f007_cliniczip = item[7].ToString().Trim();
                    cls.f008_clinicadd = item[8].ToString().Trim();

                    cls.cym = int.Parse(item[9].ToString().Trim());

                    cls.clinicnum = item[10].ToString().Trim();


                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

    }



    /// <summary>
    /// 被保険者マスタ
    /// </summary>
    public partial class imp_hihomaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                //インポートID;
        public string f001_hihonum { get; set; } = string.Empty;                //被保険者番号;
        public string f002_pname { get; set; } = string.Empty;                  //漢字氏名;
        public string f003_pkana { get; set; } = string.Empty;                  //カナ氏名;
        public string f004_pzip { get; set; } = string.Empty;                   //郵便番号;
        public string f005_padd { get; set; } = string.Empty;                   //住所漢字;
        public string f006_sendzip { get; set; } = string.Empty;                //発送用郵便番号;
        public string f007_sendadd { get; set; } = string.Empty;                //発送用住所;
        public int cym { get; set; } = 0;                                       //メホール請求年月;



        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_hihomaster> selectall(int cym)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_hihomaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();

                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_pname = item[2].ToString();
                    imp.f003_pkana = item[3].ToString();
                    imp.f004_pzip = item[4].ToString();
                    imp.f005_padd = item[5].ToString();                    
                    imp.f006_sendzip = item[7].ToString();
                    imp.f007_sendadd = item[8].ToString();
                    imp.cym = int.Parse(item[9].ToString());

                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// cymと被保険者証番号でリスト抽出
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strHihonum">被保険者証番号</param>
        /// <returns></returns>
        public static List<imp_hihomaster> select(int cym, string strHihonum)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($"  f000_importid");
            sb.AppendLine($" ,f001_hihonum");
            sb.AppendLine($" ,f002_pname");
            sb.AppendLine($" ,f003_pkana");
            sb.AppendLine($" ,f004_pzip");
            sb.AppendLine($" ,f005_padd");
            sb.AppendLine($" ,f006_sendzip");
            sb.AppendLine($" ,f007_sendadd");
            sb.AppendLine($" ,cym");

            sb.AppendLine($" from imp_hihomaster where cym={cym} and f001_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_pname = item[2].ToString();
                    imp.f003_pkana = item[3].ToString();
                    imp.f004_pzip = item[4].ToString();
                    imp.f005_padd = item[5].ToString();                    
                    imp.f006_sendzip = item[6].ToString();
                    imp.f007_sendadd = item[7].ToString();
                    imp.cym = int.Parse(item[8].ToString());


                    lst.Add(imp);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }


    }
}


