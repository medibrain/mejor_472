﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.IbarakiKoiki
{
    public partial class frmImport : Form
    {

        public string strFileTD01 { get; set; } = string.Empty;
        public string strFileTD24 { get; set; } = string.Empty;
        public string strFileHihoMaster { get; set; } = string.Empty;
        public string strFileClinicMaster { get; set; } = string.Empty;

        private static int _cym;

        public frmImport(int cym)
        {
            InitializeComponent();
            _cym = cym;

        }



        private void btnFile1_Click(object sender, EventArgs e)
        {
            //    System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            //    ofd.Filter = "CSVファイル|*.csv";
            //    ofd.FilterIndex = 0;
            //    ofd.Title = "柔整提供データ";
            //    ofd.ShowDialog();
            //    if (ofd.FileName == string.Empty) return;
            //    textBoxBase.Text= ofd.FileName;

        }



        private void btnFile3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "社団以外TD01ファイル";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxTD01.Text = ofd.FileName;
        }


        private void btnFileTD24_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "社団TD24ファイル";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxTD24.Text = ofd.FileName;
        }





        private void btnImp_Click(object sender, EventArgs e)
        {
            strFileTD01 = textBoxTD01.Text.Trim();
            strFileTD24 = textBoxTD24.Text.Trim();
            strFileHihoMaster = textBoxHihoMaster.Text.Trim();
            strFileClinicMaster= textBoxClinicMaster.Text.Trim();
            this.Close();
        }

        private void btnFileHihoMaster_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "被保険者マスタファイル";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxHihoMaster.Text = ofd.FileName;
        }

        private void btnFileClinicMaster_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "療養費機関マスタファイル";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxClinicMaster.Text = ofd.FileName;
        }
    }
}
