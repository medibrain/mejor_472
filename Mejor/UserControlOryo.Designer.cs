﻿namespace Mejor
{
    partial class UserControlOryo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewKubun = new System.Windows.Forms.DataGridView();
            this.ColumnSinsei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnKeisan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxKeisanKyori = new System.Windows.Forms.TextBox();
            this.textBoxSinseiKyori = new System.Windows.Forms.TextBox();
            this.textBoxSejutushoAddr = new System.Windows.Forms.TextBox();
            this.textBoxPatientAddr = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.buttonRecalculate = new System.Windows.Forms.Button();
            this.textBoxPOrgAddr = new System.Windows.Forms.TextBox();
            this.textBoxSOrgAddr = new System.Windows.Forms.TextBox();
            this.statusStripKeisan = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelKm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxOutMemo = new System.Windows.Forms.TextBox();
            this.textBoxMemo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonMemo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKubun)).BeginInit();
            this.statusStripKeisan.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewKubun
            // 
            this.dataGridViewKubun.AllowUserToAddRows = false;
            this.dataGridViewKubun.AllowUserToDeleteRows = false;
            this.dataGridViewKubun.AllowUserToResizeColumns = false;
            this.dataGridViewKubun.AllowUserToResizeRows = false;
            this.dataGridViewKubun.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewKubun.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewKubun.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridViewKubun.ColumnHeadersHeight = 20;
            this.dataGridViewKubun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewKubun.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnSinsei,
            this.ColumnKeisan});
            this.dataGridViewKubun.Location = new System.Drawing.Point(340, 32);
            this.dataGridViewKubun.MultiSelect = false;
            this.dataGridViewKubun.Name = "dataGridViewKubun";
            this.dataGridViewKubun.ReadOnly = true;
            this.dataGridViewKubun.RowHeadersVisible = false;
            this.dataGridViewKubun.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewKubun.RowTemplate.Height = 21;
            this.dataGridViewKubun.RowTemplate.ReadOnly = true;
            this.dataGridViewKubun.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewKubun.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewKubun.ShowCellErrors = false;
            this.dataGridViewKubun.ShowCellToolTips = false;
            this.dataGridViewKubun.ShowEditingIcon = false;
            this.dataGridViewKubun.ShowRowErrors = false;
            this.dataGridViewKubun.Size = new System.Drawing.Size(146, 110);
            this.dataGridViewKubun.TabIndex = 20;
            this.dataGridViewKubun.TabStop = false;
            // 
            // ColumnSinsei
            // 
            this.ColumnSinsei.HeaderText = "申請";
            this.ColumnSinsei.Name = "ColumnSinsei";
            this.ColumnSinsei.ReadOnly = true;
            this.ColumnSinsei.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnSinsei.Width = 70;
            // 
            // ColumnKeisan
            // 
            this.ColumnKeisan.HeaderText = "計算";
            this.ColumnKeisan.Name = "ColumnKeisan";
            this.ColumnKeisan.ReadOnly = true;
            this.ColumnKeisan.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnKeisan.Width = 70;
            // 
            // textBoxKeisanKyori
            // 
            this.textBoxKeisanKyori.Location = new System.Drawing.Point(132, 167);
            this.textBoxKeisanKyori.Name = "textBoxKeisanKyori";
            this.textBoxKeisanKyori.ReadOnly = true;
            this.textBoxKeisanKyori.Size = new System.Drawing.Size(53, 19);
            this.textBoxKeisanKyori.TabIndex = 19;
            this.textBoxKeisanKyori.TabStop = false;
            // 
            // textBoxSinseiKyori
            // 
            this.textBoxSinseiKyori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxSinseiKyori.Location = new System.Drawing.Point(17, 167);
            this.textBoxSinseiKyori.Name = "textBoxSinseiKyori";
            this.textBoxSinseiKyori.Size = new System.Drawing.Size(46, 19);
            this.textBoxSinseiKyori.TabIndex = 3;
            // 
            // textBoxSejutushoAddr
            // 
            this.textBoxSejutushoAddr.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxSejutushoAddr.Location = new System.Drawing.Point(19, 123);
            this.textBoxSejutushoAddr.Name = "textBoxSejutushoAddr";
            this.textBoxSejutushoAddr.Size = new System.Drawing.Size(316, 19);
            this.textBoxSejutushoAddr.TabIndex = 2;
            // 
            // textBoxPatientAddr
            // 
            this.textBoxPatientAddr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBoxPatientAddr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxPatientAddr.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxPatientAddr.Location = new System.Drawing.Point(19, 55);
            this.textBoxPatientAddr.Name = "textBoxPatientAddr";
            this.textBoxPatientAddr.Size = new System.Drawing.Size(316, 19);
            this.textBoxPatientAddr.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(132, 148);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 15;
            this.label25.Text = "計算距離";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 148);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 14;
            this.label24.Text = "申請距離";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 83);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 12);
            this.label23.TabIndex = 13;
            this.label23.Text = "施術所住所";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(19, 12);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 12);
            this.label22.TabIndex = 12;
            this.label22.Text = "被保険者住所";
            // 
            // buttonRecalculate
            // 
            this.buttonRecalculate.Location = new System.Drawing.Point(241, 164);
            this.buttonRecalculate.Name = "buttonRecalculate";
            this.buttonRecalculate.Size = new System.Drawing.Size(75, 23);
            this.buttonRecalculate.TabIndex = 4;
            this.buttonRecalculate.Text = "再計算";
            this.buttonRecalculate.UseVisualStyleBackColor = true;
            this.buttonRecalculate.Click += new System.EventHandler(this.buttonRecalculate_Click);
            // 
            // textBoxPOrgAddr
            // 
            this.textBoxPOrgAddr.Location = new System.Drawing.Point(19, 32);
            this.textBoxPOrgAddr.Name = "textBoxPOrgAddr";
            this.textBoxPOrgAddr.ReadOnly = true;
            this.textBoxPOrgAddr.Size = new System.Drawing.Size(316, 19);
            this.textBoxPOrgAddr.TabIndex = 22;
            this.textBoxPOrgAddr.TabStop = false;
            // 
            // textBoxSOrgAddr
            // 
            this.textBoxSOrgAddr.Location = new System.Drawing.Point(19, 98);
            this.textBoxSOrgAddr.Name = "textBoxSOrgAddr";
            this.textBoxSOrgAddr.ReadOnly = true;
            this.textBoxSOrgAddr.Size = new System.Drawing.Size(316, 19);
            this.textBoxSOrgAddr.TabIndex = 23;
            this.textBoxSOrgAddr.TabStop = false;
            // 
            // statusStripKeisan
            // 
            this.statusStripKeisan.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStripKeisan.Location = new System.Drawing.Point(0, 287);
            this.statusStripKeisan.Name = "statusStripKeisan";
            this.statusStripKeisan.Size = new System.Drawing.Size(500, 22);
            this.statusStripKeisan.TabIndex = 24;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // labelKm
            // 
            this.labelKm.AutoSize = true;
            this.labelKm.Location = new System.Drawing.Point(74, 174);
            this.labelKm.Name = "labelKm";
            this.labelKm.Size = new System.Drawing.Size(20, 12);
            this.labelKm.TabIndex = 25;
            this.labelKm.Text = "km";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "km";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.textBoxOutMemo);
            this.panel1.Controls.Add(this.textBoxMemo);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(3, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(486, 45);
            this.panel1.TabIndex = 27;
            // 
            // textBoxOutMemo
            // 
            this.textBoxOutMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOutMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxOutMemo.Location = new System.Drawing.Point(262, 3);
            this.textBoxOutMemo.Multiline = true;
            this.textBoxOutMemo.Name = "textBoxOutMemo";
            this.textBoxOutMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutMemo.Size = new System.Drawing.Size(225, 38);
            this.textBoxOutMemo.TabIndex = 9;
            // 
            // textBoxMemo
            // 
            this.textBoxMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxMemo.Location = new System.Drawing.Point(32, 2);
            this.textBoxMemo.Multiline = true;
            this.textBoxMemo.Name = "textBoxMemo";
            this.textBoxMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMemo.Size = new System.Drawing.Size(188, 38);
            this.textBoxMemo.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(225, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 24);
            this.label16.TabIndex = 0;
            this.label16.Text = "出力\r\nメモ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "メモ";
            // 
            // buttonMemo
            // 
            this.buttonMemo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMemo.Location = new System.Drawing.Point(401, 259);
            this.buttonMemo.Name = "buttonMemo";
            this.buttonMemo.Size = new System.Drawing.Size(75, 23);
            this.buttonMemo.TabIndex = 4;
            this.buttonMemo.Text = "更新";
            this.buttonMemo.UseVisualStyleBackColor = true;
            this.buttonMemo.Visible = false;
            this.buttonMemo.Click += new System.EventHandler(this.buttonMemo_Click);
            // 
            // UserControlOryo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelKm);
            this.Controls.Add(this.statusStripKeisan);
            this.Controls.Add(this.textBoxSOrgAddr);
            this.Controls.Add(this.textBoxPOrgAddr);
            this.Controls.Add(this.buttonMemo);
            this.Controls.Add(this.buttonRecalculate);
            this.Controls.Add(this.dataGridViewKubun);
            this.Controls.Add(this.textBoxKeisanKyori);
            this.Controls.Add(this.textBoxSinseiKyori);
            this.Controls.Add(this.textBoxSejutushoAddr);
            this.Controls.Add(this.textBoxPatientAddr);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.MinimumSize = new System.Drawing.Size(500, 309);
            this.Name = "UserControlOryo";
            this.Size = new System.Drawing.Size(500, 309);
            this.Load += new System.EventHandler(this.UserControlOryo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKubun)).EndInit();
            this.statusStripKeisan.ResumeLayout(false);
            this.statusStripKeisan.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewKubun;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSinsei;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnKeisan;
        private System.Windows.Forms.TextBox textBoxKeisanKyori;
        private System.Windows.Forms.TextBox textBoxSinseiKyori;
        private System.Windows.Forms.TextBox textBoxSejutushoAddr;
        private System.Windows.Forms.TextBox textBoxPatientAddr;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button buttonRecalculate;
        private System.Windows.Forms.TextBox textBoxPOrgAddr;
        private System.Windows.Forms.TextBox textBoxSOrgAddr;
        private System.Windows.Forms.StatusStrip statusStripKeisan;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Label labelKm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxOutMemo;
        private System.Windows.Forms.TextBox textBoxMemo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonMemo;
    }
}
