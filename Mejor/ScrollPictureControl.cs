﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;

namespace Mejor
{
    public partial class ScrollPictureControl : UserControl
    {
        private MemoryStream memoryStream = null;
        public event EventHandler ImageScrolled;
        protected virtual void OnImageScrolled(EventArgs e)
        {
            ImageScrolled?.Invoke(this, e);
        }

        private Dictionary<string, Point> points = new Dictionary<string, Point>();
        static XDocument xdocPoints = new XDocument();

        private float _ratio = 1;

        /// <summary>
        /// 表示倍率を取得、または設定します
        /// </summary>
        public float Ratio
        {
            get
            {
                return _ratio;
            }
            set
            {
                _ratio = value;
                if (pictureBox.Image == null) return;
                sizeAdjust(0, 0);
            }
        }

        private bool _buttosVisible = false;

        /// <summary>
        /// 操作ボタンを表示、非表示を取得、または設定します
        /// </summary>
        public bool ButtonsVisible
        {
            get
            {
                return _buttosVisible;
            }
            set
            {
                _buttosVisible = value;
                buttonImageChange.Visible = value;
                buttonImageRotateL.Visible = value;
                buttonImageRotateR.Visible = value;
                buttonFill.Visible = value;
            }
        }

        private Form parentForm;
        public ScrollPictureControl()
        {
            InitializeComponent();
            _ratio = 1;
            panel1.Dock = DockStyle.Fill;
            this.ParentChanged += ScrollPictureControl_ParentChanged;
            this.Layout += ScrollPictureControl_Layout;
            panel1.Scroll += ScrollPictureControl_Scroll;
            pictureBox.MouseWheel += ParentForm_MouseWheel;
        }

        private void ScrollPictureControl_Layout(object sender, LayoutEventArgs e)
        {
            if (parentForm != null) return;
            parentForm = FindForm();
            if (parentForm != null) parentForm.MouseWheel += ParentForm_MouseWheel;
        }

        void ScrollPictureControl_Scroll(object sender, ScrollEventArgs e)
        {
            OnImageScrolled(new EventArgs());
        }

        void ScrollPictureControl_ParentChanged(object sender, EventArgs e)
        {
            points.Clear();
            if (this.DesignMode) return;
            if (Parent == null) return;

            var dir = Application.StartupPath + "\\Points";
            var fn = dir + "\\" + this.GetType().FullName + ".xlsx";

            try
            {
                if (!System.IO.Directory.Exists(dir))
                    System.IO.Directory.CreateDirectory(dir);

                if (!System.IO.File.Exists(fn))
                {
                    xdocPoints = new XDocument();
                    xdocPoints.Add(new XElement("Pints"));
                    xdocPoints.Save(fn);
                }
                else
                {
                    xdocPoints = XDocument.Load(fn);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ImageLocation { get; private set; }

        /// <summary>
        /// 画像表示をクリアします
        /// </summary>
        public void Clear()
        {
            memoryStreamClear();
            ImageLocation = string.Empty;
            var oldImg = pictureBox.Image;
            pictureBox.Image = null;
            if (oldImg != null) oldImg.Dispose();
        }

        /// <summary>
        /// ファイル名を指定し、画像を表示します
        /// </summary>
        /// <param name="fileName"></param>
        public void SetPictureFile(string fileName)
        {
            memoryStreamClear();

            //20190729112628 furukawa st ////////////////////////
            //ファイルがない場合抜ける
            
            if (!System.IO.File.Exists(fileName)) return;
            //20190729112628 furukawa ed ////////////////////////


            ImageLocation = fileName;
            var oldImg = pictureBox.Image;
            if (ImageLocation == string.Empty)
            {
                pictureBox.Image = null;
                if (oldImg != null) oldImg.Dispose();
                return;
            }

            try
            {

                using (var fs = new System.IO.FileStream(
                    fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    var img = Image.FromStream(fs);

                    pictureBox.Size = new Size((int)(img.Width * _ratio), (int)(img.Height * _ratio));
                    pictureBox.Image = img;
                    if (oldImg != null) oldImg.Dispose();
                }

                adjustButtonLocation();
            }
            catch(Exception ex)
            {
                MessageBox.Show("画像の表示中にエラーが発生しました\r\n" + ex.Message);
            }
        }

        /// <summary>
        /// Imageのインスタンスを指定し、画像を表示します
        /// </summary>
        /// <param name="img"></param>
        /// <param name="oldImageDispose">現在表示中のImageを破棄する場合、true</param>
        public void SetImage(Image img, string fileName)
        {
            memoryStreamClear();

            ImageLocation = string.Empty;
            var oldImg = pictureBox.Image;
            pictureBox.Image = (Image)img.Clone();

            pictureBox.Size = new Size((int)(img.Width * _ratio), (int)(img.Height * _ratio));
            if (oldImg != null) oldImg.Dispose();

            adjustButtonLocation();
        }

        /// <summary>
        /// Imageのストリームを指定し、画像を表示します
        /// </summary>
        /// <param name="img"></param>
        /// <param name="oldImageDispose">現在表示中のImageを破棄する場合、true</param>
        public void SetImageMemoryStream(MemoryStream stream, string fileName)
        {
            memoryStreamClear();
            memoryStream = new MemoryStream();
            stream.Position = 0;
            stream.CopyTo(memoryStream);

            ImageLocation = string.Empty;
            var oldImg = pictureBox.Image;
            var img = Image.FromStream(memoryStream);
            pictureBox.Image = img;

            pictureBox.Size = new Size((int)(img.Width * _ratio), (int)(img.Height * _ratio));
            if (oldImg != null) oldImg.Dispose();

            adjustButtonLocation();
        }

        public Point ScrollPosition
        {
            get
            {
                var pt = panel1.AutoScrollPosition;
                if (pictureBox.Image == null) return pt;

                float rate = (float)pictureBox.Width / (float)pictureBox.Image.Width;
                return new Point((int)(-pt.X / rate), (int)(-pt.Y / rate));
            }
            set
            {
                if (pictureBox.Image == null) return;

                float rate = (float)pictureBox.Width / (float)pictureBox.Image.Width;
                var p = new PointF(value.X * rate, value.Y * rate);
                panel1.AutoScrollPosition = Point.Round(p);
            }
        }

        /// <summary>
        /// 画像の横いっぱい表示に調整します
        /// </summary>
        public void AdjustImageWidth()
        {
            var w = this.Width - SystemInformation.VerticalScrollBarWidth;
            _ratio = ((float)w) / (float)pictureBox.Image.Width;
            pictureBox.Size = new Size(w, (int)(pictureBox.Image.Height * _ratio));
        }

        /// <summary>
        /// 画像全体を表示するように調整します
        /// </summary>
        public void SetPictureBoxFill()
        {
            //20190729112533 furukawa st ////////////////////////
            //ファイルがない場合抜ける
            
            if (pictureBox.Image == null) return;
            //20190729112533 furukawa ed ////////////////////////


            this.AutoScrollPosition = new Point(0, 0);
            pictureBox.Size = this.Size;
            var rw = ((float)this.Width) / (float)pictureBox.Image.Width;
            var rh = ((float)this.Height) / (float)pictureBox.Image.Height;
            Ratio = rw < rh ? rw : rh;
        }

        ////////////////////////////////
        //
        //  ドラッグによる画像位置調整
        //
        ////////////////////////////////

        private bool is_mouse_down = false;
        private Point origin_;
        int dx = 0;
        int dy = 0;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            origin_ = pictureBox.Parent.PointToScreen(e.Location);
            Cursor.Current = Cursors.NoMove2D;
            is_mouse_down = true;

            dx = 0;
            dy = 0;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!is_mouse_down) return;

            var current = pictureBox.PointToScreen(e.Location);
            int x = current.X - origin_.X;
            int y = current.Y - origin_.Y;

            panel1.AutoScrollPosition = new Point(-x, -y);
            dx = x;
            dy = y;
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            OnImageScrolled(new EventArgs());
            Cursor.Current = Cursors.Default;
            is_mouse_down = false;
        }

        ///////////////////////////////////////////////
        //
        //  クリック・ホイールによる画像拡大縮小
        //
        ///////////////////////////////////////////////
        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (pictureBox.Image == null) return;
            if (System.Math.Abs(dx) > 10 && System.Math.Abs(dy) > 10) return;

            //クリック場所実画像座標
            float cx = e.X / _ratio;
            float cy = e.Y / _ratio;

            if (e.Button == MouseButtons.Left) _ratio *= 1.1f;
            else if (e.Button == MouseButtons.Right) _ratio /= 1.1f;
            else return;

            sizeAdjust(cx, cy);
        }

        private void ParentForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (pictureBox.Image == null) return;
            if (System.Math.Abs(dx) > 10 && System.Math.Abs(dy) > 10) return;

            //カーソル位置がコントーロール内か
            var mouseClientPos = PointToClient(MousePosition);
            if (!ClientRectangle.Contains(mouseClientPos)) return;

            float cx = e.X / _ratio;
            float cy = e.Y / _ratio;

            if (e.Delta > 0) _ratio *= 1.1f;
            else if (e.Delta < 0) _ratio /= 1.1f;
            else return;

            sizeAdjust(cx, cy);
        }

        private void sizeAdjust(float cx, float cy)
        {
            panel1.SuspendLayout();
            pictureBox.SuspendLayout();

            //スクロール位置考慮
            cx += panel1.AutoScrollPosition.X;
            cy += panel1.AutoScrollPosition.Y;
            pictureBox.Size = new Size((int)(pictureBox.Image.Width * _ratio), (int)(pictureBox.Image.Height * _ratio));

            //表示位置調整
            //var x = pictureBox.Location.X;
            ////var y = pictureBox.Location.Y;
            //var y = 0;
            //if (panel1.Width > pictureBox.Width)
            //{
            //    x = (panel1.Width - pictureBox.Width) / 2;
            //    cx = 0;
            //}
            //else
            //{
            //    x = 0;
            //}
            //if (Height > pictureBox.Height)
            //{
            //    y = (panel1.Height - pictureBox.Height) / 2;
            //    cy = 0;
            //}
            //else
            //{
            //    y = 0;
            //}
            //var p = new Point(x, y);
            //pictureBox.Location = new Point(x, y);
            panel1.AutoScrollPosition = new Point((int)(cx * _ratio), (int)(cy * _ratio));

            adjustButtonLocation();
            pictureBox.PerformLayout();
            panel1.ResumeLayout();
        }

        private void adjustButtonLocation()
        {
            int x = this.Width - buttonFill.Width;
            if (panel1.VerticalScroll.Visible) x -= SystemInformation.VerticalScrollBarWidth;

            int y = this.Height - buttonImageRotateL.Height;
            if (panel1.HorizontalScroll.Visible) y -= SystemInformation.HorizontalScrollBarHeight;

            if (buttonImageRotateL.Location.Y != y)
            {
                buttonImageRotateL.Location = new Point(buttonImageRotateL.Location.X, y);
                buttonImageRotateR.Location = new Point(buttonImageRotateR.Location.X, y);
                buttonImageChange.Location = new Point(buttonImageChange.Location.X, y);
                buttonFill.Location = new Point(x, y);
            }

            if (buttonFill.Location.X != x)
            {
                buttonFill.Location = new Point(x, y);
            }
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            if (ImageLocation == string.Empty || pictureBox.Image == null) return;
            ImageUtility.ImageRotate(ImageLocation, false);
            SetPictureFile(ImageLocation);
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            if (ImageLocation == string.Empty || pictureBox.Image == null) return;
            ImageUtility.ImageRotate(ImageLocation, true);
            SetPictureFile(ImageLocation);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            if (ImageLocation == string.Empty || pictureBox.Image == null) return;

            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                string newFileName = f.FileName;

                try
                {
                    System.IO.File.Copy(newFileName, ImageLocation, true);
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                        ImageLocation + " へのファイル差替に失敗しました");
                }
            }
        }

        private void buttonFill_Click(object sender, EventArgs e)
        {
            SetPictureBoxFill();
        }

        /// <summary>
        /// 複数ページのTiffを切り替えます
        /// </summary>
        /// <param name="pageIndex"></param>
        public void PageSelect(int pageIndex)
        {
            if (memoryStream != null)
            {
                //ストリームがある場合、それを活用
                var img = pictureBox.Image;
                var gfd = new FrameDimension(img.FrameDimensionsList[0]);
                int pageCount = img.GetFrameCount(gfd);

                if (pageCount > pageIndex && pageIndex >= 0)
                {
                    img.SelectActiveFrame(gfd, pageIndex);
                    pictureBox.Image = img;
                }
            }
            else
            {
                var oldImg = pictureBox.Image;
                if (ImageLocation == string.Empty || oldImg == null) return;

                using (var fs = new System.IO.FileStream(
                    ImageLocation, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    var img = Image.FromStream(fs);
                    var gfd = new FrameDimension(img.FrameDimensionsList[0]);
                    int pageCount = img.GetFrameCount(gfd);

                    if (pageCount > pageIndex && pageIndex >= 0)
                    {
                        img.SelectActiveFrame(gfd, pageIndex);
                        pictureBox.Image = img;
                        oldImg.Dispose();
                    }
                }
            }
        }

        private void memoryStreamClear()
        {
            if (memoryStream != null)
            {
                memoryStream.Dispose();
                memoryStream = null;
            }
        }
    }
}
