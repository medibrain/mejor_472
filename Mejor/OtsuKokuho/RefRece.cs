﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Windows.Forms;

namespace Mejor.OtsuKokuho
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int RrID { get; private set; }
        public int CYM { get; private set; }
        public int YM { get; private set; }
        public string Num { get; private set; }
        public int Family { get; set; }
        public SEX Sex { get; private set; }
        public DateTime Birth { get; set; }
        public DateTime FirstDate { get; set; }
        public int Days { get; private set; }
        public NEW_CONT NewCont { get; private set; }
        public int Total { get; private set; }
        public int Partial { get; private set; }
        public int Charge { get; private set; }
        public int Ratio { get; private set; }
        public string DrNum { get; private set; }
        public string SearchNum { get; private set; }
        public int AID { get; private set; }

        [DB.DbAttribute.Ignore]
        public string BirthJdate => DateTimeEx.ToJDateShortStr(Birth);

        /// <summary>
        /// apptype 7=鍼灸 8=あんま
        /// </summary>
        public static bool Import()
        {
            string fileName;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "大津国保提供Excelデータ|*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            var wf = new WaitForm();
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
            wf.ShowDialogOtherTask();
            wf.LogPrint("インポートを開始します");

            wf.LogPrint("Excelファイルを読み込み中です…");
            var xlsx = new XSSFWorkbook(fileName);
            var sheet = xlsx.GetSheetAt(0);
            var rowCount = sheet.LastRowNum + 2;
            var l = new List<RefRece>();

            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    var row = sheet.GetRow(i);
                    if (row == null) continue;
                    var c = row.GetCell(0);
                    if (c == null) continue;
                    if (c.CellType != CellType.Numeric) continue;

                    var rr = new RefRece();
                    rr.CYM = DateTimeEx.GetAdYearMonthFromJyymm((int)row.GetCell(0).NumericCellValue);
                    rr.SearchNum = row.GetCell(3).NumericCellValue.ToString();
                    rr.DrNum = row.GetCell(5).NumericCellValue.ToString();
                    rr.Num = row.GetCell(12).NumericCellValue.ToString().PadLeft(7, '0');
                    rr.Birth = DateTimeEx.GetDateFromJInt7((int)row.GetCell(13).NumericCellValue);
                    rr.Sex = row.GetCell(14).NumericCellValue == 1 ? SEX.男 : row.GetCell(14).NumericCellValue == 2 ? SEX.女 : SEX.Null;
                    rr.YM = DateTimeEx.GetAdYearMonthFromJyymm((int)row.GetCell(36).NumericCellValue);
                    rr.Family = (int)row.GetCell(42).NumericCellValue;
                    rr.FirstDate = DateTimeEx.GetDateFromJInt7((int)row.GetCell(50).NumericCellValue);
                    rr.Days = (int)row.GetCell(71).NumericCellValue;
                    rr.NewCont = (row.GetCell(220)?.CellType ?? CellType.Blank) == CellType.Numeric && row.GetCell(220).NumericCellValue == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
                    rr.Total = (int)row.GetCell(662).NumericCellValue;
                    rr.Partial = (int)row.GetCell(668).NumericCellValue;
                    rr.Charge = (int)row.GetCell(664).NumericCellValue;
                    rr.Ratio = ((int)row.GetCell(420).NumericCellValue) / 10;
                    l.Add(rr);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(i.ToString() + "行目のデータ読み込みに失敗しました" +
                        "\r\n" + ex.Message);
                    continue;
                }
            }

            wf.LogPrint("データベースへ書き込み中です…");
            wf.SetMax(l.Count);

            using (var tran = DB.Main.CreateTransaction())
            {
                try
                {
                    foreach (var item in l)
                    {
                        if(!DB.Main.Insert(item, tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                        wf.InvokeValue++;
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    MessageBox.Show("インポートに失敗しました");
                    return false;
                }
                finally
                {
                    wf.Dispose();
                }

                tran.Commit();
                System.Windows.Forms.MessageBox.Show("インポートが完了しました");
                return true;
            }
        }

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cym, int y, int m, string num, int total)
        {
            
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            int ym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;
            //int ym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            return DB.Main.Select<RefRece>(new { num = num, ym = ym, total = total, cym = cym }).ToList();
        }

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cym, string num, int total)
        {
            return DB.Main.Select<RefRece>(new { cym = cym, num = num, total = total }).ToList();
        }

        /// <summary>
        /// refreceテーブル中のrridに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="rrid"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int rrid, int aid, DB.Transaction tran)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery()) return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// refreceテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }

        public static List<RefRece> SelectAll(int cyyyymm)
        {
            return DB.Main.Select<RefRece>(new { cym = cyyyymm }).ToList();
        }

        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int jy, int m)
        {
            int dcy = DateTimeEx.GetAdYear("平成" + jy.ToString());
            int cym = dcy * 100 + m;

            var sql = "SELECT " +
                "rrid, ym, days, total, num, name, kana, clinicnum, drname, clinicname, " +
                "searchnum, aid, cym, zip, add, destname, destzip, destadd, apptype " +
                "FROM refrece WHERE cym = @cym " +
                "AND aid = 0 ORDER BY rrid";

            return DB.Main.Query<RefRece>(sql, new { cym = cym }).ToList();
        }

        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cyyyymm)
        {
            var sql = "SELECT " +
                "rrid, ym, days, total, num, name, kana, clinicnum, drname, clinicname, " +
                "searchnum, aid, cym, zip, add, destname, destzip, destadd, apptype " +
                "FROM refrece WHERE cym = @cym " +
                "AND aid = 0 ORDER BY rrid";

            return DB.Main.Query<RefRece>(sql, new { cym = cyyyymm }).ToList();
        }

        public static RefRece Select(int rrid)
        {
            var sql = "SELECT " +
                "rrid, ym, days, total, num, name, kana, clinicnum, drname, clinicname, " +
                "searchnum, aid, cym, zip, add, destname, destzip, destadd, apptype " +
                "FROM refrece " +
                "WHERE rrid=@rrid;";

            return DB.Main.Query<RefRece>(sql, new { rrid = rrid }).FirstOrDefault();
        }
    }
}
