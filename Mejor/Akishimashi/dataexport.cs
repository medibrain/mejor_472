﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Akishimashi

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export
    {
       

        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            //支給決定日入力フォーム
            frmExport frm = new frmExport();
            frm.ShowDialog();

            //支給決定日
            string strKetteibi = frm.strKetteibi;

            //出力先
            string strOutputPath = frm.strOutputPath;
            if (strOutputPath == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("出力先が指定されていません");
                return false;
            }

            if (!System.IO.Directory.Exists(strOutputPath)) System.IO.Directory.CreateDirectory(strOutputPath);

            List<App> lst = App.GetApps(cym);

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //納品データ作成
                if (!CreateExportData(lst, cym, strOutputPath, strKetteibi, wf)) return false;

                //画像データ出力（マルチtiff）
                if (!ExportImage(lst, strOutputPath, wf)) return false;

                //仮コードリスト
                if (!CreateNewAccountData(strOutputPath, wf, cym)) return false;

                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 新規作成した口座のリスト（仮コード）出力
        /// </summary>
        /// <param name="strOutputPath"></param>
        /// <param name="wf"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool CreateNewAccountData(string strOutputPath,WaitForm wf,int cym)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strOutputPath}\\仮コードリスト.csv",false,System.Text.Encoding.GetEncoding("utf-8"));
            DB.Command cmd = null;

            try
            {
                StringBuilder sb=new StringBuilder();
                sb.Clear();
                sb.AppendLine($" select f001_tempcode,f002_accountnum,f003_biko,f004_code from paycode_temporary where cym={cym} and f003_biko ~'新規口座作成'");

                cmd = DB.Main.CreateCmd(sb.ToString());
                var lst = cmd.TryExecuteReaderList();
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;

                sw.WriteLine($"仮コード,口座番号,備考,本コード");
                foreach (var item in lst)
                {
                    wf.LogPrint($"仮コードリスト出力中 口座番号: {item[1]}");
                    string strLine = $"{item[0]},{item[1]},{item[2]},{item[3]}";
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }

                wf.LogPrint($"仮コードリスト出力終了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }

        }



        /// <summary>
        /// 不要以外画像出力
        /// </summary>
        /// <param name="lst">出力するリスト</param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(List<App> lst, string strDir, WaitForm wf)
        {
            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ

            if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

            List<string> lstImageFilePath = new List<string>();

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            wf.SetMax(lst.Count);

            try
            {
                //ファイル名はナンバリング6桁前ゼロ埋め
                //0番目のファイルパス
                imageName = $"{strImageDir}\\{lst[0].Numbering.PadLeft(6,'0')}.tif";

                for (int r = 0; r < lst.Count(); r++)
                {

                    //apptypeが以下の場合、申請書と見なす
                    if ((new APP_TYPE[]{APP_TYPE.柔整,APP_TYPE.あんま,APP_TYPE.鍼灸 }.Contains(lst[r].AppType)) && (lstImageFilePath.Count != 0))
                    {
                        //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                        //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                        //その上で、この申請書をマルチTIFF候補リストに追加
                        wf.LogPrint($"申請書出力中:{imageName}");
                        if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                        {
                            System.Windows.Forms.MessageBox.Show(
                                System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                            return false;
                        }

                        lstImageFilePath.Clear();
                        lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
                        imageName = $"{strImageDir}\\{lst[r].Numbering.PadLeft(6, '0')}.tif";
                    }
                    else if ((new APP_TYPE[] { APP_TYPE.柔整, APP_TYPE.あんま, APP_TYPE.鍼灸 }.Contains(lst[r].AppType)) && (lstImageFilePath.Count == 0))
                    {
                        //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
                        lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
                        wf.LogPrint($"申請書出力中:{imageName}");
                    }

                    else if (new APP_TYPE[] { 
                        APP_TYPE.総括票, APP_TYPE.続紙,APP_TYPE.同意書,APP_TYPE.往療内訳,
                        APP_TYPE.長期,APP_TYPE.施術報告書,APP_TYPE.状態記入書,APP_TYPE.同意書裏}.Contains(lst[r].AppType))
                    {
                        //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
                        lstImageFilePath.Add(App.GetApp(lst[r].Aid).GetImageFullPath());
                        wf.LogPrint($"申請書以外:{imageName}");
                    }


                    wf.InvokeValue++;

                }


                //最終画像出力
                //ループの最後の画像を出力
                if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        /// <summary>
        /// 納品データ作成
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strOutputPath">出力先</param>
        /// <param name="strKetteibi">支給決定日（GYYMMDD)</param>
        /// <returns></returns>
        private static bool CreateExportData(List<App> lst,int cym, string strOutputPath,string strKetteibi,WaitForm wf)
        {

            //出力ファイル名            
            string strFileName = $"{strOutputPath}\\{DateTime.Now.ToString("yyyyMMdd")}";

           
            lst.OrderBy(item => item.PayCode).ThenBy(item => int.Parse(item.Numbering));


            System.Text.StringBuilder sb = new StringBuilder();
            //ナンバリングの空欄は0扱いにして数値にして比較
            sb.AppendLine($" select ");
            sb.AppendLine($" min(cast(");
            sb.AppendLine($" 	case numbering when '' then 0 else cast(numbering as int) end");
            sb.AppendLine($" as int))");
            sb.AppendLine($" ,max(cast(");
            sb.AppendLine($" 	case numbering when '' then 0 else cast(numbering as int) end ");
            sb.AppendLine($" 	as int))");
            sb.AppendLine($"  from application");
            sb.AppendLine($"  where");
            sb.AppendLine($"  cym={cym}");
            sb.AppendLine($"  and numbering <>''");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            var l = cmd.TryExecuteReaderList();
            
            string strfirstnum = l[0][0].ToString();
            string strfinishnum = l[0][1].ToString();

            //20220420180016 furukawa st ////////////////////////
            //ANSI＝シフトJISで作る
            
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("shift-jis"));
            //System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("UTF-8"));
            //20220420180016 furukawa ed ////////////////////////


            wf.SetMax(lst.Count);

            try
            {

                foreach (App item in lst)
                {
                    //申請書以外飛ばす
                    if ((int)item.AppType < 0) continue;

                    wf.LogPrint($"出力中 AID {item.Aid}");

                    clsExport exp = new clsExport();
                    exp.f001_kbn = "2";
                    exp.f002_code = item.PayCode.PadLeft(9, '0');
                    exp.f003_firstnum = strfirstnum.PadLeft(6, '0');
                    exp.f004_finishnum = strfinishnum.PadLeft(6, '0');
                    exp.f005_ketteibi = strKetteibi.PadLeft(7, '0');
                    exp.f006_maisu = lst.Count.ToString().PadLeft(4, '0');
                    exp.f007_skip = string.Empty.PadLeft(1, ' ');
                    exp.f008_kbn2 = "7";
                    exp.f009_skip = string.Empty.PadLeft(1, ' ');


                    string fushofinishW = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoFinishDate1).ToString();
                    if (fushofinishW == "0") fushofinishW = "0".PadLeft(7, '0');
                    string strFushoFinishY = fushofinishW.Substring(1, 2);
                    string strFushoFinishM = fushofinishW.Substring(3, 2);
                    string strFushoFinishD = fushofinishW.Substring(5, 2);

                    exp.f010_year = strFushoFinishY.PadLeft(2, '0');
                    exp.f011_month = strFushoFinishM.PadLeft(2, '0');
                    exp.f012_insnum = item.InsNum==string.Empty ? string.Empty.PadLeft(2,'0'):item.InsNum.Substring(0, 2);


                    exp.f013_hnum = item.HihoNum == string.Empty ? string.Empty.PadLeft(6,'0'):item.HihoNum.Substring(item.HihoNum.Length - 6, 6);
                    exp.f014_skip3 = string.Empty.PadLeft(5, ' ');
                    exp.f015_gender = item.Sex.ToString();

                    string birthdayW = DateTimeEx.GetIntJpDateWithEraNumber(item.Birthday).ToString();
                    if (birthdayW == "0") birthdayW = "0".PadLeft(7, '0');
                    string birthNengo = birthdayW.Substring(0, 1);
                    string birthYearW = birthdayW.Substring(1, 2);
                    exp.f016_birthnengo = birthNengo;
                    exp.f017_birthyear = birthYearW;

                    string fushoStartW = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();
                    if (fushoStartW == "0") fushoStartW = "0".PadLeft(7, '0');
                    string strFushoStartY = fushoStartW.Substring(1, 2);
                    string strFushoStartM = fushoStartW.Substring(3, 2);
                    string strFushoStartD = fushoStartW.Substring(5, 2);

                    exp.f018_startyear = strFushoStartY;
                    exp.f019_startmonth = strFushoStartM;
                    exp.f020_startday = strFushoStartD;

                    exp.f021_finishyear = strFushoFinishY;
                    exp.f022_finishmonth = strFushoFinishM;
                    exp.f023_finishday = strFushoFinishD;

                    exp.f024_counteddays = item.CountedDays.ToString().PadLeft(2, '0');
                    exp.f025_total = item.Total.ToString().PadLeft(6, '0');
                    exp.f026_chrage = item.Charge.ToString().PadLeft(6, '0');
                    exp.f027_number = item.Numbering.PadLeft(6, '0');

                               
                    if (item.AppType == APP_TYPE.柔整)
                        //20220428112647 furukawa st ////////////////////////
                        //登録記号番号：柔整＝2桁目から9文字、あはき＝1～8桁＋10桁目の9文字
                        exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : item.DrNum.Substring(1, 9).PadLeft(9, '0');
                        //      exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : item.DrNum.Substring(0, 9).PadLeft(9, '0');
                        //20220428112647 furukawa ed ////////////////////////
                    else if (item.AppType == APP_TYPE.あんま || item.AppType == APP_TYPE.鍼灸)
                        exp.f028_sregnumber = item.DrNum == string.Empty || item.DrNum.Length != 10 ? string.Empty.PadLeft(9, '0') : (item.DrNum.Substring(0, 8) + item.DrNum.Substring(9, 1)).PadLeft(9, '0');

                    

                    if (item.TaggedDatas.KouhiNum == string.Empty) exp.f029_fukushi = "00";
                    else exp.f029_fukushi = item.TaggedDatas.KouhiNum.Substring(0, 2);


                    sw.WriteLine(CreateLine(exp));

                }


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
                cmd.Dispose();
            }

        }

        /// <summary>
        /// 1行作成
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        private static string CreateLine(clsExport exp)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(exp.f001_kbn);
            sb.Append(exp.f002_code);
            sb.Append(exp.f003_firstnum);
            sb.Append(exp.f004_finishnum);
            sb.Append(exp.f005_ketteibi);
            sb.Append(exp.f006_maisu);
            sb.Append(exp.f007_skip);
            sb.Append(exp.f008_kbn2);
            sb.Append(exp.f009_skip);
            sb.Append(exp.f010_year);
            sb.Append(exp.f011_month);
            sb.Append(exp.f012_insnum);
            sb.Append(exp.f013_hnum);
            sb.Append(exp.f014_skip3);
            sb.Append(exp.f015_gender);
            sb.Append(exp.f016_birthnengo);
            sb.Append(exp.f017_birthyear);
            sb.Append(exp.f018_startyear);
            sb.Append(exp.f019_startmonth);
            sb.Append(exp.f020_startday);
            sb.Append(exp.f021_finishyear);
            sb.Append(exp.f022_finishmonth);
            sb.Append(exp.f023_finishday);
            sb.Append(exp.f024_counteddays);
            sb.Append(exp.f025_total);
            sb.Append(exp.f026_chrage);
            sb.Append(exp.f027_number);
            sb.Append(exp.f028_sregnumber);
            sb.Append(exp.f029_fukushi);

            return sb.ToString();
        }


    }
}
