﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Akishimashi

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName_Saikensha = frm.strFileSaikensha;

            if (strFileName_Saikensha == string.Empty) return;



            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                
                if (strFileName_Saikensha != string.Empty)
                {
                    wf.LogPrint("提供データ_コード表インポート");
                    if (!dataImport_paycode.dataImport(_cym, wf, strFileName_Saikensha))
                    {
                        wf.LogPrint("提供データ_コード表インポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_コード表インポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_コード表インポート終了");



                    wf.LogPrint("提供データ_仮コード表インポート");
                    if (!dataImport_paycode.dataImport_2(_cym, wf, strFileName_Saikensha))
                    {
                        wf.LogPrint("提供データ_仮コード表インポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_仮コード表インポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_仮コード表インポート終了");

                }
                else
                {
                    wf.LogPrint("提供データ_債権者コードファイルのファイルが指定されていないため処理しない");
                }

                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                return;
            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    partial class dataImport_paycode
    {
   
        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        /// <summary>
        /// xls用
        /// </summary>
        //static NPOI.HSSF.UserModel.HSSFWorkbook wbhs;
        
        /// <summary>
        /// 債権者コードファイル（コード表）インポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport(int _cym,WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            int intcym = _cym;

            
            //xlsファイル
            //System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.Open);
            //wbhs = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);
            //NPOI.SS.UserModel.ISheet ws = wbhs.GetSheet("検索用");

            //xlsx
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);
            
            NPOI.SS.UserModel.IRow rowcheck = ws.GetRow(0);
            NPOI.SS.UserModel.ICell cellcheck = rowcheck.Cells[0];
            string strCheck = cellcheck.StringCellValue.Trim();
            

            if (strCheck==string.Empty || strCheck!="コード表")
            {
                System.Windows.Forms.MessageBox.Show("コード表シートがありません。取り込めません。ファイルの中身を確認してください"
                      , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                return false;
            }

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from paycode where cym={intcym} ", tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            
            int col = 0;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし

                //最終行を最大値とする                
                wf.SetMax(ws.LastRowNum);

                //列数確認
                NPOI.SS.UserModel.IRow rowheader = ws.GetRow(1);
                if (rowheader.Cells.Count != 4)
                {
                    System.Windows.Forms.MessageBox.Show("列数が4ではありません。取り込めません。ファイルの中身を確認してください"
                        , System.Windows.Forms.Application.ProductName
                        , System.Windows.Forms.MessageBoxButtons.OK
                        , System.Windows.Forms.MessageBoxIcon.Exclamation);


                    return false;
                }

                //登録しない行はスキップ
                bool flgskip = false;

                for (int r = 2; r <= ws.LastRowNum; r++)
                {
                    paycode imp = new paycode();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);              

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    for (int c = 1; c < row.LastCellNum; c++)
                    {

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //2列目が数値に変換できない場合は見出し行と見なし飛ばす                        
                        //ループを抜けないと行が終わらない                        
                        if (c == 1)
                        {
                            if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp))

                            {
                                flgskip = true;
                                break;
                            }
                            else
                            {
                                flgskip = false;
                            }
                        }


                        #region 値の取込

                        if (c == 1) imp.f001_accountnum = getCellValueToString(cell).ToString();    //口座番号;
                        if (c == 2) imp.f002_code = getCellValueToString(cell);                     //昭島市用口座管理コード;
                        if (c == 3) imp.f003_biko = getCellValueToString(cell);                     //備考;

                        if (c == 4)
                        {
                            if (double.TryParse(getCellValueToString(cell), out double tmpaddupddate))
                            {
                                imp.f004_addupddate = DateTime.FromOADate(tmpaddupddate).ToString("yyyy-MM-dd");               //更新日;
                            }
                        }
                        if (c == 4) imp.cym = intcym;                                               //メホール請求年月;

                        #endregion


                    }
                                   
                    if (!flgskip)
                    {
                        wf.InvokeValue++;
                        wf.LogPrint($"口座番号: {imp.f001_accountnum}");

                        if (!DB.Main.Insert<paycode>(imp, tran)) return false;
                    }

                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                //wbhs.Close();
                wb.Close();
                //fs.Close();
            }

        }



        /// <summary>
        /// 仮コード表インポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool dataImport_2(int _cym, WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            int intcym = _cym;


            //xlsファイル
            //System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.Open);
            //wbhs = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);
            //NPOI.SS.UserModel.ISheet ws = wbhs.GetSheet("検索用");

            //xlsx
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(1);

            NPOI.SS.UserModel.IRow rowcheck = ws.GetRow(0);
            NPOI.SS.UserModel.ICell cellcheck = rowcheck.Cells[0];
            string strCheck = cellcheck.StringCellValue.Trim();


            if (strCheck == string.Empty || strCheck != "仮コード表")
            {
                System.Windows.Forms.MessageBox.Show("仮コード表シートがありません。取り込めません。ファイルの中身を確認してください"
                      , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                return false;
            }

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from paycode_temporary where cym={intcym} ", tran);
            if (!cmd.TryExecuteNonQuery()) return false;


            int col = 0;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし

                //最終行を最大値とする                
                wf.SetMax(ws.LastRowNum);

                //列数確認
                NPOI.SS.UserModel.IRow rowheader = ws.GetRow(6);
                if (rowheader.Cells.Count != 4)
                {
                    System.Windows.Forms.MessageBox.Show("列数が4ではありません。取り込めません。ファイルの中身を確認してください"
                        , System.Windows.Forms.Application.ProductName
                        , System.Windows.Forms.MessageBoxButtons.OK
                        , System.Windows.Forms.MessageBoxIcon.Exclamation);


                    return false;
                }

                //登録しない行はスキップ
                bool flgskip = false;

                for (int r = 7; r <= ws.LastRowNum; r++)
                {
                    paycode_temporary imp = new paycode_temporary();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    for (int c = 1; c < row.LastCellNum; c++)
                    {

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //2列目が数値に変換できない場合は見出し行と見なし飛ばす                        
                        //ループを抜けないと行が終わらない                        
                        if (c == 1)
                        {
                            if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp))

                            {
                                flgskip = true;
                                break;
                            }
                            else
                            {
                                flgskip = false;
                            }
                        }


                        #region 値の取込

                        if (c == 1) imp.f001_tempcode = getCellValueToString(cell).ToString();     //仮コード;
                        if (c == 2) imp.f002_accountnum = getCellValueToString(cell);              //口座番号;
                        if (c == 3) imp.f003_biko = getCellValueToString(cell);                    //備考;
                        if (c == 4) imp.f004_code = getCellValueToString(cell);                    //本コード;     

                        if (c == 5) imp.cym = intcym;                                              //メホール請求年月;

                        #endregion


                    }

                    if (!flgskip)
                    {
                        wf.InvokeValue++;
                        wf.LogPrint($"口座番号: {imp.f002_accountnum}");

                        if (!DB.Main.Insert<paycode_temporary>(imp, tran)) return false;
                    }

                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                //wbhs.Close();
                wb.Close();
                //fs.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }



    }


}
