﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Akishimashi
{
    class clsExport
    {
        
        public string f001_kbn { get; set; } = string.Empty;                        //データ区分;
        public string f002_code { get; set; } = string.Empty;                       //債権者コード;
        public string f003_firstnum { get; set; } = string.Empty;                   //最初の申請書番号;
        public string f004_finishnum { get; set; } = string.Empty;                  //最後の申請書番号;
        public string f005_ketteibi { get; set; } = string.Empty;                   //支給決定日;
        public string f006_maisu { get; set; } = string.Empty;                      //枚数;
        public string f007_skip { get; set; } = string.Empty;                       //スキップ;
        public string f008_kbn2 { get; set; } = string.Empty;                       //データ区分２;
        public string f009_skip { get; set; } = string.Empty;                       //スキップ;
        public string f010_year { get; set; } = string.Empty;                       //該当　年;
        public string f011_month { get; set; } = string.Empty;                      //　　　月;
        public string f012_insnum { get; set; } = string.Empty;                     //保険者番号;
        public string f013_hnum { get; set; } = string.Empty;                       //記号番号;
        public string f014_skip3 { get; set; } = string.Empty;                      //スキップ;
        public string f015_gender { get; set; } = string.Empty;                     //性別;
        public string f016_birthnengo { get; set; } = string.Empty;                 //生年号;
        public string f017_birthyear { get; set; } = string.Empty;                  //生年;
        public string f018_startyear { get; set; } = string.Empty;                  //施術開始　　年;
        public string f019_startmonth { get; set; } = string.Empty;                 //施術開始　　月;
        public string f020_startday { get; set; } = string.Empty;                   //施術開始　　日;
        public string f021_finishyear { get; set; } = string.Empty;                 //施術終了　　年;
        public string f022_finishmonth { get; set; } = string.Empty;                //施術終了　　月;
        public string f023_finishday { get; set; } = string.Empty;                  //施術終了　　日;
        public string f024_counteddays { get; set; } = string.Empty;                //実日数;
        public string f025_total { get; set; } = string.Empty;                      //合計;
        public string f026_chrage { get; set; } = string.Empty;                     //請求金額;
        public string f027_number { get; set; } = string.Empty;                     //申請書番号;
        public string f028_sregnumber { get; set; } = string.Empty;                 //登録記号番号;
        public string f029_fukushi { get; set; } = string.Empty;                    //福祉区分;

      
    }

}
