﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Akishimashi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 合計金額位置
        /// </summary>        
        Point posTotal = new Point(1000, 1400);


        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts;

   
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();          
            
            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM ,};

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, panelhnum, panelPname,};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal, panelTotal,};

            #endregion

          
            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //初回表示時
            if (!InitControl(app, verifyBoxY)) return;
            
            if (app != null)
            {
                setApp(app);
            }        

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //種類に応じたセンシティブ制御
            App app = (App)bsApp.Current;            
            InitControl(app, verifyBoxY);            
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// センシティブ制御
        /// </summary>
        /// <param name="app"></param>
        /// <param name="verifyboxY"></param>
        /// <returns></returns>
        private bool InitControl(App app,VerifyBox verifyboxY)
        {
          
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            panelPname.Visible = false;
            panelhnum.Visible = false;            
            panelTotal.Visible = false;            
            
            verifyBoxF1.Visible = false;
            labelF1.Visible = false;
            
            verifyBoxDouiDr.Visible = false;
            labelDouiDr.Visible = false;
            
            pDoui.Visible = false;

            labelBui.Visible = false;
            verifyBoxBui.Visible = false;

            checkBoxVisit.Visible = false;
            checkBoxVisitKasan.Visible = false;

            labelNewCont.Visible = false;
            labelNewCont2.Visible = false;
            verifyBoxNewCont.Visible = false;

            //AppがNull（入力前）のときはscanのを採用
            APP_TYPE type = app.AppType == APP_TYPE.NULL ? scan.AppType : app.AppType;


            //20220421105306 furukawa st ////////////////////////
            //ベリファイ後入力修正する際のセンシティブ
            
            //ベリファイ済みで1回目ボタンの場合＝多分間違いを修正するときだけ
            if (app.StatusFlagCheck(StatusFlag.ベリファイ済) && firstTime)
            {
                
                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.状態記入書:
                        break;

                    case clsInputKind.施術同意書:
                        
                        break;

                    default:

                        //scan.apptypeにしとかないと、間違って続紙等登録したとき、
                        //App.apptypeは変わらないので、間違ったままの判定になってしまい入力欄が出ない
                        switch (scan.AppType)
                        {

                            case APP_TYPE.柔整:
                                panelPname.Visible = true;
                                panelhnum.Visible = true;
                                panelTotal.Visible = true;

                                labelBui.Visible = true;
                                verifyBoxBui.Visible = true;

                                labelNewCont.Visible = true;
                                labelNewCont2.Visible = true;
                                verifyBoxNewCont.Visible = true;

                                break;

                            case APP_TYPE.あんま:
                                panelPname.Visible = true;
                                panelhnum.Visible = true;                                
                                panelTotal.Visible = true;

                                verifyBoxDouiDr.Visible = true;
                                labelDouiDr.Visible = true;
                                pDoui.Visible = true;

                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;
                                break;

                            case APP_TYPE.鍼灸:

                                panelPname.Visible = true;
                                panelhnum.Visible = true;
                                panelTotal.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxDouiDr.Visible = true;
                                labelDouiDr.Visible = true;
                                pDoui.Visible = true;

                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;


                                break;

                            default:break;
                        }

                        break;
                }

            }
            //20220421105306 furukawa ed ////////////////////////



            //未入力・入力後表示前
            else if (verifyboxY.Text.Trim() == string.Empty)
            {
                switch (type)
                {
                    case APP_TYPE.長期:
                    case APP_TYPE.続紙:
                    case APP_TYPE.不要:
                    case APP_TYPE.エラー:
                    case APP_TYPE.同意書裏:
                    case APP_TYPE.施術報告書:
                    case APP_TYPE.状態記入書:
                        break;

                    case APP_TYPE.同意書:
                        
                        break;

                    case APP_TYPE.柔整:
                        panelPname.Visible = true;
                        panelhnum.Visible = true;
                        panelTotal.Visible = true;

                        labelBui.Visible = true;
                        verifyBoxBui.Visible = true;

                        labelNewCont.Visible = true;
                        labelNewCont2.Visible = true;
                        verifyBoxNewCont.Visible = true;

                        break;

                    case APP_TYPE.鍼灸:
                        panelPname.Visible = true;
                        panelhnum.Visible = true;
                        panelTotal.Visible = true;

                        verifyBoxF1.Visible = true;
                        labelF1.Visible = true;
                        
                        verifyBoxDouiDr.Visible =true;
                        labelDouiDr.Visible = true;
                        pDoui.Visible = true;

                        checkBoxVisit.Visible = true;
                        checkBoxVisitKasan.Visible = true;

                        break;

                    case APP_TYPE.あんま:
                        panelPname.Visible = true;
                        panelhnum.Visible =  true;                        
                        panelTotal.Visible = true;

                        verifyBoxDouiDr.Visible = true;
                        labelDouiDr.Visible = true;
                        pDoui.Visible = true;

                        checkBoxVisit.Visible = true;
                        checkBoxVisitKasan.Visible = true;

                        break;
                    default:
                        break;
                }
            }

            //登録後、再表示時
            else
            {

                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.状態記入書:
                        break;

                    case clsInputKind.施術同意書:
                        
                        break;

                    default:

                 
                        switch (type)
                        {

                            case APP_TYPE.柔整:
                                panelPname.Visible = true;
                                panelhnum.Visible = true;
                                panelTotal.Visible = true;

                                labelBui.Visible = true;
                                verifyBoxBui.Visible = true;

                                labelNewCont.Visible = true;
                                labelNewCont2.Visible = true;
                                verifyBoxNewCont.Visible = true;

                                break;

                            case APP_TYPE.あんま:
                                panelPname.Visible = true;
                                panelhnum.Visible = true;                                
                                panelTotal.Visible = true;

                                verifyBoxDouiDr.Visible = true;
                                labelDouiDr.Visible = true;
                                pDoui.Visible = true;

                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;
                                break;

                            case APP_TYPE.鍼灸:

                                panelPname.Visible = true;
                                panelhnum.Visible = true;
                                panelTotal.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                verifyBoxDouiDr.Visible = true;
                                labelDouiDr.Visible = true;
                                pDoui.Visible = true;

                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;



                                break;
                            default:
                                
                                
                                break;
                        }

                        break;
                }
            }

            return true;
        }


    

        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            ////表示初期化
            //phnum.Visible = false;
            //panelTotal.Visible = false;            
            //pZenkai.Enabled = false;
            //dgv.Visible = false;


            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = 
                "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

          //  InitControl(scan.AppType);


            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    

                    //被保険者記号
                    setValue(verifyBoxHmark, app.HihoNum.ToString() != string.Empty ? app.HihoNum.Substring(0,2) : string.Empty, firstTime, nv);

                    //被保険者番号
                    setValue(verifyBoxHnum, app.HihoNum.ToString() != string.Empty ? app.HihoNum.Substring(2) : string.Empty, firstTime, nv) ;
                    

                    //受療者名
                    setValue(verifyBoxPname, app.PersonName.ToString(), firstTime, nv);

                    //受療者性別
                    setValue(verifyBoxSex, app.Sex, firstTime, nv);
                   
                    //受療者生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);


                    //施術開始日1日
                    setValue(verifyBoxF1Start, app.FushoStartDate1.Day.ToString(), firstTime, nv);

                    //施術終了日1日
                    setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day.ToString(), firstTime, nv);

                    

                    //実日数
                    setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);
                    

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //請求額
                    setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                    

                    //柔整師登録記号番号
                    setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);


                    //20220418160324 furukawa st ////////////////////////
                    //以下は1回入力にする



                    //ナンバリング
                    //20220421102022 furukawa st ////////////////////////
                    //ナンバリングをベリファイにして重複を避ける（2022/04/21　福田さん
                    
                    setValue(verifyBoxNumbering, app.Numbering.ToString(), firstTime, nv);
                    //setValue(verifyBoxNumbering, app.Numbering.ToString(), firstTime, false);
                    //20220421102022 furukawa ed ////////////////////////


                    //公費負担者
                    setValue(verifyBoxPublicExpanse, app.TaggedDatas.KouhiNum, firstTime, false);

                    //20220616172340 furukawa st ////////////////////////
                    //公費受給者番号不要
                    
                    //公費受給者
                    //      setValue(verifyBoxReci, app.TaggedDatas.JukyuNum, firstTime, false);
                    //20220616172340 furukawa ed ////////////////////////


                    //被保険者名
                    setValue(verifyBoxHihoname, app.HihoName, firstTime, false);

                    //新規継続
                    setValue(verifyBoxNewCont, (int)app.NewContType, firstTime, false);

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, false, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, false);

                    //部位
                    setValue(verifyBoxBui, app.TaggedDatas.count, firstTime, false);

                    //施術師名
                    setValue(verifyBoxDrName, app.DrName, firstTime, false);

                    //施術所名
                    setValue(verifyBoxHosName, app.ClinicName, firstTime, false);

                    //同意日
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, false, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    //同意医師名
                    setValue(verifyBoxDouiDr, app.TaggedDatas.GeneralString1, firstTime, false);


                    //往療あり
                    setValue(checkBoxVisit, app.Distance == 0 ? false : true, firstTime, false);
                    
                    //往療加算
                    setValue(checkBoxVisitKasan, app.VisitAdd == 0 ? false : true, firstTime, false);


                    //setValue(verifyBoxNumbering, app.Numbering.ToString(), firstTime, nv);
                    //setValue(verifyBoxPublicExpanse, app.TaggedDatas.KouhiNum, firstTime, nv);
                    //setValue(verifyBoxReci, app.TaggedDatas.JukyuNum, firstTime, nv);
                    //setValue(verifyBoxHihoname, app.HihoName, firstTime, nv);
                    //setValue(verifyBoxNewCont, (int)app.NewContType, firstTime, nv);
                    //setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE,verifyBoxF1FirstD);
                    //setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
                    //setValue(verifyBoxBui, app.TaggedDatas.count, firstTime, nv);
                    //setValue(verifyBoxDrName, app.DrName, firstTime, nv);
                    //setValue(verifyBoxHosName, app.ClinicName, firstTime, nv);
                    //setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);
                    //setValue(verifyBoxDouiDr, app.TaggedDatas.GeneralString1, firstTime, nv);
                    //checkBoxVisit.Checked = app.Distance == 0 ? false : true;
                    //checkBoxVisitKasan.Checked = app.VisitAdd == 0 ? false : true;
                    //20220418160324 furukawa ed ////////////////////////

                    break;
            }
        }


        #endregion

        #region 各種更新

        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //ナンバリング
                    int numbering = verifyBoxNumbering.GetIntValue();
                    setStatus(verifyBoxNumbering, numbering < 0);

                    //公費負担者
                    string strPublicExpence = verifyBoxPublicExpanse.Text.Trim();
                
                    //20220616172151 furukawa st ////////////////////////
                    //公費受給者番号不要

                    //      公費受給者
                    //      string strPublicReci = verifyBoxReci.Text.Trim();
                    //20220616172151 furukawa ed ////////////////////////


                    //被保険者証記号
                    string strHMark = verifyBoxHmark.Text.Trim();
                    setStatus(verifyBoxHmark, strHMark == string.Empty);

                    //被保険者証番号   
                    string strHnum = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, strHnum==string.Empty);

                    //被保険者名
                    string strHihoName = verifyBoxHihoname.Text.Trim();

                    //受療者
                    string strPname = verifyBoxPname.Text.Trim();

                    //性別
                    int intSex = verifyBoxSex.GetIntValue();
                    setStatus(verifyBoxSex, !new int[] { 1, 2 }.Contains(intSex));

                    //生年月日
                    DateTime dtPBirthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //新規継続=>柔整
                    int newcont = verifyBoxNewCont.GetIntValue();
                    if(scan.AppType==APP_TYPE.柔整) setStatus(verifyBoxNewCont, !new int[] { 1, 2 }.Contains(newcont));

                    //負傷名1=>鍼灸のみ
                    string f1 = verifyBoxF1.Text.Trim();
                    if (scan.AppType == APP_TYPE.鍼灸) setStatus(verifyBoxF1, f1.Trim() == string.Empty);

                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM,verifyBoxF1FirstD);


                    //開始日
                    DateTime f1Start1 = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1Start.GetIntValue());

                    //終了日
                    DateTime f1Finish1 = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1Finish.GetIntValue());


                    //実日数
                    int counteddays = verifyBoxCountedDays.GetIntValue();
                    setStatus(verifyBoxCountedDays, counteddays < 1);

                    //部位
                    int bui = verifyBoxBui.GetIntValue();
                    if (scan.AppType == APP_TYPE.柔整) setStatus(verifyBoxBui, bui < 0 || bui>10);


                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //請求金額
                    int charge = verifyBoxCharge.GetIntValue();
                    setStatus(verifyBoxCharge, charge < 100 || charge > 200000);

                    //施術所名
                    string strClinicName = verifyBoxHosName.Text.Trim();
                    setStatus(verifyBoxHosName, strClinicName == string.Empty);

                    //施術師名
                    string strDrName = verifyBoxDrName.Text.Trim();
                    setStatus(verifyBoxDrName, strDrName == string.Empty);

                    //柔整師登録記号番号
                    string strDrCode = verifyBoxDrCode.Text.Trim();
                    setStatus(verifyBoxDrCode, strDrCode.Length > 10);

                    //往療 入力チェック無し
                    //往療加算　入力チェック無し

                    
                    
                    string strDouiDr = verifyBoxDouiDr.Text.Trim();
                    DateTime dtDoui = DateTime.MinValue;

                    if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                    {
                        //同意医師名
                        setStatus(verifyBoxDouiDr, strDouiDr == string.Empty);

                        //同意年月日
                        dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);
                    }


                    //20220421101949 furukawa st ////////////////////////
                    //連番チェック機構。ベリファイでも良いというので未使用

                    //if ((numbering > 0) && (!CheckNumbering(app.Aid, app.CYM, numbering.ToString())))

                    //{
                    //    verifyBoxNumbering.BackColor = Color.YellowGreen;
                    //    var res = MessageBox.Show("ナンバリングが連番ではありませんが宜しいですか？", "",
                    //        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    //    if (res != System.Windows.Forms.DialogResult.Yes) return false;
                    //}
                    //20220421101949 furukawa ed ////////////////////////


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    


                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月


                    //ナンバリング
                    app.Numbering = numbering.ToString();

                    //公費負担者
                    app.TaggedDatas.KouhiNum = strPublicExpence;

                    //20220616172318 furukawa st ////////////////////////
                    //公費受給者番号不要
                    
                    //公費受給者
                    //      app.TaggedDatas.JukyuNum = strPublicReci;
                    //20220616172318 furukawa ed ////////////////////////


                    //被保険者証記号番号
                    app.HihoNum =$"{strHMark}{strHnum}";

                    //被保険者名
                    app.HihoName = strHihoName;

                    //受療者
                    app.PersonName = strPname;

                    //性別
                    app.Sex = intSex;

                    //生年月日
                    app.Birthday = dtPBirthday;

                    //新規継続
                    app.NewContType = newcont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

                    //申請書種別
                    app.AppType = scan.AppType;

                    //初検日
                    app.FushoFirstDate1 = f1FirstDt;


                    //開始日
                    app.FushoStartDate1 = f1Start1;

                    //終了日
                    app.FushoFinishDate1 = f1Finish1;


                    //負傷名1 
                    if (verifyBoxF1.Visible) app.FushoName1 = verifyBoxF1.Text.Trim();


                    //部位
                    app.TaggedDatas.count = bui;

                    //合計
                    app.Total = total;

                    //実日数
                    app.CountedDays = counteddays;

                    //請求金額
                    app.Charge = charge;

                    //施術所名
                    app.ClinicName = strClinicName;

                    //施術師名
                    app.DrName = strDrName;

                    //柔整師登録記号番号
                    app.DrNum = strDrCode;

                    //往療
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;

                    //往療加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;


                    //同意医師名
                    app.TaggedDatas.GeneralString1 = strDouiDr;

                    //同意年月日
                    app.TaggedDatas.DouiDate = dtDoui;


                    #endregion


                    break;

            }
          

            return true;
        }


        //20220421101552 furukawa st ////////////////////////
        //連番チェック機構。ベリファイでも良いというので未使用だが、良いアイデアなので置いとく
        
        /// <summary>
        /// ナンバリング連番チェック
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="year"></param>
        /// <param name="strNumbering"></param>
        /// <returns></returns>
        private bool CheckNumbering(int aid, int cym, string strNumbering)
        {            
            //１．このグループIDと一つ前のグループIDの申請書だけを取得し、AID降順で並べる。
            //２．取得したリストの0番目（AID最大）と現在の申請書のナンバリングの差が１以外の場合はfalse
            //そうすればグループまたぎも確認できる。グループIDが増えたところで、一つ前のグループIDしか見ないので、最大でも200件となるのでさほど重くない
            using (DB.Command cmd = DB.Main.CreateCmd($"select aid,numbering from application " +
                $"where cym={cym} and " +
                $"groupid in ({this.scanGroup.GroupID},{this.scanGroup.GroupID-1}) and " +
                $"aid<{aid} and aapptype in (6,7,8) order by aid desc"))
            {
                var lst = cmd.TryExecuteReaderList();
                if (lst.Count == 0) return true;
                if (lst[0][1].ToString() == string.Empty) return true;

                int currNumbering = int.Parse(strNumbering);
                App prevApp = new App();
                prevApp.Aid = int.Parse(lst[0][0].ToString());
                prevApp.Numbering = lst[0][1].ToString();
                int prevNumbering = int.Parse(prevApp.Numbering);

                if (currNumbering - prevNumbering != 1) return false;
                else return true;
            }
        }
        //20220421101552 furukawa ed ////////////////////////

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

    
       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
   

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void verifyBoxPname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー => mediveryよりもってきた
            {            
                var cvb = (VerifyBox)sender;
                cvb.Text = verifyBoxHihoname.Text;
                //かつ、次のコントロールへ移る
                SelectNextControl(verifyBoxPname, true, true, true, false);
                return;
             
            }
        }

        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            //1ヶ月前データを表示
            if (verifyBoxHmark.Text.Trim() == string.Empty || verifyBoxHnum.Text.Trim() == string.Empty) return;
            
            //20220616173916 furukawa st ////////////////////////
            //ハイフンなし繋ぎ

            PastData.PersonalData p=PastData.PersonalData.SelectRecordOne($"hnum='{verifyBoxHmark.Text.Trim()}{verifyBoxHnum.Text.Trim()}'");
            //PastData.PersonalData p = PastData.PersonalData.SelectRecordOne($"hnum='{verifyBoxHmark.Text.Trim()}-{verifyBoxHnum.Text.Trim()}'");
            //20220616173916 furukawa ed ////////////////////////

            if (p == null) return;
            setValue(verifyBoxHihoname, p.hname, firstTime,true);
            setValue(verifyBoxPname, p.pname, firstTime, true);
            setValue(verifyBoxSex, p.psex, firstTime, true);
            setDateValue(p.pbirthday, firstTime, true, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
        }

        private void verifyBoxDrCode_Validated(object sender, EventArgs e)
        {
            if (verifyBoxDrCode.Text.Trim() == string.Empty) return;
            PastData.ClinicData c = PastData.ClinicData.SelectRecordOne($"doctornum='{verifyBoxDrCode.Text.Trim()}'");
            if (c == null) return;
            setValue(verifyBoxDrName, c.doctorname, firstTime, true);
            setValue(verifyBoxHosName, c.clinicname, firstTime, true);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotal;
            
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            
        }
        #endregion


    
       
    }      
}
