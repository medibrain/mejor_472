﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Akishimashi
{
    public partial class PayInputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsPayCode = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(400, 2000);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 400);
        Point posFusho = new Point(100, 800);
        Point posCost = new Point(800, 2000);
        Point posDays = new Point(600, 800);
        Point posNewCont = new Point(800, 1200);
        Point posOryo = new Point(400, 1200);
        Point posNumbering = new Point(800, 2600);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, numberingControls;

        //口座テーブル
        List<paycode> lst = new List<paycode>();

        public PayInputForm(ScanGroup sGroup, bool firstTime, int aid)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxAccountNo };
            hihoNumControls = new Control[] { };
            personControls = new Control[] { };
            dayControls = new Control[] { };
            costControls = new Control[] { };
            fushoControls = new Control[] { };
            newContControls = new Control[] { };
            oryoControls = new Control[] { };
            numberingControls = new Control[] { };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            list = list.FindAll(x => x.YM > 0);
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.PayCode)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.PayCode)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.PayCode)].HeaderText = "支払コード";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].HeaderText = "状態";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            var l = new List<paycode>();
            bsPayCode.DataSource = l;
            dataGridPayCode.DataSource = bsPayCode;

            dataGridPayCode.Columns[nameof(paycode.f000_importid)].HeaderText = "インポートID";
            dataGridPayCode.Columns[nameof(paycode.f001_accountnum)].HeaderText = "口座番号";
            dataGridPayCode.Columns[nameof(paycode.f002_code)].HeaderText = "債権者コード";
            dataGridPayCode.Columns[nameof(paycode.f003_biko)].HeaderText = "備考";
            dataGridPayCode.Columns[nameof(paycode.f004_addupddate)].HeaderText = "更新日";
          

            dataGridPayCode.Columns[nameof(paycode.f000_importid)].Visible = true;
            dataGridPayCode.Columns[nameof(paycode.f001_accountnum)].Visible = true;
            dataGridPayCode.Columns[nameof(paycode.f002_code)].Visible = true;
            dataGridPayCode.Columns[nameof(paycode.f003_biko)].Visible = true;
            dataGridPayCode.Columns[nameof(paycode.f004_addupddate)].Visible = true;
            dataGridPayCode.Columns[nameof(paycode.cym)].Visible = false;
          
            dataGridPayCode.Columns[nameof(paycode.f000_importid)].Width = 70;
            dataGridPayCode.Columns[nameof(paycode.f001_accountnum)].Width = 80;
            dataGridPayCode.Columns[nameof(paycode.f002_code)].Width = 80;
            dataGridPayCode.Columns[nameof(paycode.f003_biko)].Width = 160;
            dataGridPayCode.Columns[nameof(paycode.f004_addupddate)].Width = 100;
          

            dataGridPayCode.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridPayCode.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;


            bsPayCode.CurrentChanged += BsPayCode_CurrentChanged;

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
          
            if (app != null)
            {
                lst = paycode.SelectAll(app.CYM);
                setApp(app);
            }
            verifyBoxAccountNo.Focus();
        }

        private void BsPayCode_CurrentChanged(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrWhiteSpace(verifyBoxPayCode.Text)) return;

            if (bsPayCode.Count < 1) return;
            //入力エラー時、グリッドの値取得箇所でエラー落ちするのを回避

            try
            {
                foreach(paycode item in lst)
                {
                    if(item.f001_accountnum== verifyBoxPayCode.Text.Trim())
                    {
                        verifyBoxPayCode.Text = item.f002_code;
                        break;
                    }
                }
            }
            catch
            {

            }


        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (numberingControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            int aNumber = verifyBoxAccountNo.GetIntValue();
            setStatus(verifyBoxAccountNo, aNumber < 1);

            int gCode = verifyBoxPayCode.GetIntValue();
            setStatus(verifyBoxPayCode, gCode < 1 || verifyBoxPayCode.Text.Trim().Length < 1);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //値の反映
            app.AccountNumber = verifyBoxAccountNo.Text.Trim();
            app.PayCode = verifyBoxPayCode.Text.Trim();       
         
            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.拡張ベリ済)) return true;

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {                
                var ut = firstTime ? App.UPDATE_TYPE.FirstInputEx : App.UPDATE_TYPE.SecondInputEx;
                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                
                //支払先コードをAUX matchingid03に入れる
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, "", "", app.PayCode)) return false;
                


                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);
            bsPayCode.Clear();

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.UfirstEx) +
                "\r\n入力2:  " + User.GetUserName(app.UsecondEx);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.拡張入力済))
            {
                setValues(app);
            }
            else
            {
                bsPayCode.ResetBindings(false);
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// 新規口座作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="apptype"></param>
        /// <param name="strAccountNum"></param>
        /// <returns></returns>
        private bool CreateNewAccount(int cym,APP_TYPE apptype,string strAccountNum)
        {
            DB.Command cmd = null;
            DB.Transaction tran = new DB.Transaction(DB.Main);
            StringBuilder sb = new StringBuilder();
            string strApptype = String.Empty;

            if (MessageBox.Show($"登録しますか？",
                Application.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
            
            
            try
            {
                switch (apptype)
                {
                    case APP_TYPE.柔整:strApptype = "1";    
                        break;
                    case APP_TYPE.鍼灸:strApptype = "5";    
                        break;
                    case APP_TYPE.あんま:strApptype = "6";    
                        break;
                }

                sb.Clear();

                //1.paycode_temporaryから仮コード（柔整１，鍼灸５、あんま６始まり）の最小値＆口座番号が入っていないレコード取得

                sb.AppendLine($" select ");
                sb.AppendLine($" max(f001_tempcode) ");
                sb.AppendLine($" from paycode_temporary ");
                sb.AppendLine($" where cym={cym} ");
                sb.AppendLine($" and f002_accountnum='' ");
                sb.AppendLine($" and substr(f001_tempcode,1,1)='{strApptype}'");
                 
                
                cmd = DB.Main.CreateCmd(sb.ToString());
                var data = cmd.TryExecuteScalar();
                //2.1が存在しない場合、新規レコードを登録する
                //paycode_temporaryの仮コードの最大値+1を新規レコードとして登録。そのレコードの「処理」に「新規口座」、「口座番号」に口座番号を入れる
                if (data.ToString()==String.Empty)
                {
                    //仮コードテーブルに入れる
                    sb.Clear();
                    sb.AppendLine($"INSERT INTO paycode_temporary(");
                    sb.AppendLine($"	f001_tempcode, f002_accountnum, f003_biko,  cym)");
                    sb.AppendLine($"	VALUES (");
                    sb.AppendLine($"	(select cast(max(f001_tempcode) as int) +1 from paycode_temporary where cym={cym} and substr(f001_tempcode,1,1)='{strApptype}')");
                    sb.AppendLine($"	,'{strAccountNum}','{DateTime.Now.ToString("yyyy-MM-dd")}新規口座作成',{cym});");
                    cmd = DB.Main.CreateCmd(sb.ToString(),tran);
                    cmd.TryExecuteNonQuery();

                  
                }
                else
                {
                    //3.1が存在したらそのレコードを更新
                    sb.Clear();
                    sb.AppendLine($" update paycode_temporary set ");
                    sb.AppendLine($" f002_accountnum='{strAccountNum}'");
                    sb.AppendLine($" ,f003_biko='{DateTime.Now.ToString("yyyy-MM-dd")}新規口座を割り当て'");
                    sb.AppendLine($" where cym={cym} ");
                    sb.AppendLine($" and f001_tempcode='{data.ToString()}'");
                    cmd = DB.Main.CreateCmd(sb.ToString(), tran);
                    cmd.TryExecuteNonQuery();

                    
                }


                //仮コードテーブルに入れたレコードを本コードにもコピー
                sb.Clear();
                sb.AppendLine($"INSERT INTO paycode(");
                sb.AppendLine($"	f001_accountnum,f002_code, f003_biko,  cym)");
                sb.AppendLine($"	select f002_accountnum,f001_tempcode,f003_biko,cym from paycode_temporary ");
                sb.AppendLine($"	where f002_accountnum='{strAccountNum}' and cym={cym};");
                cmd = DB.Main.CreateCmd(sb.ToString(), tran);
                cmd.TryExecuteNonQuery();

                tran.Commit();


                //口座リスト取り直し
                lst = paycode.SelectAll(scan.CYM);

                bsPayCode.DataSource = selectPaycode(strAccountNum);
                bsPayCode.ResetBindings(false);

                verifyBoxPayCode.Text = dataGridPayCode.CurrentRow.Cells[nameof(paycode.f002_code)].Value.ToString().Trim();
                MessageBox.Show($"新規口座作成しました");

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

 

        private void verifyBoxAccountNo_Leave(object sender, EventArgs e)        
        {
            
            //引数で指定した値を用いてデータソースを設定する

            bsPayCode.DataSource = selectPaycode(verifyBoxAccountNo.Text.Trim());
            //bsPayCode.DataSource = paycode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.ResetBindings(false);

            if (verifyBoxAccountNo.Text.Trim() == string.Empty) return;

            //入力した口座番号が存在しない場合新規作成
            if (bsPayCode.Count == 0)
            {
                verifyBoxPayCode.Text = string.Empty;

                MessageBox.Show("口座番号が存在しません。新規作成してください"
                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else verifyBoxPayCode.Text = dataGridPayCode.CurrentRow.Cells[nameof(paycode.f002_code)].Value.ToString().Trim();

        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        #region 画像関連
        private void buttonCreateNewAccount_Click(object sender, EventArgs e)
        {
            //新規口座作成処理

            if (!CreateNewAccount(scan.CYM, scan.AppType, verifyBoxAccountNo.Text.Trim()))
            {
                MessageBox.Show("新規口座登録に失敗しました"
                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
        }

      

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (numberingControls.Contains(t)) posNumbering = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        #endregion



        //20220414151020 furukawa st ////////////////////////
        //選択したときに支払先コードを入れる
        
        private void dataGridPayCode_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            verifyBoxPayCode.Text = dataGridPayCode.CurrentRow.Cells[nameof(paycode.f002_code)].Value.ToString().Trim();
        }
        //20220414151020 furukawa ed ////////////////////////


        private List<paycode> selectPaycode(string strAccNum)
        {
            List<paycode> l = new List<paycode>();

            //リスト
            foreach (paycode item in lst)
            {
                if (item.f001_accountnum == strAccNum)
                {
                    l.Add(item);

                    //20220414150941 furukawa st ////////////////////////
                    //口座情報は複数ある前提にする

                    //break;
                    //20220414150941 furukawa ed ////////////////////////
                }
            }
            return l;
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.拡張入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.拡張ベリ済);

            //申請書
            setValue(verifyBoxAccountNo, app.AccountNumber, firstTime, nv);
            setValue(verifyBoxPayCode, app.PayCode, firstTime, nv);

            List<paycode> l = selectPaycode(verifyBoxAccountNo.Text.Trim());

           
            //var l = paycode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.DataSource = l;
            bsPayCode.ResetBindings(false);

            
            //複数行あるときはループで検索
            
            if (l.Count > 1)
            {
                if (verifyBoxPayCode.Text != string.Empty)
                {
                    for(int r = 0; r < l.Count; r++)
                    {
                        //複数行ある場合支払先コードに合致する行を選択
                        if (dataGridPayCode.Rows[r].Cells[2].Value.ToString().Contains(verifyBoxPayCode.Text))
                        {
                            dataGridPayCode.Rows[r].Selected = true;
                            break;
                        }
                        else dataGridPayCode.Rows[r].Selected = false;
                    }
                }
            }


            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxAccountNo.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
