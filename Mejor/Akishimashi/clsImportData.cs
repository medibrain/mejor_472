﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Akishimashi
{
    /// <summary>
    /// コード表
    /// </summary>
    public partial class paycode
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                                  //インポートID;        
        public string f001_accountnum { get; set; } = string.Empty;               //口座番号;
        public string f002_code { get; set; } = string.Empty;                     //口座番号に付けるコード;
        public string f003_biko { get; set; } = string.Empty;                     //備考;
        public string f004_addupddate { get; set; } = string.Empty;               //追加/更新日;
        public int cym { get; set; } = 0;                                         //メホール請求年月;

        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<paycode> SelectAll(int cym)
        {
            List<paycode> lst = new List<paycode>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from paycode where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    paycode p = new paycode();
                    p.f000_importid = int.Parse(item[0].ToString());
                    p.f001_accountnum = item[1].ToString().Trim();
                    p.f002_code = item[2].ToString().Trim();
                    p.f003_biko = item[3].ToString().Trim();
                    p.f004_addupddate = item[4].ToString().Trim();
                    p.cym = cym;

                    lst.Add(p);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }


    /// <summary>
    /// 仮コード表
    /// </summary>
    public partial class paycode_temporary
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                          //インポートID;
        public string f001_tempcode { get; set; } = string.Empty;         //仮コード;
        public string f002_accountnum { get; set; } = string.Empty;       //口座番号;
        public string f003_biko { get; set; } = string.Empty;             //備考;
        public string f004_code { get; set; } = string.Empty;             //本コード;
        public int cym { get; set; } = 0;                                 //メホール請求年月;

        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<paycode_temporary> SelectAll(int cym)
        {
            List<paycode_temporary> lst = new List<paycode_temporary>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from paycode_temp where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    paycode_temporary p = new paycode_temporary();
                    p.f000_importid = int.Parse(item[0].ToString());
                    p.f001_tempcode = item[1].ToString().Trim();
                    p.f002_accountnum = item[2].ToString().Trim();
                    p.f003_biko = item[3].ToString().Trim();
                    p.f004_code = item[4].ToString().Trim();
                    p.cym = cym;

                    lst.Add(p);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}

