﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Akishimashi
{
    public partial class frmExport : Form
    {
        /// <summary>
        /// 支給決定日は保険者より連絡がある
        /// </summary>
        public string strKetteibi { get; set; } = string.Empty;

        /// <summary>
        /// 出力先
        /// </summary>
        public string strOutputPath { get; set; } = string.Empty;

        public frmExport()
        {
            InitializeComponent();
        }

        private void buttonExp_Click(object sender, EventArgs e)
        {
            if (textBoxKetteibi.Text.Trim().Length != 7)
            {
                MessageBox.Show("年号＋YYMMDD（和暦）で入力してください"
                    ,Application.ProductName
                    ,MessageBoxButtons.OK
                    ,MessageBoxIcon.Exclamation);
                return;
            }

            if (textBoxOutputPath.Text.Trim()== string.Empty)
            {
                MessageBox.Show("出力先を選択してください"
                    , Application.ProductName
                    , MessageBoxButtons.OK
                    , MessageBoxIcon.Exclamation);

                return;
            }

            strKetteibi = textBoxKetteibi.Text.Trim();
            this.Close();
        }

        private void buttonOutputPath_Click(object sender, EventArgs e)
        {
            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            strOutputPath = $"{dlg.Name}\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
            textBoxOutputPath.Text = strOutputPath;

        }
    }
}
