﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;


namespace Mejor.Ibarakishi
{
    /// <summary>
    /// 保険者から提供される　給付データテーブルクラス→2021/04/05国保データに移行するかも
    /// </summary>
    public class dataimport_ahk
    {

        #region テーブル構造

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int F000importid { get;set; }=0;                                     //管理用ID;
        public int F001renban { get; set; } = 0;                                    //No.;
        public string F002insnum { get; set; } = string.Empty;                      //保険者番号;
        public string F003kohifutan { get; set; } = string.Empty;                   //公費負担者番号;
        public string F004hnum { get; set; } = string.Empty;                        //被保険者証番号;
        public string F005kohijyukyu { get; set; } = string.Empty;                  //公費受給者番号;
        public string F006pbirthday { get; set; } = string.Empty;                   //生年月日;
        public string F007pgender { get; set; } = string.Empty;                     //性別;
        public string F008pname { get; set; } = string.Empty;                       //氏名;
        public string F009ym { get; set; } = string.Empty;                          //施術年月;
        public string F010sid { get; set; } = string.Empty;                         //医療機関コード;
        public string F011sregnumber { get; set; } = string.Empty;                  //登録記号番号;
        public string F012shinsaym { get; set; } = string.Empty;                    //審査年月;
        public string F013comnum { get; set; } = string.Empty;                      //レセプト全国共通キー;
        public string F014 { get; set; } = string.Empty;                            //支給区分;
        public string F015 { get; set; } = string.Empty;                            //返戻理由;        
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;             //生年月日西暦;
        public int ymad { get; set; } = 0;                                         //施術年月西暦;
        public int shinsaymad { get; set; } = 0;                                   //審査年月西暦;
        public int cym { get; set; } = 0;                                          //メホール請求年月;

        #endregion 

        public static int _cym = 0;

        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int cym)
        {
            _cym = cym;
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "茨木市あはき提供データ";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;


            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                var lst = new List<dataimport_ahk>();
                wf.LogPrint("Excelファイルを読み込んでいます");

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    
                    var ex = System.IO.Path.GetExtension(fileName);
                    var xlsx = new XSSFWorkbook(fs);
                    var sheet = xlsx.GetSheetAt(1);
                    lst.AddRange(sheetTodataimport_ahks(sheet));

                }

                wf.SetMax(lst.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in lst)
                    {
                        wf.InvokeValue++;
                        if (!DB.Main.Insert(item))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
            }

            MessageBox.Show("インポートが終了しました");
            return true;

        }


        /// <summary>
        /// シートからデータをコピー
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns></returns>
        private static List<dataimport_ahk> sheetTodataimport_ahks(ISheet sheet)
        {
            var l = new List<dataimport_ahk>();
            var rowCount = sheet.LastRowNum + 2;

            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    var row = sheet.GetRow(i);
                    if (row.GetCell(1).CellType != CellType.Numeric) continue;
                    var dataimportahk = new dataimport_ahk();

                    dataimportahk.F001renban = int.Parse(row.GetCell(1).NumericCellValue.ToString());   //No.;
                    dataimportahk.F002insnum = row.GetCell(2).StringCellValue;                          //保険者番号;
                    dataimportahk.F003kohifutan = row.GetCell(3).StringCellValue;                       //公費負担者番号;
                    dataimportahk.F004hnum = row.GetCell(4).StringCellValue;                            //被保険者証番号;
                    dataimportahk.F005kohijyukyu = row.GetCell(5).StringCellValue;                      //公費受給者番号;
                    dataimportahk.F006pbirthday = row.GetCell(6).StringCellValue;                       //生年月日;
                    dataimportahk.F007pgender = row.GetCell(7).StringCellValue;                         //性別;
                    dataimportahk.F008pname = row.GetCell(8).StringCellValue;                           //氏名;
                    dataimportahk.F009ym = row.GetCell(9).StringCellValue;                              //施術年月;
                    dataimportahk.F010sid = row.GetCell(10).StringCellValue;                            //医療機関コード;
                    dataimportahk.F011sregnumber = row.GetCell(11).StringCellValue;                     //登録記号番号;
                    dataimportahk.F012shinsaym = row.GetCell(12).StringCellValue;                       //審査年月;
                    dataimportahk.F013comnum = row.GetCell(13).StringCellValue;                         //レセプト全国共通キー;
                    dataimportahk.F014 = row.GetCell(14).StringCellValue;                               //支給区分;
                    dataimportahk.F015 = row.GetCell(15).StringCellValue;                               //返戻理由;

                    dataimportahk.pbirthdayad = DateTimeEx.GetDateFromJstr7(dataimportahk.F006pbirthday);                                    //生年月日西暦;
                    dataimportahk.ymad =DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(dataimportahk.F009ym));                                 //施術年月西暦;
                    dataimportahk.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(dataimportahk.F012shinsaym));                    //審査年月西暦;
                    dataimportahk.cym = _cym;

                    l.Add(dataimportahk);
                }
                catch
                {
                    continue;
                }
            }
            return l;
        }



        /// <summary>
        /// 給付データ抽出
        /// </summary>
        /// <param name="strwhere">抽出条件(whereいらんsql)</param>
        /// <returns></returns>
        public static List<dataimport_ahk> select(string strwhere)
        {
            List<dataimport_ahk> lst = new List<dataimport_ahk>();
            var l=DB.Main.Select<dataimport_ahk>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }


        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport_ahk where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport_ahk group by cym order by cym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

    }

}
