﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Fuchushi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;
      
        /// <summary>
        /// 国保データ柔整あはき両方
        /// </summary>
        List<kokuhodata> lstKokuho = new List<kokuhodata>();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount,verifyBoxCountedDays};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge,checkBoxVisit,checkBoxVisitKasan };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5 };

           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //国保データのリスト（柔整）                
              //  lstKokuho = kokuhodata.select(app.CYM);

                setApp(app);
            }

            setInputControlbyAppType();
            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            setInputControlbyAppType();
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {      
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelTotal.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelTotal.Visible = false;
                    pDoui.Visible = true;
                    break;
                    
                default:
                    panelTotal.Visible = true;
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            //国保データ

            kokuhodata k = lstKokuho[0];

            dgv.Columns[nameof(k.f000_kokuhoid)].Width = 50;             //シーケンス番号
            dgv.Columns[nameof(k.f001_hihonum)].Width = 80;             //証番号　全角想定
            dgv.Columns[nameof(k.f002_pname)].Width = 100;               //氏名
            dgv.Columns[nameof(k.f003_shinryoym)].Width = 80;            //診療年月 和暦想定
            dgv.Columns[nameof(k.f004_sid)].Width = 100;                 //機関コード
            dgv.Columns[nameof(k.f005_sname)].Width = 150;               //医療機関名
            dgv.Columns[nameof(k.f006_setainum)].Width = 40;             //世帯番号
            dgv.Columns[nameof(k.f007_atenanum)].Width = 40;             //宛名番号
            dgv.Columns[nameof(k.f008_pbirthday)].Width = 100;           //生年月日 和暦想定
            dgv.Columns[nameof(k.f009_pgender)].Width = 40;              //性別
            dgv.Columns[nameof(k.f010_ratio)].Width = 40;                //給付割合
            dgv.Columns[nameof(k.f011_counteddays)].Width = 40;          //実日数
            dgv.Columns[nameof(k.f012_total)].Width = 80;                //決定点数

            dgv.Columns[nameof(k.f013_score_kbn)].Width = 40;             //点数表

            dgv.Columns[nameof(k.f014_honke)].Width = 40;                 //本家
            dgv.Columns[nameof(k.f015_nyugai)].Width = 40;                //入外
            dgv.Columns[nameof(k.f016_hihomark)].Width = 140;              //証記号

            dgv.Columns[nameof(k.f017)].Width = 40;                       //種別１
            dgv.Columns[nameof(k.f018)].Width = 40;                       //種別２
            dgv.Columns[nameof(k.f019)].Width = 40;                       //診療・療養費別

            dgv.Columns[nameof(k.f020_shinsaym)].Width = 60;              //審査年月

            dgv.Columns[nameof(k.f021_insnum)].Width = 40;                //保険者番号

            dgv.Columns[nameof(k.f022_comnum)].Width = 150;                //レセプト全国共通キー

            dgv.Columns[nameof(k.f023_dcode)].Width = 40;                 //処方機関コード
            dgv.Columns[nameof(k.f024_dname)].Width = 40;                 //処方機関名

            dgv.Columns[nameof(k.f025)].Width = 40;                        //クリ
            dgv.Columns[nameof(k.f026)].Width = 40;                        //ケア
            dgv.Columns[nameof(k.f027)].Width = 40;                        //容認
            dgv.Columns[nameof(k.f028_newest)].Width = 40;                 //最新履歴
            dgv.Columns[nameof(k.f029)].Width = 40;                        //原本所在
            dgv.Columns[nameof(k.f030_prefcorss)].Width = 40;              //県内外
            dgv.Columns[nameof(k.f031)].Width = 40;                        //療養費種別
            dgv.Columns[nameof(k.f032)].Width = 40;                        //食事基準額
            dgv.Columns[nameof(k.f033)].Width = 40;                        //不当
            dgv.Columns[nameof(k.f034)].Width = 40;                        //第三者
            dgv.Columns[nameof(k.f035_kyufu_limit)].Width = 40;            //給付制限
            dgv.Columns[nameof(k.f036_expensive)].Width = 40;              //高額
            dgv.Columns[nameof(k.f037)].Width = 40;                        //予約
            dgv.Columns[nameof(k.f038)].Width = 40;                        //処理
            dgv.Columns[nameof(k.f039)].Width = 40;                        //疑義種別
            dgv.Columns[nameof(k.f040)].Width = 40;                        //参考
            dgv.Columns[nameof(k.f041)].Width = 40;                        //状態
            dgv.Columns[nameof(k.f042)].Width = 40;                        //コード情報
            dgv.Columns[nameof(k.f043)].Width = 40;                        //原本区分
            dgv.Columns[nameof(k.f044_fusen01)].Width = 40;               //付箋01
            dgv.Columns[nameof(k.f045_fusen02)].Width = 40;               //付箋02
            dgv.Columns[nameof(k.f046_fusen03)].Width = 40;               //付箋03
            dgv.Columns[nameof(k.f047_fusen04)].Width = 40;               //付箋04
            dgv.Columns[nameof(k.f048_fusen05)].Width = 40;               //付箋05
            dgv.Columns[nameof(k.f049_fusen06)].Width = 40;               //付箋06
            dgv.Columns[nameof(k.f050_fusen07)].Width = 40;               //付箋07
            dgv.Columns[nameof(k.f051_fusen08)].Width = 40;               //付箋08
            dgv.Columns[nameof(k.f052_fusen09)].Width = 40;               //付箋09
            dgv.Columns[nameof(k.f053_fusen10)].Width = 40;               //付箋10
            dgv.Columns[nameof(k.f054_fusen11)].Width = 40;               //付箋11
            dgv.Columns[nameof(k.f055_fusen12)].Width = 40;               //付箋12
            dgv.Columns[nameof(k.f056_fusen13)].Width = 40;               //付箋13
            dgv.Columns[nameof(k.f057_fusen14)].Width = 40;               //付箋14
            dgv.Columns[nameof(k.hihonum_narrow)].Width = 40;               //被保険者記号番号メホール用
            dgv.Columns[nameof(k.shinryoymad)].Width = 40;                //診療年月西暦メホール用
            dgv.Columns[nameof(k.shinsaymad)].Width = 40;                 //審査年月西暦メホール用
            dgv.Columns[nameof(k.birthdayad)].Width = 40;                 //生年月日西暦メホール用
            dgv.Columns[nameof(k.cym)].Width = 80;                        //メホール請求年月


            dgv.Columns[nameof(k.f000_kokuhoid)].HeaderText = "rrid";
            dgv.Columns[nameof(k.f001_hihonum)].HeaderText = "証番号";
            dgv.Columns[nameof(k.f002_pname)].HeaderText = "氏名";
            dgv.Columns[nameof(k.f003_shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(k.f004_sid)].HeaderText = "機関コード";
            dgv.Columns[nameof(k.f005_sname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(k.f006_setainum)].HeaderText = "世帯";
            dgv.Columns[nameof(k.f007_atenanum)].HeaderText = "宛名";
            dgv.Columns[nameof(k.f008_pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(k.f009_pgender)].HeaderText = "性別";
            dgv.Columns[nameof(k.f010_ratio)].HeaderText = "割合";
            dgv.Columns[nameof(k.f011_counteddays)].HeaderText = "日数";
            dgv.Columns[nameof(k.f012_total)].HeaderText = "決定";
            dgv.Columns[nameof(k.f013_score_kbn)].HeaderText = "点数表";
            dgv.Columns[nameof(k.f014_honke)].HeaderText = "本家";
            dgv.Columns[nameof(k.f015_nyugai)].HeaderText = "入外";
            dgv.Columns[nameof(k.f016_hihomark)].HeaderText = "証記号";
            dgv.Columns[nameof(k.f017)].HeaderText = "種別１";
            dgv.Columns[nameof(k.f018)].HeaderText = "種別２";
            dgv.Columns[nameof(k.f019)].HeaderText = "診療・療養費別";
            dgv.Columns[nameof(k.f020_shinsaym)].HeaderText = "審査年月";
            dgv.Columns[nameof(k.f021_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(k.f022_comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(k.f023_dcode)].HeaderText = "処方機関コード";
            dgv.Columns[nameof(k.f024_dname)].HeaderText = "処方機関名";

            dgv.Columns[nameof(k.f025)].HeaderText = "クリ";
            dgv.Columns[nameof(k.f026)].HeaderText = "ケア";
            dgv.Columns[nameof(k.f027)].HeaderText = "容認";
            dgv.Columns[nameof(k.f028_newest)].HeaderText = "最新履歴";
            dgv.Columns[nameof(k.f029)].HeaderText = "原本所在";
            dgv.Columns[nameof(k.f030_prefcorss)].HeaderText = "県内外";
            dgv.Columns[nameof(k.f031)].HeaderText = "療養費種別";
            dgv.Columns[nameof(k.f032)].HeaderText = "食事基準額";
            dgv.Columns[nameof(k.f033)].HeaderText = "不当";
            dgv.Columns[nameof(k.f034)].HeaderText = "第三者";
            dgv.Columns[nameof(k.f035_kyufu_limit)].HeaderText = "給付制限";
            dgv.Columns[nameof(k.f036_expensive)].HeaderText = "高額";
            dgv.Columns[nameof(k.f037)].HeaderText = "予約";
            dgv.Columns[nameof(k.f038)].HeaderText = "処理";
            dgv.Columns[nameof(k.f039)].HeaderText = "疑義種別";
            dgv.Columns[nameof(k.f040)].HeaderText = "参考";
            dgv.Columns[nameof(k.f041)].HeaderText = "状態";
            dgv.Columns[nameof(k.f042)].HeaderText = "コード情報";
            dgv.Columns[nameof(k.f043)].HeaderText = "原本区分";
            dgv.Columns[nameof(k.f044_fusen01)].HeaderText = "付箋01";
            dgv.Columns[nameof(k.f045_fusen02)].HeaderText = "付箋02";
            dgv.Columns[nameof(k.f046_fusen03)].HeaderText = "付箋03";
            dgv.Columns[nameof(k.f047_fusen04)].HeaderText = "付箋04";
            dgv.Columns[nameof(k.f048_fusen05)].HeaderText = "付箋05";
            dgv.Columns[nameof(k.f049_fusen06)].HeaderText = "付箋06";
            dgv.Columns[nameof(k.f050_fusen07)].HeaderText = "付箋07";
            dgv.Columns[nameof(k.f051_fusen08)].HeaderText = "付箋08";
            dgv.Columns[nameof(k.f052_fusen09)].HeaderText = "付箋09";
            dgv.Columns[nameof(k.f053_fusen10)].HeaderText = "付箋10";
            dgv.Columns[nameof(k.f054_fusen11)].HeaderText = "付箋11";
            dgv.Columns[nameof(k.f055_fusen12)].HeaderText = "付箋12";
            dgv.Columns[nameof(k.f056_fusen13)].HeaderText = "付箋13";
            dgv.Columns[nameof(k.f057_fusen14)].HeaderText = "付箋14";
            dgv.Columns[nameof(k.hihonum_narrow)].HeaderText = "被保険者記号番号メホール用";
            dgv.Columns[nameof(k.shinryoymad)].HeaderText = "診療年月西暦メホール用";
            dgv.Columns[nameof(k.shinsaymad)].HeaderText = "審査年月西暦メホール用";
            dgv.Columns[nameof(k.birthdayad)].HeaderText = "生年月日西暦メホール用";
            dgv.Columns[nameof(k.cym)].HeaderText = "メホール請求年月";


            dgv.Columns[nameof(k.f000_kokuhoid)].Visible = true;    //シーケンス
            dgv.Columns[nameof(k.f001_hihonum)].Visible = true;        //証番号　全角想定
            dgv.Columns[nameof(k.f002_pname)].Visible = true;          //氏名
            dgv.Columns[nameof(k.f003_shinryoym)].Visible = true;      //診療年月 和暦想定
            dgv.Columns[nameof(k.f004_sid)].Visible = true;            //機関コード
            dgv.Columns[nameof(k.f005_sname)].Visible = true;          //医療機関名
            dgv.Columns[nameof(k.f006_setainum)].Visible = false;       //世帯番号
            dgv.Columns[nameof(k.f007_atenanum)].Visible = false;       //宛名番号
            dgv.Columns[nameof(k.f008_pbirthday)].Visible = true;      //生年月日 和暦想定
            dgv.Columns[nameof(k.f009_pgender)].Visible = true;        //性別
            dgv.Columns[nameof(k.f010_ratio)].Visible = true;          //給付割合
            dgv.Columns[nameof(k.f011_counteddays)].Visible = true;    //実日数
            dgv.Columns[nameof(k.f012_total)].Visible = true;          //決定点数
            dgv.Columns[nameof(k.f013_score_kbn)].Visible = false;      //点数表
            dgv.Columns[nameof(k.f014_honke)].Visible = true;          //本家
            dgv.Columns[nameof(k.f015_nyugai)].Visible = true;         //入外
            dgv.Columns[nameof(k.f016_hihomark)].Visible = false;       //証記号
            dgv.Columns[nameof(k.f017)].Visible = false;                //種別１
            dgv.Columns[nameof(k.f018)].Visible = false;                //種別２
            dgv.Columns[nameof(k.f019)].Visible = false;                //診療・療養費別
            dgv.Columns[nameof(k.f020_shinsaym)].Visible = true;       //審査年月
            dgv.Columns[nameof(k.f021_insnum)].Visible = false;         //保険者番号
            dgv.Columns[nameof(k.f022_comnum)].Visible = true;         //レセプト全国共通キー
            dgv.Columns[nameof(k.f023_dcode)].Visible = false;          //処方機関コード
            dgv.Columns[nameof(k.f024_dname)].Visible = false;          //処方機関名

            dgv.Columns[nameof(k.f025)].Visible = false;               //クリ
            dgv.Columns[nameof(k.f026)].Visible = false;               //ケア
            dgv.Columns[nameof(k.f027)].Visible = false;               //容認
            dgv.Columns[nameof(k.f028_newest)].Visible = false;        //最新履歴
            dgv.Columns[nameof(k.f029)].Visible = false;               //原本所在
            dgv.Columns[nameof(k.f030_prefcorss)].Visible = false;     //県内外
            dgv.Columns[nameof(k.f031)].Visible = false;               //療養費種別
            dgv.Columns[nameof(k.f032)].Visible = false;               //食事基準額
            dgv.Columns[nameof(k.f033)].Visible = false;               //不当
            dgv.Columns[nameof(k.f034)].Visible = false;               //第三者
            dgv.Columns[nameof(k.f035_kyufu_limit)].Visible = false;   //給付制限
            dgv.Columns[nameof(k.f036_expensive)].Visible = false;     //高額
            dgv.Columns[nameof(k.f037)].Visible = false;               //予約
            dgv.Columns[nameof(k.f038)].Visible = false;               //処理
            dgv.Columns[nameof(k.f039)].Visible = false;               //疑義種別
            dgv.Columns[nameof(k.f040)].Visible = false;               //参考
            dgv.Columns[nameof(k.f041)].Visible = false;               //状態
            dgv.Columns[nameof(k.f042)].Visible = false;               //コード情報
            dgv.Columns[nameof(k.f043)].Visible = false;               //原本区分
            dgv.Columns[nameof(k.f044_fusen01)].Visible = false;       //付箋01
            dgv.Columns[nameof(k.f045_fusen02)].Visible = false;       //付箋02
            dgv.Columns[nameof(k.f046_fusen03)].Visible = false;       //付箋03
            dgv.Columns[nameof(k.f047_fusen04)].Visible = false;       //付箋04
            dgv.Columns[nameof(k.f048_fusen05)].Visible = false;       //付箋05
            dgv.Columns[nameof(k.f049_fusen06)].Visible = false;       //付箋06
            dgv.Columns[nameof(k.f050_fusen07)].Visible = false;       //付箋07
            dgv.Columns[nameof(k.f051_fusen08)].Visible = false;       //付箋08
            dgv.Columns[nameof(k.f052_fusen09)].Visible = false;       //付箋09
            dgv.Columns[nameof(k.f053_fusen10)].Visible = false;       //付箋10
            dgv.Columns[nameof(k.f054_fusen11)].Visible = false;       //付箋11
            dgv.Columns[nameof(k.f055_fusen12)].Visible = false;       //付箋12
            dgv.Columns[nameof(k.f056_fusen13)].Visible = false;       //付箋13
            dgv.Columns[nameof(k.f057_fusen14)].Visible = false;       //付箋14
            dgv.Columns[nameof(k.hihonum_narrow)].Visible = false;       //被保険者記号番号メホール用
            dgv.Columns[nameof(k.shinryoymad)].Visible = false;        //診療年月西暦メホール用
            dgv.Columns[nameof(k.shinsaymad)].Visible = false;         //審査年月西暦メホール用
            dgv.Columns[nameof(k.birthdayad)].Visible = false;         //生年月日西暦メホール用
            dgv.Columns[nameof(k.cym)].Visible = true;                 //メホール請求年月


        }

        /// <summary>
        /// 国保用グリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid_kokuho(App app = null)
        {
            List<kokuhodata> lstimp = new List<kokuhodata>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            
            foreach (kokuhodata item in lstKokuho)
            {
                //被保番半角、メホール請求年月、合計金額で探す
                if (item.hihonum_narrow == verifyBoxHnum.Text.Trim() &&
                    item.cym == scan.CYM &&
                    item.f012_total.Replace(",","") == verifyBoxTotal.Text.ToString().Trim())

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            if (lstimp.Count == 0) return;

            InitGrid();

            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstKokuho.Count > 1) dgv.ClearSelection();


            if (app != null && app.RrID.ToString() != string.Empty)//&& app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {

                    int intRRid = int.Parse(r.Cells["f000_kokuhoid"].Value.ToString());
                                     
                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells["f022_comnum"].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (r.Cells["importid"].Value.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {

                        if (intTotal==int.Parse(verifyBoxTotal.Text.Trim()) && strhnum==verifyBoxHnum.Text.Trim())
                        {
                            //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;

                        }
                        //提供データIDと違う場合の処理抜け                        
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }
            
            if (lstKokuho == null || lstKokuho.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            //20200812164508 furukawa st ////////////////////////
            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstKokuho.Count == 1 || dgv.SelectedRows.Count == 1)
            //else if (lstimp.Count == 1)
            //20200812164508 furukawa ed ////////////////////////
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }
    
        #endregion


        private void setInputControlbyAppType()
        {

            //verifyBoxF1.Visible = false;
            //verifyBoxF2.Visible = false;
            //verifyBoxF3.Visible = false;
            //verifyBoxF4.Visible = false;
            //verifyBoxF5.Visible = false;
            //checkBoxVisit.Visible = false;
            //checkBoxVisitKasan.Visible = false;




            //柔整のみ使用

            //20210422101300 furukawa st ////////////////////////
            //合計金額は両方になったので制御不要

            //verifyBoxTotal.Enabled = scan.AppType == APP_TYPE.柔整 ? true : false;
            //verifyBoxTotal.Visible= scan.AppType == APP_TYPE.柔整 ? true : false;
            //labelTotal.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;
            //20210422101300 furukawa ed ////////////////////////



            verifyBoxFushoCount.Enabled = scan.AppType == APP_TYPE.柔整 ? true : false;
            verifyBoxFushoCount.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;
            labelFushoCount.Visible = scan.AppType == APP_TYPE.柔整 ? true : false;

            //あはきのみ使用
            checkBoxVisitKasan.Enabled = scan.AppType == APP_TYPE.柔整 ? false : true;
            checkBoxVisitKasan.Visible = scan.AppType == APP_TYPE.柔整 ? false : true;
            checkBoxVisit.Enabled = scan.AppType == APP_TYPE.柔整 ? false : true;            
            checkBoxVisit.Visible = scan.AppType == APP_TYPE.柔整 ? false : true;

            //20210422095349 furukawa st ////////////////////////
            //あはきのみ請求金額有効           
            verifyBoxCharge.Visible = scan.AppType == APP_TYPE.柔整 ? false : true;
            labelCharge.Visible = scan.AppType == APP_TYPE.柔整 ? false : true;
            //20210422095349 furukawa ed ////////////////////////


            //鍼の場合
            verifyBoxF1.Enabled = scan.AppType == APP_TYPE.鍼灸 ? true : false;
            verifyBoxF1.Visible = scan.AppType == APP_TYPE.鍼灸 ? true : false;
            labelF1.Visible = scan.AppType == APP_TYPE.鍼灸 ? true : false;




        }

        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

        

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum,VbStrConv.Narrow), firstTime, nv);

                    //受療者性別
                    setValue(verifyBoxPsex, app.Sex, firstTime, nv);

                    //受療者生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                    

                    //負傷カウント                   
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);  

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //負傷1負傷名 本来鍼だけだが、鍼以外入力できないので空欄になるので区別しない
                    setValue(verifyBoxF1, app.FushoName1.ToString(), firstTime, nv);


                    //往療距離
                    setValue(checkBoxVisit, app.Distance == 0 ? false : true, firstTime, nv);
                    //往療加算
                    setValue(checkBoxVisitKasan, app.VisitAdd == 0 ? false : true, firstTime, nv);
                    

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //請求金額
                    if(app.AppType != APP_TYPE.柔整) setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //実日数
                    setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                    ////グリッド選択する                    
                    //createGrid_kokuho(app);
                   
                    
                    break;
            }
        }


        #endregion

        #region 各種更新


        private bool CheckNumbering(int intNumbering)
        {
            if (intPrevNumbering == 0) return true;
            if (intPrevNumbering > intNumbering) return true;//戻ったときの対策
            if (Math.Abs(intNumbering - intPrevNumbering) == 1) return true;
            else return false;
            
        }

        /// <summary>
        /// 提供データ(国保システムCSV)をAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistKokuho(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    kokuhodata k = lstKokuho[0];
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(k.f000_kokuhoid)].Value.ToString()); //ID

                    
                    //入力を優先させる
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                                //被保険者証番号
                    app.PersonName = dgv.CurrentRow.Cells[nameof(k.f002_pname)].Value.ToString();                      //受療者名
                    app.InsNum = dgv.CurrentRow.Cells[nameof(k.f021_insnum)].Value.ToString();                         //保険者番号
                    app.Sex = dgv.CurrentRow.Cells[nameof(k.f009_pgender)].Value.ToString() == "男" ? 1 : 2;           //受療者性別                    
                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.birthdayad)].Value.ToString());        //受療者生年月日
                    app.ClinicName = dgv.CurrentRow.Cells[nameof(k.f005_sname)].Value.ToString();                      //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f004_sid)].Value.ToString();                         //医療機関コード
                    app.ComNum = dgv.CurrentRow.Cells[nameof(k.f022_comnum)].Value.ToString();                         //レセプト全国共通キー
                    app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(k.f011_counteddays)].Value.ToString()  );  //施術日数
                    app.AppType = APP_TYPE.柔整;          //種別

                    app.ClinicName=dgv.CurrentRow.Cells[nameof(k.f005_sname)].Value.ToString(); //施術所名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f004_sid)].Value.ToString();  //施術所番号
                    app.TaggedDatas.GeneralString2 = dgv.CurrentRow.Cells[nameof(k.shinsaymad)].Value.ToString();  //審査年月
                    app.TaggedDatas.GeneralString3 = dgv.CurrentRow.Cells[nameof(k.f020_shinsaym)].Value.ToString();  //審査年月和暦

                    //本家区分=本人家族x100+本家区分
                    //string strHonke = dgv.CurrentRow.Cells[nameof(k.f014_honke)].Value.ToString();
                    string strfamily = dgv.CurrentRow.Cells[nameof(k.f014_honke)].Value.ToString() == "本人" ? "2" : "6";
                    app.Family = int.Parse(strfamily);

                    //app.Family = int.Parse(strfamily) * 100;

                    //switch (strHonke)
                    //{
                    //    case "本人":
                    //        app.Family += 2;
                    //        break;

                    //    case "家族":
                    //        app.Family += 6;
                    //        break;

                    //    case "高齢者一般":
                    //        app.Family += 8;
                    //        break;

                    //    case "高齢者７割":
                    //        app.Family += 0;
                    //        break;

                    //    default:
                    //        app.Family += 99;//エラー用
                    //        break;
                    //}


                    


                }
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


    
        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //受療者生年月日
                    DateTime dtpbirthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //受療者性別
                    int intverifyBoxPsex = verifyBoxPsex.GetIntValue();
                    setStatus(verifyBoxPsex, intverifyBoxPsex<0 || intverifyBoxPsex>2);


                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);


                    //負傷1負傷名 本来鍼だけだがテキスト項目かつ、コントロールが非表示なだけなので区分せず取得
                    string strverifyBoxF1Name = verifyBoxF1.Text.Trim();



                    //20210422120255 furukawa st ////////////////////////
                    //CheckVerifyは2回入力時しか対応してないぽい

                    //往療有無
                    //checkBoxVisit.CheckVerify();
                    ////往療加算
                    //checkBoxVisitKasan.CheckVerify();
                    //20210422120255 furukawa ed ////////////////////////




                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);


                    //20210422095434 furukawa st ////////////////////////
                    //あはきのみ請求金額を登録

                    //請求金額
                    int intcharge = verifyBoxCharge.GetIntValue();
                    if(verifyBoxCharge.Visible) setStatus(verifyBoxCharge, intcharge < 100 || intcharge > 200000);
                    //20210422095434 furukawa ed ////////////////////////

                    //負傷数
                    int fushoCount = verifyBoxFushoCount.GetIntValue();
                    setStatus(verifyBoxFushoCount, fushoCount <0 || fushoCount>10);


                    //実日数 0未満31超でエラー
                    int intCountedDays = verifyBoxCountedDays.GetIntValue();
                    setStatus(verifyBoxCountedDays,intCountedDays<0 || intCountedDays>31);

                    
                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    //20210422132035 furukawa st ////////////////////////
                    //請求金額チェック

                    if (app.AppType == APP_TYPE.鍼灸 || app.AppType == APP_TYPE.あんま)
                    {
                        bool ratioError = false;
                        ratioError = (int)(total * 7 / 10) != intcharge;
                        if (ratioError) ratioError = (int)(total * 8 / 10) != intcharge;
                        if (ratioError) ratioError = (int)(total * 9 / 10) != intcharge;
                        if (ratioError) ratioError = (int)(total * 10 / 10) != intcharge;


                        //合計金額：請求金額チェック
                        //金額でのエラーがあればいったん登録中断
                        if (ratioError)
                        {
                            verifyBoxTotal.BackColor = Color.GreenYellow;
                            verifyBoxCharge.BackColor = Color.GreenYellow;
                            var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                                "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            if (res != System.Windows.Forms.DialogResult.OK) return false;
                        }
                    }
                    //20210422132035 furukawa ed ////////////////////////

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月

                    app.HihoNum =hnumN;                //被保険者証番号                                       

                    //受療者性別
                    app.Sex = intverifyBoxPsex;

                    //受療者生年月日
                    app.Birthday = dtpbirthday;


                    //負傷1負傷名　本来鍼だけだがテキスト項目かつ、コントロールが非表示なだけなので区分せず取得
                    app.FushoName1 = strverifyBoxF1Name;

                    //初検日1
                    app.FushoFirstDate1 = f1FirstDt;


                    //往療距離
                    app.Distance = checkBoxVisit.Checked ? 999:0;

                    //往療料加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;


                    //申請書種別
                    app.AppType = scan.AppType;

                    //負傷数   柔整のみ
                    fushoCount = app.AppType == APP_TYPE.柔整 ? fushoCount : 0;
                    app.TaggedDatas.count = fushoCount;


                    //20210422102322 furukawa st ////////////////////////
                    //合計金額は両方になったので制御不要
                    
                    //合計　柔整のみ
                    //total = app.AppType == APP_TYPE.柔整 ? total : 0;
                    
                    //20210422102322 furukawa ed ////////////////////////
                    app.Total = total;


                    //実日数                    
                    app.CountedDays = intCountedDays;

                    //20210422095557 furukawa st ////////////////////////
                    //あはきのみ請求金額を登録                    
                    //請求金額
                    if (verifyBoxCharge.Visible) app.Charge = intcharge;
                    //20210422095557 furukawa ed ////////////////////////



                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    //20211124161403 furukawa st ////////////////////////
                    //新規継続判定修正
                    

                    if (app.FushoFirstDate1.Year == adYear &&
                        app.FushoFirstDate1.Month == month)
                    //          if (app.FushoFirstDate1.Year == f1FirstDt.Year &&
                    //          app.FushoFirstDate1.Month == f1FirstDt.Month)
                    
                        
                        //20211124161403 furukawa ed ////////////////////////    

                    {

                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

               
                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            createGrid_kokuho(app);
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }


            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
               
        

        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            //往療加算は、有無がチェックされたときに有効とする　おかしな申請書が入力できなくなるのでやめとく
            //VerifyCheckBox c = (VerifyCheckBox)sender;
            //checkBoxVisitKasan.Enabled = c.Checked;
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
           // else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
