﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace Mejor
{
    class MonthBox : TextBox
    {
        public enum DATE_MODE { 西暦, 和暦 }

        public bool _setIndexToMonth = false;

        [System.ComponentModel.Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string Text
        {
            get { return base.Text; }
            private set { base.Text = value; }
        }

        public bool SetIndexToMonth
        {
            get { return _setIndexToMonth; }
            set { _setIndexToMonth = value; }
        }

        [System.ComponentModel.Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new int MaxLength
        {
            get { return base.MaxLength; }
            private set { base.MaxLength = value; }
        }

        /// <summary>
        /// 現在設定されているテキストがデフォルトかどうかを取得します。
        /// </summary>
        public bool IsDefault
        {
            get { return this.Text == DefaultText; }
        }

        /// <summary>
        /// 年月をセットし、日付は無視されます。失敗した場合、またはNULLの場合、デフォルトの表示を行います。
        /// </summary>
        /// <param name="dt"></param>
        public void SetDate(DateTime dt)
        {
            if (dt == DateTimeEx.DateTimeNull)
                this.Text = DefaultText;

            if (_dateMode == DATE_MODE.和暦)
            {
                var t = DateTimeEx.GetJpDateStr(dt);
                this.Text = t == string.Empty ? DefaultText : t.Remove(8);
            }
            else
            {
                this.Text = dt.ToString("yyyy／MM");
            }
        }

        DATE_MODE _dateMode;
        public DATE_MODE DateMode
        {
            get
            {
                return _dateMode;
            }
            set
            {
                if (_dateMode == value) return;

                if (value == DATE_MODE.和暦)
                {
                    DefaultText = "＿＿__年__月";
                    MaxLength = 8;
                    var yStr = DateTimeEx.GetJpYearStr(Text.Remove(4));

                    _dateMode = DATE_MODE.和暦;
                    if (yStr == string.Empty)
                    {
                        Text = "＿＿__年" + Text.Substring(5, 2) + "月";
                    }
                    else
                    {
                        Text = yStr + "年" + Text.Substring(5, 2) + "月";
                    }
                }
                else
                {
                    DefaultText = "____／__";
                    MaxLength = 7;
                    int y = DateTimeEx.GetAdYear(Text.Remove(4));

                    _dateMode = DATE_MODE.西暦;
                    if (y == 0 || y < 1000)
                    {
                        Text = "____／" + Text.Substring(5, 2);
                    }
                    else
                    {
                        Text = y.ToString() + "／" + Text.Substring(5, 2);
                    }
                }
            }
        }

        public string DefaultText { get; private set; }

        /// <summary>
        /// 指定されている年月の日付(必ず1日)を返します。
        /// </summary>
        /// <returns></returns>
        public DateTime GetDate()
        {
            if (this.IsDefault) return DateTimeEx.DateTimeNull;
            var dateText = Text.Replace('_', ' ');
            int y, m;

            if (DateMode == DATE_MODE.和暦)
            {
                if (!DateTimeEx.TryGetYear(dateText.Remove(4), out y)) return DateTimeEx.DateTimeNull;
                if (!int.TryParse(dateText.Substring(5, 2), out m)) return DateTimeEx.DateTimeNull;
            }
            else
            {
                if (!int.TryParse(dateText.Remove(4), out y)) return DateTimeEx.DateTimeNull;
                if (!int.TryParse(dateText.Substring(5, 2), out m)) return DateTimeEx.DateTimeNull;
            }
            if (!DateTimeEx.IsDate(y, m, 1)) return DateTimeEx.DateTimeNull;
            return new DateTime(y, m, 1);
        }

        public MonthBox()
        {
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DefaultText = "＿＿__年__月";
            this._dateMode = DATE_MODE.和暦;
            this.MaxLength = 8;
            this.Text = DefaultText;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MonthBox_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MonthBox_KeyPress);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.MonthBox_Validating);
            this.Enter += new System.EventHandler(this.MonthBox_Enter);
            this.Click += MonthBox_Click;
        }

        void MonthBox_Click(object sender, EventArgs e)
        {
            int ss = SelectionStart;
            if (DateMode == DATE_MODE.和暦)
            {
                if (ss == 0 || ss == 1)
                {
                    ss = 0;
                }
                else if (ss == 2 || ss == 3 || ss == 4)
                {
                    ss = 2;
                }
                else if (ss == 5 || ss == 6 || ss == 7)
                {
                    ss = 5;
                }
                SelectionStart = ss;
                SelectionLength = 2;
            }
            else
            {
                int len = 2;
                if (ss == 0 || ss == 1 || ss == 2 || ss == 3 || ss == 4)
                {
                    ss = 0;
                    len = 4;
                }
                else if (ss == 5 || ss == 6 || ss == 7)
                {
                    ss = 5;
                }
                SelectionStart = ss;
                SelectionLength = len;
            }
        }

        protected override void WndProc(ref Message m)
        {
            //切り取り貼り付け無効
            const int WM_PASTE = 0x0302;
            const int WM_CUT = 0x0300;

            if (m.Msg != WM_PASTE && m.Msg != WM_CUT) base.WndProc(ref m);
        }

        private void MonthBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ReadOnly) return;
            if (DateMode == DATE_MODE.和暦)
            {
                textAdjustJDate(e);
            }
            else
            {
                textAdjustWDate(e);
            }
        }

        private void textAdjustJDate(KeyPressEventArgs e)
        {
            int sIndex = this.SelectionStart;

            //バックスぺースの場合、別処理
            if (e.KeyChar == '\b')
            {
                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = "＿＿__年__月".Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                    SelectionStart = sIndex;
                }
                else if (sIndex == 0 || sIndex == 1 || sIndex == 2)
                {
                    Text = "＿＿" + Text.Substring(2);
                    SelectionStart = 0;
                }
                else if (sIndex == 5 || sIndex == 8)
                {
                    this.Text = Text.Remove(sIndex - 2) + "_" + Text.Substring(sIndex - 1);
                    SelectionStart = sIndex - 2;
                }
                else if (sIndex == 3 || sIndex == 4 || sIndex == 6 || sIndex == 7)
                {
                    this.Text = Text.Remove(sIndex - 1) + "_" + Text.Substring(sIndex);
                    SelectionStart = sIndex - 1;
                }
                e.Handled = true;
            }
            else
            {
                if (!char.IsLetterOrDigit(e.KeyChar))
                {
                    //文字以外の場合処理終了
                    e.Handled = true;
                    return;
                }

                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = "＿＿__年__月".Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                }

                if (sIndex == 0 || sIndex == 1)
                {
                    string era = DateTimeEx.GetEraName(e.KeyChar.ToString());
                    if (era == string.Empty) era = "＿＿";
                    Text = era + Text.Substring(2);
                    SelectionStart = 2;
                }
                else if (sIndex == 2)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 1;
                }
                else if (sIndex == 5)
                {
                    if (e.KeyChar == '0' || e.KeyChar == '1')
                    {
                        this.Text = Text.Remove(5) + e.KeyChar + Text.Substring(sIndex + 1);
                        SelectionStart = 6;
                    }
                    else
                    {
                        this.Text = Text.Remove(5) + "0" + e.KeyChar + Text.Substring(7);
                        SelectionStart = 8;
                    }
                }
                else if (sIndex == 3 || sIndex == 6)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 2;
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// 西暦表示時の処理
        /// </summary>
        /// <param name="e"></param>
        private void textAdjustWDate(KeyPressEventArgs e)
        {
            int sIndex = this.SelectionStart;


            //バックスぺースの場合、別処理
            if (e.KeyChar == '\b')
            {
                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = "____／__".Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                    SelectionStart = sIndex;
                }
                else if (sIndex == 1 || sIndex == 2 || sIndex == 3 || sIndex == 4 || sIndex == 6 || sIndex == 7)
                {
                    this.Text = Text.Remove(sIndex - 1) + "_" + Text.Substring(sIndex);
                    SelectionStart = sIndex - 1;
                }
                else if (sIndex == 5)
                {
                    this.Text = Text.Remove(sIndex - 2) + "_" + Text.Substring(sIndex - 1);
                    SelectionStart = sIndex - 2;
                }
                e.Handled = true;
            }
            else
            {
                if (!char.IsLetterOrDigit(e.KeyChar))
                {
                    //文字以外の場合処理終了
                    e.Handled = true;
                    return;
                }

                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = "____／__".Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                }

                if (sIndex == 0 || sIndex == 1 || sIndex == 2 || sIndex == 6)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 1;
                }
                else if (sIndex == 5)
                {
                    if (e.KeyChar == '0' || e.KeyChar == '1')
                    {
                        this.Text = Text.Remove(5) + e.KeyChar + Text.Substring(sIndex + 1);
                        SelectionStart = 6;
                    }
                    else
                    {
                        this.Text = Text.Remove(5) + "0" + e.KeyChar + Text.Substring(7);
                        SelectionStart = 8;
                    }
                }
                else if (sIndex == 3)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 2;
                }
                e.Handled = true;
            }
        }

        private void MonthBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (ReadOnly) return;
            if (e.KeyCode == Keys.Delete)
            {
                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = DefaultText.Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                }

                int sIndex = this.SelectionStart;

                if (DateMode == DATE_MODE.和暦)
                {
                    if (sIndex == 0 || sIndex == 1)
                    {
                        Text = "＿＿" + Text.Substring(2);
                        SelectionStart = 0;
                    }
                    else if (sIndex == 2 || sIndex == 3 || sIndex == 5 || sIndex == 6)
                    {
                        this.Text = Text.Remove(sIndex) + "_" + Text.Substring(sIndex + 1);
                        SelectionStart = sIndex;
                    }
                }
                else
                {
                    if (sIndex == 0 || sIndex == 1 || sIndex == 2 || sIndex == 3 ||
                        sIndex == 5 || sIndex == 6)
                    {
                        this.Text = Text.Remove(sIndex) + "_" + Text.Substring(sIndex + 1);
                        SelectionStart = sIndex;
                    }
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Space && e.Shift)
            {
                DateMode = DateMode == DATE_MODE.和暦 ? DATE_MODE.西暦 : DATE_MODE.和暦;
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                e.Handled = true;
                int ss = SelectionStart;
                var dt = GetDate();
                if (dt.IsNullDate()) return;
                if (DateMode == DATE_MODE.和暦)
                {
                    if (ss == 0 || ss == 1 || ss == 2 || ss == 3 || ss == 4)
                    {
                        dt = dt.AddYears(1);
                    }
                    else
                    {
                        dt = dt.AddMonths(1);
                    }
                }
                else
                {
                    if (ss == 0 || ss == 1 || ss == 2 || ss == 3 || ss == 4)
                    {
                        dt = dt.AddYears(1);
                    }
                    else
                    {
                        dt = dt.AddMonths(1);
                    }
                }
                SetDate(dt);
                SelectionStart = ss;
            }
            else if (e.KeyCode == Keys.Down)
            {
                e.Handled = true;
                int ss = SelectionStart;
                var dt = GetDate();
                if (dt.IsNullDate()) return;
                if (DateMode == DATE_MODE.和暦)
                {
                    if (ss == 0 || ss == 1 || ss == 2 || ss == 3 || ss == 4)
                    {
                        dt = dt.AddYears(-1);
                    }
                    else
                    {
                        dt = dt.AddMonths(-1);
                    }
                }
                else
                {
                    if (ss == 0 || ss == 1 || ss == 2 || ss == 3 || ss == 4)
                    {
                        dt = dt.AddYears(-1);
                    }
                    else
                    {
                        dt = dt.AddMonths(-1);
                    }
                }
                SetDate(dt);
                SelectionStart = ss;
            }
        }

        private void MonthBox_Validating(object sender, CancelEventArgs e)
        {
            var dt = GetDate();
            if (dt != DateTimeEx.DateTimeNull) SetDate(dt);
        }

        private void MonthBox_Enter(object sender, EventArgs e)
        {
            SelectAll();
        }

        /// <summary>
        /// デフォルトの表示に戻します。("＿＿__年__月" or "____ / __ ")
        /// </summary>
        public override void ResetText()
        {
            this.Text = DefaultText;
        }
    }
}
