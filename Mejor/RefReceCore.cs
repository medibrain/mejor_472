﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;

namespace Mejor
{
    class RefReceCore
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int RrID { get; protected set; }                                 //refreceID
        public APP_TYPE AppType { get; protected set; }                         //申請書タイプ
        public int CYM { get; protected set; }                                  //メホール処理年月
        public int MYM { get; protected set; }                                  //請求年月？
        public string InsNum { get; protected set; } = string.Empty;            //保険者番号
        public string Num { get; protected set; } = string.Empty;               //被保険者証番号
        public string Name { get; protected set; } = string.Empty;              //被保険者名
        public int Days { get; protected set; }                                 //実日数
        public int Total { get; protected set; }                                //合計金額
        public int Charge { get; protected set; }                               //請求金額
        public string ClinicNum { get; protected set; } = string.Empty;         //医療機関番号（施術所番号
        public string ClinicName { get; protected set; } = string.Empty;        //医療機関名（施術所名
        public string DrNum { get; protected set; } = string.Empty;             //柔整師登録記号番号
        public string DrName { get; protected set; } = string.Empty;            //柔整師名
        public string SearchNum { get; protected set; } = string.Empty;         //レセプト全国共通キー
        public int ImportID { get; set; }                                       //インポートごとのID
        public int AID { get; set; }                                            //AID

        public class RefReceImport
        {
            [DB.DbAttribute.PrimaryKey]
            public int ImportID { get; protected set; }
            public APP_TYPE AppType { get; set; }
            public int CYM { get; set; }
            public int ReceCount { get; set; }
            public DateTime ImportDate { get; protected set; }
            public string FileName { get; set; }
            public int UserID { get; protected set; }
            [DB.DbAttribute.Ignore]
            public string UserName => User.GetUserName(UserID);

            private RefReceImport() { }

            public static RefReceImport GetNextImport()
            {
                var sql = "SELECT COALESCE(MAX(importid), 0) FROM refreceimport;";
                var res = DB.Main.QueryFirstOrDefault<int>(sql);

                int iid = res;
                iid++;

                var rri = new RefReceImport();
                rri.ImportID = iid;
                rri.ImportDate = DateTime.Today;
                rri.UserID = User.CurrentUser.UserID;
                return rri;
            }

            public bool Insert(DB.Transaction tran)
            {
                if (ImportID == 0) throw new Exception("ImportIDが指定されていません");
                if (CYM == 0) throw new Exception("処理年月が指定されていません");
                if (AppType == APP_TYPE.NULL) throw new Exception("申請書のタイプが指定されていません");

                return DB.Main.Insert(this, tran);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static RefReceImport GetNextImport() =>
            RefReceImport.GetNextImport();

        public static List<RefReceImport> GetImportList() =>
            DB.Main.SelectAll<RefReceImport>().ToList();


        /// <summary>
        /// refreceテーブル中のrridに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="rrid"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int rrid, int aid, DB.Transaction tran)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = rrid;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery()) return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// refreceテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }


        public static bool DeleteImport(int importID)
        {
            using (var tran = DB.Main.CreateTransaction())
            {
                try
                {
                    var sql = $"DELETE FROM refreceimport WHERE importid={importID}";
                    if (!DB.Main.Excute(sql, tran)) return false;

                    sql = $"DELETE FROM refrece WHERE importid={importID}";
                    if (!DB.Main.Excute(sql, tran)) return false;
                }
                catch(Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
                tran.Commit();
            }
            return true;
        }
    }
}
