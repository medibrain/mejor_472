﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.NagoyaKouwan
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 0);
        Point posHnum = new Point(800, 0);

        //20190226174216 furukawa st ////////////////////////
        //被保番と同じ位置にする
        //Point posPerson = new Point(60, 540);
        Point posPerson = new Point(800, 0);
        //20190226174216 furukawa ed ////////////////////////



        Point posTotal = new Point(800, 2060);
        Point posBuiName = new Point(120, 780);
        Point posBuiDate = new Point(950, 780);
        Point posNumbering = new Point(80, 2100);

        //20190226170302 furukawa st ////////////////////////
        //使用部分のみとする
        Control[] ymConts, hnumConts, personConts, totalConts, buiDateConts;
        //Control[] ymConts, hnumConts, personConts, totalConts, buiNameConts, buiDateConts, numberingConts;
        //20190226170302 furukawa ed ////////////////////////


        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymConts = new Control[] { verifyBoxY, verifyBoxM };
            hnumConts = new Control[] { verifyBoxHnumS, verifyBoxHnumN, verifyBoxFamily, verifyBoxRatio };
            personConts = new Control[] { verifyBoxSex, verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD };


            //20190226165126 furukawa st ////////////////////////
            //合計金額のみにする
            //totalConts = new Control[] { verifyBoxTotal, verifyBoxSeikyu };
            totalConts = new Control[] { verifyBoxTotal, };
            //20190226165126 furukawa ed ////////////////////////


            // buiNameConts = new Control[] { verifyBoxF1, };


            //20191101170229 furukawa st ////////////////////////
            //初検日年号追加
            buiDateConts = new Control[] { verifyBoxDays, verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD, verifyBoxF1Start, verifyBoxF1Finish, };
            //buiDateConts = new Control[] { verifyBoxDays, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD, verifyBoxF1Start, verifyBoxF1Finish, };
            //20191101170229 furukawa ed ////////////////////////


            //numberingConts = new Control[] { verifyBoxNumbering };

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);


            /*
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            */
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


            /*
            //公費
            var kohi = verifyBoxKohi.GetIntValue();
            if (verifyBoxKohi.Text == "") kohi = 0;//空欄が許可されていなくなっていたので修正
            setStatus(verifyBoxKohi, kohi != 0 && kohi != 1 && kohi != 2);
            */

            //被保険者番号
            int hnumS = verifyBoxHnumS.GetIntValue();
            setStatus(verifyBoxHnumS, hnumS < 1);
            int hnumN = verifyBoxHnumN.GetIntValue();
            setStatus(verifyBoxHnumN, hnumN < 1);
            string hnum = verifyBoxHnumS.Text.Trim() + "-" + verifyBoxHnumN.Text.Trim();

            //支払先コード
            //int payCode = verifyBoxPayCode.GetIntValue();

            //2019/01/29古川削除
            //setStatus(verifyBoxPayCode, payCode < 1 || verifyBoxPayCode.Text.Trim().Length != 5);

            //本家区分
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, family != 2 && family != 6);

            //割合
            int ratio = verifyBoxRatio.GetIntValue();
            setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

            //ナンバリング
           // int n = verifyBoxNumbering.GetIntValue();

            //2019/01/29古川削除
            //setStatus(verifyBoxNumbering, n < 10000000 || 99999999 < n);



            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || total > 200000);

            //2019/01/30古川削除
            //請求
            //int seikyu = verifyBoxSeikyu.GetIntValue();

            //2019/01/29古川削除
            //setStatus(verifyBoxSeikyu, seikyu < 100 || total < seikyu);

            //2019/01/29古川削除
            //負傷
            //fusho1Check(verifyBoxF1);

            //負傷数
            int fushoCount = 0;
           // int.TryParse(verifyBoxFushoCount.Text, out fushoCount);
            
            //2019/01/29古川削除
            //setStatus(verifyBoxFushoCount, fushoCount < 1 || 5 < fushoCount);

            DateTime f1FushoDt = DateTimeEx.DateTimeNull;
            DateTime f2FushoDt = DateTimeEx.DateTimeNull;
            DateTime f3FushoDt = DateTimeEx.DateTimeNull;
            DateTime f1FirstDt = DateTimeEx.DateTimeNull;
            DateTime f2FirstDt = DateTimeEx.DateTimeNull;
            DateTime f3FirstDt = DateTimeEx.DateTimeNull;
            DateTime f1Start = DateTimeEx.DateTimeNull;
            DateTime f2Start = DateTimeEx.DateTimeNull;
            DateTime f3Start = DateTimeEx.DateTimeNull;
            DateTime f1Finish = DateTimeEx.DateTimeNull;
            DateTime f2Finish = DateTimeEx.DateTimeNull;
            DateTime f3Finish = DateTimeEx.DateTimeNull;


            //20191101162509 furukawa st ////////////////////////
            //初検日は年号必須
            
            f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            //f1FirstDt = dateCheck(4, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            //20191101162509 furukawa ed ////////////////////////



            f1Start = DateTimeEx.IsDate(adYear, month, verifyBoxF1Start.GetIntValue()) ?
                new DateTime(adYear, month, verifyBoxF1Start.GetIntValue()) :
                DateTimeEx.DateTimeNull;
            setStatus(verifyBoxF1Start, f1Start.IsNullDate());

            f1Finish = DateTimeEx.IsDate(adYear, month, verifyBoxF1Finish.GetIntValue()) ?
                new DateTime(adYear, month, verifyBoxF1Finish.GetIntValue()) :
                DateTimeEx.DateTimeNull;
            setStatus(verifyBoxF1Finish, f1Finish.IsNullDate());

            //ここまでのチェックで必須エラーが検出されたらfalse
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //2019/01/30古川削除
            /*
            //合計金額：請求金額：本家区分のトリプルチェック
            //金額でのエラーがあればいったん登録中断
            bool ratioError = (int)(total * ratio / 10) != seikyu;
            if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
            if (ratioError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxSeikyu.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }*/



            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            //app.HihoType = kohi;
            app.HihoNum = hnum;
            app.Family = family;
            app.Ratio = ratio;
            app.Sex = sex;
            app.Birthday = birth;
            //app.Numbering = verifyBoxNumbering.Text.Trim();
            app.CountedDays = days;
            app.Total = total;

            //20190130101216 furukawa st ////////////////////////
            //請求金額は不要
            //app.Charge = seikyu;
            //20190130101216 furukawa ed ////////////////////////

            //app.PayCode = verifyBoxPayCode.Text.Trim();

            //申請書種別
            app.AppType = scan.AppType;

            //負傷
           // app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoDate1 = f1FushoDt;
            app.FushoFirstDate1 = f1FirstDt;
            app.FushoStartDate1 = f1Start;
            app.FushoFinishDate1 = f1Finish;
            app.FushoName2 = fushoCount > 1 ? "Exist" : "";
            app.FushoName3 = fushoCount > 2 ? "Exist" : "";
            app.FushoName4 = fushoCount > 3 ? "Exist" : "";
            app.FushoName5 = fushoCount > 4 ? "Exist" : "";

            //チェックOKならデータを格納したインスタンスを返す
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                //ベリファイ入力時
                setVerify(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setInputedApp(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                        //verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    }
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        private void setVerify(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //表示
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                setVerifyVal(verifyBoxY, "**", nv);
            }
            else
            {
                //申請書
                setVerifyVal(verifyBoxY, app.MediYear, nv);

                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                //setVerifyVal(verifyBoxKohi, app.HihoType == 0 ? "" : app.HihoType.ToString(), nv);

                //被保険者情報
                var sm = app.HihoNum.Split('-');
                if (sm.Length == 2)
                {
                    setVerifyVal(verifyBoxHnumS, sm[0], nv);
                    setVerifyVal(verifyBoxHnumN, sm[1], nv);
                }
                else
                {
                    setVerifyVal(verifyBoxHnumS, string.Empty, nv);
                    setVerifyVal(verifyBoxHnumN, string.Empty, nv);
                }
                setVerifyVal(verifyBoxFamily, app.Family.ToString(), nv);
                setVerifyVal(verifyBoxRatio, app.Ratio, nv);

                //支払先コード
                //setVerifyVal(verifyBoxPayCode, app.PayCode, nv);

                //個人欄
                setVerifyVal(verifyBoxSex, app.Sex, nv);
                setVerifyVal(verifyBoxBirthE, DateTimeEx.GetEraNumber(app.Birthday), nv);
                setVerifyVal(verifyBoxBirthY, DateTimeEx.GetJpYear(app.Birthday), nv);
                setVerifyVal(verifyBoxBirthM, app.Birthday.Month, nv);
                setVerifyVal(verifyBoxBirthD, app.Birthday.Day, nv);

                //申請情報
                //setVerifyVal(verifyBoxNumbering, app.Numbering, nv);
                setVerifyVal(verifyBoxDays, app.CountedDays, nv);
                setVerifyVal(verifyBoxTotal, app.Total, nv);

                //2019/01/30古川削除
                //setVerifyVal(verifyBoxSeikyu, app.Charge, nv);

                //負傷
                //setVerifyVal(verifyBoxF1, app.FushoName1, nv);
                if (!app.FushoFirstDate1.IsNullDate())
                {                    
                    //20191101163459 furukawa st ////////////////////////
                    //初検日は平成をまたぐ可能性があるので年号必須

                    int wareki = 0;
                    wareki = DateTimeEx.GetEraNumberYear(app.FushoFirstDate1);
                    int we, wy;
                    we = int.Parse(wareki.ToString().Substring(0, 1));
                    wy = int.Parse(wareki.ToString().Substring(1, 2));
                    setValue(verifyBoxF1FirstE, we, firstTime, nv);
                    setValue(verifyBoxF1FirstY, wy, firstTime, nv);

                
                    //setVerifyVal(verifyBoxF1FirstY, DateTimeEx.GetJpYear(app.FushoFirstDate1), nv);
                    //20191101163459 furukawa ed ////////////////////////

                    setVerifyVal(verifyBoxF1FirstM, app.FushoFirstDate1.Month, nv);
                    setVerifyVal(verifyBoxF1FirstD, app.FushoFirstDate1.Day, nv);
                }
                else
                {
                    //20191101163618 furukawa st ////////////////////////
                    //初検日年号
                    
                    setVerifyVal(verifyBoxF1FirstE, string.Empty, nv);
                    //20191101163618 furukawa ed ////////////////////////


                    setVerifyVal(verifyBoxF1FirstY, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FirstM, string.Empty, nv);
                    setVerifyVal(verifyBoxF1FirstD, string.Empty, nv);
                }

                if (!app.FushoStartDate1.IsNullDate())
                    setVerifyVal(verifyBoxF1Start, app.FushoStartDate1.Day, nv);
                else
                    setVerifyVal(verifyBoxF1Start, string.Empty, nv);

                if (!app.FushoFinishDate1.IsNullDate())
                    setVerifyVal(verifyBoxF1Finish, app.FushoFinishDate1.Day, nv);
                else
                    setVerifyVal(verifyBoxF1Finish, string.Empty, nv);

                /*
                //負傷数 ベリファイ不要
                var FushoCount = string.IsNullOrEmpty(app.FushoName1) ? 0 :
                    string.IsNullOrEmpty(app.FushoName2) ? 1 :
                    string.IsNullOrEmpty(app.FushoName3) ? 2 :
                    string.IsNullOrEmpty(app.FushoName4) ? 3 :
                    string.IsNullOrEmpty(app.FushoName5) ? 4 : 5;
                setVerifyVal(verifyBoxFushoCount, FushoCount, false);
                */
            }
            missCounterReset();
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else if(app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                //verifyBoxKohi.Text = app.HihoType == 0 ? "" : app.HihoType.ToString();

                //被保険者情報
                var sm = app.HihoNum.Split('-');
                if (sm.Length == 2)
                {
                    verifyBoxHnumS.Text = sm[0];
                    verifyBoxHnumN.Text = sm[1];
                }
                verifyBoxFamily.Text= app.Family.ToString();
                verifyBoxRatio.Text = app.Ratio.ToString();

                //支払先コード
               // verifyBoxPayCode.Text = app.PayCode;

                //個人欄
                verifyBoxSex.Text = app.Sex.ToString();
                verifyBoxBirthE.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                verifyBoxBirthY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                verifyBoxBirthM.Text = app.Birthday.Month.ToString();
                verifyBoxBirthD.Text = app.Birthday.Day.ToString();

                //申請情報
               // verifyBoxNumbering.Text = app.Numbering;
                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxTotal.Text = app.Total.ToString();
                //verifyBoxSeikyu.Text = app.Charge.ToString();

                //負傷
                //verifyBoxF1.Text = app.FushoName1;
                if (!app.FushoFirstDate1.IsNullDate())
                {
                    //20191101165253 furukawa st ////////////////////////
                    //初検日年号
                    
                    int wareki = 0;
                    wareki = DateTimeEx.GetEraNumberYear(app.FushoFirstDate1);
                    int we, wy;
                    we = int.Parse(wareki.ToString().Substring(0, 1));
                    wy = int.Parse(wareki.ToString().Substring(1, 2));

                    verifyBoxF1FirstE.Text = we.ToString();
                    verifyBoxF1FirstY.Text = wy.ToString();
                    
                    //verifyBoxF1FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString();
                    //20191101165253 furukawa ed ////////////////////////


                    verifyBoxF1FirstM.Text = app.FushoFirstDate1.Month.ToString();
                    verifyBoxF1FirstD.Text = app.FushoFirstDate1.Day.ToString();
                }
                if(!app.FushoStartDate1.IsNullDate())
                    verifyBoxF1Start.Text = app.FushoStartDate1.Day.ToString();
                if (!app.FushoFinishDate1.IsNullDate())
                    verifyBoxF1Finish.Text = app.FushoFinishDate1.Day.ToString();


                /*
                //負傷数
                var FushoCount = string.IsNullOrEmpty(app.FushoName1) ? 0 :
                    string.IsNullOrEmpty(app.FushoName2) ? 1 :
                    string.IsNullOrEmpty(app.FushoName3) ? 2 :
                    string.IsNullOrEmpty(app.FushoName4) ? 3 :
                    string.IsNullOrEmpty(app.FushoName5) ? 4 : 5;
                verifyBoxFushoCount.Text = FushoCount.ToString();
                */
            }
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            Action<Control, bool> act = null;
            act = new Action<Control, bool>((c, b) =>
            {
                foreach (Control item in c.Controls) act(item, b);
                if ((c is IVerifiable == false) && (c is Label == false)) return;
                if (ignoreControls.Contains(c)) return;
                c.Visible = b;
                if (c is IVerifiable == false) return;
                c.BackColor = c.Enabled ? SystemColors.Info : SystemColors.Menu;
            });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                act(panelRight, false);
            }
            else
            {
                //申請書の場合
                act(panelRight, true);
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;
            //else if (totalConts.Contains(sender)) p = posTotal;
            //else if (buiNameConts.Contains(sender)) p = posBuiName;
            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            //else if (numberingConts.Contains(sender)) p = posNumbering;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        //フォーム表示時
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;

            //20190226170340 furukawa st ////////////////////////
            //使用部分のみとする
            //else if (buiNameConts.Any(c => c.Focused)) posBuiName = pos;
            //20190226170340 furukawa ed ////////////////////////

            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;

            //20190226170403 furukawa st ////////////////////////
            //使用部分のみとする
            //else if (numberingConts.Any(c => c.Focused)) posNumbering = pos;
            //20190226170403 furukawa ed ////////////////////////

        }
    }      
}
