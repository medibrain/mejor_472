﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    class Fusho
    {
        static List<ChangeWords> changeWords = new List<ChangeWords>();

        public class ChangeWords
        {
            public readonly string WrongWord;
            public readonly string RightWord;


            public ChangeWords(string w, string r)
            {
                this.WrongWord = w;
                this.RightWord = r;
                changeWords.Add(this);
            }
        }

        /// <summary>
        /// 誤認文字の置換
        /// </summary>
        static Fusho()
        {
            new ChangeWords("黷黼挫", "膝関節捻挫");
            new ChangeWords("黷鸚挫", "膝関節捻挫");
            new ChangeWords("上牆脯", "上腕部挫傷");
            new ChangeWords("鵬捻挫", "関節捻挫");
            new ChangeWords("鼎捻挫", "頚部捻挫");
            new ChangeWords("頸鼬挫", "頚部捻挫");
            new ChangeWords("頚鼬挫", "頚部捻挫");
            new ChangeWords("頸膸挫", "頚部捻挫");
            new ChangeWords("頸部髓", "頚部捻挫");
            new ChangeWords("頚膸挫", "頚部捻挫");
            new ChangeWords("頚髄挫", "頚部捻挫");
            new ChangeWords("頚贈挫", "頚部捻挫");
            new ChangeWords("頚部髓", "頚部捻挫");
            new ChangeWords("頚部鼬", "頚部捻挫");
            new ChangeWords("腰髄挫", "腰部捻挫");
            new ChangeWords("腰魄挫", "腰部捻挫");
            new ChangeWords("鼬聽挫", "腰部捻挫");
            new ChangeWords("腰鼬挫", "腰部捻挫");
            new ChangeWords("腰膸挫", "腰部捻挫");
            new ChangeWords("腰繖挫", "腰部捻挫");
            new ChangeWords("腰贈挫", "腰部捻挫");

            new ChangeWords("足難", "足関節");
            new ChangeWords("足贈", "足関節");
            new ChangeWords("手難", "手関節");
            new ChangeWords("手髏", "足関節");
            new ChangeWords("足髏", "足関節");
            new ChangeWords("股髏", "股関節");
            new ChangeWords("肘髏", "肘関節");
            new ChangeWords("股髏", "股関節");
            new ChangeWords("嬾節", "膝関節");

            new ChangeWords("手贈", "手関節");
            new ChangeWords("足贈", "足関節");
            new ChangeWords("股贈", "股関節");
            new ChangeWords("肘贈", "肘関節");
            new ChangeWords("股贈", "股関節");

            new ChangeWords("腰部髓", "腰部捻挫");
            new ChangeWords("腰部鼬", "腰部捻挫");
            new ChangeWords("腰鵬挫", "腰部捻挫");
            new ChangeWords("腰繼挫", "腰部捻挫");
            new ChangeWords("腰饑挫", "腰部捻挫");
            new ChangeWords("腰髓挫", "腰部捻挫");
            new ChangeWords("腰鰌挫", "腰部捻挫");
            new ChangeWords("腰鸚挫", "腰部捻挫");
            new ChangeWords("腰鄒捻挫", "腰部捻挫");
            new ChangeWords("鼎髓", "頚部捻挫");
            new ChangeWords("鼎鼬", "頚部捻挫");
            new ChangeWords("嬾鸚挫", "膝関節捻挫");
            new ChangeWords("黷黜挫", "膝関節捻挫");
            new ChangeWords("黷黯挫", "膝関節捻挫");
            new ChangeWords("黷鼬挫", "膝関節捻挫");
            new ChangeWords("鵬捻挫", "関節捻挫");
            new ChangeWords("鵬捻挫", "関節捻挫");
            new ChangeWords("鵬捻挫", "関節捻挫");
            new ChangeWords("鵬捻挫", "関節捻挫");
            new ChangeWords("圜1聴挫", "関節捻挫");
            new ChangeWords("圜i隱挫", "関節捻挫");

            new ChangeWords("贈髓", "関節捻挫");
            new ChangeWords("黼挫", "節捻挫");
            new ChangeWords("肩贈", "肩関節");
            new ChangeWords("屑関節", "肩関節");

            new ChangeWords("上鯵", "上腕部");

            new ChangeWords("挫饂", "挫傷");
            new ChangeWords("挫廛", "挫傷");
            new ChangeWords("挂傷", "挫傷");
            new ChangeWords("挫廰", "挫傷");
            new ChangeWords("挫廛", "挫傷");
            new ChangeWords("哇傷", "挫傷");
            new ChangeWords("賤傷", "挫傷");
            new ChangeWords("桎傷", "挫傷");
            new ChangeWords("挫嗇", "挫傷");
            new ChangeWords("挫偬", "挫傷");
            new ChangeWords("挫傴", "挫傷");
            new ChangeWords("挫膓", "挫傷");
            new ChangeWords("挫糜", "挫傷");
            new ChangeWords("挫縻", "挫傷");
            new ChangeWords("挫隴", "挫傷");
            new ChangeWords("挫麕", "挫傷");
            new ChangeWords("挫麾", "挫傷");
            new ChangeWords("挫慯", "挫傷");
            new ChangeWords("鷦", "挫傷");

            new ChangeWords("辯頂", "部打撲");
            new ChangeWords("打樸", "打撲");
            new ChangeWords("抃撲", "打撲");
            new ChangeWords("打揆", "打撲");
            new ChangeWords("打挨", "打撲");
            new ChangeWords("打俟", "打撲");
            new ChangeWords("打擯", "打撲");

            new ChangeWords("捻挂", "捻挫");
            new ChangeWords("捻捏", "捻挫");
            new ChangeWords("搶挫", "捻挫");
            new ChangeWords("捻咎", "捻挫");
            new ChangeWords("聰挫", "捻挫");
            new ChangeWords("聡挫", "捻挫");
            new ChangeWords("聽挫", "捻挫");
            new ChangeWords("謎", "捻挫");

            new ChangeWords("邵", "部");
            new ChangeWords("卻", "部");

            new ChangeWords("願部", "頚部");
            new ChangeWords("頤部", "頚部");
            new ChangeWords("爾部", "頚部");
            new ChangeWords("順部", "頚部");
            new ChangeWords("項部", "頚部");
            new ChangeWords("潁部", "頚部");
            new ChangeWords("瀕部", "頚部");
            new ChangeWords("濔部", "頚部");
            new ChangeWords("頽部", "頚部");
            new ChangeWords("嶺部", "頚部");

            new ChangeWords("膸部", "腿部");

            new ChangeWords("冂要部", "腰部");
            new ChangeWords("瞿部", "腰部");
            new ChangeWords("嵳部", "腰部");
            new ChangeWords("坪部", "腰部");
            new ChangeWords("瞿部", "腰部");
            new ChangeWords("洫部", "腰部");
            new ChangeWords("踝部", "腰部");
            new ChangeWords("要部", "腰部");
            new ChangeWords("踝部", "腰部");
            new ChangeWords("櫻部", "腰部");
            new ChangeWords("嬖部", "腰部");
            new ChangeWords("鰥部", "腰部");
            new ChangeWords("顰部", "腰部");

            new ChangeWords("膤", "腰部");
            new ChangeWords("朧", "腰部");
            new ChangeWords("雖", "頚部");

            new ChangeWords("毆部", "殿部");
            new ChangeWords("毆部", "殿部");

            new ChangeWords("膺部", "背部");
            new ChangeWords("脣部", "背部");
            new ChangeWords("膚部", "背部");
            new ChangeWords("清部", "背部");

            new ChangeWords("詢部", "胸部");

            new ChangeWords("圜市", "関節");


            new ChangeWords("C上", "(上");
            new ChangeWords("C下", "(下");
            new ChangeWords("C巾", "(中");
        }

        const string headChars = "〉'.Jj:(0123456789)二幻";
        const string footChars = "LRUD-0123456789";

        /// <summary>
        /// 負傷名の取得
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetFushoName(string s)
        {
            var fr = Change(s);

            //20200205105532 furukawa st ////////////////////////
            //OCRエラー時の＃が間にあるときに、入力画面のカーソル順が変動するのをやめたい
            
            if (fr == string.Empty && s.Length > 3) return string.Empty;
            if (!BasicWordsCheck(fr)) return string.Empty;

            //if (fr == string.Empty && s.Length > 3) return "#";
            //if (!BasicWordsCheck(fr)) return "#";

            //20200205105532 furukawa ed ////////////////////////

            return fr;
        }

        public static string GetFusho1(string[] aall)
        {
            if (aall.Length < 108) return string.Empty;
            return GetFushoName(aall[40]);
        }

        public static string GetFusho2(string[] aall)
        {
            if (aall.Length < 108) return string.Empty;
            return GetFushoName(aall[57]);
        }

        public static string GetFusho3(string[] aall)
        {
            if (aall.Length < 108) return string.Empty;
            return GetFushoName(aall[74]);
        }

        public static string GetFusho4(string[] aall)
        {
            if (aall.Length < 108) return string.Empty;
            return GetFushoName(aall[91]);
        }
        
        public static string GetFusho5(string[] aall)
        {
            if (aall.Length < 108) return string.Empty;
            return GetFushoName(aall[108]);
        }

        public static string Change(string bui)
        {
            //頭に付きがちな、不要文字列を排除
            while (bui.Length > 2 && headChars.Contains(bui[0]))
            {
                bui = bui.Substring(1);
            }

            //けつに付きがちな…
            while (bui.Length > 2 && footChars.Contains(bui[bui.Length - 1]))
            {
                bui = bui.Remove(bui.Length - 1);
            }

            //よく出る認識ミス単語を変換
            foreach (var item in changeWords)
            {
                if(bui.Contains(item.WrongWord))
                    bui = bui.Replace(item.WrongWord, item.RightWord);
            }

            //頭の文字でよくある間違いを訂正
            if (bui.Length > 2 && bui[0] == '石') bui = '右' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '有') bui = '右' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '冶') bui = '右' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '布') bui = '右' + bui.Substring(1);
            //if (bui.Length > 2 && bui[0] == '声' && bui[1] != '部') bui = '右' + bui.Substring(1);
            //if (bui.Length > 2 && bui[0] == '芦' && bui[1] != '部') bui = '右' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '霖') bui = '左' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '友') bui = '左' + bui.Substring(1);
            if (bui.Length > 2 && bui[0] == '衣') bui = '左' + bui.Substring(1);

            //一部の字を統一
            bui = bui.Replace((char)26863, '捻');
            bui = bui.Replace('頸', '頚');
            bui = bui.Replace('（', '(');
            bui = bui.Replace('）', ')');
            bui = bui.Replace('〉', ')');

            bui = bui.Trim();
            if (bui.Length < 3)
                return string.Empty;

            int i = bui.IndexOf("捻挫");
            if (i == -1) i = bui.IndexOf("挫傷");
            if (i == -1) i = bui.IndexOf("打撲");
            if (i == -1) return bui;

            int i2 = bui.IndexOf(')', i);
            if (i2 != -1)
            {
                if (bui.Length - 1 <= i2) return bui;
                return bui.Remove(i2 + 1);
            }

            var jyo = bui.IndexOf('上', i);
            if (jyo != -1 && i + 1 < jyo)
            {
                return bui.Remove(i + 2) + "(上部)";
            }

            var ka = bui.IndexOf('下', i);
            if (ka != -1 && i + 1 < ka)
            {
                return bui.Remove(i + 2) + "(下部)";
            }

            var ch = bui.IndexOf("中央", i);
            if (ch != -1 && i + 1 < ch)
            {
                return bui.Remove(i + 3) + "(中央部)";
            }

            if (bui.Length <= i + 2) return bui;
            return bui.Remove(i + 2);
        }

        /// <summary>
        /// 一般的な部位名だけで構成されているか
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static bool BasicWordsCheck(string bs)
        {
            string b;
            return BasicWordsCheck(bs, out b);
        }

        /// <summary>
        /// 一般的な部位名だけで構成されているか
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static bool BasicWordsCheck(string bs, out string rest)
        {
            bs = bs.Replace("右", "");
            bs = bs.Replace("左", "");
            bs = bs.Replace("両", "");
            bs = bs.Replace("第一", "");
            bs = bs.Replace("第二", "");
            bs = bs.Replace("第三", "");
            bs = bs.Replace("第四", "");
            bs = bs.Replace("第五", "");
            bs = bs.Replace("第１", "");
            bs = bs.Replace("第２", "");
            bs = bs.Replace("第３", "");
            bs = bs.Replace("第４", "");
            bs = bs.Replace("第５", "");
            bs = bs.Replace("第1", "");
            bs = bs.Replace("第2", "");
            bs = bs.Replace("第3", "");
            bs = bs.Replace("第4", "");
            bs = bs.Replace("第5", "");
            bs = bs.Replace("腰殿部", "");
            bs = bs.Replace("足根部", "");
            bs = bs.Replace("上腕部", "");
            bs = bs.Replace("中手部", "");
            bs = bs.Replace("中足部", "");
            bs = bs.Replace("下腿部", ""); 
            bs = bs.Replace("前腕部", "");
            bs = bs.Replace("大腿部", "");
            bs = bs.Replace("手根部", "");
            bs = bs.Replace("足趾部", "");
            bs = bs.Replace("足底部", "");
            bs = bs.Replace("顔面部", "");
            bs = bs.Replace("頚部", "");
            bs = bs.Replace("胸部", "");
            bs = bs.Replace("腰部", "");
            bs = bs.Replace("殿部", "");
            bs = bs.Replace("頸部", "");
            bs = bs.Replace("肘部", "");
            bs = bs.Replace("膝部", "");
            bs = bs.Replace("肩部", "");
            bs = bs.Replace("背部", "");
            bs = bs.Replace("殿部", "");
            bs = bs.Replace("臀部", "");
            bs = bs.Replace("趾部", "");
            bs = bs.Replace("頭部", "");
            bs = bs.Replace("指部", "");
            bs = bs.Replace("手部", "");
            bs = bs.Replace("足部", "");
            //bs = bs.Replace("顔部", "");
            bs = bs.Replace("腰椎", "");
            bs = bs.Replace("頸椎", "");
            bs = bs.Replace("頚椎", "");
            bs = bs.Replace("指指節間関節", "");
            bs = bs.Replace("中足趾節関節", "");
            bs = bs.Replace("中足趾関節", "");
            bs = bs.Replace("指節関節", "");
            bs = bs.Replace("拇指関節", "");
            bs = bs.Replace("拇趾関節", "");
            bs = bs.Replace("足根関節", "");
            bs = bs.Replace("肩関節", "");
            bs = bs.Replace("手関節", "");
            bs = bs.Replace("肘関節", "");
            bs = bs.Replace("股関節", "");
            bs = bs.Replace("膝関節", "");
            bs = bs.Replace("足関節", "");
            bs = bs.Replace("趾関節", "");
            bs = bs.Replace("指関節", "");
            bs = bs.Replace("顎関節", "");
            bs = bs.Replace("中手", "");
            bs = bs.Replace("捻挫", "");
            bs = bs.Replace("挫傷", "");
            bs = bs.Replace("打撲", "");
            bs = bs.Replace("(下部)", "");
            bs = bs.Replace("(上部)", "");
            bs = bs.Replace("(中央部)", "");
            bs = bs.Replace("下部", "");
            bs = bs.Replace("上部", "");
            bs = bs.Replace("(外側)", "");
            bs = bs.Replace("下腿", "");
            bs = bs.Replace("大腿", "");
            bs = bs.Replace("上腕", "");
            bs = bs.Replace("手", "");
            bs = bs.Replace("足", "");

            rest = bs;
            return bs == string.Empty;
        }

        public static void BasicWordsCheckTest(string fileName)
        {
            var sn = System.IO.Path.GetFileNameWithoutExtension(fileName);
            var dir = System.IO.Path.GetDirectoryName(fileName);
            var rsn = dir + "\\" + sn + "-rest.csv";
            sn = dir + "\\" + sn + "-changed.csv";

            List<string> l = new List<string>();
            List<string> rl = new List<string>();

            using(var sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
            {
                while (sr.Peek() > 0)
                {
                    var ss = sr.ReadLine().Split(',');
                    if (ss.Length < 1) continue;

                    var bui = ss[0];
                    var input = ss[1];
                    var ocr = Change(ss[0]);

                    if (string.IsNullOrWhiteSpace(ocr)) continue;
                    if (string.IsNullOrWhiteSpace(ss[2])) continue;

                    //一般名外リスト
                    string rest;
                    if (!BasicWordsCheck(input, out rest))
                    {
                        rl.Add(rest + "," + bui + "," + ocr + "," + input);
                    }

                    input = Change(input);
                    if (input == ocr) continue;

                    //頭に付きがちな、不要文字列を排除
                    while (bui.Length > 2 && headChars.Contains(bui[0]))
                    {
                        bui = bui.Substring(1);
                    }

                    l.Add(bui + "," + ocr + "," + input + "," + ss[2]);


                }
            }
            l.Sort((x, y) => x.CompareTo(y));

            using (var sw = new System.IO.StreamWriter(sn, false, Encoding.GetEncoding("Shift_JIS")))
            {
                foreach (var item in l)
                {
                    sw.WriteLine(item);
                }
            }

            using (var sw = new System.IO.StreamWriter(rsn, false, Encoding.GetEncoding("Shift_JIS")))
            {
                foreach (var item in rl)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public static string ToRegularName(string str)
        {
            str = str.Replace("（", "(");
            str = str.Replace("）", ")");
            str = str.Replace("頚", "頸");
            return str;
        }

        /// <summary>
        /// 鍼灸時入力に使用している、1～7の文字から鍼灸病名を取得します
        /// </summary>
        /// <param name="fushoName"></param>
        /// <returns></returns>
        public static string GetShinkyuFushoName(string fushoName)
        {
            int hid;
            int.TryParse(fushoName, out hid);
            return (0 < hid && hid < 8) ? ((HARI_BYOMEI)hid).ToString() : fushoName;
        }
    }
}
