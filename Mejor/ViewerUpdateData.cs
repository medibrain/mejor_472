﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    class ViewerUpdateData
    {
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; set; }

        public string ShokaiID { get; set; } = string.Empty;
        public ViewerEnums.ShokaiReason ShokaiReasons { get; set; } = ViewerEnums.ShokaiReason.なし;
        public ViewerEnums.ShokaiResult ShokaiResult { get; set; } = ViewerEnums.ShokaiResult.なし;
        public ViewerEnums.KagoReason KagoReasons { get; set; } = ViewerEnums.KagoReason.なし;
        public ViewerEnums.SaishinsaReason SaishinsaReasons { get; set; } = ViewerEnums.SaishinsaReason.なし;
        public bool HenreiFlag { get; set; } = false;
        public ViewerEnums.HenreiReason HenreiReasons { get; set; } = ViewerEnums.HenreiReason.なし;
        public string Note { get; set; } = string.Empty;
        public string ShokaiFile { get; set; } = string.Empty;

        public static string Header =>
            "AID,ShokaiID,ShokaiReasons,ShokaiResult,KagoReasons," +
            "SaishinsaReasons,HenreiFlag,HenreiReasons,Note,ShokaiFile";

        public ViewerUpdateData(App app, string imageFileName)
        {
            AID = app.Aid;
            ShokaiID = app.ShokaiCode;
            if (app.ShokaiReasonCheck(ShokaiReason.部位数)) ShokaiReasons |= ViewerEnums.ShokaiReason.部位数;
            if (app.ShokaiReasonCheck(ShokaiReason.施術日数)) ShokaiReasons |= ViewerEnums.ShokaiReason.施術日数;
            if (app.ShokaiReasonCheck(ShokaiReason.施術期間)) ShokaiReasons |= ViewerEnums.ShokaiReason.施術期間;
            if (app.ShokaiReasonCheck(ShokaiReason.疑義施術所)) ShokaiReasons |= ViewerEnums.ShokaiReason.疑義施術所;
            if (app.ShokaiReasonCheck(ShokaiReason.合計金額)) ShokaiReasons |= ViewerEnums.ShokaiReason.合計金額;
            if (app.ShokaiReasonCheck(ShokaiReason.その他)) ShokaiReasons |= ViewerEnums.ShokaiReason.その他;
            if (app.ShokaiReasonCheck(ShokaiReason.宛所なし)) ShokaiResult = ViewerEnums.ShokaiResult.宛所なし;
            if (app.ShokaiReasonCheck(ShokaiReason.相違なし)) ShokaiResult = ViewerEnums.ShokaiResult.相違なし;
            if (app.ShokaiReasonCheck(ShokaiReason.相違あり)) ShokaiResult = ViewerEnums.ShokaiResult.相違あり;


            
            //20201011151057 furukawa st ////////////////////////
            //過誤フラグ追加に伴ってxml列に変更
            
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.署名違い)) KagoReasons |= ViewerEnums.KagoReason.署名違い;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.筆跡違い)) KagoReasons |= ViewerEnums.KagoReason.筆跡違い;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.家族同一筆跡)) KagoReasons |= ViewerEnums.KagoReason.家族同一筆跡;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.原因なし)) KagoReasons |= ViewerEnums.KagoReason.原因なし;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.長期理由なし)) KagoReasons |= ViewerEnums.KagoReason.長期理由なし;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.負傷原因相違)) KagoReasons |= ViewerEnums.KagoReason.負傷原因相違;
            if (app.KagoReasonsCheck_xml(Mejor.KagoReasons_Member.その他)) KagoReasons |= ViewerEnums.KagoReason.その他;


            //if (app.KagoReasonsCheck(Mejor.KagoReasons.署名違い)) KagoReasons |= ViewerEnums.KagoReason.署名違い;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.筆跡違い)) KagoReasons |= ViewerEnums.KagoReason.筆跡違い;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.家族同一筆跡)) KagoReasons |= ViewerEnums.KagoReason.家族同一筆跡;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.原因なし)) KagoReasons |= ViewerEnums.KagoReason.原因なし;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.長期理由なし)) KagoReasons |= ViewerEnums.KagoReason.長期理由なし;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.負傷原因相違)) KagoReasons |= ViewerEnums.KagoReason.負傷原因相違;
            //if (app.KagoReasonsCheck(Mejor.KagoReasons.その他)) KagoReasons |= ViewerEnums.KagoReason.その他;
            //20201011151057 furukawa ed ////////////////////////



            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.初検料)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.初検料;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.その他)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.その他;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.負傷1)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.負傷1;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.負傷2)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.負傷2;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.負傷3)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.負傷3;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.負傷4)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.負傷4;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.負傷5)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.負傷5;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.転帰なし)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.転帰なし;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.中止)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.中止;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.同一負傷名)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.同一負傷名;
            if (app.SaishinsaReasonsCheck(Mejor.SaishinsaReasons.別負傷名)) SaishinsaReasons |= ViewerEnums.SaishinsaReason.別負傷名;
            HenreiFlag = app.StatusFlagCheck(StatusFlag.返戻);
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.負傷部位)) HenreiReasons |= ViewerEnums.HenreiReason.負傷部位;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.負傷原因)) HenreiReasons |= ViewerEnums.HenreiReason.負傷原因;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.負傷時期)) HenreiReasons |= ViewerEnums.HenreiReason.負傷時期;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.けが外)) HenreiReasons |= ViewerEnums.HenreiReason.けが外;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.受療日数)) HenreiReasons |= ViewerEnums.HenreiReason.受療日数;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.勤務中)) HenreiReasons |= ViewerEnums.HenreiReason.勤務中;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.過誤)) HenreiReasons |= ViewerEnums.HenreiReason.過誤;
            if (app.HenreiReasonsCheck(Mejor.HenreiReasons.その他)) HenreiReasons |= ViewerEnums.HenreiReason.その他;
            Note = app.OutMemo;
            ShokaiFile = imageFileName;
        }

        public string CreateCsvLine()
        {
            var s = new string[10];
            int i = 0;
            //AID,ShokaiID,ShokaiReasons,ShokaiResult,KagoReasons,
            s[i++] = AID.ToString();
            s[i++] = ShokaiID;
            s[i++] = ((int)ShokaiReasons).ToString();
            s[i++] = ((int)ShokaiResult).ToString();
            s[i++] = ((int)KagoReasons).ToString();
            //SaishinsaReasons,HenreiFlag,HenreiReasons,Note,ShokaiFile
            s[i++] = ((int)SaishinsaReasons).ToString();
            s[i++] = HenreiFlag ? "True" : "False";
            s[i++] = ((int)HenreiReasons).ToString();
            s[i++] = Note;
            s[i++] = ShokaiFile;

            for (int c = 0; c < s.Length; c++)
                s[c] = $"\"{s[c].Replace("\"", "\"\"")}\"";

            return string.Join(",", s);
        }


        public static bool Export(int fromCym, int toCym, string dirName, WaitForm wf)
        {
            var fileName = dirName + "\\Update.csv";
            var imgDir = dirName + "\\ShokaiImg";
            System.IO.Directory.CreateDirectory(imgDir);

            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Marquee;
            wf.LogPrint("更新対象情報を取得しています");
            var apps = App.GetAppsWithWhere($"WHERE " +
                $"a.cym BETWEEN {fromCym} AND {toCym} " +
                $"AND (a.statusflags=(a.statusflags|{(int)StatusFlag.照会対象}) " +
                $"OR a.statusflags=(a.statusflags|{(int)StatusFlag.返戻}))");

            if (apps.Count == 0)
            {
                wf.LogPrint("更新対象となるデータはありませんでした");
                return true;
            }

            wf.LogPrint($"{apps.Count}件の更新対象を抽出しました");
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
            wf.InvokeValue = 0;
            wf.SetMax(apps.Count());
            wf.LogPrint("更新データを作成しています");

            var images = Shokai.ShokaiImage.SelectByCym(fromCym, toCym);
            var dic = new Dictionary<int, Shokai.ShokaiImage>();
            images.ForEach(img => dic.Add(img.AID, img));

            try
            {
                var fc = new TiffUtility.FastCopy();
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerUpdateData.Header);
                    foreach (var item in apps)
                    {
                        var imgFileName = dic.ContainsKey(item.Aid) ? $"{item.Aid}_s.tif" : string.Empty;
                        var v = new ViewerUpdateData(item, imgFileName);
                        sw.WriteLine(v.CreateCsvLine());

                        if (dic.ContainsKey(item.Aid))
                        {
                            //画像ファイルコピー
                            fc.FileCopy(dic[item.Aid].GetFullPath(), $"{imgDir}\\{imgFileName}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
    }
}
