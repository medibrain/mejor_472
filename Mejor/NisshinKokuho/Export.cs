﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mejor.NisshinKokuho
{
    class Export
    {
        /// <summary>
        /// 新ビューア形式出力
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool ExportViewerData(int cym)
        {
            string infoFileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                infoFileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(infoFileName);
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //ビューアデータ
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }
    }
}
