﻿namespace Mejor.KagoshimaKoiki
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPunch = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnFile2 = new System.Windows.Forms.Button();
            this.btnFile1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxKyufu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImp = new System.Windows.Forms.Button();
            this.btnFileSikyuData = new System.Windows.Forms.Button();
            this.btnFileHihoMaster = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHihoMaster = new System.Windows.Forms.TextBox();
            this.textBoxSikyu = new System.Windows.Forms.TextBox();
            this.textBoxClinicMaster = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFileClinicMaster = new System.Windows.Forms.Button();
            this.gbA = new System.Windows.Forms.GroupBox();
            this.gbJ.SuspendLayout();
            this.gbA.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxPunch
            // 
            this.textBoxPunch.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPunch.Location = new System.Drawing.Point(180, 29);
            this.textBoxPunch.Multiline = true;
            this.textBoxPunch.Name = "textBoxPunch";
            this.textBoxPunch.Size = new System.Drawing.Size(512, 44);
            this.textBoxPunch.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.Controls.Add(this.btnFile2);
            this.gbJ.Controls.Add(this.btnFile1);
            this.gbJ.Controls.Add(this.label2);
            this.gbJ.Controls.Add(this.textBoxKyufu);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxPunch);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(43, 12);
            this.gbJ.Name = "gbJ";
            this.gbJ.Size = new System.Drawing.Size(786, 81);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            // 
            // btnFile2
            // 
            this.btnFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile2.Location = new System.Drawing.Point(698, 81);
            this.btnFile2.Name = "btnFile2";
            this.btnFile2.Size = new System.Drawing.Size(48, 15);
            this.btnFile2.TabIndex = 3;
            this.btnFile2.Text = "...";
            this.btnFile2.UseVisualStyleBackColor = true;
            this.btnFile2.Visible = false;
            // 
            // btnFile1
            // 
            this.btnFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile1.Location = new System.Drawing.Point(698, 30);
            this.btnFile1.Name = "btnFile1";
            this.btnFile1.Size = new System.Drawing.Size(48, 31);
            this.btnFile1.TabIndex = 3;
            this.btnFile1.Text = "...";
            this.btnFile1.UseVisualStyleBackColor = true;
            this.btnFile1.Click += new System.EventHandler(this.btnFile1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "給付記録データ";
            this.label2.Visible = false;
            // 
            // textBoxKyufu
            // 
            this.textBoxKyufu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKyufu.Location = new System.Drawing.Point(180, 81);
            this.textBoxKyufu.Multiline = true;
            this.textBoxKyufu.Name = "textBoxKyufu";
            this.textBoxKyufu.Size = new System.Drawing.Size(512, 28);
            this.textBoxKyufu.TabIndex = 0;
            this.textBoxKyufu.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "申請書\r\nパンチデータ(CSV)";
            // 
            // btnImp
            // 
            this.btnImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImp.Location = new System.Drawing.Point(741, 521);
            this.btnImp.Name = "btnImp";
            this.btnImp.Size = new System.Drawing.Size(115, 34);
            this.btnImp.TabIndex = 3;
            this.btnImp.Text = "取込";
            this.btnImp.UseVisualStyleBackColor = true;
            this.btnImp.Click += new System.EventHandler(this.btnImp_Click);
            // 
            // btnFileSikyuData
            // 
            this.btnFileSikyuData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileSikyuData.Location = new System.Drawing.Point(698, 161);
            this.btnFileSikyuData.Name = "btnFileSikyuData";
            this.btnFileSikyuData.Size = new System.Drawing.Size(48, 31);
            this.btnFileSikyuData.TabIndex = 3;
            this.btnFileSikyuData.Text = "...";
            this.btnFileSikyuData.UseVisualStyleBackColor = true;
            this.btnFileSikyuData.Visible = false;
            this.btnFileSikyuData.Click += new System.EventHandler(this.btnFileSikyuData_Click);
            // 
            // btnFileHihoMaster
            // 
            this.btnFileHihoMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileHihoMaster.Location = new System.Drawing.Point(698, 91);
            this.btnFileHihoMaster.Name = "btnFileHihoMaster";
            this.btnFileHihoMaster.Size = new System.Drawing.Size(48, 31);
            this.btnFileHihoMaster.TabIndex = 3;
            this.btnFileHihoMaster.Text = "...";
            this.btnFileHihoMaster.UseVisualStyleBackColor = true;
            this.btnFileHihoMaster.Click += new System.EventHandler(this.btnFileHihoMaster_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 32);
            this.label5.TabIndex = 2;
            this.label5.Text = "被保険者\r\n台帳データ(CSV) ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "療養費支給\r\n決定データ(CSV)";
            this.label3.Visible = false;
            // 
            // textBoxHihoMaster
            // 
            this.textBoxHihoMaster.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHihoMaster.Location = new System.Drawing.Point(180, 89);
            this.textBoxHihoMaster.Multiline = true;
            this.textBoxHihoMaster.Name = "textBoxHihoMaster";
            this.textBoxHihoMaster.Size = new System.Drawing.Size(512, 45);
            this.textBoxHihoMaster.TabIndex = 0;
            // 
            // textBoxSikyu
            // 
            this.textBoxSikyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSikyu.Location = new System.Drawing.Point(180, 159);
            this.textBoxSikyu.Multiline = true;
            this.textBoxSikyu.Name = "textBoxSikyu";
            this.textBoxSikyu.Size = new System.Drawing.Size(512, 45);
            this.textBoxSikyu.TabIndex = 0;
            this.textBoxSikyu.Visible = false;
            // 
            // textBoxClinicMaster
            // 
            this.textBoxClinicMaster.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClinicMaster.Location = new System.Drawing.Point(180, 27);
            this.textBoxClinicMaster.Multiline = true;
            this.textBoxClinicMaster.Name = "textBoxClinicMaster";
            this.textBoxClinicMaster.Size = new System.Drawing.Size(512, 45);
            this.textBoxClinicMaster.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "施術所マスタ(CSV)";
            // 
            // btnFileClinicMaster
            // 
            this.btnFileClinicMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileClinicMaster.Location = new System.Drawing.Point(698, 29);
            this.btnFileClinicMaster.Name = "btnFileClinicMaster";
            this.btnFileClinicMaster.Size = new System.Drawing.Size(48, 31);
            this.btnFileClinicMaster.TabIndex = 3;
            this.btnFileClinicMaster.Text = "...";
            this.btnFileClinicMaster.UseVisualStyleBackColor = true;
            this.btnFileClinicMaster.Click += new System.EventHandler(this.btnFile3_Click);
            // 
            // gbA
            // 
            this.gbA.Controls.Add(this.btnFileSikyuData);
            this.gbA.Controls.Add(this.btnFileHihoMaster);
            this.gbA.Controls.Add(this.btnFileClinicMaster);
            this.gbA.Controls.Add(this.label5);
            this.gbA.Controls.Add(this.label3);
            this.gbA.Controls.Add(this.label4);
            this.gbA.Controls.Add(this.textBoxHihoMaster);
            this.gbA.Controls.Add(this.textBoxSikyu);
            this.gbA.Controls.Add(this.textBoxClinicMaster);
            this.gbA.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbA.Location = new System.Drawing.Point(43, 96);
            this.gbA.Name = "gbA";
            this.gbA.Size = new System.Drawing.Size(786, 339);
            this.gbA.TabIndex = 1;
            this.gbA.TabStop = false;
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 567);
            this.Controls.Add(this.btnImp);
            this.Controls.Add(this.gbA);
            this.Controls.Add(this.gbJ);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.gbA.ResumeLayout(false);
            this.gbA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPunch;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImp;
        private System.Windows.Forms.Button btnFile2;
        private System.Windows.Forms.Button btnFile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxKyufu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFileSikyuData;
        private System.Windows.Forms.Button btnFileHihoMaster;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHihoMaster;
        private System.Windows.Forms.TextBox textBoxSikyu;
        private System.Windows.Forms.TextBox textBoxClinicMaster;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFileClinicMaster;
        private System.Windows.Forms.GroupBox gbA;
    }
}