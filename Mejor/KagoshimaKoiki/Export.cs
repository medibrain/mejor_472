﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mejor.KagoshimaKoiki
{
    public class Export
    {
        public static bool ExportMain(int cym)
        {
            
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            odd.ShowDialog();
            string strFolderName = odd.Name;
            if (strFolderName == string.Empty) return false;

            //ビューアデータ
            if (!ExportViewerData(cym,strFolderName)) return false;

            //あはき専用ファイル
            //if (!ExportAHK.CreateAHKFile(cym,strFolderName)) return false;

            return true;
        }

        /// <summary>
        /// ビューアデータ出力
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strOutputPath"></param>
        /// <returns></returns>
        public static bool ExportViewerData(int cym, string strOutputPath)
        {
            string log = string.Empty;
            string fileName;
          
            var dir = $"{strOutputPath}\\ViewerData";

            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //wf.LogPrint("提供データを取得しています");
                //var rrs = RefRece.SelectAll(cym);
                //var dic = new Dictionary<int, RefRece>();
                //rrs.ForEach(rr => { if (rr.AID != 0) dic.Add(rr.AID, rr); });

                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                
                //大阪広域と同じ方法で出力する
                //ビューアデータ
                var vdCount = ViewerData.Export(cym, apps, fileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    //$"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);          

            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }

    }

    /// <summary>
    /// あはき用
    /// </summary>
    //public static partial class ExportAHK
    //{
    //    /// <summary>
    //    /// ヘッダ文字列
    //    /// </summary>
    //    public static class AHK_HEADER 
    //    {
    //        public static string f01 { get; set; } = "10000000D16201";                        //10000000D16201 を入力;
    //        public static string f02 { get; set; } = String.Empty.PadLeft(14);                //空白とする。;
    //        public static string f03 { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmss"); //保存日時 yyyyMMddHHmmss
    //        public static string f04 { get; set; } = String.Empty.PadLeft(958);               //空白とする。;

    //        public static string ToString()
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            sb.Append(AHK_HEADER.f01);
    //            sb.Append(AHK_HEADER.f02);
    //            sb.Append(AHK_HEADER.f03);
    //            sb.Append(AHK_HEADER.f04);
    //            return sb.ToString();

    //        }
    //    }

    //    /// <summary>
    //    /// フッタ文字列
    //    /// </summary>
    //    public static class AHK_FOOTER
    //    {            
    //        public static string f01 { get;set; }= "39999999";                                 //39999999 を入力;
    //        public static string f02 { get; set; } = string.Empty;                             //件数;
    //        public static string f03 { get; set; } = String.Empty.PadLeft(985);                //空白とする。;

    //        /// <summary>
    //        /// フッタ文字列
    //        /// </summary>
    //        /// <param name="count">件数</param>
    //        /// <returns></returns>
    //        public static string ToString(int count)
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            sb.Append(AHK_FOOTER.f01);
    //            AHK_FOOTER.f02 = count.ToString().PadLeft(7, '0');
    //            sb.Append(AHK_FOOTER.f02);
    //            sb.Append(AHK_FOOTER.f03);
    //            return sb.ToString();
    //        }
    //    }

    //    /// <summary>
    //    /// データ定義
    //    /// </summary>
    //    public static class AHK_DATA
    //    {
    //        #region メンバ
    //        public static string f01 { get; set; } = "2";                                         //レコード識別子;
    //        public static string f02 { get; set; } = string.Empty;                                         //レコード番号;
    //        public static string f03 { get; set; } = string.Empty.PadLeft(30);                                         //レセプト管理番号;
    //        public static string f04 { get; set; } = string.Empty;                                         //診療年月;
    //        public static string f05 { get; set; } = string.Empty;                                         //都道府県番号;
    //        public static string f06 { get; set; } = "9";                                         //点数表コード;
    //        public static string f07 { get; set; } = string.Empty;                                         //医療機関等コード;
    //        public static string f08 { get; set; } = "1";                                         //保険種別;
    //        public static string f09 { get; set; } = string.Empty;                                         //区分コード;
    //        public static string f10 { get; set; } = string.Empty;                                         //保険者番号;
    //        public static string f11 { get; set; } = string.Empty;                                         //被保険者番号;
    //        public static string f12 { get; set; } = string.Empty;                                         //給付割合;
    //        public static string f13 { get; set; } = string.Empty;                                         //療養費区分コード;
    //        public static string f14 { get; set; } = string.Empty;                                         //施術開始年月日;
    //        public static string f15 { get; set; } = string.Empty;                                         //施術終了年月日;
    //        public static string f16 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月１;
    //        public static string f17 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月２;
    //        public static string f18 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月３;
    //        public static string f19 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月４;
    //        public static string f20 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月５;
    //        public static string f21 { get; set; } = string.Empty.PadLeft(5,'0');                                         //年月６;
    //        public static string f22 { get; set; } = string.Empty;                                         //診療実日数;
    //        public static string f23 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数１;
    //        public static string f24 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数２;
    //        public static string f25 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数３;
    //        public static string f26 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数４;
    //        public static string f27 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数５;
    //        public static string f28 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数６;
    //        public static string f29 { get; set; } = string.Empty;                                         //請求金額;
    //        public static string f30 { get; set; } = string.Empty;                                         //決定金額;
    //        public static string f31 { get; set; } = string.Empty;                                         //費用額（合計）;
    //        public static string f32 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額１;
    //        public static string f33 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額２;
    //        public static string f34 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額３;
    //        public static string f35 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額４;
    //        public static string f36 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額５;
    //        public static string f37 { get; set; } = string.Empty.PadLeft(8,'0');                                         //金額６;
    //        public static string f38 { get; set; } = string.Empty.PadLeft(8,'0');                                         //負担金額;
    //        public static string f39 { get; set; } = string.Empty.PadLeft(3, '0');                                         //回数;
    //        public static string f40 { get; set; } = string.Empty.PadLeft(6,'0');                                         //決定金額;
    //        public static string f41 { get; set; } = string.Empty.PadLeft(6,'0');                                         //標準負担額;
    //        public static string f42 { get; set; } = string.Empty;                                         //負担者番号;
    //        public static string f43 { get; set; } = string.Empty;                                         //受給者番号;
    //        public static string f44 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数;
    //        public static string f45 { get; set; } = string.Empty.PadLeft(7,'0');                                         //請求点数;
    //        public static string f46 { get; set; } = string.Empty.PadLeft(7,'0');                                         //決定点数;
    //        public static string f47 { get; set; } = string.Empty.PadLeft(8,'0');                                         //公費対象負担金額;
    //        public static string f48 { get; set; } = string.Empty.PadLeft(8,'0');                                         //公費患者負担額;
    //        public static string f49 { get; set; } = string.Empty.PadLeft(3,'0');                                         //食事回数;
    //        public static string f50 { get; set; } = string.Empty.PadLeft(6,'0');                                         //食事決定金額;
    //        public static string f51 { get; set; } = string.Empty.PadLeft(6,'0');                                         //食事標準負担額;
    //        public static string f52 { get; set; } = string.Empty.PadLeft(8);                                         //負担者番号;
    //        public static string f53 { get; set; } = string.Empty.PadLeft(7);                                         //受給者番号;
    //        public static string f54 { get; set; } = string.Empty.PadLeft(2,'0');                                         //日数;
    //        public static string f55 { get; set; } = string.Empty.PadLeft(7,'0');                                         //請求点数;
    //        public static string f56 { get; set; } = string.Empty.PadLeft(7,'0');                                         //決定点数;
    //        public static string f57 { get; set; } = string.Empty.PadLeft(8,'0');                                         //公費対象負担金額;
    //        public static string f58 { get; set; } = string.Empty.PadLeft(8,'0');                                         //公費患者負担額;
    //        public static string f59 { get; set; } = string.Empty.PadLeft(3,'0');                                         //食事回数;
    //        public static string f60 { get; set; } = string.Empty.PadLeft(6,'0');                                         //食事決定金額;
    //        public static string f61 { get; set; } = string.Empty.PadLeft(6,'0');                                         //食事標準負担額;
    //        public static string f62 { get; set; } = "2";                                         //支払先区分コード;
    //        public static string f63 { get; set; } = string.Empty.PadLeft(2);                                         //都道府県番号;
    //        public static string f64 { get; set; } = string.Empty.PadLeft(1);                                         //点数表コード;
    //        public static string f65 { get; set; } = string.Empty.PadLeft(7);                                         //番号;
    //        public static string f66 { get; set; } = string.Empty.PadLeft(7,'0');                                         //受付年月日;
    //        public static string f67 { get; set; } = string.Empty.PadLeft(7,'0');                                         //決定年月日;
    //        public static string f68 { get; set; } = string.Empty.PadLeft(7,'0');                                         //支出年月日;
    //        public static string f69 { get; set; } = string.Empty;                                         //支給決定額;
    //        public static string f70 { get; set; } = string.Empty.PadLeft(8);                                         //充当額;
    //        public static string f71 { get; set; } = string.Empty.PadLeft(8);                                         //支給調整額;
    //        public static string f72 { get; set; } = string.Empty.PadLeft(8);                                         //支給額;
    //        public static string f73 { get; set; } = string.Empty.PadLeft(7);                                         //郵便番号;
    //        public static string f74 { get; set; } = string.Empty.PadLeft(16);                                         //電話番号;
    //        public static string f75 { get; set; } = string.Empty.PadLeft(60);                                         //氏名（漢字）;
    //        public static string f76 { get; set; } = string.Empty.PadLeft(200);                                         //住所（漢字）;
    //        public static string f77 { get; set; } = string.Empty.PadLeft(30);                                         //被保険者との関係;
    //        public static string f78 { get; set; } = string.Empty.PadLeft(1);                                         //金融機関区分コード;
    //        public static string f79 { get; set; } = string.Empty.PadLeft(4);                                         //金融機関コード;
    //        public static string f80 { get; set; } = string.Empty.PadLeft(3);                                         //金融機関店舗コード;
    //        public static string f81 { get; set; } = string.Empty.PadLeft(1);                                         //預金種別コード;
    //        public static string f82 { get; set; } = string.Empty.PadLeft(13);                                         //口座番号;
    //        public static string f83 { get; set; } = string.Empty.PadLeft(200);                                         //口座名義人氏名（カナ）;
    //        public static string f84 { get; set; } = string.Empty;                                         //生年月日;
    //        public static string f85 { get; set; } = string.Empty.PadLeft(40);                                         //予備;
    //        #endregion

    //        /// <summary>
    //        /// データ用文字列
    //        /// </summary>
    //        /// <returns></returns>
    //        public static string ToString()
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            sb.Append(f01);
    //            sb.Append(f02);
    //            sb.Append(f03);
    //            sb.Append(f04);
    //            sb.Append(f05);
    //            sb.Append(f06);
    //            sb.Append(f07);
    //            sb.Append(f08);
    //            sb.Append(f09);
    //            sb.Append(f10);
    //            sb.Append(f11);
    //            sb.Append(f12);
    //            sb.Append(f13);
    //            sb.Append(f14);
    //            sb.Append(f15);
    //            sb.Append(f16);
    //            sb.Append(f17);
    //            sb.Append(f18);
    //            sb.Append(f19);
    //            sb.Append(f20);
    //            sb.Append(f21);
    //            sb.Append(f22);
    //            sb.Append(f23);
    //            sb.Append(f24);
    //            sb.Append(f25);
    //            sb.Append(f26);
    //            sb.Append(f27);
    //            sb.Append(f28);
    //            sb.Append(f29);
    //            sb.Append(f30);
    //            sb.Append(f31);
    //            sb.Append(f32);
    //            sb.Append(f33);
    //            sb.Append(f34);
    //            sb.Append(f35);
    //            sb.Append(f36);
    //            sb.Append(f37);
    //            sb.Append(f38);
    //            sb.Append(f39);
    //            sb.Append(f40);
    //            sb.Append(f41);
    //            sb.Append(f42);
    //            sb.Append(f43);
    //            sb.Append(f44);
    //            sb.Append(f45);
    //            sb.Append(f46);
    //            sb.Append(f47);
    //            sb.Append(f48);
    //            sb.Append(f49);
    //            sb.Append(f50);
    //            sb.Append(f51);
    //            sb.Append(f52);
    //            sb.Append(f53);
    //            sb.Append(f54);
    //            sb.Append(f55);
    //            sb.Append(f56);
    //            sb.Append(f57);
    //            sb.Append(f58);
    //            sb.Append(f59);
    //            sb.Append(f60);
    //            sb.Append(f61);
    //            sb.Append(f62);
    //            sb.Append(f63);
    //            sb.Append(f64);
    //            sb.Append(f65);
    //            sb.Append(f66);
    //            sb.Append(f67);
    //            sb.Append(f68);
    //            sb.Append(f69);
    //            sb.Append(f70);
    //            sb.Append(f71);
    //            sb.Append(f72);
    //            sb.Append(f73);
    //            sb.Append(f74);
    //            sb.Append(f75);
    //            sb.Append(f76);
    //            sb.Append(f77);
    //            sb.Append(f78);
    //            sb.Append(f79);
    //            sb.Append(f80);
    //            sb.Append(f81);
    //            sb.Append(f82);
    //            sb.Append(f83);
    //            sb.Append(f84);
    //            sb.Append(f85);
    //            return sb.ToString();
    //        }

    //    }


    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="cym"></param>
    //    /// <returns></returns>
    //    public static bool CreateAHKFile(int cym,string strOutputPath)
    //    {
    //        StreamWriter sw = null;

    //        //20220404131825 furukawa st ////////////////////////
    //        //フォルダを引数から取得に変更

    //        //OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
    //        //odd.ShowDialog();
    //        //string strOutputPath = odd.Name;

    //        //20220404131825 furukawa ed ////////////////////////

    //        if (strOutputPath == string.Empty) return false;

    //        string strFileName = strOutputPath + "\\KRGY10.RYOUYOU.txt";//ファイル名は固定？
    //        try
    //        {
                
    //            //サンプルデータよりシフトＪＩＳと判断
    //            sw = new StreamWriter(strFileName,false,System.Text.Encoding.GetEncoding("shift-jis"));

    //            sw.Write(AHK_HEADER.ToString());

    //            int count = 0;
    //            string strData = string.Empty;
    //            if (!CreateAHKData(cym,out strData,out count)) return false;
    //            sw.Write(strData);

    //            sw.Write(AHK_FOOTER.ToString(count));
                
    //            MessageBox.Show("終了しました");

    //            return true;
    //        }
    //        catch(Exception ex)
    //        {
    //            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
    //            return false;
    //        }
    //        finally
    //        {
    //            if(sw!=null)sw.Close();
    //        }


    //    }


    //    /// <summary>
    //    /// データ本体
    //    /// </summary>
    //    /// <param name="cym">メホール請求年月</param>
    //    /// <param name="strData">出力文字列</param>
    //    /// <param name="count">出力件数</param>
    //    /// <returns></returns>
    //    public static bool CreateAHKData(int cym,out string strData,out int count)
    //    {
    //        string strRet = string.Empty;

    //        WaitForm wf = new WaitForm();
    //        wf.ShowDialogOtherTask();


    //        List<App> lst = App.GetApps(cym);
    //        lst.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            
    //        try
    //        {
    //            //出力用件数
    //            int intRecNo = 0;


    //            foreach (App a in lst)
    //            {
    //                if (a.AppType != APP_TYPE.あんま && a.AppType != APP_TYPE.鍼灸) continue;

    //                if (CommonTool.WaitFormCancelProcess(wf))
    //                {
    //                    strData = string.Empty;
    //                    count = -1;
    //                    return false;
    //                }

    //                //1からなので先にインクリメント
    //                intRecNo++;

    //                wf.LogPrint($"レコード番号 {intRecNo} 作成");

    //                //レコード番号	「0000001」から連番を設定する。
    //                AHK_DATA.f02 = intRecNo.ToString().PadLeft(7,'0');

    //                //診療年月	診療年月(和暦)を設定する。例 平成20年4月→「42004」
    //                AHK_DATA.f04 = (DateTimeEx.GetEraNumberYearFromYYYYMM(a.YM)*100+a.MediMonth).ToString();

    //                //都道府県番号	医療機関又は施術師の都道府県番号を設定する。
    //                AHK_DATA.f05 = a.ClinicNum.Substring(0, 2);


    //                //医療機関等コード	施術師の番号を設定する。有効桁に対して右詰前ZERO設定し、不足桁には空白を設定する。
                    
    //                //20220324174614 furukawa st ////////////////////////
    //                //柔整師登録記号番号でなく、医療機関コード
    //                AHK_DATA.f07 = a.ClinicNum.Substring(3).PadLeft(7, '0');
    //                //AHK_DATA.f07 = a.DrNum.Substring(3).PadLeft(7, '0');
    //                //20220324174614 furukawa ed ////////////////////////



    //                //区分コード	08：外来8割、外来9割　00：外来7割のいずれかを設定する。
    //                AHK_DATA.f09 = a.Ratio == 7 ? "00" : "08";

    //                //保険者番号	保険者番号を設定する。
    //                AHK_DATA.f10 = a.InsNum;

    //                //被保険者番号 被保険者番号を右詰め前ZEROで設定する。
    //                AHK_DATA.f11 = a.HihoNum.PadLeft(8, '0');

    //                //給付割合	給付割合を％形式の数値で設定する。090:9割 070:7割　080;8割
    //                AHK_DATA.f12 =  (a.Ratio*10).ToString().PadLeft(3,'0');

    //                //療養費区分コード	療養費区分コード( 05:マッサージ 06:鍼灸 ）のいずれかを設定する。
    //                if (a.AppType == APP_TYPE.あんま) AHK_DATA.f13 = "05";
    //                if (a.AppType == APP_TYPE.鍼灸) AHK_DATA.f13 = "06";

    //                //施術開始年月日 施術(診療)開始年月日(和暦)を設定する。例 平成20年4月1日→「4200401」未設定の場合はZERO埋めとする。
    //                if (a.FushoStartDate1 == new DateTime(1, 1, 1)) AHK_DATA.f14 = string.Empty.PadLeft(7, '0');
    //                 else AHK_DATA.f14 = DateTimeEx.GetIntJpDateWithEraNumber(a.FushoStartDate1).ToString();

    //                //施術終了年月日 施術(診療)終了年月日(和暦)を設定する。例 平成20年5月31日→「4200531」未設定の場合はZERO埋めとする。
    //                if (a.FushoFinishDate1 == new DateTime(1, 1, 1)) AHK_DATA.f15 = string.Empty.PadLeft(7, '0');
    //                else AHK_DATA.f15 = DateTimeEx.GetIntJpDateWithEraNumber(a.FushoFinishDate1).ToString();

    //                //診療実日数 診療実日数を右詰で前ZERO埋めして設定する。未設定の場合はZERO埋めする。
    //                AHK_DATA.f22 = a.CountedDays.ToString().PadLeft(2, '0');

    //                //請求金額	療養費の請求合計金額(保険負担分)を右詰め前ZEROで設定する。
    //                AHK_DATA.f29 = a.Charge.ToString().PadLeft(8, '0');

    //                //決定金額	上と同額。請求金額に査定がある場合は請求額の査定後金額を右詰め前ZEROで設定する。
    //                //査定がな場合は請求額と同額を右詰め前ZEROで設定する。未設定の場合はZERO埋めする
    //                AHK_DATA.f30 = a.Charge.ToString().PadLeft(8, '0');

    //                //費用額（合計）	費用額を右詰め前ZEROで設定する。
    //                AHK_DATA.f31 = a.Total.ToString().PadLeft(8, '0');



    //                //負担者番号 公費１負担者番号を左詰で設定する。未設定の場合は空白埋めする。
    //                AHK_DATA.f42 = a.TaggedDatas.KouhiNum.PadRight(8);

    //                //受給者番号 公費１負担者番号を左詰で設定する。未設定の場合は空白埋めする。
    //                AHK_DATA.f43 = a.TaggedDatas.JukyuNum.PadRight(7);


    //                //支給決定額	「決定金額」と「支給決定額」は同額を設定する。
    //                AHK_DATA.f69 = AHK_DATA.f30;

    //                //生年月日    生年月日を設定する。　例　昭和２年１月１日 → 3020101未設定の場合は空白とする。
    //                AHK_DATA.f84 = DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString();



    //                //文字列長チェック
    //                if (AHK_DATA.ToString().GetByteLength() != 1000)
    //                {
    //                    MessageBox.Show($"レコード番号 {intRecNo} 1000bytesではありません");
    //                    strData = string.Empty;
    //                    count = -1;
    //                    return false;
    //                }

    //                strRet += AHK_DATA.ToString();
                    
    //                //intRecNo++;//件数
    //            }

    //            strData = strRet;//データ文字列
    //            count = intRecNo;//件数
    //            return true;;
    //        }
    //        catch(Exception ex)
    //        {
    //            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
    //            strData = string.Empty;
    //            count = -1;
    //            return false;
    //        }
    //        finally
    //        {
    //            wf.Dispose();
    //        }

    //    }

    //}
}
