﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.KagoshimaKoiki
{
   
    public partial class imp_punch
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                          //インポートID;

        public string f001_chargeym { get; set; } = string.Empty;                //1_請求年月yyyymm;
        public string f002_hihonum { get; set; } = string.Empty;                 //5_被保険者番号;
        public string f003_rezekind { get; set; } = string.Empty;                //6_レセプト種類コード;
        public string f004_mediym { get; set; } = string.Empty;                  //8_診療年月yyyymm;
        public string f005_insnum { get; set; } = string.Empty;                  //14_保険者番号;
        public string f006_gender { get; set; } = string.Empty;                  //15_性別コード;
        public string f007_birthdayw { get; set; } = string.Empty;               //16_生年月日和暦;
        public string f008_counteddays { get; set; } = string.Empty;             //30_診療実日数;
        public string f009_ratio { get; set; } = string.Empty;                   //31_給付割合パーセント;
        public string f010_chargescore { get; set; } = string.Empty;             //32_請求点数;
        public string f011_decidescore { get; set; } = string.Empty;             //33_決定点数;
        public string f012_partial { get; set; } = string.Empty;                 //34_一部負担額;
        public string f013_total { get; set; } = string.Empty;                   //91_費用金額;
        public string f014_futan { get; set; } = string.Empty;                   //92_保険者負担額;
        public string f015_futan2 { get; set; } = string.Empty;                  //93_一部負担相当額;
        public string f016_zikofutan { get; set; } = string.Empty;               //97_自己負担額;
        public string f017_kanrinum { get; set; } = string.Empty;                //121_電算管理番号;
        public string f018_clinicnum { get; set; } = string.Empty;               //医療機関番号;
        public string f019_clinicname { get; set; } = string.Empty;              //医療機関名;
        public int cym { get; set; } = 0;                                        //メホール請求年月;
        public int mediymad { get; set; } = 0;                                   //施術年月西暦;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;            //生年月日西暦;




        /// <summary>
        /// cymと被保険者証番号でリスト抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_punch> select(int cym, string strHihonum)
        {
            List<imp_punch> lst = new List<imp_punch>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($"  * ");
            sb.AppendLine($" from imp_punch where cym={cym} and f002_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                ////レコードがない場合最小値返し

                //if (l.Count == 0)
                //{
                //    imp_punch imp = new imp_punch();

                //    imp.f000_importid = 0;
                //    imp.f001_chargeym = string.Empty;
                //    imp.f002_hihonum = string.Empty;
                //    imp.f003_rezekind = string.Empty;
                //    imp.f004_mediym = string.Empty;
                //    imp.f005_insnum = string.Empty;
                //    imp.f006_gender = string.Empty;
                //    imp.f007_birthdayw = string.Empty;
                //    imp.f008_counteddays = string.Empty;
                //    imp.f009_ratio = string.Empty;
                //    imp.f010_chargescore = string.Empty;
                //    imp.f011_decidescore = string.Empty;
                //    imp.f012_partial = string.Empty;
                //    imp.f013_total = string.Empty;
                //    imp.f014_futan = string.Empty;
                //    imp.f015_futan2 = string.Empty;
                //    imp.f016_zikofutan = string.Empty;
                //    imp.f017_kanrinum = string.Empty;
                //    imp.f018_clinicnum = string.Empty;
                //    imp.f019_clinicname = string.Empty;

                //    imp.mediymad = 0;
                //    imp.cym = cym;
                //    imp.birthdayad = DateTime.MinValue;
                //    lst.Add(imp);
                //    return lst;
                //}


                foreach (var item in l)
                {
                    imp_punch imp = new imp_punch();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_chargeym = item[1].ToString();
                    imp.f002_hihonum = item[2].ToString();
                    imp.f003_rezekind = item[3].ToString();
                    imp.f004_mediym = item[4].ToString();
                    imp.f005_insnum = item[5].ToString();
                    imp.f006_gender = item[6].ToString();
                    imp.f007_birthdayw = item[7].ToString();
                    imp.f008_counteddays = item[8].ToString();
                    imp.f009_ratio = item[9].ToString();
                    imp.f010_chargescore = item[10].ToString();
                    imp.f011_decidescore = item[11].ToString();
                    imp.f012_partial = item[12].ToString();
                    imp.f013_total = item[13].ToString();
                    imp.f014_futan = item[14].ToString();
                    imp.f015_futan2 = item[15].ToString();
                    imp.f016_zikofutan = item[16].ToString();
                    imp.f017_kanrinum = item[17].ToString();
                    imp.f018_clinicnum = item[18].ToString();
                    imp.f019_clinicname = item[19].ToString();

                    imp.cym = int.Parse(item[20].ToString());
                    imp.mediymad = int.Parse(item[21].ToString());
                    imp.birthdayad = DateTime.Parse(item[22].ToString());

                    lst.Add(imp);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }




    }


    /// <summary>
    /// 施術所マスタ
    /// </summary>
    public partial class imp_clinicmaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]     
        public int f000_importid { get;set; }=0;                            //インポートID;
        public string f001_clinicnum { get; set; } = string.Empty;          //医療機関コード;
        public string f002_dantainum { get; set; } = string.Empty;          //柔整団体機関コード;
        public string f003_clinickana { get; set; } = string.Empty;         //医療機関名（カナ）;
        public string f004_clinicname { get; set; } = string.Empty;         //医療機関名（漢字）;
        public string f005_cliniczip { get; set; } = string.Empty;          //郵便番号;
        public string f006_clinicadd { get; set; } = string.Empty;          //住所;
        public string f007_clinicbanch { get; set; } = string.Empty;        //番地;
        public string f008_clinickatagaki { get; set; } = string.Empty;     //方書;
        public string f009_clinictel { get; set; } = string.Empty;          //電話番号;
        public string f010_clinicperson { get; set; } = string.Empty;       //代表者名;
        public int cym { get; set; } = 0;                                   //メホール請求年月;



        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_clinicmaster> selectall(int cym)
        {
            List<imp_clinicmaster> lst = new List<imp_clinicmaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_clinicmaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_clinicmaster imp = new imp_clinicmaster();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_clinicnum = item[1].ToString();
                    imp.f002_dantainum = item[2].ToString();
                    imp.f003_clinickana = item[3].ToString();
                    imp.f004_clinicname = item[4].ToString();
                    imp.f005_cliniczip = item[5].ToString();
                    imp.f006_clinicadd = item[6].ToString();
                    imp.f007_clinicbanch = item[7].ToString();
                    imp.f008_clinickatagaki = item[8].ToString();
                    imp.f009_clinictel = item[9].ToString();
                    imp.f010_clinicperson = item[10].ToString();
                    imp.cym = int.Parse(item[11].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 医療機関番号指定取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_clinicmaster> select(int cym,string strClinicNum)
        {
            List<imp_clinicmaster> lst = new List<imp_clinicmaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_clinicmaster where cym={cym} and f001_clinicnum='{strClinicNum}' order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_clinicmaster imp = new imp_clinicmaster();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_clinicnum = item[1].ToString();
                    imp.f002_dantainum = item[2].ToString();
                    imp.f003_clinickana = item[3].ToString();
                    imp.f004_clinicname = item[4].ToString();
                    imp.f005_cliniczip = item[5].ToString();
                    imp.f006_clinicadd = item[6].ToString();
                    imp.f007_clinicbanch = item[7].ToString();
                    imp.f008_clinickatagaki = item[8].ToString();
                    imp.f009_clinictel = item[9].ToString();
                    imp.f010_clinicperson = item[10].ToString();
                    imp.cym = int.Parse(item[11].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

    }

    /// <summary>
    /// 被保険者マスタ
    /// </summary>
    public partial class imp_hihomaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                   //インポートID;
        public string f001_hihonum { get; set; } = string.Empty;                   //被保険者番号;
        public string f002_pname { get; set; } = string.Empty;                     //漢字氏名;
        public string f003_pkana { get; set; } = string.Empty;                     //カナ氏名;
        public string f004_pbirthday { get; set; } = string.Empty;                 //生年月日;
        public string f005_pgender { get; set; } = string.Empty;                   //性別;
        public string f006_sendzip { get; set; } = string.Empty;                   //発送用郵便番号;
        public string f007_sendadd { get; set; } = string.Empty;                   //発送用住所;
        public string f008_futankbn1 { get; set; } = string.Empty;                 //負担区分1;
        public int cym { get; set; } = 0;                                          //メホール請求年月;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;              //生年月日西暦;

        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_hihomaster> selectall(int cym)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_hihomaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();

                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_pname = item[2].ToString();
                    imp.f003_pkana = item[3].ToString();
                    imp.f004_pbirthday = item[4].ToString();
                    imp.f005_pgender = item[5].ToString();
                    imp.f006_sendzip = item[6].ToString();
                    imp.f007_sendadd = item[7].ToString();
                    imp.f008_futankbn1 = item[8].ToString();
                    imp.cym = int.Parse(item[9].ToString()); 
                    imp.birthdayad = DateTime.Parse(item[10].ToString());




                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// cymと被保険者証番号でリスト抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_hihomaster> select (int cym,string strHihonum)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($"  f000_importid");
            sb.AppendLine($" ,f001_hihonum");
            sb.AppendLine($" ,f002_pname");
            sb.AppendLine($" ,f003_pkana");
            sb.AppendLine($" ,f004_pbirthday");
            sb.AppendLine($" ,f005_pgender");
            sb.AppendLine($" ,f006_sendzip");
            sb.AppendLine($" ,f007_sendadd");
            sb.AppendLine($" ,f008_futankbn1");
            sb.AppendLine($" ,cym");
            sb.AppendLine($" ,birthdayad");
            sb.AppendLine($" from imp_hihomaster where cym={cym} and f001_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();
             
                ////レコードがない場合最小値返し
                
                //if (l.Count == 0)
                //{
                //    imp_hihomaster imp = new imp_hihomaster();
                //    imp.f000_importid = 0;
                //    imp.f001_hihonum = string.Empty;
                //    imp.f002_pname = string.Empty;
                //    imp.f003_pkana = string.Empty;
                //    imp.f004_pbirthday = string.Empty;
                //    imp.f005_pgender = string.Empty;
                //    imp.f006_sendzip = string.Empty;
                //    imp.f007_sendadd = string.Empty;
                //    imp.f008_futankbn1 = string.Empty;
                //    imp.cym = cym;
                //    imp.birthdayad = DateTime.MinValue;
                //    lst.Add(imp);
                //    return lst;
                //}
             

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_pname = item[2].ToString();
                    imp.f003_pkana = item[3].ToString();
                    imp.f004_pbirthday = item[4].ToString();
                    imp.f005_pgender = item[5].ToString();
                    imp.f006_sendzip = item[6].ToString();
                    imp.f007_sendadd = item[7].ToString();
                    imp.f008_futankbn1 = item[8].ToString();
                    imp.cym = int.Parse(item[9].ToString());
                    imp.birthdayad = DateTime.Parse(item[10].ToString());

                    lst.Add(imp);
                }
                return lst;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
            
        }

      
    }

    /// <summary>
    /// 支給決定データ
    /// </summary>
    public partial class imp_chargedata
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]      
        public int f000_importid { get;set; }=0;                                  //インポートID;
        public string f001_insnum { get; set; } = string.Empty;                   //保険者番号;
        public string f002_insname { get; set; } = string.Empty;                  //保険者名;
        public string f003_createymd { get; set; } = string.Empty;                //作成年月日yyyymmdd;
        public string f004_shishutsuymd { get; set; } = string.Empty;             //支出年月日yyyymmdd;
        public string f005_hihonum { get; set; } = string.Empty;                  //被保険者番号;
        public string f006_pname { get; set; } = string.Empty;                    //氏名;
        public string f007_sikyukind { get; set; } = string.Empty;                //支給種別;
        public string f008_sikyunum { get; set; } = string.Empty;                 //支給整理番号;
        public string f009_mediym { get; set; } = string.Empty;                   //診療年月yyyymm;
        public string f010_sikyukgk { get; set; } = string.Empty;                 //支給金額;
        public string f011_sikyuketteikgk { get; set; } = string.Empty;           //支給決定額;
        public string f012_jutogaku { get; set; } = string.Empty;                 //充当額;
        public string f013_sikyuchoseigaku { get; set; } = string.Empty;          //支給調整額;
        public string f014_total { get; set; } = string.Empty;                    //費用額;
        public string f015_partial { get; set; } = string.Empty;                  //一部負担額;
        public string f016_shiharaikbn { get; set; } = string.Empty;              //支払区分コード;
        public string f017_tuchinum { get; set; } = string.Empty;                 //通知番号;
        public string f018_shiharaisaki_clinicnum { get; set; } = string.Empty;   //支払先医療機関コード;
        public string f019_shiharaisaki_name { get; set; } = string.Empty;        //支払先医療機関名;
        public string f020_shiharaisaki_add { get; set; } = string.Empty;         //支払先医療機関住所;
        public string f021_shiharaisaki_zip { get; set; } = string.Empty;         //支払先医療機関郵便番号;
        public string f022_kojinkbn { get; set; } = string.Empty;                 //個人区分コード;
        public string f023_atenanum { get; set; } = string.Empty;                 //宛名番号;
        public int cym { get; set; } = 0;                                         //メホール請求年月;



        /// <summary>
        /// 支給決定データ取得
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// <returns></returns>
        public static List<imp_chargedata> selectall(int cym)
        {
            List<imp_chargedata> lst = new List<imp_chargedata>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_chargedata where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_chargedata imp = new imp_chargedata();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_insnum = item[1].ToString();
                    imp.f002_insname = item[2].ToString();
                    imp.f003_createymd = item[3].ToString();
                    imp.f004_shishutsuymd = item[4].ToString();
                    imp.f005_hihonum = item[5].ToString();
                    imp.f006_pname = item[6].ToString();
                    imp.f007_sikyukind = item[7].ToString();
                    imp.f008_sikyunum = item[8].ToString();
                    imp.f009_mediym = item[9].ToString();
                    imp.f010_sikyukgk = item[10].ToString();
                    imp.f011_sikyuketteikgk = item[11].ToString();
                    imp.f012_jutogaku = item[12].ToString();
                    imp.f013_sikyuchoseigaku = item[13].ToString();
                    imp.f014_total = item[14].ToString();
                    imp.f015_partial = item[15].ToString();
                    imp.f016_shiharaikbn = item[16].ToString();
                    imp.f017_tuchinum = item[17].ToString();
                    imp.f018_shiharaisaki_clinicnum = item[18].ToString();
                    imp.f019_shiharaisaki_name = item[19].ToString();
                    imp.f020_shiharaisaki_add = item[20].ToString();
                    imp.f021_shiharaisaki_zip = item[21].ToString();
                    imp.f022_kojinkbn = item[22].ToString();
                    imp.f023_atenanum = item[23].ToString();
                   
                    imp.cym = int.Parse(item[24].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch(Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 支給決定データ被保険者番号指定取得
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// <param name="strHihoNum">ヒホバン</param>
        /// <returns></returns>
        public static List<imp_chargedata> select(int cym,string strHihoNum,int total,int mediym)
        {
            List<imp_chargedata> lst = new List<imp_chargedata>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_chargedata where " +
                $"cym={cym} and f005_hihonum='{strHihoNum}' and f014_total='{total.ToString()}' " +
                $"and f009_mediym='{mediym.ToString()}' order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_chargedata imp = new imp_chargedata();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_insnum = item[1].ToString();
                    imp.f002_insname = item[2].ToString();
                    imp.f003_createymd = item[3].ToString();
                    imp.f004_shishutsuymd = item[4].ToString();
                    imp.f005_hihonum = item[5].ToString();
                    imp.f006_pname = item[6].ToString();
                    imp.f007_sikyukind = item[7].ToString();
                    imp.f008_sikyunum = item[8].ToString();
                    imp.f009_mediym = item[9].ToString();
                    imp.f010_sikyukgk = item[10].ToString();
                    imp.f011_sikyuketteikgk = item[11].ToString();
                    imp.f012_jutogaku = item[12].ToString();
                    imp.f013_sikyuchoseigaku = item[13].ToString();
                    imp.f014_total = item[14].ToString();
                    imp.f015_partial = item[15].ToString();
                    imp.f016_shiharaikbn = item[16].ToString();
                    imp.f017_tuchinum = item[17].ToString();
                    imp.f018_shiharaisaki_clinicnum = item[18].ToString();
                    imp.f019_shiharaisaki_name = item[19].ToString();
                    imp.f020_shiharaisaki_add = item[20].ToString();
                    imp.f021_shiharaisaki_zip = item[21].ToString();
                    imp.f022_kojinkbn = item[22].ToString();
                    imp.f023_atenanum = item[23].ToString();

                    imp.cym = int.Parse(item[24].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
