﻿namespace Mejor.KagoshimaKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelAppStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panelFirstDate = new System.Windows.Forms.Panel();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.labelNewCont2 = new System.Windows.Forms.Label();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.labelNewCont = new System.Windows.Forms.Label();
            this.pSejutu = new System.Windows.Forms.Panel();
            this.vbSejutuG = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblSejutu = new System.Windows.Forms.Label();
            this.lblSejutuM = new System.Windows.Forms.Label();
            this.lblSejutuY = new System.Windows.Forms.Label();
            this.vbSejutuM = new Mejor.VerifyBox();
            this.vbSejutuY = new Mejor.VerifyBox();
            this.vcSejutu = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.panelMatchWhere = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.checkBoxNumber = new System.Windows.Forms.CheckBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelF1 = new System.Windows.Forms.Label();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyCheckBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.labelF2 = new System.Windows.Forms.Label();
            this.labelF3 = new System.Windows.Forms.Label();
            this.labelF4 = new System.Windows.Forms.Label();
            this.labelF5 = new System.Windows.Forms.Label();
            this.labelHarikyuFushoName = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelFirstDate.SuspendLayout();
            this.pSejutu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMatchWhere.SuspendLayout();
            this.SuspendLayout();
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(148, 20);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(203, 20);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(326, 20);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxHnum.TabIndex = 7;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.textBox_Enter);
            this.verifyBoxHnum.Leave += new System.EventHandler(this.verifyBoxHnum_Leave);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(571, 6);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 23);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(186, 31);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(241, 31);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(326, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(41, 12);
            this.labelHnum.TabIndex = 6;
            this.labelHnum.Text = "被保番";
            // 
            // labelAppStatus
            // 
            this.labelAppStatus.AutoSize = true;
            this.labelAppStatus.Location = new System.Drawing.Point(921, 10);
            this.labelAppStatus.Name = "labelAppStatus";
            this.labelAppStatus.Size = new System.Drawing.Size(58, 12);
            this.labelAppStatus.TabIndex = 10;
            this.labelAppStatus.Text = "AppStatus";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(922, 30);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 11;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(896, 602);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 8;
            this.labelTotal.Text = "合計";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(899, 616);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 33;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.textBox_Enter);
            this.verifyBoxTotal.Leave += new System.EventHandler(this.verifyBoxTotal_Leave);
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 768);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 768);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(73, 743);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(1, 743);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(37, 743);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-191, 743);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 768);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.panelFirstDate);
            this.panelRight.Controls.Add(this.labelNewCont2);
            this.panelRight.Controls.Add(this.verifyBoxNewCont);
            this.panelRight.Controls.Add(this.labelNewCont);
            this.panelRight.Controls.Add(this.pSejutu);
            this.panelRight.Controls.Add(this.vcSejutu);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisitKasan);
            this.panelRight.Controls.Add(this.verifyBoxHnum);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.label1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelF1);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisit);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.labelF2);
            this.panelRight.Controls.Add(this.labelAppStatus);
            this.panelRight.Controls.Add(this.labelF3);
            this.panelRight.Controls.Add(this.labelF4);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.labelF5);
            this.panelRight.Controls.Add(this.labelHarikyuFushoName);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 768);
            this.panelRight.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(979, 625);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 103;
            this.label3.Text = "円";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(4, 659);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 74);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 29;
            this.dgv.TabStop = false;
            // 
            // panelFirstDate
            // 
            this.panelFirstDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelFirstDate.Controls.Add(this.verifyBoxF1Finish);
            this.panelFirstDate.Controls.Add(this.label2);
            this.panelFirstDate.Controls.Add(this.verifyBoxF1Start);
            this.panelFirstDate.Controls.Add(this.label9);
            this.panelFirstDate.Controls.Add(this.label7);
            this.panelFirstDate.Controls.Add(this.verifyBoxF1FirstD);
            this.panelFirstDate.Controls.Add(this.label5);
            this.panelFirstDate.Controls.Add(this.verifyBoxF1FirstY);
            this.panelFirstDate.Controls.Add(this.label6);
            this.panelFirstDate.Controls.Add(this.verifyBoxF1FirstM);
            this.panelFirstDate.Controls.Add(this.label14);
            this.panelFirstDate.Controls.Add(this.label11);
            this.panelFirstDate.Controls.Add(this.label13);
            this.panelFirstDate.Controls.Add(this.label12);
            this.panelFirstDate.Controls.Add(this.verifyBoxF1FirstE);
            this.panelFirstDate.Location = new System.Drawing.Point(6, 596);
            this.panelFirstDate.Name = "panelFirstDate";
            this.panelFirstDate.Size = new System.Drawing.Size(330, 55);
            this.panelFirstDate.TabIndex = 20;
            this.panelFirstDate.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(274, 22);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 56;
            this.verifyBoxF1Finish.TextV = "";
            this.verifyBoxF1Finish.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(31, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 36);
            this.label2.TabIndex = 61;
            this.label2.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(216, 22);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 53;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(272, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 55;
            this.label9.Text = "終了日";
            this.label9.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(214, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 52;
            this.label7.Text = "開始日";
            this.label7.Visible = false;
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(155, 22);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstD.TabIndex = 50;
            this.verifyBoxF1FirstD.TextV = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(302, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 57;
            this.label5.Text = "日";
            this.label5.Visible = false;
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(65, 22);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 46;
            this.verifyBoxF1FirstY.TextV = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 45;
            this.label6.Text = "初検日";
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(110, 22);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 48;
            this.verifyBoxF1FirstM.TextV = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(244, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 54;
            this.label14.Text = "日";
            this.label14.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 47;
            this.label11.Text = "年";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(183, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 51;
            this.label13.Text = "日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(138, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 49;
            this.label12.Text = "月";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(4, 22);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 44;
            this.verifyBoxF1FirstE.TextV = "";
            // 
            // labelNewCont2
            // 
            this.labelNewCont2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNewCont2.AutoSize = true;
            this.labelNewCont2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelNewCont2.Location = new System.Drawing.Point(338, 617);
            this.labelNewCont2.Name = "labelNewCont2";
            this.labelNewCont2.Size = new System.Drawing.Size(41, 24);
            this.labelNewCont2.TabIndex = 102;
            this.labelNewCont2.Text = "新規：1\r\n継続：2";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(380, 618);
            this.verifyBoxNewCont.MaxLength = 1;
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(30, 23);
            this.verifyBoxNewCont.TabIndex = 22;
            this.verifyBoxNewCont.TextV = "";
            // 
            // labelNewCont
            // 
            this.labelNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNewCont.AutoSize = true;
            this.labelNewCont.Location = new System.Drawing.Point(369, 603);
            this.labelNewCont.Name = "labelNewCont";
            this.labelNewCont.Size = new System.Drawing.Size(53, 12);
            this.labelNewCont.TabIndex = 101;
            this.labelNewCont.Text = "新規継続";
            // 
            // pSejutu
            // 
            this.pSejutu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pSejutu.Controls.Add(this.vbSejutuG);
            this.pSejutu.Controls.Add(this.label15);
            this.pSejutu.Controls.Add(this.lblSejutu);
            this.pSejutu.Controls.Add(this.lblSejutuM);
            this.pSejutu.Controls.Add(this.lblSejutuY);
            this.pSejutu.Controls.Add(this.vbSejutuM);
            this.pSejutu.Controls.Add(this.vbSejutuY);
            this.pSejutu.Enabled = false;
            this.pSejutu.Location = new System.Drawing.Point(709, 596);
            this.pSejutu.Name = "pSejutu";
            this.pSejutu.Size = new System.Drawing.Size(175, 52);
            this.pSejutu.TabIndex = 31;
            this.pSejutu.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // vbSejutuG
            // 
            this.vbSejutuG.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuG.Location = new System.Drawing.Point(10, 21);
            this.vbSejutuG.MaxLength = 2;
            this.vbSejutuG.Name = "vbSejutuG";
            this.vbSejutuG.NewLine = false;
            this.vbSejutuG.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuG.TabIndex = 51;
            this.vbSejutuG.TextV = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(41, 20);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 24);
            this.label15.TabIndex = 83;
            this.label15.Text = "4:平\r\n5:令";
            // 
            // lblSejutu
            // 
            this.lblSejutu.AutoSize = true;
            this.lblSejutu.Enabled = false;
            this.lblSejutu.Location = new System.Drawing.Point(7, 7);
            this.lblSejutu.Name = "lblSejutu";
            this.lblSejutu.Size = new System.Drawing.Size(53, 12);
            this.lblSejutu.TabIndex = 39;
            this.lblSejutu.Text = "前回支給";
            // 
            // lblSejutuM
            // 
            this.lblSejutuM.AutoSize = true;
            this.lblSejutuM.Location = new System.Drawing.Point(150, 30);
            this.lblSejutuM.Name = "lblSejutuM";
            this.lblSejutuM.Size = new System.Drawing.Size(17, 12);
            this.lblSejutuM.TabIndex = 41;
            this.lblSejutuM.Text = "月";
            // 
            // lblSejutuY
            // 
            this.lblSejutuY.AutoSize = true;
            this.lblSejutuY.Location = new System.Drawing.Point(99, 30);
            this.lblSejutuY.Name = "lblSejutuY";
            this.lblSejutuY.Size = new System.Drawing.Size(17, 12);
            this.lblSejutuY.TabIndex = 40;
            this.lblSejutuY.Text = "年";
            // 
            // vbSejutuM
            // 
            this.vbSejutuM.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuM.Location = new System.Drawing.Point(122, 21);
            this.vbSejutuM.MaxLength = 2;
            this.vbSejutuM.Name = "vbSejutuM";
            this.vbSejutuM.NewLine = false;
            this.vbSejutuM.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuM.TabIndex = 53;
            this.vbSejutuM.TextV = "";
            // 
            // vbSejutuY
            // 
            this.vbSejutuY.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuY.Location = new System.Drawing.Point(71, 21);
            this.vbSejutuY.MaxLength = 2;
            this.vbSejutuY.Name = "vbSejutuY";
            this.vbSejutuY.NewLine = false;
            this.vbSejutuY.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuY.TabIndex = 52;
            this.vbSejutuY.TextV = "";
            // 
            // vcSejutu
            // 
            this.vcSejutu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.vcSejutu.BackColor = System.Drawing.SystemColors.Info;
            this.vcSejutu.CheckedV = false;
            this.vcSejutu.Location = new System.Drawing.Point(608, 617);
            this.vcSejutu.Name = "vcSejutu";
            this.vcSejutu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.vcSejutu.Size = new System.Drawing.Size(95, 24);
            this.vcSejutu.TabIndex = 30;
            this.vcSejutu.Text = "交付料あり";
            this.vcSejutu.UseVisualStyleBackColor = false;
            this.vcSejutu.CheckedChanged += new System.EventHandler(this.vcSejutu_CheckedChanged);
            this.vcSejutu.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyCheckBoxVisitKasan
            // 
            this.verifyCheckBoxVisitKasan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisitKasan.CheckedV = false;
            this.verifyCheckBoxVisitKasan.Enabled = false;
            this.verifyCheckBoxVisitKasan.Location = new System.Drawing.Point(518, 617);
            this.verifyCheckBoxVisitKasan.Name = "verifyCheckBoxVisitKasan";
            this.verifyCheckBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisitKasan.Size = new System.Drawing.Size(87, 24);
            this.verifyCheckBoxVisitKasan.TabIndex = 26;
            this.verifyCheckBoxVisitKasan.Text = "加算有り";
            this.verifyCheckBoxVisitKasan.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 24);
            this.label1.TabIndex = 36;
            this.label1.Text = "続紙    : \"--\"     \r\n不要    : \"++\"\r\n";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelInputerName);
            this.panel1.Controls.Add(this.panelMatchWhere);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Controls.Add(this.buttonUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 736);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 32);
            this.panel1.TabIndex = 100;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.AutoSize = true;
            this.labelInputerName.Location = new System.Drawing.Point(3, 4);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(35, 12);
            this.labelInputerName.TabIndex = 35;
            this.labelInputerName.Text = "入力：";
            // 
            // panelMatchWhere
            // 
            this.panelMatchWhere.Controls.Add(this.label4);
            this.panelMatchWhere.Controls.Add(this.checkBoxTotal);
            this.panelMatchWhere.Controls.Add(this.checkBoxNumber);
            this.panelMatchWhere.Location = new System.Drawing.Point(228, 6);
            this.panelMatchWhere.Name = "panelMatchWhere";
            this.panelMatchWhere.Size = new System.Drawing.Size(242, 23);
            this.panelMatchWhere.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "突合候補条件";
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Checked = true;
            this.checkBoxTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTotal.Location = new System.Drawing.Point(160, 4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(72, 16);
            this.checkBoxTotal.TabIndex = 38;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            this.checkBoxTotal.CheckedChanged += new System.EventHandler(this.checkBoxMatchWhere_CheckedChanged);
            // 
            // checkBoxNumber
            // 
            this.checkBoxNumber.AutoSize = true;
            this.checkBoxNumber.Checked = true;
            this.checkBoxNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNumber.Location = new System.Drawing.Point(94, 4);
            this.checkBoxNumber.Name = "checkBoxNumber";
            this.checkBoxNumber.Size = new System.Drawing.Size(60, 16);
            this.checkBoxNumber.TabIndex = 37;
            this.checkBoxNumber.Text = "被保番";
            this.checkBoxNumber.UseVisualStyleBackColor = true;
            this.checkBoxNumber.CheckedChanged += new System.EventHandler(this.checkBoxMatchWhere_CheckedChanged);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(475, 6);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 30;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelF1
            // 
            this.labelF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(6, 532);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(47, 12);
            this.labelF1.TabIndex = 17;
            this.labelF1.Text = "負傷名1";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(204, 546);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF2.TabIndex = 11;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF2.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(407, 546);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF3.TabIndex = 12;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF3.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyCheckBoxVisit
            // 
            this.verifyCheckBoxVisit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisit.CheckedV = false;
            this.verifyCheckBoxVisit.Location = new System.Drawing.Point(428, 617);
            this.verifyCheckBoxVisit.Margin = new System.Windows.Forms.Padding(5, 3, 0, 0);
            this.verifyCheckBoxVisit.Name = "verifyCheckBoxVisit";
            this.verifyCheckBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisit.Size = new System.Drawing.Size(87, 25);
            this.verifyCheckBoxVisit.TabIndex = 25;
            this.verifyCheckBoxVisit.Text = "往療あり";
            this.verifyCheckBoxVisit.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisit.CheckedChanged += new System.EventHandler(this.verifyCheckBoxVisit_CheckedChanged);
            this.verifyCheckBoxVisit.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(1, 546);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF1.TabIndex = 10;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 63);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 100);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 462);
            this.scrollPictureControl1.TabIndex = 28;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(610, 546);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF4.TabIndex = 13;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF4.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(812, 546);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(200, 23);
            this.verifyBoxF5.TabIndex = 14;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelF2
            // 
            this.labelF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF2.AutoSize = true;
            this.labelF2.Location = new System.Drawing.Point(207, 532);
            this.labelF2.Name = "labelF2";
            this.labelF2.Size = new System.Drawing.Size(47, 12);
            this.labelF2.TabIndex = 19;
            this.labelF2.Text = "負傷名2";
            // 
            // labelF3
            // 
            this.labelF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF3.AutoSize = true;
            this.labelF3.Location = new System.Drawing.Point(410, 532);
            this.labelF3.Name = "labelF3";
            this.labelF3.Size = new System.Drawing.Size(47, 12);
            this.labelF3.TabIndex = 21;
            this.labelF3.Text = "負傷名3";
            // 
            // labelF4
            // 
            this.labelF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF4.AutoSize = true;
            this.labelF4.Location = new System.Drawing.Point(613, 532);
            this.labelF4.Name = "labelF4";
            this.labelF4.Size = new System.Drawing.Size(47, 12);
            this.labelF4.TabIndex = 23;
            this.labelF4.Text = "負傷名4";
            // 
            // labelF5
            // 
            this.labelF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelF5.AutoSize = true;
            this.labelF5.Location = new System.Drawing.Point(815, 532);
            this.labelF5.Name = "labelF5";
            this.labelF5.Size = new System.Drawing.Size(47, 12);
            this.labelF5.TabIndex = 25;
            this.labelF5.Text = "負傷名5";
            // 
            // labelHarikyuFushoName
            // 
            this.labelHarikyuFushoName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHarikyuFushoName.AutoSize = true;
            this.labelHarikyuFushoName.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelHarikyuFushoName.Location = new System.Drawing.Point(9, 572);
            this.labelHarikyuFushoName.Name = "labelHarikyuFushoName";
            this.labelHarikyuFushoName.Size = new System.Drawing.Size(484, 12);
            this.labelHarikyuFushoName.TabIndex = 27;
            this.labelHarikyuFushoName.Text = "鍼灸時：　　1.神経痛　2.リウマチ　3.頚腕症候群　4.五十肩　5.腰痛症　6.頸椎捻挫後遺症　7.その他";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 768);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 768);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 760);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelFirstDate.ResumeLayout(false);
            this.panelFirstDate.PerformLayout();
            this.pSejutu.ResumeLayout(false);
            this.pSejutu.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelMatchWhere.ResumeLayout(false);
            this.panelMatchWhere.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label labelHarikyuFushoName;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelAppStatus;
        private System.Windows.Forms.DataGridView dgv;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label labelF5;
        private System.Windows.Forms.Label labelF4;
        private System.Windows.Forms.Label labelF3;
        private System.Windows.Forms.Label labelF2;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyCheckBox verifyCheckBoxVisit;
        private System.Windows.Forms.Panel panelMatchWhere;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.CheckBox checkBoxNumber;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label1;
        private VerifyCheckBox verifyCheckBoxVisitKasan;
        private System.Windows.Forms.Panel pSejutu;
        private VerifyBox vbSejutuG;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSejutu;
        private System.Windows.Forms.Label lblSejutuM;
        private System.Windows.Forms.Label lblSejutuY;
        private VerifyBox vbSejutuM;
        private VerifyBox vbSejutuY;
        private VerifyCheckBox vcSejutu;
        private System.Windows.Forms.Panel panelFirstDate;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxF1Start;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxF1FirstD;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxF1FirstM;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxF1FirstE;
        private System.Windows.Forms.Label labelNewCont2;
        private VerifyBox verifyBoxNewCont;
        private System.Windows.Forms.Label labelNewCont;
        private System.Windows.Forms.Label label3;
    }
}