﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.KagoshimaKoiki
{
    public partial class frmImport : Form
    {
        
        public string strFileClinicMaster { get; set; } = string.Empty;
        public string strFilePunch { get; set; } = string.Empty;
        public string strFileHihoMaster { get; set; } = string.Empty;
        public string strFileSikyuData { get; set; } = string.Empty;

        private static int _cym;

        public frmImport(int cym)
        {
            InitializeComponent();
            _cym = cym;

        }
        
        

        private void btnFile1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "申請書パンチデータ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;
            textBoxPunch.Text= ofd.FileName;
            
        }

      

        private void btnFile3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "施術所マスタ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxClinicMaster.Text = ofd.FileName;
        }

      
        private void btnFileHarikyuMaster_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xls";
            ofd.FilterIndex = 0;
            ofd.Title = "施術所マスタ_鍼灸";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            //textBoxHariMaster.Text = ofd.FileName;
        }





        private void btnImp_Click(object sender, EventArgs e)
        {
            strFileClinicMaster = textBoxClinicMaster.Text.Trim();
            strFilePunch = textBoxPunch.Text.Trim();
            strFileHihoMaster = textBoxHihoMaster.Text.Trim();
            strFileSikyuData = textBoxSikyu.Text.Trim();
            this.Close();
        }

        private void btnFileHihoMaster_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "被保険者台帳csv";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxHihoMaster.Text = ofd.FileName;
        }

        private void btnFileSikyuData_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "療養費支給決定データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxSikyu.Text = ofd.FileName;
        }
    }
}
