﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.KagoshimaKoiki
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsHihoMaster;
        private InputMode inputMode;
        protected override Control inputPanel => panelRight;
        int cym;
        
        private bool firstTime = false;

        //過去に入力したapp
        List<App> lstPastData = new List<App>();

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(80, 30); //(800, 60);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1200);
        Point posTotal = new Point(350, 1200); //(800, 1600);
        Point posBui = new Point(80, 30); //(80, 750);

        #region コンストラクタ

        public InputForm(InputMode mode, ScanGroup sGroup,bool firsttime, int aid = 0)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode == InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");


            this.firstTime = firsttime;

            panelInfo.Visible = true;
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;

            this.scanGroup = sGroup;
            var list = new List<App>();
            list = App.GetAppsGID(scanGroup.GroupID);

            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));

            //Appリスト
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            

            //各グリッド表示調整
            initializeGridView();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            
            var app = (App)bsApp.Current;
            cym = app.CYM;
            LoadPastData();//過去1ヶ月データロード

            if (app != null) setApp(app);


        }

        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            initializeGridView();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }


        #endregion


        /// <summary>
        /// 過去1ヶ月前入力データロード
        /// </summary>
        private void LoadPastData()
        {
        
            int Past1Month = DateTimeEx.Int6YmAddMonth(cym, -1);
            lstPastData = App.GetApps(Past1Month);
            lstPastData.Sort((x, y) => x.HihoNum.CompareTo(y.HihoNum));

        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                    MatchingApp.GetNotMatchCsv(cym); //20220909_1 ito st /////マッチングチェック追加
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;
            bsApp.ResetBindings(false);

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            focusBack(false);
        }


        /// <summary>
        /// 提供データグリッド初期化
        /// </summary>
        private void InitGrid()
        {
            //広域データ欄
            bsHihoMaster = new BindingSource();
            bsHihoMaster.DataSource = new List<imp_punch>();
            dgv.DataSource = bsHihoMaster;

            foreach (DataGridViewColumn c in dgv.Columns) c.Visible = false;
            dgv.Columns[nameof(imp_punch.f000_importid)].Visible = true;
            dgv.Columns[nameof(imp_punch.f001_chargeym)].Visible = false;
            dgv.Columns[nameof(imp_punch.f002_hihonum)].Visible = true;
            dgv.Columns[nameof(imp_punch.f003_rezekind)].Visible = true;
            dgv.Columns[nameof(imp_punch.f004_mediym)].Visible = true;
            dgv.Columns[nameof(imp_punch.f005_insnum)].Visible = false;
            dgv.Columns[nameof(imp_punch.f006_gender)].Visible = true;
            dgv.Columns[nameof(imp_punch.f007_birthdayw)].Visible = false;
            dgv.Columns[nameof(imp_punch.f008_counteddays)].Visible = true;
            dgv.Columns[nameof(imp_punch.f009_ratio)].Visible = true;
            dgv.Columns[nameof(imp_punch.f010_chargescore)].Visible = false;
            dgv.Columns[nameof(imp_punch.f011_decidescore)].Visible = false;
            dgv.Columns[nameof(imp_punch.f012_partial)].Visible = false;
            dgv.Columns[nameof(imp_punch.f013_total)].Visible = true;
            dgv.Columns[nameof(imp_punch.f014_futan)].Visible = true;
            dgv.Columns[nameof(imp_punch.f015_futan2)].Visible = true;
            dgv.Columns[nameof(imp_punch.f016_zikofutan)].Visible = false;
            dgv.Columns[nameof(imp_punch.f017_kanrinum)].Visible = true;
            dgv.Columns[nameof(imp_punch.f018_clinicnum)].Visible = true;
            dgv.Columns[nameof(imp_punch.f019_clinicname)].Visible = true;
            dgv.Columns[nameof(imp_punch.cym)].Visible = false;
            dgv.Columns[nameof(imp_punch.mediymad)].Visible = true;
            dgv.Columns[nameof(imp_punch.birthdayad)].Visible = true;


            dgv.Columns[nameof(imp_punch.f000_importid)].Width = 60;
            dgv.Columns[nameof(imp_punch.f001_chargeym)].Width = 60;
            dgv.Columns[nameof(imp_punch.f002_hihonum)].Width = 60;
            dgv.Columns[nameof(imp_punch.f003_rezekind)].Width = 60;
            dgv.Columns[nameof(imp_punch.f004_mediym)].Width = 60;
            dgv.Columns[nameof(imp_punch.f005_insnum)].Width = 60;
            dgv.Columns[nameof(imp_punch.f006_gender)].Width = 40;
            dgv.Columns[nameof(imp_punch.f007_birthdayw)].Width = 60;
            dgv.Columns[nameof(imp_punch.f008_counteddays)].Width = 60;
            dgv.Columns[nameof(imp_punch.f009_ratio)].Width = 60;
            dgv.Columns[nameof(imp_punch.f010_chargescore)].Width = 60;
            dgv.Columns[nameof(imp_punch.f011_decidescore)].Width = 60;
            dgv.Columns[nameof(imp_punch.f012_partial)].Width = 60;
            dgv.Columns[nameof(imp_punch.f013_total)].Width = 70;
            dgv.Columns[nameof(imp_punch.f014_futan)].Width = 60;
            dgv.Columns[nameof(imp_punch.f015_futan2)].Width = 60;
            dgv.Columns[nameof(imp_punch.f016_zikofutan)].Width = 60;
            dgv.Columns[nameof(imp_punch.f017_kanrinum)].Width = 100;
            dgv.Columns[nameof(imp_punch.f018_clinicnum)].Width = 80;
            dgv.Columns[nameof(imp_punch.f019_clinicname)].Width = 100;
            dgv.Columns[nameof(imp_punch.cym)].Width = 60;
            dgv.Columns[nameof(imp_punch.mediymad)].Width = 80;
            dgv.Columns[nameof(imp_punch.birthdayad)].Width = 100;

            dgv.Columns[nameof(imp_punch.f000_importid)].HeaderText = "インポートID";
            dgv.Columns[nameof(imp_punch.f001_chargeym)].HeaderText = "請求年月";
            dgv.Columns[nameof(imp_punch.f002_hihonum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(imp_punch.f003_rezekind)].HeaderText = "レセプト種類";
            dgv.Columns[nameof(imp_punch.f004_mediym)].HeaderText = "診療年月";
            dgv.Columns[nameof(imp_punch.f005_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(imp_punch.f006_gender)].HeaderText = "性別";
            dgv.Columns[nameof(imp_punch.f007_birthdayw)].HeaderText = "生年月日和暦";
            dgv.Columns[nameof(imp_punch.f008_counteddays)].HeaderText = "診療実日数";
            dgv.Columns[nameof(imp_punch.f009_ratio)].HeaderText = "給付割合%";
            dgv.Columns[nameof(imp_punch.f010_chargescore)].HeaderText = "請求点数";
            dgv.Columns[nameof(imp_punch.f011_decidescore)].HeaderText = "決定点数";
            dgv.Columns[nameof(imp_punch.f012_partial)].HeaderText = "一部負担額";
            dgv.Columns[nameof(imp_punch.f013_total)].HeaderText = "費用金額";
            dgv.Columns[nameof(imp_punch.f014_futan)].HeaderText = "保険者負担額";
            dgv.Columns[nameof(imp_punch.f015_futan2)].HeaderText = "一部負担相当額";
            dgv.Columns[nameof(imp_punch.f016_zikofutan)].HeaderText = "自己負担額";
            dgv.Columns[nameof(imp_punch.f017_kanrinum)].HeaderText = "電算管理番号";
            dgv.Columns[nameof(imp_punch.f018_clinicnum)].HeaderText = "医療機関番号";
            dgv.Columns[nameof(imp_punch.f019_clinicname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(imp_punch.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(imp_punch.mediymad)].HeaderText = "施術年月西暦";
            dgv.Columns[nameof(imp_punch.birthdayad)].HeaderText = "生年月日西暦";


        }

        /// <summary>
        /// 左グリッド
        /// </summary>
        private void initializeGridView()
        {
            foreach (DataGridViewColumn c in dataGridViewPlist.Columns) c.Visible = false;
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

         

            //pgupで選択行が変わるのを防ぐ
            dgv.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            //if (inputMode == InputMode.MatchCheck) scanGroup = ScanGroup.Select(app.GroupID); //20220908_2 ito st /////マッチングチェック時用

            if (app == null)
            {
                clearApp();
                return;
            }

            setApp(app);
            if (app.StatusFlagCheck(StatusFlag.自動マッチ済)) verifyBoxNewCont.Focus();
            else verifyBoxY.Focus();
        }
        
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }

            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            //選択中参照レセとAIDが違う場合は変更がなくてもUpdate
            //var app = (App)bsApp.Current;
            //var rr = (imp_hihomaster)bsHihoMaster.Current;
            //if (app != null && rr != null && rr.AID != app.Aid) setDataChanged(true);

            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            
            createGrid(app);
            
        }



        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid(App app)
        {

            List<imp_punch> lstimp = new List<imp_punch>();
            List<imp_punch> lstImport = new List<imp_punch>();

            string strhnum = verifyBoxHnum.Text.Trim();

            lstImport = imp_punch.select(app.CYM, strhnum);
            if (lstImport == null) return;

            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

            foreach (imp_punch item in lstImport)
            {
                if (item.f002_hihonum == strhnum &&
                    item.f013_total == intTotal.ToString() &&
                    item.mediymad == intymad &&
                    //20220510180929 furukawa st ////////////////////////
                    //元ファイルのchrgeymと合致しないと意図したレコードが取れない
                    
                    item.f001_chargeym == app.CYM.ToString()
                    //item.cym == app.CYM
                    //20220510180929 furukawa ed ////////////////////////

                    )
                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;

            //0件の場合グリッド作らない
            if (lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
                return;
            }

            InitGrid();

            dgv.DataSource = lstimp;

         

            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();


            if (app != null && app.RrID.ToString() != string.Empty && app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    string strImportID = r.Cells[nameof(imp_punch.f000_importid)].Value.ToString();
                    if (strImportID == Application_AUX.Select(app.Aid).matchingID01)
                    {
                        //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない                            
                        if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                        else r.Selected = true;
                    }
                    //提供データIDと違う場合の処理抜け                        
                    else
                    {
                        r.Selected = false;
                    }
                }
            }




            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }


            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        /// <summary>
        /// あはき専用項目
        /// </summary>
        /// <param name="app"></param>
        private bool checkAppAHK(App app)
        {
         
            //交付あり
            bool flgKofu = vcSejutu.Checked;

            //前回交付年月
            string PastKofu = DateTimeEx.GetAdYearMonthFromJyymm(vbSejutuG.GetIntValue()*10000+ vbSejutuY.GetInt100Value()+vbSejutuM.GetIntValue()).ToString();


            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }


            
            //交付あり
            app.TaggedDatas.flgKofuUmu = flgKofu;

            //前回交付
            app.TaggedDatas.PastSupplyYM = PastKofu;

            return true;
        }


        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            string strHihonum = verifyBoxHnum.Text.Trim();
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out long hnumTemp));

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //新規・継続の抜けチェック
            int newCont = verifyBoxNewCont.GetIntValue();
            if (scan.AppType == APP_TYPE.柔整)
            {
                setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);
            }


            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //往療料
            bool visit = verifyCheckBoxVisit.Checked;

            //加算あり
            bool visitkasan = verifyCheckBoxVisitKasan.Checked;


            //初検日
            DateTime dtFirst1 = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            setStatus(verifyBoxF1FirstY, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstM, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstD, dtFirst1 == DateTime.MinValue);
            setStatus(verifyBoxF1FirstE, dtFirst1 == DateTime.MinValue);


            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                if(!checkAppAHK(app)) return false;
            }


            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = strHihonum.PadLeft(8,'0');
            app.Total = total;
            app.Distance = visit ? 999 : 0;            
            app.VisitAdd = visitkasan ? 999 : 0;

            //申請書タイプ
            app.AppType = scan.AppType;


            //新規・継続
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //初検日
            app.FushoFirstDate1 = dtFirst1;


            //実日数
            //app.CountedDays = intCountedDays;

            //申請書パンチデータ取得
            if (!setSupplyData(app)) return false;
            

            return true;
        }

        /// <summary>
        /// 提供データ取得
        /// </summary>
        /// <param name="app"></param>
        private bool setSupplyData(App app)
        {

            //申請書パンチデータ
            imp_punch imp_punch = (imp_punch)dgv.CurrentRow.DataBoundItem;
            app.InsNum = imp_punch.f005_insnum;

            if (!int.TryParse(imp_punch.f009_ratio, out int tmpratio)) app.Ratio = 0; else app.Ratio = tmpratio / 10;
            if (!int.TryParse(imp_punch.f013_total, out int tmptotal)) app.Total = 0; else app.Total = tmptotal;
            if (!int.TryParse(imp_punch.f016_zikofutan, out int tmppartial)) app.Partial = 0; else app.Partial = tmppartial;
            if (!int.TryParse(imp_punch.f014_futan, out int tmpcharge)) app.Charge = 0; else app.Charge = tmpcharge;
            if (!int.TryParse(imp_punch.f008_counteddays, out int tmpCountedDays)) app.CountedDays = 0; else app.CountedDays = tmpCountedDays;

            app.ClinicNum = imp_punch.f018_clinicnum;
            app.RrID = imp_punch.f000_importid;
            app.ComNum = imp_punch.f017_kanrinum;

            switch (imp_punch.f003_rezekind)
            {
                case "7":
                    app.AppType = APP_TYPE.柔整;
                    break;
                case "8":
                    app.AppType = APP_TYPE.鍼灸;
                    break;
                case "9":
                    app.AppType = APP_TYPE.あんま;
                    break;
                default:
                    app.AppType = APP_TYPE.NULL;
                    break;
            }


            //施術所マスタ 合致するのは1行の前提 2022/04/28後藤智紀
            List<imp_clinicmaster> lst_clinicmaster = imp_clinicmaster.select(app.CYM, app.ClinicNum);
            if (lst_clinicmaster.Count == 0)
            {
                if (MessageBox.Show($"一致する施術所マスタが見つかりません。このまま登録しますか？",
                    Application.ProductName, MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
            }
            if (lst_clinicmaster.Count == 1)
            {
                app.ClinicName = lst_clinicmaster[0].f004_clinicname;
                app.ClinicZip = lst_clinicmaster[0].f005_cliniczip;
                app.ClinicAdd = lst_clinicmaster[0].f006_clinicadd + lst_clinicmaster[0].f007_clinicbanch + lst_clinicmaster[0].f008_clinickatagaki;
                app.ClinicTel = lst_clinicmaster[0].f009_clinictel;
                app.TaggedDatas.GeneralString6 = lst_clinicmaster[0].f000_importid.ToString();
            }

            //被保険者マスタ 合致するのは1行の前提 2022/04/28後藤智紀
            List<imp_hihomaster> lst_hihomaster = imp_hihomaster.select(app.CYM, app.HihoNum);
            if (lst_hihomaster.Count == 0)
            {
                MessageBox.Show($"一致する被保険者マスタが見つかりません。登録できません",
                    Application.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return false;
            }
            if (lst_hihomaster.Count == 1)
            {
                app.PersonName = lst_hihomaster[0].f002_pname;
                if (!int.TryParse(lst_hihomaster[0].f005_pgender, out int tmpgender)) app.Sex = 0; else app.Sex = tmpgender;                
                app.Birthday = lst_hihomaster[0].birthdayad;
                app.TaggedDatas.DestZip = lst_hihomaster[0].f006_sendzip;
                app.TaggedDatas.DestAdd = lst_hihomaster[0].f007_sendadd;
                app.TaggedDatas.Kana = lst_hihomaster[0].f003_pkana;
                app.TaggedDatas.GeneralString3 = lst_hihomaster[0].f008_futankbn1;
                app.TaggedDatas.GeneralString7 = lst_hihomaster[0].f000_importid.ToString();
            }



            //20220428100436 furukawa st ////////////////////////
            //支給決定データ使用しない

            ////支給決定データ
            //List<imp_chargedata> lst_chargedata = imp_chargedata.select(app.CYM, app.HihoNum, app.Total,app.YM);
            //if (lst_chargedata.Count == 0)
            //{
            //    if (MessageBox.Show($"一致する支給決定データが見つかりません。このまま登録しますか？",
            //        Application.ProductName, MessageBoxButtons.YesNo,
            //        MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return false;
            //}
            //if (lst_chargedata.Count == 1)
            //{
            //    app.TaggedDatas.GeneralString1 = lst_chargedata[0].f023_atenanum;
            //    app.TaggedDatas.GeneralString2 = lst_chargedata[0].f017_tuchinum;
            //    app.TaggedDatas.GeneralString8 = lst_chargedata[0].f000_importid.ToString();
            //}

            //20220428100436 furukawa ed ////////////////////////

            return true;

        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            switch (verifyBoxY.Text) 
            {
                case "--":
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;
                case "++":
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;
                case "..":
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.AppType = APP_TYPE.長期;
                    break;
                case "**":
                    //エラー
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                    break;

                case "901":
                    //施術同意書
                    resetInputData(app);
                    hasError = false;
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                //    DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);
               //     app.TaggedDatas.DouiDate = dtDoui;


                    break;

                case "902":
                    //施術同意書裏
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case "911":
                    //施術報告書
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case "921":
                    //状態記入書
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;


                default:
                    //申請書の場合
                    if (scan.AppType == APP_TYPE.柔整)
                    {
                        if (dgv.CurrentCell == null)
                        {
                            //20220511094541 furukawa st ////////////////////////
                            //メッセージ変更
                            
                            if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;

                            //      MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                            //          "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                            //          "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //      return false;

                            //20220511094541 furukawa ed ////////////////////////
                        }

                        var oldRridStr = app.Numbering;
                    }

                    if (!checkApp(app))
                    {
                        focusBack(true);
                        return false;
                    }
                    break;
            }

            //ベリファイチェック あはきのみベリファイ
            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                if (!firstTime && !checkVerify()) return false;
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {

                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; // 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran
                    ,app.RrID.ToString()                       //パンチデータID
                    ,app.TaggedDatas.GeneralString6//施術所マスタID
                    ,app.TaggedDatas.GeneralString7//被保険者マスタID
                    ,app.TaggedDatas.GeneralString8)//支給決定データID
                    ) return false;



                jyuTran.Commit();
                tran.Commit();
            }

            return true;
        }

        /// <summary>
        /// Appを表示します
        /// </summary>
        private void setApp(App app)
        {
            if (inputMode == InputMode.MatchCheck) scanGroup = ScanGroup.Select(app.GroupID); //20220908_2 ito st /////マッチングチェック時用

            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            InitControl();

            //App_Flagのチェック
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }
            else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }

            ////提供データは柔整のみ
            //if (scan.AppType == APP_TYPE.柔整)
            //{
            //    //広域データ表示
            //    selectRefRece(app);
            //}
            createGrid(app);

            
            changedReset(app);
        }

        private void clearApp()
        {
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            iVerifiableAllClear(panelRight);
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //20220908_2 ito st /////マッチングチェック時用
            if (bsHihoMaster != null)
            {
                bsHihoMaster.Clear();
                bsHihoMaster.ResetBindings(false);
            }
            //bsHihoMaster.Clear();
            //bsHihoMaster.ResetBindings(false);
            //20220908_2 ito st /////マッチングチェック時用
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //メイン画像の表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                var ocr = app.OcrData.Split(',');
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (string.IsNullOrEmpty(appsg.note2))
                    {
                        //OCRデータがあれば、部位のみ挿入
                        if (!string.IsNullOrWhiteSpace(app.OcrData))
                        {
                            verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                            verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                            verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                            verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                            verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                        }

                        //新規/継続
                        if (ocr.Length > 127)
                        {
                            if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                            if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                        }
                    }
                }
                catch
                {
                    //握り潰し問題なし
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            try
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxHnum.Text = app.HihoNum;
                verifyBoxTotal.Text = app.Total.ToString();

                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (string.IsNullOrEmpty(appsg.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }

                    //新規/継続
                    if (ocr.Length > 127)
                    {
                        if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                        if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                    }
                }
            }
            catch
            {
                //握り潰し問題なし
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            switch (app.MediYear) 
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    //続紙                    
                    setValue(verifyBoxY,"--", firstTime, nv);
                    break;
                case (int)APP_SPECIAL_CODE.不要:
                    //その他
                    setValue(verifyBoxY, "++", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.長期:
                    setValue(verifyBoxY, "..", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.エラー:
                    //エラー
                    setValue(verifyBoxY, "**", firstTime, nv);                    
                    break;
                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, "901", firstTime, nv);

           //         setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    break;
                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, "902", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, "911", firstTime, nv);
                    
                    break;
                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, "921", firstTime, nv);
                    
                    break;
                default:

                    //申請書年月
                    verifyBoxY.Text = app.MediYear.ToString();
                    verifyBoxM.Text = app.MediMonth.ToString();

                    //被保険者番号
                    verifyBoxHnum.Text = app.HihoNum;

                    //合計金額
                    verifyBoxTotal.Text = app.Total.ToString();

                    verifyBoxF1.Text = app.FushoName1;
                    verifyBoxF2.Text = app.FushoName2;
                    verifyBoxF3.Text = app.FushoName3;
                    verifyBoxF4.Text = app.FushoName4;
                    verifyBoxF5.Text = app.FushoName5;


                    //往療料
                    setValue(verifyCheckBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);

                    //加算あり                                                
                    setValue(verifyCheckBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);


                    //初検日
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);


                    if (scan.AppType == APP_TYPE.柔整)
                    {
                       

                        //新規/継続
                        verifyBoxNewCont.Text =
                            app.NewContType == NEW_CONT.新規 ? "1" :
                            app.NewContType == NEW_CONT.継続 ? "2" : "";

                        //往療
                        verifyCheckBoxVisit.Checked = app.Distance == 999;
  
                    }

                    
                    else if(scan.AppType==APP_TYPE.あんま||scan.AppType==APP_TYPE.鍼灸)
                    {

                        //前回交付
                        setValue(vcSejutu, app.TaggedDatas.flgKofuUmu, firstTime, nv);


                        //前回交付年月
                        if (int.TryParse(app.TaggedDatas.PastSupplyYM, out int intPast) && intPast > 0)
                        {

                            int intSejutuG = int.Parse(DateTimeEx.GetEraNumberYearFromYYYYMM(intPast).ToString().Substring(0, 1));
                            int intSejutuY = int.Parse(DateTimeEx.GetEraNumberYearFromYYYYMM(intPast).ToString().Substring(1, 2));
                            int intSejutuM = int.Parse(intPast.ToString().Substring(4, 2));
                            setValue(vbSejutuG, intSejutuG, firstTime, nv);
                            setValue(vbSejutuY, intSejutuY, firstTime, nv);
                            setValue(vbSejutuM, intSejutuM, firstTime, nv);
                        }
                    }
                    break;
            }

        }


        /// <summary>
        /// コントロール使用可否
        /// </summary>
        private void InitControl()
        {

            #region 初期化
         
            panelFirstDate.Visible = false;
      
            verifyBoxF1.Visible = false;
            verifyBoxF2.Visible = false;
            verifyBoxF3.Visible = false;
            verifyBoxF4.Visible = false;
            verifyBoxF5.Visible = false;
            labelF1.Visible = false;
            labelF2.Visible = false;
            labelF3.Visible = false;
            labelF4.Visible = false;
            labelF5.Visible = false;
            labelHarikyuFushoName.Visible = false;

            verifyCheckBoxVisit.Visible = false;
            verifyCheckBoxVisitKasan.Visible = false;
            verifyCheckBoxVisitKasan.Enabled = false;

            verifyBoxTotal.Visible = false;
            labelTotal.Visible = false;

          
            verifyBoxHnum.Visible = false;
            verifyBoxM.Visible = false;
            verifyBoxNewCont.Visible = false;

            labelHnum.Visible = false;
            labelM.Visible = false;
            labelNewCont.Visible = false;
            labelNewCont2.Visible = false;

            dgv.Visible = false;

            labelAppStatus.Visible = false;

          
            pSejutu.Visible = false;
            pSejutu.Enabled = false;
            vcSejutu.Visible = false;

            #endregion


            //続紙、白バッジ、その他の場合、入力なし
            if (verifyBoxY.Text == clsInputKind.続紙 ||
                verifyBoxY.Text == clsInputKind.不要 ||
                verifyBoxY.Text == clsInputKind.長期 ||
                verifyBoxY.Text == clsInputKind.エラー ||
                verifyBoxY.Text == clsInputKind.施術同意書裏 ||
                verifyBoxY.Text == clsInputKind.施術報告書 ||
                verifyBoxY.Text == clsInputKind.状態記入書) return;
            
            //同意書

            if (verifyBoxY.Text == clsInputKind.施術同意書)
            {
                //pDoui.Visible = true;
                return;
            }

            //申請書の場合

            if (scan.AppType == APP_TYPE.柔整)
            {
                verifyBoxNewCont.Visible = true;
                labelNewCont.Visible = true;
                labelNewCont2.Visible = true;
            }
            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                labelHarikyuFushoName.Visible = true;

                pSejutu.Visible = true;
                //有効にしないとベリファイテキストに値が入らない                
                //if (!firstTime) pSejutu.Enabled = true;

                vcSejutu.Visible = true;
            }



            //柔整あはき共通
            verifyCheckBoxVisitKasan.Visible = true;
            //有効にしないとベリファイテキストに値が入らない                
            //if (!firstTime) verifyCheckBoxVisitKasan.Enabled = true;

            dgv.Visible = true;

            panelFirstDate.Visible = true;

            verifyBoxF1.Visible = true;
            verifyBoxF2.Visible = true;
            verifyBoxF3.Visible = true;
            verifyBoxF4.Visible = true;
            verifyBoxF5.Visible = true;
            labelF1.Visible = true;
            labelF2.Visible = true;
            labelF3.Visible = true;
            labelF4.Visible = true;
            labelF5.Visible = true;
            
            verifyCheckBoxVisit.Visible = true;
            
            verifyBoxTotal.Visible = true;
            labelTotal.Visible = true;

           
            verifyBoxHnum.Visible = true;
            verifyBoxM.Visible = true;
                

            labelHnum.Visible = true;
            labelM.Visible = true;
                

                

            labelAppStatus.Visible = true;

            buiTabStopAdjust();
           
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            InitControl();  
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;
            else if (sender == verifyBoxTotal) p = posTotal;
            else if (sender == verifyBoxNewCont) p = posHnum;
            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;
            else if (sender == verifyCheckBoxVisit) p = posVisit;

         
            else if (sender == panelFirstDate) p = posHnum;
            
            else if(sender==vcSejutu) p = posVisit;//前回交付
            else if(sender==pSejutu) p = posVisit;

            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (verifyBoxY.Focused == true) posYM = pos;
            else if (verifyBoxM.Focused == true) posYM = pos;
            else if (verifyBoxHnum.Focused == true) posHnum = pos;
            else if (verifyBoxNewCont.Focused == true) posNew = pos;
            else if (verifyBoxTotal.Focused == true) posTotal = pos;
            else if (verifyBoxF1 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF2 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF3 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF4 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF5 == this.ActiveControl) posBui = pos;
        }

        private void checkBoxMatchWhere_CheckedChanged(object sender, EventArgs e)
        {
            //var app = (App)bsApp.Current;
            //selectRefRece(app);
        }

        private void verifyCheckBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            verifyCheckBoxVisitKasan.Enabled = verifyCheckBoxVisit.Checked;
        }


        private void vcSejutu_CheckedChanged(object sender, EventArgs e)
        {
            pSejutu.Enabled = vcSejutu.Checked;
        }

        private void verifyBoxHnum_Leave(object sender, EventArgs e)
        {
            verifyBoxHnum.Text = verifyBoxHnum.Text.Trim().PadLeft(8, '0');
        }
    }
}
