﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.KagoshimaKoiki

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName_clinicmaster = frm.strFileClinicMaster;
            string strFileName_punch = frm.strFilePunch;
            string strFileName_hihomaster = frm.strFileHihoMaster;
            string strFileName_sikyu = frm.strFileSikyuData;

            if (strFileName_clinicmaster == string.Empty &&
                strFileName_punch == string.Empty &&
                strFileName_hihomaster == string.Empty &&
                strFileName_sikyu == string.Empty) return;



            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName_punch != string.Empty)
                {
                    wf.LogPrint("提供データ_パンチデータインポート");
                    if (!dataImport_punch(_cym, wf, strFileName_punch))
                    {
                        wf.LogPrint("提供データ_パンチデータインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_パンチデータインポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_パンチデータインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_パンチデータのファイルが指定されていないため処理しない");
                }



                if (strFileName_clinicmaster != string.Empty)
                {
                    wf.LogPrint("提供データ_施術所マスタインポート");
                    if (!dataImport_ClinicMaster(_cym, wf, strFileName_clinicmaster))
                    {
                        wf.LogPrint("提供データ_施術所マスタインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_施術所マスタインポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_施術所マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_施術所マスタのファイルが指定されていないため処理しない");
                }

              

                if (strFileName_hihomaster != string.Empty)
                {
                    wf.LogPrint("提供データ_被保険者マスタインポート");
                    if (!dataImport_hihoMaster(_cym, wf, strFileName_hihomaster))
                    {
                        wf.LogPrint("提供データ_被保険者マスタインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_被保険者マスタインポート失敗"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_被保険者マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_被保険者マスタのファイルが指定されていないため処理しない");
                }


                //20220428100331 furukawa st ////////////////////////
                //支給決定データ使用しない

                //if (strFileName_sikyu != string.Empty)
                //{
                //    wf.LogPrint("提供データ_支給決定データインポート");
                //    if (!dataImport_ChargeData(_cym,  wf, strFileName_sikyu))
                //    {
                //        wf.LogPrint("提供データ_支給決定データインポート失敗");
                //        System.Windows.Forms.MessageBox.Show("提供データ_支給決定データインポート失敗"
                //            , System.Windows.Forms.Application.ProductName
                //            , System.Windows.Forms.MessageBoxButtons.OK
                //            , System.Windows.Forms.MessageBoxIcon.Exclamation);
                //        return;
                //    }
                //    wf.LogPrint("提供データ_支給決定データインポート終了");
                //}
                //else
                //{
                //    wf.LogPrint("提供データ_支給決定データのファイルが指定されていないため処理しない");
                //}

                //20220428100331 furukawa ed ////////////////////////


                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                return;
            }
            finally
            {
                wf.Dispose();
            }
        }
 
 
        

        /// <summary>
        /// パンチデータインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool dataImport_punch(int _cym,WaitForm wf,string strFileName)
        {

            int intcym = _cym;

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_punch where cym={intcym}", tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            var lst = CommonTool.CsvImportMultiCode(strFileName);

            try
            {
                wf.LogPrint("データ取得");

                //最終行を最大値とする                
                wf.SetMax(lst.Count);

                foreach (var item in lst)
                {
                    imp_punch imp = new imp_punch();

                    //列数確認 ファイルによって違う
                    int colcnt = 19;
                    if (lst[0].Length != colcnt)
                    {
                        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                        return false;
                    }

                    if (CommonTool.WaitFormCancelProcess(wf)) return false;


                    //1列目が数値に変換できない場合は見出し行と見なし飛ばす
                    if (!int.TryParse(item[0].ToString(), out int tmp))
                    {
                        continue;
                    }


                    wf.LogPrint($"申請書パンチデータ 被保険者証番号: {item[1].ToString()}");

                    imp.f001_chargeym = item[0].ToString();
                    imp.f002_hihonum = item[1].ToString();
                    imp.f003_rezekind = item[2].ToString();
                    imp.f004_mediym = item[3].ToString();
                    imp.f005_insnum = item[4].ToString();
                    imp.f006_gender = item[5].ToString();
                    imp.f007_birthdayw = item[6].ToString();
                    imp.f008_counteddays = item[7].ToString();
                    imp.f009_ratio = item[8].ToString();
                    imp.f010_chargescore = item[9].ToString();
                    imp.f011_decidescore = item[10].ToString();
                    imp.f012_partial = item[11].ToString();
                    imp.f013_total = item[12].ToString();
                    imp.f014_futan = item[13].ToString();
                    imp.f015_futan2 = item[14].ToString();
                    imp.f016_zikofutan = item[15].ToString();
                    imp.f017_kanrinum = item[16].ToString();
                    imp.f018_clinicnum = item[17].ToString();
                    imp.f019_clinicname = item[18].ToString();
                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    imp.mediymad = int.Parse(imp.f004_mediym);                    
                    imp.birthdayad = DateTimeEx.GetDateFromJstr7(imp.f007_birthdayw);


                    wf.InvokeValue++;


                    if (!DB.Main.Insert<imp_punch>(imp, tran)) return false;
                }


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// 施術所マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_ClinicMaster(int _cym,WaitForm wf, string strFileName)
        {

            int intcym = _cym;

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_clinicmaster where cym={intcym}", tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            var lst = CommonTool.CsvImportMultiCode(strFileName);

            try
            {
                wf.LogPrint("データ取得");

                //最終行を最大値とする                
                wf.SetMax(lst.Count);

                foreach (var item in lst)
                {
                    imp_clinicmaster imp = new imp_clinicmaster();

                    //列数確認 ファイルによって違う
                    int colcnt = 94;
                    if (lst[0].Length != colcnt)
                    {
                        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                        return false;
                    }

                    if (CommonTool.WaitFormCancelProcess(wf)) return false;


                    //1列目が数値に変換できない場合は見出し行と見なし飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        continue;
                    }


                    wf.LogPrint($"施術所マスタインポート 医療機関コード: {item[0].ToString()}");

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    imp.f001_clinicnum = item[0].ToString();
                    imp.f002_dantainum = item[16].ToString();
                    imp.f003_clinickana = item[25].ToString();
                    imp.f004_clinicname = item[26].ToString();
                    imp.f005_cliniczip = item[28].ToString();
                    imp.f006_clinicadd = item[29].ToString();
                    imp.f007_clinicbanch = item[30].ToString();
                    imp.f008_clinickatagaki = item[31].ToString();
                    imp.f009_clinictel = item[32].ToString();
                    imp.f010_clinicperson = item[35].ToString();

                    wf.InvokeValue++;
                    

                    if (!DB.Main.Insert<imp_clinicmaster>(imp, tran)) return false;
                }


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// 支給決定ファイルインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool dataImport_ChargeData(int _cym,  WaitForm wf, string strFileName)
        {


            int intcym = _cym;

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_chargedata where cym={intcym}", tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            var lst = CommonTool.CsvImportMultiCode(strFileName);

            try
            {
                wf.LogPrint("データ取得");

                //最終行を最大値とする                
                wf.SetMax(lst.Count);

                foreach (var item in lst)
                {
                    imp_chargedata imp = new imp_chargedata();

                    //列数確認 ファイルによって違う
                    int colcnt = 73;
                    if (lst[0].Length != colcnt)
                    {
                        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                        return false;
                    }

                    if (CommonTool.WaitFormCancelProcess(wf)) return false;


                    //1列目が数値に変換できない場合は見出し行と見なし飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        continue;
                    }


                    wf.LogPrint($"支給決定データインポート 被保険者証番号: {item[18].ToString()}");

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    imp.f001_insnum = item[0].ToString();
                    imp.f002_insname = item[1].ToString().Trim();
                    imp.f003_createymd = item[2].ToString();
                    imp.f004_shishutsuymd = item[10].ToString();
                    imp.f005_hihonum = item[18].ToString();
                    imp.f006_pname = item[19].ToString();
                    imp.f007_sikyukind = item[20].ToString();
                    imp.f008_sikyunum = item[21].ToString();
                    imp.f009_mediym = item[22].ToString();
                    imp.f010_sikyukgk = item[28].ToString() != string.Empty ? int.Parse(item[28].ToString()).ToString() : "0";
                    imp.f011_sikyuketteikgk = item[29].ToString() != string.Empty ? int.Parse(item[29].ToString()).ToString() : "0";
                    imp.f012_jutogaku = item[30].ToString() != string.Empty ? int.Parse(item[30].ToString()).ToString() : "0";
                    imp.f013_sikyuchoseigaku = item[31].ToString() != string.Empty ? int.Parse(item[31].ToString()).ToString() : "0";
                    imp.f014_total = item[32].ToString() != string.Empty ? int.Parse(item[32].ToString()).ToString() : "0";
                    imp.f015_partial = item[33].ToString() != string.Empty ? int.Parse(item[33].ToString()).ToString() : "0";
                    imp.f016_shiharaikbn = item[47].ToString();
                    imp.f017_tuchinum = item[48].ToString();
                    imp.f018_shiharaisaki_clinicnum = item[49].ToString() + item[50].ToString() + item[51].ToString() + item[52].ToString();
                    imp.f019_shiharaisaki_name = item[53].ToString();
                    imp.f020_shiharaisaki_add = item[54].ToString();
                    imp.f021_shiharaisaki_zip = item[55].ToString();
                    imp.f022_kojinkbn = item[61].ToString();
                    imp.f023_atenanum = item[62].ToString();
                                      

                    wf.InvokeValue++;


                    if (!DB.Main.Insert<imp_chargedata>(imp, tran)) return false;
                }


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }


        }



        /// <summary>
        /// 被保険者マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool dataImport_hihoMaster(int _cym, WaitForm wf, string strFileName)
        {
            int intcym = _cym;

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);

            StringBuilder sb = new StringBuilder();
            //パーティションテーブル作成
            sb.AppendLine($"create table if not exists imp_hihomaster_{_cym} partition of imp_hihomaster for values in ({_cym}) ;");
            sb.AppendLine($"delete from imp_hihomaster where cym={intcym};");
            DB.Command cmd = new DB.Command(sb.ToString(),tran);
            if (!cmd.TryExecuteNonQuery()) return false;
            tran.Commit();


            tran = new DB.Transaction(DB.Main);

            //本番ファイルを加工用にコピー
            string strFileNameTmp = $"{System.IO.Path.GetDirectoryName(strFileName)}\\{System.IO.Path.GetFileNameWithoutExtension(strFileName)}_tmp.csv";
            System.IO.File.Copy(strFileName, strFileNameTmp,true);

            //加工用コピーファイルをロード
            var lst = CommonTool.CsvImportMultiCode(strFileNameTmp);
            //            var lst = CommonTool.CsvImportMultiCode(strFileName);

            

            try
            {
                
                wf.LogPrint("被保険者マスタインポート準備");
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                //string strFileNameTmp =$"{System.IO.Path.GetDirectoryName(strFileName)}\\{System.IO.Path.GetFileNameWithoutExtension(strFileName)}_tmp.csv";
                //System.IO.File.Copy(strFileName, strFileNameTmp);

                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileNameTmp, false);

                //加工用ファイルの加工
                for (int r = 0; r < lst.Count; r++)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        sw.Close();
                        return false;
                    }

                    if (r == 0) continue;
                    if (r == lst.Count - 1) continue;

                    //列数確認 ファイルによって違う
                    int colcnt = 162;
                    if (lst[r].Length != colcnt)
                    {
                        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                        return false;
                    }

                    
                    wf.LogPrint($"被保険者マスタインポート用加工 被保険者証番号: {lst[r][1].ToString()}");

                 
                    string strLine = string.Empty;

                    strLine += $"{lst[r][1].ToString()},";      //被保険者番号
                    strLine += $"{lst[r][2].ToString()},";      //漢字氏名
                    strLine += $"{lst[r][3].ToString()},";      //カナ氏名
                    strLine += $"{lst[r][4].ToString()},";      //生年月日
                    strLine += $"{lst[r][5].ToString()},";      //性別
                    strLine += $"{lst[r][10].ToString()},";     //発送用郵便番号
                    strLine += $"{lst[r][11].ToString()},";     //発送用住所                    
                    strLine += $"{lst[r][60].ToString()},";     //負担区分1

                    strLine += $"{_cym},";
                    strLine += $"\'{DateTimeEx.GetDateFromJstr7(lst[r][4].ToString())}\'";
                    
                    //strLine = strLine.Substring(0, strLine.Length - 1);
                    sw.WriteLine(strLine);
                    wf.InvokeValue++;
                }
                sw.Close();


                //必要項目だけに絞った加工用ファイルをテーブルにcopy
                wf.LogPrint($"インポート");
                if (!DirectImport(strFileNameTmp, intcym)) return false;


                ////最終行を最大値とする                
                //wf.SetMax(lst.Count);

                //foreach (var item in lst)
                //{
                //    imp_hihomaster imp = new imp_hihomaster();

                   
                //    if (CommonTool.WaitFormCancelProcess(wf)) return false;

                //    //1列目が1,3の場合はヘッダ/フッタレコードなので飛ばす
                //    if (int.TryParse(item[0].ToString(), out int tmp))
                //    {
                //        if (tmp == 1 || tmp == 3) continue;
                //    }

                //    //列数確認 ファイルによって違う
                //    int colcnt = 162;
                //    if (item.Length != colcnt)
                //    {
                //        System.Windows.Forms.MessageBox.Show($"列数が{colcnt}ではありません。取り込めません。ファイルの中身を確認してください"
                //            , System.Windows.Forms.Application.ProductName
                //            , System.Windows.Forms.MessageBoxButtons.OK
                //            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                //        return false;
                //    }

                //    //1列目が数値に変換できない場合は見出し行と見なし飛ばす
                //    if (!int.TryParse(item[0].ToString(), out int tmp2))
                //    {
                //        continue;
                //    }


                //    wf.LogPrint($"被保険者証番号: {item[1].ToString()}");


                //    imp.f001_hihonum = item[1].ToString();
                //    imp.f002_pname = item[2].ToString();
                //    imp.f003_pkana = item[3].ToString();
                //    imp.f004_pbirthday = item[4].ToString();
                //    imp.f005_pgender = item[5].ToString();
                //    imp.f006_sendzip = item[6].ToString();
                //    imp.f007_sendadd = item[7].ToString();
                //    imp.f008_futankbn1 = item[8].ToString();

                //    imp.cym = intcym;//	メホール上の処理年月 メホール管理用 
                //    imp.birthdayad = DateTimeEx.GetDateFromJstr7(imp.f004_pbirthday);


                //    wf.InvokeValue++;


                //    if (!DB.Main.Insert<imp_hihomaster>(imp, tran)) return false;
                //}


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
                cmd.Dispose();
                
            }


        }

   


        /// <summary>
        /// copyコマンドでインポート
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool DirectImport(string strFileName,int cym)
        {
            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.AppendLine($"copy imp_hihomaster(");
            sb.AppendLine($"f001_hihonum, f002_pname, f003_pkana, f004_pbirthday, f005_pgender,f006_sendzip, f007_sendadd, f008_futankbn1,cym,birthdayad )");
            sb.AppendLine($" from ");
            sb.AppendLine($" stdin with csv NULL AS ' ' encoding 'utf-8' ");
           
            if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
            {
                System.Windows.Forms.MessageBox.Show("CSV取込失敗",
                    System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }

            return true;

        }


        /// <summary>
        /// 被保険者マスタ
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool dataImport_HihoMaster(int _cym, WaitForm wf,string strFileName)
        {
            int intcym = _cym;

            
            //copyインポート時に不具合が起きるためutf8以外受け付けない
            
            System.Text.Encoding e = CommonTool.CSVEncodingCheck(strFileName);
            if (e != System.Text.Encoding.UTF8)
            {
                System.Windows.Forms.MessageBox.Show("文字コードがUTF8ではありません。変換してください",
                      System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }
            

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_hihomaster where cym={intcym}", tran);
            wf.LogPrint($"{intcym}削除");
            cmd.TryExecuteNonQuery();

            

            try
            {
         
                //件数が100万と多いためcopyでインポートに変更
                

                wf.LogPrint($"{intcym}CSVインポート");
                if(!DirectImport(strFileName, _cym))return false;
       
                //あまりやりたくないが100万と件数が多いためsqlで即席の和暦西暦変換
                wf.LogPrint($"{intcym}生年月日変換中...");
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($" update imp_hihomaster set birthad=");
                sb.AppendLine($" case substr(f003_hihobirth,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" getdatead1=");
                sb.AppendLine($" case substr(f006_getdate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" losedatead1=");
                sb.AppendLine($" case substr(f007_losedate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" tekiyodatead1=");
                sb.AppendLine($" case substr(f010_tekiyodate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" else 0 end,");
                sb.AppendLine($" tekiyodatead2=");
                sb.AppendLine($" case substr(f012_tekiyodate2,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" else 0 end, ");

                sb.AppendLine($" cym={intcym} ");

                sb.AppendLine($" where cym=0");

                cmd = DB.Main.CreateCmd(sb.ToString(), tran);
                cmd.TryExecuteNonQuery();



                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                
            }

        }



        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine(" update application set ");
            sb.AppendLine("  ayear			= cast(substr(ahk.f029shinryoym,2,2) as int) ");
            sb.AppendLine(" ,amonth			= cast(substr(ahk.f029shinryoym,4,2) as int) ");
            sb.AppendLine(" ,inum			= ahk.f012insnum	");
            sb.AppendLine(" ,hnum			= ahk.hihonum_narrow	");
            sb.AppendLine(" ,hname			= ahk.f019hihokanji	");
            sb.AppendLine(" ,psex			= cast(ahk.f020pgender	as int) ");
            sb.AppendLine(" ,pbirthday		= ahk.pbirthdayad	");
            sb.AppendLine(" ,afamily		= cast(ahk.f046family as int) ");
            sb.AppendLine(" ,aratio			= cast(ahk.f045ratio as int)/10		"); 

            sb.AppendLine(" ,istartdate1	= ahk.startdate1ad ");
            sb.AppendLine(" ,ifinishdate1	= ahk.finishdate1ad ");

            sb.AppendLine(" ,baccname		= ahk.f104acckanji	");
            sb.AppendLine(" ,bkana		    = ahk.f103acckana	");
            sb.AppendLine(" ,baccnumber		= ahk.f102accnumber	");

            sb.AppendLine(" ,atotal			= cast(ahk.f090total as int) ");
            sb.AppendLine(" ,apartial		= cast(ahk.f093partial as int) ");
            sb.AppendLine(" ,acharge		= cast(ahk.f092charge as int) ");

            sb.AppendLine(" ,aapptype		= case ahk.f043apptype ");
            sb.AppendLine($"                     when '04' then {(int)APP_TYPE.あんま} ");
            sb.AppendLine($"                     when '05' then {(int)APP_TYPE.鍼灸} ");
            sb.AppendLine(" end ");

            sb.AppendLine(" ,comnum			= ahk.f002comnum");

            sb.AppendLine(",taggeddatas");
            sb.AppendLine("       = 'GeneralString1:\"' || ahk.f021pnum || '\"' ");
            sb.AppendLine("       || 'GeneralString2:\"' || ahk.f022atenanum || '\"' ");

            sb.AppendLine(" from dataimport_ahk ahk  ");

            sb.AppendLine(" where ");
            sb.AppendLine($" application.comnum=ahk.f002comnum and application.cym={cym}");



            DB.Command cmd = new DB.Command(DB.Main, sb.ToString(),true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }



    }


}
