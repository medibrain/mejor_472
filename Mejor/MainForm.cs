﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.IO;

namespace Mejor
{    
    public partial class MainForm : Form
    {
        private bool isLoggedIn = false;
        BindingSource bsCount = new BindingSource();

        public MainForm()
        {
            InitializeComponent();
            labelVersion.Text = "Ver:" + Application.ProductVersion;

            bsCount.DataSource = new List<AppCounter>();
            dataGridViewMonth.DataSource = bsCount;
            dataGridViewMonth.Columns[nameof(AppCounter.CYM)].Visible = false;
            dataGridViewMonth.Columns[nameof(AppCounter.ViewYM)].Visible = false;
            dataGridViewMonth.Columns[nameof(AppCounter.ViewYear)].HeaderText = "年";
            dataGridViewMonth.Columns[nameof(AppCounter.Month)].HeaderText = "月";
            dataGridViewMonth.Columns[nameof(AppCounter.Counter)].HeaderText = "件数";
            dataGridViewMonth.ColumnHeadersVisible = true;
            dataGridViewMonth.RowHeadersVisible = false;
            dataGridViewMonth.Columns[nameof(AppCounter.ViewYear)].Width = 100;
            dataGridViewMonth.Columns[nameof(AppCounter.Month)].Width = 40;
            dataGridViewMonth.Columns[nameof(AppCounter.Counter)].Width = 80;
            dataGridViewMonth.Columns[nameof(AppCounter.Counter)].DefaultCellStyle.Format = "#,0";
            dataGridViewMonth.DefaultCellStyle.Font = new Font("Arial", 12);
            dataGridViewMonth.DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //DB内に保存されている設定をロード
            var load = true;
            load &= Medivery.Medivery.GetMediveryList();
            load &= Pref.GetPrefList();
            if (!load)
            {
                MessageBox.Show("設定のロードに失敗しました。", "エラー");
                Environment.Exit(0);
                this.Close();
                return;
            }

            CommonTool.setFormColor(this); //20190424092845 furukawa //本番サーバでない場合、フォーム背景色をThistleにする

            //バージョン管理関連
            //var local = Utility.StartupPath.Remove(3) == "C:\\";
            //buttonVersion.Visible = local;
        }

        private void getInsurerData()
        {
            //保険者一覧を取得
            while (!Insurer.GetInsurers(true))
            {
                //データベースに接続できなかった場合
                var ret = MessageBox.Show("設定メニューからサーバーのIPアドレスを指定してください",
                    "データベースに接続できません", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                //キャンセルが押されたらプログラム終了
                if (ret != DialogResult.OK)
                {
                    Environment.Exit(0);
                    this.Close();
                }
                //設定画面を表示する
                using (var f = new SettingForm())
                {
                    f.ShowDialog();
                }
            }

            //全ての機能ボタンを無効に設定
            buttonScan.Enabled = false;
            buttonOCRCheck.Enabled = false;
            buttonImport.Enabled = false;
            buttonDataExport.Enabled = false;
            buttonMatching.Enabled = false;
            buttonInspectCheck.Enabled = false;
            buttonQuery.Enabled = false;
            buttonHusen.Enabled = false;
            buttonShokai.Enabled = false;

            buttonListInput.Enabled = false;//20200606084858 furukawaリスト抽出入力ボタン初期化

            listBoxInsurer.DisplayMember = nameof(Insurer.InsurerName);
            showInsurers();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            //プログラム終了時の処理を記述
            this.Close();
        }

        class AppCounter
        {
            public int CYM { get; private set; }
            public string ViewYM => CYM.ToString("0000/00") +
                "  (" + DateTimeEx.GetInitialEraJpYearMonth(CYM)  + ")";
            public string ViewYear => CYM / 100 + " (" + DateTimeEx.GetInitialEraJpYear(CYM) + ")";
            public int Month => CYM % 100;
            public int Counter { get; private set; }

            //20220331095000 furukawa st //旧データ表示チェックボックス対応           
            public static List<AppCounter> GetCount(Insurer ins,bool flgPrevious)
            //      public static List<AppCounter> GetCount(Insurer ins)
            //20220331095000 furukawa ed ////////////////////////
            {
                //20200915171337 furukawa st ////////////////////////
                //AWS段階移行で、JyuseiDBがあるDBサーバのpg_databaseを見るため、AWSに移行してしまってメホールサーバにないDBの場合、
                //メホールサーバ上に無い判定になってロードを飛ばしてしまう
                //      if (!chkDBExists(ins.dbName)) return null;//20190423175428 furukawa //DBがない場合スルー
                //20200915171337 furukawa ed ////////////////////////

                var db = new DB(ins.dbName);

                //20220331094846 furukawa st //旧データ表示チェックボックス対応
                var sql = "SELECT cym, counter FROM appcounter WHERE counter>0 ";

                //20221215 ito st プロジェクトK入力、センター生には最新月しか見せない
                //if (!flgPrevious) sql += " and cym > 201812;";
                if (!flgPrevious) sql += " and cym > 201812";
                if (User.CurrentUser.belong == "外部業者") sql += " order by cym desc limit 1";
                sql += ";";
                //20221215 ito ed

                //      20200918104226 furukawa st //保険者のリストには2018/12以前は出さない
                //      var sql = "SELECT cym, counter FROM appcounter WHERE counter>0 and cym>201812;";
                //      //        var sql = "SELECT cym, counter FROM appcounter WHERE counter>0;";
                //      20200918104226 furukawa ed ////////////////////////
                //20220331094846 furukawa ed ////////////////////////

                var res = db.Query<AppCounter>(sql);

                //20200918104329 furukawa st //dbがないとき、メッセージがループするのでつぶす
                try
                {
                    var l = res.ToList();
                    l.Sort((x, y) => y.CYM.CompareTo(x.CYM));

                    return l;
                }
                catch(Exception ex)
                {
                    return null;
                }
                //var l = res.ToList();
                //20200918104329 furukawa ed ////////////////////////
            }
        }


        #region 保険者のDBがあるか調査 20190423182636 furukawa 

        /// <summary>
        /// 保険者のDBがあるか調査
        /// </summary>
        /// <param name="dbname">db名</param>
        /// <returns></returns>
        public static bool chkDBExists(string dbname)
        {

            var db = new DB("jyusei");
            var sql = "SELECT datname FROM pg_database";
            var res = db.Query<string>(sql);
            var l = res.ToList();
            if (l.Contains(dbname)) return true;
            return false;

        }
        #endregion


        //月データリストを更新
        private void monthListUpdate()
        {
            var ac = (AppCounter)bsCount.Current;
            bsCount.Clear();

            List<AppCounter> l = null;
            var ins = Insurer.CurrrentInsurer;

            //20220331095101 furukawa st ////////////////////////
            //旧保険者復旧時の旧データ表示対応
            
            var t = Task.Factory.StartNew(() => l = AppCounter.GetCount(ins,checkBoxPrevious.Checked));
            //      var t = Task.Factory.StartNew(() => l = AppCounter.GetCount(ins));
            //20220331095101 furukawa ed ////////////////////////


            t.ContinueWith(_ =>
            {
                //取得中に保険者が変わっていないかチェック
                if (ins?.InsurerID != Insurer.CurrrentInsurer?.InsurerID) return;
                bsCount.DataSource = l;
                bsCount.ResetBindings(false);

                //元の月にカーソル配置
                if (ac == null) return;
                for (int i = 0; i < dataGridViewMonth.RowCount; i++)
                {
                    if ((int)dataGridViewMonth[nameof(AppCounter.CYM), i].Value != ac.CYM) continue;
                    dataGridViewMonth.CurrentCell = dataGridViewMonth[nameof(AppCounter.ViewYear), i];
                    break;
                }

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        //保険者を選択
        private void listBoxInsurer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //対象Insurerインスタンスを指定
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            Insurer.CurrrentInsurer = ins;

            if (ins == null)
            {
                gbSagyo.Enabled = false;
                bsCount.Clear();
                bsCount.ResetBindings(false);
                return;
            }

            gbSagyo.Enabled = true;            

            //接続先データベースを設定
            DB.SetMainDBName(ins.dbName);

            //画像登録を許可
            buttonScan.Enabled = true;

            //20200131144230 furukawa st ////////////////////////
            //接続先取得、表示            
            lblconn.Text = DB.getJyuseiDBConnection();
            //20200131144230 furukawa ed ////////////////////////

            CommonTool.setFormColor(this);

            //月データリストを更新
            monthListUpdate();

            //20210127184238 furukawa st ////////////////////////
            //コード整頓、デフォルトの機能を有効に
            buttonScan.Enabled = true;                   //画像登録
            buttonOCRCheck.Enabled = true;               //入力
            buttonDataExport.Enabled = true;             //データ出力
            buttonInspectCheck.Enabled = true;           //点検作業
            buttonQuery.Enabled = true;                  //リスト作成
            buttonHusen.Visible = false;                 //付箋 →機能自体がない

            #region 不要だろう

            ////対象データが存在したら機能ボタンを有効に
            //switch ((InsurerID)ins.InsurerID)
            //{
            //    case InsurerID.OSAKA_KOIKI:
            //    case InsurerID.HYOGO_KOIKI:                    //20190406105719 furukawa 兵庫県後期広域追加
            //    case InsurerID.HIROSHIMA_KOIKI:
            //    case InsurerID.MIYAZAKI_KOIKI:
            //    case InsurerID.NARA_KOIKI:
            //    case InsurerID.SAPPORO_KOKUHO:
            //    case InsurerID.TOYOHASHI_KOKUHO:                //20190824232230 furukawa豊橋国保medivery
            //    case InsurerID.SHARP_KENPO:                     //20190826163040 furukawa シャープ健保medivery  
            //    case InsurerID.KISHIWADA_KOKUHO:                //20190827115444 furukawa 岸和田国保medivery                                                
            //        buttonScan.Enabled = true;                      //画像登録
            //        buttonOCRCheck.Enabled = true;                  //入力
            //        buttonDataExport.Enabled = true;                //データ出力
            //        buttonInspectCheck.Enabled = true;              //点検作業
            //        buttonQuery.Enabled = true;                     //リスト作成
            //        buttonHusen.Enabled = false;                    //付箋
            //        buttonShokai.Enabled = true;                    //照会
            //        break;


            //    #region 旧コード　上にまとめた

            //    //case InsurerID.OSAKA_KOIKI:
            //    //    buttonScan.Enabled = true;
            //    //    buttonOCRCheck.Enabled = true;
            //    //    buttonDataExport.Enabled = true;
            //    //    buttonInspectCheck.Enabled = true;
            //    //    buttonQuery.Enabled = true;
            //    //    buttonHusen.Enabled = false;
            //    //    buttonShokai.Enabled = true;
            //    //    break;


            //    //case InsurerID.HYOGO_KOIKI:                    //20190406105719 furukawa 兵庫県後期広域追加
            //    //case InsurerID.HIROSHIMA_KOIKI:
            //    //case InsurerID.MIYAZAKI_KOIKI:
            //    //case InsurerID.NARA_KOIKI:
            //    //    buttonScan.Enabled = true;
            //    //    buttonOCRCheck.Enabled = true;
            //    //    buttonDataExport.Enabled = true;
            //    //    buttonInspectCheck.Enabled = true;
            //    //    buttonQuery.Enabled = true;
            //    //    buttonHusen.Enabled = false;
            //    //    buttonShokai.Enabled = true;
            //    //    break;

            //    //case InsurerID.SAPPORO_KOKUHO:
            //    //    buttonScan.Enabled = true;
            //    //    buttonOCRCheck.Enabled = true;
            //    //    buttonDataExport.Enabled = true;
            //    //    buttonInspectCheck.Enabled = true;
            //    //    buttonQuery.Enabled = true;
            //    //    buttonHusen.Enabled = false;
            //    //    buttonShokai.Enabled = true;
            //    //    break;
            //    #endregion

            //    default:
            //        buttonScan.Enabled = true;                   //画像登録
            //        buttonOCRCheck.Enabled = true;               //入力
            //        buttonDataExport.Enabled = true;             //データ出力
            //        buttonInspectCheck.Enabled = true;           //点検作業
            //        buttonQuery.Enabled = true;                  //リスト作成
            //        buttonHusen.Enabled = false;                 //付箋
            //        buttonShokai.Enabled = false;                //照会
            //        break;
            //}


            #endregion
            //20210127184238 furukawa ed ////////////////////////

            //20210127183122 furukawa st ////////////////////////
            //照会ボタンはMediveryテーブルにある保険者から取る。Mediveryを使う為には照会ボタンを使わないといけないので

            //20210330130915 furukawa st ////////////////////////
            //照会ボタン（medivery）使用可否をmediveryxml版から取る            
            buttonShokai.Enabled = Medivery.MediveryXML.dicUseMvXML[ins.InsurerID.ToString()].usemedivery;
            //Medivery.Medivery medivery = Medivery.Medivery.GetMediVery(ins.InsurerID);
            //buttonShokai.Enabled = medivery == null ? false : true;
            //20210330130915 furukawa ed ////////////////////////
            #region old
            ////照会ボタン
            //buttonShokai.Enabled = Array.Exists(new[]
            //    { InsurerID.HIROSHIMA_KOIKI, InsurerID.MIYAZAKI_KOIKI, InsurerID.SHIZUOKA_KOIKI,
            //    InsurerID.OSAKA_GAKKO,InsurerID.CHIKIIRYO, InsurerID.NARA_KOIKI, InsurerID.IRUMA_KOKUHO,
            //    InsurerID.OSAKA_KOIKI, InsurerID.AICHI_TOSHI, InsurerID.MUSASHINO_KOKUHO, InsurerID.HIRAKATA_KOKUHO,
            //    InsurerID.MITSUBISHI_SEISHI, InsurerID.OTARU_KOKUHO, InsurerID.YAMAZAKI_MAZAK, InsurerID.AMAGASAKI_KOKUHO,
            //    InsurerID.NAGOYA_MOKUZAI, InsurerID.HIGASHIMURAYAMA_KOKUHO, InsurerID.FUTABA, InsurerID.KAGOME,
            //    InsurerID.KYOTO_KOIKI, InsurerID.YACHIYO_KOKUHO, InsurerID.NARITA_KOKUHO, InsurerID.CHUO_RADIO,
            //    InsurerID.ASAKURA_KOKUHO, InsurerID.OTSU_KOKUHO, InsurerID.DENKI_GARASU, InsurerID.NISSHIN_KOKUHO,
            //    InsurerID.HIGASHIOSAKA_KOKUHO, InsurerID.UJI_KOKUHO, InsurerID.KYOTOFU_NOKYO,
            //    InsurerID.HYOGO_KOIKI ,             //20190726102310 furukawa 兵庫広域を照会ボタン使用可能保険者に追加
            //    InsurerID.SHIMANE_KOIKI,            //20190730101511 furukawa 島根広域を照会ボタン使用可能保険者に追加
            //    InsurerID.TOYOHASHI_KOKUHO,         //20190824232624 furukawa 豊橋国保を照会ボタン使用可能保険者に追加
            //    InsurerID.SHARP_KENPO,              //20190826163252 furukawa シャープ健保を照会ボタン使用可能保険者に追加
            //    InsurerID.KISHIWADA_KOKUHO,         //20190827115531 furukawa 岸和田国保を照会ボタン使用可能保険者に追加
            //    InsurerID.ARAKAWAKU,                //20200518184817 furukawa 荒川区追加
            //    InsurerID.MIYAGI_KOKUHO,            //20200622142318 furukawa 宮城県国保追加
            //    InsurerID.SHIZUOKASHI_AHAKI,        //20200805103852 furukawa 静岡市あはき追加
            //    InsurerID.BEPPUSHI_KOKUHO,          //20201001141000 furukawa 別府市追加


            //},

            //    iid => iid == (InsurerID)ins.InsurerID) ||
            //    ins.InsurerType == INSURER_TYPE.学校共済;
            #endregion

            //20210127183122 furukawa ed ////////////////////////

            //◆マッチングボタン
            buttonMatching.Enabled = Array.Exists(new[]
            {
                InsurerID.OSAKA_KOIKI,
                InsurerID.HIROSHIMA_KOIKI,
                InsurerID.SHIZUOKA_KOIKI,
                InsurerID.NARA_KOIKI,
                InsurerID.TOKUSHIMA_KOIKI,
                InsurerID.OTSU_KOKUHO,
                InsurerID.SHARP_KENPO,
                InsurerID.KISHIWADA_KOKUHO,                     //20190723092640 furukawa 岸和田国保追加、コード整頓
                InsurerID.SAKURASHI_KOKUHO,
                InsurerID.EHIME_KOIKI,                          //20220616115712 furukawa 愛媛広域追加
                InsurerID.HYOGO_KOIKI2022,                      //20220908_2 ito 兵庫広域追加
                InsurerID.KAGOSHIMA_KOIKI,                      //20220908_3 ito 鹿児島広域追加
   
            },
               iid => iid == (InsurerID)ins.InsurerID);

            #region 旧コード
            //20190501154516 furukawa st ////////////////////////
            //シャープ健保もマッチング機能追加

            //buttonMatching.Enabled = Array.Exists(new[]
            //    { InsurerID.OSAKA_KOIKI, InsurerID.HIROSHIMA_KOIKI, InsurerID.SHIZUOKA_KOIKI,
            //    InsurerID.NARA_KOIKI, InsurerID.TOKUSHIMA_KOIKI, InsurerID.OTSU_KOKUHO, InsurerID.SHARP_KENPO, },
            //    iid => iid == (InsurerID)ins.InsurerID);

            //buttonMatching.Enabled = Array.Exists(new[]
            //    { InsurerID.OSAKA_KOIKI, InsurerID.HIROSHIMA_KOIKI, InsurerID.SHIZUOKA_KOIKI,
            //    InsurerID.NARA_KOIKI, InsurerID.TOKUSHIMA_KOIKI, InsurerID.OTSU_KOKUHO, },
            //    iid => iid == (InsurerID)ins.InsurerID);

            //20190501154516 furukawa ed ////////////////////////
            #endregion
            //20190723092640 furukawa ed ////////////////////////   

            //◆インポートボタン
            buttonImport.Enabled = Array.Exists(new[]
            { 
                InsurerID.AMAGASAKI_KOKUHO, 
                InsurerID.OSAKA_KOIKI, 
                InsurerID.HIROSHIMA_KOIKI,
                InsurerID.MIYAZAKI_KOIKI, 
                InsurerID.NARA_KOIKI, 
                InsurerID.SHIZUOKA_KOIKI,
                InsurerID.TOKUSHIMA_KOIKI, 
                InsurerID.HIGASHIMURAYAMA_KOKUHO, 
                InsurerID.KAGOME,
                InsurerID.KYOTO_KOIKI, 
                InsurerID.CHUO_RADIO, 
                InsurerID.MITSUBISHI_SEISHI,
                InsurerID.NAGOYA_MOKUZAI, 
                InsurerID.OTSU_KOKUHO, 
                InsurerID.KISHIWADA_KOKUHO,
                InsurerID.HIGASHIOSAKA_KOKUHO, 
                InsurerID.MIYAGI_KOIKI,
                InsurerID.NAGOYA_KOUWAN,                          //20190130111702 furukawa st 名古屋港湾健保もインポートボタンを有効に
                InsurerID.SHARP_KENPO,                            //20190212101828 furukawa st シャープ健保データ取り込み用
                InsurerID.HYOGO_KOIKI,                            //20190406121406 furukawa st 兵庫県後期広域追加
                InsurerID.KYOKAIKENPO_HIROSHIMA,                  //20200227133031 furukawa 協会けんぽ広島支部
                InsurerID.MIYAGI_KOKUHO,                          //20200316182258 furukawa 宮城県柔整追加
                InsurerID.NAGOYASHI_KOKUHO,                       //20200513152015 furukawa 名古屋市国保追加
                InsurerID.SHIZUOKASHI_AHAKI,                      //20200623161855 furukawa 静岡市あはき追加
                InsurerID.SAKURASHI_KOKUHO,                       //20200709105709 furukawa インポート佐倉市追加
                InsurerID.CHIBASHI_KOKUHO,                        //20200721095105 furukawa 千葉市国保追加
                InsurerID.BEPPUSHI_KOKUHO,                        //20200929155408 furukawa 別府市追加
                InsurerID.KOSHIGAYASHI_KOKUHO,                    //20201218153802 furukawa 越谷市柔整追加
                InsurerID.SHINAGAWAKU,                              //20210402090437 furukawa 品川区追加
                InsurerID.IBARAKISHI_KOKUHO,                        //20210402090437 furukawa 茨木市追加
                InsurerID.KODAIRASHI,                               //20210402090437 furukawa 小平市追加         
                InsurerID.SAKAISHI,                                 //20210402090437 furukawa 堺市追加
                InsurerID.KASHIWASHI,                               //20210402090437 furukawa 柏市追加
                InsurerID.ICHIHARASHI,                              //20210402090437 furukawa 市原市追加
                InsurerID.MATSUDOSHI,                               //20210414092500 furukawa 千葉県松戸市
                InsurerID.NAKANOKU,                                 //20210513164427 furukawa 中野区追加
                InsurerID.AWAJISHI,                                 //20210714182250 furukawa 淡路市追加
                InsurerID.KAWAGOESHI,                               //20211125142907 furukawa 川越市インポート追加
                InsurerID.TOYONAKASHI_COVID19,                      //20220111163912 furukawa 豊中市コロナワクチン接種追加
                InsurerID.TOYONAKASHI_COVID19_SHIGAI,               //20220201164819 furukawa 豊中市コロナワクチン接種市外
                InsurerID.HIROSHIMA_KOIKI2022,                      //20220312132259 furukawa 広島広域2022追加
                InsurerID.CHIBA_KOIKI_AHK,                          //20220322115413 furukawa 千葉広域あはき追加
                InsurerID.HYOGO_KOIKI2022,                          //20220326164235 furukawa 兵庫広域2022追加
                InsurerID.AKISHIMASHI,                              //20220403152623 furukawa 昭島市追加
                InsurerID.NISHINOMIYASHI,                           //20220415141510 furukawa 西宮市国保追加
                InsurerID.KAGOSHIMA_KOIKI,                          //20220420092912 furukawa 鹿児島広域追加
                InsurerID.IBARAKI_KOIKI,                            //20220511164110 furukawa 茨城広域追加
                InsurerID.EHIME_KOIKI,                              //20220609165504 furukawa 愛媛広域追加

            },
            iid => iid == (InsurerID)ins.InsurerID);

            //◆照会印刷ボタン
            buttonHenreiPrint.Enabled =
                ins.InsurerType == INSURER_TYPE.学校共済;

            //20200423121029 furukawa st ////////////////////////
            //◆データ出力ボタンの保険者別使用可否
            //以下の保険者はデータ出力ボタン使用させない。リスト作成からにする            
            buttonDataExport.Enabled = !Array.Exists(new[] {
                InsurerID.MIYAGI_KOKUHO,
                InsurerID.NAGOYASHI_KOKUHO,     //20200525164920 furukawa 名古屋市国保はリスト出力の保険者個別から出力
                //InsurerID.CHIBASHI_KOKUHO,    //20200731161457 furukawa 千葉市国保のデータ出力はリスト出力から
                InsurerID.BEPPUSHI_KOKUHO,      //20201001115251 furukawa 別府市はリスト作成画面から出す
                InsurerID.KOSHIGAYASHI_KOKUHO,  //20201221110745 furukawa 越谷市柔整はリスト作成画面から出す
                InsurerID.SHINAGAWAKU,          //20210405134111 furukawa 品川区はリスト作成画面から納品データを手で作る
                InsurerID.KODAIRASHI,           //20210405134111 furukawa 小平市はリスト作成画面から納品データを手で作る
                InsurerID.FUCHUSHI,             //20210405134111 furukawa 府中市はリスト作成画面から納品データを手で作る
                InsurerID.SAKAISHI,             //20210405134111 furukawa 堺市はリスト作成画面から納品データを手で作る
                InsurerID.KASHIWASHI,           //20210405134111 furukawa 柏市はリスト作成画面から納品データを手で作る              
                InsurerID.NAKANOKU,             //20210514142430 furukawa 中野区納品データなし
                InsurerID.NISSHIN_KOKUHO,       //20210519131424 furukawa 日進市国保納品データ出力無し
                InsurerID.HAKODATESHI,          //20210617155313 furukawa 函館市定型納品データなし
                InsurerID.KAWAGOESHI,           //20211125171728 furukawa 川越市納品データはリスト作成から

            },
            iid => iid == (InsurerID)ins.InsurerID);
            //20200423121029 furukawa ed ////////////////////////

            //20200606085455 furukawa st ////////////////////////
            //◆リスト抽出入力ボタンの有効化保険者            
            buttonListInput.Enabled = Array.Exists(new[]
            {
                InsurerID.MIYAGI_KOKUHO,
            },
            iid => iid == (InsurerID)ins.InsurerID);
            //20200606085455 furukawa ed ////////////////////////
          
            //◆入力しない保険者
            buttonOCRCheck.Enabled = !Array.Exists(new[]
            {
                InsurerID.ICHIHARASHI, //20210411165738 furukawa 市原市は入力無し
                InsurerID.KASHIWASHI,//20210412161415 furukawa 柏市は入力無し
                
                //20210430133624 furukawa st ////////////////////////
                //松戸市あはきだけ入力有りにする
                //InsurerID.MATSUDOSHI,//20210414092550 furukawa 千葉県松戸市通常入力無し
                //20210430133624 furukawa ed ////////////////////////

            },
            iid => iid == (InsurerID)ins.InsurerID);

            //20211224101534 furukawa st ////////////////////////
            //制限ユーザには入力ボタン以外触らせない
            panelUpper.Visible = !User.CurrentUser.limiteduser;
            panelDown.Visible = !User.CurrentUser.limiteduser;
            //設定ボタンも触らせない
            buttonSettings.Visible= !User.CurrentUser.limiteduser;
            //20211224101534 furukawa ed ////////////////////////
        }

        //0.システム設定
        private void buttonSettings_Click(object sender, EventArgs e)
        {
            using (var f = new SettingMenuForm()) f.ShowDialog();
        }

        //画像登録
        private void buttonScan_Click(object sender, EventArgs e)
        {
            if (listBoxInsurer.SelectedIndex == -1) return;
            var ins = (Insurer)listBoxInsurer.SelectedItem;

            using (var f = new ScanForm(ins))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        //OCRチェック
        private void buttonOCRCheck_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            using (var f = new GroupSelectForm(cnt.CYM, ins))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        //点検チェック
        private void buttonInspect_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;


            switch (ins.InsurerID)
            {
                case (int)InsurerID.SHARP_KENPO:

                    using (var f = new OtherTenkenMenuForm(ins, cnt.CYM))
                    {
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.ShowDialog();
                    }
                    break;
                default:

                    using (var f = new Inspect.InspectListForm(cnt.CYM))
                    {
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.ShowDialog();
                    }
                    break;
            }

            /*
            using (var f = new Inspect.InspectListForm(cnt.CYM))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }*/
        }

        //保険者よりの請求データインポート
        private void buttonImport_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            switch ((InsurerID)ins.InsurerID)
            {
                case InsurerID.AMAGASAKI_KOKUHO:
                    using (var f = new OpenFileDialog())
                    {
                        f.Filter = "CSVファイル|*.csv";
                        if (f.ShowDialog() != DialogResult.OK) return;

                        //20200416162716 furukawa /////同請求年月が存在する場合、削除判定させる                        
                        if (!Amagasaki.RefRece.chkDataExist(cnt.CYM)) return;

                        Amagasaki.RefRece.Import(f.FileName);

                        //20200412151954 furukawa /////元画像ファイル名を元に、提供データをApplicationに適用する
                        Amagasaki.RefRece.UpdateApp(cnt.CYM);
                    }
                    break;
                case InsurerID.AICHI_TOSHI:
                    break;
                case InsurerID.OSAKA_KOIKI:
                    using (var f = new RefReceImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                case InsurerID.HIROSHIMA_KOIKI:
                    using (var f = new RefReceImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                //20220511164333 furukawa /////茨城広域追加                
                case InsurerID.IBARAKI_KOIKI:
                    IbarakiKoiki.dataImport.import_main(cnt.CYM);
                    break;
                //20220614142108 furukawa /////愛媛広域追加                
                case InsurerID.EHIME_KOIKI:
                    EhimeKoiki.dataImport_AHK.dataImport_AHK_main(cnt.CYM);
                    break;
                //20220404151223 furukawa /////昭島市追加
                case InsurerID.AKISHIMASHI:
                    Akishimashi.dataImport.import_main(cnt.CYM);
                    break;
                //20220322115642 furukawa /////千葉広域あはき追加                
                case InsurerID.CHIBA_KOIKI_AHK:
                    ChibaKoikiAHK.dataImport.import_main(cnt.CYM);                    
                    break;
                //20220326164445 furukawa /////兵庫広域2022追加                
                case InsurerID.HYOGO_KOIKI2022:
                    using (HyogoKoiki2022.frmImport f = new HyogoKoiki2022.frmImport(cnt.CYM)) f.ShowDialog();                    
                    break;
                //20220312132514 furukawa /////広島広域2022追加
                case InsurerID.HIROSHIMA_KOIKI2022:
                    using (var f = new RefReceImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                case InsurerID.HIGASHIOSAKA_KOKUHO:
                    using (var f = new RefReceImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                case InsurerID.MIYAZAKI_KOIKI:
                    using (var f = new MiyazakiKoiki.ImportForm()) f.ShowDialog();
                    break;
                //20190406120814 furukawa /////兵庫県後期広域追加（あはきもあるので大阪広域と同じにしておく）
                case InsurerID.HYOGO_KOIKI:
                    using (var f = new RefReceImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                //20200623174904 furukawa /////静岡市あはき用口座情報メンテナンスメニュー追加                
                case InsurerID.SHIZUOKASHI_AHAKI:
                    using (var f = new Shizuokashi_Ahaki.BankMaintenance()) f.ShowDialog();
                    break;
                //20200709110022 furukawa /////インポート処理佐倉市追加                
                case InsurerID.SAKURASHI_KOKUHO:
                    Sakurashi_Kokuho.importdata.Import(cnt.CYM);
                    break;
                //20210401173752 furukawa /////品川区インポート画面                
                case InsurerID.SHINAGAWAKU:
                    using (Shinagawaku.FormImport f = new Shinagawaku.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210411110911 furukawa st /////茨木市国保                
                case InsurerID.IBARAKISHI_KOKUHO:
                    using (Ibarakishi.FormImport f = new Ibarakishi.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210411110911 furukawa st /////府中市
                case InsurerID.FUCHUSHI:
                    using (Fuchushi.FormImport f = new Fuchushi.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210411170603 furukawa st /////市原市は入力無し                
                case InsurerID.ICHIHARASHI:
                    Ichiharashi.dataImport.import_main(cnt.CYM);                    
                    break;
                //20210415092923 furukawa st /////柏市追加
                case InsurerID.KASHIWASHI:
                    Kashiwashi.dataImport.import_main(cnt.CYM);
                    break;
                //20211125143227 furukawa /////川越市
                case InsurerID.KAWAGOESHI:
                    using (Kawagoeshi.FormImport f = new Kawagoeshi.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210415092900 furukawa /////堺市追加
                case InsurerID.SAKAISHI:
                    using (Sakaishi.FormImport f = new Sakaishi.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210513170217 furukawa /////中野区追加                
                case InsurerID.NAKANOKU:
                    using (Nakanoku.FormImport f = new Nakanoku.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20210714181911 furukawa /////淡路市追加
                case InsurerID.AWAJISHI:
                    using(Awajishi.FormImport f=new Awajishi.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20220425105555 furukawa /////鹿児島広域追加                
                case InsurerID.KAGOSHIMA_KOIKI:
                    KagoshimaKoiki.dataImport.import_main(cnt.CYM);
                    break;
                //20220113140643 furukawa /////豊中市コロナワクチン接種追加
                case InsurerID.TOYONAKASHI_COVID19:
                    using (Toyonakashi_covid19.FormImport f = new Toyonakashi_covid19.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                //20220201165034 furukawa /////豊中市コロナワクチン接種市外
                case InsurerID.TOYONAKASHI_COVID19_SHIGAI:
                    using (Toyonakashi_covid19_shigai.FormImport f = new Toyonakashi_covid19_shigai.FormImport(cnt.CYM)) f.ShowDialog();
                    break;
                case InsurerID.MATSUDOSHI:
                    Matsudoshi.dataImport.import_main(cnt.CYM);                    
                    break;
                case InsurerID.NARA_KOIKI:
                    using (var f = new NaraKoiki.ImportForm(cnt.CYM)) f.ShowDialog();
                    break;
                case InsurerID.SHIZUOKA_KOIKI:
                    using (var f = new FolderBrowserDialog())
                    {
                        if (f.ShowDialog() != DialogResult.OK) return;
                        ShizuokaKoiki.RefRece.Import(f.SelectedPath);
                    }
                    break;
                case InsurerID.TOKUSHIMA_KOIKI:
                    using (var f = new OpenFileDialog())
                    {
                        if (f.ShowDialog() != DialogResult.OK) return;
                        TokushimaKoiki.RefRece.Import(f.FileName);
                    }
                    break;
                case InsurerID.HIGASHIMURAYAMA_KOKUHO:
                    HigashimurayamaKokuho.RefRece.Import();
                    break;
                case InsurerID.KAGOME:
                    Kagome.JuryoMaster.Import();
                    break;
                case InsurerID.KYOTO_KOIKI:
                    KyotoKoiki.Doctor.Import();
                    break;
                case InsurerID.CHUO_RADIO:
                    ChuoRadio.PayCodeImporter.Import();
                    break;
                case InsurerID.MITSUBISHI_SEISHI:
                    MitsubishiSeishi.PayCodeImporter.Import();
                    break;
                //20210419161949 furukawa /////小平市                
                case InsurerID.KODAIRASHI:
                    Kodairashi.RefRece.Import();
                    break;
                //20200929155730 furukawa /////別府市追加
                case InsurerID.BEPPUSHI_KOKUHO:
                    beppushi_kokuho.dataimport_jyusei.Import(cnt.CYM);
                    break;
                //20200721094818 furukawa /////千葉市国保追加                
                case InsurerID.CHIBASHI_KOKUHO:
                    Chibashi_kokuho.dataImport.import_main(cnt.CYM);
                    break;
                //20190130113934 furukawa /////名古屋港湾健保インポートボタン処理
                case InsurerID.NAGOYA_KOUWAN:
                    NagoyaKouwan.PayCodeImporter.Import();
                    break;
                //20200511160956 furukawa /////名古屋市国保追加                
                case InsurerID.NAGOYASHI_KOKUHO:
                    NagoyashiKokuho.memberdata.Import(cnt.CYM);
                    break;
                //20201218154025 furukawa /////越谷市柔整追加                
                case InsurerID.KOSHIGAYASHI_KOKUHO:
                    Koshigayashi_Kokuho.importdata.Import(cnt.CYM);
                    break;
                //20220415141644 furukawa /////西宮市国保追加                
                case InsurerID.NISHINOMIYASHI:
                    Nishinomiyashi.dataImport.import_main(cnt.CYM);
                    break;
                case InsurerID.NAGOYA_MOKUZAI:
                    NagoyaMokuzai.PayCodeImporter.Import();
                    break;
                case InsurerID.OTSU_KOKUHO:
                    OtsuKokuho.RefRece.Import();
                    break;
                case InsurerID.KISHIWADA_KOKUHO:
                    KishiwadaKokuho.RefRece.Import(cnt.CYM);
                    break;
                case InsurerID.MIYAGI_KOIKI:
                    MiyagiKoiki.Person.Import();
                    break;
                //20200316181752 furukawa /////宮城県柔整追加
                case InsurerID.MIYAGI_KOKUHO:
                    MiyagiKokuho.kyufudata.Import(cnt.CYM);
                    break;
                //20200227100409 furukawa /////協会けんぽ広島支部
                case InsurerID.KYOKAIKENPO_HIROSHIMA:
                    kyokaikenpo_hiroshima.KKData.Import();
                    break;
                //20190212095046 furukawa /////シャープ健保データ取り込み用
                case InsurerID.SHARP_KENPO:
                    using (var f = new SharpKenpo.ImportForm()) f.ShowDialog();
                    break;
            }
        }

        //提出データ出力
        private void buttonDataExport_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            using (var f = new ExportMenuForm(ins, cnt.CYM))
            {
                f.ShowDialog();
            }
        }

        //申出作成
        private void buttonQuery_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            try
            {
                this.Visible = false;//作業しやすいように
                using (var f = new ListCreate.ListCreateForm(cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            finally
            {
                this.Visible = true;
            }
        }

        //付箋作成
        private void buttonHusen_Click(object sender, EventArgs e)
        {
            MessageBox.Show("この機能はまだありません");

        }

        //フォーカスがMainFormに移るたびに月データを更新
        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (this.isLoggedIn)
            {
                monthListUpdate();
            }
        }

        
        private bool updateCheck()
        {
            //強制アップデート
            var info = VersionSelect.VersionInfo.ExistsForceUpdate();
            if (info != null)
            {
                var res = MessageBox.Show("アップデートが必要です。" +
                        "OKボタンを押してアップデートを実行してください。",
                        "アップデート確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (res != DialogResult.OK)
                {
                    res = MessageBox.Show("再確認\r\n\r\n" +
                        "アップデートが必要です。" +
                        "特別な理由がない限り、OKボタンを押してアップデートを実行してください。",
                        "アップデート再確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                    if (res != DialogResult.OK) return false;
                }

                info.Change();
                return true;
            }
            return false;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (updateCheck()) return;

            //20200131125201 furukawa /////各保険者接続先リストを作成
            if (!DB.CreateDBList()) return;

            //初回起動時、ログイン画面を表示
            using (var f = new LoginForm())
            {
                if (f.ShowDialog() != DialogResult.OK || f.LoginUserID == 0)
                {
                    this.Close();
                    return;
                }

                User.SetCurrentUser(f.LoginUserID);
                this.Text += " - User: " + User.CurrentUser.Name;

                //データを取得できるようにする
                this.isLoggedIn = true;
                getInsurerData();
            }

#if DEBUG
            //開発者、かつデバッグ時のみテストボタン表示
            buttonTest.Visible = User.CurrentUser.Developer;

            //20201108143530 furukawa st ////////////////////////
            //接続先ラベルを管理者にも確認用で見せる
            
            lblconn.Visible = User.CurrentUser.Developer || User.CurrentUser.Admin;
            //lblconn.Visible = User.CurrentUser.Developer;
            //20201108143530 furukawa ed ////////////////////////
#endif

        }

        /// <summary>
        /// 保険者ごとの個別機能入口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShokai_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            using (var f = new Shokai.ShokaiMenuForm(ins, cnt.CYM))
            {
                f.StartPosition = FormStartPosition.Manual;
                f.Top = Top;
                f.Left = Left + 630;
                f.ShowDialog();
            }
        }

        //マッチングチェック
        private void buttonMatching_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;
            int iID = ins.InsurerID;

            if (iID == (int)InsurerID.OSAKA_KOIKI)
            {
                using (var f = new OsakaKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            //20190723091236 furukawa /////岸和田国保追加
            else if (iID == (int)InsurerID.KISHIWADA_KOKUHO)
            {
                using (var f = new KishiwadaKokuho.InputForm(InputMode.MatchCheck, cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            else if (iID == (int)InsurerID.HIROSHIMA_KOIKI)
            {
                using (var f = new HiroshimaKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            else if (iID == (int)InsurerID.SHIZUOKA_KOIKI)
            {
                using (var f = new ShizuokaKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.NARA_KOIKI)
            {
                using (var f = new NaraKoiki.OtherForm(cnt.CYM))
                    f.ShowDialog();
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.TOKUSHIMA_KOIKI)
            {
                using (var f = new TokushimaKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                    f.ShowDialog();
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.SAKURASHI_KOKUHO)
            {
                using (var f = new Sakurashi_Kokuho.InputForm(InputMode.MatchCheck, cnt.CYM))
                    f.ShowDialog();
            }
            //20220616115601 furukawa /////愛媛広域追加
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.EHIME_KOIKI)
            {
                using (var f = new EhimeKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                    f.ShowDialog();
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.SHARP_KENPO)
            {
                using (var f = new SharpKenpo.OtherForm(cnt.CYM))
                    f.ShowDialog();
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.MIYAZAKI_KOIKI)
            {
                //MiyazakiKoiki.RefRece.AfterMatching(cnt.CYM);
            }
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.OTSU_KOKUHO)
            {
                using (var f = new OtsuKokuho.InputForm(InputMode.MatchCheck, cnt.CYM))
                {
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            //20220908_2 ito /////兵庫広域追加
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.HYOGO_KOIKI2022)
            {
                using (var f = new HyogoKoiki2022.InputForm(InputMode.MatchCheck, cnt.CYM))
                    f.ShowDialog();
            }
            //20220908_3 ito /////鹿児島広域追加
            else if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.KAGOSHIMA_KOIKI)
            {
                using (var f = new KagoshimaKoiki.InputForm(InputMode.MatchCheck, cnt.CYM))
                    f.ShowDialog();
            }

        }

        private void buttonAidSearch_Click(object sender, EventArgs e)
        {
            using (var f = new AidSearchForm()) f.ShowDialog();
        }

        private List<PostalCode> selectedPostalCodeHistory = new List<PostalCode>();//Mejor起動中に選択した都道府県情報を維持
        private void buttonOther_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            try
            {
                Visible = false;
                using (var f = new OtherMenuForm(cnt.CYM, selectedPostalCodeHistory))
                {
                    f.ShowDialog();
                }
            }
            finally
            {
                Visible = true;
            }
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            showInsurers();
        }

        private void showInsurers()
        {
            listBoxInsurer.SelectedIndex = -1;
            var l = Insurer.GetInsurers();

            if (radioButtonKokuho.Checked) l = l.FindAll(i => i.InsurerType == INSURER_TYPE.国民健康保険);
            else if (radioButtonKoiki.Checked) l = l.FindAll(i => i.InsurerType == INSURER_TYPE.後期高齢);
            else if (radioButtonKenpo.Checked) l = l.FindAll(i => i.InsurerType == INSURER_TYPE.健保組合);
            else if (radioButtonGakko.Checked) l = l.FindAll(i => i.InsurerType == INSURER_TYPE.学校共済);
            else if (radioButtonOther.Checked) l = l.FindAll(i => i.InsurerType == INSURER_TYPE.その他);

            l.Sort((x, y) => x.InsurerType == y.InsurerType ?
                x.ViewIndex.CompareTo(y.ViewIndex) : x.InsurerType.CompareTo(y.InsurerType));

            listBoxInsurer.DataSource = l;
        }

        private void buttonVersion_Click(object sender, EventArgs e)
        {
            using (var f = new VersionSelect.VersionSelectForm()) f.ShowDialog();
        }

        private void buttonHenreiPrint_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            using (var f = new ZenkokuGakko.HenreiPrintForm(cnt.CYM)) f.ShowDialog();
        }


        private void buttonTest_Click(object sender, EventArgs e)
        {
            //return;


            #region 兵庫広域　写し文字入れテスト

            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            string inputfile = ofd.FileName;
            if (inputfile == string.Empty) return;
            string outputfile = Path.GetDirectoryName(ofd.FileName);

            //string inputfile = "C:\\Users\\ito_ryuta\\Downloads" + "\\test.tif";
            //string outputfile = "C:\\Users\\ito_ryuta\\Downloads" + "\\test_out.tif";
            //string wm = "試";
            //int h = 50;
            //int v = 50;

            TiffUtility.AddWaterMarkToTif(inputfile, outputfile);//, wm);//, v, h);

            // string fileName = "D:\\projects\\mejor_docs\\11_保険者\\28_兵庫県後期広域\\2022-04-12_114544出力\\Data\\HYKA02\\Send\\50404\\あんま\\SH50404A01000008.tif";
            // string strOutputPath = "D:\\projects\\mejor_docs\\11_保険者\\28_兵庫県後期広域\\2022-04-12_114544出力\\Data\\HYKA02\\Send\\50404";
            // string strNewFileName = $"{strOutputPath}\\{System.IO.Path.GetFileNameWithoutExtension(fileName)}new{System.IO.Path.GetExtension(fileName)}";
            // string watermarkText = "写";

            // //Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);

            // List<Image> lst = TiffUtility.GetFrameImages(fileName);
            //// TiffUtility.MargeOrCopyTiff(TiffUtility.FastCopy,)
            //     Bitmap bmp = new Bitmap(fileName);
            //     Bitmap newbmp = new Bitmap(bmp.Width, bmp.Height);
            //     Graphics g = Graphics.FromImage(newbmp);
            //     g.DrawImage(bmp, 0, 0, bmp.Width, bmp.Height);
            //     Brush brush = new SolidBrush(Color.Black);
            //     Font font = new Font("Arial", 130.0f, FontStyle.Bold, GraphicsUnit.Pixel);
            //     g.DrawString(watermarkText, font, brush, 10, 10);
            //     newbmp.Save(strNewFileName);
            //     bmp.Dispose();

            // return;


            //System.Drawing.Imaging.BitmapData d = new System.Drawing.Imaging.BitmapData();
            //d = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            //    System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format16bppArgb1555);

            //Bitmap image =new Bitmap(bmp.Width, bmp.Height, d.Stride, System.Drawing.Imaging.PixelFormat.Format16bppGrayScale, d.Scan0);

            ////Graphics g = Graphics.FromImage(image);
            ////Brush brush = new SolidBrush(Color.Red);
            ////Font font = new Font("Arial", 130.0f, FontStyle.Bold, GraphicsUnit.Pixel);

            ////g.DrawString("test", font, brush, 0, 0);
            //image.Save(strNewFileName);

            //bmp.UnlockBits(d);



            ////OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            ////odd.ShowDialog();

            ////List<string>lst=System.IO.Directory.GetFiles(odd.Name).ToList<string>();

            ////foreach (string strfile in lst)
            ////{
            ////    using (Bitmap bmp = new Bitmap(strfile))
            ////    {
            ////        int basewidth = bmp.Width;
            ////        int baseheight = bmp.Height;
            ////        Rectangle rect = new Rectangle(0, 900, basewidth, 1500);// baseheight - 900);
            ////        string newfile = System.IO.Path.GetDirectoryName(strfile) + "\\half_" + System.IO.Path.GetFileName(strfile) ;
            ////        Bitmap bmpnew = bmp.Clone(rect, bmp.PixelFormat);
            ////        bmpnew.Save(newfile, System.Drawing.Imaging.ImageFormat.Tiff);
            ////        bmpnew.Dispose();
            ////    }
            ////}
            ////MessageBox.Show("end");

            #endregion

        }

        private void checkBoxAllVisible_CheckedChanged(object sender, EventArgs e)
        {
            Insurer.GetInsurers(!checkBoxAllVisible.Checked);
            showInsurers();
        }

        private void buttonListInput_Click(object sender, EventArgs e)
        {
            var ins = (Insurer)listBoxInsurer.SelectedItem;
            var cnt = (AppCounter)bsCount.Current;
            if (ins == null || cnt == null) return;

            using (var f = new Mejor.OtherInput.OtherInputListForm(cnt.CYM, ins))
            {
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }


        //20220216171331 furukawa st ////////////////////////
        //外部業者用メニュー

        private void buttonGaibu_Click(object sender, EventArgs e)
        {
            try
            {
                var cnt = (AppCounter)bsCount.Current;

                Mejor.ExternalMenuForm frm = new Mejor.ExternalMenuForm(cnt.CYM);
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                //Appcounterが入っていないとき（CYMが取得できない場合）は0で入る
                Mejor.ExternalMenuForm frm = new Mejor.ExternalMenuForm(0);
                frm.ShowDialog();

            }
        }
        //20220216171331 furukawa ed ////////////////////////

        //20221215 ito st /////プロジェクトK入力
        public static bool fixedMaskingInsurer(Insurer ins)
        {
            bool fixedMasking =
                //Agroup
                ins.InsurerID == (int)InsurerID.GAKKO_01HOKKAIDO ||
                ins.InsurerID == (int)InsurerID.GAKKO_11SAITAMA ||
                ins.InsurerID == (int)InsurerID.GAKKO_13TOKYO ||
                ins.InsurerID == (int)InsurerID.GAKKO_12CHIBA ||
                ins.InsurerID == (int)InsurerID.GAKKO_23AICHI ||
                ins.InsurerID == (int)InsurerID.GAKKO_28HYOGO ||
                //Bgroup
                ins.InsurerID == (int)InsurerID.OSAKA_GAKKO ||
                ins.InsurerID == (int)InsurerID.GAKKO_40FUKUOKA ||
                ins.InsurerID == (int)InsurerID.GAKKO_14KANAGAWA ||
                ins.InsurerID == (int)InsurerID.GAKKO_98TEST;

            return fixedMasking;
        }
        //20221215 ito ed /////プロジェクトK入力

    }
}
