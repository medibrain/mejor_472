﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    class InputStarter
    {
        public static void Start(int groupID, INPUT_TYPE iType, int aid = 0)
        {
            //直前にデータベースから他のユーザーが作業中かチェック
            var sg = ScanGroup.SelectWithScanData(groupID);
            if (!string.IsNullOrWhiteSpace(sg.WorkingUsers))
            {
                if (MessageBox.Show("このグループは現在他のユーザーが作業中です。\r\n" +
                    "このグループを編集しますか？", "ユーザー重複警告",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) != DialogResult.OK) return;
            }

            var lastStatus = sg.Status;

            //ScanGroupに現在のステータスを記録
            ScanGroup.WorkStart(groupID, ScanGroup.WorkType.入力);

            Form f = null;
            try
            {
                var verify = iType == INPUT_TYPE.Second;
                InsurerID iid = (InsurerID)Insurer.CurrrentInsurer.InsurerID;

                f = iid == InsurerID.AMAGASAKI_KOKUHO ? new Amagasaki.InputForm(sg, aid) :
                    iid == InsurerID.AICHI_TOSHI ? new AichiToshi.InputForm(sg, !verify, aid) :
                    //iid == InsurerID.OSAKA_GAKKO ? new OsakaGakko.InputForm(sg, !verify, aid) :
                    iid == InsurerID.OSAKA_KOIKI ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new OsakaKoiki.InputLongSpecialForm(sg) :
                        (Form)new OsakaKoiki.InputForm(InputMode.Input, sg, aid) :
                    iid == InsurerID.HIROSHIMA_KOIKI ? new HiroshimaKoiki.InputForm(InputMode.Input, sg, aid) :
                    iid == InsurerID.MUSASHINO_KOKUHO ? new Musashino.InputForm(sg, !verify, aid) :
                    iid == InsurerID.SAPPORO_KOKUHO ? string.IsNullOrWhiteSpace(sg.note2) ?
                        (Form)new Sapporoshi.FormOCRCheckJyu2(sg, !verify) :
                        (Form)new Sapporoshi.FormOCRCheckSK(sg, !verify) :
                    iid == InsurerID.MIYAZAKI_KOIKI ? new MiyazakiKoiki.InputForm(sg, aid) :

                    //20220322092359 furukawa st //千葉広域あはき追加
                    iid == InsurerID.CHIBA_KOIKI_AHK ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new ChibaKoikiAHK.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new ChibaKoikiAHK.PayInputForm(sg, false, aid) :
                        new ChibaKoikiAHK.InputForm(sg, !verify, aid) :
                    //20220322092359 furukawa ed ////////////////////////

                    //20220616100337 furukawa st //愛媛広域追加
                    iid == InsurerID.EHIME_KOIKI ? new EhimeKoiki.InputForm(sg, !verify, aid) :
                    //20220616100337 furukawa ed ////////////////////////

                    //20220311150659 furukawa st //広島広域2022追加
                    iid == InsurerID.HIROSHIMA_KOIKI2022 ? new HiroshimaKoiki2022.InputForm(InputMode.Input, sg, !verify, aid) :
                    //20220311150659 furukawa ed ////////////////////////

                    //20190406114250 furukawa st //兵庫県後期広域追加
                    iid == InsurerID.HYOGO_KOIKI ? new HyogoKoiki.InputForm(sg, aid) :
                    //20190406114250 furukawa ed ////////////////////////

                    //20220327103754 furukawa st ////兵庫広域2022追加                    
                    iid == InsurerID.HYOGO_KOIKI2022 ? new HyogoKoiki2022.InputForm(sg, aid) :
                    //20220327103754 furukawa ed ////////////////////////

                    //20220421130516 furukawa st //鹿児島広域追加
                    iid == InsurerID.KAGOSHIMA_KOIKI ? new KagoshimaKoiki.InputForm(InputMode.Input, sg, !verify, aid) :
                    //20220421130516 furukawa ed ////////////////////////

                    //20190705110526 furukawa st //島根広域追加                    
                    //20190717171000 furukawa st //ベリファイ入力にする
                    iid == InsurerID.SHIMANE_KOIKI ? new ShimaneKoiki.InputForm(sg, !verify, aid) :
                    //iid == InsurerID.SHIMANE_KOIKI ? new ShimaneKoiki.InputForm(sg, true, aid) :
                    //20190717171000 furukawa ed ////////////////////////
                    //20190705110526 furukawa ed ////////////////////////

                    //20200228173441 furukawa st //協会けんぽ広島支部追加
                    iid == InsurerID.KYOKAIKENPO_HIROSHIMA ? new kyokaikenpo_hiroshima.InputForm(sg, !verify, aid) :
                    //20200228173441 furukawa ed ////////////////////////

                    //20190807130355 furukawa st //豊橋市国保追加                    
                    iid == InsurerID.TOYOHASHI_KOKUHO ? new ToyohashiKokuho.InputForm(sg, !verify, aid) :
                    //20190807130355 furukawa ed ////////////////////////

                    //20220111164758 furukawa st //豊中市コロナワクチン接種追加                    
                    iid == InsurerID.TOYONAKASHI_COVID19 ? new Toyonakashi_covid19.InputForm(sg, !verify, aid) :
                    //20220111164758 furukawa ed ////////////////////////

                    //20220201130501 furukawa st //豊中市コロナワクチン接種市外                    
                    iid == InsurerID.TOYONAKASHI_COVID19_SHIGAI ? new Toyonakashi_covid19_shigai.InputForm(sg, !verify, aid) :
                    //20220201130501 furukawa ed ////////////////////////

                    iid == InsurerID.NARA_KOIKI ? new NaraKoiki.InputForm(sg, aid) :
                    iid == InsurerID.CHIKIIRYO ? new ChikiIryo.InputForm(sg, !verify, aid) :

                    //20200708110524 furukawa st //佐倉市追加                    
                    iid == InsurerID.SAKURASHI_KOKUHO ? new Sakurashi_Kokuho.InputForm(sg, !verify, aid) :
                    //20200708110524 furukawa ed ////////////////////////

                    //20210126155349 furukawa st //協会けんぽ仮置き
                    iid == InsurerID.KYOKAIKENPO ? new kyokaikenpo.InputForm(sg, !verify, aid) :
                    //20210126155349 furukawa ed ////////////////////////

                    //20190130110349 furukawa st //名古屋港湾入力画面、拡張入力追加
                    iid == InsurerID.NAGOYA_KOUWAN ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                        (Form)new NagoyaKouwan.InputForm(sg, !verify, aid) :
                    //20190130110349 furukawa ed ////////////////////////

                    //20190207091232 furukawa st //シャープ健保追加
                    iid == InsurerID.SHARP_KENPO ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                        (Form)new SharpKenpo.InputForm(sg, !verify, aid) :
                    //20190207091232 furukawa ed ////////////////////////

                    //20200313151939 furukawa st //荒川区柔整追加ベリ有り
                    //20200403172341 furukawa st //荒川区に変更
                    //iid == InsurerID.ARAKAWAKU_JYUSEI ?
                    iid == InsurerID.ARAKAWAKU ?
                    //20200403172341 furukawa ed ////////////////////////
                    // iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                    // iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                    (Form)new Arakawaku.InputForm(sg, !verify, aid) :
                    //20200313151939 furukawa ed ////////////////////////

                    //20200928163537 furukawa st //別府市追加
                    iid == InsurerID.BEPPUSHI_KOKUHO ?
                    (Form)new beppushi_kokuho.InputForm(sg, !verify, aid) :
                    //20200928163537 furukawa ed ////////////////////////

                    //20200311143406 furukawa st //宮城県柔整追加。拡張入力は支払情報ではなく負傷名、初検日～転記が５つ。理由は入力項目が多いため人員を分けたい
                    iid == InsurerID.MIYAGI_KOKUHO ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new MiyagiKokuho.InputForm_fusho(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new MiyagiKokuho.InputForm_fusho(sg, false, aid) :
                        //20200616092445 furukawa st ////////////////////////
                        //先行入力項目変更による起動フォーム変更
                        (Form)new MiyagiKokuho.InputForm1(sg, !verify, aid) :
                    //(Form)new MiyagiKokuho.InputForm(sg, !verify, aid) :
                    //20200616092445 furukawa ed ////////////////////////
                    //20200311143406 furukawa ed ////////////////////////

                    //20200511111418 furukawa st ////////////////////////
                    //名古屋市国保追加  
                    //拡張入力は負傷名１～５
                    //保険者特有は負傷理由専用画面
                    iid == InsurerID.NAGOYASHI_KOKUHO ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new NagoyashiKokuho.InputForm_fusho(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new NagoyashiKokuho.InputForm_fusho(sg, false, aid) :
                        iType == INPUT_TYPE.Extra1 ? (Form)new NagoyashiKokuho.InputForm_reason(sg, true, aid) :
                        iType == INPUT_TYPE.Extra2 ? (Form)new NagoyashiKokuho.InputForm_reason(sg, false, aid) :
                        (Form)new NagoyashiKokuho.InputForm(sg, !verify, aid) :
                    //20200511111418 furukawa ed ////////////////////////

                    iid == InsurerID.MITSUBISHI_SEISHI ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                        (Form)new MitsubishiSeishi.InputForm(sg, !verify, aid) :
                    iid == InsurerID.SHIZUOKA_KOIKI ? new ShizuokaKoiki.InputForm(InputMode.Input, sg, aid) :

                    //20200623145932 furukawa st //静岡市あはき追加
                    iid == InsurerID.SHIZUOKASHI_AHAKI ? new Shizuokashi_Ahaki.InputForm(sg, !verify, aid) :
                    //20200623145932 furukawa ed ////////////////////////

                    //20200413152612 furukawa st //外務省療養費追加                    
                    iid == InsurerID.GAIMUSHO_RYOUYOUHI ? new GaimusyoRyouyouhi.InputForm(sg, !verify, aid) :
                    //20200413152612 furukawa ed ////////////////////////

                    //20200722103251 furukawa st //千葉市国保追加  あはきのみ              
                    iid == InsurerID.CHIBASHI_KOKUHO ? (Form)new Chibashi_kokuho.InputForm(sg, !verify, aid) :
                    //20200722103251 furukawa ed ////////////////////////

                    //20201218101934 furukawa st //越谷市柔整追加                    
                    iid == InsurerID.KOSHIGAYASHI_KOKUHO ? (Form)new Koshigayashi_Kokuho.InputForm(sg, !verify, aid) :
                    //20201218101934 furukawa ed ////////////////////////

                    //20210402165640 furukawa st //品川区追加                    
                    iid == InsurerID.SHINAGAWAKU ? (Form)new Shinagawaku.InputForm(sg, aid) :
                    //20210402165640 furukawa ed ////////////////////////

                    //20210405100415 furukawa st //茨木市追加                    
                    iid == InsurerID.IBARAKISHI_KOKUHO ? (Form)new Ibarakishi.InputForm(sg, !verify, aid) :
                    //20210405100415 furukawa ed ////////////////////////

                    //20210414152230 furukawa st //府中市追加                    
                    iid == InsurerID.FUCHUSHI ? (Form)new Fuchushi.InputForm(sg, !verify, aid) :
                    //20210414152230 furukawa ed ////////////////////////

                    //20210414152253 furukawa st //堺市追加                    
                    iid == InsurerID.SAKAISHI ? (Form)new Sakaishi.InputForm(sg, !verify, aid) :
                    //20210414152253 furukawa ed ////////////////////////

                    //20210416103014 furukawa st //小平市追加                    
                    iid == InsurerID.KODAIRASHI ? (Form)new Kodairashi.InputForm(sg, !verify, aid) :
                    //20210416103014 furukawa ed ////////////////////////

                    //20210430133932 furukawa st //松戸市あはきだけ入力有りにする                    
                    iid == InsurerID.MATSUDOSHI ? (Form)new Matsudoshi.InputForm(sg, !verify, aid) :
                    //20210430133932 furukawa ed ////////////////////////

                    //20210513143333 furukawa st //中野区追加                    
                    iid == InsurerID.NAKANOKU ? (Form)new Nakanoku.InputForm(sg, !verify, aid) :
                    //20210513143333 furukawa ed ////////////////////////

                    //20210520141025 furukawa st //千葉県白井市                    
                    iid == InsurerID.SHIROISHI ? (Form)new Shiroishi.InputForm(sg, !verify, aid) :
                    //20210520141025 furukawa ed ////////////////////////

                    //20210617101845 furukawa st //函館市追加                    
                    iid == InsurerID.HAKODATESHI ? (Form)new Hakodateshi.InputForm(sg, !verify, aid) :
                    //20210617101845 furukawa ed ////////////////////////

                    //20210713093004 furukawa st //淡路市追加                    
                    iid == InsurerID.AWAJISHI ? (Form)new Awajishi.InputForm(sg, !verify, aid) :
                    //20210713093004 furukawa ed ////////////////////////

                    //20211116171203 furukawa st //川越市追加
                    iid == InsurerID.KAWAGOESHI ? (Form)new Kawagoeshi.InputForm(sg, !verify, aid) :
                     //20211116171203 furukawa ed ////////////////////////

                     //20220403152526 furukawa st //昭島市追加
                     iid == InsurerID.AKISHIMASHI ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Akishimashi.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Akishimashi.PayInputForm(sg, false, aid) :
                        (Form)new Akishimashi.InputForm(sg, !verify, aid) :
                    //20220403152526 furukawa ed ////////////////////////

                    //20220414164726 furukawa st //西宮市追加
                    iid == InsurerID.NISHINOMIYASHI ? (Form)new Nishinomiyashi.InputForm(sg, !verify, aid) :
                    //20220414164726 furukawa ed ////////////////////////

                    //20220512130333 furukawa st //茨城広域追加                    
                    iid == InsurerID.IBARAKI_KOIKI ? (Form)new IbarakiKoiki.InputForm(sg, !verify, aid) :
                    //20220512130333 furukawa ed ////////////////////////

                    iid == InsurerID.TOKUSHIMA_KOIKI ? new TokushimaKoiki.InputForm(InputMode.Input, sg, aid) :
                    iid == InsurerID.KISHIWADA_KOKUHO ? new KishiwadaKokuho.InputForm(InputMode.Input, sg, aid) :
                    iid == InsurerID.OTARU_KOKUHO ? new OtaruKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.KYOTOFU_NOKYO ? new KyotofuNokyo.InputForm(sg, !verify, aid) :
                    iid == InsurerID.IRUMA_KOKUHO ? new IrumaKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.HIRAKATA_KOKUHO ? new HirakataKokuho.InputForm(sg, aid) :
                    iid == InsurerID.NAGOYA_MOKUZAI ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                        (Form)new NagoyaMokuzai.InputForm(sg, !verify, aid) :
                    iid == InsurerID.YAMAZAKI_MAZAK ? new YamazakiMazak.InputForm(sg, aid) :
                    iid == InsurerID.CHUO_RADIO ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new Pay.PayInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new Pay.PayInputForm(sg, false, aid) :
                        (Form)new ChuoRadio.InputForm(sg, !verify, aid) :
                    iid == InsurerID.HIGASHIMURAYAMA_KOKUHO ? new HigashimurayamaKokuho.InputForm(sg, aid) :
                    iid == InsurerID.KAGOME ? new Kagome.InputForm(sg, !verify, aid) :
                    iid == InsurerID.FUTABA ? new Futaba.InputForm(sg, !verify, aid) :
                    iid == InsurerID.KYOTO_KOIKI ?
                        iType == INPUT_TYPE.ExFirst ? (Form)new KyotoKoiki.SubInputForm(sg, true, aid) :
                        iType == INPUT_TYPE.ExSecond ? (Form)new KyotoKoiki.SubInputForm(sg, false, aid) :
                        (Form)new KyotoKoiki.InputForm(sg, !verify, aid) :
                    iid == InsurerID.YACHIYO_KOKUHO ? new YachiyoKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.IBARAKI_SEIHO ? new IbarakiSeiho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.NARITA_KOKUHO ? new NaritaKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.ASAKURA_KOKUHO ? new AsakuraKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.OTSU_KOKUHO ? new OtsuKokuho.InputForm(InputMode.Input, sg, aid) :
                    iid == InsurerID.DENKI_GARASU ? new DenkiGarasu.InputForm(sg, !verify, aid) :

                    //20201105091845 furukawa st //協会けんぽ埼玉支部追加
                    iid == InsurerID.KYOKAIKENPO_SAITAMA ? new kyokaikenpo_saitama.InputForm(sg, !verify, aid) :
                    //20201105091845 furukawa ed ////////////////////////

                    //20190712145507 furukawa st //入力回数、申請書タイプで区別
                    iid == InsurerID.HIGASHIOSAKA_KOKUHO ?
                        sg.AppType == APP_TYPE.柔整 && iType == INPUT_TYPE.First ? (Form)new HigashiOsakaKokuho.InputFormJyu(InputMode.Input, sg, 1, aid) :
                        sg.AppType == APP_TYPE.柔整 && iType == INPUT_TYPE.Second ? (Form)new HigashiOsakaKokuho.InputFormJyu(InputMode.Input, sg, 2, aid) :
                        (sg.AppType == APP_TYPE.あんま || sg.AppType == APP_TYPE.鍼灸) ? (Form)new HigashiOsakaKokuho.InputFormAhaki(sg, !verify, aid) :
                        (Form)new HigashiOsakaKokuho.InputFormAhaki(sg, !verify, aid) :
                    // iid == InsurerID.HIGASHIOSAKA_KOKUHO ? sg.AppType == APP_TYPE.柔整 ?
                    //(Form)new HigashiOsakaKokuho.InputFormJyu(InputMode.Input, sg, aid) :
                    //(Form)new HigashiOsakaKokuho.InputFormAhaki(sg, !verify, aid) :
                    //20190712145507 furukawa ed ////////////////////////

                    iid == InsurerID.NISSHIN_KOKUHO ? new NisshinKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.MIYAGI_KOIKI ? new MiyagiKoiki.InputForm(sg, !verify, aid) :
                    iid == InsurerID.UJI_KOKUHO ? new UjiKokuho.InputForm(sg, !verify, aid) :
                    iid == InsurerID.HANNAN_KOKUHO ? sg.AppType == APP_TYPE.柔整 ?
                        (Form)new HannanKokuho.InputJyuForm(sg, aid) :
                        (Form)new HannanKokuho.InputForm(sg, !verify, aid) :

                    //20221215 ito st /////プロジェクトK入力
                    iType == INPUT_TYPE.GaibuInput1 ? (Form)new ZenkokuGakko.InputFormGaibu(sg, !verify, iType, aid) :
                    iType == INPUT_TYPE.GaibuInput2 ? (Form)new ZenkokuGakko.InputFormGaibu(sg, !verify, iType, aid) :
                    //iType == INPUT_TYPE.HonsyaCheck ? (Form)new ZenkokuGakko.InputFormGaibu(sg, !verify, iType, aid) :
                    //iType == INPUT_TYPE.HonsyaVerify ? (Form)new ZenkokuGakko.InputFormGaibu(sg, !verify, iType, aid) :
                    //20221215 ito end /////プロジェクトK入力

                    //20210317104029 furukawa st //学校共済のシフト押下画面が常態化しているので、こちらを採用
                    Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済 ? (Form)new ZenkokuGakko.InputFormRev(sg, !verify, aid) :
                    //          Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.学校共済 ?
                    //                  Control.ModifierKeys.HasFlag(Keys.Shift) ?
                    //                  (Form)new ZenkokuGakko.InputFormRev(sg, !verify, aid) :
                    //                  (Form)new ZenkokuGakko.InputForm(sg, !verify, aid) :
                    //20210317104029 furukawa ed ////////////////////////


                    throw new Exception("現在入力に対応していない保険者です");

                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
            finally
            {
                f.Dispose();
                ScanGroup.WorkFinish(sg.GroupID);
            }
        }

        /// <summary>
        /// 対象グループのステータス情報を取得
        /// </summary>
        /// <param name="sg"></param>
        private static GroupStatus GetScanGroupStatus(ScanGroup group)
        {
            int groupID = group.GroupID;
            var sql = "SELECT statusflags " +
                "FROM application " +
                $"WHERE groupid={groupID} " +
                "GROUP BY statusflags;";
            var fs = DB.Main.Query<int>(sql).ToList();

            bool flagCheck(int flags, StatusFlag flag) => ((StatusFlag)flags & flag) == flag;

            var inq = fs.Where(x => flagCheck(x, StatusFlag.点検対象)).ToList();

            GroupStatus gf;
            if (inq.Count > 0 && inq.All(x => flagCheck(x, StatusFlag.点検済)))
                gf = GroupStatus.点検済み;
            else if (inq.Count > 0 && inq.Exists(x => flagCheck(x, StatusFlag.点検済)))
                gf = GroupStatus.点検中;
            else if (fs.All(x => flagCheck(x, StatusFlag.ベリファイ済)))
                gf = GroupStatus.ベリファイ済み;
            else if (fs.All(x => flagCheck(x, StatusFlag.入力済)))
                gf = GroupStatus.入力済み;
            else if (fs.Exists(x => flagCheck(x, StatusFlag.入力済)))
                gf = GroupStatus.入力中;
            else
            {
                var s = Scan.Select(group.ScanID);
                gf = s.Status == SCAN_STATUS.MATCHED || s.Status == SCAN_STATUS.OCR済み ?
                    GroupStatus.OCR済み : GroupStatus.OCR待ち;
            }
            return gf;
        }

    }
}
