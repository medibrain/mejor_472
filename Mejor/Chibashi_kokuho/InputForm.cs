﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;
using NPOI.XWPF.UserModel;

namespace Mejor.Chibashi_kokuho

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 提供データ
        /// </summary>
        List<dataImport_AHK> lstImport = new List<dataImport_AHK>();

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxHmark,  };

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal, cbZenkai, verifyBoxZG, verifyBoxZM, verifyBoxZY, checkBoxVisit, checkBoxVisitKasan, };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 };

            //同意年月日
            douiConts = new Control[] { pDoui, };

     
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                
                if(scan.AppType != APP_TYPE.あんま && scan.AppType != APP_TYPE.鍼灸)
                {
                    System.Windows.Forms.MessageBox.Show("あはきのみ入力できます",
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    dataGridViewPlist.Visible = false;
                    verifyBoxY.Enabled = false;
                    return;
                }
                //提供データ取得
                lstImport = dataImport_AHK.SelectAll(app.CYM);
                setApp(app);
            }        

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書

                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書

                    pDoui.Visible = true;
                    break;
                    
                default:
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {

            dgv.Columns[nameof(dataImport_AHK.importid)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.cym)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.insnum)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.hihomark)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.hihonum)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.pname)].Width = 80;//20200818190551 furukawa 受療者名追加                                                                 
            dgv.Columns[nameof(dataImport_AHK.shinryoym)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.clinicnum)].Width = 70;
            dgv.Columns[nameof(dataImport_AHK.clinicname)].Width = 150;
            dgv.Columns[nameof(dataImport_AHK.atenanum)].Width = 40;//20200818161419 furukawa 宛名番号追加
            dgv.Columns[nameof(dataImport_AHK.pbirthday)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.psex)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.kind)].Width = 100;
            dgv.Columns[nameof(dataImport_AHK.ratio)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.shinsaym)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.counteddays)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.total)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.partial)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.charge)].Width = 40;
            dgv.Columns[nameof(dataImport_AHK.comnum)].Width = 150;
            dgv.Columns[nameof(dataImport_AHK.hihozip)].Width = 60;
            dgv.Columns[nameof(dataImport_AHK.hihoadd)].Width = 150;
            //dgv.Columns[nameof(dataImport_AHK.hihoname)].Width = 80;//20200904110320 furukawa 被保険者名はないので削除
            dgv.Columns[nameof(dataImport_AHK.shinsaymad)].Width = 80;
            dgv.Columns[nameof(dataImport_AHK.ymad)].Width = 80;
            dgv.Columns[nameof(dataImport_AHK.pbirthad)].Width = 80;


            dgv.Columns[nameof(dataImport_AHK.importid)].HeaderText = "id";
            dgv.Columns[nameof(dataImport_AHK.cym)].HeaderText = "処理年月";
            dgv.Columns[nameof(dataImport_AHK.insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(dataImport_AHK.hihomark)].HeaderText = "記号";
            dgv.Columns[nameof(dataImport_AHK.hihonum)].HeaderText = "番号";
            dgv.Columns[nameof(dataImport_AHK.pname)].HeaderText = "受療者名";//20200818190525 furukawa受療者名追加
            dgv.Columns[nameof(dataImport_AHK.shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(dataImport_AHK.clinicnum)].HeaderText = "機関コード";
            dgv.Columns[nameof(dataImport_AHK.clinicname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(dataImport_AHK.atenanum)].HeaderText = "宛名番号";//20200818161539 furukawa 宛名番号追加
            dgv.Columns[nameof(dataImport_AHK.pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(dataImport_AHK.psex)].HeaderText = "性別";
            dgv.Columns[nameof(dataImport_AHK.kind)].HeaderText = "種別２";
            dgv.Columns[nameof(dataImport_AHK.ratio)].HeaderText = "給付";
            dgv.Columns[nameof(dataImport_AHK.shinsaym)].HeaderText = "審査年月";
            dgv.Columns[nameof(dataImport_AHK.counteddays)].HeaderText = "日数";
            dgv.Columns[nameof(dataImport_AHK.total)].HeaderText = "決定点数";
            dgv.Columns[nameof(dataImport_AHK.partial)].HeaderText = "負担金";
            dgv.Columns[nameof(dataImport_AHK.charge)].HeaderText = "請求額";
            dgv.Columns[nameof(dataImport_AHK.comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(dataImport_AHK.hihozip)].HeaderText = "郵便番号";
            dgv.Columns[nameof(dataImport_AHK.hihoadd)].HeaderText = "住所";
            //dgv.Columns[nameof(dataImport_AHK.hihoname)].HeaderText = "氏名";//20200904110356 furukawa 被保険者名はないので削除
            dgv.Columns[nameof(dataImport_AHK.shinsaymad)].HeaderText = "審査年月";
            dgv.Columns[nameof(dataImport_AHK.ymad)].HeaderText = "診療年月";
            dgv.Columns[nameof(dataImport_AHK.pbirthad)].HeaderText = "生年月日";


            dgv.Columns[nameof(dataImport_AHK.importid)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.cym)].Visible = false;
            dgv.Columns[nameof(dataImport_AHK.insnum)].Visible = false;
            dgv.Columns[nameof(dataImport_AHK.hihomark)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.hihonum)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.pname)].Visible = true;//20200818190450 furukawa 受療者名追加
            dgv.Columns[nameof(dataImport_AHK.shinryoym)].Visible = false;
            dgv.Columns[nameof(dataImport_AHK.clinicnum)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.clinicname)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.atenanum)].Visible = false;//20200818161601 furukawa宛名番号追加
            dgv.Columns[nameof(dataImport_AHK.pbirthday)].Visible = false;
            dgv.Columns[nameof(dataImport_AHK.psex)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.kind)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.ratio)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.shinsaym)].Visible = false;
            dgv.Columns[nameof(dataImport_AHK.counteddays)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.total)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.partial)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.charge)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.comnum)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.hihozip)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.hihoadd)].Visible = true;
            //dgv.Columns[nameof(dataImport_AHK.hihoname)].Visible = true;//20200904110420 furukawa 被保険者名はないので削除
            dgv.Columns[nameof(dataImport_AHK.shinsaymad)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.ymad)].Visible = true;
            dgv.Columns[nameof(dataImport_AHK.pbirthad)].Visible = true;

        }

     
        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid(string importid="")
        {

            List<dataImport_AHK> lstimp = new List<dataImport_AHK>();


            string strhmark = verifyBoxHmark.Text.Trim();
            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            
            foreach (dataImport_AHK item in lstImport)
            {
                //診療年月、被保険者証記号/番号、合計金額で合致させる
                if (item.ymad==intymad.ToString() && item.hihomark==strhmark && item.hihonum==strhnum && item.total==intTotal)
                {
                    lstimp.Add(item);
                }
            }
            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            InitGrid();

        

            if (importid != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    //20200817185137 furukawa st ////////////////////////
                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒

                    if (r.Cells["comnum"].Value.ToString() == importid)
                    //if (r.Cells["importid"].Value.ToString() == importid)
                    //20200817185137 furukawa ed ////////////////////////
                    {
                        r.Selected = true;
                        //break;
                    }
                    //20200812163816 furukawa st ////////////////////////
                    //提供データIDと違う場合の処理抜け

                    else
                    {
                        r.Selected = false;
                        //break;

                    }
                    //20200812163816 furukawa ed ////////////////////////
                }
            }
            //20201013131708 furukawa st ////////////////////////
            //レセプト全国共通キーが無く（入力前）、マッチング条件で2行以上の場合、必ず選択させる(こっちで判定できない)為Selectedしない
            
            else if (dgv.Rows.Count>1 && (importid == string.Empty))
            {
                foreach(DataGridViewRow r in dgv.Rows)
                {
                    r.Selected = false;
                }
            }
            //20201013131708 furukawa ed ////////////////////////







            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            //20200812164508 furukawa st ////////////////////////
            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            //else if (lstimp.Count == 1)
            //20200812164508 furukawa ed ////////////////////////
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
      
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            //panelHnum.Visible = false;
            //panelTotal.Visible = false;            
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                //setVerify(app);
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //setInputedApp(app);
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:

                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証記号番号
                    string strhmark = app.HihoNum.Split('-')[0];
                    string strhnum = app.HihoNum.Split('-')[1];
                    setValue(verifyBoxHnum, strhnum, firstTime, nv);
                    setValue(verifyBoxHmark, strhmark,firstTime,nv);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, nv);

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //往療有無
                    setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);

                    //加算有無
                    setValue(checkBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);
           
                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);

                    //PastSupplyYM:前回支給
                    
                    setValue(cbZenkai, app.TaggedDatas.PastSupplyYM.ToString() == string.Empty ? false : true, firstTime, nv);
                    setDateValue(app.TaggedDatas.PastSupplyYM.ToString() == string.Empty ? 
                        DateTime.MinValue :
                        DateTimeEx.ToDateTime6(app.TaggedDatas.PastSupplyYM),
                        firstTime, nv, verifyBoxZY, verifyBoxZM, verifyBoxZG);


                    //20200817184936 furukawa st ////////////////////////
                    //グリッドはレセプト全国共通キーで合致させる（rridだとNextValなので再取込したときに面倒
                    
                    //グリッド選択する
                    createGrid(app.ComNum.ToString());
                    //createGrid(app.RrID.ToString());
                    //20200817184936 furukawa ed ////////////////////////

                    break;
            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証記号番号
                    string hMark = verifyBoxHmark.Text.Trim();
                    setStatus(verifyBoxHmark, hMark.Length != 2);
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);
                                    
                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);

                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //DouiDate:同意年月日               
                    DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //PastSupplyYM:前回支給
                    //20200805154009 furukawa st ////////////////////////
                    //前回支給空欄でもOK
                    
                    DateTime dtPastSupply = dateCheck(verifyBoxZG, verifyBoxZY, verifyBoxZM,true);
                    //DateTime dtPastSupply = dateCheck(verifyBoxZG, verifyBoxZY, verifyBoxZM);
                    //20200805154009 furukawa ed ////////////////////////


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月
                    app.HihoNum = $"{hMark}-{hnumN}";                //被保険者証記号番号
                    

                    //申請書種別
                    app.AppType = scan.AppType;

                    //負傷                
                    app.FushoFirstDate1 = f1FirstDt;    //初検日
                    
                    //負傷名1
                    app.FushoName1 = verifyBoxF1.Text.Trim();

                    //往療有無
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;

                    //加算有無
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;

                    //合計
                    app.Total = total;


                    if (dgv.Rows.Count > 0)
                    {
                        
                        app.RrID= int.Parse(dgv.CurrentRow.Cells["importid"].Value.ToString());                                                //ID
                        app.Ratio=int.Parse(dgv.CurrentRow.Cells["ratio"].Value.ToString());                                                   //給付割合

                        //マッチングなしを考慮し画面入力を優先させる
                        //app.HihoNum= $"{dgv.CurrentRow.Cells["hihomark"].Value.ToString()}-{dgv.CurrentRow.Cells["hihonum"].Value.ToString()}";      //被保険者証記号番号


                        //20200904105416 furukawa st ////////////////////////
                        //被保険者名じゃなく受療者名だった
                        app.PersonName = dgv.CurrentRow.Cells["pname"].Value.ToString();
                        //app.HihoName = dgv.CurrentRow.Cells["hihoname"].Value.ToString();                                                      //被保険者名
                        //20200904105416 furukawa ed ////////////////////////


                        app.HihoZip=dgv.CurrentRow.Cells["hihozip"].Value.ToString();                                                          //被保険者郵便番号
                        app.HihoAdd=dgv.CurrentRow.Cells["hihoadd"].Value.ToString();                                                          //被保険者住所
                        app.InsNum = dgv.CurrentRow.Cells["insnum"].Value.ToString();                                                          //保険者番号
                        app.Sex = int.Parse(dgv.CurrentRow.Cells["psex"].Value.ToString());                                                    //受療者性別
                        app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells["pbirthad"].Value.ToString());                                      //受療者生年月日
                        app.ClinicName = dgv.CurrentRow.Cells["clinicname"].Value.ToString();                                                  //医療機関名
                        app.ClinicNum = dgv.CurrentRow.Cells["clinicnum"].Value.ToString();                                                    //医療機関コード
                        app.ComNum = dgv.CurrentRow.Cells["comnum"].Value.ToString();                                                          //レセプト全国共通キー
                        app.CountedDays = int.Parse(dgv.CurrentRow.Cells["counteddays"].Value.ToString());                                     //施術日数

                        //種別
                        if (System.Text.RegularExpressions.Regex.IsMatch(dgv.CurrentRow.Cells["kind"].Value.ToString(), "[あんま]"))
                        {
                            app.AppType = APP_TYPE.あんま;
                        }
                        else if (System.Text.RegularExpressions.Regex.IsMatch(dgv.CurrentRow.Cells["kind"].Value.ToString(), "[はり]"))
                        {
                            app.AppType = APP_TYPE.鍼灸;
                        }

                        //新規継続
                        //初検日と診療年月が同じ場合は新規
                        if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells["ymad"].Value.ToString().Substring(0, 4)) &&
                            app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells["ymad"].Value.ToString().Substring(4, 2)))
                        {
                            app.NewContType = NEW_CONT.新規;
                        }
                        else
                        {
                            app.NewContType = NEW_CONT.継続;
                        }


                        app.Partial=  int.Parse(dgv.CurrentRow.Cells["partial"].Value.ToString());              //負担金額                
                        app.Charge =  int.Parse(dgv.CurrentRow.Cells["charge"].Value.ToString());               //請求金額
                    }

                    

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDoui;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDoui == DateTime.MinValue ? false : true;

                    //PastSupplyYM:前回支給                 
                    app.TaggedDatas.PastSupplyYM = cbZenkai.Checked ? dtPastSupply.ToString("yyyyMM") : string.Empty;
                    app.TaggedDatas.flgKofuUmu = cbZenkai.Checked ? true : false;


                    //20201228144059 furukawa st ////////////////////////
                    //マッチング無し確認は申請書のみ
                    
                    if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    {

                        if (dgv.Rows.Count == 0)
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
                    //20201228144059 furukawa ed ////////////////////////

                    #endregion



                    break;

            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

     

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211028144525 furukawa st ////////////////////////
                //auxにcomnumを入れておく
                
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString(),app.ComNum.ToString())) return false;

                //      20211011132116 furukawa st ////////////////////////
                //      AUXにもApptype追加

                //        20210604133106 furukawa st ////////////////////////
                //        AUXにもrridを更新            


                //        if (!Application_AUX.Update(app.Aid, app.AppType,tran,app.RrID.ToString())) return false;
                //            if (!Application_AUX.Update(app.Aid, tran, app.RrID.ToString())) return false;
                //        20210604133106 furukawa ed ////////////////////////
                //      20211011132116 furukawa ed ////////////////////////
                //20211028144525 furukawa ed ////////////////////////
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            createGrid();
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;

            //20200805155835 furukawa st ////////////////////////
            //同意書の同意年月を下に要望あり
            
            //同意書の同意年月は上
            else if ((verifyBoxY.Text == clsInputKind.施術同意書) && (douiConts.Contains(sender))) p = posTotal;
            //20200805155835 furukawa ed ////////////////////////

            else if (douiConts.Contains(sender)) p = posHname;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
