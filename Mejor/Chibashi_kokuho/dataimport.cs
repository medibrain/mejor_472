﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Chibashi_kokuho

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName = frm.strFileBaseData;
            string strFileName_jyuseikyufu = frm.strFileKyufuData;
            string strFileName_ahk = frm.strFileAHKRyouyouhiData;

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName != string.Empty)
                {
                    wf.LogPrint("基本データインポート");
                    if (!dataImport_base.dataImport_main(_cym, wf, strFileName))
                    {
                        wf.LogPrint("基本データインポート異常終了");
                        return;
                    }
                    
                    wf.LogPrint("基本データインポート終了");
                }
                else
                {
                    wf.LogPrint("基本データのファイルが指定されていないため処理しない");
                }

                if (strFileName_jyuseikyufu!=string.Empty)
                {
                    wf.LogPrint("給付データインポート");
                    if (!dataImport_JyuseiKyufu.dataImport_JyuseiKyufu_main(_cym, wf, strFileName_jyuseikyufu))
                    {
                        wf.LogPrint("給付データインポート異常終了");
                        return;
                    }

                    wf.LogPrint("給付データインポート終了");
                }
                else
                {
                    wf.LogPrint("給付データのファイルが指定されていないため処理しない");
                }

                if (strFileName_ahk != string.Empty)
                {
                    wf.LogPrint("あはき提供データインポート");
                    if (!dataImport_AHK.dataImport_AHK_main(_cym, wf, strFileName_ahk))
                    {
                        wf.LogPrint("あはき提供データインポート終了");
                        return;
                    }
                   

                    wf.LogPrint("あはき提供データインポート終了");
                }
                else
                {
                    wf.LogPrint("あはき提供データのファイルが指定されていないため処理しない");
                }

                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    /// <summary>
    /// 柔整基本データ（イメージデータ）インポート
    /// </summary>
    partial class dataImport_base
    {

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int importid { get;set; }=0;                                       //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                                         //メホール上の処理年月 メホール管理用;
        public string insnum { get; set; } = string.Empty;                        //保険者番号;
        public string shinsaym { get; set; } = string.Empty;                      //審査年月;
        public string comnum { get; set; } = string.Empty;                        //レセプト全国共通キー;
        public string kokuhonum { get; set; } = string.Empty;                     //国保連レセプト番号;
        public string ym { get; set; } = string.Empty;                            //施術年月;
        public string honkanyugai { get; set; } = string.Empty;                   //本人家族入外区分;
        public string hihomark { get; set; } = string.Empty;                      //被保険者証記号;
        public string hihonum { get; set; } = string.Empty;                       //被保険者証番号;
        public string psex { get; set; } = string.Empty;                          //性別;
        public string pbirth { get; set; } = string.Empty;                        //生年月日;
        public int ratio { get; set; } = 0;                                       //給付割合;
        public string clinicnum { get; set; } = string.Empty;                     //施術所コード;
        public int counteddays { get; set; } = 0;                                 //回数;
        public int total { get; set; } = 0;                                       //決定金額;
        public int charge { get; set; } = 0;                                      //保険者負担額;
        public int partial { get; set; } = 0;                                     //患者負担額;
        public string firstdate1 { get; set; } = string.Empty;                    //初検年月日;
        public int fushocount { get; set; } = 0;                                  //負傷名数;
        public string hihozip { get; set; } = string.Empty;                       //被保険者郵便番号;
        public string hihoadd { get; set; } = string.Empty;                       //被保険者住所;
        public string hihoname { get; set; } = string.Empty;                      //被保険者名;
        public string hihokana { get; set; } = string.Empty;                      //被保険者名カナ;

        public string shinsaymad { get; set; } = string.Empty;                    //審査年月西暦;
        public string ymad { get; set; } = string.Empty;                          //施術年月西暦;
        public DateTime pbirthad { get; set; } = DateTime.MinValue;               //受療者生年月日西暦;
        public DateTime firstdate1ad { get; set; } = DateTime.MinValue;           //初検日西暦;




        #endregion


        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        static int intcym = 0;

        List<dataImport_base> lstImp = new List<dataImport_base>();

        /// <summary>
        /// 基本データインポート
        /// </summary>
        /// <returns></returns>
        public static bool dataImport_main(int _cym,WaitForm wf,string strFileName)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");
                
                //npoiで開きデータ取りだし
                //dataimport_baseに登録
                if(!EntryExcelData(strFileName, wf)) return false;

                
                wf.LogPrint("Appテーブル作成");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
                if(!UpdateApp(intcym)) return false;


                //20210604130959 furukawa st ////////////////////////
                //Applicationと提供データを紐づけて、提供データのIDをAUXに入れておく
                wf.LogPrint("AppAUXテーブル更新");
                if (!UpdateAUX(intcym)) return false;

                //20210604130959 furukawa ed ////////////////////////




                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
           

        }



        //20210604125311 furukawa st////////////////////////
        
        /// <summary>
        /// Application補完情報の更新
        /// Applicationと提供データを紐づけて、提供データのIDをAUXに入れておく
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool UpdateAUX(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();


            sb.AppendLine("	update application_aux set  ");
            sb.AppendLine("	 matchingID01		=	c.importid  ");     //提供データID
            sb.AppendLine("	,matchingid01date 	= 	now()  ");          //更新時刻＝現在時刻

            //20211028145043 furukawa st ////////////////////////
            //auxにcomnumを入れておく
            
            sb.AppendLine("	,matchingID02		=	c.comnum  ");     //レセプト全国共通キー
            sb.AppendLine("	,matchingid02date 	= 	now()  ");          //更新時刻＝現在時刻
            //20211028145043 furukawa ed ////////////////////////


            sb.AppendLine("	from  ");

            //applicationと提供データが紐付くテーブル
            
            
            //20211028145105 furukawa st ////////////////////////
            //auxにcomnumを登録するため
            
            sb.AppendLine("	(select a.aid,b.importid,a.comnum  ");
            //sb.AppendLine("	(select a.aid,b.importid  ");
            //20211028145105 furukawa ed ////////////////////////


            sb.AppendLine("		from dataimport_base b  ");
            sb.AppendLine("		inner join application a on  ");
            sb.AppendLine("		b.comnum=a.comnum and b.cym=a.cym) c ");

            //aidで紐付いて、マッチングIDが入ってないレコード
            sb.AppendLine("	where application_aux.aid=c.aid  ");            
            sb.AppendLine("	and application_aux.matchingid01=''");

            string strsql = sb.ToString();

            DB.Command cmd = new DB.Command(DB.Main, strsql, true);

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }
        //20210604125311 furukawa ed////////////////////////


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine("update application set    " );
            sb.AppendLine("inum				          = b.insnum" );                                                 //保険者番号
            sb.AppendLine(",comnum			          = b.comnum" );                                                 //レセプト全国共通キー
            sb.AppendLine(",afamily			          = case b.honkanyugai when '2' then 2 else 6 end " );           //本人家族入外区分

            //20200817181311 furukawa st ////////////////////////
            //被保険者証記号番号のあいだはハイフン
            sb.AppendLine(",hnum				          = b.hihomark || '-' || b.hihonum" );                                  //被保険者証記号番号
                                 //",hnum				          = b.hihomark || b.hihonum" );                                  //被保険者証記号番号
            //20200817181311 furukawa ed ////////////////////////


            sb.AppendLine(",psex				      = cast(b.psex as int) " );                                     //性別
            sb.AppendLine(",aratio			          = b.ratio/10" );                                               //給付割合
            sb.AppendLine(",sid				          = b.clinicnum" );                                              //施術所コード
            sb.AppendLine(",acounteddays		      = cast(b.counteddays as int) " );                              //回数（実日数）
            sb.AppendLine(",atotal			          = cast(b.total as int) " );                                    //決定金額
            sb.AppendLine(",acharge			          = cast(b.charge as int)" );                                    //保険者負担額
            sb.AppendLine(",apartial			          = cast(b.partial as int)" );                                   //患者負担額
            sb.AppendLine(",taggeddatas	              " );
            sb.AppendLine("       = 'count:\"' || b.fushocount || '\"|' || 'GeneralString1:\"' || b.hihokana || '\"' " );//負傷数、被保険者名カナ
                                //"       = taggeddatas || 'count:\"' || b.fushocount || '\"|' || 'GeneralString1:\"' || b.hihokana || '\"' " );//負傷数、被保険者名カナ

            sb.AppendLine(",hzip				          = b.hihozip" );                            //被保険者郵便番号
            sb.AppendLine(",haddress			          = b.hihoadd" );                            //被保険者住所
            sb.AppendLine(",hname				          = b.hihoname" );                           //被保険者名

            sb.AppendLine(",ym				            = cast(b.ymad as int) " );                          //診療年月
            sb.AppendLine(",ayear               = cast(substr(b.ym,2,2) as int)");       //施術年和暦
            sb.AppendLine(",amonth              = cast(substr(b.ym,4,2) as int)");       //施術月和暦            

            sb.AppendLine(",pbirthday				        = cast(b.pbirthad as date) " );                     //受療者生年月日
            sb.AppendLine(",ifirstdate1				    = cast(b.firstdate1ad as date)  " );                //初検日1
            sb.AppendLine(",fchargetype=  " );
            sb.AppendLine("	case when cast(b.ym as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end " );//新規継続


            //20200803192553 furukawa st ////////////////////////
            //千葉市の柔整は画面入力しないので、こっちで更新

            sb.AppendLine(",aapptype=6 " ); //申請書種類は6柔整
            //20200803192553 furukawa ed ////////////////////////

            sb.AppendLine("from dataimport_base b " );
            sb.AppendLine("where " );
            sb.AppendLine($"application.numbering=b.kokuhonum and application.cym={cym};");




            string strsql = sb.ToString();

                
            DB.Command cmd = new DB.Command(DB.Main,strsql,true);
           

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データExcelを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName ,WaitForm wf)
        {
            wb= new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd=new DB.Command($"delete from dataimport_base where cym={intcym}",tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");
            
            try
            {
                wf.InvokeValue = 0;
                wf.SetMax(ws.LastRowNum);
                
                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;


                //20200817181106 furukawa st ////////////////////////
                //最終行が拾えてなかった
                
                for (int r = 1; r <= ws.LastRowNum; r++)
                //for (int r = 1; r < ws.LastRowNum; r++)
                //20200817181106 furukawa ed ////////////////////////

                {
                    dataImport_base imp = new dataImport_base();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

                    for (int c = 0; c < cellcnt; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        if (c == 0) imp.insnum = getCellValueToString(cell);                         //保険者番号
                        if (c == 1) imp.shinsaym = getCellValueToString(cell);                       //審査年月
                        if (c == 2) imp.comnum = getCellValueToString(cell);                         //レセプト全国共通キー
                        if (c == 3) imp.kokuhonum = getCellValueToString(cell);                      //国保連レセプト番号
                        if (c == 4) imp.ym = getCellValueToString(cell);                             //施術年月
                        if (c == 7) imp.honkanyugai = getCellValueToString(cell);                    //本人家族入外区分
                        if (c == 8) imp.hihomark = getCellValueToString(cell);                       //被保険者証記号
                        if (c == 9) imp.hihonum = getCellValueToString(cell);                        //被保険者証番号
                        if (c == 10) imp.psex = getCellValueToString(cell);                          //性別
                        if (c == 11) imp.pbirth = getCellValueToString(cell);                        //生年月日
                        if (c == 12) imp.ratio = int.Parse(getCellValueToString(cell));              //給付割合
                        if (c == 21) imp.clinicnum = getCellValueToString(cell);                     //施術所コード
                        if (c == 23) imp.counteddays = int.Parse(getCellValueToString(cell));        //回数
                        if (c == 24) imp.total = int.Parse(getCellValueToString(cell));              //決定金額
                        if (c == 27) imp.charge = int.Parse(getCellValueToString(cell));             //保険者負担額
                        if (c == 29) imp.partial = int.Parse(getCellValueToString(cell));            //患者負担額					
                        if (c == 31) imp.firstdate1 = getCellValueToString(cell);                    //初検年月日
                        if (c == 32) imp.fushocount = int.Parse(getCellValueToString(cell));         //負傷名数
                        if (c == 79) imp.hihozip = getCellValueToString(cell);                       //被保険者郵便番号
                        if (c == 80) imp.hihoadd = getCellValueToString(cell);                       //被保険者住所
                        if (c == 81) imp.hihoname = getCellValueToString(cell);                      //被保険者名
                        if (c == 82) imp.hihokana = getCellValueToString(cell);                      //被保険者名カナ



                        if (c == 1) imp.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.shinsaym)).ToString();   //審査年月西暦
                        if (c == 4) imp.ymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.ym)).ToString();               //診療年月西暦
                        if (c == 11) imp.pbirthad = DateTimeEx.GetDateFromJstr7(imp.pbirth);                                   //生年月日西暦
                        if (c == 31) imp.firstdate1ad = DateTimeEx.GetDateFromJstr7(imp.firstdate1);                           //初検日西暦


                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"柔整イメージデータ(基本　レセプト全国共通キー:{imp.comnum}");

                    if (!DB.Main.Insert<dataImport_base>(imp, tran)) return false;
                }

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
                wb.Close();
            }
            
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch(cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }
           
            
        }

    }

    /// <summary>
    /// 柔整給付データインポート
    /// </summary>
    partial class dataImport_JyuseiKyufu
    {
        #region テーブル構造
        [DB.DbAttribute.Serial]

        public int importid { get;set; }=0;                            //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                              //メホール上の処理年月 メホール管理用;
        public string comnum { get; set; } = string.Empty;             //レセプト全国共通キー;
        public string clinicnum { get; set; } = string.Empty;          //医療機関番号（施術所コード;
        public string clinicname { get; set; } = string.Empty;         //医療機関名（施術所名;
        #endregion

        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 柔整給付データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_JyuseiKyufu_main(int _cym,WaitForm wf,string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //dataimport_jyuseikyufuに登録
                if (!EntryExcelData(strFileName, wf)) return false;


                wf.LogPrint("Appテーブル作成");
                //レセプト全国共通キーで紐づけてアップデートする
                if(!UpdateApp(intcym))return false;
                

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
         
        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +
                "sid        = k.clinicnum" +     //医療機関コード
                ",sname		= k.clinicname " +     //医療機関名
                "from dataimport_jyuseikyufu k " +
                "where " +
                $"application.comnum=k.comnum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main, strsql, true);

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd = new DB.Command($"delete from dataimport_jyuseikyufu where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            try
            {
                wf.SetMax(ws.LastRowNum);
                //int c = 0;

                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;

                //20200817181434 furukawa st ////////////////////////
                //最終行が拾えてなかった
                
                for (int r = 1; r <= ws.LastRowNum; r++)
                //for (int r = 1; r < ws.LastRowNum; r++)
                //20200817181434 furukawa ed ////////////////////////
                {
                    dataImport_JyuseiKyufu imp = new dataImport_JyuseiKyufu();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

                    for (int c = 0; c < cellcnt; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        
                        if (c == 5) imp.clinicnum = getCellValueToString(cell);    //医療機関番号（施術所番号
                        if (c == 6) imp.clinicname = getCellValueToString(cell);   //医療機関名（施術所
                        if (c == 24) imp.comnum = getCellValueToString(cell);      //レセプト全国共通キー

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"給付データ  レセプト全国共通キー:{imp.comnum}");

                    if (!DB.Main.Insert<dataImport_JyuseiKyufu>(imp, tran)) return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                tran.Commit();
                wb.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }




    }



    partial class dataImport_AHK
    {
        #region テーブル構造
        [DB.DbAttribute.Serial]

        public int importid { get;set; }=0;                                   //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                                     //メホール上の処理年月 メホール管理用;
        public string insnum { get; set; } = string.Empty;                    //保険者番号;
        public string hihomark { get; set; } = string.Empty;                  //証記号;
        public string hihonum { get; set; } = string.Empty;                   //証番号;
        public string pname { get; set; } = string.Empty;                     //受療者名;
        public string shinryoym { get; set; } = string.Empty;                 //診療年月;
        public string clinicnum { get; set; } = string.Empty;                 //機関コード;
        public string clinicname { get; set; } = string.Empty;                //医療機関名;
        public string atenanum { get; set; } = string.Empty;                  //宛名番号;
        public string pbirthday { get; set; } = string.Empty;                 //生年月日;
        public string psex { get; set; } = string.Empty;                      //性別;
        public string kind { get; set; } = string.Empty;                      //種別２;
        public int ratio { get; set; } = 0;                                   //給付割合;
        public string shinsaym { get; set; } = string.Empty;                  //審査年月;
        public int counteddays { get; set; } = 0;                             //実日数;
        public int total { get; set; } = 0;                                   //決定点数（金額）;
        public int charge { get; set; } = 0;                                  //請求金額;
        public int partial { get; set; } = 0;                                 //患者負担金;
        public string comnum { get; set; } = string.Empty;                    //レセプト全国共通キー;
        public string hihozip { get; set; } = string.Empty;                   //郵便番号;
        public string hihoadd { get; set; } = string.Empty;                   //住所;
        public string shinsaymad { get; set; } = string.Empty;                //審査年月西暦;
        public string ymad { get; set; } = string.Empty;                      //診療年月西暦;
        public DateTime pbirthad { get; set; } = DateTime.MinValue;           //受療者生年月日西暦;




        #endregion


        static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// 柔整給付データインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_main(int _cym, WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //dataimport_AHKに登録
                if(!EntryExcelData(strFileName, wf)) return false;


                //入力での紐付けなので,現段階では不可能　2020/09/04furukawa
                //wf.LogPrint("Appテーブル作成");
                ////レセプト全国共通キーで紐づけてアップデートする
                //if (!UpdateApp(intcym)) return false;


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }

        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +
                " inum         = ahk.insnum" +                   //保険者番号

                //20200817180739 furukawa st ////////////////////////
                //被保険者証記号番号のあいだはハイフン
                
                ",hnum		   = ahk.hihomark || '-' || ahk.hihonum " +    //証記号 //証番号                
                //",hnum		   = ahk.hihomark || ahk.hihonum " +    //証記号 //証番号          
                //20200817180739 furukawa ed ////////////////////////


                ",ym           = cast(ahk.ymad as int) " +                  //診療年月西暦
                ",sid          = ahk.clinicnum" +                           //機関コード
                ",sname        = ahk.clinicname" +                          //医療機関名
                ",pbirthday    = ahk.pbirthad" +                            //受療者生年月日西暦
                ",psex         = cast(ahk.psex as int) " +                  //性別
                //",apptype    = ahk.clinicnum" +                           //種別２ あんま・鍼灸 →画像登録時にとる
                ",aratio       = cast(ahk.ratio as int) " +                 //給付割合                
                ",acounteddays = cast(ahk.counteddays as int) " +           //実日数
                ",atotal       = cast(ahk.total as int) " +                 //決定点数
                ",comnum       = ahk.comnum" +                              //レセプト全国共通キー
                ",hzip         = ahk.hihozip" +                             //郵便番号
                ",haddress     = ahk.hihoadd" +                             //住所
               // ",hname     = ahk.hihoname" +                               //氏名

                ",pname     = ahk.pname " +                             //20200818185850 furukawa 受療者名納品データに必要なので入れとく
                                                                        

                ",taggeddatas	              " +
                "       = 'GeneralString1:\"' || ahk.atenanum || '\"' " +//宛名番号


                " from dataimport_ahk ahk " +
                " where " +
                $" application.comnum=ahk.comnum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main, strsql,true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        private partial class names
        {
            public string comnum { get; set; } = string.Empty;
            public string zip { get; set; } = string.Empty;
            public string address { get; set; } = string.Empty;
            public string name { get; set; } = string.Empty;
            public string kana { get; set; } = string.Empty;
        }

        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName, WaitForm wf)
        {
            
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            
            NPOI.SS.UserModel.ISheet ws = wb.GetSheet("給付記録に並び替え");

            wf.LogPrint($"住所、受療者取得");
            List<names> lstname = new List<names>();

            //20200817200745 furukawa st ////////////////////////
            //最終行が拾えてなかった
            
            for (int r = 1; r <= ws.LastRowNum; r++)
            //for (int r = 1; r < ws.LastRowNum; r++)
            //20200817200745 furukawa ed ////////////////////////

            {
                NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

                //20210610151011 furukawa st ////////////////////////
                //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす

                if (row1 == null) continue;
                //20210610151011 furukawa ed ////////////////////////


                names n = new names();
                for (int c = 0; c < row1.Cells.Count; c++)
                {                    
                    NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                   

                    if (c == 57)
                    {
                        if (getCellValueToString(cell) == string.Empty)
                        {
                            System.Windows.Forms.MessageBox.Show("住所、受療者が指定の位置にありません");
                            wb.Close();
                            return false;
                        }
                    }
                    if (c == 24) n.comnum = getCellValueToString(cell);               //レセプト全国共通キー
                    if (c == 57) n.zip = getCellValueToString(cell);                  //郵便番号
                    if (c == 58) n.address = getCellValueToString(cell);              //住所
                    if (c == 59) n.name = getCellValueToString(cell);                 //氏名(受療者名)
                    if (c == 60) n.kana = getCellValueToString(cell);                 //氏名（受療者名）kana

                }
                lstname.Add(n);
            }



            ws = wb.GetSheet("療養費審査結果一覧");

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd = new DB.Command($"delete from dataimport_ahk where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            int col = 0;

            try
            {
                //20200817180644 furukawa st ////////////////////////
                //最終行を最大値とする
                
                wf.SetMax(ws.LastRowNum);
                //wf.SetMax(ws.LastRowNum - 1);
                //20200817180644 furukawa ed ////////////////////////

                //20200817202130 furukawa st ////////////////////////
                //最終行が拾えてなかった
                
                if (lstname.Count != ws.LastRowNum)
                //if (lstname.Count != ws.LastRowNum-1)
                //20200817202130 furukawa ed ////////////////////////
                {
                    System.Windows.Forms.MessageBox.Show("住所氏名とデータの行数が一致しません");
                    wb.Close();
                    return false;
                }
                    //int c = 0;

                    //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                    int cellcnt = ws.GetRow(1).Cells.Count;

                //20200817180603 furukawa st ////////////////////////
                //最終行が拾えてなかった
                
                for (int r = 1; r <= ws.LastRowNum; r++)
                //for (int r = 1; r < ws.LastRowNum; r++)
                //20200817180603 furukawa ed ////////////////////////
                {
                    dataImport_AHK imp = new dataImport_AHK();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

                    int inttotal = 0;
                    int intcharge = 0;
                    

                    for (int c = 0; c < cellcnt; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        col = c;
                        //2枚めのシートから取得するとき
                        if (c == 30) imp.insnum = getCellValueToString(cell);                      //保険者番号
                        if (c == 6) imp.hihomark = getCellValueToString(cell);                     //証記号
                        if (c == 7) imp.hihonum = getCellValueToString(cell);                      //証番号

                        if (c == 15) imp.pname = getCellValueToString(cell);                        //20200818191443 furukawa 受療者名納品データに必要
                                                                                                    

                        //20200818143002 furukawa st ////////////////////////
                        //宛名番号ぬけてた

                        if (c == 13) imp.atenanum = getCellValueToString(cell);                      //宛名番号
                        //20200818143002 furukawa ed ////////////////////////


                        if (c == 21)
                        {
                            imp.shinryoym = getCellValueToString(cell);     //診療年月
                            DateTimeEx.TryGetYearMonthTimeFromJdate(imp.shinryoym + ".01",out DateTime tmp);
                            imp.ymad = (tmp.Year*100+tmp.Month).ToString();                         //診療年月西暦

                        }
                        if (c == 25) imp.clinicnum = getCellValueToString(cell);                    //機関コード
                        if (c == 26) imp.clinicname = getCellValueToString(cell);                   //医療機関名

                        if (c == 44)
                        {
                            imp.pbirthday = getCellValueToString(cell);                              //生年月日
                            imp.pbirthad =DateTime.Parse(cell.DateCellValue.ToString());             //生年月日西暦

                        }

                        if (c == 45) imp.psex = getCellValueToString(cell);                       //性別
                        if (c == 32) imp.kind = getCellValueToString(cell);                       //種別２
                        
                        
                        if (c == 27)
                        {
                            inttotal = int.Parse(getCellValueToString(cell));                       //給付割合(合計金額)
                        }
                        if (c == 29)
                        {
                            intcharge = int.Parse(getCellValueToString(cell));

                            decimal dectotal = decimal.Parse(inttotal.ToString());
                            decimal deccharge = decimal.Parse(intcharge.ToString());
                            decimal decratio =  deccharge/ dectotal;

                            imp.ratio = int.Parse(Math.Truncate(decratio * 10).ToString());           //給付割合（請求金額）
                        }

                        if (c == 38) imp.shinsaym = getCellValueToString(cell);                    //審査年月
                        if (c == 22) imp.counteddays = int.Parse(getCellValueToString(cell));      //実日数
                        if (c == 27) imp.total = int.Parse(getCellValueToString(cell));            //決定点数
                        if (c == 28) imp.partial = int.Parse(getCellValueToString(cell));          //一部負担金
                        if (c == 29) imp.charge = int.Parse(getCellValueToString(cell));           //請求金額

                        if (c == 10)
                        {
                            string tmp= getCellValueToString(cell);
                            if(System.Text.RegularExpressions.Regex.IsMatch(tmp,"[E\\+]"))
                            {
                                System.Windows.Forms.MessageBox.Show("レセプト全国共通キーが壊れています。インポートデータを修正してください");
                                return false;
                            }
                            imp.comnum = getCellValueToString(cell);                      //レセプト全国共通キー
                        }

                        //レセプト全国共通キーで合致させる
                        if (c == 11)
                        {
                            foreach (names n in lstname)
                            {
                                if (n.comnum == imp.comnum)
                                {
                                    imp.hihozip = lstname[r - 1].zip.ToString();                     //郵便番号
                                    imp.hihoadd = lstname[r - 1].address.ToString();                 //住所

                                    //imp.hihoname = lstname[r - 1].name.ToString();                     //氏名

                                    //20200903130024 furukawa st ////////////////////////
                                    //1シート目にメディで入れている「氏名」は受療者名                                    
                                    imp.pname= lstname[r - 1].name.ToString();//受療者名
                                    //20200903130024 furukawa ed ////////////////////////


                                    break;
                                }
                            }
                        }
                        //if (lstname[r - 1].comnum == imp.comnum)
                        //{
                        //    if (c == 11) imp.hihozip = lstname[r - 1].zip.ToString();                     //郵便番号
                        //    if (c == 11) imp.hihoadd = lstname[r - 1].address.ToString();                      //住所
                        //    if (c == 11) imp.hihoname = lstname[r - 1].name.ToString();                     //氏名
                        //}

                        if (c == 38) imp.shinsaymad = getCellValueToString(cell);  //審査年月西暦




                        //if (c == 0) imp.insnum = getCellValueToString(cell);                       //保険者番号
                        //if (c == 1) imp.hihomark = getCellValueToString(cell);                     //証記号
                        //if (c == 2) imp.hihonum = getCellValueToString(cell);                      //証番号
                        //if (c == 4) imp.shinryoym = 
                        //        System.Text.RegularExpressions.Regex.Replace(getCellValueToString(cell), "[R]([0-9]{2})\\.([0-9]{2})", "5$1$2");
                        //if (c == 5) imp.clinicnum = getCellValueToString(cell);                    //機関コード
                        //if (c == 6) imp.clinicname = getCellValueToString(cell);                   //医療機関名
                        //if (c == 10) imp.pbirthday = getCellValueToString(cell);                   //生年月日
                        //if (c == 11) imp.psex = getCellValueToString(cell);                        //性別
                        //if (c == 13) imp.kind = getCellValueToString(cell);                        //種別２
                        //if (c == 16) imp.ratio = int.Parse(getCellValueToString(cell));            //給付割合
                        //if (c == 20) imp.shinsaym = getCellValueToString(cell);                    //審査年月
                        //if (c == 21) imp.counteddays = int.Parse(getCellValueToString(cell));      //実日数
                        //if (c == 22) imp.total = int.Parse(getCellValueToString(cell));            //決定点数
                        //if (c == 24) imp.comnum = getCellValueToString(cell);                      //レセプト全国共通キー
                        //if (c == 57) imp.hihozip = getCellValueToString(cell);                     //郵便番号
                        //if (c == 58) imp.hihoadd = getCellValueToString(cell);                     //住所
                        //if (c == 59) imp.hihoname = getCellValueToString(cell);                    //氏名

                        //if (c == 20) imp.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.shinsaym)).ToString();  //審査年月西暦
                        //if (c == 4) imp.ymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.shinryoym)).ToString();         //診療年月西暦
                        //if (c == 10) imp.pbirthad = DateTimeEx.GetDateFromJstr7(imp.pbirthday);                                //受療者生年月日西暦

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"あはきデータ　レセプト全国共通キー:{imp.comnum}");

                    if (!DB.Main.Insert<dataImport_AHK>(imp, tran)) return false;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col +"\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
                wb.Close();
            }

        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }


        /// <summary>
        /// あはき提供データの取得
        /// </summary>
        /// <returns></returns>
        public static List<dataImport_AHK> SelectAll(int _cym)
        {
            List<dataImport_AHK> lst = new List<dataImport_AHK>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport_ahk where cym={_cym}");
            var l=cmd.TryExecuteReaderList();
            for (int r = 0; r < l.Count; r++)
            {
                int c = 0;
                dataImport_AHK d = new dataImport_AHK();

                d.importid = int.Parse(l[r][c++].ToString());
                d.cym = int.Parse(l[r][c++].ToString());
                d.insnum = l[r][c++].ToString();
                d.hihomark = l[r][c++].ToString();
                d.hihonum = l[r][c++].ToString();
                d.pname = l[r][c++].ToString();//20200818190302 furukawa受療者名納品データに必要なので入れとく
                d.shinryoym = l[r][c++].ToString();
                d.clinicnum = l[r][c++].ToString();
                d.clinicname = l[r][c++].ToString();

                d.atenanum = l[r][c++].ToString();//20200818162856 furukawa宛名番号追加

                d.pbirthday = l[r][c++].ToString();
                d.psex = l[r][c++].ToString();
                d.kind = l[r][c++].ToString();
                d.ratio = int.Parse(l[r][c++].ToString());
                d.shinsaym = l[r][c++].ToString();
                d.counteddays = int.Parse(l[r][c++].ToString());
                d.total = int.Parse(l[r][c++].ToString());
                d.charge = int.Parse(l[r][c++].ToString());
                d.partial = int.Parse(l[r][c++].ToString());
                d.comnum = l[r][c++].ToString();
                d.hihozip = l[r][c++].ToString();
                d.hihoadd = l[r][c++].ToString();
                //d.hihoname = l[r][c++].ToString();//20200904110809 furukawa 被保険者名はないので削除
                d.shinsaymad = l[r][c++].ToString();
                d.ymad = l[r][c++].ToString();
                d.pbirthad = DateTime.Parse(l[r][c++].ToString());

                lst.Add(d);
            }

            return lst;

        }


    }


    /// <summary>
    /// 既出テーブルだが使わないかも
    /// (既に出力した人は出さない場合があるので、その人を記録しておくテーブル)
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
