﻿namespace Mejor
{
    partial class InputMemoControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxMemo = new System.Windows.Forms.TextBox();
            this.labelEditCancel = new System.Windows.Forms.Label();
            this.labelMemoUpdate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.borderCheckBoxError = new Mejor.BorderCheckBox();
            this.SuspendLayout();
            // 
            // textBoxMemo
            // 
            this.textBoxMemo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxMemo.Location = new System.Drawing.Point(0, 26);
            this.textBoxMemo.Multiline = true;
            this.textBoxMemo.Name = "textBoxMemo";
            this.textBoxMemo.Size = new System.Drawing.Size(214, 137);
            this.textBoxMemo.TabIndex = 7;
            this.textBoxMemo.TabStop = false;
            this.textBoxMemo.TextChanged += new System.EventHandler(this.textBoxMemo_TextChanged);
            this.textBoxMemo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMemo_KeyDown);
            // 
            // labelEditCancel
            // 
            this.labelEditCancel.BackColor = System.Drawing.Color.LightCyan;
            this.labelEditCancel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelEditCancel.Location = new System.Drawing.Point(151, 2);
            this.labelEditCancel.Name = "labelEditCancel";
            this.labelEditCancel.Size = new System.Drawing.Size(61, 22);
            this.labelEditCancel.TabIndex = 3;
            this.labelEditCancel.Text = "キャンセル";
            this.labelEditCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelEditCancel.Visible = false;
            this.labelEditCancel.Click += new System.EventHandler(this.labelEditCancel_Click);
            // 
            // labelMemoUpdate
            // 
            this.labelMemoUpdate.BackColor = System.Drawing.Color.LightCyan;
            this.labelMemoUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMemoUpdate.Location = new System.Drawing.Point(104, 2);
            this.labelMemoUpdate.Name = "labelMemoUpdate";
            this.labelMemoUpdate.Size = new System.Drawing.Size(47, 22);
            this.labelMemoUpdate.TabIndex = 4;
            this.labelMemoUpdate.Text = "更新";
            this.labelMemoUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMemoUpdate.Click += new System.EventHandler(this.labelMemoUpdate_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(214, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "メモ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // borderCheckBoxError
            // 
            this.borderCheckBoxError.BackColor = System.Drawing.SystemColors.Control;
            this.borderCheckBoxError.Location = new System.Drawing.Point(42, 2);
            this.borderCheckBoxError.Name = "borderCheckBoxError";
            this.borderCheckBoxError.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.borderCheckBoxError.Size = new System.Drawing.Size(62, 22);
            this.borderCheckBoxError.TabIndex = 8;
            this.borderCheckBoxError.TabStop = false;
            this.borderCheckBoxError.Text = "エラー";
            this.borderCheckBoxError.UseVisualStyleBackColor = false;
            this.borderCheckBoxError.CheckedChanged += new System.EventHandler(this.borderCheckBoxError_CheckedChanged);
            // 
            // InputMemoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.Controls.Add(this.textBoxMemo);
            this.Controls.Add(this.labelEditCancel);
            this.Controls.Add(this.labelMemoUpdate);
            this.Controls.Add(this.borderCheckBoxError);
            this.Controls.Add(this.label1);
            this.Name = "InputMemoControl";
            this.Size = new System.Drawing.Size(214, 163);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelEditCancel;
        private System.Windows.Forms.Label labelMemoUpdate;
        private System.Windows.Forms.Label label1;
        private BorderCheckBox borderCheckBoxError;
        public System.Windows.Forms.TextBox textBoxMemo;
    }
}
