﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Mejor.HyogoKoiki2022
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        public string numbering { get; set; } = string.Empty;                //電算管理番号(レセプト全国共通キー);

        public string insnumber { get; set; } = string.Empty;                //保険者番号;
        public string number { get; set; } = string.Empty;                   //被保険者証番号;
        public int sex { get; set; } = 0;                                    //性別;
        public DateTime birth { get; set; } = DateTime.MinValue;             //生年月日;
        public string drnumber { get; set; } = string.Empty;                 //医療機関番号;
        public int cym { get; set; } = 0;                                    //メホール請求年月;
        public int ym { get; set; } = 0;                                     //診療年月;
        public DateTime firstday { get; set; } = DateTime.MinValue;          //初検日;
        public int days { get; set; } = 0;                                   //実日数;
        public int ratio { get; set; } = 0;                                  //給付割合;
        public int total { get; set; } = 0;                                  //合計金額;
        public int charge { get; set; } = 0;                                 //請求金額;
        public int futan { get; set; } = 0;                                  //一時負担額;
        public string name { get; set; } = string.Empty;                     //ここでは画像名;

        public string zip { get; set; } = string.Empty;                      //ここでは空欄;
        public string prefname { get; set; } = string.Empty;                 //ここでは空欄;
        public string cityname { get; set; } = string.Empty;                 //ここでは空欄;
        public string add { get; set; } = string.Empty;                      //ここでは空欄;
        public string destname { get; set; } = string.Empty;                 //ここでは空欄;



        //兵庫広域からのレセデータインポート

        public static bool RefRece_Import(string fileName,WaitForm wf)
        {
            var l = CommonTool.CsvImportMultiCode(fileName);
            if (l == null) return false;

            
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;
            wf.LogPrint("柔整用データインポート");
            try
            {
                
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf, tran))
                        {
                            if (System.Windows.Forms.MessageBox.Show("中止しますか？") ==
                                System.Windows.Forms.DialogResult.Yes) return false;                            
                        }

                        if (item.Length < 24) continue;

                        //CM以外飛ばす
                        if (item[0] != "CM") continue;

                        int.TryParse(item[29], out int sex);            //性別
                        int.TryParse(item[42], out int days);           //診療実日数
                        
                        int.TryParse(item[20], out int ratio);          //給付割合
                        int.TryParse(item[55], out int total);          //合計金額
                                                            
                        var rr = new RefRece();
                        rr.numbering = item[63];                        //電算管理番号=レセプト全国共通キー
                        rr.insnumber = item[17];                        //保険者番号
                        rr.number = Microsoft.VisualBasic.Strings.StrConv(item[19], Microsoft.VisualBasic.VbStrConv.Narrow);
                        rr.sex = sex;
                        rr.birth = DateTimeEx.GetDateFromJstr7(item[30]);
                        rr.drnumber = item[5] + "7" + item[10];     //item[10].Remove(2) + item[10].Substring(3);//医療機関コード
                        rr.cym = DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[7])), 1);//処理年月
                        rr.ym = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[3]));//診療年月
                        rr.firstday = DateTime.MinValue;        // DateTimeEx.GetDateFromJstr7("4" + item[35]);//初検日→画面から取得予定
                        rr.days = days;
                        rr.ratio = ratio;
                        rr.total = total;
                        rr.charge = int.Parse(item[50]);
                        rr.futan = 0;
                        rr.name = item[9];//画像ファイル名 string.Empty;
                        rr.zip = string.Empty;
                        rr.prefname = string.Empty;
                        rr.cityname = string.Empty;
                        rr.add = string.Empty;
                        rr.destname = string.Empty;

                        if (!DB.Main.Insert(rr, tran)) return false;

                        wf.InvokeValue++;
                    }
                    tran.Commit();
                }

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
             
            }

        }

        

        /// <summary>
        /// 新システム用として使えそう
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool RefRece_ImportAHK(string fileName,WaitForm wf)
        {
            var l = CommonTool.CsvImportMultiCode(fileName);
            

            if (l == null) return false;

                                
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;

            try
            {
         
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf, tran))
                        {
                            if (System.Windows.Forms.MessageBox.Show("中止しますか？") ==
                                System.Windows.Forms.DialogResult.Yes) return false;
                        }

                        if (item.Length < 19) continue;
                        if (!int.TryParse(item[0], out int tmp)) continue;
                        int.TryParse(item[2], out int sex);//性別
                        int.TryParse(item[14], out int days);//診療実日数
                        int.TryParse(item[13], out int ratio);//給付割合
                        int.TryParse(item[12], out int total);//合計金額

                        var rr = new RefRece();
                        rr.numbering = item[1].Remove(16); //item[0];//電算管理番号=レセプト全国共通キー あはきの場合は画像ファイル名の方が適切？
                        rr.insnumber = item[4];     //保険者番号
                        rr.number = item[5];        //被保険者証番号
                        rr.sex = sex;
                        rr.birth = DateTimeEx.GetDateFromJstr7(item[3]);

                        //医療機関番号
                        //都道府県番号＋柔整あはきコード（７～９）＋医療機関コード
                        rr.drnumber = item[7];// item[5] + item[11] + item[7];//item[10].Remove(2) + item[10].Substring(3);//医療機関コード

                        rr.cym = int.Parse(item[10]);// DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[10])), 1);//処理年月
                        rr.ym = int.Parse(item[9]);// DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(item[9]));//診療年月
                        rr.firstday = DateTime.MinValue;// DateTimeEx.GetDateFromJstr7("4" + item[35]);//初検日→画面から取得予定
                        rr.days = days;
                        rr.ratio = ratio;
                        rr.total = total;
                        rr.charge = 0;//請求金額が載っていない
                        rr.futan = 0;
                        rr.name = item[6];
                        rr.zip = string.Empty;
                        rr.prefname = string.Empty;
                        rr.cityname = string.Empty;
                        rr.add = string.Empty;
                        rr.destname = item[0];//string.Empty; あはきの場合は電算処理番号？が短く完全じゃないぽいのでこっちに移す

                        if (!DB.Main.Insert(rr, tran)) return false;

                        wf.InvokeValue++;
                    }
                    tran.Commit();
                    
                }

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
            
            }
        }

        public static RefRece Select(string numbering)
        {
            //インジェクション回避
            //if (!(long.TryParse(numbering, out long temp))) return null;
            return DB.Main.Select<RefRece>($"numbering='{numbering}'").SingleOrDefault();
        }

        public static List<RefRece> SelectAll(int cym)
        {
            return DB.Main.Select<RefRece>($"cym={cym}").ToList();
        }

        public static List<RefRece> SelectAll()
        {
            return DB.Main.SelectAll<RefRece>().ToList();
        }

        /// <summary>
        /// マッチングができなかったデータを取得します
        /// </summary>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cym)
        {
            var sql = "SELECT r.numbering, r.insnumber, r.number, r.sex, r.birth, " +
                "r.drnumber, r.cym, r.ym, r.firstday, r.days, r.ratio, r.total, " +
                "r.charge, r.futan, r.name, r.zip, r.prefname, r.cityname, r.add, r.destname " +
                "FROM RefRece AS r " +
                "LEFT OUTER JOIN application AS a ON r.numbering=a.numbering " +
                "WHERE r.cym=:cym AND a.numbering IS NULL;";

            return DB.Main.Query<RefRece>(sql, new { cym = cym }).ToList();
        }

        public bool CreateNotMatchCsv(System.IO.StreamWriter sw)
        {
            try
            {
                var l = new List<string>();

                l.Add(numbering);
                l.Add(insnumber);
                l.Add(number);
                l.Add(sex.ToString());
                l.Add(DateTimeEx.GetIntJpDateWithEraNumber(birth).ToString());
                l.Add(drnumber);
                l.Add(cym.ToString());
                l.Add(ym.ToString());
                l.Add(firstday.ToString("yyyyMMdd"));
                l.Add(days.ToString());
                l.Add(ratio.ToString());
                l.Add(total.ToString());
                l.Add(charge.ToString());
                l.Add(futan.ToString());
                l.Add(name);
                l.Add(zip);
                l.Add(prefname);
                l.Add(cityname);
                l.Add(add);
                l.Add(destname);

                for (int i = 0; i < l.Count; i++)
                {
                    l[i] = "\"" + l[i] + "\"";
                }
                sw.WriteLine(string.Join(",", l));
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        public static bool AfterMatching(int cym, List<RefRece> refList)
        {
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("提供データを取得しています");
            var rrs = SelectAll(cym);
            var dic = new Dictionary<string, RefRece>();
            rrs.ForEach(x => dic.Add(x.numbering, x));
            
            wf.LogPrint("申請情報を取得しています");
            var apps = App.GetApps(cym);
            
            wf.LogPrint("関連付けを行なっています");
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
            wf.SetMax(apps.Count);
            int c = 0, nc = 0, oc=0;

            try
            {
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var app in apps)
                    {
                        wf.InvokeValue++;
                        //すでにマッチング済みかどうかを、名前の有無でチェック
                        if (!string.IsNullOrEmpty(app.PersonName))
                        {
                            oc++;
                            continue;
                        }

                        if (!dic.ContainsKey(app.Numbering))
                        {
                            //提供データにない申請書の扱い
                            if (app.YM > 0)
                            {
                                nc++;
                                app.StatusFlagSet(StatusFlag.支払保留);
                            }
                        }
                        else
                        {
                            //データの反映
                            c++;
                            var rr = dic[app.Numbering];
                            var ymd = DateTimeEx.ToDateTime(rr.ym * 100 + 1);
                            app.MediYear = DateTimeEx.GetJpYear(ymd);
                            app.MediMonth = rr.ym % 100;
                            app.InsNum = rr.insnumber;
                            app.HihoNum = rr.number;
                            app.Family = 2;
                            app.HihoType = 0;

                            //app.Sex = rr.gender;
                            //app.Birthday = rr.Birthday;
                            app.Sex = rr.sex;
                            app.Birthday = rr.birth;

                            app.PersonName = rr.name;
                            app.HihoZip = rr.zip;
                            app.HihoAdd = rr.prefname + rr.cityname + rr.add;
                            app.CountedDays = rr.days;
                            app.Total = rr.total;
                            app.Charge = rr.charge;
                            app.Partial = rr.futan;
                            app.NewContType = ymd.Year == rr.firstday.Year && ymd.Month == rr.firstday.Month ? NEW_CONT.新規 : NEW_CONT.継続;
                            app.FushoFirstDate1 = rr.firstday;
                            app.Ratio = rr.ratio;
                            app.DrNum = rr.drnumber;
                            app.ComNum = rr.numbering;

                            app.TaggedDatas.DestName = rr.destname == rr.name ? string.Empty : rr.destname;
                        }

                        if (!app.Update(0, App.UPDATE_TYPE.Null, tran))
                        {
                            System.Windows.Forms.MessageBox.Show("データの更新に失敗しました。処理を中止します");
                            return false;
                        }
                    }
                    tran.Commit();

                    System.Windows.Forms.MessageBox.Show(
                        $"今回マッチングした申請書: {c}枚\r\n" +
                        $"データなし申請書: {nc}枚\r\n" +
                        $"マッチング済み申請書: {oc}枚\r\n\r\n" +
                        "データの更新が完了しました。");
                    return true;
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }
    }


    /*
     * 広域より提供されたデータ順
     * 
        0	保険者番号
        1	被保険者番号
        2	性別
        3	和暦生年月日
        4	4,6,7で施術師番号 県番号
        5	不明
        6	4,6,7で施術師番号
        7	4,6,7で施術師番号
        8	電算番号
        9	審査年月
        10	診療年月
        11	初検年月日
        12	実日数
        13	給付割合？
        14	合計
        15	支給額
        16	一部負担金
        17	被保険者氏名
        18	市町村番号
        19	郵便番号
        20	県名
        21	市町村名
        22	住所
        23	送付先氏名
     */

}
