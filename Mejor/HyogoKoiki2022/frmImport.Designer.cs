﻿namespace Mejor.HyogoKoiki2022
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxJyu = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnFile1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImp = new System.Windows.Forms.Button();
            this.textBoxAHKOldSystem = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFile2 = new System.Windows.Forms.Button();
            this.gbA = new System.Windows.Forms.GroupBox();
            this.btnFile3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAHKNewSystem = new System.Windows.Forms.TextBox();
            this.btnFile5 = new System.Windows.Forms.Button();
            this.btnFile4 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHiho = new System.Windows.Forms.TextBox();
            this.textBoxSendTo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxClinic = new System.Windows.Forms.TextBox();
            this.btnFile6 = new System.Windows.Forms.Button();
            this.gbJ.SuspendLayout();
            this.gbA.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxJyu
            // 
            this.textBoxJyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJyu.Location = new System.Drawing.Point(187, 42);
            this.textBoxJyu.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxJyu.Multiline = true;
            this.textBoxJyu.Name = "textBoxJyu";
            this.textBoxJyu.Size = new System.Drawing.Size(523, 57);
            this.textBoxJyu.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gbJ.Controls.Add(this.btnFile1);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxJyu);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(21, 16);
            this.gbJ.Margin = new System.Windows.Forms.Padding(4);
            this.gbJ.Name = "gbJ";
            this.gbJ.Padding = new System.Windows.Forms.Padding(4);
            this.gbJ.Size = new System.Drawing.Size(807, 124);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            this.gbJ.Text = "柔整提供データ";
            // 
            // btnFile1
            // 
            this.btnFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile1.Location = new System.Drawing.Point(718, 50);
            this.btnFile1.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile1.Name = "btnFile1";
            this.btnFile1.Size = new System.Drawing.Size(64, 41);
            this.btnFile1.TabIndex = 3;
            this.btnFile1.Text = "...";
            this.btnFile1.UseVisualStyleBackColor = true;
            this.btnFile1.Click += new System.EventHandler(this.btnFile1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "柔整申請書データ";
            // 
            // btnImp
            // 
            this.btnImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImp.Location = new System.Drawing.Point(675, 591);
            this.btnImp.Margin = new System.Windows.Forms.Padding(4);
            this.btnImp.Name = "btnImp";
            this.btnImp.Size = new System.Drawing.Size(153, 45);
            this.btnImp.TabIndex = 3;
            this.btnImp.Text = "取込";
            this.btnImp.UseVisualStyleBackColor = true;
            this.btnImp.Click += new System.EventHandler(this.btnImp_Click);
            // 
            // textBoxAHKOldSystem
            // 
            this.textBoxAHKOldSystem.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHKOldSystem.Location = new System.Drawing.Point(187, 39);
            this.textBoxAHKOldSystem.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAHKOldSystem.Multiline = true;
            this.textBoxAHKOldSystem.Name = "textBoxAHKOldSystem";
            this.textBoxAHKOldSystem.Size = new System.Drawing.Size(523, 59);
            this.textBoxAHKOldSystem.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 47);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "旧システム用\r\nあはき申請書データ";
            // 
            // btnFile2
            // 
            this.btnFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile2.Location = new System.Drawing.Point(722, 49);
            this.btnFile2.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile2.Name = "btnFile2";
            this.btnFile2.Size = new System.Drawing.Size(64, 41);
            this.btnFile2.TabIndex = 3;
            this.btnFile2.Text = "...";
            this.btnFile2.UseVisualStyleBackColor = true;
            this.btnFile2.Click += new System.EventHandler(this.btnFile2_Click);
            // 
            // gbA
            // 
            this.gbA.BackColor = System.Drawing.Color.LightYellow;
            this.gbA.Controls.Add(this.btnFile3);
            this.gbA.Controls.Add(this.btnFile2);
            this.gbA.Controls.Add(this.label2);
            this.gbA.Controls.Add(this.label4);
            this.gbA.Controls.Add(this.textBoxAHKNewSystem);
            this.gbA.Controls.Add(this.textBoxAHKOldSystem);
            this.gbA.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbA.Location = new System.Drawing.Point(21, 165);
            this.gbA.Margin = new System.Windows.Forms.Padding(4);
            this.gbA.Name = "gbA";
            this.gbA.Padding = new System.Windows.Forms.Padding(4);
            this.gbA.Size = new System.Drawing.Size(807, 197);
            this.gbA.TabIndex = 1;
            this.gbA.TabStop = false;
            this.gbA.Text = "あはき提供データ";
            // 
            // btnFile3
            // 
            this.btnFile3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile3.Location = new System.Drawing.Point(722, 123);
            this.btnFile3.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile3.Name = "btnFile3";
            this.btnFile3.Size = new System.Drawing.Size(64, 41);
            this.btnFile3.TabIndex = 3;
            this.btnFile3.Text = "...";
            this.btnFile3.UseVisualStyleBackColor = true;
            this.btnFile3.Click += new System.EventHandler(this.btnFile3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 123);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "新システム用\r\nあはき申請書データ";
            // 
            // textBoxAHKNewSystem
            // 
            this.textBoxAHKNewSystem.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHKNewSystem.Location = new System.Drawing.Point(187, 114);
            this.textBoxAHKNewSystem.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAHKNewSystem.Multiline = true;
            this.textBoxAHKNewSystem.Name = "textBoxAHKNewSystem";
            this.textBoxAHKNewSystem.Size = new System.Drawing.Size(523, 59);
            this.textBoxAHKNewSystem.TabIndex = 0;
            // 
            // btnFile5
            // 
            this.btnFile5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile5.Location = new System.Drawing.Point(745, 457);
            this.btnFile5.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile5.Name = "btnFile5";
            this.btnFile5.Size = new System.Drawing.Size(64, 41);
            this.btnFile5.TabIndex = 3;
            this.btnFile5.Text = "...";
            this.btnFile5.UseVisualStyleBackColor = true;
            this.btnFile5.Click += new System.EventHandler(this.btnFileSendTo_Click);
            // 
            // btnFile4
            // 
            this.btnFile4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile4.Location = new System.Drawing.Point(745, 389);
            this.btnFile4.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile4.Name = "btnFile4";
            this.btnFile4.Size = new System.Drawing.Size(64, 41);
            this.btnFile4.TabIndex = 3;
            this.btnFile4.Text = "...";
            this.btnFile4.UseVisualStyleBackColor = true;
            this.btnFile4.Click += new System.EventHandler(this.btnFileHiho_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 389);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 32);
            this.label5.TabIndex = 2;
            this.label5.Text = "被保険者\r\nパンチデータ(CSV) ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 457);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "送付先\r\nパンチデータ(CSV)";
            // 
            // textBoxHiho
            // 
            this.textBoxHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHiho.Location = new System.Drawing.Point(205, 379);
            this.textBoxHiho.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHiho.Multiline = true;
            this.textBoxHiho.Name = "textBoxHiho";
            this.textBoxHiho.Size = new System.Drawing.Size(523, 59);
            this.textBoxHiho.TabIndex = 0;
            // 
            // textBoxSendTo
            // 
            this.textBoxSendTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSendTo.Location = new System.Drawing.Point(205, 446);
            this.textBoxSendTo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSendTo.Multiline = true;
            this.textBoxSendTo.Name = "textBoxSendTo";
            this.textBoxSendTo.Size = new System.Drawing.Size(523, 59);
            this.textBoxSendTo.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 524);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 32);
            this.label6.TabIndex = 2;
            this.label6.Text = "医療機関\r\nパンチデータ(CSV)";
            // 
            // textBoxClinic
            // 
            this.textBoxClinic.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxClinic.Location = new System.Drawing.Point(205, 513);
            this.textBoxClinic.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClinic.Multiline = true;
            this.textBoxClinic.Name = "textBoxClinic";
            this.textBoxClinic.Size = new System.Drawing.Size(523, 59);
            this.textBoxClinic.TabIndex = 0;
            // 
            // btnFile6
            // 
            this.btnFile6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile6.Location = new System.Drawing.Point(745, 524);
            this.btnFile6.Margin = new System.Windows.Forms.Padding(4);
            this.btnFile6.Name = "btnFile6";
            this.btnFile6.Size = new System.Drawing.Size(64, 41);
            this.btnFile6.TabIndex = 3;
            this.btnFile6.Text = "...";
            this.btnFile6.UseVisualStyleBackColor = true;
            this.btnFile6.Click += new System.EventHandler(this.btnFile6_Click);
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 649);
            this.Controls.Add(this.btnFile6);
            this.Controls.Add(this.btnFile5);
            this.Controls.Add(this.btnImp);
            this.Controls.Add(this.btnFile4);
            this.Controls.Add(this.gbA);
            this.Controls.Add(this.gbJ);
            this.Controls.Add(this.textBoxClinic);
            this.Controls.Add(this.textBoxSendTo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxHiho);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.gbA.ResumeLayout(false);
            this.gbA.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxJyu;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImp;
        private System.Windows.Forms.Button btnFile1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAHKOldSystem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFile2;
        private System.Windows.Forms.GroupBox gbA;
        private System.Windows.Forms.Button btnFile5;
        private System.Windows.Forms.Button btnFile4;
        private System.Windows.Forms.Button btnFile3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHiho;
        private System.Windows.Forms.TextBox textBoxAHKNewSystem;
        private System.Windows.Forms.TextBox textBoxSendTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxClinic;
        private System.Windows.Forms.Button btnFile6;
    }
}