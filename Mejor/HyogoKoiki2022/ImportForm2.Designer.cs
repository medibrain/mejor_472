﻿namespace Mejor.HyogoKoiki2022
{
    partial class ImportForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.lblJusei = new System.Windows.Forms.Label();
            this.lblAhk = new System.Windows.Forms.Label();
            this.lblClinic = new System.Windows.Forms.Label();
            this.lblSendto = new System.Windows.Forms.Label();
            this.lblHiho = new System.Windows.Forms.Label();
            this.buttonOldSystem = new System.Windows.Forms.Button();
            this.lblOldSystem = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(20, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(14, 136);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(57, 239);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(157, 27);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(57, 23);
            this.btnImport.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(157, 27);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "柔整";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(57, 56);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 27);
            this.button1.TabIndex = 5;
            this.button1.Text = "あはき";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(57, 122);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(157, 27);
            this.button2.TabIndex = 6;
            this.button2.Text = "医療機関";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(57, 155);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(157, 27);
            this.button3.TabIndex = 7;
            this.button3.Text = "送付先";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(57, 188);
            this.button4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 27);
            this.button4.TabIndex = 8;
            this.button4.Text = "被保険者";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lblJusei
            // 
            this.lblJusei.AutoSize = true;
            this.lblJusei.Location = new System.Drawing.Point(231, 30);
            this.lblJusei.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblJusei.Name = "lblJusei";
            this.lblJusei.Size = new System.Drawing.Size(137, 13);
            this.lblJusei.TabIndex = 9;
            this.lblJusei.Text = "完了 2019/05/17 11:10:12";
            // 
            // lblAhk
            // 
            this.lblAhk.AutoSize = true;
            this.lblAhk.Location = new System.Drawing.Point(231, 63);
            this.lblAhk.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAhk.Name = "lblAhk";
            this.lblAhk.Size = new System.Drawing.Size(31, 13);
            this.lblAhk.TabIndex = 10;
            this.lblAhk.Text = "完了";
            // 
            // lblClinic
            // 
            this.lblClinic.AutoSize = true;
            this.lblClinic.Location = new System.Drawing.Point(231, 129);
            this.lblClinic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClinic.Name = "lblClinic";
            this.lblClinic.Size = new System.Drawing.Size(31, 13);
            this.lblClinic.TabIndex = 11;
            this.lblClinic.Text = "完了";
            // 
            // lblSendto
            // 
            this.lblSendto.AutoSize = true;
            this.lblSendto.Location = new System.Drawing.Point(231, 162);
            this.lblSendto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendto.Name = "lblSendto";
            this.lblSendto.Size = new System.Drawing.Size(31, 13);
            this.lblSendto.TabIndex = 12;
            this.lblSendto.Text = "完了";
            // 
            // lblHiho
            // 
            this.lblHiho.AutoSize = true;
            this.lblHiho.Location = new System.Drawing.Point(231, 195);
            this.lblHiho.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHiho.Name = "lblHiho";
            this.lblHiho.Size = new System.Drawing.Size(31, 13);
            this.lblHiho.TabIndex = 13;
            this.lblHiho.Text = "完了";
            // 
            // buttonOldSystem
            // 
            this.buttonOldSystem.Location = new System.Drawing.Point(57, 89);
            this.buttonOldSystem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttonOldSystem.Name = "buttonOldSystem";
            this.buttonOldSystem.Size = new System.Drawing.Size(157, 27);
            this.buttonOldSystem.TabIndex = 5;
            this.buttonOldSystem.Text = "あはき旧システム用データ";
            this.buttonOldSystem.UseVisualStyleBackColor = true;
            this.buttonOldSystem.Click += new System.EventHandler(this.buttonOldSystem_Click);
            // 
            // lblOldSystem
            // 
            this.lblOldSystem.AutoSize = true;
            this.lblOldSystem.Location = new System.Drawing.Point(231, 96);
            this.lblOldSystem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOldSystem.Name = "lblOldSystem";
            this.lblOldSystem.Size = new System.Drawing.Size(31, 13);
            this.lblOldSystem.TabIndex = 10;
            this.lblOldSystem.Text = "完了";
            // 
            // ImportForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 301);
            this.Controls.Add(this.lblHiho);
            this.Controls.Add(this.lblSendto);
            this.Controls.Add(this.lblClinic);
            this.Controls.Add(this.lblOldSystem);
            this.Controls.Add(this.lblAhk);
            this.Controls.Add(this.lblJusei);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonOldSystem);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ImportForm2";
            this.Text = "兵庫広域データインポート";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lblJusei;
        private System.Windows.Forms.Label lblAhk;
        private System.Windows.Forms.Label lblClinic;
        private System.Windows.Forms.Label lblSendto;
        private System.Windows.Forms.Label lblHiho;
        private System.Windows.Forms.Button buttonOldSystem;
        private System.Windows.Forms.Label lblOldSystem;
    }
}