﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki2022
{
    public enum MATCH_STATUS { 未確定, マッチ済 }
    class MatchingApp
    {
        public static List<App> GetNotMatchApp(int cym)
        {
            
            //var where = "FROM application AS a " +
            //    "LEFT OUTER JOIN refrece AS k ON a.aid = k.aid " +
            //    "WHERE k.aid IS NULL " +
            //    $"AND a.cym={cym} " +
            //    "AND (a.ayear>0 OR a.ayear=-999)";
            var where = "FROM application AS a " +
                "WHERE a.comnum = '' " +
                $"AND a.cym={cym} " +
                "AND (a.aapptype = 7 OR a.aapptype = 8)"; //提供データがあるのはあはきのみ

            return App.InspectSelect(where);
        }

        public static List<App> GetOverlapApp(int cym)
        {
            //var where = "FROM application AS a " +
            //    "WHERE a.rrid IN( " +
            //        "SELECT a2.rrid FROM application AS a2 " +
            //        $"WHERE a2.cym={cym} " +
            //        "AND a2.ayear>0 " +
            //        "GROUP BY a2.rrid HAVING COUNT(a2.rrid) > 1) ";
            var where = "FROM application AS a " +
                "WHERE a.comnum IN( " +
                "SELECT a2.comnum FROM application AS a2 " +
                $"WHERE a2.cym={cym} " +
                "AND a2.ayear>0 " +
                "GROUP BY a2.comnum HAVING COUNT(a2.comnum) > 1) ";

            return App.InspectSelect(where);
        }

        //20220909_1 ito st /////マッチングチェック追加
        /// <summary>
        /// 保険者からの提供データのうち、どのScan画像とも紐づかなかったレコードを抽出します
        /// </summary>
        /// <param name="cym"></param>
        public static void GetNotMatchCsv(int cym)
        {
            var strsql =
                "with tmp as ( " +
                "SELECT i.*, a.aid, a.cym as acym FROM public.imp_ahk_oldsystem as i " +
                "left outer join application as a on i.f036_comnum = a.comnum " +
                $"where i.cym = {cym} and f004_recetype != '7' " + // 兵庫広域においては、7:柔整には紐づかないデータがたくさんあるため
                "order by i.f000_importid) " +
                "select * from tmp " +
                "where aid is null or aid = 0";

            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();

            DB.Command cmd = new DB.Command(strsql, tran);
            var lst = cmd.TryExecuteReaderList();

            var strlist = new List<string[]>();
            
            //for (int r = 0; r < lst.Count; r++)
            foreach (var item in lst)
            {
                var strary = new string[item.Length];
                for (int c = 0; c < item.Length; c++) strary[c] = item[c].ToString();
                strlist.Add(strary);
            }

            if (strlist.Count != 0) csvExport(strlist,cym);

            return;
        }

        private static bool csvExport(List<string[]> lstExp, int cym)
        {
            //出力csv名
            string strDir = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string strFileName = $"{strDir}\\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_兵庫広域あはき突合エラーリスト_{cym}.csv";

            //出力csv
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("shift-jis"));

            try
            {
                int c = 0;
                foreach (var item in lstExp)
                {
                    string strres = string.Empty;
                    foreach (var item2 in item)
                    {
                        strres += $"{item2},";
                    }
                    sw.WriteLine(strres);
                    c++;
                }

                sw.Close();

                MessageBox.Show($"提供データのうち {c} 件が突合できませんでした。\r\nCSVファイルをデスクトップに出力しました。","", MessageBoxButtons.OK, MessageBoxIcon.Information);


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }
        //20220909_1 ito end /////マッチングチェック追加
    }
}
