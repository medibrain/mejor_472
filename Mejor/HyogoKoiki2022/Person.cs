﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki2022
{
    class Person
    {
        [DB.DbAttribute.PrimaryKey]
        public string hihonum { get; set; } = string.Empty;             //被保険者番号;

        public string hihoname { get; set; } = string.Empty;            //被保険者名;
        public string kana { get; set; } = string.Empty;                //被保険者名カナ;
        public DateTime birthday { get; set; } = DateTime.MinValue;     //被保険者生年月日;
        public string zip { get; set; } = string.Empty;                 //被保険者郵便番号;
        public string address1 { get; set; } = string.Empty;            //被保険者都道府県;
        public string address2 { get; set; } = string.Empty;            //被保険者市町村名;
        public string address3 { get; set; } = string.Empty;            //被保険者住所;
        

        public static bool Import(string fileName,WaitForm wf)
        {
            

            wf.LogPrint("すでに登録された被保険者情報を取得しています");
            var pl = DB.Main.SelectAll<Person>();
            var dic = new Dictionary<string, Person>();

            foreach (var item in pl) dic.Add(item.hihonum, item);
                

            wf.LogPrint($"{pl.Count()}件の登録済み被保険者情報を取得しました");

            wf.LogPrint("CSVを読み込んでいます");
            var l = new List<Person>();
            var csv = CommonTool.CsvImportMultiCode(fileName);
                

            for (int i = 1; i < csv.Count; i++)
            {
                if (wf.Cancel)
                {
                    var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Asterisk);
                    if (cres == DialogResult.Yes) return false;
                    wf.Cancel = false;
                }
                var item = csv[i];

                if (item.Length < 8) continue;
                    
                var p = new Person();
                p.hihonum = item[0].Trim();
                p.hihoname = item[1].Trim();
                p.kana = item[2].Trim();

                p.birthday = DateTimeEx.ToDateTime(item[3].Trim());
                p.zip = item[4].Trim();
                p.address1 = item[5].Trim() ;
                p.address2 = item[6].Trim();
                p.address3 = item[7].Trim();


                l.Add(p);
            }

            wf.LogPrint($"CSVより{l.Count}件の被保険者情報を取得しました");

            wf.LogPrint("データベースへ登録/更新を行ないます");
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;
            int updateCount = 0;
            int insertCount = 0;

            using (var tran = DB.Main.CreateTransaction())
            {
                foreach (var item in l)
                {
                    if (wf.Cancel)
                    {
                        var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Asterisk);
                        if (cres == DialogResult.Yes) return false;
                        wf.Cancel = false;
                    }

                    wf.InvokeValue++;
                    if (dic.ContainsKey(item.hihonum))
                        
                    {
                        //全て一致する場合は飛ばす
                        if (dic[item.hihonum].hihonum == item.hihonum &&
                            dic[item.hihonum].hihoname == item.hihoname &&
                            dic[item.hihonum].zip == item.zip &&
                            dic[item.hihonum].address1 == item.address1 &&
                            dic[item.hihonum].address2 == item.address2 &&
                            dic[item.hihonum].address3 == item.address3)
                            continue;

                        if (!DB.Main.Update(item, tran))
                        {
                            MessageBox.Show("被保険者情報のインポートに失敗しました");
                            return false;
                        }
                        updateCount++;
                    }
                    else
                    {
                        if (!DB.Main.Insert(item, tran))
                        {
                            MessageBox.Show("被保険者情報のインポートに失敗しました");
                            return false;
                        }
                        insertCount++;
                    }
                }
                tran.Commit();
            }

            MessageBox.Show(
                $"新規登録：{insertCount}件\r\n" +
                $"更新：{updateCount}件\r\n\r\n" +
                $"被保険者情報のインポートが完了しました");
            return true;
            
        }

        public static Person GetPerson(string insdNum) =>
            DB.Main.Select<Person>(new { InsdNum = insdNum }).FirstOrDefault();

        public static Person GetPerson(string hnum,DateTime birth) =>
            DB.Main.Select<Person>(new { hihonum = hnum , birthday=birth}).FirstOrDefault();

    }
}
