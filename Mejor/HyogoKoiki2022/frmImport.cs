﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki2022
{
    public partial class frmImport : Form
    {
        public string strFileJyusei { get; set; } = string.Empty;
        public string strFileAHKOldSystem{ get; set; } = string.Empty;
        public string strFileAHKNewSystem { get; set; } = string.Empty;
        public string strFileClinic { get; set; } = string.Empty;
        public string strFileHiho { get; set; } = string.Empty;
        public string strFileSendTo { get; set; } = string.Empty;

        private static int cym;

        public frmImport(int _cym)
        {
            InitializeComponent();
            cym = _cym;

        }
        
        

        private void btnFile1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "柔整提供データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;
            textBoxJyu.Text= ofd.FileName;
            
        }

        private void btnFile2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "旧システム用_あはき";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHKOldSystem.Text = ofd.FileName;
        }

      
        private void btnFile3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "新システム用_あはき";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHKNewSystem.Text = ofd.FileName;
        }

        private void btnImp_Click(object sender, EventArgs e)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                strFileJyusei = textBoxJyu.Text.Trim();
                if (strFileJyusei == string.Empty) wf.LogPrint("柔整データ　ファイル指定無し、スキップします");
                else RefRece.RefRece_Import(strFileJyusei, wf);

                strFileAHKOldSystem = textBoxAHKOldSystem.Text.Trim();
                if (strFileAHKOldSystem == string.Empty) wf.LogPrint("旧システム用あはきデータ　ファイル指定無し、スキップします");
                else imp_AHK_oldSystem.import(strFileAHKOldSystem,cym, wf);

                strFileAHKNewSystem = textBoxAHKNewSystem.Text.Trim();
                if (strFileAHKNewSystem == string.Empty) wf.LogPrint("新システム用あはきデータ　ファイル指定無し、スキップします");
                else RefRece.RefRece_ImportAHK(strFileAHKNewSystem, wf);

                strFileHiho = textBoxHiho.Text.Trim();
                if (strFileHiho == string.Empty) wf.LogPrint("被保険者データ　ファイル指定無し、スキップします");
                else Person.Import(strFileHiho, wf);

                strFileSendTo = textBoxSendTo.Text.Trim();
                if (strFileSendTo == string.Empty) wf.LogPrint("送付先データ　ファイル指定無し、スキップします");
                else SendTo.Import(strFileSendTo, wf);


                strFileClinic = textBoxClinic.Text.Trim();
                if (strFileClinic == string.Empty) wf.LogPrint("医療機関データ　ファイル指定無し、スキップします");
                else Clinic.Import(strFileClinic, wf);
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
            
        }

        private void btnFileHiho_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "csvファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "被保険者データcsv";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxHiho.Text = ofd.FileName;
        }

        private void btnFileSendTo_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "送付先データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxSendTo.Text = ofd.FileName;
        }

        private void btnFile6_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title ="医療機関データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxClinic.Text = ofd.FileName;
        }
    }
}
