﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.HyogoKoiki2022
{
    class Clinic
    {
        [DB.DbAttribute.PrimaryKey]
        public string clinicnum { get; set; } = string.Empty;     //医療機関番号;
        public string name { get; set; } = string.Empty;          //医療機関名;
        public string zip { get; set; } = string.Empty;           //医療機関郵便番号;
        public string address { get; set; } = string.Empty;       //医療機関住所;

        //2022年度は不要と予想
        //public double Ido { get; set; }
        //public double Keido { get; set; }
        private int branchNo = 0;

        public static bool Import(string fileName,WaitForm wf)
        {
        
                        
            var csv = CommonTool.CsvImportMultiCode(fileName);
            var dic = new Dictionary<string, Clinic>();
            var pl = DB.Main.SelectAll<Clinic>();

            //今あるレコードを入れる
            foreach (var item in pl) dic.Add(item.clinicnum, item);

            List<Clinic> lst = new List<Clinic>();

            wf.SetMax(csv.Count);
            wf.InvokeValue = 0;
            wf.LogPrint("医療機関データ取込");
            //csv取得
            for (int i = 1; i < csv.Count; i++)
            {
                wf.InvokeValue++;
                var line = csv[i];
                if (line.Length < 4) continue;

                var c = new Clinic();
                c.clinicnum = line[0].Trim();
                c.name = line[1].Trim(); 
                c.zip = line[2].Trim(); 
                c.address = line[3].Trim();

                //緯度経度を取得しているが今回2022年度は不要か？
                //var g = GeoData.GetGeoData(c.Address);
                //if (g != null)
                //{
                //    c.Ido = g.Ido;
                //    c.Keido = g.Keido;
                //}
               

                if (dic.ContainsKey(c.clinicnum))
                {
                    //何をしようとしてた？
                    //if (dic[c.clinicnum].branchNo > c.branchNo) continue;
                    //dic[c.clinicnum] = c;
                    continue;
                }
                else
                {
                    lst.Add(c);
                    //dic.Add(c.clinicnum, c);
                }
            }

            using (var tran = DB.Main.CreateTransaction())
            {
                wf.InvokeValue = 0;
                wf.SetMax(lst.Count);
                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    wf.InvokeValue++;
                    if (dic.ContainsKey(item.clinicnum))
                    {
                        //全て同じ値の場合は飛ばす
                        if(dic[item.clinicnum].clinicnum==item.clinicnum &&
                            dic[item.clinicnum].name==item.name &&
                            dic[item.clinicnum].address==item.address &&
                            dic[item.clinicnum].zip==item.zip
                            )
                            continue;
                        else
                        {
                            //違いがあればupdate
                            if (DB.Main.Update<Clinic>(item, tran)) return false;
                            wf.LogPrint($"医療機関コード: {item.clinicnum} 更新");
                        }

                    }
                    else
                    {
                        //dbになければ登録
                        if (!DB.Main.Insert<Clinic>(item, tran)) return false;
                        wf.LogPrint($"医療機関コード: {item.clinicnum} 登録");
                        //if (!DB.Main.Insert(item.Value, tran)) return false;
                    }

                }
                tran.Commit();

            }
            return true;
        }

        public static Clinic GetClinic(string clinicNum) =>
            DB.Main.Select<Clinic>(new { clinicNum }).FirstOrDefault();
    }
}
