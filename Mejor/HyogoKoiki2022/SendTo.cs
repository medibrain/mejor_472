﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki2022
{
    class SendTo
    {
        [DB.DbAttribute.PrimaryKey]
        public string hihonum { get; set; } = string.Empty;               //被保険者番号;

        public string destname { get; set; } = string.Empty;              //送付先氏名;
        public string destkana { get; set; } = string.Empty;              //送付先氏名カナ;
        public string destzip { get; set; } = string.Empty;               //送付先郵便番号;
        public string destaddress1 { get; set; } = string.Empty;          //送付先都道府県;
        public string destaddress2 { get; set; } = string.Empty;          //送付先市町村名;
        public string destaddress3 { get; set; } = string.Empty;          //送付先住所;


        public static bool Import(string fileName,WaitForm wf)
        {
          
          
            wf.LogPrint("すでに登録された送付先情報を取得しています");
            var pl = DB.Main.SelectAll<SendTo>();
            var dic = new Dictionary<string, SendTo>();
            foreach (var item in pl) dic.Add(item.hihonum, item);
                

            wf.LogPrint($"{pl.Count()}件の登録済み送付先情報を取得しました");

            wf.LogPrint("CSVを読み込んでいます");
            var l = new List<SendTo>();
            var csv = CommonTool.CsvImportMultiCode(fileName);
                

            for (int i = 1; i < csv.Count; i++)
            {
                if (wf.Cancel)
                {
                    var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Asterisk);
                    if (cres == DialogResult.Yes) return false;
                    wf.Cancel = false;
                }
                var item = csv[i];

                if (item.Length < 7) continue;
                    
                var p = new SendTo();
                p.hihonum = item[0].Trim();
                p.destname = item[1].Trim();
                p.destkana = item[2].Trim();                    
                p.destzip = item[3].Trim();
                p.destaddress1 = item[4].Trim();
                p.destaddress2 = item[5].Trim() ;
                p.destaddress3 = item[6].Trim(); 
                    
                l.Add(p);
            }

            wf.LogPrint($"CSVより{l.Count}件の送付先情報を取得しました");

            wf.LogPrint("データベースへ登録/更新を行ないます");
            wf.SetMax(l.Count);
            wf.InvokeValue = 0;
            int updateCount = 0;
            int insertCount = 0;

            using (var tran = DB.Main.CreateTransaction())
            {
                //全削除ごインポート
                DB.Main.Excute("truncate table sendto",tran);

                foreach (var item in l)
                {
                    if (wf.Cancel)
                    {
                        var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Asterisk);
                        if (cres == DialogResult.Yes) return false;
                        wf.Cancel = false;
                    }

                    wf.InvokeValue++;

                    if (!DB.Main.Insert(item, tran))
                    {
                        MessageBox.Show("送付先情報のインポートに失敗しました");
                        return false;
                    }
                    insertCount++;
                        
                }
                tran.Commit();
            }

            MessageBox.Show(
                $"新規登録：{insertCount}件\r\n" +
                $"更新：{updateCount}件\r\n\r\n" +
                $"送付先情報のインポートが完了しました");
            return true;
            
        }

        public static SendTo GetSendTo(string hihonum) =>
            DB.Main.Select<SendTo>(new { hihonum }).FirstOrDefault();

    }
}
