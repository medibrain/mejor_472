﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Mejor.HyogoKoiki2022
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        
        private InputMode inputMode;
        
        int cym;
        private APP_TYPE currentAppType;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);       
        Point posHnum = new Point(400, 450); //(400, 0);      
        Point posTotal = new Point(0, 1400);

        Control[] ymControls, fushoControls, oryoControls,totalControls;

        /// <summary>
        /// 旧システム用あはきデータのリスト
        /// </summary>
        List<imp_AHK_oldSystem> lstAHKOld = new List<imp_AHK_oldSystem>();

        /// <summary>
        /// 柔整用RefReceリスト
        /// </summary>
        List<RefRece> lstRefrece = new List<RefRece>();


        public InputForm(ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM,verifyBoxHnum,};
            fushoControls = new Control[] { verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxBuisu, verifyBoxF1 ,verifyBoxF1FirstE,};
            oryoControls = new Control[] { checkBoxVisit, checkBoxVisitKasan, verifyBoxVisitTimes, };
            totalControls = new Control[] { verifyBoxTotal, };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox || item is CheckBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //20220617162556 furukawa st ////////////////////////
            //関数使う
            
            //表示調整
            initialize();

            //for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            //{
            //    dataGridViewPlist.Columns[j].Visible = false;
            //}
            //dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            //dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            //dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            //dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            //dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            //dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            //dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            //dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            //dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            //dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            //dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            //20220617162556 furukawa ed ////////////////////////

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //あはき提供データ取得
                if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                {
                    lstAHKOld = imp_AHK_oldSystem.selectAll(app.CYM);
                }
                else if (scan.AppType == APP_TYPE.柔整)
                {
                    lstRefrece = RefRece.SelectAll(app.CYM);

                }
                setApp(app);
            }

            focusBack(false);
        }


        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            
            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }


        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                    MatchingApp.GetNotMatchCsv(cym); //20220909_1 ito st /////マッチングチェック追加
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }


        /// <summary>
        /// 左グリッド
        /// </summary>
        private void initialize()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

      
        }

        #region 表示レセが変わったとき
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            //現在のレセタイプを取得      
            //if (inputMode == InputMode.MatchCheck) scanGroup = ScanGroup.Select(app.GroupID); //20220908_2 ito st /////マッチングチェック時用
            currentAppType = scan.AppType;

            setApp(app);
            focusBack(false);
        }
        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion

        #region 登録ボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }
        #endregion


        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //20220908_2 ito st /////
        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {

            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }

            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
            //focusBack(false);
        }
        //20220908_2 ito end /////


        /*
        private void selectRefRece(App app)
        {
            bsRefReceData.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefReceData.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時
                int cym = scan.CYM;
                int mym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
                l = RefRece.SerchByInput(cym, mym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefReceData.DataSource = l;
            bsRefReceData.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridRefRece.RowCount; i++)
                {
                    var rrid = (int)dataGridRefRece[nameof(RefRece.RrID), i].Value;
                    if (app.RrID == rrid)
                    {
                        //既に一意の広域データとマッチング済みの場合。
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridRefRece.CurrentCell = dataGridRefRece[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridRefRece.CurrentCell = null;
            }
        }

        */


        /// <summary>
        /// あはき提供データグリッド
        /// </summary>
        /// <param name="app"></param>
        private void CreateGrid_AHK(App app)
        {
            List<imp_AHK_oldSystem> lstimp = new List<imp_AHK_oldSystem>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            
            //リスト表示は入力項目で合致させる
            foreach (imp_AHK_oldSystem item in lstAHKOld)
            {
                //20221013_1 ito st /////
                //int totalMax = Convert.ToInt32(item.f030_total) + 1;    //20221118_1 ito 森安さんより、2円までゆらぎあり
                //int totalMin = Convert.ToInt32(item.f030_total) - 1;
                int totalMax = Convert.ToInt32(item.f030_total) + 2;
                int totalMin = Convert.ToInt32(item.f030_total) - 2;

                //被保番半角8桁、メホール請求年月、合計金額,診療年月で探す
                if (item.f003_hihonum == strhnum.PadLeft(8,'0') &&
                    item.cym == scan.CYM &&
                    //20221013_1 ito st /////
                    //item.f030_total == intTotal.ToString() &&
                    (totalMin <= intTotal && intTotal <= totalMax) &&
                    item.mediymad == intymad
                    )

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            labelMacthCheck.BackColor = Color.Pink;
            labelMacthCheck.Text = "マッチング無し";
            labelMacthCheck.Visible = true;

            if (lstimp.Count == 0) return;

            InitGrid_AHK();

            //1行確定の時は自動選択
            if (lstimp.Count == 1 && app.StatusFlagCheck(StatusFlag.未処理))
            {
                dgv.Rows[0].Selected = true;
            }
            else
            {

                //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
                if (lstimp.Count > 1) dgv.ClearSelection();


                //自動選択は確定したrrid
                if (app != null && app.RrID.ToString() != string.Empty)
                {
                    foreach (DataGridViewRow r in dgv.Rows)
                    {
                        //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                        string strcomnum = r.Cells[nameof(imp_AHK_oldSystem.f036_comnum)].Value.ToString();

                        //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                        if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                        {

                            if (r.Cells["f000_importid"].Value.ToString() == app.RrID.ToString())
                            {
                                r.Selected = true;
                            }

                            //提供データIDと違う場合の処理抜け
                            else
                            {
                                r.Selected = false;
                            }
                        }
                        else
                        {
                            //1回目終了時、2回目終了時は自動選択
                            if (
                                (app.ComNum == strcomnum && app.StatusFlagCheck(StatusFlag.入力済)) ||
                                (app.ComNum == strcomnum && app.StatusFlagCheck(StatusFlag.ベリファイ済))
                                )
                            {
                                //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                                if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                                else r.Selected = true;

                            }
                            //提供データIDと違う場合の処理                        
                            else
                            {
                                r.Selected = false;
                            }
                        }

                    }
                }
            }

            if (lstimp == null || lstimp.Count == 0)

            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }


            //マッチングOK条件に選択行が1行の場合も追加
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }

        /// <summary>
        /// あはき提供データグリッド初期化
        /// </summary>
        private void InitGrid_AHK()
        {
            if (lstAHKOld == null || lstAHKOld.Count==0) return;
            imp_AHK_oldSystem imp = lstAHKOld[0];
            

            dgv.Columns[nameof(imp.f000_importid)].Width = 60;
            dgv.Columns[nameof(imp.f001_chargeym)].Width = 60;
            dgv.Columns[nameof(imp.f002_rirekinum)].Width = 60;
            dgv.Columns[nameof(imp.f003_hihonum)].Width = 60;
            dgv.Columns[nameof(imp.f004_recetype)].Width = 60;
            dgv.Columns[nameof(imp.f005_kyufutype)].Width = 60;
            dgv.Columns[nameof(imp.f006_mediym)].Width = 60;
            dgv.Columns[nameof(imp.f007_clinicpref)].Width = 60;
            dgv.Columns[nameof(imp.f008_cliniccity)].Width = 60;
            dgv.Columns[nameof(imp.f009_clinicnum)].Width = 60;
            dgv.Columns[nameof(imp.f010_clinictype)].Width = 60;
            dgv.Columns[nameof(imp.f011_hokentype)].Width = 60;
            dgv.Columns[nameof(imp.f012_insnum)].Width = 60;
            dgv.Columns[nameof(imp.f013_gender)].Width = 50;
            dgv.Columns[nameof(imp.f014_pbirthdayw)].Width = 60;
            dgv.Columns[nameof(imp.f015_startdate1w)].Width = 60;
            dgv.Columns[nameof(imp.f016_startdate2w)].Width = 60;
            dgv.Columns[nameof(imp.f017_startdate3w)].Width = 60;
            dgv.Columns[nameof(imp.f018_tenki1)].Width = 60;
            dgv.Columns[nameof(imp.f019_tenki2)].Width = 60;
            dgv.Columns[nameof(imp.f020_counteddays)].Width = 60;
            dgv.Columns[nameof(imp.f021_ratio)].Width = 50;
            dgv.Columns[nameof(imp.f022_charge)].Width = 60;
            dgv.Columns[nameof(imp.f023_decidecharge)].Width = 60;
            dgv.Columns[nameof(imp.f024_partial)].Width = 60;
            dgv.Columns[nameof(imp.f025_genmenkbn)].Width = 60;
            dgv.Columns[nameof(imp.f026_genmenratio)].Width = 60;
            dgv.Columns[nameof(imp.f027_genmencharge)].Width = 60;
            dgv.Columns[nameof(imp.f028_shotokukbn)].Width = 60;
            dgv.Columns[nameof(imp.f029_ryouyouhinum)].Width = 60;
            dgv.Columns[nameof(imp.f030_total)].Width = 60;
            dgv.Columns[nameof(imp.f031_futancharge)].Width = 60;
            dgv.Columns[nameof(imp.f032_partialcharge)].Width = 60;
            dgv.Columns[nameof(imp.f033_selfcharge)].Width = 60;
            dgv.Columns[nameof(imp.f034_genbutsucharge)].Width = 60;
            dgv.Columns[nameof(imp.f035_jotaikbn)].Width = 60;
            dgv.Columns[nameof(imp.f036_comnum)].Width = 150;
            dgv.Columns[nameof(imp.f037_daisanshakbn)].Width = 60;
            dgv.Columns[nameof(imp.f038_shoriymd)].Width = 60;
            dgv.Columns[nameof(imp.f039_clinicname)].Width = 100;
            dgv.Columns[nameof(imp.f040_hihoname)].Width = 100;
            dgv.Columns[nameof(imp.cym)].Width = 60;
            dgv.Columns[nameof(imp.birthdayad)].Width = 60;
            dgv.Columns[nameof(imp.startdate1ad)].Width = 60;
            dgv.Columns[nameof(imp.startdate2ad)].Width = 60;
            dgv.Columns[nameof(imp.startdate3ad)].Width = 60;
            dgv.Columns[nameof(imp.shoriymdad)].Width = 60;
            dgv.Columns[nameof(imp.chargeymad)].Width = 60;
            dgv.Columns[nameof(imp.mediymad)].Width = 60;




            dgv.Columns[nameof(imp.f000_importid)].Visible = true;
            dgv.Columns[nameof(imp.f001_chargeym)].Visible = false;
            dgv.Columns[nameof(imp.f002_rirekinum)].Visible = false;
            dgv.Columns[nameof(imp.f003_hihonum)].Visible = true;
            dgv.Columns[nameof(imp.f004_recetype)].Visible = false;
            dgv.Columns[nameof(imp.f005_kyufutype)].Visible = false;
            dgv.Columns[nameof(imp.f006_mediym)].Visible = false;
            dgv.Columns[nameof(imp.f007_clinicpref)].Visible = false;
            dgv.Columns[nameof(imp.f008_cliniccity)].Visible = false;
            dgv.Columns[nameof(imp.f009_clinicnum)].Visible = false;
            dgv.Columns[nameof(imp.f010_clinictype)].Visible = false;
            dgv.Columns[nameof(imp.f011_hokentype)].Visible = false;
            dgv.Columns[nameof(imp.f012_insnum)].Visible = true;
            dgv.Columns[nameof(imp.f013_gender)].Visible = true;
            dgv.Columns[nameof(imp.f014_pbirthdayw)].Visible = false;
            dgv.Columns[nameof(imp.f015_startdate1w)].Visible = false;
            dgv.Columns[nameof(imp.f016_startdate2w)].Visible = false;
            dgv.Columns[nameof(imp.f017_startdate3w)].Visible = false;
            dgv.Columns[nameof(imp.f018_tenki1)].Visible = false;
            dgv.Columns[nameof(imp.f019_tenki2)].Visible = false;
            dgv.Columns[nameof(imp.f020_counteddays)].Visible = true;
            dgv.Columns[nameof(imp.f021_ratio)].Visible = true;
            dgv.Columns[nameof(imp.f022_charge)].Visible = false;
            dgv.Columns[nameof(imp.f023_decidecharge)].Visible = false;
            dgv.Columns[nameof(imp.f024_partial)].Visible = true;
            dgv.Columns[nameof(imp.f025_genmenkbn)].Visible = false;
            dgv.Columns[nameof(imp.f026_genmenratio)].Visible = false;
            dgv.Columns[nameof(imp.f027_genmencharge)].Visible = false;
            dgv.Columns[nameof(imp.f028_shotokukbn)].Visible = false;
            dgv.Columns[nameof(imp.f029_ryouyouhinum)].Visible = false;
            dgv.Columns[nameof(imp.f030_total)].Visible = true;
            dgv.Columns[nameof(imp.f031_futancharge)].Visible = false;
            dgv.Columns[nameof(imp.f032_partialcharge)].Visible = false;
            dgv.Columns[nameof(imp.f033_selfcharge)].Visible = false;
            dgv.Columns[nameof(imp.f034_genbutsucharge)].Visible = false;
            dgv.Columns[nameof(imp.f035_jotaikbn)].Visible = false;
            dgv.Columns[nameof(imp.f036_comnum)].Visible = true;
            dgv.Columns[nameof(imp.f037_daisanshakbn)].Visible = false;
            dgv.Columns[nameof(imp.f038_shoriymd)].Visible = false;
            dgv.Columns[nameof(imp.f039_clinicname)].Visible = true;
            dgv.Columns[nameof(imp.f040_hihoname)].Visible = true;
            dgv.Columns[nameof(imp.cym)].Visible = false;
            dgv.Columns[nameof(imp.birthdayad)].Visible = true;
            dgv.Columns[nameof(imp.startdate1ad)].Visible = true;
            dgv.Columns[nameof(imp.startdate2ad)].Visible = false;
            dgv.Columns[nameof(imp.startdate3ad)].Visible = false;
            dgv.Columns[nameof(imp.shoriymdad)].Visible = true;
            dgv.Columns[nameof(imp.chargeymad)].Visible = true;
            dgv.Columns[nameof(imp.mediymad)].Visible = true;



            dgv.Columns[nameof(imp.f000_importid)].HeaderText = "インポートID";
            dgv.Columns[nameof(imp.f001_chargeym)].HeaderText = "請求年月";
            dgv.Columns[nameof(imp.f002_rirekinum)].HeaderText = "履歴番号";
            dgv.Columns[nameof(imp.f003_hihonum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(imp.f004_recetype)].HeaderText = "レセプト種類";
            dgv.Columns[nameof(imp.f005_kyufutype)].HeaderText = "給付区分";
            dgv.Columns[nameof(imp.f006_mediym)].HeaderText = "診療年月";
            dgv.Columns[nameof(imp.f007_clinicpref)].HeaderText = "都道府県コード";
            dgv.Columns[nameof(imp.f008_cliniccity)].HeaderText = "医療機関市区町村コード";
            dgv.Columns[nameof(imp.f009_clinicnum)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(imp.f010_clinictype)].HeaderText = "診療科目コード";
            dgv.Columns[nameof(imp.f011_hokentype)].HeaderText = "保険種別コード";
            dgv.Columns[nameof(imp.f012_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(imp.f013_gender)].HeaderText = "性別";
            dgv.Columns[nameof(imp.f014_pbirthdayw)].HeaderText = "生年月日和暦";
            dgv.Columns[nameof(imp.f015_startdate1w)].HeaderText = "診療開始年月日１";
            dgv.Columns[nameof(imp.f016_startdate2w)].HeaderText = "診療開始年月日２";
            dgv.Columns[nameof(imp.f017_startdate3w)].HeaderText = "診療開始年月日３";
            dgv.Columns[nameof(imp.f018_tenki1)].HeaderText = "転帰１";
            dgv.Columns[nameof(imp.f019_tenki2)].HeaderText = "転帰２";
            dgv.Columns[nameof(imp.f020_counteddays)].HeaderText = "実日数";
            dgv.Columns[nameof(imp.f021_ratio)].HeaderText = "割合";
            dgv.Columns[nameof(imp.f022_charge)].HeaderText = "請求点";
            dgv.Columns[nameof(imp.f023_decidecharge)].HeaderText = "決定点";
            dgv.Columns[nameof(imp.f024_partial)].HeaderText = "負担額";
            dgv.Columns[nameof(imp.f025_genmenkbn)].HeaderText = "減免区分コード";
            dgv.Columns[nameof(imp.f026_genmenratio)].HeaderText = "減額割合";
            dgv.Columns[nameof(imp.f027_genmencharge)].HeaderText = "減額金額";
            dgv.Columns[nameof(imp.f028_shotokukbn)].HeaderText = "所得者区分コード";
            dgv.Columns[nameof(imp.f029_ryouyouhinum)].HeaderText = "療養費番号";
            dgv.Columns[nameof(imp.f030_total)].HeaderText = "費用額";
            dgv.Columns[nameof(imp.f031_futancharge)].HeaderText = "保険者負担額";
            dgv.Columns[nameof(imp.f032_partialcharge)].HeaderText = "一部負担相当額";
            dgv.Columns[nameof(imp.f033_selfcharge)].HeaderText = "自己負担額";
            dgv.Columns[nameof(imp.f034_genbutsucharge)].HeaderText = "高額療養費現物給付金額";
            dgv.Columns[nameof(imp.f035_jotaikbn)].HeaderText = "状態区分コード";
            dgv.Columns[nameof(imp.f036_comnum)].HeaderText = "電算管理番号";
            dgv.Columns[nameof(imp.f037_daisanshakbn)].HeaderText = "第三者区分コード";
            dgv.Columns[nameof(imp.f038_shoriymd)].HeaderText = "処理年月日";
            dgv.Columns[nameof(imp.f039_clinicname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(imp.f040_hihoname)].HeaderText = "被保険者氏名";
            dgv.Columns[nameof(imp.cym)].HeaderText = "	メホール請求年月";
            dgv.Columns[nameof(imp.birthdayad)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(imp.startdate1ad)].HeaderText = "診療開始年月日１西暦";
            dgv.Columns[nameof(imp.startdate2ad)].HeaderText = "診療開始年月日２西暦";
            dgv.Columns[nameof(imp.startdate3ad)].HeaderText = "診療開始年月日３西暦";
            dgv.Columns[nameof(imp.shoriymdad)].HeaderText = "処理年月日西暦";
            dgv.Columns[nameof(imp.chargeymad)].HeaderText = "請求年月";
            dgv.Columns[nameof(imp.mediymad)].HeaderText = "診療年月";

        }

       
        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            #region 入力チェック

            //20220617163144 furukawa st ////////////////////////
            //柔整は提供データから診療年月取得するのでチェックしない
            
            int mediy = 0;
            int medim = 0;

            if (scan.AppType != APP_TYPE.柔整)
            {
                mediy = verifyBoxY.GetIntValue();
                medim = verifyBoxM.GetIntValue();
                setStatus(verifyBoxM, medim.ToString() == string.Empty || medim < 0);
            }

            //  int mediy = verifyBoxY.GetIntValue();            
            //  int medim = verifyBoxM.GetIntValue();
            //  setStatus(verifyBoxM, medim.ToString()==string.Empty || medim < 0);
            //20220617163144 furukawa ed ////////////////////////

            //初検日
            DateTime firstDate = DateTime.MinValue;          
            setStatus(verifyBoxF1FirstE, !int.TryParse(verifyBoxF1FirstE.Text, out int resE));
            setStatus(verifyBoxF1FirstY, !int.TryParse(verifyBoxF1FirstY.Text, out int resY));
            setStatus(verifyBoxF1FirstM, !int.TryParse(verifyBoxF1FirstM.Text, out int resM));
            firstDate = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);
                                
           


            int bui = 0;//部位数

            if (scan.AppType == APP_TYPE.柔整 || scan.AppType == APP_TYPE.あんま)
            {
                //部位数
                setStatus(verifyBoxBuisu, (!int.TryParse(verifyBoxBuisu.Text, out bui) && verifyBoxBuisu.Text != string.Empty) ||
                                            (bui < 1 || bui > 10));
            }


            //負傷名チェック
            string strF1 = string.Empty;
            if (scan.AppType == APP_TYPE.鍼灸)
            {
                strF1= verifyBoxF1.Text.Trim();
                fusho1Check(verifyBoxF1);
           
            }


            int total = 0;
            int visittimes = 0;
            string strHnum = string.Empty;
            if (scan.AppType == APP_TYPE.鍼灸 || scan.AppType == APP_TYPE.あんま)
            {
                //被保険者証番号
                strHnum = verifyBoxHnum.Text.Trim();
                setStatus(verifyBoxHnum, strHnum == string.Empty);

                //合計
                setStatus(verifyBoxTotal, (!int.TryParse(verifyBoxTotal.Text, out total) && verifyBoxTotal.Text != string.Empty) ||
                                            (total < 1 || total > 200000));

                //往療回数
                //20220422103956 furukawa st ////////////////////////
                //往療有りチェックON時往療回数必須、Off時不要

                if (checkBoxVisit.Checked)
                {
                    visittimes = verifyBoxVisitTimes.GetIntValue();
                    setStatus(verifyBoxVisitTimes, verifyBoxVisitTimes.GetIntValue() < 0);
                }
                //setStatus(verifyBoxVisitTimes, (!int.TryParse(verifyBoxVisitTimes.Text, out visittimes) && verifyBoxVisitTimes.Text != string.Empty));
                //20220422103956 furukawa ed ////////////////////////
            }


            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
            #endregion


            //画面より登録 

            app.MediYear = mediy;
            app.MediMonth = medim;


            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                //app.HihoNum = strHnum;      //被保険者証番号
                app.HihoNum = strHnum.PadLeft(8,'0');   //20221205 ito
                app.VisitTimes = visittimes;//往療回数
                app.Total = total;          //合計
                app.Bui = bui;              //あんま部位数

            }

            //負傷名1
            if (scan.AppType == APP_TYPE.鍼灸)
            {
                app.FushoName1 = strF1;
            }
            
            //申請書タイプ            
            app.AppType = scan.AppType;            

            //部位数タグデータに持たせ、リスト作成画面で検索できるようにする                      
            app.TaggedDatas.count = bui;

            //初検日
            app.FushoFirstDate1 = firstDate;

            //往療有無
            app.Distance = checkBoxVisit.Checked == true ? 999 : 0;

            //往料加算のチェックがない場合、加算距離は0で登録。あはきは距離を入れないので無条件で-1
            if (currentAppType == APP_TYPE.あんま || currentAppType == APP_TYPE.鍼灸)
            {
                app.VisitAdd = checkBoxVisitKasan.Checked ? -1 : 0;
            }
            

            #region 広域データからデータ取得し、Applicaitionテーブルに反映
            //柔整提供データより
            if (scan.AppType == APP_TYPE.柔整)
            {
                //広域データからデータ取得                
                var rr = RefRece.Select(app.Numbering);//画像名=レセプト全国共通キー=ナンバリング
                if (rr == null)
                {
                    MessageBox.Show("広域提供データから一致する情報を見つけられませんでした。登録できません。\r\n",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                //20220617160408 furukawa st ////////////////////////
                //診療年月を提供データから取得
                
                if (rr.ym != 0)
                {
                    //20220714162841 furukawa st ////////////////////////
                    //refreceは西暦なのでmediyearにあわせて和暦に変換する必要がある                                       
                    app.MediYear = DateTimeEx.GetJpYearFromYM(int.Parse(rr.ym.ToString()));
                    //  app.MediYear = int.Parse(rr.ym.ToString().Substring(0, 4));
                    //20220714162841 furukawa ed ////////////////////////

                    app.MediMonth = int.Parse(rr.ym.ToString().Substring(4, 2));
                }
                else
                {
                    MessageBox.Show("広域提供データから診療年月を見つけられませんでした。登録できません。\r\n",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                //20220617160408 furukawa ed ////////////////////////


                app.InsNum = rr.insnumber;           //保険者番号	
                //app.HihoNum = rr.number;             //被保険者記号番号
                app.HihoNum = rr.number.PadLeft(8, '0');   //20221205 ito
                app.Sex = rr.sex;                    //受療者性別	
                app.Birthday = rr.birth;             //受療者生年月日	
                app.Ratio = rr.ratio;                //給付割合	
                app.Total = rr.total;                //合計額	
                app.Partial = rr.futan;              //一部負担額	
                app.Charge = rr.charge;              //請求額	
                app.CountedDays = rr.days;           //実日数	
                app.ComNum = rr.numbering;           //電算管理番号	

                app.ClinicNum = rr.drnumber;         //医療機関コード（本来は柔整師登録記号番号が入ってる？）
                
                app.Family = 2;
                app.HihoType = 0;

            }
            else if(scan.AppType==APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                //20220426154630 furukawa st ////////////////////////
                //あはきも提供データが無い場合登録不可
                
                if (dgv.Rows.Count == 0)
                {
                    MessageBox.Show("広域提供データから一致する情報を見つけられませんでした。登録できません。\r\n",
                       "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                //20220426154630 furukawa ed ////////////////////////

                if (dgv.Rows.Count >= 1)
                {
                   
                    app.InsNum = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f012_insnum)].Value.ToString();//保険者番号				
                    app.HihoName = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f040_hihoname)].Value.ToString();//被保険者名				
                    app.PersonName = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f040_hihoname)].Value.ToString();//受療者名=被保険者名				

                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f013_gender)].Value.ToString(), out int tmpsex)) app.Sex = tmpsex;//受療者性別				

                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.birthdayad)].Value.ToString());//受療者生年月日				

                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f021_ratio)].Value.ToString(), out int tmpratio)) app.Ratio = tmpratio;//給付割合				


                    app.FushoStartDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.startdate1ad)].Value.ToString());//　開始日1				
                    app.FushoStartDate2 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.startdate2ad)].Value.ToString());//　開始日2				
                    app.FushoStartDate3 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.startdate3ad)].Value.ToString());//　開始日3				

                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f024_partial)].Value.ToString(), out int tmppartial)) app.Partial = tmppartial;//一部負担額				

                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f022_charge)].Value.ToString(), out int tmpcharge)) app.Charge = tmpcharge;//請求額				

                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f020_counteddays)].Value.ToString(), out int tmpcounteddays)) app.CountedDays = tmpcounteddays;//実日数				

                    //20221013_1 ito st /////
                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f030_total)].Value.ToString(), out int tmptotal))
                    {
                        if(app.Total != tmptotal) app.OutMemo = "印刷合計金額＝" + app.Total.ToString(); //提供データの合計金額と紙に印刷された金額が異なる場合、Outmemoに入力金額を保存
                        app.Total = tmptotal; //合計金額は常に提供データを採用
                    }
                    //20221013_1 ito end /////

                    app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.shoriymdad)].Value.ToString();//処理年月                               
                    app.ComNum = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f036_comnum)].Value.ToString();//電算管理番号	

                    //医療機関番号=都道府県番号＋点数表番号？＋市町村番号＋医療機関番号
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f007_clinicpref)].Value.ToString() +
                        (scan.AppType == APP_TYPE.あんま ? "9" : "8").ToString() +
                        dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f008_cliniccity)].Value.ToString() +
                        dgv.CurrentRow.Cells[nameof(imp_AHK_oldSystem.f009_clinicnum)].Value.ToString();
                }
            }

            //柔整あはき共通処理
            //医療機関情報
            Clinic clinic=Clinic.GetClinic(app.ClinicNum);
            if (clinic != null)
            {
                app.ClinicAdd = clinic.address.Trim();
                app.ClinicName = clinic.name.Trim();
                app.ClinicNum = clinic.clinicnum;
                app.ClinicZip = clinic.zip;
            }

            //送付先
            SendTo sendto = SendTo.GetSendTo(app.HihoNum);
            if (sendto != null)
            {
                app.TaggedDatas.DestZip = sendto.destzip;
                app.TaggedDatas.DestName = sendto.destname.Trim();
                app.TaggedDatas.DestKana = sendto.destkana.Trim();
                app.TaggedDatas.DestAdd = sendto.destaddress1.Trim() + sendto.destaddress2.Trim() + sendto.destaddress3.Trim();
            }


            //被保険者情報
            Person person = Person.GetPerson(app.HihoNum, app.Birthday);
            if (person != null)
            {
                app.PersonName = person.hihoname.Trim();//兵庫広域では被保険者名＝受療者名
                app.HihoName = person.hihoname.Trim();
                app.HihoZip = person.zip;
                app.HihoAdd = person.address1.Trim() + person.address2.Trim() + person.address3.Trim();
                app.TaggedDatas.Kana = person.kana;
            }

            int mediyad = DateTimeEx.GetAdYearFromHs(mediy * 100 + medim);         
            //初検日で新規継続判定　初検年月＜＞診療年月の場合継続
            if (scan.AppType == APP_TYPE.柔整 || scan.AppType == APP_TYPE.鍼灸)
            {
                app.NewContType = mediyad == app.FushoFirstDate1.Year && medim == app.FushoFirstDate1.Month ? NEW_CONT.新規 : NEW_CONT.継続;
            }
            else
            {
                //あんまは初検日の入力をしないため判別できない
                app.NewContType = NEW_CONT.Null;
            }


            #endregion

            app.TaggedDatas.GeneralString8 = scan.Note1;//一括取込で、フォルダ名を申請書に反映
                        
            return true;
        }

   


        #region db更新
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            
            //numberingを控える           
            string tmpNumbering = app.Numbering;
            


            //続紙分類の追加
            switch (verifyBoxY.Text)
            {
                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    break;

                case clsInputKind.長期:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.AppType = APP_TYPE.長期;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;

                case clsInputKind.エラー:
                    //エラー
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;


                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    /*if (!checkAppFor901(app))
                    {
                        focusBack(true);
                        return false;
                    }
                    */
                    break;


                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;

                case clsInputKind.状態記入書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    
                    break;

                default:
                    //申請書の場合
                    
                    
                    /* いる？
                    if (dataGridRefRece.CurrentCell == null)
                    {
                        MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                            "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    */

                    int oldDid;
                    int.TryParse(app.Numbering, out oldDid);
                    var oldDis = app.Distance;

                    if (!checkApp(app))
                    {
                        focusBack(true);
                        return false;
                    }

                    break;

            }
            
            //控えたnumberingを戻す
            app.Numbering = tmpNumbering;
         


            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = App.UPDATE_TYPE.FirstInput;

                if (app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; 
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //AUXにApptype登録 scan.note1の内容を5番目に
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString(),"","","",app.TaggedDatas.GeneralString8)) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }
        #endregion



        #region 次のレセの表示
        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            if (inputMode == InputMode.MatchCheck) scanGroup = ScanGroup.Select(app.GroupID); //20220908_2 ito st /////マッチングチェック時用

            //表示リセット
            iVerifiableAllClear(panelRight);

            //20220628172509 furukawa st ////////////////////////
            //不要

            //コントロール表示初期化
            //pDoui.Visible = false;            
            //panelHnum.Visible = false;
            //panelTotal.Visible = false;
            //dgv.Visible = false;

            //20220628172509 furukawa ed ////////////////////////


            labelMacthCheck.Visible = false;

            dgv.DataSource = null;

            //入力ユーザー表示
            labelInputerName.Text = "入力:  " + User.GetUserName(app.Ufirst);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    if (scan.AppType == APP_TYPE.鍼灸)
                    {
                        var ocr = app.OcrData.Split(',');
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    }

                }
            }

            //20190416205234 furukawa st ////////////////////////
            //現在のレセタイプを取得
            currentAppType = scanGroup.AppType;
            //20190416205234 furukawa ed ////////////////////////

            //画像の表示
            setImage(app);
            changedReset(app);
        }
        #endregion


        #region 画像表示
        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="a"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //20221129 ito st プロジェクトKマスキング
                    Image img2 = img;

                    if (User.CurrentUser.belong == "外部業者")
                    {
                        var canvas = new Bitmap(img.Width, img.Height);
                        var graphic = Graphics.FromImage(canvas);
                        graphic.SetClip(new Rectangle(400, 450, 1300, 700)); //(300, 400, 1500, 800)
                        graphic.DrawImage(img, 0, 0, img.Width, img.Height);
                        img2 = canvas;
                    }
                    //20221129 ito end プロジェクトKマスキング


                    //全体表示
                    userControlImage1.SetImage(img2, fn);   //(img, fn);  //20221129 ito プロジェクトKマスキング
                    userControlImage1.SetPictureBoxFill();                  

                    scrollPictureControl1.SetImage(img2, fn);   //(img, fn);    //20221129 ito プロジェクトKマスキング

                    //20221129 ito st プロジェクトKマスキング
                    if (scan.AppType == APP_TYPE.柔整)
                    {
                        scrollPictureControl1.Ratio = 0.8f;
                        scrollPictureControl1.ScrollPosition = posHnum;
                    }
                    else
                    {
                        scrollPictureControl1.Ratio = 0.4f;
                        scrollPictureControl1.ScrollPosition = posYM;
                    }

                    //scrollPictureControl1.Ratio = 0.4f;
                    //scrollPictureControl1.ScrollPosition = posYM;
                    //20221129 ito ed プロジェクトKマスキング
                }

                labelImageName.Text = fn;
               // scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }
        #endregion

        #region 書面の種類を判定 

        private void setInputKind(App app)
        {
            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    verifyBoxY.Text = clsInputKind.続紙;
                    break;
                case (int)APP_SPECIAL_CODE.不要:
                    verifyBoxY.Text = clsInputKind.不要;
                    break;
                case (int)APP_SPECIAL_CODE.エラー:
                    verifyBoxY.Text = clsInputKind.エラー;
                    break;
                case (int)APP_SPECIAL_CODE.長期:
                    verifyBoxY.Text = clsInputKind.長期;
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    verifyBoxY.Text = clsInputKind.施術同意書;

                    break;
                case (int)APP_SPECIAL_CODE.同意書裏:
                    verifyBoxY.Text = clsInputKind.施術同意書裏;
                    break;
                case (int)APP_SPECIAL_CODE.施術報告書:
                    verifyBoxY.Text = clsInputKind.施術報告書;
                    break;
                case (int)APP_SPECIAL_CODE.状態記入書:
                    verifyBoxY.Text = clsInputKind.状態記入書;
                    break;

                default:
                    break;

            }
        }
        #endregion
               
        #region 入力データを画面にロード
        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App app)
        {
                      
            //書面判定を関数へ移行            
            setInputKind(app);

            if (verifyBoxY.Text.Trim() == string.Empty)
            {
                if (app.AppType != APP_TYPE.柔整)
                {
                    setValue(verifyBoxY, app.MediYear, true, false);
                    setValue(verifyBoxM, app.MediMonth, true, false);
                }
                else
                {
                    InitControl();
                }
            }

            //if (verifyBoxY.Text.Trim() == string.Empty) setValue(verifyBoxY, app.MediYear, true, false);
            //setValue(verifyBoxM, app.MediMonth, true, false);



            if (app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
            {
                setValue(verifyBoxHnum, app.HihoNum, true, false);

                //往療回数
                setValue(verifyBoxVisitTimes, app.VisitTimes, true, false);
                //合計
                setValue(verifyBoxTotal, app.Total, true, false);

                //あはき用グリッド
                CreateGrid_AHK(app);
            }
          

            //申請書
            ChangeVisitControls(false);
            ChangeVisitKasanControls(false);

            //往療
            checkBoxVisit.Checked = app.Distance == 999;



            //あはきの場合、往料加算チェックが入ると距離を-1としたので、0未満の場合にチェックを入れる            
            checkBoxVisitKasan.Checked = 0 > app.VisitAdd;
           

         
            //部位数            
            setValue(verifyBoxBuisu, app.TaggedDatas.count, true, false);
            
            //初検年月
            setDateValue(app.FushoFirstDate1, true, false, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);


            //負傷名1
            verifyBoxF1.Text = app.FushoName1;



        }

        #endregion


        /// <summary>
        /// 20220617180922 furukawa コントロール初期化関数        
        /// </summary>
        private void InitControl()
        {
            pDoui.Visible = false;
            panelHnum.Visible = false;
            panelTotal.Visible = false;
            dgv.Visible = false;


            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                case clsInputKind.不要:
                case clsInputKind.続紙:
                case clsInputKind.長期:

                case clsInputKind.施術同意書裏:
                case clsInputKind.施術報告書:
                case clsInputKind.状態記入書:
                    break;

                case clsInputKind.施術同意書:
                    //pDoui.Visible = true;//20220422104125 furukawa 同意年月日不要                    
                    break;

                default:
                    //申請書の場合                    
                    panelHnum.Visible = true;
                    panelTotal.Visible = true;
                    dgv.Visible = true;

                    switch (scan.AppType)
                    {
                        case APP_TYPE.柔整:
                            panelTotal.Visible = true;
                            foreach (Control c in panelTotal.Controls) c.Visible = false;
                            labelHnum.Visible = false;
                            verifyBoxHnum.Visible = false;
                            checkBoxVisit.Visible = true;
                            verifyBoxBuisu.Visible = true;
                            lblBui.Visible = true;
                            dgv.Visible = false;

                            labelMacthCheck.Visible = false;

                            lblFirst.Visible = true;
                            lblFirst2.Visible = true;
                            lblFirstY.Visible = true;
                            lblFirstM.Visible = true;
                            verifyBoxF1FirstE.Visible = true;
                            verifyBoxF1FirstY.Visible = true;
                            verifyBoxF1FirstM.Visible = true;

                            //20220617162806 furukawa st ////////////////////////
                            //柔整は提供データから診療年月取得するので不可視
                            verifyBoxM.Visible = false;
                            labelHs.Visible = false;
                            labelYear.Visible = false;
                            labelM.Visible = false;
                            //20220617162806 furukawa ed ////////////////////////
                            break;

                        case APP_TYPE.あんま:
                            verifyBoxF1.Visible = false;
                            labelF1.Visible = false;
                            break;

                        case APP_TYPE.鍼灸:
                            lblBui.Visible = false;
                            verifyBoxBuisu.Visible = false;
                            break;

                        default:
                            break;       
                    }


                    break;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //20220628172612 furukawa st ////////////////////////
            //テキストが変化したときにも判定が必要
            
            InitControl();
            //20220628172612 furukawa ed ////////////////////////



            //20220617180825 furukawa st ////////////////////////
            //verifyBoxY_Validatedに移動


            //      pDoui.Visible = false;
            //      panelHnum.Visible = false;
            //      panelTotal.Visible = false;
            //      dgv.Visible = false;


            //      switch (verifyBoxY.Text)
            //      {
            //          case clsInputKind.エラー:
            //          case clsInputKind.不要:
            //          case clsInputKind.続紙:
            //          case clsInputKind.長期:

            //          case clsInputKind.施術同意書裏:
            //          case clsInputKind.施術報告書:
            //          case clsInputKind.状態記入書:
            //              break;

            //          case clsInputKind.施術同意書:
            //              //pDoui.Visible = true;//20220422104125 furukawa 同意年月日不要                    
            //              break;

            //          default:
            //              //申請書の場合                    
            //              panelHnum.Visible = true;
            //              panelTotal.Visible = true;
            //              dgv.Visible = true;

            //              switch (scan.AppType) 
            //              {
            //                  case APP_TYPE.柔整:
            //                      panelTotal.Visible = true;
            //                      foreach (Control c in panelTotal.Controls) c.Visible = false;
            //                      labelHnum.Visible = false;
            //                      verifyBoxHnum.Visible = false;
            //                      checkBoxVisit.Visible=true;
            //                      verifyBoxBuisu.Visible = true;
            //                      lblBui.Visible = true;
            //                      dgv.Visible = false;

            //                      labelMacthCheck.Visible = false;

            //                      lblFirst.Visible = true;
            //                      lblFirst2.Visible = true;
            //                      lblFirstY.Visible = true;
            //                      lblFirstM.Visible = true;
            //                      verifyBoxF1FirstE.Visible = true;
            //                      verifyBoxF1FirstY.Visible = true;
            //                      verifyBoxF1FirstM.Visible = true;


            //                      //20220617162806 furukawa st ////////////////////////
            //                      //柔整は提供データから診療年月取得するので不可視

            //                      verifyBoxM.Visible = false;
            //                      labelHs.Visible = false;
            //                      labelYear.Visible = false;
            //                      labelM.Visible = false;
            //                      //20220617162806 furukawa ed ////////////////////////



            //                      break;

            //                  case APP_TYPE.あんま:

            //                      verifyBoxF1.Visible = false;
            //                      labelF1.Visible = false;

            //                      break;

            //                  case APP_TYPE.鍼灸:
            //                      lblBui.Visible = false;
            //                      verifyBoxBuisu.Visible = false;
            //                      break;
            //              }


            //              break;
            //      }

            //20220617180825 furukawa ed ////////////////////////
        }


        private void item_Enter(object sender, EventArgs e)
        {
            //20221129 ito st プロジェクトKマスキング
            if (scan.AppType == APP_TYPE.柔整)
            {
                scrollPictureControl1.Ratio = 0.8f;
                scrollPictureControl1.ScrollPosition = posHnum;
            }
            else
            {
                scrollPictureControl1.Ratio = 0.4f;
                if (ymControls.Contains(sender)) scrollPictureControl1.ScrollPosition = posYM;
                else if (totalControls.Contains(sender)) scrollPictureControl1.ScrollPosition = posTotal; 
                else if (oryoControls.Contains(sender)) scrollPictureControl1.ScrollPosition = posTotal;
            }


            //Point p;

            //if (ymControls.Contains(sender)) p = posYM;            
            //else if (totalControls.Contains(sender)) p = posTotal;
            //else return;

            //scrollPictureControl1.ScrollPosition = p;
            //20221129 ito ed プロジェクトKマスキング
        }

        //20220908_2 ito st /////マッチングチェックに対応させる
        //private void FormOCRCheck_Shown(object sender, EventArgs e)
        //{
        //    panelLeft.Width = this.Width - 1028;
        //    verifyBoxY.Focus();
        //}
        //20220908_2 ito end /////

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            //あはきの場合のみグリッド表示
            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                CreateGrid_AHK((App)bsApp.Current);
            }
        }


        #region 往療料関連コントロール
        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisit.Checked;
            ChangeVisitControls(bEnable);
            ChangeVisitKasanControls(checkBoxVisitKasan.Checked && bEnable);
        }

        private void checkBoxVisitKasan_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisitKasan.Checked;
            ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitControls(bool bEnable)
        {
            checkBoxVisitKasan.Enabled = bEnable;
            verifyBoxVisitTimes.Enabled = bEnable;//20220422103843 furukawa 往療有りチェックON時往療回数使用可能、Off時使用不可                                                  
            if (checkBoxVisitKasan.Checked) ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitKasanControls(bool bEnable)
        {
            if (currentAppType != APP_TYPE.柔整) bEnable = false;
        }

        #endregion

        #region 画像関連ボタン
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void verifyBoxY_Validated(object sender, EventArgs e)
        {
            InitControl();
            //20220617180655 furukawa st ////////////////////////
            //フォーカスを正しいコントロールに当てる
            
            if (scan.AppType == APP_TYPE.柔整) verifyBoxF1FirstE.Focus();
            else verifyBoxM.Focus();
            //20220617180655 furukawa ed ////////////////////////
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        #endregion

        #region 画像表示コントロール

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var t = (Control)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;                        
            else if (totalControls.Contains(t)) posTotal = pos;
        }
        #endregion

        #region 戻るボタン
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
        #endregion

    }
}
