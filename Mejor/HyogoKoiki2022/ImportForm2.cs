﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.HyogoKoiki2022
{
    public partial class ImportForm2 : Form
    {
        int cym = 0;

        [DB.DbAttribute.DifferentTableName("refrece")]
        class RefReceCounter
        {            
            public int Cym { get; set; }
            public int ReceCount { get; set; }

            public static List<RefReceCounter> GetCount()
            {
                var sql = "SELECT cym, count(cym) AS rececount FROM refrece " +
                    "GROUP BY cym ORDER BY cym DESC;";

                return DB.Main.Query<RefReceCounter>(sql).ToList();
            }
        }

        private  void Init()
        {
            lblClinic.Text = "未完了";
            lblClinic.ForeColor = Color.Gray;
            lblAhk.Text = "未完了";
            lblAhk.ForeColor = Color.Gray;

            //20220325144156 furukawa st ////////////////////////
            //旧システム用追加
            
            lblOldSystem.Text = "未完了";
            lblOldSystem.ForeColor = Color.Gray;
            //20220325144156 furukawa ed ////////////////////////


            lblHiho.Text = "未完了";
            lblHiho.ForeColor = Color.Gray;
            lblJusei.Text = "未完了";
            lblJusei.ForeColor = Color.Gray;
            lblSendto.Text = "未完了";
            lblSendto.ForeColor = Color.Gray;
        }

        public ImportForm2(int _cym)
        {
            InitializeComponent();
            dataGridView1.DataSource = RefReceCounter.GetCount();
            dataGridView1.Columns[nameof(RefReceCounter.Cym)].HeaderText = "処理年月";
            dataGridView1.Columns[nameof(RefReceCounter.Cym)].DefaultCellStyle.Format = "0000/00";
            dataGridView1.Columns[nameof(RefReceCounter.ReceCount)].HeaderText = "件数";

            this.cym = _cym;

            Init();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "柔整申請書点検用データ|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;

                //20190517133054 furukawa st ////////////////////////
                //インポート完了表示

                if (RefRece.RefRece_Import(f.FileName))
                {
                    lblJusei.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblJusei.ForeColor = Color.Blue;
                }
                else
                {
                    lblJusei.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblJusei.ForeColor = Color.Gray;
                }

                    //RefRece.RefRece_Import(f.FileName);
                //20190517133054 furukawa ed ////////////////////////
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            using (var f = new OpenFileDialog())
            {
                //20190517135421 furukawa st ////////////////////////
                //インポート完了表示
                
                f.Filter = "被保険者データ|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;

                if (Person.Import())
                {
                    lblHiho.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblHiho.ForeColor = Color.Blue;
                }
                else
                {
                    lblHiho.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblHiho.ForeColor = Color.Gray;
                }            
                    //Person.Import();
                //20190517135421 furukawa ed ////////////////////////
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //using (var f = new OpenFileDialog())
            //{
                //f.Filter = "医療機関データ|*.csv";
                //if (f.ShowDialog() != DialogResult.OK) return;

            //}

            //20190517134932 furukawa st ////////////////////////
            //インポート完了表示

            if (Clinic.Import())
            {
                lblClinic.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                lblClinic.ForeColor = Color.Blue;
            }
            else
            {
                lblClinic.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                lblClinic.ForeColor = Color.Gray;
            }
            //Clinic.Import();
            //20190517134932 furukawa ed ////////////////////////

        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "送付先データ|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;

                //20190517135038 furukawa st ////////////////////////
                //インポート完了表示

                if (SendTo.Import())
                {
                    lblSendto.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblSendto.ForeColor = Color.Blue;
                }
                else
                {
                    lblSendto.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblSendto.ForeColor = Color.Gray;
                }
                    //SendTo.Import();
                //20190517135038 furukawa ed ////////////////////////                
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "あはき申請書点検用データ|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;

                //20190517134337 furukawa st ////////////////////////
                //インポート完了表示


                if (RefRece.RefRece_ImportAHK(f.FileName))
                {
                    lblAhk.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblAhk.ForeColor = Color.Blue;

                }
                else
                {
                    lblAhk.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    lblAhk.ForeColor = Color.Gray;
                }

                    //RefRece.RefRece_ImportAHK(f.FileName);
                //20190517134337 furukawa ed ////////////////////////


            }
        }


        /// <summary>
        /// 20220325144235 furukawa 旧システム用csvファイルインポート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOldSystem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "旧システム用CSVファイル|*.csv";
            if (ofd.ShowDialog() == DialogResult.Cancel) return;

            string strFileName = ofd.FileName;
            if (strFileName == string.Empty) return;

            if(imp_AHK_oldSystem.import(strFileName, cym))
            {
                lblOldSystem.Text = "完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                lblOldSystem.ForeColor = Color.Blue;
            }
            else
            {
                lblOldSystem.Text = "未完了 " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                lblOldSystem.ForeColor = Color.Gray;
            }
            
        }
    }
}
