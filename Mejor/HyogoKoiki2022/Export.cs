﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

namespace Mejor.HyogoKoiki2022
{

    public partial class AppwithScan
    {
        public int no { get; set; } = 0;
        public int Aid { get; set; } = 0;
        public int scanid { get; set; } = 0;
        public int scanapptype { get; set; } = 0;
        public int aapptype { get; set; } = 0;
        public string comnum { get; set; } = string.Empty;

        public string filePath { get; set; } = string.Empty;////20220412170305 furukawa 「写」を入れた画像フォルダのプロパティ
                                                            

        public static Dictionary<int,AppwithScan> getAppWithScan(int cym)
        {
            Dictionary<int,AppwithScan> dicAppwithScan = new Dictionary<int, AppwithScan>();

            DB.Command cmd = DB.Main.CreateCmd($"select a.aid,s.sid,s.apptype,a.aapptype,a.comnum from application a inner join scan s " +
                $" on a.scanid=s.sid" +
                $" where a.cym={cym} and s.apptype <> {(int)APP_TYPE.柔整} " +
                $" order by a.aid ;");
            var lst = cmd.TryExecuteReaderList();
            int no = 0;

            foreach(var item in lst)
            {
                AppwithScan aws = new AppwithScan();
                aws.no = no;
                aws.Aid = int.Parse(item[0].ToString());
                aws.scanid=int.Parse(item[1].ToString());
                aws.scanapptype = int.Parse(item[2].ToString());
                aws.aapptype = int.Parse(item[3].ToString());
                aws.comnum = item[4].ToString();

                dicAppwithScan.Add(no,aws);
                no++;
            }

            cmd.Dispose();
            return dicAppwithScan;
        }



    }

    class Export
    {
        static int cym=0;

        /// <summary>
        /// マルチTIFF候補リスト（tiffファイルのパスが入る）
        /// </summary>
        public static List<string> lstImageFilePath = new List<string>();

        public static Dictionary<int, string> dicImageFileName = new Dictionary<int, string>();

        public static string strCYMw = string.Empty;

        /// <summary>
        /// 新旧データ一括出力
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool export_main(int _cym)
        {
            cym = _cym;
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            odd.ShowDialog();
            string strOutputPath = odd.Name;
            if (strOutputPath == string.Empty) return false;

            //メホール請求年月の和暦
            strCYMw = DateTimeEx.GetEraNumberYearFromYYYYMM(cym) + cym.ToString().Substring(4);

            //20220419101439 furukawa st ////////////////////////
            //ビューアデータ専用フォルダ            
            string strOutputPathforViewer = $"{strOutputPath}\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
            //20220419101439 furukawa ed ////////////////////////


            //納品データ専用フォルダ
            strOutputPath = $"{strOutputPath}\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力\\Data\\HYKA02\\Send\\{strCYMw}";
            
            if (!System.IO.Directory.Exists(strOutputPath)) System.IO.Directory.CreateDirectory(strOutputPath);
                        
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                //1.旧システム用出力
                //あはき　画像ファイル出力
                if (!ExportImage(strOutputPath, wf)) return false;
                //あはき　旧システム用csvテーブル作成
                if (!CreateExportTable(wf)) return false;
                //あはき　旧システム用csvファイル出力
                if (!CreateCSVFile(cym, strOutputPath, wf)) return false;

                //2.MejorViewerデータ出力               
                //20220419101514 furukawa st ////////////////////////
                //ビューアデータ専用フォルダに入れる                
                if (!ExportNewViewerData(cym, strOutputPathforViewer)) return false;
                //  if (!ExportNewViewerData(cym,strOutputPath)) return false;
                //20220419101514 furukawa ed ////////////////////////

                //WaterMarkAddToTif(strOutputPath);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }

        /// <summary>
        /// 20221212 ito MejorViewerへの移行に伴い、前業者システム用旧データの出力が不要に。柔整も不要との広域からの指示。
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool export_ahaki(int _cym)
        {
            cym = _cym;
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            odd.ShowDialog();
            string strOutputPath = odd.Name;
            if (strOutputPath == string.Empty) return false;

            //メホール請求年月の和暦
            strCYMw = DateTimeEx.GetEraNumberYearFromYYYYMM(cym) + cym.ToString().Substring(4);

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                //1.MejorViewerデータ出力               
                if (!ExportAhakiViewerData(cym,strOutputPath)) return false;

                //2.透かし追加
                if (!AddWaterMark(cym, strOutputPath)) return false;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }

        public static bool export_tiffWMtool(int _cym)
        {
            cym = _cym;
            OpenDirectoryDiarog odd = new OpenDirectoryDiarog();
            odd.ShowDialog();
            string strOutputPath = odd.Name;
            if (strOutputPath == string.Empty) return false;

            try
            {
                if (!AddWaterMark(cym, strOutputPath)) return false;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }



        /// <summary>
        /// あんま、はりでファイル名の種別、フォルダ名が異なる
        /// </summary>
        /// <param name="lstScan"></param>
        /// <param name="a"></param>        
        private static void getKind(int scan_apptype, out string strKind,out string strDir)
        {
            
            //application.apptypeでは続紙が続紙という種類になるのでscan.apptypeから取得
     
            switch (scan_apptype)
            {
                case (int)APP_TYPE.あんま: 
                    strKind= "A";
                    strDir = "あんま";
                    break;
                case (int)APP_TYPE.鍼灸: 
                    strKind= "H";
                    strDir = "鍼灸";
                    break;
                      
                default: 
                    strKind= "_";
                    strDir = "不明";
                    break;

            }
          

        }

        /// <summary>
        /// 旧システム用画像出力
        /// </summary>
        /// <param name="lstApp">柔整以外のappリスト</param>
        /// <param name="strDir"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(string strDir, WaitForm wf)
        {
            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}";           //tifコピー先フォルダ


            //List<AppwithScan> listaws=AppwithScan.getAppWithScan(cym);


            Dictionary<int,AppwithScan> dic= AppwithScan.getAppWithScan(cym);


            //20220412165946 furukawa st ////////////////////////
            //一時フォルダに全画像コピーし、画像一枚ずつに「写」の文字を入れる
            
            wf.SetMax(dic.Count);
            wf.InvokeValue = 0;

            string strTmpPath = $"{strDir}\\tmp";//一時コピーフォルダ
            string strWMPath = $"{strDir}\\wm";//写を入れた画像フォルダ

            if (!System.IO.Directory.Exists(strTmpPath)) System.IO.Directory.CreateDirectory(strTmpPath);
            if (!System.IO.Directory.Exists(strWMPath)) System.IO.Directory.CreateDirectory(strWMPath);


            //20220419101607 furukawa st ////////////////////////
            //先にクライアントPCに全コピーしてから写を入れる
            
            TiffUtility.FastCopy fstcpy = new TiffUtility.FastCopy();

            //ScanIDリスト作成
            List<int> lstscanid = new List<int>();
            for(int r=0;r< dic.Count;r++)
            {
                if (lstscanid.Contains(dic[r].scanid)) continue;
                lstscanid.Add(dic[r].scanid);
            }

            //ScanIDリストを元にフォルダごとコピー
            foreach (int intScanid in lstscanid)
            {
                string strScanIDFolder= $"{App.GetImageFolderPath(DB.GetMainDBName())}\\{Insurer.GetInsurer(Insurer.CurrrentInsurer.InsurerID).dbName}\\{intScanid}";
                wf.LogPrint($"{strScanIDFolder}コピー中...");
                fstcpy.DirCopy($"{strScanIDFolder}", strTmpPath);
            }

            //先にコピーしたファイルに写入れる
            for (int r = 0; r < dic.Count; r++) 
            {

                string strOrigFileName = App.GetApp(dic[r].Aid).GetImageFullPath();
                string strFileName = System.IO.Path.GetFileName(strOrigFileName);

                //写を入れた画像フォルダ　にあるファイルのパスを記憶
                dic[r].filePath = $"{strWMPath}\\{strFileName}";

                wf.LogPrint($"画像文字追加処理: {strFileName}");
                if (!StringAddToTif(strWMPath, $"{strTmpPath}\\{strFileName}")) return false;
                wf.InvokeValue++;
            }

            #region old
            //      for (int r=0;r<dic.Count;r++)
            //      {
            //          if (CommonTool.WaitFormCancelProcess(wf)) return false;

            //          wf.LogPrint($"画像文字追加処理: {dic[r].Aid}");
            //          string strOrigFileName = App.GetApp(dic[r].Aid).GetImageFullPath();
            //          string strFileName = System.IO.Path.GetFileName(strOrigFileName);

            //          fstcpy.FileCopy($"{strOrigFileName}", $"{strTmpPath}\\{strFileName}");
            //          //System.IO.File.Copy($"{strOrigFileName}", $"{strTmpPath}\\{strFileName}");

            //          //写を入れた画像フォルダ　にあるファイルのパスを記憶
            //          dic[r].filePath = $"{strWMPath}\\{strFileName}";

            //          if (!StringAddToTif(strWMPath, $"{strTmpPath}\\{strFileName}")) return false;
            //          wf.InvokeValue++;
            //      }
            #endregion


            //20220419101607 furukawa ed ////////////////////////



            //20220412165946 furukawa ed ////////////////////////

            //List<App> lstApp = App.GetAppsWithWhere($" where a.cym={cym} and a.aapptype<>6 ");
            //lstApp.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            //List<Scan> lstScan = Scan.GetScanListYM(cym);
            //lstScan.Sort((x, y) => x.SID.CompareTo(y.SID));

            dicImageFileName.Clear();
            lstImageFilePath.Clear();

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            wf.SetMax(dic.Count);
            //wf.SetMax(lstApp.Count);

            //ファイル名用連番
            int fileNo = 1;

            try
            {

                //0番目のファイルパス=>不要ぽい

                string strSatsuban = string.Empty;// App.GetApp(dic[0].Aid).TaggedDatas.GeneralString8;//lstApp[0].TaggedDatas.GeneralString8;
                string strDirKind = string.Empty;
                string strAHK = string.Empty;
                
            //    getScanAppType(dic[0].scanapptype,out strAHK,out strDirKind);

             //   strImageDir = $"{strDir}\\{strDirKind}";
             //   if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

                string strCYMw = DateTimeEx.GetEraNumberYearFromYYYYMM(cym).ToString() + cym.ToString().Substring(4);

            //    imageName = $"{strImageDir}\\SH{strCYMw}{strAHK}{strSatsuban.PadLeft(2,'0')}{fileNo.ToString().PadLeft(6,'0')}.tif";
            //    dicImageFileName.Add(dic[0].Aid, System.IO.Path.GetFileName(imageName));//AIDとファイル名を控える

                

                for (int r = 0; r < dic.Count; r++)
                {
                    

                    //レセプト全国共通キーが20桁の場合、申請書と見なす

                    if ((dic[r].comnum.Trim().Length>= 10) && (lstImageFilePath.Count != 0))
                    {
                        //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                        //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                        //その上で、この申請書をマルチTIFF候補リストに追加
                        wf.LogPrint($"申請書出力中:{imageName}");
                        if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                        {
                            System.Windows.Forms.MessageBox.Show(
                                System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                            return false;
                        }

                        lstImageFilePath.Clear();

                        //写しマークを入れたファイルパスを取得
                        lstImageFilePath.Add(dic[r].filePath);
                        //lstImageFilePath.Add(App.GetApp(dic[r].Aid).GetImageFullPath());

                        //申請書レコードのときに冊番とapptype取得
                        strSatsuban = App.GetApp(dic[r].Aid).TaggedDatas.GeneralString8.Substring(App.GetApp(dic[r].Aid).TaggedDatas.GeneralString8.Length-2,2);//<=冊番のフォーマットが不明な以上、判明してからの対応しか無理
                        getKind(dic[r].scanapptype, out strAHK, out strDirKind);

                        //フォルダ階層　GYYMM＋種別＋冊番
                        strImageDir = $"{strDir}\\{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}";
                        //strImageDir = $"{strDir}\\{strDirKind}\\{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}";
                        //strImageDir = $"{strDir}\\{strDirKind}";

                        if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

                        imageName = $"{strImageDir}\\SH{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}{fileNo.ToString().PadLeft(6, '0')}.tif";
                        dicImageFileName.Add(dic[r].Aid, System.IO.Path.GetFileName(imageName));//AIDとファイル名を控える

                        fileNo++;
                        
                    }
                    else if ((dic[r].comnum.Trim().Length >= 10) && (lstImageFilePath.Count == 0))
                    {
                        //申請書レコードのときに冊番とapptype取得
                        strSatsuban = App.GetApp(dic[r].Aid).TaggedDatas.GeneralString8.Substring(App.GetApp(dic[r].Aid).TaggedDatas.GeneralString8.Length - 2, 2);
                        getKind(dic[r].scanapptype,out strAHK,out strDirKind);

                        //フォルダ階層　GYYMM＋種別＋冊番
                        strImageDir = $"{strDir}\\{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}";
                        //strImageDir = $"{strDir}\\{strDirKind}\\{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}";
                        //strImageDir = $"{strDir}\\{strDirKind}";

                        if (!System.IO.Directory.Exists(strImageDir)) System.IO.Directory.CreateDirectory(strImageDir);

                        imageName = $"{strImageDir}\\SH{strCYMw}{strAHK}{strSatsuban.PadLeft(2, '0')}{fileNo.ToString().PadLeft(6, '0')}.tif";
                        dicImageFileName.Add(dic[r].Aid, System.IO.Path.GetFileName(imageName));//AIDとファイル名を控える

                        fileNo++;


                        //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
                        lstImageFilePath.Add(dic[r].filePath);
                        //lstImageFilePath.Add(App.GetApp(dic[r].Aid).GetImageFullPath());

                        wf.LogPrint($"申請書出力中:{imageName}");
                    }

                    else if (new int[] { -1, -3, -5, -6, -9, -11, -13, -14 }.Contains((int)dic[r].aapptype))
                    {
                        //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
                        
                        lstImageFilePath.Add(dic[r].filePath);
                        //lstImageFilePath.Add(App.GetApp(dic[r].Aid).GetImageFullPath());

                        wf.LogPrint($"申請書以外:{imageName}");
                    }


                    wf.InvokeValue++;

                }


                //最終画像出力
                //ループの最後の画像を出力
                if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                System.IO.Directory.Delete(strWMPath,true);
                System.IO.Directory.Delete(strTmpPath,true);                
            }

        }

        /// <summary>
        /// //20220412165828 furukawa 画像一枚ずつに「写」の文字を入れる
        /// </summary>
        /// <param name="strOutputPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static bool StringAddToTif(string strOutputPath,string fileName)
        {
            if (fileName == string.Empty) return true;
            string strNewFileName = $"{strOutputPath}\\{System.IO.Path.GetFileName(fileName)}";
            //string strNewFileName = $"{strOutputPath}\\{System.IO.Path.GetFileNameWithoutExtension(fileName)}\\{System.IO.Path.GetExtension(fileName)}";
            string watermarkText = "写";

            Bitmap bmp = new Bitmap(fileName);
            Bitmap newbmp = new Bitmap(bmp.Width, bmp.Height);
          
            Graphics g = Graphics.FromImage(newbmp);            
            g.DrawImage(bmp, 0, 0, bmp.Width, bmp.Height);            
            
            Brush brush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 130.0f, FontStyle.Bold, GraphicsUnit.Pixel);
            try
            {
                g.DrawString(watermarkText, font, brush, float.Parse((bmp.Width * 0.9).ToString()), 30);//帳票右上に印字
                //g.DrawString(watermarkText, font, brush, 2230, 30);//帳票右上に印字

                //ファイルサイズが大きくなるのでクローンして1bppカラーで保存
                Bitmap bb = newbmp.Clone(new Rectangle(0, 0, newbmp.Width, newbmp.Height), System.Drawing.Imaging.PixelFormat.Format1bppIndexed);



                //20220505104140 furukawa st ////////////////////////
                //解像度をオリジナルから変更すると印刷したときのサイズが変わって文句言われる
                
                bb.SetResolution(300, 300);

                //      20220428105511 furukawa st ////////////////////////
                //      解像度が200dpi以上に指定されている
                //      bb.SetResolution(200, 200);

                //      20220428105511 furukawa ed ////////////////////////

                //20220505104140 furukawa ed ////////////////////////

                bb.Save(strNewFileName, System.Drawing.Imaging.ImageFormat.Tiff);
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                brush.Dispose();
                font.Dispose();
                g.Dispose();
                bmp.Dispose();
                newbmp.Dispose();
            }

        }


        /// <summary>
        /// csvファイル作成
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strOutputPath"></param>
        /// <returns></returns>
        private static bool CreateCSVFile(int cym,string strOutputPath,WaitForm wf)
        {

            //ヘッダ文字列
            StringBuilder sbheader = new StringBuilder();
            sbheader.Append("\"電算管理番号\"");
            sbheader.Append(",\"画像ファイル名\"");
            sbheader.Append(",\"性別コード\"");
            sbheader.Append(",\"生年月日和暦\"");
            sbheader.Append(",\"保険者番号\"");
            sbheader.Append(",\"被保険者番号\"");
            sbheader.Append(",\"被保険者氏名\"");
            sbheader.Append(",\"医療機関番号\"");
            sbheader.Append(",\"医療機関名\"");
            sbheader.Append(",\"診療年月\"");
            sbheader.Append(",\"請求年月\"");
            sbheader.Append(",\"レセプト種類コード \"");
            sbheader.Append(",\"費用金額\"");
            sbheader.Append(",\"給付割合\"");
            sbheader.Append(",\"施術日数\"");
            sbheader.Append(",\"往療料の有無\"");
            sbheader.Append(",\"往療日数\"");
            sbheader.Append(",\"施術部位数\"");
            sbheader.Append(",\"傷病名\"");



            //ファイル名
            string strFileName = $"{strOutputPath}\\RHSKYSNSS_KEYJH_{strCYMw}.csv";//仕様書より

            StreamWriter sw = new StreamWriter(strFileName,false,System.Text.Encoding.GetEncoding("shift-jis"));
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"SELECT				");
            sb.AppendLine($"  f001_comnum       ");
            sb.AppendLine($"  , f002_image      ");
            sb.AppendLine($"  , f003_gender     ");
            sb.AppendLine($"  , f004_pbirthdayw ");
            sb.AppendLine($"  , f005_insnum     ");
            sb.AppendLine($"  , f006_hihonum    ");
            sb.AppendLine($"  , f007_hihoname   ");
            sb.AppendLine($"  , f008_clinicnum  ");
            sb.AppendLine($"  , f009_clinicname ");
            sb.AppendLine($"  , f010_mediym     ");
            sb.AppendLine($"  , f011_chargeym   ");
            sb.AppendLine($"  , f012_rezecode   ");
            sb.AppendLine($"  , f013_total      ");
            sb.AppendLine($"  , f014_ratio      ");
            sb.AppendLine($"  , f015_counteddays");
            sb.AppendLine($"  , f016_visit      ");
            sb.AppendLine($"  , f017_visittimes ");
            sb.AppendLine($"  , f018_bui        ");
            sb.AppendLine($"  , f019_f1name     ");
            sb.AppendLine($"FROM                ");
            sb.AppendLine($"  exp_ahk_oldsystem ");
            sb.AppendLine($"  where cym={cym} order by f000_exportid  ");

            
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            var lst = cmd.TryExecuteReaderList();
            wf.SetMax(lst.Count);
            wf.LogPrint($"あはきcsvファイル作成中");
            wf.InvokeValue = 0;
            try
            {
                //ヘッダ文字列
                sw.WriteLine(sbheader.ToString());

                foreach(var item in lst)
                {
                    //1行
                    string strLine = string.Empty;

                    //列回し
                    for(int c = 0; c < item.Length; c++)
                    {
                        strLine += $"\"{item[c]}\",";
                    }

                    //最後のカンマ取る
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    
                    sw.WriteLine(strLine);

                }


                wf.LogPrint($"あはきcsvファイル作成完了");
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
            }
        }



        /// <summary>
        /// exp_ahk_oldsystemテーブル作成
        /// </summary>
        /// <returns></returns>
        private static bool CreateExportTable(WaitForm wf)
        {

            
            List<App> lstApp = App.GetApps(cym);
            lstApp.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            List<imp_AHK_oldSystem> lstAHK_OldSystem = imp_AHK_oldSystem.selectAll(cym);
            lstAHK_OldSystem.Sort((x, y) => x.f036_comnum.CompareTo(y.f036_comnum));

     

            //今月分を先に削除
            DB.Transaction tran = DB.Main.CreateTransaction();
            wf.LogPrint($"CSV出力テーブル{cym}削除");

            DB.Command cmd = DB.Main.CreateCmd($"delete from exp_ahk_oldsystem where cym={cym}",tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            try
            {
                wf.LogPrint($"CSV出力テーブル作成...");
                foreach (App a in lstApp)
                {
                    if (a.AppType == APP_TYPE.柔整) continue;

                    foreach(imp_AHK_oldSystem imp in lstAHK_OldSystem)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf))
                        {
                            tran.Rollback();
                            return false;
                        }

                        //合致キー（仕様書より）で合致したレコードはexp_ahk_oldsystemに登録
                        if(imp.mediymad==a.YM &&
                            
                            //20220413094434 furukawa st ////////////////////////
                            //合致条件のヒホバンは8桁に                            
                            imp.f003_hihonum==a.HihoNum.PadLeft(8,'0') &&
                            //  imp.f003_hihonum==a.HihoNum &&
                            //20220413094434 furukawa ed ////////////////////////

                            imp.f030_total == a.Total.ToString())
                        {

                            exp_AHK_oldSystem e = new exp_AHK_oldSystem();
                            e.f001_comnum = imp.f036_comnum;

                            //控えたファイル名を取得する
                            if (dicImageFileName.ContainsKey(a.Aid))
                            {
                                e.f002_image = dicImageFileName[a.Aid];
                            }
                          
                            e.f003_gender = imp.f013_gender;
                            e.f004_pbirthdayw = imp.f014_pbirthdayw;
                            e.f005_insnum = imp.f012_insnum;
                            e.f006_hihonum = imp.f003_hihonum;
                            e.f007_hihoname = imp.f040_hihoname.PadRight(40,'　'); //Nvarcher2なので

                            //20220404175317 furukawa st ////////////////////////
                            //医療機関コードの組み合わせ漏れ
                            
                            e.f008_clinicnum = imp.f007_clinicpref + imp.f004_recetype+ imp.f008_cliniccity+imp.f009_clinicnum;
                            //e.f008_clinicnum = imp.f009_clinicnum;
                            //20220404175317 furukawa ed ////////////////////////


                            e.f009_clinicname = imp.f039_clinicname.PadRight(30, '　');//Nvarcher2なので
                            e.f010_mediym = a.YM.ToString();
                            e.f011_chargeym = imp.chargeymad.ToString();
                            e.f012_rezecode = imp.f004_recetype;
                            e.f013_total =a.Total.ToString();
                            e.f014_ratio = imp.f021_ratio;
                            e.f015_counteddays = imp.f020_counteddays;
                            e.f016_visit = a.Distance==999 ? "1": "0";
                            e.f017_visittimes = a.VisitTimes.ToString();
                            e.f018_bui = a.Bui.ToString();
                            e.f019_f1name = a.FushoName1;
                            e.cym = cym;
                            e.aid = a.Aid;
                            if (!DB.Main.Insert<exp_AHK_oldSystem>(e, tran)) return false;
                            wf.LogPrint($"CSV出力テーブル登録 AID {a.Aid}");

                        }
                        else continue;
                        
                    }

                }
                tran.Commit();

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

     
        /// <summary>
        /// 新ビューア形式出力
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool ExportNewViewerData(int cym,string strOutputPath)
        {
            string infoFileName;
          
            
            var dir = $"{strOutputPath}\\ViewerData";
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);

            infoFileName = $"{dir}\\Info.txt";
            
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");

                var apps = App.GetApps(cym);

                //提供データ取得
                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                var hs = new HashSet<string>();
                rrs.ForEach(rr => hs.Add(rr.numbering));

                //マッチング確認
                wf.LogPrint("マッチングを確認しています");
                foreach (var app in apps)
                {
                    if (app.YM < 0) continue;
                    if (!hs.Contains(app.ComNum))
                    {
                        wf.LogPrint($"突合データがない申請書があります AID:{app.Aid}");
                        continue;
                    }
                }

                //突合なしデータ
                wf.LogPrint("突合なしデータを抽出しています");
                string notMatchCSV = dir + $"\\突合なし広域データ{cym}.csv";
                var notList = RefRece.GetNotMatchDatas(cym);
                using (var sw = new StreamWriter(notMatchCSV, false, Encoding.UTF8))
                {
                    sw.WriteLine("\"電算管理番号\",\"保険者番号\"," +
                        "\"被保険者番号\",\"性別\",\"生年月日\",\"医療機関番号\"," +
                        "\"請求年月\",\"診療年月\",\"初検日\",\"実日数\"," +
                        "\"負担率\",\"合計額\",\"請求額\",\"負担額\",\"氏名\"," +
                        "\"郵便番号\",\"都道府県\",\"市町村\",\"住所\",\"送付先氏名\"");

                    foreach (var item in notList)
                    {
                        if (!item.CreateNotMatchCsv(sw))
                        {
                            var res = MessageBox.Show("エラーが発生しました。出力を続けますか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (res != DialogResult.OK) return false;
                        }
                    }
                }

                //ビューアデータ
                wf.LogPrint("新形式のViewerデータで処理します");
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nマッチングなしデータ数 :{notList.Count}" + 
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }


        public static bool ExportAhakiViewerData(int cym, string strOutputPath)
        {
            string infoFileName;


            var dir = $"{strOutputPath}\\ViewerData";
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);

            infoFileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");

                //20221212 ito st あはきのみでOK
                //var apps = App.GetApps(cym);
                List<App> apps = App.GetAppsWithWhere($" where a.cym={cym} and a.aapptype <>6");

                /*
                //提供データ取得
                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                var hs = new HashSet<string>();
                rrs.ForEach(rr => hs.Add(rr.numbering));

                //マッチング確認
                wf.LogPrint("マッチングを確認しています");
                foreach (var app in apps)
                {
                    if (app.YM < 0) continue;
                    if (!hs.Contains(app.ComNum))
                    {
                        wf.LogPrint($"突合データがない申請書があります AID:{app.Aid}");
                        continue;
                    }
                }

                //突合なしデータ
                wf.LogPrint("突合なしデータを抽出しています");
                string notMatchCSV = dir + $"\\突合なし広域データ{cym}.csv";
                var notList = RefRece.GetNotMatchDatas(cym);
                using (var sw = new StreamWriter(notMatchCSV, false, Encoding.UTF8))
                {
                    sw.WriteLine("\"電算管理番号\",\"保険者番号\"," +
                        "\"被保険者番号\",\"性別\",\"生年月日\",\"医療機関番号\"," +
                        "\"請求年月\",\"診療年月\",\"初検日\",\"実日数\"," +
                        "\"負担率\",\"合計額\",\"請求額\",\"負担額\",\"氏名\"," +
                        "\"郵便番号\",\"都道府県\",\"市町村\",\"住所\",\"送付先氏名\"");

                    foreach (var item in notList)
                    {
                        if (!item.CreateNotMatchCsv(sw))
                        {
                            var res = MessageBox.Show("エラーが発生しました。出力を続けますか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (res != DialogResult.OK) return false;
                        }
                    }
                }
                */
                //20221212 ito end

                //ビューアデータ
                wf.LogPrint("新形式のViewerデータで処理します");
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    //$"\r\n広域からのデータ総数   :{rrs.Count}" +    //20221212 ito
                    //$"\r\nマッチングなしデータ数 :{notList.Count}" + //20221212 ito
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }


        public static bool AddWaterMark(int cym, string strOutputPath)
        {
            string infoFileName;

            var dir = $"{strOutputPath}\\ViewerData";
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);

            infoFileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("Tiff画像への透かしを追加しています");

                DirectoryInfo folderInfo;
                if (Directory.Exists(dir + "\\img")) folderInfo = new DirectoryInfo(dir + "\\img");
                else return false;

                FileInfo[] allFiles = folderInfo.GetFiles("*.tif", SearchOption.AllDirectories);

                var saveDir = dir + "\\wm\\";
                if (!System.IO.Directory.Exists(saveDir)) System.IO.Directory.CreateDirectory(saveDir);

                wf.SetMax(allFiles.Length);
                int i = 0;

                foreach (var itemFile in allFiles)
                {
                    TiffUtility.AddWaterMarkToTif(itemFile.FullName, saveDir + itemFile.Name);
                    wf.InvokeValue = i;
                    i++;

                    if (i % 50 == 0 ) GC.Collect();
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                $"\r\nTiff画像への透かし追加処理を終了しました。\r\n";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("Tiff画像への透かし追加処理に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }

    }
}
