﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.HyogoKoiki2022
{
        
    public partial class imp_AHK_oldSystem
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                                 //インポートID;

        public string f001_chargeym { get; set; } = string.Empty;                   //請求年月;
        public string f002_rirekinum { get; set; } = string.Empty;                  //履歴番号;
        public string f003_hihonum { get; set; } = string.Empty;                    //被保険者番号;
        public string f004_recetype { get; set; } = string.Empty;                   //レセプト種類コード;
        public string f005_kyufutype { get; set; } = string.Empty;                  //給付区分コード;
        public string f006_mediym { get; set; } = string.Empty;                     //診療年月;
        public string f007_clinicpref { get; set; } = string.Empty;                 //都道府県コード;
        public string f008_cliniccity { get; set; } = string.Empty;                 //医療機関市区町村コード;
        public string f009_clinicnum { get; set; } = string.Empty;                  //医療機関コード;
        public string f010_clinictype { get; set; } = string.Empty;                 //診療科目コード;
        public string f011_hokentype { get; set; } = string.Empty;                  //保険種別コード;
        public string f012_insnum { get; set; } = string.Empty;                     //保険者番号;
        public string f013_gender { get; set; } = string.Empty;                     //性別コード;
        public string f014_pbirthdayw { get; set; } = string.Empty;                 //生年月日和暦;
        public string f015_startdate1w { get; set; } = string.Empty;                //診療開始年月日１;
        public string f016_startdate2w { get; set; } = string.Empty;                //診療開始年月日２;
        public string f017_startdate3w { get; set; } = string.Empty;                //診療開始年月日３;
        public string f018_tenki1 { get; set; } = string.Empty;                     //転帰１;
        public string f019_tenki2 { get; set; } = string.Empty;                     //転帰２;
        public string f020_counteddays { get; set; } = string.Empty;                //診療実日数;
        public string f021_ratio { get; set; } = string.Empty;                      //給付割合;
        public string f022_charge { get; set; } = string.Empty;                     //請求点数;
        public string f023_decidecharge { get; set; } = string.Empty;               //決定点数;
        public string f024_partial { get; set; } = string.Empty;                    //一部負担額;
        public string f025_genmenkbn { get; set; } = string.Empty;                  //減免区分コード;
        public string f026_genmenratio { get; set; } = string.Empty;                //減額割合;
        public string f027_genmencharge { get; set; } = string.Empty;               //減額金額;
        public string f028_shotokukbn { get; set; } = string.Empty;                 //所得者区分コード;
        public string f029_ryouyouhinum { get; set; } = string.Empty;               //療養費番号;
        public string f030_total { get; set; } = string.Empty;                      //費用金額;
        public string f031_futancharge { get; set; } = string.Empty;                //保険者負担額;
        public string f032_partialcharge { get; set; } = string.Empty;              //一部負担相当額;
        public string f033_selfcharge { get; set; } = string.Empty;                 //自己負担額;
        public string f034_genbutsucharge { get; set; } = string.Empty;             //高額療養費現物給付金額;
        public string f035_jotaikbn { get; set; } = string.Empty;                   //状態区分コード;
        public string f036_comnum { get; set; } = string.Empty;                     //電算管理番号;
        public string f037_daisanshakbn { get; set; } = string.Empty;               //第三者区分コード;
        public string f038_shoriymd { get; set; } = string.Empty;                    //処理年月日;
        public string f039_clinicname { get; set; } = string.Empty;                 //医療機関名（漢字）;
        public string f040_hihoname { get; set; } = string.Empty;                   //被保険者氏名（漢字）;

        public int cym { get; set; } = 0;                                           //メホール請求年月;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;               //生年月日西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;             //診療開始年月日１西暦;
        public DateTime startdate2ad { get; set; } = DateTime.MinValue;             //診療開始年月日２西暦;
        public DateTime startdate3ad { get; set; } = DateTime.MinValue;             //診療開始年月日３西暦;
        public DateTime shoriymdad { get; set; } = DateTime.MinValue;               //処理年月日西暦;
        public int chargeymad { get; set; } = 0;                                    //請求年月西暦;
        public int mediymad { get; set; } = 0;                                      //診療年月西暦;
        #endregion

        
        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="cym">CYM</param>
        /// <param name="wf">WaitForm</param>
        /// <returns></returns>
        public static bool import(string strFileName,int cym,WaitForm wf)
        {
            
            List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);
            DB.Transaction tran = DB.Main.CreateTransaction();

            wf.LogPrint($"あはき旧システム用{cym}削除");
            StringBuilder sb = new StringBuilder();


            DB.Command cmd = null;

            //20220509110858 furukawa st ////////////////////////
            //今月分があったらシーケンス戻す
            
            sb.AppendLine($"select * from imp_ahk_oldsystem where cym={cym};");
            cmd = DB.Main.CreateCmd(sb.ToString(), tran);
            var tmplst = cmd.TryExecuteReaderList();
            if (tmplst.Count > 0)
            {

                //シーケンスを戻す
                sb.Clear();
                sb.AppendLine($"select setval('imp_ahk_oldsystem_seq',(select min(f000_importid) from imp_ahk_oldsystem  where cym={cym})-1,true);");
                cmd = DB.Main.CreateCmd(sb.ToString(), tran);
                if (!cmd.TryExecuteNonQuery()) return false;

            }

            //          //シーケンスを戻す
            //          sb.AppendLine($"select setval('imp_ahk_oldsystem_seq',(select min(f000_importid) from imp_ahk_oldsystem  where cym={cym})-1,true);");
            //          DB.Command cmd = DB.Main.CreateCmd(sb.ToString(), tran);
            //          if (!cmd.TryExecuteNonQuery()) return false;


            //20220509110858 furukawa ed ////////////////////////


            //削除
            sb.Clear();
            sb.AppendLine($"delete from imp_ahk_oldsystem where cym={cym};");
            cmd =DB.Main.CreateCmd(sb.ToString(),tran);
            if(!cmd.TryExecuteNonQuery()) return false;
            


            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            wf.LogPrint("あはき旧システム用CSV取込中...");

            try
            {
                foreach(var item in lst)
                {
                    //1列目が数字でない場合飛ばす
                    if (!int.TryParse(item[0], out int tmp)) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }

                    wf.LogPrint($"被保険者証番号: {item[2].ToString()}");

                    imp_AHK_oldSystem imp = new imp_AHK_oldSystem();
                    imp.f001_chargeym = item[0].ToString();
                    imp.f002_rirekinum = item[1].ToString();
                    imp.f003_hihonum = item[2].ToString();
                    imp.f004_recetype = item[3].ToString();
                    imp.f005_kyufutype = item[4].ToString();
                    imp.f006_mediym = item[5].ToString();
                    imp.f007_clinicpref = item[6].ToString();
                    imp.f008_cliniccity = item[7].ToString();
                    imp.f009_clinicnum = item[8].ToString();
                    imp.f010_clinictype = item[9].ToString();
                    imp.f011_hokentype = item[10].ToString();
                    imp.f012_insnum = item[11].ToString();
                    imp.f013_gender = item[12].ToString();
                    imp.f014_pbirthdayw = item[13].ToString();
                    imp.f015_startdate1w = item[14].ToString();
                    imp.f016_startdate2w = item[15].ToString();
                    imp.f017_startdate3w = item[16].ToString();
                    imp.f018_tenki1 = item[17].ToString();
                    imp.f019_tenki2 = item[18].ToString();
                    imp.f020_counteddays = item[19].ToString();
                    imp.f021_ratio = item[20].ToString();
                    imp.f022_charge = item[21].ToString();
                    imp.f023_decidecharge = item[22].ToString();
                    imp.f024_partial = item[23].ToString();
                    imp.f025_genmenkbn = item[24].ToString();
                    imp.f026_genmenratio = item[25].ToString();
                    imp.f027_genmencharge = item[26].ToString();
                    imp.f028_shotokukbn = item[27].ToString();
                    imp.f029_ryouyouhinum = item[28].ToString();
                    imp.f030_total = item[29].ToString();
                    imp.f031_futancharge = item[30].ToString();
                    imp.f032_partialcharge = item[31].ToString();
                    imp.f033_selfcharge = item[32].ToString();
                    imp.f034_genbutsucharge = item[33].ToString();
                    imp.f035_jotaikbn = item[34].ToString();
                    imp.f036_comnum = item[35].ToString();
                    imp.f037_daisanshakbn = item[36].ToString();
                    imp.f038_shoriymd = item[37].ToString();
                    imp.f039_clinicname = item[38].ToString();
                    imp.f040_hihoname = item[39].ToString();

                    imp.cym = cym;
                    
                    imp.birthdayad = DateTimeEx.GetDateFromJstr7(imp.f014_pbirthdayw);
                                       
                    imp.startdate1ad = cngDate(imp.f015_startdate1w);
                    imp.startdate2ad = cngDate(imp.f016_startdate2w);
                    imp.startdate3ad = cngDate(imp.f017_startdate3w);

                    imp.shoriymdad = cngDate(imp.f038_shoriymd);

                    //元々西暦yyyymm型と推測する=>西暦確定
                    if (int.TryParse(imp.f001_chargeym, out int tmpchargeym)) imp.chargeymad = tmpchargeym;
                    if (int.TryParse(imp.f006_mediym, out int tmpmediymadm)) imp.mediymad = tmpmediymadm;

                    if (!DB.Main.Insert<imp_AHK_oldSystem>(imp, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 西暦変換
        /// </summary>
        /// <param name="inputstring">元になる文字列</param>
        /// <returns>西暦</returns>
        private static DateTime cngDate(string inputstring)
        {
            //数字で0でなく、8，7桁の場合は西暦に変換、無ければ0001-01-01
            if (int.TryParse(inputstring.Trim(), out int tmpout))
            {
                if (tmpout == 0) return  DateTimeEx.DateTimeNull;
                else
                {
                    if (tmpout.ToString().Length == 8) return DateTimeEx.ToDateTime(tmpout);
                    else if (tmpout.ToString().Length == 7) return DateTimeEx.GetDateFromJInt7(tmpout);
                    else return  DateTime.MinValue;
                }
            }
            else return DateTimeEx.DateTimeNull;
        }




        public static List<imp_AHK_oldSystem> selectAll(int cym)
        {
            List<imp_AHK_oldSystem> lst = new List<imp_AHK_oldSystem>();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_AHK_oldSystem where cym={cym} order by f000_importid");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_AHK_oldSystem imp = new imp_AHK_oldSystem();

                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_chargeym = item[1].ToString();
                    imp.f002_rirekinum = item[2].ToString();
                    imp.f003_hihonum = item[3].ToString();
                    imp.f004_recetype = item[4].ToString();
                    imp.f005_kyufutype = item[5].ToString();
                    imp.f006_mediym = item[6].ToString();
                    imp.f007_clinicpref = item[7].ToString();
                    imp.f008_cliniccity = item[8].ToString();
                    imp.f009_clinicnum = item[9].ToString();
                    imp.f010_clinictype = item[10].ToString();
                    imp.f011_hokentype = item[11].ToString();
                    imp.f012_insnum = item[12].ToString();
                    imp.f013_gender = item[13].ToString();
                    imp.f014_pbirthdayw = item[14].ToString();
                    imp.f015_startdate1w = item[15].ToString();
                    imp.f016_startdate2w = item[16].ToString();
                    imp.f017_startdate3w = item[17].ToString();
                    imp.f018_tenki1 = item[18].ToString();
                    imp.f019_tenki2 = item[19].ToString();
                    imp.f020_counteddays = item[20].ToString();
                    imp.f021_ratio = item[21].ToString();
                    imp.f022_charge = item[22].ToString();
                    imp.f023_decidecharge = item[23].ToString();
                    imp.f024_partial = item[24].ToString();
                    imp.f025_genmenkbn = item[25].ToString();
                    imp.f026_genmenratio = item[26].ToString();
                    imp.f027_genmencharge = item[27].ToString();
                    imp.f028_shotokukbn = item[28].ToString();
                    imp.f029_ryouyouhinum = item[29].ToString();
                    imp.f030_total = item[30].ToString();
                    imp.f031_futancharge = item[31].ToString();
                    imp.f032_partialcharge = item[32].ToString();
                    imp.f033_selfcharge = item[33].ToString();
                    imp.f034_genbutsucharge = item[34].ToString();
                    imp.f035_jotaikbn = item[35].ToString();
                    imp.f036_comnum = item[36].ToString();
                    imp.f037_daisanshakbn = item[37].ToString();
                    imp.f038_shoriymd = item[38].ToString();
                    imp.f039_clinicname = item[39].ToString();
                    imp.f040_hihoname = item[40].ToString();

                    imp.cym =          int.Parse(item[41].ToString());
                    imp.birthdayad =   DateTime.Parse(item[42].ToString());
                    imp.startdate1ad = DateTime.Parse(item[43].ToString());
                    imp.startdate2ad = DateTime.Parse(item[44].ToString());
                    imp.startdate3ad = DateTime.Parse(item[45].ToString());
                    imp.shoriymdad =   DateTime.Parse(item[46].ToString());
                    imp.chargeymad= int.Parse(item[47].ToString());
                    imp.mediymad = int.Parse(item[48].ToString());



                    lst.Add(imp);
                }



                return lst;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }
    }
}
