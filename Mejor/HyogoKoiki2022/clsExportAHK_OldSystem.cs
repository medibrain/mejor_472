﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.HyogoKoiki2022
{
    class clsExportAHK_OldSystem
    {
    }

    public partial class exp_AHK_oldSystem
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_exportid { get; set; } = 0;                              //エクスポートID;

        public string f001_comnum { get; set; } = string.Empty;                  //電算管理番号;
        public string f002_image { get; set; } = string.Empty;                   //画像ファイル名;
        public string f003_gender { get; set; } = string.Empty;                  //性別コード;
        public string f004_pbirthdayw { get; set; } = string.Empty;              //生年月日和暦;
        public string f005_insnum { get; set; } = string.Empty;                  //保険者番号;
        public string f006_hihonum { get; set; } = string.Empty;                 //被保険者番号;
        public string f007_hihoname { get; set; } = string.Empty;                //被保険者氏名;
        public string f008_clinicnum { get; set; } = string.Empty;               //医療機関番号;
        public string f009_clinicname { get; set; } = string.Empty;              //医療機関名;
        public string f010_mediym { get; set; } = string.Empty;                  //診療年月 ;
        public string f011_chargeym { get; set; } = string.Empty;                //請求年月 ;
        public string f012_rezecode { get; set; } = string.Empty;                //レセプト種類コード ;
        public string f013_total { get; set; } = string.Empty;                   //費用金額 ;
        public string f014_ratio { get; set; } = string.Empty;                   //給付割合 ;
        public string f015_counteddays { get; set; } = string.Empty;             //施術日数 ;
        public string f016_visit { get; set; } = string.Empty;                   //往療料の有無 ;
        public string f017_visittimes { get; set; } = string.Empty;              //往療日数 ;
        public string f018_bui { get; set; } = string.Empty;                     //施術部位数 ;
        public string f019_f1name { get; set; } = string.Empty;                  //傷病名 ;
        public int cym { get; set; } = 0;                                        //メホール請求年月;
        public int aid { get; set; } = 0;                                       //aid
        public int apptype { get; set; } = 0;                                          //画像出力判定用apptype;

        #endregion


    }

}
