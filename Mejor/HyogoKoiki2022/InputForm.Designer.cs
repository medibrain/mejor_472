﻿namespace Mejor.HyogoKoiki2022
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.pDoui = new System.Windows.Forms.Panel();
            this.vbDouiG = new Mejor.VerifyBox();
            this.vbDouiD = new Mejor.VerifyBox();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDoui = new System.Windows.Forms.Label();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelOCR = new System.Windows.Forms.Label();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.lblFirst2 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.labelF1 = new System.Windows.Forms.Label();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxVisitTimes = new Mejor.VerifyBox();
            this.verifyBoxBuisu = new Mejor.VerifyBox();
            this.labelVisitTimes = new System.Windows.Forms.Label();
            this.lblBui = new System.Windows.Forms.Label();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.lblFirst = new System.Windows.Forms.Label();
            this.lblFirstY = new System.Windows.Forms.Label();
            this.lblFirstM = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.pDoui.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panelTotal.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(672, 679);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 200;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 685);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(58, 705);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(58, 705);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 680);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 680);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 680);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-25, 680);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(4);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(58, 705);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.pDoui);
            this.panelRight.Controls.Add(this.dgv);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(275, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 705);
            this.panelRight.TabIndex = 2;
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(917, 23);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(84, 12);
            this.labelMacthCheck.TabIndex = 201;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Controls.Add(this.label3);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Location = new System.Drawing.Point(506, 33);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(300, 46);
            this.pDoui.TabIndex = 4;
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(100, 5);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(247, 5);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(195, 5);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(141, 5);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(55, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 24);
            this.label3.TabIndex = 84;
            this.label3.Text = "4:平成\r\n5:令和";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Location = new System.Drawing.Point(14, 5);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(41, 24);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(225, 14);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(17, 12);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(278, 14);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(17, 12);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(173, 14);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(17, 12);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(3, 595);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 83);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 151;
            this.dgv.TabStop = false;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(197, 30);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 1;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Validated += new System.EventHandler(this.verifyBoxY_Validated);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(236, 40);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(17, 12);
            this.labelYear.TabIndex = 73;
            this.labelYear.Text = "年";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(163, 40);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(29, 12);
            this.labelHs.TabIndex = 71;
            this.labelHs.Text = "和暦";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(7, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 84);
            this.label6.TabIndex = 21;
            this.label6.Text = "柔整申請書 :空欄\r\n続紙    : \"--\"     \r\n不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  " +
    ": \"911\"\r\n状態記入書  : \"921\"";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 99);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(4);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 420);
            this.scrollPictureControl1.TabIndex = 20;
            this.scrollPictureControl1.TabStop = false;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(576, 679);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 19;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 679);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(41, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "label23";
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.lblFirst2);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstE);
            this.panelTotal.Controls.Add(this.labelF1);
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.verifyBoxVisitTimes);
            this.panelTotal.Controls.Add(this.verifyBoxBuisu);
            this.panelTotal.Controls.Add(this.labelVisitTimes);
            this.panelTotal.Controls.Add(this.lblBui);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstY);
            this.panelTotal.Controls.Add(this.verifyBoxF1FirstM);
            this.panelTotal.Controls.Add(this.lblFirst);
            this.panelTotal.Controls.Add(this.lblFirstY);
            this.panelTotal.Controls.Add(this.lblFirstM);
            this.panelTotal.Location = new System.Drawing.Point(0, 530);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(950, 65);
            this.panelTotal.TabIndex = 20;
            // 
            // lblFirst2
            // 
            this.lblFirst2.AutoSize = true;
            this.lblFirst2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirst2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblFirst2.Location = new System.Drawing.Point(94, 13);
            this.lblFirst2.Name = "lblFirst2";
            this.lblFirst2.Size = new System.Drawing.Size(25, 36);
            this.lblFirst2.TabIndex = 75;
            this.lblFirst2.Text = "3:昭\r\n4:平\r\n5:令";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(64, 18);
            this.verifyBoxF1FirstE.MaxLength = 1;
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstE.TabIndex = 49;
            this.verifyBoxF1FirstE.TextV = "";
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(224, 5);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(47, 12);
            this.labelF1.TabIndex = 73;
            this.labelF1.Text = "負傷名1";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxF1.Location = new System.Drawing.Point(227, 19);
            this.verifyBoxF1.MaxLength = 1;
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF1.TabIndex = 53;
            this.verifyBoxF1.TextV = "";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(847, 17);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 71;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Validated += new System.EventHandler(this.verifyBoxTotal_Validated);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(812, 26);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 12);
            this.labelTotal.TabIndex = 72;
            this.labelTotal.Text = "合計";
            // 
            // verifyBoxVisitTimes
            // 
            this.verifyBoxVisitTimes.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxVisitTimes.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxVisitTimes.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxVisitTimes.Location = new System.Drawing.Point(766, 17);
            this.verifyBoxVisitTimes.Name = "verifyBoxVisitTimes";
            this.verifyBoxVisitTimes.NewLine = false;
            this.verifyBoxVisitTimes.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxVisitTimes.TabIndex = 62;
            this.verifyBoxVisitTimes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxVisitTimes.TextV = "";
            // 
            // verifyBoxBuisu
            // 
            this.verifyBoxBuisu.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuisu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuisu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuisu.Location = new System.Drawing.Point(445, 19);
            this.verifyBoxBuisu.Name = "verifyBoxBuisu";
            this.verifyBoxBuisu.NewLine = false;
            this.verifyBoxBuisu.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxBuisu.TabIndex = 55;
            this.verifyBoxBuisu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBuisu.TextV = "";
            // 
            // labelVisitTimes
            // 
            this.labelVisitTimes.AutoSize = true;
            this.labelVisitTimes.Location = new System.Drawing.Point(707, 26);
            this.labelVisitTimes.Name = "labelVisitTimes";
            this.labelVisitTimes.Size = new System.Drawing.Size(53, 12);
            this.labelVisitTimes.TabIndex = 70;
            this.labelVisitTimes.Text = "往療回数";
            // 
            // lblBui
            // 
            this.lblBui.AutoSize = true;
            this.lblBui.Location = new System.Drawing.Point(442, 5);
            this.lblBui.Name = "lblBui";
            this.lblBui.Size = new System.Drawing.Size(41, 12);
            this.lblBui.TabIndex = 70;
            this.lblBui.Text = "部位数";
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(609, 16);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisitKasan.TabIndex = 58;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            this.checkBoxVisitKasan.CheckedChanged += new System.EventHandler(this.checkBoxVisitKasan_CheckedChanged);
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(513, 16);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisit.TabIndex = 57;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            this.checkBoxVisit.CheckedChanged += new System.EventHandler(this.checkBoxVisit_CheckedChanged);
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(123, 18);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 50;
            this.verifyBoxF1FirstY.TextV = "";
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(173, 18);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 51;
            this.verifyBoxF1FirstM.TextV = "";
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.Location = new System.Drawing.Point(20, 29);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(41, 12);
            this.lblFirst.TabIndex = 63;
            this.lblFirst.Text = "初検日";
            // 
            // lblFirstY
            // 
            this.lblFirstY.AutoSize = true;
            this.lblFirstY.Location = new System.Drawing.Point(152, 29);
            this.lblFirstY.Name = "lblFirstY";
            this.lblFirstY.Size = new System.Drawing.Size(17, 12);
            this.lblFirstY.TabIndex = 64;
            this.lblFirstY.Text = "年";
            // 
            // lblFirstM
            // 
            this.lblFirstM.AutoSize = true;
            this.lblFirstM.Location = new System.Drawing.Point(202, 29);
            this.lblFirstM.Name = "lblFirstM";
            this.lblFirstM.Size = new System.Drawing.Size(17, 12);
            this.lblFirstM.TabIndex = 65;
            this.lblFirstM.Text = "月";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.AutoSize = true;
            this.labelInputerName.Location = new System.Drawing.Point(268, 684);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(31, 12);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "入力:";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Location = new System.Drawing.Point(200, 11);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(300, 65);
            this.panelHnum.TabIndex = 3;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(163, 20);
            this.verifyBoxHnum.MaxLength = 10;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(61, 20);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(160, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(41, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(96, 28);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(272, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 705);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1295, 705);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxBuisu;
        private System.Windows.Forms.Label lblBui;
        private VerifyCheckBox checkBoxVisitKasan;
        private VerifyCheckBox checkBoxVisit;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxF1FirstM;
        private System.Windows.Forms.Label lblFirst;
        private System.Windows.Forms.Label lblFirstY;
        private System.Windows.Forms.Label lblFirstM;
        private VerifyBox verifyBoxY;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private VerifyBox verifyBoxVisitTimes;
        private System.Windows.Forms.Label labelVisitTimes;
        private System.Windows.Forms.Label labelF1;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel pDoui;
        private VerifyBox vbDouiG;
        private VerifyBox vbDouiD;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDoui;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label lblFirst2;
        private VerifyBox verifyBoxF1FirstE;
        private System.Windows.Forms.Label labelMacthCheck;
    }
}