﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.kyokaikenpo_hiroshima
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);        
        Point posFusho = new Point(0, 0);
        
        Control[] ymControls, fushoControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] { verifyBoxY, verifyBoxM };
                    
            fushoControls = new Control[] { verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5,
                                            verifyBoxF1Nengo,verifyBoxF2Nengo,verifyBoxF3Nengo,verifyBoxF4Nengo,verifyBoxF5Nengo,
                                            verifyBoxF1Y,verifyBoxF2Y,verifyBoxF3Y,verifyBoxF4Y,verifyBoxF5Y,
                                            verifyBoxF1M,verifyBoxF2M,verifyBoxF3M,verifyBoxF4M,verifyBoxF5M,                                            
                                            verifyBoxF1Days,verifyBoxF2Days,verifyBoxF3Days,verifyBoxF4Days,verifyBoxF5Days,
                                            verifyBoxF1Tenki,verifyBoxF2Tenki,verifyBoxF3Tenki,verifyBoxF4Tenki,verifyBoxF5Tenki,
            };

            
            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            panelHnum.Visible = false;
            panelTotal.Visible = false;

            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }
        #endregion
        
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion


        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1 );

            //20200304194456 furukawa st ////////////////////////
            //月があったほうが入力者が打ちやすい
            
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month>12);
            //20200304194456 furukawa ed ////////////////////////




            //負傷ごとのチェック＋appへの反映
            if (!chkFusho(app,out app)) hasError = true;

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
        
            
            //値の反映
            
            //施術年
            app.MediYear = year;


            //20200304200543 furukawa st ////////////////////////
            //月があったほうが入力者が打ちやすい
            
            //施術月
            app.MediMonth = month;
            //20200304200543 furukawa ed ////////////////////////

            return true;
        }


        /// <summary>
        /// 負傷ごとのチェック
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool chkFusho(App app,out App outapp)
        {
            

            //負傷名1チェック
            fusho1Check(verifyBoxF1);
            int days1 = 0;
            int tenki1 = 0;
            if (verifyBoxF1.Text != string.Empty)
            {
                //初検年号、初検年月チェック
                app.FushoFirstDate1 = chkFushoDate(verifyBoxF1Nengo, verifyBoxF1Y, verifyBoxF1M);

                //実日数1
                days1 = verifyBoxF1Days.GetIntValue();
                setStatus(verifyBoxF1Days, verifyBoxF1.Text != string.Empty && days1 < 1 || 31 < days1);
                
                //転帰1 継続0 治癒1 中止2 転医3
                tenki1 = verifyBoxF1Tenki.GetIntValue();
                setStatus(verifyBoxF1Tenki, tenki1 < 0 || 4 < tenki1);
            }


            //負傷名2チェック
            fushoCheck(verifyBoxF2);
            int days2 = 0;
            int tenki2 = 0;
            if (verifyBoxF2.Text != string.Empty)
            {

                app.FushoFirstDate2 = chkFushoDate(verifyBoxF2Nengo, verifyBoxF2Y, verifyBoxF2M);
                //実日数2
                days2 = verifyBoxF2Days.GetIntValue();
                setStatus(verifyBoxF2Days, days2 < 1 || 31 < days2);
                //転帰2
                tenki2 = verifyBoxF2Tenki.GetIntValue();
                setStatus(verifyBoxF2Tenki, tenki2 < 0 || 4 < tenki2);
            }


            //負傷名3チェック
            fushoCheck(verifyBoxF3);
            int days3 = 0;
            int tenki3 = 0;
            if (verifyBoxF3.Text != string.Empty)
            {
                app.FushoFirstDate3 = chkFushoDate(verifyBoxF3Nengo, verifyBoxF3Y, verifyBoxF3M);
                //実日数3
                days3 = verifyBoxF3Days.GetIntValue();
                setStatus(verifyBoxF3Days, days3 < 1 || 31 < days3);
                //転帰3
                tenki3 = verifyBoxF3Tenki.GetIntValue();
                setStatus(verifyBoxF3Tenki, tenki3 < 0 || 4 < tenki3);
            }

            //負傷名4チェック
            fushoCheck(verifyBoxF4);
            int days4 = 0;
            int tenki4 = 0;
            if (verifyBoxF4.Text != string.Empty)
            {
                app.FushoFirstDate4 = chkFushoDate(verifyBoxF4Nengo, verifyBoxF4Y, verifyBoxF4M);
                //実日数4
                days4 = verifyBoxF4Days.GetIntValue();
                setStatus(verifyBoxF4Days, days4 < 1 || 31 < days4);
                //転帰4
                tenki4 = verifyBoxF4Tenki.GetIntValue();
                setStatus(verifyBoxF4Tenki, tenki4 < 0 || 4 < tenki4);

            }

            //負傷名5チェック
            fushoCheck(verifyBoxF5);
            int days5 = 0;
            int tenki5 = 0;
            if (verifyBoxF5.Text != string.Empty)
            {
                app.FushoFirstDate5 = chkFushoDate(verifyBoxF5Nengo, verifyBoxF5Y, verifyBoxF5M);
                //実日数5
                days5 = verifyBoxF5Days.GetIntValue();
                setStatus(verifyBoxF5Days, days5 < 1 || 31 < days5);
                //転帰5
                tenki5 = verifyBoxF5Tenki.GetIntValue();
                setStatus(verifyBoxF5Tenki, tenki5 < 0 || 4 < tenki5);
            }

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                outapp = app;
                return false;                
            }


            //負傷名
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //負傷ごとの実日数
            app.FushoDays1 = days1;
            app.FushoDays2 = days2;
            app.FushoDays3 = days3;
            app.FushoDays4 = days4;
            app.FushoDays5 = days5;

            //負傷ごとの転帰
            app.FushoCourse1 = tenki1;
            app.FushoCourse2 = tenki2;
            app.FushoCourse3 = tenki3;
            app.FushoCourse4 = tenki4;
            app.FushoCourse5 = tenki5;

            outapp = app;
            return true;
        }


           
        /// <summary>
        /// 負傷ごとの初検日チェック
        /// </summary>
        /// <param name="vbNengo">年号</param>
        /// <param name="vbY">和暦年</param>
        /// <param name="vbM">月</param>
        /// <param name="FushoDate">負傷ごとの初検日</param>
        /// <returns>初検日</returns>
        private DateTime chkFushoDate(VerifyBox vbNengo,VerifyBox vbY,VerifyBox vbM)
        {

            //年号
            int sG = vbNengo.GetIntValue();
            setStatus(vbNengo, sG < 1 || 5 < sG);

            //初検年月
            int sY = vbY.GetIntValue();
            int sM = vbM.GetIntValue();
            
            DateTime shoken = DateTime.MinValue;

            //西暦変換
            
            int.TryParse(sG.ToString() + sY.ToString("00") + sM.ToString("00"), out int inttmp);
            if (inttmp > 0) sY = DateTimeEx.GetAdYearMonthFromJyymm(inttmp) / 100;

            if (!DateTimeEx.IsDate(sY, sM, 1))
            {
                setStatus(vbY, true);
                setStatus(vbM, true);
            }
            else
            {
                setStatus(vbY, false);
                setStatus(vbM, false);
                shoken = new DateTime(sY, sM, 1);
            }
            return shoken;
        }



        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
                verifyBoxBY.Text = app.Birthday.Year.ToString();


                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.6f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号
                var hn = app.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }

                //申請書
                //20200309211022 furukawa st ////////////////////////
                //ベリファイするときに平成から令和に直して打つのが面倒らしいので、ベリファイなしにする
                
                setValue(verifyBoxY, app.MediYear, firstTime, false);
                //setValue(verifyBoxY, app.MediYear, firstTime, nv);
                //20200309211022 furukawa ed ////////////////////////


                //20200304194659 furukawa st ////////////////////////
                //月があったほうが入力者が打ちやすい

                //20200309211127 furukawa st ////////////////////////
                //施術年ベリファイなしにしたので月もなし
                
                setValue(verifyBoxM, app.MediMonth, firstTime, false);
                //setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                //20200309211127 furukawa ed ////////////////////////


                //20200304194659 furukawa ed ////////////////////////



                //負傷ごとの値をセット
                setFusho(app,nv);
            }
        }

        /// <summary>
        /// 負傷ごとのデータを表示
        /// </summary>
        /// <param name="app"></param>
        private void setFusho(App app,bool nv)
        {

            //負傷名1
            setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
            if (app.FushoName1 != string.Empty)
            {
                //初検年月1
                setValue(verifyBoxF1Nengo, DateTimeEx.GetEraNumber(app.FushoFirstDate1).ToString(), firstTime, nv);
                setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, nv);
                setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, nv);

                //実日数1
                setValue(verifyBoxF1Days, app.FushoDays1, firstTime, nv);

                //転帰1
                setValue(verifyBoxF1Tenki, app.FushoCourse1, firstTime, nv);
            }
            else
            {
                //初検日1              
                setValue(verifyBoxF1Nengo, string.Empty, firstTime, nv);
                setValue(verifyBoxF1Y, string.Empty, firstTime, nv);
                setValue(verifyBoxF1M, string.Empty, firstTime, nv);
                //実日数1
                setValue(verifyBoxF1Days, app.FushoDays1, firstTime, nv);
                //転帰1
                setValue(verifyBoxF1Tenki, app.FushoCourse1, firstTime, nv);
            }


            //負傷名2
            setValue(verifyBoxF2, app.FushoName2, firstTime, nv);
            if (app.FushoName2 != string.Empty)
            {
                //初検年月2
                setValue(verifyBoxF2Nengo, DateTimeEx.GetEraNumber(app.FushoFirstDate2).ToString(), firstTime, nv);
                setValue(verifyBoxF2Y, DateTimeEx.GetJpYear(app.FushoFirstDate2).ToString(), firstTime, nv);
                setValue(verifyBoxF2M, app.FushoFirstDate2.Month.ToString(), firstTime, nv);
                //実日数2
                setValue(verifyBoxF2Days, app.FushoDays2, firstTime, nv);
                //転帰2
                setValue(verifyBoxF2Tenki, app.FushoCourse2, firstTime, nv);
            }
            else
            {
                //初検日2              
                setValue(verifyBoxF2Nengo, string.Empty, firstTime, nv);
                setValue(verifyBoxF2Y, string.Empty, firstTime, nv);
                setValue(verifyBoxF2M, string.Empty, firstTime, nv);
                //実日数2
                setValue(verifyBoxF2Days, app.FushoDays2, firstTime, nv);
                //転帰2
                setValue(verifyBoxF2Tenki, app.FushoCourse2, firstTime, nv);
            }


            //負傷名3
            setValue(verifyBoxF3, app.FushoName3, firstTime, nv);
            if (app.FushoName3 != string.Empty)
            {
                //初検年月3
                setValue(verifyBoxF3Nengo, DateTimeEx.GetEraNumber(app.FushoFirstDate3).ToString(), firstTime, nv);
                setValue(verifyBoxF3Y, DateTimeEx.GetJpYear(app.FushoFirstDate3).ToString(), firstTime, nv);
                setValue(verifyBoxF3M, app.FushoFirstDate3.Month.ToString(), firstTime, nv);

                //実日数3
                setValue(verifyBoxF3Days, app.FushoDays3, firstTime, nv);
                //転帰3
                setValue(verifyBoxF3Tenki, app.FushoCourse3, firstTime, nv);
            }
            else
            {
                //初検日3              
                setValue(verifyBoxF3Nengo, string.Empty, firstTime, nv);
                setValue(verifyBoxF3Y, string.Empty, firstTime, nv);
                setValue(verifyBoxF3M, string.Empty, firstTime, nv);
                //実日数3
                setValue(verifyBoxF3Days, app.FushoDays3, firstTime, nv);
                //転帰3
                setValue(verifyBoxF3Tenki, app.FushoCourse3, firstTime, nv);

            }


            //負傷名4
            setValue(verifyBoxF4, app.FushoName4, firstTime, nv);
            if (app.FushoName4 != string.Empty)
            {
                //初検年月4 
                setValue(verifyBoxF4Nengo, DateTimeEx.GetEraNumber(app.FushoFirstDate4).ToString(), firstTime, nv);
                setValue(verifyBoxF4Y, DateTimeEx.GetJpYear(app.FushoFirstDate4).ToString(), firstTime, nv);
                setValue(verifyBoxF4M, app.FushoFirstDate4.Month.ToString(), firstTime, nv);

                //実日数4
                setValue(verifyBoxF4Days, app.FushoDays4, firstTime, nv);
                //転帰4
                setValue(verifyBoxF4Tenki, app.FushoCourse4, firstTime, nv);
            }
            else
            {
                //初検日4              
                setValue(verifyBoxF4Nengo, string.Empty, firstTime, nv);
                setValue(verifyBoxF4Y, string.Empty, firstTime, nv);
                setValue(verifyBoxF4M, string.Empty, firstTime, nv);
                //実日数4
                setValue(verifyBoxF4Days, app.FushoDays4, firstTime, nv);
                //転帰4
                setValue(verifyBoxF4Tenki, app.FushoCourse4, firstTime, nv);
            }


            //負傷名5
            setValue(verifyBoxF5, app.FushoName5, firstTime, nv);
            if (app.FushoName5 != string.Empty)
            {
                //初検年月5
                setValue(verifyBoxF5Nengo, DateTimeEx.GetEraNumber(app.FushoFirstDate5).ToString(), firstTime, nv);
                setValue(verifyBoxF5Y, DateTimeEx.GetJpYear(app.FushoFirstDate5).ToString(), firstTime, nv);
                setValue(verifyBoxF5M, app.FushoFirstDate5.Month.ToString(), firstTime, nv);
                //実日数5
                setValue(verifyBoxF5Days, app.FushoDays5, firstTime, nv);
                //転機5
                setValue(verifyBoxF5Tenki, app.FushoCourse5, firstTime, nv);
            }
            else
            {
                //初検日5              
                setValue(verifyBoxF5Nengo, string.Empty, firstTime, nv);
                setValue(verifyBoxF5Y, string.Empty, firstTime, nv);
                setValue(verifyBoxF5M, string.Empty, firstTime, nv);
                //実日数5
                setValue(verifyBoxF5Days, app.FushoDays5, firstTime, nv);
                //転帰5
                setValue(verifyBoxF5Tenki, app.FushoCourse5, firstTime, nv);
            }


        }


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                //panelHnum.Visible = true;
                panelTotal.Visible = true;
                
                
                //20200304194351 furukawa st ////////////////////////
                //月があったほうが入力者が打ちやすい
                
                verifyBoxM.Visible = true;
                labelM.Visible = true;
                //20200304194351 furukawa ed ////////////////////////


            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            
            else if (fushoControls.Contains(t)) posFusho = pos;
            

        }

        private void verifyBoxAppType_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxAppType.Text != "7" && firstTime)
            {
                verifyBoxF1.Enabled = true;
                verifyBoxF2.Enabled = true;
                verifyBoxF3.Enabled = true;
                verifyBoxF4.Enabled = true;
                verifyBoxF5.Enabled = true;
            }
            else
            {
                verifyBoxF1.Enabled = false;
                verifyBoxF2.Enabled = false;
                verifyBoxF3.Enabled = false;
                verifyBoxF4.Enabled = false;
                verifyBoxF5.Enabled = false;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
