﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.kyokaikenpo_hiroshima
{
    /// <summary>
    /// 協会けんぽ広島支部用インポート・エクスポートデータ
    /// </summary>
    class KKData
    {
        #region テーブル構造
        [DB.DbAttribute.PrimaryKey]
        public  string shibu_cd { get; set; } = string.Empty;                                         //担当支部コード
        public  string hmark { get; set; } = string.Empty;                                            //被保険者証記号
        public  string hnum { get; set; } = string.Empty;                                             //被保険者証番号
        public  string taisho_no { get; set; } = string.Empty;                                        //対象者番号
        public  string uke_shibucd { get; set; } = string.Empty;                                      //受付番号＿支部コード
        public  string uke_scanner { get; set; } = string.Empty;                                      //受付番号＿スキャナー
        public  string uke_ukeymd { get; set; } = string.Empty;                                       //受付番号＿受付年月日
        public  string uke_gyomu { get; set; } = string.Empty;                                        //受付番号＿業務種別
        public  string uke_renban { get; set; } = string.Empty;                                       //受付番号＿連番
        public  string kousei { get; set; } = string.Empty;                                           //更正取消通番
        public  string hkana { get; set; } = string.Empty;                                            //被保険者氏名カナ
        public  string hname { get; set; } = string.Empty;                                            //被保険者氏名
        public  string hzip { get; set; } = string.Empty;                                             //被保険者郵便番号
        public  string hadd1 { get; set; } = string.Empty;                                            //被保険者住所１
        public  string hadd2 { get; set; } = string.Empty;                                            //被保険者住所２
        public  string hadd3 { get; set; } = string.Empty;                                            //被保険者住所３
        public  string ym { get; set; } = string.Empty;                                               //診療年月
        public  string honke { get; set; } = string.Empty;                                            //本家区分
        public  string gender { get; set; } = string.Empty;                                           //性別
        public  string jbirth { get; set; } = string.Empty;                                           //受診者生年月日
        public  string jname { get; set; } = string.Empty;                                            //受診者氏名
        public  string shoken1 { get; set; } = string.Empty;                                          //初検年月日１
        public  string shoken2 { get; set; } = string.Empty;                                          //初検年月日２
        public  string shoken3 { get; set; } = string.Empty;                                          //初検年月日３
        public  string shoken4 { get; set; } = string.Empty;                                          //初検年月日４
        public  string shoken5 { get; set; } = string.Empty;                                          //初検年月日５
        public  int days { get; set; } = 0;                                                           //診療日数
        public  string jkaicode { get; set; } = string.Empty;                                         //柔整師会コード
        public  string jcode { get; set; } = string.Empty;                                            //柔整師コード
        public  int total { get; set; } = 0;                                                          //総合計
        public  int kettei { get; set; } = 0;                                                         //支給決定金額
        public  int bui { get; set; } = 0;                                                            //部位数
        public string cym { get; set; } = string.Empty;                                               //診療月（柔整でつけてもらう）

        public  string medi_key { get; set; } = string.Empty;                                         //ユニークキー（画像ファイル名）（柔整でつけてもらう）
        public  string medi_fusho1 { get; set; } = string.Empty;                                      //負傷名1
        public  string medi_shoken1 { get; set; } = string.Empty;                                     //初検年月日1　入力は和暦(020226)、出力は西暦（200226)
        public  int medi_days1 { get; set; } = 0;                                                     //実日数1
        public  int medi_tenki1 { get; set; } = 0;                                                    //転帰1　継続：０，治癒：１、中止：２、転医：３
        public  string medi_fusho2 { get; set; } = string.Empty;                                      //負傷名2
        public  string medi_shoken2 { get; set; } = string.Empty;                                     //初検年月日2
        public  int medi_days2 { get; set; } = 0;                                                     //実日数2
        public  int medi_tenki2 { get; set; } = 0;                                                    //転帰2
        public  string medi_fusho3 { get; set; } = string.Empty;                                      //負傷名3
        public  string medi_shoken3 { get; set; } = string.Empty;                                     //初検年月日3
        public  int medi_days3 { get; set; } = 0;                                                     //実日数3
        public  int medi_tenki3 { get; set; } = 0;                                                    //転帰3
        public  string medi_fusho4 { get; set; } = string.Empty;                                      //負傷名4
        public  string medi_shoken4 { get; set; } = string.Empty;                                     //初検年月日4
        public  int medi_days4 { get; set; } = 0;                                                     //実日数4
        public  int medi_tenki4 { get; set; } = 0;                                                    //転帰4
        public  string medi_fusho5 { get; set; } = string.Empty;                                      //負傷名5
        public  string medi_shoken5 { get; set; } = string.Empty;                                     //初検年月日5
        public  int medi_days5 { get; set; } = 0;                                                     //実日数5
        public  int medi_tenki5 { get; set; } = 0;                                                    //転帰5
        #endregion



        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool ExportKKData()
        {

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                wf.LogPrint("出力データ更新");

                //kkdataを更新
                if (!UpdateApplicaitonForExport()) return false;

                wf.LogPrint("データ取得");
                //KKdataテーブルを取得 npoiでブック作成、保存
                if (!Export()) return false;

                
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// applicationテーブルに入力した項目をKKDAtaに反映
        /// </summary>
        /// <returns></returns>
        private static bool UpdateApplicaitonForExport()
        {
            DB.Command cmd = DB.Main.CreateCmd($"" +
                $"update kkdata set	" +
                $"medi_fusho1		=	a.iname1				 ,  " +
                $"medi_shoken1		=	a.ifirstdate1            ,  " +
                $"medi_days1		=	a.idays1                 ,  " +
                $"medi_tenki1		=	a.icourse1               ,  " +
                $"medi_fusho2		=	a.iname2                 ,  " +
                $"medi_shoken2		=	a.ifirstdate2            ,  " +
                $"medi_days2		=	a.idays2                 ,  " +
                $"medi_tenki2		=	a.icourse2               ,  " +
                $"medi_fusho3		=	a.iname3                 ,  " +
                $"medi_shoken3		=	a.ifirstdate3            ,  " +
                $"medi_days3		=	a.idays3                 ,  " +
                $"medi_tenki3		=	a.icourse3               ,  " +
                $"medi_fusho4		=	a.iname4                 ,  " +
                $"medi_shoken4		=	a.ifirstdate4            ,  " +
                $"medi_days4		=	a.idays4                 ,  " +
                $"medi_tenki4		=	a.icourse4               ,  " +
                $"medi_fusho5		=	a.iname5                 ,  " +
                $"medi_shoken5		=	a.ifirstdate5            ,  " +
                $"medi_days5		=	a.idays5                 ,  " +
                $"medi_tenki5		=	a.icourse5                  " +
                $"from application a                  " +
                $"where kkdata.medi_key=a.numbering   ");

            if (!cmd.TryExecuteNonQuery()) return false;
            return true;


        }

        /// <summary>
        /// DBからデータを取得してExcel出力まで
        /// </summary>
        /// <returns></returns>
        private static bool Export()
        {
            System.Windows.Forms.SaveFileDialog sd = new System.Windows.Forms.SaveFileDialog();
            sd.DefaultExt = "Excel|*.xlsx";
            sd.Filter = "Excel|*.xlsx";
            sd.FilterIndex = 0;

            if (sd.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strFileName = sd.FileName;

            string strheader = "担当支部コード, 被保険者証記号, 被保険者証番号, 対象者番号, " +
                "受付番号＿支部コード, 受付番号＿スキャナー, 受付番号＿受付年月日, 受付番号＿業務種別, 受付番号＿連番, 更正取消通番, " +
                "被保険者氏名カナ, 被保険者氏名, 被保険者郵便番号, 被保険者住所１, 被保険者住所２, 被保険者住所３, " +
                "診療年月, 本家区分, 性別, 受診者生年月日, 受診者氏名, " +
                "初検年月日１, 初検年月日２, 初検年月日３, 初検年月日４, 初検年月日５, 診療日数, " +
                "柔整師会コード, 柔整師コード, 総合計, 支給決定金額, 部位数, 請求月, " +
                "画像ファイル名, 負傷名1, 初検日1, 実日数1, 転帰1, 負傷名2, 初検日2, 実日数2, 転帰2, " +
                "負傷名3, 初検日3, 実日数3, 転帰3, 負傷名4, 初検日4, 実日数4, 転帰4, 負傷名5, 初検日5, 実日数5, 転帰5";


            wb=new NPOI.XSSF.UserModel.XSSFWorkbook();
            wb.CreateSheet("支給記録データ");

            NPOI.SS.UserModel.ISheet sh = wb.GetSheetAt(0);

            NPOI.SS.UserModel.IRow r;

            using (var cmd = DB.Main.CreateCmd(
                "SELECT * FROM kkdata "))
            {
                try
                {         
                    var res = cmd.TryExecuteReaderList();

                    #region セルスタイル
                    NPOI.SS.UserModel.IFont font = wb.CreateFont();
                    font.FontName = "ＭＳ ゴシック";
                    font.FontHeightInPoints = 9;

                    NPOI.SS.UserModel.ICellStyle csHeader=wb.CreateCellStyle();
                    csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                    csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                    csHeader.SetFont(font);
                

                    NPOI.SS.UserModel.ICellStyle csData = wb.CreateCellStyle();
                    csData.FillPattern = NPOI.SS.UserModel.FillPattern.NoFill;
                    csData.SetFont(font);
                    #endregion

                    #region ヘッダ行
                    r = sh.CreateRow(0);

                    var hs = strheader.Split(',');
                    for (int cnt = 0; cnt < hs.Length; cnt++){
                        NPOI.SS.UserModel.ICell hdcell = r.CreateCell(cnt);
                        hdcell.CellStyle = csHeader;
                        hdcell.SetCellValue(hs[cnt]);
                    
                    }
                    #endregion

                    #region データ
                    for (int cntRes=0;cntRes<res.Count;cntRes++)
                    {
                        //ヘッダを飛ばして2行から
                        r = sh.CreateRow(cntRes+1);

                        if (res == null || res.Count == 0) continue;
                        for (int c = 0; c < res[cntRes].Length; c++)
                        {
                    
                            NPOI.SS.UserModel.ICell cell=r.CreateCell(c);
                            cell.CellStyle = csData;
                            

                            //c=34 画像ファイル名
                            //35負傷名1	36初検日1	37実日数1	38転帰1
                            //転帰は文字に置き換え
                            if (c==37 || c==41 || c==45 || c==49 || c==53)
                            {
                                string strTenki = string.Empty;
                                switch (res[cntRes][c].ToString())
                                {
                                    case "1":strTenki = "継続"; break;
                                    case "2":strTenki = "治癒";break;
                                    case "3":strTenki = "中止";break;
                                    case "4":strTenki = "転医";break;
                                }
                                cell.SetCellValue(strTenki);
                            }
                            else
                            {
                                if (res[cntRes][c].ToString() == "0001-01-01") continue;
                                if (res[cntRes][c].ToString() == "0") continue;

                                cell.SetCellValue(res[cntRes][c].ToString());
                            }

                        }

                    }

                    for (int c = 0; c < res[0].Length; c++)
                    {
                        sh.AutoSizeColumn(c);
                    }
                    #endregion


                    System.IO.FileStream fs =
                        new System.IO.FileStream(strFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    wb.Write(fs);
                    wb.Close();

                    return true;
                }
                catch(Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
            }



        }



        #region インポート
        /// <summary>
        /// excelインポート処理
        /// </summary>
        public static void Import()
        {
            string strFileName = string.Empty;

            System.Windows.Forms.OpenFileDialog fd = new System.Windows.Forms.OpenFileDialog();
            fd.Filter = "Excelファイル|*.xlsx";
            fd.Title = "協会けんぽ広島支部　提供データExcel取り込み";

            //Excelファイル指定
            if (fd.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            
            strFileName = fd.FileName;
            

            //シート名取得
            if (!getSheetData(strFileName)) return;

            //Applicationの最低限の項目を更新
            if (!UpdateApplication()) return;

            System.Windows.Forms.MessageBox.Show("取り込み完了");

        }

        /// <summary>
        /// kkdataからApplicationに更新
        /// </summary>
        /// <returns></returns>
        private static bool UpdateApplication()
        {
            string strSQL = string.Empty;
            strSQL = $"update application set " +
                    "hnum=trim(kkdata.hmark) || '-' || trim(kkdata.hnum)," +
                    "pbirthday=cast(trim(kkdata.jbirth) as date)," +
                    "psex=cast(kkdata.gender as int)," +
                    "ym=cast(kkdata.ym as int)," +
                    "sregnumber=trim(kkdata.jcode)," +
                    "baccnumber=trim(kkdata.jkaicode)," +
                    "atotal=kkdata.total," +
                    "acharge=kkdata.kettei " +
                    "from kkdata " +
                    "where application.numbering=kkdata.medi_key";

            DB.Command cmd = new DB.Command(DB.Main,strSQL);
            if (!cmd.TryExecuteNonQuery()) {

                System.Windows.Forms.MessageBox.Show("Application更新失敗");
                return false;
            }

            
            return true;

        }

        /// <summary>
        /// シート名を取得
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private static bool getSheetData(string strFileName)
        {
            lstSheet.Clear();
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                wf.LogPrint("データ確認中");

                //シート名取得
                wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                NPOI.SS.UserModel.ISheet sheet = new NPOI.XSSF.UserModel.XSSFSheet();

                //必要なシートを　支給記録　と仮定する
                for (int cnt = 0; cnt < wb.NumberOfSheets; cnt++)
                {
                    if (wb.GetSheetName(cnt).ToString().Contains("支給記録"))
                        lstSheet.Add(wb.GetSheetName(cnt).ToString());
                }

                if(lstSheet.Count>1)
                {
                    System.Windows.Forms.MessageBox.Show("シートが複数あります");
                    return false;
                }

                wf.LogPrint("データ取得中");
                //取得したシート名でデータを登録する
                sheet = wb.GetSheet(lstSheet[0].ToString());

                //DB登録
                getExcelData(sheet,wf);

                

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();

            }
        }


        /// <summary>
        /// シートの行をKKdataテーブルに登録
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="wf"></param>
        private static void getExcelData(NPOI.SS.UserModel.ISheet ws,WaitForm wf)
        {
            //先に全削除
            DB.Command cmd = new DB.Command(DB.Main,"truncate table kkdata");
            cmd.TryExecuteNonQuery();


            KKData cls = new KKData();

            NPOI.SS.UserModel.IRow row;

            wf.SetMax(ws.LastRowNum);
            wf.LogPrint("取り込み中です");
            
            for (int r = 1; r <= ws.LastRowNum; r++)
            {
                string strRow = string.Empty;
                row = ws.GetRow(r);
                int c = 0;

                cls.shibu_cd = row.GetCell(c++).ToString();
                cls.hmark = row.GetCell(c++).ToString();
                cls.hnum = row.GetCell(c++).ToString();
                cls.taisho_no = row.GetCell(c++).ToString();
                cls.uke_shibucd = row.GetCell(c++).ToString();
                cls.uke_scanner = row.GetCell(c++).ToString();
                cls.uke_ukeymd = row.GetCell(c++).ToString();
                cls.uke_gyomu = row.GetCell(c++).ToString();
                cls.uke_renban = row.GetCell(c++).ToString();
                cls.kousei = row.GetCell(c++).ToString();
                cls.hkana = row.GetCell(c++).ToString();
                cls.hname = row.GetCell(c++).ToString();
                cls.hzip = row.GetCell(c++).ToString();
                cls.hadd1 = row.GetCell(c++).ToString();
                cls.hadd2 = row.GetCell(c++).ToString();
                cls.hadd3 = row.GetCell(c++).ToString();
                cls.ym = row.GetCell(c++).ToString();
                cls.honke = row.GetCell(c++).ToString();
                cls.gender = row.GetCell(c++).ToString();
                cls.jbirth = row.GetCell(c++).ToString();
                cls.jname = row.GetCell(c++).ToString();
                cls.shoken1 = row.GetCell(c++).ToString();
                cls.shoken2 = row.GetCell(c++).ToString();
                cls.shoken3 = row.GetCell(c++).ToString();
                cls.shoken4 = row.GetCell(c++).ToString();
                cls.shoken5 = row.GetCell(c++).ToString();
                cls.days = int.Parse(row.GetCell(c++).ToString());
                cls.jkaicode = row.GetCell(c++).ToString();
                cls.jcode = row.GetCell(c++).ToString();
                cls.total = int.Parse(row.GetCell(c++).ToString());
                cls.kettei = int.Parse(row.GetCell(c++).ToString());
                cls.bui = int.Parse(row.GetCell(c++).ToString());

                cls.cym = row.GetCell(c++).ToString();//柔整で作成した診療年月
                cls.medi_key = row.GetCell(c++).StringCellValue;//柔整で作成したプライマリキー

                cls.medi_fusho1 = string.Empty;
                cls.medi_shoken1 = string.Empty;
                cls.medi_days1 = 0;
                cls.medi_tenki1 = 0;
                cls.medi_fusho2 = string.Empty;
                cls.medi_shoken2 = string.Empty;
                cls.medi_days2 = 0;
                cls.medi_tenki2 = 0;
                cls.medi_fusho3 = string.Empty;
                cls.medi_shoken3 = string.Empty;
                cls.medi_days3 = 0;
                cls.medi_tenki3 = 0;
                cls.medi_fusho4 = string.Empty;
                cls.medi_shoken4 = string.Empty;
                cls.medi_days4 = 0;
                cls.medi_tenki4 = 0;
                cls.medi_fusho5 = string.Empty;
                cls.medi_shoken5 = string.Empty;
                cls.medi_days5 = 0;
                cls.medi_tenki5 = 0;

                wf.InvokeValue++;

                //プライマリキーで判別するので、重複は飛ばす
                if(!DB.Main.Insert(cls)) continue;
            }
            

        }


        /// <summary>
        /// シートを取得し、DBにコピー
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        private static bool Import2DB(string strFileName)
        {
            KKData cls = new KKData();
            

            try
            {

                System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection();
                cn.ConnectionString =
                    $"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    $"data source={strFileName};" +
                    $"Extended Properties=Excel 12.0 xml;" +
                    $"HDR=YES";

                cn.Open();

                System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter();
                System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand();
                System.Data.DataTable dt = new System.Data.DataTable();
                System.Data.OleDb.OleDbDataReader drd;

                //シートのデータ取得
                cmd.CommandText = $"select * from {lstSheet[0].ToString()}";
                da.SelectCommand = cmd;
                drd = cmd.ExecuteReader();

                DB.Transaction tran = new DB.Transaction(DB.Main);

                
                while (drd.Read())
                {
           
                    cls.shibu_cd = (string)drd.GetValue(0);
                    cls.hmark = (string)drd.GetValue(1);
                    cls.hnum = (string)drd.GetValue(2);
                    cls.taisho_no = (string)drd.GetValue(3);
                    cls.uke_shibucd = (string)drd.GetValue(4);
                    cls.uke_scanner = (string)drd.GetValue(5);
                    cls.uke_ukeymd = (string)drd.GetValue(6);
                    cls.uke_gyomu = (string)drd.GetValue(7);
                    cls.uke_renban = (string)drd.GetValue(8);
                    cls.kousei = (string)drd.GetValue(9);
                    cls.hkana = (string)drd.GetValue(10);
                    cls.hname = (string)drd.GetValue(11);
                    cls.hzip = (string)drd.GetValue(12);
                    cls.hadd1 = (string)drd.GetValue(13);
                    cls.hadd2 = (string)drd.GetValue(14);
                    cls.hadd3 = (string)drd.GetValue(15);
                    cls.ym = (string)drd.GetValue(16);
                    cls.honke = (string)drd.GetValue(17);
                    cls.gender = (string)drd.GetValue(18);
                    cls.jbirth = (string)drd.GetValue(19);
                    cls.jname = (string)drd.GetValue(20);
                    cls.shoken1 = (string)drd.GetValue(21);
                    cls.shoken2 = (string)drd.GetValue(22);
                    cls.shoken3 = (string)drd.GetValue(23);
                    cls.shoken4 = (string)drd.GetValue(24);
                    cls.shoken5 = (string)drd.GetValue(25);
                    cls.days = (int)drd.GetValue(26);
                    cls.jkaicode = (string)drd.GetValue(27);
                    cls.jcode = (string)drd.GetValue(28);
                    cls.total = (int)drd.GetValue(29);
                    cls.kettei = (int)drd.GetValue(30);
                    cls.bui = (int)drd.GetValue(31);
                    cls.medi_key = (string)drd.GetValue(32);

                    cls.medi_fusho1 = string.Empty;
                    cls.medi_shoken1 = string.Empty;
                    cls.medi_days1 = 0;
                    cls.medi_tenki1 = 0;
                    cls.medi_fusho2 = string.Empty;
                    cls.medi_shoken2 = string.Empty;
                    cls.medi_days2 = 0;
                    cls.medi_tenki2 = 0;
                    cls.medi_fusho3 = string.Empty;
                    cls.medi_shoken3 = string.Empty;
                    cls.medi_days3 = 0;
                    cls.medi_tenki3 = 0;
                    cls.medi_fusho4 = string.Empty;
                    cls.medi_shoken4 = string.Empty;
                    cls.medi_days4 = 0;
                    cls.medi_tenki4 = 0;
                    cls.medi_fusho5 = string.Empty;
                    cls.medi_shoken5 = string.Empty;
                    cls.medi_days5 = 0;
                    cls.medi_tenki5 = 0;

                    DB.Main.Insert(cls,tran);
                    
                }


                //postgresに入れる

                tran.Commit();



                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }

        }
        #endregion


    }

}