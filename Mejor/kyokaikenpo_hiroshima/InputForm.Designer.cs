﻿namespace Mejor.kyokaikenpo_hiroshima
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelFamily = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.labelOCR = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.verifyBoxF5M = new Mejor.VerifyBox();
            this.verifyBoxF5Y = new Mejor.VerifyBox();
            this.verifyBoxF5Nengo = new Mejor.VerifyBox();
            this.verifyBoxF5Tenki = new Mejor.VerifyBox();
            this.verifyBoxF5Days = new Mejor.VerifyBox();
            this.verifyBoxF4M = new Mejor.VerifyBox();
            this.verifyBoxF4Y = new Mejor.VerifyBox();
            this.verifyBoxF4Nengo = new Mejor.VerifyBox();
            this.verifyBoxF4Tenki = new Mejor.VerifyBox();
            this.verifyBoxF4Days = new Mejor.VerifyBox();
            this.verifyBoxF3M = new Mejor.VerifyBox();
            this.verifyBoxF3Y = new Mejor.VerifyBox();
            this.verifyBoxF3Nengo = new Mejor.VerifyBox();
            this.verifyBoxF3Tenki = new Mejor.VerifyBox();
            this.verifyBoxF3Days = new Mejor.VerifyBox();
            this.verifyBoxF2M = new Mejor.VerifyBox();
            this.verifyBoxF2Y = new Mejor.VerifyBox();
            this.verifyBoxF2Nengo = new Mejor.VerifyBox();
            this.verifyBoxF2Tenki = new Mejor.VerifyBox();
            this.verifyBoxF2Days = new Mejor.VerifyBox();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.verifyBoxF1Nengo = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxF1Tenki = new Mejor.VerifyBox();
            this.verifyBoxF1Days = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxHnumM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxHosNumber = new Mejor.VerifyBox();
            this.verifyBoxPref = new Mejor.VerifyBox();
            this.verifyBoxAppType = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelPref = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 13;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 22);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(190, 22);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(6, 65);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(254, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 36);
            this.label6.TabIndex = 14;
            this.label6.Text = "本人:2 六歳:4\r\n家族:6 高齢:8\r\n高７:0";
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(224, 61);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(59, 12);
            this.labelFamily.TabIndex = 12;
            this.labelFamily.Text = "本人/家族";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(21, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 14;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 54);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 450);
            this.scrollPictureControl1.TabIndex = 19;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 695);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(29, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 11);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(155, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextV = "";
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.verifyBoxF5M);
            this.panelTotal.Controls.Add(this.verifyBoxF5Y);
            this.panelTotal.Controls.Add(this.verifyBoxF5Nengo);
            this.panelTotal.Controls.Add(this.verifyBoxF5Tenki);
            this.panelTotal.Controls.Add(this.verifyBoxF5Days);
            this.panelTotal.Controls.Add(this.verifyBoxF4M);
            this.panelTotal.Controls.Add(this.verifyBoxF4Y);
            this.panelTotal.Controls.Add(this.verifyBoxF4Nengo);
            this.panelTotal.Controls.Add(this.verifyBoxF4Tenki);
            this.panelTotal.Controls.Add(this.verifyBoxF4Days);
            this.panelTotal.Controls.Add(this.verifyBoxF3M);
            this.panelTotal.Controls.Add(this.verifyBoxF3Y);
            this.panelTotal.Controls.Add(this.verifyBoxF3Nengo);
            this.panelTotal.Controls.Add(this.verifyBoxF3Tenki);
            this.panelTotal.Controls.Add(this.verifyBoxF3Days);
            this.panelTotal.Controls.Add(this.verifyBoxF2M);
            this.panelTotal.Controls.Add(this.verifyBoxF2Y);
            this.panelTotal.Controls.Add(this.verifyBoxF2Nengo);
            this.panelTotal.Controls.Add(this.verifyBoxF2Tenki);
            this.panelTotal.Controls.Add(this.verifyBoxF2Days);
            this.panelTotal.Controls.Add(this.verifyBoxF1M);
            this.panelTotal.Controls.Add(this.verifyBoxF1Y);
            this.panelTotal.Controls.Add(this.verifyBoxF1Nengo);
            this.panelTotal.Controls.Add(this.label26);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.label13);
            this.panelTotal.Controls.Add(this.label25);
            this.panelTotal.Controls.Add(this.label14);
            this.panelTotal.Controls.Add(this.verifyBoxF5);
            this.panelTotal.Controls.Add(this.verifyBoxF4);
            this.panelTotal.Controls.Add(this.verifyBoxF3);
            this.panelTotal.Controls.Add(this.verifyBoxF1Tenki);
            this.panelTotal.Controls.Add(this.verifyBoxF1Days);
            this.panelTotal.Controls.Add(this.verifyBoxF2);
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.label22);
            this.panelTotal.Controls.Add(this.label21);
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.label15);
            this.panelTotal.Controls.Add(this.label9);
            this.panelTotal.Controls.Add(this.labelDays);
            this.panelTotal.Location = new System.Drawing.Point(9, 504);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(850, 185);
            this.panelTotal.TabIndex = 10;
            // 
            // verifyBoxF5M
            // 
            this.verifyBoxF5M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5M.Location = new System.Drawing.Point(438, 148);
            this.verifyBoxF5M.MaxLength = 2;
            this.verifyBoxF5M.Name = "verifyBoxF5M";
            this.verifyBoxF5M.NewLine = false;
            this.verifyBoxF5M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5M.TabIndex = 33;
            this.verifyBoxF5M.TextV = "";
            // 
            // verifyBoxF5Y
            // 
            this.verifyBoxF5Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Y.Location = new System.Drawing.Point(407, 148);
            this.verifyBoxF5Y.MaxLength = 2;
            this.verifyBoxF5Y.Name = "verifyBoxF5Y";
            this.verifyBoxF5Y.NewLine = false;
            this.verifyBoxF5Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5Y.TabIndex = 32;
            this.verifyBoxF5Y.TextV = "";
            // 
            // verifyBoxF5Nengo
            // 
            this.verifyBoxF5Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Nengo.Location = new System.Drawing.Point(375, 148);
            this.verifyBoxF5Nengo.MaxLength = 1;
            this.verifyBoxF5Nengo.Name = "verifyBoxF5Nengo";
            this.verifyBoxF5Nengo.NewLine = false;
            this.verifyBoxF5Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF5Nengo.TabIndex = 31;
            this.verifyBoxF5Nengo.TextV = "";
            // 
            // verifyBoxF5Tenki
            // 
            this.verifyBoxF5Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Tenki.Location = new System.Drawing.Point(525, 148);
            this.verifyBoxF5Tenki.Name = "verifyBoxF5Tenki";
            this.verifyBoxF5Tenki.NewLine = false;
            this.verifyBoxF5Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF5Tenki.TabIndex = 36;
            this.verifyBoxF5Tenki.TextV = "";
            // 
            // verifyBoxF5Days
            // 
            this.verifyBoxF5Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF5Days.Location = new System.Drawing.Point(479, 148);
            this.verifyBoxF5Days.Name = "verifyBoxF5Days";
            this.verifyBoxF5Days.NewLine = false;
            this.verifyBoxF5Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF5Days.TabIndex = 35;
            this.verifyBoxF5Days.TextV = "";
            // 
            // verifyBoxF4M
            // 
            this.verifyBoxF4M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4M.Location = new System.Drawing.Point(438, 120);
            this.verifyBoxF4M.MaxLength = 2;
            this.verifyBoxF4M.Name = "verifyBoxF4M";
            this.verifyBoxF4M.NewLine = false;
            this.verifyBoxF4M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4M.TabIndex = 26;
            this.verifyBoxF4M.TextV = "";
            // 
            // verifyBoxF4Y
            // 
            this.verifyBoxF4Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Y.Location = new System.Drawing.Point(407, 120);
            this.verifyBoxF4Y.MaxLength = 2;
            this.verifyBoxF4Y.Name = "verifyBoxF4Y";
            this.verifyBoxF4Y.NewLine = false;
            this.verifyBoxF4Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4Y.TabIndex = 25;
            this.verifyBoxF4Y.TextV = "";
            // 
            // verifyBoxF4Nengo
            // 
            this.verifyBoxF4Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Nengo.Location = new System.Drawing.Point(375, 120);
            this.verifyBoxF4Nengo.MaxLength = 1;
            this.verifyBoxF4Nengo.Name = "verifyBoxF4Nengo";
            this.verifyBoxF4Nengo.NewLine = false;
            this.verifyBoxF4Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF4Nengo.TabIndex = 24;
            this.verifyBoxF4Nengo.TextV = "";
            // 
            // verifyBoxF4Tenki
            // 
            this.verifyBoxF4Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Tenki.Location = new System.Drawing.Point(525, 120);
            this.verifyBoxF4Tenki.Name = "verifyBoxF4Tenki";
            this.verifyBoxF4Tenki.NewLine = false;
            this.verifyBoxF4Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF4Tenki.TabIndex = 29;
            this.verifyBoxF4Tenki.TextV = "";
            // 
            // verifyBoxF4Days
            // 
            this.verifyBoxF4Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF4Days.Location = new System.Drawing.Point(479, 120);
            this.verifyBoxF4Days.Name = "verifyBoxF4Days";
            this.verifyBoxF4Days.NewLine = false;
            this.verifyBoxF4Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF4Days.TabIndex = 28;
            this.verifyBoxF4Days.TextV = "";
            // 
            // verifyBoxF3M
            // 
            this.verifyBoxF3M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3M.Location = new System.Drawing.Point(438, 92);
            this.verifyBoxF3M.MaxLength = 2;
            this.verifyBoxF3M.Name = "verifyBoxF3M";
            this.verifyBoxF3M.NewLine = false;
            this.verifyBoxF3M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3M.TabIndex = 17;
            this.verifyBoxF3M.TextV = "";
            // 
            // verifyBoxF3Y
            // 
            this.verifyBoxF3Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Y.Location = new System.Drawing.Point(407, 92);
            this.verifyBoxF3Y.MaxLength = 2;
            this.verifyBoxF3Y.Name = "verifyBoxF3Y";
            this.verifyBoxF3Y.NewLine = false;
            this.verifyBoxF3Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3Y.TabIndex = 16;
            this.verifyBoxF3Y.TextV = "";
            // 
            // verifyBoxF3Nengo
            // 
            this.verifyBoxF3Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Nengo.Location = new System.Drawing.Point(375, 92);
            this.verifyBoxF3Nengo.MaxLength = 1;
            this.verifyBoxF3Nengo.Name = "verifyBoxF3Nengo";
            this.verifyBoxF3Nengo.NewLine = false;
            this.verifyBoxF3Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF3Nengo.TabIndex = 15;
            this.verifyBoxF3Nengo.TextV = "";
            // 
            // verifyBoxF3Tenki
            // 
            this.verifyBoxF3Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Tenki.Location = new System.Drawing.Point(525, 92);
            this.verifyBoxF3Tenki.Name = "verifyBoxF3Tenki";
            this.verifyBoxF3Tenki.NewLine = false;
            this.verifyBoxF3Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF3Tenki.TabIndex = 20;
            this.verifyBoxF3Tenki.TextV = "";
            // 
            // verifyBoxF3Days
            // 
            this.verifyBoxF3Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF3Days.Location = new System.Drawing.Point(479, 92);
            this.verifyBoxF3Days.Name = "verifyBoxF3Days";
            this.verifyBoxF3Days.NewLine = false;
            this.verifyBoxF3Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF3Days.TabIndex = 19;
            this.verifyBoxF3Days.TextV = "";
            // 
            // verifyBoxF2M
            // 
            this.verifyBoxF2M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2M.Location = new System.Drawing.Point(438, 65);
            this.verifyBoxF2M.MaxLength = 2;
            this.verifyBoxF2M.Name = "verifyBoxF2M";
            this.verifyBoxF2M.NewLine = false;
            this.verifyBoxF2M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2M.TabIndex = 10;
            this.verifyBoxF2M.TextV = "";
            // 
            // verifyBoxF2Y
            // 
            this.verifyBoxF2Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Y.Location = new System.Drawing.Point(407, 65);
            this.verifyBoxF2Y.MaxLength = 2;
            this.verifyBoxF2Y.Name = "verifyBoxF2Y";
            this.verifyBoxF2Y.NewLine = false;
            this.verifyBoxF2Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2Y.TabIndex = 9;
            this.verifyBoxF2Y.TextV = "";
            // 
            // verifyBoxF2Nengo
            // 
            this.verifyBoxF2Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Nengo.Location = new System.Drawing.Point(375, 65);
            this.verifyBoxF2Nengo.MaxLength = 1;
            this.verifyBoxF2Nengo.Name = "verifyBoxF2Nengo";
            this.verifyBoxF2Nengo.NewLine = false;
            this.verifyBoxF2Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF2Nengo.TabIndex = 8;
            this.verifyBoxF2Nengo.TextV = "";
            // 
            // verifyBoxF2Tenki
            // 
            this.verifyBoxF2Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Tenki.Location = new System.Drawing.Point(525, 65);
            this.verifyBoxF2Tenki.Name = "verifyBoxF2Tenki";
            this.verifyBoxF2Tenki.NewLine = false;
            this.verifyBoxF2Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF2Tenki.TabIndex = 13;
            this.verifyBoxF2Tenki.TextV = "";
            // 
            // verifyBoxF2Days
            // 
            this.verifyBoxF2Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF2Days.Location = new System.Drawing.Point(479, 65);
            this.verifyBoxF2Days.Name = "verifyBoxF2Days";
            this.verifyBoxF2Days.NewLine = false;
            this.verifyBoxF2Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF2Days.TabIndex = 12;
            this.verifyBoxF2Days.TextV = "";
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(438, 37);
            this.verifyBoxF1M.MaxLength = 2;
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1M.TabIndex = 3;
            this.verifyBoxF1M.TextV = "";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(407, 37);
            this.verifyBoxF1Y.MaxLength = 2;
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Y.TabIndex = 2;
            this.verifyBoxF1Y.TextV = "";
            // 
            // verifyBoxF1Nengo
            // 
            this.verifyBoxF1Nengo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Nengo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Nengo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Nengo.Location = new System.Drawing.Point(375, 37);
            this.verifyBoxF1Nengo.MaxLength = 1;
            this.verifyBoxF1Nengo.Name = "verifyBoxF1Nengo";
            this.verifyBoxF1Nengo.NewLine = false;
            this.verifyBoxF1Nengo.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1Nengo.TabIndex = 1;
            this.verifyBoxF1Nengo.TextV = "";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label26.Location = new System.Drawing.Point(522, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(181, 12);
            this.label26.TabIndex = 26;
            this.label26.Text = "継続：１、治癒：２、中止：３、転医：４";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(296, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "昭:３，平:４、令:５";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(442, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 31;
            this.label13.Text = "月";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(376, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 12);
            this.label25.TabIndex = 29;
            this.label25.Text = "年号";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(412, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 29;
            this.label14.Text = "年";
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(65, 149);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF5.TabIndex = 30;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(65, 121);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF4.TabIndex = 23;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(65, 93);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF3.TabIndex = 14;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF1Tenki
            // 
            this.verifyBoxF1Tenki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Tenki.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Tenki.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Tenki.Location = new System.Drawing.Point(525, 37);
            this.verifyBoxF1Tenki.Name = "verifyBoxF1Tenki";
            this.verifyBoxF1Tenki.NewLine = false;
            this.verifyBoxF1Tenki.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF1Tenki.TabIndex = 6;
            this.verifyBoxF1Tenki.TextV = "";
            // 
            // verifyBoxF1Days
            // 
            this.verifyBoxF1Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Days.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Days.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Days.Location = new System.Drawing.Point(479, 37);
            this.verifyBoxF1Days.Name = "verifyBoxF1Days";
            this.verifyBoxF1Days.NewLine = false;
            this.verifyBoxF1Days.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxF1Days.TabIndex = 5;
            this.verifyBoxF1Days.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(65, 66);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF2.TabIndex = 7;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(65, 38);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(300, 23);
            this.verifyBoxF1.TabIndex = 0;
            this.verifyBoxF1.TextV = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 154);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 8;
            this.label22.Text = "負傷名5";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 126);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 6;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 4;
            this.label20.Text = "負傷名3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 2;
            this.label11.Text = "負傷名2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(529, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 10;
            this.label15.Text = "転帰";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷名1";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(477, 22);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(41, 12);
            this.labelDays.TabIndex = 10;
            this.labelDays.Text = "実日数";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxHnumM);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxHosNumber);
            this.panelHnum.Controls.Add(this.verifyBoxPref);
            this.panelHnum.Controls.Add(this.verifyBoxAppType);
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelFamily);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label6);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Controls.Add(this.label23);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.label24);
            this.panelHnum.Controls.Add(this.labelPref);
            this.panelHnum.Location = new System.Drawing.Point(300, 0);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(600, 50);
            this.panelHnum.TabIndex = 7;
            // 
            // verifyBoxHnumM
            // 
            this.verifyBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumM.Location = new System.Drawing.Point(8, 80);
            this.verifyBoxHnumM.Name = "verifyBoxHnumM";
            this.verifyBoxHnumM.NewLine = false;
            this.verifyBoxHnumM.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnumM.TabIndex = 10;
            this.verifyBoxHnumM.TextV = "";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(109, 80);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(111, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxHosNumber
            // 
            this.verifyBoxHosNumber.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosNumber.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosNumber.Location = new System.Drawing.Point(548, 11);
            this.verifyBoxHosNumber.Name = "verifyBoxHosNumber";
            this.verifyBoxHosNumber.NewLine = false;
            this.verifyBoxHosNumber.Size = new System.Drawing.Size(96, 23);
            this.verifyBoxHosNumber.TabIndex = 8;
            this.verifyBoxHosNumber.TextV = "";
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPref.Location = new System.Drawing.Point(433, 11);
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.NewLine = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxPref.TabIndex = 6;
            this.verifyBoxPref.TextV = "";
            // 
            // verifyBoxAppType
            // 
            this.verifyBoxAppType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAppType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAppType.Location = new System.Drawing.Point(328, 11);
            this.verifyBoxAppType.Name = "verifyBoxAppType";
            this.verifyBoxAppType.NewLine = false;
            this.verifyBoxAppType.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxAppType.TabIndex = 4;
            this.verifyBoxAppType.TextV = "";
            this.verifyBoxAppType.TextChanged += new System.EventHandler(this.verifyBoxAppType_TextChanged);
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(226, 80);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 13;
            this.verifyBoxFamily.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(557, 80);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 24;
            this.verifyBoxBD.TextV = "";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(514, 80);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 22;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(471, 80);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 20;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(420, 80);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 19;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(345, 80);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 16;
            this.verifyBoxSex.TextV = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(417, 66);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 18;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(343, 65);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 15;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(446, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "昭:3\r\n平:4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(372, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(583, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 25;
            this.label16.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(540, 90);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 23;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(497, 90);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 21;
            this.label18.Text = "年";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(275, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "申請区分";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(275, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 3;
            this.label19.Text = "針灸は[7]\r\n";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(495, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 24);
            this.label24.TabIndex = 7;
            this.label24.Text = "医療機関\r\nコード";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPref
            // 
            this.labelPref.AutoSize = true;
            this.labelPref.Location = new System.Drawing.Point(380, 13);
            this.labelPref.Name = "labelPref";
            this.labelPref.Size = new System.Drawing.Size(53, 24);
            this.labelPref.TabIndex = 5;
            this.labelPref.Text = "都道府県\r\n番号";
            this.labelPref.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Panel panelHnum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxFamily;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF1Days;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private VerifyBox verifyBoxHosNumber;
        private VerifyBox verifyBoxPref;
        private VerifyBox verifyBoxAppType;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelPref;
        private VerifyBox verifyBoxHnumM;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private VerifyBox verifyBoxF5M;
        private VerifyBox verifyBoxF5Y;
        private VerifyBox verifyBoxF5Nengo;
        private VerifyBox verifyBoxF5Tenki;
        private VerifyBox verifyBoxF5Days;
        private VerifyBox verifyBoxF4M;
        private VerifyBox verifyBoxF4Y;
        private VerifyBox verifyBoxF4Nengo;
        private VerifyBox verifyBoxF4Tenki;
        private VerifyBox verifyBoxF4Days;
        private VerifyBox verifyBoxF3M;
        private VerifyBox verifyBoxF3Y;
        private VerifyBox verifyBoxF3Nengo;
        private VerifyBox verifyBoxF3Tenki;
        private VerifyBox verifyBoxF3Days;
        private VerifyBox verifyBoxF2M;
        private VerifyBox verifyBoxF2Y;
        private VerifyBox verifyBoxF2Nengo;
        private VerifyBox verifyBoxF2Tenki;
        private VerifyBox verifyBoxF2Days;
        private VerifyBox verifyBoxF1M;
        private VerifyBox verifyBoxF1Y;
        private VerifyBox verifyBoxF1Nengo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxF1Tenki;
        private System.Windows.Forms.Label label15;
    }
}