﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

using System.Windows.Media.Imaging;
using ZXing.Common;
using System.Windows.Media.Media3D;

namespace Mejor
{
    public class TiffUtility
    {
        const int StripOffsets = 0x0111;
        const int StripByteCounts = 0x0117;
        const int JPEGInterchangeFormat = 0x0201;

        public class TiffTag
        {
            public int Code;
            public int TypeNum;
            public int Count;
            public byte[] Data = new byte[4];
            public byte[] ExData;
        }

        private int stripCount = 0;
        private List<TiffTag> tags = new List<TiffTag>();
        private List<byte[]> imageData = new List<byte[]>();
        private int imageDataTotalLen => imageData.Sum(data => data.Length);
        private byte[] optionData;

        private static int typeNumGetByteCount(int typeNum)
        {
            if (typeNum == 1) return 1;     //コード１…BYTE型(１バイト整数)
            if (typeNum == 2) return 1;     //コード２…ASCII型(１バイトのASCII文字)
            if (typeNum == 3) return 2;     //コード３…SHORT型(２バイト短整数)
            if (typeNum == 4) return 4;     //コード４…LONG型(４バイト長整数)
            if (typeNum == 5) return 8;     //コード５…RATIONAL型(８バイト分数、４バイトの分子とそれに続く４バイトの分母)
            if (typeNum == 6) return 1;     //コード６…SBYTE型(１バイト符号付き整数)
            if (typeNum == 7) return 1;     //コード７…UNDEFINED型(あらゆる１バイトデータ)
            if (typeNum == 8) return 2;     //コード８…SSHORT型(２バイト符号付き短整数)
            if (typeNum == 9) return 4;     //コード９…SLONG型(４バイト符号付き長整数)
            if (typeNum == 10) return 8;    //コード10…SRATIONAL型(８バイト符号付き分数、４バイトの分子とそれに続く４バイトの分母)
            if (typeNum == 11) return 4;    //コード11…FLOAT型(４バイト実数、IEEE浮動小数点形式)
            if (typeNum == 12) return 8;    //コード12…DOUBLE型(８バイト倍精度実数、IEEE倍精度浮動小数点形式)
            return 0;
        }

        private void getImageDatas(byte[] bs)
        {
            var dataOffset = tags.First(t => t.Code == StripOffsets);
            var dataLength = tags.First(t => t.Code == StripByteCounts);

            if (stripCount == 1)
            {
                var data = new byte[BitConverter.ToInt32(dataLength.Data, 0)];
                Array.Copy(bs, BitConverter.ToInt32(dataOffset.Data, 0), data, 0, data.Length);
                imageData.Add(data);
                return;
            }

            for (int i = 0; i < stripCount; i++)
            {
                var offset = BitConverter.ToUInt32(dataOffset.ExData, i * 4);
                var length = BitConverter.ToUInt32(dataLength.ExData, i * 4);
                var data = new byte[length];
                Array.Copy(bs, offset, data, 0, length);
                imageData.Add(data);
            }
        }

        /// <summary>
        /// シングルページTIFFファイルのTiffUtilityインスタンスを取得します。
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        private static TiffUtility getIFDs(byte[] bs)
        {
            var result = getTiffUtilitys(bs);
            if (result != null && result.Count == 1) return result[0];
            throw new Exception("このTIFFファイルはマルチページになっています。");
        }

        /// <summary>
        /// マルチページTIFFファイルの各ページのTiffUtilityインスタンスを取得します。
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        private static List<TiffUtility> getTiffUtilitys(byte[] bs)
        {
            var list = new List<TiffUtility>();

            var intelMode = bs[0] == 0x49;
            int readIndex = BitConverter.ToInt32(bs, 4);

            for (int e = 0; e < 1000; e++)
            {
                int tagCount = BitConverter.ToInt16(bs, readIndex);
                readIndex += 2;

                var ifd = new TiffUtility();

                for (int i = 0; i < tagCount; i++)
                {
                    var t = new TiffTag();
                    t.Code = BitConverter.ToUInt16(bs, readIndex + i * 12);
                    t.TypeNum = BitConverter.ToInt16(bs, readIndex + i * 12 + 2);
                    t.Count = BitConverter.ToInt16(bs, readIndex + i * 12 + 4);
                    Array.Copy(bs, readIndex + i * 12 + 8, t.Data, 0, 4);

                    //ポインタ処理
                    var length = typeNumGetByteCount(t.TypeNum) * t.Count;
                    if (length > 4)
                    {
                        t.ExData = new byte[length];
                        Array.Copy(bs, BitConverter.ToInt32(t.Data, 0), t.ExData, 0, length);
                        t.Data = new byte[4];
                    }

                    ifd.tags.Add(t);
                    if (t.Code == StripOffsets) ifd.stripCount = t.Count;
                }

                readIndex = BitConverter.ToInt32(bs, readIndex + tagCount * 12);
                ifd.getImageDatas(bs);

                list.Add(ifd);

                if (readIndex == 0) return list;
            }
            throw new Exception("IFD count is over 1000. Maybe file broken.");
        }

        private void setDataPointers(List<int> list)
        {
            var tag = tags.First(t => t.Code == StripOffsets);
            var jpgTag = tags.FirstOrDefault(t => t.Code == JPEGInterchangeFormat);

            if (list.Count == 1)
            {
                tag.Data = BitConverter.GetBytes(list[0]);
                if (jpgTag != null) jpgTag.Data = tag.Data;
            }
            else
            {
                tag.ExData = new byte[list.Count * 4];
                if (jpgTag != null) Array.Copy(BitConverter.GetBytes(list[0]), 0, jpgTag.Data, 0, 4);

                for (int i = 0; i < list.Count; i++)
                    Array.Copy(BitConverter.GetBytes(list[i]), 0, tag.ExData, i * 4, 4);

                tag.Data = new byte[4];
            }
        }

        private byte[] createNewIFD(int startAdd)
        {
            var bl = new List<byte>();
            var optionBl = new List<byte>();
            startAdd = startAdd + tags.Count * 12 + 6;

            //IFD数を記録
            bl.AddRange(BitConverter.GetBytes((short)tags.Count));

            var act = new Action<TiffTag>(t =>
            {
                bl.AddRange(BitConverter.GetBytes((short)t.Code));
                bl.AddRange(BitConverter.GetBytes((short)t.TypeNum));
                bl.AddRange(BitConverter.GetBytes(t.Count));

                var len = t.Count * typeNumGetByteCount(t.TypeNum);
                if (len > 4)
                {
                    bl.AddRange(BitConverter.GetBytes(startAdd + optionBl.Count));
                    optionBl.AddRange(t.ExData);
                }
                else
                {
                    bl.AddRange(t.Data);
                }
            });

            foreach (var item in tags) act(item);

            optionData = optionBl.ToArray();
            return bl.ToArray();
        }

        /// <summary>
        /// 複数枚のtiffであればマルチページ化し、1枚であればコピーします
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="fileNames"></param>
        /// <param name="saveFileName"></param>
        public static bool MargeOrCopyTiff(FastCopy fc, IEnumerable<string> fileNames, string saveFileName)
        {
            if (fileNames.Count() == 1)
            {
                return fc.FileCopy(fileNames.First(), saveFileName);
            }
            else
            {
                return MargeTiff(fileNames, saveFileName);
            }
        }

        public static int GetFrameCount(string tiffFileName)
        {
            int frameCount = 0;
            using (var fs = new FileStream(tiffFileName, FileMode.Open))
            {
                byte[] bs;
                bs = new byte[fs.Length];
                fs.Read(bs, 0, (int)fs.Length);

                var intelMode = bs[0] == 0x49;
                int readIndex = BitConverter.ToInt32(bs, 4);

                for (int e = 0; e < 1000; e++)
                {
                    frameCount++;
                    int tagCount = BitConverter.ToInt16(bs, readIndex);
                    readIndex += 2;

                    readIndex = BitConverter.ToInt32(bs, readIndex + tagCount * 12);
                    if (readIndex == 0) return frameCount;
                }
                throw new Exception("IFD count is over 1000. Maybe file broken.");
            }
        }

        /// <summary>
        /// TIFFファイルのバイトストリームを取得します。リストは各ページごとに分かれています。
        /// ストリームはそのままTIFFファイルとして書き出すことができます。
        /// </summary>
        /// <param name="tiffFileName"></param>
        /// <returns></returns>
        public static List<byte[]> GetFrameStreams(string tiffFileName)
        {
            var tiffs = new List<byte[]>();

            using (var fs = new FileStream(tiffFileName, FileMode.Open))
            {
                //バイトストリームの取得
                var bs = new byte[fs.Length];
                fs.Read(bs, 0, (int)fs.Length);

                //各ページのTIFF情報の取得
                var tus = getTiffUtilitys(bs);

                //分割
                foreach (var tu in tus)
                {
                    //====================================================
                    //
                    // 以下の構成でTIFFを作成する
                    //
                    // 1.ヘッダー
                    // 2.最初のIFD開始座標
                    // 3.画像データ
                    // 4.IFD
                    // 5.次のIFD開始座標（Single : 00 00 00 00）
                    // 6.各種テーブル（各データポインタが指すテーブル）
                    //
                    //====================================================

                    //TIFFファイルの作成
                    var tiff = new List<byte>();

                    //ヘッダ
                    tiff.AddRange(new byte[] { 0x49, 0x49, 0x2A, 0x00 });

                    //最初のIFD開始座標（先に画像データがどれくらいあるか計算）
                    var ifdAddress = tiff.Count + tu.imageDataTotalLen + 4; //4:firstIFD
                    tiff.AddRange(BitConverter.GetBytes(ifdAddress));

                    //IFD
                    var dataPointers = new List<int>();
                    foreach (var strip in tu.imageData)
                    {
                        dataPointers.Add(tiff.Count);   //strip座標
                        tiff.AddRange(strip);
                    }
                    tu.setDataPointers(dataPointers);
                    tiff.AddRange(tu.createNewIFD(tiff.Count));

                    //次のIFD座標（シングルページになるので終了コード）
                    tiff.AddRange(new byte[] { 0x00, 0x00, 0x00, 0x00 });

                    //各種テーブル
                    tiff.AddRange(tu.optionData);

                    tiffs.Add(tiff.ToArray());
                }
            }

            return tiffs;
        }

        /// <summary>
        /// 指定のTIFFファイルのSystem.Drawing.Imageインスタンスを取得します。リストは各ページごとに分かれています。
        /// </summary>
        /// <param name="tiffFileName"></param>
        /// <returns></returns>
        public static List<Image> GetFrameImages(string tiffFileName)
        {
            var images = new List<Image>();

            var tiffs = GetFrameStreams(tiffFileName);
            foreach (var tiff in tiffs)
            {
                var ms = new MemoryStream(tiff);
                images.Add(Image.FromStream(ms));
            }

            return images;
        }

        /// <summary>
        /// 指定のTIFFファイルをページごとに分割します。ページ数と名前リストの要素数は一致している必要があります。
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="saveFileNames"></param>
        /// <returns></returns>
        public static bool SplitTiff(string fileName, List<string> saveFileNames)
        {
            var tiffs = GetFrameStreams(fileName);
            if (tiffs.Count != saveFileNames.Count) return false;

            try
            {
                for (var i = 0; i < tiffs.Count; i++)
                {
                    using (var fs = new FileStream(saveFileNames[i], FileMode.Create))
                    {
                        fs.Write(tiffs[i].ToArray(), 0, tiffs[i].Count());
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool MargeTiff(IEnumerable<string> fileNames, string saveFileName)
        {
            var ifds = new List<TiffUtility>();
            byte[] bs;

            try
            {
                foreach (var item in fileNames)
                {
                    using (var fs = new FileStream(item, FileMode.Open))
                    {
                        bs = new byte[fs.Length];
                        fs.Read(bs, 0, (int)fs.Length);
                        ifds.Add(TiffUtility.getIFDs(bs));
                    }
                }

                //新ファイル用byte配列
                var newFile = new List<byte>();
                var dataPointers = new List<int>();

                //ヘッダ
                newFile.AddRange(new byte[] { 0x49, 0x49, 0x2A, 0x00 });

                for (int i = 0; i < ifds.Count; i++)
                {
                    //IFD情報ポインタ記録
                    int ifdAdd = newFile.Count + ifds[i].imageDataTotalLen + 4;
                    if (i != 0) ifdAdd += ifds[i - 1].optionData.Length;
                    newFile.AddRange(BitConverter.GetBytes(ifdAdd));

                    //前IFDに関係するオプション情報記録
                    if (i != 0) newFile.AddRange(ifds[i - 1].optionData);

                    //画像データの記録
                    foreach (var item in ifds[i].imageData)
                    {
                        dataPointers.Add(newFile.Count);
                        newFile.AddRange(item);
                    }

                    //記録用IFD情報作成＆記録
                    ifds[i].setDataPointers(dataPointers);
                    var ifd = ifds[i].createNewIFD(newFile.Count);
                    newFile.AddRange(ifd);

                    dataPointers.Clear();
                }

                //終了コード
                newFile.AddRange(new byte[] { 0x00, 0x00, 0x00, 0x00 });
                newFile.AddRange(ifds[ifds.Count - 1].optionData);

                using (var fs = new FileStream(saveFileName, FileMode.Create))
                {
                    fs.Write(newFile.ToArray(), 0, newFile.Count);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// jpg圧縮によるtiffファイルの作成
        /// </summary>
        /// <param name="img"></param>
        /// <param name="fileName"></param>
        /// <param name="quality">1-100のjpg圧縮度</param>
        /// <returns></returns>
        public static bool CreateTiffWrappedJpeg(Image img, string fileName, int quality)
        {
            //JPGの品質を指定
            var eps = new EncoderParameters(1);
            var ep = new EncoderParameter(Encoder.Quality, quality);
            eps.Param[0] = ep;

            //イメージエンコーダに関する情報を取得する
            var ici = ImageCodecInfo.GetImageEncoders().First(x => x.MimeType == "image/jpeg");

            var ms = new MemoryStream();
            img.Save(ms, ici, eps);

            var bs = ms.ToArray();
            var inchOrCm = bs[13];
            var xdpi = new[] { bs[15], bs[14] };
            var ydpi = new[] { bs[17], bs[16] };

            //新ファイル用byte配列
            ms.Position = 0;
            var byteImage = ms.ToArray();
            var newFile = new List<byte>();
            var dataPointers = new List<int>();

            int valueAdd = 10 + 12 * 19 + 4;

            //ヘッダ 10バイト
            newFile.AddRange(new byte[] { 0x49, 0x49, 0x2A, 0x00, 0x08, 0x00, 0x00, 0x00 });
            newFile.AddRange(new byte[] { 0x13, 0x00 });    //IFD 13個

            //IFD 12バイト x 19
            newFile.AddRange(new byte[] { 0xFE, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(0));
            newFile.AddRange(new byte[] { 0x00, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(img.Width));
            newFile.AddRange(new byte[] { 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(img.Height));
            newFile.AddRange(new byte[] { 0x02, 0x01, 0x03, 0x00, 0x03, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(valueAdd)); //アドレス
            newFile.AddRange(new byte[] { 0x03, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(new byte[] { 0x06, 0x00, 0x00, 0x00 });    //Jpegであることを表す
            newFile.AddRange(new byte[] { 0x06, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(new byte[] { 0x06, 0x00, 0x00, 0x00 });    //カラー 6でYCbCr ？
            newFile.AddRange(new byte[] { 0x11, 0x01, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(valueAdd + 22));    //jpegデータ開始アドレス
            newFile.AddRange(new byte[] { 0x15, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(new byte[] { 0x03, 0x00, 0x00, 0x00 });    //24ビットカラー
            newFile.AddRange(new byte[] { 0x16, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(img.Height));    //データ行数≒Height
            newFile.AddRange(new byte[] { 0x17, 0x01, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(byteImage.Length));    //データバイト数
            newFile.AddRange(new byte[] { 0x1A, 0x01, 0x05, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(valueAdd + 6));    //横解像度データ開始アドレス
            newFile.AddRange(new byte[] { 0x1B, 0x01, 0x05, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(valueAdd + 14));    //縦解像度データ開始アドレス
            newFile.AddRange(new byte[] { 0x28, 0x01, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(inchOrCm == 2 ? 1 : inchOrCm == 1 ? 2 : 0));     //inch or cm
            newFile.AddRange(new byte[] { 0x00, 0x02, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(1));    //jpegデータ開始アドレス
            newFile.AddRange(new byte[] { 0x01, 0x02, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(valueAdd + 22));    //jpegデータ開始アドレス
            newFile.AddRange(new byte[] { 0x02, 0x02, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(byteImage.Length));    //jpegデータ開始アドレス
            newFile.AddRange(new byte[] { 0x03, 0x02, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(0));    //jpegデータ開始アドレス
            newFile.AddRange(new byte[] { 0x12, 0x02, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00 });
            newFile.AddRange(new byte[] { 0x02, 0x00, 0x02, 0x00 });
            newFile.AddRange(new byte[] { 0x13, 0x02, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(BitConverter.GetBytes(1));

            //IFD終了コード 4バイト
            newFile.AddRange(new byte[] { 0x00, 0x00, 0x00, 0x00 });

            //色深度 6バイト
            newFile.AddRange(new byte[] { 0x08, 0x00, 0x08, 0x00, 0x08, 0x00 });

            //解像度 16バイト
            newFile.AddRange(xdpi);
            newFile.AddRange(new byte[] { 0x00, 0x00, 0x01, 0x00, 0x00, 0x00 });
            newFile.AddRange(ydpi);
            newFile.AddRange(new byte[] { 0x00, 0x00, 0x01, 0x00, 0x00, 0x00 });

            //画像データの記録
            newFile.AddRange(byteImage);

            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create))
                {
                    fs.Write(newFile.ToArray(), 0, newFile.Count);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }


        /// <summary>
        /// Tiff画像に透かしを挿入します
        /// </summary>
        /// <param name="strInputPath">元ファイルフルパス</param>
        /// <param name="strOutputPath">保存先フルパス(省略した場合元ファイルに上書き)</param>
        /// <param name="marktext">挿入文字(省略した場合「写」)</param>
        /// <param name="verticalPos">高さ位置%(省略した場合5)</param>
        /// <param name="horizontalPos">左端からの横位置%(省略した場合90)</param>
        /// <returns></returns>
        public static bool AddWaterMarkToTif(string strInputPath, string strOutputPath = null, string marktext = "写", int verticalPos = 5, int horizontalPos = 90)
        {
            if (strInputPath == string.Empty) return true;

            string strNewFileName = strOutputPath == null ? strInputPath : strOutputPath;
            string watermarkText = marktext;

            EncoderParameters ep = null;
            ImageCodecInfo ici = GetEncoderInfo("image/tiff");
            if (ici == null) return false;

            //マルチページTiffのページ数を取得
            using (Image img = Image.FromFile(strInputPath))
            using (Brush brush = new SolidBrush(Color.Black))
            using (Font font = new Font("Arial", 130.0f, FontStyle.Bold, GraphicsUnit.Pixel))
            {
                FrameDimension fd = new FrameDimension(img.FrameDimensionsList[0]);
                int pageCount = img.GetFrameCount(fd);

                ////透かしの設定
                //Brush brush = new SolidBrush(Color.Black);
                //Font font = new Font("Arial", 130.0f, FontStyle.Bold, GraphicsUnit.Pixel);

                //1ページ目を取得
                img.SelectActiveFrame(fd, 0);
                //Bitmap tiffImage = new Bitmap(img);
                using (Bitmap tiffImage = new Bitmap(img))
                {
                    Graphics g = Graphics.FromImage(tiffImage);
                    g.DrawImage(tiffImage, 0, 0, tiffImage.Width, tiffImage.Height);
                    g.DrawString(watermarkText, font, brush, float.Parse((tiffImage.Width * horizontalPos / 100).ToString()), float.Parse((tiffImage.Width * verticalPos / 100).ToString()));

                    //シングルページの場合
                    if (pageCount == 1)
                    {
                        //マルチTIFFではなく、1枚だけ保存する
                        ep = new EncoderParameters(1);
                        ep.Param[0] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                        tiffImage.Save(strNewFileName, ici, ep);
                        ep.Dispose();


                        brush.Dispose();
                        font.Dispose();
                        img.Dispose();
                        g.Dispose();
                        tiffImage.Dispose();

                        return true;
                    }

                    //マルチページの場合
                    for (int i = 0; i < pageCount; i++)
                    {
                        if (i == 0)
                        {
                            //はじめのフレームはMultiFrameで保存する
                            ep = new EncoderParameters(2);
                            ep.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
                            ep.Param[1] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);

                            tiffImage.Save(strNewFileName, ici, ep);
                            ep.Dispose();
                        }
                        else
                        {
                            //2ページ目以降を取得
                            img.SelectActiveFrame(fd, i);
                            Image newPage = new Bitmap(img);
                            g = Graphics.FromImage(newPage);
                            g.DrawString(watermarkText, font, brush, float.Parse((newPage.Width * horizontalPos / 100).ToString()), float.Parse((newPage.Width * verticalPos / 100).ToString()));

                            //2枚目からはFrameDimensionPageで追加する
                            ep = new EncoderParameters(2);
                            ep.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
                            ep.Param[1] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);

                            tiffImage.SaveAdd(newPage, ep);
                            ep.Dispose();
                            newPage.Dispose();
                        }

                        if (i == pageCount - 1)
                        {
                            //最後にFlushで閉じる
                            ep = new EncoderParameters(1);
                            ep.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.Flush);

                            tiffImage.SaveAdd(ep);
                            ep.Dispose();
                        }
                    }

                    //解放しておかないとMejor起動中ずっと画像を握ってしまう
                    brush.Dispose();
                    font.Dispose();
                    img.Dispose();
                    g.Dispose();
                    tiffImage.Dispose();

                    return true;
                }
            }    
        }

        private static System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            //GDI+ に組み込まれたイメージ エンコーダに関する情報をすべて取得
            System.Drawing.Imaging.ImageCodecInfo[] encs = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            //指定されたMimeTypeを探して見つかれば返す
            foreach (System.Drawing.Imaging.ImageCodecInfo enc in encs)
            {
                if (enc.MimeType == mimeType)
                {
                    return enc;
                }
            }
            return null;
        }




        public class FastCopy
        {
            //10メガバイト確保
            byte[] bs = new byte[10000000];
            int len = 0;

            public bool FileCopy(string sourceFileName, string destFileName)
            {
                try
                {
                    using (var rs = new FileStream(sourceFileName, FileMode.Open))
                    {
                        len = (int)rs.Length;
                        if (len > 10000000)
                        {
                            //10メガキャッシュ以上の場合、遅いがそのままコピー
                            File.Copy(sourceFileName, destFileName);
                            return true;
                        }
                        rs.Read(bs, 0, len);
                    }

                    using (var ws = new FileStream(destFileName, FileMode.Create))
                    {
                        ws.Write(bs, 0, len);
                    }                    
                }
                catch
                {
                    return false;
                }
                return true;
            }

            public bool DirCopy(string copyDir, string sendDir)
            {
                var fs = Directory.GetFiles(copyDir);
                int len = 0;

                try
                {
                    foreach (var item in fs)
                    {
                        var fileName = sendDir + "\\" + Path.GetFileName(item);
                        using (var rs = new FileStream(item, FileMode.Open))
                        {
                            len = (int)rs.Length;
                            if (len > 1000000)
                            {
                                //1メガキャッシュ以上の場合、遅いがそのままコピー
                                File.Copy(item, fileName);
                                continue;
                            }

                            rs.Read(bs, 0, len);
                        }

                        using (var ws = new FileStream(fileName, FileMode.Create))
                        {
                            ws.Write(bs, 0, len);
                        }
                    }
                }
                catch
                {
                    return false;
                }
                return true;
            }
        }
    }
}
