﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace Mejor.Shinagawaku
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        public string Numbering { get; set; }
        public string ReceKey { get; set; }
        public int ChargeYM { get; set; }
        [DB.DbAttribute.PrimaryKey]
        public int MediYM { get; set; }
        public string HihoNum { get; set; }
        public int Sex { get; set; }
        public DateTime Birth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int Days { get; set; }
        public int Total { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int Ratio { get; set; }
        public NEW_CONT NewContType { get; set; }
        public string DrNum { get; set; }
        public string DrName { get; set; }

        //20210520095243 furukawa st ////////////////////////
        //帳票イメージ番号、宛名番号追加
        
        public string imagenum { get; set; } = string.Empty;     //帳票イメージ番号;
        public string atenanum { get; set; } = string.Empty;     //宛名番号;
        //20210520095243 furukawa ed ////////////////////////


        static int cym = 0;

        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool Import(int _cym)
        {
            cym = _cym;

            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "Mejor紐づけ用データ|Mejor紐づけ用データ.xlsx;Mejor紐づけ用データ.xls";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                var l = new List<RefRece>();
                wf.LogPrint("Excelファイルを読み込んでいます");

                try
                {
                    using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                    {
                        //国保、退職、それぞれのシートから読み込み
                        var ex = System.IO.Path.GetExtension(fileName);

                        if (ex == ".xls")
                        {
                            var xls = new HSSFWorkbook(fs);

                            var sheet = xls.GetSheet("国保");
                            l.AddRange(sheetToRefReces(sheet));

                            sheet = xls.GetSheet("退職");
                            l.AddRange(sheetToRefReces(sheet));
                        }
                        else if (ex == ".xlsx")
                        {
                            var xlsx = new XSSFWorkbook(fs);

                            var sheet = xlsx.GetSheet("国保");
                            l.AddRange(sheetToRefReces(sheet));

                            sheet = xlsx.GetSheet("退職");
                            l.AddRange(sheetToRefReces(sheet));
                        }
                        else
                        {
                            throw new Exception("Excelファイルの形式が拡張子より判別できませんでした");
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"{ex.Message}\r\n場所：{ System.Reflection.MethodBase.GetCurrentMethod().Name}");
                    return false;
                }

                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");


                
                using (var tran = DB.Main.CreateTransaction())
                {
                    wf.LogPrint($"{cym}削除");

                    //20210520102916 furukawa st ////////////////////////
                    //トランザクションする

                    DB.Command cmd = new DB.Command($"delete from refrece where chargeym={cym}",tran);
                    //DB.Command cmd = new DB.Command(DB.Main, $"delete from refrece where chargeym={cym}");
                    //20210520102916 furukawa ed ////////////////////////


                    cmd.TryExecuteNonQuery();

                    wf.LogPrint($"{cym}登録開始");
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                        if (!DB.Main.Insert(item,tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
            }

            MessageBox.Show("インポートが終了しました");
            return true;

        }


        public static List<RefRece> lstRefrece = new List<RefRece>();

        private static List<RefRece> sheetToRefReces(ISheet sheet)
        {
            
            lstRefrece.Clear();
            
            var rowCount = sheet.LastRowNum+1;// + 2;

            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    var row = sheet.GetRow(i);
                    if (row.GetCell(1).CellType != CellType.Numeric) continue;
                    var rr = new RefRece();
                    rr.Numbering = row.GetCell(7).StringCellValue;
                    rr.ReceKey = row.GetCell(6).StringCellValue;

                    int.TryParse(row.GetCell(5).StringCellValue, out int cym);
                    rr.ChargeYM = DateTimeEx.GetAdYearMonthFromJyymm(cym);

                    rr.MediYM = DateTimeEx.GetAdYearMonthFromJyymm((int)row.GetCell(8).NumericCellValue);

                    var mark = Utility.ToHalfWithoutKatakana(row.GetCell(9).StringCellValue);
                    var number = Utility.ToHalfWithoutKatakana(row.GetCell(10).StringCellValue);
                    rr.HihoNum = (mark + "・" + number);

                    rr.Sex = row.GetCell(11).StringCellValue == "2" ? 2 : 1;
                    rr.Birth = DateTimeEx.GetDateFromJInt7((int)row.GetCell(12).NumericCellValue);
                    rr.StartDate = DateTimeEx.GetDateFromJInt7((int)row.GetCell(14).NumericCellValue);
                    rr.FinishDate = DateTimeEx.GetDateFromJInt7((int)row.GetCell(15).NumericCellValue);
                    rr.Days = (int)row.GetCell(16).NumericCellValue;
                    rr.Partial = (int)row.GetCell(18).NumericCellValue;
                    rr.Ratio = (int)row.GetCell(19).NumericCellValue / 10;
                    rr.Charge = (int)row.GetCell(20).NumericCellValue;
                    rr.Total = (int)row.GetCell(21).NumericCellValue;
                    rr.NewContType = row.GetCell(22).StringCellValue == "1" ? NEW_CONT.新規 : NEW_CONT.継続;
                    rr.DrNum = row.GetCell(23).StringCellValue;
                    rr.DrName = row.GetCell(24).StringCellValue;

                    //20210520095328 furukawa st ////////////////////////
                    //帳票イメージ番号、宛名番号追加
                    
                    rr.imagenum= row.GetCell(7).StringCellValue.Trim();
                    rr.atenanum= row.GetCell(13).StringCellValue.Trim();
                    //20210520095328 furukawa ed ////////////////////////


                    lstRefrece.Add(rr);
                }
                catch
                {
                    continue;
                }
            }
            return lstRefrece;
        }



        /// <summary>
        /// メホール請求年月のrefrece一覧取得
        /// </summary>
        /// <param name="_cym"></param>
        public static void getRefreceList(int _cym)
        {
            DB.Command cmd = DB.Main.CreateCmd($"select * from refrece where chargeym={_cym}");
            var lst = cmd.TryExecuteReaderList();
            foreach(var v in lst)
            {
                RefRece r = new RefRece();
                r.Numbering = v[0].ToString();
                r.ReceKey = v[1].ToString();
                r.ChargeYM = int.Parse(v[2].ToString());
                r.MediYM = int.Parse(v[3].ToString());
                r.HihoNum = v[4].ToString();
                r.Sex = int.Parse(v[5].ToString());
                r.Birth = DateTime.Parse(v[6].ToString());
                r.StartDate = DateTime.Parse(v[7].ToString());
                r.FinishDate = DateTime.Parse(v[8].ToString());
                r.Days = int.Parse(v[9].ToString());
                r.Total = int.Parse(v[10].ToString());
                r.Partial = int.Parse(v[11].ToString());
                r.Charge = int.Parse(v[12].ToString());
                r.Ratio = int.Parse(v[13].ToString());
                r.NewContType = int.Parse(v[14].ToString()) == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
                r.DrNum = v[15].ToString();
                r.DrName = v[16].ToString();

                //20210520095358 furukawa st ////////////////////////
                //帳票イメージ番号、宛名番号追加
                
                r.imagenum = v[17].ToString();
                r.atenanum = v[18].ToString();
                //20210520095358 furukawa ed ////////////////////////
                lstRefrece.Add(r);
            }

            //20210428100932 furukawa st ////////////////////////
            //コマンド解放しないとコマンドPoolがいっぱいになる            
            cmd.Dispose();
            //20210428100932 furukawa ed ////////////////////////
        }


        /// <summary>
        /// ファイル名とnumberingを突合してrefrece取得
        /// </summary>
        /// <param name="strFilename"></param>
        /// <returns></returns>
        public static RefRece Select(string strFilename)
        {
            foreach(RefRece r in lstRefrece)
            {
                if (r.Numbering == strFilename) return r;
            }
            return null;   
        }



        //不要
        //public static RefRece Select(string numbering)
        //{
        //    return DB.Main.Select<RefRece>(new { numbering = numbering }).SingleOrDefault();
        //}






        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from refrece where chargeym={_cym} group by chargeym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;
            
            //20210428101111 furukawa st ////////////////////////
            //コマンド解放しないとコマンドPoolがいっぱいになる
            
            cmd.Dispose();
            //20210428101111 furukawa ed ////////////////////////

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select chargeym,count(*) from refrece group by chargeym order by chargeym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("chargeym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
