﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Mejor
{
    public partial class PrinterSelectForm : Form
    {
        public string SelectPrinterName;

        public PrinterSelectForm(string nowSelectPrinterName)
        {
            InitializeComponent();

            SelectPrinterName = nowSelectPrinterName;
            string pName;
            using (var pd = new PrintDocument())
            {
                pName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == pName)
                    comboBox1.SelectedIndex = i;
            }

            if (comboBox1.Items.Contains(nowSelectPrinterName))
            {
                comboBox1.SelectedItem = nowSelectPrinterName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SelectPrinterName = comboBox1.SelectedItem.ToString();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
