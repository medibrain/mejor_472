﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Mejor
{
    public partial class InputLogForm : Form
    {
        BindingSource bs = new BindingSource();
        DataGridViewCellStyle spanStyle = new DataGridViewCellStyle();

        public InputLogForm()
        {
            InitializeComponent();
            var t = DateTime.Today;
            dateBoxS.SetDate(new DateTime(t.Year, t.Month, 1));
            dateBoxE.SetDate(new DateTime(t.Year, t.Month, 1).AddMonths(1).AddDays(-1));
            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridView1.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;

            typeof(DataGridView).
                GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic).
                SetValue(dataGridView1, true, null);

            var l = new List<InputLog.AggregateLog>();
            bs.DataSource = l;
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(InputLog.AggregateLog.UID)].Visible = false;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InsurerID)].Visible = false;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InsurerType)].Visible = false;

            dataGridView1.Columns[nameof(InputLog.AggregateLog.UserName)].HeaderText = "ユーザー名";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputDt)].HeaderText = "日時";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InsurerName)].HeaderText = "保険者名";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalCount)].HeaderText = "入力計";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputCount)].HeaderText = "入力計 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifyCount)].HeaderText = "入力計 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputCount)].HeaderText = "柔整 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifyCount)].HeaderText = "柔整 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputCount)].HeaderText = "あんま 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifyCount)].HeaderText = "あんま 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputCount)].HeaderText = "鍼灸 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifyCount)].HeaderText = "鍼灸 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputCount)].HeaderText = "その他 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifyCount)].HeaderText = "その他 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalMiss)].HeaderText = "ミス計";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputMiss)].HeaderText = "ミス計 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifyMiss)].HeaderText = "ミス計 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputMiss)].HeaderText = "ミス柔 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifyMiss)].HeaderText = "ミス柔 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputMiss)].HeaderText = "ミスあ 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifyMiss)].HeaderText = "ミスあ 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputMiss)].HeaderText = "ミスは 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifyMiss)].HeaderText = "ミスは 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputMiss)].HeaderText = "ミス他 1";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifyMiss)].HeaderText = "ミス他 2";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifySpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifySpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifySpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifySpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputSpan)].HeaderText = "時間";
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifySpan)].HeaderText = "時間";


            dataGridView1.Columns[nameof(InputLog.AggregateLog.UserName)].Width = 100;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputDt)].Width = 120;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InsurerName)].Width = 170;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifyCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifySpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifyCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifySpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifyCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifySpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifyCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifySpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputSpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifyCount)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifySpan)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.TotalMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.InputMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.VerifyMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuInputMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.JyuVerifyMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaInputMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.AnmaVerifyMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuInputMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.HariKyuVerifyMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherInputMiss)].Width = 60;
            dataGridView1.Columns[nameof(InputLog.AggregateLog.OtherVerifyMiss)].Width = 60;

            //フィルター用
            comboBox1.DataSource = Enum.GetValues(typeof(INSURER_TYPE));
            comboBox1.SelectedIndex = 0;

            var insurers = new List<Insurer>();
            var ins = new Insurer();
            ins.InsurerID = 0;
            ins.InsurerName = "指定なし";
            insurers.Add(ins);
            insurers.AddRange(Insurer.GetInsurers());
            comboBoxInsurer.ValueMember = nameof(Insurer.InsurerID);
            comboBoxInsurer.DisplayMember = nameof(Insurer.InsurerName);
            comboBoxInsurer.DataSource = insurers;

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            var logMerge = new Action<InputLog.AggregateLog, InputLog.AggregateLog>((x, c) =>
                 {
                     x.JyuInputCount += c.JyuInputCount;
                     x.JyuVerifyCount += c.JyuVerifyCount;
                     x.AnmaInputCount += c.AnmaInputCount;
                     x.AnmaVerifyCount += c.AnmaVerifyCount;
                     x.HariKyuInputCount += c.HariKyuInputCount;
                     x.HariKyuVerifyCount += c.HariKyuVerifyCount;
                     x.OtherInputCount += c.OtherInputCount;
                     x.OtherVerifyCount += c.OtherVerifyCount;

                     x.JyuInputMiss += c.JyuInputMiss;
                     x.JyuVerifyMiss += c.JyuVerifyMiss;
                     x.AnmaInputMiss += c.AnmaInputMiss;
                     x.AnmaVerifyMiss += c.AnmaVerifyMiss;
                     x.HariKyuInputMiss += c.HariKyuInputMiss;
                     x.HariKyuVerifyMiss += c.HariKyuVerifyMiss;
                     x.OtherInputMiss += c.OtherInputMiss;
                     x.OtherVerifyMiss += c.OtherVerifyMiss;

                     x.JyuInputDateTime.AddRange(c.JyuInputDateTime);
                     x.JyuVerifyDateTime.AddRange(c.JyuVerifyDateTime);
                     x.AnmaInputDateTime.AddRange(c.AnmaInputDateTime);
                     x.AnmaVerifyDateTime.AddRange(c.AnmaVerifyDateTime);
                     x.HariKyuInputDateTime.AddRange(c.HariKyuInputDateTime);
                     x.HariKyuVerifyDateTime.AddRange(c.HariKyuVerifyDateTime);
                     x.OtherInputDateTime.AddRange(c.OtherInputDateTime);
                     x.OtherVerifyDateTime.AddRange(c.OtherVerifyDateTime);
                 });

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("入力情報を取得中です");
                var sdt = dateBoxS.GetDate();
                var edt = dateBoxE.GetDate();

                if (sdt.IsNullDate() || edt.IsNullDate()) return;
                var l = InputLog.GetLogWithSpan(sdt, edt);

                //フィルター
                if (comboBox1.SelectedIndex != 0)
                {
                    l = l.FindAll(x => x.InsurerType == (INSURER_TYPE)comboBox1.SelectedValue);
                }
                if (comboBoxInsurer.SelectedIndex != 0)
                {
                    l = l.FindAll(x => x.InsurerID == (int)comboBoxInsurer.SelectedValue);
                }

                //保険者集計なし
                if (!checkBox1.Checked)
                {
                    l.ForEach(x => x.InsurerID = null);
                }

                wf.LogPrint("集計中です");

                var v = new List<InputLog.AggregateLog>();
                //期間ごと集計
                if (radioButtonMonth.Checked)
                {
                    var g = l.GroupBy(x => new { x.InputDt.Value.Year, x.InputDt.Value.Month, x.InsurerID, x.UID });
                    foreach (var item in g)
                    {
                        //20210301111942 furukawa st ////////////////////////
                        //キャンセル処理追加                        
                        if (CommonTool.WaitFormCancelProcess(wf)) break;
                        //20210301111942 furukawa ed ////////////////////////

                        var f = item.First();
                        var x = new InputLog.AggregateLog();
                        x.UID = f.UID;
                        x.InputDt = new DateTime(f.InputDt.Value.Year, f.InputDt.Value.Month, 1);
                        x.InsurerID = f.InsurerID;

                        foreach (var c in item)
                        {
                            logMerge(x, c);
                        }
                        v.Add(x);
                    }
                }
                else if (radioButtonDay.Checked)
                {
                    var g = l.GroupBy(x => new { x.InputDt.Value.Date, x.InsurerID, x.UID });
                    foreach (var item in g)
                    {
                        //20210301111942 furukawa st ////////////////////////
                        //キャンセル処理追加                        
                        if (CommonTool.WaitFormCancelProcess(wf)) break;
                        //20210301111942 furukawa ed ////////////////////////

                        var f = item.First();
                        var x = new InputLog.AggregateLog();
                        x.UID = f.UID;
                        x.InputDt = f.InputDt.Value.Date;
                        x.InsurerID = f.InsurerID;

                        foreach (var c in item)
                        {
                            logMerge(x, c);
                        }
                        v.Add(x);
                    }
                }
                else if (radioButtonAll.Checked)
                {
                    var g = l.GroupBy(x => new { x.InsurerID, x.UID });
                    foreach (var item in g)
                    {

                        //20210301111942 furukawa st ////////////////////////
                        //キャンセル処理追加                        
                        if (CommonTool.WaitFormCancelProcess(wf)) break;
                        //20210301111942 furukawa ed ////////////////////////


                        var f = item.First();
                        var x = new InputLog.AggregateLog();
                        x.UID = f.UID;
                        x.InputDt = null;
                        x.InsurerID = f.InsurerID;

                        foreach (var c in item)
                        {
                            logMerge(x, c);
                        }
                        v.Add(x);
                    }
                }
                else
                {
                    v = l;
                }

                bs.DataSource = v;
                bs.ResetBindings(false);
            }
        }

        private void buttonLastMonth_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today;
            dateBoxS.SetDate(new DateTime(t.Year, t.Month, 1).AddMonths(-1));
            dateBoxE.SetDate(new DateTime(t.Year, t.Month, 1).AddDays(-1));
        }

        private void buttonTodayMonth_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today;
            dateBoxS.SetDate(new DateTime(t.Year, t.Month, 1));
            dateBoxE.SetDate(new DateTime(t.Year, t.Month, 1).AddMonths(1).AddDays(-1));
        }

        private void buttonLastWeek_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today;
            int w = t.DayOfWeek == DayOfWeek.Sunday ? 6 : (int)t.DayOfWeek - 1;
            t = t.AddDays(-w - 7);
            dateBoxS.SetDate(t);
            dateBoxE.SetDate(t.AddDays(6));
        }

        private void buttonTodayWeek_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today;
            int w = t.DayOfWeek == DayOfWeek.Sunday ? 6 : (int)t.DayOfWeek - 1;
            t = t.AddDays(-w);
            dateBoxS.SetDate(t);
            dateBoxE.SetDate(t.AddDays(6));
        }

        private void buttonLastDay_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today.AddDays(-1);
            dateBoxS.SetDate(t);
            dateBoxE.SetDate(t);
        }

        private void buttonToday_Click(object sender, EventArgs e)
        {
            var t = DateTime.Today;
            dateBoxS.SetDate(t);
            dateBoxE.SetDate(t);
        }

        string sortColumnName = string.Empty;
        bool sortASC = true;
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var v = dataGridView1;
            var ds = v.DataSource;
            if (ds == null || !(ds is BindingSource bs)) return;
            if (e.ColumnIndex < 0) return;

            var name = v.Columns[e.ColumnIndex].Name;
            if (name == sortColumnName) sortASC = !sortASC;
            else sortASC = true;
            sortColumnName = name;

            var ps = typeof(InputLog.AggregateLog).GetProperties();
            var p = Array.Find(ps, pp => pp.Name == name);
            if (p == null) return;

            ((List<InputLog.AggregateLog>)(bs.DataSource)).Sort((x, y) =>
            {
                var xp = p.GetValue(x, null);
                var yp = p.GetValue(y, null);

                //nullを最小に
                if (xp == null && yp == null) return 0;
                if (xp == null) return sortASC ? -1 : 1;
                if (yp == null) return sortASC ? 1 : -1;

                int compareRes = 0;
                if (p.PropertyType == typeof(int?)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(int)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(DateTime)) compareRes = ((DateTime)xp).CompareTo(((DateTime)yp));
                else if (p.PropertyType == typeof(string)) compareRes = ((string)xp).CompareTo(((string)yp));
                else compareRes = (xp.ToString()).CompareTo(yp.ToString());

                return sortASC ? compareRes : -compareRes;
            });

            bs.ResetBindings(false);
        }

        private void buttonCSV_Click(object sender, EventArgs e)
        {
            var fn = "入力ログ集計.csv";
            using (var f = new SaveFileDialog())
            {
                f.FileName = fn;
                if (f.ShowDialog() != DialogResult.OK) return;
                fn = f.FileName;
            }

            Utility.ViewToCsv(dataGridView1, fn);
        }
    }
}
