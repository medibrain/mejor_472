﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZXing;
using System.Drawing;

namespace Mejor
{
    class BarDecode
    {

        public static string Decode(Bitmap bmp, BarcodeFormat codeFormat)
        {
            var reader = new BarcodeReader();
            reader.AutoRotate = true;
            reader.TryInverted = true;
            reader.Options.PossibleFormats = new BarcodeFormat[] { codeFormat };
            reader.Options.TryHarder = true;

            try
            {
                var result = reader.Decode(bmp);
                return result?.Text ?? string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
