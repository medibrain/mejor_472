﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;
using System.Drawing.Imaging;

namespace Mejor.Sapporoshi
{
    public partial class FormOCRCheckJyu2 : Form
    {
        private Dictionary<int, App> dic;
        private BindingSource bs = new BindingSource();

        private ScanGroup sg;
        int currentAid = -1;
        private bool firstTime;

        private int cYear;
        private int cMonth;


        // --------------------------------------------------------------------------------
        //
        // クラス定義部
        //
        // --------------------------------------------------------------------------------

        public enum a2tp { 未チェック = 0, First = 1, Other = 2 };

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point headerPos = new Point(0, 50);
        Point iryoPos = new Point(0, 150);
        Point costPos = new Point(0, 500);
        Point shomeiPos = new Point(0, 800);
        Point drNumPos = new Point(0, 1000);

        Control[] headerControls, iryoControls, costControls, shomeiControls;

        // --------------------------------------------------------------------------------
        //
        // メイン部
        //
        // --------------------------------------------------------------------------------
        public FormOCRCheckJyu2(ScanGroup sGroup, bool firstTime)
        {
            InitializeComponent();

            headerControls = new Control[] { textBoxY, textBoxY2, 
                textBoxM, textBoxM2, textBoxHnum, textBoxHnum2, textBoxRatio, textBoxRatio2,
                textBoxBirthday, textBoxBirthday2, textBoxBY, textBoxBY2, textBoxBM, 
                textBoxBM2, textBoxBD, textBoxBD2, textBoxSex, textBoxSex2, textBoxName, 
                textBoxName2,};

            iryoControls = new Control[] { textBoxDays, textBox1Fusyo, textBox1Fusyo2, };

            costControls = new Control[] {textBoxTotal, textBoxTotal2, textBoxCharge, 
                textBoxCharge2, textBoxShinryoDays, textBoxShinryoDays2, };

            shomeiControls = new Control[] { textBoxHosName, textBoxHosName2, textBoxDrName,
                textBoxDrName2, };


            panelReceipt.Visible = false;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += textBoxs_Enter;
                        item.Leave += textBoxs_Leave;
                    }
                    func(item);
                }
            });
            func(panelControls);

            this.sg = sGroup;
            cYear = sg.cyear;
            cMonth = sg.cmonth;
            this.firstTime = firstTime;

            //GIDで検索
            int gid = sg.GroupID;
            var list = App.GetAppsGID(gid);

            //ベリファイ入力時は申請書以外を最初から除外
            if (!firstTime) list = list.FindAll(a => a.MediYear > 0);

            dic = new Dictionary<int, App>();
            foreach (var item in list) dic.Add(item.Aid, item);
            bs.DataSource = list;
            dataGridViewPlist.DataSource = bs;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = false;

            labelSeikyu.Text = $"処理月: {sg.cyear}/{sg.cmonth}";
            labelNote1.Text = "Note: " + sg.note1.ToString();
            labelScanID.Text = "ScanID: " + sg.ScanID.ToString();
            labelGroupID.Text = "GroupID: " + sg.GroupID.ToString();

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void regist()
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (!appDBupdate(ri)) return;

            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri + 1];
        }

        //登録ボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        //キー入力監視
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) regist();
            if (e.KeyCode == Keys.PageDown) showPreview();
        }


        //ひとつ前の申請書に戻る
        private void showPreview()
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri <= 0) return;
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri - 1];
        }

        //EnterキーでTABキーの動作をさせる
        private void FormOCRCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        // --------------------------------------------------------------------------------
        //
        // 入力チェック部(文字種チェック→値チェック→一回目のみOCR結果との比較)
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowindex"></param>
        /// <returns></returns>
        private bool checkApp(ref App app, out AppEx appex)
        {
            bool err = false;
            appex = new AppEx(app.Aid);

            var check = new Action<TextBox, bool>((tb, error) =>
                {
                    if (error)
                    {
                        tb.BackColor = Color.Pink;
                        err = true;
                    }
                    else
                    {
                        if (!tb.Enabled) return;
                        tb.BackColor = SystemColors.Info;
                    }
                });

            //年 種別
            int year;
            if (!int.TryParse(textBoxY.Text, out year)) check(textBoxY, true);
            else if (textBoxY.Text.Length != 2 || year < app.ChargeYear - 5) check(textBoxY, true);
            else check(textBoxY, false);

            //月
            int month;
            if (!int.TryParse(textBoxM.Text, out month)) check(textBoxM, true);
            else if (month < 0 || month > 12) check(textBoxM, true);
            else check(textBoxM, false);

            //被保険者番号のチェック
            int hnumTemp;
            if (textBoxHnum.Text.Trim().Length != 7) check(textBoxHnum, true);
            else if (!int.TryParse(textBoxHnum.Text.Trim(), out hnumTemp)) check(textBoxHnum, true);
            else check(textBoxHnum, false);
            var hnum = textBoxHnum.Text.Trim();

            //給付割合
            int ratio = 0;
            if (textBoxRatio.Text.Trim().Length != 0 && !int.TryParse(textBoxRatio.Text, out ratio)) check(textBoxRatio, true);
            else if (ratio != 0 && ratio != 7 && ratio != 8 && ratio != 9) check(textBoxRatio, true);
            else check(textBoxRatio, false);

            //生年月日
            var birth = checkDate(textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, true);

            //性別
            int sex;
            if (!int.TryParse(textBoxSex.Text, out sex)) check(textBoxSex, true);
            else if (sex < 1 || sex > 2) check(textBoxSex, true);
            else check(textBoxSex, false);

            //療養者氏名
            check(textBoxName, textBoxName.Text.Length < 3);

            //診療日数
            int days;
            if (!int.TryParse(textBoxDays.Text, out days)) check(textBoxDays, true);
            else if (days < 1 || days > 31) check(textBoxDays, true);
            else check(textBoxDays, false);

            //負傷名 1のみチェック
            var f1 = textBox1Fusyo.Text.Trim();
            var f2 = textBox2Fusyo.Text.Trim();
            var f3 = textBox3Fusyo.Text.Trim();
            var f4 = textBox4Fusyo.Text.Trim();
            var f5 = textBox5Fusyo.Text.Trim();
            check(textBox3Fusyo, f3.Length > 0 && f2.Length == 0);
            check(textBox4Fusyo, f4.Length > 0 && f3.Length == 0);
            check(textBox5Fusyo, f5.Length > 0 && f4.Length == 0);

            //初検日
            var f1day = f1.Length > 0 ? checkDate(null, textBox1FusyoY, textBox1FusyoM, textBox1FusyoD, true) : DateTime.MinValue;
            var f2day = f2.Length > 0 ? checkDate(null, textBox2FusyoY, textBox2FusyoM, textBox2FusyoD, true) : DateTime.MinValue;
            var f3day = f3.Length > 0 ? checkDate(null, textBox3FusyoY, textBox3FusyoM, textBox3FusyoD, true) : DateTime.MinValue;
            var f4day = f4.Length > 0 ? checkDate(null, textBox4FusyoY, textBox4FusyoM, textBox4FusyoD, true) : DateTime.MinValue;
            var f5day = f5.Length > 0 ? checkDate(null, textBox5FusyoY, textBox5FusyoM, textBox5FusyoD, true) : DateTime.MinValue;

            //日数
            int f1days, f2days, f3days, f4days, f5days;
            int.TryParse(textBox1FusyoDays.Text, out f1days);
            int.TryParse(textBox2FusyoDays.Text, out f2days);
            int.TryParse(textBox3FusyoDays.Text, out f3days);
            int.TryParse(textBox4FusyoDays.Text, out f4days);
            int.TryParse(textBox5FusyoDays.Text, out f5days);
            check(textBox1FusyoDays, f1.Length > 0 && (f1days < 1 || f1days > 31));
            check(textBox2FusyoDays, f2.Length > 0 && (f1days < 1 || f1days > 31));
            check(textBox3FusyoDays, f3.Length > 0 && (f1days < 1 || f1days > 31));
            check(textBox4FusyoDays, f4.Length > 0 && (f1days < 1 || f1days > 31));
            check(textBox5FusyoDays, f5.Length > 0 && (f1days < 1 || f1days > 31));

            //転帰
            int f1Ten, f2Ten, f3Ten, f4Ten, f5Ten;
            int.TryParse(textBox1FusyoTen.Text, out f1Ten);
            int.TryParse(textBox2FusyoTen.Text, out f2Ten);
            int.TryParse(textBox3FusyoTen.Text, out f3Ten);
            int.TryParse(textBox4FusyoTen.Text, out f4Ten);
            int.TryParse(textBox5FusyoTen.Text, out f5Ten);
            check(textBox1FusyoTen, f1.Length > 0 && textBox1FusyoTen.Text.Trim().Length > 0 && (f1Ten < 1 || f1Ten > 3));
            check(textBox2FusyoTen, f2.Length > 0 && textBox2FusyoTen.Text.Trim().Length > 0 && (f2Ten < 1 || f2Ten > 3));
            check(textBox3FusyoTen, f3.Length > 0 && textBox3FusyoTen.Text.Trim().Length > 0 && (f3Ten < 1 || f3Ten > 3));
            check(textBox4FusyoTen, f4.Length > 0 && textBox4FusyoTen.Text.Trim().Length > 0 && (f4Ten < 1 || f4Ten > 3));
            check(textBox5FusyoTen, f5.Length > 0 && textBox5FusyoTen.Text.Trim().Length > 0 && (f5Ten < 1 || f5Ten > 3));

            //診療日
            string sinryoDayList = string.Empty;
            if (string.IsNullOrWhiteSpace(textBoxShinryoDays.Text)) check(textBoxShinryoDays, true);
            var sdays = Utility.ToHalfWithoutKatakana(textBoxShinryoDays.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            bool sdayserror = false;
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;

                if (int.TryParse(item, out d))
                {
                    l.Add(item.Trim());
                }
                else
                {
                    check(textBoxShinryoDays, true);
                    sdayserror = true;
                }
            }
            if (sdayserror || l.Count == 0)
            {
                check(textBoxShinryoDays, true);
            }
            else
            {
                check(textBoxShinryoDays, false);
                sinryoDayList = string.Join(" ", l);
            }

            //合計金額
            int total;
            if (!int.TryParse(textBoxTotal.Text, out total)) check(textBoxTotal, true);
            else if (total < 100 || total > 300000) check(textBoxTotal, true);
            else check(textBoxTotal, false);

            //請求金額
            int charge = 0;
            if (textBoxCharge.Text.Trim().Length != 0 && !int.TryParse(textBoxCharge.Text, out charge)) check(textBoxCharge, true);
            else if (charge > total) check(textBoxCharge, true);
            else check(textBoxCharge, false);

            //負担金額
            int futan = 0;
            if (textBoxFutan.Text.Trim().Length != 0 && !int.TryParse(textBoxFutan.Text, out futan)) check(textBoxFutan, true);
            else if (charge + futan > total) check(textBoxFutan, true);
            else check(textBoxFutan, false);

            //機関コード
            check(textBoxDrCode, string.IsNullOrWhiteSpace(textBoxDrCode.Text.Trim()));

            //施術師名 スペースがあるか、スペースの後に文字があるかチェック
            int nameIndex = textBoxDrName.Text.IndexOf('　');
            check(textBoxDrName, nameIndex < 1 || nameIndex >= textBoxDrName.Text.Trim().Length);

            if (err)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            //金額でのエラーがあればいったん登録中断
            bool ratioError = (int)(total * ratio / 10) != charge;
            if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != charge;

            if (ratioError && charge != 0)
            {
                textBoxTotal.BackColor = Color.GreenYellow;
                textBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //施術所名がない場合もメッセージで確認
            if (firstTime && textBoxHosName.Text.Length < 3)
            {
                textBoxHosName.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("施術名が指定されていません。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }
            else check(textBoxHosName, false);


            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = hnum;
            app.Ratio = ratio;
            app.Sex = sex;
            app.Birthday = birth;
            app.PersonName = textBoxName.Text.Trim();
            app.CountedDays = days;
            app.FushoName1 = textBox1Fusyo.Text.Trim();
            app.FushoName2 = textBox2Fusyo.Text.Trim();
            app.FushoName3 = textBox3Fusyo.Text.Trim();
            app.FushoName4 = textBox4Fusyo.Text.Trim();
            app.FushoName5 = textBox5Fusyo.Text.Trim();
            app.FushoDate1 = f1day;
            app.FushoDate2 = f2day;
            app.FushoDate3 = f3day;
            app.FushoDate4 = f4day;
            app.FushoDate5 = f5day;
            app.FushoDays1 = f1days;
            app.FushoDays2 = f2days;
            app.FushoDays3 = f3days;
            app.FushoDays4 = f4days;
            app.FushoDays5 = f5days;
            app.FushoCourse1 = f1Ten;
            app.FushoCourse2 = f2Ten;
            app.FushoCourse3 = f3Ten;
            app.FushoCourse4 = f4Ten;
            app.FushoCourse5 = f5Ten;
            appex.SejyutuDayList = sinryoDayList;
            app.Total = total;
            app.Charge = charge;
            app.Partial = futan;
            app.DrNum = textBoxDrCode.Text.Trim();
            app.ClinicName = textBoxHosName.Text.Trim();
            app.DrName = textBoxDrName.Text.Trim();
            return true;
        }

        // --------------------------------------------------------------------------------
        //
        // Application操作部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 現在の行のApplicationを表示します
        /// </summary>
        private void currentRowAppRead()
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri < 0) return;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            //次のApplicationを表示
            displayDataReflesh(r);

            //textBoxYにフォーカスを移動
            focusBack(false);
        }

        /// <summary>
        /// Applicationの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(int rowIndex)
        {
            if (rowIndex == -1) return false;
            var aid = (int)dataGridViewPlist["Aid", rowIndex].Value;

            if (currentAid != aid)
            {
                MessageBox.Show("更新しようとしているデータと現在表示中のデータが一致しません。" +
                    "ご迷惑をおかけしますが、システムエラーを回避するため、この画面を閉じます。" +
                    "再度このグループを開いて作業を行ってください。", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return false;
            }

            App app = null;
            AppEx appEx = null;

            //ベリファイチェック時
            if (!firstTime)
            {
                app = dic[aid];
                if (app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

                if (!checkVerify())
                {
                    MessageBox.Show("データが一致していません。" +
                        "赤で示されたデータをご確認ください。登録できません。",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    focusBack(true);
                    return false;
                }
            }


            if (textBoxY.Text == "--")
            {
                //続紙の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
            }
            else if (textBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
            }
            else
            {
                //データチェック
                app = dic[aid];
                if (!checkApp(ref app, out appEx))
                {
                    //データベースから読み直し
                    dic[aid] = App.GetApp(aid);
                    focusBack(true);
                    return false;
                }

                //被保険者情報の管理
                var p = Person.GetPerson(app.HihoNum, app.Birthday);
                if (p == null)
                {
                    p = new Person(app);
                    p.Insert();
                }
                else if (firstTime)
                {
                    if (!p.IsEqual(app))
                    {
                        if (MessageBox.Show("過去に入力された被保険者データと一致しません。" +
                            "被保険者データを更新し、入力データを登録してもよろしいですか？",
                            "被保険者情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    p.Update(app, !firstTime);
                }

                //施術師情報の管理
                var d = Doctor.GetDoctor(app.DrNum);
                if (d == null)
                {
                    d = new Doctor(app, appEx);
                    d.Insert();
                }
                else if (firstTime)
                {
                    if (!d.IsEqual(app, appEx))
                    {
                        if (MessageBox.Show("過去に入力された施術師データと一致しません。" +
                            "施術師データを更新し、入力データを登録してもよろしいですか？",
                            "施術師情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    d.Update(app, appEx, !firstTime);
                }
            }

            //データベースへ反映
            var upType = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
            using (var tran = DB.Main.CreateTransaction())
            {
                if (app.Update(User.CurrentUser.UserID, upType, tran) &&
                    (appEx == null || appEx.Upsert(tran)))
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// 次のApplicationを表示します
        /// </summary>
        /// <param name="app"></param>
        private void displayDataReflesh(App app)
        {
            currentAid = app.Aid;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item.GetType() == typeof(TextBox))
                    {
                        item.Text = "";
                        item.BackColor = SystemColors.Info;
                    }
                    func(item);
                }
            });
            func(panelControls);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(app.Ufirst) +
                "  2:" + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                //ベリファイ入力時
                var cai = new CheckInputAall(dic[app.Aid]);
                setVerify(cai);
            }
            else
            {
                //App_Flagのチェック
                if (!app.StatusFlagCheck(StatusFlag.入力済))
                {
                    var ocr = app.OcrData.Split(',');
                    //一度もチェックしていない画像
                    if (ocr.Length > 40 && ocr[40] != null)
                    {
                        var f = Fusho.Change(ocr[40]);
                        if (Fusho.BasicWordsCheck(f)) textBox1Fusyo.Text = f;
                    }
                    if (ocr.Length > 57 && ocr[57] != null)
                    {
                        var f = Fusho.Change(ocr[57]);
                        if (Fusho.BasicWordsCheck(f)) textBox2Fusyo.Text = f;
                    }
                    if (ocr.Length > 74 && ocr[74] != null)
                    {
                        var f = Fusho.Change(ocr[74]);
                        if (Fusho.BasicWordsCheck(f)) textBox3Fusyo.Text = f;
                    }
                    if (ocr.Length > 91 && ocr[91] != null)
                    {
                        var f = Fusho.Change(ocr[91]);
                        if (Fusho.BasicWordsCheck(f)) textBox4Fusyo.Text = f;
                    }
                    if (ocr.Length > 108 && ocr[108] != null)
                    {
                        var f = Fusho.Change(ocr[108]);
                        if (Fusho.BasicWordsCheck(f)) textBox5Fusyo.Text = f;
                    }
                }
                else
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    setDataFromRcd(app);
                }
            }

            //画像の表示
            setImage(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                //メイン画像の表示
                scrollPictureControl1.SetPictureFile(fn);
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="a"></param>
        private void setDataFromRcd(App a)
        {
            //OCRチェックが済んだ画像の場合
            if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                textBoxY.Text = "--";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                textBoxY.Text = "++";
            }
            else
            {
                var ax = AppEx.Select(a.Aid);
                textBoxY.Text = a.MediYear.ToString();
                textBoxM.Text = a.MediMonth.ToString();
                textBoxHnum.Text = a.HihoNum;
                textBoxRatio.Text = a.Ratio == 0 ? "" : a.Ratio.ToString();
                textBoxSex.Text = a.Sex.ToString();
                textBoxBirthday.Text = DateTimeEx.GetEraNumber(a.Birthday).ToString();
                textBoxBY.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM.Text = a.Birthday.Month.ToString();
                textBoxBD.Text = a.Birthday.Day.ToString();
                textBoxName.Text = a.PersonName;
                textBoxDays.Text = a.CountedDays.ToString();
                textBox1Fusyo.Text = a.FushoName1;
                textBox2Fusyo.Text = a.FushoName2;
                textBox3Fusyo.Text = a.FushoName3;
                textBox4Fusyo.Text = a.FushoName4;
                textBox5Fusyo.Text = a.FushoName5;
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate1).ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate2).ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate3).ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate4).ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate5).ToString();
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoM.Text = a.FushoDate1.Month.ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoM.Text = a.FushoDate2.Month.ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoM.Text = a.FushoDate3.Month.ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoM.Text = a.FushoDate4.Month.ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoM.Text = a.FushoDate5.Month.ToString();
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoD.Text = a.FushoDate1.Day.ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoD.Text = a.FushoDate2.Day.ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoD.Text = a.FushoDate3.Day.ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoD.Text = a.FushoDate4.Day.ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoD.Text = a.FushoDate5.Day.ToString();
                if (a.FushoDays1 != 0) textBox1FusyoDays.Text = a.FushoDays1.ToString();
                if (a.FushoDays2 != 0) textBox2FusyoDays.Text = a.FushoDays2.ToString();
                if (a.FushoDays3 != 0) textBox3FusyoDays.Text = a.FushoDays3.ToString();
                if (a.FushoDays4 != 0) textBox4FusyoDays.Text = a.FushoDays4.ToString();
                if (a.FushoDays5 != 0) textBox5FusyoDays.Text = a.FushoDays5.ToString();
                if (a.FushoCourse1 != 0) textBox1FusyoTen.Text = a.FushoCourse1.ToString();
                if (a.FushoCourse2 != 0) textBox2FusyoTen.Text = a.FushoCourse2.ToString();
                if (a.FushoCourse3 != 0) textBox3FusyoTen.Text = a.FushoCourse3.ToString();
                if (a.FushoCourse4 != 0) textBox4FusyoTen.Text = a.FushoCourse4.ToString();
                if (a.FushoCourse5 != 0) textBox5FusyoTen.Text = a.FushoCourse5.ToString();
                textBoxShinryoDays.Text = ax.SejyutuDayList;
                textBoxTotal.Text = a.Total.ToString();
                textBoxCharge.Text = a.Charge == 0 ? "" : a.Charge.ToString();
                textBoxFutan.Text = a.Partial == 0 ? "" : a.Partial.ToString();
                textBoxDrCode.Text = a.DrNum;
                textBoxHosName.Text = a.ClinicName;
                textBoxDrName.Text = a.DrName;
            }
        }

        private int getBuiCount(App r)
        {
            int c = 1;
            if (r.FushoName2 != string.Empty) c++;
            if (r.FushoName3 != string.Empty) c++;
            if (r.FushoName4 != string.Empty) c++;
            if (r.FushoName5 != string.Empty) c++;
            return c;
        }

        private void textBoxs_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == Color.LightCyan) t.BackColor = SystemColors.Info;
        }

        private void textBoxs_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            //コントロール部分のスクロール
            int y = -panelControlScroll.AutoScrollPosition.Y;
            if (panelControlScroll.Height + y - 160 < t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, panelControlScroll.Height);
            }
            else if (y + 100 > t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, 0);
            }

            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;

            if (headerControls.Contains(t)) scrollPictureControl1.ScrollPosition = headerPos;
            else if (headerControls.Contains(t)) scrollPictureControl1.ScrollPosition = headerPos;
            else if (iryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = iryoPos;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = costPos;
            else if (shomeiControls.Contains(t)) scrollPictureControl1.ScrollPosition = shomeiPos;
            else if (textBoxDrCode == t) scrollPictureControl1.ScrollPosition = drNumPos;
        }


        private void focusBack(bool infoColorFocus)
        {
            Control[] controls = { textBoxY, textBoxM,textBoxHnum, textBoxRatio,
                    textBoxDrCode, textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, 
                    textBoxSex, textBoxName, textBoxDays, textBox1Fusyo, textBoxTotal, 
                    textBoxCharge, textBoxShinryoDays, textBoxHosName,textBoxDrName,};

            if (infoColorFocus)
            {
                foreach (var item in controls)
                {
                    if (item.BackColor != SystemColors.Info)
                    {
                        item.Focus();
                        return;
                    }
                }
            }

            foreach (var item in controls)
            {
                if (item.Enabled)
                {
                    item.Focus();
                    return;
                }
            }

            buttonRegist.Focus();
        }

        //private void panelIN_Scroll(object sender, ScrollEventArgs e)
        //{
        //    if (!(ActiveControl is TextBox)) return;

        //    var t = (TextBox)ActiveControl;
        //    var pt = scrollPictureControl1.ScrollPosition;
        //    var pos = new Point(-pt.X, -pt.Y);

        //    if (headerControls.Contains(t)) headerPos = pos;
        //    else if (headerControls.Contains(t)) headerPos = pos;
        //    else if (iryoControls.Contains(t)) iryoPos = pos;
        //    else if (costControls.Contains(t)) costPos = pos;
        //    else if (shomeiControls.Contains(t)) shomeiPos = pos;
        //    else if (textBoxDrCode == t) drNumPos = pos;
        //}

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(CheckInputAall ca)
        {
            var a = ca.app;
            var ax = AppEx.Select(a.Aid);

            //1回目入力の値を挿入
            if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                textBoxY2.Text = "--";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                textBoxY2.Text = "++";
            }
            else
            {
                textBoxY2.Text = a.MediYear.ToString();
                textBoxM2.Text = a.MediMonth.ToString();
                textBoxDrCode2.Text = a.DrNum;
                textBoxHnum2.Text = a.HihoNum;
                textBoxRatio2.Text = a.Ratio == 0 ? "7" : a.Ratio.ToString();
                textBoxBirthday2.Text = DateTimeEx.GetEraNumber(a.Birthday).ToString();
                textBoxBY2.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM2.Text = a.Birthday.Month.ToString();
                textBoxBD2.Text = a.Birthday.Day.ToString();
                textBoxSex2.Text = a.Sex.ToString();
                textBoxName2.Text = a.PersonName;
                textBoxTotal2.Text = a.Total.ToString();

                //ここから先はベリファイなし
                textBoxDays.Text = a.CountedDays.ToString();
                textBox1Fusyo.Text = a.FushoName1;
                textBoxShinryoDays.Text = ax.SejyutuDayList;
                textBoxHosName.Text = a.ClinicName;
                textBoxDrName.Text = a.DrName;
                textBoxCharge.Text = a.Charge == 0 ? "" : a.Charge.ToString();
                textBoxFutan.Text = a.Partial == 0 ? "" : a.Partial.ToString();

                textBox1Fusyo.Text = a.FushoName1;
                textBox2Fusyo.Text = a.FushoName2;
                textBox3Fusyo.Text = a.FushoName3;
                textBox4Fusyo.Text = a.FushoName4;
                textBox5Fusyo.Text = a.FushoName5;
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate1).ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate2).ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate3).ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate4).ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoY.Text = DateTimeEx.GetJpYear(a.FushoDate5).ToString();
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoM.Text = a.FushoDate1.Month.ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoM.Text = a.FushoDate2.Month.ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoM.Text = a.FushoDate3.Month.ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoM.Text = a.FushoDate4.Month.ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoM.Text = a.FushoDate5.Month.ToString();
                if (a.FushoDate1 != DateTime.MinValue) textBox1FusyoD.Text = a.FushoDate1.Day.ToString();
                if (a.FushoDate2 != DateTime.MinValue) textBox2FusyoD.Text = a.FushoDate2.Day.ToString();
                if (a.FushoDate3 != DateTime.MinValue) textBox3FusyoD.Text = a.FushoDate3.Day.ToString();
                if (a.FushoDate4 != DateTime.MinValue) textBox4FusyoD.Text = a.FushoDate4.Day.ToString();
                if (a.FushoDate5 != DateTime.MinValue) textBox5FusyoD.Text = a.FushoDate5.Day.ToString();
                if (a.FushoDays1 != 0) textBox1FusyoDays.Text = a.FushoDays1.ToString();
                if (a.FushoDays2 != 0) textBox2FusyoDays.Text = a.FushoDays2.ToString();
                if (a.FushoDays3 != 0) textBox3FusyoDays.Text = a.FushoDays3.ToString();
                if (a.FushoDays4 != 0) textBox4FusyoDays.Text = a.FushoDays4.ToString();
                if (a.FushoDays5 != 0) textBox5FusyoDays.Text = a.FushoDays5.ToString();
                if (a.FushoCourse1 != 0) textBox1FusyoTen.Text = a.FushoCourse1.ToString();
                if (a.FushoCourse2 != 0) textBox2FusyoTen.Text = a.FushoCourse2.ToString();
                if (a.FushoCourse3 != 0) textBox3FusyoTen.Text = a.FushoCourse3.ToString();
                if (a.FushoCourse4 != 0) textBox4FusyoTen.Text = a.FushoCourse4.ToString();
                if (a.FushoCourse5 != 0) textBox5FusyoTen.Text = a.FushoCourse5.ToString();


                //ベリファイない分のBoxを無効化
                textBoxDays.Enabled = false;
                textBox1Fusyo.Enabled = false;
                textBoxCharge.Enabled = false;
                textBoxShinryoDays.Enabled = false;
                textBoxHosName.Enabled = false;
                textBoxDrName.Enabled = false;
                textBoxFutan.Enabled = false;

                textBox1Fusyo.Enabled = false;
                textBox1FusyoY.Enabled = false;
                textBox1FusyoM.Enabled = false;
                textBox1FusyoD.Enabled = false;
                textBox1FusyoDays.Enabled = false;
                textBox1FusyoTen.Enabled = false;

                textBox2Fusyo.Enabled = false;
                textBox2FusyoY.Enabled = false;
                textBox2FusyoM.Enabled = false;
                textBox2FusyoD.Enabled = false;
                textBox2FusyoDays.Enabled = false;
                textBox2FusyoTen.Enabled = false;

                textBox3Fusyo.Enabled = false;
                textBox3FusyoY.Enabled = false;
                textBox3FusyoM.Enabled = false;
                textBox3FusyoD.Enabled = false;
                textBox3FusyoDays.Enabled = false;
                textBox3FusyoTen.Enabled = false;

                textBox4Fusyo.Enabled = false;
                textBox4FusyoY.Enabled = false;
                textBox4FusyoM.Enabled = false;
                textBox4FusyoD.Enabled = false;
                textBox4FusyoDays.Enabled = false;
                textBox4FusyoTen.Enabled = false;

                textBox5Fusyo.Enabled = false;
                textBox5FusyoY.Enabled = false;
                textBox5FusyoM.Enabled = false;
                textBox5FusyoD.Enabled = false;
                textBox5FusyoDays.Enabled = false;
                textBox5FusyoTen.Enabled = false;
            }

            //エラーに応じてテキストボックスを有効化、値設定
            var act = new Action<TextBox, bool, string>((t, b, s) =>
                {
                    if (a.StatusFlagCheck(StatusFlag.ベリファイ済))
                    {
                        t.Enabled = false;
                        t.BackColor = SystemColors.Control;
                        t.Text = s;
                    }
                    else
                    {
                        t.Enabled = b;
                        t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                        t.Text = b ? string.Empty : s;
                    }
                });

            //これらの項目はかならず入力、ベリファイ済みの時のみ表示
            if (a.StatusFlagCheck(StatusFlag.ベリファイ済))
            {
                act(textBoxRatio, false, a.Ratio.ToString());
                act(textBoxSex, false, a.Sex.ToString());
                act(textBoxDrCode, false, a.DrNum);
            }
            else
            {
                act(textBoxRatio, true, a.Ratio.ToString());
                act(textBoxSex, true, a.Sex.ToString());
                act(textBoxDrCode, true, a.DrNum);
            }

            act(textBoxY, ca.ErrAyear, a.MediYear == -3 ? "--" : a.MediYear == -4 ? "++" : a.MediYear.ToString());
            if (a.MediYear > 0)
            {
                var fn = Person.GetVerifiedFamilyNameAndAdd(a.HihoNum);
                var p = Person.GetPerson(a.HihoNum, a.Birthday);

                act(textBoxY, ca.ErrAyear, a.MediYear.ToString());
                act(textBoxM, ca.ErrAmonth, a.MediMonth.ToString());
                act(textBoxHnum, ca.ErrHnum, a.HihoNum);

                act(textBoxBirthday, ca.ErrBirth, DateTimeEx.GetEraNumber(a.Birthday).ToString());
                act(textBoxBY, ca.ErrBirth, DateTimeEx.GetJpYear(a.Birthday).ToString());
                act(textBoxBM, ca.ErrBirth, a.Birthday.Month.ToString());
                act(textBoxBD, ca.ErrBirth, a.Birthday.Day.ToString());
                act(textBoxName, p == null || !p.Verified, a.PersonName);
                var familyNeedCheck = ca.ErrHnum || fn == null;
                var personNeedCheck = ca.ErrHnum || ca.ErrBirth || p == null || !p.Verified;
                act(textBoxName, personNeedCheck, a.PersonName);
                act(textBoxTotal, ca.ErrAtotal, a.Total.ToString());

            }
            else
            {
                if (a.MediYear == -3)
                {
                    act(textBoxY, !a.StatusFlagCheck(StatusFlag.ベリファイ済), "--");
                }
                else if (a.MediYear == -4)
                {
                    act(textBoxY, !a.StatusFlagCheck(StatusFlag.ベリファイ済), "++");
                }

                act(textBoxM, false, "");
                act(textBoxHnum, false, "");
                act(textBoxRatio, false, "");
                act(textBoxSex, false, "");
                act(textBoxBirthday, false, "");
                act(textBoxBY, false, "");
                act(textBoxBM, false, "");
                act(textBoxBD, false, "");
                act(textBoxName, false, "");
                act(textBox1Fusyo, false, "");
                act(textBoxDays, false, "");
                act(textBoxTotal, false, "");
                act(textBoxCharge, false, "");
                act(textBoxDrCode, false, "");
                act(textBoxDrName, false, "");
                act(textBoxHosName, false, "");
            }
        }

        /// <summary>
        /// ベリファイ入力時、1回目と数値が一致するかチェックします
        /// </summary>
        /// <returns></returns>
        private bool checkVerify()
        {
            bool error = false;
            var act = new Action<TextBox, TextBox>((t1, t2) =>
                {
                    if (!t1.Enabled) return;
                    if (t1.Text == t2.Text)
                    {
                        t1.BackColor = SystemColors.Info;
                        t2.BackColor = SystemColors.Info;
                        t2.Visible = false;
                    }
                    else
                    {
                        t1.BackColor = Color.Pink;
                        t2.BackColor = Color.Pink;
                        t2.Visible = true;
                        error = true;
                    }
                });

            act(textBoxY, textBoxY2);
            act(textBoxM, textBoxM2);
            act(textBoxHnum, textBoxHnum2);
            act(textBoxRatio, textBoxRatio2);
            act(textBoxBirthday, textBoxBirthday2);
            act(textBoxSex, textBoxSex2);
            act(textBoxBY, textBoxBY2);
            act(textBoxBM, textBoxBM2);
            act(textBoxBD, textBoxBD2);
            act(textBoxName, textBoxName2);

            act(textBoxTotal, textBoxTotal2);
            if (textBoxDrCode.Text.Contains("*") && textBoxDrCode2.Text.Contains("*"))
            {
                textBoxDrCode2.Text = textBoxDrCode.Text;
            }
            else
            {
                act(textBoxDrCode, textBoxDrCode2);
            }

            return !error;
        }

        int prevIndex = -1;
        private void dataGridViewPlist_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {

#if DEBUG
#else
            if (!firstTime)
            {
                MessageBox.Show("現在ベリファイ入力には対応していません。画面を閉じます。", 
                    "ベリファイ未対応",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                this.Close();
                return;
            }
#endif

            if (dataGridViewPlist.RowCount != 0)
            {
                dataGridViewPlist.CurrentCell = null;
                dataGridViewPlist.CurrentCell = dataGridViewPlist["Aid", 0];
                scrollPictureControl1.AdjustImageWidth();
            }
        }

        private void dataGridViewPlist_CurrentCellChanged_1(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void textBoxDrCode_Leave(object sender, EventArgs e)
        {
            //ベリファイ時は引っ張らない
            if (!firstTime) return;

            var code = textBoxDrCode.Text.Trim();
            var d = Doctor.GetDoctor(code);
            if (d == null || !d.Verified) return;

            if (textBoxHosName.Text == string.Empty) textBoxHosName.Text = d.HosName;

            if (textBoxDrName.Text.Length != 0) return;

            //2016.2.27 松本 優介指示により、施術師名を姓のみからフルネーム引っ張ってくるように
            //int i = d.DrName.IndexOf('　');
            //if (i < 1) return;
            //textBoxDrName.Text = d.DrName.Remove(i + 1);
            textBoxDrName.Text = d.DrName;
        }

        private void textBoxHnum_Leave(object sender, EventArgs e)
        {
            var hnum = textBoxHnum.Text.Trim();
            var fn = Person.GetVerifiedFamilyNameAndAdd(hnum);
            if (fn == null) return;
        }

        private void textBoxBD_Leave(object sender, EventArgs e)
        {
            if (textBoxName.Text != string.Empty) return;
            var hnum = textBoxHnum.Text.Trim();

            int j, y, m, d;
            DateTime dt;
            if (!int.TryParse(textBoxBirthday.Text, out j)) return;
            if (!int.TryParse(textBoxBY.Text, out y)) return;
            if (!int.TryParse(textBoxBM.Text, out m)) return;
            if (!int.TryParse(textBoxBD.Text, out d)) return;

            var dts = j == 1 ? "明治" : j == 2 ? "大正" : j == 3 ? "昭和" : j == 4 ? "平成" : "";
            dts += textBoxBY.Text + "/" + textBoxBM.Text + "/" + textBoxBD.Text;
            DateTimeEx.TryGetDateTimeFromJdate(dts, out dt);

            var p = Person.GetPerson(hnum, dt);
            if (p == null || !p.Verified) return;
            textBoxName.Text = p.Pname;
            textBoxSex.Text = p.Psex.ToString();
        }

        private void textBoxKanaToHalf_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            t.Text = Utility.ToHalfWithoutKatakana(t.Text);
        }

        private void textBoxShinryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }

        private void textBoxOryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.Text.Trim() == "----")
            {
                t.Text = textBoxShinryoDays.Text;
                return;
            }

            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            int y;
            if (textBoxY.Text == "--" || textBoxY.Text == "++")
            {
                panelReceipt.Visible = false;
                panelMonth.Visible = false;
            }
            else if (textBoxY.Text.Length > 0 && int.TryParse(textBoxY.Text, out y))
            {
                panelReceipt.Visible = true;
                panelMonth.Visible = true;
            }
            else
            {
                panelReceipt.Visible = false;
                panelMonth.Visible = false;
            }
        }

        private DateTime checkDate(TextBox tera, TextBox ty, TextBox tm, TextBox td, bool colorChange)
        {
            var error = false;
            var setError = new Action<TextBox>(t =>
                {
                    error = true;
                    if (colorChange) t.BackColor = Color.MistyRose;
                });
            var setChecked = new Action<TextBox>(t => t.BackColor = SystemColors.Info);

            int ad, y, m, d;
            string era = string.Empty;

            if (tera == null) era = "平成";
            else era = DateTimeEx.GetEraName(tera.Text.Trim());
            if (string.IsNullOrEmpty(era)) setError(tera);

            int.TryParse(ty.Text.Trim(), out y);
            if (y == 0) setError(ty);
            int.TryParse(tm.Text.Trim(), out m);
            if (m == 0) setError(tm);
            int.TryParse(td.Text.Trim(), out d);
            if (d == 0) setError(td);

            ad = DateTimeEx.GetAdYear(era + y.ToString());
            if (DateTimeEx.IsDate(ad, m, d))
            {
                if (tera != null) setChecked(tera);
                setChecked(ty);
                setChecked(tm);
                setChecked(td);
                return new DateTime(ad, m, d);
            }
            else
            {
                if (!error)
                {
                    setError(tera);
                    setError(ty);
                    setError(tm);
                    setError(td);
                }
                return DateTime.MinValue;
            }
        }

        private void textBox1Fusyo_TextChanged(object sender, EventArgs e)
        {
            var b = textBox1Fusyo.Text.Trim().Length > 0;
            textBox2Fusyo.TabStop = b;
        }

        private void textBox2Fusyo_TextChanged(object sender, EventArgs e)
        {
            var b = textBox2Fusyo.Text.Trim().Length > 0;
            textBox2FusyoY.TabStop = b;
            textBox2FusyoM.TabStop = b;
            textBox2FusyoD.TabStop = b;
            textBox2FusyoDays.TabStop = b;
            textBox2FusyoTen.TabStop = b;
            textBox3Fusyo.TabStop = b;
        }

        private void textBox3Fusyo_TextChanged(object sender, EventArgs e)
        {
            var b = textBox3Fusyo.Text.Trim().Length > 0;
            textBox3FusyoY.TabStop = b;
            textBox3FusyoM.TabStop = b;
            textBox3FusyoD.TabStop = b;
            textBox3FusyoDays.TabStop = b;
            textBox3FusyoTen.TabStop = b;
            textBox4Fusyo.TabStop = b;
        }

        private void textBox4Fusyo_TextChanged(object sender, EventArgs e)
        {
            var b = textBox4Fusyo.Text.Trim().Length > 0;
            textBox4FusyoY.TabStop = b;
            textBox4FusyoM.TabStop = b;
            textBox4FusyoD.TabStop = b;
            textBox4FusyoDays.TabStop = b;
            textBox4FusyoTen.TabStop = b;
            textBox5Fusyo.TabStop = b;
        }

        private void textBox5Fusyo_TextChanged(object sender, EventArgs e)
        {
            var b = textBox5Fusyo.Text.Trim().Length > 0;
            textBox5FusyoY.TabStop = b;
            textBox5FusyoM.TabStop = b;
            textBox5FusyoD.TabStop = b;
            textBox5FusyoDays.TabStop = b;
            textBox5FusyoTen.TabStop = b;
        }

        private void textBoxPersons_TextChanged(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length != 0) return;
            var d = checkDate(textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, false);
            if (d == DateTime.MinValue) return;
            var p = Person.GetPerson(textBoxHnum.Text.Trim(), d);
            if (p == null) return;
            textBoxName.Text = p.Pname;
        }

        private void textBoxRatioTotal_TextChanged(object sender, EventArgs e)
        {
            int ratio, total;
            int.TryParse(textBoxRatio.Text, out ratio);
            int.TryParse(textBoxTotal.Text, out total);

            if (ratio == 0 || total == 0)
            {
                labelChargeInfo.Text = "※請求金額参考情報\r\n判別不能";
            }
            else
            {
                labelChargeInfo.Text = "※請求金額参考情報\r\n" +
                    "     給付割合：" + ratio.ToString() + "\r\n" +
                    "     参考金額：" + (total * ratio / 10).ToString();
            }
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pt = scrollPictureControl1.ScrollPosition;
            var pos = new Point(-pt.X, -pt.Y);

            if (headerControls.Contains(t)) headerPos = pos;
            else if (headerControls.Contains(t)) headerPos = pos;
            else if (iryoControls.Contains(t)) iryoPos = pos;
            else if (costControls.Contains(t)) costPos = pos;
            else if (shomeiControls.Contains(t)) shomeiPos = pos;
            else if (textBoxDrCode == t) drNumPos = pos;
        }
    }
}
