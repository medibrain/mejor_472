﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Sapporoshi
{
    public partial class ExportForm : Form
    {
        int cym;
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        int cy => DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100);
        //int cy => DateTimeEx.GetHsYearFromAd(cym / 100);
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        int cm => cym % 100;

        public ExportForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (radioButtonJyuShinsei.Checked)
            {
                exportJyuShinsei();
            }
            else if (radioButtonJyuGigi.Checked)
            {
                exportJyuGigi();
            }
            else if (radioButtonJyuChosa.Checked)
            {
                using (var f = new SaveFileDialog())
                {
                    f.FileName = "test" + cm.ToString() + ".csv";
                    if (f.ShowDialog() != DialogResult.OK) return;
                    Export.DaysCheck(cym, f.FileName);
                }
            }
            else if (radioButtonNumbering.Checked)
            {
                shinkyuNumbering();
            }
            else if (radioButtonShinkyuShinsei.Checked)
            {
                exportShinkyu();
            }
            else if(radioButtonOtherCheck.Checked)
            {
                using (var f = new FormOCRCheckOtherCheckSK(cy, cm)) f.ShowDialog();
            }    
            else
            {
                MessageBox.Show("出力形式を選択して下さい");
            }
        }


        private void exportJyuShinsei()
        {
            using (var f = new SaveFileDialog())
            {
                f.FileName = cm.ToString() + "月柔道整復師申請書データ.xlsx";
                f.Filter = "Excelファイル(*.xlsx)|*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return;
                Export.ExportJyuShinsei(cym, f.FileName);
            }
        }

        private void exportJyuGigi()
        {
            using (var f = new SaveFileDialog())
            {
                f.FileName = cm.ToString() + "月疑義対象者データ.xlsx";
                f.Filter = "Excelファイル(*.xlsx)|*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return;
                Export.ExportJyuGigi(cym, f.FileName);
            }
        }

        private void exportShinkyu()
        {
            var wf = new WaitForm();
            var dir = string.Empty;

            using (var f = new FolderBrowserDialog())
            {
                if (System.IO.Directory.Exists("F:\\medi_jyusei\\さ　札幌市\\鍼灸納品データ"))
                    f.SelectedPath= "F:\\medi_jyusei\\さ　札幌市\\鍼灸納品データ";

                if (f.ShowDialog() != DialogResult.OK) return;
                dir = f.SelectedPath + "\\" + cm.ToString("00") + "月";
            }

            System.IO.Directory.CreateDirectory(dir);

            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("区一覧を作成しています…");

                var ss = Scan.GetScanListYM(cym);
                ss = ss.FindAll(s => s.Note2 != string.Empty);

                var kuList = new HashSet<string>();
                kuList.Add("国保連");
                foreach (var item in ss)
                {
                    if (!item.Note1.Contains("区")) continue;
                    var name = item.Note1.Substring(4);
                    name = name.Remove(name.IndexOf("区") + 1);
                    kuList.Add(name);
                }

                foreach (var item in kuList)
                {
                    wf.LogPrint(item + "分の出力を行なっています…");

                    var fileName = dir + "\\" + item + "\\鍼灸データ.xlsx";
                    var res = Export.ExportShinkyu(cym, item, fileName, wf);
                    if (!res)
                    {
                        MessageBox.Show("ユーザーによるキャンセル、またはエラーにより処理を中断しました。" +
                            "出力は完了していませんのでご注意ください。",
                            "出力中止",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Asterisk);
                        return;
                    }
                }
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が完了しました");
        }

        private void shinkyuNumbering()
        {
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("整理番号の確認をしています。");

                var sl = Scan.GetScanListYM(cym);
                sl = sl.FindAll(s => s.Note2 != string.Empty);

                var apps = new List<App>();
                sl.ForEach(s => apps.AddRange(App.GetAppsSID(s.SID)));
                apps = apps.FindAll(a => a.MediYear > 0);
                apps.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                var c = apps.Count(a => a.Numbering == string.Empty);

                if (c == 0)
                {
                    MessageBox.Show("整理番号の付加が新たに必要なデータはありません。");
                    return;
                }
                else
                {
                    wf.LogPrint("整理番号の付与が必要なデータが" + c.ToString() + "件見つかりました。");
                    if (MessageBox.Show("鍼灸マッサージのデータ" + c.ToString() + "件に管理番号をつけます。よろしいですか？",
                        "管理番号確認",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Question) != DialogResult.OK) return;
                }

                int maxID;
                var maxStr = apps.Max(a => a.Numbering);
                if (!int.TryParse(maxStr, out maxID))
                {
                    if (maxStr != string.Empty)
                        throw new Exception("整理番号に数値以外の文字が検出されました");
                }
                if (maxID == 0) maxID = cm * 1000000 + 700000;

                var sql = "UPDATE application SET Numbering=:nb WHERE aid=:aid;";
                using (var tran = DB.Main.CreateTransaction())
                using (var cmd = DB.Main.CreateCmd(sql, tran))
                {
                    cmd.Parameters.Add("nb", NpgsqlTypes.NpgsqlDbType.Text);
                    cmd.Parameters.Add("aid", NpgsqlTypes.NpgsqlDbType.Integer);

                    foreach (var item in apps)
                    {
                        if (item.Numbering != string.Empty) continue;
                        maxID++;
                        var id = maxID.ToString("00000000");
                        item.Numbering = string.Empty;

                        cmd.Parameters["nb"].Value = id;
                        cmd.Parameters["aid"].Value = item.Aid;
                        if (!cmd.TryExecuteNonQuery())
                        {
                            tran.Rollback();
                            return;
                        }
                    }
                    tran.Commit();
                }
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("管理番号の付与が終わりました。");

        }
    }
}
