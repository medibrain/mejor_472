﻿namespace Mejor.Sapporoshi
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExport = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.radioButtonJyuShinsei = new System.Windows.Forms.RadioButton();
            this.radioButtonJyuGigi = new System.Windows.Forms.RadioButton();
            this.radioButtonJyuChosa = new System.Windows.Forms.RadioButton();
            this.radioButtonShinkyuShinsei = new System.Windows.Forms.RadioButton();
            this.radioButtonNumbering = new System.Windows.Forms.RadioButton();
            this.radioButtonOtherCheck = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // buttonExport
            // 
            this.buttonExport.Location = new System.Drawing.Point(64, 261);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(75, 23);
            this.buttonExport.TabIndex = 5;
            this.buttonExport.Text = "実行";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(145, 261);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.Text = "閉じる";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // radioButtonJyuShinsei
            // 
            this.radioButtonJyuShinsei.AutoSize = true;
            this.radioButtonJyuShinsei.Location = new System.Drawing.Point(26, 25);
            this.radioButtonJyuShinsei.Name = "radioButtonJyuShinsei";
            this.radioButtonJyuShinsei.Size = new System.Drawing.Size(147, 16);
            this.radioButtonJyuShinsei.TabIndex = 0;
            this.radioButtonJyuShinsei.TabStop = true;
            this.radioButtonJyuShinsei.Text = "柔道整復師申請書データ";
            this.radioButtonJyuShinsei.UseVisualStyleBackColor = true;
            // 
            // radioButtonJyuGigi
            // 
            this.radioButtonJyuGigi.AutoSize = true;
            this.radioButtonJyuGigi.Location = new System.Drawing.Point(26, 57);
            this.radioButtonJyuGigi.Name = "radioButtonJyuGigi";
            this.radioButtonJyuGigi.Size = new System.Drawing.Size(135, 16);
            this.radioButtonJyuGigi.TabIndex = 1;
            this.radioButtonJyuGigi.TabStop = true;
            this.radioButtonJyuGigi.Text = "柔整疑義対象者データ";
            this.radioButtonJyuGigi.UseVisualStyleBackColor = true;
            // 
            // radioButtonJyuChosa
            // 
            this.radioButtonJyuChosa.AutoSize = true;
            this.radioButtonJyuChosa.Location = new System.Drawing.Point(26, 89);
            this.radioButtonJyuChosa.Name = "radioButtonJyuChosa";
            this.radioButtonJyuChosa.Size = new System.Drawing.Size(232, 16);
            this.radioButtonJyuChosa.TabIndex = 2;
            this.radioButtonJyuChosa.TabStop = true;
            this.radioButtonJyuChosa.Text = "被保険者調査結果データ（基本データのみ）";
            this.radioButtonJyuChosa.UseVisualStyleBackColor = true;
            // 
            // radioButtonShinkyuShinsei
            // 
            this.radioButtonShinkyuShinsei.AutoSize = true;
            this.radioButtonShinkyuShinsei.Location = new System.Drawing.Point(26, 175);
            this.radioButtonShinkyuShinsei.Name = "radioButtonShinkyuShinsei";
            this.radioButtonShinkyuShinsei.Size = new System.Drawing.Size(175, 16);
            this.radioButtonShinkyuShinsei.TabIndex = 4;
            this.radioButtonShinkyuShinsei.TabStop = true;
            this.radioButtonShinkyuShinsei.Text = "はりきゅうマッサージ申請書データ";
            this.radioButtonShinkyuShinsei.UseVisualStyleBackColor = true;
            // 
            // radioButtonNumbering
            // 
            this.radioButtonNumbering.AutoSize = true;
            this.radioButtonNumbering.Location = new System.Drawing.Point(26, 143);
            this.radioButtonNumbering.Name = "radioButtonNumbering";
            this.radioButtonNumbering.Size = new System.Drawing.Size(200, 16);
            this.radioButtonNumbering.TabIndex = 3;
            this.radioButtonNumbering.TabStop = true;
            this.radioButtonNumbering.Text = "はりきゅうマッサージ整理番号割り当て";
            this.radioButtonNumbering.UseVisualStyleBackColor = true;
            // 
            // radioButtonOtherCheck
            // 
            this.radioButtonOtherCheck.AutoSize = true;
            this.radioButtonOtherCheck.Location = new System.Drawing.Point(26, 228);
            this.radioButtonOtherCheck.Name = "radioButtonOtherCheck";
            this.radioButtonOtherCheck.Size = new System.Drawing.Size(102, 16);
            this.radioButtonOtherCheck.TabIndex = 4;
            this.radioButtonOtherCheck.TabStop = true;
            this.radioButtonOtherCheck.Text = "不要画像チェック";
            this.radioButtonOtherCheck.UseVisualStyleBackColor = true;
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 291);
            this.Controls.Add(this.radioButtonOtherCheck);
            this.Controls.Add(this.radioButtonNumbering);
            this.Controls.Add(this.radioButtonShinkyuShinsei);
            this.Controls.Add(this.radioButtonJyuChosa);
            this.Controls.Add(this.radioButtonJyuGigi);
            this.Controls.Add(this.radioButtonJyuShinsei);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonExport);
            this.Name = "ExportForm";
            this.Text = "札幌市";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.RadioButton radioButtonJyuShinsei;
        private System.Windows.Forms.RadioButton radioButtonJyuGigi;
        private System.Windows.Forms.RadioButton radioButtonJyuChosa;
        private System.Windows.Forms.RadioButton radioButtonShinkyuShinsei;
        private System.Windows.Forms.RadioButton radioButtonNumbering;
        private System.Windows.Forms.RadioButton radioButtonOtherCheck;
    }
}