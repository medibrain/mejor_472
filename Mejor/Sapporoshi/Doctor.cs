﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.Sapporoshi
{
    class Doctor
    {
        /*
        CREATE TABLE doctor
        (
          code text NOT NULL,
          dr_name text NOT NULL,
          dr_add text NOT NULL,
          hos_name text,
          group_name text NOT NULL,
          dairi_name text NOT NULL,
          dairi_add text NOT NULL,
          verified boolean NOT NULL DEFAULT false,
          insert_uid integer NOT NULL DEFAULT 0,
          update_uid integer NOT NULL DEFAULT 0,
          lastaid integer NOT NULL DEFAULT 0,
          CONSTRAINT doctor_pkey PRIMARY KEY (code)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE doctor
          OWNER TO postgres;
        */

        public string Code { get; private set; }
        public string DrName { get; private set; }
        public string DrAdd { get; private set; }
        public string HosName { get; private set; }
        public string GroupName { get; private set; }
        public string DairiName { get; private set; }
        public string DairiAdd { get; private set; }
        public bool Verified { get; private set; }
        public int LastAID { get; private set; }

        private Doctor(string code)
        {
            this.Code = code;
            this.Verified = false;
        }

        public Doctor(App app, AppEx appEx)
        {
            this.Code = app.DrNum;
            this.DrName = app.DrName;
            this.DrAdd = appEx.DrAdd;
            this.HosName = app.ClinicName;
            this.GroupName = app.AccountName;
            this.DairiName = appEx.DairiName;
            this.DairiAdd = appEx.DairiAdd;
            this.LastAID = app.Aid;
            this.Verified = false;
        }

        /// <summary>
        /// 施術師情報を比較します。
        /// 施術師コードが違うものを比較した場合、例外が発生します。
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public bool IsEqual(App app, AppEx appEx)
        {
            if (this.Code != app.DrNum) throw new Exception("施術師Noが違う情報を比較しようとしました");

            return //this.DrName == app.Sdoctor &&
            this.HosName == app.ClinicName &&
            this.GroupName == app.AccountName &&
            this.DrAdd == appEx.DrAdd &&
            this.DairiName == appEx.DairiName &&
            this.DairiAdd == appEx.DairiAdd;
        }

        public static Doctor GetDoctor(string code)
        {
            var sql = "SELECT code, dr_name, dr_add, hos_name, group_name, " +
                "dairi_name, dairi_add, verified " +
                "FROM doctor " +
                "WHERE code=:code";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = code;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var d = new Doctor(code);
                d.DrName = (string)res[0][1];
                d.DrAdd = (string)res[0][2];
                d.HosName = (string)res[0][3];
                d.GroupName = (string)res[0][4];
                d.DairiName = (string)res[0][5];
                d.DairiAdd = (string)res[0][6];
                d.Verified = (bool)res[0][7];

                return d;
            }
        }

        public bool Update(App app, AppEx appEx, bool forceVerify)
        {
            if (this.Code != app.DrNum) throw new Exception("施術師Noが違う情報を更新しようとしました");

            if (IsEqual(app, appEx))
            {
                if (this.Verified) return true;

                //情報が一致し、情報元aidが違う場合にベリファイ済みとみなす
                this.Verified = forceVerify ? true : this.LastAID != app.Aid;
                this.LastAID = app.Aid;
            }
            else
            {
                this.DrName = app.DrName;
                this.DrAdd = appEx.DrAdd;
                this.HosName = app.ClinicName;
                this.GroupName = app.AccountName;
                this.DairiName = appEx.DairiName;
                this.DairiAdd = appEx.DairiAdd;
                this.LastAID = app.Aid;
                this.Verified = forceVerify ? true : false;
            }

            var sql = "UPDATE doctor SET " +
                "dr_name=:dr_name, dr_add=:dr_add, hos_name=:hos_name, " +
                "group_name=:group_name, dairi_name=:dairi_name, dairi_add=:dairi_add, " +
                "verified=:verified, update_uid=:update_uid, lastaid=:lastaid " +
                "WHERE code = :code;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("dr_name", NpgsqlDbType.Text).Value = this.DrName;
                cmd.Parameters.Add("dr_add", NpgsqlDbType.Text).Value = this.DrAdd;
                cmd.Parameters.Add("hos_name", NpgsqlDbType.Text).Value = this.HosName;
                cmd.Parameters.Add("group_name", NpgsqlDbType.Text).Value = this.GroupName;
                cmd.Parameters.Add("dairi_name", NpgsqlDbType.Text).Value = this.DairiName;
                cmd.Parameters.Add("dairi_add", NpgsqlDbType.Text).Value = this.DairiAdd;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("update_uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("lastaid", NpgsqlDbType.Integer).Value = this.LastAID;
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = this.Code;

                return cmd.TryExecuteNonQuery();
            }
        }

        public bool Insert()
        {
            var sql = "INSERT INTO doctor " +
                "(code, dr_name, dr_add, hos_name, group_name, dairi_name, dairi_add, " +
                "verified, insert_uid, update_uid, lastaid) " +
                "VALUES " +
                "(:code, :dr_name, :dr_add, :hos_name, :group_name, :dairi_name, " +
                ":dairi_add, :verified, :insert_uid, :update_uid, :lastaid);";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = this.Code;
                cmd.Parameters.Add("dr_name", NpgsqlDbType.Text).Value = this.DrName;
                cmd.Parameters.Add("dr_add", NpgsqlDbType.Text).Value = this.DrAdd;
                cmd.Parameters.Add("hos_name", NpgsqlDbType.Text).Value = this.HosName;
                cmd.Parameters.Add("group_name", NpgsqlDbType.Text).Value = this.GroupName;
                cmd.Parameters.Add("dairi_name", NpgsqlDbType.Text).Value = this.DairiName;
                cmd.Parameters.Add("dairi_add", NpgsqlDbType.Text).Value = this.DairiAdd;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("insert_uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("update_uid", NpgsqlDbType.Integer).Value = 0;
                cmd.Parameters.Add("lastaid", NpgsqlDbType.Integer).Value = this.LastAID; ;

                return cmd.TryExecuteNonQuery();
            }
        }
    }
}
