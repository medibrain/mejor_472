﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Mejor.Sapporoshi
{
    class Export
    {
        public static bool ExportJyuShinsei(int cym, string fileName)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("対象情報を検索中です。");
                var sl = Scan.GetScanListYM(cym);
                sl = sl.FindAll(s => s.Note2 == string.Empty);

                var apps = new List<App>();
                sl.ForEach(s => apps.AddRange(App.GetAppsSID(s.SID)));
                apps = apps.FindAll(a => a.MediYear > 0);
                if (apps.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show("対象となるデータはありません。");
                    return true;
                }
                wf.LogPrint(apps.Count.ToString() + "件のデータを確認しました。");

                //連番設定の確認
                wf.LogPrint("整理番号を確認しています。");
                if (apps.All(a => a.Numbering == string.Empty))
                {
                    if (System.Windows.Forms.MessageBox.Show(
                        "整理番号が設定されていないデータがあります。整理番号を割り当てますか？",
                        "整理番号確認",
                        System.Windows.Forms.MessageBoxButtons.OKCancel,
                        System.Windows.Forms.MessageBoxIcon.Question)
                        != System.Windows.Forms.DialogResult.OK) return false;

                    //連番設定
                    wf.LogPrint("連番を割り当てています。");
                    int maxID;
                    var maxStr = apps.Max(a => a.Numbering);
                    if (!int.TryParse(maxStr, out maxID))
                    {
                        if (maxStr != string.Empty)
                            throw new Exception("整理番号に数値以外の文字が検出されました");
                    }
                    if (maxID == 0) maxID = (cym % 100) * 1000000;

                    var sql = "UPDATE application SET Numbering=:nb WHERE aid=:aid;";
                    using (var tran = DB.Main.CreateTransaction())
                    using (var cmd = DB.Main.CreateCmd(sql, tran))
                    {
                        cmd.Parameters.Add("nb", NpgsqlDbType.Text);
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer);

                        wf.SetMax(apps.Count);
                        wf.InvokeValue = 0;
                        wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                        foreach (var item in apps)
                        {
                            wf.InvokeValue++;
                            if (item.Numbering != string.Empty) continue;
                            maxID++;
                            var id = maxID.ToString("00000000");
                            item.Numbering = id;

                            cmd.Parameters["nb"].Value = id;
                            cmd.Parameters["aid"].Value = item.Aid;
                            if (!cmd.TryExecuteNonQuery())
                            {
                                tran.Rollback();
                                return false;
                            }
                        }
                        tran.Commit();
                        wf.LogPrint("整理番号の割り当てが完了しました。");
                    }
                }

                //施術機関、被保番順にソート
                apps.Sort((x, y) =>
                {
                    if (x.DrNum.Contains("*") && y.DrNum.Contains("*"))
                    {
                        return
                            x.ClinicName != y.ClinicName ? x.ClinicName.CompareTo(y.ClinicName) :
                            x.DrName != y.DrName ? x.DrName.CompareTo(y.DrName) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }
                    else if (!x.DrNum.Contains("*") && !y.DrNum.Contains("*"))
                    {
                        return
                            x.DrNum != y.DrNum ? x.DrNum.CompareTo(y.DrNum) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }

                    //どちらか一方にだけ「*」
                    return y.DrNum.CompareTo(x.DrNum);
                });


                wf.LogPrint("Excelデータを作成しています。");
                try
                {
                    wf.SetMax(apps.Count);
                    wf.InvokeValue = 0;
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    var fn = System.Windows.Forms.Application.StartupPath +
                        "\\Xlsx\\SapporoJyuShinsei.xlsx";

                    using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open))
                    {
                        var wb = new XSSFWorkbook(fs);
                        var ws = wb.GetSheetAt(0);
                        var rowCount = ws.LastRowNum + 2;

                        for (int i = 0; i < apps.Count; i++)
                        {
                            var row = ws.GetRow(i + 2);

                            wf.InvokeValue++;
                            var a = apps[i];
                            var aex = AppEx.Select(a.Aid);
                            row.GetCell(1).SetCellValue(a.Numbering);
                            row.GetCell(2).SetCellValue("4" + a.MediYear.ToString("00") + a.MediMonth.ToString("00"));

                            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                            row.GetCell(3).SetCellValue("4" + DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100).ToString("00") + (cym % 100).ToString("00"));
                            //row.GetCell(3).SetCellValue("4" + DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00") + (cym % 100).ToString("00"));

                            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                            row.GetCell(4).SetCellValue(a.HihoNum);
                            row.GetCell(5).SetCellValue(a.PersonName);
                            row.GetCell(6).SetCellValue(a.Sex.ToString());
                            row.GetCell(7).SetCellValue(a.Ratio == 0 ? "" : a.Ratio.ToString());
                            row.GetCell(8).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString());
                            row.GetCell(9).SetCellValue(a.DrNum.Contains("*") ? "999999999" : a.DrNum);
                            row.GetCell(10).SetCellValue(a.ClinicName);
                            row.GetCell(11).SetCellValue(a.DrName);
                            row.GetCell(12).SetCellValue(a.Total.ToString());

                            if (a.Partial != 0) row.GetCell(13).SetCellValue(a.Partial.ToString());
                            else if (a.Charge != 0) row.GetCell(13).SetCellValue((a.Total - a.Charge).ToString());
                            else if (a.Ratio != 0) row.GetCell(13).SetCellValue((a.Total - (a.Total * a.Ratio / 10)).ToString());

                            if (a.Charge != 0) row.GetCell(14).SetCellValue(a.Charge.ToString());
                            else if (a.Ratio != 0) row.GetCell(14).SetCellValue((a.Total * a.Ratio / 10).ToString());

                            row.GetCell(15).SetCellValue(aex.SejyutuDayList.Replace(" ", ","));
                            row.GetCell(16).SetCellValue(a.FushoName1);
                            row.GetCell(17).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.FushoDate1).ToString());
                            row.GetCell(18).SetCellValue(a.FushoDays1.ToString());
                            row.GetCell(19).SetCellValue(a.FushoCourse1 == 1 ? "治癒" : a.FushoCourse1 == 2 ? "中止" : a.FushoCourse1 == 3 ? "転医" : "");
                            if (a.FushoName2 != string.Empty)
                            {
                                row.GetCell(20).SetCellValue(a.FushoName2);
                                row.GetCell(21).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.FushoDate2).ToString());
                                row.GetCell(22).SetCellValue(a.FushoDays2.ToString());
                                row.GetCell(23).SetCellValue(a.FushoCourse2 == 1 ? "治癒" : a.FushoCourse2 == 2 ? "中止" : a.FushoCourse2 == 3 ? "転医" : "");
                            }
                            if (a.FushoName3 != string.Empty)
                            {
                                row.GetCell(24).SetCellValue(a.FushoName3);
                                row.GetCell(25).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.FushoDate3).ToString());
                                row.GetCell(26).SetCellValue(a.FushoDays3.ToString());
                                row.GetCell(27).SetCellValue(a.FushoCourse3 == 1 ? "治癒" : a.FushoCourse3 == 2 ? "中止" : a.FushoCourse3 == 3 ? "転医" : "");
                            }
                            if (a.FushoName4 != string.Empty)
                            {
                                row.GetCell(28).SetCellValue(a.FushoName4);
                                row.GetCell(29).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.FushoDate4).ToString());
                                row.GetCell(30).SetCellValue(a.FushoDays4.ToString());
                                row.GetCell(31).SetCellValue(a.FushoCourse4 == 1 ? "治癒" : a.FushoCourse4 == 2 ? "中止" : a.FushoCourse4 == 3 ? "転医" : "");
                            }
                            row.GetCell(32).SetCellValue(a.ScanID.ToString());
                            row.GetCell(33).SetCellValue(a.GroupID.ToString());
                            row.GetCell(34).SetCellValue(a.Aid.ToString());
                            row.GetCell(35).SetCellValue(System.IO.Path.GetFileName(a.GetImageFullPath()));
                            //break);
                        }
                        wf.LogPrint("Excelデータを出力しています。");
                        wb.Close();   
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            return true;
        }


        public static bool ExportJyuGigi(int cym, string fileName)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("対象情報を検索中です。");
                var sl = Scan.GetScanListYM(cym);
                sl = sl.FindAll(s => s.Note2 == string.Empty);

                var apps = new List<App>();
                sl.ForEach(s => apps.AddRange(App.GetAppsSID(s.SID)));
                apps = apps.FindAll(a => a.MediYear > 0);
                if (apps.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show("対象となるデータはありません。");
                    return true;
                }
                wf.LogPrint(apps.Count.ToString() + "件のデータを確認しました。");

                //連番設定の確認
                wf.LogPrint("整理番号を確認しています。");
                if (apps.All(a => a.Numbering == string.Empty))
                {
                    System.Windows.Forms.MessageBox.Show(
                        "整理番号が設定されていないデータがあります。申請書データを先に出力して整理番号を割り当てて下さい。",
                        "整理番号確認",
                        System.Windows.Forms.MessageBoxButtons.OK,
                        System.Windows.Forms.MessageBoxIcon.Stop);
                    return false;
                }

                //施術機関、被保番順にソート
                apps.Sort((x, y) =>
                {
                    if (x.DrNum.Contains("*") && y.DrNum.Contains("*"))
                    {
                        return
                            x.ClinicName != y.ClinicName ? x.ClinicName.CompareTo(y.ClinicName) :
                            x.DrName != y.DrName ? x.DrName.CompareTo(y.DrName) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }
                    else if (!x.DrNum.Contains("*") && !y.DrNum.Contains("*"))
                    {
                        return
                            x.DrNum != y.DrNum ? x.DrNum.CompareTo(y.DrNum) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }

                        //どちらか一方にだけ「*」
                        return y.DrNum.CompareTo(x.DrNum);
                });

                wf.LogPrint("Excelデータを作成しています。");
                try
                {
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
                    int month = cym % 100;
                    int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    wf.SetMax(apps.Count);
                    wf.InvokeValue = 0;
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    var fn = System.Windows.Forms.Application.StartupPath +
                        "\\Xlsx\\SapporoJyuGigi.xlsx";
                    using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                    {
                        var ex = System.IO.Path.GetExtension(fileName);

                            var wb = new XSSFWorkbook(fs);

                            var ws = wb.GetSheetAt(0);
                            var rowCount = ws.LastRowNum + 2;



                        for (int i = 0; i < apps.Count; i++)
                        {
                            wf.InvokeValue++;
                            var row = ws.GetRow(i + 2);
                            var a = apps[i];
                            var aex = AppEx.Select(a.Aid);
                            row.GetCell(1).SetCellValue(a.Numbering);
                            row.GetCell(2).SetCellValue("4" + a.MediYear.ToString("00") + a.MediMonth.ToString("00"));
                            row.GetCell(3).SetCellValue("4" + jYear.ToString("00") + month.ToString("00"));
                            row.GetCell(4).SetCellValue(a.HihoNum);
                            row.GetCell(5).SetCellValue(a.PersonName);
                            row.GetCell(6).SetCellValue(a.Sex.ToString());
                            row.GetCell(7).SetCellValue(a.Ratio == 0 ? "" : a.Ratio.ToString());
                            row.GetCell(8).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString());
                            row.GetCell(9).SetCellValue(a.DrNum.Contains("*") ? "999999999" : a.DrNum);
                            row.GetCell(10).SetCellValue(a.ClinicName);
                            row.GetCell(11).SetCellValue(a.DrName);
                            row.GetCell(12).SetCellValue(a.Total.ToString());

                            if (a.Partial != 0) row.GetCell(13).SetCellValue(a.Partial.ToString());
                            else if (a.Charge != 0) row.GetCell(13).SetCellValue((a.Total - a.Charge).ToString());
                            else if (a.Ratio != 0) row.GetCell(13).SetCellValue((a.Total - (a.Total * a.Ratio / 10)).ToString());

                            if (a.Charge != 0) row.GetCell(14).SetCellValue(a.Charge.ToString());
                            else if (a.Ratio != 0) row.GetCell(14).SetCellValue((a.Total * a.Ratio / 10).ToString());

                            //最終施術日取得

                            //20190816120817 furukawa st ////////////////////////
                            //月がないためGetAdYearMonthFromJyymmに変更
                            
                            int aAD = DateTimeEx.GetAdYearFromHs(int.Parse(a.MediYear.ToString("00") + a.MediMonth.ToString("00")));
                                //int aAD = DateTimeEx.GetAdYearFromEraYear(4, a.MediYear);
                            //20190816120817 furukawa ed ////////////////////////



                            var lastDays = new List<int>();
                            Array.ForEach(aex.SejyutuDayList.Split(' '), s => lastDays.Add(int.Parse(s)));
                            int lastDay = lastDays.Max();
                            if (lastDay == 0) throw new Exception("施術日一覧が読み取れませんでした。AID:" + a.Aid.ToString());
                            if (!DateTimeEx.IsDate(aAD, a.MediMonth, lastDay))
                            {
                                System.Windows.Forms.MessageBox.Show("");
                            }
                            var lastDate = new DateTime(aAD, a.MediMonth, lastDay);

                            //負傷名増加判断
                            bool zoka = false;

                            if ((aAD == a.FushoDate1.Year && a.MediMonth > a.FushoDate1.Month) || aAD > a.FushoDate1.Year)
                            {
                                if ((a.FushoDate2.Year == aAD && a.FushoDate2.Month == a.MediMonth) ||
                                    (a.FushoDate3.Year == aAD && a.FushoDate3.Month == a.MediMonth) ||
                                    (a.FushoDate4.Year == aAD && a.FushoDate4.Month == a.MediMonth) ||
                                    (a.FushoDate5.Year == aAD && a.FushoDate5.Month == a.MediMonth))
                                {
                                    zoka = true;
                                }
                            }

                            row.GetCell(15).SetCellValue(aex.SejyutuDayList.Replace(" ", ","));
                            row.GetCell(16).SetCellValue(a.CountedDays >= 10 ? "1" : "2");
                            row.GetCell(17).SetCellValue(a.FushoName3 != string.Empty ? "1" : "2");
                            row.GetCell(18).SetCellValue((lastDate - a.FushoDate1).TotalDays >= 90 ? "1" : "2");
                            row.GetCell(19).SetCellValue(zoka ? "1" : "2");
                            row.GetCell(20).SetCellValue("");

                            row.GetCell(21).SetCellValue(a.ScanID.ToString());
                            row.GetCell(22).SetCellValue(a.GroupID.ToString());
                            row.GetCell(23).SetCellValue(a.Aid.ToString());
                            row.GetCell(24).SetCellValue(System.IO.Path.GetFileName(a.GetImageFullPath()));

                        }
                        wf.LogPrint("Excelデータを出力しています。");
                        wb.Close();
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            return true;
        }

        public static bool ExportShinkyu(int cym, string kuName, string fileName, WaitForm wf)
        {
            var imageDir = System.IO.Path.GetDirectoryName(fileName) + "\\img";
            System.IO.Directory.CreateDirectory(imageDir);
            imageDir += "\\";

            wf.LogPrint("対象情報を検索中です。");
            var sl = Scan.GetScanListYM(cym);
            sl = sl.FindAll(s => s.Note2 != string.Empty && kuName == "国保連" ? !s.Note1.Contains("区") : s.Note1.Contains(kuName));

            var apps = new List<App>();
            sl.ForEach(s => apps.AddRange(App.GetAppsSID(s.SID)));
            apps.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            var receiptApps = apps.FindAll(a => a.MediYear > 0);
            if (receiptApps.Count == 0)
            {
                wf.LogPrint("対象となるデータはありませんでした。");
                return true;
            }
            wf.LogPrint(apps.Count.ToString() + "件のデータを確認しました。");

            //施術機関、被保番順にソート
            receiptApps.Sort((x, y) =>
            {
                if (x.DrNum.Contains("*") && y.DrNum.Contains("*"))
                {
                    return
                        x.ClinicName != y.ClinicName ? x.ClinicName.CompareTo(y.ClinicName) :
                        x.DrName != y.DrName ? x.DrName.CompareTo(y.DrName) :
                        x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                        x.Numbering.CompareTo(y.Numbering);
                }
                else if (!x.DrNum.Contains("*") && !y.DrNum.Contains("*"))
                {
                    return
                        x.DrNum != y.DrNum ? x.DrNum.CompareTo(y.DrNum) :
                        x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                        x.Numbering.CompareTo(y.Numbering);
                }

                //どちらか一方にだけ「*」
                return y.DrNum.CompareTo(x.DrNum);
            });


            //並び替え
            receiptApps.Sort((x, y) =>
                {
                    if (x.DrNum.Contains("*") && y.DrNum.Contains("*"))
                    {
                        return
                            x.ClinicName != y.ClinicName ? x.ClinicName.CompareTo(y.ClinicName) :
                            x.DrName != y.DrName ? x.DrName.CompareTo(y.DrName) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }
                    else
                    {
                        return
                            x.DrNum != y.DrNum ? x.DrNum.CompareTo(y.DrNum) :
                            x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                            x.Numbering.CompareTo(y.Numbering);
                    }
                });

            //連番設定の確認
            wf.LogPrint("整理番号を確認しています。");
            if (apps.All(a => a.Numbering == string.Empty))
            {
                System.Windows.Forms.MessageBox.Show(
                    "整理番号が設定されていないデータがあります。整理番号を先に割り当てて下さい。",
                    "整理番号確認",
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Asterisk);
                return false;
            }

            wf.LogPrint("Excelデータを作成しています。");
            try
            {
                wf.SetMax(apps.Count);
                wf.InvokeValue = 0;
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                var fn = System.Windows.Forms.Application.StartupPath +
                    "\\Xlsx\\SapporoShinkyu.xlsx";

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    var wb = new XSSFWorkbook(fs);
                    var ws = wb.GetSheetAt(0);
                    var rowCount = ws.LastRowNum + 2;
                    int excelLine = 1;

                    wf.SetMax(receiptApps.Count);
                    for (int i = 0; i < receiptApps.Count; i++)
                    {
                        if (wf.Cancel)
                        {
                            if (System.Windows.Forms.MessageBox.Show("キャンセルしてもよろしいですか？",
                                "キャンセル確認",
                                System.Windows.Forms.MessageBoxButtons.YesNo,
                                System.Windows.Forms.MessageBoxIcon.Question) ==
                                System.Windows.Forms.DialogResult.Yes)
                                return false;
                        }
                        wf.InvokeValue++;

                        //次の画像情報を見て添付資料の有無を確認
                        int currentIndex = apps.FindIndex(app => app.Aid == receiptApps[i].Aid);
                        var doui = false;
                        var tenpuList = new HashSet<string>();
                        var tenpuApps = new List<App>();
                        string douiName = string.Empty, douiAdd = string.Empty, douiDate = string.Empty;
                        for (int j = currentIndex + 1; j < apps.Count; j++)
                        {
                            if (apps[j].AppType == APP_TYPE.鍼灸 || apps[j].AppType == APP_TYPE.あんま) break;

                            if (apps[j].MediYear == (int)APP_SPECIAL_CODE.同意書)
                            {
                                doui = true;
                                tenpuList.Add("同意書");
                                var nextAx = AppEx.Select(apps[j].Aid);
                                douiName = nextAx.DouiName;
                                douiAdd = nextAx.DouiAdd;
                                douiDate = DateTimeEx.GetIntJpDateWithEraNumber(nextAx.DouiDate).ToString();
                                tenpuApps.Add(apps[j]);
                            }
                            else if (apps[j].MediYear == (int)APP_SPECIAL_CODE.往療内訳)
                            {
                                tenpuList.Add("往療内訳");
                                tenpuApps.Add(apps[j]);
                            }
                            else if (apps[j].MediYear == (int)APP_SPECIAL_CODE.続紙)
                            {
                                tenpuList.Add("その他");
                                tenpuApps.Add(apps[j]);
                            }
                        }

                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
                        int month = cym % 100;
                        int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        excelLine++;
                        var a = receiptApps[i];
                        var aex = AppEx.Select(a.Aid);
                        var row = ws.GetRow(excelLine);
                        row.GetCell(1).SetCellValue(a.AppType == APP_TYPE.鍼灸 ? "1" : a.AppType == APP_TYPE.あんま ? "2" : "");
                        row.GetCell(2).SetCellValue(a.Numbering);
                        row.GetCell(3).SetCellValue("4" + a.MediYear.ToString("00") + a.MediMonth.ToString("00"));
                        row.GetCell(4).SetCellValue("4" + jYear.ToString("00") + month.ToString("00"));
                        row.GetCell(5).SetCellValue(a.HihoNum);
                        row.GetCell(6).SetCellValue(a.PersonName);
                        row.GetCell(7).SetCellValue(a.Sex.ToString());
                        row.GetCell(8).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString());
                        row.GetCell(9).SetCellValue(a.Ratio == 0 ? "" : a.Ratio.ToString());
                        row.GetCell(10).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(a.FushoFirstDate1).ToString());
                        row.GetCell(11).SetCellValue(a.CountedDays.ToString());
                        row.GetCell(12).SetCellValue(a.FushoName1);
                        row.GetCell(13).SetCellValue(a.FushoDate1.IsNullDate() ? aex.FushoDate : DateTimeEx.GetIntJpDateWithEraNumber(a.FushoDate1).ToString());
                        row.GetCell(14).SetCellValue(a.DrNum.Contains("*") ? "999999999" : a.DrNum);
                        row.GetCell(15).SetCellValue(a.ClinicName);
                        row.GetCell(16).SetCellValue(a.DrName);
                        row.GetCell(17).SetCellValue(a.VisitFee.ToString());
                        row.GetCell(18).SetCellValue(a.VisitAdd.ToString());
                        row.GetCell(19).SetCellValue(a.Total.ToString());
                        row.GetCell(20).SetCellValue(a.Partial != 0 ? a.Partial.ToString() :
                            a.Charge != 0 ? (a.Total - a.Charge).ToString() :
                            a.Ratio != 0 ? (a.Total - (a.Total * a.Ratio / 10)).ToString() : "");
                        row.GetCell(21).SetCellValue(a.Charge != 0 ? a.Charge.ToString() :
                            a.Ratio != 0 ? (a.Total * a.Ratio / 10).ToString() : "");
                        row.GetCell(22).SetCellValue(aex.SejyutuDayList.Replace(" ", ","));
                        row.GetCell(23).SetCellValue(aex.OryoDayList.Replace(" ", ","));
                        row.GetCell(24).SetCellValue(aex.OryoDayList == string.Empty ? 0 : aex.OryoDayList.Split(' ').Length);
                        row.GetCell(25).SetCellValue(aex.TekiouUmu ? "1" : "2");
                        row.GetCell(26).SetCellValue(string.Join(",", tenpuList));
                        row.GetCell(27).SetCellValue(aex.DairiAdd);
                        row.GetCell(28).SetCellValue(aex.DairiName);
                        row.GetCell(29).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(aex.ShomeiDate));
                        row.GetCell(30).SetCellValue(aex.DrAdd);
                        row.GetCell(31).SetCellValue(a.DrName);
                        row.GetCell(32).SetCellValue(DateTimeEx.GetIntJpDateWithEraNumber(aex.IninDate));
                        row.GetCell(33).SetCellValue(a.HihoAdd);
                        row.GetCell(34).SetCellValue(a.HihoName);
                        row.GetCell(35).SetCellValue(doui ? douiName : "");
                        row.GetCell(36).SetCellValue(doui ? douiAdd : "");
                        row.GetCell(37).SetCellValue(doui ? douiDate : "");
                        row.GetCell(38).SetCellValue(a.FushoName1);

                        if (tenpuApps.Count == 0)
                        {
                            var sorce = a.GetImageFullPath(DB.GetMainDBName());
                            var dest = imageDir + a.Numbering + ".tiff";
                            System.IO.File.Copy(sorce, dest);
                        }
                        else
                        {
                            var files = new List<string>();
                            files.Add(a.GetImageFullPath(DB.GetMainDBName()));
                            tenpuApps.ForEach(ax => files.Add(ax.GetImageFullPath(DB.GetMainDBName())));
                            var dest = imageDir + a.Numbering + ".tiff";
                            ImageUtility.Save(files, dest);
                        }
                    }

                    wf.LogPrint("Excelデータを出力しています。");
                    wb.Close();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                return false;
            }
            return true;
        }

        /// <summary>
        /// 診療日、および往診日の矛盾をチェックします
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="fileName"></param>
        public static void DaysCheck(int cym, string fileName)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("対象情報を検索中です。");
                var sl = Scan.GetScanListYM(cym);

                var apps = new List<App>();
                sl.ForEach(s => apps.AddRange(App.GetAppsSID(s.SID)));
                apps = apps.FindAll(a => a.MediYear > 0);
                if (apps.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show("対象となるデータはありません。");
                    return;
                }
                wf.LogPrint(apps.Count.ToString() + "件のデータを確認しました。");

                wf.InvokeValue = 0;
                wf.SetMax(apps.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    for (int i = 0; i < apps.Count; i++)
                    {
                        wf.InvokeValue++;
                        var a = apps[i];
                        var aex = AppEx.Select(a.Aid);

                        //施術日チェック
                        //20190816121026 furukawa st ////////////////////////
                        //月がないためGetAdYearMonthFromJyymmに変更
                        
                                //int aAD = DateTimeEx.GetAdYearFromEraYear(4, a.MediYear);

                        int aAD = DateTimeEx.GetAdYearFromHs(int.Parse(a.MediYear.ToString("00") + a.MediMonth.ToString("00")));
                        //20190816121026 furukawa ed ////////////////////////



                        var lastDays = new List<int>();
                        int ls = 0;
                        bool error = false;
                        for (int d = 0; d < lastDays.Count; i++)
                        {
                            if (lastDays[d] < ls) error = true;
                            ls = lastDays[d];
                        }

                        Array.ForEach(aex.SejyutuDayList.Split(' '), s => lastDays.Add(int.Parse(s)));
                        int lastDay = lastDays.Max();
                        if (lastDay == 0) throw new Exception("施術日一覧が読み取れませんでした。AID:" + a.Aid.ToString());
                        if (!DateTimeEx.IsDate(aAD, a.MediMonth, lastDay) || error)
                        {
                            sw.WriteLine(a.Aid.ToString() + "," + a.GroupID.ToString() + ",診療日," + aex.SejyutuDayList);
                        }

                        //往療日チェック
                        if (aex.OryoDayList == string.Empty) continue;
                        lastDays.Clear();
                        ls = 0;
                        error = false;
                        for (int d = 0; d < lastDays.Count; i++)
                        {
                            if (lastDays[d] < ls) error = true;
                            ls = lastDays[d];
                        }

                        Array.ForEach(aex.OryoDayList.Split(' '), s => lastDays.Add(int.Parse(s)));
                        int oLastDay = lastDays.Max();
                        if (oLastDay == 0) throw new Exception("施術日一覧が読み取れませんでした。AID:" + a.Aid.ToString());
                        if (!DateTimeEx.IsDate(aAD, a.MediMonth, oLastDay) || error)
                        {
                            sw.WriteLine(a.Aid.ToString() + "," + a.GroupID.ToString() + ",往療日," + aex.SejyutuDayList);
                        }
                    }
                }
            }
        }
    }
}
