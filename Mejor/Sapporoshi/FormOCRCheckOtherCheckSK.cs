﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;
using System.Drawing.Imaging;

namespace Mejor.Sapporoshi
{
    public partial class FormOCRCheckOtherCheckSK : Form
    {
        private Dictionary<int, App> dic;
        private BindingSource bs = new BindingSource();

        //private ScanGroup sg;
        int currentAid = -1;
        private bool firstTime = true;

        // --------------------------------------------------------------------------------
        //
        // クラス定義部
        //
        // --------------------------------------------------------------------------------

        public enum a2tp { 未チェック = 0, First = 1, Other = 2 };

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point headerPos = new Point(0, 50);
        Point iryoPos = new Point(0, 150);
        Point costPos = new Point(0, 300);
        Point shomeiPos = new Point(0, 800);
        Point drNumPos = new Point(0, 900);

        Control[] headerControls, iryoControls, costControls, shomeiControls;
        
        // --------------------------------------------------------------------------------
        //
        // メイン部
        //
        // --------------------------------------------------------------------------------
        public FormOCRCheckOtherCheckSK(int cy, int cm)
        {
            InitializeComponent();

            headerControls = new Control[] { textBoxType, textBoxType2, textBoxY, textBoxY2, 
                textBoxM, textBoxM2, textBoxHnum, textBoxHnum2, textBoxRatio, textBoxRatio2,
                textBoxBirthday, textBoxBirthday2, textBoxBY, textBoxBY2, textBoxBM, 
                textBoxBM2, textBoxBD, textBoxBD2, textBoxSex, textBoxSex2, textBoxName, 
                textBoxName2,};

            iryoControls = new Control[] { textBoxShokenY, textBoxShokenY2, textBoxShokenM,
                textBoxShokenM2, textBoxShokenD, textBoxShokenD2, textBoxDays, 
                textBoxFusho, textBoxFusho2, textBoxFushoY, textBoxFushoY2, 
                textBoxFushoM, textBoxFushoM2, textBoxFushoD, textBoxFushoD2,
                textBoxFushoFree, textBoxFushoFree2, };

            costControls = new Control[] { textBoxOryo, textBoxOryo2, textBoxKasan, textBoxKasan2, 
                textBoxTotal, textBoxTotal2, textBoxCharge, textBoxCharge2, textBoxTekiou,
                textBoxTekiou2, textBoxShinryoDays, textBoxShinryoDays2, textBoxOryoDays,
                textBoxOryoDays2,};

            shomeiControls = new Control[] { textBoxDairiAdd, textBoxDairiAdd2, textBoxDairiName, 
                textBoxDairiName2, textBoxShomeiY, textBoxShomeiY2, textBoxShomeiM, 
                textBoxShomeiM2, textBoxShomeiD, textBoxShomeiD2,textBoxHosAdd,
                textBoxHosAdd2, textBoxHosName, textBoxHosName2, textBoxDrName,
                textBoxDrName2, textBoxIninY, textBoxIninY2, textBoxIninM,
                textBoxIninM2, textBoxIninD, textBoxIninD2, textBoxAdd, textBoxAdd2,
                textBoxFname, textBoxFname2, };


            panelDoui.Visible = false;
            panelReceipt.Visible = false;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += textBoxs_Enter;
                        item.Leave += textBoxs_Leave;
                    }
                    func(item);
                }
            });
            func(panelControls);


            //特殊検索
            var where = "WHERE a.achargeyear=" + cy.ToString() + " " +
                "AND a.achargemonth=" + cm.ToString() + " " +
                //"AND EXISTS(SELECT b.aid FROM application AS b WHERE b.ayear>0 AND b.aid BETWEEN a.aid-4 AND a.aid) " +
                "AND a.ayear = -4";
            var list = App.GetAppsWithWhere(where);
            labelSeikyu.Text = cy.ToString("00") + "年" + cm.ToString("00") + "月請求分";
            labelNote1.Text = "不要画像チェック:" + list.Count.ToString() +"件";
            
            //ベリファイ入力時は申請書以外を最初から除外
            if (!firstTime) list = list.FindAll(a => a.MediYear > 0);

            dic = new Dictionary<int, App>();
            foreach (var item in list) dic.Add(item.Aid, item);
            bs.DataSource = list;
            dataGridViewPlist.DataSource = bs;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = false;

            this.Text += " - Insurer: その他続紙チェック" ;
            if (!firstTime) this.Text += "ベリファイ入力モード";
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void regist()
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (!appDBupdate(ri)) return;

            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri + 1];
        }

        //Nextボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        //キー入力監視
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
        }

        //EnterキーでTABキーの動作をさせる
        private void FormOCRCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        // --------------------------------------------------------------------------------
        //
        // 入力チェック部(文字種チェック→値チェック→一回目のみOCR結果との比較)
        //
        // --------------------------------------------------------------------------------
        private bool checkDoui(ref App app, out AppEx appex)
        {
            bool err = false;
            appex = new AppEx(app.Aid);

            var setError = new Action<TextBox>(t =>
                {
                    t.BackColor = Color.Pink;
                    err = true;
                });

            var setChecked = new Action<TextBox>(t =>
                {
                    if (!t.Enabled) return;
                    t.BackColor = SystemColors.Info;
                });

            //同意者氏名
            if (string.IsNullOrWhiteSpace(textBoxDouiName.Text.Trim()))
                setError(textBoxDouiName);
            else
                setChecked(textBoxDouiName);

            //同意者住所
            if (string.IsNullOrWhiteSpace(textBoxDouiAdd.Text.Trim()))
                setError(textBoxDouiAdd);
            else
                setChecked(textBoxDouiAdd);

            //同意年月日
            var DouiDT = DateTime.MinValue;
            int dY;
            if (!int.TryParse(textBoxDouiY.Text, out dY)) setError(textBoxDouiY);
            else if (dY < 1 || dY > 65) setError(textBoxDouiY);
            else setChecked(textBoxDouiY);

            int dM;
            if (!int.TryParse(textBoxDouiM.Text, out dM)) setError(textBoxDouiM);
            else if (dM < 1 || dM > 12) setError(textBoxDouiM);
            else setChecked(textBoxDouiM);

            int dD;
            if (!int.TryParse(textBoxDouiD.Text, out dD)) setError(textBoxDouiD);
            else if (dD < 1 || dD > 31) setError(textBoxDouiD);
            else setChecked(textBoxDouiD);

            int y = DateTimeEx.GetAdYear("平成" + textBoxDouiY.Text);
            if (!DateTimeEx.IsDate(y, dM, dD))
            {
                setError(textBoxDouiY);
                setError(textBoxDouiM);
                setError(textBoxDouiD);
            }
            else
            {
                DouiDT = new DateTime(y, dM, dD);
            }

            //同意負傷名
            if (string.IsNullOrWhiteSpace(textBoxDouiFusho.Text.Trim()))
                setError(textBoxDouiFusho);
            else
                setChecked(textBoxDouiFusho);

            if (err) return false;

            //値記録
            appex.DouiName = textBoxDouiName.Text.Trim();
            appex.DouiAdd = textBoxDouiAdd.Text.Trim();
            appex.DouiDate = DouiDT;
            appex.DouiFusho = textBoxDouiFusho.Text.Trim();
            return true;
        }

        // --------------------------------------------------------------------------------
        //
        // App操作部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 現在の行のAppを表示します
        /// </summary>
        private void currentRowAppRead()
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri < 0) return;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            //次のAppを表示
            displayDataReflesh(r);

            //textBoxYにフォーカスを移動
            focusBack(false);
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(int rowIndex)
        {
            if (rowIndex == -1) return false;
            var aid = (int)dataGridViewPlist["Aid", rowIndex].Value;

            if (currentAid != aid)
            {
                MessageBox.Show("更新しようとしているデータと現在表示中のデータが一致しません。"+
                    "ご迷惑をおかけしますが、システムエラーを回避するため、この画面を閉じます。"+
                    "再度このグループを開いて作業を行ってください。", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return false;
            }

            App app = null;
            AppEx appEx = null;

            if (textBoxType.Text == "**")
            {
                //同意書
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.同意書;

                //データチェック
                if (!checkDoui(ref app, out appEx))
                {
                    //データベースから読み直し
                    dic[aid] = App.GetApp(aid);
                    focusBack(true);
                    return false;
                }
            }
            else if (textBoxType.Text == "//")
            {
                //往療内訳
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.往療内訳;
            }
            else if (textBoxType.Text == "--")
            {
                //続紙の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
            }
            else if (textBoxType.Text == "++")
            {
                //白バッジ、その他の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
            }
            else
            {
                MessageBox.Show("このモードでは申請書入力は受け付けません。いったん画面を閉じます。");
                this.Close();
                return false;
            }

            //データベースへ反映
            using (var tran = DB.Main.CreateTransaction())
            {
                if (app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.SecondInput, tran) &&
                    (appEx == null || appEx.Upsert(tran)))
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void displayDataReflesh(App app)
        {
            currentAid = app.Aid;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Text = "";
                        item.BackColor = SystemColors.Info;
                    }
                    func(item);
                }
            });
            func(panelControls);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(app.Ufirst) +
                "  2:" + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (!app.StatusFlagCheck(StatusFlag.入力済))
            {
                var ocr = app.OcrData.Split(',');
                //一度もチェックしていない画像
                if (ocr.Length > 40 && ocr[40] != null)
                {
                    var f = Fusho.Change(ocr[40]);
                    if (Fusho.BasicWordsCheck(f)) textBoxFusho.Text = f;
                }
            }
            else
            {
                //既にチェック済みの画像はデータベースからデータ表示
                displayDataRefleshFromRcd(app);
            }

            //画像の表示
            setImage(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                //メイン画像の表示
                var img = System.Drawing.Image.FromFile(fn);

                labelImageName.Text = fn + " )";

                //通常表示
                float ratio2 = 0.4f;// 0.3f;
                var act = new Action<PictureBox>((pb) =>
                {
                    pb.Size = new Size((int)(img.Width * ratio2), (int)(img.Height * ratio2));
                    pb.SizeMode = PictureBoxSizeMode.Zoom;
                    pb.Image = img;
                });
                act(pictureBoxIN);
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="a"></param>
        private void displayDataRefleshFromRcd(App a)
        {
            //OCRチェックが済んだ画像の場合
            if(a.MediYear ==(int)APP_SPECIAL_CODE.同意書)
            {
                textBoxType.Text = "**";
                var ax = AppEx.Select(a.Aid);
                textBoxDouiName.Text = ax.DouiName;
                textBoxDouiAdd.Text = ax.DouiAdd;
                if (ax.DouiDate != DateTime.MinValue)
                {
                    textBoxDouiY.Text = DateTimeEx.GetJpYear(ax.DouiDate).ToString();
                    textBoxDouiM.Text = ax.DouiDate.Month.ToString();
                    textBoxDouiD.Text = ax.DouiDate.Day.ToString();
                }
                textBoxDouiFusho.Text = ax.DouiFusho;
            }
            else if(a.MediYear==(int)APP_SPECIAL_CODE.往療内訳)
            {
                textBoxType.Text = "//";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                textBoxType.Text = "--";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                textBoxType.Text = "++";
            }
            else
            {
                var ax = AppEx.Select(a.Aid);
                textBoxType.Text = a.AppType == APP_TYPE.鍼灸 ? "7" : a.AppType == APP_TYPE.あんま ? "8" : "";
                textBoxY.Text = a.MediYear.ToString();
                textBoxM.Text = a.MediMonth.ToString();
                textBoxDrCode.Text = a.DrNum;
                textBoxHnum.Text = a.HihoNum;
                textBoxRatio.Text = a.Ratio == 0 ? string.Empty : a.Ratio.ToString();
                textBoxBirthday.Text = DateTimeEx.GetEraNumber(a.Birthday).ToString();
                textBoxBY.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM.Text = a.Birthday.Month.ToString();
                textBoxBD.Text = a.Birthday.Day.ToString();
                textBoxSex.Text = a.Sex.ToString();
                textBoxName.Text = a.PersonName;
                textBoxShokenY.Text = DateTimeEx.GetJpYear(a.FushoFirstDate1).ToString();
                textBoxShokenM.Text = a.FushoFirstDate1.Month.ToString();
                textBoxShokenD.Text = a.FushoFirstDate1.Day.ToString();
                textBoxDays.Text = a.CountedDays.ToString();
                textBoxFusho.Text = a.FushoName1;
                if (a.FushoDate1 != DateTime.MinValue)
                {
                    textBoxFushoY.Text = DateTimeEx.GetJpYear(a.FushoDate1).ToString();
                    textBoxFushoM.Text = a.FushoDate1.Month.ToString();
                    textBoxFushoD.Text = a.FushoDate1.Day.ToString();
                }
                textBoxFushoFree.Text = ax.FushoDate;
                textBoxOryo.Text = a.VisitFee == 0 ? string.Empty : a.VisitFee.ToString();
                textBoxKasan.Text = a.VisitAdd == 0  ? string.Empty : a.VisitAdd.ToString();
                textBoxTotal.Text = a.Total.ToString();
                textBoxCharge.Text = a.Charge == 0 ? string.Empty : a.Charge.ToString();
                textBoxFutan.Text = a.Partial == 0 ? string.Empty : a.Partial.ToString();
                textBoxTekiou.Text = ax.TekiouUmu ? "1" : "2";
                textBoxShinryoDays.Text = ax.SejyutuDayList == string.Empty ? "*" : ax.SejyutuDayList;
                textBoxOryoDays.Text = ax.OryoDayList;
                textBoxDairiAdd.Text = ax.DairiAdd;
                textBoxDairiName.Text = ax.DairiName;
                if (ax.ShomeiDate != DateTime.MinValue)
                {
                    textBoxShomeiY.Text = DateTimeEx.GetJpYear(ax.ShomeiDate).ToString();
                    textBoxShomeiM.Text = ax.ShomeiDate.Month.ToString();
                    textBoxShomeiD.Text = ax.ShomeiDate.Day.ToString();
                }
                textBoxHosAdd.Text = ax.DrAdd;
                textBoxHosName.Text = a.ClinicName;
                textBoxDrName.Text = a.DrName;
                if (ax.IninDate != DateTime.MinValue)
                {
                    textBoxIninY.Text = DateTimeEx.GetJpYear(ax.IninDate).ToString();
                    textBoxIninM.Text = ax.IninDate.Month.ToString();
                    textBoxIninD.Text = ax.IninDate.Day.ToString();
                }
                textBoxAdd.Text = a.HihoAdd;
                textBoxFname.Text = a.HihoName;
            }
        }

        private int getBuiCount(App r)
        {
            int c = 1;
            if (r.FushoName2 != string.Empty) c++;
            if (r.FushoName3 != string.Empty) c++;
            if (r.FushoName4 != string.Empty) c++;
            if (r.FushoName5 != string.Empty) c++;
            return c;
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxType_TextChanged(object sender, EventArgs e)
        {
            if (textBoxType.Text == "**")
            {
                panelDoui.Visible = true;
                panelReceipt.Visible = false;
            }
            else if (textBoxType.Text == "8" || textBoxType.Text == "9")
            {
                panelDoui.Visible = false;
                panelReceipt.Visible = true;
            }
            else
            {
                panelDoui.Visible = false;
                panelReceipt.Visible = false;
            }
        }
        
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            var app = imageDispose();
            string fn = app.GetImageFullPath();
            ImageUtility.ImageRotate(fn, true);

            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            var app = imageDispose();
            string fn = app.GetImageFullPath();

            ImageUtility.ImageRotate(fn, false);

            setImage(app);
        }



        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var apl = imageDispose();
            string fn = apl.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(apl);
        }

        private App imageDispose()
        {
            if (pictureBoxIN.Image != null) pictureBoxIN.Image.Dispose();

            var ri = dataGridViewPlist.CurrentCell.RowIndex;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            return r;
        }

        private void textBoxs_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == Color.LightCyan) t.BackColor = SystemColors.Info;
        }

        private void textBoxs_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            //コントロール部分のスクロール
            int y = -panelControlScroll.AutoScrollPosition.Y;
            if (panelControlScroll.Height + y - 160 < t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, panelControlScroll.Height);
            }
            else if (y + 100 > t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, 0);
            }

            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;


            if (headerControls.Contains(t)) panelIN.AutoScrollPosition = headerPos;
            else if (headerControls.Contains(t)) panelIN.AutoScrollPosition = headerPos;
            else if (iryoControls.Contains(t)) panelIN.AutoScrollPosition = iryoPos;
            else if (costControls.Contains(t)) panelIN.AutoScrollPosition = costPos;
            else if (shomeiControls.Contains(t)) panelIN.AutoScrollPosition = shomeiPos;
            else if (textBoxDrCode == t) panelIN.AutoScrollPosition = drNumPos;

            t.SelectAll();
        }


        private void focusBack(bool infoColorFocus)
        {
            Control[] controls = { textBoxType, textBoxY, textBoxM, textBoxDrCode,
                    textBoxHnum, textBoxRatio, textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, 
                    textBoxSex, textBoxName, textBoxShokenY, textBoxShokenM, textBoxShokenD, 
                    textBoxDays, textBoxFusho, textBoxFushoY, textBoxFushoM,textBoxFushoD,
                    textBoxFushoFree, textBoxOryo,textBoxKasan, textBoxTotal, textBoxCharge,
                    textBoxTekiou, textBoxShinryoDays, textBoxOryoDays,
                    textBoxDairiAdd,textBoxDairiName,textBoxShomeiY,textBoxShomeiM,textBoxShomeiD,
                    textBoxHosAdd, textBoxHosName,textBoxDrName,
                    textBoxIninY,textBoxIninM, textBoxIninD, textBoxAdd, textBoxFname,
                    textBoxDouiName, textBoxDouiAdd, 
                    textBoxDouiY,textBoxDouiM,textBoxDouiD,textBoxDouiFusho};

            if (infoColorFocus)
            {
                foreach (var item in controls)
                {
                    if (item.BackColor != SystemColors.Info)
                    {
                        item.Focus();
                        return;
                    }
                }
            }

            foreach (var item in controls)
            {
                if (item.Enabled)
                {
                    item.Focus();
                    if (item is TextBox) ((TextBox)item).SelectAll();
                    return;
                }
            }

            buttonRegist.Focus();
        }

        private void panelIN_Scroll(object sender, ScrollEventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = new Point(-panelIN.AutoScrollPosition.X, -panelIN.AutoScrollPosition.Y);

            if (headerControls.Contains(t)) headerPos = pos;
            else if (headerControls.Contains(t)) headerPos = pos;
            else if (iryoControls.Contains(t)) iryoPos = pos;
            else if (costControls.Contains(t)) costPos = pos;
            else if (shomeiControls.Contains(t)) shomeiPos = pos;
            else if (textBoxDrCode == t) drNumPos = pos;
        }

        int prevIndex = -1;
        private void dataGridViewPlist_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount != 0)
            {
                dataGridViewPlist.CurrentCell = null;
                dataGridViewPlist.CurrentCell = dataGridViewPlist["Aid", 0];
            }
        }

        private void dataGridViewPlist_CurrentCellChanged_1(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void textBoxDrCode_Leave(object sender, EventArgs e)
        {
            //ベリファイ時は引っ張らない
            if (!firstTime) return;

            var code = textBoxDrCode.Text.Trim();
            var d = Doctor.GetDoctor(code);
            if (d == null || !d.Verified) return;

            //if (textBoxDrName.Text == string.Empty) textBoxDrName.Text = d.DrName;
            if (textBoxHosName.Text == string.Empty) textBoxHosName.Text = d.HosName;
            if (textBoxHosAdd.Text == string.Empty) textBoxHosAdd.Text = d.DrAdd;
            if (textBoxDairiAdd.Text == string.Empty) textBoxDairiAdd.Text = d.DairiAdd;
            if (textBoxDairiName.Text == string.Empty) textBoxDairiName.Text = d.DairiName;
        }

        private void textBoxHnum_Leave(object sender, EventArgs e)
        {
            //ベリファイ時は引っ張らない
            if (!firstTime) return;

            if (textBoxFname.Text != string.Empty) return;
            var hnum = textBoxHnum.Text.Trim();

            var fn = Person.GetVerifiedFamilyNameAndAdd(hnum);
            if (fn == null) return;
            textBoxFname.Text = fn[0];
            textBoxAdd.Text = fn[1];
        }

        private void textBoxBD_Leave(object sender, EventArgs e)
        {
            if (textBoxName.Text != string.Empty) return;
            var hnum = textBoxHnum.Text.Trim();

            int j, y, m, d;
            DateTime dt;
            if (!int.TryParse(textBoxBirthday.Text, out j)) return;
            if (!int.TryParse(textBoxBY.Text, out y)) return;
            if (!int.TryParse(textBoxBM.Text, out m)) return;
            if (!int.TryParse(textBoxBD.Text, out d)) return;

            var dts = j == 1 ? "明治" : j == 2 ? "大正" : j == 3 ? "昭和" : j == 4 ? "平成" : "";
            dts += textBoxBY.Text + "/" + textBoxBM.Text + "/" + textBoxBD.Text;
            DateTimeEx.TryGetDateTimeFromJdate(dts, out dt);

            var p = Person.GetPerson(hnum, dt);
            if (p == null || !p.Verified) return;
            textBoxName.Text = p.Pname;
            //textBoxSex.Text = p.Psex.ToString();
            if (ActiveControl == textBoxName) textBoxShokenY.Focus();
        }

        private void textBoxKanaToHalf_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            t.Text = Utility.ToHalfWithoutKatakana(t.Text);
        }

        private void textBoxShinryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.Text == "*") return;

            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }

        private void textBoxOryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.Text.Trim() == "----")
            {
                t.Text = textBoxShinryoDays.Text;
                return;
            }

            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }
    }
}
