﻿namespace Mejor.Sapporoshi
{
    partial class FormOCRCheckOtherCheckSK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBoxIN = new System.Windows.Forms.PictureBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.textBoxHnum = new System.Windows.Forms.TextBox();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.panelIN = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.textBoxDouiY = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxDouiM = new System.Windows.Forms.TextBox();
            this.textBoxDouiD = new System.Windows.Forms.TextBox();
            this.textBoxDouiY2 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBoxDouiM2 = new System.Windows.Forms.TextBox();
            this.textBoxDouiD2 = new System.Windows.Forms.TextBox();
            this.textBoxDouiName2 = new System.Windows.Forms.TextBox();
            this.textBoxDouiFusho2 = new System.Windows.Forms.TextBox();
            this.textBoxDouiAdd2 = new System.Windows.Forms.TextBox();
            this.textBoxDouiName = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxDouiFusho = new System.Windows.Forms.TextBox();
            this.textBoxDouiAdd = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxIninY = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxIninM = new System.Windows.Forms.TextBox();
            this.textBoxIninD = new System.Windows.Forms.TextBox();
            this.textBoxIninY2 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.textBoxIninM2 = new System.Windows.Forms.TextBox();
            this.textBoxIninD2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxShomeiY = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxShomeiM = new System.Windows.Forms.TextBox();
            this.textBoxShomeiD = new System.Windows.Forms.TextBox();
            this.textBoxShomeiY2 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxShomeiM2 = new System.Windows.Forms.TextBox();
            this.textBoxShomeiD2 = new System.Windows.Forms.TextBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.textBoxDrCode2 = new System.Windows.Forms.TextBox();
            this.textBoxDairiName2 = new System.Windows.Forms.TextBox();
            this.textBoxDrName2 = new System.Windows.Forms.TextBox();
            this.textBoxDairiAdd2 = new System.Windows.Forms.TextBox();
            this.textBoxHosAdd2 = new System.Windows.Forms.TextBox();
            this.textBoxHosName2 = new System.Windows.Forms.TextBox();
            this.textBoxDairiName = new System.Windows.Forms.TextBox();
            this.textBoxDrCode = new System.Windows.Forms.TextBox();
            this.textBoxDrName = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxDairiAdd = new System.Windows.Forms.TextBox();
            this.textBoxHosAdd = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxHosName = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelControlScroll = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.panelReceipt = new System.Windows.Forms.Panel();
            this.textBoxTekiou2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxBirthday2 = new System.Windows.Forms.TextBox();
            this.textBoxSex2 = new System.Windows.Forms.TextBox();
            this.textBoxBY2 = new System.Windows.Forms.TextBox();
            this.textBoxY2 = new System.Windows.Forms.TextBox();
            this.textBoxBM2 = new System.Windows.Forms.TextBox();
            this.textBoxM2 = new System.Windows.Forms.TextBox();
            this.textBoxBD2 = new System.Windows.Forms.TextBox();
            this.textBoxHnum2 = new System.Windows.Forms.TextBox();
            this.textBoxRatio2 = new System.Windows.Forms.TextBox();
            this.textBoxFushoFree2 = new System.Windows.Forms.TextBox();
            this.textBoxDays = new System.Windows.Forms.TextBox();
            this.textBoxFushoFree = new System.Windows.Forms.TextBox();
            this.textBoxDays2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.textBoxOryoDays2 = new System.Windows.Forms.TextBox();
            this.textBoxAdd2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxFname2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFname = new System.Windows.Forms.TextBox();
            this.textBoxBY = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxSex = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxTekiou = new System.Windows.Forms.TextBox();
            this.textBoxShinryoDays2 = new System.Windows.Forms.TextBox();
            this.textBoxOryoDays = new System.Windows.Forms.TextBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.textBoxFushoY2 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBoxFutan2 = new System.Windows.Forms.TextBox();
            this.textBoxCharge2 = new System.Windows.Forms.TextBox();
            this.textBoxBM = new System.Windows.Forms.TextBox();
            this.textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.textBoxFushoM2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxFushoD2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxKasan2 = new System.Windows.Forms.TextBox();
            this.textBoxBD = new System.Windows.Forms.TextBox();
            this.textBoxOryo2 = new System.Windows.Forms.TextBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxFusho2 = new System.Windows.Forms.TextBox();
            this.textBoxShokenY2 = new System.Windows.Forms.TextBox();
            this.textBoxRatio = new System.Windows.Forms.TextBox();
            this.textBoxName2 = new System.Windows.Forms.TextBox();
            this.textBoxShokenM2 = new System.Windows.Forms.TextBox();
            this.textBoxShokenD2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxShokenD = new System.Windows.Forms.TextBox();
            this.textBoxAdd = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxFusho = new System.Windows.Forms.TextBox();
            this.textBoxShokenM = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxShokenY = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxShinryoDays = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxFushoY = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxOryo = new System.Windows.Forms.TextBox();
            this.textBoxFutan = new System.Windows.Forms.TextBox();
            this.textBoxFushoD = new System.Windows.Forms.TextBox();
            this.textBoxCharge = new System.Windows.Forms.TextBox();
            this.textBoxFushoM = new System.Windows.Forms.TextBox();
            this.textBoxKasan = new System.Windows.Forms.TextBox();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.textBoxType2 = new System.Windows.Forms.TextBox();
            this.textBoxType = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.panelDoui = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.panelUP = new System.Windows.Forms.Panel();
            this.labelNote1 = new System.Windows.Forms.Label();
            this.labelGroupID = new System.Windows.Forms.Label();
            this.labelScanID = new System.Windows.Forms.Label();
            this.labelSeikyu = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIN)).BeginInit();
            this.panelIN.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelControlScroll.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panelReceipt.SuspendLayout();
            this.panelDoui.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelUP.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxIN
            // 
            this.pictureBoxIN.Location = new System.Drawing.Point(-2, 0);
            this.pictureBoxIN.Name = "pictureBoxIN";
            this.pictureBoxIN.Size = new System.Drawing.Size(721, 737);
            this.pictureBoxIN.TabIndex = 2;
            this.pictureBoxIN.TabStop = false;
            // 
            // textBoxY
            // 
            this.textBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxY.Location = new System.Drawing.Point(43, 1);
            this.textBoxY.MaxLength = 2;
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(32, 26);
            this.textBoxY.TabIndex = 5;
            this.textBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxM
            // 
            this.textBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxM.Location = new System.Drawing.Point(93, 1);
            this.textBoxM.MaxLength = 2;
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(32, 26);
            this.textBoxM.TabIndex = 8;
            this.textBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHnum
            // 
            this.textBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHnum.Location = new System.Drawing.Point(228, 56);
            this.textBoxHnum.Name = "textBoxHnum";
            this.textBoxHnum.Size = new System.Drawing.Size(85, 26);
            this.textBoxHnum.TabIndex = 15;
            this.textBoxHnum.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxHnum.Leave += new System.EventHandler(this.textBoxHnum_Leave);
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(227, 3);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(111, 23);
            this.buttonRegist.TabIndex = 1;
            this.buttonRegist.Text = "登録 (PageUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(76, 15);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 7;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(126, 15);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 10;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(199, 58);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(29, 24);
            this.labelHnum.TabIndex = 14;
            this.labelHnum.Text = "被保\r\n番";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelIN
            // 
            this.panelIN.AutoScroll = true;
            this.panelIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelIN.Controls.Add(this.buttonImageChange);
            this.panelIN.Controls.Add(this.labelImageName);
            this.panelIN.Controls.Add(this.buttonImageRotateR);
            this.panelIN.Controls.Add(this.buttonImageRotateL);
            this.panelIN.Controls.Add(this.pictureBoxIN);
            this.panelIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelIN.Location = new System.Drawing.Point(202, 0);
            this.panelIN.Name = "panelIN";
            this.panelIN.Size = new System.Drawing.Size(721, 739);
            this.panelIN.TabIndex = 1;
            this.panelIN.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panelIN_Scroll);
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(75, 713);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 54;
            this.buttonImageChange.TabStop = false;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Visible = false;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(121, 724);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 55;
            this.labelImageName.Text = "ImageName";
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(38, 713);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 53;
            this.buttonImageRotateR.TabStop = false;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(2, 713);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 52;
            this.buttonImageRotateL.TabStop = false;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(14, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(29, 24);
            this.label47.TabIndex = 6;
            this.label47.Text = "同意\r\n日付";
            this.label47.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(75, 124);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(17, 12);
            this.label48.TabIndex = 9;
            this.label48.Text = "年";
            // 
            // textBoxDouiY
            // 
            this.textBoxDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiY.Location = new System.Drawing.Point(43, 110);
            this.textBoxDouiY.MaxLength = 2;
            this.textBoxDouiY.Name = "textBoxDouiY";
            this.textBoxDouiY.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiY.TabIndex = 7;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(124, 124);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(17, 12);
            this.label49.TabIndex = 12;
            this.label49.Text = "月";
            // 
            // textBoxDouiM
            // 
            this.textBoxDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiM.Location = new System.Drawing.Point(92, 110);
            this.textBoxDouiM.MaxLength = 2;
            this.textBoxDouiM.Name = "textBoxDouiM";
            this.textBoxDouiM.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiM.TabIndex = 10;
            // 
            // textBoxDouiD
            // 
            this.textBoxDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiD.Location = new System.Drawing.Point(141, 110);
            this.textBoxDouiD.MaxLength = 2;
            this.textBoxDouiD.Name = "textBoxDouiD";
            this.textBoxDouiD.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiD.TabIndex = 13;
            // 
            // textBoxDouiY2
            // 
            this.textBoxDouiY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiY2.Location = new System.Drawing.Point(43, 135);
            this.textBoxDouiY2.MaxLength = 2;
            this.textBoxDouiY2.Name = "textBoxDouiY2";
            this.textBoxDouiY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiY2.TabIndex = 8;
            this.textBoxDouiY2.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(173, 124);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(17, 12);
            this.label50.TabIndex = 15;
            this.label50.Text = "日";
            // 
            // textBoxDouiM2
            // 
            this.textBoxDouiM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiM2.Location = new System.Drawing.Point(92, 135);
            this.textBoxDouiM2.MaxLength = 2;
            this.textBoxDouiM2.Name = "textBoxDouiM2";
            this.textBoxDouiM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiM2.TabIndex = 11;
            this.textBoxDouiM2.Visible = false;
            // 
            // textBoxDouiD2
            // 
            this.textBoxDouiD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDouiD2.Location = new System.Drawing.Point(141, 135);
            this.textBoxDouiD2.MaxLength = 2;
            this.textBoxDouiD2.Name = "textBoxDouiD2";
            this.textBoxDouiD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxDouiD2.TabIndex = 14;
            this.textBoxDouiD2.Visible = false;
            // 
            // textBoxDouiName2
            // 
            this.textBoxDouiName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiName2.Location = new System.Drawing.Point(43, 27);
            this.textBoxDouiName2.Name = "textBoxDouiName2";
            this.textBoxDouiName2.Size = new System.Drawing.Size(160, 26);
            this.textBoxDouiName2.TabIndex = 2;
            this.textBoxDouiName2.Visible = false;
            this.textBoxDouiName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDouiFusho2
            // 
            this.textBoxDouiFusho2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiFusho2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiFusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiFusho2.Location = new System.Drawing.Point(43, 189);
            this.textBoxDouiFusho2.Name = "textBoxDouiFusho2";
            this.textBoxDouiFusho2.Size = new System.Drawing.Size(360, 26);
            this.textBoxDouiFusho2.TabIndex = 18;
            this.textBoxDouiFusho2.Visible = false;
            this.textBoxDouiFusho2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDouiAdd2
            // 
            this.textBoxDouiAdd2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiAdd2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiAdd2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiAdd2.Location = new System.Drawing.Point(43, 81);
            this.textBoxDouiAdd2.Name = "textBoxDouiAdd2";
            this.textBoxDouiAdd2.Size = new System.Drawing.Size(360, 26);
            this.textBoxDouiAdd2.TabIndex = 5;
            this.textBoxDouiAdd2.Visible = false;
            this.textBoxDouiAdd2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDouiName
            // 
            this.textBoxDouiName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiName.Location = new System.Drawing.Point(43, 2);
            this.textBoxDouiName.Name = "textBoxDouiName";
            this.textBoxDouiName.Size = new System.Drawing.Size(160, 26);
            this.textBoxDouiName.TabIndex = 1;
            this.textBoxDouiName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(2, 4);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 24);
            this.label52.TabIndex = 0;
            this.label52.Text = "同意\r\n医師名";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxDouiFusho
            // 
            this.textBoxDouiFusho.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiFusho.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiFusho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiFusho.Location = new System.Drawing.Point(43, 164);
            this.textBoxDouiFusho.Name = "textBoxDouiFusho";
            this.textBoxDouiFusho.Size = new System.Drawing.Size(360, 26);
            this.textBoxDouiFusho.TabIndex = 17;
            this.textBoxDouiFusho.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxDouiFusho.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBoxDouiAdd
            // 
            this.textBoxDouiAdd.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDouiAdd.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDouiAdd.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDouiAdd.Location = new System.Drawing.Point(43, 56);
            this.textBoxDouiAdd.Name = "textBoxDouiAdd";
            this.textBoxDouiAdd.Size = new System.Drawing.Size(360, 26);
            this.textBoxDouiAdd.TabIndex = 4;
            this.textBoxDouiAdd.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxDouiAdd.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(2, 164);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 24);
            this.label55.TabIndex = 16;
            this.label55.Text = "同意\r\n負傷名";
            this.label55.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(2, 56);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(41, 24);
            this.label56.TabIndex = 3;
            this.label56.Text = "同意者\r\n住所";
            this.label56.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(13, 962);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(29, 24);
            this.label39.TabIndex = 118;
            this.label39.Text = "委任\r\n日付";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(75, 974);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 12);
            this.label40.TabIndex = 121;
            this.label40.Text = "年";
            // 
            // textBoxIninY
            // 
            this.textBoxIninY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninY.Location = new System.Drawing.Point(43, 960);
            this.textBoxIninY.MaxLength = 2;
            this.textBoxIninY.Name = "textBoxIninY";
            this.textBoxIninY.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninY.TabIndex = 119;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(124, 974);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 12);
            this.label41.TabIndex = 124;
            this.label41.Text = "月";
            // 
            // textBoxIninM
            // 
            this.textBoxIninM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninM.Location = new System.Drawing.Point(92, 960);
            this.textBoxIninM.MaxLength = 2;
            this.textBoxIninM.Name = "textBoxIninM";
            this.textBoxIninM.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninM.TabIndex = 122;
            // 
            // textBoxIninD
            // 
            this.textBoxIninD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninD.Location = new System.Drawing.Point(141, 960);
            this.textBoxIninD.MaxLength = 2;
            this.textBoxIninD.Name = "textBoxIninD";
            this.textBoxIninD.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninD.TabIndex = 125;
            // 
            // textBoxIninY2
            // 
            this.textBoxIninY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninY2.Location = new System.Drawing.Point(43, 985);
            this.textBoxIninY2.MaxLength = 2;
            this.textBoxIninY2.Name = "textBoxIninY2";
            this.textBoxIninY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninY2.TabIndex = 120;
            this.textBoxIninY2.Visible = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(173, 974);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(17, 12);
            this.label42.TabIndex = 127;
            this.label42.Text = "日";
            // 
            // textBoxIninM2
            // 
            this.textBoxIninM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninM2.Location = new System.Drawing.Point(92, 985);
            this.textBoxIninM2.MaxLength = 2;
            this.textBoxIninM2.Name = "textBoxIninM2";
            this.textBoxIninM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninM2.TabIndex = 123;
            this.textBoxIninM2.Visible = false;
            // 
            // textBoxIninD2
            // 
            this.textBoxIninD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxIninD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxIninD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxIninD2.Location = new System.Drawing.Point(141, 985);
            this.textBoxIninD2.MaxLength = 2;
            this.textBoxIninD2.Name = "textBoxIninD2";
            this.textBoxIninD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxIninD2.TabIndex = 126;
            this.textBoxIninD2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 739);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 24);
            this.label6.TabIndex = 99;
            this.label6.Text = "証明\r\n日付";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(75, 751);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 12);
            this.label34.TabIndex = 102;
            this.label34.Text = "年";
            // 
            // textBoxShomeiY
            // 
            this.textBoxShomeiY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiY.Location = new System.Drawing.Point(43, 737);
            this.textBoxShomeiY.MaxLength = 2;
            this.textBoxShomeiY.Name = "textBoxShomeiY";
            this.textBoxShomeiY.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiY.TabIndex = 100;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(124, 751);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 105;
            this.label37.Text = "月";
            // 
            // textBoxShomeiM
            // 
            this.textBoxShomeiM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiM.Location = new System.Drawing.Point(92, 737);
            this.textBoxShomeiM.MaxLength = 2;
            this.textBoxShomeiM.Name = "textBoxShomeiM";
            this.textBoxShomeiM.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiM.TabIndex = 103;
            // 
            // textBoxShomeiD
            // 
            this.textBoxShomeiD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiD.Location = new System.Drawing.Point(141, 737);
            this.textBoxShomeiD.MaxLength = 2;
            this.textBoxShomeiD.Name = "textBoxShomeiD";
            this.textBoxShomeiD.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiD.TabIndex = 106;
            // 
            // textBoxShomeiY2
            // 
            this.textBoxShomeiY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiY2.Location = new System.Drawing.Point(43, 762);
            this.textBoxShomeiY2.MaxLength = 2;
            this.textBoxShomeiY2.Name = "textBoxShomeiY2";
            this.textBoxShomeiY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiY2.TabIndex = 101;
            this.textBoxShomeiY2.Visible = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(173, 751);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 108;
            this.label38.Text = "日";
            // 
            // textBoxShomeiM2
            // 
            this.textBoxShomeiM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiM2.Location = new System.Drawing.Point(92, 762);
            this.textBoxShomeiM2.MaxLength = 2;
            this.textBoxShomeiM2.Name = "textBoxShomeiM2";
            this.textBoxShomeiM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiM2.TabIndex = 104;
            this.textBoxShomeiM2.Visible = false;
            // 
            // textBoxShomeiD2
            // 
            this.textBoxShomeiD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShomeiD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShomeiD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShomeiD2.Location = new System.Drawing.Point(141, 762);
            this.textBoxShomeiD2.MaxLength = 2;
            this.textBoxShomeiD2.Name = "textBoxShomeiD2";
            this.textBoxShomeiD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShomeiD2.TabIndex = 107;
            this.textBoxShomeiD2.Visible = false;
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(2, 58);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(41, 24);
            this.labelNumbering.TabIndex = 11;
            this.labelNumbering.Text = "施術師\r\nコード";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxDrCode2
            // 
            this.textBoxDrCode2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrCode2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrCode2.Location = new System.Drawing.Point(43, 81);
            this.textBoxDrCode2.Name = "textBoxDrCode2";
            this.textBoxDrCode2.Size = new System.Drawing.Size(147, 26);
            this.textBoxDrCode2.TabIndex = 13;
            this.textBoxDrCode2.Visible = false;
            this.textBoxDrCode2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDairiName2
            // 
            this.textBoxDairiName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDairiName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDairiName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDairiName2.Location = new System.Drawing.Point(43, 706);
            this.textBoxDairiName2.Name = "textBoxDairiName2";
            this.textBoxDairiName2.Size = new System.Drawing.Size(324, 26);
            this.textBoxDairiName2.TabIndex = 98;
            this.textBoxDairiName2.Visible = false;
            this.textBoxDairiName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDrName2
            // 
            this.textBoxDrName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDrName2.Location = new System.Drawing.Point(43, 929);
            this.textBoxDrName2.Name = "textBoxDrName2";
            this.textBoxDrName2.Size = new System.Drawing.Size(160, 26);
            this.textBoxDrName2.TabIndex = 117;
            this.textBoxDrName2.Visible = false;
            this.textBoxDrName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDairiAdd2
            // 
            this.textBoxDairiAdd2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDairiAdd2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDairiAdd2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDairiAdd2.Location = new System.Drawing.Point(43, 650);
            this.textBoxDairiAdd2.Name = "textBoxDairiAdd2";
            this.textBoxDairiAdd2.Size = new System.Drawing.Size(360, 26);
            this.textBoxDairiAdd2.TabIndex = 95;
            this.textBoxDairiAdd2.Visible = false;
            this.textBoxDairiAdd2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHosAdd2
            // 
            this.textBoxHosAdd2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosAdd2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosAdd2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosAdd2.Location = new System.Drawing.Point(43, 817);
            this.textBoxHosAdd2.Name = "textBoxHosAdd2";
            this.textBoxHosAdd2.Size = new System.Drawing.Size(360, 26);
            this.textBoxHosAdd2.TabIndex = 111;
            this.textBoxHosAdd2.Visible = false;
            this.textBoxHosAdd2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHosName2
            // 
            this.textBoxHosName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosName2.Location = new System.Drawing.Point(43, 873);
            this.textBoxHosName2.Name = "textBoxHosName2";
            this.textBoxHosName2.Size = new System.Drawing.Size(238, 26);
            this.textBoxHosName2.TabIndex = 114;
            this.textBoxHosName2.Visible = false;
            this.textBoxHosName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDairiName
            // 
            this.textBoxDairiName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDairiName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDairiName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDairiName.Location = new System.Drawing.Point(43, 681);
            this.textBoxDairiName.Name = "textBoxDairiName";
            this.textBoxDairiName.Size = new System.Drawing.Size(324, 26);
            this.textBoxDairiName.TabIndex = 97;
            this.textBoxDairiName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDrCode
            // 
            this.textBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrCode.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrCode.Location = new System.Drawing.Point(43, 56);
            this.textBoxDrCode.Name = "textBoxDrCode";
            this.textBoxDrCode.Size = new System.Drawing.Size(147, 26);
            this.textBoxDrCode.TabIndex = 12;
            this.textBoxDrCode.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxDrCode.Leave += new System.EventHandler(this.textBoxDrCode_Leave);
            // 
            // textBoxDrName
            // 
            this.textBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDrName.Location = new System.Drawing.Point(43, 904);
            this.textBoxDrName.Name = "textBoxDrName";
            this.textBoxDrName.Size = new System.Drawing.Size(160, 26);
            this.textBoxDrName.TabIndex = 116;
            this.textBoxDrName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(15, 906);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 24);
            this.label31.TabIndex = 115;
            this.label31.Text = "施術\r\n師名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxDairiAdd
            // 
            this.textBoxDairiAdd.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDairiAdd.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDairiAdd.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDairiAdd.Location = new System.Drawing.Point(43, 625);
            this.textBoxDairiAdd.Name = "textBoxDairiAdd";
            this.textBoxDairiAdd.Size = new System.Drawing.Size(360, 26);
            this.textBoxDairiAdd.TabIndex = 94;
            this.textBoxDairiAdd.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxDairiAdd.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBoxHosAdd
            // 
            this.textBoxHosAdd.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosAdd.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosAdd.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosAdd.Location = new System.Drawing.Point(43, 792);
            this.textBoxHosAdd.Name = "textBoxHosAdd";
            this.textBoxHosAdd.Size = new System.Drawing.Size(360, 26);
            this.textBoxHosAdd.TabIndex = 110;
            this.textBoxHosAdd.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxHosAdd.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(2, 683);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 24);
            this.label35.TabIndex = 96;
            this.label35.Text = "代理人\r\n名";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(14, 794);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 24);
            this.label36.TabIndex = 109;
            this.label36.Text = "施術\r\n住所";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(2, 627);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 24);
            this.label32.TabIndex = 93;
            this.label32.Text = "代理人\r\n住所";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(14, 850);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 24);
            this.label33.TabIndex = 112;
            this.label33.Text = "施術\r\n所名";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxHosName
            // 
            this.textBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosName.Location = new System.Drawing.Point(43, 848);
            this.textBoxHosName.Name = "textBoxHosName";
            this.textBoxHosName.Size = new System.Drawing.Size(238, 26);
            this.textBoxHosName.TabIndex = 113;
            this.textBoxHosName.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxHosName.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(344, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "和暦";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(77, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 36);
            this.label3.TabIndex = 3;
            this.label3.Text = "鍼灸:7 あんま:8\r\n同意書:** 往療内訳://\r\nその他続紙:-- 不要:++";
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelControlScroll);
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(923, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(421, 739);
            this.panelRight.TabIndex = 2;
            // 
            // panelControlScroll
            // 
            this.panelControlScroll.AutoScroll = true;
            this.panelControlScroll.Controls.Add(this.panelControls);
            this.panelControlScroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlScroll.Location = new System.Drawing.Point(0, 0);
            this.panelControlScroll.Name = "panelControlScroll";
            this.panelControlScroll.Size = new System.Drawing.Size(421, 711);
            this.panelControlScroll.TabIndex = 0;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.panelReceipt);
            this.panelControls.Controls.Add(this.label3);
            this.panelControls.Controls.Add(this.textBoxType2);
            this.panelControls.Controls.Add(this.textBoxType);
            this.panelControls.Controls.Add(this.label43);
            this.panelControls.Controls.Add(this.panelDoui);
            this.panelControls.Location = new System.Drawing.Point(0, 0);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(404, 1186);
            this.panelControls.TabIndex = 0;
            // 
            // panelReceipt
            // 
            this.panelReceipt.Controls.Add(this.textBoxTekiou2);
            this.panelReceipt.Controls.Add(this.label9);
            this.panelReceipt.Controls.Add(this.textBoxBirthday2);
            this.panelReceipt.Controls.Add(this.textBoxSex2);
            this.panelReceipt.Controls.Add(this.textBoxBY2);
            this.panelReceipt.Controls.Add(this.textBoxY2);
            this.panelReceipt.Controls.Add(this.textBoxBM2);
            this.panelReceipt.Controls.Add(this.textBoxM2);
            this.panelReceipt.Controls.Add(this.textBoxBD2);
            this.panelReceipt.Controls.Add(this.textBoxHnum2);
            this.panelReceipt.Controls.Add(this.labelM);
            this.panelReceipt.Controls.Add(this.textBoxDrCode2);
            this.panelReceipt.Controls.Add(this.textBoxM);
            this.panelReceipt.Controls.Add(this.textBoxRatio2);
            this.panelReceipt.Controls.Add(this.labelY);
            this.panelReceipt.Controls.Add(this.textBoxY);
            this.panelReceipt.Controls.Add(this.label8);
            this.panelReceipt.Controls.Add(this.labelNumbering);
            this.panelReceipt.Controls.Add(this.textBoxIninY2);
            this.panelReceipt.Controls.Add(this.textBoxFushoFree2);
            this.panelReceipt.Controls.Add(this.textBoxIninM2);
            this.panelReceipt.Controls.Add(this.textBoxDays);
            this.panelReceipt.Controls.Add(this.textBoxIninD2);
            this.panelReceipt.Controls.Add(this.textBoxFushoFree);
            this.panelReceipt.Controls.Add(this.textBoxShomeiY2);
            this.panelReceipt.Controls.Add(this.textBoxDays2);
            this.panelReceipt.Controls.Add(this.textBoxShomeiM2);
            this.panelReceipt.Controls.Add(this.label1);
            this.panelReceipt.Controls.Add(this.textBoxShomeiD2);
            this.panelReceipt.Controls.Add(this.labelDays);
            this.panelReceipt.Controls.Add(this.textBoxOryoDays2);
            this.panelReceipt.Controls.Add(this.textBoxAdd2);
            this.panelReceipt.Controls.Add(this.label18);
            this.panelReceipt.Controls.Add(this.textBoxFname2);
            this.panelReceipt.Controls.Add(this.label4);
            this.panelReceipt.Controls.Add(this.textBoxFname);
            this.panelReceipt.Controls.Add(this.textBoxBY);
            this.panelReceipt.Controls.Add(this.label39);
            this.panelReceipt.Controls.Add(this.label11);
            this.panelReceipt.Controls.Add(this.label30);
            this.panelReceipt.Controls.Add(this.textBoxSex);
            this.panelReceipt.Controls.Add(this.label40);
            this.panelReceipt.Controls.Add(this.label17);
            this.panelReceipt.Controls.Add(this.textBoxIninY);
            this.panelReceipt.Controls.Add(this.textBoxTekiou);
            this.panelReceipt.Controls.Add(this.label41);
            this.panelReceipt.Controls.Add(this.textBoxShinryoDays2);
            this.panelReceipt.Controls.Add(this.textBoxOryoDays);
            this.panelReceipt.Controls.Add(this.labelSex);
            this.panelReceipt.Controls.Add(this.textBoxIninM);
            this.panelReceipt.Controls.Add(this.textBoxFushoY2);
            this.panelReceipt.Controls.Add(this.textBoxIninD);
            this.panelReceipt.Controls.Add(this.label44);
            this.panelReceipt.Controls.Add(this.textBoxFutan2);
            this.panelReceipt.Controls.Add(this.textBoxCharge2);
            this.panelReceipt.Controls.Add(this.label42);
            this.panelReceipt.Controls.Add(this.textBoxBM);
            this.panelReceipt.Controls.Add(this.label6);
            this.panelReceipt.Controls.Add(this.textBoxTotal2);
            this.panelReceipt.Controls.Add(this.label34);
            this.panelReceipt.Controls.Add(this.textBoxBirthday);
            this.panelReceipt.Controls.Add(this.textBoxShomeiY);
            this.panelReceipt.Controls.Add(this.textBoxFushoM2);
            this.panelReceipt.Controls.Add(this.label37);
            this.panelReceipt.Controls.Add(this.label15);
            this.panelReceipt.Controls.Add(this.textBoxShomeiM);
            this.panelReceipt.Controls.Add(this.textBoxFushoD2);
            this.panelReceipt.Controls.Add(this.textBoxShomeiD);
            this.panelReceipt.Controls.Add(this.label5);
            this.panelReceipt.Controls.Add(this.label38);
            this.panelReceipt.Controls.Add(this.textBoxKasan2);
            this.panelReceipt.Controls.Add(this.textBoxDairiName2);
            this.panelReceipt.Controls.Add(this.textBoxBD);
            this.panelReceipt.Controls.Add(this.textBoxDairiName);
            this.panelReceipt.Controls.Add(this.textBoxOryo2);
            this.panelReceipt.Controls.Add(this.label35);
            this.panelReceipt.Controls.Add(this.labelBirthday);
            this.panelReceipt.Controls.Add(this.textBoxDairiAdd2);
            this.panelReceipt.Controls.Add(this.label16);
            this.panelReceipt.Controls.Add(this.textBoxHosAdd2);
            this.panelReceipt.Controls.Add(this.textBoxFusho2);
            this.panelReceipt.Controls.Add(this.textBoxDairiAdd);
            this.panelReceipt.Controls.Add(this.textBoxHnum);
            this.panelReceipt.Controls.Add(this.label32);
            this.panelReceipt.Controls.Add(this.textBoxShokenY2);
            this.panelReceipt.Controls.Add(this.textBoxHosName2);
            this.panelReceipt.Controls.Add(this.textBoxRatio);
            this.panelReceipt.Controls.Add(this.textBoxHosAdd);
            this.panelReceipt.Controls.Add(this.textBoxName2);
            this.panelReceipt.Controls.Add(this.label36);
            this.panelReceipt.Controls.Add(this.labelHnum);
            this.panelReceipt.Controls.Add(this.textBoxDrName2);
            this.panelReceipt.Controls.Add(this.textBoxShokenM2);
            this.panelReceipt.Controls.Add(this.label33);
            this.panelReceipt.Controls.Add(this.textBoxHosName);
            this.panelReceipt.Controls.Add(this.textBoxShokenD2);
            this.panelReceipt.Controls.Add(this.textBoxDrName);
            this.panelReceipt.Controls.Add(this.label31);
            this.panelReceipt.Controls.Add(this.label7);
            this.panelReceipt.Controls.Add(this.label25);
            this.panelReceipt.Controls.Add(this.label22);
            this.panelReceipt.Controls.Add(this.label19);
            this.panelReceipt.Controls.Add(this.textBoxShokenD);
            this.panelReceipt.Controls.Add(this.textBoxAdd);
            this.panelReceipt.Controls.Add(this.label14);
            this.panelReceipt.Controls.Add(this.textBoxFusho);
            this.panelReceipt.Controls.Add(this.textBoxShokenM);
            this.panelReceipt.Controls.Add(this.label24);
            this.panelReceipt.Controls.Add(this.label13);
            this.panelReceipt.Controls.Add(this.textBoxShokenY);
            this.panelReceipt.Controls.Add(this.label27);
            this.panelReceipt.Controls.Add(this.label29);
            this.panelReceipt.Controls.Add(this.label28);
            this.panelReceipt.Controls.Add(this.textBoxDrCode);
            this.panelReceipt.Controls.Add(this.textBoxShinryoDays);
            this.panelReceipt.Controls.Add(this.label21);
            this.panelReceipt.Controls.Add(this.textBoxFushoY);
            this.panelReceipt.Controls.Add(this.textBoxName);
            this.panelReceipt.Controls.Add(this.label20);
            this.panelReceipt.Controls.Add(this.labelTotal);
            this.panelReceipt.Controls.Add(this.label12);
            this.panelReceipt.Controls.Add(this.label2);
            this.panelReceipt.Controls.Add(this.labelCharge);
            this.panelReceipt.Controls.Add(this.label26);
            this.panelReceipt.Controls.Add(this.label23);
            this.panelReceipt.Controls.Add(this.textBoxOryo);
            this.panelReceipt.Controls.Add(this.textBoxFutan);
            this.panelReceipt.Controls.Add(this.textBoxFushoD);
            this.panelReceipt.Controls.Add(this.textBoxCharge);
            this.panelReceipt.Controls.Add(this.textBoxFushoM);
            this.panelReceipt.Controls.Add(this.textBoxKasan);
            this.panelReceipt.Controls.Add(this.textBoxTotal);
            this.panelReceipt.Location = new System.Drawing.Point(0, 60);
            this.panelReceipt.Name = "panelReceipt";
            this.panelReceipt.Size = new System.Drawing.Size(404, 1126);
            this.panelReceipt.TabIndex = 3;
            // 
            // textBoxTekiou2
            // 
            this.textBoxTekiou2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTekiou2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTekiou2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTekiou2.Location = new System.Drawing.Point(287, 423);
            this.textBoxTekiou2.MaxLength = 1;
            this.textBoxTekiou2.Name = "textBoxTekiou2";
            this.textBoxTekiou2.Size = new System.Drawing.Size(26, 26);
            this.textBoxTekiou2.TabIndex = 76;
            this.textBoxTekiou2.Visible = false;
            this.textBoxTekiou2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label9.Location = new System.Drawing.Point(320, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 36);
            this.label9.TabIndex = 3;
            this.label9.Text = "記載がない場合\r\n六歳、高一： 8\r\nそれ以外： 7";
            // 
            // textBoxBirthday2
            // 
            this.textBoxBirthday2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBirthday2.Location = new System.Drawing.Point(161, 138);
            this.textBoxBirthday2.Name = "textBoxBirthday2";
            this.textBoxBirthday2.Size = new System.Drawing.Size(26, 26);
            this.textBoxBirthday2.TabIndex = 25;
            this.textBoxBirthday2.Visible = false;
            this.textBoxBirthday2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxSex2
            // 
            this.textBoxSex2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxSex2.Location = new System.Drawing.Point(43, 138);
            this.textBoxSex2.MaxLength = 1;
            this.textBoxSex2.Name = "textBoxSex2";
            this.textBoxSex2.Size = new System.Drawing.Size(26, 26);
            this.textBoxSex2.TabIndex = 23;
            this.textBoxSex2.Visible = false;
            this.textBoxSex2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBY2
            // 
            this.textBoxBY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBY2.Location = new System.Drawing.Point(216, 138);
            this.textBoxBY2.MaxLength = 2;
            this.textBoxBY2.Name = "textBoxBY2";
            this.textBoxBY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBY2.TabIndex = 27;
            this.textBoxBY2.Visible = false;
            this.textBoxBY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxY2
            // 
            this.textBoxY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxY2.Location = new System.Drawing.Point(43, 26);
            this.textBoxY2.MaxLength = 2;
            this.textBoxY2.Name = "textBoxY2";
            this.textBoxY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxY2.TabIndex = 6;
            this.textBoxY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY2.Visible = false;
            this.textBoxY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBM2
            // 
            this.textBoxBM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBM2.Location = new System.Drawing.Point(265, 138);
            this.textBoxBM2.MaxLength = 2;
            this.textBoxBM2.Name = "textBoxBM2";
            this.textBoxBM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBM2.TabIndex = 29;
            this.textBoxBM2.Visible = false;
            this.textBoxBM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxM2
            // 
            this.textBoxM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxM2.Location = new System.Drawing.Point(93, 26);
            this.textBoxM2.MaxLength = 2;
            this.textBoxM2.Name = "textBoxM2";
            this.textBoxM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxM2.TabIndex = 9;
            this.textBoxM2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM2.Visible = false;
            this.textBoxM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBD2
            // 
            this.textBoxBD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBD2.Location = new System.Drawing.Point(314, 138);
            this.textBoxBD2.MaxLength = 2;
            this.textBoxBD2.Name = "textBoxBD2";
            this.textBoxBD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBD2.TabIndex = 31;
            this.textBoxBD2.Visible = false;
            this.textBoxBD2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHnum2
            // 
            this.textBoxHnum2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHnum2.Location = new System.Drawing.Point(228, 81);
            this.textBoxHnum2.Name = "textBoxHnum2";
            this.textBoxHnum2.Size = new System.Drawing.Size(85, 26);
            this.textBoxHnum2.TabIndex = 16;
            this.textBoxHnum2.Visible = false;
            this.textBoxHnum2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxRatio2
            // 
            this.textBoxRatio2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxRatio2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxRatio2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxRatio2.Location = new System.Drawing.Point(351, 81);
            this.textBoxRatio2.MaxLength = 1;
            this.textBoxRatio2.Name = "textBoxRatio2";
            this.textBoxRatio2.Size = new System.Drawing.Size(26, 26);
            this.textBoxRatio2.TabIndex = 19;
            this.textBoxRatio2.Visible = false;
            this.textBoxRatio2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFushoFree2
            // 
            this.textBoxFushoFree2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoFree2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoFree2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFushoFree2.Location = new System.Drawing.Point(260, 366);
            this.textBoxFushoFree2.Name = "textBoxFushoFree2";
            this.textBoxFushoFree2.Size = new System.Drawing.Size(124, 26);
            this.textBoxFushoFree2.TabIndex = 67;
            this.textBoxFushoFree2.Visible = false;
            // 
            // textBoxDays
            // 
            this.textBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDays.Location = new System.Drawing.Point(225, 227);
            this.textBoxDays.MaxLength = 2;
            this.textBoxDays.Name = "textBoxDays";
            this.textBoxDays.Size = new System.Drawing.Size(32, 26);
            this.textBoxDays.TabIndex = 50;
            this.textBoxDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFushoFree
            // 
            this.textBoxFushoFree.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoFree.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoFree.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFushoFree.Location = new System.Drawing.Point(260, 341);
            this.textBoxFushoFree.Name = "textBoxFushoFree";
            this.textBoxFushoFree.Size = new System.Drawing.Size(124, 26);
            this.textBoxFushoFree.TabIndex = 66;
            this.textBoxFushoFree.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBoxDays2
            // 
            this.textBoxDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDays2.Location = new System.Drawing.Point(225, 252);
            this.textBoxDays2.MaxLength = 2;
            this.textBoxDays2.Name = "textBoxDays2";
            this.textBoxDays2.Size = new System.Drawing.Size(32, 26);
            this.textBoxDays2.TabIndex = 51;
            this.textBoxDays2.Visible = false;
            this.textBoxDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 24);
            this.label1.TabIndex = 65;
            this.label1.Text = "負傷日\r\n自由記入";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(195, 229);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(29, 24);
            this.labelDays.TabIndex = 49;
            this.labelDays.Text = "診療\r\n日数";
            // 
            // textBoxOryoDays2
            // 
            this.textBoxOryoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxOryoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOryoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxOryoDays2.Location = new System.Drawing.Point(43, 593);
            this.textBoxOryoDays2.Name = "textBoxOryoDays2";
            this.textBoxOryoDays2.Size = new System.Drawing.Size(360, 26);
            this.textBoxOryoDays2.TabIndex = 92;
            this.textBoxOryoDays2.Visible = false;
            this.textBoxOryoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxAdd2
            // 
            this.textBoxAdd2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxAdd2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxAdd2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxAdd2.Location = new System.Drawing.Point(43, 1041);
            this.textBoxAdd2.Name = "textBoxAdd2";
            this.textBoxAdd2.Size = new System.Drawing.Size(360, 26);
            this.textBoxAdd2.TabIndex = 130;
            this.textBoxAdd2.Visible = false;
            this.textBoxAdd2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(248, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 27;
            this.label18.Text = "年";
            // 
            // textBoxFname2
            // 
            this.textBoxFname2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFname2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFname2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFname2.Location = new System.Drawing.Point(43, 1098);
            this.textBoxFname2.Name = "textBoxFname2";
            this.textBoxFname2.Size = new System.Drawing.Size(120, 26);
            this.textBoxFname2.TabIndex = 132;
            this.textBoxFname2.Visible = false;
            this.textBoxFname2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(70, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 24);
            this.label4.TabIndex = 23;
            this.label4.Text = "男 : 1\r\n女 : 2";
            // 
            // textBoxFname
            // 
            this.textBoxFname.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFname.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFname.Location = new System.Drawing.Point(43, 1073);
            this.textBoxFname.Name = "textBoxFname";
            this.textBoxFname.Size = new System.Drawing.Size(120, 26);
            this.textBoxFname.TabIndex = 131;
            this.textBoxFname.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBY
            // 
            this.textBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBY.Location = new System.Drawing.Point(216, 113);
            this.textBoxBY.MaxLength = 2;
            this.textBoxBY.Name = "textBoxBY";
            this.textBoxBY.Size = new System.Drawing.Size(32, 26);
            this.textBoxBY.TabIndex = 26;
            this.textBoxBY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label11.Location = new System.Drawing.Point(314, 400);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 24);
            this.label11.TabIndex = 77;
            this.label11.Text = "あり : 1\r\nなし : 2";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 582);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 90;
            this.label30.Text = "往療日";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxSex
            // 
            this.textBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxSex.Location = new System.Drawing.Point(43, 113);
            this.textBoxSex.MaxLength = 1;
            this.textBoxSex.Name = "textBoxSex";
            this.textBoxSex.Size = new System.Drawing.Size(26, 26);
            this.textBoxSex.TabIndex = 22;
            this.textBoxSex.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(297, 127);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 29;
            this.label17.Text = "月";
            // 
            // textBoxTekiou
            // 
            this.textBoxTekiou.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTekiou.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTekiou.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTekiou.Location = new System.Drawing.Point(287, 398);
            this.textBoxTekiou.MaxLength = 1;
            this.textBoxTekiou.Name = "textBoxTekiou";
            this.textBoxTekiou.Size = new System.Drawing.Size(26, 26);
            this.textBoxTekiou.TabIndex = 75;
            this.textBoxTekiou.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxShinryoDays2
            // 
            this.textBoxShinryoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShinryoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShinryoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShinryoDays2.Location = new System.Drawing.Point(43, 536);
            this.textBoxShinryoDays2.Name = "textBoxShinryoDays2";
            this.textBoxShinryoDays2.Size = new System.Drawing.Size(360, 26);
            this.textBoxShinryoDays2.TabIndex = 89;
            this.textBoxShinryoDays2.Visible = false;
            this.textBoxShinryoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxOryoDays
            // 
            this.textBoxOryoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxOryoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOryoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxOryoDays.Location = new System.Drawing.Point(43, 568);
            this.textBoxOryoDays.Name = "textBoxOryoDays";
            this.textBoxOryoDays.Size = new System.Drawing.Size(360, 26);
            this.textBoxOryoDays.TabIndex = 91;
            this.textBoxOryoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxOryoDays.Leave += new System.EventHandler(this.textBoxOryoDays_Leave);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(14, 127);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 21;
            this.labelSex.Text = "性別";
            // 
            // textBoxFushoY2
            // 
            this.textBoxFushoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoY2.Location = new System.Drawing.Point(43, 366);
            this.textBoxFushoY2.MaxLength = 2;
            this.textBoxFushoY2.Name = "textBoxFushoY2";
            this.textBoxFushoY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoY2.TabIndex = 57;
            this.textBoxFushoY2.Visible = false;
            this.textBoxFushoY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(258, 412);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(29, 12);
            this.label44.TabIndex = 74;
            this.label44.Text = "摘要";
            // 
            // textBoxFutan2
            // 
            this.textBoxFutan2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFutan2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFutan2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFutan2.Location = new System.Drawing.Point(276, 478);
            this.textBoxFutan2.Name = "textBoxFutan2";
            this.textBoxFutan2.Size = new System.Drawing.Size(80, 26);
            this.textBoxFutan2.TabIndex = 86;
            this.textBoxFutan2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxFutan2.Visible = false;
            this.textBoxFutan2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxCharge2
            // 
            this.textBoxCharge2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxCharge2.Location = new System.Drawing.Point(161, 480);
            this.textBoxCharge2.Name = "textBoxCharge2";
            this.textBoxCharge2.Size = new System.Drawing.Size(80, 26);
            this.textBoxCharge2.TabIndex = 83;
            this.textBoxCharge2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCharge2.Visible = false;
            this.textBoxCharge2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBM
            // 
            this.textBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBM.Location = new System.Drawing.Point(265, 113);
            this.textBoxBM.MaxLength = 2;
            this.textBoxBM.Name = "textBoxBM";
            this.textBoxBM.Size = new System.Drawing.Size(32, 26);
            this.textBoxBM.TabIndex = 28;
            this.textBoxBM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxTotal2
            // 
            this.textBoxTotal2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTotal2.Location = new System.Drawing.Point(43, 480);
            this.textBoxTotal2.Name = "textBoxTotal2";
            this.textBoxTotal2.Size = new System.Drawing.Size(80, 26);
            this.textBoxTotal2.TabIndex = 80;
            this.textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal2.Visible = false;
            this.textBoxTotal2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBirthday.Location = new System.Drawing.Point(161, 113);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(26, 26);
            this.textBoxBirthday.TabIndex = 24;
            this.textBoxBirthday.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFushoM2
            // 
            this.textBoxFushoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoM2.Location = new System.Drawing.Point(92, 366);
            this.textBoxFushoM2.MaxLength = 2;
            this.textBoxFushoM2.Name = "textBoxFushoM2";
            this.textBoxFushoM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoM2.TabIndex = 60;
            this.textBoxFushoM2.Visible = false;
            this.textBoxFushoM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(322, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 24);
            this.label15.TabIndex = 17;
            this.label15.Text = "給付\r\n割合";
            // 
            // textBoxFushoD2
            // 
            this.textBoxFushoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoD2.Location = new System.Drawing.Point(141, 366);
            this.textBoxFushoD2.MaxLength = 2;
            this.textBoxFushoD2.Name = "textBoxFushoD2";
            this.textBoxFushoD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoD2.TabIndex = 63;
            this.textBoxFushoD2.Visible = false;
            this.textBoxFushoD2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(187, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 24);
            this.label5.TabIndex = 25;
            this.label5.Text = "昭：3\r\n平：4";
            // 
            // textBoxKasan2
            // 
            this.textBoxKasan2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxKasan2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxKasan2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxKasan2.Location = new System.Drawing.Point(161, 423);
            this.textBoxKasan2.Name = "textBoxKasan2";
            this.textBoxKasan2.Size = new System.Drawing.Size(80, 26);
            this.textBoxKasan2.TabIndex = 73;
            this.textBoxKasan2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxKasan2.Visible = false;
            this.textBoxKasan2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBD
            // 
            this.textBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBD.Location = new System.Drawing.Point(314, 113);
            this.textBoxBD.MaxLength = 2;
            this.textBoxBD.Name = "textBoxBD";
            this.textBoxBD.Size = new System.Drawing.Size(32, 26);
            this.textBoxBD.TabIndex = 30;
            this.textBoxBD.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxBD.Leave += new System.EventHandler(this.textBoxBD_Leave);
            // 
            // textBoxOryo2
            // 
            this.textBoxOryo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxOryo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOryo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxOryo2.Location = new System.Drawing.Point(43, 423);
            this.textBoxOryo2.Name = "textBoxOryo2";
            this.textBoxOryo2.Size = new System.Drawing.Size(80, 26);
            this.textBoxOryo2.TabIndex = 70;
            this.textBoxOryo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxOryo2.Visible = false;
            this.textBoxOryo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(132, 115);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(29, 24);
            this.labelBirthday.TabIndex = 23;
            this.labelBirthday.Text = "生年\r\n月日";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(346, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 31;
            this.label16.Text = "日";
            // 
            // textBoxFusho2
            // 
            this.textBoxFusho2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFusho2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFusho2.Location = new System.Drawing.Point(43, 309);
            this.textBoxFusho2.Name = "textBoxFusho2";
            this.textBoxFusho2.Size = new System.Drawing.Size(360, 26);
            this.textBoxFusho2.TabIndex = 54;
            this.textBoxFusho2.Visible = false;
            this.textBoxFusho2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxShokenY2
            // 
            this.textBoxShokenY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenY2.Location = new System.Drawing.Point(43, 252);
            this.textBoxShokenY2.MaxLength = 2;
            this.textBoxShokenY2.Name = "textBoxShokenY2";
            this.textBoxShokenY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenY2.TabIndex = 41;
            this.textBoxShokenY2.Visible = false;
            this.textBoxShokenY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxRatio
            // 
            this.textBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxRatio.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxRatio.Location = new System.Drawing.Point(351, 56);
            this.textBoxRatio.MaxLength = 1;
            this.textBoxRatio.Name = "textBoxRatio";
            this.textBoxRatio.Size = new System.Drawing.Size(26, 26);
            this.textBoxRatio.TabIndex = 18;
            this.textBoxRatio.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxName2
            // 
            this.textBoxName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxName2.Location = new System.Drawing.Point(43, 195);
            this.textBoxName2.Name = "textBoxName2";
            this.textBoxName2.Size = new System.Drawing.Size(120, 26);
            this.textBoxName2.TabIndex = 38;
            this.textBoxName2.Visible = false;
            this.textBoxName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxShokenM2
            // 
            this.textBoxShokenM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenM2.Location = new System.Drawing.Point(92, 252);
            this.textBoxShokenM2.MaxLength = 2;
            this.textBoxShokenM2.Name = "textBoxShokenM2";
            this.textBoxShokenM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenM2.TabIndex = 44;
            this.textBoxShokenM2.Visible = false;
            this.textBoxShokenM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxShokenD2
            // 
            this.textBoxShokenD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenD2.Location = new System.Drawing.Point(141, 252);
            this.textBoxShokenD2.MaxLength = 2;
            this.textBoxShokenD2.Name = "textBoxShokenD2";
            this.textBoxShokenD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenD2.TabIndex = 47;
            this.textBoxShokenD2.Visible = false;
            this.textBoxShokenD2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 1018);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 24);
            this.label7.TabIndex = 128;
            this.label7.Text = "委任\r\n住所";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(173, 241);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 48;
            this.label25.Text = "日";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(0, 298);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 52;
            this.label22.Text = "負傷名";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 1075);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 24);
            this.label19.TabIndex = 130;
            this.label19.Text = "委任\r\n氏名";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxShokenD
            // 
            this.textBoxShokenD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenD.Location = new System.Drawing.Point(141, 227);
            this.textBoxShokenD.MaxLength = 2;
            this.textBoxShokenD.Name = "textBoxShokenD";
            this.textBoxShokenD.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenD.TabIndex = 46;
            this.textBoxShokenD.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxAdd
            // 
            this.textBoxAdd.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxAdd.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxAdd.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxAdd.Location = new System.Drawing.Point(43, 1016);
            this.textBoxAdd.Name = "textBoxAdd";
            this.textBoxAdd.Size = new System.Drawing.Size(360, 26);
            this.textBoxAdd.TabIndex = 129;
            this.textBoxAdd.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxAdd.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 355);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 55;
            this.label14.Text = "負傷日";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxFusho
            // 
            this.textBoxFusho.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFusho.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFusho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFusho.Location = new System.Drawing.Point(43, 284);
            this.textBoxFusho.Name = "textBoxFusho";
            this.textBoxFusho.Size = new System.Drawing.Size(360, 26);
            this.textBoxFusho.TabIndex = 53;
            this.textBoxFusho.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxFusho.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBoxShokenM
            // 
            this.textBoxShokenM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenM.Location = new System.Drawing.Point(92, 227);
            this.textBoxShokenM.MaxLength = 2;
            this.textBoxShokenM.Name = "textBoxShokenM";
            this.textBoxShokenM.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenM.TabIndex = 43;
            this.textBoxShokenM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(124, 241);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 45;
            this.label24.Text = "月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(75, 355);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 58;
            this.label13.Text = "年";
            // 
            // textBoxShokenY
            // 
            this.textBoxShokenY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShokenY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokenY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShokenY.Location = new System.Drawing.Point(43, 227);
            this.textBoxShokenY.MaxLength = 2;
            this.textBoxShokenY.Name = "textBoxShokenY";
            this.textBoxShokenY.Size = new System.Drawing.Size(32, 26);
            this.textBoxShokenY.TabIndex = 40;
            this.textBoxShokenY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 400);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 24);
            this.label27.TabIndex = 68;
            this.label27.Text = "往療\r\n金額";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 525);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 87;
            this.label29.Text = "診療日";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(132, 400);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 24);
            this.label28.TabIndex = 71;
            this.label28.Text = "加算\r\n金額";
            // 
            // textBoxShinryoDays
            // 
            this.textBoxShinryoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShinryoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShinryoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShinryoDays.Location = new System.Drawing.Point(43, 511);
            this.textBoxShinryoDays.Name = "textBoxShinryoDays";
            this.textBoxShinryoDays.Size = new System.Drawing.Size(360, 26);
            this.textBoxShinryoDays.TabIndex = 88;
            this.textBoxShinryoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxShinryoDays.Leave += new System.EventHandler(this.textBoxShinryoDays_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(173, 355);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 64;
            this.label21.Text = "日";
            // 
            // textBoxFushoY
            // 
            this.textBoxFushoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoY.Location = new System.Drawing.Point(43, 341);
            this.textBoxFushoY.MaxLength = 2;
            this.textBoxFushoY.Name = "textBoxFushoY";
            this.textBoxFushoY.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoY.TabIndex = 56;
            this.textBoxFushoY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxName.Location = new System.Drawing.Point(43, 170);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(120, 26);
            this.textBoxName.TabIndex = 37;
            this.textBoxName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(2, 172);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 36;
            this.label20.Text = "療養者\r\n氏名";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(14, 457);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 24);
            this.labelTotal.TabIndex = 78;
            this.labelTotal.Text = "合計\r\n金額";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(124, 355);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 61;
            this.label12.Text = "月";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 455);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 24);
            this.label2.TabIndex = 84;
            this.label2.Text = "負担\r\n金額";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(132, 457);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(29, 24);
            this.labelCharge.TabIndex = 81;
            this.labelCharge.Text = "請求\r\n金額";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, 241);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 12);
            this.label26.TabIndex = 39;
            this.label26.Text = "初療日";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(75, 241);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 42;
            this.label23.Text = "年";
            // 
            // textBoxOryo
            // 
            this.textBoxOryo.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxOryo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOryo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxOryo.Location = new System.Drawing.Point(43, 398);
            this.textBoxOryo.Name = "textBoxOryo";
            this.textBoxOryo.Size = new System.Drawing.Size(80, 26);
            this.textBoxOryo.TabIndex = 69;
            this.textBoxOryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxOryo.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFutan
            // 
            this.textBoxFutan.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFutan.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFutan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFutan.Location = new System.Drawing.Point(276, 453);
            this.textBoxFutan.Name = "textBoxFutan";
            this.textBoxFutan.Size = new System.Drawing.Size(80, 26);
            this.textBoxFutan.TabIndex = 85;
            this.textBoxFutan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxFutan.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFushoD
            // 
            this.textBoxFushoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoD.Location = new System.Drawing.Point(141, 341);
            this.textBoxFushoD.MaxLength = 2;
            this.textBoxFushoD.Name = "textBoxFushoD";
            this.textBoxFushoD.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoD.TabIndex = 62;
            this.textBoxFushoD.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxCharge
            // 
            this.textBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxCharge.Location = new System.Drawing.Point(161, 455);
            this.textBoxCharge.Name = "textBoxCharge";
            this.textBoxCharge.Size = new System.Drawing.Size(80, 26);
            this.textBoxCharge.TabIndex = 82;
            this.textBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCharge.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxFushoM
            // 
            this.textBoxFushoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFushoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFushoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFushoM.Location = new System.Drawing.Point(92, 341);
            this.textBoxFushoM.MaxLength = 2;
            this.textBoxFushoM.Name = "textBoxFushoM";
            this.textBoxFushoM.Size = new System.Drawing.Size(32, 26);
            this.textBoxFushoM.TabIndex = 59;
            this.textBoxFushoM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxKasan
            // 
            this.textBoxKasan.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxKasan.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxKasan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxKasan.Location = new System.Drawing.Point(161, 398);
            this.textBoxKasan.Name = "textBoxKasan";
            this.textBoxKasan.Size = new System.Drawing.Size(80, 26);
            this.textBoxKasan.TabIndex = 72;
            this.textBoxKasan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxKasan.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTotal.Location = new System.Drawing.Point(43, 455);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(80, 26);
            this.textBoxTotal.TabIndex = 79;
            this.textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxType2
            // 
            this.textBoxType2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxType2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxType2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxType2.Location = new System.Drawing.Point(43, 33);
            this.textBoxType2.MaxLength = 2;
            this.textBoxType2.Name = "textBoxType2";
            this.textBoxType2.Size = new System.Drawing.Size(32, 26);
            this.textBoxType2.TabIndex = 2;
            this.textBoxType2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxType2.Visible = false;
            this.textBoxType2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxType
            // 
            this.textBoxType.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxType.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxType.Location = new System.Drawing.Point(43, 8);
            this.textBoxType.MaxLength = 2;
            this.textBoxType.Name = "textBoxType";
            this.textBoxType.Size = new System.Drawing.Size(32, 26);
            this.textBoxType.TabIndex = 1;
            this.textBoxType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxType.TextChanged += new System.EventHandler(this.textBoxType_TextChanged);
            this.textBoxType.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(14, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 12);
            this.label43.TabIndex = 0;
            this.label43.Text = "種別";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelDoui
            // 
            this.panelDoui.Controls.Add(this.textBoxDouiAdd2);
            this.panelDoui.Controls.Add(this.textBoxDouiName2);
            this.panelDoui.Controls.Add(this.textBoxDouiY2);
            this.panelDoui.Controls.Add(this.textBoxDouiM2);
            this.panelDoui.Controls.Add(this.textBoxDouiD2);
            this.panelDoui.Controls.Add(this.textBoxDouiFusho2);
            this.panelDoui.Controls.Add(this.textBoxDouiName);
            this.panelDoui.Controls.Add(this.label52);
            this.panelDoui.Controls.Add(this.label56);
            this.panelDoui.Controls.Add(this.textBoxDouiAdd);
            this.panelDoui.Controls.Add(this.label50);
            this.panelDoui.Controls.Add(this.textBoxDouiD);
            this.panelDoui.Controls.Add(this.textBoxDouiM);
            this.panelDoui.Controls.Add(this.label49);
            this.panelDoui.Controls.Add(this.textBoxDouiY);
            this.panelDoui.Controls.Add(this.label48);
            this.panelDoui.Controls.Add(this.label55);
            this.panelDoui.Controls.Add(this.textBoxDouiFusho);
            this.panelDoui.Controls.Add(this.label47);
            this.panelDoui.Location = new System.Drawing.Point(0, 60);
            this.panelDoui.Name = "panelDoui";
            this.panelDoui.Size = new System.Drawing.Size(404, 220);
            this.panelDoui.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonExit);
            this.panel2.Controls.Add(this.buttonRegist);
            this.panel2.Controls.Add(this.labelInputerName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.panel2.Location = new System.Drawing.Point(0, 711);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(421, 28);
            this.panel2.TabIndex = 0;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(8, 8);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(203, 16);
            this.labelInputerName.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(199, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 739);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(199, 739);
            this.panelDatalist.TabIndex = 0;
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(199, 739);
            this.dataGridViewPlist.TabIndex = 0;
            this.dataGridViewPlist.CurrentCellChanged += new System.EventHandler(this.dataGridViewPlist_CurrentCellChanged_1);
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.GreenYellow;
            this.panelUP.Controls.Add(this.labelNote1);
            this.panelUP.Controls.Add(this.labelGroupID);
            this.panelUP.Controls.Add(this.labelScanID);
            this.panelUP.Controls.Add(this.labelSeikyu);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(199, 35);
            this.panelUP.TabIndex = 2;
            // 
            // labelNote1
            // 
            this.labelNote1.AutoSize = true;
            this.labelNote1.Location = new System.Drawing.Point(2, 20);
            this.labelNote1.Name = "labelNote1";
            this.labelNote1.Size = new System.Drawing.Size(31, 12);
            this.labelNote1.TabIndex = 8;
            this.labelNote1.Text = "Note:";
            // 
            // labelGroupID
            // 
            this.labelGroupID.AutoSize = true;
            this.labelGroupID.Location = new System.Drawing.Point(93, 20);
            this.labelGroupID.Name = "labelGroupID";
            this.labelGroupID.Size = new System.Drawing.Size(48, 12);
            this.labelGroupID.TabIndex = 6;
            this.labelGroupID.Text = "GroupID:";
            // 
            // labelScanID
            // 
            this.labelScanID.AutoSize = true;
            this.labelScanID.Location = new System.Drawing.Point(93, 3);
            this.labelScanID.Name = "labelScanID";
            this.labelScanID.Size = new System.Drawing.Size(43, 12);
            this.labelScanID.TabIndex = 0;
            this.labelScanID.Text = "ScanID:";
            // 
            // labelSeikyu
            // 
            this.labelSeikyu.AutoSize = true;
            this.labelSeikyu.Location = new System.Drawing.Point(2, 3);
            this.labelSeikyu.Name = "labelSeikyu";
            this.labelSeikyu.Size = new System.Drawing.Size(31, 12);
            this.labelSeikyu.TabIndex = 5;
            this.labelSeikyu.Text = "請求:";
            // 
            // FormOCRCheckOtherCheckSK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 739);
            this.Controls.Add(this.panelIN);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelDatalist);
            this.KeyPreview = true;
            this.Name = "FormOCRCheckOtherCheckSK";
            this.Text = "OCR Check";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIN)).EndInit();
            this.panelIN.ResumeLayout(false);
            this.panelIN.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelControlScroll.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.panelReceipt.ResumeLayout(false);
            this.panelReceipt.PerformLayout();
            this.panelDoui.ResumeLayout(false);
            this.panelDoui.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelDatalist.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIN;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.TextBox textBoxHnum;
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panelIN;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox textBoxSex;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.TextBox textBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelNumbering;
        private System.Windows.Forms.TextBox textBoxDrCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxBD;
        private System.Windows.Forms.TextBox textBoxBM;
        private System.Windows.Forms.TextBox textBoxBY;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.TextBox textBoxDays;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDrCode2;
        private System.Windows.Forms.TextBox textBoxHnum2;
        private System.Windows.Forms.TextBox textBoxY2;
        private System.Windows.Forms.TextBox textBoxM2;
        private System.Windows.Forms.TextBox textBoxBD2;
        private System.Windows.Forms.TextBox textBoxBM2;
        private System.Windows.Forms.TextBox textBoxBY2;
        private System.Windows.Forms.TextBox textBoxSex2;
        private System.Windows.Forms.TextBox textBoxBirthday2;
        private System.Windows.Forms.TextBox textBoxCharge2;
        private System.Windows.Forms.TextBox textBoxTotal2;
        private System.Windows.Forms.TextBox textBoxDays2;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxRatio2;
        private System.Windows.Forms.TextBox textBoxRatio;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelControlScroll;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxShokenY;
        private System.Windows.Forms.TextBox textBoxShokenY2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBoxShokenM;
        private System.Windows.Forms.TextBox textBoxShokenM2;
        private System.Windows.Forms.TextBox textBoxShokenD;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxShokenD2;
        private System.Windows.Forms.TextBox textBoxFusho2;
        private System.Windows.Forms.TextBox textBoxFusho;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxName2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxFname;
        private System.Windows.Forms.TextBox textBoxFname2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxHosName;
        private System.Windows.Forms.TextBox textBoxHosName2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBoxHosAdd;
        private System.Windows.Forms.TextBox textBoxHosAdd2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxDrName;
        private System.Windows.Forms.TextBox textBoxDrName2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxAdd;
        private System.Windows.Forms.TextBox textBoxAdd2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxFushoY;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxFushoM;
        private System.Windows.Forms.TextBox textBoxFushoD;
        private System.Windows.Forms.TextBox textBoxFushoY2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxFushoM2;
        private System.Windows.Forms.TextBox textBoxFushoD2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxOryoDays;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxShinryoDays;
        private System.Windows.Forms.TextBox textBoxKasan;
        private System.Windows.Forms.TextBox textBoxOryo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxKasan2;
        private System.Windows.Forms.TextBox textBoxOryo2;
        private System.Windows.Forms.TextBox textBoxOryoDays2;
        private System.Windows.Forms.TextBox textBoxShinryoDays2;
        private System.Windows.Forms.TextBox textBoxType;
        private System.Windows.Forms.TextBox textBoxType2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBoxIninY;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxIninM;
        private System.Windows.Forms.TextBox textBoxIninD;
        private System.Windows.Forms.TextBox textBoxIninY2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBoxIninM2;
        private System.Windows.Forms.TextBox textBoxIninD2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxShomeiY;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBoxShomeiM;
        private System.Windows.Forms.TextBox textBoxShomeiD;
        private System.Windows.Forms.TextBox textBoxShomeiY2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxShomeiM2;
        private System.Windows.Forms.TextBox textBoxShomeiD2;
        private System.Windows.Forms.TextBox textBoxDairiName2;
        private System.Windows.Forms.TextBox textBoxDairiAdd2;
        private System.Windows.Forms.TextBox textBoxDairiName;
        private System.Windows.Forms.TextBox textBoxDairiAdd;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBoxDouiY;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxDouiM;
        private System.Windows.Forms.TextBox textBoxDouiD;
        private System.Windows.Forms.TextBox textBoxDouiY2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBoxDouiM2;
        private System.Windows.Forms.TextBox textBoxDouiD2;
        private System.Windows.Forms.TextBox textBoxDouiName2;
        private System.Windows.Forms.TextBox textBoxDouiFusho2;
        private System.Windows.Forms.TextBox textBoxDouiAdd2;
        private System.Windows.Forms.TextBox textBoxDouiName;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBoxDouiFusho;
        private System.Windows.Forms.TextBox textBoxDouiAdd;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBoxTekiou2;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBoxTekiou;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxFushoFree2;
        private System.Windows.Forms.TextBox textBoxFushoFree;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelDoui;
        private System.Windows.Forms.Panel panelReceipt;
        private System.Windows.Forms.TextBox textBoxFutan2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFutan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Label labelNote1;
        private System.Windows.Forms.Label labelGroupID;
        private System.Windows.Forms.Label labelScanID;
        private System.Windows.Forms.Label labelSeikyu;
    }
}