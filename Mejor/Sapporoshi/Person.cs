﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.Sapporoshi
{
    class Person
    {
        /*
        CREATE TABLE person
        (
          pid serial NOT NULL,
          hnum text NOT NULL DEFAULT ''::text,
          pbirth date NOT NULL DEFAULT '0001-01-01'::date,
          psex integer,
          hname text NOT NULL,
          hadd text NOT NULL,
          pname text NOT NULL,
          verified boolean NOT NULL DEFAULT false,
          insert_uid integer NOT NULL DEFAULT 0,
          update_uid integer NOT NULL DEFAULT 0,
          lastaid integer NOT NULL DEFAULT 0,
          CONSTRAINT person_pkey PRIMARY KEY (pid),
          CONSTRAINT person_hnum_pbirth_key UNIQUE (hnum, pbirth)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE person
          OWNER TO postgres;

        CREATE INDEX person_hnum_pbirth_idx
          ON person
          USING btree
          (hnum COLLATE pg_catalog."default", pbirth);
        */

        public int PID { get; private set; }
        public string Hnum { get; private set; }
        public string Hadd { get; private set; }
        public DateTime Pbirth { get; private set; }
        public int Psex { get; private set; }
        public string Hname { get; private set; }
        public string Pname { get; private set; }
        public bool Verified { get; private set; }
        public int LastAID { get; private set; }

        private Person()
        {
            PID = 0;
        }

        public Person(App app)
        {
            PID = 0;
            this.Hnum = app.HihoNum;
            this.Hadd = app.HihoAdd;
            this.Pbirth = app.Birthday;
            this.Psex = app.Sex;
            this.Hname = app.HihoName;
            this.Pname = app.PersonName;
            this.LastAID = app.Aid;
        }

        /// <summary>
        /// 確定された被保険者氏名を返します　ない場合nullが返ります
        /// </summary>
        /// <param name="number"></param>
        /// <returns>被保険者氏名、住所のstring配列</returns>
        public static string[] GetVerifiedFamilyNameAndAdd(string number)
        {
            var sql = "SELECT hname, hadd FROM person " +
                "WHERE hnum=:hnum AND verified LIMIT 1";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlTypes.NpgsqlDbType.Text).Value = number;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;

                return new string[] { (string)res[0][0], (string)res[0][1] };
            }
        }

        public bool IsEqual(App app)
        {
            if (PID == 0) throw new Exception("データベースにない情報と比較しようとしました");
            if (this.Hnum != app.HihoNum) throw new Exception("被保険者番号が違う情報を比較しようとしました");
            if (this.Pbirth != app.Birthday) throw new Exception("生年月日が違う情報を比較しようとしました");

            return this.Hname == app.HihoName &&
                this.Psex == app.Sex &&
                this.Pname == app.PersonName &&
                this.Hadd == app.HihoAdd;
        }

        public static Person GetPerson(string number, DateTime birth)
        {
            var sql = "SELECT pid, hnum, hadd, pbirth, " +
                "psex, hname, pname, verified, lastaid " +
                "FROM person " +
                "WHERE hnum=:hnum AND pbirth=:pbirth";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlTypes.NpgsqlDbType.Text).Value = number;
                cmd.Parameters.Add("pbirth", NpgsqlTypes.NpgsqlDbType.Date).Value = birth;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var p = new Person();
                p.PID = (int)res[0][0];
                p.Hnum = (string)res[0][1];
                p.Hadd = (string)res[0][2];
                p.Pbirth = (DateTime)res[0][3];
                p.Psex = (int)res[0][4];
                p.Hname = (string)res[0][5];
                p.Pname = (string)res[0][6];
                p.Verified = (bool)res[0][7];
                p.LastAID = (int)res[0][8];
                return p;
            }
        }


        public bool Insert()
        {
            if (PID != 0) return false;

            var sql = "INSERT INTO person " +
                "(hnum, pbirth, psex, hname, hadd, pname, verified, insert_uid, lastaid) " +
                "VALUES (:hnum, :pbirth, :psex, :hname, :hadd, :pname, :verified, :inuid, :aid);";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = this.Hnum;
                cmd.Parameters.Add("hadd", NpgsqlDbType.Text).Value = this.Hadd;
                cmd.Parameters.Add("pbirth", NpgsqlDbType.Date).Value = this.Pbirth;
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = this.Psex;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = this.Hname;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = this.Pname;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("inuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.LastAID;
                return cmd.TryExecuteNonQuery();
            }
        }

        public bool Update(App app, bool forceVerify)
        {
            if (PID == 0) return false;
            if (this.Hnum != app.HihoNum) return false;
            if (this.Pbirth != app.Birthday) return false;

            if (IsEqual(app))
            {
                if (this.Verified) return true;

                //情報が一致し、情報元aidが違う場合にベリファイ済みとみなす
                this.Verified = forceVerify ? true : this.LastAID != app.Aid;
                this.LastAID = app.Aid;
            }
            else
            {
                this.Hname = app.HihoName;
                this.Hadd = app.HihoAdd;
                this.Psex = app.Sex;
                this.Pname = app.PersonName;
                this.LastAID = app.Aid;
                this.Verified = forceVerify ? true : false;
            }

            var sql = "UPDATE person " +
                "SET psex=:psex, hname=:hname, hadd=:hadd, pname=:pname, " +
                "verified=:verified, update_uid=:upuid, lastaid=:aid " +
                "WHERE pid=:pid";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = this.Psex;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = this.Hname;
                cmd.Parameters.Add("hadd", NpgsqlDbType.Text).Value = this.Hadd;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = this.Pname;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("upuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.LastAID;
                cmd.Parameters.Add("pid", NpgsqlDbType.Integer).Value = this.PID;
                return cmd.TryExecuteNonQuery();
            }
        }
    }
}
