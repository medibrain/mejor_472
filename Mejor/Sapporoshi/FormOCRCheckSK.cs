﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;
using System.Drawing.Imaging;

namespace Mejor.Sapporoshi
{
    public partial class FormOCRCheckSK : Form
    {
        private Dictionary<int, App> dic;
        private BindingSource bs = new BindingSource();

        private ScanGroup sg;
        int currentAid = -1;
        private bool firstTime;

        private int cYear;
        private int cMonth;


        // --------------------------------------------------------------------------------
        //
        // クラス定義部
        //
        // --------------------------------------------------------------------------------

        public enum a2tp { 未チェック = 0, First = 1, Other = 2 };

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point headerPos = new Point(0, 50);
        Point iryoPos = new Point(0, 150);
        Point costPos = new Point(0, 300);
        Point shomeiPos = new Point(0, 800);
        Point drNumPos = new Point(0, 900);

        Control[] headerControls, iryoControls, costControls, shomeiControls;
        
        // --------------------------------------------------------------------------------
        //
        // メイン部
        //
        // --------------------------------------------------------------------------------
        public FormOCRCheckSK(ScanGroup sGroup, bool firstTime)
        {
            InitializeComponent();

            headerControls = new Control[] { textBoxType, textBoxType2, textBoxY, textBoxY2, 
                textBoxM, textBoxM2, textBoxHnum, textBoxHnum2, textBoxRatio, textBoxRatio2,
                textBoxBirthday, textBoxBirthday2, textBoxBY, textBoxBY2, textBoxBM, 
                textBoxBM2, textBoxBD, textBoxBD2, textBoxSex, textBoxSex2, textBoxName, 
                textBoxName2,};

            iryoControls = new Control[] { textBoxShokenY, textBoxShokenY2, textBoxShokenM,
                textBoxShokenM2, textBoxShokenD, textBoxShokenD2, textBoxDays, 
                textBoxFusho, textBoxFusho2, textBoxFushoY, textBoxFushoY2, 
                textBoxFushoM, textBoxFushoM2, textBoxFushoD, textBoxFushoD2,
                textBoxFushoFree, textBoxFushoFree2, };

            costControls = new Control[] { textBoxOryo, textBoxOryo2, textBoxKasan, textBoxKasan2, 
                textBoxTotal, textBoxTotal2, textBoxCharge, textBoxCharge2, textBoxTekiou,
                textBoxTekiou2, textBoxShinryoDays, textBoxShinryoDays2, textBoxOryoDays,
                textBoxOryoDays2,};

            shomeiControls = new Control[] { textBoxDairiAdd, textBoxDairiAdd2, textBoxDairiName, 
                textBoxDairiName2, textBoxShomeiY, textBoxShomeiY2, textBoxShomeiM, 
                textBoxShomeiM2, textBoxShomeiD, textBoxShomeiD2,textBoxHosAdd,
                textBoxHosAdd2, textBoxHosName, textBoxHosName2, textBoxDrName,
                textBoxDrName2, textBoxIninY, textBoxIninY2, textBoxIninM,
                textBoxIninM2, textBoxIninD, textBoxIninD2, textBoxAdd, textBoxAdd2,
                textBoxFname, textBoxFname2, };


            panelDoui.Visible = false;
            panelReceipt.Visible = false;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += textBoxs_Enter;
                        item.Leave += textBoxs_Leave;
                    }
                    func(item);
                }
            });
            func(panelControls);

            this.sg = sGroup;
            cYear = sg.cyear;
            cMonth = sg.cmonth;
            this.firstTime = firstTime;

            //GIDで検索
            int gid = sg.GroupID;
            var list = App.GetAppsGID(gid);
            
            //ベリファイ入力時は申請書以外を最初から除外
            if (!firstTime) list = list.FindAll(a => a.MediYear > 0);

            dic = new Dictionary<int, App>();
            foreach (var item in list) dic.Add(item.Aid, item);
            bs.DataSource = list;
            dataGridViewPlist.DataSource = bs;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = false;

            labelSeikyu.Text = $"処理月: {sg.cyear}/{sg.cmonth}";
            labelNote1.Text = "Note: " + sg.note1.ToString();
            labelScanID.Text = "ScanID: " + sg.ScanID.ToString();
            labelGroupID.Text = "GroupID: " + sg.GroupID.ToString();

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void regist()
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (!appDBupdate(ri)) return;

            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri + 1];
        }

        //Nextボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        //キー入力監視
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
        }

        //EnterキーでTABキーの動作をさせる
        private void FormOCRCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        // --------------------------------------------------------------------------------
        //
        // 入力チェック部(文字種チェック→値チェック→一回目のみOCR結果との比較)
        //
        // --------------------------------------------------------------------------------
        
        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowindex"></param>
        /// <returns></returns>
        private bool checkApp(ref App app, out AppEx appex)
        {
            bool err = false;
            appex = new AppEx(app.Aid);

            var setError = new Action<TextBox>(t =>
                {
                    t.BackColor = Color.Pink;
                    err = true;
                });

            var setChecked = new Action<TextBox>(t =>
                {
                    if (!t.Enabled) return;
                    t.BackColor = SystemColors.Info;
                });

            //種別
            int appType;
            if (!int.TryParse(textBoxType.Text, out appType)) setError(textBoxType);
            else if (appType != 7 && appType != 8) setError(textBoxType);
            else setChecked(textBoxType);

            //年
            int year;
            if (!int.TryParse(textBoxY.Text, out year)) setError(textBoxY);
            else if (textBoxY.Text.Length != 2 || year < app.ChargeYear - 5) setError(textBoxY);
            else setChecked(textBoxY);

            //月
            int month;
            if (!int.TryParse(textBoxM.Text, out month)) setError(textBoxM);
            else if (month < 0 || month > 12) setError(textBoxM);
            else setChecked(textBoxM);

            //施術師コード
            if (string.IsNullOrWhiteSpace(textBoxDrCode.Text.Trim()))
                setError(textBoxDrCode);
            else
                setChecked(textBoxDrCode);

            //被保険者番号のチェック
            int hnumTemp;
            if (textBoxHnum.Text.Trim().Length != 7) setError(textBoxHnum);
            else if (!int.TryParse(textBoxHnum.Text.Trim(), out hnumTemp)) setError(textBoxHnum);
            else setChecked(textBoxHnum);
            var hnum = textBoxHnum.Text.Trim();

            //給付割合
            int ratio = 0;
            if (textBoxRatio.Text.Trim().Length != 0 && !int.TryParse(textBoxRatio.Text, out ratio)) setError(textBoxRatio);
            else if (ratio != 0 && ratio != 7 && ratio != 8 && ratio != 9) setError(textBoxRatio);
            else setChecked(textBoxRatio);

            //生年月日
            DateTime birth = DateTime.MinValue;
            int bG;
            if (!int.TryParse(textBoxBirthday.Text, out bG)) setError(textBoxBirthday);
            else if (bG < 1 || bG > 4) setError(textBoxBirthday);
            else setChecked(textBoxBirthday);

            int bY;
            if (!int.TryParse(textBoxBY.Text, out bY)) setError(textBoxBY);
            else
            {
                if (bY < 1 || bY > 65) setError(textBoxBY);
                else setChecked(textBoxBY);
            }

            int bM;
            if (!int.TryParse(textBoxBM.Text, out bM)) setError(textBoxBM);
            else
            {
                if (bM < 1 || bM > 12) setError(textBoxBM);
                else setChecked(textBoxBM);
            }

            int bD;
            if (!int.TryParse(textBoxBD.Text, out bD)) setError(textBoxBD);
            else
            {
                if (bD < 1 || bD > 31) setError(textBoxBD);
                else setChecked(textBoxBD);
            }

            var era = bG == 1 ? "明治" : bG == 2 ? "大正" : bG == 3 ? "昭和" : bG == 4 ? "平成" : "";
            era = era + textBoxBY.Text;
            int y = DateTimeEx.GetAdYear(era);

            if (!DateTimeEx.IsDate(y, bM, bD))
            {
                setError(textBoxBY);
                setError(textBoxBM);
                setError(textBoxBD);
            }
            else
            {
                birth = new DateTime(y, bM, bD);
            }

            //性別
            int sex;
            if (!int.TryParse(textBoxSex.Text, out sex)) setError(textBoxSex);
            else if (sex < 1 || sex > 2) setError(textBoxSex);
            else setChecked(textBoxSex);

            //療養者氏名
            if (textBoxName.Text.Length < 3) setError(textBoxName);

            //初検日
            var shokenDT = DateTime.MinValue;
            int sY;
            if (!int.TryParse(textBoxShokenY.Text, out sY)) setError(textBoxShokenY);
            else if (sY < 1 || sY > 65) setError(textBoxShokenY);
            else setChecked(textBoxShokenY);

            int sM;
            if (!int.TryParse(textBoxShokenM.Text, out sM)) setError(textBoxShokenM);
            else if (sM < 1 || sM > 12) setError(textBoxShokenM);
            else setChecked(textBoxShokenM);

            int sD;
            if (!int.TryParse(textBoxShokenD.Text, out sD)) setError(textBoxShokenD);
            else if (sD < 1 || sD > 31) setError(textBoxShokenD);
            else setChecked(textBoxShokenD);

            y = DateTimeEx.GetAdYear("平成" + textBoxShokenY.Text);
            if (!DateTimeEx.IsDate(y, sM, sD))
            {
                setError(textBoxShokenY);
                setError(textBoxShokenM);
                setError(textBoxShokenD);
            }
            else
            {
                shokenDT = new DateTime(y, sM, sD);
            }

            //診療日数
            int days;
            if (!int.TryParse(textBoxDays.Text, out days)) setError(textBoxDays);
            else if (days < 1 || days > 31) setError(textBoxDays);
            else setChecked(textBoxDays);

            //負傷名
            if (textBoxFusho.Text.Length < 3) setError(textBoxFusho);
            else setChecked(textBoxFusho);

            //負傷日
            DateTime fushoDT = DateTime.MinValue;
            string fushoTextDT = string.Empty;
            if (!string.IsNullOrWhiteSpace(textBoxFushoFree.Text))
            {
                if (!string.IsNullOrWhiteSpace(textBoxFushoY.Text) ||
                    !string.IsNullOrWhiteSpace(textBoxFushoM.Text) ||
                    !string.IsNullOrWhiteSpace(textBoxFushoD.Text))
                {
                    setError(textBoxFushoY);
                    setError(textBoxFushoM);
                    setError(textBoxFushoD);
                    setError(textBoxFushoFree);
                }
                else
                {
                    setChecked(textBoxFushoY);
                    setChecked(textBoxFushoM);
                    setChecked(textBoxFushoD);
                    setChecked(textBoxFushoFree);
                    fushoTextDT = Utility.ToHalfWithoutKatakana(textBoxFushoFree.Text);
                }
            }
            else
            {
                setChecked(textBoxFushoFree);

                int fY;
                if (!int.TryParse(textBoxFushoY.Text, out fY)) setError(textBoxFushoY);
                else if (fY < 1 || fY > 65) setError(textBoxFushoY);
                else setChecked(textBoxFushoY);

                int fM;
                if (!int.TryParse(textBoxFushoM.Text, out fM)) setError(textBoxFushoM);
                else if (fM < 1 || fM > 12) setError(textBoxFushoM);
                else setChecked(textBoxFushoM);

                int fD;
                if (!int.TryParse(textBoxFushoD.Text, out fD)) setError(textBoxFushoD);
                else if (fD < 1 || fD > 31) setError(textBoxFushoD);
                else setChecked(textBoxFushoD);

                y = DateTimeEx.GetAdYear("平成" + textBoxFushoY.Text);

                if (!DateTimeEx.IsDate(y, fM, fD))
                {
                    setError(textBoxFushoY);
                    setError(textBoxFushoM);
                    setError(textBoxFushoD);
                }
                else
                {
                    fushoDT = new DateTime(y, fM, fD);
                }
            }

            //往療金額
            int oryo = 0;
            if (textBoxOryo.Text.Trim().Length!= 0 && !int.TryParse(textBoxOryo.Text, out oryo)) setError(textBoxOryo);
            else if (oryo < 0 || oryo > 200000) setError(textBoxOryo);
            else setChecked(textBoxOryo);

            //加算金額
            int kasan = 0;
            if (textBoxKasan.Text.Trim().Length != 0 && !int.TryParse(textBoxKasan.Text, out kasan)) setError(textBoxKasan);
            else if (kasan < 0 || kasan > 200000) setError(textBoxKasan);
            else setChecked(textBoxKasan);

            //合計金額
            int total;
            if (!int.TryParse(textBoxTotal.Text, out total)) setError(textBoxTotal);
            else if (total < 100 || total > 300000) setError(textBoxTotal);
            else setChecked(textBoxTotal);

            //請求金額
            int charge = 0;
            if (textBoxCharge.Text.Trim().Length != 0 && !int.TryParse(textBoxCharge.Text, out charge)) setError(textBoxCharge);
            else if (charge > total) setError(textBoxCharge);
            else setChecked(textBoxCharge);

            //負担金額
            int futan = 0;
            if (textBoxFutan.Text.Trim().Length != 0 && !int.TryParse(textBoxFutan.Text, out futan)) setError(textBoxFutan);
            else if (charge + futan > total) setError(textBoxFutan);
            else setChecked(textBoxFutan);

            //適応
            int tekiou;
            if (!int.TryParse(textBoxTekiou.Text, out tekiou)) setError(textBoxTekiou);
            else if (tekiou < 1 || tekiou > 2) setError(textBoxTekiou);
            else setChecked(textBoxTekiou);

            //診療日
            string sinryoDayList = string.Empty;
            if (string.IsNullOrWhiteSpace(textBoxShinryoDays.Text)) setError(textBoxShinryoDays);
            var sdays = Utility.ToHalfWithoutKatakana(textBoxShinryoDays.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            bool sdayserror = false;
            int lastd = 0;
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item == "*")continue;

                if (int.TryParse(item, out d))
                {
                    l.Add(item.Trim());
                }
                else
                {
                    setError(textBoxShinryoDays);
                    sdayserror = true;
                }

                //2016.3.7 松本 31日を超えていないか、日付順かチェック
                if (d > 31) sdayserror = true;
                if (lastd >= d) sdayserror = true;
                lastd = d;
            }

            if (textBoxShinryoDays.Text == "*")
            {
                //2016.3.25 松本
                //申請書に日付一覧の記入がいない場合の対処
                setChecked(textBoxShinryoDays);
                sinryoDayList = string.Empty;
            }
            else if (sdayserror || l.Count == 0)
            {
                setError(textBoxShinryoDays);
            }
            else
            {
                setChecked(textBoxShinryoDays);
                sinryoDayList = string.Join(" ", l);
            }

            //往療日
            string oryoDayList = string.Empty;
            int oryoDays = 0;
            var odays = Utility.ToHalfWithoutKatakana(textBoxOryoDays.Text.Trim()).Split(" ,./".ToCharArray());
            l.Clear();
            lastd = 0;
            bool odaysError = false;
            foreach (var item in odays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                
                if(int.TryParse(item, out d))
                {
                    l.Add(item.Trim());
                }
                else
                {
                    setError(textBoxOryoDays);
                    odaysError = true;
                }

                //2016.3.7 松本 31日を超えていないか、日付順かチェック
                if (d > 31) sdayserror = true;
                if (lastd >= d) sdayserror = true;
                lastd = d;
            }
            oryoDays = l.Count;
            if (odaysError || ((oryo + kasan) != 0 && l.Count == 0))
            {
                setChecked(textBoxOryoDays);
            }
            else
            {
                setChecked(textBoxOryoDays);
                oryoDayList = string.Join(" ", l);
            }

            //代理人住所
            //チェックなし

            //代理人氏名
            //チェックなし

            //証明日付
            var shomeiDT = DateTime.MinValue;
            int smY;
            if (!int.TryParse(textBoxShomeiY.Text, out smY)) setError(textBoxShomeiY);
            else if (smY < 1 || smY > 65) setError(textBoxShomeiY);
            else setChecked(textBoxShomeiY);

            int smM;
            if (!int.TryParse(textBoxShomeiM.Text, out smM)) setError(textBoxShomeiM);
            else if (smM < 1 || smM > 12) setError(textBoxShomeiM);
            else setChecked(textBoxShomeiM);

            int smD;
            if (!int.TryParse(textBoxShomeiD.Text, out smD)) setError(textBoxShomeiD);
            else if (smD < 1 || smD > 31) setError(textBoxShomeiD);
            else setChecked(textBoxShomeiD);

            y = DateTimeEx.GetAdYear("平成" + textBoxShomeiY.Text);
            if (!DateTimeEx.IsDate(y, smM, smD))
            {
                setError(textBoxShokenY);
                setError(textBoxShokenM);
                setError(textBoxShokenD);
            }
            else
            {
                shomeiDT = new DateTime(y, smM, smD);
            }

            //施術所住所
            if (textBoxHosAdd.Text.Length < 3) setError(textBoxHosAdd);
            else setChecked(textBoxHosAdd);

            //施術師名
            if (textBoxDrName.Text.Length < 3) setError(textBoxDrName);
            else setChecked(textBoxDrName);

            //委任日付
            var iniDT = DateTime.MinValue;
            int iY;
            if (!int.TryParse(textBoxIninY.Text, out iY)) setError(textBoxIninY);
            else if (iY < 1 || iY > 65) setError(textBoxIninY);
            else setChecked(textBoxIninY);

            int iM;
            if (!int.TryParse(textBoxIninM.Text, out iM)) setError(textBoxIninM);
            else if (iM < 1 || iM > 12) setError(textBoxIninM);
            else setChecked(textBoxIninM);

            int iD;
            if (!int.TryParse(textBoxIninD.Text, out iD)) setError(textBoxIninD);
            else if (iD < 1 || iD > 31) setError(textBoxIninD);
            else setChecked(textBoxIninD);

            y = DateTimeEx.GetAdYear("平成" + textBoxIninY.Text);
            if (!DateTimeEx.IsDate(y, iM, iD))
            {
                setError(textBoxIninY);
                setError(textBoxIninM);
                setError(textBoxIninD);
            }
            else
            {
                iniDT = new DateTime(y, iM, iD);
            }

            //被保険者住所 委任住所
            if (textBoxAdd.Text.Length < 3) setError(textBoxAdd);
            else setChecked(textBoxAdd);

            //被保険者氏名 委任氏名
            if (textBoxFname.Text.Length < 3) setError(textBoxFname);
            else setChecked(textBoxFname);

            if (err)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            //金額でのエラーがあればいったん登録中断
            bool ratioError = (int)(total * ratio / 10) != charge;
            if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != charge;

            if (ratioError && charge != 0)
            {
                textBoxTotal.BackColor = Color.GreenYellow;
                textBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //施術所名がない場合もメッセージで確認
            if (firstTime && textBoxHosName.Text.Length < 3)
            {
                textBoxHosName.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("施術名が指定されていません。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }
            else setChecked(textBoxHosName);

            //値の反映
            app.AppType = appType == 7 ? APP_TYPE.鍼灸 : appType == 8 ? APP_TYPE.あんま : APP_TYPE.NULL;
            app.MediYear = year;
            app.MediMonth = month;
            app.DrNum = textBoxDrCode.Text.Trim();
            app.HihoNum = hnum;
            app.Ratio = ratio;
            app.Birthday = birth;
            app.Sex = sex;
            app.PersonName = textBoxName.Text.Trim();
            app.FushoFirstDate1 = shokenDT;
            app.CountedDays = days;
            app.FushoName1 = textBoxFusho.Text.Trim();
            app.FushoDate1 = fushoDT;
            appex.FushoDate = fushoTextDT;
            app.VisitFee = oryo;
            app.VisitAdd = kasan;
            app.Total = total;
            app.Charge = charge;
            app.Partial = futan;
            appex.TekiouUmu = tekiou == 1;
            appex.SejyutuDayList = sinryoDayList;
            appex.OryoDayList = oryoDayList;
            appex.OryoDays = oryoDays;
            appex.DairiAdd = textBoxDairiAdd.Text.Trim();
            appex.DairiName = textBoxDairiName.Text.Trim();
            appex.ShomeiDate = shomeiDT;
            appex.DrAdd = textBoxHosAdd.Text.Trim();
            app.ClinicName = textBoxHosName.Text.Trim();
            app.DrName = textBoxDrName.Text.Trim();
            appex.IninDate = iniDT;
            app.HihoAdd = textBoxAdd.Text.Trim();
            app.HihoName = textBoxFname.Text.Trim();
            return true;
        }

        private bool checkDoui(ref App app, out AppEx appex)
        {
            bool err = false;
            appex = new AppEx(app.Aid);

            var setError = new Action<TextBox>(t =>
                {
                    t.BackColor = Color.Pink;
                    err = true;
                });

            var setChecked = new Action<TextBox>(t =>
                {
                    if (!t.Enabled) return;
                    t.BackColor = SystemColors.Info;
                });

            //同意者氏名
            if (string.IsNullOrWhiteSpace(textBoxDouiName.Text.Trim()))
                setError(textBoxDouiName);
            else
                setChecked(textBoxDouiName);

            //同意者住所
            if (string.IsNullOrWhiteSpace(textBoxDouiAdd.Text.Trim()))
                setError(textBoxDouiAdd);
            else
                setChecked(textBoxDouiAdd);

            //同意年月日
            var DouiDT = DateTime.MinValue;
            int dY;
            if (!int.TryParse(textBoxDouiY.Text, out dY)) setError(textBoxDouiY);
            else if (dY < 1 || dY > 65) setError(textBoxDouiY);
            else setChecked(textBoxDouiY);

            int dM;
            if (!int.TryParse(textBoxDouiM.Text, out dM)) setError(textBoxDouiM);
            else if (dM < 1 || dM > 12) setError(textBoxDouiM);
            else setChecked(textBoxDouiM);

            int dD;
            if (!int.TryParse(textBoxDouiD.Text, out dD)) setError(textBoxDouiD);
            else if (dD < 1 || dD > 31) setError(textBoxDouiD);
            else setChecked(textBoxDouiD);

            int y = DateTimeEx.GetAdYear("平成" + textBoxDouiY.Text);
            if (!DateTimeEx.IsDate(y, dM, dD))
            {
                setError(textBoxDouiY);
                setError(textBoxDouiM);
                setError(textBoxDouiD);
            }
            else
            {
                DouiDT = new DateTime(y, dM, dD);
            }

            //同意負傷名
            if (string.IsNullOrWhiteSpace(textBoxDouiFusho.Text.Trim()))
                setError(textBoxDouiFusho);
            else
                setChecked(textBoxDouiFusho);

            if (err) return false;

            //値記録
            appex.DouiName = textBoxDouiName.Text.Trim();
            appex.DouiAdd = textBoxDouiAdd.Text.Trim();
            appex.DouiDate = DouiDT;
            appex.DouiFusho = textBoxDouiFusho.Text.Trim();
            return true;
        }

        // --------------------------------------------------------------------------------
        //
        // Application操作部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 現在の行のApplicationを表示します
        /// </summary>
        private void currentRowAppRead()
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri < 0) return;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            //次のApplicationを表示
            displayDataReflesh(r);

            //textBoxYにフォーカスを移動
            focusBack(false);
        }

        /// <summary>
        /// Applicationの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool appDBupdate(int rowIndex)
        {
            if (rowIndex == -1) return false;
            var aid = (int)dataGridViewPlist["Aid", rowIndex].Value;

            if (currentAid != aid)
            {
                MessageBox.Show("更新しようとしているデータと現在表示中のデータが一致しません。"+
                    "ご迷惑をおかけしますが、システムエラーを回避するため、この画面を閉じます。"+
                    "再度このグループを開いて作業を行ってください。", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return false;
            }

            App app = null;
            AppEx appEx = null;

            //ベリファイチェック時
            if (!firstTime)
            {
                app = dic[aid];
                if (app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

                if (!checkVerify())
                {
                    MessageBox.Show("データが一致していません。" +
                        "赤で示されたデータをご確認ください。登録できません。",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    focusBack(true);
                    return false;
                }
            }


            if (textBoxType.Text == "**")
            {
                //同意書
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.同意書;

                //データチェック
                if (!checkDoui(ref app, out appEx))
                {
                    //データベースから読み直し
                    dic[aid] = App.GetApp(aid);
                    focusBack(true);
                    return false;
                }
            }
            else if (textBoxType.Text == "//")
            {
                //往療内訳
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.往療内訳;
            }
            else if (textBoxType.Text == "--")
            {
                //続紙の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
            }
            else if (textBoxType.Text == "++")
            {
                //白バッジ、その他の場合
                app = dic[aid];
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
            }
            else
            {
                //データチェック
                app = dic[aid];
                if (!checkApp(ref app, out appEx))
                {
                    //データベースから読み直し
                    dic[aid] = App.GetApp(aid);
                    focusBack(true);
                    return false;
                }

                //被保険者情報の管理
                var p = Person.GetPerson(app.HihoNum, app.Birthday);
                if (p == null)
                {
                    p = new Person(app);
                    p.Insert();
                }
                else if (firstTime)
                {
                    if (!p.IsEqual(app))
                    {
                        if (MessageBox.Show("過去に入力された被保険者データと一致しません。" +
                            "被保険者データを更新し、入力データを登録してもよろしいですか？",
                            "被保険者情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    p.Update(app, !firstTime);
                }

                //施術師情報の管理
                var d = Doctor.GetDoctor(app.DrNum);
                if (d == null)
                {
                    d = new Doctor(app, appEx);
                    d.Insert();
                }
                else if (firstTime)
                {
                    if (!d.IsEqual(app, appEx))
                    {
                        if (MessageBox.Show("過去に入力された施術師データと一致しません。" +
                            "施術師データを更新し、入力データを登録してもよろしいですか？",
                            "施術師情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    d.Update(app, appEx, !firstTime);
                }
            }

            //データベースへ反映
            var upType = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
            using (var tran = DB.Main.CreateTransaction())
            {
                if (app.Update(User.CurrentUser.UserID, upType, tran) &&
                    (appEx == null || appEx.Upsert(tran)))
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// 次のApplicationを表示します
        /// </summary>
        /// <param name="app"></param>
        private void displayDataReflesh(App app)
        {
            currentAid = app.Aid;

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Text = "";
                        item.BackColor = SystemColors.Info;
                    }
                    func(item);
                }
            });
            func(panelControls);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(app.Ufirst) +
                "  2:" + User.GetUserName(app.Usecond);
            
            if (!firstTime)
            {
                //ベリファイ入力時
                var cai = new CheckInputAall(dic[app.Aid]);
                setVerify(cai);
            }
            else
            {
                //App_Flagのチェック
                if (!app.StatusFlagCheck(StatusFlag.入力済))
                {
                    var ocr = app.OcrData.Split(',');
                    //一度もチェックしていない画像
                    if (ocr.Length > 40 && ocr[40] != null)
                    {
                        var f = Fusho.Change(ocr[40]);
                        if (Fusho.BasicWordsCheck(f)) textBoxFusho.Text = f;
                    }
                }
                else
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    displayDataRefleshFromRcd(app);
                }
            }

            //画像の表示
            setImage(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string dir = Settings.ImageFolder;
            dir += "\\" + DB.GetMainDBName() + "\\" + app.ScanID.ToString();
            string fn = app.GetImageFullPath();

            try
            {
                //メイン画像の表示
                var img = System.Drawing.Image.FromFile(fn);

                labelImageName.Text = fn + " )";

                //通常表示
                float ratio2 = 0.4f;// 0.3f;
                var act = new Action<PictureBox>((pb) =>
                {
                    pb.Size = new Size((int)(img.Width * ratio2), (int)(img.Height * ratio2));
                    pb.SizeMode = PictureBoxSizeMode.Zoom;
                    pb.Image = img;
                });
                act(pictureBoxIN);
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="a"></param>
        private void displayDataRefleshFromRcd(App a)
        {
            //OCRチェックが済んだ画像の場合
            if(a.MediYear ==(int)APP_SPECIAL_CODE.同意書)
            {
                textBoxType.Text = "**";
                var ax = AppEx.Select(a.Aid);
                textBoxDouiName.Text = ax.DouiName;
                textBoxDouiAdd.Text = ax.DouiAdd;
                if (ax.DouiDate != DateTime.MinValue)
                {
                    textBoxDouiY.Text = DateTimeEx.GetJpYear(ax.DouiDate).ToString();
                    textBoxDouiM.Text = ax.DouiDate.Month.ToString();
                    textBoxDouiD.Text = ax.DouiDate.Day.ToString();
                }
                textBoxDouiFusho.Text = ax.DouiFusho;
            }
            else if(a.MediYear==(int)APP_SPECIAL_CODE.往療内訳)
            {
                textBoxType.Text = "//";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                textBoxType.Text = "--";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                textBoxType.Text = "++";
            }
            else
            {
                var ax = AppEx.Select(a.Aid);
                textBoxType.Text = a.AppType == APP_TYPE.鍼灸 ? "7" : a.AppType == APP_TYPE.あんま ? "8" : "";
                textBoxY.Text = a.MediYear.ToString();
                textBoxM.Text = a.MediMonth.ToString();
                textBoxDrCode.Text = a.DrNum;
                textBoxHnum.Text = a.HihoNum;
                textBoxRatio.Text = a.Ratio == 0 ? string.Empty : a.Ratio.ToString();
                textBoxBirthday.Text = DateTimeEx.GetEraNumber(a.Birthday).ToString();
                textBoxBY.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM.Text = a.Birthday.Month.ToString();
                textBoxBD.Text = a.Birthday.Day.ToString();
                textBoxSex.Text = a.Sex.ToString();
                textBoxName.Text = a.PersonName;
                textBoxShokenY.Text = DateTimeEx.GetJpYear(a.FushoFirstDate1).ToString();
                textBoxShokenM.Text = a.FushoFirstDate1.Month.ToString();
                textBoxShokenD.Text = a.FushoFirstDate1.Day.ToString();
                textBoxDays.Text = a.CountedDays.ToString();
                textBoxFusho.Text = a.FushoName1;
                if (a.FushoDate1 != DateTime.MinValue)
                {
                    textBoxFushoY.Text = DateTimeEx.GetJpYear(a.FushoDate1).ToString();
                    textBoxFushoM.Text = a.FushoDate1.Month.ToString();
                    textBoxFushoD.Text = a.FushoDate1.Day.ToString();
                }
                textBoxFushoFree.Text = ax.FushoDate;
                textBoxOryo.Text = a.VisitFee == 0 ? string.Empty : a.VisitFee.ToString();
                textBoxKasan.Text = a.VisitAdd == 0  ? string.Empty : a.VisitAdd.ToString();
                textBoxTotal.Text = a.Total.ToString();
                textBoxCharge.Text = a.Charge == 0 ? string.Empty : a.Charge.ToString();
                textBoxFutan.Text = a.Partial == 0 ? string.Empty : a.Partial.ToString();
                textBoxTekiou.Text = ax.TekiouUmu ? "1" : "2";
                textBoxShinryoDays.Text = ax.SejyutuDayList == string.Empty ? "*" : ax.SejyutuDayList;
                textBoxOryoDays.Text = ax.OryoDayList;
                textBoxDairiAdd.Text = ax.DairiAdd;
                textBoxDairiName.Text = ax.DairiName;
                if (ax.ShomeiDate != DateTime.MinValue)
                {
                    textBoxShomeiY.Text = DateTimeEx.GetJpYear(ax.ShomeiDate).ToString();
                    textBoxShomeiM.Text = ax.ShomeiDate.Month.ToString();
                    textBoxShomeiD.Text = ax.ShomeiDate.Day.ToString();
                }
                textBoxHosAdd.Text = ax.DrAdd;
                textBoxHosName.Text = a.ClinicName;
                textBoxDrName.Text = a.DrName;
                if (ax.IninDate != DateTime.MinValue)
                {
                    textBoxIninY.Text = DateTimeEx.GetJpYear(ax.IninDate).ToString();
                    textBoxIninM.Text = ax.IninDate.Month.ToString();
                    textBoxIninD.Text = ax.IninDate.Day.ToString();
                }
                textBoxAdd.Text = a.HihoAdd;
                textBoxFname.Text = a.HihoName;
            }
        }

        private int getBuiCount(App r)
        {
            int c = 1;
            if (r.FushoName2 != string.Empty) c++;
            if (r.FushoName3 != string.Empty) c++;
            if (r.FushoName4 != string.Empty) c++;
            if (r.FushoName5 != string.Empty) c++;
            return c;
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxType_TextChanged(object sender, EventArgs e)
        {
            if (textBoxType.Text == "**")
            {
                panelDoui.Visible = true;
                panelReceipt.Visible = false;
            }
            else if (textBoxType.Text == "8" || textBoxType.Text == "9")
            {
                panelDoui.Visible = false;
                panelReceipt.Visible = true;
            }
            else
            {
                panelDoui.Visible = false;
                panelReceipt.Visible = false;
            }
        }
        
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            var app = imageDispose();
            ImageUtility.ImageRotate(app.GetImageFullPath(), true);

            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            var app = imageDispose();
            ImageUtility.ImageRotate(app.GetImageFullPath(), false);

            setImage(app);
        }



        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var apl = imageDispose();
            var fn = apl.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(apl);
        }

        private App imageDispose()
        {
            if (pictureBoxIN.Image != null) pictureBoxIN.Image.Dispose();

            var ri = dataGridViewPlist.CurrentCell.RowIndex;

            //aIdで一意のAppインスタンスを選択
            var aid = (int)dataGridViewPlist["Aid", ri].Value;
            var r = dic[aid];

            return r;
        }

        private void textBoxs_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == Color.LightCyan) t.BackColor = SystemColors.Info;
        }

        private void textBoxs_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            //コントロール部分のスクロール
            int y = -panelControlScroll.AutoScrollPosition.Y;
            if (panelControlScroll.Height + y - 160 < t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, panelControlScroll.Height);
            }
            else if (y + 100 > t.Location.Y)
            {
                panelControlScroll.AutoScrollPosition = new Point(0, 0);
            }

            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;


            if (headerControls.Contains(t)) panelIN.AutoScrollPosition = headerPos;
            else if (headerControls.Contains(t)) panelIN.AutoScrollPosition = headerPos;
            else if (iryoControls.Contains(t)) panelIN.AutoScrollPosition = iryoPos;
            else if (costControls.Contains(t)) panelIN.AutoScrollPosition = costPos;
            else if (shomeiControls.Contains(t)) panelIN.AutoScrollPosition = shomeiPos;
            else if (textBoxDrCode == t) panelIN.AutoScrollPosition = drNumPos;
        }


        private void focusBack(bool infoColorFocus)
        {
            Control[] controls = { textBoxType, textBoxY, textBoxM, textBoxDrCode,
                    textBoxHnum, textBoxRatio, textBoxBirthday, textBoxBY, textBoxBM, textBoxBD, 
                    textBoxSex, textBoxName, textBoxShokenY, textBoxShokenM, textBoxShokenD, 
                    textBoxDays, textBoxFusho, textBoxFushoY, textBoxFushoM,textBoxFushoD,
                    textBoxFushoFree, textBoxOryo,textBoxKasan, textBoxTotal, textBoxCharge,
                    textBoxTekiou, textBoxShinryoDays, textBoxOryoDays,
                    textBoxDairiAdd,textBoxDairiName,textBoxShomeiY,textBoxShomeiM,textBoxShomeiD,
                    textBoxHosAdd, textBoxHosName,textBoxDrName,
                    textBoxIninY,textBoxIninM, textBoxIninD, textBoxAdd, textBoxFname,
                    textBoxDouiName, textBoxDouiAdd, 
                    textBoxDouiY,textBoxDouiM,textBoxDouiD,textBoxDouiFusho};

            if (infoColorFocus)
            {
                foreach (var item in controls)
                {
                    if (item.BackColor != SystemColors.Info)
                    {
                        item.Focus();
                        return;
                    }
                }
            }

            foreach (var item in controls)
            {
                if (item.Enabled)
                {
                    item.Focus();
                    return;
                }
            }

            buttonRegist.Focus();
        }

        private void panelIN_Scroll(object sender, ScrollEventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = new Point(-panelIN.AutoScrollPosition.X, -panelIN.AutoScrollPosition.Y);

            if (headerControls.Contains(t)) headerPos = pos;
            else if (headerControls.Contains(t)) headerPos = pos;
            else if (iryoControls.Contains(t)) iryoPos = pos;
            else if (costControls.Contains(t)) costPos = pos;
            else if (shomeiControls.Contains(t)) shomeiPos = pos;
            else if (textBoxDrCode == t) drNumPos = pos;
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(CheckInputAall ca)
        {   
            var a = ca.app;
            var ax = AppEx.Select(a.Aid);

            //1回目入力の値を挿入
            if(a.MediYear == (int)APP_SPECIAL_CODE.往療内訳)
            {
                textBoxType2.Text = "//";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                textBoxType2.Text = "--";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                textBoxType2.Text = "++";
            }
            else if (a.MediYear == (int)APP_SPECIAL_CODE.同意書)
            {
                textBoxType2.Text = "**";
                textBoxDouiName2.Text = ax.DouiName;
                textBoxDouiAdd2.Text = ax.DouiAdd;
                if (ax.DouiDate != DateTime.MinValue)
                {
                    textBoxDouiY2.Text = DateTimeEx.GetJpYear(ax.DouiDate).ToString();
                    textBoxDouiM2.Text = ax.DouiDate.Month.ToString();
                    textBoxDouiD2.Text = ax.DouiDate.Day.ToString();
                }
                textBoxDouiFusho2.Text = ax.DouiFusho;
            }
            else
            {
                textBoxType2.Text = a.AppType == APP_TYPE.鍼灸 ? "7" : a.AppType == APP_TYPE.あんま ? "8" : "";
                textBoxY2.Text = a.MediYear.ToString();
                textBoxM2.Text = a.MediMonth.ToString();

                //note1に「区」が含まれ、機関コードに*が含まれている場合
                //医療機関コード以外の判別コードで入力不要
                if (sg.note1.Contains("区") && a.DrNum.Contains("*"))
                {
                    textBoxDrCode.Text = a.DrNum;
                    textBoxDrCode.Enabled = false;
                }
                else
                {
                    textBoxDrCode2.Text = a.DrNum;
                    textBoxDrCode.Enabled = true;
                }

                textBoxHnum2.Text = a.HihoNum;
                textBoxRatio2.Text = a.Ratio.ToString();
                textBoxBirthday2.Text = DateTimeEx.GetEraNumber(a.Birthday).ToString();
                textBoxBY2.Text = DateTimeEx.GetJpYear(a.Birthday).ToString();
                textBoxBM2.Text = a.Birthday.Month.ToString();
                textBoxBD2.Text = a.Birthday.Day.ToString();
                textBoxSex2.Text = a.Sex.ToString();
                textBoxName2.Text = a.PersonName;
                textBoxTotal2.Text = a.Total.ToString();
                textBoxTekiou2.Text = ax.TekiouUmu ? "1" : "2";

                //ここから先はベリファイなし
                textBoxShokenY.Text = DateTimeEx.GetJpYear(a.FushoFirstDate1).ToString();
                textBoxShokenM.Text = a.FushoFirstDate1.Month.ToString();
                textBoxShokenD.Text = a.FushoFirstDate1.Day.ToString();
                textBoxDays.Text = a.CountedDays.ToString();
                textBoxFusho.Text = a.FushoName1;
                if (a.FushoDate1 != DateTime.MinValue)
                {
                    textBoxFushoY.Text = DateTimeEx.GetJpYear(a.FushoDate1).ToString();
                    textBoxFushoM.Text = a.FushoDate1.Month.ToString();
                    textBoxFushoD.Text = a.FushoDate1.Day.ToString();
                }
                textBoxFushoFree.Text = ax.FushoDate;
                textBoxOryo.Text = a.VisitFee.ToString();
                textBoxKasan.Text = a.VisitAdd.ToString();

                textBoxCharge.Text = a.Charge.ToString();
                textBoxShinryoDays.Text = ax.SejyutuDayList==string.Empty ? "*":ax.SejyutuDayList;
                textBoxOryoDays.Text = ax.OryoDayList;
                textBoxDairiAdd.Text = ax.DairiAdd;
                textBoxDairiName.Text = ax.DairiName;
                if (ax.ShomeiDate != DateTime.MinValue)
                {
                    textBoxShomeiY.Text = DateTimeEx.GetJpYear(ax.ShomeiDate).ToString();
                    textBoxShomeiM.Text = ax.ShomeiDate.Month.ToString();
                    textBoxShomeiD.Text = ax.ShomeiDate.Day.ToString();
                }
                textBoxHosAdd.Text = ax.DrAdd;
                textBoxHosName.Text = a.ClinicName;
                textBoxDrName.Text = a.DrName;
                if (ax.IninDate != DateTime.MinValue)
                {
                    textBoxIninY.Text = DateTimeEx.GetJpYear(ax.IninDate).ToString();
                    textBoxIninM.Text = ax.IninDate.Month.ToString();
                    textBoxIninD.Text = ax.IninDate.Day.ToString();
                }
                textBoxAdd.Text = a.HihoAdd;
                textBoxFname.Text = a.HihoName;
                textBoxFutan.Text = a.Partial.ToString();

                //ベリファイない分のBoxを無効化
                textBoxShokenY.Enabled = false;
                textBoxShokenM.Enabled = false;
                textBoxShokenD.Enabled = false;
                textBoxDays.Enabled = false;
                textBoxFusho.Enabled = false;
                textBoxFushoY.Enabled = false;
                textBoxFushoM.Enabled = false;
                textBoxFushoD.Enabled = false;
                textBoxFushoFree.Enabled = false;
                textBoxOryo.Enabled = false;
                textBoxKasan.Enabled = false;
                textBoxFutan.Enabled = false;

                textBoxCharge.Enabled = false;
                textBoxShinryoDays.Enabled = false;
                textBoxOryoDays.Enabled = false;
                textBoxDairiAdd.Enabled = false;
                textBoxDairiName.Enabled = false;
                textBoxShomeiY.Enabled = false;
                textBoxShomeiM.Enabled = false;
                textBoxShomeiD.Enabled = false;
                textBoxHosAdd.Enabled = false;
                textBoxHosName.Enabled = false;
                textBoxDrName.Enabled = false;
                textBoxIninY.Enabled = false;
                textBoxIninM.Enabled = false;
                textBoxIninD.Enabled = false;
                textBoxAdd.Enabled = false;
                textBoxFname.Enabled = false;
            }
            
            //エラーに応じてテキストボックスを有効化、値設定
            var act = new Action<TextBox, bool, string>((t, b, s) =>
                {
                    if (a.StatusFlagCheck(StatusFlag.ベリファイ済))
                    {
                        t.Enabled = false;
                        t.BackColor = SystemColors.Control;
                        t.Text = s;
                    }
                    else
                    {
                        t.Enabled = b;
                        t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                        t.Text = b ? string.Empty : s;
                    }
                });

            //これらの項目はかならず入力、ベリファイ済みの時のみ表示
            if (a.StatusFlagCheck(StatusFlag.ベリファイ済))
            {
                act(textBoxRatio, false, a.Ratio.ToString());
                act(textBoxSex, false, a.Sex.ToString());
                act(textBoxTekiou, false, ax.TekiouUmu ? "1" : "2");
            }
            else
            {
                act(textBoxRatio, true, a.Ratio.ToString());
                act(textBoxSex, true, a.Sex.ToString());
                act(textBoxTekiou, true, ax.TekiouUmu ? "1" : "2");
            }
            
            act(textBoxY, ca.ErrAyear, a.MediYear == -3 ? "--" : a.MediYear == -4 ? "++" : a.MediYear.ToString());
            if (a.MediYear > 0)
            {
                var fn = Person.GetVerifiedFamilyNameAndAdd(a.HihoNum);
                var p = Person.GetPerson(a.HihoNum, a.Birthday);
                var d = Doctor.GetDoctor(a.DrNum);

                act(textBoxY, ca.ErrAyear, a.MediYear.ToString());
                act(textBoxM, ca.ErrAmonth, a.MediMonth.ToString());
                act(textBoxHnum, ca.ErrHnum, a.HihoNum);

                act(textBoxBirthday, ca.ErrBirth, DateTimeEx.GetEraNumber(a.Birthday).ToString());
                act(textBoxBY, ca.ErrBirth, DateTimeEx.GetJpYear(a.Birthday).ToString());
                act(textBoxBM, ca.ErrBirth, a.Birthday.Month.ToString());
                act(textBoxBD, ca.ErrBirth, a.Birthday.Day.ToString());
                act(textBoxName, p == null || !p.Verified, a.PersonName);
                var familyNeedCheck = ca.ErrHnum || fn == null;
                
                //act(textBoxFname, familyNeedCheck, a.Hname);
                var personNeedCheck = ca.ErrHnum || ca.ErrBirth || p == null || !p.Verified;
                act(textBoxName, personNeedCheck, a.PersonName);

                //textBoxFusho.Text = Fusho.ToRegularName(textBoxFusho.Text);
                //var iname1 = Fusho.ToRegularName(a.Iname1);
                //act(textBoxFusho, ca.ErrFusho, iname1);

                //act(textBoxShokenY, ca.ErrShokenDate, DateTimeEx.GetJyear(a.Ifirstdate1).ToString());
                //act(textBoxShokenM, ca.ErrShokenDate, a.Ifirstdate1.Month.ToString());
                //act(textBoxShokenD, ca.ErrShokenDate, a.Ifirstdate1.Day.ToString());
                //act(textBoxDays, ca.ErrAcountedDays, a.Acounteddays.ToString());
                act(textBoxTotal, ca.ErrAtotal, a.Total.ToString());
                //act(textBoxCharge, ca.ErrAcharge, a.Acharge.ToString());
                if (textBoxDrCode.Enabled) act(textBoxDrCode, ca.ErrDrCode, a.DrNum);
                //var drNeedCheck = ca.ErrDrCode || !d.Verified;
                //act(textBoxDrName, drNeedCheck, a.Sdoctor);
                //act(textBoxHosName, drNeedCheck, a.Sname);
                //act(textBoxHosAdd, drNeedCheck, a.Baccname);
            }
            else
            {
                if (a.MediYear == (int)APP_SPECIAL_CODE.続紙)
                {
                    act(textBoxY, !a.StatusFlagCheck(StatusFlag.ベリファイ済), "--");
                }
                else if (a.MediYear == (int)APP_SPECIAL_CODE.不要)
                {
                    act(textBoxY, !a.StatusFlagCheck(StatusFlag.ベリファイ済), "++");
                }

                act(textBoxM, false, "");
                act(textBoxHnum, false, "");
                act(textBoxRatio, false, "");
                act(textBoxFname, false, "");
                act(textBoxSex, false, "");
                act(textBoxBirthday, false, "");
                act(textBoxBY, false, "");
                act(textBoxBM, false, "");
                act(textBoxBD, false, "");
                act(textBoxName, false, "");
                act(textBoxFusho, false, "");
                act(textBoxShokenY, false, "");
                act(textBoxShokenM, false, "");
                act(textBoxShokenD, false, "");
                act(textBoxDays, false, "");
                act(textBoxTotal, false, "");
                act(textBoxCharge, false, "");
                act(textBoxDrCode, false, "");
                act(textBoxDrName, false, "");
                act(textBoxHosName, false, "");
                act(textBoxHosAdd, false, "");
            }
        }

        /// <summary>
        /// ベリファイ入力時、1回目と数値が一致するかチェックします
        /// </summary>
        /// <returns></returns>
        private bool checkVerify()
        {
            bool error = false;
            var act = new Action<TextBox, TextBox>((t1, t2) =>
                {
                    if (!t1.Enabled) return;
                    if (t1.Text == t2.Text)
                    {
                        t1.BackColor = SystemColors.Info;
                        t2.BackColor = SystemColors.Info;
                        t2.Visible = false;
                    }
                    else
                    {
                        t1.BackColor = Color.Pink;
                        t2.BackColor = Color.Pink;
                        t2.Visible = true;
                        error = true;
                    }
                });

            act(textBoxY, textBoxY2);
            act(textBoxM, textBoxM2);
            act(textBoxHnum, textBoxHnum2);
            act(textBoxRatio, textBoxRatio2);
            //act(textBoxFname, textBoxFname2);
            act(textBoxBirthday, textBoxBirthday2);
            act(textBoxSex, textBoxSex2);
            act(textBoxBY, textBoxBY2);
            act(textBoxBM, textBoxBM2);
            act(textBoxBD, textBoxBD2);
            act(textBoxName, textBoxName2);
            act(textBoxTekiou, textBoxTekiou2);

            act(textBoxTotal, textBoxTotal2);

            //act(textBoxDrCode, textBoxDrCode2);
            if (textBoxDrCode.Text.Contains("*") && textBoxDrCode2.Text.Contains("*"))
            {
                textBoxDrCode2.Text = textBoxDrCode.Text;
            }
            else
            {
                act(textBoxDrCode, textBoxDrCode2);
            }

            return !error;
        }

        int prevIndex = -1;
        private void dataGridViewPlist_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount != 0)
            {
                dataGridViewPlist.CurrentCell = null;
                dataGridViewPlist.CurrentCell = dataGridViewPlist["Aid", 0];
            }
        }

        private void dataGridViewPlist_CurrentCellChanged_1(object sender, EventArgs e)
        {
            if (dataGridViewPlist.CurrentCell == null) return;
            if (dataGridViewPlist.CurrentCell.RowIndex == prevIndex) return;
            prevIndex = dataGridViewPlist.CurrentCell.RowIndex;
            currentRowAppRead();
        }

        private void textBoxDrCode_Leave(object sender, EventArgs e)
        {
            //ベリファイ時は引っ張らない
            if (!firstTime) return;

            var code = textBoxDrCode.Text.Trim();
            var d = Doctor.GetDoctor(code);
            if (d == null || !d.Verified) return;

            //if (textBoxDrName.Text == string.Empty) textBoxDrName.Text = d.DrName;
            if (textBoxHosName.Text == string.Empty) textBoxHosName.Text = d.HosName;
            if (textBoxHosAdd.Text == string.Empty) textBoxHosAdd.Text = d.DrAdd;
            if (textBoxDairiAdd.Text == string.Empty) textBoxDairiAdd.Text = d.DairiAdd;
            if (textBoxDairiName.Text == string.Empty) textBoxDairiName.Text = d.DairiName;
        }

        private void textBoxHnum_Leave(object sender, EventArgs e)
        {
            //ベリファイ時は引っ張らない
            if (!firstTime) return;

            if (textBoxFname.Text != string.Empty) return;
            var hnum = textBoxHnum.Text.Trim();

            var fn = Person.GetVerifiedFamilyNameAndAdd(hnum);
            if (fn == null) return;
            textBoxFname.Text = fn[0];
            textBoxAdd.Text = fn[1];
        }

        private void textBoxBD_Leave(object sender, EventArgs e)
        {
            if (textBoxName.Text != string.Empty) return;
            var hnum = textBoxHnum.Text.Trim();

            int j, y, m, d;
            DateTime dt;
            if (!int.TryParse(textBoxBirthday.Text, out j)) return;
            if (!int.TryParse(textBoxBY.Text, out y)) return;
            if (!int.TryParse(textBoxBM.Text, out m)) return;
            if (!int.TryParse(textBoxBD.Text, out d)) return;

            var dts = j == 1 ? "明治" : j == 2 ? "大正" : j == 3 ? "昭和" : j == 4 ? "平成" : "";
            dts += textBoxBY.Text + "/" + textBoxBM.Text + "/" + textBoxBD.Text;
            DateTimeEx.TryGetDateTimeFromJdate(dts, out dt);

            var p = Person.GetPerson(hnum, dt);
            if (p == null || !p.Verified) return;
            textBoxName.Text = p.Pname;
            //textBoxSex.Text = p.Psex.ToString();
            if (ActiveControl == textBoxName) textBoxShokenY.Focus();
        }

        private void textBoxKanaToHalf_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            t.Text = Utility.ToHalfWithoutKatakana(t.Text);
        }

        private void textBoxShinryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.Text == "*") return;

            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }

        private void textBoxOryoDays_Leave(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.Text.Trim() == "----")
            {
                t.Text = textBoxShinryoDays.Text;
                return;
            }

            var sdays = Utility.ToHalfWithoutKatakana(t.Text.Trim()).Split(" ,./".ToCharArray());
            var l = new List<string>();
            foreach (var item in sdays)
            {
                int d;
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (int.TryParse(item, out d)) l.Add(item.Trim());
            }
            t.Text = string.Join(" ", l);
        }
    }
}
