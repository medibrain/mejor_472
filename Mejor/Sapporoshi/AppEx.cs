﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;
namespace Mejor.Sapporoshi
{
    /// <summary>
    /// 札幌国保特有の記録群
    /// </summary>
    class AppEx
    {
        /*
        CREATE TABLE app_ex
        (
          aid integer NOT NULL,
          fusho_date text NOT NULL,
          dairi_name text NOT NULL,
          dairi_add text NOT NULL,
          dr_add text NOT NULL,
          sejyutu_daylist text NOT NULL,
          oryo_daylist text NOT NULL,
          oryo_days integer NOT NULL,
          tekiou_umu boolean NOT NULL,
          shomei_date date NOT NULL,
          inin_date date NOT NULL,
          doui_umu bool NOT NULL,
          doui_name text NOT NULL,
          doui_add text NOT NULL,
          doui_date date NOT NULL,
          doui_fusho text NOT NULL,
          CONSTRAINT app_ex_pkey PRIMARY KEY (aid)
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE app_ex
          OWNER TO postgres;
        */

        public int AID { get; private set; }
        public string FushoDate { get; set; }
        public string DairiName { get; set; }
        public string DairiAdd { get; set; }
        public string DrAdd { get; set; }
        public string SejyutuDayList { get; set; }
        public string OryoDayList { get; set; }
        public int OryoDays { get; set; }
        public bool TekiouUmu { get; set; }
        public DateTime ShomeiDate { get; set; }
        public DateTime IninDate { get; set; }
        public bool DouiUmu { get; set; }
        public string DouiName { get; set; }
        public string DouiAdd { get; set; }
        public DateTime DouiDate { get; set; }
        public string DouiFusho { get; set; }

        public AppEx(int aid)
        {
            this.AID = aid;
            this.FushoDate = string.Empty;
            this.DairiName = string.Empty;
            this.DairiAdd = string.Empty;
            this.DrAdd = string.Empty;
            this.SejyutuDayList = string.Empty;
            this.OryoDayList = string.Empty;
            this.OryoDays = 0;
            this.TekiouUmu = false;
            this.ShomeiDate = DateTime.MinValue;
            this.IninDate = DateTime.MinValue;
            this.DouiUmu = false;
            this.DouiName = string.Empty;
            this.DouiAdd = string.Empty;
            this.DouiDate = DateTime.MinValue;
            this.DouiFusho = string.Empty;
        }

        public bool Upsert(DB.Transaction tran)
        {
            var sql = "SELECT aid FROM app_ex WHERE aid=:aid;";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = AID;
                var res = cmd.TryExecuteScalar();

                if (res == null || res == DBNull.Value) return insert(tran);
                else return update(tran);
            }
        }

        private bool insert(DB.Transaction tran)
        {
            var sql = "INSERT INTO app_ex(" +
                "aid, fusho_date, dairi_name, dairi_add, dr_add, sejyutu_daylist, " +
                "oryo_daylist, oryo_days, tekiou_umu, shomei_date, inin_date, " +
                "doui_umu, doui_name, doui_add, doui_date, doui_fusho) " +
                "VALUES (" +
                ":aid, :fusho_date, :dairi_name, :dairi_add, :dr_add, :sejyutu_daylist, " +
                ":oryo_daylist, :oryo_days, :tekiou_umu, :shomei_date, :inin_date, " +
                ":doui_umu, :doui_name, :doui_add, :doui_date, :doui_fusho) ";

            using (var cmd = DB.Main.CreateCmd(sql, tran))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = AID;
                cmd.Parameters.Add("fusho_date", NpgsqlDbType.Text).Value = FushoDate;
                cmd.Parameters.Add("dairi_name", NpgsqlDbType.Text).Value = DairiName;
                cmd.Parameters.Add("dairi_add", NpgsqlDbType.Text).Value = DairiAdd;
                cmd.Parameters.Add("dr_add", NpgsqlDbType.Text).Value = DrAdd;
                cmd.Parameters.Add("sejyutu_daylist", NpgsqlDbType.Text).Value = SejyutuDayList;
                cmd.Parameters.Add("oryo_daylist", NpgsqlDbType.Text).Value = OryoDayList;
                cmd.Parameters.Add("oryo_days", NpgsqlDbType.Integer).Value = OryoDays;
                cmd.Parameters.Add("tekiou_umu", NpgsqlDbType.Boolean).Value = TekiouUmu;
                cmd.Parameters.Add("shomei_date", NpgsqlDbType.Date).Value = ShomeiDate;
                cmd.Parameters.Add("inin_date", NpgsqlDbType.Date).Value = IninDate;
                cmd.Parameters.Add("doui_umu", NpgsqlDbType.Boolean).Value = DouiUmu;
                cmd.Parameters.Add("doui_name", NpgsqlDbType.Text).Value = DouiName;
                cmd.Parameters.Add("doui_add", NpgsqlDbType.Text).Value = DouiAdd;
                cmd.Parameters.Add("doui_date", NpgsqlDbType.Date).Value = DouiDate;
                cmd.Parameters.Add("doui_fusho", NpgsqlDbType.Text).Value = DouiFusho;

                return cmd.TryExecuteNonQuery();
            }
        }

        private bool update(DB.Transaction tran)
        {
            var sql = "UPDATE app_ex " +
                "SET fusho_date=:fusho_date, dairi_name=:dairi_name, dairi_add=:dairi_add, " +
                "dr_add=:dr_add, sejyutu_daylist=:sejyutu_daylist, " +
                "oryo_daylist=:oryo_daylist, oryo_days=:oryo_days, tekiou_umu=:tekiou_umu, " +
                "shomei_date=:shomei_date, inin_date=:inin_date, doui_name=:doui_name, "+
                "doui_umu=:doui_umu, doui_add=:doui_add, doui_date=:doui_date, " +
                "doui_fusho=:doui_fusho " +
                "WHERE aid=:aid;";

            using (var cmd = DB.Main.CreateCmd(sql, tran))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = AID;
                cmd.Parameters.Add("fusho_date", NpgsqlDbType.Text).Value = FushoDate;
                cmd.Parameters.Add("dairi_name", NpgsqlDbType.Text).Value = DairiName;
                cmd.Parameters.Add("dairi_add", NpgsqlDbType.Text).Value = DairiAdd;
                cmd.Parameters.Add("dr_add", NpgsqlDbType.Text).Value = DrAdd;
                cmd.Parameters.Add("sejyutu_daylist", NpgsqlDbType.Text).Value = SejyutuDayList;
                cmd.Parameters.Add("oryo_daylist", NpgsqlDbType.Text).Value = OryoDayList;
                cmd.Parameters.Add("oryo_days", NpgsqlDbType.Integer).Value = OryoDays;
                cmd.Parameters.Add("tekiou_umu", NpgsqlDbType.Boolean).Value = TekiouUmu;
                cmd.Parameters.Add("shomei_date", NpgsqlDbType.Date).Value = ShomeiDate;
                cmd.Parameters.Add("inin_date", NpgsqlDbType.Date).Value = IninDate;
                cmd.Parameters.Add("doui_umu", NpgsqlDbType.Boolean).Value = DouiUmu;
                cmd.Parameters.Add("doui_name", NpgsqlDbType.Text).Value = DouiName;
                cmd.Parameters.Add("doui_add", NpgsqlDbType.Text).Value = DouiAdd;
                cmd.Parameters.Add("doui_date", NpgsqlDbType.Date).Value = DouiDate;
                cmd.Parameters.Add("doui_fusho", NpgsqlDbType.Text).Value = DouiFusho;

                return cmd.TryExecuteNonQuery();
            }
        }

        public static AppEx Select(int aid)
        {
            var sql = "SELECT aid, fusho_date, dairi_name, dairi_add, dr_add, sejyutu_daylist, " +
                "oryo_daylist, oryo_days, tekiou_umu, shomei_date, inin_date, " +
                "doui_umu, doui_name, doui_add, doui_date, doui_fusho " +
                "FROM app_ex WHERE aid=:aid;";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;

                var appex = new AppEx(aid);
                appex.FushoDate = (string)res[0][1];
                appex.DairiName = (string)res[0][2];
                appex.DairiAdd = (string)res[0][3];
                appex.DrAdd = (string)res[0][4];
                appex.SejyutuDayList = (string)res[0][5];
                appex.OryoDayList = (string)res[0][6];
                appex.OryoDays = (int)res[0][7];
                appex.TekiouUmu = (Boolean)res[0][8];
                appex.ShomeiDate = (DateTime)res[0][9];
                appex.IninDate = (DateTime)res[0][10];
                appex.DouiUmu = (Boolean)res[0][11];
                appex.DouiName = (string)res[0][12];
                appex.DouiAdd = (string)res[0][13];
                appex.DouiDate = (DateTime)res[0][14];
                appex.DouiFusho = (string)res[0][15];
                return appex;
            }
        }

        public static bool Delete(int aid)
        {
            var sql = "DELETE FROM app_ex WHERE aid=:aid";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return cmd.TryExecuteNonQuery();
            }
        }

    }
}
