﻿namespace Mejor.Sapporoshi
{
    partial class FormOCRCheckJyu2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.textBoxHnum = new System.Windows.Forms.TextBox();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.textBoxDrCode2 = new System.Windows.Forms.TextBox();
            this.textBoxDrName2 = new System.Windows.Forms.TextBox();
            this.textBoxHosName2 = new System.Windows.Forms.TextBox();
            this.textBoxDrCode = new System.Windows.Forms.TextBox();
            this.textBoxDrName = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxHosName = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelControlScroll = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.panelMonth = new System.Windows.Forms.Panel();
            this.textBoxM2 = new System.Windows.Forms.TextBox();
            this.textBoxY2 = new System.Windows.Forms.TextBox();
            this.panelReceipt = new System.Windows.Forms.Panel();
            this.labelChargeInfo = new System.Windows.Forms.Label();
            this.textBoxFutan2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxFutan = new System.Windows.Forms.TextBox();
            this.textBox5FusyoY2 = new System.Windows.Forms.TextBox();
            this.textBox4FusyoY2 = new System.Windows.Forms.TextBox();
            this.textBox3FusyoY2 = new System.Windows.Forms.TextBox();
            this.textBox2FusyoY2 = new System.Windows.Forms.TextBox();
            this.textBox1FusyoY2 = new System.Windows.Forms.TextBox();
            this.textBo5FusyoM2 = new System.Windows.Forms.TextBox();
            this.textBox4FusyoM2 = new System.Windows.Forms.TextBox();
            this.textBox3FusyoM2 = new System.Windows.Forms.TextBox();
            this.textBox2FusyoM2 = new System.Windows.Forms.TextBox();
            this.textBox1FusyoM2 = new System.Windows.Forms.TextBox();
            this.textBox5FusyoD2 = new System.Windows.Forms.TextBox();
            this.textBox4FusyoD2 = new System.Windows.Forms.TextBox();
            this.textBox3FusyoD2 = new System.Windows.Forms.TextBox();
            this.textBox2FusyoD2 = new System.Windows.Forms.TextBox();
            this.textBox1FusyoD2 = new System.Windows.Forms.TextBox();
            this.textBox5FusyoTen2 = new System.Windows.Forms.TextBox();
            this.textBox4FusyoTen2 = new System.Windows.Forms.TextBox();
            this.textBox3FusyoTen2 = new System.Windows.Forms.TextBox();
            this.textBox2FusyoTen2 = new System.Windows.Forms.TextBox();
            this.textBox1FusyoTen2 = new System.Windows.Forms.TextBox();
            this.textBox5FusyoDays2 = new System.Windows.Forms.TextBox();
            this.textBox4FusyoDays2 = new System.Windows.Forms.TextBox();
            this.textBox3FusyoDays2 = new System.Windows.Forms.TextBox();
            this.textBox2FusyoDays2 = new System.Windows.Forms.TextBox();
            this.textBox1FusyoDays2 = new System.Windows.Forms.TextBox();
            this.textBox5Fusyo2 = new System.Windows.Forms.TextBox();
            this.textBox4Fusyo2 = new System.Windows.Forms.TextBox();
            this.textBox3Fusyo2 = new System.Windows.Forms.TextBox();
            this.textBox2Fusyo2 = new System.Windows.Forms.TextBox();
            this.textBox1Fusyo2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox5FusyoY = new System.Windows.Forms.TextBox();
            this.textBox4FusyoY = new System.Windows.Forms.TextBox();
            this.textBox3FusyoY = new System.Windows.Forms.TextBox();
            this.textBox2FusyoY = new System.Windows.Forms.TextBox();
            this.textBox1FusyoY = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox5FusyoM = new System.Windows.Forms.TextBox();
            this.textBox4FusyoM = new System.Windows.Forms.TextBox();
            this.textBox3FusyoM = new System.Windows.Forms.TextBox();
            this.textBox2FusyoM = new System.Windows.Forms.TextBox();
            this.textBox1FusyoM = new System.Windows.Forms.TextBox();
            this.textBox5FusyoD = new System.Windows.Forms.TextBox();
            this.textBox4FusyoD = new System.Windows.Forms.TextBox();
            this.textBox3FusyoD = new System.Windows.Forms.TextBox();
            this.textBox2FusyoD = new System.Windows.Forms.TextBox();
            this.textBox1FusyoD = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxBirthday2 = new System.Windows.Forms.TextBox();
            this.textBoxSex2 = new System.Windows.Forms.TextBox();
            this.textBoxBY2 = new System.Windows.Forms.TextBox();
            this.textBoxBM2 = new System.Windows.Forms.TextBox();
            this.textBoxBD2 = new System.Windows.Forms.TextBox();
            this.textBoxHnum2 = new System.Windows.Forms.TextBox();
            this.textBoxRatio2 = new System.Windows.Forms.TextBox();
            this.textBox5FusyoTen = new System.Windows.Forms.TextBox();
            this.textBox4FusyoTen = new System.Windows.Forms.TextBox();
            this.textBox3FusyoTen = new System.Windows.Forms.TextBox();
            this.textBox2FusyoTen = new System.Windows.Forms.TextBox();
            this.textBox1FusyoTen = new System.Windows.Forms.TextBox();
            this.textBox5FusyoDays = new System.Windows.Forms.TextBox();
            this.textBox4FusyoDays = new System.Windows.Forms.TextBox();
            this.textBox3FusyoDays = new System.Windows.Forms.TextBox();
            this.textBox2FusyoDays = new System.Windows.Forms.TextBox();
            this.textBox1FusyoDays = new System.Windows.Forms.TextBox();
            this.textBoxDays = new System.Windows.Forms.TextBox();
            this.textBoxDays2 = new System.Windows.Forms.TextBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBY = new System.Windows.Forms.TextBox();
            this.textBoxSex = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxShinryoDays2 = new System.Windows.Forms.TextBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.textBoxCharge2 = new System.Windows.Forms.TextBox();
            this.textBoxBM = new System.Windows.Forms.TextBox();
            this.textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBD = new System.Windows.Forms.TextBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxRatio = new System.Windows.Forms.TextBox();
            this.textBoxName2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox5Fusyo = new System.Windows.Forms.TextBox();
            this.textBox4Fusyo = new System.Windows.Forms.TextBox();
            this.textBox3Fusyo = new System.Windows.Forms.TextBox();
            this.textBox2Fusyo = new System.Windows.Forms.TextBox();
            this.textBox1Fusyo = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxShinryoDays = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.textBoxCharge = new System.Windows.Forms.TextBox();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelUP = new System.Windows.Forms.Panel();
            this.labelNote1 = new System.Windows.Forms.Label();
            this.labelGroupID = new System.Windows.Forms.Label();
            this.labelScanID = new System.Windows.Forms.Label();
            this.labelSeikyu = new System.Windows.Forms.Label();
            this.panelRight.SuspendLayout();
            this.panelControlScroll.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panelMonth.SuspendLayout();
            this.panelReceipt.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelUP.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxY
            // 
            this.textBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxY.Location = new System.Drawing.Point(43, 6);
            this.textBoxY.MaxLength = 2;
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(32, 26);
            this.textBoxY.TabIndex = 1;
            this.textBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY.TextChanged += new System.EventHandler(this.textBoxY_TextChanged);
            this.textBoxY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxM
            // 
            this.textBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxM.Location = new System.Drawing.Point(3, 6);
            this.textBoxM.MaxLength = 2;
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(32, 26);
            this.textBoxM.TabIndex = 4;
            this.textBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHnum
            // 
            this.textBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHnum.Location = new System.Drawing.Point(43, 4);
            this.textBoxHnum.Name = "textBoxHnum";
            this.textBoxHnum.Size = new System.Drawing.Size(85, 26);
            this.textBoxHnum.TabIndex = 0;
            this.textBoxHnum.TextChanged += new System.EventHandler(this.textBoxPersons_TextChanged);
            this.textBoxHnum.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxHnum.Leave += new System.EventHandler(this.textBoxHnum_Leave);
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(227, 3);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(111, 23);
            this.buttonRegist.TabIndex = 1;
            this.buttonRegist.Text = "登録 (PageUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(76, 20);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(36, 20);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 6;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(14, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(29, 24);
            this.labelHnum.TabIndex = 0;
            this.labelHnum.Text = "被保\r\n番";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(216, 6);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(32, 24);
            this.labelNumbering.TabIndex = 4;
            this.labelNumbering.Text = "機関\r\nコード";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxDrCode2
            // 
            this.textBoxDrCode2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrCode2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrCode2.Location = new System.Drawing.Point(250, 29);
            this.textBoxDrCode2.Name = "textBoxDrCode2";
            this.textBoxDrCode2.Size = new System.Drawing.Size(150, 26);
            this.textBoxDrCode2.TabIndex = 5;
            this.textBoxDrCode2.Visible = false;
            this.textBoxDrCode2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDrName2
            // 
            this.textBoxDrName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDrName2.Location = new System.Drawing.Point(43, 749);
            this.textBoxDrName2.Name = "textBoxDrName2";
            this.textBoxDrName2.Size = new System.Drawing.Size(160, 26);
            this.textBoxDrName2.TabIndex = 133;
            this.textBoxDrName2.Visible = false;
            this.textBoxDrName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHosName2
            // 
            this.textBoxHosName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosName2.Location = new System.Drawing.Point(43, 692);
            this.textBoxHosName2.Name = "textBoxHosName2";
            this.textBoxHosName2.Size = new System.Drawing.Size(238, 26);
            this.textBoxHosName2.TabIndex = 130;
            this.textBoxHosName2.Visible = false;
            this.textBoxHosName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDrCode
            // 
            this.textBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrCode.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrCode.Location = new System.Drawing.Point(250, 4);
            this.textBoxDrCode.Name = "textBoxDrCode";
            this.textBoxDrCode.Size = new System.Drawing.Size(150, 26);
            this.textBoxDrCode.TabIndex = 4;
            this.textBoxDrCode.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxDrCode.Leave += new System.EventHandler(this.textBoxDrCode_Leave);
            // 
            // textBoxDrName
            // 
            this.textBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDrName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDrName.Location = new System.Drawing.Point(43, 724);
            this.textBoxDrName.Name = "textBoxDrName";
            this.textBoxDrName.Size = new System.Drawing.Size(160, 26);
            this.textBoxDrName.TabIndex = 132;
            this.textBoxDrName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(15, 726);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 24);
            this.label31.TabIndex = 131;
            this.label31.Text = "施術\r\n師名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(14, 669);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 24);
            this.label33.TabIndex = 128;
            this.label33.Text = "施術\r\n所名";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxHosName
            // 
            this.textBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHosName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxHosName.Location = new System.Drawing.Point(43, 667);
            this.textBoxHosName.Name = "textBoxHosName";
            this.textBoxHosName.Size = new System.Drawing.Size(238, 26);
            this.textBoxHosName.TabIndex = 129;
            this.textBoxHosName.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxHosName.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(344, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "和暦";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(166, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "続紙:--\r\n不要:++";
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelControlScroll);
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(923, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(421, 739);
            this.panelRight.TabIndex = 2;
            // 
            // panelControlScroll
            // 
            this.panelControlScroll.AutoScroll = true;
            this.panelControlScroll.Controls.Add(this.panelControls);
            this.panelControlScroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlScroll.Location = new System.Drawing.Point(0, 0);
            this.panelControlScroll.Name = "panelControlScroll";
            this.panelControlScroll.Size = new System.Drawing.Size(421, 711);
            this.panelControlScroll.TabIndex = 0;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.panelMonth);
            this.panelControls.Controls.Add(this.label3);
            this.panelControls.Controls.Add(this.textBoxY);
            this.panelControls.Controls.Add(this.label8);
            this.panelControls.Controls.Add(this.labelY);
            this.panelControls.Controls.Add(this.textBoxY2);
            this.panelControls.Controls.Add(this.panelReceipt);
            this.panelControls.Location = new System.Drawing.Point(0, 0);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(404, 839);
            this.panelControls.TabIndex = 0;
            // 
            // panelMonth
            // 
            this.panelMonth.Controls.Add(this.textBoxM);
            this.panelMonth.Controls.Add(this.textBoxM2);
            this.panelMonth.Controls.Add(this.labelM);
            this.panelMonth.Location = new System.Drawing.Point(94, 0);
            this.panelMonth.Name = "panelMonth";
            this.panelMonth.Size = new System.Drawing.Size(68, 61);
            this.panelMonth.TabIndex = 4;
            // 
            // textBoxM2
            // 
            this.textBoxM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxM2.Location = new System.Drawing.Point(3, 31);
            this.textBoxM2.MaxLength = 2;
            this.textBoxM2.Name = "textBoxM2";
            this.textBoxM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxM2.TabIndex = 5;
            this.textBoxM2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxM2.Visible = false;
            this.textBoxM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxY2
            // 
            this.textBoxY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxY2.Location = new System.Drawing.Point(43, 31);
            this.textBoxY2.MaxLength = 2;
            this.textBoxY2.Name = "textBoxY2";
            this.textBoxY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxY2.TabIndex = 2;
            this.textBoxY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxY2.Visible = false;
            this.textBoxY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // panelReceipt
            // 
            this.panelReceipt.Controls.Add(this.labelChargeInfo);
            this.panelReceipt.Controls.Add(this.textBoxFutan2);
            this.panelReceipt.Controls.Add(this.label30);
            this.panelReceipt.Controls.Add(this.textBoxFutan);
            this.panelReceipt.Controls.Add(this.textBox5FusyoY2);
            this.panelReceipt.Controls.Add(this.textBox4FusyoY2);
            this.panelReceipt.Controls.Add(this.textBox3FusyoY2);
            this.panelReceipt.Controls.Add(this.textBox2FusyoY2);
            this.panelReceipt.Controls.Add(this.textBox1FusyoY2);
            this.panelReceipt.Controls.Add(this.textBo5FusyoM2);
            this.panelReceipt.Controls.Add(this.textBox4FusyoM2);
            this.panelReceipt.Controls.Add(this.textBox3FusyoM2);
            this.panelReceipt.Controls.Add(this.textBox2FusyoM2);
            this.panelReceipt.Controls.Add(this.textBox1FusyoM2);
            this.panelReceipt.Controls.Add(this.textBox5FusyoD2);
            this.panelReceipt.Controls.Add(this.textBox4FusyoD2);
            this.panelReceipt.Controls.Add(this.textBox3FusyoD2);
            this.panelReceipt.Controls.Add(this.textBox2FusyoD2);
            this.panelReceipt.Controls.Add(this.textBox1FusyoD2);
            this.panelReceipt.Controls.Add(this.textBox5FusyoTen2);
            this.panelReceipt.Controls.Add(this.textBox4FusyoTen2);
            this.panelReceipt.Controls.Add(this.textBox3FusyoTen2);
            this.panelReceipt.Controls.Add(this.textBox2FusyoTen2);
            this.panelReceipt.Controls.Add(this.textBox1FusyoTen2);
            this.panelReceipt.Controls.Add(this.textBox5FusyoDays2);
            this.panelReceipt.Controls.Add(this.textBox4FusyoDays2);
            this.panelReceipt.Controls.Add(this.textBox3FusyoDays2);
            this.panelReceipt.Controls.Add(this.textBox2FusyoDays2);
            this.panelReceipt.Controls.Add(this.textBox1FusyoDays2);
            this.panelReceipt.Controls.Add(this.textBox5Fusyo2);
            this.panelReceipt.Controls.Add(this.textBox4Fusyo2);
            this.panelReceipt.Controls.Add(this.textBox3Fusyo2);
            this.panelReceipt.Controls.Add(this.textBox2Fusyo2);
            this.panelReceipt.Controls.Add(this.textBox1Fusyo2);
            this.panelReceipt.Controls.Add(this.label28);
            this.panelReceipt.Controls.Add(this.label25);
            this.panelReceipt.Controls.Add(this.label21);
            this.panelReceipt.Controls.Add(this.label13);
            this.panelReceipt.Controls.Add(this.label1);
            this.panelReceipt.Controls.Add(this.textBox5FusyoY);
            this.panelReceipt.Controls.Add(this.textBox4FusyoY);
            this.panelReceipt.Controls.Add(this.textBox3FusyoY);
            this.panelReceipt.Controls.Add(this.textBox2FusyoY);
            this.panelReceipt.Controls.Add(this.textBox1FusyoY);
            this.panelReceipt.Controls.Add(this.label27);
            this.panelReceipt.Controls.Add(this.label24);
            this.panelReceipt.Controls.Add(this.label19);
            this.panelReceipt.Controls.Add(this.label12);
            this.panelReceipt.Controls.Add(this.label2);
            this.panelReceipt.Controls.Add(this.textBox5FusyoM);
            this.panelReceipt.Controls.Add(this.textBox4FusyoM);
            this.panelReceipt.Controls.Add(this.textBox3FusyoM);
            this.panelReceipt.Controls.Add(this.textBox2FusyoM);
            this.panelReceipt.Controls.Add(this.textBox1FusyoM);
            this.panelReceipt.Controls.Add(this.textBox5FusyoD);
            this.panelReceipt.Controls.Add(this.textBox4FusyoD);
            this.panelReceipt.Controls.Add(this.textBox3FusyoD);
            this.panelReceipt.Controls.Add(this.textBox2FusyoD);
            this.panelReceipt.Controls.Add(this.textBox1FusyoD);
            this.panelReceipt.Controls.Add(this.label26);
            this.panelReceipt.Controls.Add(this.label23);
            this.panelReceipt.Controls.Add(this.label14);
            this.panelReceipt.Controls.Add(this.label11);
            this.panelReceipt.Controls.Add(this.label7);
            this.panelReceipt.Controls.Add(this.textBoxBirthday2);
            this.panelReceipt.Controls.Add(this.textBoxSex2);
            this.panelReceipt.Controls.Add(this.textBoxBY2);
            this.panelReceipt.Controls.Add(this.textBoxBM2);
            this.panelReceipt.Controls.Add(this.textBoxBD2);
            this.panelReceipt.Controls.Add(this.textBoxHnum2);
            this.panelReceipt.Controls.Add(this.textBoxDrCode2);
            this.panelReceipt.Controls.Add(this.textBoxRatio2);
            this.panelReceipt.Controls.Add(this.labelNumbering);
            this.panelReceipt.Controls.Add(this.textBox5FusyoTen);
            this.panelReceipt.Controls.Add(this.textBox4FusyoTen);
            this.panelReceipt.Controls.Add(this.textBox3FusyoTen);
            this.panelReceipt.Controls.Add(this.textBox2FusyoTen);
            this.panelReceipt.Controls.Add(this.textBox1FusyoTen);
            this.panelReceipt.Controls.Add(this.textBox5FusyoDays);
            this.panelReceipt.Controls.Add(this.textBox4FusyoDays);
            this.panelReceipt.Controls.Add(this.textBox3FusyoDays);
            this.panelReceipt.Controls.Add(this.textBox2FusyoDays);
            this.panelReceipt.Controls.Add(this.textBox1FusyoDays);
            this.panelReceipt.Controls.Add(this.textBoxDays);
            this.panelReceipt.Controls.Add(this.textBoxDays2);
            this.panelReceipt.Controls.Add(this.labelDays);
            this.panelReceipt.Controls.Add(this.label18);
            this.panelReceipt.Controls.Add(this.label34);
            this.panelReceipt.Controls.Add(this.label4);
            this.panelReceipt.Controls.Add(this.textBoxBY);
            this.panelReceipt.Controls.Add(this.textBoxSex);
            this.panelReceipt.Controls.Add(this.label17);
            this.panelReceipt.Controls.Add(this.textBoxShinryoDays2);
            this.panelReceipt.Controls.Add(this.labelSex);
            this.panelReceipt.Controls.Add(this.textBoxCharge2);
            this.panelReceipt.Controls.Add(this.textBoxBM);
            this.panelReceipt.Controls.Add(this.textBoxTotal2);
            this.panelReceipt.Controls.Add(this.textBoxBirthday);
            this.panelReceipt.Controls.Add(this.label15);
            this.panelReceipt.Controls.Add(this.label5);
            this.panelReceipt.Controls.Add(this.textBoxBD);
            this.panelReceipt.Controls.Add(this.labelBirthday);
            this.panelReceipt.Controls.Add(this.label16);
            this.panelReceipt.Controls.Add(this.textBoxHnum);
            this.panelReceipt.Controls.Add(this.textBoxHosName2);
            this.panelReceipt.Controls.Add(this.textBoxRatio);
            this.panelReceipt.Controls.Add(this.textBoxName2);
            this.panelReceipt.Controls.Add(this.labelHnum);
            this.panelReceipt.Controls.Add(this.textBoxDrName2);
            this.panelReceipt.Controls.Add(this.label33);
            this.panelReceipt.Controls.Add(this.textBoxHosName);
            this.panelReceipt.Controls.Add(this.textBoxDrName);
            this.panelReceipt.Controls.Add(this.label10);
            this.panelReceipt.Controls.Add(this.label31);
            this.panelReceipt.Controls.Add(this.label9);
            this.panelReceipt.Controls.Add(this.label6);
            this.panelReceipt.Controls.Add(this.label22);
            this.panelReceipt.Controls.Add(this.textBox5Fusyo);
            this.panelReceipt.Controls.Add(this.textBox4Fusyo);
            this.panelReceipt.Controls.Add(this.textBox3Fusyo);
            this.panelReceipt.Controls.Add(this.textBox2Fusyo);
            this.panelReceipt.Controls.Add(this.textBox1Fusyo);
            this.panelReceipt.Controls.Add(this.label29);
            this.panelReceipt.Controls.Add(this.textBoxDrCode);
            this.panelReceipt.Controls.Add(this.textBoxShinryoDays);
            this.panelReceipt.Controls.Add(this.textBoxName);
            this.panelReceipt.Controls.Add(this.label20);
            this.panelReceipt.Controls.Add(this.labelTotal);
            this.panelReceipt.Controls.Add(this.labelCharge);
            this.panelReceipt.Controls.Add(this.textBoxCharge);
            this.panelReceipt.Controls.Add(this.textBoxTotal);
            this.panelReceipt.Location = new System.Drawing.Point(0, 60);
            this.panelReceipt.Name = "panelReceipt";
            this.panelReceipt.Size = new System.Drawing.Size(404, 779);
            this.panelReceipt.TabIndex = 8;
            // 
            // labelChargeInfo
            // 
            this.labelChargeInfo.AutoSize = true;
            this.labelChargeInfo.Location = new System.Drawing.Point(159, 614);
            this.labelChargeInfo.Name = "labelChargeInfo";
            this.labelChargeInfo.Size = new System.Drawing.Size(113, 12);
            this.labelChargeInfo.TabIndex = 134;
            this.labelChargeInfo.Text = "※請求金額参考情報";
            // 
            // textBoxFutan2
            // 
            this.textBoxFutan2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFutan2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFutan2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFutan2.Location = new System.Drawing.Point(283, 585);
            this.textBoxFutan2.Name = "textBoxFutan2";
            this.textBoxFutan2.Size = new System.Drawing.Size(80, 26);
            this.textBoxFutan2.TabIndex = 124;
            this.textBoxFutan2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxFutan2.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(254, 562);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 24);
            this.label30.TabIndex = 122;
            this.label30.Text = "※\r\n金額";
            // 
            // textBoxFutan
            // 
            this.textBoxFutan.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxFutan.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxFutan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxFutan.Location = new System.Drawing.Point(283, 560);
            this.textBoxFutan.Name = "textBoxFutan";
            this.textBoxFutan.Size = new System.Drawing.Size(80, 26);
            this.textBoxFutan.TabIndex = 123;
            this.textBoxFutan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox5FusyoY2
            // 
            this.textBox5FusyoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoY2.Location = new System.Drawing.Point(178, 426);
            this.textBox5FusyoY2.MaxLength = 2;
            this.textBox5FusyoY2.Name = "textBox5FusyoY2";
            this.textBox5FusyoY2.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoY2.TabIndex = 96;
            this.textBox5FusyoY2.TabStop = false;
            this.textBox5FusyoY2.Visible = false;
            // 
            // textBox4FusyoY2
            // 
            this.textBox4FusyoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoY2.Location = new System.Drawing.Point(178, 374);
            this.textBox4FusyoY2.MaxLength = 2;
            this.textBox4FusyoY2.Name = "textBox4FusyoY2";
            this.textBox4FusyoY2.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoY2.TabIndex = 81;
            this.textBox4FusyoY2.TabStop = false;
            this.textBox4FusyoY2.Visible = false;
            // 
            // textBox3FusyoY2
            // 
            this.textBox3FusyoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoY2.Location = new System.Drawing.Point(179, 322);
            this.textBox3FusyoY2.MaxLength = 2;
            this.textBox3FusyoY2.Name = "textBox3FusyoY2";
            this.textBox3FusyoY2.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoY2.TabIndex = 66;
            this.textBox3FusyoY2.TabStop = false;
            this.textBox3FusyoY2.Visible = false;
            // 
            // textBox2FusyoY2
            // 
            this.textBox2FusyoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoY2.Location = new System.Drawing.Point(178, 270);
            this.textBox2FusyoY2.MaxLength = 2;
            this.textBox2FusyoY2.Name = "textBox2FusyoY2";
            this.textBox2FusyoY2.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoY2.TabIndex = 51;
            this.textBox2FusyoY2.TabStop = false;
            this.textBox2FusyoY2.Visible = false;
            // 
            // textBox1FusyoY2
            // 
            this.textBox1FusyoY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoY2.Location = new System.Drawing.Point(178, 218);
            this.textBox1FusyoY2.MaxLength = 2;
            this.textBox1FusyoY2.Name = "textBox1FusyoY2";
            this.textBox1FusyoY2.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoY2.TabIndex = 36;
            this.textBox1FusyoY2.Visible = false;
            // 
            // textBo5FusyoM2
            // 
            this.textBo5FusyoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBo5FusyoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBo5FusyoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBo5FusyoM2.Location = new System.Drawing.Point(227, 426);
            this.textBo5FusyoM2.MaxLength = 2;
            this.textBo5FusyoM2.Name = "textBo5FusyoM2";
            this.textBo5FusyoM2.Size = new System.Drawing.Size(32, 23);
            this.textBo5FusyoM2.TabIndex = 99;
            this.textBo5FusyoM2.TabStop = false;
            this.textBo5FusyoM2.Visible = false;
            // 
            // textBox4FusyoM2
            // 
            this.textBox4FusyoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoM2.Location = new System.Drawing.Point(227, 374);
            this.textBox4FusyoM2.MaxLength = 2;
            this.textBox4FusyoM2.Name = "textBox4FusyoM2";
            this.textBox4FusyoM2.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoM2.TabIndex = 84;
            this.textBox4FusyoM2.TabStop = false;
            this.textBox4FusyoM2.Visible = false;
            // 
            // textBox3FusyoM2
            // 
            this.textBox3FusyoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoM2.Location = new System.Drawing.Point(228, 322);
            this.textBox3FusyoM2.MaxLength = 2;
            this.textBox3FusyoM2.Name = "textBox3FusyoM2";
            this.textBox3FusyoM2.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoM2.TabIndex = 69;
            this.textBox3FusyoM2.TabStop = false;
            this.textBox3FusyoM2.Visible = false;
            // 
            // textBox2FusyoM2
            // 
            this.textBox2FusyoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoM2.Location = new System.Drawing.Point(227, 270);
            this.textBox2FusyoM2.MaxLength = 2;
            this.textBox2FusyoM2.Name = "textBox2FusyoM2";
            this.textBox2FusyoM2.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoM2.TabIndex = 54;
            this.textBox2FusyoM2.TabStop = false;
            this.textBox2FusyoM2.Visible = false;
            // 
            // textBox1FusyoM2
            // 
            this.textBox1FusyoM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoM2.Location = new System.Drawing.Point(227, 218);
            this.textBox1FusyoM2.MaxLength = 2;
            this.textBox1FusyoM2.Name = "textBox1FusyoM2";
            this.textBox1FusyoM2.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoM2.TabIndex = 39;
            this.textBox1FusyoM2.Visible = false;
            // 
            // textBox5FusyoD2
            // 
            this.textBox5FusyoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoD2.Location = new System.Drawing.Point(276, 426);
            this.textBox5FusyoD2.MaxLength = 2;
            this.textBox5FusyoD2.Name = "textBox5FusyoD2";
            this.textBox5FusyoD2.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoD2.TabIndex = 102;
            this.textBox5FusyoD2.TabStop = false;
            this.textBox5FusyoD2.Visible = false;
            // 
            // textBox4FusyoD2
            // 
            this.textBox4FusyoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoD2.Location = new System.Drawing.Point(276, 374);
            this.textBox4FusyoD2.MaxLength = 2;
            this.textBox4FusyoD2.Name = "textBox4FusyoD2";
            this.textBox4FusyoD2.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoD2.TabIndex = 87;
            this.textBox4FusyoD2.TabStop = false;
            this.textBox4FusyoD2.Visible = false;
            // 
            // textBox3FusyoD2
            // 
            this.textBox3FusyoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoD2.Location = new System.Drawing.Point(277, 322);
            this.textBox3FusyoD2.MaxLength = 2;
            this.textBox3FusyoD2.Name = "textBox3FusyoD2";
            this.textBox3FusyoD2.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoD2.TabIndex = 72;
            this.textBox3FusyoD2.TabStop = false;
            this.textBox3FusyoD2.Visible = false;
            // 
            // textBox2FusyoD2
            // 
            this.textBox2FusyoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoD2.Location = new System.Drawing.Point(276, 270);
            this.textBox2FusyoD2.MaxLength = 2;
            this.textBox2FusyoD2.Name = "textBox2FusyoD2";
            this.textBox2FusyoD2.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoD2.TabIndex = 57;
            this.textBox2FusyoD2.TabStop = false;
            this.textBox2FusyoD2.Visible = false;
            // 
            // textBox1FusyoD2
            // 
            this.textBox1FusyoD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoD2.Location = new System.Drawing.Point(276, 218);
            this.textBox1FusyoD2.MaxLength = 2;
            this.textBox1FusyoD2.Name = "textBox1FusyoD2";
            this.textBox1FusyoD2.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoD2.TabIndex = 42;
            this.textBox1FusyoD2.Visible = false;
            // 
            // textBox5FusyoTen2
            // 
            this.textBox5FusyoTen2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoTen2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoTen2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoTen2.Location = new System.Drawing.Point(367, 426);
            this.textBox5FusyoTen2.MaxLength = 2;
            this.textBox5FusyoTen2.Name = "textBox5FusyoTen2";
            this.textBox5FusyoTen2.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoTen2.TabIndex = 107;
            this.textBox5FusyoTen2.TabStop = false;
            this.textBox5FusyoTen2.Visible = false;
            this.textBox5FusyoTen2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox4FusyoTen2
            // 
            this.textBox4FusyoTen2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoTen2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoTen2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoTen2.Location = new System.Drawing.Point(367, 374);
            this.textBox4FusyoTen2.MaxLength = 2;
            this.textBox4FusyoTen2.Name = "textBox4FusyoTen2";
            this.textBox4FusyoTen2.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoTen2.TabIndex = 92;
            this.textBox4FusyoTen2.TabStop = false;
            this.textBox4FusyoTen2.Visible = false;
            this.textBox4FusyoTen2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox3FusyoTen2
            // 
            this.textBox3FusyoTen2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoTen2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoTen2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoTen2.Location = new System.Drawing.Point(368, 322);
            this.textBox3FusyoTen2.MaxLength = 2;
            this.textBox3FusyoTen2.Name = "textBox3FusyoTen2";
            this.textBox3FusyoTen2.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoTen2.TabIndex = 77;
            this.textBox3FusyoTen2.TabStop = false;
            this.textBox3FusyoTen2.Visible = false;
            this.textBox3FusyoTen2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox2FusyoTen2
            // 
            this.textBox2FusyoTen2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoTen2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoTen2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoTen2.Location = new System.Drawing.Point(367, 270);
            this.textBox2FusyoTen2.MaxLength = 2;
            this.textBox2FusyoTen2.Name = "textBox2FusyoTen2";
            this.textBox2FusyoTen2.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoTen2.TabIndex = 62;
            this.textBox2FusyoTen2.TabStop = false;
            this.textBox2FusyoTen2.Visible = false;
            this.textBox2FusyoTen2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox1FusyoTen2
            // 
            this.textBox1FusyoTen2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoTen2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoTen2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoTen2.Location = new System.Drawing.Point(367, 218);
            this.textBox1FusyoTen2.MaxLength = 2;
            this.textBox1FusyoTen2.Name = "textBox1FusyoTen2";
            this.textBox1FusyoTen2.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoTen2.TabIndex = 47;
            this.textBox1FusyoTen2.Visible = false;
            this.textBox1FusyoTen2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox5FusyoDays2
            // 
            this.textBox5FusyoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoDays2.Location = new System.Drawing.Point(329, 426);
            this.textBox5FusyoDays2.MaxLength = 2;
            this.textBox5FusyoDays2.Name = "textBox5FusyoDays2";
            this.textBox5FusyoDays2.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoDays2.TabIndex = 105;
            this.textBox5FusyoDays2.TabStop = false;
            this.textBox5FusyoDays2.Visible = false;
            this.textBox5FusyoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox4FusyoDays2
            // 
            this.textBox4FusyoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoDays2.Location = new System.Drawing.Point(329, 374);
            this.textBox4FusyoDays2.MaxLength = 2;
            this.textBox4FusyoDays2.Name = "textBox4FusyoDays2";
            this.textBox4FusyoDays2.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoDays2.TabIndex = 90;
            this.textBox4FusyoDays2.TabStop = false;
            this.textBox4FusyoDays2.Visible = false;
            this.textBox4FusyoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox3FusyoDays2
            // 
            this.textBox3FusyoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoDays2.Location = new System.Drawing.Point(330, 322);
            this.textBox3FusyoDays2.MaxLength = 2;
            this.textBox3FusyoDays2.Name = "textBox3FusyoDays2";
            this.textBox3FusyoDays2.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoDays2.TabIndex = 75;
            this.textBox3FusyoDays2.TabStop = false;
            this.textBox3FusyoDays2.Visible = false;
            this.textBox3FusyoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox2FusyoDays2
            // 
            this.textBox2FusyoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoDays2.Location = new System.Drawing.Point(329, 270);
            this.textBox2FusyoDays2.MaxLength = 2;
            this.textBox2FusyoDays2.Name = "textBox2FusyoDays2";
            this.textBox2FusyoDays2.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoDays2.TabIndex = 60;
            this.textBox2FusyoDays2.TabStop = false;
            this.textBox2FusyoDays2.Visible = false;
            this.textBox2FusyoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox1FusyoDays2
            // 
            this.textBox1FusyoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoDays2.Location = new System.Drawing.Point(329, 218);
            this.textBox1FusyoDays2.MaxLength = 2;
            this.textBox1FusyoDays2.Name = "textBox1FusyoDays2";
            this.textBox1FusyoDays2.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoDays2.TabIndex = 45;
            this.textBox1FusyoDays2.Visible = false;
            this.textBox1FusyoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox5Fusyo2
            // 
            this.textBox5Fusyo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5Fusyo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5Fusyo2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox5Fusyo2.Location = new System.Drawing.Point(3, 426);
            this.textBox5Fusyo2.Name = "textBox5Fusyo2";
            this.textBox5Fusyo2.Size = new System.Drawing.Size(168, 23);
            this.textBox5Fusyo2.TabIndex = 94;
            this.textBox5Fusyo2.TabStop = false;
            this.textBox5Fusyo2.Visible = false;
            this.textBox5Fusyo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox4Fusyo2
            // 
            this.textBox4Fusyo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4Fusyo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4Fusyo2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox4Fusyo2.Location = new System.Drawing.Point(3, 374);
            this.textBox4Fusyo2.Name = "textBox4Fusyo2";
            this.textBox4Fusyo2.Size = new System.Drawing.Size(168, 23);
            this.textBox4Fusyo2.TabIndex = 79;
            this.textBox4Fusyo2.TabStop = false;
            this.textBox4Fusyo2.Visible = false;
            this.textBox4Fusyo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox3Fusyo2
            // 
            this.textBox3Fusyo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3Fusyo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3Fusyo2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox3Fusyo2.Location = new System.Drawing.Point(4, 322);
            this.textBox3Fusyo2.Name = "textBox3Fusyo2";
            this.textBox3Fusyo2.Size = new System.Drawing.Size(168, 23);
            this.textBox3Fusyo2.TabIndex = 64;
            this.textBox3Fusyo2.TabStop = false;
            this.textBox3Fusyo2.Visible = false;
            this.textBox3Fusyo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox2Fusyo2
            // 
            this.textBox2Fusyo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2Fusyo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2Fusyo2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox2Fusyo2.Location = new System.Drawing.Point(3, 270);
            this.textBox2Fusyo2.Name = "textBox2Fusyo2";
            this.textBox2Fusyo2.Size = new System.Drawing.Size(168, 23);
            this.textBox2Fusyo2.TabIndex = 49;
            this.textBox2Fusyo2.TabStop = false;
            this.textBox2Fusyo2.Visible = false;
            this.textBox2Fusyo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox1Fusyo2
            // 
            this.textBox1Fusyo2.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1Fusyo2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1Fusyo2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox1Fusyo2.Location = new System.Drawing.Point(3, 218);
            this.textBox1Fusyo2.Name = "textBox1Fusyo2";
            this.textBox1Fusyo2.Size = new System.Drawing.Size(168, 23);
            this.textBox1Fusyo2.TabIndex = 34;
            this.textBox1Fusyo2.Visible = false;
            this.textBox1Fusyo2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(210, 418);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 97;
            this.label28.Text = "年";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(210, 366);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 82;
            this.label25.Text = "年";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(211, 314);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 67;
            this.label21.Text = "年";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(210, 262);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 52;
            this.label13.Text = "年";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(210, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 37;
            this.label1.Text = "年";
            // 
            // textBox5FusyoY
            // 
            this.textBox5FusyoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoY.Location = new System.Drawing.Point(178, 404);
            this.textBox5FusyoY.MaxLength = 2;
            this.textBox5FusyoY.Name = "textBox5FusyoY";
            this.textBox5FusyoY.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoY.TabIndex = 95;
            this.textBox5FusyoY.TabStop = false;
            // 
            // textBox4FusyoY
            // 
            this.textBox4FusyoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoY.Location = new System.Drawing.Point(178, 352);
            this.textBox4FusyoY.MaxLength = 2;
            this.textBox4FusyoY.Name = "textBox4FusyoY";
            this.textBox4FusyoY.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoY.TabIndex = 80;
            this.textBox4FusyoY.TabStop = false;
            // 
            // textBox3FusyoY
            // 
            this.textBox3FusyoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoY.Location = new System.Drawing.Point(179, 300);
            this.textBox3FusyoY.MaxLength = 2;
            this.textBox3FusyoY.Name = "textBox3FusyoY";
            this.textBox3FusyoY.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoY.TabIndex = 65;
            this.textBox3FusyoY.TabStop = false;
            // 
            // textBox2FusyoY
            // 
            this.textBox2FusyoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoY.Location = new System.Drawing.Point(178, 248);
            this.textBox2FusyoY.MaxLength = 2;
            this.textBox2FusyoY.Name = "textBox2FusyoY";
            this.textBox2FusyoY.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoY.TabIndex = 50;
            this.textBox2FusyoY.TabStop = false;
            // 
            // textBox1FusyoY
            // 
            this.textBox1FusyoY.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoY.Location = new System.Drawing.Point(178, 196);
            this.textBox1FusyoY.MaxLength = 2;
            this.textBox1FusyoY.Name = "textBox1FusyoY";
            this.textBox1FusyoY.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoY.TabIndex = 35;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(259, 418);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 100;
            this.label27.Text = "月";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(259, 366);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 85;
            this.label24.Text = "月";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(260, 314);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 70;
            this.label19.Text = "月";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(259, 262);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 55;
            this.label12.Text = "月";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(259, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 40;
            this.label2.Text = "月";
            // 
            // textBox5FusyoM
            // 
            this.textBox5FusyoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoM.Location = new System.Drawing.Point(227, 404);
            this.textBox5FusyoM.MaxLength = 2;
            this.textBox5FusyoM.Name = "textBox5FusyoM";
            this.textBox5FusyoM.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoM.TabIndex = 98;
            this.textBox5FusyoM.TabStop = false;
            // 
            // textBox4FusyoM
            // 
            this.textBox4FusyoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoM.Location = new System.Drawing.Point(227, 352);
            this.textBox4FusyoM.MaxLength = 2;
            this.textBox4FusyoM.Name = "textBox4FusyoM";
            this.textBox4FusyoM.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoM.TabIndex = 83;
            this.textBox4FusyoM.TabStop = false;
            // 
            // textBox3FusyoM
            // 
            this.textBox3FusyoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoM.Location = new System.Drawing.Point(228, 300);
            this.textBox3FusyoM.MaxLength = 2;
            this.textBox3FusyoM.Name = "textBox3FusyoM";
            this.textBox3FusyoM.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoM.TabIndex = 68;
            this.textBox3FusyoM.TabStop = false;
            // 
            // textBox2FusyoM
            // 
            this.textBox2FusyoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoM.Location = new System.Drawing.Point(227, 248);
            this.textBox2FusyoM.MaxLength = 2;
            this.textBox2FusyoM.Name = "textBox2FusyoM";
            this.textBox2FusyoM.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoM.TabIndex = 53;
            this.textBox2FusyoM.TabStop = false;
            // 
            // textBox1FusyoM
            // 
            this.textBox1FusyoM.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoM.Location = new System.Drawing.Point(227, 196);
            this.textBox1FusyoM.MaxLength = 2;
            this.textBox1FusyoM.Name = "textBox1FusyoM";
            this.textBox1FusyoM.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoM.TabIndex = 38;
            // 
            // textBox5FusyoD
            // 
            this.textBox5FusyoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoD.Location = new System.Drawing.Point(276, 404);
            this.textBox5FusyoD.MaxLength = 2;
            this.textBox5FusyoD.Name = "textBox5FusyoD";
            this.textBox5FusyoD.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoD.TabIndex = 101;
            this.textBox5FusyoD.TabStop = false;
            // 
            // textBox4FusyoD
            // 
            this.textBox4FusyoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoD.Location = new System.Drawing.Point(276, 352);
            this.textBox4FusyoD.MaxLength = 2;
            this.textBox4FusyoD.Name = "textBox4FusyoD";
            this.textBox4FusyoD.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoD.TabIndex = 86;
            this.textBox4FusyoD.TabStop = false;
            // 
            // textBox3FusyoD
            // 
            this.textBox3FusyoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoD.Location = new System.Drawing.Point(277, 300);
            this.textBox3FusyoD.MaxLength = 2;
            this.textBox3FusyoD.Name = "textBox3FusyoD";
            this.textBox3FusyoD.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoD.TabIndex = 71;
            this.textBox3FusyoD.TabStop = false;
            // 
            // textBox2FusyoD
            // 
            this.textBox2FusyoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoD.Location = new System.Drawing.Point(276, 248);
            this.textBox2FusyoD.MaxLength = 2;
            this.textBox2FusyoD.Name = "textBox2FusyoD";
            this.textBox2FusyoD.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoD.TabIndex = 56;
            this.textBox2FusyoD.TabStop = false;
            // 
            // textBox1FusyoD
            // 
            this.textBox1FusyoD.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoD.Location = new System.Drawing.Point(276, 196);
            this.textBox1FusyoD.MaxLength = 2;
            this.textBox1FusyoD.Name = "textBox1FusyoD";
            this.textBox1FusyoD.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoD.TabIndex = 41;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(308, 418);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(17, 12);
            this.label26.TabIndex = 103;
            this.label26.Text = "日";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(308, 366);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 88;
            this.label23.Text = "日";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(309, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 73;
            this.label14.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(308, 262);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 58;
            this.label11.Text = "日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(308, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 43;
            this.label7.Text = "日";
            // 
            // textBoxBirthday2
            // 
            this.textBoxBirthday2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBirthday2.Location = new System.Drawing.Point(161, 86);
            this.textBoxBirthday2.Name = "textBoxBirthday2";
            this.textBoxBirthday2.Size = new System.Drawing.Size(26, 26);
            this.textBoxBirthday2.TabIndex = 12;
            this.textBoxBirthday2.Visible = false;
            this.textBoxBirthday2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxSex2
            // 
            this.textBoxSex2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxSex2.Location = new System.Drawing.Point(43, 86);
            this.textBoxSex2.MaxLength = 1;
            this.textBoxSex2.Name = "textBoxSex2";
            this.textBoxSex2.Size = new System.Drawing.Size(26, 26);
            this.textBoxSex2.TabIndex = 8;
            this.textBoxSex2.Visible = false;
            this.textBoxSex2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBY2
            // 
            this.textBoxBY2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBY2.Location = new System.Drawing.Point(216, 86);
            this.textBoxBY2.MaxLength = 2;
            this.textBoxBY2.Name = "textBoxBY2";
            this.textBoxBY2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBY2.TabIndex = 15;
            this.textBoxBY2.Visible = false;
            this.textBoxBY2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBM2
            // 
            this.textBoxBM2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBM2.Location = new System.Drawing.Point(265, 86);
            this.textBoxBM2.MaxLength = 2;
            this.textBoxBM2.Name = "textBoxBM2";
            this.textBoxBM2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBM2.TabIndex = 18;
            this.textBoxBM2.Visible = false;
            this.textBoxBM2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBD2
            // 
            this.textBoxBD2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBD2.Location = new System.Drawing.Point(314, 86);
            this.textBoxBD2.MaxLength = 2;
            this.textBoxBD2.Name = "textBoxBD2";
            this.textBoxBD2.Size = new System.Drawing.Size(32, 26);
            this.textBoxBD2.TabIndex = 21;
            this.textBoxBD2.Visible = false;
            this.textBoxBD2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxHnum2
            // 
            this.textBoxHnum2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxHnum2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHnum2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHnum2.Location = new System.Drawing.Point(43, 29);
            this.textBoxHnum2.Name = "textBoxHnum2";
            this.textBoxHnum2.Size = new System.Drawing.Size(85, 26);
            this.textBoxHnum2.TabIndex = 1;
            this.textBoxHnum2.Visible = false;
            this.textBoxHnum2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxRatio2
            // 
            this.textBoxRatio2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxRatio2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxRatio2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxRatio2.Location = new System.Drawing.Point(170, 29);
            this.textBoxRatio2.MaxLength = 1;
            this.textBoxRatio2.Name = "textBoxRatio2";
            this.textBoxRatio2.Size = new System.Drawing.Size(26, 26);
            this.textBoxRatio2.TabIndex = 3;
            this.textBoxRatio2.Visible = false;
            this.textBoxRatio2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox5FusyoTen
            // 
            this.textBox5FusyoTen.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoTen.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoTen.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoTen.Location = new System.Drawing.Point(367, 404);
            this.textBox5FusyoTen.MaxLength = 2;
            this.textBox5FusyoTen.Name = "textBox5FusyoTen";
            this.textBox5FusyoTen.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoTen.TabIndex = 106;
            this.textBox5FusyoTen.TabStop = false;
            this.textBox5FusyoTen.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox4FusyoTen
            // 
            this.textBox4FusyoTen.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoTen.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoTen.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoTen.Location = new System.Drawing.Point(367, 352);
            this.textBox4FusyoTen.MaxLength = 2;
            this.textBox4FusyoTen.Name = "textBox4FusyoTen";
            this.textBox4FusyoTen.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoTen.TabIndex = 91;
            this.textBox4FusyoTen.TabStop = false;
            this.textBox4FusyoTen.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox3FusyoTen
            // 
            this.textBox3FusyoTen.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoTen.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoTen.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoTen.Location = new System.Drawing.Point(368, 300);
            this.textBox3FusyoTen.MaxLength = 2;
            this.textBox3FusyoTen.Name = "textBox3FusyoTen";
            this.textBox3FusyoTen.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoTen.TabIndex = 76;
            this.textBox3FusyoTen.TabStop = false;
            this.textBox3FusyoTen.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox2FusyoTen
            // 
            this.textBox2FusyoTen.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoTen.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoTen.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoTen.Location = new System.Drawing.Point(367, 248);
            this.textBox2FusyoTen.MaxLength = 2;
            this.textBox2FusyoTen.Name = "textBox2FusyoTen";
            this.textBox2FusyoTen.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoTen.TabIndex = 61;
            this.textBox2FusyoTen.TabStop = false;
            this.textBox2FusyoTen.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox1FusyoTen
            // 
            this.textBox1FusyoTen.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoTen.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoTen.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoTen.Location = new System.Drawing.Point(367, 196);
            this.textBox1FusyoTen.MaxLength = 2;
            this.textBox1FusyoTen.Name = "textBox1FusyoTen";
            this.textBox1FusyoTen.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoTen.TabIndex = 46;
            this.textBox1FusyoTen.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox5FusyoDays
            // 
            this.textBox5FusyoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5FusyoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5FusyoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox5FusyoDays.Location = new System.Drawing.Point(329, 404);
            this.textBox5FusyoDays.MaxLength = 2;
            this.textBox5FusyoDays.Name = "textBox5FusyoDays";
            this.textBox5FusyoDays.Size = new System.Drawing.Size(32, 23);
            this.textBox5FusyoDays.TabIndex = 104;
            this.textBox5FusyoDays.TabStop = false;
            this.textBox5FusyoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox4FusyoDays
            // 
            this.textBox4FusyoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4FusyoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4FusyoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox4FusyoDays.Location = new System.Drawing.Point(329, 352);
            this.textBox4FusyoDays.MaxLength = 2;
            this.textBox4FusyoDays.Name = "textBox4FusyoDays";
            this.textBox4FusyoDays.Size = new System.Drawing.Size(32, 23);
            this.textBox4FusyoDays.TabIndex = 89;
            this.textBox4FusyoDays.TabStop = false;
            this.textBox4FusyoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox3FusyoDays
            // 
            this.textBox3FusyoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3FusyoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3FusyoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox3FusyoDays.Location = new System.Drawing.Point(330, 300);
            this.textBox3FusyoDays.MaxLength = 2;
            this.textBox3FusyoDays.Name = "textBox3FusyoDays";
            this.textBox3FusyoDays.Size = new System.Drawing.Size(32, 23);
            this.textBox3FusyoDays.TabIndex = 74;
            this.textBox3FusyoDays.TabStop = false;
            this.textBox3FusyoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox2FusyoDays
            // 
            this.textBox2FusyoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2FusyoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2FusyoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox2FusyoDays.Location = new System.Drawing.Point(329, 248);
            this.textBox2FusyoDays.MaxLength = 2;
            this.textBox2FusyoDays.Name = "textBox2FusyoDays";
            this.textBox2FusyoDays.Size = new System.Drawing.Size(32, 23);
            this.textBox2FusyoDays.TabIndex = 59;
            this.textBox2FusyoDays.TabStop = false;
            this.textBox2FusyoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBox1FusyoDays
            // 
            this.textBox1FusyoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1FusyoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1FusyoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1FusyoDays.Location = new System.Drawing.Point(329, 196);
            this.textBox1FusyoDays.MaxLength = 2;
            this.textBox1FusyoDays.Name = "textBox1FusyoDays";
            this.textBox1FusyoDays.Size = new System.Drawing.Size(32, 23);
            this.textBox1FusyoDays.TabIndex = 44;
            this.textBox1FusyoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDays
            // 
            this.textBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDays.Location = new System.Drawing.Point(209, 118);
            this.textBoxDays.MaxLength = 2;
            this.textBoxDays.Name = "textBoxDays";
            this.textBoxDays.Size = new System.Drawing.Size(32, 26);
            this.textBoxDays.TabIndex = 27;
            this.textBoxDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxDays2
            // 
            this.textBoxDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxDays2.Location = new System.Drawing.Point(209, 143);
            this.textBoxDays2.MaxLength = 2;
            this.textBoxDays2.Name = "textBoxDays2";
            this.textBoxDays2.Size = new System.Drawing.Size(32, 26);
            this.textBoxDays2.TabIndex = 28;
            this.textBoxDays2.Visible = false;
            this.textBoxDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(179, 120);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(29, 24);
            this.labelDays.TabIndex = 26;
            this.labelDays.Text = "診療\r\n日数";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(248, 75);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 16;
            this.label18.Text = "年";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label34.Location = new System.Drawing.Point(238, 452);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(161, 12);
            this.label34.TabIndex = 108;
            this.label34.Text = "※転帰　治癒:1　中止:2　転医:3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(70, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "男 : 1\r\n女 : 2";
            // 
            // textBoxBY
            // 
            this.textBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBY.Location = new System.Drawing.Point(216, 61);
            this.textBoxBY.MaxLength = 2;
            this.textBoxBY.Name = "textBoxBY";
            this.textBoxBY.Size = new System.Drawing.Size(32, 26);
            this.textBoxBY.TabIndex = 13;
            this.textBoxBY.TextChanged += new System.EventHandler(this.textBoxPersons_TextChanged);
            this.textBoxBY.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxSex
            // 
            this.textBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxSex.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxSex.Location = new System.Drawing.Point(43, 61);
            this.textBoxSex.MaxLength = 1;
            this.textBoxSex.Name = "textBoxSex";
            this.textBoxSex.Size = new System.Drawing.Size(26, 26);
            this.textBoxSex.TabIndex = 7;
            this.textBoxSex.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(297, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 19;
            this.label17.Text = "月";
            // 
            // textBoxShinryoDays2
            // 
            this.textBoxShinryoDays2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShinryoDays2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShinryoDays2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShinryoDays2.Location = new System.Drawing.Point(43, 528);
            this.textBoxShinryoDays2.Name = "textBoxShinryoDays2";
            this.textBoxShinryoDays2.Size = new System.Drawing.Size(360, 26);
            this.textBoxShinryoDays2.TabIndex = 115;
            this.textBoxShinryoDays2.Visible = false;
            this.textBoxShinryoDays2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(14, 75);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 6;
            this.labelSex.Text = "性別";
            // 
            // textBoxCharge2
            // 
            this.textBoxCharge2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxCharge2.Location = new System.Drawing.Point(161, 585);
            this.textBoxCharge2.Name = "textBoxCharge2";
            this.textBoxCharge2.Size = new System.Drawing.Size(80, 26);
            this.textBoxCharge2.TabIndex = 121;
            this.textBoxCharge2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCharge2.Visible = false;
            this.textBoxCharge2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBM
            // 
            this.textBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBM.Location = new System.Drawing.Point(265, 61);
            this.textBoxBM.MaxLength = 2;
            this.textBoxBM.Name = "textBoxBM";
            this.textBoxBM.Size = new System.Drawing.Size(32, 26);
            this.textBoxBM.TabIndex = 17;
            this.textBoxBM.TextChanged += new System.EventHandler(this.textBoxPersons_TextChanged);
            this.textBoxBM.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxTotal2
            // 
            this.textBoxTotal2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTotal2.Location = new System.Drawing.Point(43, 585);
            this.textBoxTotal2.Name = "textBoxTotal2";
            this.textBoxTotal2.Size = new System.Drawing.Size(80, 26);
            this.textBoxTotal2.TabIndex = 118;
            this.textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal2.Visible = false;
            this.textBoxTotal2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBirthday.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBirthday.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBirthday.Location = new System.Drawing.Point(161, 61);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(26, 26);
            this.textBoxBirthday.TabIndex = 11;
            this.textBoxBirthday.TextChanged += new System.EventHandler(this.textBoxPersons_TextChanged);
            this.textBoxBirthday.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(141, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 24);
            this.label15.TabIndex = 2;
            this.label15.Text = "給付\r\n割合";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(187, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 24);
            this.label5.TabIndex = 14;
            this.label5.Text = "昭：3\r\n平：4";
            // 
            // textBoxBD
            // 
            this.textBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxBD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxBD.Location = new System.Drawing.Point(314, 61);
            this.textBoxBD.MaxLength = 2;
            this.textBoxBD.Name = "textBoxBD";
            this.textBoxBD.Size = new System.Drawing.Size(32, 26);
            this.textBoxBD.TabIndex = 20;
            this.textBoxBD.TextChanged += new System.EventHandler(this.textBoxPersons_TextChanged);
            this.textBoxBD.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxBD.Leave += new System.EventHandler(this.textBoxBD_Leave);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(132, 63);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(29, 24);
            this.labelBirthday.TabIndex = 10;
            this.labelBirthday.Text = "生年\r\n月日";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(346, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 22;
            this.label16.Text = "日";
            // 
            // textBoxRatio
            // 
            this.textBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxRatio.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxRatio.Location = new System.Drawing.Point(170, 4);
            this.textBoxRatio.MaxLength = 1;
            this.textBoxRatio.Name = "textBoxRatio";
            this.textBoxRatio.Size = new System.Drawing.Size(26, 26);
            this.textBoxRatio.TabIndex = 2;
            this.textBoxRatio.TextChanged += new System.EventHandler(this.textBoxRatioTotal_TextChanged);
            this.textBoxRatio.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxName2
            // 
            this.textBoxName2.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxName2.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxName2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxName2.Location = new System.Drawing.Point(43, 143);
            this.textBoxName2.Name = "textBoxName2";
            this.textBoxName2.Size = new System.Drawing.Size(120, 26);
            this.textBoxName2.TabIndex = 25;
            this.textBoxName2.Visible = false;
            this.textBoxName2.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(365, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 32;
            this.label10.Text = "転帰";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(327, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 31;
            this.label9.Text = "日数";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(176, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 30;
            this.label6.Text = "初検日";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1, 181);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 29;
            this.label22.Text = "負傷名";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox5Fusyo
            // 
            this.textBox5Fusyo.BackColor = System.Drawing.SystemColors.Info;
            this.textBox5Fusyo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox5Fusyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox5Fusyo.Location = new System.Drawing.Point(3, 404);
            this.textBox5Fusyo.Name = "textBox5Fusyo";
            this.textBox5Fusyo.Size = new System.Drawing.Size(168, 23);
            this.textBox5Fusyo.TabIndex = 93;
            this.textBox5Fusyo.TabStop = false;
            this.textBox5Fusyo.TextChanged += new System.EventHandler(this.textBox5Fusyo_TextChanged);
            this.textBox5Fusyo.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBox5Fusyo.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBox4Fusyo
            // 
            this.textBox4Fusyo.BackColor = System.Drawing.SystemColors.Info;
            this.textBox4Fusyo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox4Fusyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox4Fusyo.Location = new System.Drawing.Point(3, 352);
            this.textBox4Fusyo.Name = "textBox4Fusyo";
            this.textBox4Fusyo.Size = new System.Drawing.Size(168, 23);
            this.textBox4Fusyo.TabIndex = 78;
            this.textBox4Fusyo.TabStop = false;
            this.textBox4Fusyo.TextChanged += new System.EventHandler(this.textBox4Fusyo_TextChanged);
            this.textBox4Fusyo.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBox4Fusyo.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBox3Fusyo
            // 
            this.textBox3Fusyo.BackColor = System.Drawing.SystemColors.Info;
            this.textBox3Fusyo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox3Fusyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox3Fusyo.Location = new System.Drawing.Point(4, 300);
            this.textBox3Fusyo.Name = "textBox3Fusyo";
            this.textBox3Fusyo.Size = new System.Drawing.Size(168, 23);
            this.textBox3Fusyo.TabIndex = 63;
            this.textBox3Fusyo.TabStop = false;
            this.textBox3Fusyo.TextChanged += new System.EventHandler(this.textBox3Fusyo_TextChanged);
            this.textBox3Fusyo.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBox3Fusyo.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBox2Fusyo
            // 
            this.textBox2Fusyo.BackColor = System.Drawing.SystemColors.Info;
            this.textBox2Fusyo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox2Fusyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox2Fusyo.Location = new System.Drawing.Point(3, 248);
            this.textBox2Fusyo.Name = "textBox2Fusyo";
            this.textBox2Fusyo.Size = new System.Drawing.Size(168, 23);
            this.textBox2Fusyo.TabIndex = 48;
            this.textBox2Fusyo.TabStop = false;
            this.textBox2Fusyo.TextChanged += new System.EventHandler(this.textBox2Fusyo_TextChanged);
            this.textBox2Fusyo.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBox2Fusyo.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // textBox1Fusyo
            // 
            this.textBox1Fusyo.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1Fusyo.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1Fusyo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox1Fusyo.Location = new System.Drawing.Point(3, 196);
            this.textBox1Fusyo.Name = "textBox1Fusyo";
            this.textBox1Fusyo.Size = new System.Drawing.Size(168, 23);
            this.textBox1Fusyo.TabIndex = 33;
            this.textBox1Fusyo.TextChanged += new System.EventHandler(this.textBox1Fusyo_TextChanged);
            this.textBox1Fusyo.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBox1Fusyo.Leave += new System.EventHandler(this.textBoxKanaToHalf_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 517);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 113;
            this.label29.Text = "診療日";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxShinryoDays
            // 
            this.textBoxShinryoDays.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxShinryoDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShinryoDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxShinryoDays.Location = new System.Drawing.Point(43, 503);
            this.textBoxShinryoDays.Name = "textBoxShinryoDays";
            this.textBoxShinryoDays.Size = new System.Drawing.Size(360, 26);
            this.textBoxShinryoDays.TabIndex = 114;
            this.textBoxShinryoDays.Enter += new System.EventHandler(this.textBoxs_Enter);
            this.textBoxShinryoDays.Leave += new System.EventHandler(this.textBoxShinryoDays_Leave);
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxName.Location = new System.Drawing.Point(43, 118);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(120, 26);
            this.textBoxName.TabIndex = 24;
            this.textBoxName.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(2, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 23;
            this.label20.Text = "療養者\r\n氏名";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(14, 562);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 24);
            this.labelTotal.TabIndex = 116;
            this.labelTotal.Text = "合計\r\n金額";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(132, 562);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(29, 24);
            this.labelCharge.TabIndex = 119;
            this.labelCharge.Text = "請求\r\n金額";
            // 
            // textBoxCharge
            // 
            this.textBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxCharge.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxCharge.Location = new System.Drawing.Point(161, 560);
            this.textBoxCharge.Name = "textBoxCharge";
            this.textBoxCharge.Size = new System.Drawing.Size(80, 26);
            this.textBoxCharge.TabIndex = 120;
            this.textBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCharge.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxTotal.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxTotal.Location = new System.Drawing.Point(43, 560);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(80, 26);
            this.textBoxTotal.TabIndex = 117;
            this.textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotal.TextChanged += new System.EventHandler(this.textBoxRatioTotal_TextChanged);
            this.textBoxTotal.Enter += new System.EventHandler(this.textBoxs_Enter);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonExit);
            this.panel2.Controls.Add(this.buttonRegist);
            this.panel2.Controls.Add(this.labelInputerName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.panel2.Location = new System.Drawing.Point(0, 711);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(421, 28);
            this.panel2.TabIndex = 0;
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(8, 8);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(203, 16);
            this.labelInputerName.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(199, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 739);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(199, 739);
            this.panelDatalist.TabIndex = 0;
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(199, 739);
            this.dataGridViewPlist.TabIndex = 0;
            this.dataGridViewPlist.CurrentCellChanged += new System.EventHandler(this.dataGridViewPlist_CurrentCellChanged_1);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.AutoScroll = true;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControl1.Location = new System.Drawing.Point(202, 0);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(721, 739);
            this.scrollPictureControl1.TabIndex = 57;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.GreenYellow;
            this.panelUP.Controls.Add(this.labelNote1);
            this.panelUP.Controls.Add(this.labelGroupID);
            this.panelUP.Controls.Add(this.labelScanID);
            this.panelUP.Controls.Add(this.labelSeikyu);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(199, 35);
            this.panelUP.TabIndex = 2;
            // 
            // labelNote1
            // 
            this.labelNote1.AutoSize = true;
            this.labelNote1.Location = new System.Drawing.Point(2, 20);
            this.labelNote1.Name = "labelNote1";
            this.labelNote1.Size = new System.Drawing.Size(31, 12);
            this.labelNote1.TabIndex = 8;
            this.labelNote1.Text = "Note:";
            // 
            // labelGroupID
            // 
            this.labelGroupID.AutoSize = true;
            this.labelGroupID.Location = new System.Drawing.Point(93, 20);
            this.labelGroupID.Name = "labelGroupID";
            this.labelGroupID.Size = new System.Drawing.Size(48, 12);
            this.labelGroupID.TabIndex = 6;
            this.labelGroupID.Text = "GroupID:";
            // 
            // labelScanID
            // 
            this.labelScanID.AutoSize = true;
            this.labelScanID.Location = new System.Drawing.Point(93, 3);
            this.labelScanID.Name = "labelScanID";
            this.labelScanID.Size = new System.Drawing.Size(43, 12);
            this.labelScanID.TabIndex = 0;
            this.labelScanID.Text = "ScanID:";
            // 
            // labelSeikyu
            // 
            this.labelSeikyu.AutoSize = true;
            this.labelSeikyu.Location = new System.Drawing.Point(2, 3);
            this.labelSeikyu.Name = "labelSeikyu";
            this.labelSeikyu.Size = new System.Drawing.Size(31, 12);
            this.labelSeikyu.TabIndex = 5;
            this.labelSeikyu.Text = "請求:";
            // 
            // FormOCRCheckJyu2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 739);
            this.Controls.Add(this.scrollPictureControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelDatalist);
            this.KeyPreview = true;
            this.Name = "FormOCRCheckJyu2";
            this.Text = "OCR Check";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.panelRight.ResumeLayout(false);
            this.panelControlScroll.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.panelMonth.ResumeLayout(false);
            this.panelMonth.PerformLayout();
            this.panelReceipt.ResumeLayout(false);
            this.panelReceipt.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelDatalist.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.TextBox textBoxHnum;
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox textBoxSex;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.TextBox textBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelNumbering;
        private System.Windows.Forms.TextBox textBoxDrCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxBD;
        private System.Windows.Forms.TextBox textBoxBM;
        private System.Windows.Forms.TextBox textBoxBY;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.TextBox textBoxDays;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDrCode2;
        private System.Windows.Forms.TextBox textBoxHnum2;
        private System.Windows.Forms.TextBox textBoxY2;
        private System.Windows.Forms.TextBox textBoxM2;
        private System.Windows.Forms.TextBox textBoxBD2;
        private System.Windows.Forms.TextBox textBoxBM2;
        private System.Windows.Forms.TextBox textBoxBY2;
        private System.Windows.Forms.TextBox textBoxSex2;
        private System.Windows.Forms.TextBox textBoxBirthday2;
        private System.Windows.Forms.TextBox textBoxCharge2;
        private System.Windows.Forms.TextBox textBoxTotal2;
        private System.Windows.Forms.TextBox textBoxDays2;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxRatio2;
        private System.Windows.Forms.TextBox textBoxRatio;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelControlScroll;
        private System.Windows.Forms.TextBox textBox1Fusyo2;
        private System.Windows.Forms.TextBox textBox1Fusyo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxName2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxHosName;
        private System.Windows.Forms.TextBox textBoxHosName2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxDrName;
        private System.Windows.Forms.TextBox textBoxDrName2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxShinryoDays;
        private System.Windows.Forms.TextBox textBoxShinryoDays2;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Panel panelReceipt;
        private System.Windows.Forms.TextBox textBox5FusyoY2;
        private System.Windows.Forms.TextBox textBox4FusyoY2;
        private System.Windows.Forms.TextBox textBox3FusyoY2;
        private System.Windows.Forms.TextBox textBox2FusyoY2;
        private System.Windows.Forms.TextBox textBox1FusyoY2;
        private System.Windows.Forms.TextBox textBo5FusyoM2;
        private System.Windows.Forms.TextBox textBox4FusyoM2;
        private System.Windows.Forms.TextBox textBox3FusyoM2;
        private System.Windows.Forms.TextBox textBox2FusyoM2;
        private System.Windows.Forms.TextBox textBox1FusyoM2;
        private System.Windows.Forms.TextBox textBox5FusyoD2;
        private System.Windows.Forms.TextBox textBox4FusyoD2;
        private System.Windows.Forms.TextBox textBox3FusyoD2;
        private System.Windows.Forms.TextBox textBox2FusyoD2;
        private System.Windows.Forms.TextBox textBox1FusyoD2;
        private System.Windows.Forms.TextBox textBox5FusyoTen2;
        private System.Windows.Forms.TextBox textBox4FusyoTen2;
        private System.Windows.Forms.TextBox textBox3FusyoTen2;
        private System.Windows.Forms.TextBox textBox2FusyoTen2;
        private System.Windows.Forms.TextBox textBox1FusyoTen2;
        private System.Windows.Forms.TextBox textBox5FusyoDays2;
        private System.Windows.Forms.TextBox textBox4FusyoDays2;
        private System.Windows.Forms.TextBox textBox3FusyoDays2;
        private System.Windows.Forms.TextBox textBox2FusyoDays2;
        private System.Windows.Forms.TextBox textBox1FusyoDays2;
        private System.Windows.Forms.TextBox textBox5Fusyo2;
        private System.Windows.Forms.TextBox textBox4Fusyo2;
        private System.Windows.Forms.TextBox textBox3Fusyo2;
        private System.Windows.Forms.TextBox textBox2Fusyo2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox5FusyoY;
        private System.Windows.Forms.TextBox textBox4FusyoY;
        private System.Windows.Forms.TextBox textBox3FusyoY;
        private System.Windows.Forms.TextBox textBox2FusyoY;
        private System.Windows.Forms.TextBox textBox1FusyoY;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox5FusyoM;
        private System.Windows.Forms.TextBox textBox4FusyoM;
        private System.Windows.Forms.TextBox textBox3FusyoM;
        private System.Windows.Forms.TextBox textBox2FusyoM;
        private System.Windows.Forms.TextBox textBox1FusyoM;
        private System.Windows.Forms.TextBox textBox5FusyoD;
        private System.Windows.Forms.TextBox textBox4FusyoD;
        private System.Windows.Forms.TextBox textBox3FusyoD;
        private System.Windows.Forms.TextBox textBox2FusyoD;
        private System.Windows.Forms.TextBox textBox1FusyoD;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5FusyoTen;
        private System.Windows.Forms.TextBox textBox4FusyoTen;
        private System.Windows.Forms.TextBox textBox3FusyoTen;
        private System.Windows.Forms.TextBox textBox2FusyoTen;
        private System.Windows.Forms.TextBox textBox1FusyoTen;
        private System.Windows.Forms.TextBox textBox5FusyoDays;
        private System.Windows.Forms.TextBox textBox4FusyoDays;
        private System.Windows.Forms.TextBox textBox3FusyoDays;
        private System.Windows.Forms.TextBox textBox2FusyoDays;
        private System.Windows.Forms.TextBox textBox1FusyoDays;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5Fusyo;
        private System.Windows.Forms.TextBox textBox4Fusyo;
        private System.Windows.Forms.TextBox textBox3Fusyo;
        private System.Windows.Forms.TextBox textBox2Fusyo;
        private System.Windows.Forms.TextBox textBoxFutan2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxFutan;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panelMonth;
        private System.Windows.Forms.Label labelChargeInfo;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Label labelNote1;
        private System.Windows.Forms.Label labelGroupID;
        private System.Windows.Forms.Label labelScanID;
        private System.Windows.Forms.Label labelSeikyu;
    }
}