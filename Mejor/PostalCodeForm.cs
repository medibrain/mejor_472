﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Mejor
{
    public partial class PostalCodeForm : Form
    {
        public const int FORM_WIDTH = 532;
        public const int FORM_HEIGHT = 590;

        public PostalCode SelectedPostalCode;

        private Action<PostalCode> selectedListener;
        private BindingSource bsPC = new BindingSource();


        /// <summary>
        /// selectedHistory:選択履歴欄に表示する項目を表示します
        /// </summary>
        /// <param name="selectedListener"></param>
        /// <param name="selectedHistory"></param>
        /// <param name="showLocationX"></param>
        /// <param name="showLocationY"></param>
        /// <param name="prevSikutyoson"></param>
        /// <param name="prevTodofuken"></param>
        public PostalCodeForm(Action<PostalCode> selectedListener = null, List<PostalCode> selectedHistory = null, int showLocationX = -1, int showLocationY = -1, string prevTodofuken = "", string prevSikutyoson = "")
        {
            InitializeComponent();

            this.selectedListener = selectedListener;

            if (showLocationX == -1 || showLocationY == -1)
            {
                this.StartPosition = FormStartPosition.CenterParent;
            }
            else
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(showLocationX, showLocationY);
            }

            if(!string.IsNullOrEmpty(prevTodofuken)) textBoxTodofuken.Text = prevTodofuken;
            if(!string.IsNullOrEmpty(prevSikutyoson)) textBoxSikutyoson.Text = prevSikutyoson;

            if (selectedHistory != null)
            {
                var row = 1;
                foreach (var item in selectedHistory)
                {
                    var zip = item.Postal_Code.ToString("0000000");
                    var address = $"{item.Todofuken}{item.Sichokuson}{item.Choiki}";
                    var index = dataGridViewHistory.Rows.Add();
                    var data = dataGridViewHistory.Rows[index];
                    data.Cells[0].Value = row;
                    data.Cells[1].Value = zip;
                    data.Cells[2].Value = address;
                    row++;
                }
                if(selectedHistory.Count > 0)
                {
                    dataGridViewHistory.Rows[dataGridViewHistory.Rows.Count - 1].Cells[0].Selected = true;
                }                
            }

            bsPC.DataSource = new List<PostalCode>();
            dataGridView1.DataSource = bsPC;
            foreach (DataGridViewColumn col in dataGridView1.Columns) col.Visible = false;
            dataGridView1.Columns[nameof(PostalCode.Postal_Code)].HeaderText = "〒番号";
            dataGridView1.Columns[nameof(PostalCode.Postal_Code)].Visible = true;
            dataGridView1.Columns[nameof(PostalCode.Postal_Code)].Width = 54;
            dataGridView1.Columns[nameof(PostalCode.Todofuken)].HeaderText = "都道府県";
            dataGridView1.Columns[nameof(PostalCode.Todofuken)].Visible = true;
            dataGridView1.Columns[nameof(PostalCode.Todofuken)].Width = 70;
            dataGridView1.Columns[nameof(PostalCode.Sichokuson)].HeaderText = "市区町村";
            dataGridView1.Columns[nameof(PostalCode.Sichokuson)].Visible = true;
            dataGridView1.Columns[nameof(PostalCode.Sichokuson)].Width = 85;
            dataGridView1.Columns[nameof(PostalCode.Choiki)].HeaderText = "町域";
            dataGridView1.Columns[nameof(PostalCode.Choiki)].Visible = true;
            dataGridView1.Columns[nameof(PostalCode.Choiki)].Width = 277;

            bsPC.CurrentChanged += bsPC_CurrentChanged;
        }

        private void PostalCodeForm_Shown(object sender, EventArgs e)
        {
            textBoxSikutyoson.Focus();
        }

        //EnterキーでTAB挙動
        private void PostalCodeForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        //F5でキャンセル、PgUpでOK　の挙動
        private void PostalCodeForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                buttonCancel_Click(null, null);
                e.Handled = true;
                return;
            }
            e.Handled = false;
        }

        //数値以外を排他
        private void textBoxZipcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        //都道府県：侵入時全選択
        private void textBoxTodofuken_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        //市区町村：侵入時全選択
        private void textBoxSikutyoson_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        //町域：侵入時全選択
        private void textBoxChoiki_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        //DataGridView内でのTabキーは確定　の挙動
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (e.Shift)
                {
                    //検索へ
                    buttonSearch.Focus();
                    e.Handled = true;
                    return;
                }
                else
                {
                    //確定へ
                    buttonOK.Focus();
                    e.Handled = true;
                    return;
                }
            }
        }

        //郵便番号コピー
        private void textBoxShowZipcode_DoubleClick(object sender, EventArgs e)
        {
            textBoxShowZipcode.SelectAll();
            textBoxShowZipcode.Copy();

            selected((PostalCode)bsPC.Current);
        }

        //住所コピー
        private void textBoxShowAddress_DoubleClick(object sender, EventArgs e)
        {
            textBoxShowAddress.SelectAll();
            textBoxShowAddress.Copy();

            selected((PostalCode)bsPC.Current);
        }

        //選択履歴コピー
        private void dataGridViewHistory_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            var val = dataGridViewHistory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            Clipboard.SetData(DataFormats.Text, val);
        }

        //行選択時、下部テキストボックスに表示
        private void bsPC_CurrentChanged(object sender, EventArgs e)
        {
            var pc = (PostalCode)bsPC.Current;
            if(pc != null)
            {
                textBoxShowZipcode.Text = pc.Postal_Code.ToString("0000000");
                textBoxShowAddress.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
            }
            else
            {
                textBoxShowZipcode.Text = "-------";
                textBoxShowAddress.Text = "-------";
            }
        }

        //閉じるボタン：フォームを閉じる
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            SelectedPostalCode = null;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        //確定ボタン：リスナー通知
        private void buttonOK_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count == 1)
            {
                selected((PostalCode)bsPC.Current);
            }
        }

        //検索ボタン
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            var list = search();
            if (list == null) return;

            showList(list);
            if (list.Count() > 0) dataGridView1.Focus();
        }

        private void selected(PostalCode pc)
        {
            if (dataGridViewHistory.Rows.Count > 0)
            {
                var lastIndex = dataGridViewHistory.Rows.Count - 1;
                if (lastIndex == -1) return;
                var lastRow = dataGridViewHistory.Rows[lastIndex];
                if ((string)lastRow.Cells[1].Value != textBoxShowZipcode.Text
                    && (string)lastRow.Cells[2].Value != textBoxShowAddress.Text)
                {
                    var index = dataGridViewHistory.Rows.Add();
                    var row = dataGridViewHistory.Rows[index];
                    row.Cells[0].Value = index + 1;
                    row.Cells[1].Value = textBoxShowZipcode.Text;
                    row.Cells[2].Value = textBoxShowAddress.Text;
                    row.Cells[0].Selected = true;
                }
            }
            else
            {
                var index = dataGridViewHistory.Rows.Add();
                var row = dataGridViewHistory.Rows[index];
                row.Cells[0].Value = index + 1;
                row.Cells[1].Value = textBoxShowZipcode.Text;
                row.Cells[2].Value = textBoxShowAddress.Text;
                row.Cells[0].Selected = true;
            }

            SelectedPostalCode = pc;
            selectedListener?.Invoke(SelectedPostalCode);
        }

        /// <summary>
        /// 既定の開始位置へフォーカスを当てます
        /// </summary>
        public void MoveDefaultFocus()
        {
            textBoxSikutyoson.Focus();
        }

        private List<PostalCode> search()
        {
            bool exisZipcode = false;
            var zipcode = textBoxZipcode.Text;
            int zipcodeInt = -1;
            if (zipcode != "")
            {                
                if (string.IsNullOrWhiteSpace(zipcode) || !int.TryParse(zipcode, out zipcodeInt))
                {
                    MessageBox.Show("郵便番号は数字のみでなければなりません", "エラー");
                    return null;
                }
                exisZipcode = true;
            }

            bool exisTodofuken = false;
            var todofuken = textBoxTodofuken.Text;
            if (!string.IsNullOrWhiteSpace(todofuken)) exisTodofuken = true;

            bool exisSikutyoson = false;
            var sikutyoson = textBoxSikutyoson.Text;
            if (!string.IsNullOrWhiteSpace(sikutyoson)) exisSikutyoson = true;

            bool exisChoiki = false;
            var choiki = textBoxChoiki.Text;
            if (!string.IsNullOrWhiteSpace(choiki)) exisChoiki = true;

            return PostalCode.SearchPostalCode(
                exisZipcode ? zipcodeInt : -1,
                exisTodofuken ? todofuken : null,
                exisSikutyoson ? sikutyoson : null,
                exisChoiki ? choiki : null);
        }

        private void showList(List<PostalCode> list)
        {
            bsPC.DataSource = list;
            bsPC.ResetBindings(false);
        }
    }
}
