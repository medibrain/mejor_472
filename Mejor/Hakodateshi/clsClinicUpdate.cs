﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Hakodateshi
{
    class clsClinicUpdate
    {
       
        /// <summary>
        /// 施術所情報取得
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool UpdateClinic(int cym,WaitForm wf)
        {
            try
            {

                wf.LogPrint("施術所情報取得/更新中");

                DB.Transaction tran = DB.Main.CreateTransaction();
                StringBuilder sbsql = new StringBuilder();


                sbsql.AppendLine($" update application set ");
                sbsql.AppendLine($" sid= b.sid,");
                sbsql.AppendLine($" sname=b.sname ");
                sbsql.AppendLine($" from  ");

                //aux.batchaidのappとauxを取得して、app.aidとaux.aidが合致する申請書にバッチの情報を更新
                sbsql.AppendLine($" (select x.aid xaid,a.aid,a.sid,a.sname from application a ");
                sbsql.AppendLine($" inner join application_aux x on  ");
                sbsql.AppendLine($" a.aid=x.batchaid order by x.aid) b");

                sbsql.AppendLine($" where application.aid=b.xaid");

                DB.Command cmd = new DB.Command(sbsql.ToString(), tran);
                if (!cmd.TryExecuteNonQuery()) return false;


                tran.Commit();
                wf.LogPrint("正常終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
                return false;
            }
            finally
            {
                
            }
        }

        
    }
}
