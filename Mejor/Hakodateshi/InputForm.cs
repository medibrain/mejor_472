﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Hakodateshi
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 400);
        Point posFusho = new Point(100, 800);
        Point posCost = new Point(800, 2000);
        Point posDays = new Point(600, 800);
        Point posNewCont = new Point(800, 1200);
        Point posOryo = new Point(400, 1200);
        Point posNumbering = new Point(800, 2600);

        //20210708103441 furukawa st ////////////////////////
        //施術所コード、施術所名部追加
        
        Point posClinic = new Point(800, 0);
        //20210708103441 furukawa ed ////////////////////////


        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, numberingControls,
            clinicControls;//20210708103511 furukawa 施術所コード、施術所名追加
                           

        public InputForm(ScanGroup sGroup, bool firstTime, int aid)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hihoNumControls = new Control[] {  verifyBoxHnum,verifyBoxFamily,panelHnum , verifyBoxClinicNum, };           
            personControls = new Control[] { verifyBoxPsex, verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD, };
            costControls = new Control[] {  verifyBoxCharge, verifyBoxClinicName, };        
            fushoControls = new Control[] {  panelF1FirstDate, panelF, verifyBoxCountedDays, verifyBoxTotal, };

            //20210708103418 furukawa st ////////////////////////
            //施術所コード、施術所名追加
            
            clinicControls = new Control[] { panelClinic ,verifyBoxClinicName,verifyBoxClinicNum,};
            //20210708103418 furukawa ed ////////////////////////



            #region 動的イベント追加（item_enter)
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    if (item is Panel) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);
            #endregion

            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            #region 申請書一覧
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

    
        void item_Enter(object sender, EventArgs e)
        {

            Point p;

            if (ymControls.Contains(sender)) p = posYM;
            else if (hihoNumControls.Contains(sender)) p = posHihoNum;
            else if (costControls.Contains(sender)) p = posCost;
            else if (fushoControls.Contains(sender)) p = posFusho;
            else if (clinicControls.Contains(sender)) p = posClinic;//20210708103345 furukawa 施術所コード、施術所名追加
                                                                    
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }


        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;
            bool payError = false;
            

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

         
            //被保険者番号チェック 1文字以上かつ数字に直せること          
            int numN = verifyBoxHnum.GetIntValue();
            setStatus(verifyBoxHnum, numN < 1);
                    
            //本家区分
            int intverifyBoxFamily = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !new[] { 2, 6 }.Contains(intverifyBoxFamily));

            //性別のチェック
            int sex = verifyBoxPsex.GetIntValue();
            setStatus(verifyBoxPsex, sex != 1 && sex != 2);

            //生年月日のチェック
            var birth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

            if (birth>DateTime.Now)
            {
                verifyBoxBirthE.BackColor = Color.GreenYellow;
                verifyBoxBirthY.BackColor = Color.GreenYellow;
                verifyBoxBirthM.BackColor = Color.GreenYellow;
                verifyBoxBirthD.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本日より未来です。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }


            //負傷1負傷名
            string strverifyBoxF1Name = verifyBoxF1Name.Text.Trim();

            //負傷2負傷名
            string strverifyBoxF2Name = verifyBoxF2Name.Text.Trim();

            //負傷3負傷名
            string strverifyBoxF3Name = verifyBoxF3Name.Text.Trim();

            //負傷4負傷名
            string strverifyBoxF4Name = verifyBoxF4Name.Text.Trim();

            //負傷5負傷名
            string strverifyBoxF5Name = verifyBoxF5Name.Text.Trim();

            //医療機関番号
            string strverifyBoxClinicNum = verifyBoxClinicNum.Text.Trim();
            setStatus(verifyBoxClinicNum, strverifyBoxClinicNum.Length > 12);


            //施術所名
            string strverifyBoxClinicName = verifyBoxClinicName.Text.Trim();


            //初検日は平成をまたぐ可能性があるので年号必須
            //初検日 dateCheck内で令和チェック済
         
            //初検日入力チェック          
            DateTime dtifirstdate1 = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);
            if (dtifirstdate1 > DateTime.Now)
            {
                verifyBoxF1FirstE.BackColor = Color.GreenYellow;
                verifyBoxF1FirstY.BackColor = Color.GreenYellow;
                verifyBoxF1FirstM.BackColor = Color.GreenYellow;
                verifyBoxF1FirstD.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本日より未来です。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }


            //実日数
            int days = verifyBoxCountedDays.GetIntValue();
            setStatus(verifyBoxCountedDays, days < 1 || 31 < days);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);


            //20210618132557 furukawa st ////////////////////////
            //削除　2021年6月18日福田さんより要望
            
            //請求金額
            //int charge = verifyBoxCharge.GetIntValue();
            //setStatus(verifyBoxCharge, charge < 100 || total < charge);
            //if (total <= charge) payError = true;

            //20210618132557 furukawa ed ////////////////////////


            //負傷名チェック
            fusho1Check(verifyBoxF1Name);
            fushoCheck(verifyBoxF2Name);
            fushoCheck(verifyBoxF3Name);
            fushoCheck(verifyBoxF4Name);
            fushoCheck(verifyBoxF5Name);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

       
            //値の反映
            app.MediYear = year;        //診療和暦年
            app.MediMonth = month;      //診療和暦月

            //被保険者証記号
            //被保険者証番号
            app.HihoNum = verifyBoxHnum.Text.Trim();

            //受療者性別
            app.Sex = sex;

            //受療者生年月日
            app.Birthday = birth;

            //家族区分
            app.Family = intverifyBoxFamily;
            
            //初検日
            app.FushoFirstDate1 = dtifirstdate1;

            //新規継続
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            
            app.CountedDays = days;//実日数（施術日数）
            app.Total = total;      //合計額

            //20210618132625 furukawa st ////////////////////////
            //請求金額削除により計算不可能となる　2021年6月18日福田さんより要望
            
            //app.Charge = charge;    //請求額
            //app.Partial = total - charge;//一時負担金（不要だが計算で入れとく）
            //20210618132625 furukawa ed ////////////////////////



            app.FushoName1 = verifyBoxF1Name.Text.Trim();       //負傷1　負傷名
            app.FushoName2 = verifyBoxF2Name.Text.Trim();       //負傷2　負傷名
            app.FushoName3 = verifyBoxF3Name.Text.Trim();       //負傷3　負傷名
            app.FushoName4 = verifyBoxF4Name.Text.Trim();       //負傷4　負傷名
            app.FushoName5 = verifyBoxF5Name.Text.Trim();       //負傷5　負傷名


            //医療機関番号
            app.ClinicNum = strverifyBoxClinicNum;

            //施術所名
            app.ClinicName = strverifyBoxClinicName;


            app.AppType = scan.AppType;

            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            //20210618152813 furukawa st ////////////////////////
            //医療機関のヘッダシート対応
            
            else if (verifyBoxY.Text == "..")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                app.AppType = APP_TYPE.バッチ;

                //20210708103655 furukawa st ////////////////////////
                //施術所ヘッダ用更新処理
                
                app.ClinicNum = verifyBoxClinicNum.Text.Trim();
                app.ClinicName = verifyBoxClinicName.Text.Trim();
                //20210708103655 furukawa ed ////////////////////////

            }
            //20210618152813 furukawa ed ////////////////////////
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                //OCRデータがあれば、部位のみ挿入
                var ocr = app.OcrData.Split(',');
                verifyBoxF1Name.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2Name.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3Name.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4Name.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5Name.Text = Fusho.GetFusho5(ocr);
            }
            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {


            Control[] ignoreControls = new Control[] { labelH, labelY,
                verifyBoxY, labelInputerName, };

            panelHnum.Visible = false;
            panelF.Visible = false;
            panelF1FirstDate.Visible = false;
            verifyBoxTotal.Visible = false;
            labelTotal.Visible = false;
            verifyBoxCharge.Visible = false;
            labelCharge.Visible = false;

            //20210708110140 furukawa st ////////////////////////
            //パネルに変更
            panelClinic.Visible = false;

            //verifyBoxClinicName.Visible = false;
            //labelClinicName.Visible = false;
            //verifyBoxClinicNum.Visible = false;
            //labelClinicNum.Visible = false;


            //20210708110140 furukawa ed ////////////////////////


            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                    //20210708095823 furukawa st ////////////////////////
                    //施術所コード、施術所名は施術所ヘッダだけ
                    panelClinic.Visible = true;
                    panelHnum.Visible = false;
                    //verifyBoxClinicName.Visible = true;
                    //labelClinicName.Visible = true;
                    //verifyBoxClinicNum.Visible = true;
                    //labelClinicNum.Visible = true;
                    //20210708095823 furukawa ed ////////////////////////

                    break;

                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                case clsInputKind.施術同意書:// "901"://施術同意書

                    
                    break;

                default:
                    panelHnum.Visible = true;
                    panelF.Visible = true;
                    panelF1FirstDate.Visible = true;
                    verifyBoxTotal.Visible = true;
                    labelTotal.Visible = true;

                    //20210708095716 furukawa st ////////////////////////
                    //施術所コード、施術所名は施術所ヘッダだけ

                    panelClinic.Visible = false;

                    //verifyBoxClinicName.Visible = false;
                    //labelClinicName.Visible = false;
                    //verifyBoxClinicNum.Visible = false;
                    //labelClinicNum.Visible = false;

                    //20210708095716 furukawa ed ////////////////////////

                    break;
            }




            //var cs = new Control[] { verifyBoxY, labelH, labelY, labelInputerName };

            //var visible = new Action<bool>(b =>
            //{
            //    foreach (Control item in panelRight.Controls)
            //    {
            //        if (!(item is IVerifiable || item is Label)) continue;
            //        if (cs.Contains(item)) continue;
            //        item.Visible = b;
            //    }
            //});

            //if ((verifyBoxY.Text == "--" || verifyBoxY.Text == "++") && verifyBoxY.Text.Length == 2)
            //{
            //    //続紙その他
            //    visible(false);
            //}
            //else
            //{
            //    //申請書の場合
            //    visible(true);
            //}
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;

            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (clinicControls.Contains(t)) posClinic = pos;//20210709143132 furukawa 施術所ヘッダ用追加
                                                                 

            //20210618132103 furukawa st ////////////////////////
            //コントロールが含まれないので削除しないとNullException
            //else if (dayControls.Contains(t)) posDays = pos;
            //else if (newContControls.Contains(t)) posNewCont = pos;
            //else if (oryoControls.Contains(t)) posOryo = pos;
            //else if (numberingControls.Contains(t)) posNumbering = pos;
            //20210618132103 furukawa ed ////////////////////////
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            //20210618152737 furukawa st ////////////////////////
            //医療機関のヘッダシート対応
            
            else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {
                setValue(verifyBoxY, ".." ,firstTime, nv);

                //20210708111556 furukawa st ////////////////////////
                //施術所コード、施術所名追加
                
                setValue(verifyBoxClinicNum, app.ClinicNum, firstTime, nv);
                setValue(verifyBoxClinicName, app.ClinicName, firstTime, nv);
                //20210708111556 furukawa ed ////////////////////////
            }
            //20210618152737 furukawa ed ////////////////////////
            else
            {

                //診療和暦年
                setValue(verifyBoxY, app.MediYear, firstTime, nv);

                //診療和暦月
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);

                //被保険者証 番号
                setValue(verifyBoxHnum, app.HihoNum.ToString(), firstTime, nv);//被保険者証番号
                
                //受療者性別
                setValue(verifyBoxPsex, app.Sex, firstTime, nv);
             
                //家族区分
                setValue(verifyBoxFamily, app.Family, firstTime, nv);

                ////本人家族
                //setValue(verifyBoxHonke, app.Family, firstTime, nv);


                //受療者生年月日
                setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                //初検日1
                setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                //負傷1負傷名
                setValue(verifyBoxF1Name, app.FushoName1.ToString(), firstTime, nv);

                //負傷2負傷名
                setValue(verifyBoxF2Name, app.FushoName2.ToString(), firstTime, nv);

                //負傷3負傷名
                setValue(verifyBoxF3Name, app.FushoName3.ToString(), firstTime, nv);

                //負傷4負傷名
                setValue(verifyBoxF4Name, app.FushoName4.ToString(), firstTime, nv);

                //負傷5負傷名
                setValue(verifyBoxF5Name, app.FushoName5.ToString(), firstTime, nv);

                //新規継続                
                if (app.NewContType == NEW_CONT.新規) setValue(verifyBoxNewCont, "1", firstTime, nv);
                if (app.NewContType == NEW_CONT.継続) setValue(verifyBoxNewCont, "2", firstTime, nv);
                if (app.NewContType == NEW_CONT.Null) setValue(verifyBoxNewCont, "", firstTime, nv);

                //医療機関番号
                setValue(verifyBoxClinicNum, app.ClinicNum.ToString(), firstTime, nv);

                //施術所名
                setValue(verifyBoxClinicName, app.ClinicName.ToString(), firstTime, nv);

                //合計額
                setValue(verifyBoxTotal, app.Total, firstTime, nv);

                //20210618132455 furukawa st ////////////////////////
                //削除　2021年6月18日福田さんより要望
                
                //請求額
                //setValue(verifyBoxCharge, app.Charge, firstTime, nv);
                //20210618132455 furukawa ed ////////////////////////


                //実日数
                setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

            
            }
            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3Name.TabStop = verifyBoxF2Name.Text != string.Empty;
            verifyBoxF4Name.TabStop = verifyBoxF3Name.Text != string.Empty;
            verifyBoxF5Name.TabStop = verifyBoxF4Name.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
