﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.MediMatch
{
    class Sick
    {
        const string dbName = "jyusei";
        static Dictionary<int, Sick> master = new Dictionary<int, Sick>();

        static Sick()
        {
            Load();
        }

        public int ID { get; set; }
        public string Name { get; set; }

        /// <summary>
        ///  データベースからマスター情報を読み込みます
        /// </summary>
        public static void Load()
        {
            var sql = "SELECT id, name FROM sick;";
            var cdb = new DB(dbName);
            var l = cdb.Query<Sick>(sql);

            master.Clear();
            foreach (var item in l)
            {
                if (!master.ContainsKey(item.ID))
                    master.Add(item.ID, item);
                else
                    master[item.ID] = item;
            }
        }

        public static bool Import(string fileName)
        {
            var l = CommonTool.CsvImportShiftJis(fileName);

            //CSVデータを取得、年度判断のため、
            try
            {
                foreach (var item in l)
                {
                    if (item.Length < 22) continue;

                    int id;
                    int.TryParse(item[2], out id);

                    //未コード化傷病名
                    if (id == 999) continue;
                    
                    var s = new Sick();
                    s.ID = id;
                    s.Name = item[5];

                    if (!master.ContainsKey(id)) master[id] = s;
                    else master.Add(id, s);
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            
            return save();
        }

        private static bool save()
        {
            var cdb = new DB(dbName);
            var l = master.Values.ToList();

            using (var tran = cdb.CreateTransaction())
            {
                var sql = "DELETE FROM sick;";
                if (!cdb.Excute(sql, null, tran))
                {
                    tran.Rollback();
                    return false;
                }

                sql = "INSERT INTO sick(id, name) VALUES(@id, @name);";
                if (!cdb.Excute(sql, l, tran))
                {
                    tran.Rollback();
                    return false;
                }

                tran.Commit();
                return true;
            }
        }

        private static int getTargetYear(int year, int month)
        {
            if (month < 4) year =- 1;
            return year / 2 * 2;
        }

        private void GetName(int id, int year, int month)
        {
            var y = getTargetYear(year, month);
        }





    }
}
