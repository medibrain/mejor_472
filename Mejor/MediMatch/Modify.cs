﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.MediMatch
{
    public class Modify
    {
        const string dbName = "jyusei";
        static Dictionary<int, Modify> master = new Dictionary<int, Modify>();

        static Modify()
        {
            Load();
        }

        public int ID { get; set; }              // 修飾語ID
        public string Word { get; set; }         // 修飾語

        /// <summary>
        ///  データベースからマスター情報を読み込みます
        /// </summary>
        public static void Load()
        {
            var sql = "SELECT id, word FROM modify;";
            var cdb = new DB(dbName);
            var l = cdb.Query<Modify>(sql);

            master.Clear();
            foreach (var item in l) master.Add(item.ID, item);
        }

        public static bool Import(string fileName)
        {
            var l = CommonTool.CsvImportShiftJis(fileName);

            try
            {
                foreach (var item in l)
                {
                    if (item.Length < 10) continue;

                    int id;
                    int.TryParse(item[2], out id);
                    if (master.ContainsKey(id))
                    {
                        master[id].Word = item[6];
                    }
                    else
                    {
                        var m = new Modify();
                        m.ID = id;
                        m.Word = item[6];
                        master.Add(m.ID, m);
                    }
                }
            }
            catch
            {
                return false;
            }
            return save();
        }

        /// <summary>
        /// データベースへ記録します
        /// </summary>
        /// <returns></returns>
        private static bool save()
        {
            var cdb = new DB(dbName);
            using (var tran = cdb.CreateTransaction())
            {
                var sql = "DELETE FROM modify;";
                if (!cdb.Excute(sql, null, tran))
                {
                    tran.Rollback();
                    return false;
                }

                sql = "INSERT INTO modify (id, word) VALUES(@id, @word);";
                var l = master.Values.ToList();
                if (!cdb.Excute(sql, l, tran))
                {
                    tran.Rollback();
                    return false;
                }

                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 修飾語コードを指定し、修飾語を返します
        /// </summary>
        /// <param name="id">修飾語コード</param>
        /// <returns>修飾語</returns>
        public string GetModifyWord(int id)
        {
            Modify s;
            if (master.TryGetValue(id, out s)) return s.Word;
            return "";
        }

        /// <summary>
        /// 修飾語を付け加えて返します
        /// </summary>
        /// <param name="word">修飾前のワード</param>
        /// <param name="modifyCode">修飾語コード (テキスト状態でつなげたまま)</param>
        public string ModifyWordsAdd(string word, string modifyCode)
        {
            int stringlengs = modifyCode.Length;
            int wordcount = stringlengs / 4;
            int[] spiritCode = new int[wordcount];
            string preStr = "";
            string pstStr = "";
            for (int i = 0; i < stringlengs; i = i + 4)
            {
                spiritCode[i / 4] = int.Parse(modifyCode.Substring(0 + i, 4));
            }

            //修飾語付加
            for (int i = 0; i < spiritCode.Length; i++)
            {
                if (spiritCode[i] < 8000)
                    preStr += GetModifyWord(spiritCode[i]);
                else
                    pstStr += GetModifyWord(spiritCode[i]);
            }
            return preStr + word + pstStr;
        }
    }
}
