﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    //20210210110917 furukawa 
    //入力された請求金額と計算結果で差異がないかチェックする
    
    class clsChargeCheck
    {
        public int aaid { get; set; }
        public int atotal { get; set; }
        public int achrage { get; set; }
        public int aratio { get; set; }
        public int calc_charge { get; set; }

        List<App> lstApp = new List<App>();
        List<clsChargeCheck> lstChk = new List<clsChargeCheck>();
        public int cym;
        
        /// <summary>
        /// 請求金額チェック
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public void chkMain(int _cym)
        {
            cym = _cym;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            if (!loadApp(wf))
            {
                wf.Dispose();
                return;
            }

            outcsv(wf);

            wf.Dispose();
        }

        private bool loadApp(WaitForm wf)
        {

            try
            {
                lstApp = App.GetApps(cym, Insurer.CurrrentInsurer.EnumInsID);
                wf.SetMax(lstApp.Count);
                wf.LogPrint("計算中");

                foreach (App a in lstApp)
                {
                    if (CommonTool.WaitFormCancelProcess(wf)) return false;

                    clsChargeCheck chk = new clsChargeCheck();
                    chk.aaid = a.Aid;
                    chk.atotal = a.Total;
                    chk.achrage = a.Charge;
                    chk.aratio = a.Ratio;

                    //共通関数での計算結果
                    chk.calc_charge = CommonTool.CalcCharge(chk.atotal, chk.aratio);

                    lstChk.Add(chk);
                    wf.InvokeValue++;
                }
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// リスト保存
        /// </summary>
        /// <param name="wf"></param>
        private void outcsv(WaitForm wf)
        {
            System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
            string strpath = string.Empty;
            dlg.Filter = "csv|*.csv";
            dlg.FileName=$"請求金額チェック_{Insurer.CurrrentInsurer.InsurerName}_メホール処理年月{cym}_出力日{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.csv";
            dlg.DefaultExt = "csv";
            dlg.AddExtension = true;            
            dlg.ShowDialog();
            strpath = dlg.FileName;

            wf.SetMax(lstChk.Count);
            wf.InvokeValue = 0;

            System.IO.StreamWriter sw = new System.IO.StreamWriter(strpath, false, System.Text.Encoding.GetEncoding("utf-8"));
            try
            {
                wf.LogPrint("出力中");

                sw.WriteLine($"aid,合計金額,給付割合,請求金額（テーブル）,請求金額（合計decimal×割合decimal/10　AND　小数点以下切り捨て）,結果");
                int cntNG = 0;
                foreach (clsChargeCheck cls in lstChk)
                {
                    if (CommonTool.WaitFormCancelProcess(wf)) return;

                    string strRes = $"{cls.aaid.ToString()},{cls.atotal.ToString()},{cls.aratio.ToString()}," +
                        $"{cls.achrage.ToString()},{cls.calc_charge.ToString()},";
                    if (cls.achrage.ToString() != cls.calc_charge.ToString()) { strRes += "NG"; cntNG++; }
                    else strRes += "OK";


                    sw.WriteLine(strRes);
                    wf.InvokeValue++;
                }

                sw.WriteLine($",,,,NG件数=,{cntNG}");

                System.Windows.Forms.MessageBox.Show("出力完了");
              
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                
            }
            finally
            {
                sw.Close();
            }
        }

    }
}
