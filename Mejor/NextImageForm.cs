﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class NextImageForm : Form
    {
        BindingSource bs = new BindingSource();
        bool toNext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentApp"></param>
        /// <param name="Next"></param>
        public NextImageForm(App currentApp, bool toNext = true)
        {
            InitializeComponent();
            this.toNext = toNext;

            bs.DataSource = toNext ?
                App.GetAppsWithLimit($"WHERE a.aid>{currentApp.Aid}", 20, true) :
                App.GetAppsWithLimit($"WHERE a.aid<{currentApp.Aid}", 5, false);

            buttonBack.Enabled = false;
            bs.CurrentChanged += Bs_CurrentChanged;
            if (!toNext) bs.MoveLast();
            showImage();

            Shown += NextImageForm_Shown;
        }

        private void NextImageForm_Shown(object sender, EventArgs e)
        {
            if (bs.Count == 0)
            {
                MessageBox.Show("次のIDの画像はありません");
                this.Close();
            }
        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            showImage();
        }

        private void showImage()
        {
            buttonBack.Enabled = bs.Position != 0;
            buttonNext.Enabled = bs.Position != bs.List.Count - 1;

            try
            {
                var app = (App)bs.Current;
                if (app == null) return;

                var fn = app.GetImageFullPath();
                userControlImageMain.SetPictureFile(fn);
                userControlImageMain.SetPictureBoxFill();
                labelImageName.Text = fn;

                var page = toNext ? $"{bs.Position + 1}枚後" : $"{bs.Count - bs.Position}枚前";
                labelAid.Text = $"AID：{app.Aid}   ( {page} )";
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }
        
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bs.MovePrevious();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            bs.MoveNext();
        }
    }
}
