﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Mejor.Inspect
{
    public partial class FamilyImageForm : Form
    {
        private BindingSource bs = new BindingSource();
        private Form parent;
        bool nowSetting = false;

        public int RelationAID { get; set; }

        public FamilyImageForm(App currentApp, Form parent, bool relationBoxVisible)
        {
            InitializeComponent();
            this.parent = parent;
            RelationAID = currentApp.TaggedDatas.RelationAID;
            checkBoxRelation.Visible = relationBoxVisible;

            //関連申請書に誕生日を検索条件としてつけるか
            //現在広域かどうかで判断
            var birthIgnore = Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.後期高齢;
            
            var l = App.GetAppsFamily(currentApp.HihoNum, currentApp.CYM);
            if (l == null) return;
            // 同一処理年月も含める
            l.RemoveAll(x => currentApp.YM < x.YM);
            if (!birthIgnore) l.RemoveAll(x => x.Birthday != currentApp.Birthday);

            bs.DataSource = l;
            bs.ResetBindings(false);

            buttonBack.Enabled = false;
            Shown += FamilyImageForm_Shown;

        }

        private void FamilyImageForm_Shown(object sender, EventArgs e)
        {
            Location = new Point(Location.X, parent.Location.Y < 10 ? 10 : parent.Location.Y);
            Size = new Size(Size.Width, parent.Height - 20);
            if (bs.Count == 0)
            {
                MessageBox.Show("次のIDの画像はありません");
                this.Close();
            }

            foreach (Control c in userControlImageMain.Controls)
            {
                if(c is PictureBox pb)
                {
                    pb.Paint += Pb_Paint;
                    break;
                }
            }

            if (RelationAID != 0)
            {
                var p = ((List<App>)bs.DataSource).FindIndex(a => a.Aid == RelationAID);
                bs.Position = p;
            }
            else
            {
                bs.MoveLast();
            }

            showImage();
            bs.CurrentChanged += Bs_CurrentChanged;
        }

        private void Pb_Paint(object sender, PaintEventArgs e)
        {
            var app = (App)bs.Current;
            if (app == null) return;
            if (RelationAID != app.Aid) return;

            var p = PointToScreen(ClientRectangle.Location);
            p.Offset(30, 30);
            p = ((PictureBox)sender).PointToClient(p);

            var g = e.Graphics;
            using (var f = new Font(Font.FontFamily, 50, FontStyle.Bold))
            using (var b = new SolidBrush(Color.FromArgb(140, 255, 0, 0)))
            {
                g.DrawString("参考申請書", f, b, p);
            }
        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            showImage();
        }

        private void showImage()
        {
            buttonBack.Enabled = bs.Position != 0;
            buttonNext.Enabled = bs.Position != bs.List.Count - 1;

            try
            {
                nowSetting = true;
                var app = (App)bs.Current;
                if (app == null) return;

                var fn = app.GetImageFullPath();
                userControlImageMain.SetPictureFile(fn);
                userControlImageMain.SetPictureBoxFill();
                labelIndex.Text = $"{bs.Position + 1} / {bs.Count}";
                Text = $"関連レセ　ScanID:{app.ScanID} Group:{app.GroupID} 処理年月:{app.CYM.ToString("0000/00")}";
                checkBoxRelation.Checked = RelationAID == app.Aid;

                //20201016151726 furukawa st ////////////////////////
                //ステータスを表示
                inspectControl1.SetApp(app, false);
                //20201016151726 furukawa ed ////////////////////////
                


            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
            finally
            {
                nowSetting = false;
            }

            var ff = this.Font.FontFamily;
            using (var g = CreateGraphics())
            using (var b = new SolidBrush(Color.FromArgb(100, 255, 0, 0)))
            {
                g.DrawString("参考レセ", new Font(ff, 20), b, new PointF(20, 20));
            }

            
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bs.MovePrevious();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            bs.MoveNext();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            var app = (App)bs.Current;

            var pn = Settings.ShokaiPrinterName;
            if (pn == string.Empty) pn = Settings.DefaultPrinterName;
            var ip = new ImagePrint(pn);
            ip.Print(app, true, false);
        }

        private void checkBoxRelation_CheckedChanged(object sender, EventArgs e)
        {
            if (nowSetting) return;
            if (checkBoxRelation.Checked)
            {
                var app = (App)bs.Current;
                if (app == null)
                {
                    checkBoxRelation.Checked = false;
                    return;
                }
                RelationAID = app.Aid;
            }
            else
            {
                RelationAID = 0;
            }

            userControlImageMain.Refresh();
        }
    }
}
