﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace Mejor.Inspect
{
    public partial class InspectListForm : Form
    {
        private BindingSource bs = new BindingSource();
        private int cym;

        public InspectListForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;

            var l = GroupInspectStatus.GetList(cym);
            bs.DataSource = l;
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(GroupInspectStatus.GroupID)].Width = 60;
            dataGridView1.Columns[nameof(GroupInspectStatus.GroupID)].HeaderText = "GroupID";
            dataGridView1.Columns[nameof(GroupInspectStatus.AppType)].Width = 45;
            dataGridView1.Columns[nameof(GroupInspectStatus.AppType)].HeaderText = "種類";
            dataGridView1.Columns[nameof(GroupInspectStatus.Note1)].Width = 70;
            dataGridView1.Columns[nameof(GroupInspectStatus.Note1)].HeaderText = "Note1";
            dataGridView1.Columns[nameof(GroupInspectStatus.Note2)].Width = 40;
            dataGridView1.Columns[nameof(GroupInspectStatus.Note2)].HeaderText = "Note2";
            dataGridView1.Columns[nameof(GroupInspectStatus.UserName)].Width = 80;
            dataGridView1.Columns[nameof(GroupInspectStatus.UserName)].HeaderText = "ユーザー";
            dataGridView1.Columns[nameof(GroupInspectStatus.ShokaiCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.ShokaiCount)].HeaderText = "照会";
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenStatus)].Width = 80;
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenStatus)].HeaderText = "点検状況";
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenCount)].HeaderText = "対象";
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenFinishCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.TenkenFinishCount)].HeaderText = "点検済";
            dataGridView1.Columns[nameof(GroupInspectStatus.SaishinCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.SaishinCount)].HeaderText = "再審査";
            dataGridView1.Columns[nameof(GroupInspectStatus.KagoCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.KagoCount)].HeaderText = "過誤";
            dataGridView1.Columns[nameof(GroupInspectStatus.HoryuCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.HoryuCount)].HeaderText = "保留";
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoStatus)].Width = 80;
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoStatus)].HeaderText = "往療状況";
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoCount)].HeaderText = "対象";
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoFinishCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoFinishCount)].HeaderText = "点検済";
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoGigiCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.OryoGigiCount)].HeaderText = "疑義";
            dataGridView1.Columns[nameof(GroupInspectStatus.HenreiCount)].Width = 50;
            dataGridView1.Columns[nameof(GroupInspectStatus.HenreiCount)].HeaderText = "返戻";

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年度{cym % 100}月処理分";
            //labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100)}年度{cym % 100}月処理分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.Text += " - " + Mejor.Insurer.CurrrentInsurer.InsurerName;

            showCount(l);
            Shown += (s, e) => dataGridView1.Focus();
        }

        private void showCount(List<GroupInspectStatus> l)
        {
            textBoxShokai.Text = l.Sum(g => g.ShokaiCount).ToString();
            textBoxTenken.Text = l.Sum(g => g.TenkenCount).ToString();
            textBoxKago.Text = l.Sum(g => g.KagoCount).ToString();
            textBoxSai.Text = l.Sum(g => g.SaishinCount).ToString();
            textBoxOryo.Text = l.Sum(g => g.OryoCount).ToString();
            textBoxOryoGigi.Text = l.Sum(g => g.OryoGigiCount).ToString();
            textBoxHenrei.Text = l.Sum(g => g.HenreiCount).ToString();
        }

        string sortColumnName;
        bool sortASC;
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var l = (List<GroupInspectStatus>)bs.DataSource;
            if (l == null) return;
            if (e.ColumnIndex < 0) return;
            
            var name = dataGridView1.Columns[e.ColumnIndex].Name;
            sortASC = (name == sortColumnName) ? !sortASC : true;
            sortColumnName = name;

            var ps = typeof(GroupInspectStatus).GetProperties();
            var p = Array.Find(ps, pp => pp.Name == name);
            if (p == null) return;

            l.Sort((x, y) =>
            {
                var xp = p.GetValue(x, null);
                var yp = p.GetValue(y, null);

                //nullを最小に
                if (xp == null && yp == null) return 0;
                if (xp == null) return sortASC ? -1 : 1;
                if (yp == null) return sortASC ? 1 : -1;

                int compareRes = 0;
                if (p.PropertyType == typeof(int?)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(int)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(DateTime)) compareRes = ((DateTime)xp).CompareTo(((DateTime)yp));
                else if (p.PropertyType == typeof(string)) compareRes = ((string)xp).CompareTo(((string)yp));
                else compareRes = (xp.ToString()).CompareTo(yp.ToString());

                return sortASC ? compareRes : -compareRes;
            });
            bs.ResetBindings(false);
        }

        private void buttonInspect_Click(object sender, EventArgs e)
        {
            var g = (GroupInspectStatus)bs.Current;
            if (g == null) return;

            //点検開始
            try
            {
                if (!setTenkenUser()) return;
                using (var f = new InspectForm(cym, g)) f.ShowDialog();
            }
            finally
            {
                releaseTenkenUser();
            }

            //ステータス/カウント再取得
            resetBindings();
            dataGridView1.Focus();
        }

        private void buttonOryoInspect_Click(object sender, EventArgs e)
        {
            var g = (GroupInspectStatus)bs.Current;
            if (g == null) return;

            //点検開始
            try
            {
                if (!setTenkenUser()) return;
                using (var f = new InspectOryoForm(cym, g)) f.ShowDialog();
            }
            finally
            {
                releaseTenkenUser();
            }

            //ステータス/カウント再取得
            resetBindings();
            dataGridView1.Focus();
        }

        private bool setTenkenUser()
        {
            var g = (GroupInspectStatus)bs.Current;
            var uid = ScanGroup.GetInquiryUID(g.GroupID);

            //もし他のユーザーが作業中なら警告
            if (uid != 0)
            {
                var r = MessageBox.Show("このグループは現在他のユーザーが作業中です。\r\n" +
                    "このグループを編集しますか？", "ユーザーの重複の警告",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (r == DialogResult.Cancel)
                {
                    resetBindings();
                    return false;
                }
            }

            return ScanGroup.SetInquiryUID(g.GroupID, User.CurrentUser.UserID);
        }

        private void releaseTenkenUser()
        {
            var g = (GroupInspectStatus)bs.Current;
            var uid = ScanGroup.GetInquiryUID(g.GroupID);
            ScanGroup.SetInquiryUID(g.GroupID, 0);
        }

        private void resetBindings()
        {
            var l = GroupInspectStatus.GetList(cym);
            showCount(l);

            if (checkBoxTenken.Checked) l = l.FindAll(g => g.TenkenStatus != "点検なし");
            if (checkBoxOryo.Checked) l = l.FindAll(g => g.OryoStatus != "点検なし");

            bs.DataSource = l;
            bs.ResetBindings(false);
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            resetBindings();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        /// <summary>
        /// //20210222100221 furukawa あはき専用点検ボタン        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInspectAHK_Click(object sender, EventArgs e)
        {
            var g = (GroupInspectStatus)bs.Current;
            if (g == null) return;
            if(g.AppType!=APP_TYPE.あんま && g.AppType!=APP_TYPE.鍼灸)
            {
                MessageBox.Show("あはき以外使用できません");
                return;
            }

            //点検開始
            try
            {
                if (!setTenkenUser()) return;
                using (var f = new InspectFormAHK(cym, g)) f.ShowDialog();
            }
            finally
            {
                releaseTenkenUser();
            }

            //ステータス/カウント再取得
            resetBindings();
            dataGridView1.Focus();
        }
    }
}
