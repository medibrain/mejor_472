﻿namespace Mejor.Inspect
{
    partial class InspectOryoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelSubImageName = new System.Windows.Forms.Label();
            this.buttonImageFillSub = new System.Windows.Forms.Button();
            this.panelSubCursor = new System.Windows.Forms.Panel();
            this.buttonSubPrev = new System.Windows.Forms.Button();
            this.buttonSubNext = new System.Windows.Forms.Button();
            this.labelSubCnt = new System.Windows.Forms.Label();
            this.panelSub = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelCenter = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxThisApp = new System.Windows.Forms.GroupBox();
            this.labelTenkenResult = new System.Windows.Forms.Label();
            this.radioButtonOryoGigiYes = new System.Windows.Forms.RadioButton();
            this.radioButtonOryoGigiNo = new System.Windows.Forms.RadioButton();
            this.buttonPrint2 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.プリンター選択ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonSet = new System.Windows.Forms.Button();
            this.panelDown = new System.Windows.Forms.Panel();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.userControlImageMain = new UserControlImage();
            this.userControlOryo1 = new Mejor.UserControlOryo();
            this.userControlImageSub = new UserControlImage();
            this.panelSubCursor.SuspendLayout();
            this.panelSub.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxThisApp.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panelDown.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSubImageName
            // 
            this.labelSubImageName.AutoSize = true;
            this.labelSubImageName.Location = new System.Drawing.Point(87, 12);
            this.labelSubImageName.Name = "labelSubImageName";
            this.labelSubImageName.Size = new System.Drawing.Size(77, 13);
            this.labelSubImageName.TabIndex = 67;
            this.labelSubImageName.Text = "画像ファイル名";
            // 
            // buttonImageFillSub
            // 
            this.buttonImageFillSub.Location = new System.Drawing.Point(7, 4);
            this.buttonImageFillSub.Name = "buttonImageFillSub";
            this.buttonImageFillSub.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFillSub.TabIndex = 69;
            this.buttonImageFillSub.TabStop = false;
            this.buttonImageFillSub.Text = "全体表示";
            this.buttonImageFillSub.UseVisualStyleBackColor = true;
            this.buttonImageFillSub.Click += new System.EventHandler(this.buttonImageFillSub_Click);
            // 
            // panelSubCursor
            // 
            this.panelSubCursor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSubCursor.Controls.Add(this.buttonSubPrev);
            this.panelSubCursor.Controls.Add(this.buttonSubNext);
            this.panelSubCursor.Location = new System.Drawing.Point(444, 3);
            this.panelSubCursor.Name = "panelSubCursor";
            this.panelSubCursor.Size = new System.Drawing.Size(50, 33);
            this.panelSubCursor.TabIndex = 73;
            // 
            // buttonSubPrev
            // 
            this.buttonSubPrev.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSubPrev.Font = new System.Drawing.Font("ＭＳ ゴシック", 16F, System.Drawing.FontStyle.Bold);
            this.buttonSubPrev.Location = new System.Drawing.Point(0, 0);
            this.buttonSubPrev.Name = "buttonSubPrev";
            this.buttonSubPrev.Size = new System.Drawing.Size(27, 33);
            this.buttonSubPrev.TabIndex = 70;
            this.buttonSubPrev.TabStop = false;
            this.buttonSubPrev.Text = "←";
            this.buttonSubPrev.UseVisualStyleBackColor = true;
            this.buttonSubPrev.Click += new System.EventHandler(this.buttonSubPrev_Click);
            // 
            // buttonSubNext
            // 
            this.buttonSubNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSubNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 16F, System.Drawing.FontStyle.Bold);
            this.buttonSubNext.Location = new System.Drawing.Point(25, 0);
            this.buttonSubNext.Name = "buttonSubNext";
            this.buttonSubNext.Size = new System.Drawing.Size(25, 33);
            this.buttonSubNext.TabIndex = 71;
            this.buttonSubNext.TabStop = false;
            this.buttonSubNext.Text = "→";
            this.buttonSubNext.UseVisualStyleBackColor = true;
            this.buttonSubNext.Click += new System.EventHandler(this.buttonSubNext_Click);
            // 
            // labelSubCnt
            // 
            this.labelSubCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubCnt.AutoSize = true;
            this.labelSubCnt.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelSubCnt.Location = new System.Drawing.Point(383, 9);
            this.labelSubCnt.Name = "labelSubCnt";
            this.labelSubCnt.Size = new System.Drawing.Size(55, 21);
            this.labelSubCnt.TabIndex = 72;
            this.labelSubCnt.Text = "(0/0)";
            // 
            // panelSub
            // 
            this.panelSub.Controls.Add(this.panelSubCursor);
            this.panelSub.Controls.Add(this.buttonImageFillSub);
            this.panelSub.Controls.Add(this.labelSubCnt);
            this.panelSub.Controls.Add(this.labelSubImageName);
            this.panelSub.Controls.Add(this.userControlImageSub);
            this.panelSub.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSub.Location = new System.Drawing.Point(0, 0);
            this.panelSub.Name = "panelSub";
            this.panelSub.Size = new System.Drawing.Size(500, 782);
            this.panelSub.TabIndex = 74;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(500, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 782);
            this.splitter1.TabIndex = 75;
            this.splitter1.TabStop = false;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.userControlImageMain);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(505, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(501, 782);
            this.panelLeft.TabIndex = 76;
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Location = new System.Drawing.Point(6, 4);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 66;
            this.buttonImageFill.TabStop = false;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            // 
            // labelImageName
            // 
            this.labelImageName.Location = new System.Drawing.Point(590, 12);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(75, 13);
            this.labelImageName.TabIndex = 66;
            this.labelImageName.Text = "画像ファイル名";
            // 
            // panelCenter
            // 
            this.panelCenter.Controls.Add(this.userControlOryo1);
            this.panelCenter.Controls.Add(this.dataGridView1);
            this.panelCenter.Controls.Add(this.groupBoxThisApp);
            this.panelCenter.Controls.Add(this.panelDown);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelCenter.Location = new System.Drawing.Point(1006, 0);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(475, 782);
            this.panelCenter.TabIndex = 78;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(475, 274);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // groupBoxThisApp
            // 
            this.groupBoxThisApp.BackColor = System.Drawing.Color.LightSkyBlue;
            this.groupBoxThisApp.Controls.Add(this.labelTenkenResult);
            this.groupBoxThisApp.Controls.Add(this.radioButtonOryoGigiYes);
            this.groupBoxThisApp.Controls.Add(this.radioButtonOryoGigiNo);
            this.groupBoxThisApp.Controls.Add(this.buttonPrint2);
            this.groupBoxThisApp.Controls.Add(this.buttonSet);
            this.groupBoxThisApp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxThisApp.Location = new System.Drawing.Point(0, 595);
            this.groupBoxThisApp.MaximumSize = new System.Drawing.Size(475, 139);
            this.groupBoxThisApp.Name = "groupBoxThisApp";
            this.groupBoxThisApp.Size = new System.Drawing.Size(475, 139);
            this.groupBoxThisApp.TabIndex = 2;
            this.groupBoxThisApp.TabStop = false;
            this.groupBoxThisApp.Text = "今月点検結果";
            // 
            // labelTenkenResult
            // 
            this.labelTenkenResult.AutoSize = true;
            this.labelTenkenResult.Location = new System.Drawing.Point(32, 30);
            this.labelTenkenResult.Name = "labelTenkenResult";
            this.labelTenkenResult.Size = new System.Drawing.Size(55, 13);
            this.labelTenkenResult.TabIndex = 8;
            this.labelTenkenResult.Text = "申請距離";
            // 
            // radioButtonOryoGigiYes
            // 
            this.radioButtonOryoGigiYes.AutoSize = true;
            this.radioButtonOryoGigiYes.Checked = true;
            this.radioButtonOryoGigiYes.Location = new System.Drawing.Point(350, 44);
            this.radioButtonOryoGigiYes.Name = "radioButtonOryoGigiYes";
            this.radioButtonOryoGigiYes.Size = new System.Drawing.Size(91, 17);
            this.radioButtonOryoGigiYes.TabIndex = 4;
            this.radioButtonOryoGigiYes.TabStop = true;
            this.radioButtonOryoGigiYes.Text = "往療疑義あり";
            this.radioButtonOryoGigiYes.UseVisualStyleBackColor = true;
            // 
            // radioButtonOryoGigiNo
            // 
            this.radioButtonOryoGigiNo.AutoSize = true;
            this.radioButtonOryoGigiNo.Location = new System.Drawing.Point(350, 68);
            this.radioButtonOryoGigiNo.Name = "radioButtonOryoGigiNo";
            this.radioButtonOryoGigiNo.Size = new System.Drawing.Size(92, 17);
            this.radioButtonOryoGigiNo.TabIndex = 5;
            this.radioButtonOryoGigiNo.TabStop = true;
            this.radioButtonOryoGigiNo.Text = "往療疑義なし";
            this.radioButtonOryoGigiNo.UseVisualStyleBackColor = true;
            this.radioButtonOryoGigiNo.CheckedChanged += new System.EventHandler(this.radioButtonOryoGigiNo_CheckedChanged);
            // 
            // buttonPrint2
            // 
            this.buttonPrint2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint2.ContextMenuStrip = this.contextMenuStrip1;
            this.buttonPrint2.Location = new System.Drawing.Point(397, 96);
            this.buttonPrint2.Name = "buttonPrint2";
            this.buttonPrint2.Size = new System.Drawing.Size(75, 25);
            this.buttonPrint2.TabIndex = 7;
            this.buttonPrint2.TabStop = false;
            this.buttonPrint2.Text = "資料印刷";
            this.buttonPrint2.UseVisualStyleBackColor = true;
            this.buttonPrint2.Click += new System.EventHandler(this.buttonPrint2_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.プリンター選択ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 26);
            // 
            // プリンター選択ToolStripMenuItem
            // 
            this.プリンター選択ToolStripMenuItem.Name = "プリンター選択ToolStripMenuItem";
            this.プリンター選択ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.プリンター選択ToolStripMenuItem.Text = "プリンター選択";
            this.プリンター選択ToolStripMenuItem.Click += new System.EventHandler(this.プリンター選択ToolStripMenuItem_Click);
            // 
            // buttonSet
            // 
            this.buttonSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSet.Location = new System.Drawing.Point(312, 96);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(75, 25);
            this.buttonSet.TabIndex = 6;
            this.buttonSet.Text = "確定";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // panelDown
            // 
            this.panelDown.Controls.Add(this.buttonExit);
            this.panelDown.Controls.Add(this.buttonNext);
            this.panelDown.Controls.Add(this.buttonBack);
            this.panelDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelDown.Location = new System.Drawing.Point(0, 734);
            this.panelDown.Name = "panelDown";
            this.panelDown.Size = new System.Drawing.Size(475, 48);
            this.panelDown.TabIndex = 35;
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExit.Location = new System.Drawing.Point(120, 13);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(60, 25);
            this.buttonExit.TabIndex = 9;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNext.Location = new System.Drawing.Point(192, 13);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 25);
            this.buttonNext.TabIndex = 8;
            this.buttonNext.Text = "次 >> PgUp";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(32, 13);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 25);
            this.buttonBack.TabIndex = 10;
            this.buttonBack.Text = "PgDn << 前";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // userControlImageMain
            // 
            this.userControlImageMain.AutoScroll = true;
            this.userControlImageMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImageMain.Location = new System.Drawing.Point(0, 0);
            this.userControlImageMain.Name = "userControlImageMain";
            this.userControlImageMain.Size = new System.Drawing.Size(501, 782);
            this.userControlImageMain.TabIndex = 0;
            this.userControlImageMain.TabStop = false;
            // 
            // userControlOryo1
            // 
            this.userControlOryo1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlOryo1.Location = new System.Drawing.Point(0, 280);
            this.userControlOryo1.Name = "userControlOryo1";
            this.userControlOryo1.Size = new System.Drawing.Size(472, 309);
            this.userControlOryo1.TabIndex = 37;
            // 
            // userControlImageSub
            // 
            this.userControlImageSub.AutoScroll = true;
            this.userControlImageSub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImageSub.Location = new System.Drawing.Point(0, 0);
            this.userControlImageSub.Name = "userControlImageSub";
            this.userControlImageSub.Size = new System.Drawing.Size(500, 782);
            this.userControlImageSub.TabIndex = 0;
            this.userControlImageSub.TabStop = false;
            // 
            // InspectOryoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1481, 782);
            this.Controls.Add(this.labelImageName);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelSub);
            this.KeyPreview = true;
            this.Name = "InspectOryoForm";
            this.Text = "Inspect";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormInspect_Load);
            this.SizeChanged += new System.EventHandler(this.FormInspect_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormInspect_KeyDown);
            this.panelSubCursor.ResumeLayout(false);
            this.panelSub.ResumeLayout(false);
            this.panelSub.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxThisApp.ResumeLayout(false);
            this.groupBoxThisApp.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panelDown.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonSubNext;
        private System.Windows.Forms.Button buttonSubPrev;
        private System.Windows.Forms.Label labelSubCnt;
        private System.Windows.Forms.Panel panelSubCursor;
        private UserControlImage userControlImageSub;
        private System.Windows.Forms.Label labelSubImageName;
        private System.Windows.Forms.Button buttonImageFillSub;
        private System.Windows.Forms.Panel panelSub;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImageMain;
        private System.Windows.Forms.Panel panelCenter;
        private UserControlOryo userControlOryo1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBoxThisApp;
        private System.Windows.Forms.Label labelTenkenResult;
        private System.Windows.Forms.RadioButton radioButtonOryoGigiYes;
        private System.Windows.Forms.RadioButton radioButtonOryoGigiNo;
        private System.Windows.Forms.Button buttonPrint2;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Panel panelDown;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem プリンター選択ToolStripMenuItem;
    }
}