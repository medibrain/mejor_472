﻿namespace Mejor.Inspect
{
    partial class FamilyImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxRelation = new System.Windows.Forms.CheckBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.labelIndex = new System.Windows.Forms.Label();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.userControlImageMain = new UserControlImage();
            this.inspectControl1 = new Mejor.Inspect.InspectControl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxRelation);
            this.panel1.Controls.Add(this.buttonPrint);
            this.panel1.Controls.Add(this.labelIndex);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 1014);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(996, 27);
            this.panel1.TabIndex = 0;
            // 
            // checkBoxRelation
            // 
            this.checkBoxRelation.AutoSize = true;
            this.checkBoxRelation.Location = new System.Drawing.Point(297, 5);
            this.checkBoxRelation.Name = "checkBoxRelation";
            this.checkBoxRelation.Size = new System.Drawing.Size(136, 17);
            this.checkBoxRelation.TabIndex = 6;
            this.checkBoxRelation.Text = "参考申請書として指定";
            this.checkBoxRelation.UseVisualStyleBackColor = true;
            this.checkBoxRelation.CheckedChanged += new System.EventHandler(this.checkBoxRelation_CheckedChanged);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(3, 1);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 25);
            this.buttonPrint.TabIndex = 5;
            this.buttonPrint.Text = "印刷";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // labelIndex
            // 
            this.labelIndex.AutoSize = true;
            this.labelIndex.Location = new System.Drawing.Point(84, 7);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(55, 13);
            this.labelIndex.TabIndex = 3;
            this.labelIndex.Text = "labelIndex";
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(918, 1);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 25);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = "次へ >>";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(840, 1);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 25);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "閉じる";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBack.Location = new System.Drawing.Point(762, 1);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 25);
            this.buttonBack.TabIndex = 0;
            this.buttonBack.Text = "<< 戻る";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // userControlImageMain
            // 
            this.userControlImageMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userControlImageMain.AutoScroll = true;
            this.userControlImageMain.Location = new System.Drawing.Point(0, 0);
            this.userControlImageMain.Name = "userControlImageMain";
            this.userControlImageMain.Size = new System.Drawing.Size(690, 1014);
            this.userControlImageMain.TabIndex = 1;
            // 
            // inspectControl1
            // 
            this.inspectControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.inspectControl1.Enabled = false;
            this.inspectControl1.Location = new System.Drawing.Point(696, 0);
            this.inspectControl1.MinimumSize = new System.Drawing.Size(300, 930);
            this.inspectControl1.Name = "inspectControl1";
            this.inspectControl1.RelationAid = 0;
            this.inspectControl1.RelationButtonVisible = true;
            this.inspectControl1.Size = new System.Drawing.Size(300, 1014);
            this.inspectControl1.TabIndex = 2;
            // 
            // FamilyImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 1041);
            this.Controls.Add(this.inspectControl1);
            this.Controls.Add(this.userControlImageMain);
            this.Controls.Add(this.panel1);
            this.Name = "FamilyImageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "関連レセ";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonBack;
        private UserControlImage userControlImageMain;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.CheckBox checkBoxRelation;
        private InspectControl inspectControl1;
    }
}