﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    class InspectCommon
    {
        /// <summary>
        /// メイン画像の表示
        /// </summary>
        /// <param name="r"></param>
        public static void displayImageRefresh(
            App app, 
            Label imageLabel,
            UserControlImage image,
            bool isMain
            )
        {
            string fn = app.GetImageFullPath();

            try
            {
                var img = System.Drawing.Image.FromFile(fn);
                image.SetPictureFile(fn);
                if (isMain)
                {
                    image.SetPictureBoxFill();
                }

                imageLabel.Text = fn;
            }
            catch
            {
                string msg = isMain ? "メイン画像表示でエラーが発生しました" : "サブ画像表示でエラーが発生しました";
                MessageBox.Show(msg);
                return;
            }
        }
    }
}
