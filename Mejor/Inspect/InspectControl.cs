﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Mejor.Inspect
{
    [DefaultEvent("DataChanged")]
    public partial class InspectControl : UserControl
    {
        bool _changed = false;
        App currentApp = null;

        //20201016174129 furukawa st ////////////////////////
        //リードオンリーフラグ追加
        //更新させるか：基本更新させる、関連レセSetAppのFlgEnabled=falseの場合更新させない
        bool flgEnabled = true;
        //20201016174129 furukawa ed ////////////////////////

        public bool ShokaiChecked => checkBoxShokai.Checked;

        /// <summary>
        /// SetApp()以外の時に、内容が変更されたかどうかを表します
        /// </summary>
        public bool Changed
        {
            get { return _changed; }
            private set
            {
                if (!value || _changed == value) return;
                _changed = value;
                DataChanged?.Invoke(this, new EventArgs());
            }
        }

        bool nowSetting = false;

        /// <summary>
        /// 入力内容が変更された時、始めに1度だけ発生します
        /// </summary>
        public event EventHandler DataChanged;

        public InspectControl()
        {
            InitializeComponent();

            void setChangeEvent(Control c)
            {
                foreach (Control item in c.Controls) setChangeEvent(item);
                if (c is TextBox tb) tb.TextChanged += Controls_Changed;
                else if (c is CheckBox cb) cb.CheckedChanged += Controls_Changed;
                else if (c is RadioButton rb) rb.CheckedChanged += Controls_Changed;

                //enterでのコントロール移動＆テキストボックス改行阻止
                if (c is TextBox || c is CheckBox || c is RadioButton)
                    c.KeyPress += (s, e) =>
                    {
                        if (e.KeyChar == (char)Keys.Enter)
                        {
                            SendKeys.Send("{TAB}");
                            e.Handled = true;
                        }
                    };
            }
            setChangeEvent(this);
        }

        public bool RelationButtonVisible
        {
            get => buttonRelationAppShow.Visible;
            set => buttonRelationAppShow.Visible = value;
        }

        private void Controls_Changed(object sender, EventArgs e)
        {
            if (nowSetting) return;
            Changed = true;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked) return;

            if (radioButtonSai.Checked)
            {
                groupBoxSaishinsa.Enabled = true;
                groupBoxKago.Enabled = false;
            }
            else if (radioButtonKago.Checked)
            {
                groupBoxSaishinsa.Enabled = false;
                groupBoxKago.Enabled = true;
            }
            else
            {
                groupBoxSaishinsa.Enabled = false;
                groupBoxKago.Enabled = false;
            }
        }

        /// <summary>
        /// 点検対象時、申し出なしにフォーカスを移動します
        /// </summary>
        public void NoneFocus()
        {
            if (!checkBoxTenken.Checked) return;
            radioButtonNone.Focus();
        }

        int _relationAid = 0;
        public int RelationAid
        {
            get
            {
                return _relationAid;
            }
            set
            {
                _relationAid = value;
                labelRefRece.Text = _relationAid == 0 ?
                    "なし" : $"AID:{_relationAid}";
            }
        }

        

        
        /// <summary>
        /// ステータスを画面に表示
        /// </summary>
        /// <param name="app">Appデータ</param>
        /// <param name="flgEnabled">更新させる場合＝True、させない=falsse</param>
        
        //20201016152431 furukawa st ////////////////////////
        //リードオンリーフラグ追加
        public void SetApp(App app, bool flgEnabled = true)
        //public void SetApp(App app)        
        //20201016152431 furukawa ed ////////////////////////
        {
            this.flgEnabled = flgEnabled;
            try
            {
                SuspendLayout();
                nowSetting = true;
                currentApp = app;
                Enabled = app != null;

                AllClear();
                if (app == null) return;

                RelationAid = app.TaggedDatas.RelationAID;

                if (app.StatusFlagCheck(StatusFlag.照会対象))
                {
                    checkBoxShokai.Checked = true;
                    checkBoxBuiCount.Checked = app.ShokaiReasonCheck(ShokaiReason.部位数);
                    checkBoxClinic.Checked = app.ShokaiReasonCheck(ShokaiReason.疑義施術所);
                    checkBoxDayCount.Checked = app.ShokaiReasonCheck(ShokaiReason.施術日数);
                    checkBoxPeriod.Checked = app.ShokaiReasonCheck(ShokaiReason.施術期間);
                    checkBoxTotal.Checked = app.ShokaiReasonCheck(ShokaiReason.合計金額);
                    checkBoxOther.Checked = app.ShokaiReasonCheck(ShokaiReason.その他);
                    textBoxShokaiMemo.Text = app.MemoShokai;

                    if (app.ShokaiReasonCheck(ShokaiReason.宛所なし)) radioButtonShokaiAteNashi.Checked = true;
                    else if (app.ShokaiReasonCheck(ShokaiReason.相違なし)) radioButtonShokaiSouiNashi.Checked = true;
                    else if (app.ShokaiReasonCheck(ShokaiReason.相違あり)) radioButtonShokaiSouiAri.Checked = true;
                    else radioButtonShokaiMihenshin.Checked = true;
                }

                if (app.StatusFlagCheck(StatusFlag.点検対象))
                {
                    checkBoxTenken.Checked = true;

                    if (app.StatusFlagCheck(StatusFlag.保留)) radioButtonPend.Checked = true;
                    else if (app.StatusFlagCheck(StatusFlag.過誤)) radioButtonKago.Checked = true;
                    else if (app.StatusFlagCheck(StatusFlag.再審査)) radioButtonSai.Checked = true;
                    else if (app.StatusFlagCheck(StatusFlag.点検済)) radioButtonNone.Checked = true;

                    

                    #region 過誤
                    //20201011151622 furukawa st ////////////////////////
                    //過誤フラグ追加に伴ってxml列に変更
                    
                    checkBoxKagoGenin.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし);
                    checkBoxKagoGen1.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし1);
                    checkBoxKagoGen2.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし2);
                    checkBoxKagoGen3.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし3);
                    checkBoxKagoGen4.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし4);
                    checkBoxKagoGen5.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし5);

                    checkBoxKagoLong.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし);
                    checkBoxKagoLong1.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし1);
                    checkBoxKagoLong2.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし2);
                    checkBoxKagoLong3.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし3);
                    checkBoxKagoLong4.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし4);
                    checkBoxKagoLong5.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし5);

                    checkBoxKagoDiff.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違);
                    checkBoxKagoDiff1.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違1);
                    checkBoxKagoDiff2.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違2);
                    checkBoxKagoDiff3.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違3);
                    checkBoxKagoDiff4.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違4);
                    checkBoxKagoDiff5.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違5);

                    checkBoxKagoShomei.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.署名違い);
                    checkBoxKagoHisseki.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.筆跡違い);
                    checkBoxKagoFamily.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.家族同一筆跡);
                    checkBoxKagoOther.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.その他);


                    checkBoxKagoLongDiff.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違);
                    checkBoxKagoLongDiff1.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違1);
                    checkBoxKagoLongDiff2.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違2);
                    checkBoxKagoLongDiff3.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違3);
                    checkBoxKagoLongDiff4.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違4);
                    checkBoxKagoLongDiff5.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違5);

                    checkBoxKagoShometsu.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.療養費請求権の消滅時効);
                    checkBoxKagoHonke.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.本家区分誤り);
                    checkBoxKagoInsSoui.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.保険者相違);
                    checkBoxKagoOuryoNoReason.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし);
                    checkBoxKagoOuryoOver16km.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上);

                    checkBoxKagoDoui.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.同意書);
                    checkBoxKagoDouiNone.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.同意書添付なし);
                    checkBoxKagoDouiMis.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.同意書期限切れ);

                    checkBoxKagoHoukoku.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書);
                    checkBoxKagoHoukokuNone.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書添付なし);
                    checkBoxKagoHoukokuMis.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書記載不備);

                    checkBoxKagoJotai.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書);
                    checkBoxKagoJotaiNone.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書添付なし);
                    checkBoxKagoJotaiMis.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書記載不備);

                    //20210201154354 furukawa st ////////////////////////
                    //往療内訳書追加
                    
                    checkBoxKagoOryoUchiwake.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書);
                    checkBoxKagoOryoUchiwakeNone.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書添付なし);
                    checkBoxKagoOryoUchiwakeMis.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書記載不備);
                    //20210201154354 furukawa ed ////////////////////////

                    checkBoxKagoDokuzi.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自);
                    checkBoxKagoDokuzi1.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自1);
                    checkBoxKagoDokuzi2.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自2);
                    checkBoxKagoDokuzi3.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自3);
                    checkBoxKagoDokuzi4.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自4);
                    checkBoxKagoDokuzi5.Checked = app.KagoReasonsCheck_xml(KagoReasons_Member.独自5);




                    //従来
                    //checkBoxKagoGenin.Checked = app.KagoReasonsCheck(KagoReasons.原因なし);
                    //checkBoxKagoGen1.Checked = app.KagoReasonsCheck(KagoReasons.原因なし1);
                    //checkBoxKagoGen2.Checked = app.KagoReasonsCheck(KagoReasons.原因なし2);
                    //checkBoxKagoGen3.Checked = app.KagoReasonsCheck(KagoReasons.原因なし3);
                    //checkBoxKagoGen4.Checked = app.KagoReasonsCheck(KagoReasons.原因なし4);
                    //checkBoxKagoGen5.Checked = app.KagoReasonsCheck(KagoReasons.原因なし5);
                    //checkBoxKagoLong.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし);
                    //checkBoxKagoLong1.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし1);
                    //checkBoxKagoLong2.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし2);
                    //checkBoxKagoLong3.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし3);
                    //checkBoxKagoLong4.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし4);
                    //checkBoxKagoLong5.Checked = app.KagoReasonsCheck(KagoReasons.長期理由なし5);
                    //checkBoxKagoDiff.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違);
                    //checkBoxKagoDiff1.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違1);
                    //checkBoxKagoDiff2.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違2);
                    //checkBoxKagoDiff3.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違3);
                    //checkBoxKagoDiff4.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違4);
                    //checkBoxKagoDiff5.Checked = app.KagoReasonsCheck(KagoReasons.負傷原因相違5);
                    //checkBoxKagoShomei.Checked = app.KagoReasonsCheck(KagoReasons.署名違い);
                    //checkBoxKagoHisseki.Checked = app.KagoReasonsCheck(KagoReasons.筆跡違い);
                    //checkBoxKagoFamily.Checked = app.KagoReasonsCheck(KagoReasons.家族同一筆跡);
                    //checkBoxKagoOther.Checked = app.KagoReasonsCheck(KagoReasons.その他);


                    //20201011151622 furukawa ed ////////////////////////


                    #endregion


                    #region 再審査

                    radioButtonGigiShoken.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.初検料);
                    checkBoxF1.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.負傷1);
                    checkBoxF2.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.負傷2);
                    checkBoxF3.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.負傷3);
                    checkBoxF4.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.負傷4);
                    checkBoxF5.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.負傷5);
                    radioButtonGigiTenki.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.転帰なし);
                    radioButtonGigiChushi.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.中止);
                    radioButtonGigiDoitsu.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.同一負傷名);
                    radioButtonGigiBetsu.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.別負傷名);
                    radioButtonSaiOther.Checked = app.SaishinsaReasonsCheck(SaishinsaReasons.その他);
                    #endregion


                    textBoxTenkenMemo.Text = app.MemoInspect;
                }

                if (app.StatusFlagCheck(StatusFlag.往療点検対象))
                {
                    checkBoxOryo.Checked = true;
                    if (app.StatusFlagCheck(StatusFlag.往療疑義)) radioButtonOryoYes.Checked = true;
                    else if (app.StatusFlagCheck(StatusFlag.往療点検済)) radioButtonOryoNo.Checked = true;
                }

                #region 返戻/架電
                if (app.StatusFlagCheck(StatusFlag.返戻) || app.StatusFlagCheck(StatusFlag.架電対象))
                {
                    if (app.StatusFlagCheck(StatusFlag.返戻)) checkBoxHenrei.Checked = true;
                    if (app.StatusFlagCheck(StatusFlag.架電対象)) checkBoxTel.Checked = true;
                    checkBoxHenBui.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位);
                    checkBoxHenBui1.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位1);
                    checkBoxHenBui2.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位2);
                    checkBoxHenBui3.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位3);
                    checkBoxHenBui4.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位4);
                    checkBoxHenBui5.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷部位5);
                    checkBoxHenReason.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因);
                    checkBoxHenReason1.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因1);
                    checkBoxHenReason2.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因2);
                    checkBoxHenReason3.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因3);
                    checkBoxHenReason4.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因4);
                    checkBoxHenReason5.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷原因5);
                    checkBoxHenJiki.Checked = app.HenreiReasonsCheck(HenreiReasons.負傷時期);
                    checkBoxHenKega.Checked = app.HenreiReasonsCheck(HenreiReasons.けが外);
                    checkBoxHenKago.Checked = app.HenreiReasonsCheck(HenreiReasons.過誤);
                    checkBoxHenDays.Checked = app.HenreiReasonsCheck(HenreiReasons.受療日数);
                    checkBoxHenWork.Checked = app.HenreiReasonsCheck(HenreiReasons.勤務中);
                    checkBoxHenOther.Checked = app.HenreiReasonsCheck(HenreiReasons.その他);

                    if (!app.TaggedDatas.ContactDt.IsNullDate())
                    {
                        textBoxContactY.Text = app.TaggedDatas.ContactDt.Year.ToString();
                        textBoxContactM.Text = app.TaggedDatas.ContactDt.Month.ToString();
                        textBoxContactD.Text = app.TaggedDatas.ContactDt.Day.ToString();
                        textBoxContactH.Text = app.TaggedDatas.ContactDt.Hour.ToString();
                        textBoxContactMin.Text = app.TaggedDatas.ContactDt.Minute.ToString();
                    }
                    checkBoxContactNo.Checked = app.TaggedDatas.ContactStatusCheck(ContactStatus.連絡不要);
                    checkBoxContactOrg.Checked = app.TaggedDatas.ContactStatusCheck(ContactStatus.団体);
                    textBoxContactName.Text = app.TaggedDatas.ContactName;
                }
                #endregion

                if (app.StatusFlagCheck(StatusFlag.支払保留))
                {
                    checkBoxPend.Checked = true;
                }

                if (app.StatusFlagCheck(StatusFlag.入力時エラー))
                {
                    checkBoxInputError.Checked = true;
                }

                if (app.StatusFlagCheck(StatusFlag.支払済))
                {
                    checkBoxPaid.Checked = true;
                }

                checkBoxProcess1.Checked = app.StatusFlagCheck(StatusFlag.処理1);
                checkBoxProcess2.Checked = app.StatusFlagCheck(StatusFlag.処理2);
                checkBoxProcess3.Checked = app.StatusFlagCheck(StatusFlag.処理3);
                checkBoxProcess4.Checked = app.StatusFlagCheck(StatusFlag.処理4);
                //checkBoxProcess5.Checked = app.StatusFlagCheck(StatusFlag.処理5);

                textBoxMemo.Text = app.Memo;
                textBoxOutMemo.Text = app.OutMemo;


           

            }
            finally
            {
                ResumeLayout();
                nowSetting = false;
                _changed = false;
            }
        }

        /// <summary>
        /// すべての表示をクリアします
        /// </summary>
        public void AllClear()
        {
            nowSetting = true;
            clear(this);
            nowSetting = false;
            RelationAid = 0;
        }

        private void clear(Control c)
        {
            foreach (Control item in c.Controls) clear(item);
            if (c is TextBox) ((TextBox)c).Clear();
            else if (c is CheckBox) ((CheckBox)c).Checked = false;
            else if (c is RadioButton) ((RadioButton)c).Checked = false;
        }

        /// <summary>
        /// 現在の入力状態でアップデートできるか検証します
        /// </summary>
        /// <returns></returns>
        public bool CanUpdate(App app, bool withMsg)
        {
            if (checkBoxShokai.Checked && withMsg)
            {
                if (!checkBoxBuiCount.Checked && !checkBoxClinic.Checked &&
                    !checkBoxDayCount.Checked && !checkBoxPeriod.Checked &&
                    !checkBoxTotal.Checked && !checkBoxOther.Checked)
                {
                    var res = MessageBox.Show("照会理由が指定されていませんがよろしいですか？",
                        "照会理由", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    if (res != DialogResult.OK) return false;
                }
            }

            if (checkBoxTenken.Checked)
            {
                if (app.StatusFlagCheck(StatusFlag.点検済) &&
                    !radioButtonNone.Checked && !radioButtonSai.Checked &&
                    !radioButtonKago.Checked && !radioButtonPend.Checked)
                {
                    MessageBox.Show("いずれかの点検結果にチェックを入れてください");
                    return false;
                }
            }
            else if (app.StatusFlagCheck(StatusFlag.点検済) && withMsg)
            {
                var res = MessageBox.Show("点検済みのデータを対象外にしようとしています。" +
                    "点検結果は失われますがよろしいですか？", "対象外確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            if (checkBoxOryo.Checked)
            {
                if (app.StatusFlagCheck(StatusFlag.往療点検済) &&
                    !radioButtonOryoYes.Checked && !radioButtonOryoNo.Checked)
                {
                    MessageBox.Show("いずれかの往療点検結果にチェックを入れてください");
                    return false;
                }
            }
            else if (app.StatusFlagCheck(StatusFlag.往療点検済) && withMsg)
            {
                var res = MessageBox.Show("往療点検済みのデータを対象外にしようとしています。" +
                    "往療点検結果は失われますがよろしいですか？", "対象外確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            if (checkBoxHenrei.Checked || checkBoxTel.Checked)
            {
                if (!checkBoxHenBui.Checked && !checkBoxHenReason.Checked && !checkBoxHenJiki.Checked && !checkBoxHenKago.Checked &&
                    !checkBoxHenKega.Checked && !checkBoxHenDays.Checked && !checkBoxHenWork.Checked && !checkBoxHenOther.Checked)
                {
                    MessageBox.Show("いずれかの返戻/架電理由にチェックを入れてください");
                    return false;
                }
                else if(checkBoxHenKago.Checked && 
                    (!checkBoxTenken.Checked || !radioButtonKago.Checked))
                {
                    MessageBox.Show("過誤による返戻を指定した場合は「点検対象」かつ「過誤」に指定してください");
                    return false;
                }

                //連絡記録
                if (textBoxContactY.Text.Trim().Length > 0 ||
                    textBoxContactM.Text.Trim().Length > 0 ||
                    textBoxContactD.Text.Trim().Length > 0 ||
                    textBoxContactH.Text.Trim().Length > 0 ||
                    textBoxContactMin.Text.Trim().Length > 0)
                {
                    if (!int.TryParse(textBoxContactY.Text, out int Y) ||
                        !int.TryParse(textBoxContactM.Text, out int M) ||
                        !int.TryParse(textBoxContactD.Text, out int D) ||
                        !int.TryParse(textBoxContactH.Text, out int H) ||
                        !int.TryParse(textBoxContactMin.Text, out int Min) ||
                        Y < DateTime.Today.Year - 2 || Y > DateTime.Today.Year ||
                        H < 0 || 23 < H ||
                        Min < 0 || 59 < Min ||
                        !DateTimeEx.IsDate(Y, M, D))
                    {
                        MessageBox.Show("連絡記録の日付を正確に記述してください");
                        return false;
                    }

                    if ((H < 9 || 20 < H) && withMsg)
                    {
                        var res = MessageBox.Show("連絡記録の時刻は24時間表記で入力してください(午後2時は14時)。" +
                            "通常連絡しない時刻が指定されていますが、このまま登録してもよろしいですか？", "時刻確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != DialogResult.OK) return false;
                    }
                }
            }
            else if (withMsg &&
                (app.HenreiReasonsCheck(HenreiReasons.負傷部位) |
                app.HenreiReasonsCheck(HenreiReasons.負傷原因) |
                app.HenreiReasonsCheck(HenreiReasons.負傷時期) |
                app.HenreiReasonsCheck(HenreiReasons.過誤) |
                app.HenreiReasonsCheck(HenreiReasons.けが外) |
                app.HenreiReasonsCheck(HenreiReasons.受療日数) |
                app.HenreiReasonsCheck(HenreiReasons.勤務中) |
                app.HenreiReasonsCheck(HenreiReasons.その他)))
            {
                var res = MessageBox.Show("返戻/架電理由がクリアされますがよろしいですか？", "対象外確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            return true;
        }

        /// <summary>
        /// 登録
        /// </summary>
        /// <param name="app"></param>
        public void DataSetToApp(App app)
        {
            //20201016152326 furukawa st ////////////////////////
            //リードオンリーフラグつける
            if (!flgEnabled) return;
            //20201016152326 furukawa ed ////////////////////////

            if (!CanUpdate(app, false)) throw new Exception("入力データのチェックが必要です");

            //照会フラグ
           if (checkBoxShokai.Checked) app.StatusFlagSet(StatusFlag.照会対象);

            else app.StatusFlagRemove(StatusFlag.照会対象);

            //照会理由
            app.ShokaiReasonOverwrite(ShokaiReason.なし);
            if (checkBoxShokai.Checked)
            {
                if(checkBoxBuiCount.Checked) app.ShokaiReasonSet(ShokaiReason.部位数);
                if(checkBoxClinic.Checked) app.ShokaiReasonSet(ShokaiReason.疑義施術所);
                if(checkBoxDayCount.Checked) app.ShokaiReasonSet(ShokaiReason.施術日数);
                if(checkBoxPeriod.Checked) app.ShokaiReasonSet(ShokaiReason.施術期間);
                if(checkBoxTotal.Checked) app.ShokaiReasonSet(ShokaiReason.合計金額);
                if(checkBoxOther.Checked) app.ShokaiReasonSet(ShokaiReason.その他);
                app.MemoShokai = textBoxShokaiMemo.Text.Trim();

                //照会結果リセット
                app.ShokaiReasonRemove(ShokaiReason.宛所なし | ShokaiReason.相違なし | ShokaiReason.相違あり);

                if (radioButtonShokaiAteNashi.Checked) app.ShokaiReasonSet(ShokaiReason.宛所なし);
                else if (radioButtonShokaiSouiNashi.Checked) app.ShokaiReasonSet(ShokaiReason.相違なし);
                else if (radioButtonShokaiSouiAri.Checked) app.ShokaiReasonSet(ShokaiReason.相違あり);
            }

            //新フラグリセット
            app.KagoReasons = KagoReasons.なし;//現行


            //20201011151421 furukawa st ////////////////////////
            //過誤フラグ追加に伴ってxml列追加
                                             
            app.KagoReasons_xml(KagoReasons_Member.初期化.ToString(), App.FlgValue.add);
            //20201011151421 furukawa ed ////////////////////////



            app.SaishinsaReasons = SaishinsaReasons.なし;
            app.HenreiReasons = HenreiReasons.なし;
            app.TaggedDatas.ContactStatus = ContactStatus.Null;

            //点検フラグ管理
            app.StatusFlagRemove(StatusFlag.保留 | StatusFlag.過誤 | StatusFlag.再審査);
            app.MemoInspect = textBoxTenkenMemo.Text.Trim();

            if (checkBoxTenken.Checked)
            {
                app.StatusFlagSet(StatusFlag.点検対象);

                if (radioButtonNone.Checked || radioButtonPend.Checked ||
                    radioButtonKago.Checked || radioButtonSai.Checked)
                    app.StatusFlagSet(StatusFlag.点検済);

                if (radioButtonPend.Checked) app.StatusFlagSet(StatusFlag.保留);
                else if (radioButtonKago.Checked) app.StatusFlagSet(StatusFlag.過誤);
                else if (radioButtonSai.Checked) app.StatusFlagSet(StatusFlag.再審査);


                if (radioButtonKago.Checked)
                {
                    //20201012172513 furukawa st ////////////////////////
                    //コントロール間違い
                    
                    app.KagoReasons_xml(KagoReasons_Member.原因なし.ToString(), checkBoxKagoGenin.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.原因なし1.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.原因なし2.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen2.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.原因なし3.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen3.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.原因なし4.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen4.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.原因なし5.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen5.Checked ? App.FlgValue.add : App.FlgValue.del);

                    //app.KagoReasons_xml(KagoReasons_Member.原因なし1.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    //app.KagoReasons_xml(KagoReasons_Member.原因なし2.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    //app.KagoReasons_xml(KagoReasons_Member.原因なし3.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    //app.KagoReasons_xml(KagoReasons_Member.原因なし4.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    //app.KagoReasons_xml(KagoReasons_Member.原因なし5.ToString(), checkBoxKagoGenin.Checked && checkBoxKagoGen1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    //20201012172513 furukawa ed ////////////////////////
                }

                if (checkBoxKagoLong.Checked)
                {

                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし.ToString(), checkBoxKagoLong.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし1.ToString(), checkBoxKagoLong.Checked && checkBoxKagoLong1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし2.ToString(), checkBoxKagoLong.Checked && checkBoxKagoLong2.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし3.ToString(), checkBoxKagoLong.Checked && checkBoxKagoLong3.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし4.ToString(), checkBoxKagoLong.Checked && checkBoxKagoLong4.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由なし5.ToString(), checkBoxKagoLong.Checked && checkBoxKagoLong5.Checked ? App.FlgValue.add : App.FlgValue.del);
                    
                }

                if (checkBoxKagoDiff.Checked)
                {

                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違.ToString(), checkBoxKagoDiff.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違1.ToString(), checkBoxKagoDiff.Checked && checkBoxKagoDiff1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違2.ToString(), checkBoxKagoDiff.Checked && checkBoxKagoDiff2.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違3.ToString(), checkBoxKagoDiff.Checked && checkBoxKagoDiff3.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違4.ToString(), checkBoxKagoDiff.Checked && checkBoxKagoDiff4.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.負傷原因相違5.ToString(), checkBoxKagoDiff.Checked && checkBoxKagoDiff5.Checked ? App.FlgValue.add : App.FlgValue.del);

                }

                if (checkBoxKagoLongDiff.Checked)
                {
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違.ToString(), checkBoxKagoLongDiff.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違1.ToString(), checkBoxKagoLongDiff.Checked && checkBoxKagoLongDiff1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違2.ToString(), checkBoxKagoLongDiff.Checked && checkBoxKagoLongDiff2.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違3.ToString(), checkBoxKagoLongDiff.Checked && checkBoxKagoLongDiff3.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違4.ToString(), checkBoxKagoLongDiff.Checked && checkBoxKagoLongDiff4.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.長期理由相違5.ToString(), checkBoxKagoLongDiff.Checked && checkBoxKagoLongDiff5.Checked ? App.FlgValue.add : App.FlgValue.del);
                }

                app.KagoReasons_xml(KagoReasons_Member.署名違い.ToString(), checkBoxKagoShomei.Checked ? App.FlgValue.add : App.FlgValue.del);
                app.KagoReasons_xml(KagoReasons_Member.筆跡違い.ToString(), checkBoxKagoHisseki.Checked ? App.FlgValue.add : App.FlgValue.del);
                app.KagoReasons_xml(KagoReasons_Member.家族同一筆跡.ToString(), checkBoxKagoFamily.Checked ? App.FlgValue.add : App.FlgValue.del);
                app.KagoReasons_xml(KagoReasons_Member.その他.ToString(), checkBoxKagoOther.Checked ? App.FlgValue.add : App.FlgValue.del);

                app.KagoReasons_xml(KagoReasons_Member.療養費請求権の消滅時効.ToString(), checkBoxKagoShometsu.Checked ? App.FlgValue.add : App.FlgValue.del);
                app.KagoReasons_xml(KagoReasons_Member.本家区分誤り.ToString(), checkBoxKagoHonke.Checked ? App.FlgValue.add : App.FlgValue.del);
                app.KagoReasons_xml(KagoReasons_Member.保険者相違.ToString(), checkBoxKagoInsSoui.Checked ? App.FlgValue.add : App.FlgValue.del);

                app.KagoReasons_xml(KagoReasons_Member.往療理由記載なし.ToString(), checkBoxKagoOuryoNoReason.Checked ? App.FlgValue.add : App.FlgValue.del);
                if (checkBoxKagoOuryoNoReason.Checked)
                {                    
                    app.KagoReasons_xml(KagoReasons_Member.往療16km以上.ToString(), checkBoxKagoOuryoOver16km.Checked ? App.FlgValue.add : App.FlgValue.del);
                }

                if (checkBoxKagoDoui.Checked)
                {

                    app.KagoReasons_xml(KagoReasons_Member.同意書.ToString(), checkBoxKagoDoui.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.同意書添付なし.ToString(), checkBoxKagoDouiNone.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.同意書期限切れ.ToString(), checkBoxKagoDouiMis.Checked ? App.FlgValue.add : App.FlgValue.del);

                }

                if (checkBoxKagoHoukoku.Checked)
                {

                    app.KagoReasons_xml(KagoReasons_Member.施術報告書.ToString(), checkBoxKagoHoukoku.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.施術報告書添付なし.ToString(), checkBoxKagoHoukokuNone.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.施術報告書記載不備.ToString(), checkBoxKagoHoukokuMis.Checked ? App.FlgValue.add : App.FlgValue.del);
                }

                if (checkBoxKagoJotai.Checked)
                {
                    app.KagoReasons_xml(KagoReasons_Member.施術継続理由状態記入書.ToString(), checkBoxKagoJotai.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.施術継続理由状態記入書添付なし.ToString(), checkBoxKagoJotaiNone.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.施術継続理由状態記入書記載不備.ToString(), checkBoxKagoJotaiMis.Checked ? App.FlgValue.add : App.FlgValue.del);
                }

                //20210201154055 furukawa st ////////////////////////
                //往療内訳書追加
                
                if (checkBoxKagoOryoUchiwake.Checked)
                {
                    app.KagoReasons_xml(KagoReasons_Member.往療内訳書.ToString(), checkBoxKagoOryoUchiwake.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.往療内訳書添付なし.ToString(), checkBoxKagoOryoUchiwakeNone.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.往療内訳書記載不備.ToString(), checkBoxKagoOryoUchiwakeMis.Checked ? App.FlgValue.add : App.FlgValue.del);
                }
                //20210201154055 furukawa ed ////////////////////////

                if (checkBoxKagoDokuzi.Checked)
                {
                    app.KagoReasons_xml(KagoReasons_Member.独自.ToString(), checkBoxKagoDokuzi.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.独自1.ToString(), checkBoxKagoDokuzi.Checked && checkBoxKagoDokuzi1.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.独自2.ToString(), checkBoxKagoDokuzi.Checked && checkBoxKagoDokuzi2.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.独自3.ToString(), checkBoxKagoDokuzi.Checked && checkBoxKagoDokuzi3.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.独自4.ToString(), checkBoxKagoDokuzi.Checked && checkBoxKagoDokuzi4.Checked ? App.FlgValue.add : App.FlgValue.del);
                    app.KagoReasons_xml(KagoReasons_Member.独自5.ToString(), checkBoxKagoDokuzi.Checked && checkBoxKagoDokuzi5.Checked ? App.FlgValue.add : App.FlgValue.del);
                }


                #region 旧コード


                //if (checkBoxKagoShomei.Checked) app.KagoReasons_xml(KagoReasons.署名違い.ToString(), App.FlgValue.add);
                //if (checkBoxKagoHisseki.Checked) app.KagoReasons_xml(KagoReasons.筆跡違い.ToString(), App.FlgValue.add);
                //if (checkBoxKagoFamily.Checked) app.KagoReasons_xml(KagoReasons.家族同一筆跡.ToString(), App.FlgValue.add);
                //if (checkBoxKagoOther.Checked) app.KagoReasons_xml(KagoReasons.その他.ToString(), App.FlgValue.add);



                ////新点検フラグ
                //if (checkBoxKagoGenin.Checked)
                //{
                //    app.KagoReasonsSet(KagoReasons.原因なし);
                //    if (checkBoxKagoGen1.Checked) app.KagoReasonsSet(KagoReasons.原因なし1);
                //    if (checkBoxKagoGen2.Checked) app.KagoReasonsSet(KagoReasons.原因なし2);
                //    if (checkBoxKagoGen3.Checked) app.KagoReasonsSet(KagoReasons.原因なし3);
                //    if (checkBoxKagoGen4.Checked) app.KagoReasonsSet(KagoReasons.原因なし4);
                //    if (checkBoxKagoGen5.Checked) app.KagoReasonsSet(KagoReasons.原因なし5);
                //}
                //if (checkBoxKagoLong.Checked)
                //{
                //    app.KagoReasonsSet(KagoReasons.長期理由なし);
                //    if (checkBoxKagoLong1.Checked) app.KagoReasonsSet(KagoReasons.長期理由なし1);
                //    if (checkBoxKagoLong2.Checked) app.KagoReasonsSet(KagoReasons.長期理由なし2);
                //    if (checkBoxKagoLong3.Checked) app.KagoReasonsSet(KagoReasons.長期理由なし3);
                //    if (checkBoxKagoLong4.Checked) app.KagoReasonsSet(KagoReasons.長期理由なし4);
                //    if (checkBoxKagoLong5.Checked) app.KagoReasonsSet(KagoReasons.長期理由なし5);
                //}
                //if (checkBoxKagoDiff.Checked)
                //{
                //    app.KagoReasonsSet(KagoReasons.負傷原因相違);
                //    if (checkBoxKagoDiff1.Checked) app.KagoReasonsSet(KagoReasons.負傷原因相違1);
                //    if (checkBoxKagoDiff2.Checked) app.KagoReasonsSet(KagoReasons.負傷原因相違2);
                //    if (checkBoxKagoDiff3.Checked) app.KagoReasonsSet(KagoReasons.負傷原因相違3);
                //    if (checkBoxKagoDiff4.Checked) app.KagoReasonsSet(KagoReasons.負傷原因相違4);
                //    if (checkBoxKagoDiff5.Checked) app.KagoReasonsSet(KagoReasons.負傷原因相違5);
                //}
                //if (checkBoxKagoShomei.Checked) app.KagoReasonsSet(KagoReasons.署名違い);
                //if (checkBoxKagoHisseki.Checked) app.KagoReasonsSet(KagoReasons.筆跡違い);
                //if (checkBoxKagoFamily.Checked) app.KagoReasonsSet(KagoReasons.家族同一筆跡);
                //if (checkBoxKagoOther.Checked) app.KagoReasonsSet(KagoReasons.その他);
                #endregion



                if (radioButtonSai.Checked)
                {
                    if (radioButtonGigiShoken.Checked)
                    {
                        //新点検フラグ
                        app.SaishinsaReasonsSet(SaishinsaReasons.初検料);
                        if (checkBoxF1.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.負傷1);
                        if (checkBoxF2.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.負傷2);
                        if (checkBoxF3.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.負傷3);
                        if (checkBoxF4.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.負傷4);
                        if (checkBoxF5.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.負傷5);
                        if (radioButtonGigiTenki.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.転帰なし);
                        if (radioButtonGigiChushi.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.中止);
                        if (radioButtonGigiDoitsu.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.同一負傷名);
                        if (radioButtonGigiBetsu.Checked) app.SaishinsaReasonsSet(SaishinsaReasons.別負傷名);
                    }
                    if (radioButtonSaiOther.Checked)
                    {
                        //新点検フラグ
                        app.SaishinsaReasonsSet(SaishinsaReasons.その他);
                    }
                }
            }
            else
            {
                app.StatusFlagRemove(StatusFlag.点検対象 | StatusFlag.点検済);
            }

            //往療フラグ
            if (checkBoxOryo.Checked)
            {
                app.StatusFlagSet(StatusFlag.往療点検対象);
                if (radioButtonOryoYes.Checked || radioButtonOryoNo.Checked)
                    app.StatusFlagSet(StatusFlag.往療点検済);

                if (radioButtonOryoYes.Checked) app.StatusFlagSet(StatusFlag.往療疑義);
                else app.StatusFlagRemove(StatusFlag.往療疑義);
            }
            else
            {
                app.StatusFlagRemove(StatusFlag.往療点検対象 | StatusFlag.往療点検済 | StatusFlag.往療疑義);
            }


            //返戻/架電フラグ
            if (checkBoxHenrei.Checked || checkBoxTel.Checked)
            {
                if (checkBoxHenBui.Checked)
                {
                    app.HenreiReasonsSet(HenreiReasons.負傷部位);
                    if (checkBoxHenBui1.Checked) app.HenreiReasonsSet(HenreiReasons.負傷部位1);
                    if (checkBoxHenBui2.Checked) app.HenreiReasonsSet(HenreiReasons.負傷部位2);
                    if (checkBoxHenBui3.Checked) app.HenreiReasonsSet(HenreiReasons.負傷部位3);
                    if (checkBoxHenBui4.Checked) app.HenreiReasonsSet(HenreiReasons.負傷部位4);
                    if (checkBoxHenBui5.Checked) app.HenreiReasonsSet(HenreiReasons.負傷部位5);
                }
                if (checkBoxHenReason.Checked)
                {
                    app.HenreiReasonsSet(HenreiReasons.負傷原因);
                    if (checkBoxHenReason1.Checked) app.HenreiReasonsSet(HenreiReasons.負傷原因1);
                    if (checkBoxHenReason2.Checked) app.HenreiReasonsSet(HenreiReasons.負傷原因2);
                    if (checkBoxHenReason3.Checked) app.HenreiReasonsSet(HenreiReasons.負傷原因3);
                    if (checkBoxHenReason4.Checked) app.HenreiReasonsSet(HenreiReasons.負傷原因4);
                    if (checkBoxHenReason5.Checked) app.HenreiReasonsSet(HenreiReasons.負傷原因5);
                }
                if (checkBoxHenJiki.Checked) app.HenreiReasonsSet(HenreiReasons.負傷時期);
                if (checkBoxHenKago.Checked) app.HenreiReasonsSet(HenreiReasons.過誤);
                if (checkBoxHenKega.Checked) app.HenreiReasonsSet(HenreiReasons.けが外);
                if (checkBoxHenDays.Checked) app.HenreiReasonsSet(HenreiReasons.受療日数);
                if (checkBoxHenWork.Checked) app.HenreiReasonsSet(HenreiReasons.勤務中);
                if (checkBoxHenOther.Checked) app.HenreiReasonsSet(HenreiReasons.その他);

                //連絡記録
                if (textBoxContactY.Text.Trim().Length > 0 ||
                    textBoxContactM.Text.Trim().Length > 0 ||
                    textBoxContactD.Text.Trim().Length > 0 ||
                    textBoxContactH.Text.Trim().Length > 0 ||
                    textBoxContactMin.Text.Trim().Length > 0)
                {
                    int.TryParse(textBoxContactY.Text, out int Y);
                    int.TryParse(textBoxContactM.Text, out int M);
                    int.TryParse(textBoxContactD.Text, out int D);
                    int.TryParse(textBoxContactH.Text, out int H);
                    int.TryParse(textBoxContactMin.Text, out int Min);
                    app.TaggedDatas.ContactDt = new DateTime(Y, M, D, H, Min, 0);
                }
                else
                {
                    app.TaggedDatas.ContactDt = DateTimeEx.DateTimeNull;
                }
                if (checkBoxContactNo.Checked) app.TaggedDatas.ContactStatusSet(ContactStatus.連絡不要);
                if (checkBoxContactOrg.Checked) app.TaggedDatas.ContactStatusSet(ContactStatus.団体);
                app.TaggedDatas.ContactName = textBoxContactName.Text.Trim();
            }
            else
            {
                app.TaggedDatas.ContactDt = DateTimeEx.DateTimeNull;
                if (checkBoxContactNo.Checked) app.TaggedDatas.ContactStatus = ContactStatus.Null;
                app.TaggedDatas.ContactName = string.Empty;
            }

            //返戻
            if (checkBoxHenrei.Checked)
                app.StatusFlagSet(StatusFlag.返戻);
            else
                app.StatusFlagRemove(StatusFlag.返戻);

            //家電
            if (checkBoxTel.Checked)
                app.StatusFlagSet(StatusFlag.架電対象);
            else
                app.StatusFlagRemove(StatusFlag.架電対象);

            //支払保留
            if (checkBoxPend.Checked)
                app.StatusFlagSet(StatusFlag.支払保留);
            else
                app.StatusFlagRemove(StatusFlag.支払保留);

            //入力エラー
            if(checkBoxInputError.Checked)
                app.StatusFlagSet(StatusFlag.入力時エラー);
            else
                app.StatusFlagRemove(StatusFlag.入力時エラー);

            //支払済み
            if(checkBoxPaid.Checked)
                app.StatusFlagSet(StatusFlag.支払済);
            else
                app.StatusFlagRemove(StatusFlag.支払済);

            //処理1
            if (checkBoxProcess1.Checked)
                app.StatusFlagSet(StatusFlag.処理1);
            else
                app.StatusFlagRemove(StatusFlag.処理1);

            //処理2
            if (checkBoxProcess2.Checked)
                app.StatusFlagSet(StatusFlag.処理2);
            else
                app.StatusFlagRemove(StatusFlag.処理2);

            //処理3
            if (checkBoxProcess3.Checked)
                app.StatusFlagSet(StatusFlag.処理3);
            else
                app.StatusFlagRemove(StatusFlag.処理3);

            //処理4
            if (checkBoxProcess4.Checked)
                app.StatusFlagSet(StatusFlag.処理4);
            else
                app.StatusFlagRemove(StatusFlag.処理4);

            //処理5
            //if (checkBoxProcess5.Checked)
            //    app.StatusFlagSet(StatusFlag.処理5);
            //else
            //    app.StatusFlagRemove(StatusFlag.処理5);
            
            //メモ
            app.Memo = textBoxMemo.Text.Trim();
            app.OutMemo = textBoxOutMemo.Text.Trim();

            //参考AID
            app.TaggedDatas.RelationAID = RelationAid;
        }

        /// <summary>
        /// 点検結果として何も指定されていない場合、申出なしにチェックを入れます
        /// </summary>
        public void SetToNoneWhenNull()
        {
            if (radioButtonPend.Checked || radioButtonKago.Checked || radioButtonSai.Checked)
                return;

            radioButtonNone.Checked = true;
        }


        #region オブジェクトイベント
        private void checkBoxShokai_CheckedChanged(object sender, EventArgs e)
        {
            panelShokai.Enabled = checkBoxShokai.Checked;
            checkBox_CheckedChanged(checkBoxShokai);
        }

        private void checkBoxTenken_CheckedChanged(object sender, EventArgs e)
        {
            panelTenken.Enabled = checkBoxTenken.Checked;
            groupBoxKago.Enabled = radioButtonKago.Checked;
            groupBoxSaishinsa.Enabled = radioButtonSai.Checked;
            checkBox_CheckedChanged(checkBoxTenken);
        }

        private void checkBoxOryoCheck_CheckedChanged(object sender, EventArgs e)
        {
            panelOryo.Enabled = checkBoxOryo.Checked;
            checkBox_CheckedChanged(checkBoxOryo);
        }

        private void checkBoxHenrei_CheckedChanged(object sender, EventArgs e)
        {
            panelHenrei.Enabled = checkBoxHenrei.Checked || checkBoxTel.Checked;
            panelContact.Enabled = checkBoxHenrei.Checked || checkBoxTel.Checked;
            checkBox_CheckedChanged(checkBoxHenrei);
        }

        private void checkBoxPend_CheckedChanged(object sender, EventArgs e)
        {
            checkBox_CheckedChanged(checkBoxPend);
        }

        private void checkBox_CheckedChanged(CheckBox cb)
        {
            cb.BackColor = cb.Checked ? Color.LightPink : Color.Transparent;
        }

        private void radioButtonGigiShoken_CheckedChanged(object sender, EventArgs e)
        {
            panelShokenGigi.Enabled = radioButtonGigiShoken.Checked;
            panelShokenGigi2.Enabled = radioButtonGigiShoken.Checked;
        }

        private void checkBoxInputError_CheckedChanged(object sender, EventArgs e)
        {
            checkBox_CheckedChanged(checkBoxInputError);
        }

        private void checkBoxTel_CheckedChanged(object sender, EventArgs e)
        {
            panelHenrei.Enabled = checkBoxHenrei.Checked || checkBoxTel.Checked;
            panelContact.Enabled = checkBoxHenrei.Checked || checkBoxTel.Checked;
            checkBox_CheckedChanged(checkBoxTel);
        }

        private void checkBoxPaid_CheckedChanged(object sender, EventArgs e)
        {
            checkBox_CheckedChanged(checkBoxPaid);
        }

        private void checkBoxProcess_CheckedChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            cb.BackColor = cb.Checked ? Color.LightPink : Color.Transparent;
        }

        private void checkBoxKagoGenin_CheckedChanged(object sender, EventArgs e)
        {
            var b = checkBoxKagoGenin.Checked;
            panelKagoGenin.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoGen1.Enabled = b;
            checkBoxKagoGen2.Enabled = b;
            checkBoxKagoGen3.Enabled = b;
            checkBoxKagoGen4.Enabled = b;
            checkBoxKagoGen5.Enabled = b;
        }

        private void checkBoxKagoLong_CheckedChanged(object sender, EventArgs e)
        {
            var b = checkBoxKagoLong.Checked;
            panelKagoLong.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoLong1.Enabled = b;
            checkBoxKagoLong2.Enabled = b;
            checkBoxKagoLong3.Enabled = b;
            checkBoxKagoLong4.Enabled = b;
            checkBoxKagoLong5.Enabled = b;
        }

        private void checkBoxKagoDiff_CheckedChanged(object sender, EventArgs e)
        {
            var b = checkBoxKagoDiff.Checked;
            panelKagoDiff.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoDiff1.Enabled = b;
            checkBoxKagoDiff2.Enabled = b;
            checkBoxKagoDiff3.Enabled = b;
            checkBoxKagoDiff4.Enabled = b;
            checkBoxKagoDiff5.Enabled = b;
        }

        private void checkBoxHenBui_CheckedChanged(object sender, EventArgs e)
        {
            var b = checkBoxHenBui.Checked;
            panelHenBui.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxHenBui1.Enabled = b;
            checkBoxHenBui2.Enabled = b;
            checkBoxHenBui3.Enabled = b;
            checkBoxHenBui4.Enabled = b;
            checkBoxHenBui5.Enabled = b;
        }

        private void checkBoxHenReason_CheckedChanged(object sender, EventArgs e)
        {
            var b = checkBoxHenReason.Checked;
            panelHenReason.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxHenReason1.Enabled = b;
            checkBoxHenReason2.Enabled = b;
            checkBoxHenReason3.Enabled = b;
            checkBoxHenReason4.Enabled = b;
            checkBoxHenReason5.Enabled = b;
        }

        private void buttonRelationAppShow_Click(object sender, EventArgs e)
        {
            using (var f = new FamilyImageForm(currentApp, ParentForm,  true))
            {
                f.RelationAID = RelationAid;
                f.ShowDialog();
                if (RelationAid != f.RelationAID) Changed = true;
                RelationAid = f.RelationAID;
            }
        }

        
        /// <summary>
        ///20201005160550 furukawa 独自チェックを入れて後ろの項目を使用可能にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxKagoDokuzi_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoDokuzi.Checked;
            panelKagoDokuzi.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoDokuzi1.Enabled = b;
            checkBoxKagoDokuzi2.Enabled = b;
            checkBoxKagoDokuzi3.Enabled = b;
            checkBoxKagoDokuzi4.Enabled = b;
            checkBoxKagoDokuzi5.Enabled = b;
        }

        
        /// <summary>
        /// 20201005160710 furukawa 長期理由チェックを入れて後ろの項目を使用可能にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxKagoLongDiff_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoLongDiff.Checked;
            panelKagoLongDiff.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoLongDiff1.Enabled = b;
            checkBoxKagoLongDiff2.Enabled = b;
            checkBoxKagoLongDiff3.Enabled = b;
            checkBoxKagoLongDiff4.Enabled = b;
            checkBoxKagoLongDiff5.Enabled = b;
        }
     

        private void checkBoxKagoJotai_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoJotai.Checked;
            panelKagoJotai.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoJotaiNone.Enabled = b;
            checkBoxKagoJotaiMis.Enabled = b;
            
        }

        private void checkBoxKagoHoukoku_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoHoukoku.Checked;
            panelKagoHoukoku.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoHoukokuNone.Enabled = b;
            checkBoxKagoHoukokuMis.Enabled = b;            
        }

        private void checkBoxKagoDoui_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoDoui.Checked;
            panelKagoDoui.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoDouiNone.Enabled = b;
            checkBoxKagoDouiMis.Enabled = b;            
        }

        private void checkBoxKagoOuryoNoReason_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoOuryoNoReason.Checked;
            panelKagoOuryo.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoOuryoOver16km.Enabled = b;            
        }


    
        /// <summary>
        /// 20210201154857 furukawa 往療内訳書チェックを入れて後ろの項目を使用可能にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxKagoOryoUchiwake_CheckedChanged(object sender, EventArgs e)
        {
            bool b = checkBoxKagoOryoUchiwake.Checked;
            panelKagoOryoUchiwake.BackColor = b ? Color.LightCyan : Color.Transparent;
            checkBoxKagoOryoUchiwakeNone.Enabled = b;
            checkBoxKagoOryoUchiwakeMis.Enabled = b;
        }

        #endregion

    }
}
