﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Inspect
{
    [DB.DbAttribute.DifferentTableName("application")]
    class AppConverter
    {
        [Flags]
        public enum INSPECT_REASON
        {
            過誤署名違い = 0x01, 過誤筆跡違い = 0x02, 過誤家族同一筆跡 = 0x04, 過誤原因なし = 0x08,
            過誤署名なし = 0x10, 過誤長期理由なし = 0x20, 過誤その他 = 0x40, 過誤負傷原因相違 = 0x400,

            再審初検料 = 0x100, 再審その他 = 0x200,

            再審負傷1 = 0x1000, 再審負傷2 = 0x2000, 再審負傷3 = 0x4000,
            再審負傷4 = 0x8000, 再審負傷5 = 0x1_0000,

            再審転帰なし = 0x10_0000, 再審中止 = 0x20_0000,
            再審同一負傷名 = 0x40_0000, 再審別負傷名 = 0x80_0000,

            返戻部位 = 0x100_0000, 返戻原因 = 0x200_0000, 返戻時期 = 0x400_0000,
            返戻けが外 = 0x800_0000, 返戻日数 = 0x1000_0000, 返戻勤務中 = 0x2000_0000,
            返戻その他 = 0x4000_0000,
            返戻過誤 = 0x80,
        }



        [DB.DbAttribute.PrimaryKey]
        public int AID { get; private set; }
        

        [DB.DbAttribute.UpdateIgnore]
        public DateTime ContactDt { get; set; }
        [DB.DbAttribute.UpdateIgnore]
        public string ContactName { get; set; }
        [DB.DbAttribute.UpdateIgnore]
        public ContactStatus ContactStatus { get; set; }

        public string TaggedDatas { get; private set; }


        public bool Update(DB.Transaction tran) => DB.Main.Update(this, tran);


        public static bool Convert()
        {
            int changeCount = 0;

            using (var tran = DB.Main.CreateTransaction())
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("変換対象のデータを読み込んでいます");
                var l = DB.Main.SelectAll<AppConverter>();

                wf.LogPrint("データを更新しています");
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(l.Count());

                foreach (var item in l)
                {
                    wf.InvokeValue++;

                    if (item.ContactDt.IsNullDate() &&
                        item.ContactName == string.Empty &&
                        item.ContactStatus == ContactStatus.Null)
                        continue;

                    var t = new TaggedDatas(item.TaggedDatas);
                    t.ContactDt = item.ContactDt;
                    t.ContactName = item.ContactName;
                    t.ContactStatus = item.ContactStatus;
                    item.TaggedDatas = t.CreateDbStr();

                    changeCount++;

                    try
                    {
                        item.Update(tran);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        return false;
                    }
                }
                System.Windows.Forms.MessageBox.Show($"change count:{changeCount}");
                tran.Commit();
            }
            return true;
        }
    }
}
