﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.Inspect
{
    public class GroupInspectStatus
    {
        public int GroupID { get; private set; }
        public APP_TYPE AppType { get; private set; }
        public string Note1 { get; private set; }
        public string Note2 { get; private set; }
        public string UserName { get; private set; }
        
        public int ShokaiCount => list.Where(a => a.FlagCheck(StatusFlag.照会対象)).Select(a => a.Count).Sum();
        public string TenkenStatus => TenkenCount == 0 ? "点検なし" :
            TenkenFinishCount == 0 ? "未点検" :
            TenkenCount == TenkenFinishCount ? "点検済" : "点検中";
        public int TenkenCount => list.Where(a => a.FlagCheck(StatusFlag.点検対象)).Select(a => a.Count).Sum();
        public int TenkenFinishCount => list.Where(a => a.FlagCheck(StatusFlag.点検済)).Select(a => a.Count).Sum();
        public int KagoCount => list.Where(a => a.FlagCheck(StatusFlag.過誤)).Select(a => a.Count).Sum();
        public int SaishinCount => list.Where(a => a.FlagCheck(StatusFlag.再審査)).Select(a => a.Count).Sum();
        public int HoryuCount => list.Where(a => a.FlagCheck(StatusFlag.保留)).Select(a => a.Count).Sum();
        public string OryoStatus => OryoCount == 0 ? "点検なし" :
            OryoFinishCount == 0 ? "未点検" :
            OryoCount == OryoFinishCount ? "点検済" : "点検中";
        public int OryoCount => list.Where(a => a.FlagCheck(StatusFlag.往療点検対象)).Select(a => a.Count).Sum();
        public int OryoFinishCount => list.Where(a => a.FlagCheck(StatusFlag.往療点検済)).Select(a => a.Count).Sum();
        public int OryoGigiCount => list.Where(a => a.FlagCheck(StatusFlag.往療疑義)).Select(a => a.Count).Sum();
        public int HenreiCount => list.Where(a => a.FlagCheck(StatusFlag.返戻)).Select(a => a.Count).Sum();

        private List<StatusOnlyApp> list = new List<StatusOnlyApp>();

        [DB.DbAttribute.DifferentTableName("application")]
        class StatusOnlyApp
        {
            public int GroupID { get; private set; }
            public StatusFlag StatusFlags { get; set; }
            public int Count { get; private set; }
            public bool FlagCheck(StatusFlag flag) => (StatusFlags & flag) == flag;

            public static IEnumerable<StatusOnlyApp> GetList(int cym)
            {
                var sql = "SELECT groupid, statusflags, COUNT(statusflags) " +
                    "FROM application WHERE cym=:cym " +
                    "GROUP BY groupid, statusflags;";

                var l = new List<StatusOnlyApp>();
                using (var cmd = DB.Main.CreateCmd(sql))
                {
                    cmd.Parameters.Add("cym", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                    var res  = cmd.TryExecuteReaderList();
                    if (res == null) return l;
                    foreach (var item in res)
                    {
                        var sa = new StatusOnlyApp();
                        sa.GroupID = (int)item[0];
                        sa.StatusFlags = (StatusFlag)(int)item[1];
                        sa.Count = (int)(long)item[2];
                        l.Add(sa);
                    }
                    return l;
                }
            }
        }

        public static List<GroupInspectStatus>GetList(int cym)
        {
            var dic = new Dictionary<int, GroupInspectStatus>();
            var l = StatusOnlyApp.GetList(cym);

            foreach (var item in l)
            {
                if (!dic.ContainsKey(item.GroupID))
                {
                    dic.Add(item.GroupID, new GroupInspectStatus());
                    dic[item.GroupID].GroupID = item.GroupID;
                }
               dic[item.GroupID].list.Add(item);
            }

            var sql = "SELECT g.groupid, s.apptype, s.note1, s.note2, g.inquiryuser " +
                "FROM scangroup as g, scan as s " +
                "WHERE g.scanid=s.sid AND s.cym=:cym;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("cym", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                var res = cmd.TryExecuteReaderList();

                foreach (var item in res)
                {
                    int gid = (int)item[0];
                    if (!dic.ContainsKey(gid)) continue;

                    dic[gid].AppType = (APP_TYPE)(int)item[1];
                    dic[gid].Note1 = (string)item[2];
                    dic[gid].Note2 = (string)item[3];
                    dic[gid].UserName = User.GetUserName((int)item[4]);
                }
            }

            var rl = dic.Values.ToList();
            rl.Sort((x, y) => x.GroupID.CompareTo(y.GroupID));
            return rl;
        }

    }
}
