﻿namespace Mejor.Inspect
{
    partial class InspectFormAHK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrint2 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.プリンター選択ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.checkBoxRelation = new Mejor.BorderCheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panelSubCursor = new System.Windows.Forms.Panel();
            this.buttonSubPrev = new System.Windows.Forms.Button();
            this.buttonSubNext = new System.Windows.Forms.Button();
            this.labelSubCnt = new System.Windows.Forms.Label();
            this.buttonImageFillSub = new System.Windows.Forms.Button();
            this.userControlImageSub = new UserControlImage();
            this.panelCenter = new System.Windows.Forms.Panel();
            this.buttonNextImage = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImageMain = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.inspectControl1 = new Mejor.Inspect.InspectControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonExit = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelSubCursor.SuspendLayout();
            this.panelCenter.SuspendLayout();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.Location = new System.Drawing.Point(3, 4);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(334, 165);
            this.dgv.TabIndex = 1;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(3, 1);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 25);
            this.buttonBack.TabIndex = 0;
            this.buttonBack.Text = "PgDn << 前";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(84, 1);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 25);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = "次 >> PgUp";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrint2
            // 
            this.buttonPrint2.ContextMenuStrip = this.contextMenuStrip1;
            this.buttonPrint2.Location = new System.Drawing.Point(196, 1);
            this.buttonPrint2.Name = "buttonPrint2";
            this.buttonPrint2.Size = new System.Drawing.Size(75, 25);
            this.buttonPrint2.TabIndex = 7;
            this.buttonPrint2.Text = "印刷";
            this.buttonPrint2.UseVisualStyleBackColor = true;
            this.buttonPrint2.Click += new System.EventHandler(this.buttonPrint2_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.プリンター選択ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 26);
            // 
            // プリンター選択ToolStripMenuItem
            // 
            this.プリンター選択ToolStripMenuItem.Name = "プリンター選択ToolStripMenuItem";
            this.プリンター選択ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.プリンター選択ToolStripMenuItem.Text = "プリンター選択";
            this.プリンター選択ToolStripMenuItem.Click += new System.EventHandler(this.プリンター選択ToolStripMenuItem_Click);
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.checkBoxRelation);
            this.panelLeft.Controls.Add(this.button1);
            this.panelLeft.Controls.Add(this.panelSubCursor);
            this.panelLeft.Controls.Add(this.labelSubCnt);
            this.panelLeft.Controls.Add(this.buttonImageFillSub);
            this.panelLeft.Controls.Add(this.userControlImageSub);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(494, 824);
            this.panelLeft.TabIndex = 35;
            // 
            // checkBoxRelation
            // 
            this.checkBoxRelation.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxRelation.Location = new System.Drawing.Point(81, 2);
            this.checkBoxRelation.Name = "checkBoxRelation";
            this.checkBoxRelation.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxRelation.Size = new System.Drawing.Size(87, 27);
            this.checkBoxRelation.TabIndex = 75;
            this.checkBoxRelation.Text = "参考指定";
            this.checkBoxRelation.UseVisualStyleBackColor = false;
            this.checkBoxRelation.Visible = false;
            this.checkBoxRelation.CheckedChanged += new System.EventHandler(this.checkBoxRelation_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(430, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 25);
            this.button1.TabIndex = 74;
            this.button1.Text = "次ID画像";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelSubCursor
            // 
            this.panelSubCursor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSubCursor.Controls.Add(this.buttonSubPrev);
            this.panelSubCursor.Controls.Add(this.buttonSubNext);
            this.panelSubCursor.Location = new System.Drawing.Point(371, 3);
            this.panelSubCursor.Name = "panelSubCursor";
            this.panelSubCursor.Size = new System.Drawing.Size(58, 25);
            this.panelSubCursor.TabIndex = 73;
            // 
            // buttonSubPrev
            // 
            this.buttonSubPrev.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSubPrev.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonSubPrev.Location = new System.Drawing.Point(0, 0);
            this.buttonSubPrev.Name = "buttonSubPrev";
            this.buttonSubPrev.Size = new System.Drawing.Size(30, 25);
            this.buttonSubPrev.TabIndex = 70;
            this.buttonSubPrev.Text = "←";
            this.buttonSubPrev.UseVisualStyleBackColor = true;
            this.buttonSubPrev.Click += new System.EventHandler(this.buttonSubPrev_Click);
            // 
            // buttonSubNext
            // 
            this.buttonSubNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSubNext.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonSubNext.Location = new System.Drawing.Point(28, 0);
            this.buttonSubNext.Name = "buttonSubNext";
            this.buttonSubNext.Size = new System.Drawing.Size(30, 25);
            this.buttonSubNext.TabIndex = 71;
            this.buttonSubNext.Text = "→";
            this.buttonSubNext.UseVisualStyleBackColor = true;
            this.buttonSubNext.Click += new System.EventHandler(this.buttonSubNext_Click);
            // 
            // labelSubCnt
            // 
            this.labelSubCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubCnt.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelSubCnt.Location = new System.Drawing.Point(313, 4);
            this.labelSubCnt.Name = "labelSubCnt";
            this.labelSubCnt.Size = new System.Drawing.Size(57, 23);
            this.labelSubCnt.TabIndex = 72;
            this.labelSubCnt.Text = "(0/0)";
            this.labelSubCnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSubCnt.Visible = false;
            // 
            // buttonImageFillSub
            // 
            this.buttonImageFillSub.Location = new System.Drawing.Point(2, 3);
            this.buttonImageFillSub.Name = "buttonImageFillSub";
            this.buttonImageFillSub.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFillSub.TabIndex = 69;
            this.buttonImageFillSub.Text = "全体表示";
            this.buttonImageFillSub.UseVisualStyleBackColor = true;
            this.buttonImageFillSub.Click += new System.EventHandler(this.buttonImageFillSub_Click);
            // 
            // userControlImageSub
            // 
            this.userControlImageSub.AutoScroll = true;
            this.userControlImageSub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImageSub.Location = new System.Drawing.Point(0, 0);
            this.userControlImageSub.Name = "userControlImageSub";
            this.userControlImageSub.Size = new System.Drawing.Size(494, 824);
            this.userControlImageSub.TabIndex = 0;
            // 
            // panelCenter
            // 
            this.panelCenter.Controls.Add(this.buttonNextImage);
            this.panelCenter.Controls.Add(this.buttonImageFill);
            this.panelCenter.Controls.Add(this.userControlImageMain);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCenter.Location = new System.Drawing.Point(497, 0);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(505, 824);
            this.panelCenter.TabIndex = 67;
            // 
            // buttonNextImage
            // 
            this.buttonNextImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextImage.Location = new System.Drawing.Point(439, 3);
            this.buttonNextImage.Name = "buttonNextImage";
            this.buttonNextImage.Size = new System.Drawing.Size(64, 25);
            this.buttonNextImage.TabIndex = 66;
            this.buttonNextImage.Text = "次ID画像";
            this.buttonNextImage.UseVisualStyleBackColor = true;
            this.buttonNextImage.Visible = false;
            this.buttonNextImage.Click += new System.EventHandler(this.buttonNextImage_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Location = new System.Drawing.Point(2, 3);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 66;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImageMain
            // 
            this.userControlImageMain.AutoScroll = true;
            this.userControlImageMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImageMain.Location = new System.Drawing.Point(0, 0);
            this.userControlImageMain.Name = "userControlImageMain";
            this.userControlImageMain.Size = new System.Drawing.Size(505, 824);
            this.userControlImageMain.TabIndex = 67;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.splitContainer1);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(1002, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(342, 824);
            this.panelRight.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.dgv);
            this.splitContainer1.Panel1MinSize = 10;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.inspectControl1);
            this.splitContainer1.Size = new System.Drawing.Size(342, 798);
            this.splitContainer1.SplitterDistance = 172;
            this.splitContainer1.TabIndex = 10;
            // 
            // inspectControl1
            // 
            this.inspectControl1.Enabled = false;
            this.inspectControl1.Location = new System.Drawing.Point(3, 2);
            this.inspectControl1.MinimumSize = new System.Drawing.Size(280, 0);
            this.inspectControl1.Name = "inspectControl1";
            this.inspectControl1.RelationAid = 0;
            this.inspectControl1.RelationButtonVisible = false;
            this.inspectControl1.Size = new System.Drawing.Size(319, 900);
            this.inspectControl1.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonBack);
            this.panel1.Controls.Add(this.buttonPrint2);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 798);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 26);
            this.panel1.TabIndex = 9;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(277, 1);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(60, 25);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(494, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 824);
            this.splitter1.TabIndex = 68;
            this.splitter1.TabStop = false;
            // 
            // InspectFormAHK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 824);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelLeft);
            this.KeyPreview = true;
            this.Name = "InspectFormAHK";
            this.Text = "Inspect";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormInspect_Load);
            this.SizeChanged += new System.EventHandler(this.FormInspect_SizeChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormInspect_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            this.panelSubCursor.ResumeLayout(false);
            this.panelCenter.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private UserControlImage userControlImageSub;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Button buttonPrint2;
        private System.Windows.Forms.Button buttonImageFillSub;
        private System.Windows.Forms.Button buttonSubNext;
        private System.Windows.Forms.Button buttonSubPrev;
        private System.Windows.Forms.Label labelSubCnt;
        private System.Windows.Forms.Panel panelSubCursor;
        private UserControlImage userControlImageMain;
        private System.Windows.Forms.Panel panelCenter;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem プリンター選択ToolStripMenuItem;
        private Inspect.InspectControl inspectControl1;
        private System.Windows.Forms.Button buttonNextImage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private BorderCheckBox checkBoxRelation;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}