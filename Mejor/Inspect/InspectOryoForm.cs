﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Mejor.Inspect
{
    public partial class InspectOryoForm : Form
    {
        private int cym;
        //private int cm;
        private App rComp;
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsSub = new BindingSource();
        private bool isConfirmed = false;

        public InspectOryoForm(int cym, GroupInspectStatus g)
        {
            InitializeComponent();
            //labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100)}年度{cym % 100}月処理分";
            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            this.cym = cym;

            var l = App.GetAppsGID(g.GroupID);
            l = l.FindAll(a => a.StatusFlagCheck(StatusFlag.往療点検対象));
            bsApp.DataSource = l;
            dataGridView1.DataSource = bsApp;
            bsApp.CurrentChanged += BsApp_CurrentChanged;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            appRead();
        }

        private void subSearch(int sid, int aid)
        {
            var subList = App.GetAppsSID(sid);
            var resultList = new List<App>();
            rComp = null;

            if (subList == null) return;

            bool bFlg = false;
            foreach (App app in subList)
            {
                if (app.Aid == aid)
                {
                    bFlg = true;
                    continue;
                }
                if (app.Aid > aid && bFlg)
                {
                    bFlg = app.MediYear == -3;
                    if (bFlg)
                    {
                        if (rComp == null) rComp = app;
                        subImageRefresh();
                        resultList.Add(app);
                    }
                }
                else if (app.Aid > aid) break;
            }

            bsSub.DataSource = resultList;
            bsSub.ResetBindings(false);
            bsSub.MoveFirst();

            buttonSubNext.Enabled = false;
            buttonSubPrev.Enabled = false;
            if (bsSub.Count > 1)
            {
                if (bsSub.Position != 0) buttonSubPrev.Enabled = true;
                if (bsSub.Position != bsSub.Count - 1) buttonSubNext.Enabled = true;
            }
            labelSubCnt.Text = string.Format("({0}/{1})", (bsSub.Position + 1), bsSub.Count);
            if (bsSub.Count == 0)
            {
                labelSubImageName.Text = string.Empty;
            }
            else if (bsSub.Count > 0)
            {
                labelSubCnt.BackColor = Color.Aqua;
            }
        }

        private bool updateINQ()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (!radioButtonOryoGigiYes.Checked && !radioButtonOryoGigiNo.Checked)
            {
                MessageBox.Show("点検結果のどちらかにチェックを入れてください");
                return false;
            }

            //新フラグ管理
            app.StatusFlagSet(StatusFlag.往療点検済);
            if (isConfirmed && radioButtonOryoGigiYes.Checked) app.StatusFlagSet(StatusFlag.往療疑義);
            else if (isConfirmed && radioButtonOryoGigiNo.Checked) app.StatusFlagRemove(StatusFlag.往療疑義);
            app.TaggedDatas.DistCalcAdd = userControlOryo1.PatientCalcAdd;
            app.TaggedDatas.DistCalcClinicAdd = userControlOryo1.Clinic_OsakaKoikiCalcAdd;
            app.TaggedDatas.AppDistance = userControlOryo1.AppDistance_OsakaKoiki;
            app.TaggedDatas.CalcDistance = userControlOryo1.CalcDistance_OsakaKoiki;
            app.Uinquiry = User.CurrentUser.UserID;

            //20210205171948 furukawa st ////////////////////////
            //メモ、出力用メモ更新
            
            app.Memo = userControlOryo1.Memo;
            app.OutMemo = userControlOryo1.Memo;
            //20210205171948 furukawa ed ////////////////////////

            try
            {
                app.UpdateINQ();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "\r\n\r\n申請書データの更新に失敗しました");
                return false;
            }

            bsApp.ResetBindings(false);
            return true;
        }

        /// <summary>
        /// 次のApplicationに移ります（すべての操作）
        /// </summary>
        /// <returns></returns>
        protected bool appRead()
        {
            radioButtonOryoGigiYes.Checked = false;
            radioButtonOryoGigiNo.Checked = false;
            userControlImageSub.Clear();

            //aIdで一意のAppインスタンスを選択
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //次のApplicationを表示
            InspectCommon.displayImageRefresh(app, labelImageName, userControlImageMain, true);

            ////選択されたAppと同じ被保番のAppを検索、リストと画像を表示
            subSearch(app.ScanID, app.Aid);
            
            //往療関係
            userControlOryo1.ResetControl();
            userControlOryo1.SetApp(app);

            isConfirmed = false;

            if (app.StatusFlagCheck(StatusFlag.往療点検済))
            {
                if (app.StatusFlagCheck(StatusFlag.往療疑義))
                    radioButtonOryoGigiYes.Checked = true;
                else
                    radioButtonOryoGigiNo.Checked = true;
            }

            this.ActiveControl = userControlOryo1;
            return true;
        }

        /// <summary>
        /// サブ画像の表示
        /// </summary>
        private void subImageRefresh()
        {
            string fn = rComp.GetImageFullPath();

            try
            {
                userControlImageSub.SetPictureFile(fn);
                //userControlImageSub.SetPictureBoxFill();

                labelSubImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("サブ画像表示でエラーが発生しました");
                return;
            }
        }

        // --------------------------------------------------------------------------------
        //
        // 表示・操作部
        //
        // --------------------------------------------------------------------------------

        private void FormInspect_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bsApp;
            for (int j = 0; j < dataGridView1.ColumnCount; j++)
            {
                dataGridView1.Columns[j].Visible = false;
                dataGridView1.Columns[j].ReadOnly = true;
            }
            dataGridView1.Columns[nameof(App.Aid)].Visible = true;
            dataGridView1.Columns[nameof(App.Aid)].Width = 70;
            dataGridView1.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridView1.Columns[nameof(App.ScanID)].Visible = true;
            dataGridView1.Columns[nameof(App.ScanID)].Width = 50;
            dataGridView1.Columns[nameof(App.ScanID)].HeaderText = "ScanID";
            dataGridView1.Columns[nameof(App.GroupID)].Visible = true;
            dataGridView1.Columns[nameof(App.GroupID)].Width = 50;
            dataGridView1.Columns[nameof(App.GroupID)].HeaderText = "GroupID";
            dataGridView1.Columns[nameof(App.MediYear)].Visible = true;
            dataGridView1.Columns[nameof(App.MediYear)].Width = 25;
            dataGridView1.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridView1.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridView1.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridView1.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridView1.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridView1.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridView1.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(App.AppType)].Visible = true;
            dataGridView1.Columns[nameof(App.AppType)].Width = 25;
            dataGridView1.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridView1.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridView1.Columns[nameof(App.InputStatus)].Width = 60;
            dataGridView1.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridView1.Columns[nameof(App.InspectInfo)].Visible = true;
            dataGridView1.Columns[nameof(App.InspectInfo)].Width = 80;
            dataGridView1.Columns[nameof(App.InspectInfo)].HeaderText = "点検結果";
            dataGridView1.Columns[nameof(App.InspectInfo)].DisplayIndex = 3;
            dataGridView1.Columns[nameof(App.DrNum)].Visible = true;
            dataGridView1.Columns[nameof(App.DrNum)].Width = 70;
            dataGridView1.Columns[nameof(App.DrNum)].HeaderText = "施術所No";
            dataGridView1.Columns[nameof(App.Total)].Visible = true;
            dataGridView1.Columns[nameof(App.Total)].Width = 70;
            dataGridView1.Columns[nameof(App.Total)].HeaderText = "合計金額";

            //int all, kago, gigi, oryogigi, syokai, ad, adOryo;
            //App.InspectCount(cym, out all, out kago, out oryogigi, out gigi, out syokai, out ad, out adOryo);
            //textBoxAll.Text = all.ToString();
            //textBoxGigi.Text = oryogigi.ToString();
            //textBoxAD.Text = adOryo.ToString();

            if (dataGridView1.Rows.Count == 0) return;
            appRead();
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            isConfirmed = true;
            updateINQ();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCellAddress.Y == 0) return;
            dataGridView1.CurrentCell = dataGridView1[0, dataGridView1.CurrentCell.RowIndex - 1];
            bsApp.MovePrevious();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            
            //もし点検結果にチェックがされていればデータをUpdateしてから次を読み込む
          //  int ri = dataGridView1.CurrentCell.RowIndex;

            if (!updateINQ())
            {
                MessageBox.Show("データベースの更新に失敗しました");
                return;
            }

            bsApp.MoveNext();
            dataGridView1.ResumeLayout();
        }

        private void FormInspect_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                // デフォルトの動作をOFFにする
                e.Handled = true;

                //もし点検結果にチェックがされていればデータをUpdateしてから次を読み込む
                if (!updateINQ()) return;
                bsApp.MoveNext();
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                // デフォルトの動作をOFFにする
                e.Handled = true;

                bsApp.MovePrevious();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void buttonSubNext_Click(object sender, EventArgs e)
        {
            bsSub.MoveNext();
            rComp = (App)bsSub.Current;
            InspectCommon.displayImageRefresh(rComp, labelSubImageName, userControlImageSub, false);

            if (bsSub.Position == bsSub.Count - 1) buttonSubNext.Enabled = false;
            buttonSubPrev.Enabled = true;
            labelSubCnt.Text = "(" + (bsSub.Position + 1) + "/" + bsSub.Count + ")";
            if (bsSub.Count > 0)
            {
                labelSubCnt.BackColor = Color.Aqua;
            }
        }

        private void buttonSubPrev_Click(object sender, EventArgs e)
        {
            bsSub.MovePrevious();
            rComp = (App)bsSub.Current;
            InspectCommon.displayImageRefresh(rComp, labelSubImageName, userControlImageSub, false);
            if (bsSub.Position == 0) buttonSubPrev.Enabled = false;
            buttonSubNext.Enabled = true;
            labelSubCnt.Text = "(" + (bsSub.Position + 1) + "/" + bsSub.Count + ")";
            if (bsSub.Count > 0)
            {
                labelSubCnt.BackColor = Color.Aqua;
            }
        }

        private void FormInspect_SizeChanged(object sender, EventArgs e)
        {
            int H = this.Height - SystemInformation.CaptionHeight;
            int W = this.Width - SystemInformation.FrameBorderSize.Width * 2;
            int subW = (int)((float)(W - panelCenter.Width) / 2f);
            int mainW = W - subW - panelCenter.Width;

            panelLeft.Size = new Size(subW, H);
            userControlImageMain.Size = new Size(mainW, H);
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImageMain.SetPictureBoxFill();
        }

        private void buttonImageFillSub_Click(object sender, EventArgs e)
        {
            userControlImageSub.SetPictureBoxFill();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            appRead();
        }

        /// <summary>
        /// 資料印刷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrint2_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null) return;

            var pn = Settings.InspectPrinterName;
            if (pn == string.Empty) pn = Settings.DefaultPrinterName;

            var ip = new ImagePrint(pn);

            if (rComp == null)
            {
                ip.Print(app, true, false);
            }
            else if (app.Aid != rComp.Aid)
            {
                ip.Print(app, true, false, rComp);
            }
            else
            {
                ip.Print(app, true, false);
            }
        }

        //private string getFreeinputNote()
        //{
        //    var sinsei = userControlOryo1.SinseiKyori;
        //    var keisan = userControlOryo1.KeisanKyori;
        //    Kyori.KyoriKubun keisanKubun = Kyori.CalculateKubun(keisan);
        //    Kyori.KyoriKubun sinseiKubun = Kyori.CalculateKubun(sinsei);

        //    if (radioButtonOryoGigiNo.Checked)
        //        return string.Empty;
        //    int calcPrice = Math.Min(4800, 1800 + (int)keisanKubun * 800);

        //    return string.Format("1回分の申請距離は {0:0.0} kmで {1} 円\r\n　 　　　計算距離は {2:0.0} kmで {3} 円\r\n差額は {4} 円です。",
        //        sinsei,
        //        1800 + (int)sinseiKubun * 800,
        //        keisan,
        //        calcPrice,
        //        (int)keisanKubun * 800 - (int)sinseiKubun * 800);
        //}

        private void recalculateDistanceEvent(object sender, EventArgs e)
        {
            //labelTenkenResult.Text = getFreeinputNote();
        }

        private void radioButtonOryoGigiNo_CheckedChanged(object sender, EventArgs e)
        {
            //labelTenkenResult.Text = getFreeinputNote();
        }

        private void プリンター選択ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new PrinterSelectForm(Settings.InspectPrinterName))
            {
                f.ShowDialog();
                Settings.InspectPrinterName = f.SelectPrinterName;
            }
        }
    }
}
