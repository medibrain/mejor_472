﻿namespace Mejor.Inspect
{
    partial class InspectControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxPend = new System.Windows.Forms.CheckBox();
            this.checkBoxShokai = new System.Windows.Forms.CheckBox();
            this.checkBoxTenken = new System.Windows.Forms.CheckBox();
            this.checkBoxOryo = new System.Windows.Forms.CheckBox();
            this.checkBoxHenrei = new System.Windows.Forms.CheckBox();
            this.panelOryo = new System.Windows.Forms.Panel();
            this.radioButtonOryoNo = new System.Windows.Forms.RadioButton();
            this.radioButtonOryoYes = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panelTenken = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonKago = new System.Windows.Forms.RadioButton();
            this.groupBoxKago = new System.Windows.Forms.GroupBox();
            this.panelKagoOuryo = new System.Windows.Forms.Panel();
            this.checkBoxKagoOuryoNoReason = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoOuryoOver16km = new System.Windows.Forms.CheckBox();
            this.panelKagoDoui = new System.Windows.Forms.Panel();
            this.checkBoxKagoDoui = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDouiMis = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDouiNone = new System.Windows.Forms.CheckBox();
            this.panelKagoHoukoku = new System.Windows.Forms.Panel();
            this.checkBoxKagoHoukoku = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoHoukokuMis = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoHoukokuNone = new System.Windows.Forms.CheckBox();
            this.panelKagoJotai = new System.Windows.Forms.Panel();
            this.checkBoxKagoJotai = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoJotaiMis = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoJotaiNone = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoShometsu = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoInsSoui = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoHonke = new System.Windows.Forms.CheckBox();
            this.panelKagoLongDiff = new System.Windows.Forms.Panel();
            this.checkBoxKagoLongDiff5 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLongDiff2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLongDiff4 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLongDiff1 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLongDiff3 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLongDiff = new System.Windows.Forms.CheckBox();
            this.panelKagoDokuzi = new System.Windows.Forms.Panel();
            this.checkBoxKagoDokuzi5 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDokuzi2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDokuzi4 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDokuzi1 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDokuzi3 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDokuzi = new System.Windows.Forms.CheckBox();
            this.panelKagoDiff = new System.Windows.Forms.Panel();
            this.checkBoxKagoDiff5 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDiff2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDiff4 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDiff1 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDiff3 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoDiff = new System.Windows.Forms.CheckBox();
            this.panelKagoLong = new System.Windows.Forms.Panel();
            this.checkBoxKagoLong5 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLong2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLong4 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLong1 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLong3 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoLong = new System.Windows.Forms.CheckBox();
            this.panelKagoGenin = new System.Windows.Forms.Panel();
            this.checkBoxKagoGen5 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoGen2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoGen4 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoGen1 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoGen3 = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoGenin = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoShomei = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoHisseki = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoFamily = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoOther = new System.Windows.Forms.CheckBox();
            this.radioButtonSai = new System.Windows.Forms.RadioButton();
            this.textBoxTenkenMemo = new System.Windows.Forms.TextBox();
            this.radioButtonNone = new System.Windows.Forms.RadioButton();
            this.radioButtonPend = new System.Windows.Forms.RadioButton();
            this.groupBoxSaishinsa = new System.Windows.Forms.GroupBox();
            this.panelShokenGigi2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButtonGigiDoitsu = new System.Windows.Forms.RadioButton();
            this.radioButtonGigiBetsu = new System.Windows.Forms.RadioButton();
            this.radioButtonSaiOther = new System.Windows.Forms.RadioButton();
            this.radioButtonGigiShoken = new System.Windows.Forms.RadioButton();
            this.panelShokenGigi = new System.Windows.Forms.Panel();
            this.checkBoxF5 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxF4 = new System.Windows.Forms.CheckBox();
            this.checkBoxF3 = new System.Windows.Forms.CheckBox();
            this.checkBoxF2 = new System.Windows.Forms.CheckBox();
            this.radioButtonGigiChushi = new System.Windows.Forms.RadioButton();
            this.checkBoxF1 = new System.Windows.Forms.CheckBox();
            this.radioButtonGigiTenki = new System.Windows.Forms.RadioButton();
            this.panelShokai = new System.Windows.Forms.Panel();
            this.radioButtonShokaiAteNashi = new System.Windows.Forms.RadioButton();
            this.radioButtonShokaiSouiNashi = new System.Windows.Forms.RadioButton();
            this.radioButtonShokaiSouiAri = new System.Windows.Forms.RadioButton();
            this.radioButtonShokaiMihenshin = new System.Windows.Forms.RadioButton();
            this.textBoxShokaiMemo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxOther = new System.Windows.Forms.CheckBox();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.checkBoxClinic = new System.Windows.Forms.CheckBox();
            this.checkBoxPeriod = new System.Windows.Forms.CheckBox();
            this.checkBoxDayCount = new System.Windows.Forms.CheckBox();
            this.checkBoxBuiCount = new System.Windows.Forms.CheckBox();
            this.panelHenrei = new System.Windows.Forms.Panel();
            this.panelHenReason = new System.Windows.Forms.Panel();
            this.checkBoxHenReason5 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason2 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason4 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason1 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason3 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenReason = new System.Windows.Forms.CheckBox();
            this.panelHenBui = new System.Windows.Forms.Panel();
            this.checkBoxHenBui5 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui2 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui4 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui1 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui3 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenBui = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxHenKago = new System.Windows.Forms.CheckBox();
            this.checkBoxHenOther = new System.Windows.Forms.CheckBox();
            this.checkBoxHenWork = new System.Windows.Forms.CheckBox();
            this.checkBoxHenKega = new System.Windows.Forms.CheckBox();
            this.checkBoxHenDays = new System.Windows.Forms.CheckBox();
            this.checkBoxHenJiki = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxOutMemo = new System.Windows.Forms.TextBox();
            this.textBoxMemo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxPaid = new System.Windows.Forms.CheckBox();
            this.checkBoxTel = new System.Windows.Forms.CheckBox();
            this.checkBoxInputError = new System.Windows.Forms.CheckBox();
            this.panelContact = new System.Windows.Forms.Panel();
            this.textBoxContactName = new System.Windows.Forms.TextBox();
            this.textBoxContactMin = new System.Windows.Forms.TextBox();
            this.textBoxContactD = new System.Windows.Forms.TextBox();
            this.textBoxContactH = new System.Windows.Forms.TextBox();
            this.textBoxContactM = new System.Windows.Forms.TextBox();
            this.textBoxContactY = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBoxContactOrg = new System.Windows.Forms.CheckBox();
            this.checkBoxContactNo = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonRelationAppShow = new System.Windows.Forms.Button();
            this.labelRefRece = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxProcess1 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess2 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess3 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess4 = new System.Windows.Forms.CheckBox();
            this.panelKagoOryoUchiwake = new System.Windows.Forms.Panel();
            this.checkBoxKagoOryoUchiwake = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoOryoUchiwakeMis = new System.Windows.Forms.CheckBox();
            this.checkBoxKagoOryoUchiwakeNone = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelOryo.SuspendLayout();
            this.panelTenken.SuspendLayout();
            this.groupBoxKago.SuspendLayout();
            this.panelKagoOuryo.SuspendLayout();
            this.panelKagoDoui.SuspendLayout();
            this.panelKagoHoukoku.SuspendLayout();
            this.panelKagoJotai.SuspendLayout();
            this.panelKagoLongDiff.SuspendLayout();
            this.panelKagoDokuzi.SuspendLayout();
            this.panelKagoDiff.SuspendLayout();
            this.panelKagoLong.SuspendLayout();
            this.panelKagoGenin.SuspendLayout();
            this.groupBoxSaishinsa.SuspendLayout();
            this.panelShokenGigi2.SuspendLayout();
            this.panelShokenGigi.SuspendLayout();
            this.panelShokai.SuspendLayout();
            this.panelHenrei.SuspendLayout();
            this.panelHenReason.SuspendLayout();
            this.panelHenBui.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panelContact.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panelKagoOryoUchiwake.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.checkBoxPend, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxShokai, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxTenken, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxOryo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxHenrei, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(300, 22);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // checkBoxPend
            // 
            this.checkBoxPend.AutoSize = true;
            this.checkBoxPend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxPend.Location = new System.Drawing.Point(237, 1);
            this.checkBoxPend.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxPend.Name = "checkBoxPend";
            this.checkBoxPend.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxPend.Size = new System.Drawing.Size(62, 20);
            this.checkBoxPend.TabIndex = 4;
            this.checkBoxPend.Text = "払保";
            this.checkBoxPend.UseVisualStyleBackColor = true;
            this.checkBoxPend.CheckedChanged += new System.EventHandler(this.checkBoxPend_CheckedChanged);
            // 
            // checkBoxShokai
            // 
            this.checkBoxShokai.AutoSize = true;
            this.checkBoxShokai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxShokai.Location = new System.Drawing.Point(1, 1);
            this.checkBoxShokai.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxShokai.Name = "checkBoxShokai";
            this.checkBoxShokai.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxShokai.Size = new System.Drawing.Size(58, 20);
            this.checkBoxShokai.TabIndex = 0;
            this.checkBoxShokai.Text = "照会";
            this.checkBoxShokai.UseVisualStyleBackColor = true;
            this.checkBoxShokai.CheckedChanged += new System.EventHandler(this.checkBoxShokai_CheckedChanged);
            // 
            // checkBoxTenken
            // 
            this.checkBoxTenken.AutoSize = true;
            this.checkBoxTenken.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxTenken.Location = new System.Drawing.Point(60, 1);
            this.checkBoxTenken.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxTenken.Name = "checkBoxTenken";
            this.checkBoxTenken.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxTenken.Size = new System.Drawing.Size(58, 20);
            this.checkBoxTenken.TabIndex = 1;
            this.checkBoxTenken.Text = "点検";
            this.checkBoxTenken.UseVisualStyleBackColor = true;
            this.checkBoxTenken.CheckedChanged += new System.EventHandler(this.checkBoxTenken_CheckedChanged);
            // 
            // checkBoxOryo
            // 
            this.checkBoxOryo.AutoSize = true;
            this.checkBoxOryo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxOryo.Location = new System.Drawing.Point(119, 1);
            this.checkBoxOryo.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxOryo.Name = "checkBoxOryo";
            this.checkBoxOryo.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxOryo.Size = new System.Drawing.Size(58, 20);
            this.checkBoxOryo.TabIndex = 2;
            this.checkBoxOryo.Text = "往点";
            this.checkBoxOryo.UseVisualStyleBackColor = true;
            this.checkBoxOryo.CheckedChanged += new System.EventHandler(this.checkBoxOryoCheck_CheckedChanged);
            // 
            // checkBoxHenrei
            // 
            this.checkBoxHenrei.AutoSize = true;
            this.checkBoxHenrei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxHenrei.Location = new System.Drawing.Point(178, 1);
            this.checkBoxHenrei.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxHenrei.Name = "checkBoxHenrei";
            this.checkBoxHenrei.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxHenrei.Size = new System.Drawing.Size(58, 20);
            this.checkBoxHenrei.TabIndex = 3;
            this.checkBoxHenrei.Text = "返戻";
            this.checkBoxHenrei.UseVisualStyleBackColor = true;
            this.checkBoxHenrei.CheckedChanged += new System.EventHandler(this.checkBoxHenrei_CheckedChanged);
            // 
            // panelOryo
            // 
            this.panelOryo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelOryo.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelOryo.Controls.Add(this.radioButtonOryoNo);
            this.panelOryo.Controls.Add(this.radioButtonOryoYes);
            this.panelOryo.Controls.Add(this.label2);
            this.panelOryo.Enabled = false;
            this.panelOryo.Location = new System.Drawing.Point(0, 599);
            this.panelOryo.Name = "panelOryo";
            this.panelOryo.Size = new System.Drawing.Size(300, 21);
            this.panelOryo.TabIndex = 5;
            // 
            // radioButtonOryoNo
            // 
            this.radioButtonOryoNo.AutoSize = true;
            this.radioButtonOryoNo.Location = new System.Drawing.Point(186, 3);
            this.radioButtonOryoNo.Name = "radioButtonOryoNo";
            this.radioButtonOryoNo.Size = new System.Drawing.Size(66, 16);
            this.radioButtonOryoNo.TabIndex = 2;
            this.radioButtonOryoNo.TabStop = true;
            this.radioButtonOryoNo.Text = "疑義なし";
            this.radioButtonOryoNo.UseVisualStyleBackColor = true;
            // 
            // radioButtonOryoYes
            // 
            this.radioButtonOryoYes.AutoSize = true;
            this.radioButtonOryoYes.Location = new System.Drawing.Point(97, 3);
            this.radioButtonOryoYes.Name = "radioButtonOryoYes";
            this.radioButtonOryoYes.Size = new System.Drawing.Size(65, 16);
            this.radioButtonOryoYes.TabIndex = 1;
            this.radioButtonOryoYes.TabStop = true;
            this.radioButtonOryoYes.Text = "疑義あり";
            this.radioButtonOryoYes.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "往療疑義";
            // 
            // panelTenken
            // 
            this.panelTenken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTenken.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelTenken.Controls.Add(this.label1);
            this.panelTenken.Controls.Add(this.radioButtonKago);
            this.panelTenken.Controls.Add(this.groupBoxKago);
            this.panelTenken.Controls.Add(this.radioButtonSai);
            this.panelTenken.Controls.Add(this.textBoxTenkenMemo);
            this.panelTenken.Controls.Add(this.radioButtonNone);
            this.panelTenken.Controls.Add(this.radioButtonPend);
            this.panelTenken.Controls.Add(this.groupBoxSaishinsa);
            this.panelTenken.Enabled = false;
            this.panelTenken.Location = new System.Drawing.Point(0, 155);
            this.panelTenken.Name = "panelTenken";
            this.panelTenken.Size = new System.Drawing.Size(300, 415);
            this.panelTenken.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "今月点検結果";
            // 
            // radioButtonKago
            // 
            this.radioButtonKago.AutoSize = true;
            this.radioButtonKago.Location = new System.Drawing.Point(9, 21);
            this.radioButtonKago.Name = "radioButtonKago";
            this.radioButtonKago.Size = new System.Drawing.Size(47, 16);
            this.radioButtonKago.TabIndex = 3;
            this.radioButtonKago.TabStop = true;
            this.radioButtonKago.Text = "過誤";
            this.radioButtonKago.UseVisualStyleBackColor = true;
            this.radioButtonKago.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // groupBoxKago
            // 
            this.groupBoxKago.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxKago.Controls.Add(this.panelKagoOryoUchiwake);
            this.groupBoxKago.Controls.Add(this.panelKagoOuryo);
            this.groupBoxKago.Controls.Add(this.panelKagoDoui);
            this.groupBoxKago.Controls.Add(this.panelKagoHoukoku);
            this.groupBoxKago.Controls.Add(this.panelKagoJotai);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoShometsu);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoInsSoui);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoHonke);
            this.groupBoxKago.Controls.Add(this.panelKagoLongDiff);
            this.groupBoxKago.Controls.Add(this.panelKagoDokuzi);
            this.groupBoxKago.Controls.Add(this.panelKagoDiff);
            this.groupBoxKago.Controls.Add(this.panelKagoLong);
            this.groupBoxKago.Controls.Add(this.panelKagoGenin);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoShomei);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoHisseki);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoFamily);
            this.groupBoxKago.Controls.Add(this.checkBoxKagoOther);
            this.groupBoxKago.Enabled = false;
            this.groupBoxKago.Location = new System.Drawing.Point(4, 22);
            this.groupBoxKago.Name = "groupBoxKago";
            this.groupBoxKago.Size = new System.Drawing.Size(293, 253);
            this.groupBoxKago.TabIndex = 4;
            this.groupBoxKago.TabStop = false;
            // 
            // panelKagoOuryo
            // 
            this.panelKagoOuryo.Controls.Add(this.checkBoxKagoOuryoNoReason);
            this.panelKagoOuryo.Controls.Add(this.checkBoxKagoOuryoOver16km);
            this.panelKagoOuryo.Location = new System.Drawing.Point(3, 133);
            this.panelKagoOuryo.Name = "panelKagoOuryo";
            this.panelKagoOuryo.Size = new System.Drawing.Size(283, 17);
            this.panelKagoOuryo.TabIndex = 52;
            // 
            // checkBoxKagoOuryoNoReason
            // 
            this.checkBoxKagoOuryoNoReason.AutoSize = true;
            this.checkBoxKagoOuryoNoReason.Location = new System.Drawing.Point(6, 3);
            this.checkBoxKagoOuryoNoReason.Name = "checkBoxKagoOuryoNoReason";
            this.checkBoxKagoOuryoNoReason.Size = new System.Drawing.Size(117, 16);
            this.checkBoxKagoOuryoNoReason.TabIndex = 57;
            this.checkBoxKagoOuryoNoReason.Text = "往療理由記載無し";
            this.checkBoxKagoOuryoNoReason.UseVisualStyleBackColor = true;
            this.checkBoxKagoOuryoNoReason.CheckedChanged += new System.EventHandler(this.checkBoxKagoOuryoNoReason_CheckedChanged);
            // 
            // checkBoxKagoOuryoOver16km
            // 
            this.checkBoxKagoOuryoOver16km.AutoSize = true;
            this.checkBoxKagoOuryoOver16km.Enabled = false;
            this.checkBoxKagoOuryoOver16km.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxKagoOuryoOver16km.Location = new System.Drawing.Point(133, 3);
            this.checkBoxKagoOuryoOver16km.Name = "checkBoxKagoOuryoOver16km";
            this.checkBoxKagoOuryoOver16km.Size = new System.Drawing.Size(72, 16);
            this.checkBoxKagoOuryoOver16km.TabIndex = 60;
            this.checkBoxKagoOuryoOver16km.Text = "16km以上";
            this.checkBoxKagoOuryoOver16km.UseVisualStyleBackColor = true;
            // 
            // panelKagoDoui
            // 
            this.panelKagoDoui.Controls.Add(this.checkBoxKagoDoui);
            this.panelKagoDoui.Controls.Add(this.checkBoxKagoDouiMis);
            this.panelKagoDoui.Controls.Add(this.checkBoxKagoDouiNone);
            this.panelKagoDoui.Location = new System.Drawing.Point(3, 152);
            this.panelKagoDoui.Name = "panelKagoDoui";
            this.panelKagoDoui.Size = new System.Drawing.Size(283, 17);
            this.panelKagoDoui.TabIndex = 55;
            // 
            // checkBoxKagoDoui
            // 
            this.checkBoxKagoDoui.AutoSize = true;
            this.checkBoxKagoDoui.Location = new System.Drawing.Point(6, 3);
            this.checkBoxKagoDoui.Name = "checkBoxKagoDoui";
            this.checkBoxKagoDoui.Size = new System.Drawing.Size(84, 16);
            this.checkBoxKagoDoui.TabIndex = 63;
            this.checkBoxKagoDoui.Text = "施術同意書";
            this.checkBoxKagoDoui.UseVisualStyleBackColor = true;
            this.checkBoxKagoDoui.CheckedChanged += new System.EventHandler(this.checkBoxKagoDoui_CheckedChanged);
            // 
            // checkBoxKagoDouiMis
            // 
            this.checkBoxKagoDouiMis.AutoSize = true;
            this.checkBoxKagoDouiMis.Enabled = false;
            this.checkBoxKagoDouiMis.Location = new System.Drawing.Point(168, 3);
            this.checkBoxKagoDouiMis.Name = "checkBoxKagoDouiMis";
            this.checkBoxKagoDouiMis.Size = new System.Drawing.Size(48, 16);
            this.checkBoxKagoDouiMis.TabIndex = 65;
            this.checkBoxKagoDouiMis.Text = "不備";
            this.checkBoxKagoDouiMis.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDouiNone
            // 
            this.checkBoxKagoDouiNone.AutoSize = true;
            this.checkBoxKagoDouiNone.Enabled = false;
            this.checkBoxKagoDouiNone.Location = new System.Drawing.Point(104, 3);
            this.checkBoxKagoDouiNone.Name = "checkBoxKagoDouiNone";
            this.checkBoxKagoDouiNone.Size = new System.Drawing.Size(60, 16);
            this.checkBoxKagoDouiNone.TabIndex = 64;
            this.checkBoxKagoDouiNone.Text = "添付無";
            this.checkBoxKagoDouiNone.UseVisualStyleBackColor = true;
            // 
            // panelKagoHoukoku
            // 
            this.panelKagoHoukoku.Controls.Add(this.checkBoxKagoHoukoku);
            this.panelKagoHoukoku.Controls.Add(this.checkBoxKagoHoukokuMis);
            this.panelKagoHoukoku.Controls.Add(this.checkBoxKagoHoukokuNone);
            this.panelKagoHoukoku.Location = new System.Drawing.Point(3, 171);
            this.panelKagoHoukoku.Name = "panelKagoHoukoku";
            this.panelKagoHoukoku.Size = new System.Drawing.Size(283, 17);
            this.panelKagoHoukoku.TabIndex = 58;
            // 
            // checkBoxKagoHoukoku
            // 
            this.checkBoxKagoHoukoku.AutoSize = true;
            this.checkBoxKagoHoukoku.Location = new System.Drawing.Point(6, 3);
            this.checkBoxKagoHoukoku.Name = "checkBoxKagoHoukoku";
            this.checkBoxKagoHoukoku.Size = new System.Drawing.Size(84, 16);
            this.checkBoxKagoHoukoku.TabIndex = 82;
            this.checkBoxKagoHoukoku.Text = "施術報告書";
            this.checkBoxKagoHoukoku.UseVisualStyleBackColor = true;
            this.checkBoxKagoHoukoku.CheckedChanged += new System.EventHandler(this.checkBoxKagoHoukoku_CheckedChanged);
            // 
            // checkBoxKagoHoukokuMis
            // 
            this.checkBoxKagoHoukokuMis.AutoSize = true;
            this.checkBoxKagoHoukokuMis.Enabled = false;
            this.checkBoxKagoHoukokuMis.Location = new System.Drawing.Point(168, 3);
            this.checkBoxKagoHoukokuMis.Name = "checkBoxKagoHoukokuMis";
            this.checkBoxKagoHoukokuMis.Size = new System.Drawing.Size(48, 16);
            this.checkBoxKagoHoukokuMis.TabIndex = 84;
            this.checkBoxKagoHoukokuMis.Text = "不備";
            this.checkBoxKagoHoukokuMis.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoHoukokuNone
            // 
            this.checkBoxKagoHoukokuNone.AutoSize = true;
            this.checkBoxKagoHoukokuNone.Enabled = false;
            this.checkBoxKagoHoukokuNone.Location = new System.Drawing.Point(104, 3);
            this.checkBoxKagoHoukokuNone.Name = "checkBoxKagoHoukokuNone";
            this.checkBoxKagoHoukokuNone.Size = new System.Drawing.Size(60, 16);
            this.checkBoxKagoHoukokuNone.TabIndex = 83;
            this.checkBoxKagoHoukokuNone.Text = "添付無";
            this.checkBoxKagoHoukokuNone.UseVisualStyleBackColor = true;
            // 
            // panelKagoJotai
            // 
            this.panelKagoJotai.Controls.Add(this.checkBoxKagoJotai);
            this.panelKagoJotai.Controls.Add(this.checkBoxKagoJotaiMis);
            this.panelKagoJotai.Controls.Add(this.checkBoxKagoJotaiNone);
            this.panelKagoJotai.Location = new System.Drawing.Point(3, 190);
            this.panelKagoJotai.Name = "panelKagoJotai";
            this.panelKagoJotai.Size = new System.Drawing.Size(283, 17);
            this.panelKagoJotai.TabIndex = 60;
            // 
            // checkBoxKagoJotai
            // 
            this.checkBoxKagoJotai.AutoSize = true;
            this.checkBoxKagoJotai.Location = new System.Drawing.Point(6, 2);
            this.checkBoxKagoJotai.Name = "checkBoxKagoJotai";
            this.checkBoxKagoJotai.Size = new System.Drawing.Size(162, 16);
            this.checkBoxKagoJotai.TabIndex = 73;
            this.checkBoxKagoJotai.Text = "施術継続理由/状態記入書";
            this.checkBoxKagoJotai.UseVisualStyleBackColor = true;
            this.checkBoxKagoJotai.CheckedChanged += new System.EventHandler(this.checkBoxKagoJotai_CheckedChanged);
            // 
            // checkBoxKagoJotaiMis
            // 
            this.checkBoxKagoJotaiMis.AutoSize = true;
            this.checkBoxKagoJotaiMis.Enabled = false;
            this.checkBoxKagoJotaiMis.Location = new System.Drawing.Point(232, 2);
            this.checkBoxKagoJotaiMis.Name = "checkBoxKagoJotaiMis";
            this.checkBoxKagoJotaiMis.Size = new System.Drawing.Size(48, 16);
            this.checkBoxKagoJotaiMis.TabIndex = 75;
            this.checkBoxKagoJotaiMis.Text = "不備";
            this.checkBoxKagoJotaiMis.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoJotaiNone
            // 
            this.checkBoxKagoJotaiNone.AutoSize = true;
            this.checkBoxKagoJotaiNone.Enabled = false;
            this.checkBoxKagoJotaiNone.Location = new System.Drawing.Point(168, 2);
            this.checkBoxKagoJotaiNone.Name = "checkBoxKagoJotaiNone";
            this.checkBoxKagoJotaiNone.Size = new System.Drawing.Size(60, 16);
            this.checkBoxKagoJotaiNone.TabIndex = 74;
            this.checkBoxKagoJotaiNone.Text = "添付無";
            this.checkBoxKagoJotaiNone.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoShometsu
            // 
            this.checkBoxKagoShometsu.AutoSize = true;
            this.checkBoxKagoShometsu.Location = new System.Drawing.Point(9, 102);
            this.checkBoxKagoShometsu.Name = "checkBoxKagoShometsu";
            this.checkBoxKagoShometsu.Size = new System.Drawing.Size(154, 16);
            this.checkBoxKagoShometsu.TabIndex = 37;
            this.checkBoxKagoShometsu.Text = "療養費請求権の消滅時効";
            this.checkBoxKagoShometsu.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoInsSoui
            // 
            this.checkBoxKagoInsSoui.AutoSize = true;
            this.checkBoxKagoInsSoui.Location = new System.Drawing.Point(9, 119);
            this.checkBoxKagoInsSoui.Name = "checkBoxKagoInsSoui";
            this.checkBoxKagoInsSoui.Size = new System.Drawing.Size(84, 16);
            this.checkBoxKagoInsSoui.TabIndex = 50;
            this.checkBoxKagoInsSoui.Text = "保険者相違";
            this.checkBoxKagoInsSoui.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoHonke
            // 
            this.checkBoxKagoHonke.AutoSize = true;
            this.checkBoxKagoHonke.Location = new System.Drawing.Point(167, 102);
            this.checkBoxKagoHonke.Name = "checkBoxKagoHonke";
            this.checkBoxKagoHonke.Size = new System.Drawing.Size(92, 16);
            this.checkBoxKagoHonke.TabIndex = 40;
            this.checkBoxKagoHonke.Text = "本家区分誤り";
            this.checkBoxKagoHonke.UseVisualStyleBackColor = true;
            // 
            // panelKagoLongDiff
            // 
            this.panelKagoLongDiff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff5);
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff2);
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff4);
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff1);
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff3);
            this.panelKagoLongDiff.Controls.Add(this.checkBoxKagoLongDiff);
            this.panelKagoLongDiff.Location = new System.Drawing.Point(3, 66);
            this.panelKagoLongDiff.Name = "panelKagoLongDiff";
            this.panelKagoLongDiff.Size = new System.Drawing.Size(283, 17);
            this.panelKagoLongDiff.TabIndex = 9;
            // 
            // checkBoxKagoLongDiff5
            // 
            this.checkBoxKagoLongDiff5.AutoSize = true;
            this.checkBoxKagoLongDiff5.Enabled = false;
            this.checkBoxKagoLongDiff5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxKagoLongDiff5.Name = "checkBoxKagoLongDiff5";
            this.checkBoxKagoLongDiff5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLongDiff5.TabIndex = 5;
            this.checkBoxKagoLongDiff5.Text = "5";
            this.checkBoxKagoLongDiff5.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLongDiff2
            // 
            this.checkBoxKagoLongDiff2.AutoSize = true;
            this.checkBoxKagoLongDiff2.Enabled = false;
            this.checkBoxKagoLongDiff2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxKagoLongDiff2.Name = "checkBoxKagoLongDiff2";
            this.checkBoxKagoLongDiff2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLongDiff2.TabIndex = 2;
            this.checkBoxKagoLongDiff2.Text = "2";
            this.checkBoxKagoLongDiff2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLongDiff4
            // 
            this.checkBoxKagoLongDiff4.AutoSize = true;
            this.checkBoxKagoLongDiff4.Enabled = false;
            this.checkBoxKagoLongDiff4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxKagoLongDiff4.Name = "checkBoxKagoLongDiff4";
            this.checkBoxKagoLongDiff4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLongDiff4.TabIndex = 4;
            this.checkBoxKagoLongDiff4.Text = "4";
            this.checkBoxKagoLongDiff4.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLongDiff1
            // 
            this.checkBoxKagoLongDiff1.AutoSize = true;
            this.checkBoxKagoLongDiff1.Enabled = false;
            this.checkBoxKagoLongDiff1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxKagoLongDiff1.Name = "checkBoxKagoLongDiff1";
            this.checkBoxKagoLongDiff1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLongDiff1.TabIndex = 1;
            this.checkBoxKagoLongDiff1.Text = "1";
            this.checkBoxKagoLongDiff1.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLongDiff3
            // 
            this.checkBoxKagoLongDiff3.AutoSize = true;
            this.checkBoxKagoLongDiff3.Enabled = false;
            this.checkBoxKagoLongDiff3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxKagoLongDiff3.Name = "checkBoxKagoLongDiff3";
            this.checkBoxKagoLongDiff3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLongDiff3.TabIndex = 3;
            this.checkBoxKagoLongDiff3.Text = "3";
            this.checkBoxKagoLongDiff3.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLongDiff
            // 
            this.checkBoxKagoLongDiff.AutoSize = true;
            this.checkBoxKagoLongDiff.Location = new System.Drawing.Point(6, 1);
            this.checkBoxKagoLongDiff.Name = "checkBoxKagoLongDiff";
            this.checkBoxKagoLongDiff.Size = new System.Drawing.Size(96, 16);
            this.checkBoxKagoLongDiff.TabIndex = 0;
            this.checkBoxKagoLongDiff.Text = "長期理由相違";
            this.checkBoxKagoLongDiff.UseVisualStyleBackColor = true;
            this.checkBoxKagoLongDiff.CheckedChanged += new System.EventHandler(this.checkBoxKagoLongDiff_CheckedChanged);
            // 
            // panelKagoDokuzi
            // 
            this.panelKagoDokuzi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi5);
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi2);
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi4);
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi1);
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi3);
            this.panelKagoDokuzi.Controls.Add(this.checkBoxKagoDokuzi);
            this.panelKagoDokuzi.Location = new System.Drawing.Point(3, 228);
            this.panelKagoDokuzi.Name = "panelKagoDokuzi";
            this.panelKagoDokuzi.Size = new System.Drawing.Size(283, 17);
            this.panelKagoDokuzi.TabIndex = 70;
            // 
            // checkBoxKagoDokuzi5
            // 
            this.checkBoxKagoDokuzi5.AutoSize = true;
            this.checkBoxKagoDokuzi5.Enabled = false;
            this.checkBoxKagoDokuzi5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxKagoDokuzi5.Name = "checkBoxKagoDokuzi5";
            this.checkBoxKagoDokuzi5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDokuzi5.TabIndex = 5;
            this.checkBoxKagoDokuzi5.Text = "5";
            this.checkBoxKagoDokuzi5.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDokuzi2
            // 
            this.checkBoxKagoDokuzi2.AutoSize = true;
            this.checkBoxKagoDokuzi2.Enabled = false;
            this.checkBoxKagoDokuzi2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxKagoDokuzi2.Name = "checkBoxKagoDokuzi2";
            this.checkBoxKagoDokuzi2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDokuzi2.TabIndex = 2;
            this.checkBoxKagoDokuzi2.Text = "2";
            this.checkBoxKagoDokuzi2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDokuzi4
            // 
            this.checkBoxKagoDokuzi4.AutoSize = true;
            this.checkBoxKagoDokuzi4.Enabled = false;
            this.checkBoxKagoDokuzi4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxKagoDokuzi4.Name = "checkBoxKagoDokuzi4";
            this.checkBoxKagoDokuzi4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDokuzi4.TabIndex = 4;
            this.checkBoxKagoDokuzi4.Text = "4";
            this.checkBoxKagoDokuzi4.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDokuzi1
            // 
            this.checkBoxKagoDokuzi1.AutoSize = true;
            this.checkBoxKagoDokuzi1.Enabled = false;
            this.checkBoxKagoDokuzi1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxKagoDokuzi1.Name = "checkBoxKagoDokuzi1";
            this.checkBoxKagoDokuzi1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDokuzi1.TabIndex = 1;
            this.checkBoxKagoDokuzi1.Text = "1";
            this.checkBoxKagoDokuzi1.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDokuzi3
            // 
            this.checkBoxKagoDokuzi3.AutoSize = true;
            this.checkBoxKagoDokuzi3.Enabled = false;
            this.checkBoxKagoDokuzi3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxKagoDokuzi3.Name = "checkBoxKagoDokuzi3";
            this.checkBoxKagoDokuzi3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDokuzi3.TabIndex = 3;
            this.checkBoxKagoDokuzi3.Text = "3";
            this.checkBoxKagoDokuzi3.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDokuzi
            // 
            this.checkBoxKagoDokuzi.AutoSize = true;
            this.checkBoxKagoDokuzi.Location = new System.Drawing.Point(6, 1);
            this.checkBoxKagoDokuzi.Name = "checkBoxKagoDokuzi";
            this.checkBoxKagoDokuzi.Size = new System.Drawing.Size(48, 16);
            this.checkBoxKagoDokuzi.TabIndex = 0;
            this.checkBoxKagoDokuzi.Text = "独自";
            this.checkBoxKagoDokuzi.UseVisualStyleBackColor = true;
            this.checkBoxKagoDokuzi.CheckedChanged += new System.EventHandler(this.checkBoxKagoDokuzi_CheckedChanged);
            // 
            // panelKagoDiff
            // 
            this.panelKagoDiff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff5);
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff2);
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff4);
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff1);
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff3);
            this.panelKagoDiff.Controls.Add(this.checkBoxKagoDiff);
            this.panelKagoDiff.Location = new System.Drawing.Point(3, 30);
            this.panelKagoDiff.Name = "panelKagoDiff";
            this.panelKagoDiff.Size = new System.Drawing.Size(283, 17);
            this.panelKagoDiff.TabIndex = 2;
            // 
            // checkBoxKagoDiff5
            // 
            this.checkBoxKagoDiff5.AutoSize = true;
            this.checkBoxKagoDiff5.Enabled = false;
            this.checkBoxKagoDiff5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxKagoDiff5.Name = "checkBoxKagoDiff5";
            this.checkBoxKagoDiff5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDiff5.TabIndex = 5;
            this.checkBoxKagoDiff5.Text = "5";
            this.checkBoxKagoDiff5.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDiff2
            // 
            this.checkBoxKagoDiff2.AutoSize = true;
            this.checkBoxKagoDiff2.Enabled = false;
            this.checkBoxKagoDiff2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxKagoDiff2.Name = "checkBoxKagoDiff2";
            this.checkBoxKagoDiff2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDiff2.TabIndex = 2;
            this.checkBoxKagoDiff2.Text = "2";
            this.checkBoxKagoDiff2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDiff4
            // 
            this.checkBoxKagoDiff4.AutoSize = true;
            this.checkBoxKagoDiff4.Enabled = false;
            this.checkBoxKagoDiff4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxKagoDiff4.Name = "checkBoxKagoDiff4";
            this.checkBoxKagoDiff4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDiff4.TabIndex = 4;
            this.checkBoxKagoDiff4.Text = "4";
            this.checkBoxKagoDiff4.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDiff1
            // 
            this.checkBoxKagoDiff1.AutoSize = true;
            this.checkBoxKagoDiff1.Enabled = false;
            this.checkBoxKagoDiff1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxKagoDiff1.Name = "checkBoxKagoDiff1";
            this.checkBoxKagoDiff1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDiff1.TabIndex = 1;
            this.checkBoxKagoDiff1.Text = "1";
            this.checkBoxKagoDiff1.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDiff3
            // 
            this.checkBoxKagoDiff3.AutoSize = true;
            this.checkBoxKagoDiff3.Enabled = false;
            this.checkBoxKagoDiff3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxKagoDiff3.Name = "checkBoxKagoDiff3";
            this.checkBoxKagoDiff3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoDiff3.TabIndex = 3;
            this.checkBoxKagoDiff3.Text = "3";
            this.checkBoxKagoDiff3.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoDiff
            // 
            this.checkBoxKagoDiff.AutoSize = true;
            this.checkBoxKagoDiff.Location = new System.Drawing.Point(6, 1);
            this.checkBoxKagoDiff.Name = "checkBoxKagoDiff";
            this.checkBoxKagoDiff.Size = new System.Drawing.Size(96, 16);
            this.checkBoxKagoDiff.TabIndex = 0;
            this.checkBoxKagoDiff.Text = "負傷原因相違";
            this.checkBoxKagoDiff.UseVisualStyleBackColor = true;
            this.checkBoxKagoDiff.CheckedChanged += new System.EventHandler(this.checkBoxKagoDiff_CheckedChanged);
            // 
            // panelKagoLong
            // 
            this.panelKagoLong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong5);
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong2);
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong4);
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong1);
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong3);
            this.panelKagoLong.Controls.Add(this.checkBoxKagoLong);
            this.panelKagoLong.Location = new System.Drawing.Point(3, 48);
            this.panelKagoLong.Name = "panelKagoLong";
            this.panelKagoLong.Size = new System.Drawing.Size(283, 17);
            this.panelKagoLong.TabIndex = 5;
            // 
            // checkBoxKagoLong5
            // 
            this.checkBoxKagoLong5.AutoSize = true;
            this.checkBoxKagoLong5.Enabled = false;
            this.checkBoxKagoLong5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxKagoLong5.Name = "checkBoxKagoLong5";
            this.checkBoxKagoLong5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLong5.TabIndex = 5;
            this.checkBoxKagoLong5.Text = "5";
            this.checkBoxKagoLong5.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLong2
            // 
            this.checkBoxKagoLong2.AutoSize = true;
            this.checkBoxKagoLong2.Enabled = false;
            this.checkBoxKagoLong2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxKagoLong2.Name = "checkBoxKagoLong2";
            this.checkBoxKagoLong2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLong2.TabIndex = 2;
            this.checkBoxKagoLong2.Text = "2";
            this.checkBoxKagoLong2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLong4
            // 
            this.checkBoxKagoLong4.AutoSize = true;
            this.checkBoxKagoLong4.Enabled = false;
            this.checkBoxKagoLong4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxKagoLong4.Name = "checkBoxKagoLong4";
            this.checkBoxKagoLong4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLong4.TabIndex = 4;
            this.checkBoxKagoLong4.Text = "4";
            this.checkBoxKagoLong4.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLong1
            // 
            this.checkBoxKagoLong1.AutoSize = true;
            this.checkBoxKagoLong1.Enabled = false;
            this.checkBoxKagoLong1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxKagoLong1.Name = "checkBoxKagoLong1";
            this.checkBoxKagoLong1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLong1.TabIndex = 1;
            this.checkBoxKagoLong1.Text = "1";
            this.checkBoxKagoLong1.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLong3
            // 
            this.checkBoxKagoLong3.AutoSize = true;
            this.checkBoxKagoLong3.Enabled = false;
            this.checkBoxKagoLong3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxKagoLong3.Name = "checkBoxKagoLong3";
            this.checkBoxKagoLong3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoLong3.TabIndex = 3;
            this.checkBoxKagoLong3.Text = "3";
            this.checkBoxKagoLong3.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoLong
            // 
            this.checkBoxKagoLong.AutoSize = true;
            this.checkBoxKagoLong.Location = new System.Drawing.Point(6, 1);
            this.checkBoxKagoLong.Name = "checkBoxKagoLong";
            this.checkBoxKagoLong.Size = new System.Drawing.Size(91, 16);
            this.checkBoxKagoLong.TabIndex = 0;
            this.checkBoxKagoLong.Text = "長期理由なし";
            this.checkBoxKagoLong.UseVisualStyleBackColor = true;
            this.checkBoxKagoLong.CheckedChanged += new System.EventHandler(this.checkBoxKagoLong_CheckedChanged);
            // 
            // panelKagoGenin
            // 
            this.panelKagoGenin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGen5);
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGen2);
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGen4);
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGen1);
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGen3);
            this.panelKagoGenin.Controls.Add(this.checkBoxKagoGenin);
            this.panelKagoGenin.Location = new System.Drawing.Point(3, 13);
            this.panelKagoGenin.Name = "panelKagoGenin";
            this.panelKagoGenin.Size = new System.Drawing.Size(283, 17);
            this.panelKagoGenin.TabIndex = 0;
            // 
            // checkBoxKagoGen5
            // 
            this.checkBoxKagoGen5.AutoSize = true;
            this.checkBoxKagoGen5.Enabled = false;
            this.checkBoxKagoGen5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxKagoGen5.Name = "checkBoxKagoGen5";
            this.checkBoxKagoGen5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoGen5.TabIndex = 5;
            this.checkBoxKagoGen5.Text = "5";
            this.checkBoxKagoGen5.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoGen2
            // 
            this.checkBoxKagoGen2.AutoSize = true;
            this.checkBoxKagoGen2.Enabled = false;
            this.checkBoxKagoGen2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxKagoGen2.Name = "checkBoxKagoGen2";
            this.checkBoxKagoGen2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoGen2.TabIndex = 2;
            this.checkBoxKagoGen2.Text = "2";
            this.checkBoxKagoGen2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoGen4
            // 
            this.checkBoxKagoGen4.AutoSize = true;
            this.checkBoxKagoGen4.Enabled = false;
            this.checkBoxKagoGen4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxKagoGen4.Name = "checkBoxKagoGen4";
            this.checkBoxKagoGen4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoGen4.TabIndex = 4;
            this.checkBoxKagoGen4.Text = "4";
            this.checkBoxKagoGen4.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoGen1
            // 
            this.checkBoxKagoGen1.AutoSize = true;
            this.checkBoxKagoGen1.Enabled = false;
            this.checkBoxKagoGen1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxKagoGen1.Name = "checkBoxKagoGen1";
            this.checkBoxKagoGen1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoGen1.TabIndex = 1;
            this.checkBoxKagoGen1.Text = "1";
            this.checkBoxKagoGen1.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoGen3
            // 
            this.checkBoxKagoGen3.AutoSize = true;
            this.checkBoxKagoGen3.Enabled = false;
            this.checkBoxKagoGen3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxKagoGen3.Name = "checkBoxKagoGen3";
            this.checkBoxKagoGen3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxKagoGen3.TabIndex = 3;
            this.checkBoxKagoGen3.Text = "3";
            this.checkBoxKagoGen3.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoGenin
            // 
            this.checkBoxKagoGenin.AutoSize = true;
            this.checkBoxKagoGenin.Location = new System.Drawing.Point(6, 1);
            this.checkBoxKagoGenin.Name = "checkBoxKagoGenin";
            this.checkBoxKagoGenin.Size = new System.Drawing.Size(91, 16);
            this.checkBoxKagoGenin.TabIndex = 0;
            this.checkBoxKagoGenin.Text = "負傷原因なし";
            this.checkBoxKagoGenin.UseVisualStyleBackColor = true;
            this.checkBoxKagoGenin.CheckedChanged += new System.EventHandler(this.checkBoxKagoGenin_CheckedChanged);
            // 
            // checkBoxKagoShomei
            // 
            this.checkBoxKagoShomei.AutoSize = true;
            this.checkBoxKagoShomei.Location = new System.Drawing.Point(9, 84);
            this.checkBoxKagoShomei.Name = "checkBoxKagoShomei";
            this.checkBoxKagoShomei.Size = new System.Drawing.Size(70, 16);
            this.checkBoxKagoShomei.TabIndex = 11;
            this.checkBoxKagoShomei.Text = "署名違い";
            this.checkBoxKagoShomei.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoHisseki
            // 
            this.checkBoxKagoHisseki.AutoSize = true;
            this.checkBoxKagoHisseki.Location = new System.Drawing.Point(80, 84);
            this.checkBoxKagoHisseki.Name = "checkBoxKagoHisseki";
            this.checkBoxKagoHisseki.Size = new System.Drawing.Size(70, 16);
            this.checkBoxKagoHisseki.TabIndex = 14;
            this.checkBoxKagoHisseki.Text = "筆跡違い";
            this.checkBoxKagoHisseki.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoFamily
            // 
            this.checkBoxKagoFamily.AutoSize = true;
            this.checkBoxKagoFamily.Location = new System.Drawing.Point(152, 84);
            this.checkBoxKagoFamily.Name = "checkBoxKagoFamily";
            this.checkBoxKagoFamily.Size = new System.Drawing.Size(84, 16);
            this.checkBoxKagoFamily.TabIndex = 25;
            this.checkBoxKagoFamily.Text = "家族同筆跡";
            this.checkBoxKagoFamily.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoOther
            // 
            this.checkBoxKagoOther.AutoSize = true;
            this.checkBoxKagoOther.Location = new System.Drawing.Point(237, 84);
            this.checkBoxKagoOther.Name = "checkBoxKagoOther";
            this.checkBoxKagoOther.Size = new System.Drawing.Size(55, 16);
            this.checkBoxKagoOther.TabIndex = 30;
            this.checkBoxKagoOther.Text = "その他";
            this.checkBoxKagoOther.UseVisualStyleBackColor = true;
            // 
            // radioButtonSai
            // 
            this.radioButtonSai.AutoSize = true;
            this.radioButtonSai.Location = new System.Drawing.Point(8, 280);
            this.radioButtonSai.Name = "radioButtonSai";
            this.radioButtonSai.Size = new System.Drawing.Size(59, 16);
            this.radioButtonSai.TabIndex = 5;
            this.radioButtonSai.TabStop = true;
            this.radioButtonSai.Text = "再審査";
            this.radioButtonSai.UseVisualStyleBackColor = true;
            this.radioButtonSai.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // textBoxTenkenMemo
            // 
            this.textBoxTenkenMemo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTenkenMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxTenkenMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxTenkenMemo.Location = new System.Drawing.Point(2, 371);
            this.textBoxTenkenMemo.Multiline = true;
            this.textBoxTenkenMemo.Name = "textBoxTenkenMemo";
            this.textBoxTenkenMemo.Size = new System.Drawing.Size(294, 38);
            this.textBoxTenkenMemo.TabIndex = 7;
            // 
            // radioButtonNone
            // 
            this.radioButtonNone.AutoSize = true;
            this.radioButtonNone.Location = new System.Drawing.Point(122, 4);
            this.radioButtonNone.Name = "radioButtonNone";
            this.radioButtonNone.Size = new System.Drawing.Size(66, 16);
            this.radioButtonNone.TabIndex = 1;
            this.radioButtonNone.TabStop = true;
            this.radioButtonNone.Text = "申出なし";
            this.radioButtonNone.UseVisualStyleBackColor = true;
            this.radioButtonNone.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // radioButtonPend
            // 
            this.radioButtonPend.AutoSize = true;
            this.radioButtonPend.Location = new System.Drawing.Point(212, 4);
            this.radioButtonPend.Name = "radioButtonPend";
            this.radioButtonPend.Size = new System.Drawing.Size(47, 16);
            this.radioButtonPend.TabIndex = 2;
            this.radioButtonPend.TabStop = true;
            this.radioButtonPend.Text = "保留";
            this.radioButtonPend.UseVisualStyleBackColor = true;
            this.radioButtonPend.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // groupBoxSaishinsa
            // 
            this.groupBoxSaishinsa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSaishinsa.Controls.Add(this.panelShokenGigi2);
            this.groupBoxSaishinsa.Controls.Add(this.radioButtonSaiOther);
            this.groupBoxSaishinsa.Controls.Add(this.radioButtonGigiShoken);
            this.groupBoxSaishinsa.Controls.Add(this.panelShokenGigi);
            this.groupBoxSaishinsa.Enabled = false;
            this.groupBoxSaishinsa.Location = new System.Drawing.Point(2, 281);
            this.groupBoxSaishinsa.Name = "groupBoxSaishinsa";
            this.groupBoxSaishinsa.Size = new System.Drawing.Size(294, 89);
            this.groupBoxSaishinsa.TabIndex = 6;
            this.groupBoxSaishinsa.TabStop = false;
            // 
            // panelShokenGigi2
            // 
            this.panelShokenGigi2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelShokenGigi2.BackColor = System.Drawing.Color.LightCyan;
            this.panelShokenGigi2.Controls.Add(this.label7);
            this.panelShokenGigi2.Controls.Add(this.radioButtonGigiDoitsu);
            this.panelShokenGigi2.Controls.Add(this.radioButtonGigiBetsu);
            this.panelShokenGigi2.Enabled = false;
            this.panelShokenGigi2.Location = new System.Drawing.Point(183, 31);
            this.panelShokenGigi2.Name = "panelShokenGigi2";
            this.panelShokenGigi2.Size = new System.Drawing.Size(110, 56);
            this.panelShokenGigi2.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "当月";
            // 
            // radioButtonGigiDoitsu
            // 
            this.radioButtonGigiDoitsu.AutoSize = true;
            this.radioButtonGigiDoitsu.Location = new System.Drawing.Point(17, 19);
            this.radioButtonGigiDoitsu.Name = "radioButtonGigiDoitsu";
            this.radioButtonGigiDoitsu.Size = new System.Drawing.Size(83, 16);
            this.radioButtonGigiDoitsu.TabIndex = 1;
            this.radioButtonGigiDoitsu.TabStop = true;
            this.radioButtonGigiDoitsu.Text = "同一負傷名";
            this.radioButtonGigiDoitsu.UseVisualStyleBackColor = true;
            // 
            // radioButtonGigiBetsu
            // 
            this.radioButtonGigiBetsu.AutoSize = true;
            this.radioButtonGigiBetsu.Location = new System.Drawing.Point(17, 37);
            this.radioButtonGigiBetsu.Name = "radioButtonGigiBetsu";
            this.radioButtonGigiBetsu.Size = new System.Drawing.Size(71, 16);
            this.radioButtonGigiBetsu.TabIndex = 2;
            this.radioButtonGigiBetsu.TabStop = true;
            this.radioButtonGigiBetsu.Text = "別負傷名";
            this.radioButtonGigiBetsu.UseVisualStyleBackColor = true;
            // 
            // radioButtonSaiOther
            // 
            this.radioButtonSaiOther.AutoSize = true;
            this.radioButtonSaiOther.Location = new System.Drawing.Point(168, 15);
            this.radioButtonSaiOther.Name = "radioButtonSaiOther";
            this.radioButtonSaiOther.Size = new System.Drawing.Size(54, 16);
            this.radioButtonSaiOther.TabIndex = 1;
            this.radioButtonSaiOther.TabStop = true;
            this.radioButtonSaiOther.Text = "その他";
            this.radioButtonSaiOther.UseVisualStyleBackColor = true;
            // 
            // radioButtonGigiShoken
            // 
            this.radioButtonGigiShoken.AutoSize = true;
            this.radioButtonGigiShoken.Location = new System.Drawing.Point(25, 15);
            this.radioButtonGigiShoken.Name = "radioButtonGigiShoken";
            this.radioButtonGigiShoken.Size = new System.Drawing.Size(83, 16);
            this.radioButtonGigiShoken.TabIndex = 0;
            this.radioButtonGigiShoken.TabStop = true;
            this.radioButtonGigiShoken.Text = "初検料疑義";
            this.radioButtonGigiShoken.UseVisualStyleBackColor = true;
            this.radioButtonGigiShoken.CheckedChanged += new System.EventHandler(this.radioButtonGigiShoken_CheckedChanged);
            // 
            // panelShokenGigi
            // 
            this.panelShokenGigi.BackColor = System.Drawing.Color.LightCyan;
            this.panelShokenGigi.Controls.Add(this.checkBoxF5);
            this.panelShokenGigi.Controls.Add(this.label6);
            this.panelShokenGigi.Controls.Add(this.checkBoxF4);
            this.panelShokenGigi.Controls.Add(this.checkBoxF3);
            this.panelShokenGigi.Controls.Add(this.checkBoxF2);
            this.panelShokenGigi.Controls.Add(this.radioButtonGigiChushi);
            this.panelShokenGigi.Controls.Add(this.checkBoxF1);
            this.panelShokenGigi.Controls.Add(this.radioButtonGigiTenki);
            this.panelShokenGigi.Enabled = false;
            this.panelShokenGigi.Location = new System.Drawing.Point(1, 31);
            this.panelShokenGigi.Name = "panelShokenGigi";
            this.panelShokenGigi.Size = new System.Drawing.Size(179, 56);
            this.panelShokenGigi.TabIndex = 2;
            // 
            // checkBoxF5
            // 
            this.checkBoxF5.AutoSize = true;
            this.checkBoxF5.Location = new System.Drawing.Point(138, 20);
            this.checkBoxF5.Name = "checkBoxF5";
            this.checkBoxF5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxF5.TabIndex = 5;
            this.checkBoxF5.Text = "5";
            this.checkBoxF5.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "前月";
            // 
            // checkBoxF4
            // 
            this.checkBoxF4.AutoSize = true;
            this.checkBoxF4.Location = new System.Drawing.Point(108, 20);
            this.checkBoxF4.Name = "checkBoxF4";
            this.checkBoxF4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxF4.TabIndex = 4;
            this.checkBoxF4.Text = "4";
            this.checkBoxF4.UseVisualStyleBackColor = true;
            // 
            // checkBoxF3
            // 
            this.checkBoxF3.AutoSize = true;
            this.checkBoxF3.Location = new System.Drawing.Point(78, 20);
            this.checkBoxF3.Name = "checkBoxF3";
            this.checkBoxF3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxF3.TabIndex = 3;
            this.checkBoxF3.Text = "3";
            this.checkBoxF3.UseVisualStyleBackColor = true;
            // 
            // checkBoxF2
            // 
            this.checkBoxF2.AutoSize = true;
            this.checkBoxF2.Location = new System.Drawing.Point(48, 20);
            this.checkBoxF2.Name = "checkBoxF2";
            this.checkBoxF2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxF2.TabIndex = 2;
            this.checkBoxF2.Text = "2";
            this.checkBoxF2.UseVisualStyleBackColor = true;
            // 
            // radioButtonGigiChushi
            // 
            this.radioButtonGigiChushi.AutoSize = true;
            this.radioButtonGigiChushi.Location = new System.Drawing.Point(122, 37);
            this.radioButtonGigiChushi.Name = "radioButtonGigiChushi";
            this.radioButtonGigiChushi.Size = new System.Drawing.Size(47, 16);
            this.radioButtonGigiChushi.TabIndex = 7;
            this.radioButtonGigiChushi.TabStop = true;
            this.radioButtonGigiChushi.Text = "中止";
            this.radioButtonGigiChushi.UseVisualStyleBackColor = true;
            // 
            // checkBoxF1
            // 
            this.checkBoxF1.AutoSize = true;
            this.checkBoxF1.Location = new System.Drawing.Point(18, 20);
            this.checkBoxF1.Name = "checkBoxF1";
            this.checkBoxF1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxF1.TabIndex = 1;
            this.checkBoxF1.Text = "1";
            this.checkBoxF1.UseVisualStyleBackColor = true;
            // 
            // radioButtonGigiTenki
            // 
            this.radioButtonGigiTenki.AutoSize = true;
            this.radioButtonGigiTenki.Location = new System.Drawing.Point(50, 37);
            this.radioButtonGigiTenki.Name = "radioButtonGigiTenki";
            this.radioButtonGigiTenki.Size = new System.Drawing.Size(66, 16);
            this.radioButtonGigiTenki.TabIndex = 6;
            this.radioButtonGigiTenki.TabStop = true;
            this.radioButtonGigiTenki.Text = "転帰なし";
            this.radioButtonGigiTenki.UseVisualStyleBackColor = true;
            // 
            // panelShokai
            // 
            this.panelShokai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelShokai.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelShokai.Controls.Add(this.radioButtonShokaiAteNashi);
            this.panelShokai.Controls.Add(this.radioButtonShokaiSouiNashi);
            this.panelShokai.Controls.Add(this.radioButtonShokaiSouiAri);
            this.panelShokai.Controls.Add(this.radioButtonShokaiMihenshin);
            this.panelShokai.Controls.Add(this.textBoxShokaiMemo);
            this.panelShokai.Controls.Add(this.label3);
            this.panelShokai.Controls.Add(this.checkBoxOther);
            this.panelShokai.Controls.Add(this.checkBoxTotal);
            this.panelShokai.Controls.Add(this.checkBoxClinic);
            this.panelShokai.Controls.Add(this.checkBoxPeriod);
            this.panelShokai.Controls.Add(this.checkBoxDayCount);
            this.panelShokai.Controls.Add(this.checkBoxBuiCount);
            this.panelShokai.Enabled = false;
            this.panelShokai.Location = new System.Drawing.Point(0, 63);
            this.panelShokai.Name = "panelShokai";
            this.panelShokai.Size = new System.Drawing.Size(300, 91);
            this.panelShokai.TabIndex = 2;
            // 
            // radioButtonShokaiAteNashi
            // 
            this.radioButtonShokaiAteNashi.AutoSize = true;
            this.radioButtonShokaiAteNashi.Location = new System.Drawing.Point(78, 73);
            this.radioButtonShokaiAteNashi.Name = "radioButtonShokaiAteNashi";
            this.radioButtonShokaiAteNashi.Size = new System.Drawing.Size(66, 16);
            this.radioButtonShokaiAteNashi.TabIndex = 9;
            this.radioButtonShokaiAteNashi.TabStop = true;
            this.radioButtonShokaiAteNashi.Text = "宛所なし";
            this.radioButtonShokaiAteNashi.UseVisualStyleBackColor = true;
            // 
            // radioButtonShokaiSouiNashi
            // 
            this.radioButtonShokaiSouiNashi.AutoSize = true;
            this.radioButtonShokaiSouiNashi.Location = new System.Drawing.Point(144, 73);
            this.radioButtonShokaiSouiNashi.Name = "radioButtonShokaiSouiNashi";
            this.radioButtonShokaiSouiNashi.Size = new System.Drawing.Size(66, 16);
            this.radioButtonShokaiSouiNashi.TabIndex = 10;
            this.radioButtonShokaiSouiNashi.TabStop = true;
            this.radioButtonShokaiSouiNashi.Text = "相違なし";
            this.radioButtonShokaiSouiNashi.UseVisualStyleBackColor = true;
            // 
            // radioButtonShokaiSouiAri
            // 
            this.radioButtonShokaiSouiAri.AutoSize = true;
            this.radioButtonShokaiSouiAri.Location = new System.Drawing.Point(210, 73);
            this.radioButtonShokaiSouiAri.Name = "radioButtonShokaiSouiAri";
            this.radioButtonShokaiSouiAri.Size = new System.Drawing.Size(65, 16);
            this.radioButtonShokaiSouiAri.TabIndex = 11;
            this.radioButtonShokaiSouiAri.TabStop = true;
            this.radioButtonShokaiSouiAri.Text = "相違あり";
            this.radioButtonShokaiSouiAri.UseVisualStyleBackColor = true;
            // 
            // radioButtonShokaiMihenshin
            // 
            this.radioButtonShokaiMihenshin.AutoSize = true;
            this.radioButtonShokaiMihenshin.Location = new System.Drawing.Point(19, 73);
            this.radioButtonShokaiMihenshin.Name = "radioButtonShokaiMihenshin";
            this.radioButtonShokaiMihenshin.Size = new System.Drawing.Size(59, 16);
            this.radioButtonShokaiMihenshin.TabIndex = 8;
            this.radioButtonShokaiMihenshin.TabStop = true;
            this.radioButtonShokaiMihenshin.Text = "未返信";
            this.radioButtonShokaiMihenshin.UseVisualStyleBackColor = true;
            // 
            // textBoxShokaiMemo
            // 
            this.textBoxShokaiMemo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShokaiMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxShokaiMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxShokaiMemo.Location = new System.Drawing.Point(3, 34);
            this.textBoxShokaiMemo.Multiline = true;
            this.textBoxShokaiMemo.Name = "textBoxShokaiMemo";
            this.textBoxShokaiMemo.Size = new System.Drawing.Size(294, 38);
            this.textBoxShokaiMemo.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "照会理由";
            // 
            // checkBoxOther
            // 
            this.checkBoxOther.AutoSize = true;
            this.checkBoxOther.Location = new System.Drawing.Point(225, 18);
            this.checkBoxOther.Name = "checkBoxOther";
            this.checkBoxOther.Size = new System.Drawing.Size(55, 16);
            this.checkBoxOther.TabIndex = 6;
            this.checkBoxOther.Text = "その他";
            this.checkBoxOther.UseVisualStyleBackColor = true;
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Location = new System.Drawing.Point(140, 3);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(72, 16);
            this.checkBoxTotal.TabIndex = 2;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            // 
            // checkBoxClinic
            // 
            this.checkBoxClinic.AutoSize = true;
            this.checkBoxClinic.Location = new System.Drawing.Point(140, 18);
            this.checkBoxClinic.Name = "checkBoxClinic";
            this.checkBoxClinic.Size = new System.Drawing.Size(84, 16);
            this.checkBoxClinic.TabIndex = 5;
            this.checkBoxClinic.Text = "疑義施術所";
            this.checkBoxClinic.UseVisualStyleBackColor = true;
            // 
            // checkBoxPeriod
            // 
            this.checkBoxPeriod.AutoSize = true;
            this.checkBoxPeriod.Location = new System.Drawing.Point(63, 18);
            this.checkBoxPeriod.Name = "checkBoxPeriod";
            this.checkBoxPeriod.Size = new System.Drawing.Size(72, 16);
            this.checkBoxPeriod.TabIndex = 4;
            this.checkBoxPeriod.Text = "施術期間";
            this.checkBoxPeriod.UseVisualStyleBackColor = true;
            // 
            // checkBoxDayCount
            // 
            this.checkBoxDayCount.AutoSize = true;
            this.checkBoxDayCount.Location = new System.Drawing.Point(225, 3);
            this.checkBoxDayCount.Name = "checkBoxDayCount";
            this.checkBoxDayCount.Size = new System.Drawing.Size(72, 16);
            this.checkBoxDayCount.TabIndex = 3;
            this.checkBoxDayCount.Text = "施術日数";
            this.checkBoxDayCount.UseVisualStyleBackColor = true;
            // 
            // checkBoxBuiCount
            // 
            this.checkBoxBuiCount.AutoSize = true;
            this.checkBoxBuiCount.Location = new System.Drawing.Point(63, 3);
            this.checkBoxBuiCount.Name = "checkBoxBuiCount";
            this.checkBoxBuiCount.Size = new System.Drawing.Size(60, 16);
            this.checkBoxBuiCount.TabIndex = 1;
            this.checkBoxBuiCount.Text = "部位数";
            this.checkBoxBuiCount.UseVisualStyleBackColor = true;
            // 
            // panelHenrei
            // 
            this.panelHenrei.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHenrei.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelHenrei.Controls.Add(this.panelHenReason);
            this.panelHenrei.Controls.Add(this.panelHenBui);
            this.panelHenrei.Controls.Add(this.label4);
            this.panelHenrei.Controls.Add(this.checkBoxHenKago);
            this.panelHenrei.Controls.Add(this.checkBoxHenOther);
            this.panelHenrei.Controls.Add(this.checkBoxHenWork);
            this.panelHenrei.Controls.Add(this.checkBoxHenKega);
            this.panelHenrei.Controls.Add(this.checkBoxHenDays);
            this.panelHenrei.Controls.Add(this.checkBoxHenJiki);
            this.panelHenrei.Enabled = false;
            this.panelHenrei.Location = new System.Drawing.Point(0, 622);
            this.panelHenrei.Name = "panelHenrei";
            this.panelHenrei.Size = new System.Drawing.Size(300, 88);
            this.panelHenrei.TabIndex = 6;
            // 
            // panelHenReason
            // 
            this.panelHenReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHenReason.Controls.Add(this.checkBoxHenReason5);
            this.panelHenReason.Controls.Add(this.checkBoxHenReason2);
            this.panelHenReason.Controls.Add(this.checkBoxHenReason4);
            this.panelHenReason.Controls.Add(this.checkBoxHenReason1);
            this.panelHenReason.Controls.Add(this.checkBoxHenReason3);
            this.panelHenReason.Controls.Add(this.checkBoxHenReason);
            this.panelHenReason.Location = new System.Drawing.Point(9, 35);
            this.panelHenReason.Name = "panelHenReason";
            this.panelHenReason.Size = new System.Drawing.Size(283, 17);
            this.panelHenReason.TabIndex = 1;
            // 
            // checkBoxHenReason5
            // 
            this.checkBoxHenReason5.AutoSize = true;
            this.checkBoxHenReason5.Enabled = false;
            this.checkBoxHenReason5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxHenReason5.Name = "checkBoxHenReason5";
            this.checkBoxHenReason5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenReason5.TabIndex = 5;
            this.checkBoxHenReason5.Text = "5";
            this.checkBoxHenReason5.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason2
            // 
            this.checkBoxHenReason2.AutoSize = true;
            this.checkBoxHenReason2.Enabled = false;
            this.checkBoxHenReason2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxHenReason2.Name = "checkBoxHenReason2";
            this.checkBoxHenReason2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenReason2.TabIndex = 2;
            this.checkBoxHenReason2.Text = "2";
            this.checkBoxHenReason2.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason4
            // 
            this.checkBoxHenReason4.AutoSize = true;
            this.checkBoxHenReason4.Enabled = false;
            this.checkBoxHenReason4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxHenReason4.Name = "checkBoxHenReason4";
            this.checkBoxHenReason4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenReason4.TabIndex = 4;
            this.checkBoxHenReason4.Text = "4";
            this.checkBoxHenReason4.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason1
            // 
            this.checkBoxHenReason1.AutoSize = true;
            this.checkBoxHenReason1.Enabled = false;
            this.checkBoxHenReason1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxHenReason1.Name = "checkBoxHenReason1";
            this.checkBoxHenReason1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenReason1.TabIndex = 1;
            this.checkBoxHenReason1.Text = "1";
            this.checkBoxHenReason1.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason3
            // 
            this.checkBoxHenReason3.AutoSize = true;
            this.checkBoxHenReason3.Enabled = false;
            this.checkBoxHenReason3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxHenReason3.Name = "checkBoxHenReason3";
            this.checkBoxHenReason3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenReason3.TabIndex = 3;
            this.checkBoxHenReason3.Text = "3";
            this.checkBoxHenReason3.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenReason
            // 
            this.checkBoxHenReason.AutoSize = true;
            this.checkBoxHenReason.Location = new System.Drawing.Point(6, 1);
            this.checkBoxHenReason.Name = "checkBoxHenReason";
            this.checkBoxHenReason.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenReason.TabIndex = 0;
            this.checkBoxHenReason.Text = "負傷原因";
            this.checkBoxHenReason.UseVisualStyleBackColor = true;
            this.checkBoxHenReason.CheckedChanged += new System.EventHandler(this.checkBoxHenReason_CheckedChanged);
            // 
            // panelHenBui
            // 
            this.panelHenBui.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHenBui.Controls.Add(this.checkBoxHenBui5);
            this.panelHenBui.Controls.Add(this.checkBoxHenBui2);
            this.panelHenBui.Controls.Add(this.checkBoxHenBui4);
            this.panelHenBui.Controls.Add(this.checkBoxHenBui1);
            this.panelHenBui.Controls.Add(this.checkBoxHenBui3);
            this.panelHenBui.Controls.Add(this.checkBoxHenBui);
            this.panelHenBui.Location = new System.Drawing.Point(9, 17);
            this.panelHenBui.Name = "panelHenBui";
            this.panelHenBui.Size = new System.Drawing.Size(283, 17);
            this.panelHenBui.TabIndex = 0;
            // 
            // checkBoxHenBui5
            // 
            this.checkBoxHenBui5.AutoSize = true;
            this.checkBoxHenBui5.Enabled = false;
            this.checkBoxHenBui5.Location = new System.Drawing.Point(232, 1);
            this.checkBoxHenBui5.Name = "checkBoxHenBui5";
            this.checkBoxHenBui5.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenBui5.TabIndex = 5;
            this.checkBoxHenBui5.Text = "5";
            this.checkBoxHenBui5.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui2
            // 
            this.checkBoxHenBui2.AutoSize = true;
            this.checkBoxHenBui2.Enabled = false;
            this.checkBoxHenBui2.Location = new System.Drawing.Point(136, 1);
            this.checkBoxHenBui2.Name = "checkBoxHenBui2";
            this.checkBoxHenBui2.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenBui2.TabIndex = 2;
            this.checkBoxHenBui2.Text = "2";
            this.checkBoxHenBui2.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui4
            // 
            this.checkBoxHenBui4.AutoSize = true;
            this.checkBoxHenBui4.Enabled = false;
            this.checkBoxHenBui4.Location = new System.Drawing.Point(200, 1);
            this.checkBoxHenBui4.Name = "checkBoxHenBui4";
            this.checkBoxHenBui4.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenBui4.TabIndex = 4;
            this.checkBoxHenBui4.Text = "4";
            this.checkBoxHenBui4.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui1
            // 
            this.checkBoxHenBui1.AutoSize = true;
            this.checkBoxHenBui1.Enabled = false;
            this.checkBoxHenBui1.Location = new System.Drawing.Point(103, 1);
            this.checkBoxHenBui1.Name = "checkBoxHenBui1";
            this.checkBoxHenBui1.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenBui1.TabIndex = 1;
            this.checkBoxHenBui1.Text = "1";
            this.checkBoxHenBui1.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui3
            // 
            this.checkBoxHenBui3.AutoSize = true;
            this.checkBoxHenBui3.Enabled = false;
            this.checkBoxHenBui3.Location = new System.Drawing.Point(168, 1);
            this.checkBoxHenBui3.Name = "checkBoxHenBui3";
            this.checkBoxHenBui3.Size = new System.Drawing.Size(30, 16);
            this.checkBoxHenBui3.TabIndex = 3;
            this.checkBoxHenBui3.Text = "3";
            this.checkBoxHenBui3.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenBui
            // 
            this.checkBoxHenBui.AutoSize = true;
            this.checkBoxHenBui.Location = new System.Drawing.Point(6, 1);
            this.checkBoxHenBui.Name = "checkBoxHenBui";
            this.checkBoxHenBui.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenBui.TabIndex = 0;
            this.checkBoxHenBui.Text = "負傷部位";
            this.checkBoxHenBui.UseVisualStyleBackColor = true;
            this.checkBoxHenBui.CheckedChanged += new System.EventHandler(this.checkBoxHenBui_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "返戻/架電理由";
            // 
            // checkBoxHenKago
            // 
            this.checkBoxHenKago.AutoSize = true;
            this.checkBoxHenKago.Location = new System.Drawing.Point(15, 70);
            this.checkBoxHenKago.Name = "checkBoxHenKago";
            this.checkBoxHenKago.Size = new System.Drawing.Size(48, 16);
            this.checkBoxHenKago.TabIndex = 5;
            this.checkBoxHenKago.Text = "過誤";
            this.checkBoxHenKago.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenOther
            // 
            this.checkBoxHenOther.AutoSize = true;
            this.checkBoxHenOther.Location = new System.Drawing.Point(203, 70);
            this.checkBoxHenOther.Name = "checkBoxHenOther";
            this.checkBoxHenOther.Size = new System.Drawing.Size(55, 16);
            this.checkBoxHenOther.TabIndex = 7;
            this.checkBoxHenOther.Text = "その他";
            this.checkBoxHenOther.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenWork
            // 
            this.checkBoxHenWork.AutoSize = true;
            this.checkBoxHenWork.Location = new System.Drawing.Point(107, 70);
            this.checkBoxHenWork.Name = "checkBoxHenWork";
            this.checkBoxHenWork.Size = new System.Drawing.Size(60, 16);
            this.checkBoxHenWork.TabIndex = 6;
            this.checkBoxHenWork.Text = "勤務中";
            this.checkBoxHenWork.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenKega
            // 
            this.checkBoxHenKega.AutoSize = true;
            this.checkBoxHenKega.Location = new System.Drawing.Point(15, 53);
            this.checkBoxHenKega.Name = "checkBoxHenKega";
            this.checkBoxHenKega.Size = new System.Drawing.Size(68, 16);
            this.checkBoxHenKega.TabIndex = 2;
            this.checkBoxHenKega.Text = "けが以外";
            this.checkBoxHenKega.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenDays
            // 
            this.checkBoxHenDays.AutoSize = true;
            this.checkBoxHenDays.Location = new System.Drawing.Point(107, 53);
            this.checkBoxHenDays.Name = "checkBoxHenDays";
            this.checkBoxHenDays.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenDays.TabIndex = 3;
            this.checkBoxHenDays.Text = "受療日数";
            this.checkBoxHenDays.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenJiki
            // 
            this.checkBoxHenJiki.AutoSize = true;
            this.checkBoxHenJiki.Location = new System.Drawing.Point(203, 53);
            this.checkBoxHenJiki.Name = "checkBoxHenJiki";
            this.checkBoxHenJiki.Size = new System.Drawing.Size(72, 16);
            this.checkBoxHenJiki.TabIndex = 4;
            this.checkBoxHenJiki.Text = "負傷時期";
            this.checkBoxHenJiki.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel1.Controls.Add(this.textBoxOutMemo);
            this.panel1.Controls.Add(this.textBoxMemo);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(0, 755);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 101);
            this.panel1.TabIndex = 8;
            // 
            // textBoxOutMemo
            // 
            this.textBoxOutMemo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxOutMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxOutMemo.Location = new System.Drawing.Point(32, 41);
            this.textBoxOutMemo.Multiline = true;
            this.textBoxOutMemo.Name = "textBoxOutMemo";
            this.textBoxOutMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutMemo.Size = new System.Drawing.Size(266, 58);
            this.textBoxOutMemo.TabIndex = 9;
            // 
            // textBoxMemo
            // 
            this.textBoxMemo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMemo.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxMemo.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxMemo.Location = new System.Drawing.Point(32, 2);
            this.textBoxMemo.Multiline = true;
            this.textBoxMemo.Name = "textBoxMemo";
            this.textBoxMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMemo.Size = new System.Drawing.Size(266, 38);
            this.textBoxMemo.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 24);
            this.label16.TabIndex = 0;
            this.label16.Text = "出力\r\nメモ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "メモ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Window;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.checkBoxPaid, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.checkBoxTel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.checkBoxInputError, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 21);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(300, 22);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // checkBoxPaid
            // 
            this.checkBoxPaid.AutoSize = true;
            this.checkBoxPaid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxPaid.Location = new System.Drawing.Point(119, 1);
            this.checkBoxPaid.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxPaid.Name = "checkBoxPaid";
            this.checkBoxPaid.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxPaid.Size = new System.Drawing.Size(58, 20);
            this.checkBoxPaid.TabIndex = 3;
            this.checkBoxPaid.Text = "払済";
            this.checkBoxPaid.UseVisualStyleBackColor = true;
            this.checkBoxPaid.CheckedChanged += new System.EventHandler(this.checkBoxPaid_CheckedChanged);
            // 
            // checkBoxTel
            // 
            this.checkBoxTel.AutoSize = true;
            this.checkBoxTel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxTel.Location = new System.Drawing.Point(60, 1);
            this.checkBoxTel.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxTel.Name = "checkBoxTel";
            this.checkBoxTel.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxTel.Size = new System.Drawing.Size(58, 20);
            this.checkBoxTel.TabIndex = 2;
            this.checkBoxTel.Text = "架電";
            this.checkBoxTel.UseVisualStyleBackColor = true;
            this.checkBoxTel.CheckedChanged += new System.EventHandler(this.checkBoxTel_CheckedChanged);
            // 
            // checkBoxInputError
            // 
            this.checkBoxInputError.AutoSize = true;
            this.checkBoxInputError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxInputError.Location = new System.Drawing.Point(1, 1);
            this.checkBoxInputError.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxInputError.Name = "checkBoxInputError";
            this.checkBoxInputError.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxInputError.Size = new System.Drawing.Size(58, 20);
            this.checkBoxInputError.TabIndex = 0;
            this.checkBoxInputError.Text = "エラー";
            this.checkBoxInputError.UseVisualStyleBackColor = true;
            this.checkBoxInputError.CheckedChanged += new System.EventHandler(this.checkBoxInputError_CheckedChanged);
            // 
            // panelContact
            // 
            this.panelContact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContact.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelContact.Controls.Add(this.textBoxContactName);
            this.panelContact.Controls.Add(this.textBoxContactMin);
            this.panelContact.Controls.Add(this.textBoxContactD);
            this.panelContact.Controls.Add(this.textBoxContactH);
            this.panelContact.Controls.Add(this.textBoxContactM);
            this.panelContact.Controls.Add(this.textBoxContactY);
            this.panelContact.Controls.Add(this.label14);
            this.panelContact.Controls.Add(this.label10);
            this.panelContact.Controls.Add(this.label13);
            this.panelContact.Controls.Add(this.label12);
            this.panelContact.Controls.Add(this.label11);
            this.panelContact.Controls.Add(this.label9);
            this.panelContact.Controls.Add(this.checkBoxContactOrg);
            this.panelContact.Controls.Add(this.checkBoxContactNo);
            this.panelContact.Controls.Add(this.label5);
            this.panelContact.Enabled = false;
            this.panelContact.Location = new System.Drawing.Point(0, 708);
            this.panelContact.Name = "panelContact";
            this.panelContact.Size = new System.Drawing.Size(300, 45);
            this.panelContact.TabIndex = 7;
            // 
            // textBoxContactName
            // 
            this.textBoxContactName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxContactName.Location = new System.Drawing.Point(177, 23);
            this.textBoxContactName.Name = "textBoxContactName";
            this.textBoxContactName.Size = new System.Drawing.Size(103, 19);
            this.textBoxContactName.TabIndex = 13;
            // 
            // textBoxContactMin
            // 
            this.textBoxContactMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxContactMin.Location = new System.Drawing.Point(272, 2);
            this.textBoxContactMin.MaxLength = 2;
            this.textBoxContactMin.Name = "textBoxContactMin";
            this.textBoxContactMin.Size = new System.Drawing.Size(24, 19);
            this.textBoxContactMin.TabIndex = 9;
            // 
            // textBoxContactD
            // 
            this.textBoxContactD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxContactD.Location = new System.Drawing.Point(197, 2);
            this.textBoxContactD.MaxLength = 2;
            this.textBoxContactD.Name = "textBoxContactD";
            this.textBoxContactD.Size = new System.Drawing.Size(24, 19);
            this.textBoxContactD.TabIndex = 6;
            // 
            // textBoxContactH
            // 
            this.textBoxContactH.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxContactH.Location = new System.Drawing.Point(240, 2);
            this.textBoxContactH.MaxLength = 2;
            this.textBoxContactH.Name = "textBoxContactH";
            this.textBoxContactH.Size = new System.Drawing.Size(24, 19);
            this.textBoxContactH.TabIndex = 7;
            // 
            // textBoxContactM
            // 
            this.textBoxContactM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxContactM.Location = new System.Drawing.Point(162, 2);
            this.textBoxContactM.MaxLength = 2;
            this.textBoxContactM.Name = "textBoxContactM";
            this.textBoxContactM.Size = new System.Drawing.Size(24, 19);
            this.textBoxContactM.TabIndex = 4;
            // 
            // textBoxContactY
            // 
            this.textBoxContactY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxContactY.Location = new System.Drawing.Point(119, 2);
            this.textBoxContactY.MaxLength = 4;
            this.textBoxContactY.Name = "textBoxContactY";
            this.textBoxContactY.Size = new System.Drawing.Size(32, 19);
            this.textBoxContactY.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(280, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 12;
            this.label14.Text = "様";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(136, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "了承者";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(265, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(7, 12);
            this.label13.TabIndex = 8;
            this.label13.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(186, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 12);
            this.label12.TabIndex = 5;
            this.label12.Text = "/";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(151, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "/";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(89, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 1;
            this.label9.Text = "西暦";
            // 
            // checkBoxContactOrg
            // 
            this.checkBoxContactOrg.AutoSize = true;
            this.checkBoxContactOrg.Location = new System.Drawing.Point(85, 25);
            this.checkBoxContactOrg.Name = "checkBoxContactOrg";
            this.checkBoxContactOrg.Size = new System.Drawing.Size(48, 16);
            this.checkBoxContactOrg.TabIndex = 11;
            this.checkBoxContactOrg.Text = "団体";
            this.checkBoxContactOrg.UseVisualStyleBackColor = true;
            // 
            // checkBoxContactNo
            // 
            this.checkBoxContactNo.AutoSize = true;
            this.checkBoxContactNo.Location = new System.Drawing.Point(13, 25);
            this.checkBoxContactNo.Name = "checkBoxContactNo";
            this.checkBoxContactNo.Size = new System.Drawing.Size(72, 16);
            this.checkBoxContactNo.TabIndex = 10;
            this.checkBoxContactNo.Text = "連絡不要";
            this.checkBoxContactNo.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "連絡記録";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel2.Controls.Add(this.buttonRelationAppShow);
            this.panel2.Controls.Add(this.labelRefRece);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Location = new System.Drawing.Point(0, 576);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 21);
            this.panel2.TabIndex = 4;
            // 
            // buttonRelationAppShow
            // 
            this.buttonRelationAppShow.Location = new System.Drawing.Point(219, 0);
            this.buttonRelationAppShow.Name = "buttonRelationAppShow";
            this.buttonRelationAppShow.Size = new System.Drawing.Size(77, 21);
            this.buttonRelationAppShow.TabIndex = 1;
            this.buttonRelationAppShow.Text = "表示/変更";
            this.buttonRelationAppShow.UseVisualStyleBackColor = true;
            this.buttonRelationAppShow.Click += new System.EventHandler(this.buttonRelationAppShow_Click);
            // 
            // labelRefRece
            // 
            this.labelRefRece.AutoSize = true;
            this.labelRefRece.Location = new System.Drawing.Point(76, 5);
            this.labelRefRece.Name = "labelRefRece";
            this.labelRefRece.Size = new System.Drawing.Size(24, 12);
            this.labelRefRece.TabIndex = 0;
            this.labelRefRece.Text = "なし";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "参考申請書";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Window;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.checkBoxProcess1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxProcess2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxProcess3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxProcess4, 3, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 41);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(300, 22);
            this.tableLayoutPanel3.TabIndex = 10;
            // 
            // checkBoxProcess1
            // 
            this.checkBoxProcess1.AutoSize = true;
            this.checkBoxProcess1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxProcess1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxProcess1.Location = new System.Drawing.Point(1, 1);
            this.checkBoxProcess1.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxProcess1.Name = "checkBoxProcess1";
            this.checkBoxProcess1.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxProcess1.Size = new System.Drawing.Size(58, 20);
            this.checkBoxProcess1.TabIndex = 12;
            this.checkBoxProcess1.Text = "処理1";
            this.checkBoxProcess1.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess2
            // 
            this.checkBoxProcess2.AutoSize = true;
            this.checkBoxProcess2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxProcess2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxProcess2.Location = new System.Drawing.Point(60, 1);
            this.checkBoxProcess2.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxProcess2.Name = "checkBoxProcess2";
            this.checkBoxProcess2.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxProcess2.Size = new System.Drawing.Size(58, 20);
            this.checkBoxProcess2.TabIndex = 15;
            this.checkBoxProcess2.Text = "処理2";
            this.checkBoxProcess2.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess3
            // 
            this.checkBoxProcess3.AutoSize = true;
            this.checkBoxProcess3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxProcess3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxProcess3.Location = new System.Drawing.Point(119, 1);
            this.checkBoxProcess3.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxProcess3.Name = "checkBoxProcess3";
            this.checkBoxProcess3.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxProcess3.Size = new System.Drawing.Size(58, 20);
            this.checkBoxProcess3.TabIndex = 14;
            this.checkBoxProcess3.Text = "処理3";
            this.checkBoxProcess3.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess4
            // 
            this.checkBoxProcess4.AutoSize = true;
            this.checkBoxProcess4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxProcess4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxProcess4.Location = new System.Drawing.Point(178, 1);
            this.checkBoxProcess4.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxProcess4.Name = "checkBoxProcess4";
            this.checkBoxProcess4.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.checkBoxProcess4.Size = new System.Drawing.Size(58, 20);
            this.checkBoxProcess4.TabIndex = 17;
            this.checkBoxProcess4.Text = "処理4";
            this.checkBoxProcess4.UseVisualStyleBackColor = true;
            // 
            // panelKagoOryoUchiwake
            // 
            this.panelKagoOryoUchiwake.Controls.Add(this.checkBoxKagoOryoUchiwake);
            this.panelKagoOryoUchiwake.Controls.Add(this.checkBoxKagoOryoUchiwakeMis);
            this.panelKagoOryoUchiwake.Controls.Add(this.checkBoxKagoOryoUchiwakeNone);
            this.panelKagoOryoUchiwake.Location = new System.Drawing.Point(3, 209);
            this.panelKagoOryoUchiwake.Name = "panelKagoOryoUchiwake";
            this.panelKagoOryoUchiwake.Size = new System.Drawing.Size(283, 17);
            this.panelKagoOryoUchiwake.TabIndex = 66;
            // 
            // checkBoxKagoOryoUchiwake
            // 
            this.checkBoxKagoOryoUchiwake.AutoSize = true;
            this.checkBoxKagoOryoUchiwake.Location = new System.Drawing.Point(6, 2);
            this.checkBoxKagoOryoUchiwake.Name = "checkBoxKagoOryoUchiwake";
            this.checkBoxKagoOryoUchiwake.Size = new System.Drawing.Size(84, 16);
            this.checkBoxKagoOryoUchiwake.TabIndex = 73;
            this.checkBoxKagoOryoUchiwake.Text = "往療内訳書";
            this.checkBoxKagoOryoUchiwake.UseVisualStyleBackColor = true;
            this.checkBoxKagoOryoUchiwake.CheckedChanged += new System.EventHandler(this.checkBoxKagoOryoUchiwake_CheckedChanged);
            // 
            // checkBoxKagoOryoUchiwakeMis
            // 
            this.checkBoxKagoOryoUchiwakeMis.AutoSize = true;
            this.checkBoxKagoOryoUchiwakeMis.Enabled = false;
            this.checkBoxKagoOryoUchiwakeMis.Location = new System.Drawing.Point(232, 2);
            this.checkBoxKagoOryoUchiwakeMis.Name = "checkBoxKagoOryoUchiwakeMis";
            this.checkBoxKagoOryoUchiwakeMis.Size = new System.Drawing.Size(48, 16);
            this.checkBoxKagoOryoUchiwakeMis.TabIndex = 75;
            this.checkBoxKagoOryoUchiwakeMis.Text = "不備";
            this.checkBoxKagoOryoUchiwakeMis.UseVisualStyleBackColor = true;
            // 
            // checkBoxKagoOryoUchiwakeNone
            // 
            this.checkBoxKagoOryoUchiwakeNone.AutoSize = true;
            this.checkBoxKagoOryoUchiwakeNone.Enabled = false;
            this.checkBoxKagoOryoUchiwakeNone.Location = new System.Drawing.Point(168, 2);
            this.checkBoxKagoOryoUchiwakeNone.Name = "checkBoxKagoOryoUchiwakeNone";
            this.checkBoxKagoOryoUchiwakeNone.Size = new System.Drawing.Size(60, 16);
            this.checkBoxKagoOryoUchiwakeNone.TabIndex = 74;
            this.checkBoxKagoOryoUchiwakeNone.Text = "添付無";
            this.checkBoxKagoOryoUchiwakeNone.UseVisualStyleBackColor = true;
            // 
            // InspectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.panelHenrei);
            this.Controls.Add(this.panelShokai);
            this.Controls.Add(this.panelTenken);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelContact);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelOryo);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Enabled = false;
            this.MinimumSize = new System.Drawing.Size(300, 858);
            this.Name = "InspectControl";
            this.Size = new System.Drawing.Size(300, 858);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panelOryo.ResumeLayout(false);
            this.panelOryo.PerformLayout();
            this.panelTenken.ResumeLayout(false);
            this.panelTenken.PerformLayout();
            this.groupBoxKago.ResumeLayout(false);
            this.groupBoxKago.PerformLayout();
            this.panelKagoOuryo.ResumeLayout(false);
            this.panelKagoOuryo.PerformLayout();
            this.panelKagoDoui.ResumeLayout(false);
            this.panelKagoDoui.PerformLayout();
            this.panelKagoHoukoku.ResumeLayout(false);
            this.panelKagoHoukoku.PerformLayout();
            this.panelKagoJotai.ResumeLayout(false);
            this.panelKagoJotai.PerformLayout();
            this.panelKagoLongDiff.ResumeLayout(false);
            this.panelKagoLongDiff.PerformLayout();
            this.panelKagoDokuzi.ResumeLayout(false);
            this.panelKagoDokuzi.PerformLayout();
            this.panelKagoDiff.ResumeLayout(false);
            this.panelKagoDiff.PerformLayout();
            this.panelKagoLong.ResumeLayout(false);
            this.panelKagoLong.PerformLayout();
            this.panelKagoGenin.ResumeLayout(false);
            this.panelKagoGenin.PerformLayout();
            this.groupBoxSaishinsa.ResumeLayout(false);
            this.groupBoxSaishinsa.PerformLayout();
            this.panelShokenGigi2.ResumeLayout(false);
            this.panelShokenGigi2.PerformLayout();
            this.panelShokenGigi.ResumeLayout(false);
            this.panelShokenGigi.PerformLayout();
            this.panelShokai.ResumeLayout(false);
            this.panelShokai.PerformLayout();
            this.panelHenrei.ResumeLayout(false);
            this.panelHenrei.PerformLayout();
            this.panelHenReason.ResumeLayout(false);
            this.panelHenReason.PerformLayout();
            this.panelHenBui.ResumeLayout(false);
            this.panelHenBui.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panelContact.ResumeLayout(false);
            this.panelContact.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panelKagoOryoUchiwake.ResumeLayout(false);
            this.panelKagoOryoUchiwake.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBoxShokai;
        private System.Windows.Forms.CheckBox checkBoxTenken;
        private System.Windows.Forms.CheckBox checkBoxOryo;
        private System.Windows.Forms.CheckBox checkBoxHenrei;
        private System.Windows.Forms.Panel panelOryo;
        private System.Windows.Forms.Panel panelTenken;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonKago;
        private System.Windows.Forms.GroupBox groupBoxKago;
        private System.Windows.Forms.CheckBox checkBoxKagoShomei;
        private System.Windows.Forms.CheckBox checkBoxKagoHisseki;
        private System.Windows.Forms.CheckBox checkBoxKagoLong;
        private System.Windows.Forms.CheckBox checkBoxKagoFamily;
        private System.Windows.Forms.CheckBox checkBoxKagoGenin;
        private System.Windows.Forms.CheckBox checkBoxKagoOther;
        private System.Windows.Forms.RadioButton radioButtonSai;
        private System.Windows.Forms.TextBox textBoxTenkenMemo;
        private System.Windows.Forms.RadioButton radioButtonNone;
        private System.Windows.Forms.RadioButton radioButtonPend;
        private System.Windows.Forms.GroupBox groupBoxSaishinsa;
        private System.Windows.Forms.Panel panelShokenGigi2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButtonGigiDoitsu;
        private System.Windows.Forms.RadioButton radioButtonGigiBetsu;
        private System.Windows.Forms.RadioButton radioButtonSaiOther;
        private System.Windows.Forms.RadioButton radioButtonGigiShoken;
        private System.Windows.Forms.Panel panelShokenGigi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBoxF4;
        private System.Windows.Forms.CheckBox checkBoxF3;
        private System.Windows.Forms.CheckBox checkBoxF5;
        private System.Windows.Forms.CheckBox checkBoxF2;
        private System.Windows.Forms.RadioButton radioButtonGigiChushi;
        private System.Windows.Forms.CheckBox checkBoxF1;
        private System.Windows.Forms.RadioButton radioButtonGigiTenki;
        private System.Windows.Forms.RadioButton radioButtonOryoNo;
        private System.Windows.Forms.RadioButton radioButtonOryoYes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelShokai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxOther;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.CheckBox checkBoxClinic;
        private System.Windows.Forms.CheckBox checkBoxPeriod;
        private System.Windows.Forms.CheckBox checkBoxDayCount;
        private System.Windows.Forms.CheckBox checkBoxBuiCount;
        private System.Windows.Forms.Panel panelHenrei;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxHenOther;
        private System.Windows.Forms.CheckBox checkBoxHenWork;
        private System.Windows.Forms.CheckBox checkBoxHenKega;
        private System.Windows.Forms.CheckBox checkBoxHenBui;
        private System.Windows.Forms.CheckBox checkBoxHenReason;
        private System.Windows.Forms.CheckBox checkBoxHenDays;
        private System.Windows.Forms.CheckBox checkBoxHenJiki;
        private System.Windows.Forms.TextBox textBoxShokaiMemo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxMemo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxPend;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox checkBoxInputError;
        private System.Windows.Forms.RadioButton radioButtonShokaiAteNashi;
        private System.Windows.Forms.RadioButton radioButtonShokaiSouiNashi;
        private System.Windows.Forms.RadioButton radioButtonShokaiSouiAri;
        private System.Windows.Forms.RadioButton radioButtonShokaiMihenshin;
        private System.Windows.Forms.CheckBox checkBoxPaid;
        private System.Windows.Forms.CheckBox checkBoxTel;
        private System.Windows.Forms.CheckBox checkBoxHenKago;
        private System.Windows.Forms.Panel panelHenReason;
        private System.Windows.Forms.CheckBox checkBoxHenReason5;
        private System.Windows.Forms.CheckBox checkBoxHenReason2;
        private System.Windows.Forms.CheckBox checkBoxHenReason4;
        private System.Windows.Forms.CheckBox checkBoxHenReason1;
        private System.Windows.Forms.CheckBox checkBoxHenReason3;
        private System.Windows.Forms.Panel panelHenBui;
        private System.Windows.Forms.CheckBox checkBoxHenBui5;
        private System.Windows.Forms.CheckBox checkBoxHenBui2;
        private System.Windows.Forms.CheckBox checkBoxHenBui4;
        private System.Windows.Forms.CheckBox checkBoxHenBui1;
        private System.Windows.Forms.CheckBox checkBoxHenBui3;
        private System.Windows.Forms.Panel panelKagoDiff;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff5;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff2;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff4;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff1;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff3;
        private System.Windows.Forms.CheckBox checkBoxKagoDiff;
        private System.Windows.Forms.Panel panelKagoLong;
        private System.Windows.Forms.CheckBox checkBoxKagoLong5;
        private System.Windows.Forms.CheckBox checkBoxKagoLong2;
        private System.Windows.Forms.CheckBox checkBoxKagoLong4;
        private System.Windows.Forms.CheckBox checkBoxKagoLong1;
        private System.Windows.Forms.CheckBox checkBoxKagoLong3;
        private System.Windows.Forms.Panel panelKagoGenin;
        private System.Windows.Forms.CheckBox checkBoxKagoGen5;
        private System.Windows.Forms.CheckBox checkBoxKagoGen2;
        private System.Windows.Forms.CheckBox checkBoxKagoGen4;
        private System.Windows.Forms.CheckBox checkBoxKagoGen1;
        private System.Windows.Forms.CheckBox checkBoxKagoGen3;
        private System.Windows.Forms.Panel panelContact;
        private System.Windows.Forms.TextBox textBoxContactName;
        private System.Windows.Forms.TextBox textBoxContactMin;
        private System.Windows.Forms.TextBox textBoxContactD;
        private System.Windows.Forms.TextBox textBoxContactH;
        private System.Windows.Forms.TextBox textBoxContactM;
        private System.Windows.Forms.TextBox textBoxContactY;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBoxContactOrg;
        private System.Windows.Forms.CheckBox checkBoxContactNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonRelationAppShow;
        private System.Windows.Forms.Label labelRefRece;
        private System.Windows.Forms.TextBox textBoxOutMemo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.CheckBox checkBoxProcess1;
        private System.Windows.Forms.CheckBox checkBoxProcess2;
        private System.Windows.Forms.CheckBox checkBoxProcess3;
        private System.Windows.Forms.CheckBox checkBoxProcess4;
        private System.Windows.Forms.CheckBox checkBoxKagoOuryoNoReason;
        private System.Windows.Forms.CheckBox checkBoxKagoOuryoOver16km;
        private System.Windows.Forms.CheckBox checkBoxKagoShometsu;
        private System.Windows.Forms.CheckBox checkBoxKagoInsSoui;
        private System.Windows.Forms.CheckBox checkBoxKagoHonke;
        private System.Windows.Forms.Panel panelKagoLongDiff;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff5;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff2;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff4;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff1;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff3;
        private System.Windows.Forms.CheckBox checkBoxKagoLongDiff;
        private System.Windows.Forms.Panel panelKagoDokuzi;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi5;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi2;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi4;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi1;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi3;
        private System.Windows.Forms.CheckBox checkBoxKagoDokuzi;
        private System.Windows.Forms.CheckBox checkBoxKagoDoui;
        private System.Windows.Forms.CheckBox checkBoxKagoJotai;
        private System.Windows.Forms.CheckBox checkBoxKagoJotaiNone;
        private System.Windows.Forms.CheckBox checkBoxKagoDouiMis;
        private System.Windows.Forms.CheckBox checkBoxKagoJotaiMis;
        private System.Windows.Forms.CheckBox checkBoxKagoDouiNone;
        private System.Windows.Forms.CheckBox checkBoxKagoHoukoku;
        private System.Windows.Forms.CheckBox checkBoxKagoHoukokuNone;
        private System.Windows.Forms.CheckBox checkBoxKagoHoukokuMis;
        private System.Windows.Forms.Panel panelKagoDoui;
        private System.Windows.Forms.Panel panelKagoHoukoku;
        private System.Windows.Forms.Panel panelKagoJotai;
        private System.Windows.Forms.Panel panelKagoOuryo;
        private System.Windows.Forms.Panel panelKagoOryoUchiwake;
        private System.Windows.Forms.CheckBox checkBoxKagoOryoUchiwake;
        private System.Windows.Forms.CheckBox checkBoxKagoOryoUchiwakeMis;
        private System.Windows.Forms.CheckBox checkBoxKagoOryoUchiwakeNone;
    }
}
