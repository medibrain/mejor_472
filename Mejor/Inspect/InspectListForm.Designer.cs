﻿namespace Mejor.Inspect
{
    partial class InspectListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxHenrei = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxOryoGigi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxOryo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxShokai = new System.Windows.Forms.TextBox();
            this.textBoxTenken = new System.Windows.Forms.TextBox();
            this.textBoxKago = new System.Windows.Forms.TextBox();
            this.labelY = new System.Windows.Forms.Label();
            this.textBoxSai = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxOryo = new System.Windows.Forms.CheckBox();
            this.checkBoxTenken = new System.Windows.Forms.CheckBox();
            this.buttonOryoInspect = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonInspect = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonInspectAHK = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxHenrei);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxOryoGigi);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxOryo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxShokai);
            this.panel1.Controls.Add(this.textBoxTenken);
            this.panel1.Controls.Add(this.textBoxKago);
            this.panel1.Controls.Add(this.labelY);
            this.panel1.Controls.Add(this.textBoxSai);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(975, 31);
            this.panel1.TabIndex = 0;
            // 
            // textBoxHenrei
            // 
            this.textBoxHenrei.Location = new System.Drawing.Point(902, 5);
            this.textBoxHenrei.Name = "textBoxHenrei";
            this.textBoxHenrei.ReadOnly = true;
            this.textBoxHenrei.Size = new System.Drawing.Size(40, 20);
            this.textBoxHenrei.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(865, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "返戻：";
            // 
            // textBoxOryoGigi
            // 
            this.textBoxOryoGigi.Location = new System.Drawing.Point(803, 7);
            this.textBoxOryoGigi.Name = "textBoxOryoGigi";
            this.textBoxOryoGigi.ReadOnly = true;
            this.textBoxOryoGigi.Size = new System.Drawing.Size(40, 20);
            this.textBoxOryoGigi.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(742, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "往療疑義：";
            // 
            // textBoxOryo
            // 
            this.textBoxOryo.Location = new System.Drawing.Point(680, 7);
            this.textBoxOryo.Name = "textBoxOryo";
            this.textBoxOryo.ReadOnly = true;
            this.textBoxOryo.Size = new System.Drawing.Size(40, 20);
            this.textBoxOryo.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(619, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "往点対象：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "照会：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(286, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "点検対象：";
            // 
            // textBoxShokai
            // 
            this.textBoxShokai.Location = new System.Drawing.Point(224, 7);
            this.textBoxShokai.Name = "textBoxShokai";
            this.textBoxShokai.ReadOnly = true;
            this.textBoxShokai.Size = new System.Drawing.Size(40, 20);
            this.textBoxShokai.TabIndex = 2;
            // 
            // textBoxTenken
            // 
            this.textBoxTenken.Location = new System.Drawing.Point(347, 7);
            this.textBoxTenken.Name = "textBoxTenken";
            this.textBoxTenken.ReadOnly = true;
            this.textBoxTenken.Size = new System.Drawing.Size(40, 20);
            this.textBoxTenken.TabIndex = 4;
            // 
            // textBoxKago
            // 
            this.textBoxKago.Location = new System.Drawing.Point(446, 7);
            this.textBoxKago.Name = "textBoxKago";
            this.textBoxKago.ReadOnly = true;
            this.textBoxKago.Size = new System.Drawing.Size(40, 20);
            this.textBoxKago.TabIndex = 6;
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(12, 10);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(123, 13);
            this.labelY.TabIndex = 0;
            this.labelY.Text = "平成27年度　月処理分";
            // 
            // textBoxSai
            // 
            this.textBoxSai.Location = new System.Drawing.Point(557, 7);
            this.textBoxSai.Name = "textBoxSai";
            this.textBoxSai.ReadOnly = true;
            this.textBoxSai.Size = new System.Drawing.Size(40, 20);
            this.textBoxSai.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(409, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "過誤：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(508, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "再審査：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBoxOryo);
            this.panel2.Controls.Add(this.checkBoxTenken);
            this.panel2.Controls.Add(this.buttonOryoInspect);
            this.panel2.Controls.Add(this.buttonExit);
            this.panel2.Controls.Add(this.buttonInspectAHK);
            this.panel2.Controls.Add(this.buttonInspect);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 789);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(975, 37);
            this.panel2.TabIndex = 2;
            // 
            // checkBoxOryo
            // 
            this.checkBoxOryo.AutoSize = true;
            this.checkBoxOryo.Location = new System.Drawing.Point(87, 11);
            this.checkBoxOryo.Name = "checkBoxOryo";
            this.checkBoxOryo.Size = new System.Drawing.Size(95, 17);
            this.checkBoxOryo.TabIndex = 3;
            this.checkBoxOryo.Text = "往療点検のみ";
            this.checkBoxOryo.UseVisualStyleBackColor = true;
            this.checkBoxOryo.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxTenken
            // 
            this.checkBoxTenken.AutoSize = true;
            this.checkBoxTenken.Location = new System.Drawing.Point(12, 11);
            this.checkBoxTenken.Name = "checkBoxTenken";
            this.checkBoxTenken.Size = new System.Drawing.Size(71, 17);
            this.checkBoxTenken.TabIndex = 3;
            this.checkBoxTenken.Text = "点検のみ";
            this.checkBoxTenken.UseVisualStyleBackColor = true;
            this.checkBoxTenken.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // buttonOryoInspect
            // 
            this.buttonOryoInspect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOryoInspect.Location = new System.Drawing.Point(429, 7);
            this.buttonOryoInspect.Name = "buttonOryoInspect";
            this.buttonOryoInspect.Size = new System.Drawing.Size(99, 25);
            this.buttonOryoInspect.TabIndex = 1;
            this.buttonOryoInspect.Text = "往療点検開始";
            this.buttonOryoInspect.UseVisualStyleBackColor = true;
            this.buttonOryoInspect.Click += new System.EventHandler(this.buttonOryoInspect_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExit.Location = new System.Drawing.Point(761, 7);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 25);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonInspect
            // 
            this.buttonInspect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInspect.Location = new System.Drawing.Point(337, 7);
            this.buttonInspect.Name = "buttonInspect";
            this.buttonInspect.Size = new System.Drawing.Size(86, 25);
            this.buttonInspect.TabIndex = 0;
            this.buttonInspect.Text = "点検開始";
            this.buttonInspect.UseVisualStyleBackColor = true;
            this.buttonInspect.Click += new System.EventHandler(this.buttonInspect_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 31);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(975, 758);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // buttonInspectAHK
            // 
            this.buttonInspectAHK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInspectAHK.Location = new System.Drawing.Point(214, 7);
            this.buttonInspectAHK.Name = "buttonInspectAHK";
            this.buttonInspectAHK.Size = new System.Drawing.Size(117, 25);
            this.buttonInspectAHK.TabIndex = 0;
            this.buttonInspectAHK.Text = "あはき点検開始";
            this.buttonInspectAHK.UseVisualStyleBackColor = true;
            this.buttonInspectAHK.Click += new System.EventHandler(this.buttonInspectAHK_Click);
            // 
            // InspectListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 826);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "InspectListForm";
            this.Text = "点検リスト";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBoxKago;
        private System.Windows.Forms.TextBox textBoxSai;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonInspect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTenken;
        private System.Windows.Forms.Button buttonOryoInspect;
        private System.Windows.Forms.TextBox textBoxOryo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOryoGigi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxOryo;
        private System.Windows.Forms.CheckBox checkBoxTenken;
        private System.Windows.Forms.TextBox textBoxHenrei;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxShokai;
        private System.Windows.Forms.Button buttonInspectAHK;
    }
}