﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Mejor.Inspect
{
    public partial class InspectForm : Form
    {
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsSub = new BindingSource();

        private int relationAID;
        bool nowSetting = false;

        public InspectForm(int cym, GroupInspectStatus g)
        {
            InitializeComponent();
            //labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100)}年度{cym % 100}月処理分";
            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            var l = App.GetAppsGID(g.GroupID);
            l = l.FindAll(a => a.StatusFlagCheck(StatusFlag.点検対象));
            bsApp.DataSource = l;
            dgv.DataSource = bsApp;
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            bsSub.CurrentChanged += BsSub_CurrentChanged;

            this.Shown += InspectForm_Shown;

            //初回のみイベントが発生しないため
            appRead();
        }




        /// <summary>
        /// 20201008165204 furukawa aidのみでリスト作成画面からロード
        /// </summary>
        /// <param name="aid"></param>
        public InspectForm(int aid)
        {
            InitializeComponent();
            //labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100)}年度{cym % 100}月処理分";
            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            var l = App.GetApp(aid);

            //l = l.FindAll(a => a.StatusFlagCheck(StatusFlag.点検対象));

            bsApp.DataSource = l;
            dgv.DataSource = bsApp;
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            bsSub.CurrentChanged += BsSub_CurrentChanged;

            this.Shown += InspectForm_Shown;

            //初回のみイベントが発生しないため
            appRead();
        }



        private void InspectForm_Shown(object sender, EventArgs e)
        {
            foreach (Control c in userControlImageSub.Controls)
            {
                if (c is PictureBox pb)
                {
                    pb.Paint += Pb_Paint;
                    break;
                }
            }
        }
        private void Pb_Paint(object sender, PaintEventArgs e)
        {
            var app = (App)bsSub.Current;
            if (app == null) return;
            if (relationAID != app.Aid) return;

            var p = PointToScreen(ClientRectangle.Location);
            p.Offset(30, 30);
            p = ((PictureBox)sender).PointToClient(p);

            var g = e.Graphics;
            using (var f = new Font(Font.FontFamily, 50, FontStyle.Bold))
            using (var b = new SolidBrush(Color.FromArgb(140, 255, 0, 0)))
            {
                g.DrawString("参考申請書", f, b, p);
            }
        }


        private void BsSub_CurrentChanged(object sender, EventArgs e)
        {
            nowSetting = true;
            var currentSubApp = (App)bsSub.Current;
            string fn = currentSubApp?.GetImageFullPath() ?? string.Empty;
            buttonSubNext.Enabled = bsSub.Position != (bsSub.Count - 1);
            buttonSubPrev.Enabled = bsSub.Position != 0;

            //20210129103129 furukawa st ////////////////////////
            //参考レセがnullの場合に例外発生するのを回避
            
            if (currentSubApp != null) 
                checkBoxRelation.Checked = relationAID == currentSubApp.Aid;

            //checkBoxRelation.Checked = relationAID == currentSubApp.Aid;
            //20210129103129 furukawa ed ////////////////////////




            nowSetting = false;

            try
            {
                userControlImageSub.SetPictureFile(fn);
            }
            catch
            {
                MessageBox.Show("サブ画像表示でエラーが発生しました");
                return;
            }

            labelSubCnt.Text = "(" + (bsSub.Position + 1) + "/" + bsSub.Count + ")";
            if (bsSub.Count > 0)
            {
                labelSubCnt.BackColor = Color.Aqua;
            }
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            appRead();
        }

        /// <summary>
        /// 次のApplicationに移ります（すべての操作）
        /// </summary>
        /// <returns></returns>
        private bool appRead()
        {
            userControlImageSub.Clear();
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //次のApplicationを表示
            relationAID = app.TaggedDatas.RelationAID;
            inspectControl1.SetApp(app);
            displayImageRefresh();

            ////選択されたAppと同じ被保番のAppを検索、リストと画像を表示
            sameHnumSearch(app);

            return true;
        }

        /// <summary>
        /// メイン画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void displayImageRefresh()
        {
            var app = (App)bsApp.Current;

            string fn = app.GetImageFullPath();

            try
            {
                userControlImageMain.SetPictureFile(fn);
                userControlImageMain.SetPictureBoxFill();
            }
            catch
            {
                MessageBox.Show("メイン画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 被保険者番号で該当年のAppを検索
        /// </summary>
        /// <param name="hnum"></param>
        private void sameHnumSearch(App app)
        {
            //関連申請書に誕生日を検索条件としてつけるか
            //現在広域かどうかで判断
            var birthIgnore = Insurer.CurrrentInsurer.InsurerType == INSURER_TYPE.後期高齢;

            var subList = App.GetAppsFamily(app.HihoNum, app.CYM);
            if (subList == null) return;
            subList.RemoveAll(x => x.Aid == app.Aid);
            if (!birthIgnore) subList.RemoveAll(x => x.Birthday != app.Birthday);
            subList.Sort((x, y) => x.YM.CompareTo(y.YM));

            bsSub.DataSource = subList;
            bsSub.ResetBindings(false);

            var pogision = subList.FindLastIndex(a => a.YM <= app.YM);
            if (pogision == -1) bsSub.MoveLast();
            else bsSub.Position = pogision;

            if (bsSub.Count > 1)
            {
                if (bsSub.Position != 0) buttonSubPrev.Enabled = true;
                if (bsSub.Position != bsSub.Count - 1) buttonNext.Enabled = true;
            }
            labelSubCnt.Text = "(" + (bsSub.Position + 1) + "/" + bsSub.Count + ")";
            if (bsSub.Count > 0)
            {
                labelSubCnt.BackColor = Color.Aqua;
            }
        }

        private bool updateINQ()
        {
            try
            {
                var app = (App)bsApp.Current;

                if (!inspectControl1.CanUpdate(app, true)) return false;
                inspectControl1.DataSetToApp(app);
                app.Uinquiry = User.CurrentUser.UserID;
                app.UpdateINQ();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "\r\n\r\n申請書データの更新に失敗しました");
                return false;
            }

            bsApp.ResetBindings(false);
            return true;
        }

        /// <summary>
        /// 資料印刷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrint2_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null) return;

            var pn = Settings.InspectPrinterName;
            if (pn == string.Empty) pn = Settings.DefaultPrinterName;

            var ip = new ImagePrint(pn);
            ip.Print(app, true, false);
            
            //var currentSubApp = (App)bsSub?.Current;
            //if (currentSubApp == null)
            //{
            //    ip.DoPrint(app, true, false);
            //}
            //else if (app.Aid != currentSubApp.Aid)
            //{
            //    ip.DoPrint(app, true, false, currentSubApp);
            //}
            //else
            //{
            //    ip.DoPrint(app, true, false);
            //}
        }

        private void FormInspect_Load(object sender, EventArgs e)
        {
            for (int j = 0; j < dgv.ColumnCount; j++)
            {
                dgv.Columns[j].Visible = false;
                dgv.Columns[j].ReadOnly = true;
            }
            dgv.Columns[nameof(App.Aid)].Visible = true;
            dgv.Columns[nameof(App.Aid)].Width = 70;
            dgv.Columns[nameof(App.Aid)].HeaderText = "ID";
            dgv.Columns[nameof(App.MediYear)].Visible = true;
            dgv.Columns[nameof(App.MediYear)].Width = 25;
            dgv.Columns[nameof(App.MediYear)].HeaderText = "年";
            dgv.Columns[nameof(App.MediMonth)].Visible = true;
            dgv.Columns[nameof(App.MediMonth)].Width = 25;
            dgv.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dgv.Columns[nameof(App.HihoNum)].Visible = true;
            dgv.Columns[nameof(App.HihoNum)].Width = 70;
            dgv.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dgv.Columns[nameof(App.AppType)].Visible = true;
            dgv.Columns[nameof(App.AppType)].Width = 25;
            dgv.Columns[nameof(App.AppType)].HeaderText = "種";
            dgv.Columns[nameof(App.TenkenResult)].Visible = true;
            dgv.Columns[nameof(App.TenkenResult)].Width = 60;
            dgv.Columns[nameof(App.TenkenResult)].HeaderText = "点検結果";
            dgv.Columns[nameof(App.TenkenResult)].DisplayIndex = 3;
            dgv.Columns[nameof(App.DrNum)].Visible = true;
            dgv.Columns[nameof(App.DrNum)].Width = 70;
            dgv.Columns[nameof(App.DrNum)].HeaderText = "施術所No";
            dgv.Columns[nameof(App.Total)].Visible = true;
            dgv.Columns[nameof(App.Total)].Width = 60;
            dgv.Columns[nameof(App.Total)].HeaderText = "合計";

            if (dgv.Rows.Count == 0) return;
            dgv.CurrentCell = dgv[0, 0];
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentCellAddress.Y == 0) return;
            dgv.CurrentCell = dgv[0, dgv.CurrentCell.RowIndex - 1];
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            int ri = dgv.CurrentCell.RowIndex;

            if (!updateINQ())
            {
                MessageBox.Show("データベースの更新に失敗しました");
                return;
            }

            if (ri == dgv.RowCount - 1) return;
            dgv.SuspendLayout();
            dgv.CurrentCell = dgv[0, ri + 1];
            dgv.ResumeLayout();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImageMain.SetPictureBoxFill();
        }

        private void buttonImageFillSub_Click(object sender, EventArgs e)
        {
            userControlImageSub.SetPictureBoxFill();
        }

        private void FormInspect_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                //もし点検結果にチェックがされていればデータをUpdateしてから次を読み込む
                if (!updateINQ()) return;
                bsApp.MoveNext();
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                bsApp.MovePrevious();
            }
        }

        private void buttonSubNext_Click(object sender, EventArgs e)
        {
            bsSub.MoveNext();
        }

        private void buttonSubPrev_Click(object sender, EventArgs e)
        {
            bsSub.MovePrevious();
        }

        private void FormInspect_SizeChanged(object sender, EventArgs e)
        {
            int H = this.Height - SystemInformation.CaptionHeight;
            int W = this.Width - SystemInformation.FrameBorderSize.Width * 2;
            int subW = (int)((float)(W - panelRight.Width) / 2f);
            int mainW = W - subW - panelRight.Width;

            panelLeft.Size = new Size(subW, H);
            userControlImageMain.Size = new Size(mainW, H);
        }

        private void プリンター選択ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new PrinterSelectForm(Settings.InspectPrinterName))
            {
                f.ShowDialog();
                Settings.InspectPrinterName = f.SelectPrinterName;
            }
        }

        private void buttonNextImage_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null) return;
            using (var f = new NextImageForm(app)) f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var app = (App)bsSub.Current;
            if (app == null) return;
            using (var f = new NextImageForm(app)) f.ShowDialog();
        }

        private void checkBoxRelation_CheckedChanged(object sender, EventArgs e)
        {
            if (nowSetting) return;
            if (checkBoxRelation.Checked)
            {
                var subApp = (App)bsSub.Current;
                if (subApp == null)
                {
                    checkBoxRelation.Checked = false;
                    return;
                }
                relationAID = subApp.Aid;
                inspectControl1.RelationAid = relationAID;
            }
            else
            {
                relationAID = 0;
                inspectControl1.RelationAid = 0;
            }

            userControlImageSub.Refresh();
        }



        //20201013154436 furukawa st ////////////////////////
        //リストダブルクリックで入力画面（1回目）へ
        
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null) return;
            InputStarter.Start(app.GroupID, INPUT_TYPE.First, app.Aid);
        }
        //20201013154436 furukawa ed ////////////////////////
    }
}
