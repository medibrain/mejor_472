﻿namespace Mejor.Ibarakishi
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewRefrece = new System.Windows.Forms.DataGridView();
            this.dataGridViewKokuho = new System.Windows.Forms.DataGridView();
            this.buttonExcel = new System.Windows.Forms.Button();
            this.buttonCSV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRefrece)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKokuho)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewRefrece
            // 
            this.dataGridViewRefrece.AllowUserToAddRows = false;
            this.dataGridViewRefrece.AllowUserToDeleteRows = false;
            this.dataGridViewRefrece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRefrece.Location = new System.Drawing.Point(52, 27);
            this.dataGridViewRefrece.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewRefrece.Name = "dataGridViewRefrece";
            this.dataGridViewRefrece.Size = new System.Drawing.Size(363, 133);
            this.dataGridViewRefrece.TabIndex = 0;
            // 
            // dataGridViewKokuho
            // 
            this.dataGridViewKokuho.AllowUserToAddRows = false;
            this.dataGridViewKokuho.AllowUserToDeleteRows = false;
            this.dataGridViewKokuho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewKokuho.Location = new System.Drawing.Point(52, 211);
            this.dataGridViewKokuho.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewKokuho.Name = "dataGridViewKokuho";
            this.dataGridViewKokuho.Size = new System.Drawing.Size(363, 131);
            this.dataGridViewKokuho.TabIndex = 0;
            // 
            // buttonExcel
            // 
            this.buttonExcel.Location = new System.Drawing.Point(457, 55);
            this.buttonExcel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(156, 74);
            this.buttonExcel.TabIndex = 1;
            this.buttonExcel.Text = "あはき提供データ\r\nインポート";
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttonExcel_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Location = new System.Drawing.Point(457, 235);
            this.buttonCSV.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(156, 67);
            this.buttonCSV.TabIndex = 1;
            this.buttonCSV.Text = "柔整国保システムCSV\r\nインポート";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 377);
            this.Controls.Add(this.buttonCSV);
            this.Controls.Add(this.buttonExcel);
            this.Controls.Add(this.dataGridViewKokuho);
            this.Controls.Add(this.dataGridViewRefrece);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormImport";
            this.Text = "FormImport";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRefrece)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewKokuho)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewRefrece;
        private System.Windows.Forms.DataGridView dataGridViewKokuho;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.Button buttonCSV;
    }
}