﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Ibarakishi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);


        //20210610115251 furukawa st ////////////////////////
        //あはき用調整
        
        Point posTotalAHK = new Point(1000, 800);
        //      Point posTotalAHK = new Point(1000, 1000);
        //20210610115251 furukawa ed ////////////////////////

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;
      
        /// <summary>
        /// 国保データ柔整
        /// </summary>
        List<kokuhodata> lstKokuho = new List<kokuhodata>();

        /// <summary>
        /// あはき用提供データ
        /// </summary>
        List<dataimport_ahk> lstAHK = new List<dataimport_ahk>();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        /// <summary>
        /// グループのナンバリングを持っておく
        /// </summary>
        Dictionary<int, string> dicNumbering = new Dictionary<int, string>();

        /// <summary>
        /// 同一申請書の続紙カウンタ
        /// </summary>
        int NumberingForZokushi = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount, verifyBoxFamily, verifyBoxNumbering,pBirthday,verifyBoxSex};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 };

            douiConts = new Control[] { pDoui };
           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //20210705180352 furukawa st ////////////////////////
            //ここを通さなくてもBsApp_CurrentChangedかShownで通るので削除

            //if (app != null)
            //{
            //    if (scan.AppType == APP_TYPE.柔整)
            //    {
            //        //国保データのリスト（柔整）                
            //        lstKokuho = kokuhodata.select(app.CYM);
            //    }
            //    else if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            //    {
            //        //柔整リスト
            //        lstAHK = dataimport_ahk.select($"cym={app.CYM}");
            //    }

            //    setApp(app);
            //}        
            //20210705180352 furukawa ed ////////////////////////


            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
            //20210705173557 furukawa st ////////////////////////
            //AID検索からきたとき最初の表示時、再度表示させないとコントロールの状態がリセットされグリッドがきっちり反映できない
            
            App app = (App)bsApp.Current;
            if (app != null)
            {
                if (scan.AppType == APP_TYPE.柔整)
                {
                    //国保データのリスト（柔整）                
                    lstKokuho = kokuhodata.select(app.CYM);
                }
                else if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                {
                    //柔整リスト
                    lstAHK = dataimport_ahk.select($"cym={app.CYM}");
                }

                setApp(app);
            }
            //20210705173557 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            if(verifyBoxNumbering.Visible) intPrevNumbering = verifyBoxNumbering.Text == string.Empty ? 0 :int.Parse(verifyBoxNumbering.Text);
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //20211007104943 furukawa st ////////////////////////
            //InitControl関数に処理を任せる
            
            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期:InitControl(APP_TYPE.長期);break; //ヘッダ
                case clsInputKind.続紙:InitControl(APP_TYPE.続紙);break; // "--"://続紙
                case clsInputKind.不要:InitControl(APP_TYPE.不要);break; //"++"://不要
                case clsInputKind.エラー:InitControl(APP_TYPE.エラー);break;//**
                case clsInputKind.施術同意書裏:InitControl(APP_TYPE.同意書裏);break;// "902"://施術同意書裏
                case clsInputKind.施術報告書:InitControl(APP_TYPE.施術報告書);break;// "911"://施術報告書/
                case clsInputKind.状態記入書:InitControl(APP_TYPE.状態記入書);break;// "921"://状態記入書
                case clsInputKind.施術同意書:InitControl(APP_TYPE.同意書); break;// "901"://施術同意書


                default:
                   
                    switch(scan.AppType)
                    {
                        case APP_TYPE.柔整:InitControl(APP_TYPE.柔整);break;
                        case APP_TYPE.あんま:InitControl(APP_TYPE.あんま);break;
                        case APP_TYPE.鍼灸:InitControl(APP_TYPE.鍼灸);break;
                    }
                 
                    break;
            }


            #region old
            //Control[] ignoreControls = new Control[] { labelHs, labelYear,
            //    verifyBoxY, labelInputerName, };

            //phnum.Visible = false;
            //pDoui.Visible = false;
            //panelunder.Visible = false;
            //dgv.Visible = false;

            //switch (verifyBoxY.Text)
            //{
            //    case clsInputKind.長期://ヘッダ
            //    case clsInputKind.続紙:// "--"://続紙
            //    case clsInputKind.不要://"++"://不要
            //    case clsInputKind.エラー:
            //    case clsInputKind.施術同意書裏:// "902"://施術同意書裏
            //    case clsInputKind.施術報告書:// "911"://施術報告書/
            //    case clsInputKind.状態記入書:// "921"://状態記入書

            //        break;

            //    case clsInputKind.施術同意書:// "901"://施術同意書

            //        pDoui.Visible = true;
            //        break;

            //    default:
            //        phnum.Visible = true;
            //        pDoui.Visible = true;
            //        panelunder.Visible = true;

            //        //20211007104943 furukawa st ////////////////////////
            //        //柔整のみ提供データリスト必要                    
            //        if (scan.AppType == APP_TYPE.柔整) dgv.Visible = true;
            //        //      dgv.Visible = true;
            //        //20211007104943 furukawa ed ////////////////////////

            //        break;
            //}

            #endregion

            //20211007104943 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            

            //国保データ
            if (lstKokuho.Count == 0) return;

            kokuhodata k = lstKokuho[0];
            #region width
            dgv.Columns[nameof(k.f000_kokuhoid)].Width = 50;             //シーケンス番号
            dgv.Columns[nameof(k.f001_hihonum)].Width = 80;             //証番号　全角想定
            dgv.Columns[nameof(k.f002_pname)].Width = 100;               //氏名
            dgv.Columns[nameof(k.f003_shinryoym)].Width = 80;            //診療年月 和暦想定
            dgv.Columns[nameof(k.f004_sid)].Width = 100;                 //機関コード
            dgv.Columns[nameof(k.f005_sname)].Width = 150;               //医療機関名
            dgv.Columns[nameof(k.f006_setainum)].Width = 40;             //世帯番号
            dgv.Columns[nameof(k.f007_atenanum)].Width = 40;             //宛名番号
            dgv.Columns[nameof(k.f008_pbirthday)].Width = 100;           //生年月日 和暦想定
            dgv.Columns[nameof(k.f009_pgender)].Width = 40;              //性別
            dgv.Columns[nameof(k.f010_ratio)].Width = 40;                //給付割合
            dgv.Columns[nameof(k.f011_counteddays)].Width = 40;          //実日数
            dgv.Columns[nameof(k.f012_total)].Width = 80;                //決定点数
            dgv.Columns[nameof(k.f013_score_kbn)].Width = 40;             //点数表
            dgv.Columns[nameof(k.f014_honke)].Width = 40;                 //本家
            dgv.Columns[nameof(k.f015_nyugai)].Width = 40;                //入外
            dgv.Columns[nameof(k.f016_hihomark)].Width = 140;              //証記号
            dgv.Columns[nameof(k.f017)].Width = 40;                       //種別１
            dgv.Columns[nameof(k.f018)].Width = 40;                       //種別２
            dgv.Columns[nameof(k.f019)].Width = 40;                       //診療・療養費別
            dgv.Columns[nameof(k.f020_shinsaym)].Width = 60;              //審査年月
            dgv.Columns[nameof(k.f021_insnum)].Width = 40;                //保険者番号
            dgv.Columns[nameof(k.f022_comnum)].Width = 150;                //レセプト全国共通キー
            dgv.Columns[nameof(k.f023_dcode)].Width = 40;                 //処方機関コード
            dgv.Columns[nameof(k.f024_dname)].Width = 40;                 //処方機関名
            dgv.Columns[nameof(k.f025)].Width = 40;                        //クリ
            dgv.Columns[nameof(k.f026)].Width = 40;                        //ケア
            dgv.Columns[nameof(k.f027)].Width = 40;                        //容認
            dgv.Columns[nameof(k.f028_newest)].Width = 40;                 //最新履歴
            dgv.Columns[nameof(k.f029)].Width = 40;                        //原本所在
            dgv.Columns[nameof(k.f030_prefcorss)].Width = 40;              //県内外
            dgv.Columns[nameof(k.f031)].Width = 40;                        //療養費種別
            dgv.Columns[nameof(k.f032)].Width = 40;                        //食事基準額
            dgv.Columns[nameof(k.f033)].Width = 40;                        //不当
            dgv.Columns[nameof(k.f034)].Width = 40;                        //第三者
            dgv.Columns[nameof(k.f035_kyufu_limit)].Width = 40;            //給付制限
            dgv.Columns[nameof(k.f036_expensive)].Width = 40;              //高額
            dgv.Columns[nameof(k.f037)].Width = 40;                        //予約
            dgv.Columns[nameof(k.f038)].Width = 40;                        //処理
            dgv.Columns[nameof(k.f039)].Width = 40;                        //疑義種別
            dgv.Columns[nameof(k.f040)].Width = 40;                        //参考
            dgv.Columns[nameof(k.f041)].Width = 40;                        //状態
            dgv.Columns[nameof(k.f042)].Width = 40;                        //コード情報
            dgv.Columns[nameof(k.f043)].Width = 40;                        //原本区分
            dgv.Columns[nameof(k.f044_fusen01)].Width = 40;               //付箋01
            dgv.Columns[nameof(k.f045_fusen02)].Width = 40;               //付箋02
            dgv.Columns[nameof(k.f046_fusen03)].Width = 40;               //付箋03
            dgv.Columns[nameof(k.f047_fusen04)].Width = 40;               //付箋04
            dgv.Columns[nameof(k.f048_fusen05)].Width = 40;               //付箋05
            dgv.Columns[nameof(k.f049_fusen06)].Width = 40;               //付箋06
            dgv.Columns[nameof(k.f050_fusen07)].Width = 40;               //付箋07
            dgv.Columns[nameof(k.f051_fusen08)].Width = 40;               //付箋08
            dgv.Columns[nameof(k.f052_fusen09)].Width = 40;               //付箋09
            dgv.Columns[nameof(k.f053_fusen10)].Width = 40;               //付箋10
            dgv.Columns[nameof(k.f054_fusen11)].Width = 40;               //付箋11
            dgv.Columns[nameof(k.f055_fusen12)].Width = 40;               //付箋12
            dgv.Columns[nameof(k.f056_fusen13)].Width = 40;               //付箋13
            dgv.Columns[nameof(k.f057_fusen14)].Width = 40;               //付箋14
            dgv.Columns[nameof(k.hihonum_narrow)].Width = 40;               //被保険者記号番号メホール用
            dgv.Columns[nameof(k.shinryoymad)].Width = 40;                //診療年月西暦メホール用
            dgv.Columns[nameof(k.shinsaymad)].Width = 40;                 //審査年月西暦メホール用
            dgv.Columns[nameof(k.birthdayad)].Width = 40;                 //生年月日西暦メホール用
            dgv.Columns[nameof(k.cym)].Width = 80;                        //メホール請求年月

            #endregion

            #region headertext
            dgv.Columns[nameof(k.f000_kokuhoid)].HeaderText = "rrid";
            dgv.Columns[nameof(k.f001_hihonum)].HeaderText = "証番号";
            dgv.Columns[nameof(k.f002_pname)].HeaderText = "氏名";
            dgv.Columns[nameof(k.f003_shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(k.f004_sid)].HeaderText = "機関コード";
            dgv.Columns[nameof(k.f005_sname)].HeaderText = "医療機関名";
            dgv.Columns[nameof(k.f006_setainum)].HeaderText = "世帯";
            dgv.Columns[nameof(k.f007_atenanum)].HeaderText = "宛名";
            dgv.Columns[nameof(k.f008_pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(k.f009_pgender)].HeaderText = "性別";
            dgv.Columns[nameof(k.f010_ratio)].HeaderText = "割合";
            dgv.Columns[nameof(k.f011_counteddays)].HeaderText = "日数";
            dgv.Columns[nameof(k.f012_total)].HeaderText = "決定";
            dgv.Columns[nameof(k.f013_score_kbn)].HeaderText = "点数表";
            dgv.Columns[nameof(k.f014_honke)].HeaderText = "本家";
            dgv.Columns[nameof(k.f015_nyugai)].HeaderText = "入外";
            dgv.Columns[nameof(k.f016_hihomark)].HeaderText = "証記号";
            dgv.Columns[nameof(k.f017)].HeaderText = "種別１";
            dgv.Columns[nameof(k.f018)].HeaderText = "種別２";
            dgv.Columns[nameof(k.f019)].HeaderText = "診療・療養費別";
            dgv.Columns[nameof(k.f020_shinsaym)].HeaderText = "審査年月";
            dgv.Columns[nameof(k.f021_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(k.f022_comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(k.f023_dcode)].HeaderText = "処方機関コード";
            dgv.Columns[nameof(k.f024_dname)].HeaderText = "処方機関名";
            dgv.Columns[nameof(k.f025)].HeaderText = "クリ";
            dgv.Columns[nameof(k.f026)].HeaderText = "ケア";
            dgv.Columns[nameof(k.f027)].HeaderText = "容認";
            dgv.Columns[nameof(k.f028_newest)].HeaderText = "最新履歴";
            dgv.Columns[nameof(k.f029)].HeaderText = "原本所在";
            dgv.Columns[nameof(k.f030_prefcorss)].HeaderText = "県内外";
            dgv.Columns[nameof(k.f031)].HeaderText = "療養費種別";
            dgv.Columns[nameof(k.f032)].HeaderText = "食事基準額";
            dgv.Columns[nameof(k.f033)].HeaderText = "不当";
            dgv.Columns[nameof(k.f034)].HeaderText = "第三者";
            dgv.Columns[nameof(k.f035_kyufu_limit)].HeaderText = "給付制限";
            dgv.Columns[nameof(k.f036_expensive)].HeaderText = "高額";
            dgv.Columns[nameof(k.f037)].HeaderText = "予約";
            dgv.Columns[nameof(k.f038)].HeaderText = "処理";
            dgv.Columns[nameof(k.f039)].HeaderText = "疑義種別";
            dgv.Columns[nameof(k.f040)].HeaderText = "参考";
            dgv.Columns[nameof(k.f041)].HeaderText = "状態";
            dgv.Columns[nameof(k.f042)].HeaderText = "コード情報";
            dgv.Columns[nameof(k.f043)].HeaderText = "原本区分";
            dgv.Columns[nameof(k.f044_fusen01)].HeaderText = "付箋01";
            dgv.Columns[nameof(k.f045_fusen02)].HeaderText = "付箋02";
            dgv.Columns[nameof(k.f046_fusen03)].HeaderText = "付箋03";
            dgv.Columns[nameof(k.f047_fusen04)].HeaderText = "付箋04";
            dgv.Columns[nameof(k.f048_fusen05)].HeaderText = "付箋05";
            dgv.Columns[nameof(k.f049_fusen06)].HeaderText = "付箋06";
            dgv.Columns[nameof(k.f050_fusen07)].HeaderText = "付箋07";
            dgv.Columns[nameof(k.f051_fusen08)].HeaderText = "付箋08";
            dgv.Columns[nameof(k.f052_fusen09)].HeaderText = "付箋09";
            dgv.Columns[nameof(k.f053_fusen10)].HeaderText = "付箋10";
            dgv.Columns[nameof(k.f054_fusen11)].HeaderText = "付箋11";
            dgv.Columns[nameof(k.f055_fusen12)].HeaderText = "付箋12";
            dgv.Columns[nameof(k.f056_fusen13)].HeaderText = "付箋13";
            dgv.Columns[nameof(k.f057_fusen14)].HeaderText = "付箋14";
            dgv.Columns[nameof(k.hihonum_narrow)].HeaderText = "被保険者記号番号メホール用";
            dgv.Columns[nameof(k.shinryoymad)].HeaderText = "診療年月西暦メホール用";
            dgv.Columns[nameof(k.shinsaymad)].HeaderText = "審査年月西暦メホール用";
            dgv.Columns[nameof(k.birthdayad)].HeaderText = "生年月日西暦メホール用";
            dgv.Columns[nameof(k.cym)].HeaderText = "メホール請求年月";
            #endregion

            #region visible

            dgv.Columns[nameof(k.f000_kokuhoid)].Visible = true;    //シーケンス
            dgv.Columns[nameof(k.f001_hihonum)].Visible = true;        //証番号　全角想定
            dgv.Columns[nameof(k.f002_pname)].Visible = true;          //氏名
            dgv.Columns[nameof(k.f003_shinryoym)].Visible = true;      //診療年月 和暦想定
            dgv.Columns[nameof(k.f004_sid)].Visible = true;            //機関コード
            dgv.Columns[nameof(k.f005_sname)].Visible = true;          //医療機関名
            dgv.Columns[nameof(k.f006_setainum)].Visible = false;       //世帯番号
            dgv.Columns[nameof(k.f007_atenanum)].Visible = false;       //宛名番号
            dgv.Columns[nameof(k.f008_pbirthday)].Visible = true;      //生年月日 和暦想定
            dgv.Columns[nameof(k.f009_pgender)].Visible = true;        //性別
            dgv.Columns[nameof(k.f010_ratio)].Visible = true;          //給付割合
            dgv.Columns[nameof(k.f011_counteddays)].Visible = true;    //実日数
            dgv.Columns[nameof(k.f012_total)].Visible = true;          //決定点数
            dgv.Columns[nameof(k.f013_score_kbn)].Visible = false;      //点数表
            dgv.Columns[nameof(k.f014_honke)].Visible = true;          //本家
            dgv.Columns[nameof(k.f015_nyugai)].Visible = true;         //入外
            dgv.Columns[nameof(k.f016_hihomark)].Visible = false;       //証記号
            dgv.Columns[nameof(k.f017)].Visible = false;                //種別１
            dgv.Columns[nameof(k.f018)].Visible = false;                //種別２
            dgv.Columns[nameof(k.f019)].Visible = false;                //診療・療養費別
            dgv.Columns[nameof(k.f020_shinsaym)].Visible = true;       //審査年月
            dgv.Columns[nameof(k.f021_insnum)].Visible = false;         //保険者番号
            dgv.Columns[nameof(k.f022_comnum)].Visible = true;         //レセプト全国共通キー
            dgv.Columns[nameof(k.f023_dcode)].Visible = false;          //処方機関コード
            dgv.Columns[nameof(k.f024_dname)].Visible = false;          //処方機関名
            dgv.Columns[nameof(k.f025)].Visible = false;               //クリ
            dgv.Columns[nameof(k.f026)].Visible = false;               //ケア
            dgv.Columns[nameof(k.f027)].Visible = false;               //容認
            dgv.Columns[nameof(k.f028_newest)].Visible = false;        //最新履歴
            dgv.Columns[nameof(k.f029)].Visible = false;               //原本所在
            dgv.Columns[nameof(k.f030_prefcorss)].Visible = false;     //県内外
            dgv.Columns[nameof(k.f031)].Visible = false;               //療養費種別
            dgv.Columns[nameof(k.f032)].Visible = false;               //食事基準額
            dgv.Columns[nameof(k.f033)].Visible = false;               //不当
            dgv.Columns[nameof(k.f034)].Visible = false;               //第三者
            dgv.Columns[nameof(k.f035_kyufu_limit)].Visible = false;   //給付制限
            dgv.Columns[nameof(k.f036_expensive)].Visible = false;     //高額
            dgv.Columns[nameof(k.f037)].Visible = false;               //予約
            dgv.Columns[nameof(k.f038)].Visible = false;               //処理
            dgv.Columns[nameof(k.f039)].Visible = false;               //疑義種別
            dgv.Columns[nameof(k.f040)].Visible = false;               //参考
            dgv.Columns[nameof(k.f041)].Visible = false;               //状態
            dgv.Columns[nameof(k.f042)].Visible = false;               //コード情報
            dgv.Columns[nameof(k.f043)].Visible = false;               //原本区分
            dgv.Columns[nameof(k.f044_fusen01)].Visible = false;       //付箋01
            dgv.Columns[nameof(k.f045_fusen02)].Visible = false;       //付箋02
            dgv.Columns[nameof(k.f046_fusen03)].Visible = false;       //付箋03
            dgv.Columns[nameof(k.f047_fusen04)].Visible = false;       //付箋04
            dgv.Columns[nameof(k.f048_fusen05)].Visible = false;       //付箋05
            dgv.Columns[nameof(k.f049_fusen06)].Visible = false;       //付箋06
            dgv.Columns[nameof(k.f050_fusen07)].Visible = false;       //付箋07
            dgv.Columns[nameof(k.f051_fusen08)].Visible = false;       //付箋08
            dgv.Columns[nameof(k.f052_fusen09)].Visible = false;       //付箋09
            dgv.Columns[nameof(k.f053_fusen10)].Visible = false;       //付箋10
            dgv.Columns[nameof(k.f054_fusen11)].Visible = false;       //付箋11
            dgv.Columns[nameof(k.f055_fusen12)].Visible = false;       //付箋12
            dgv.Columns[nameof(k.f056_fusen13)].Visible = false;       //付箋13
            dgv.Columns[nameof(k.f057_fusen14)].Visible = false;       //付箋14
            dgv.Columns[nameof(k.hihonum_narrow)].Visible = false;       //被保険者記号番号メホール用
            dgv.Columns[nameof(k.shinryoymad)].Visible = false;        //診療年月西暦メホール用
            dgv.Columns[nameof(k.shinsaymad)].Visible = false;         //審査年月西暦メホール用
            dgv.Columns[nameof(k.birthdayad)].Visible = false;         //生年月日西暦メホール用
            dgv.Columns[nameof(k.cym)].Visible = true;                 //メホール請求年月
            #endregion

            

        }


        /// <summary>
        /// 国保用グリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid_kokuho(App app = null)
        {
            
            List<kokuhodata> lstimp = new List<kokuhodata>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            DateTime dtbirth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);
            string strGender = verifyBoxSex.GetIntValue() == 1 ? "男" : "女";

            foreach (kokuhodata item in lstKokuho)
            {
                //被保番半角、メホール請求年月、合計金額、生年月日、性別、診療年月で探す
                if (item.hihonum_narrow == verifyBoxHnum.Text.Trim() &&
                    item.cym == scan.CYM &&
                    item.f012_total.Replace(",", "") == verifyBoxTotal.Text.ToString().Trim() &&
                    item.birthdayad == dtbirth &&
                    item.f009_pgender == strGender &&
                    item.shinryoymad == intymad) //20210513092016 furukawa 合致条件に診療年月追加


                {
                    lstimp.Add(item);
                }
            }


            dgv.DataSource = null;
            dgv.DataSource = lstimp;


            InitGrid();

            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();


            //20210705173817 furukawa st ////////////////////////
            //一度AUXに登録した後は、一意のキーが付いているのでそっちを呼ぶ

            List<Application_AUX> lstaux = Application_AUX.Select($" aid={app.Aid}");
            if (lstaux.Count > 0)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {

                    if (lstaux.Count == 1 &&
                          lstaux[0].matchingID01 == r.Cells["f000_kokuhoid"].Value.ToString())
                    {
                        r.Selected = true;
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        return;
                    }
                    else
                    {
                        r.Selected = false;
                    }

                }
            }
            //20210705173817 furukawa ed ////////////////////////







            if (app != null && app.RrID.ToString() != string.Empty)//&& app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {



                    int intRRid = int.Parse(r.Cells["f000_kokuhoid"].Value.ToString());

                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells["f022_comnum"].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (r.Cells["importid"].Value.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {

                        if (intTotal == int.Parse(verifyBoxTotal.Text.Trim()) && strhnum == verifyBoxHnum.Text.Trim())
                        {
                            //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;

                        }
                        //提供データIDと違う場合の処理抜け                        
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            //20200812164508 furukawa st ////////////////////////
            //マッチングOK条件に選択行が1行の場合も追加

            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            //else if (lstimp.Count == 1)
            //20200812164508 furukawa ed ////////////////////////
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }
            

        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid_AHK()
        {

            #region 要らなくなった
            dataimport_ahk data = new dataimport_ahk();

            #region width
            dgv.Columns[nameof(data.F000importid)].Width = 50;
            dgv.Columns[nameof(data.F001renban)].Width = 50;
            dgv.Columns[nameof(data.F002insnum)].Width = 80;
            dgv.Columns[nameof(data.F003kohifutan)].Width = 50;
            dgv.Columns[nameof(data.F004hnum)].Width = 100;
            dgv.Columns[nameof(data.F005kohijyukyu)].Width = 50;
            dgv.Columns[nameof(data.F006pbirthday)].Width = 80;
            dgv.Columns[nameof(data.F007pgender)].Width = 30;
            dgv.Columns[nameof(data.F008pname)].Width = 100;
            dgv.Columns[nameof(data.F009ym)].Width = 50;
            dgv.Columns[nameof(data.F010sid)].Width = 100;
            dgv.Columns[nameof(data.F011sregnumber)].Width = 100;
            dgv.Columns[nameof(data.F012shinsaym)].Width = 50;
            dgv.Columns[nameof(data.F013comnum)].Width = 130;
            dgv.Columns[nameof(data.F014)].Width = 50;
            dgv.Columns[nameof(data.F015)].Width = 50;
            dgv.Columns[nameof(data.pbirthdayad)].Width = 100;
            dgv.Columns[nameof(data.ymad)].Width = 50;
            dgv.Columns[nameof(data.shinsaymad)].Width = 50;
            dgv.Columns[nameof(data.cym)].Width = 50;
            dgv.Columns[nameof(data.hnum_narrow)].Width = 100;


            #endregion

            #region headertext
            dgv.Columns[nameof(data.F000importid)].HeaderText = "管理用ID";
            dgv.Columns[nameof(data.F001renban)].HeaderText = "No.";
            dgv.Columns[nameof(data.F002insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(data.F003kohifutan)].HeaderText = "公費負担者番号";
            dgv.Columns[nameof(data.F004hnum)].HeaderText = "被保険者証番号";
            dgv.Columns[nameof(data.F005kohijyukyu)].HeaderText = "公費受給者番号";
            dgv.Columns[nameof(data.F006pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(data.F007pgender)].HeaderText = "性別";
            dgv.Columns[nameof(data.F008pname)].HeaderText = "氏名";
            dgv.Columns[nameof(data.F009ym)].HeaderText = "施術年月";
            dgv.Columns[nameof(data.F010sid)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(data.F011sregnumber)].HeaderText = "登録記号番号";
            dgv.Columns[nameof(data.F012shinsaym)].HeaderText = "審査年月";
            dgv.Columns[nameof(data.F013comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(data.F014)].HeaderText = "支給区分";
            dgv.Columns[nameof(data.F015)].HeaderText = "返戻理由        ";
            dgv.Columns[nameof(data.pbirthdayad)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(data.ymad)].HeaderText = "施術年月西暦";
            dgv.Columns[nameof(data.shinsaymad)].HeaderText = "審査年月西暦";
            dgv.Columns[nameof(data.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(data.hnum_narrow)].HeaderText = "被保険者証番号半角";



            #endregion

            #region visible

            dgv.Columns[nameof(data.F000importid)].Visible = true;
            dgv.Columns[nameof(data.F001renban)].Visible = true;
            dgv.Columns[nameof(data.F002insnum)].Visible = true;
            dgv.Columns[nameof(data.F003kohifutan)].Visible = true;
            dgv.Columns[nameof(data.F004hnum)].Visible = true;
            dgv.Columns[nameof(data.F005kohijyukyu)].Visible = true;
            dgv.Columns[nameof(data.F006pbirthday)].Visible = true;
            dgv.Columns[nameof(data.F007pgender)].Visible = true;
            dgv.Columns[nameof(data.F008pname)].Visible = true;
            dgv.Columns[nameof(data.F009ym)].Visible = true;
            dgv.Columns[nameof(data.F010sid)].Visible = true;
            dgv.Columns[nameof(data.F011sregnumber)].Visible = true;
            dgv.Columns[nameof(data.F012shinsaym)].Visible = true;
            dgv.Columns[nameof(data.F013comnum)].Visible = true;
            dgv.Columns[nameof(data.F014)].Visible = false;
            dgv.Columns[nameof(data.F015)].Visible = false;
            dgv.Columns[nameof(data.pbirthdayad)].Visible = true;
            dgv.Columns[nameof(data.ymad)].Visible = true;
            dgv.Columns[nameof(data.shinsaymad)].Visible = true;
            dgv.Columns[nameof(data.cym)].Visible = true;
            dgv.Columns[nameof(data.hnum_narrow)].Visible = true;

            #endregion

            #endregion

        }

        /// <summary>
        /// あはきグリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid_AHK(App app = null)
        {
            #region 要らなくなった

            //List<dataimport_ahk> lstimp = new List<dataimport_ahk>();

            //関連レセ取得　emptytext3はApplicationのプロパティにないため、直接取得。emptytext2は続紙かどうか判定
            //DB.Command cmd = DB.Main.CreateCmd($"select emptytext3 from application " +
            //    $"where cym={app.CYM} and aid={app.Aid} and (emptytext2='0' or emptytext2='')");

            //emptytext2が1以上の場合は続紙で、検索条件に掛からないためnullになり得る
            //if (cmd.TryExecuteScalar() == null) return;

            //string strEmptyText3=System.Text.RegularExpressions.Regex.Replace(cmd.TryExecuteScalar().ToString(), ".tiff|.tif", "");

            //emptytext3の画像ファイル名から拡張子を抜いてrefrece.numberingと突合しrefrece取得           
            //lstimp = dataimport_ahk.select($"F013comnum='{strEmptyText3}'");

            //コマンド解放しないとコマンドPoolがいっぱいになる            
            //cmd.Dispose();


            //string strhnum = verifyBoxHnum.Text.Trim();
            //int intTotal = verifyBoxTotal.GetIntValue();
            //int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            //DateTime dtbirth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);
            //string strGender = verifyBoxSex.GetIntValue() == 1 ? "男" : "女";

            //レセプト全国共通キーで合致する前提のため不要

            //foreach (dataimport_ahk item in lstAHK)
            //{
            //    //被保番半角、メホール請求年月、生年月日、性別、診療年月で探す
            //    //if (item.hnum_narrow == verifyBoxHnum.Text.Trim() &&
            //    //    item.cym == scan.CYM &&                    
            //    //    item.pbirthdayad == dtbirth &&
            //    //    item.F007pgender == strGender &&
            //    //    item.ymad == intymad) 
            //    if(item.F013comnum==app.ComNum)
            //    {
            //        //条件合致したレコードを表示リストに登録
            //        lstimp.Add(item);
            //    }
            //}

            //dgv.DataSource = null;
            //dgv.DataSource = lstimp;//表示リストを表示させる


            //InitGrid_AHK();//グリッド初期化


            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            //if (lstimp.Count > 1) dgv.ClearSelection();

            //20210705174123 furukawa st ////////////////////////
            //一度AUXに登録した後は、一意のキーが付いているのでそっちを呼ぶ
            
            //List<Application_AUX> lstaux = Application_AUX.Select($" aid={app.Aid}");
            //if (lstaux.Count > 0)
            //{
            //    foreach (DataGridViewRow r in dgv.Rows)
            //    {

            //        if (lstaux.Count == 1 &&
            //              lstaux[0].matchingID01 == r.Cells[nameof(dataimport_ahk.F000importid)].Value.ToString())
            //        {
            //            r.Selected = true;
            //            labelMacthCheck.BackColor = Color.Cyan;
            //            labelMacthCheck.Text = "マッチングOK";
            //            labelMacthCheck.Visible = true;
            //            return;
            //        }
            //        else
            //        {
            //            r.Selected = false;
            //        }

            //    }
            //}
            //20210705174123 furukawa ed ////////////////////////


            //グリッドに複数行ある場合に選択する
            //if (app != null && app.RrID.ToString() != string.Empty)
            //{
            //    foreach (DataGridViewRow r in dgv.Rows)
            //    {

            //        int intRRid = int.Parse(r.Cells[nameof(dataimport_ahk.F000importid)].Value.ToString());

            //        合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
            //        string strcomnum = r.Cells[nameof(dataimport_ahk.F013comnum)].Value.ToString();

            //        エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
            //        if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
            //        {

            //            if (intRRid == app.RrID)
            //            {
            //                r.Selected = true;
            //            }

            //            提供データIDと違う場合の処理抜け
            //            else
            //            {
            //                r.Selected = false;
            //            }
            //        }
            //        else
            //        {

            //            if (strcomnum == strEmptyText3)
            //            {
            //                レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

            //                    if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
            //                else r.Selected = true;

            //            }
            //            提供データIDと違う場合の処理抜け                        
            //            else
            //            {
            //                r.Selected = false;
            //            }
            //        }

            //    }
            //}

            //if (lstimp == null || lstimp.Count == 0)
            //{
            //    labelMacthCheck.BackColor = Color.Pink;
            //    labelMacthCheck.Text = "マッチング無し";
            //    labelMacthCheck.Visible = true;
            //}

            //マッチングOK条件に選択行が1行の場合も追加
            //else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            //{
            //    labelMacthCheck.BackColor = Color.Cyan;
            //    labelMacthCheck.Text = "マッチングOK";
            //    labelMacthCheck.Visible = true;
            //}
            //else
            //{
            //    labelMacthCheck.BackColor = Color.Yellow;
            //    labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
            //    labelMacthCheck.Visible = true;
            //}

            #endregion
        }

        #endregion

        /// <summary>
        /// コントロール初期化
        /// </summary>
        /// <param name="app_type">APP_TYPE</param>
        private void InitControl(APP_TYPE app_type)
        {

            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            phnum.Visible = false;
            pDoui.Visible = false;
            panelunder.Visible = false;
            dgv.Visible = false;

            //20211129112942 furukawa st ////////////////////////
            //月は柔整だけ見せる。基本見せない
            
            labelM.Visible = false;
            verifyBoxM.Visible = false;
            //20211129112942 furukawa ed ////////////////////////

            //20211129133434 furukawa st ////////////////////////
            //年ラベルも柔整だけ見せる。基本見せない
            
            labelYear.Visible = false;
            labelHs.Visible = false;
            //20211129133434 furukawa ed ////////////////////////


            switch (app_type)
            {
                case APP_TYPE.長期:
                case APP_TYPE.続紙:
                case APP_TYPE.不要:
                case APP_TYPE.エラー:
                case APP_TYPE.同意書裏:
                case APP_TYPE.施術報告書:
                case APP_TYPE.状態記入書:
                    break;
                case APP_TYPE.同意書:
                    pDoui.Visible = true;
                    break;

                case APP_TYPE.柔整:
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    panelunder.Visible = true;

                    verifyBoxCounteddays.Visible = false;//20211005133138 furukawa柔整は実日数不要
                    labelCountedDays.Visible = false;     //20211005133642 furukawa 柔整は実日数不要

                    //提供データ
                    dgv.Visible = true;

                    //20211129112706 furukawa st ////////////////////////
                    //月入力は柔整だけ。あはきは提供データから取ってくる                    
                    labelM.Visible = true;
                    verifyBoxM.Visible = true;
                    //20211129112706 furukawa ed ////////////////////////

                    //20211129133347 furukawa st ////////////////////////
                    //診療年は柔整のみ
                    
                    labelYear.Visible = true;
                    labelHs.Visible = true;
                    //20211129133347 furukawa ed ////////////////////////

                    break;

                case APP_TYPE.あんま:
                case APP_TYPE.鍼灸:
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    panelunder.Visible = true;

                    labelFusyoCnt.Visible = false;
                    verifyBoxFushoCount.Visible = false;
                    labelNumbering.Visible = false;
                    verifyBoxNumbering.Visible = false;

                    //20210517173604 furukawa st ////////////////////////
                    //あはき提供データにあるので不要
                    
                    labelSex.Visible = false;
                    labelSex2.Visible = false;
                    verifyBoxSex.Visible = false;
                    pBirthday.Visible = false;
                    //20210517173604 furukawa ed ////////////////////////

                    //20211007111619 furukawa st ////////////////////////
                    //提供データを参照するので不要
                    
                    //提供データ
                    dgv.Visible = false;
                    
                    //被保険者証番号
                    verifyBoxHnum.Visible = false;                    
                    labelHnum.Visible = false;

                    //マッチングチェック
                    labelMacthCheck.Visible = false;
                    //20211007111619 furukawa ed ////////////////////////

              

                    break;

                default://申請書以外
                    dgv.Visible = false;
                    break;

            }
        }

        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            InitControl(scan.AppType);            

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            //今までのナンバリングを取得 グループで連番にしておかないと、並行で入力した場合、
            //Aグループの最後に入力登録したナンバリングを見に行って＋１するので
            //Bグループの連番がそれ始まりになるため、Aグループをどんどん登録していった場合、Bグループの連番始まりがどんどんずれていく
            if (scan.AppType == APP_TYPE.柔整)
            {
                List<App> lstscan = lstscan = App.GetAppsGID(app.GroupID);
                lstscan.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                dicNumbering.Clear();
                foreach (App a in lstscan)
                {
                    dicNumbering.Add(a.Aid, a.TaggedDatas.GeneralString1);
                    //System.Diagnostics.Debug.Print($"{a.Aid}_{a.TaggedDatas.GeneralString1}");
                }
            }

       

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                  
                    //20210610114809 furukawa st ////////////////////////
                    //柔整300/あはき200でDPI違う
                    scrollPictureControl1.Ratio = CommonTool.AutoAdjustScreen(img);
                    //      scrollPictureControl1.Ratio = 0.4f;
                    //20210610114809 furukawa ed ////////////////////////

                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //20211011111449 furukawa st ////////////////////////
            //柔整・あはきとも請求金額のみベリファイ　なので、ベリファイ有無の引数は基本False
            //請求金額のみnv
            //20211011111449 furukawa ed ////////////////////////
            


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, false);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, false);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, false);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, false);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, false, vbDouiY, vbDouiM, vbDouiG, vbDouiD);
                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, false);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, false);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, false);
                    break;

                default:

                    //申請書


                    //和暦年
                    //20211129140614 furukawa st ////////////////////////
                    //あはき申請書は年を入力しないので表示しない。柔整申請書のみ表示

                    if (app.AppType == APP_TYPE.柔整)
                    {
                        setValue(verifyBoxY, app.MediYear.ToString(), firstTime, false);
                    }
                    //あはき申請書の2回目入力時、年は空欄表示で編集不可
                    if(((app.AppType==APP_TYPE.あんま) || (app.AppType==APP_TYPE.鍼灸)) && !firstTime)
                    {
                        setValue(verifyBoxY, "", firstTime, false);
                    }
                    //      setValue(verifyBoxY, app.MediYear.ToString(), firstTime, false);
                    //20211129140614 furukawa ed ////////////////////////


                    //和暦月
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, false);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum, VbStrConv.Narrow), firstTime, false);



                    //負傷カウント                   
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, false);

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, false, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);


                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, false);

                    //請求金額 これのみあはきもベリファイ→実は柔整も必要だった
                    setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //本人家族 本人家族x100+本家区分　不要？
                    //setValue(verifyBoxFamily, app.Family.ToString().Substring(0,1), firstTime, nv);

                    //ナンバリング=処理年月和暦年月＋ナンバリング前ゼロ4桁
                    if (verifyBoxNumbering.Visible)
                    {
                        string strNumbering = app.TaggedDatas.GeneralString1 == string.Empty ? string.Empty : app.TaggedDatas.GeneralString1.Substring(4);
                        //string strNumbering = app.Numbering.Substring(4);
                        setValue(verifyBoxNumbering, strNumbering, firstTime, false);
                    }


                            
                    if (app.AppType == APP_TYPE.柔整)
                    {

                        //20210517173844 furukawa st ////////////////////////
                        //あはき提供データにあるので不要。柔整のみ
                        //受療者生年月日
                        setDateValue(app.Birthday, firstTime, false, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                        //性別
                        setValue(verifyBoxSex, app.Sex.ToString(), firstTime, false);                       
                        //20210517173844 furukawa ed ////////////////////////


                        //グリッド選択する    
                        createGrid_kokuho(app);
                    }

                    
                    else if (app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    {
                        //あはきはレセプト全国共通キーで完全一致
                        //    createGrid_AHK(app);

                        //実日数（あはき）
                        setValue(verifyBoxCounteddays, app.CountedDays, firstTime, false);

                    }


                    break;
            }
        }


        #endregion

        #region 各種更新

        
        /// <summary>
        /// 続紙用ナンバリング取得
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        private void CreateNumberingForZokushi(App app)
        {
            string retNumbering = string.Empty;
            string strPrevNumbering = string.Empty;
            int aid = app.Aid;
            int year = app.MediYear;

            if (scan.AppType != APP_TYPE.柔整) return;

            //この申請書が続紙の場合
            if (year < 0)
            {
                if (dicNumbering.TryGetValue(app.Aid - 1, out string item))
                {
                    if (item.Contains("-"))
                    {
                        //これより前が続紙の時 & この申請書が続紙の場合

                        //これより前のナンバリングに-があったら分離して、後ろを＋１
                        strPrevNumbering = item.Split('-')[0].ToString();

                        NumberingForZokushi = int.Parse(item.Split('-')[1].ToString()) + 1;

                        retNumbering = $"{strPrevNumbering}-{NumberingForZokushi.ToString().PadLeft(3, '0')}";
                    }
                    else
                    {
                        //これより前が申請書の時 &  この申請書が続紙の場合
                        retNumbering = $"{item}-{NumberingForZokushi.ToString().PadLeft(3, '0')}";
                    }
                }
                NumberingForZokushi++;
 
                app.TaggedDatas.GeneralString1 = retNumbering;

            }
            
        }


        /// <summary>
        /// ナンバリング連番チェック
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="year"></param>
        /// <param name="strNumbering"></param>
        /// <returns></returns>
        private bool CheckNumbering(int aid,int year,string strNumbering)
        {
          
            string strPrev = string.Empty;

            //最初の申請書は無条件で通すしかない
            if (int.TryParse(strNumbering, out int tmp)) if (tmp == 1) return true;
            
            if (!dicNumbering.ContainsKey(aid - 1)) return true;

            ////スキャンID単位で取得した場合、Aグループの最後とBグループの最初がAID＋１とは限らないため、さかのぼる
            ////AIDを2000さかのぼってあったらそれを取得
            //for(int cnt = 1; cnt<2000; cnt++)
            //{
            //    if (dicNumbering.ContainsKey(aid - cnt))
            //    {
            //        if (dicNumbering[aid - cnt].ToString() != string.Empty)
            //        {
            //            strPrev = dicNumbering[aid - cnt].ToString(); break;
            //        }
            //    }
            //}

            //if (strPrev == string.Empty) return true;
            
            strPrev = dicNumbering[aid - 1].ToString();

            
            if (year > 0)
            {

                //前にナンバリングが無い場合
                if (strPrev == string.Empty)
                {
                    return true;
                }
                //現在のが申請書、前のが続紙の場合
                else if (strPrev.Contains('-'))
                {
                    //20211011114754 furukawa st ////////////////////////
                    //前の申請書のマッチングがない場合、ナンバリングが取得できないので無条件true                    
                    int prevNumbering = 0;
                    if (strPrev.Split('-')[0].ToString() == string.Empty) return true;
                    if (!int.TryParse(strPrev.Split('-')[0].ToString().Substring(4), out prevNumbering)) return true;
                    //      int prevNumbering = int.Parse(strPrev.Split('-')[0].ToString().Substring(4));
                    //20211011114754 furukawa ed ////////////////////////

                    if ((int.Parse(strNumbering) - prevNumbering) == 1) return true;
                    else return false;
                }
                //現在のが申請書、前も申請書
                else
                {
                    intPrevNumbering = int.Parse(strPrev.Substring(4));
                    if (int.Parse(strNumbering) - intPrevNumbering==1) return true;
                    else return false;
                }
            }

            else
            {
                //現在のが続紙で、前には何もない状態はあり得ない。申請書に続くのが続紙だから。
                //グループまたぎは考慮しない

                //現在のが続紙、前のが続紙の場合
                if (strNumbering.Contains('-'))
                {
                    int prevNumbering = int.Parse(strPrev.Split('-')[0].ToString().Substring(4));
                    int prevBranch= int.Parse(strPrev.Split('-')[1].ToString());
                    int currNumbering= int.Parse(strNumbering.Split('-')[0].ToString().Substring(4));
                    int currBranch = int.Parse(strNumbering.Split('-')[1].ToString());

                    //ナンバリング前が同じ、ハイフン移行は1の差
                    if ((currNumbering== prevNumbering) && ( currBranch-prevBranch==1)) return true;
                    else return false;
                }
                //現在のが続紙、前が申請書
                else
                {
                    intPrevNumbering = int.Parse(strPrev);

                    int currNumbering = int.Parse(strNumbering.Split('-')[0].ToString());
                    //int currBranch = int.Parse(strNumbering.Split('-')[1].ToString());

                    if (currNumbering - intPrevNumbering == 1) return true;
                    else return false;
                }

            }
        }

        /// <summary>
        /// 提供データ(国保システムCSV)をAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistKokuho(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    kokuhodata k = lstKokuho[0];
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(k.f000_kokuhoid)].Value.ToString()); //ID

                    
                    //入力を優先させる
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                                //被保険者証番号
                    app.PersonName = dgv.CurrentRow.Cells[nameof(k.f002_pname)].Value.ToString();                      //受療者名
                    app.InsNum = dgv.CurrentRow.Cells[nameof(k.f021_insnum)].Value.ToString();                         //保険者番号
                    app.Sex = dgv.CurrentRow.Cells[nameof(k.f009_pgender)].Value.ToString() == "男" ? 1 : 2;           //受療者性別                    
                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.birthdayad)].Value.ToString());        //受療者生年月日
                    app.ClinicName = dgv.CurrentRow.Cells[nameof(k.f005_sname)].Value.ToString();                      //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f004_sid)].Value.ToString();                         //医療機関コード
                    app.ComNum = dgv.CurrentRow.Cells[nameof(k.f022_comnum)].Value.ToString();                         //レセプト全国共通キー
                    app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(k.f011_counteddays)].Value.ToString()  );  //施術日数
                    app.AppType = APP_TYPE.柔整;          //種別

                    app.ClinicName=dgv.CurrentRow.Cells[nameof(k.f005_sname)].Value.ToString(); //施術所名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f004_sid)].Value.ToString();  //施術所番号
                    app.TaggedDatas.GeneralString2 = dgv.CurrentRow.Cells[nameof(k.shinsaymad)].Value.ToString();  //審査年月
                    app.TaggedDatas.GeneralString3 = dgv.CurrentRow.Cells[nameof(k.f020_shinsaym)].Value.ToString();  //審査年月和暦

                    //本家区分=本人家族x100+本家区分
                    //string strHonke = dgv.CurrentRow.Cells[nameof(k.f014_honke)].Value.ToString();
                    string strfamily = dgv.CurrentRow.Cells[nameof(k.f014_honke)].Value.ToString() == "本人" ? "2" : "6";
                    app.Family = int.Parse(strfamily);

                    //app.Family = int.Parse(strfamily) * 100;

                    //switch (strHonke)
                    //{
                    //    case "本人":
                    //        app.Family += 2;
                    //        break;

                    //    case "家族":
                    //        app.Family += 6;
                    //        break;

                    //    case "高齢者一般":
                    //        app.Family += 8;
                    //        break;

                    //    case "高齢者７割":
                    //        app.Family += 0;
                    //        break;

                    //    default:
                    //        app.Family += 99;//エラー用
                    //        break;
                    //}




                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(k.shinryoymad)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(k.shinryoymad)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 提供データあはきをAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistAHK(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    dataimport_ahk data = new dataimport_ahk();
                    
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(data.F000importid)].Value.ToString()); //ID


                    //入力を優先させる
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                                //被保険者証番号
                    app.PersonName = dgv.CurrentRow.Cells[nameof(data.F008pname)].Value.ToString();                      //受療者名
                    app.InsNum = dgv.CurrentRow.Cells[nameof(data.F002insnum)].Value.ToString();                         //保険者番号

                    //20210517172649 furukawa st ////////////////////////
                    //男女が漢字でなく数字                    
                    if (int.TryParse(dgv.CurrentRow.Cells[nameof(data.F007pgender)].Value.ToString().Trim(), out int tmpsex)) app.Sex = tmpsex;
                    //app.Sex = dgv.CurrentRow.Cells[nameof(data.F007pgender)].Value.ToString() == "男" ? 1 : 2;           //受療者性別                    
                    //20210517172649 furukawa ed ////////////////////////


                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(data.pbirthdayad)].Value.ToString());        //受療者生年月日
                    
                    app.DrNum= dgv.CurrentRow.Cells[nameof(data.F011sregnumber)].Value.ToString();//登録記号番号
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(data.F010sid)].Value.ToString();                         //医療機関コード
                    app.ComNum = dgv.CurrentRow.Cells[nameof(data.F013comnum)].Value.ToString();                         //レセプト全国共通キー
                                       
                    app.TaggedDatas.GeneralString2 = dgv.CurrentRow.Cells[nameof(data.F012shinsaym)].Value.ToString();  //審査年月和暦
                    app.TaggedDatas.GeneralString3 = dgv.CurrentRow.Cells[nameof(data.shinsaymad)].Value.ToString();  //審査年月

                    //医療機関コードの3桁目(9)　　 AppType あんま = 8,
                    //医療機関コードの3桁目(8)       AppType 鍼灸 = 7
                    string strAppType = dgv.CurrentRow.Cells[nameof(data.F010sid)].Value.ToString().Substring(2, 1);
                    if (strAppType == "8") app.AppType = APP_TYPE.鍼灸;
                    if (strAppType == "9") app.AppType = APP_TYPE.あんま;
                    

                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(data.ymad)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(data.ymad)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }



        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

          


            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    CreateNumberingForZokushi(app);
                    
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    CreateNumberingForZokushi(app);
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    
                    //20210511102659 furukawa st ////////////////////////
                    //不要の場合はナンバリングしない

                    //CreateNumberingForZokushi(app);
                    //20210511102659 furukawa ed ////////////////////////
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    CreateNumberingForZokushi(app);
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    CreateNumberingForZokushi(app);
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    CreateNumberingForZokushi(app);
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;

                    CreateNumberingForZokushi(app);

                    break;

                default:
                    //申請書

                    //申請書になったらリセット
                    NumberingForZokushi = 0;


                    #region 入力チェック

                    
                    
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    
                    //20211129113456 furukawa st ////////////////////////
                    //あはきは入力しないのでチェックなし
                    
                    if ((scan.AppType != APP_TYPE.あんま) && (scan.AppType != APP_TYPE.鍼灸))
                    {
                        setStatus(verifyBoxM, month < 1 || 12 < month);
                    }
                    //      setStatus(verifyBoxM, month < 1 || 12 < month);
                    //20211129113456 furukawa ed ////////////////////////


                    //和暦年
                    int year = verifyBoxY.GetIntValue();

                    //20211129134047 furukawa st ////////////////////////
                    //あはきは入力しないのでチェックなし
                    
                    if ((scan.AppType != APP_TYPE.あんま) && (scan.AppType != APP_TYPE.鍼灸))
                    {
                        setStatus(verifyBoxY, year < 1 || 31 < year);
                    }
                    //   setStatus(verifyBoxY, year < 1 || 31 < year);
                    //20211129134047 furukawa ed ////////////////////////

                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
                    


                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);


                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //請求金額
                    int intcharge = verifyBoxCharge.GetIntValue();
                    setStatus(verifyBoxCharge, intcharge >= total || intcharge < 100 || intcharge > 200000);


                    //本家 不要？
                    //  string strfamily = verifyBoxFamily.Text.Trim();
                    //20201005131341 furukawa st ////////////////////////
                    //本人家族は２、６以外エラー

                    //  setStatus(verifyBoxFamily, !new string[] { "2", "6" }.Contains(strfamily));
                    //setStatus(verifyBoxFamily, strfamily == string.Empty);
                    //20201005131341 furukawa ed ////////////////////////

                    
                    string hnumN = string.Empty;    //被保険者証番号   
                    int intNumbering = 0;           //ナンバリング
                    int fushoCount = 0;             //負傷数

                    DateTime dtbirth = DateTime.MinValue;
                    int intsex = 0;                 //性別

                    int intCountedDays = 0;         //実日数（あはき）

                    if (scan.AppType == APP_TYPE.柔整)
                    {

                        //20210517173943 furukawa st ////////////////////////
                        //あはき提供データにあるので不要。柔整のみ
                        //被保険者証番号   
                        hnumN = verifyBoxHnum.Text.Trim();
                        setStatus(verifyBoxHnum, hnumN == string.Empty);

                        //生年月日
                        dtbirth = DateTimeEx.DateTimeNull;
                        dtbirth = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                        //性別
                        intsex = verifyBoxSex.GetIntValue();
                        setStatus(verifyBoxSex, intsex < 0 || intsex > 2);
                        //20210517173943 furukawa ed ////////////////////////


                        //負傷数
                        fushoCount = verifyBoxFushoCount.GetIntValue();
                        setStatus(verifyBoxFushoCount, fushoCount <= 0);


                        //ナンバリング
                        intNumbering = verifyBoxNumbering.GetIntValue();
                        setStatus(verifyBoxNumbering, intNumbering <= 0);

                        //連番確認

                        //20211129144233 furukawa st ////////////////////////
                        //連番確認は申請書のみ(年が正の値の条件を追加）                        
                        if ((intNumbering>0) && (!CheckNumbering(app.Aid, verifyBoxY.GetIntValue(), intNumbering.ToString())))
                        //      if (!CheckNumbering(app.Aid, verifyBoxY.GetIntValue(), intNumbering.ToString())
                        //20211129144233 furukawa ed ////////////////////////
                        {
                            verifyBoxNumbering.BackColor = Color.YellowGreen;
                            var res = MessageBox.Show("ナンバリングが連番ではありませんが宜しいですか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            if (res != System.Windows.Forms.DialogResult.OK) return false;
                        }
                    }
                    //20211007113302 furukawa st ////////////////////////
                    //あはきは実日数必要
                    
                    else if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                    {
                        intCountedDays = verifyBoxCounteddays.GetIntValue();
                        setStatus(verifyBoxCounteddays, intCountedDays <= 0);
                    }
                    //20211007113302 furukawa ed ////////////////////////


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    /*   
                       //合計金額：請求金額：本家区分のトリプルチェック
                       //金額でのエラーがあればいったん登録中断
                       bool ratioError = (int)(total * ratio / 10) != intcharge;
                       if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != intcharge;
                       if (ratioError)
                       {
                           verifyBoxTotal.BackColor = Color.GreenYellow;
                           verifyBoxCharge.BackColor = Color.GreenYellow;
                           verifyBoxRatio.BackColor = Color.GreenYellow;
                           var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                               "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                               MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                           if (res != System.Windows.Forms.DialogResult.OK) return false;
                       }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映



                    //20211129134221 furukawa st ////////////////////////
                    //あはきは提供データから直接更新しているのでここでは更新しない

                    if ((scan.AppType != APP_TYPE.あんま) && (scan.AppType != APP_TYPE.鍼灸))
                    {
                        app.MediYear = year;                //施術年
                    }
                    //      app.MediYear = year;                //施術年
                    //20211129134221 furukawa ed ////////////////////////



                    //20211129113740 furukawa st ////////////////////////
                    //あはきは提供データから直接更新しているのでここでは更新しない

                    if ((scan.AppType != APP_TYPE.あんま) && (scan.AppType != APP_TYPE.鍼灸))
                    {
                        app.MediMonth = month;              //施術月
                    }
                    //      app.MediMonth = month;              //施術月
                    //20211129113740 furukawa ed ////////////////////////

                    //柔整のみ
                    //app.HihoNum = hnumN;                //被保険者証番号   



                    //20210517174126 furukawa st ////////////////////////
                    //あはき提供データにあるので不要                    
                    if (!(scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸))
                    {

                        app.HihoNum = hnumN;                //被保険者証番号   


                        app.Sex = intsex;//性別
                        app.Birthday = dtbirth;//生年月日
                    }
                    //20210517174126 furukawa ed ////////////////////////

                    //20211027100421 furukawa st ////////////////////////
                    //申請書種別は柔整はResistKokuho内、あはきはデータインポート時にするのでここでやると上書きしてしまうので削除

                    //申請書種別
                    //app.AppType = scan.AppType;
                    //20211027100421 furukawa ed ////////////////////////


                    //初検日
                    app.FushoFirstDate1 = f1FirstDt;

                    //負傷数                    
                    app.TaggedDatas.count = fushoCount;


                    //合計
                    app.Total = total;

                    //請求金額
                    app.Charge = intcharge;

                    //実日数（あはき）
                    app.CountedDays = intCountedDays;


                    //当初あはきはExcelの予定だったが、国保CSVにまとまったらしい→2021/05/07まとまってない。別excel
                    //2021/05/16　あはきは国保CSVにも載っているが、基本は別Excel

                    //20211109171802 furukawa st ////////////////////////
                    //app.Apptypeに入れる前なのでscan.Apptypeで判定                    
                    if (scan.AppType == APP_TYPE.柔整)
                    //      if (app.AppType == APP_TYPE.柔整)
                    //20211109171802 furukawa ed ////////////////////////

                    {
                        RegistKokuho(app);



                        //審査年月は国保CSVから取得するので、その後じゃないとエラー落ちする
                        //ナンバリング＝審査年月（和暦年月）前ゼロがある＋前ゼロ4桁

                        //和暦年月（R0301=0301）は、前ゼロが落ちるので文字型のフィールドに入れる
                        //審査年月がエラーの場合--とする

                        if (int.TryParse(app.TaggedDatas.GeneralString2, out int shinsa_ym))
                        {
                            if (shinsa_ym == 0)
                            {
                                app.TaggedDatas.GeneralString1 =
                              "----" +
                              intNumbering.ToString().PadLeft(4, '0');
                            }
                            else
                            {
                                app.TaggedDatas.GeneralString1 =
                                    DateTimeEx.GetJpYearFromYM(shinsa_ym).ToString().PadLeft(2, '0') +
                                    app.TaggedDatas.GeneralString2.Substring(4, 2).PadLeft(2, '0') +
                                    intNumbering.ToString().PadLeft(4, '0');
                            }
                        }
                        else
                        {
                            //審査年月が取れない場合は入れない
                            app.TaggedDatas.GeneralString1 =
                            "----" +
                            intNumbering.ToString().PadLeft(4, '0');
                            //app.TaggedDatas.GeneralString1 =
                            //    "--" +
                            //    app.TaggedDatas.GeneralString2.Substring(4, 2).PadLeft(2, '0') +
                            //    intNumbering.ToString().PadLeft(4, '0');
                        }
                    }

                    //20211008113239 furukawa st ////////////////////////
                    //入力画面でのマッチング確認は柔整のみ。あはきはインポート時にやる

                    //else if (app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    //{
                    //    RegistAHK(app);
                    //}
                    //20211008113239 furukawa ed ////////////////////////


                    //20211007114649 furukawa st ////////////////////////
                    //マッチング確認は柔整のみ
                    if (app.AppType == APP_TYPE.柔整)

                        //      マッチング無し確認は申請書のみ
                        //      if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                        //20211007114649 furukawa ed ////////////////////////
                    {
                        if (dgv.Rows.Count == 0 || dgv.SelectedRows.Count == 0)
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }

                    #endregion
                    

                    

                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20210705170053 furukawa st ////////////////////////
                //AUXを更新し、今後rridはそっちから取る

                //20211011132211 furukawa st ////////////////////////
                //AUXにもApptype追加                
                if (!Application_AUX.Update(app.Aid, app.AppType,tran,app.RrID.ToString()))
                //                    if (!Application_AUX.Update(app.Aid, tran,app.RrID.ToString()))
                //20211011132211 furukawa ed ////////////////////////
                {
                    MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n付随情報更新に失敗しました。");
                    return false;
                }
                //20210705170053 furukawa ed ////////////////////////

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            if (scan.AppType == APP_TYPE.柔整)
            {
                createGrid_kokuho(app);
            }
            else if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {

                createGrid_AHK(app);
            }

        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }

            //次に移動する前に現在のナンバリングを取得
            if (verifyBoxNumbering.Visible == true) intPrevNumbering = int.Parse(verifyBoxNumbering.Text.Trim());

            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void verifyBoxNumbering_Validated(object sender, EventArgs e)
        {
            verifyBoxNumbering.Text = verifyBoxNumbering.Text.PadLeft(4, '0');
        }

        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            if (verifyBoxHnum.Text != string.Empty) verifyBoxHnum.Text = verifyBoxHnum.GetIntValue().ToString();
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;

            else return;
            
            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
