﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Ibarakishi
{
    //20210401160124 furukawa
    /// <summary>
    /// 国保システムからのデータインポート
    /// </summary>
    public class kokuhodata
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_kokuhoid { get; set; } = 0;                             //シーケンス

        public string f001_hihonum { get; set; } = string.Empty;                       //証番号　全角想定;
        public string f002_pname { get; set; } = string.Empty;                      //氏名;
        public string f003_shinryoym { get; set; } = string.Empty;                  //診療年月 和暦想定;
        public string f004_sid { get; set; } = string.Empty;                        //機関コード;
        public string f005_sname { get; set; } = string.Empty;                      //医療機関名;
        public string f006_setainum { get; set; } = string.Empty;                   //世帯番号;
        public string f007_atenanum { get; set; } = string.Empty;                   //宛名番号;
        public string f008_pbirthday { get; set; } = string.Empty;                  //生年月日 和暦想定;
        public string f009_pgender { get; set; } = string.Empty;                    //性別;
        public string f010_ratio { get; set; } = string.Empty;                      //給付割合;
        public string f011_counteddays { get; set; } = string.Empty;                //実日数;
        public string f012_total { get; set; } = string.Empty;                      //決定点数;
        public string f013_score_kbn { get; set; } = string.Empty;                  //点数表;
        public string f014_honke { get; set; } = string.Empty;                      //本家;
        public string f015_nyugai { get; set; } = string.Empty;                     //入外;
        public string f016_hihomark { get; set; } = string.Empty;                   //証記号;
        public string f017 { get; set; } = string.Empty;                            //種別１;
        public string f018 { get; set; } = string.Empty;                            //種別２;
        public string f019 { get; set; } = string.Empty;                            //診療・療養費別;
        public string f020_shinsaym { get; set; } = string.Empty;                   //審査年月;
        public string f021_insnum { get; set; } = string.Empty;                     //保険者番号;
        public string f022_comnum { get; set; } = string.Empty;                     //レセプト全国共通キー;
        public string f023_dcode { get; set; } = string.Empty;                      //処方機関コード;
        public string f024_dname { get; set; } = string.Empty;                      //処方機関名;
        public string f025 { get; set; } = string.Empty;                            //クリ;
        public string f026 { get; set; } = string.Empty;                            //ケア;
        public string f027 { get; set; } = string.Empty;                            //容認;
        public string f028_newest { get; set; } = string.Empty;                     //最新履歴;
        public string f029 { get; set; } = string.Empty;                            //原本所在;
        public string f030_prefcorss { get; set; } = string.Empty;                  //県内外;
        public string f031 { get; set; } = string.Empty;                            //療養費種別;
        public string f032 { get; set; } = string.Empty;                            //食事基準額;
        public string f033 { get; set; } = string.Empty;                            //不当;
        public string f034 { get; set; } = string.Empty;                            //第三者;
        public string f035_kyufu_limit { get; set; } = string.Empty;                //給付制限;
        public string f036_expensive { get; set; } = string.Empty;                  //高額;
        public string f037 { get; set; } = string.Empty;                            //予約;
        public string f038 { get; set; } = string.Empty;                            //処理;
        public string f039 { get; set; } = string.Empty;                            //疑義種別;
        public string f040 { get; set; } = string.Empty;                            //参考;
        public string f041 { get; set; } = string.Empty;                            //状態;
        public string f042 { get; set; } = string.Empty;                            //コード情報;
        public string f043 { get; set; } = string.Empty;                            //原本区分;
        public string f044_fusen01 { get; set; } = string.Empty;                    //付箋01;
        public string f045_fusen02 { get; set; } = string.Empty;                    //付箋02;
        public string f046_fusen03 { get; set; } = string.Empty;                    //付箋03;
        public string f047_fusen04 { get; set; } = string.Empty;                    //付箋04;
        public string f048_fusen05 { get; set; } = string.Empty;                    //付箋05;
        public string f049_fusen06 { get; set; } = string.Empty;                    //付箋06;
        public string f050_fusen07 { get; set; } = string.Empty;                    //付箋07;
        public string f051_fusen08 { get; set; } = string.Empty;                    //付箋08;
        public string f052_fusen09 { get; set; } = string.Empty;                    //付箋09;
        public string f053_fusen10 { get; set; } = string.Empty;                    //付箋10;
        public string f054_fusen11 { get; set; } = string.Empty;                    //付箋11;
        public string f055_fusen12 { get; set; } = string.Empty;                    //付箋12;
        public string f056_fusen13 { get; set; } = string.Empty;                    //付箋13;
        public string f057_fusen14 { get; set; } = string.Empty;                    //付箋14;
        public string hihonum_narrow { get; set; } = string.Empty;                    //被保険者記号番号メホール用;
        public int shinryoymad { get; set; } = 0;                                   //診療年月西暦メホール用;
        public int shinsaymad { get; set; } = 0;                                    //審査年月西暦メホール用;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;               //生年月日西暦メホール用;
        public int cym { get; set; } = 0;                                          //メホール請求年月;

        #endregion

        public static List<kokuhodata> lstclsKokuho = new List<kokuhodata>();


        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool ImportKokuho(int _cym)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.InvokeValue = 0;

            try
            {
                if (!LoadFile(wf, _cym)) return false;

                //db登録
                if (!EntryDB(wf, _cym)) return false;

                System.Windows.Forms.MessageBox.Show("取込完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("取込失敗");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }



        /// <summary>
        /// db登録
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryDB(WaitForm wf, int _cym)
        {

            wf.InvokeValue = 0;
            wf.SetMax(lstclsKokuho.Count);
            DB.Transaction tran = new DB.Transaction(DB.Main);

            try
            {
                wf.LogPrint("DB削除");
                DB.Command cmd = new DB.Command($"delete from kokuhodata where cym={_cym}", tran);
                cmd.TryExecuteNonQuery();

                wf.LogPrint("DB登録");
                foreach (kokuhodata cls in lstclsKokuho)
                {
                    if (!DB.Main.Insert<kokuhodata>(cls, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }

        private static int ChangeToADYM(string strtmp)
        {
            return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
            //return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
        }

        /// <summary>
        /// csvをロードする
        /// </summary>
        /// <returns></returns>
        private static bool LoadFile(WaitForm wf, int _cym)
        {

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            Func<string, int> delegate1 = ChangeToADYM;

            try
            {
                dlg.Filter = "csv|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

                string strFileName = dlg.FileName;


                //CSV内容ロード
                //20220208104933 furukawa st ////////////////////////
                //CSVインポート 文字コード自動判定列数制限
                
                if (!CommonTool.CsvImportMultiCodeLimitCols(strFileName, out List<string[]> lst, 37)) return false;
                //      List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);
                //20220208104933 furukawa ed ////////////////////////


                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);



                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {


                    try
                    {
                        //決定点数を判断基準とする
                        int.Parse(tmp[5].ToString().Replace(",", ""));
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    kokuhodata cls = new kokuhodata();

                    //20210516150512 furukawa st ////////////////////////
                    //国保CSVは保険者ごとにレイアウトが違う前提で、テーブルの形は固定しておき、どの項目をどこに入れるか、保険者ごとに設定する
                    
                    cls.f001_hihonum = tmp[0];                      //証番号
                    cls.f002_pname = tmp[1];                        //氏名
                    cls.f003_shinryoym = tmp[2];                    //診療年月
                    cls.f004_sid = tmp[36];                         //機関コード
                    cls.f005_sname = tmp[4];                        //医療機関名
                    cls.f006_setainum = string.Empty;               //世帯番号
                    cls.f007_atenanum = string.Empty;               //宛名番号
                    cls.f008_pbirthday = tmp[8];                    //生年月日
                    cls.f009_pgender = tmp[9];                      //性別
                    cls.f010_ratio = tmp[12];                       //給付割合
                    cls.f011_counteddays = tmp[16];                 //実日数
                    cls.f012_total = tmp[5];                        //決定点数
                    cls.f013_score_kbn = tmp[14];                   //点数表
                    cls.f014_honke = tmp[11];                       //本家
                    cls.f015_nyugai = tmp[10];                      //入外
                    cls.f016_hihomark = string.Empty;               //証記号
                    cls.f017 = string.Empty;                        //種別１
                    cls.f018 = string.Empty;                        //種別２
                    cls.f019 = tmp[13];                             //診療・療養費別
                    cls.f020_shinsaym = tmp[3];                     //審査年月
                    cls.f021_insnum = string.Empty;                 //保険者番号
                    cls.f022_comnum = tmp[6];                       //レセプト全国共通キー
                    cls.f023_dcode = tmp[18];                       //処方機関コード
                    cls.f024_dname = tmp[19];                       //処方機関名
                    cls.f025 = tmp[20];                             //クリ
                    cls.f026 = tmp[21];                             //ケア
                    cls.f027 = tmp[22];                             //容認
                    cls.f028_newest = tmp[7];                       //最新履歴
                    cls.f029 = tmp[34];                             //原本所在
                    cls.f030_prefcorss = tmp[35];                   //県内外
                    cls.f031 = tmp[15];                             //療養費種別
                    cls.f032 = tmp[17];                             //食事基準額
                    cls.f033 = tmp[23];                             //不当
                    cls.f034 = tmp[24];                             //第三者
                    cls.f035_kyufu_limit = tmp[25];                 //給付制限
                    cls.f036_expensive = tmp[26];                   //高額
                    cls.f037 = tmp[27];                             //予約
                    cls.f038 = tmp[28];                             //処理
                    cls.f039 = tmp[29];                             //疑義種別
                    cls.f040 = tmp[30];                             //参考
                    cls.f041 = tmp[31];                             //状態
                    cls.f042 = tmp[32];                             //コード情報
                    cls.f043 = tmp[33];                             //原本区分


                    cls.f044_fusen01 = string.Empty;         //付箋01;
                    cls.f045_fusen02 = string.Empty;         //付箋02;
                    cls.f046_fusen03 = string.Empty;         //付箋03;
                    cls.f047_fusen04 = string.Empty;         //付箋04;
                    cls.f048_fusen05 = string.Empty;         //付箋05;
                    cls.f049_fusen06 = string.Empty;         //付箋06;
                    cls.f050_fusen07 = string.Empty;         //付箋07;
                    cls.f051_fusen08 = string.Empty;         //付箋08;
                    cls.f052_fusen09 = string.Empty;         //付箋09;
                    cls.f053_fusen10 = string.Empty;         //付箋10;
                    cls.f054_fusen11 = string.Empty;         //付箋11;
                    cls.f055_fusen12 = string.Empty;         //付箋12;
                    cls.f056_fusen13 = string.Empty;         //付箋13;
                    cls.f057_fusen14 = string.Empty;         //付箋14;

                    //cls.f001_hihonum = tmp[0];                              //証番号　全角想定;
                    //cls.f002_pname = tmp[1];                                //氏名;
                    //cls.f003_shinryoym = tmp[2];                            //診療年月 和暦想定;
                    //cls.f004_sid = tmp[3];                                  //機関コード;
                    //cls.f005_sname = tmp[4];                                //医療機関名;
                    //cls.f006_setainum = tmp[5];                             //世帯番号;
                    //cls.f007_atenanum = tmp[6];                             //宛名番号;
                    //cls.f008_pbirthday = tmp[7];                            //生年月日 和暦想定;
                    //cls.f009_pgender = tmp[8];                              //性別;
                    //cls.f010_ratio = tmp[9];                                //給付割合;
                    //cls.f011_counteddays = tmp[10];                         //実日数;
                    //cls.f012_total = tmp[11];                               //決定点数;
                    //cls.f013_score_kbn = tmp[12];                           //点数表;
                    //cls.f014_honke = tmp[13];                               //本家;
                    //cls.f015_nyugai = tmp[14];                              //入外;
                    //cls.f016_hihomark = tmp[15];                            //証記号;
                    //cls.f017 = tmp[16];                                     //種別１;
                    //cls.f018 = tmp[17];                                     //種別２;
                    //cls.f019 = tmp[18];                                     //診療・療養費別;
                    //cls.f020_shinsaym = tmp[19];                            //審査年月;
                    //cls.f021_insnum = tmp[20];                              //保険者番号;
                    //cls.f022_comnum = tmp[21];                              //レセプト全国共通キー;
                    //cls.f023_dcode = tmp[22];                               //処方機関コード;
                    //cls.f024_dname = tmp[23];                               //処方機関名;
                    //cls.f025 = tmp[24];                                     //クリ;
                    //cls.f026 = tmp[25];                                     //ケア;
                    //cls.f027 = tmp[26];                                     //容認;
                    //cls.f028_newest = tmp[27];                              //最新履歴;
                    //cls.f029 = tmp[28];                                     //原本所在;
                    //cls.f030_prefcorss = tmp[29];                           //県内外;
                    //cls.f031 = tmp[30];                                     //療養費種別;
                    //cls.f032 = tmp[31];                                     //食事基準額;
                    //cls.f033 = tmp[32];                                     //不当;
                    //cls.f034 = tmp[33];                                     //第三者;
                    //cls.f035_kyufu_limit = tmp[34];                         //給付制限;
                    //cls.f036_expensive = tmp[35];                           //高額;
                    //cls.f037 = tmp[36];                                     //予約;
                    //cls.f038 = tmp[37];                                     //処理;
                    //cls.f039 = tmp[38];                                     //疑義種別;
                    //cls.f040 = tmp[39];                                     //参考;
                    //cls.f041 = tmp[40];                                     //状態;
                    //cls.f042 = tmp[41];                                     //コード情報;
                    //cls.f043 = tmp[42];                                     //原本区分;

                    //20210516150512 furukawa ed ////////////////////////

                    //被保険者記号番号メホール用;
                    //記号_番号の半角書式にする
                    cls.hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv($"{cls.f016_hihomark}{(cls.hihonum_narrow == string.Empty ? "" : "_")}{cls.f001_hihonum}", Microsoft.VisualBasic.VbStrConv.Narrow);

                    //西暦を控えておく
                    cls.shinryoymad = delegate1(cls.f003_shinryoym);            //診療年月西暦メホール用;
                    cls.shinsaymad = delegate1(cls.f020_shinsaym);               //審査年月西暦メホール用;


                    //cls.birthdayad = DateTime.Parse(cls.f008_pbirthday);        //生年月日西暦メホール用;
                    int y = DateTimeEx.GetAdYear(cls.f008_pbirthday.Substring(0, 4));
                    int m= int.Parse(cls.f008_pbirthday.Substring(5, 2));
                    int d =int.Parse( cls.f008_pbirthday.Substring(8, 2));
                    cls.birthdayad =new DateTime(y,m,d);        //生年月日西暦メホール用;

                    cls.cym = _cym;                                               //メホール請求年月;

                    lstclsKokuho.Add(cls);

                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                return false;
            }
        }


     
        /// <summary>
        /// 処理年月単位で取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<kokuhodata> select(int cym)
        {
            lstclsKokuho.Clear();

            //データ取得
            DB.Command cmd = new DB.Command(DB.Main, $"select * from kokuhodata where cym={cym}");
            List<object[]> lst = cmd.TryExecuteReaderList();

            try
            {


                //クラスのリストに登録
                foreach (object[] tmp in lst)
                {
                    try
                    {
                        int.Parse(tmp[11].ToString().Replace(",", ""));
                    }
                    catch
                    {
                    //    wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    kokuhodata cls = new kokuhodata();
                    cls.f000_kokuhoid = int.Parse(tmp[0].ToString());
                    cls.f001_hihonum 		= tmp[1].ToString();            //証番号　全角想定;
                    cls.f002_pname 		= tmp[2].ToString();                //氏名;
                    cls.f003_shinryoym 	= tmp[3].ToString();                //診療年月 和暦想定;
                    cls.f004_sid 			= tmp[4].ToString();            //機関コード;
                    cls.f005_sname 		= tmp[5].ToString();                //医療機関名;
                    cls.f006_setainum 		= tmp[6].ToString();            //世帯番号;
                    cls.f007_atenanum 		= tmp[7].ToString();            //宛名番号;
                    cls.f008_pbirthday 	= tmp[8].ToString();                //生年月日 和暦想定;
                    cls.f009_pgender 		= tmp[9].ToString();            //性別;
                    cls.f010_ratio 		= tmp[10].ToString();               //給付割合;
                    cls.f011_counteddays 	= tmp[11].ToString();           //実日数;
                    cls.f012_total 		= tmp[12].ToString();               //決定点数;
                    cls.f013_score_kbn 	= tmp[13].ToString();               //点数表;
                    cls.f014_honke 		= tmp[14].ToString();               //本家;
                    cls.f015_nyugai 		= tmp[15].ToString();           //入外;
                    cls.f016_hihomark 		= tmp[16].ToString();           //証記号;
                    cls.f017 				= tmp[17].ToString();           //種別１;
                    cls.f018 				= tmp[18].ToString();           //種別２;
                    cls.f019 				= tmp[19].ToString();           //診療・療養費別;
                    cls.f020_shinsaym 		= tmp[20].ToString();           //審査年月;
                    cls.f021_insnum 		= tmp[21].ToString();           //保険者番号;
                    cls.f022_comnum 		= tmp[22].ToString();           //レセプト全国共通キー;
                    cls.f023_dcode 		= tmp[23].ToString();               //処方機関コード;
                    cls.f024_dname 		= tmp[24].ToString();               //処方機関名;
                    cls.f025 				= tmp[25].ToString();           //クリ;
                    cls.f026 				= tmp[26].ToString();           //ケア;
                    cls.f027 				= tmp[27].ToString();           //容認;
                    cls.f028_newest 		= tmp[28].ToString();           //最新履歴;
                    cls.f029 				= tmp[29].ToString();           //原本所在;
                    cls.f030_prefcorss 	= tmp[30].ToString();               //県内外;
                    cls.f031 				= tmp[31].ToString();           //療養費種別;
                    cls.f032 				= tmp[32].ToString();           //食事基準額;
                    cls.f033 				= tmp[33].ToString();           //不当;
                    cls.f034 				= tmp[34].ToString();           //第三者;
                    cls.f035_kyufu_limit 	= tmp[35].ToString();           //給付制限;
                    cls.f036_expensive 	= tmp[36].ToString();               //高額;
                    cls.f037 				= tmp[37].ToString();           //予約;
                    cls.f038 				= tmp[38].ToString();           //処理;
                    cls.f039 				= tmp[39].ToString();           //疑義種別;
                    cls.f040 				= tmp[40].ToString();           //参考;
                    cls.f041 				= tmp[41].ToString();           //状態;
                    cls.f042 				= tmp[42].ToString();           //コード情報;
                    cls.f043 				= tmp[43].ToString();           //原本区分;
                    cls.f044_fusen01 		= tmp[44].ToString();           //付箋01;
                    cls.f045_fusen02 		= tmp[45].ToString();           //付箋02;
                    cls.f046_fusen03 		= tmp[46].ToString();           //付箋03;
                    cls.f047_fusen04 		= tmp[47].ToString();           //付箋04;
                    cls.f048_fusen05 		= tmp[48].ToString();           //付箋05;
                    cls.f049_fusen06 		= tmp[49].ToString();           //付箋06;
                    cls.f050_fusen07 		= tmp[50].ToString();           //付箋07;
                    cls.f051_fusen08 		= tmp[51].ToString();           //付箋08;
                    cls.f052_fusen09 		= tmp[52].ToString();           //付箋09;
                    cls.f053_fusen10 		= tmp[53].ToString();           //付箋10;
                    cls.f054_fusen11 		= tmp[54].ToString();           //付箋11;
                    cls.f055_fusen12 		= tmp[55].ToString();           //付箋12;
                    cls.f056_fusen13 		= tmp[56].ToString();           //付箋13;
                    cls.f057_fusen14 		= tmp[57].ToString();           //付箋14;
                    
                    
                    //被保険者記号番号メホール用;
                    
                    //記号_番号の半角書式にする
                    cls.hihonum_narrow 					= tmp[58].ToString();

                    //西暦を控えておく
                    cls.shinryoymad=int.Parse(tmp[59].ToString());    //診療年月西暦メホール用;
                    cls.shinsaymad= int.Parse(tmp[60].ToString());       //審査年月西暦メホール用;
                    cls.birthdayad = DateTime.Parse(tmp[61].ToString()); //生年月日西暦メホール用;
                    cls.cym = cym;                                               //メホール請求年月;

                    lstclsKokuho.Add(cls);

                    //wf.InvokeValue++;
                }

           //     wf.LogPrint("CSVロード終了");
                return lstclsKokuho;

            }


            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return lstclsKokuho;
            }
        }
    

        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {
            
            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from kokuhodata where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            
            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from kokuhodata group by cym order by cym desc");
            List<object[]> lst=cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
