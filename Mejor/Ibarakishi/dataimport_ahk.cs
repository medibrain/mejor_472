﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;


namespace Mejor.Ibarakishi
{
    /// <summary>
    /// 保険者から提供される　給付データテーブルクラス→2021/04/05国保データに移行するかも
    /// 2021/05/15結局あはきデータはエクセル
    /// </summary>
    public class dataimport_ahk
    {

        #region テーブル構造

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int F000importid { get;set; }=0;                         //管理用ID;
        public int F001renban { get; set; } = 0;                        //No.;
        public string F002insnum { get; set; } = string.Empty;          //保険者番号;
        public string F003kohifutan { get; set; } = string.Empty;       //公費負担者番号;
        public string F004hnum { get; set; } = string.Empty;            //被保険者証番号;
        public string F005kohijyukyu { get; set; } = string.Empty;      //公費受給者番号;
        public string F006pbirthday { get; set; } = string.Empty;       //生年月日;
        public string F007pgender { get; set; } = string.Empty;         //性別;
        public string F008pname { get; set; } = string.Empty;           //氏名;
        public string F009ym { get; set; } = string.Empty;              //施術年月;
        public string F010sid { get; set; } = string.Empty;             //医療機関コード;
        public string F011sregnumber { get; set; } = string.Empty;      //登録記号番号;
        public string F012shinsaym { get; set; } = string.Empty;        //審査年月;
        public string F013comnum { get; set; } = string.Empty;          //レセプト全国共通キー;
        public string F014 { get; set; } = string.Empty;                //支給区分;
        public string F015 { get; set; } = string.Empty;                //返戻理由        ;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;  //生年月日西暦;
        public int ymad { get; set; } = 0;                              //施術年月西暦;
        public int shinsaymad { get; set; } = 0;                        //審査年月西暦;
        public int cym { get; set; } = 0;                               //メホール請求年月;
        public string hnum_narrow { get; set; } = string.Empty;         //被保険者証番号半角;



        #endregion 

        public static int _cym = 0;

        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int cym)
        {
            _cym = cym;
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "茨木市あはき提供データ";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;


            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                var lst = new List<dataimport_ahk>();
                wf.LogPrint("Excelファイルを読み込んでいます");

                try
                {
                    using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                    {

                        var ex = System.IO.Path.GetExtension(fileName);
                        var xlsx = new XSSFWorkbook(fs);
                        var sheet = xlsx.GetSheetAt(0);
                        try
                        {
                            lst.AddRange(sheetTodataimport_ahks(sheet));
                            if (lst == null) return false;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + exc.Message);
                            return false;
                        }
                        finally
                        {
                            xlsx.Close();
                        }
                    }
                }
                catch(Exception exfs)
                {
                    MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + exfs.Message);
                    return false;
                }


                wf.SetMax(lst.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                

                using (var tran = DB.Main.CreateTransaction())
                {
                    //20210517165727 furukawa st ////////////////////////
                    //同じメホール請求年月のデータは先に削除
                    
                    DB.Command cmd = new DB.Command($"delete from dataimport_ahk where cym={cym}", tran);
                    cmd.TryExecuteNonQuery();
                    wf.LogPrint($"{cym}削除");
                    //20210517165727 furukawa ed ////////////////////////

                    wf.LogPrint("データベースに登録しています");

                    foreach (var item in lst)
                    {
                        //キャンセル処理
                        if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                        wf.InvokeValue++;
                        if (!DB.Main.Insert(item))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();

                }


                //20211005131848 furukawa st ////////////////////////
                //Appの更新も入力画面でなくこっちでやる


                if (!UpdateAppAndAUX(wf, cym, lst))
                {
                    MessageBox.Show("App更新に失敗しました");
                    return false;
                }
                //20211005131848 furukawa ed ////////////////////////

            }

            MessageBox.Show("インポートが終了しました");
            return true;

        }

        //20211005130100 furukawa st ////////////////////////
        //Appの更新も入力画面でなくこっちでやる

        /// <summary>
        /// App更新
        /// </summary>
        /// <param name="wf"></param>
        /// <param name="cym"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        private static bool UpdateAppAndAUX(WaitForm wf, int cym, List<dataimport_ahk> lst)
        {
            

            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd=null;
            StringBuilder sbSQL = new StringBuilder();

            try
            {

                #region app更新
                wf.LogPrint("App更新");
                sbSQL.Remove(0, sbSQL.ToString().Length);

                sbSQL.AppendLine(" update application set");
                sbSQL.AppendLine(" ayear 			= cast(substr(ahk.F009ym,2,2) as int),");
                sbSQL.AppendLine(" amonth 			= cast(substr(ahk.F009ym,4,2) as int),");
                sbSQL.AppendLine(" hnum				= ahk.hnum_narrow,");
                sbSQL.AppendLine(" pname			= ahk.f008pname,");
                sbSQL.AppendLine(" pbirthday		= ahk.pbirthdayad,");
                sbSQL.AppendLine(" psex				= cast(ahk.F007pgender as int) ,");
                sbSQL.AppendLine(" ym				= ahk.ymad,");
                sbSQL.AppendLine(" sid				= ahk.f010sid,");
                sbSQL.AppendLine(" sregnumber		= ahk.f011sregnumber,");
                sbSQL.AppendLine(" comnum			= ahk.f013comnum,");
                sbSQL.AppendLine(" inum             = ahk.F002insnum,");

                //あんま・鍼判定
                sbSQL.AppendLine($" aapptype			= ");
                sbSQL.AppendLine(" case substr(ahk.f010sid, 3, 1) ");
                sbSQL.AppendLine($" when '9' then {(int)APP_TYPE.あんま} ");
                sbSQL.AppendLine($" when '8' then {(int)APP_TYPE.鍼灸} ");
                sbSQL.AppendLine($" else {(int)APP_TYPE.NULL} end ,");


                //新規継続判定不要　2021/10/08森安さん



                sbSQL.AppendLine(" taggeddatas		= ");
                sbSQL.AppendLine(" 'GeneralString2:\"' || ahk.f012shinsaym || '\"|GeneralString3:\"' || ahk.shinsaymad || '\"'");

                sbSQL.AppendLine(" from dataimport_ahk ahk ,application_aux x");
                sbSQL.AppendLine(" where ");
                sbSQL.AppendLine(" ahk.F013comnum 			= replace(x.origfile,'.tif','') and");
                sbSQL.AppendLine(" application.aid 			= x.aid and ");
                sbSQL.AppendLine($" x.cym			={cym}");

                cmd=new  DB.Command(sbSQL.ToString(), tran);

                cmd.TryExecuteNonQuery();
                #endregion

                #region aux更新
                wf.LogPrint("AppAUX更新");
                sbSQL.Remove(0, sbSQL.ToString().Length);
                sbSQL.AppendLine(" update application_aux set ");
                sbSQL.AppendLine($" aapptype			= ");
                sbSQL.AppendLine(" case substr(ahk.f010sid, 3, 1) ");
                sbSQL.AppendLine($" when '9' then {(int)APP_TYPE.あんま} ");
                sbSQL.AppendLine($" when '8' then {(int)APP_TYPE.鍼灸} ");
                sbSQL.AppendLine($" else {(int)APP_TYPE.NULL} end ");

                //20211027111423 furukawa st ////////////////////////
                //dataimport_ahk.f000importidも取得しておく
                sbSQL.AppendLine($" ,matchingid01=cast(ahk.f000importid as varchar) ");
                //20211027111423 furukawa ed ////////////////////////


                sbSQL.AppendLine(" from dataimport_ahk ahk ");
                sbSQL.AppendLine(" where ");
                sbSQL.AppendLine(" ahk.F013comnum 			= replace(application_aux.origfile,'.tif','') ");
                sbSQL.AppendLine($" and application_aux.cym ={cym}");


                cmd = new DB.Command(sbSQL.ToString(), tran);

                cmd.TryExecuteNonQuery();

                #endregion


                wf.LogPrint("App更新成功");
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show("App更新失敗\r\n" + System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }
        
        //20211005130100 furukawa ed ////////////////////////



        /// <summary>
        /// シートからデータをコピー
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns></returns>
        private static List<dataimport_ahk> sheetTodataimport_ahks(ISheet sheet)
        {
            var l = new List<dataimport_ahk>();
            var rowCount = sheet.LastRowNum + 1;

            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    var row = sheet.GetRow(i);
                    if (i==0 && row.GetCell(12).StringCellValue != "レセプト全国共通キー")
                    {
                        MessageBox.Show("ファイルレイアウトが想定と違います。取り込んだファイルの中身を確認してください。","",
                            MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        return null;
                    }

                    if (!int.TryParse(getCellValueToString(row.GetCell(0)),out int tmpNo)) continue;
                    var dataimportahk = new dataimport_ahk();

                    dataimportahk.F001renban = int.Parse(getCellValueToString(row.GetCell(0)));     //No.;
                    dataimportahk.F002insnum = getCellValueToString(row.GetCell(1));                //保険者番号;
                    dataimportahk.F003kohifutan = getCellValueToString(row.GetCell(2));             //公費負担者番号;
                    dataimportahk.F004hnum = getCellValueToString(row.GetCell(3));                  //被保険者証番号;
                    dataimportahk.F005kohijyukyu = getCellValueToString(row.GetCell(4));            //公費受給者番号;
                    dataimportahk.F006pbirthday = getCellValueToString(row.GetCell(5));             //生年月日;
                    dataimportahk.F007pgender = getCellValueToString(row.GetCell(6));               //性別;
                    dataimportahk.F008pname = getCellValueToString(row.GetCell(7));                 //氏名;
                    dataimportahk.F009ym = getCellValueToString(row.GetCell(8));                    //施術年月;
                    dataimportahk.F010sid = getCellValueToString(row.GetCell(9));                   //医療機関コード;
                    dataimportahk.F011sregnumber = getCellValueToString(row.GetCell(10));           //登録記号番号;
                    dataimportahk.F012shinsaym = getCellValueToString(row.GetCell(11));             //審査年月;
                    dataimportahk.F013comnum = getCellValueToString(row.GetCell(12));               //レセプト全国共通キー;
                    dataimportahk.F014 = getCellValueToString(row.GetCell(13));                     //支給区分;
                    dataimportahk.F015 = getCellValueToString(row.GetCell(14));                     //返戻理由;

                    dataimportahk.pbirthdayad = DateTimeEx.GetDateFromJstr7(dataimportahk.F006pbirthday);                                    //生年月日西暦;
                    dataimportahk.ymad =DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(dataimportahk.F009ym));                                 //施術年月西暦;
                    dataimportahk.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(dataimportahk.F012shinsaym));                    //審査年月西暦;
                    dataimportahk.cym = _cym;
                    dataimportahk.hnum_narrow=Microsoft.VisualBasic.Strings.StrConv(dataimportahk.F004hnum,Microsoft.VisualBasic.VbStrConv.Narrow);

                    l.Add(dataimportahk);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                    continue;
                }
            }
            return l;
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            if (cell == null) return string.Empty;

            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }


        }


        /// <summary>
        /// データ抽出
        /// </summary>
        /// <param name="strwhere">抽出条件(whereいらんsql)</param>
        /// <returns></returns>
        public static List<dataimport_ahk> select(string strwhere)
        {
            List<dataimport_ahk> lst = new List<dataimport_ahk>();
            var l=DB.Main.Select<dataimport_ahk>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }


        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {

            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport_ahk where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport_ahk group by cym order by cym desc");
            List<object[]> lst = cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

    }

}
