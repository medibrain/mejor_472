﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Mejor
{
    class ViewerData
    {
        private const string NeedVer = "2.0.0.0";

        public int AID { get; set; }
        public int RrID { get; set; }
        public int CYM { get; set; }
        public int YM { get; set; }
        public APP_TYPE AppType { get; set; } = APP_TYPE.NULL;
        public string InsNum { get; set; } = string.Empty;
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Kana { get; set; } = string.Empty;
        public SEX Sex { get; set; } = SEX.Null;
        public int Birth { get; set; }
        public int ShokenDate { get; set; }
        public int StartDate { get; set; }
        public int FinishDate { get; set; }
        public int Family { get; set; }
        public string Fusho1 { get; set; } = string.Empty;
        public string Fusho2 { get; set; } = string.Empty;
        public string Fusho3 { get; set; } = string.Empty;
        public string Fusho4 { get; set; } = string.Empty;
        public string Fusho5 { get; set; } = string.Empty;
        public int FushoCount => string.IsNullOrEmpty(Fusho1) ? 0 :
            string.IsNullOrEmpty(Fusho2) ? 1 :
            string.IsNullOrEmpty(Fusho3) ? 2 :
            string.IsNullOrEmpty(Fusho4) ? 3 :
            string.IsNullOrEmpty(Fusho5) ? 4 : 5;
        public int AnmaCount { get; set; }
        public bool AnmaBody { get; set; } = false;
        public bool AnmaRightUpper { get; set; } = false;
        public bool AnmaLeftUpper { get; set; } = false;
        public bool AnmaRightLower { get; set; } = false;
        public bool AnmaLeftLower { get; set; } = false;
        public NEW_CONT NewCont { get; set; } = NEW_CONT.Null;
        public int Total { get; set; }
        public int Ratio { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int Days { get; set; }
        public bool VisitFee { get; set; } = false;
        public int DouiDate { get; set; }
        public string DrNum { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string ClinicNum { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public string GroupNum { get; set; } = string.Empty;
        public string GroupName { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Adds { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;
        public string DestKana { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdds { get; set; } = string.Empty;
        public string Numbering { get; set; } = string.Empty;
        public string ComNum { get; set; } = string.Empty;
        public string ImageFile { get; set; } = string.Empty;

        public static string Header =>
            "AID,RrID,CYM,YM,AppType,InsNum,Num,Name,Kana,Sex,Birth,ShokenDate," +
            "StartDate,FinishDate,Family,Fusho1,Fusho2,Fusho3,Fusho4,Fusho5," +
            "FushoCount,AnmaCount,AnmaBody,AnmaRightUpper,AnmaLeftUpper," +
            "AnmaRightLower,AnmaLeftLower,NewCont,Total,Ratio,Partial,Charge," +
            "Days,VisitFee,DouiDate,DrNum,DrName,ClinicNum,ClinicName," +
            "GroupNum,GroupName,Zip,Adds,DestName,DestKana,DestZip," +
            "DestAdds,Numbering,ComNum,ImageFile";

        public static ViewerData CreateViewerData(App app)
        {
            var vd = new ViewerData();
            vd.apply(app);
            return vd;
        }

        public void apply(App app)
        {
            AID = app.Aid;
            RrID = app.RrID;
            CYM = app.CYM;
            YM = app.YM;
            AppType = app.AppType;
            InsNum = app.InsNum;

            //20221205 ito st 兵庫広域は被保険者番号8桁ゼロパディング必要
            //Num = app.HihoNum;
            if (Insurer.CurrrentInsurer.InsurerID == (int)InsurerID.HYOGO_KOIKI2022)
            {
                Num = app.HihoNum.PadLeft(8, '0');
            }
            else
            {
                Num = app.HihoNum;
            }
            //20221205 ito end

            Name = app.PersonName != "" ? app.PersonName : app.HihoName; //20220908 ito /////愛媛広域のビューア出力の際、PersonNameが空で漢字氏名が出ないため
            //Name = app.PersonName;

            Kana = app.TaggedDatas.Kana;
            Sex = (SEX)app.Sex;
            Birth = app.Birthday.TointWithNullDate();
            ShokenDate = app.FushoFirstDate1.TointWithNullDate();
            StartDate = app.FushoStartDate1.TointWithNullDate();
            FinishDate = app.FushoFinishDate1.TointWithNullDate();
            Family = app.Family;
            if (app.AppType == APP_TYPE.鍼灸)
            {
                var fsFunc = new Func<string, string>(s =>
                {
                    int hid;
                    int.TryParse(s, out hid);
                    return (0 < hid && hid < 8) ? ((HARI_BYOMEI)hid).ToString() : s;
                });
                Fusho1 = fsFunc(app.FushoName1);
                Fusho2 = fsFunc(app.FushoName2);
                Fusho3 = fsFunc(app.FushoName3);
                Fusho4 = fsFunc(app.FushoName4);
                Fusho5 = fsFunc(app.FushoName5);
            }

            //20220526095238 furukawa st ////////////////////////
            //大阪広域2022年4月から柔整、鍼、あんま全部負傷名入れる
            

            //      20211227131722 furukawa st ////////////////////////
            //      大阪広域2021年度中はあんまの負傷名は出力しない。2022年4月から戻す予定            
            //      else if ((app.AppType == APP_TYPE.あんま) && 
            //          Insurer.CurrrentInsurer.InsurerID==(int)InsurerID.OSAKA_KOIKI)
            //      {
            //          Fusho1 = string.Empty;
            //          Fusho2 = string.Empty;
            //          Fusho3 = string.Empty;
            //          Fusho4 = string.Empty;
            //          Fusho5 = string.Empty;
            //      }
            //      //20211227131722 furukawa ed ////////////////////////
            //      else
            //      {
                Fusho1 = app.FushoName1;
                Fusho2 = app.FushoName2;
                Fusho3 = app.FushoName3;
                Fusho4 = app.FushoName4;
                Fusho5 = app.FushoName5;
            //      }

            //20220526095238 furukawa ed ////////////////////////


            AnmaCount = app.Bui >= 100000 ? app.Bui / 100000 : 0;
            AnmaBody = AnmaCount == 5 ? true : app.Bui / 10000 % 10 == 1;
            AnmaRightUpper = AnmaCount == 5 ? true : app.Bui / 1000 % 10 == 1;
            AnmaLeftUpper = AnmaCount == 5 ? true : app.Bui / 100 % 10 == 1;
            AnmaRightLower = AnmaCount == 5 ? true : app.Bui / 10 % 10 == 1;
            AnmaLeftLower = AnmaCount == 5 ? true : app.Bui % 10 == 1;
            NewCont = app.NewContType;
            Total = app.Total;
            Ratio = app.Ratio;
            Partial = app.Partial;
            Charge = app.Charge;
            Days = app.CountedDays;
            VisitFee = app.Distance > 0;
            DouiDate = app.TaggedDatas.DouiDate.TointWithNullDate();
            DrNum = app.DrNum;
            DrName = app.DrName;
            ClinicNum = app.ClinicNum;
            ClinicName = app.ClinicName;
            GroupNum = string.Empty;
            GroupName = string.Empty;
            Zip = app.HihoZip;
            Adds = app.HihoAdd;
            DestName = string.Empty;
            DestKana = string.Empty;
            DestZip = string.Empty;
            DestAdds = string.Empty;
            Numbering = app.Numbering;
            ComNum = app.ComNum;
            ImageFile = app.Aid.ToString() + ".tif";
        }

        public string CreateCsvLine()
        {
            var s = new string[50];
            int i = 0;
            //"AID,RrID,CYM,YM,AppType,InsNum,Num,Name,Kana,Sex,Birth,ShokenDate," +
            s[i++] = AID.ToString();
            s[i++] = RrID.ToString();
            s[i++] = CYM.ToString();
            s[i++] = YM.ToString();
            s[i++] = ((int)AppType).ToString();
            s[i++] = InsNum;
            s[i++] = Num;
            s[i++] = Name;
            s[i++] = Kana;
            s[i++] = ((int)Sex).ToString();
            s[i++] = Birth.ToString();
            s[i++] = ShokenDate.ToString();
            //"StartDate,FinishDate,Family,Fusho1,Fusho2,Fusho3,Fusho4,Fusho5," +
            s[i++] = StartDate.ToString();
            s[i++] = FinishDate.ToString();
            s[i++] = ((int)Family).ToString();
            s[i++] = Fusho1;
            s[i++] = Fusho2;
            s[i++] = Fusho3;
            s[i++] = Fusho4;
            s[i++] = Fusho5;
            //"FushoCount,AnmaCount,AnmaBody,AnmaRightUpper,AnmaLeftUpper," +
            s[i++] = FushoCount.ToString();
            s[i++] = AnmaCount.ToString();
            s[i++] = AnmaBody ? "1" : "0";
            s[i++] = AnmaRightUpper ? "1" : "0";
            s[i++] = AnmaLeftUpper ? "1" : "0";
            //"AnmaRightLower,AnmaLeftLower,NewCont,Total,Ratio,Partial,Charge," +
            s[i++] = AnmaRightLower ? "1" : "0";
            s[i++] = AnmaLeftLower ? "1" : "0";
            s[i++] = ((int)NewCont).ToString();
            s[i++] = Total.ToString();
            s[i++] = Ratio.ToString();
            s[i++] = Partial.ToString();
            s[i++] = Charge.ToString();
            //"Days,VisitFee,DouiDate,DrNum,DrName,ClinicNum,ClinicName," +
            s[i++] = Days.ToString();
            s[i++] = VisitFee ? "1" : "0";
            s[i++] = DouiDate.ToString();
            s[i++] = DrNum;
            s[i++] = DrName;
            s[i++] = ClinicNum;
            s[i++] = ClinicName;
            //"GroupNum,GroupName,Zip,Adds,DestName,DestKana,DestZip," +
            s[i++] = GroupNum;
            s[i++] = GroupName;
            s[i++] = Zip;
            s[i++] = Adds;
            s[i++] = DestName;
            s[i++] = DestKana;
            s[i++] = DestZip;
            //"DestAdds,Numbering,ComNum,ImageFile";
            s[i++] = DestAdds;
            s[i++] = Numbering;
            s[i++] = ComNum;
            s[i++] = ImageFile;

            for (int c = 0; c < s.Length; c++)
                s[c]=$"\"{s[c].Replace("\"", "\"\"")}\"";

            return string.Join(",", s);
        }

        public static bool CreateInfo(string fileName, string insName, int cyyyymm, int count)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine($"必要バージョン:{NeedVer}");
                    sw.WriteLine($"保険者名:{insName}");
                    sw.WriteLine($"処理年月:{cyyyymm}");
                    sw.WriteLine($"作成日:{DateTime.Now.ToString()}");
                    sw.WriteLine($"件数:{count}");
                    sw.WriteLine($"作成者ID:{User.CurrentUser.UserID}");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// ビューアデータを作成します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="apps"></param>
        /// <param name="infoFileName"></param>
        /// <param name="wf"></param>
        /// <returns>出力数</returns>
        public static int Export(int cym, List<App>apps, string infoFileName, WaitForm wf)
        {
            var dir = Path.GetDirectoryName(infoFileName);
            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            int receCount = 0;

            try
            {
                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];

                        var vd = CreateViewerData(app);

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.長期 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.往療内訳 ||
                                apps[i].YM == (int)APP_SPECIAL_CODE.同意書)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                wf.LogPrint("ViewerInfoデータを作成しています");
                if (!CreateInfo(infoFileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return -1;
            }
            catch (Exception ex)
            {
                wf.LogPrint(ex.Message);
                wf.LogPrint("Viewerデータの作成に失敗しました");
                Log.ErrorWriteWithMsg(ex);
                return -1;
            }
            return receCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool ExportGeneralViewerData(int cym)
        {
            string infoFileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                infoFileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(infoFileName);
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //ビューアデータ
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }
    }
}
