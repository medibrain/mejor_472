@echo off

REM 20210120180735 furukawa db作成、基本dbリストア処理
SET PATHPOS11=c:\\program files\\postgresql\\11\\bin
SET PATHPOS12=d:\\program files\\postgresql\\11\\bin
SET PATHPOS21=c:\\program files\\postgresql\\12\\bin
SET PATHPOS22=d:\\program files\\postgresql\\12\\bin
SET PATHPOS31=c:\\program files\\postgresql\\13\\bin
SET PATHPOS32=d:\\program files\\postgresql\\13\\bin
SET PATHPOS41=c:\\program files\\postgresql\\14\\bin
SET PATHPOS42=d:\\program files\\postgresql\\14\\bin

REM 20210404105018 furukawa psqlのフォルダ存在確認、ある方を使う
if exist %PATHPOS41% (
cd /d %PATHPOS41%
)
else if exist %PATHPOS42%(
cd /d %PATHPOS42%
)
else if exist %PATHPOS31% (
cd /d %PATHPOS31%
)
else if exist %PATHPOS32%(
cd /d %PATHPOS32%
)
else if exist %PATHPOS21% (
cd /d %PATHPOS21%
)
else if exist %PATHPOS22%(
cd /d %PATHPOS22%
)
else if exist %PATHPOS11% (
cd /d %PATHPOS11%
)
else if exist %PATHPOS12%(
cd /d %PATHPOS12%
)



REM 設定値 SERVER PORT DBNAME

SET SERVER=%1
SET PORT=%2
SET DBNAME=%3


SET FOLDER=%~dp0

rem 20210708182223 furukawa st ////////////////////////
rem ストアドを引数に

SET STORED=%4
rem SET STORED=ParentRelation2.sql

rem 20210708182223 furukawa ed ////////////////////////

SET STOREDPATH=%FOLDER:\=\\%%STORED%


REM SET STORED=%~dp0\ParentRelation2.sql

echo create stored ParentRelation
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname %DBNAME% -f  %STOREDPATH% 
REM psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname %DBNAME% --command  %STORED% 

