CREATE OR REPLACE FUNCTION public.BatchSheetRelation(
    incym integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare 
_incym integer:=incym;

curNAPP cursor for select a.aid from application a where a.cym=_incym and a.ayear>0 order by a.aid;
--curNAPP cursor for select a.aid from application a where a.cym=_incym and a.ayear<0 and a.ayear in (-7,-8) order by a.aid;


curAPP refcursor;
RecordCounter integer:=0;
NonAppAID integer:=0;
cntRel integer:=1;
AppAID integer:=0;


begin

    open curNAPP;

    loop
        fetch curNAPP INTO NonAppAID;
        raise notice '1:%',NonAppAID;
        
        if NonAppAID is null then 
            exit;
        end if;
        
        cntRel:=1;
        loop 

            
            open curAPP for  select a.aid from application a where a.cym=_incym and a.ayear in (-7) and a.aid = NonAppAID - cntRel;
            --open curAPP for select a.aid from application a where a.cym=_incym and a.ayear>0 and a.aid = NonAppAID - cntRel;
            
            fetch curAPP into AppAID;
            
            raise notice '2:%',NonAppAID - RecordCounter;
            raise notice '3:%',AppAID;

            --グループまたぎの時はAIDが1000単位で変わるので2つ以上グループを戻らないとバッチが見つからないときがある
            if AppAID is null or AppAID=0 then
                if cntRel>2001 then 
                    close curAPP;
                    exit;
                end if;
                
                cntRel:=cntRel+1;				
                --RecordCounter:=RecordCounter+1;
                
                raise notice '4:% %',AppAID,cntRel;
                
                close curAPP;
                continue;
            end if;
        
            
            raise notice '5:%' ,AppAID;
            update application_aux set batchaid=AppAID where aid=NonAppAID and batchaid=0;
            close curAPP;
            exit;
            
        end loop;
        
        RecordCounter:=RecordCounter+1;
    end loop;
    
    close curNAPP;	
	
	
	return RecordCounter;	

end;
$BODY$;
ALTER FUNCTION public.BatchSheetRelation(integer)
    OWNER TO postgres;