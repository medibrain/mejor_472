-- FUNCTION: public.parentrelation(integer)

-- DROP FUNCTION public.parentrelation(integer);

CREATE OR REPLACE FUNCTION public.parentrelation(
	incym integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare 
_incym integer :=incym;

--全appを調査する
curApp cursor for select * from application a 
	where a.cym=_incym
	order by a.aid;

rec record;
pAID integer :=0;
reccnt integer:=0;
begin

	--リセット
	update application_aux set parentaid=0 where cym=_incym;
	
	open curApp;

	loop
		fetch curApp INTO rec;
			exit when NOT FOUND;
		
		--申請書
		if rec.aapptype>0 then
			update application_aux set parentaid=0 where aid=rec.aid;
			pAID:=rec.aid;--親AIDとする
			reccnt=reccnt+1;
		--バッチ以外の続紙
 		elseif rec.aapptype<0 and rec.aapptype<>-7 and rec.aapptype<>-8 then
 			update application_aux set parentaid=pAID where aid=rec.aid;			
			reccnt=reccnt+1;
 		--上記以外
		else
 			update application_aux set parentaid=-1 where aid=rec.aid ;			
			reccnt=reccnt+1;
		end if;
		
	end loop;
	
    close curApp;	
	
	return reccnt;
end;
$BODY$;

ALTER FUNCTION public.parentrelation(integer)
    OWNER TO postgres;
