﻿namespace Mejor
{
    partial class OtherMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.buttonPostalCode = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.buttonOcr = new System.Windows.Forms.Button();
            this.buttonOryoFlag = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.buttonDistance = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttonSFMng = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(227, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(183, 44);
            this.button2.TabIndex = 15;
            this.button2.Text = "複数月\r\n金額計算ツール";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonPostalCode
            // 
            this.buttonPostalCode.Location = new System.Drawing.Point(38, 18);
            this.buttonPostalCode.Name = "buttonPostalCode";
            this.buttonPostalCode.Size = new System.Drawing.Size(183, 44);
            this.buttonPostalCode.TabIndex = 14;
            this.buttonPostalCode.Text = "郵便番号・住所 検索";
            this.buttonPostalCode.UseVisualStyleBackColor = true;
            this.buttonPostalCode.Click += new System.EventHandler(this.buttonPostalCode_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(227, 168);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(183, 44);
            this.button8.TabIndex = 13;
            this.button8.Text = "アーカイブ";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // buttonOcr
            // 
            this.buttonOcr.Location = new System.Drawing.Point(227, 68);
            this.buttonOcr.Name = "buttonOcr";
            this.buttonOcr.Size = new System.Drawing.Size(183, 44);
            this.buttonOcr.TabIndex = 11;
            this.buttonOcr.Text = "OCR過去データ突合";
            this.buttonOcr.UseVisualStyleBackColor = true;
            this.buttonOcr.Click += new System.EventHandler(this.buttonOcr_Click);
            // 
            // buttonOryoFlag
            // 
            this.buttonOryoFlag.Enabled = false;
            this.buttonOryoFlag.Location = new System.Drawing.Point(227, 118);
            this.buttonOryoFlag.Name = "buttonOryoFlag";
            this.buttonOryoFlag.Size = new System.Drawing.Size(183, 44);
            this.buttonOryoFlag.TabIndex = 12;
            this.buttonOryoFlag.Text = "往療点検\r\n対象フラグ追加";
            this.buttonOryoFlag.UseVisualStyleBackColor = true;
            this.buttonOryoFlag.Click += new System.EventHandler(this.buttonOryoFlag_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(38, 168);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(183, 44);
            this.button7.TabIndex = 10;
            this.button7.Text = "請求金額計算チェック";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // buttonDistance
            // 
            this.buttonDistance.Enabled = false;
            this.buttonDistance.Location = new System.Drawing.Point(38, 118);
            this.buttonDistance.Name = "buttonDistance";
            this.buttonDistance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonDistance.Size = new System.Drawing.Size(183, 44);
            this.buttonDistance.TabIndex = 9;
            this.buttonDistance.Text = "往療距離自動計算\r\n一覧CSV出力";
            this.buttonDistance.UseVisualStyleBackColor = true;
            this.buttonDistance.Click += new System.EventHandler(this.buttonDistance_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(38, 68);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(183, 44);
            this.button3.TabIndex = 8;
            this.button3.Text = "エラー一覧";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(38, 218);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(183, 44);
            this.button9.TabIndex = 10;
            this.button9.Text = "学校共済分析資料出力";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // buttonSFMng
            // 
            this.buttonSFMng.Location = new System.Drawing.Point(227, 218);
            this.buttonSFMng.Name = "buttonSFMng";
            this.buttonSFMng.Size = new System.Drawing.Size(183, 44);
            this.buttonSFMng.TabIndex = 13;
            this.buttonSFMng.Text = "ステータスフラグ管理";
            this.buttonSFMng.UseVisualStyleBackColor = true;
            this.buttonSFMng.Click += new System.EventHandler(this.buttonSFMng_Click);
            // 
            // OtherMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 280);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonPostalCode);
            this.Controls.Add(this.buttonSFMng);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.buttonOcr);
            this.Controls.Add(this.buttonOryoFlag);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.buttonDistance);
            this.Controls.Add(this.button3);
            this.Name = "OtherMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "その他";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonPostalCode;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button buttonOcr;
        private System.Windows.Forms.Button buttonOryoFlag;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonDistance;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttonSFMng;
    }
}