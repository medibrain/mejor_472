﻿namespace Mejor.Sakurashi_Kokuho

{
    partial class frmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJyu = new System.Windows.Forms.Button();
            this.btnAhk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnJyu
            // 
            this.btnJyu.Location = new System.Drawing.Point(26, 32);
            this.btnJyu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnJyu.Name = "btnJyu";
            this.btnJyu.Size = new System.Drawing.Size(149, 53);
            this.btnJyu.TabIndex = 0;
            this.btnJyu.Text = "柔整";
            this.btnJyu.UseVisualStyleBackColor = true;
            this.btnJyu.Click += new System.EventHandler(this.btnJyu_Click);
            // 
            // btnAhk
            // 
            this.btnAhk.Location = new System.Drawing.Point(183, 32);
            this.btnAhk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAhk.Name = "btnAhk";
            this.btnAhk.Size = new System.Drawing.Size(149, 53);
            this.btnAhk.TabIndex = 0;
            this.btnAhk.Text = "あはき";
            this.btnAhk.UseVisualStyleBackColor = true;
            this.btnAhk.Click += new System.EventHandler(this.btnAhk_Click);
            // 
            // frmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 115);
            this.Controls.Add(this.btnAhk);
            this.Controls.Add(this.btnJyu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "申請書種別";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnJyu;
        private System.Windows.Forms.Button btnAhk;
    }
}