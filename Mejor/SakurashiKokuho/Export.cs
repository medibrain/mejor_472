﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Sakurashi_Kokuho   

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class Export
    {

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public int exportid { get; set; } = 0;                     //プライマリキー メホール管理用

        public int cym { get; set; } = 0;                           //メホール上の処理年月 メホール管理用
        public int ym { get; set; } = 0;                            //診療年月 メホール管理用
        public int aid { get; set; } = 0;                           //aidメホール管理用
        public string numbering { get; set; } = string.Empty;        //ナンバリング 7 1桁目は申請場所の区番号。右6桁は連番。例）1000001・1000002・2000001
        public string shoriym { get; set; } = string.Empty;          //処理年月 5 「5」→令和
        public string shinsei_basho { get; set; } = string.Empty;    //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区
        public string apptype { get; set; } = string.Empty;          //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
        public string ippantaishoku { get; set; } = string.Empty;    //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
        public string hnum { get; set; } = string.Empty;             //被保険者証番号 8 8桁の数字とする（前「0」）。
        public string atenanum { get; set; } = string.Empty;         //宛名番号 0 ブランク
        public string mainname { get; set; } = string.Empty;         //世帯主氏名 25 全角
        public string pname { get; set; } = string.Empty;            //療養を受けた者の氏名 25 全角
        public string psex { get; set; } = string.Empty;             //性別 1 「1」→男、「2」→女
        public string pbirth { get; set; } = string.Empty;           //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
        public string startdate1 { get; set; } = string.Empty;       //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和
        public string shinseiymd { get; set; } = string.Empty;       //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和
        public string clinicname { get; set; } = string.Empty;       //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
        public string drname { get; set; } = string.Empty;           //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
        public string ratio { get; set; } = string.Empty;            //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
        public string total { get; set; } = string.Empty;            //合計金額（費用額） 9 ゼロ埋めなし。
        public string charge { get; set; } = string.Empty;           //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
        public string partial { get; set; } = string.Empty;          //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
        public string counteddays { get; set; } = string.Empty;      //実日数 2 ゼロ埋めなし。
        public string bcode { get; set; } = string.Empty;            //金融機関コード 4 4桁の数字とする。
        public string bname { get; set; } = string.Empty;            //金融機関名 15 全角
        public string bbcode { get; set; } = string.Empty;           //支店コード 3 3桁の数字とする。
        public string bbname { get; set; } = string.Empty;           //支店名 15 全角
        public string bacctype { get; set; } = string.Empty;         //科目 1 「1」→普通、「2」→当座
        public string baccnumber { get; set; } = string.Empty;       //口座番号 7 ゼロ埋め。
        public string baccname { get; set; } = string.Empty;         //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ

        #endregion
  
      

        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool ExportData(int cym)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(cym);
            
            //保存場所
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
               
                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable(lstApp, cym, wf)) return false;


                //ここでナンバリングaid順
               // if (!CreateRenban(cym,wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                //出力
                if (!DataExport(cym, strNohinDir,wf)) return false;
             

                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 保険者ごとに、aid順でソートして保険者ごとに連番
        /// </summary>   
        /// <param name="cym">cym</param>
        /// <returns>失敗=false</returns>
        private static bool CreateRenban(int cym,WaitForm wf)
        {
            wf.LogPrint("連番付与");

            string strsql = "";
            //保険者ごとに、aid順でソートして保険者ごとに連番？要試験
            //https://buralog.jp/postgresql-row-number-function-group-sequence-number/
            strsql = "update export set numbering=insnum || LPAD(cast(row_number() over (partition by insnum order by aid asc) as varchar),6,'0') " +
                $"where cym={cym} and Length(numbering)<2 " +
                $"order by aid;";

            DB.Command cmd = new DB.Command(DB.Main,strsql);

            return cmd.TryExecuteNonQuery();


        }


        
        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <returns></returns>
        private static bool InsertExportTable(List<App> lstApp,int cym ,WaitForm wf)
        {
            wf.SetMax(lstApp.Count);


            DB.Transaction tran;
            tran=DB.Main.CreateTransaction();
                        
            try
            {
                //今月分を削除
                DB.Command cmd = new DB.Command($"delete from export where cym='{cym}'", tran);
                cmd.TryExecuteNonQuery();
                
                int cnt = 0;

                foreach (App item in lstApp)
                {

                    Export exp = new Export();

                    cnt++;
                    
                    exp.cym = item.CYM;                                  //メホール上の処理年月 メホール管理用
                    exp.ym = item.YM;                                    //診療年月 メホール管理用
                    exp.aid = item.Aid;                                  //aidメホール管理用
                    exp.numbering = item.InsNum.ToString();              //ナンバリング 7 1桁目は申請場所の区番号。右6桁は連番。例）1000001・1000002・2000001
                    exp.shoriym = (DateTimeEx.GetEraNumberYearFromYYYYMM(item.CYM)*100+item.CYM%100).ToString();   //処理年月 5 「5」→令和
                    exp.shinsei_basho = item.InsNum;                     //申請場所 1 「1」→葵区、「2」→駿河区、「3」→清水区


                    if (item.AppType == APP_TYPE.あんま) exp.apptype = "17";    //療養費の種類 2 「17」→あん摩・マッサージ、「19」→はり・きゅう
                    if (item.AppType == APP_TYPE.鍼灸) exp.apptype = "19";

                    exp.ippantaishoku = item.TaggedDatas.GeneralString4;        //一般退職区分 1 「1」→一般、「2」→退職本人、「3」→退職扶養
                    exp.hnum = item.HihoNum.ToString().PadLeft(8,'0');          //被保険者証番号 8 8桁の数字とする（前「0」）。
                    exp.atenanum = string.Empty;                                //宛名番号 0 ブランク
                    exp.mainname = item.HihoName;                               //世帯主氏名 25 全角
                    exp.pname = item.PersonName;                                //療養を受けた者の氏名 25 全角
                    exp.psex = item.Sex.ToString();                             //性別 1 「1」→男、「2」→女
                    exp.pbirth = DateTimeEx.GetIntJpDateWithEraNumber(item.Birthday).ToString();                //生年月日 7 和暦の数字7桁とする。「3」→昭和、「4」→平成、「5」→令和
                    exp.startdate1 = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();     //施術年月日 7 当該月の最初の施術日、和暦の数字7桁とする。「4」→平成、「5」→令和

                    if(DateTime.TryParse(item.TaggedDatas.GeneralString1, out DateTime dtshinsei))
                        exp.shinseiymd = DateTimeEx.GetIntJpDateWithEraNumber(dtshinsei).ToString();           //申請受付年月日 7 静岡市の受付印の日付和暦の数字7桁とする。「4」→平成、「5」→令和



                    exp.clinicname = item.ClinicName;                        //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    exp.drname = item.DrName;                                //施術機関名、施術師名 18 全角。施術機関名と施術師名の間は、１文字分のスペースを空ける。出力時につなぐ
                    exp.ratio = item.Ratio.ToString();                       //給付割合 1 保険者負担割合。「7」→7割、「8」→8割、「9」→9割
                    exp.total = item.Total.ToString();                       //合計金額（費用額） 9 ゼロ埋めなし。
                    exp.charge = item.Charge.ToString();                     //請求額 9 ゼロ埋めなし。記載がない場合、合計金額に給付割合を10で除した値を乗じた額（1円未満の端数切捨て）
                    exp.partial = item.Partial.ToString();                   //一部負担金 9 ゼロ埋めなし。記載がない場合、合計金額から請求額を差し引いた額
                    exp.counteddays = item.CountedDays.ToString();           //実日数 2 ゼロ埋めなし。
                    exp.bcode = item.TaggedDatas.GeneralString2;             //金融機関コード 4 4桁の数字とする。
                    exp.bname = item.BankName;                               //金融機関名 15 全角
                    exp.bbcode = item.TaggedDatas.GeneralString3;            //支店コード 3 3桁の数字とする。
                    exp.bbname = item.BankBranch;                            //支店名 15 全角
                    exp.bacctype = item.AccountType.ToString();              //科目 1 「1」→普通、「2」→当座
                    exp.baccnumber = item.AccountNumber.PadLeft(7,'0');      //口座番号 7 ゼロ埋め。
                    exp.baccname = item.AccountKana;                         //口座名義（ﾌﾘｶﾞﾅ） 30 半角ｶﾀｶﾅ


                    //同ヒホバン、生年月日、性別の人は出さない
                    //既出テーブルを使用してもいいが、2度手間になりそう…
                    DB.Command cmddup = new DB.Command( 
                        $"select * from export where hnum='{item.HihoNum}' " +
                        $"and pbirth='{item.Birthday.ToString("yyyy/MM/dd")}' " +
                        $"and psex='{item.Sex.ToString()}'",tran);


                    if (cmddup.TryExecuteScalar() != null)
                    {
                        wf.LogPrint($"{item.HihoNum}既に出しました");                        
                        wf.InvokeValue++;
                        continue;
                    }


                    if(!DB.Main.Insert<Export>(exp, tran)) return false;

                    wf.LogPrint($"aid:{exp.aid}");
                    wf.InvokeValue++;
                }
                tran.Commit();
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }

               
        }




        /// <summary>
        /// DBからデータを取得してCSV出力まで
        /// <param name="cym">処理年月</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        private static bool DataExport(int cym,string strDir,WaitForm wf)            
        {
            
            string strFileName = strDir + "\\";
            string strHeader = string.Empty;
            string strSQL = string.Empty;

            if (!System.IO.Directory.Exists(strDir)) System.IO.Directory.CreateDirectory(strDir);

            wf.LogPrint("データ取得");

            strFileName += $"{cym}_療養費振込データ.csv";

            #region sql
            strSQL =
                "SELECT " +
                  //"numbering," +
                "shinsei_basho || lpad(cast(row_number() over (partition by shinsei_basho " +
                "order by shinsei_basho,apptype,ippantaishoku,hnum,pname,startdate1,clinicname asc) as varchar),6,'0') numbering ," +
                "shoriym," +				//処理年月
                "shinsei_basho," +          //申請場所
                "apptype," +                //療養費の種類
                "ippantaishoku," +          //一般退職区分
                "hnum," +                   //被保険者証番号
                "atenanum," +               //宛名番号
                "mainname," +               //世帯主氏名
                "pname," +                  //療養を受けた者の氏名
                "psex," +                   //性別
                "pbirth," +                 //生年月日
                "startdate1," +             //施術年月日
                "shinseiymd," +             //申請受付年月日

                //20200706120217 furukawa st ////////////////////////
                //施術所、施術師は両方で18文字なので前から取る
                
                "substring(clinicname || ' ' || drname,1,18)," +   //施術機関名、施術師名
                //"clinicname || ' ' || drname," +   //施術機関名、施術師名

                //20200706120217 furukawa ed ////////////////////////


                "ratio," +                  //給付割合
                "total," +                  //合計金額（費用額）
                "charge," +                 //請求額
                "partial," +                //一部負担金
                "counteddays," +            //実日数
                "bcode," +                  //金融機関コード
                "bname," +                  //金融機関名
                "bbcode," +                 //支店コード
                "bbname," +                 //支店名
                "bacctype," +               //科目
                "baccnumber," +             //口座番号
                "baccname ";                //口座名義（ﾌﾘｶﾞﾅ）

            #endregion

            strSQL += $"FROM export where cym ='{cym}'";

            //20200706121703 furukawa st ////////////////////////
            //申請書だけ出す
            
            strSQL += $" and  ym>0 ";
            //20200706121703 furukawa ed ////////////////////////


            //ソート順
            //住所区（葵区→駿河区→清水区）=>ナンバリング
            //療養費の種類（はり・きゅう→あん摩・マッサージ）
            //一般退職区分（一般→退職本人→退職扶養）
            //被保険者証番号
            //療養を受けた者（受診者）
            //施術年月
            //施術所


            strSQL += $" order by numbering";
            //strSQL += $" order by  numbering,apptype,ippantaishoku,hnum,pname,startdate1,clinicname";

            DB.Command cmd = new DB.Command(DB.Main, strSQL);
            var l=cmd.TryExecuteReaderList();

            wf.LogPrint("ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(l.Count);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName,false,System.Text.Encoding.GetEncoding("shift_jis"));
            
            try
            { 

                foreach(var e in l)
                {
                    string strres = string.Empty;
                    for (int colcnt = 0; colcnt < e.Length; colcnt++)
                    {
                        strres += $"{e[colcnt]},";
                    }
                    strres = strres.Substring(0, strres.Length - 1);

                    sw.WriteLine(strres);
                   
                    wf.InvokeValue++;
                }

                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                sw.Close();            
            }

        }



    }

    /// <summary>
    /// 既出テーブルだが使わないかも
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
