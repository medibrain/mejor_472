﻿namespace Mejor.Sakurashi_Kokuho

{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.labelHs = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxHonkeKbn = new Mejor.VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.verifyBoxHihoname = new Mejor.VerifyBox();
            this.label46 = new System.Windows.Forms.Label();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.pFusho = new System.Windows.Forms.Panel();
            this.verifyBoxDays1 = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxF1fsD = new Mejor.VerifyBox();
            this.verifyBoxF1fsE = new Mejor.VerifyBox();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish1 = new Mejor.VerifyBox();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label9 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstD = new Mejor.VerifyBox();
            this.verifyBoxF1fsM = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxF1fsY = new Mejor.VerifyBox();
            this.label10 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.verifyBoxBuiCount = new Mejor.VerifyBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.verifyBoxHosName = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new Mejor.VerifyBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.verifyBoxKyoKei = new Mejor.VerifyBox();
            this.verifyBoxDantaiKojin = new Mejor.VerifyBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelMatchWhere = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.checkBoxNumber = new System.Windows.Forms.CheckBox();
            this.panelLeft.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.pFusho.SuspendLayout();
            this.panelMatchWhere.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(769, 869);
            this.buttonUpdate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(120, 29);
            this.buttonUpdate.TabIndex = 200;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(183, 55);
            this.labelYear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(22, 15);
            this.labelYear.TabIndex = 3;
            this.labelYear.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(52, 43);
            this.labelM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(37, 15);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(121, 9);
            this.labelHnum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(52, 15);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保番";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(8, 12);
            this.labelYearInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(79, 45);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++\r\nヘッダ:**";
            // 
            // labelHs
            // 
            this.labelHs.AutoSize = true;
            this.labelHs.Location = new System.Drawing.Point(85, 55);
            this.labelHs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHs.Name = "labelHs";
            this.labelHs.Size = new System.Drawing.Size(37, 15);
            this.labelHs.TabIndex = 1;
            this.labelHs.Text = "和暦";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.buttonImageChange);
            this.panelLeft.Controls.Add(this.buttonImageRotateL);
            this.panelLeft.Controls.Add(this.buttonImageFill);
            this.panelLeft.Controls.Add(this.buttonImageRotateR);
            this.panelLeft.Controls.Add(this.userControlImage1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(289, 0);
            this.panelLeft.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(143, 902);
            this.panelLeft.TabIndex = 1;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(101, 869);
            this.buttonImageChange.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(53, 29);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 869);
            this.buttonImageRotateL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(47, 29);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-255, 869);
            this.buttonImageFill.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(100, 29);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(53, 869);
            this.buttonImageRotateR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(47, 29);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(5);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(143, 902);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelMatchWhere);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelHs);
            this.panelRight.Controls.Add(this.labelYear);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(432, 0);
            this.panelRight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1360, 902);
            this.panelRight.TabIndex = 0;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 99);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(5);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(266, 124);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1358, 487);
            this.scrollPictureControl1.TabIndex = 39;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxHonkeKbn);
            this.panelHnum.Controls.Add(this.label24);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.label21);
            this.panelHnum.Controls.Add(this.label22);
            this.panelHnum.Controls.Add(this.labelMacthCheck);
            this.panelHnum.Controls.Add(this.verifyBoxHihoname);
            this.panelHnum.Controls.Add(this.label46);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.label2);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Location = new System.Drawing.Point(211, 12);
            this.panelHnum.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(1133, 88);
            this.panelHnum.TabIndex = 10;
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(687, 31);
            this.verifyBoxFamily.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFamily.MaxLength = 1;
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxFamily.TabIndex = 18;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxHonkeKbn
            // 
            this.verifyBoxHonkeKbn.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHonkeKbn.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHonkeKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHonkeKbn.Location = new System.Drawing.Point(541, 31);
            this.verifyBoxHonkeKbn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxHonkeKbn.MaxLength = 1;
            this.verifyBoxHonkeKbn.Name = "verifyBoxHonkeKbn";
            this.verifyBoxHonkeKbn.NewLine = false;
            this.verifyBoxHonkeKbn.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxHonkeKbn.TabIndex = 16;
            this.verifyBoxHonkeKbn.TextV = "";
            this.verifyBoxHonkeKbn.TextChanged += new System.EventHandler(this.verifyBoxHonkeKbn_TextChanged);
            this.verifyBoxHonkeKbn.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(681, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 15);
            this.label24.TabIndex = 73;
            this.label24.Text = "本人/家族";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(539, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 15);
            this.label19.TabIndex = 71;
            this.label19.Text = "本家区分";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(723, 32);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 30);
            this.label21.TabIndex = 74;
            this.label21.Text = "2.本人\r\n6.家族";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label22.Location = new System.Drawing.Point(579, 25);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 45);
            this.label22.TabIndex = 72;
            this.label22.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(1011, 13);
            this.labelMacthCheck.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(107, 15);
            this.labelMacthCheck.TabIndex = 65;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // verifyBoxHihoname
            // 
            this.verifyBoxHihoname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHihoname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHihoname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHihoname.Location = new System.Drawing.Point(308, 31);
            this.verifyBoxHihoname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxHihoname.MaxLength = 25;
            this.verifyBoxHihoname.Name = "verifyBoxHihoname";
            this.verifyBoxHihoname.NewLine = false;
            this.verifyBoxHihoname.Size = new System.Drawing.Size(199, 27);
            this.verifyBoxHihoname.TabIndex = 15;
            this.verifyBoxHihoname.TextV = "";
            this.verifyBoxHihoname.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(304, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(82, 15);
            this.label46.TabIndex = 60;
            this.label46.Text = "被保険者名";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(5, 31);
            this.verifyBoxM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(43, 27);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label2.Location = new System.Drawing.Point(180, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "ハイフンなし";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(125, 31);
            this.verifyBoxHnum.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxHnum.MaxLength = 8;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(132, 27);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(131, 43);
            this.verifyBoxY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxY.MaxLength = 3;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(43, 27);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(385, 869);
            this.labelInputerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(248, 29);
            this.labelInputerName.TabIndex = 43;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(641, 869);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(120, 29);
            this.buttonBack.TabIndex = 255;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.label8);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.dgv);
            this.panelTotal.Controls.Add(this.pFusho);
            this.panelTotal.Controls.Add(this.label33);
            this.panelTotal.Controls.Add(this.verifyBoxHosName);
            this.panelTotal.Controls.Add(this.label31);
            this.panelTotal.Controls.Add(this.verifyBoxDrName);
            this.panelTotal.Controls.Add(this.labelNumbering);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxKyoKei);
            this.panelTotal.Controls.Add(this.verifyBoxDantaiKojin);
            this.panelTotal.Location = new System.Drawing.Point(4, 582);
            this.panelTotal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1333, 287);
            this.panelTotal.TabIndex = 70;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(1168, 119);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 30);
            this.label20.TabIndex = 152;
            this.label20.Text = "団体:0\r\n個人:1";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(151, 107);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 45);
            this.label8.TabIndex = 152;
            this.label8.Text = "柔整\r\n [協]:0\r\n [契]:1";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(12, 123);
            this.verifyBoxTotal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxTotal.MaxLength = 10;
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(109, 27);
            this.verifyBoxTotal.TabIndex = 30;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.item_Enter);
            this.verifyBoxTotal.Validated += new System.EventHandler(this.verifyBoxTotal_Validated);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(12, 104);
            this.labelTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(67, 15);
            this.labelTotal.TabIndex = 151;
            this.labelTotal.Text = "合計金額";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv.ColumnHeadersHeight = 29;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Location = new System.Drawing.Point(9, 171);
            this.dgv.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowHeadersWidth = 51;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1267, 104);
            this.dgv.StandardTab = true;
            this.dgv.TabIndex = 300;
            this.dgv.TabStop = false;
            this.dgv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_KeyDown);
            // 
            // pFusho
            // 
            this.pFusho.Controls.Add(this.verifyBoxDays1);
            this.pFusho.Controls.Add(this.label6);
            this.pFusho.Controls.Add(this.verifyBoxF1fsD);
            this.pFusho.Controls.Add(this.verifyBoxF1fsE);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstE);
            this.pFusho.Controls.Add(this.label4);
            this.pFusho.Controls.Add(this.verifyBoxF1Finish1);
            this.pFusho.Controls.Add(this.verifyBoxF1Start);
            this.pFusho.Controls.Add(this.label9);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstD);
            this.pFusho.Controls.Add(this.verifyBoxF1fsM);
            this.pFusho.Controls.Add(this.verifyBoxF1);
            this.pFusho.Controls.Add(this.verifyBoxF1fsY);
            this.pFusho.Controls.Add(this.label10);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstM);
            this.pFusho.Controls.Add(this.label15);
            this.pFusho.Controls.Add(this.verifyBoxF1FirstY);
            this.pFusho.Controls.Add(this.verifyBoxBuiCount);
            this.pFusho.Controls.Add(this.label16);
            this.pFusho.Controls.Add(this.label1);
            this.pFusho.Controls.Add(this.label12);
            this.pFusho.Controls.Add(this.label13);
            this.pFusho.Controls.Add(this.label18);
            this.pFusho.Controls.Add(this.label11);
            this.pFusho.Controls.Add(this.label14);
            this.pFusho.Controls.Add(this.label17);
            this.pFusho.Controls.Add(this.label5);
            this.pFusho.Controls.Add(this.label7);
            this.pFusho.Controls.Add(this.label42);
            this.pFusho.Controls.Add(this.label3);
            this.pFusho.Location = new System.Drawing.Point(9, 3);
            this.pFusho.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pFusho.Name = "pFusho";
            this.pFusho.Size = new System.Drawing.Size(1267, 88);
            this.pFusho.TabIndex = 20;
            this.pFusho.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxDays1
            // 
            this.verifyBoxDays1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays1.Location = new System.Drawing.Point(1213, 24);
            this.verifyBoxDays1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDays1.MaxLength = 3;
            this.verifyBoxDays1.Name = "verifyBoxDays1";
            this.verifyBoxDays1.NewLine = false;
            this.verifyBoxDays1.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxDays1.TabIndex = 72;
            this.verifyBoxDays1.TextV = "";
            this.verifyBoxDays1.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1199, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 15);
            this.label6.TabIndex = 71;
            this.label6.Text = "実日数1";
            this.label6.Visible = false;
            // 
            // verifyBoxF1fsD
            // 
            this.verifyBoxF1fsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsD.Location = new System.Drawing.Point(707, 24);
            this.verifyBoxF1fsD.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1fsD.MaxLength = 2;
            this.verifyBoxF1fsD.Name = "verifyBoxF1fsD";
            this.verifyBoxF1fsD.NewLine = false;
            this.verifyBoxF1fsD.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxF1fsD.TabIndex = 37;
            this.verifyBoxF1fsD.TextV = "";
            this.verifyBoxF1fsD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1fsE
            // 
            this.verifyBoxF1fsE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsE.Location = new System.Drawing.Point(495, 24);
            this.verifyBoxF1fsE.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1fsE.MaxLength = 1;
            this.verifyBoxF1fsE.Name = "verifyBoxF1fsE";
            this.verifyBoxF1fsE.NewLine = false;
            this.verifyBoxF1fsE.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxF1fsE.TabIndex = 30;
            this.verifyBoxF1fsE.TextV = "";
            this.verifyBoxF1fsE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(779, 24);
            this.verifyBoxF1FirstE.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxF1FirstE.TabIndex = 40;
            this.verifyBoxF1FirstE.TextV = "";
            this.verifyBoxF1FirstE.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(745, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 15);
            this.label4.TabIndex = 70;
            this.label4.Text = "日";
            // 
            // verifyBoxF1Finish1
            // 
            this.verifyBoxF1Finish1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish1.Location = new System.Drawing.Point(1137, 24);
            this.verifyBoxF1Finish1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1Finish1.MaxLength = 2;
            this.verifyBoxF1Finish1.Name = "verifyBoxF1Finish1";
            this.verifyBoxF1Finish1.NewLine = false;
            this.verifyBoxF1Finish1.Size = new System.Drawing.Size(36, 27);
            this.verifyBoxF1Finish1.TabIndex = 52;
            this.verifyBoxF1Finish1.TextV = "";
            this.verifyBoxF1Finish1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(1069, 24);
            this.verifyBoxF1Start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(36, 27);
            this.verifyBoxF1Start.TabIndex = 50;
            this.verifyBoxF1Start.TextV = "";
            this.verifyBoxF1Start.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(577, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 69;
            this.label9.Text = "負傷年月日";
            // 
            // verifyBoxF1FirstD
            // 
            this.verifyBoxF1FirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstD.Location = new System.Drawing.Point(993, 24);
            this.verifyBoxF1FirstD.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1FirstD.MaxLength = 2;
            this.verifyBoxF1FirstD.Name = "verifyBoxF1FirstD";
            this.verifyBoxF1FirstD.NewLine = false;
            this.verifyBoxF1FirstD.Size = new System.Drawing.Size(36, 27);
            this.verifyBoxF1FirstD.TabIndex = 48;
            this.verifyBoxF1FirstD.TextV = "";
            this.verifyBoxF1FirstD.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1fsM
            // 
            this.verifyBoxF1fsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsM.Location = new System.Drawing.Point(644, 24);
            this.verifyBoxF1fsM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1fsM.MaxLength = 2;
            this.verifyBoxF1fsM.Name = "verifyBoxF1fsM";
            this.verifyBoxF1fsM.NewLine = false;
            this.verifyBoxF1fsM.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxF1fsM.TabIndex = 35;
            this.verifyBoxF1fsM.TextV = "";
            this.verifyBoxF1fsM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(84, 24);
            this.verifyBoxF1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(399, 27);
            this.verifyBoxF1.TabIndex = 10;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxF1fsY
            // 
            this.verifyBoxF1fsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1fsY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1fsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1fsY.Location = new System.Drawing.Point(577, 24);
            this.verifyBoxF1fsY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1fsY.MaxLength = 2;
            this.verifyBoxF1fsY.Name = "verifyBoxF1fsY";
            this.verifyBoxF1fsY.NewLine = false;
            this.verifyBoxF1fsY.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxF1fsY.TabIndex = 33;
            this.verifyBoxF1fsY.TextV = "";
            this.verifyBoxF1fsY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(681, 38);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 15);
            this.label10.TabIndex = 68;
            this.label10.Text = "月";
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(927, 24);
            this.verifyBoxF1FirstM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(36, 27);
            this.verifyBoxF1FirstM.TabIndex = 45;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(616, 38);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 15);
            this.label15.TabIndex = 67;
            this.label15.Text = "年";
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(860, 24);
            this.verifyBoxF1FirstY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(36, 27);
            this.verifyBoxF1FirstY.TabIndex = 43;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxBuiCount
            // 
            this.verifyBoxBuiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuiCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuiCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuiCount.Location = new System.Drawing.Point(20, 24);
            this.verifyBoxBuiCount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxBuiCount.MaxLength = 3;
            this.verifyBoxBuiCount.Name = "verifyBoxBuiCount";
            this.verifyBoxBuiCount.NewLine = false;
            this.verifyBoxBuiCount.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxBuiCount.TabIndex = 8;
            this.verifyBoxBuiCount.TextV = "";
            this.verifyBoxBuiCount.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label16.Location = new System.Drawing.Point(531, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 45);
            this.label16.TabIndex = 40;
            this.label16.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(815, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 45);
            this.label1.TabIndex = 40;
            this.label1.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(965, 35);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 15);
            this.label12.TabIndex = 7;
            this.label12.Text = "月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1035, 35);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 15);
            this.label13.TabIndex = 9;
            this.label13.Text = "日";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1175, 35);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 15);
            this.label18.TabIndex = 12;
            this.label18.Text = "日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(897, 35);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 15);
            this.label11.TabIndex = 5;
            this.label11.Text = "年";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1107, 35);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 15);
            this.label14.TabIndex = 12;
            this.label14.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1135, 3);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 10;
            this.label17.Text = "終了日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(857, 3);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "初検日";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1067, 3);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 10;
            this.label7.Text = "開始日";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(12, 3);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(52, 15);
            this.label42.TabIndex = 16;
            this.label42.Text = "部位数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 15);
            this.label3.TabIndex = 44;
            this.label3.Text = "負傷名1";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(521, 119);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 30);
            this.label33.TabIndex = 49;
            this.label33.Text = "施術\r\n所名";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label33.Visible = false;
            // 
            // verifyBoxHosName
            // 
            this.verifyBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHosName.Location = new System.Drawing.Point(568, 122);
            this.verifyBoxHosName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxHosName.Name = "verifyBoxHosName";
            this.verifyBoxHosName.NewLine = false;
            this.verifyBoxHosName.Size = new System.Drawing.Size(316, 27);
            this.verifyBoxHosName.TabIndex = 130;
            this.verifyBoxHosName.TextV = "";
            this.verifyBoxHosName.Visible = false;
            this.verifyBoxHosName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(899, 119);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 30);
            this.label31.TabIndex = 47;
            this.label31.Text = "施術\r\n師名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label31.Visible = false;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(948, 123);
            this.verifyBoxDrName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.NewLine = false;
            this.verifyBoxDrName.Size = new System.Drawing.Size(212, 27);
            this.verifyBoxDrName.TabIndex = 140;
            this.verifyBoxDrName.TextV = "";
            this.verifyBoxDrName.Visible = false;
            this.verifyBoxDrName.Enter += new System.EventHandler(this.item_Enter);
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(344, 104);
            this.labelNumbering.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(142, 15);
            this.labelNumbering.TabIndex = 45;
            this.labelNumbering.Text = "柔整師登録記号番号";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNumbering.Visible = false;
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(347, 123);
            this.verifyBoxDrCode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDrCode.MaxLength = 10;
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(145, 27);
            this.verifyBoxDrCode.TabIndex = 50;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            this.verifyBoxDrCode.Visible = false;
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxKyoKei
            // 
            this.verifyBoxKyoKei.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyoKei.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyoKei.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyoKei.Location = new System.Drawing.Point(207, 123);
            this.verifyBoxKyoKei.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxKyoKei.MaxLength = 1;
            this.verifyBoxKyoKei.Name = "verifyBoxKyoKei";
            this.verifyBoxKyoKei.NewLine = false;
            this.verifyBoxKyoKei.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxKyoKei.TabIndex = 155;
            this.verifyBoxKyoKei.TextV = "";
            this.verifyBoxKyoKei.Enter += new System.EventHandler(this.item_Enter);
            // 
            // verifyBoxDantaiKojin
            // 
            this.verifyBoxDantaiKojin.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDantaiKojin.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDantaiKojin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDantaiKojin.Location = new System.Drawing.Point(1223, 122);
            this.verifyBoxDantaiKojin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDantaiKojin.MaxLength = 1;
            this.verifyBoxDantaiKojin.Name = "verifyBoxDantaiKojin";
            this.verifyBoxDantaiKojin.NewLine = false;
            this.verifyBoxDantaiKojin.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxDantaiKojin.TabIndex = 155;
            this.verifyBoxDantaiKojin.TextV = "";
            this.verifyBoxDantaiKojin.Enter += new System.EventHandler(this.item_Enter);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(428, 0);
            this.splitter2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(4, 902);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // panelMatchWhere
            // 
            this.panelMatchWhere.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelMatchWhere.Controls.Add(this.label23);
            this.panelMatchWhere.Controls.Add(this.checkBoxTotal);
            this.panelMatchWhere.Controls.Add(this.checkBoxNumber);
            this.panelMatchWhere.Location = new System.Drawing.Point(56, 871);
            this.panelMatchWhere.Margin = new System.Windows.Forms.Padding(4);
            this.panelMatchWhere.Name = "panelMatchWhere";
            this.panelMatchWhere.Size = new System.Drawing.Size(323, 29);
            this.panelMatchWhere.TabIndex = 256;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 8);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 15);
            this.label23.TabIndex = 36;
            this.label23.Text = "突合候補条件";
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Checked = true;
            this.checkBoxTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTotal.Location = new System.Drawing.Point(213, 5);
            this.checkBoxTotal.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(89, 19);
            this.checkBoxTotal.TabIndex = 38;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            // 
            // checkBoxNumber
            // 
            this.checkBoxNumber.AutoSize = true;
            this.checkBoxNumber.Checked = true;
            this.checkBoxNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNumber.Location = new System.Drawing.Point(125, 5);
            this.checkBoxNumber.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxNumber.Name = "checkBoxNumber";
            this.checkBoxNumber.Size = new System.Drawing.Size(74, 19);
            this.checkBoxNumber.TabIndex = 37;
            this.checkBoxNumber.Text = "被保番";
            this.checkBoxNumber.UseVisualStyleBackColor = true;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1792, 902);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1807, 939);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.InputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.pFusho.ResumeLayout(false);
            this.pFusho.PerformLayout();
            this.panelMatchWhere.ResumeLayout(false);
            this.panelMatchWhere.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelHs;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private VerifyBox verifyBoxF1Start;
        private VerifyBox verifyBoxF1FirstD;
        private VerifyBox verifyBoxF1FirstM;
        private VerifyBox verifyBoxF1FirstY;
        private System.Windows.Forms.Panel panelTotal;
        private VerifyBox verifyBoxBuiCount;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label1;
        private VerifyBox verifyBoxF1FirstE;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label33;
        private VerifyBox verifyBoxHosName;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private VerifyBox verifyBoxHihoname;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel pFusho;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labelMacthCheck;
        private VerifyBox verifyBoxF1fsD;
        private VerifyBox verifyBoxF1fsE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private VerifyBox verifyBoxF1fsM;
        private VerifyBox verifyBoxF1fsY;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private VerifyBox verifyBoxF1Finish1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxDantaiKojin;
        private System.Windows.Forms.Label label20;
        private VerifyBox verifyBoxFamily;
        private VerifyBox verifyBoxHonkeKbn;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private VerifyBox verifyBoxDays1;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxKyoKei;
        private System.Windows.Forms.Panel panelMatchWhere;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.CheckBox checkBoxNumber;
    }
}