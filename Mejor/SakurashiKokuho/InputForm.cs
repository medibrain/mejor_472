﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Sakurashi_Kokuho

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(800, 0);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// 柔整師登録記号番号
        /// </summary>
        Point posDrCode = new Point(1800, 2060);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, buiDateConts, hihoNameConts ,drCodeConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 保険者提供情報
        /// </summary>
        List<importdata> lstimp = new List<importdata>();

        /// <summary>
        /// 患者（受療者）情報
        /// </summary>
        List<hihoInfo> lstHiho = new List<hihoInfo>();

        public static Dictionary<string, APP_ClinicInfo> dicClinicInfoDrNum = new Dictionary<string, APP_ClinicInfo>();

        private InputMode inputMode;
        int cym;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            GroupingControl();

            //マッチングチェック用コントロール
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;


            InitAppGrid();

            panelTotal.Visible = false;
            panelHnum.Visible = false;

            #endregion



            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;

            //20210710162259 furukawa st ////////////////////////
            //Shownでやらないと、aid指定で入ってきたときにsetappが走ってないことになる

            //var app = (App)bsApp.Current;
            //if (app != null)
            //{
            //    setApp(app);
            //    //保険者提供情報取得
            //    if (lstimp.Count == 0) lstimp = importdata.SelectAll(app.CYM);

            //    //20200720111642 furukawa st ////////////////////////
            //    //被保険者名リスト取得

            //    lstHiho = hihoInfo.GetHihoInfo();
            //    //20200720111642 furukawa ed ////////////////////////


            //    //20200720111713 furukawa st ////////////////////////
            //    //提供データ選択状態にする
            //    createGrid(app.YM,app.Total,app.HihoNum,app.RrID);
            //    //createGrid(app.RrID);
            //    //20200720111713 furukawa ed ////////////////////////
            //}

            //20210710162259 furukawa ed ////////////////////////

            focusBack(false);


          


        }
        #endregion


        //20210723170826 furukawa st ////////////////////////
        //申請書リスト初期化
        
        /// <summary>
        /// 申請書グリッド初期化
        /// </summary>
        private void InitAppGrid()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
        }
        //20210723170826 furukawa ed ////////////////////////


        //20210723170752 furukawa st ////////////////////////
        //座標移動用コントロールグルーピング

        /// <summary>
        /// 座標移動用コントロールグルーピング
        /// </summary>
        private void GroupingControl()
        {
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFamily, verifyBoxHonkeKbn, };

            //合計、請求、往料、交付
            totalConts = new Control[] { verifyBoxTotal, verifyBoxKyoKei, verifyBoxDantaiKojin, };

            //初検日年号,負傷名等
            buiDateConts = new Control[] {
                verifyBoxF1FirstE,verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD,
                verifyBoxF1fsE,verifyBoxF1fsY, verifyBoxF1fsM, verifyBoxF1fsD,
                verifyBoxF1Start,
                verifyBoxF1,
                verifyBoxBuiCount, };


            //被保険者名、施術所、口座
            hihoNameConts = new Control[] { verifyBoxHihoname, };

            //柔整師登録記号番号、柔整師名、施術所名
            drCodeConts = new Control[] { verifyBoxDrCode, verifyBoxDrName, verifyBoxHosName, };

        }
        //20210723170752 furukawa ed ////////////////////////



        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            GroupingControl();


            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            InitAppGrid();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }



        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 本家区分入力時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxHonkeKbn_TextChanged(object sender, EventArgs e)
        {
            //20200713164910 furukawa st ////////////////////////
            //いらん
            
            //if (verifyBoxHonkeKbn.Text == "2") verifyBoxFamily.Text = "2";
            //else verifyBoxFamily.Text = "6";
            //20200713164910 furukawa ed ////////////////////////
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
                //20210710162442 furukawa st ////////////////////////
                //aid指定で入ってきたときにリスト選択できてないのでここでロード

                App app = (App)bsApp.Current;
                if (app != null)
                {
                    //保険者提供情報取得
                    if (lstimp.Count == 0) lstimp = importdata.SelectAll(app.CYM);

                    //グリッド含め表示　提供データ情報を取得してからでないとグリッドが出ない
                    setApp(app);


                    //20200720111642 furukawa st ////////////////////////
                    //被保険者名リスト取得
                    lstHiho = hihoInfo.GetHihoInfo();
                    //20200720111642 furukawa ed ////////////////////////

                }
                //20210710162442 furukawa ed ////////////////////////

            }
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelYearInfo, labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            panelHnum.Visible = false;
            panelTotal.Visible = false;

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }


            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
                panelHnum.Visible = true;
                panelTotal.Visible = true;

            }
            else
            {
                panelHnum.Visible = false;
                panelTotal.Visible = false;
            }
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }
        #endregion


        /// <summary>
        /// 提供データ表示グリッド設定
        /// </summary>
        private void InitGrid()
        {
            
            dgv.ColumnHeadersHeight = 20;
            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Font = new Font("Microsoft Sans Serif", 8);
            dgv.ColumnHeadersDefaultCellStyle = cs;

            dgv.Columns[nameof(importdata.importid)].Width = 40;
            dgv.Columns[nameof(importdata.cym)].Width = 70;
            dgv.Columns[nameof(importdata.insnum)].Width = 60;
            dgv.Columns[nameof(importdata.hihomark)].Width = 40;
            dgv.Columns[nameof(importdata.hihonum)].Width = 80;
            dgv.Columns[nameof(importdata.pname)].Width = 80;
            dgv.Columns[nameof(importdata.shinryoym)].Width = 100;
            dgv.Columns[nameof(importdata.clinicnum)].Width = 80;
            dgv.Columns[nameof(importdata.clinicname)].Width = 100;
            dgv.Columns[nameof(importdata.history)].Width = 40;
            dgv.Columns[nameof(importdata.setai)].Width = 40;
            dgv.Columns[nameof(importdata.atenanum)].Width = 40;
            dgv.Columns[nameof(importdata.pbirth)].Width = 120;
            dgv.Columns[nameof(importdata.pgender)].Width = 40;
            dgv.Columns[nameof(importdata.shubetsu1)].Width = 40;
            dgv.Columns[nameof(importdata.shubetsu2)].Width = 40;
            dgv.Columns[nameof(importdata.nyugai)].Width = 40;
            dgv.Columns[nameof(importdata.honke)].Width = 80;
            dgv.Columns[nameof(importdata.ratio)].Width = 40;
            dgv.Columns[nameof(importdata.shinryo_ryouyouhibetsu)].Width = 40;
            dgv.Columns[nameof(importdata.tensuhyo)].Width = 40;
            dgv.Columns[nameof(importdata.ryouyouhibetsu)].Width = 40;
            dgv.Columns[nameof(importdata.shinsaym)].Width = 80;
            dgv.Columns[nameof(importdata.days)].Width = 40;
            dgv.Columns[nameof(importdata.total)].Width = 70;
            dgv.Columns[nameof(importdata.shokuji)].Width = 40;
            dgv.Columns[nameof(importdata.comnum)].Width = 40;
            dgv.Columns[nameof(importdata.shohoukikancode)].Width = 40;
            dgv.Columns[nameof(importdata.shohoukikanname)].Width = 40;
            dgv.Columns[nameof(importdata.kuri)].Width = 40;
            dgv.Columns[nameof(importdata.kea)].Width = 40;
            dgv.Columns[nameof(importdata.younin)].Width = 40;
            dgv.Columns[nameof(importdata.futou)].Width = 40;
            dgv.Columns[nameof(importdata.dai3sha)].Width = 40;
            dgv.Columns[nameof(importdata.kyufuseigen)].Width = 40;
            dgv.Columns[nameof(importdata.kougaku)].Width = 40;
            dgv.Columns[nameof(importdata.yoyaku)].Width = 40;
            dgv.Columns[nameof(importdata.shori)].Width = 40;
            dgv.Columns[nameof(importdata.gigishubetsu)].Width = 40;
            dgv.Columns[nameof(importdata.sankou)].Width = 40;
            dgv.Columns[nameof(importdata.joutai)].Width = 40;
            dgv.Columns[nameof(importdata.codejouhou)].Width = 40;
            dgv.Columns[nameof(importdata.genponkbn)].Width = 40;
            dgv.Columns[nameof(importdata.genponshozai)].Width = 40;
            dgv.Columns[nameof(importdata.kengai)].Width = 40;
            dgv.Columns[nameof(importdata.fusen01)].Width = 40;
            dgv.Columns[nameof(importdata.fusen02)].Width = 40;
            dgv.Columns[nameof(importdata.fusen03)].Width = 40;
            dgv.Columns[nameof(importdata.fusen04)].Width = 40;
            dgv.Columns[nameof(importdata.fusen05)].Width = 40;
            dgv.Columns[nameof(importdata.fusen06)].Width = 40;
            dgv.Columns[nameof(importdata.fusen07)].Width = 40;
            dgv.Columns[nameof(importdata.fusen08)].Width = 40;
            dgv.Columns[nameof(importdata.fusen09)].Width = 40;
            dgv.Columns[nameof(importdata.fusen10)].Width = 40;
            dgv.Columns[nameof(importdata.fusen11)].Width = 40;
            dgv.Columns[nameof(importdata.fusen12)].Width = 40;
            dgv.Columns[nameof(importdata.fusen13)].Width = 40;
            dgv.Columns[nameof(importdata.fusen14)].Width = 40;
            dgv.Columns[nameof(importdata.fusen15)].Width = 40;
            dgv.Columns[nameof(importdata.fusen16)].Width = 40;
            dgv.Columns[nameof(importdata.shinryoymad)].Width = 40;
            dgv.Columns[nameof(importdata.pbirthad)].Width = 40;
            dgv.Columns[nameof(importdata.shinsaymad)].Width = 40;
            dgv.Columns[nameof(importdata.hihonumnarrow)].Width = 40;




            dgv.Columns[nameof(importdata.importid)].HeaderText = "id";
            dgv.Columns[nameof(importdata.cym)].HeaderText = "請求年月";
            dgv.Columns[nameof(importdata.insnum)].HeaderText = "保険者";
            dgv.Columns[nameof(importdata.hihomark)].HeaderText = "記号";
            dgv.Columns[nameof(importdata.hihonum)].HeaderText = "番号";
            dgv.Columns[nameof(importdata.pname)].HeaderText = "療養者";
            dgv.Columns[nameof(importdata.shinryoym)].HeaderText = "診療年月";
            dgv.Columns[nameof(importdata.clinicnum)].HeaderText = "機関CD";
            dgv.Columns[nameof(importdata.clinicname)].HeaderText = "医療機関";
            dgv.Columns[nameof(importdata.history)].HeaderText = "最新履歴";
            dgv.Columns[nameof(importdata.setai)].HeaderText = "世帯番号";
            dgv.Columns[nameof(importdata.atenanum)].HeaderText = "宛名番号";
            dgv.Columns[nameof(importdata.pbirth)].HeaderText = "生年月日";
            dgv.Columns[nameof(importdata.pgender)].HeaderText = "性別";
            dgv.Columns[nameof(importdata.shubetsu1)].HeaderText = "種別１";
            dgv.Columns[nameof(importdata.shubetsu2)].HeaderText = "種別２";
            dgv.Columns[nameof(importdata.nyugai)].HeaderText = "入外";
            dgv.Columns[nameof(importdata.honke)].HeaderText = "本家";
            dgv.Columns[nameof(importdata.ratio)].HeaderText = "割合%";
            dgv.Columns[nameof(importdata.shinryo_ryouyouhibetsu)].HeaderText = "診療・療養費別";
            dgv.Columns[nameof(importdata.tensuhyo)].HeaderText = "点数表";
            dgv.Columns[nameof(importdata.ryouyouhibetsu)].HeaderText = "療養費種別";
            dgv.Columns[nameof(importdata.shinsaym)].HeaderText = "審査年月";
            dgv.Columns[nameof(importdata.days)].HeaderText = "実日数";
            dgv.Columns[nameof(importdata.total)].HeaderText = "決定";
            dgv.Columns[nameof(importdata.shokuji)].HeaderText = "食事基準額";
            dgv.Columns[nameof(importdata.comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(importdata.shohoukikancode)].HeaderText = "処方機関コード";
            dgv.Columns[nameof(importdata.shohoukikanname)].HeaderText = "処方機関名";
            dgv.Columns[nameof(importdata.kuri)].HeaderText = "クリ";
            dgv.Columns[nameof(importdata.kea)].HeaderText = "ケア";
            dgv.Columns[nameof(importdata.younin)].HeaderText = "容認";
            dgv.Columns[nameof(importdata.futou)].HeaderText = "不当";
            dgv.Columns[nameof(importdata.dai3sha)].HeaderText = "第三者";
            dgv.Columns[nameof(importdata.kyufuseigen)].HeaderText = "給付制限";
            dgv.Columns[nameof(importdata.kougaku)].HeaderText = "高額";
            dgv.Columns[nameof(importdata.yoyaku)].HeaderText = "予約";
            dgv.Columns[nameof(importdata.shori)].HeaderText = "処理";
            dgv.Columns[nameof(importdata.gigishubetsu)].HeaderText = "疑義種別";
            dgv.Columns[nameof(importdata.sankou)].HeaderText = "参考";
            dgv.Columns[nameof(importdata.joutai)].HeaderText = "状態";
            dgv.Columns[nameof(importdata.codejouhou)].HeaderText = "コード情報";
            dgv.Columns[nameof(importdata.genponkbn)].HeaderText = "原本区分";
            dgv.Columns[nameof(importdata.genponshozai)].HeaderText = "原本所在";
            dgv.Columns[nameof(importdata.kengai)].HeaderText = "県内外";
            dgv.Columns[nameof(importdata.fusen01)].HeaderText = "付箋１  ";
            dgv.Columns[nameof(importdata.fusen02)].HeaderText = "付箋２";
            dgv.Columns[nameof(importdata.fusen03)].HeaderText = "付箋３";
            dgv.Columns[nameof(importdata.fusen04)].HeaderText = "付箋４";
            dgv.Columns[nameof(importdata.fusen05)].HeaderText = "付箋５";
            dgv.Columns[nameof(importdata.fusen06)].HeaderText = "付箋６";
            dgv.Columns[nameof(importdata.fusen07)].HeaderText = "付箋７";
            dgv.Columns[nameof(importdata.fusen08)].HeaderText = "付箋８";
            dgv.Columns[nameof(importdata.fusen09)].HeaderText = "付箋９";
            dgv.Columns[nameof(importdata.fusen10)].HeaderText = "付箋１０";
            dgv.Columns[nameof(importdata.fusen11)].HeaderText = "付箋１１";
            dgv.Columns[nameof(importdata.fusen12)].HeaderText = "付箋１２";
            dgv.Columns[nameof(importdata.fusen13)].HeaderText = "付箋１３";
            dgv.Columns[nameof(importdata.fusen14)].HeaderText = "付箋１４";
            dgv.Columns[nameof(importdata.fusen15)].HeaderText = "付箋１５";
            dgv.Columns[nameof(importdata.fusen16)].HeaderText = "付箋１６";
            dgv.Columns[nameof(importdata.shinryoymad)].HeaderText = "診療年月西暦";
            dgv.Columns[nameof(importdata.pbirthad)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(importdata.shinsaymad)].HeaderText = "審査年月西暦";
            dgv.Columns[nameof(importdata.hihonumnarrow)].HeaderText = "ヒホバン";



            dgv.Columns[nameof(importdata.importid)].Visible = true;
            dgv.Columns[nameof(importdata.cym)].Visible = true;
            dgv.Columns[nameof(importdata.insnum)].Visible = true;
            dgv.Columns[nameof(importdata.hihomark)].Visible = true;
            dgv.Columns[nameof(importdata.hihonum)].Visible = true;
            dgv.Columns[nameof(importdata.pname)].Visible = true;
            dgv.Columns[nameof(importdata.shinryoym)].Visible = true;
            dgv.Columns[nameof(importdata.clinicnum)].Visible = true;
            dgv.Columns[nameof(importdata.clinicname)].Visible = true;
            dgv.Columns[nameof(importdata.history)].Visible = false;
            dgv.Columns[nameof(importdata.setai)].Visible = false;
            dgv.Columns[nameof(importdata.atenanum)].Visible = false;
            dgv.Columns[nameof(importdata.pbirth)].Visible = true;
            dgv.Columns[nameof(importdata.pgender)].Visible = true;
            dgv.Columns[nameof(importdata.shubetsu1)].Visible = false;
            dgv.Columns[nameof(importdata.shubetsu2)].Visible = false;
            dgv.Columns[nameof(importdata.nyugai)].Visible = true;
            dgv.Columns[nameof(importdata.honke)].Visible = true;
            dgv.Columns[nameof(importdata.ratio)].Visible = true;
            dgv.Columns[nameof(importdata.shinryo_ryouyouhibetsu)].Visible = false;
            dgv.Columns[nameof(importdata.tensuhyo)].Visible = false;
            dgv.Columns[nameof(importdata.ryouyouhibetsu)].Visible = false;
            dgv.Columns[nameof(importdata.shinsaym)].Visible = true;
            dgv.Columns[nameof(importdata.days)].Visible = true;
            dgv.Columns[nameof(importdata.total)].Visible = true;
            dgv.Columns[nameof(importdata.shokuji)].Visible = false;
            dgv.Columns[nameof(importdata.comnum)].Visible = false;
            dgv.Columns[nameof(importdata.shohoukikancode)].Visible = false;
            dgv.Columns[nameof(importdata.shohoukikanname)].Visible = false;
            dgv.Columns[nameof(importdata.kuri)].Visible = false;
            dgv.Columns[nameof(importdata.kea)].Visible = false;
            dgv.Columns[nameof(importdata.younin)].Visible = false;
            dgv.Columns[nameof(importdata.futou)].Visible = false;
            dgv.Columns[nameof(importdata.dai3sha)].Visible = false;
            dgv.Columns[nameof(importdata.kyufuseigen)].Visible = false;
            dgv.Columns[nameof(importdata.kougaku)].Visible = false;
            dgv.Columns[nameof(importdata.yoyaku)].Visible = false;
            dgv.Columns[nameof(importdata.shori)].Visible = false;
            dgv.Columns[nameof(importdata.gigishubetsu)].Visible = false;
            dgv.Columns[nameof(importdata.sankou)].Visible = false;
            dgv.Columns[nameof(importdata.joutai)].Visible = false;
            dgv.Columns[nameof(importdata.codejouhou)].Visible = false;
            dgv.Columns[nameof(importdata.genponkbn)].Visible = false;
            dgv.Columns[nameof(importdata.genponshozai)].Visible = false;
            dgv.Columns[nameof(importdata.kengai)].Visible = false;
            dgv.Columns[nameof(importdata.fusen01)].Visible = false;
            dgv.Columns[nameof(importdata.fusen02)].Visible = false;
            dgv.Columns[nameof(importdata.fusen03)].Visible = false;
            dgv.Columns[nameof(importdata.fusen04)].Visible = false;
            dgv.Columns[nameof(importdata.fusen05)].Visible = false;
            dgv.Columns[nameof(importdata.fusen06)].Visible = false;
            dgv.Columns[nameof(importdata.fusen07)].Visible = false;
            dgv.Columns[nameof(importdata.fusen08)].Visible = false;
            dgv.Columns[nameof(importdata.fusen09)].Visible = false;
            dgv.Columns[nameof(importdata.fusen10)].Visible = false;
            dgv.Columns[nameof(importdata.fusen11)].Visible = false;
            dgv.Columns[nameof(importdata.fusen12)].Visible = false;
            dgv.Columns[nameof(importdata.fusen13)].Visible = false;
            dgv.Columns[nameof(importdata.fusen14)].Visible = false;
            dgv.Columns[nameof(importdata.fusen15)].Visible = false;
            dgv.Columns[nameof(importdata.fusen16)].Visible = false;
            dgv.Columns[nameof(importdata.shinryoymad)].Visible = false;
            dgv.Columns[nameof(importdata.pbirthad)].Visible = false;
            dgv.Columns[nameof(importdata.shinsaymad)].Visible = false;
            dgv.Columns[nameof(importdata.hihonumnarrow)].Visible = false;





        }



        /// <summary>
        /// 保険者提供情報表示
        /// </summary>     
        private void createGrid()
        {

            int intShinryoYM = 0;

            //何故かappを取り直さないとYMが反映されない
            App a = (App)bsApp.Current;
            a = App.GetApp(a.Aid);

            int total = 0;
            string hihonum = string.Empty;


            //初回入力、２回め入力前はコントロール値から判断
            if (firstTime || (!firstTime && a.StatusFlagCheck(StatusFlag.入力済)))
            {
                total = verifyBoxTotal.GetIntValue();
                hihonum = verifyBoxHnum.Text.Trim();
                intShinryoYM = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            }
            else
            {
                //初回入力後、２回め入力後はappにデータが有るはずなのでそっちから取る
                total = a.Total;
                hihonum = a.HihoNum;
                intShinryoYM = a.YM;
            }
            
            
            //診療年月、合計、被保険者証番号で合致する提供データを取得
            List<importdata> lstai = new List<importdata>();
          
            foreach (importdata item in lstimp)
            {
                if (item.shinryoymad == intShinryoYM &&
                    item.total==total &&
                    item.hihonumnarrow==hihonum
                    )
                {
                    lstai.Add(item);
                }
            }

            dgv.DataSource = lstai;

            InitGrid();


            //自動選択
            AutoSelectGrid(intShinryoYM, total, hihonum, a.RrID,a,lstai.Count);


            //グリッド使用可能制御
            if (!firstTime && a.StatusFlagCheck(StatusFlag.ベリファイ済))
            {
                //ベリファイ済みは変更不可、変更は初回入力に戻って
                dgv.Enabled = false ;
                dgv.RowsDefaultCellStyle.BackColor = Color.Gray;
            }
            else
            {
                dgv.Enabled= true;
                dgv.RowsDefaultCellStyle.BackColor = Color.White;
            }


        }



        //20210722164952 furukawa st ////////////////////////
        //煩雑なので自動選択を別関数に
        
        /// <summary>
        /// グリッドの自動選択選別
        /// </summary>
        /// <param name="shinryoym">診療年月</param>
        /// <param name="total">合計</param>
        /// <param name="hihonum">被保険者証番号</param>
        /// <param name="rrid">rrid</param>
        /// <param name="app">app</param>
        /// <param name="listCount">合致したリストの行数</param>
        private void AutoSelectGrid(int shinryoym, int total, string hihonum,int rrid=0,App app=null,int listCount=0)
        {

            dgv.ClearSelection();

            //20210710164913 furukawa st ////////////////////////
            //自動選択は、rridがあるand1回目入力済み、rridがあるandベリファイ済みだけ。rridがあるベリファイ前はやらない

            if (rrid != 0 && app.StatusFlagCheck(StatusFlag.入力済) && firstTime ||
                rrid != 0 && app.StatusFlagCheck(StatusFlag.ベリファイ済))
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (r.Cells["importid"].Value.ToString() == rrid.ToString())
                    {
                        r.Selected = true;
                        break;
                    }
                }
            }
            //20210710164913 furukawa ed ////////////////////////


            if (listCount == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            //リストが１つでrridがない（初回入力時）
            else if(listCount==1 && rrid == 0)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    //リストがある＝検索条件で掛かったのが一つだけ＝合致してる
                    r.Selected = true;
                    labelMacthCheck.BackColor = Color.Cyan;
                    labelMacthCheck.Text = "マッチングOK";
                    labelMacthCheck.Visible = true;
                    break;
                }

            }

            //20210710161322 furukawa st ////////////////////////
            //選択が1つまたは、rridがあるand1回目入力済み、rridがあるandベリファイ済みは確定と見なす

            else if (listCount == 1 ||
                ((rrid != 0 && app.StatusFlagCheck(StatusFlag.入力済) && firstTime) ||
                (rrid != 0 && app.StatusFlagCheck(StatusFlag.ベリファイ済)))
                )
            //else if (listCount.Count == 1)
            //20210710161322 furukawa ed ////////////////////////
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (r.Cells["importid"].Value.ToString() == rrid.ToString())
                    {
                        r.Selected = true;
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        break;
                    }
                    else
                    {
                        labelMacthCheck.BackColor = Color.Yellow;
                        labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                        labelMacthCheck.Visible = true;
                    }
                }

                
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }

        //20210722164952 furukawa ed ////////////////////////


        #region 20210722164855 furukawa 煩雑なので削除

        /// <summary>
        /// 保険者提供情報グリッド
        /// </summary>

        //20210710161035 furukawa st ////////////////////////
        //rridを間違って登録した場合、rridに合致する行しか出ないのは修正できないので、ヒホバン等の条件で表示させる

        private void createGrid(App app)
        //private void createGrid(int rrid)

        //20210710161035 furukawa ed ////////////////////////

        {

            //int shinryoym = app.YM;
            //int total = app.Total;
            //string hihonum = app.HihoNum;
            //int rrid = app.RrID;

            //List<importdata> lstai = new List<importdata>();

            ////20210710161209 furukawa st ////////////////////////
            ////リストの表示だけはヒホバン等の条件にしておく
            
            //foreach (importdata item in lstimp)
            //{
            //    if (item.shinryoymad == shinryoym &&
            //        item.total == total &&
            //        item.hihonumnarrow == hihonum
            //        )
            //    {
            //        lstai.Add(item);
            //    }
            //}


            ////foreach (importdata item in lstimp)
            ////{
            ////    if (item.importid == rrid)
            ////    {
            ////        lstai.Add(item);
            ////    }
            ////}

            ////20210710161209 furukawa ed ////////////////////////



            //dgv.DataSource = null;
            //dgv.DataSource = lstai;

            //#region グリッド初期化
            //InitGrid();
            //#endregion

            ////20210710161245 furukawa st ////////////////////////
            ////選択を解除し、rridが0でない場合は選択状態にする
            
            //dgv.ClearSelection();
            ////20210710161245 furukawa ed ////////////////////////


            ////20210710164913 furukawa st ////////////////////////
            ////自動選択は、rridがあるand1回目入力済み、rridがあるandベリファイ済みだけ。rridがあるベリファイ前はやらない
            
            //if (rrid !=0 && app.StatusFlagCheck(StatusFlag.入力済) && firstTime ||
            //    rrid!=0 && app.StatusFlagCheck(StatusFlag.ベリファイ済))
            //{
            //    foreach (DataGridViewRow r in dgv.Rows)
            //    {
            //        if (r.Cells["importid"].Value.ToString() == rrid.ToString())
            //        {
            //            r.Selected = true;
            //            break;
            //        }
            //    }
            //}
            ////20210710164913 furukawa ed ////////////////////////


            //if (lstai == null || lstai.Count == 0)
            //{
            //    labelMacthCheck.BackColor = Color.Pink;
            //    labelMacthCheck.Text = "マッチング無し";
            //    labelMacthCheck.Visible = true;
            //}

            ////20210710161322 furukawa st ////////////////////////
            ////選択が1つまたは、rridがあるand1回目入力済み、rridがあるandベリファイ済みは確定と見なす

            //else if (lstai.Count == 1 || 
            //    (rrid != 0 && app.StatusFlagCheck(StatusFlag.入力済) && firstTime ||
            //    rrid != 0 && app.StatusFlagCheck(StatusFlag.ベリファイ済))
            //    )
            ////else if (lstai.Count == 1)
            ////20210710161322 furukawa ed ////////////////////////
            //{
            //    labelMacthCheck.BackColor = Color.Cyan;
            //    labelMacthCheck.Text = "マッチングOK";
            //    labelMacthCheck.Visible = true;
            //}
            //else
            //{
            //    labelMacthCheck.BackColor = Color.Yellow;
            //    labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
            //    labelMacthCheck.Visible = true;
            //}


        }

        //20210722164855 furukawa ed ////////////////////////
        #endregion



        #region 各種ロード


        //20201123095507 furukawa st ////////////////////////
        //PastDataのロード方式に変更のため個々で作らない

        //private string getHihoName(string strhnum)
        //{
        //    lstHiho.Sort((x,y) => y.cym.CompareTo(x.cym));
        //    foreach (hihoInfo h in lstHiho)
        //    {
        //        if (h.hihoNum == strhnum) return h.hihoName;
        //    }
        //    return string.Empty;
        //}
        //20201123095507 furukawa ed ////////////////////////


        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            panelHnum.Visible = false;
            panelTotal.Visible = false;

            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        //20210722171751 furukawa st ////////////////////////
                        //OCRの負傷名を入れておく
                        
                        var ocr = app.OcrData.Split(',');
                        System.Text.RegularExpressions.Regex regex = new Regex("[(|)]");
                        verifyBoxF1.Text = regex.Replace(ocr[40].ToString().Trim(), "");

                        //20210722171751 furukawa ed ////////////////////////
                    }
                }
            }

       
            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //setvalueしないとステータスが更新されない
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //setvalueしないとステータスが更新されない                
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {

                setValue(verifyBoxY,"**", firstTime, nv);
                
            }
            else
            {
                //申請書



                //和暦年
                //和暦月
                setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                //被保険者証記号番号
                setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);

                //被保険者名
                setValue(verifyBoxHihoname, app.HihoName, firstTime, nv);

                //本人/家族、本家区分
                int family = app.Family / 100;
                int honkekubun = app.Family % 100;
                setValue(verifyBoxFamily, family, firstTime, nv);
                
                //20200717101655 furukawa st ////////////////////////
                //setvalueで数字０が空欄で代入されるので文字列に変更
                
                setValue(verifyBoxHonkeKbn, honkekubun.ToString(), firstTime, nv);
                //setValue(verifyBoxHonkeKbn, honkekubun, firstTime, nv);
                //20200717101655 furukawa ed ////////////////////////


                //負傷名1
                setValue(verifyBoxF1, app.FushoName1, firstTime, nv);

                //負傷年月日1
                setDateValue(app.FushoDate1, firstTime, nv, verifyBoxF1fsY, verifyBoxF1fsM, verifyBoxF1fsE, verifyBoxF1fsD);

                //初検日1
                setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                //施術開始日1
                if (!app.FushoStartDate1.IsNullDate())
                    setValue(verifyBoxF1Start, app.FushoStartDate1.Day.ToString(), firstTime, nv);

                //施術終了日１
                setValue(verifyBoxF1Finish1, app.FushoFinishDate1.Day, firstTime, nv);

                //20200717103624 furukawa st ////////////////////////
                //実日数不要でした

                        //実日数1
                        //setValue(verifyBoxDays1, app.FushoDays1, firstTime, nv);
                //20200717103624 furukawa ed ////////////////////////


                //20200717102535 furukawa st ////////////////////////
                //部位数入力追加                             
                setValue(verifyBoxBuiCount, app.TaggedDatas.count, firstTime, nv);
                //20200717102535 furukawa ed ////////////////////////


                //新規継続 コントロールなし


                //20200720164741 furukawa st ////////////////////////
                //漢字つきにする
                
                setValue(verifyBoxKyoKei, app.TaggedDatas.GeneralString5 == "0:協" ? "0" : "1", firstTime, nv);

                //20200717112531 furukawa st ////////////////////////
                //柔整師登録記号番号の頭一桁（協・契）の数字だけ必要
                //setValue(verifyBoxKyoKei, app.TaggedDatas.GeneralString5, firstTime, nv);


                //柔整師登録記号番号
                //setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);


                //20200717112531 furukawa ed ////////////////////////
                //20200720164741 furukawa ed ////////////////////////



                //20200717103237 furukawa st ////////////////////////
                //施術所、施術師不要でした

                ////医療機関名
                //setValue(verifyBoxHosName, app.ClinicName, firstTime, nv);

                ////施術師名
                //setValue(verifyBoxDrName, app.DrName, firstTime, nv);
                //20200717103237 furukawa ed ////////////////////////


                //合計金額
                setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);


                //団体個人
                setValue(verifyBoxDantaiKojin, app.TaggedDatas.GeneralString1 == "0:団体" ? "0" : "1", firstTime, nv);


                //グリッド選択する。コントロールから取る場合もあるので、全コントロールを埋めてから最後にやる
                createGrid();
                
            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外

            if (verifyBoxY.Text == "**"  )
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                
                //申請書タイプをバッチにする                
                app.AppType = APP_TYPE.バッチ;
           

                #endregion                
            }
            else if (verifyBoxY.Text == "--")
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;

                //申請書タイプ
                app.AppType = APP_TYPE.続紙;


                #endregion
            }
            else if (verifyBoxY.Text == "++")
            {
                #region Appへの反映

                //施術年
                app.MediYear = (int)APP_SPECIAL_CODE.不要;

                //申請書タイプ          
                app.AppType = APP_TYPE.不要;


                #endregion
            }
            //申請書
            else
            {
                #region 入力チェック
                //和暦月
                int month = verifyBoxM.GetIntValue();
                setStatus(verifyBoxM, month < 1 || 12 < month);

                //和暦年
                int year = verifyBoxY.GetIntValue();
                setStatus(verifyBoxY, year < 1 || 31 < year);
                int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                //被保険者証記号番号
                string hnumN = verifyBoxHnum.Text.Trim();
                setStatus(verifyBoxHnum, hnumN.Length<6);

                //被保険者名
                string strHihoName = verifyBoxHihoname.Text.Trim();
                setStatus(verifyBoxHihoname, strHihoName.Length < 2);

                //本家区分
                int honkekbn = verifyBoxHonkeKbn.GetIntValue();
                setStatus(verifyBoxHonkeKbn, !new[] { 0, 2, 4, 6, 8 }.Contains(honkekbn));
                
                //本人/家族
                int family = verifyBoxFamily.GetIntValue();
                setStatus(verifyBoxFamily, !new[] { 2, 6 }.Contains(family));



                //負傷年月日1
                DateTime f1Date = DateTimeEx.DateTimeNull;
                f1Date = dateCheck(verifyBoxF1fsE, verifyBoxF1fsY, verifyBoxF1fsM, verifyBoxF1fsD);


                //初検日1
                DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                //施術開始日1
                DateTime f1Start = DateTimeEx.DateTimeNull;
                f1Start = DateTimeEx.IsDate(adYear, month, verifyBoxF1Start.GetIntValue()) ?
                    new DateTime(adYear, month, verifyBoxF1Start.GetIntValue()) :
                    DateTimeEx.DateTimeNull;
                setStatus(verifyBoxF1Start, f1Start.IsNullDate());

                //施術終了日1 
                DateTime f1Finish = DateTimeEx.DateTimeNull;
                f1Finish = DateTimeEx.IsDate(adYear, month, verifyBoxF1Finish1.GetIntValue()) ?
                    new DateTime(adYear, month, verifyBoxF1Finish1.GetIntValue()) :
                    DateTimeEx.DateTimeNull;
                setStatus(verifyBoxF1Finish1, f1Finish.IsNullDate());


                //20200717103711 furukawa st ////////////////////////
                //実日数不要でした
                
                        ////実日数1
                        //int f1days = verifyBoxDays1.GetIntValue();
                        //setStatus(verifyBoxDays1, f1days<0);
                //20200717103711 furukawa ed ////////////////////////



                //20200717102717 furukawa st ////////////////////////
                //部位数入力追加

                int buicount = verifyBoxBuiCount.GetIntValue();
                setStatus(verifyBoxBuiCount, buicount<=0);
                //20200717102717 furukawa ed ////////////////////////


                //20200717110156 furukawa st ////////////////////////
                //柔整師登録記号番号の頭一桁（協・契）の数字だけ必要
                
                //協・契
                string strKyoKei = verifyBoxKyoKei.Text;
                setStatus(verifyBoxKyoKei, strKyoKei == string.Empty);


                        ////柔整師登録記号番号
                        //string strDrCode = verifyBoxDrCode.Text.Trim();
                        //setStatus(verifyBoxDrCode, strDrCode != string.Empty && strDrCode.Length < 7);


                //20200717110156 furukawa ed ////////////////////////




                //20200717103127 furukawa st ////////////////////////
                //施術所、施術師入力不要でした

                            ////医療機関名
                            //string strClinicName = verifyBoxHosName.Text.Trim();

                            ////施術師名
                            //string strDrName = verifyBoxDrName.Text.Trim();
                //20200717103127 furukawa ed ////////////////////////




                //合計金額
                int total = verifyBoxTotal.GetIntValue();
                setStatus(verifyBoxTotal, total < 100 || total > 200000);

                //団体個人
                int dantaikojin = verifyBoxDantaiKojin.GetIntValue();
                setStatus(verifyBoxDantaiKojin, !new[] { 0, 1 }.Contains(dantaikojin));


                //ここまでのチェックで必須エラーが検出されたらfalse
                if (hasError)
                {
                    showInputErrorMessage();
                    return false;
                }

                /*
                //合計金額：請求金額：本家区分のトリプルチェック
                //金額でのエラーがあればいったん登録中断
                bool ratioError = (int)(total * ratio / 10) != seikyu;
                if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                if (ratioError)
                {
                    verifyBoxTotal.BackColor = Color.GreenYellow;
                    verifyBoxCharge.BackColor = Color.GreenYellow;
                    var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                        "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    if (res != System.Windows.Forms.DialogResult.OK) return false;
                }*/

                #endregion

                #region Appへの反映

           
     
                             
                //柔整師登録記号番号（契・協区別）
                
                
                //ここから値の反映
                app.MediYear = year;                //施術年
                app.MediMonth = month;              //施術月
                app.HihoNum = hnumN;                //被保険者証記号番号

                //本人/家族×100+本家区分
                app.Family = family * 100 + honkekbn;
                

                app.Total = total;                  //合計金額

                

                //申請書種別
                app.AppType = scan.AppType;

                //負傷         
                app.FushoName1 = verifyBoxF1.Text.Trim();//負傷名1
                app.FushoDate1 = f1Date;                //負傷年月日1
                app.FushoFirstDate1 = f1FirstDt;        //初検日
                app.FushoStartDate1 = f1Start;          //開始日
                app.FushoFinishDate1 = f1Finish;        //施術終了日1

                //20200717103735 furukawa st ////////////////////////
                //実日数不要でした
                
                            //app.FushoDays1 = f1days;                //日数1
                //20200717103735 furukawa ed ////////////////////////


                //20200717103017 furukawa st ////////////////////////
                //部位数入力追加

                app.TaggedDatas.count = buicount;//部位数入力
                //20200717103017 furukawa ed ////////////////////////


                //被保険者名
                app.HihoName = strHihoName;
                                

                //負傷名1
                app.FushoName1 = verifyBoxF1.Text.Trim();

                //新規継続
                app.NewContType = f1FirstDt.Year == f1Start.Year && f1FirstDt.Month == f1Start.Month ? NEW_CONT.新規 : NEW_CONT.継続;


                //20200717112318 furukawa st ////////////////////////
                //柔整師登録記号番号の頭一桁（協・契）の数字だけ必要

                        //柔整師登録記号番号
                        //app.DrNum = strDrCode;

                        //20200720164918 furukawa st ////////////////////////
                        //漢字つきにする
                        app.TaggedDatas.GeneralString5 = strKyoKei == "0" ? "0:協" : "1:契";
                                //app.TaggedDatas.GeneralString5 = strKyoKei;
                        //20200720164918 furukawa ed ////////////////////////

                //20200717112318 furukawa ed ////////////////////////




                //20200717103127 furukawa st ////////////////////////
                //施術所、施術師入力不要でした

                ////医療機関名
                //app.ClinicName = strClinicName;

                ////施術師名
                //app.DrName = strDrName;
                //20200717103127 furukawa ed ////////////////////////


                //団体個人
                app.TaggedDatas.GeneralString1 = dantaikojin == 0 ? "0:団体" : "1:個人";



                if (dgv.Rows.Count > 0)
                {
                    using (DataGridViewRow dgvr = dgv.CurrentRow)
                    {
                        //給付割合取得
                        int.TryParse(dgvr.Cells["ratio"].Value.ToString(), out int dgvRatio);

                        //20210209172523 furukawa st ////////////////////////
                        //合計×給付割合で請求金額を算出する場合、計算はDecimal型でやらないと丸め誤差出るので関数を使用する
                        
                        app.Charge = CommonTool.CalcCharge(app.Total, dgvRatio);


                                //  app.Ratio = dgvRatio / 10;
                                ////請求金額　合計金額×給付割合　1円未満切り捨て   
                                //  double dblcharge = double.Parse(app.Total.ToString()) * double.Parse(app.Ratio.ToString()) / 10;
                                //  decimal tmpdeccharge = Math.Truncate(decimal.Parse(dblcharge.ToString()));
                                //  app.Charge = int.Parse(tmpdeccharge.ToString());

                        //20210209172523 furukawa ed ////////////////////////

                        //一部負担金
                        app.Partial = app.Total - app.Charge;

                        //rrid登録
                        app.RrID = int.Parse(dgvr.Cells["importid"].Value.ToString());

                        //レセプト全国共通キー
                        app.ComNum = dgvr.Cells["comnum"].Value.ToString();
                        //保険者番号
                        app.InsNum = dgvr.Cells["insnum"].Value.ToString();
                      
                        //生年月日
                        app.Birthday = DateTime.Parse(dgvr.Cells["pbirthad"].Value.ToString());
                        //性別
                        string strgender = dgvr.Cells["pgender"].Value.ToString();
                        app.Sex = strgender == "男" ? 1 : 2;

                        //一応入れとく
                        //種別2
                        app.TaggedDatas.GeneralString2 = dgvr.Cells["shubetsu2"].Value.ToString();
                        //本家（漢字）
                        app.TaggedDatas.GeneralString3 = dgvr.Cells["honke"].Value.ToString();

                        //被保険者証記号番号（提供データ）
                        app.TaggedDatas.GeneralString4 = 
                            dgvr.Cells["hihomark"].Value.ToString().Trim()+ 
                            dgvr.Cells["hihonum"].Value.ToString().Trim();
                    }
                }



                #endregion

            }


            return true;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
             
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                
            }
            else if (verifyBoxY.Text == "**")
            {
                //ヘッダ（バッチ扱いとする）
                resetInputData(app);

                app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                app.AppType = APP_TYPE.バッチ;

            }
            

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }


            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;


            //20201228143631 furukawa st ////////////////////////
            //マッチング無し確認は申請書のみ
            
            if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
            {
                //20200720112810 furukawa st ////////////////////////
                //マッチングなし確認
                if (dgv.Rows.Count == 0)
                {
                    if (MessageBox.Show("マッチングなしですがよろしいですか？",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        return false;
                    }
                }
                //20200720112810 furukawa ed ////////////////////////
            }
            //20201228143631 furukawa ed ////////////////////////


            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211011132019 furukawa st ////////////////////////
                //AUXにもApptype追加
                
                if (!Application_AUX.Update(app.Aid,app.AppType, tran, app.RrID.ToString())) return false;//20210723115251 furukawa AUX更新
                //  if (!Application_AUX.Update(app.Aid, tran, app.RrID.ToString())) return false;//20210723115251 furukawa AUX更新
                //20211011132019 furukawa ed ////////////////////////

                jyuTran.Commit();
                tran.Commit();


                return true;
            }
        }
        



        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            int shinryoym = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue())*100 + verifyBoxM.GetIntValue();
            
            //20210721110232 furukawa st ////////////////////////
            //未入力時はAPPに何も入っていないので提供データが表示できないため、コントロールから値を取得
               
            createGrid();

            //      App a = (App)bsApp.Current;            
            //      createGrid(a);
            //20210721110232 furukawa ed ////////////////////////

            //createGrid(shinryoym, verifyBoxTotal.GetIntValue(), verifyBoxHnum.Text, a.RrID);

        }



        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {

            //20201123095046 furukawa st ////////////////////////
            //過去データロード

                    // verifyBoxHihoname.Text=getHihoName(verifyBoxHnum.Text.Trim());


            var app = (App)bsApp.Current;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            //現在のAppが1回目入力の場合のみ自動入力



            //20210126172013 furukawa st ////////////////////////
            //自動入力判定を間違って2回目も通してたため、
            //1回目と2回目が違う値だったとしても、TextVにも自動的にヒホバンから値を取ってしまい、上書きしてしまって、
            //1回目と同じ値になってしまい、ベリファイチェックが効いていない状態になった


            if (!firstTime && app.StatusFlagCheck(StatusFlag.入力済)) return;
            //      if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return;


            //20210126172013 furukawa ed ////////////////////////


            PastData.PersonalData data = new PastData.PersonalData();

            //20201125110535 furukawa st ////////////////////////
            //選択リストを出したくないので1行のみ抽出
            
            data = PastData.PersonalData.SelectRecordOne($"hnum='{verifyBoxHnum.Text.Trim()}'");
            //data = PastData.PersonalData.SelectRecord($"hnum='{verifyBoxHnum.Text.Trim()}'");
            //20201125110535 furukawa ed ////////////////////////


            if ((data == null) || (verifyBoxHnum.Text.Trim() == string.Empty))
            {
                //抽出がない場合はクリア
                setValue(verifyBoxHihoname, string.Empty, firstTime, nv);
            }
            else
            {
                setValue(verifyBoxHihoname, data.hname, firstTime, nv);
            }
                    
            //20201123095046 furukawa ed ////////////////////////

        }


        //20210712095446 furukawa st ////////////////////////
        //選択候補リストにフォーカスがある場合、PageUpで選択行が上に移動してしまうので間違った行の状態で登録される
        
        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) e.Handled = true;
        }
        //20210712095446 furukawa ed ////////////////////////



        #endregion

        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;

            //20210723163603 furukawa st ////////////////////////
            //マッチングチェック時に邪魔な判断
            
            else if (totalConts.Contains(sender)) p = posTotal;
            //else if ((scan.AppType == APP_TYPE.柔整) && (totalConts.Contains(sender))) p = posTotal;
            //20210723163603 furukawa ed ////////////////////////

            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (hihoNameConts.Contains(sender)) p = posHname;
            else if (drCodeConts.Contains(sender)) p = posDrCode;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (hihoNameConts.Any(c => c.Focused)) posHname = pos;

        }
        #endregion


    
       
    }      
}
