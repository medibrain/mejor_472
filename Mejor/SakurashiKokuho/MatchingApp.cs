﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.Sakurashi_Kokuho
{
    class MatchingApp
    {
        public static List<App> GetNotMatchApp(int cym)
        {
            //application_auxにあるmatchingid01=rridなのでそれを取得
            //但し、matchingid01がrridかどうかは保険者による
            var where = "FROM application AS a " +
                "inner join application_aux x on "+
                "a.aid=x.aid " +
                "LEFT OUTER JOIN importdata AS r ON x.matchingid01 = cast(r.importid as varchar) " +
                "WHERE r.importid IS NULL " +
                $"AND a.cym={cym} " +
                "AND (a.ayear>0 OR a.ayear=-999)";


            //var where = "FROM application AS a " +
            //    "LEFT OUTER JOIN refrece AS r ON a.aid = r.aid " +
            //    "WHERE r.aid IS NULL " +
            //    $"AND a.cym={cym} " +
            //    "AND (a.ayear>0 OR a.ayear=-999)";

            return App.InspectSelect(where);
        }

        public static List<App> GetOverlapApp(int cym)
        {
            //applicationの中にrridが２つ以上あったら表示
            var where = "FROM application AS a " +
                $"WHERE a.rrid IN( " +
                    "SELECT a2.rrid FROM application AS a2 " +
                    $"WHERE a2.cym={cym} " +
                    "AND a2.ayear>0 " +
                    "GROUP BY a2.rrid HAVING COUNT(a2.rrid) > 1) ";


            //var where = "FROM application AS a " +
            //    "WHERE a.numbering IN( " +
            //        "SELECT a2.numbering FROM application AS a2 " +
            //        $"WHERE a2.cym={cym} " +
            //        "AND a2.ayear>0 " +
            //        "GROUP BY a2.numbering HAVING COUNT(a2.numbering) > 1) ";

            return App.InspectSelect(where);
        }
    }
}
