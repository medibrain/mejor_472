﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Sakurashi_Kokuho
{
    class importdata
    {
        #region メンバ
        [DB.DbAttribute.Serial]
        public int importid { get;set; }=0;                                      //プライマリキー メホール管理用;

        public int cym { get; set; } = 0;                                        //メホール上の処理年月 メホール管理用;
        public string insnum { get; set; } = string.Empty;                       //保険者番号;
        public string hihomark { get; set; } = string.Empty;                     //証記号;
        public string hihonum { get; set; } = string.Empty;                      //証番号;
        public string pname { get; set; } = string.Empty;                        //療養者氏名;
        public string shinryoym { get; set; } = string.Empty;                    //診療年月;
        public string clinicnum { get; set; } = string.Empty;                    //機関コード;
        public string clinicname { get; set; } = string.Empty;                   //医療機関名;

        //20200918160417 furukawa st ////////////////////////
        //決定点数の列の場所を23から8へ移動（ファイルの仕様変更
        
        public int total { get; set; } = 0;                                       //決定点数;
        //20200918160417 furukawa ed ////////////////////////


        public string history { get; set; } = string.Empty;                      //最新履歴;
        public string setai { get; set; } = string.Empty;                        //世帯番号;
        public string atenanum { get; set; } = string.Empty;                     //宛名番号;
        public string pbirth { get; set; } = string.Empty;                       //生年月日;
        public string pgender { get; set; } = string.Empty;                      //性別;
        public string shubetsu1 { get; set; } = string.Empty;                    //種別１;
        public string shubetsu2 { get; set; } = string.Empty;                    //種別２;
        public string nyugai { get; set; } = string.Empty;                       //入外;
        public string honke { get; set; } = string.Empty;                        //本家;
        public int ratio { get; set; } =0;                                      //給付割合;(パーセント）
        public string shinryo_ryouyouhibetsu { get; set; } = string.Empty;       //診療・療養費別;
        public string tensuhyo { get; set; } = string.Empty;                     //点数表;
        public string ryouyouhibetsu { get; set; } = string.Empty;               //療養費種別;
        public string shinsaym { get; set; } = string.Empty;                     //審査年月;
        public int days { get; set; } = 0;                                       //実日数;

        //20200918160437 furukawa st ////////////////////////
        //決定点数の列の場所を23から8へ移動（ファイルの仕様変更

        //public int total { get; set; } =0;                                       //決定点数;
        //20200918160437 furukawa ed ////////////////////////


        public string shokuji { get; set; } = string.Empty;                      //食事基準額;
        public string comnum { get; set; } = string.Empty;                       //レセプト全国共通キー;
        public string shohoukikancode { get; set; } = string.Empty;              //処方機関コード;
        public string shohoukikanname { get; set; } = string.Empty;              //処方機関名;
        public string kuri { get; set; } = string.Empty;                         //クリ;
        public string kea { get; set; } = string.Empty;                          //ケア;
        public string younin { get; set; } = string.Empty;                       //容認;
        public string futou { get; set; } = string.Empty;                        //不当;
        public string dai3sha { get; set; } = string.Empty;                      //第三者;
        public string kyufuseigen { get; set; } = string.Empty;                  //給付制限;
        public string kougaku { get; set; } = string.Empty;                      //高額;
        public string yoyaku { get; set; } = string.Empty;                       //予約;
        public string shori { get; set; } = string.Empty;                        //処理;
        public string gigishubetsu { get; set; } = string.Empty;                 //疑義種別;
        public string sankou { get; set; } = string.Empty;                       //参考;
        public string joutai { get; set; } = string.Empty;                       //状態;
        public string codejouhou { get; set; } = string.Empty;                   //コード情報;
        public string genponkbn { get; set; } = string.Empty;                    //原本区分;
        public string genponshozai { get; set; } = string.Empty;                 //原本所在;
        public string kengai { get; set; } = string.Empty;                       //県内外;
        public string fusen01 { get; set; } = string.Empty;                      //付箋１  ;
        public string fusen02 { get; set; } = string.Empty;                      //付箋２;
        public string fusen03 { get; set; } = string.Empty;                      //付箋３;
        public string fusen04 { get; set; } = string.Empty;                      //付箋４;
        public string fusen05 { get; set; } = string.Empty;                      //付箋５;
        public string fusen06 { get; set; } = string.Empty;                      //付箋６;
        public string fusen07 { get; set; } = string.Empty;                      //付箋７;
        public string fusen08 { get; set; } = string.Empty;                      //付箋８;
        public string fusen09 { get; set; } = string.Empty;                      //付箋９;
        public string fusen10 { get; set; } = string.Empty;                      //付箋１０;
        public string fusen11 { get; set; } = string.Empty;                      //付箋１１;
        public string fusen12 { get; set; } = string.Empty;                      //付箋１２;
        public string fusen13 { get; set; } = string.Empty;                      //付箋１３;
        public string fusen14 { get; set; } = string.Empty;                      //付箋１４;
        public string fusen15 { get; set; } = string.Empty;                      //付箋１５;
        public string fusen16 { get; set; } = string.Empty;                      //付箋１６;
        public int shinryoymad { get; set; } = 0;                                //診療年月西暦;
        public DateTime pbirthad { get; set; } =DateTime.MinValue;                                   //生年月日西暦;
        public int shinsaymad { get; set; } = 0;                                 //審査年月西暦;
        public string hihonumnarrow { get; set; } = string.Empty;                 //証番号半角数字ハイフン抜き

        #endregion


        /// <summary>
        /// 提供データ用リスト
        /// </summary>
        private static List<importdata> lstImp = new List<importdata>();



        /// <summary>
        /// 和暦変換
        /// </summary>
        private static System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-jp");
        

              

        /// <summary>
        /// インポート処理
        /// </summary>
        /// <param name="_cym">メホール処理年月</param>
        public static void Import(int _cym)
        {
            //和暦カレンダー
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //インポートファイル選択
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;
            string strFileName = ofd.FileName;



            WaitForm wf =new WaitForm();
            wf.ShowDialogOtherTask();
            

            wf.LogPrint("csv情報取得");      

            List<string[]> lst=CommonTool.CsvImportMultiCode(strFileName);

            DB.Transaction tran = DB.Main.CreateTransaction();

            //20200713103234 furukawa st ////////////////////////
            //事前削除をしない。同じメホール請求年月で複数入れる場合がある

            //wf.LogPrint($"{_cym}削除");
            //DB.Command cmd = new DB.Command($"delete from importdata where cym={_cym}", tran);
            //cmd.TryExecuteNonQuery();

            //20200713103234 furukawa ed ////////////////////////


            try
            {                
                wf.LogPrint("情報インポート");
                wf.SetMax(lst.Count);                
                
                foreach (string[] s in lst)
                {
                    //最初のレコードが数字でない場合飛ばす
                    if (!int.TryParse(s[0].ToString(), out int tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }
                    importdata impdata = new importdata();
                    int colcnt = 0;

                    
                    impdata.cym = _cym;                                                      //メホール上の処理年月 メホール管理用;
                    impdata.insnum = s[colcnt++].ToString();                                 //保険者番号;
                    impdata.hihomark = s[colcnt++].ToString();                               //証記号;
                    impdata.hihonum = s[colcnt++].ToString();                                //証番号;
                    impdata.pname = s[colcnt++].ToString();                                  //療養者氏名;
                    impdata.shinryoym = s[colcnt++].ToString();                               //診療年月; フォーマット：令和01年11月
                    impdata.clinicnum = s[colcnt++].ToString();                              //機関コード;
                    impdata.clinicname = s[colcnt++].ToString();                             //医療機関名;


                    //20200918160151 furukawa st ////////////////////////
                    //決定点数の列の場所を23から8へ移動（ファイルの仕様変更
                    
                    int tmptotal = int.Parse(s[colcnt++].ToString().Replace(",", string.Empty));
                    impdata.total = tmptotal;                                //決定点数;
                    //20200918160151 furukawa ed ////////////////////////



                    impdata.history = s[colcnt++].ToString();                                //最新履歴;
                    impdata.setai = s[colcnt++].ToString();                                  //世帯番号;
                    impdata.atenanum = s[colcnt++].ToString();                               //宛名番号;
                    impdata.pbirth = s[colcnt++].ToString();                                 //生年月日;フォーマット：昭和52年01月08日
                    impdata.pgender = s[colcnt++].ToString();                                //性別;
                    impdata.shubetsu1 = s[colcnt++].ToString();                              //種別１;
                    impdata.shubetsu2 = s[colcnt++].ToString();                              //種別２;
                    impdata.nyugai = s[colcnt++].ToString();                                 //入外;
                    impdata.honke = s[colcnt++].ToString();                                  //本家;
                    impdata.ratio = int.Parse(s[colcnt++].ToString());                       //給付割合;
                    impdata.shinryo_ryouyouhibetsu = s[colcnt++].ToString();                 //診療・療養費別;
                    impdata.tensuhyo = s[colcnt++].ToString();                               //点数表;
                    impdata.ryouyouhibetsu = s[colcnt++].ToString();                         //療養費種別;
                    impdata.shinsaym = s[colcnt++].ToString();                                //審査年月;フォーマット：令和02年01月

                    int.TryParse(s[colcnt++].ToString(), out int tmpdays);
                    impdata.days = tmpdays;             //実日数;

                    //20200918160243 furukawa st ////////////////////////
                    //決定点数の列の場所を23から8へ移動（ファイルの仕様変更
                    
                    //int tmptotal = int.Parse(s[colcnt++].ToString().Replace(",", string.Empty));
                    //impdata.total = tmptotal;                                //決定点数;
                    //20200918160243 furukawa ed ////////////////////////


                    impdata.shokuji = s[colcnt++].ToString();                                //食事基準額;
                    impdata.comnum = s[colcnt++].ToString();                                 //レセプト全国共通キー;
                    impdata.shohoukikancode = s[colcnt++].ToString();                        //処方機関コード;
                    impdata.shohoukikanname = s[colcnt++].ToString();                        //処方機関名;
                    impdata.kuri = s[colcnt++].ToString();                                   //クリ;
                    impdata.kea = s[colcnt++].ToString();                                    //ケア;
                    impdata.younin = s[colcnt++].ToString();                                 //容認;
                    impdata.futou = s[colcnt++].ToString();                                  //不当;
                    impdata.dai3sha = s[colcnt++].ToString();                                //第三者;
                    impdata.kyufuseigen = s[colcnt++].ToString();                            //給付制限;
                    impdata.kougaku = s[colcnt++].ToString();                                //高額;
                    impdata.yoyaku = s[colcnt++].ToString();                                 //予約;
                    impdata.shori = s[colcnt++].ToString();                                  //処理;
                    impdata.gigishubetsu = s[colcnt++].ToString();                           //疑義種別;
                    impdata.sankou = s[colcnt++].ToString();                                 //参考;
                    impdata.joutai = s[colcnt++].ToString();                                 //状態;
                    impdata.codejouhou = s[colcnt++].ToString();                             //コード情報;
                    impdata.genponkbn = s[colcnt++].ToString();                              //原本区分;
                    impdata.genponshozai = s[colcnt++].ToString();                           //原本所在;
                    impdata.kengai = s[colcnt++].ToString();                                 //県内外;
                    impdata.fusen01 = s[colcnt++].ToString();                                //付箋１  ;
                    impdata.fusen02 = s[colcnt++].ToString();                                //付箋２;
                    impdata.fusen03 = s[colcnt++].ToString();                                //付箋３;
                    impdata.fusen04 = s[colcnt++].ToString();                                //付箋４;
                    impdata.fusen05 = s[colcnt++].ToString();                                //付箋５;
                    impdata.fusen06 = s[colcnt++].ToString();                                //付箋６;
                    impdata.fusen07 = s[colcnt++].ToString();                                //付箋７;
                    impdata.fusen08 = s[colcnt++].ToString();                                //付箋８;
                    impdata.fusen09 = s[colcnt++].ToString();                                //付箋９;
                    impdata.fusen10 = s[colcnt++].ToString();                                //付箋１０;
                    impdata.fusen11 = s[colcnt++].ToString();                                //付箋１１;
                    impdata.fusen12 = s[colcnt++].ToString();                                //付箋１２;
                    impdata.fusen13 = s[colcnt++].ToString();                                //付箋１３;
                    impdata.fusen14 = s[colcnt++].ToString();                                //付箋１４;
                    impdata.fusen15 = s[colcnt++].ToString();                                //付箋１５;
                    impdata.fusen16 = s[colcnt++].ToString();                                //付箋１６;


                    //診療年月西暦
                    impdata.shinryoymad = DateTimeEx.GetAdYear(impdata.shinryoym.ToString().Substring(0, 4))*100 + int.Parse(impdata.shinryoym.ToString().Substring(5, 2));
                    //受療者生年月日西暦
                     impdata.pbirthad = DateTime.ParseExact(impdata.pbirth.ToString(), "ggyy年MM月dd日", ci,System.Globalization.DateTimeStyles.None);
                    //審査年月西暦
                    impdata.shinsaymad = DateTimeEx.GetAdYear(impdata.shinsaym.ToString().Substring(0, 4)) * 100 + int.Parse(impdata.shinsaym.ToString().Substring(5, 2));

                    //被保番半角ハイフン抜き
                    impdata.hihonumnarrow = Microsoft.VisualBasic.Strings.StrConv(impdata.hihonum.ToString(), Microsoft.VisualBasic.VbStrConv.Narrow).Replace("-", string.Empty);


                    wf.LogPrint($"レセプト全国共通キー:{impdata.comnum}");
                    DB.Main.Insert<importdata>(impdata, tran);

                    wf.InvokeValue++;
                }


                tran.Commit();
                System.Windows.Forms.MessageBox.Show("終了");
                return ;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return ;
            }
            finally
            {
                wf.Dispose();
            }

        }



        /// <summary>
        /// 情報全取得
        /// </summary>
        /// <returns> List<importdata> </returns>
        public static List<importdata> SelectAll(int _cym)
        {
            string strsql = string.Empty;
            strsql += "";

            DB.Command cmd = DB.Main.CreateCmd("select * from importdata " +
                $"where cym={_cym}" +
                "order by importid");

            var list=cmd.TryExecuteReaderList();
            lstImp.Clear();

            foreach (var item in list)
            {

                importdata id = new importdata();
                int col = 2;
                if (!int.TryParse(item[0].ToString(), out int tmp)) continue;

                //20200923154256 furukawa st ////////////////////////
                //決定点数の列の場所を23から8へ移動（ファイルの仕様変更、列番号使わない
                
                id.importid = int.Parse(item[0].ToString());
                id.cym = int.Parse(item[1].ToString());

                //これ以下インポートデータ
                id.insnum = item[col++].ToString();
                id.hihomark = item[col++].ToString();
                id.hihonum = item[col++].ToString();
                id.pname = item[col++].ToString();
                id.shinryoym = item[col++].ToString();
                id.clinicnum = item[col++].ToString();
                id.clinicname = item[col++].ToString();

                //合計金額
                id.total = int.Parse(item[col++].ToString());

                id.history = item[col++].ToString();
                id.setai = item[col++].ToString();
                id.atenanum = item[col++].ToString();
                id.pbirth = item[col++].ToString();
                id.pgender = item[col++].ToString();
                id.shubetsu1 = item[col++].ToString();
                id.shubetsu2 = item[col++].ToString();
                id.nyugai = item[col++].ToString();
                id.honke = item[col++].ToString();
                id.ratio = int.Parse(item[col++].ToString());
                id.shinryo_ryouyouhibetsu = item[col++].ToString();
                id.tensuhyo = item[col++].ToString();
                id.ryouyouhibetsu = item[col++].ToString();
                id.shinsaym = item[col++].ToString();
                id.days = int.Parse(item[col++].ToString());

                //合計金額は８列目に移動
                //id.total = int.Parse(item[col++].ToString());

                id.shokuji = item[col++].ToString();
                id.comnum = item[col++].ToString();
                id.shohoukikancode = item[col++].ToString();
                id.shohoukikanname = item[col++].ToString();
                id.kuri = item[col++].ToString();
                id.kea = item[col++].ToString();
                id.younin = item[col++].ToString();
                id.futou = item[col++].ToString();
                id.dai3sha = item[col++].ToString();
                id.kyufuseigen = item[col++].ToString();
                id.kougaku = item[col++].ToString();
                id.yoyaku = item[col++].ToString();
                id.shori = item[col++].ToString();
                id.gigishubetsu = item[col++].ToString();
                id.sankou = item[col++].ToString();
                id.joutai = item[col++].ToString();
                id.codejouhou = item[col++].ToString();
                id.genponkbn = item[col++].ToString();
                id.genponshozai = item[col++].ToString();
                id.kengai = item[col++].ToString();
                id.fusen01 = item[col++].ToString();
                id.fusen02 = item[col++].ToString();
                id.fusen03 = item[col++].ToString();
                id.fusen04 = item[col++].ToString();
                id.fusen05 = item[col++].ToString();
                id.fusen06 = item[col++].ToString();
                id.fusen07 = item[col++].ToString();
                id.fusen08 = item[col++].ToString();
                id.fusen09 = item[col++].ToString();
                id.fusen10 = item[col++].ToString();
                id.fusen11 = item[col++].ToString();
                id.fusen12 = item[col++].ToString();
                id.fusen13 = item[col++].ToString();
                id.fusen14 = item[col++].ToString();
                id.fusen15 = item[col++].ToString();
                id.fusen16 = item[col++].ToString();


                //診療年月西暦
                id.shinryoymad = int.Parse(item[col++].ToString());

                //受療者生年月日西暦
                id.pbirthad = DateTime.Parse(item[col++].ToString());

                //審査年月西暦
                id.shinsaymad = int.Parse(item[col++].ToString());

                //被保険者証番号半角
                id.hihonumnarrow = item[col++].ToString();



                #region old
                //id.insnum = item[2].ToString();
                //id.hihomark = item[3].ToString();
                //id.hihonum = item[4].ToString();
                //id.pname = item[5].ToString();
                //id.shinryoym = item[6].ToString();
                //id.clinicnum = item[7].ToString();
                //id.clinicname = item[8].ToString();                
                //id.history = item[9].ToString();
                //id.setai = item[10].ToString();
                //id.atenanum = item[11].ToString();
                //id.pbirth = item[12].ToString();
                //id.pgender = item[13].ToString();
                //id.shubetsu1 = item[14].ToString();
                //id.shubetsu2 = item[15].ToString();
                //id.nyugai = item[16].ToString();
                //id.honke = item[17].ToString();
                //id.ratio = int.Parse(item[18].ToString());
                //id.shinryo_ryouyouhibetsu = item[19].ToString();
                //id.tensuhyo = item[20].ToString();
                //id.ryouyouhibetsu = item[21].ToString();
                //id.shinsaym = item[22].ToString();
                //id.days = int.Parse(item[23].ToString());
                //id.total = int.Parse(item[24].ToString());
                //id.shokuji = item[25].ToString();
                //id.comnum = item[26].ToString();
                //id.shohoukikancode = item[27].ToString();
                //id.shohoukikanname = item[28].ToString();
                //id.kuri = item[29].ToString();
                //id.kea = item[30].ToString();
                //id.younin = item[31].ToString();
                //id.futou = item[32].ToString();
                //id.dai3sha = item[33].ToString();
                //id.kyufuseigen = item[34].ToString();
                //id.kougaku = item[35].ToString();
                //id.yoyaku = item[36].ToString();
                //id.shori = item[37].ToString();
                //id.gigishubetsu = item[38].ToString();
                //id.sankou = item[39].ToString();
                //id.joutai = item[40].ToString();
                //id.codejouhou = item[41].ToString();
                //id.genponkbn = item[42].ToString();
                //id.genponshozai = item[43].ToString();
                //id.kengai = item[44].ToString();
                //id.fusen01 = item[45].ToString();
                //id.fusen02 = item[46].ToString();
                //id.fusen03 = item[47].ToString();
                //id.fusen04 = item[48].ToString();
                //id.fusen05 = item[49].ToString();
                //id.fusen06 = item[50].ToString();
                //id.fusen07 = item[51].ToString();
                //id.fusen08 = item[52].ToString();
                //id.fusen09 = item[53].ToString();
                //id.fusen10 = item[54].ToString();
                //id.fusen11 = item[55].ToString();
                //id.fusen12 = item[56].ToString();
                //id.fusen13 = item[57].ToString();
                //id.fusen14 = item[58].ToString();
                //id.fusen15 = item[59].ToString();
                //id.fusen16 = item[60].ToString();


                ////診療年月西暦
                //id.shinryoymad = int.Parse(item[61].ToString());
                ////DateTimeEx.GetAdYear(id.shinryoym.ToString().Substring(0, 4))
                ////+ int.Parse(id.shinryoym.ToString().Substring(5, 2));

                ////受療者生年月日西暦
                //id.pbirthad = DateTime.Parse(item[62].ToString());
                //// DateTime.ParseExact(id.pbirth.ToString(), "ggyy年MM月dd日", ci, System.Globalization.DateTimeStyles.None);

                ////審査年月西暦
                //id.shinsaymad = int.Parse(item[63].ToString());
                //    //DateTimeEx.GetAdYear(id.shinsaym.ToString().Substring(0, 4))
                //    //+ int.Parse(id.shinsaym.ToString().Substring(5, 2));

                //id.hihonumnarrow= item[64].ToString();


                #endregion

                //20200923154256 furukawa ed ////////////////////////

                lstImp.Add(id);
            }


            return lstImp;
        }
    }
}
