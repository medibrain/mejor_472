﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.NaritaKokuho
{
    class Doctor
    {
        [DB.DbAttribute.PrimaryKey]
        public string DrCode { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public int LastAID { get; set; }
        public bool Verified { get; set; } = false;

        public static Doctor Select(string code)
        {
            return DB.Main.Select<Doctor>(new { drcode = code }).FirstOrDefault();
        }

        private bool insert()
        {
            return DB.Main.Insert(this);
        }

        private bool update()
        {
            return DB.Main.Update(this);
        }

        public static bool Upsert(App app)
        {
            if (string.IsNullOrEmpty(app.DrNum)) return true;

            var p = Select(app.DrNum);
            if (p == null)
            {
                p = new Doctor();
                p.DrCode = app.DrNum;
                p.DrName = app.DrName;
                p.ClinicName = app.ClinicName;
                p.LastAID = app.Aid;
                p.Verified = false;
                return p.insert();
            }
            else
            {
                if (p.DrName == app.DrName &&
                    p.ClinicName == app.PersonName)
                {
                    if (p.Verified) return true;
                    if (p.LastAID == app.Aid) return true;
                    p.LastAID = app.Aid;
                    p.Verified = true;
                    return p.update();
                }
                else
                {
                    p.DrName = app.DrName;
                    p.ClinicName = app.ClinicName;
                    p.Verified = false;
                    p.LastAID = app.Aid;
                    return p.update();
                }
            }
        }
    }
}