﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.NaritaKokuho
{
    public class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var sdic = new Dictionary<int, Scan>();

            foreach (var item in scans) sdic.Add(item, Scan.Select(item));

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,バッチNo.,保険証番号,世帯主名,受療者名,受療者生年月日,郵便番号,住所,施術所名,柔整師名," +
                        "施術年,施術月,新規継続,実日数,負傷名1,負傷名2,負傷名3,負傷名4,負傷名5," +
                        "合計金額,保険者負担額,照会理由,メモ,照会対象,点検対象,点検結果,過誤理由,再審査理由");

                    var ss = new List<string>();
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };

                    foreach (var item in list)
                    {
                        ss.Add(item.Aid.ToString());
                        ss.Add(string.Empty);
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(item.ClinicName);
                        ss.Add(item.DrName);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.NewContType.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.FushoName1);
                        ss.Add(item.FushoName2);
                        ss.Add(item.FushoName3);
                        ss.Add(item.FushoName4);
                        ss.Add(item.FushoName5);
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");//区切り文字が「, (半角カンマ)(半角スペース)」なのでエスケープ処理
                        ss.Add("\"" + item.Memo + "\"");
                        ss.Add(item.StatusFlagCheck(StatusFlag.照会対象) ? "○" : "");
                        ss.Add(item.StatusFlagCheck(StatusFlag.点検対象) ? "○" : "");
                        ss.Add(item.InspectInfo);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
