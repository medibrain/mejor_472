﻿using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    public class PostalCode
    {
        public int Dantai_Code { get; set; }
        public int Old_Postal_Code { get; set; }
        public int Postal_Code { get; set; }
        public string Todofuken_Kana { get; set; }
        public string Sichokuson_Kana { get; set; }
        public string Choiki_Kana { get; set; }
        public string Todofuken { get; set; }
        public string Sichokuson { get; set; }
        public string Choiki { get; set; }
        public bool Multi_Code_Flag { get; set; }
        public bool Koaza_Flag { get; set; }
        public bool Chome_Flag { get; set; }
        public bool Multi_Choiki_Flag { get; set; }
        public int Kosin { get; set; }
        public int Kosin_Reason { get; set; }

        /// <summary>
        /// 郵便番号から住所情報を取得します。
        /// 複数個取得された場合はnullを返します。
        /// </summary>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        public static PostalCode GetPostalCode(int postalcode)
        {
            var db = new DB("common");
            using (var cmd = db.CreateCmd(
                "SELECT * FROM \"PostalCode\" " +
                "WHERE postal_code=:postal_code"))
            {
                cmd.Parameters.Add("postal_code", NpgsqlDbType.Integer).Value = postalcode;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;//重複している場合は無視
                return createInstance(res[0]);
            }
        }

        /// <summary>
        /// 都道府県情報を検索します。
        /// nullまたは-1の項目はWHERE文には含まれません。すべてnullの場合はnullを返します。
        /// </summary>
        /// <param name="postalcode"></param>
        /// <param name="todofuken"></param>
        /// <param name="sikutyoson"></param>
        /// <param name="choiki"></param>
        /// <returns></returns>
        public static List<PostalCode> SearchPostalCode
            (int postalcode, string todofuken, string sikutyoson, string choiki)
        {
            if (postalcode == -1 && todofuken == null && sikutyoson == null && choiki == null)
            {
                return null;
            }

            string where = "WHERE ";
            if (postalcode != -1) where = $"{where}postal_code={postalcode} AND ";
            if (todofuken != null) where = $"{where}\"Todofuken\" LIKE '%{todofuken}%' AND ";
            if (sikutyoson != null) where = $"{where}\"Sichoukuson\" LIKE '%{sikutyoson}%' AND ";
            if (choiki != null) where = $"{where}\"Choiki\" LIKE '%{choiki}%' AND ";
            if (where.Contains("AND")) where = where.Substring(0, where.LastIndexOf("AND"));//末尾AND削除

            var db = new DB("common");
            using (var cmd = db.CreateCmd(
                "SELECT * FROM \"PostalCode\" " +
                where +
                "ORDER BY postal_code"))
            {
                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var list = new List<PostalCode>();
                res.ForEach(obj => list.Add(createInstance(obj)));
                return list;
            }
        }

        private static PostalCode createInstance(object[] obj)
        {
            var p = new PostalCode();
            p.Dantai_Code = (int)obj[0];
            p.Old_Postal_Code = (int)obj[1];
            p.Postal_Code = (int)obj[2];
            p.Todofuken_Kana = (string)obj[3];
            p.Sichokuson_Kana = (string)obj[4];
            p.Choiki_Kana = (string)obj[5];
            p.Todofuken = (string)obj[6];
            p.Sichokuson = (string)obj[7];
            p.Choiki = (string)obj[8];
            p.Multi_Code_Flag = (bool)obj[9];
            p.Koaza_Flag = (bool)obj[10];
            p.Chome_Flag = (bool)obj[11];
            p.Multi_Choiki_Flag = (bool)obj[12];
            p.Kosin = (int)obj[13];
            p.Kosin_Reason = (int)obj[14];

            return p;
        }
    }
}
