﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    
    
    
    public enum INPUT_TYPE { Null = 0,
        First = 1, Second = 2, Both = 3,
        ExFirst = 11, ExSecond = 12,
        Sp1 = 21, Sp2 = 22,//20200607172556 furukawa保険者個別機能追加
        Extra1 = 31, Extra2 = 32,//20200513173746 furukawa 保険者個別機能追加
        ListInput1 = 41, ListInput2 = 42,//20200606090613 furukawaリスト抽出入力の区分追加
        GaibuInput1 = 51, GaibuInput2 = 52, //HonsyaCheck = 53, //20221215 ito 外部業者入力用
    }

    class InputLog
    {
        static DB db = new DB("jyusei");

        [DB.DbAttribute.Serial, DB.DbAttribute.PrimaryKey]
        public int ID { get; set; }

        public int InsurerID { get; set; }
        public int AID { get; set; }
        public APP_TYPE AppType { get; set; }
        public int UID { get; set; }
        public DateTime InputDT { get; set; }
        public INPUT_TYPE InputType { get; set; }
        public int MissCount { get; set; }

        public TimeSpan timespan { get; set; }




        static Dictionary<string, string> nameTable = new Dictionary<string, string>();

        static InputLog()
        {
            nameTable.Add("verifyBoxClinicCode", "医療機関番号");
            nameTable.Add("verifyBoxShinkyuDr", "鍼灸あんま機関番号");
            nameTable.Add("verifyBoxPref", "都道府県");
            nameTable.Add("verifyBoxBatch", "バッチ番号");
            nameTable.Add("verifyBoxAllTotal", "バッチ合計額");
            nameTable.Add("verifyBoxY", "診療年");
            nameTable.Add("verifyBoxM", "診療月");
            nameTable.Add("verifyBoxHnum", "被保険者番号");
            nameTable.Add("verifyBoxBE", "生年月日-年号");
            nameTable.Add("verifyBoxBY", "生年月日-年");
            nameTable.Add("verifyBoxBM", "生年月日-月");
            nameTable.Add("verifyBoxBD", "生年月日-日");
            nameTable.Add("verifyBoxBirthE", "生年月日-年号");
            nameTable.Add("verifyBoxBirthY", "生年月日-年");
            nameTable.Add("verifyBoxBirthM", "生年月日-月");
            nameTable.Add("verifyBoxBirthD", "生年月日-日");
            nameTable.Add("verifyBoxF1Start", "負傷1開始日");
            nameTable.Add("verifyBoxF1Finish", "負傷1終了日");
            nameTable.Add("verifyBoxF1", "負傷名1");
            nameTable.Add("verifyBoxF1FirstY", "負傷1初検日-年");
            nameTable.Add("verifyBoxF1FirstM", "負傷1初検日-月");
            nameTable.Add("verifyBoxF1FirstD", "負傷1初検日-日");
            nameTable.Add("verifyBoxTotal", "合計");
            nameTable.Add("verifyBoxCharge", "請求");
            nameTable.Add("verifyBoxSeikyu", "請求");
            nameTable.Add("verifyCheckBoxSai", "支払猶予-災");
            nameTable.Add("verifyBoxCount", "合計枚数");
            nameTable.Add("verifyBoxHnumM", "被保険者記号");
            nameTable.Add("verifyBoxHnumN", "被保険者番号");
            nameTable.Add("verifyBoxAppType", "申請区分(柔整あはき)");
            nameTable.Add("verifyBoxDays", "診療日数");
            nameTable.Add("verifyBoxFamily", "本人/家族");
            nameTable.Add("verifyBoxNumbering", "ナンバリング");
            nameTable.Add("verifyBoxRatio", "給付割合");
            nameTable.Add("verifyBoxKohi", "公費");
            nameTable.Add("verifyBoxDrCode", "柔整師登録番号");
            nameTable.Add("verifyBoxFutan", "負担(請求金額下)");
            nameTable.Add("verifyBoxHihoNumber", "被保険者番号");
            nameTable.Add("verifyBoxSex", "性別");
            nameTable.Add("verifyBoxHihoMark", "被保険者記号");
            nameTable.Add("verifyBoxBank", "口座番号");
        }


        //20200812172725 furukawa st ////////////////////////
        //新バージョンと区別するため削除

        //public static bool LogWrite(App app, INPUT_TYPE type, int missCount, DB.Transaction tran)
        //{
        //    var log = new InputLog()
        //    {
        //        InsurerID = Insurer.CurrrentInsurer.InsurerID,
        //        AID = app.Aid,
        //        AppType = app.AppType,
        //        UID = User.CurrentUser.UserID,
        //        InputDT = DateTime.Now,
        //        InputType = type,
        //        MissCount = missCount,

        //    };

        //    return db.Insert(log, tran);
        //}
        //20200812172725 furukawa ed ////////////////////////


        //20200812172811 furukawa st ////////////////////////
        //timespan登録用のログ登録関数
        
        /// <summary>
        /// 入力ログ登録
        /// </summary>
        /// <param name="app"></param>
        /// <param name="type"></param>
        /// <param name="missCount"></param>
        /// <param name="ts">timespan</param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public static bool LogWriteTs(App app, INPUT_TYPE type, int missCount,TimeSpan ts, DB.Transaction tran)
        {
            var log = new InputLog()
            {
                InsurerID = Insurer.CurrrentInsurer.InsurerID,
                AID = app.Aid,
                AppType = app.AppType,
                UID = User.CurrentUser.UserID,
                InputDT = DateTime.Now,
                InputType = type,
                MissCount = missCount,
                
                timespan = ts,
            };

            return db.Insert(log, tran);
        }
        //20200812172811 furukawa ed ////////////////////////






        /// <summary>
        /// 初回入力時のミスカウントを記録します
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="missCount"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public static bool FirstMissLogWrite(int aid, int missCount, DB.Transaction tran)
        {
            var sql = "UPDATE inputlog SET misscount=@misscount " +
                "WHERE insurerid=@insurerid AND aid=@aid";

            return db.Excute(sql,
                new { missCount = missCount, insurerid = Insurer.CurrrentInsurer.InsurerID, aid = aid }, tran);
        }

        class GroupedInputLog
        {
            public int UID { get; set; }
            public DateTime InputHour { get; set; }
            public int InsurerID { get; set; }
            public INSURER_TYPE InsurerType { get; set; }
            public APP_TYPE AppType { get; set; }
            public INPUT_TYPE InputType { get; set; }
            public int Count { get; set; }
            public int Miss { get; set; }

            public static IEnumerable<GroupedInputLog> GetLog(DateTime startDt, DateTime endDt)
            {
                endDt = endDt.AddDays(1);

                var sql = $@"SELECT uid, date_trunc('hour',inputdt) AS inputhour, 
                insurerid, apptype, inputtype, COUNT(uid), SUM(misscount) AS miss FROM inputlog 
                WHERE inputdt BETWEEN '{startDt.ToString("yyyy-MM-dd")}' and '{endDt.ToString("yyyy-MM-dd")}'
                GROUP BY uid, date_trunc('hour',inputdt), insurerid, apptype, inputtype
                ORDER BY date_trunc('hour',inputdt)";

                return db.Query<GroupedInputLog>(sql);
            }
        }

        public class AggregateLog
        {
            public int UID { get; set; }
            public string UserName => User.GetUserName(UID);
            public DateTime? InputDt { get; set; }
            public int? InsurerID { get; set; }
            public INSURER_TYPE InsurerType => Insurer.GetInsurerType(InsurerID ?? 0);
            public string InsurerName => InsurerID == null ? "" : Insurer.GetInsurerName(InsurerID.Value);

            public int TotalCount => InputCount + VerifyCount;
            public string TotalSpan => getTimeSpan(TotalDateTime);
            public int InputCount => JyuInputCount + AnmaInputCount + HariKyuInputCount + OtherInputCount;
            public string InputSpan => getTimeSpan(InputDateTime);
            public int VerifyCount => JyuVerifyCount + AnmaVerifyCount + HariKyuVerifyCount + OtherVerifyCount;
            public string VerifySpan => getTimeSpan(VerifyDateTime);
            public int JyuInputCount { get; set; }
            public string JyuInputSpan => getTimeSpan(JyuInputDateTime);
            public int JyuVerifyCount { get; set; }
            public string JyuVerifySpan => getTimeSpan(JyuVerifyDateTime);
            public int AnmaInputCount { get; set; }
            public int HariKyuInputCount { get; set; }
            public string AnmaInputSpan => getTimeSpan(AnmaInputDateTime);
            public string HariKyuInputSpan => getTimeSpan(HariKyuInputDateTime);
            public int AnmaVerifyCount { get; set; }
            public int HariKyuVerifyCount { get; set; }
            public string AnmaVerifySpan => getTimeSpan(AnmaVerifyDateTime);
            public string HariKyuVerifySpan => getTimeSpan(HariKyuVerifyDateTime);
            public int OtherInputCount { get; set; }
            public string OtherInputSpan => getTimeSpan(OtherInputDateTime);
            public int OtherVerifyCount { get; set; }
            public string OtherVerifySpan => getTimeSpan(OtherVerifyDateTime);

            public static readonly TimeSpan Span15 = new TimeSpan(0, 15, 0);

            public List<DateTime> TotalDateTime
            {
                get
                {
                    var dts = new List<DateTime>();
                    dts.AddRange(InputDateTime);
                    dts.AddRange(VerifyDateTime);
                    return dts;
                }
            }

            public List<DateTime> InputDateTime
            {
                get
                {
                    var dts = new List<DateTime>();
                    dts.AddRange(JyuInputDateTime);
                    dts.AddRange(AnmaInputDateTime);
                    dts.AddRange(HariKyuInputDateTime);
                    dts.AddRange(OtherInputDateTime);
                    return dts;
                }
            }

            public List<DateTime> VerifyDateTime
            {
                get
                {
                    var dts = new List<DateTime>();
                    dts.AddRange(JyuVerifyDateTime);
                    dts.AddRange(AnmaInputDateTime);
                    dts.AddRange(HariKyuInputDateTime);
                    dts.AddRange(OtherVerifyDateTime);
                    return dts;
                }
            }

            public List<DateTime> JyuInputDateTime = new List<DateTime>();
            public List<DateTime> JyuVerifyDateTime = new List<DateTime>();
            public List<DateTime> AnmaInputDateTime = new List<DateTime>();
            public List<DateTime> HariKyuInputDateTime = new List<DateTime>();
            public List<DateTime> AnmaVerifyDateTime = new List<DateTime>();
            public List<DateTime> HariKyuVerifyDateTime = new List<DateTime>();
            public List<DateTime> OtherInputDateTime = new List<DateTime>();
            public List<DateTime> OtherVerifyDateTime = new List<DateTime>();

            private string getTimeSpan(List<DateTime> dts)
            {
                var span = new TimeSpan();
                if (dts.Count == 0) return string.Empty;
                int lastSpanCode = -1;

                dts.Sort();

                foreach (var item in dts)
                {
                    var spanCode = (item.Hour * 60 + item.Minute) / 15;
                    if (lastSpanCode != spanCode)
                    {
                        span = span.Add(Span15);
                        lastSpanCode = spanCode;
                    }
                }

                return span.TotalMinutes.ToString("0");
            }

            public int TotalMiss => InputMiss + VerifyMiss;
            public int InputMiss => JyuInputMiss + AnmaInputMiss + HariKyuInputMiss + OtherInputMiss;
            public int VerifyMiss => JyuVerifyMiss + AnmaVerifyMiss + HariKyuVerifyMiss + OtherVerifyMiss;
            public int JyuInputMiss { get; set; }
            public int JyuVerifyMiss { get; set; }
            public int AnmaInputMiss { get; set; }
            public int AnmaVerifyMiss { get; set; }
            public int HariKyuInputMiss { get; set; }
            public int HariKyuVerifyMiss { get; set; }
            public int OtherInputMiss { get; set; }
            public int OtherVerifyMiss { get; set; }
        }


        public static List<AggregateLog> GetLogWithSpan(DateTime startDt, DateTime endDt)
        {
            endDt = endDt.AddDays(1);
            var where = $"inputdt BETWEEN '{ startDt.ToString("yyyy-MM-dd")}' AND '{ endDt.ToString("yyyy-MM-dd")}'";
            var l = db.Select<InputLog>(where).ToList();
            l.Sort((x, y) =>
                x.UID != y.UID ? x.UID.CompareTo(y.UID) :
                x.InsurerID != y.InsurerID ? x.InsurerID.CompareTo(y.InsurerID) :
                x.InputDT.CompareTo(y.InputDT));

            var al = new List<AggregateLog>();
            foreach (var item in l)
            {
                var hour = new DateTime(item.InputDT.Year, item.InputDT.Month, item.InputDT.Day, item.InputDT.Hour, 0, 0);

                var log = al.FirstOrDefault(x => x.UID == item.UID &&
                   x.InputDt == hour && x.InsurerID == item.InsurerID);

                if (log == null)
                {
                    log = new AggregateLog();
                    log.InsurerID = item.InsurerID;
                    log.UID = item.UID;
                    log.InputDt = hour;
                    al.Add(log);
                }

                if (item.AppType < 0)
                {
                    if (item.InputType == INPUT_TYPE.First)
                    {
                        log.OtherInputCount++;
                        log.OtherInputMiss += item.MissCount;
                        log.OtherInputDateTime.Add(item.InputDT);
                    }
                    else if (item.InputType == INPUT_TYPE.Second)
                    {
                        log.OtherVerifyCount++;
                        log.OtherVerifyMiss += item.MissCount;
                        log.OtherVerifyDateTime.Add(item.InputDT);
                    }
                }
                else if (item.AppType == APP_TYPE.柔整)
                {
                    if (item.InputType == INPUT_TYPE.First)
                    {
                        log.JyuInputCount++;
                        log.JyuInputMiss += item.MissCount;
                        log.JyuInputDateTime.Add(item.InputDT);
                    }
                    else if (item.InputType == INPUT_TYPE.Second)
                    {
                        log.JyuVerifyCount++;
                        log.JyuVerifyMiss += item.MissCount;
                        log.JyuVerifyDateTime.Add(item.InputDT);
                    }
                }
                else if (item.AppType == APP_TYPE.あんま)
                {
                    if (item.InputType == INPUT_TYPE.First)
                    {
                        log.AnmaInputCount++;
                        log.AnmaInputMiss += item.MissCount;
                        log.AnmaInputDateTime.Add(item.InputDT);
                    }
                    else if (item.InputType == INPUT_TYPE.Second)
                    {
                        log.AnmaVerifyCount++;
                        log.AnmaVerifyMiss += item.MissCount;
                        log.AnmaVerifyDateTime.Add(item.InputDT);
                    }
                }
                else if (item.AppType == APP_TYPE.鍼灸)
                {
                    if (item.InputType == INPUT_TYPE.First)
                    {
                        log.HariKyuInputCount++;
                        log.HariKyuInputMiss += item.MissCount;
                        log.HariKyuInputDateTime.Add(item.InputDT);
                    }
                    else if (item.InputType == INPUT_TYPE.Second)
                    {
                        log.HariKyuVerifyCount++;
                        log.HariKyuVerifyMiss += item.MissCount;
                        log.HariKyuVerifyDateTime.Add(item.InputDT);
                    }
                }
            }

            al.Sort((x, y) =>
            {
                if (x.UID != y.UID) return x.UID.CompareTo(y.UID);
                if (x.InputDt != y.InputDt) return (x.InputDt ?? DateTime.MinValue).CompareTo(y.InputDt ?? DateTime.MinValue);
                return (x.InsurerID ?? 0).CompareTo(y.InsurerID ?? 0);
            });

            return al;
        }


    }
}
