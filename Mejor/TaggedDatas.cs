﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    /// <summary>
    /// データベース上に一般的でない情報をテキストで記述して保存するクラス
    /// </summary>
    public class TaggedDatas
    {
        public TaggedDatas(string dbStr)
        {
            parseProperties(dbStr);
        }       
        private string createValue(string key, string val)
        {
            return $"{key}:\"{val.Replace("\"", "\"\"")}\"";
        }

        private void parseProperties(string s)
        {
            string key = string.Empty;
            string val = string.Empty;

            int sIndex = 0, eIndex = 0;
            if (s.Length == 0) return;

            while (true)
            {
                //キー部分取り出し
                eIndex = s.IndexOf(":", sIndex);

                //20201009151907 furukawa st ////////////////////////
                //取り出し範囲が開始終了逆転したら抜ける
                
                if (eIndex < sIndex) return;
                //20201009151907 furukawa ed ////////////////////////


                key = s.Substring(sIndex, eIndex - sIndex);

                //値部分取り出し　全体をダブルクオーテーションで囲っている
                sIndex = eIndex + 2;
                eIndex = sIndex;
                while (true)
                {
                    eIndex = s.IndexOf("\"", eIndex);
                    if (eIndex < 0) throw new ApplicationException("ダブルクオーテーションが不正です");
                    if (eIndex + 1 == s.Length || s[eIndex + 1] != '"')
                    {
                        val = s.Substring(sIndex, eIndex - sIndex).Replace("\"\"", "\"");
                        break;
                    }
                    eIndex += 2;
                }
                if (key == nameof(Dates)) Dates = val;
                if (key == nameof(RelationAID)) RelationAID = int.Parse(val);
                if (key == nameof(KouhiNum)) KouhiNum = val;
                if (key == nameof(JukyuNum)) JukyuNum = val;
                if (key == nameof(DestName)) DestName = val;
                if (key == nameof(DestZip)) DestZip = val;
                if (key == nameof(DestAdd)) DestAdd = val;
                if (key == nameof(ContactDt)) ContactDt = DateTime.Parse(val);
                if (key == nameof(ContactName)) ContactName = val;
                if (key == nameof(ContactStatus)) ContactStatus = (ContactStatus)int.Parse(val);
                if (key == nameof(Kana)) Kana = val;
                if (key == nameof(DestKana)) DestKana = val;
                if (key == nameof(InsName)) InsName = val;
                if (key == nameof(AppDistance)) AppDistance = double.Parse(val);
                if (key == nameof(DistCalcAdd)) DistCalcAdd = val;
                if (key == nameof(DistCalcClinicAdd)) DistCalcClinicAdd = val;
                if (key == nameof(CalcDistance)) CalcDistance = double.Parse(val);

                //20200911162940 furukawa st ////////////////////////
                //同意日がある場合のみ取得
                
                if (key == nameof(DouiDate))
                {
                    if (DateTime.TryParse(val, out DateTime tmpDouiDate)) DouiDate = tmpDouiDate;
                }

                //if (key == nameof(DouiDate)) DouiDate = DateTime.Parse(val);
                //20200911162940 furukawa ed ////////////////////////


                //20190312164440 furukawa st ////////////////////////
                //大阪広域　施術報告書　前回支給年月追加
                if (key == nameof(PastSupplyYM)) PastSupplyYM = val;
                //20190312164440 furukawa ed ////////////////////////


                //20190425123412 furukawa st ////////////////////////
                //交付料有無フラグ、施術同意書フラグ
                if (key == nameof(flgKofuUmu)) flgKofuUmu = bool.Parse(val);
                if (key == nameof(flgSejutuDouiUmu)) flgSejutuDouiUmu = bool.Parse(val);
                //20190425123412 furukawa ed ////////////////////////


                //20190508151939 furukawa 汎用カウンタ
                if (key == nameof(count)) count= int.Parse(val);


                //20191004095317 furukawa  汎用テキストフィールド１追加ロード処理                
                if (key == nameof(GeneralString1)) GeneralString1 = val;

                //20200608170941 furukawa st ////////////////////////
                //宮城県国保等例外用フィールドのロード
                
                if (key == nameof(GeneralString2)) GeneralString2 = val;
                if (key == nameof(GeneralString3)) GeneralString3 = val;
                if (key == nameof(GeneralString4)) GeneralString4 = val;
                if (key == nameof(GeneralString5)) GeneralString5 = val;
                if (key == nameof(GeneralString6)) GeneralString6 = val;
                if (key == nameof(GeneralString7)) GeneralString7 = val;
                if (key == nameof(GeneralString8)) GeneralString8 = val;
                //20200608170941 furukawa ed ////////////////////////



                sIndex = s.IndexOf("|", eIndex);
                if (sIndex < 0) break;
                sIndex++;
            }
        }

        public string CreateDbStr()
        {
            var l = new List<string>();
            if (!string.IsNullOrWhiteSpace(Dates)) l.Add(createValue(nameof(Dates), Dates));
            if (RelationAID != 0) l.Add(createValue(nameof(RelationAID), RelationAID.ToString()));
            if (!string.IsNullOrWhiteSpace(KouhiNum)) l.Add(createValue(nameof(KouhiNum), KouhiNum));
            if (!string.IsNullOrWhiteSpace(JukyuNum)) l.Add(createValue(nameof(JukyuNum), JukyuNum));
            if (!string.IsNullOrWhiteSpace(DestName)) l.Add(createValue(nameof(DestName), DestName));
            if (!string.IsNullOrWhiteSpace(DestZip)) l.Add(createValue(nameof(DestZip), DestZip));
            if (!string.IsNullOrWhiteSpace(DestAdd)) l.Add(createValue(nameof(DestAdd), DestAdd));
            if (!ContactDt.IsNullDate()) l.Add(createValue(nameof(ContactDt), ContactDt.ToString("yyyy/MM/dd HH:mm:ss")));
            if (!string.IsNullOrWhiteSpace(ContactName)) l.Add(createValue(nameof(ContactName), ContactName));
            if (ContactStatus != ContactStatus.Null) l.Add(createValue(nameof(ContactStatus), ((int)ContactStatus).ToString()));
            if (!string.IsNullOrWhiteSpace(Kana)) l.Add(createValue(nameof(Kana), Kana));
            if (!string.IsNullOrWhiteSpace(DestKana)) l.Add(createValue(nameof(DestKana), DestKana));
            if (!string.IsNullOrWhiteSpace(InsName)) l.Add(createValue(nameof(InsName), InsName));
            if (AppDistance != 0) l.Add(createValue(nameof(AppDistance), AppDistance.ToString()));
            if (!string.IsNullOrWhiteSpace(DistCalcAdd)) l.Add(createValue(nameof(DistCalcAdd), DistCalcAdd));
            if (!string.IsNullOrWhiteSpace(DistCalcClinicAdd)) l.Add(createValue(nameof(DistCalcClinicAdd), DistCalcClinicAdd));
            if (CalcDistance != 0) l.Add(createValue(nameof(CalcDistance), CalcDistance.ToString()));
            if (DouiDate != DateTimeEx.DateTimeNull) l.Add(createValue(nameof(DouiDate), DouiDate.ToString("yyyy/MM/dd")));


            //20190312164124 furukawa st ////////////////////////
            //大阪広域　施術報告書　前回支給年月追加

            if (PastSupplyYM != string.Empty) l.Add(createValue(nameof(PastSupplyYM), PastSupplyYM.ToString()));
            //20190312164124 furukawa ed ////////////////////////

            //20190412101810 furukawa st ////////////////////////
            //施術同意書有無フラグ値作成
            l.Add(createValue(nameof(flgSejutuDouiUmu),flgSejutuDouiUmu.ToString()));
            //20190412101810 furukawa ed ////////////////////////

            //20190422101319 furukawa st ////////////////////////
            //交付料有無フラグ追加
            l.Add(createValue(nameof(flgKofuUmu), flgKofuUmu.ToString()));
            //20190422101319 furukawa ed ////////////////////////




           
            //20190508172237 furukawa st ////////////////////////
            //資格フラグは、ある場合のみ立てる


            if (flgSikaku_BirthNotMatch) l.Add(createValue(nameof(flgSikaku_BirthNotMatch), flgSikaku_BirthNotMatch.ToString()));
            if (flgSikaku_SikakuNotMatch) l.Add(createValue(nameof(flgSikaku_SikakuNotMatch), flgSikaku_SikakuNotMatch.ToString()));
            if (flgSikaku_GenderNotMatch) l.Add(createValue(nameof(flgSikaku_GenderNotMatch), flgSikaku_GenderNotMatch.ToString()));
            if (flgSikaku_RatioNotMatch) l.Add(createValue(nameof(flgSikaku_RatioNotMatch), flgSikaku_RatioNotMatch.ToString()));
            if (flgSikaku_RangeNotMatch) l.Add(createValue(nameof(flgSikaku_RangeNotMatch), flgSikaku_RangeNotMatch.ToString()));
            if (flgSikaku_SinryoBeforeIssue) l.Add(createValue(nameof(flgSikaku_SinryoBeforeIssue), flgSikaku_SinryoBeforeIssue.ToString()));



                //20190502114922 furukawa st ////////////////////////
                //資格点検用フラグ設定

                //l.Add(createValue(nameof(flgSikaku_BirthNotMatch), flgSikaku_BirthNotMatch.ToString()));
                //l.Add(createValue(nameof(flgSikaku_SikakuNotMatch), flgSikaku_SikakuNotMatch.ToString()));
                //l.Add(createValue(nameof(flgSikaku_GenderNotMatch), flgSikaku_GenderNotMatch.ToString()));
                //l.Add(createValue(nameof(flgSikaku_RatioNotMatch), flgSikaku_RatioNotMatch.ToString()));
                //l.Add(createValue(nameof(flgSikaku_RangeNotMatch), flgSikaku_RangeNotMatch.ToString()));
                //l.Add(createValue(nameof(flgSikaku_SinryoBeforeIssue), flgSikaku_SinryoBeforeIssue.ToString()));


                //20190502114922 furukawa ed ////////////////////////


            //20190508172237 furukawa ed ////////////////////////





            //20190508151817 furukawa 汎用カウンタ
            l.Add(createValue(nameof(count), count.ToString()));

            //20191004095156 furukawa 汎用テキストフィールド１追加            
            if (GeneralString1 != string.Empty) l.Add(createValue(nameof(GeneralString1), GeneralString1.ToString()));


            //20200608180416 furukawa st ////////////////////////
            //宮城県国保等例外用フィールドの更新文字列
            
            if (GeneralString2 != string.Empty) l.Add(createValue(nameof(GeneralString2), GeneralString2.ToString()));
            if (GeneralString3 != string.Empty) l.Add(createValue(nameof(GeneralString3), GeneralString3.ToString()));
            if (GeneralString4 != string.Empty) l.Add(createValue(nameof(GeneralString4), GeneralString4.ToString()));
            if (GeneralString5 != string.Empty) l.Add(createValue(nameof(GeneralString5), GeneralString5.ToString()));
            if (GeneralString6 != string.Empty) l.Add(createValue(nameof(GeneralString6), GeneralString6.ToString()));
            if (GeneralString7 != string.Empty) l.Add(createValue(nameof(GeneralString7), GeneralString7.ToString()));
            if (GeneralString8 != string.Empty) l.Add(createValue(nameof(GeneralString8), GeneralString8.ToString()));
            //20200608180416 furukawa ed ////////////////////////


            return string.Join("|", l);
        }

        public string Dates { get; set; } = string.Empty;
        public int RelationAID { get; set; } = 0;
        public string KouhiNum { get; set; } = string.Empty;
        public string JukyuNum { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdd { get; set; } = string.Empty;
        public DateTime ContactDt { get; set; } = DateTimeEx.DateTimeNull;
        public string ContactName { get; set; } = string.Empty;
        public ContactStatus ContactStatus { get; set; } = ContactStatus.Null;

        public void ContactStatusSet(ContactStatus flag) => ContactStatus |= flag;
        public void ContactStatusRemove(ContactStatus flag) => ContactStatus &= ~flag;
        public bool ContactStatusCheck(ContactStatus flag) => (ContactStatus & flag) == flag;

        public string Kana { get; set; } = string.Empty;
        public string DestKana { get; set; } = string.Empty;
        public string InsName { get; set; } = string.Empty;

        public double AppDistance { get; set; } = 0;

        public string DistCalcAdd { get; set; } = string.Empty;
        public string DistCalcClinicAdd { get; set; } = string.Empty;
        public double CalcDistance { get; set; } = 0;

        public DateTime DouiDate { get; set; } = DateTimeEx.DateTimeNull;

        //20190312164523 furukawa st ////////////////////////
        //大阪広域　施術報告書　前回支給年月追加
        /// <summary>
        /// yyyyMM型にしておこう
        /// </summary>
        public string  PastSupplyYM { get; set; } = string.Empty;
        //20190312164523 furukawa ed ////////////////////////


        //20190412101713 furukawa st ////////////////////////
        //施術同意書有無フラグ追加
        public bool flgSejutuDouiUmu { get; set; } = false;
        //20190412101713 furukawa ed ////////////////////////

        //20190422101151 furukawa st ////////////////////////
        //交付料有無フラグ追加
        public bool flgKofuUmu { get; set; } = false;
        //20190422101151 furukawa ed ////////////////////////


        //20190502115118 furukawa 資格点検用フラグ
        public bool flgSikaku_SikakuNotMatch { get; set; } = false;
        public bool flgSikaku_BirthNotMatch { get; set; } = false;
        public bool flgSikaku_GenderNotMatch { get; set; } = false;
        public bool flgSikaku_RatioNotMatch { get; set; } = false;
        public bool flgSikaku_RangeNotMatch { get; set; } = false;
        public bool flgSikaku_SinryoBeforeIssue { get; set; } = false;



        //20190508151643 furukawa 汎用カウンタ
        public int count { get; set; } = 0;


        //20191004095041 furukawa 汎用テキストフィールド１追加        
        public string GeneralString1 { get; set; } = string.Empty;

        //20200608170811 furukawa st ////////////////////////
        //宮城県国保等例外用フィールド追加
        
        public string GeneralString2 { get; set; } = string.Empty;
        public string GeneralString3 { get; set; } = string.Empty;
        public string GeneralString4 { get; set; } = string.Empty;
        public string GeneralString5 { get; set; } = string.Empty;
        public string GeneralString6 { get; set; } = string.Empty;
        public string GeneralString7 { get; set; } = string.Empty;
        public string GeneralString8 { get; set; } = string.Empty;
        //20200608170811 furukawa ed ////////////////////////



        public string ContactStatusStr
        {
            get
            {
                var l = new List<string>();
                if (ContactStatusCheck(ContactStatus.団体)) l.Add("団体");
                if (ContactStatusCheck(ContactStatus.連絡不要)) l.Add("連絡不要");
                return string.Join(" ", l);
            }
        }
    }
}
