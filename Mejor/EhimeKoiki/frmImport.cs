﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.EhimeKoiki
{
    public partial class frmImport : Form
    {

        public string strFileAHKClinic { get; set; } = string.Empty;
        public string strFileAHKNyukyo { get; set; } = string.Empty;
        public string strFileAHKHiho { get; set; } = string.Empty;
        public string strFileAHK{ get; set; } = string.Empty;
        //20220905_1 ito st /////
        public string strFileJyuClinic { get; set; } = string.Empty;
        public string strFileJyuNyukyo { get; set; } = string.Empty;
        public string strFileJYU { get; set; } = string.Empty;
        //20220905_1 ito ed /////

        private static int _cym;

        public frmImport(int cym)
        {
            InitializeComponent();
            _cym = cym;

        }

        private void btnImp_Click(object sender, EventArgs e)
        {
            //20220905_1 ito ed /////
            strFileJyuClinic = textBoxJyuClnic.Text.Trim();
            strFileJyuNyukyo = textBoxJyuNyukyo.Text.Trim();
            strFileJYU = textBoxJYU.Text.Trim();
            //20220905_1 ito ed /////
            strFileAHKClinic = textBoxAHkClnic.Text.Trim();
            strFileAHKNyukyo = textBoxAHkNyukyo.Text.Trim();
            strFileAHK = textBoxAHK.Text.Trim();
            strFileAHKHiho = textBoxAHKHiho.Text.Trim();

            this.Close();
        }

        //20220905_1 ito st /////
        private void btnJyuClinic_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "柔整医療機関データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxJyuClnic.Text = ofd.FileName;
        }

        private void btnJyuNyukyo_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "柔整入居証明書・居住地現況届提出一覧";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxJyuNyukyo.Text = ofd.FileName;
        }

        private void btnJYU_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "柔整データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxJYU.Text = ofd.FileName;
        }
        //20220905_1 ito ed /////

        private void btnAHKClinic_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "あはき医療機関データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHkClnic.Text = ofd.FileName;
        }

        private void btnAHKNyukyo_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "あはき入居証明書・居住地現況届提出一覧";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHkNyukyo.Text = ofd.FileName;
        }

        private void btnAHK_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv";
            ofd.FilterIndex = 0;
            ofd.Title = "あはきデータ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHK.Text = ofd.FileName;
        }

        private void btnAHKHiho_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "鍼灸あんま用被保険者マスタ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHKHiho.Text = ofd.FileName;
        }

    }
}
