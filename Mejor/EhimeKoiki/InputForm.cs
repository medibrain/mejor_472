﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;
using NPOI.XWPF.UserModel;

namespace Mejor.EhimeKoiki

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        List<App> list = new List<App>();

        int cym=0;
        private InputMode inputMode;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 提供データ
        /// </summary>
        //20220906_1 ito st /////
        List<imp_jyu> lstImport_jyu = new List<imp_jyu>();
        List<imp_ahk> lstImport_ahk = new List<imp_ahk>();
        //List<imp_ahk> lstImport = new List<imp_ahk>();
        //20220906_1 ito end /////

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            GroupingControl();
          
            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            InitAppGrid();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //20220906_1 ito st /////
                //if (scan.AppType != APP_TYPE.あんま && scan.AppType != APP_TYPE.鍼灸)
                //{
                //    System.Windows.Forms.MessageBox.Show("あはきのみ入力できます",
                //        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    dataGridViewPlist.Visible = false;
                //    verifyBoxY.Enabled = false;
                //    return;
                //}
                //20220906_1 ito end /////

                //提供データ取得
                //20220906_1 ito st /////
                if (scan.AppType == APP_TYPE.柔整)
                {
                    lstImport_jyu = imp_jyu.SelectAll(app.CYM);
                }
                else
                {
                    lstImport_ahk = imp_ahk.SelectAll(app.CYM);
                }
                //lstImport = imp_ahk.SelectAll(app.CYM);
                //20220906_1 ito end /////
                setApp(app);
            }        

            focusBack(false);

        }


        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            GroupingControl();


            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            InitAppGrid();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }
        #endregion
        
        private void InitAppGrid()
        {
            #region 左リスト
            
         
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;

            #endregion
        }

        /// <summary>
        /// 座標移動用コントロールグルーピング
        /// </summary>
        private void GroupingControl()
        {
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, };

            //合計、往療
            totalConts = new Control[] { verifyBoxTotal, checkBoxVisit, checkBoxVisitKasan, };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, };

        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                    MatchingApp.GetNotMatchCsv(cym); //20220909_1 ito st /////マッチングチェック追加
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }


        #region オブジェクトイベント
        private void clearApp()
        {
            //画像クリア
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            //情報クリア
            iVerifiableAllClear(panelRight);
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //bsRefReces.Clear();
            //bsRefReces.ResetBindings(false);
        }


        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            if (app == null)
            {
                clearApp();
                return;
            }

            setApp(app);
            if (app.StatusFlagCheck(StatusFlag.自動マッチ済)) verifyBoxNewCont.Focus();
            else verifyBoxY.Focus();
            //focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {

            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }

            verifyBoxY.Focus();
            //focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            panelTotal.Visible = false;
            verifyBoxBui.Visible = false;
            labelBui.Visible = false;
            checkBoxVisitKasan.Visible = false;
            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                case clsInputKind.施術同意書:// "901"://施術同意書             
                    break;
                    
                default:                    
                    phnum.Visible = true;
                    panelTotal.Visible = true;
                    if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                    {
                        checkBoxVisitKasan.Visible = true;
                    }
                    else
                    {
                        labelBui.Visible = true;
                        verifyBoxBui.Visible = true;
                    }
                    break;
            }
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid_ahk()
        {
            dgv.Columns[nameof(imp_ahk.f000_importid)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f001_hihonum)].Width = 80;
            dgv.Columns[nameof(imp_ahk.f002_insnum)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f003_uketsukedate)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f004_shinseiname)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f005_shinseizip)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f006_shinseiadd)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f007_shinseizokugara)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f008_shinseitel)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f009_chargeym)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f010_mediym)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f011_startdate1)].Width = 100;
            dgv.Columns[nameof(imp_ahk.f012_finishdate1)].Width = 100;
            dgv.Columns[nameof(imp_ahk.f013_pref)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f014_tensu)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f015_cliniccity)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f016_clinicnumber)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f017_ratiokbn)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f018_ratio)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f019_counteddays)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f020_total)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f021_charge)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f022_partial)].Width = 60;
            dgv.Columns[nameof(imp_ahk.f023_ryouyuhikbn)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f024_bankkbn)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f025_bankcode)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f026_banktenpo)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f027_bankyokin)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f028_accountnum)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f029_accountname)].Width = 40;
            dgv.Columns[nameof(imp_ahk.f030_losedate)].Width = 40;
            dgv.Columns[nameof(imp_ahk.cym)].Width = 60;
            dgv.Columns[nameof(imp_ahk.clinicnum)].Width = 100;
            dgv.Columns[nameof(imp_ahk.uketsukedatead)].Width = 100;
            dgv.Columns[nameof(imp_ahk.startdate1ad)].Width = 100;
            dgv.Columns[nameof(imp_ahk.finishdate1ad)].Width = 100;
            dgv.Columns[nameof(imp_ahk.losedatead)].Width = 100;

            dgv.Columns[nameof(imp_ahk.f000_importid)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f001_hihonum)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f002_insnum)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f003_uketsukedate)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f004_shinseiname)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f005_shinseizip)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f006_shinseiadd)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f007_shinseizokugara)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f008_shinseitel)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f009_chargeym)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f010_mediym)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f011_startdate1)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f012_finishdate1)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f013_pref)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f014_tensu)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f015_cliniccity)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f016_clinicnumber)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f017_ratiokbn)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f018_ratio)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f019_counteddays)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f020_total)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f021_charge)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f022_partial)].Visible = true;
            dgv.Columns[nameof(imp_ahk.f023_ryouyuhikbn)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f024_bankkbn)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f025_bankcode)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f026_banktenpo)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f027_bankyokin)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f028_accountnum)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f029_accountname)].Visible = false;
            dgv.Columns[nameof(imp_ahk.f030_losedate)].Visible = false;
            dgv.Columns[nameof(imp_ahk.cym)].Visible = true;
            dgv.Columns[nameof(imp_ahk.clinicnum)].Visible = true;
            dgv.Columns[nameof(imp_ahk.uketsukedatead)].Visible = true;
            dgv.Columns[nameof(imp_ahk.startdate1ad)].Visible = true;
            dgv.Columns[nameof(imp_ahk.finishdate1ad)].Visible = true;
            dgv.Columns[nameof(imp_ahk.losedatead)].Visible = false;

            dgv.Columns[nameof(imp_ahk.f000_importid)].HeaderText = "インポートID";
            dgv.Columns[nameof(imp_ahk.f001_hihonum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(imp_ahk.f002_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(imp_ahk.f003_uketsukedate)].HeaderText = "受付年月日yyyymmdd";
            dgv.Columns[nameof(imp_ahk.f004_shinseiname)].HeaderText = "申請者氏名（漢字）";
            dgv.Columns[nameof(imp_ahk.f005_shinseizip)].HeaderText = "申請者郵便番号";
            dgv.Columns[nameof(imp_ahk.f006_shinseiadd)].HeaderText = "申請者住所（漢字）";
            dgv.Columns[nameof(imp_ahk.f007_shinseizokugara)].HeaderText = "申請者被保険者との関係";
            dgv.Columns[nameof(imp_ahk.f008_shinseitel)].HeaderText = "申請者電話番号";
            dgv.Columns[nameof(imp_ahk.f009_chargeym)].HeaderText = "請求年月yyyymm";
            dgv.Columns[nameof(imp_ahk.f010_mediym)].HeaderText = "診療年月yyyymm";
            dgv.Columns[nameof(imp_ahk.f011_startdate1)].HeaderText = "診療開始年月日yyyymmdd";
            dgv.Columns[nameof(imp_ahk.f012_finishdate1)].HeaderText = "診療終了年月日yyyymmdd";
            dgv.Columns[nameof(imp_ahk.f013_pref)].HeaderText = "都道府県コード";
            dgv.Columns[nameof(imp_ahk.f014_tensu)].HeaderText = "点数表コード";
            dgv.Columns[nameof(imp_ahk.f015_cliniccity)].HeaderText = "医療機関市区町村コード";
            dgv.Columns[nameof(imp_ahk.f016_clinicnumber)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(imp_ahk.f017_ratiokbn)].HeaderText = "給付区分コード";
            dgv.Columns[nameof(imp_ahk.f018_ratio)].HeaderText = "給付割合%";
            dgv.Columns[nameof(imp_ahk.f019_counteddays)].HeaderText = "実日数";
            dgv.Columns[nameof(imp_ahk.f020_total)].HeaderText = "費用金額";
            dgv.Columns[nameof(imp_ahk.f021_charge)].HeaderText = "保険者負担額";
            dgv.Columns[nameof(imp_ahk.f022_partial)].HeaderText = "一部負担相当額";
            dgv.Columns[nameof(imp_ahk.f023_ryouyuhikbn)].HeaderText = "療養費区分コード";
            dgv.Columns[nameof(imp_ahk.f024_bankkbn)].HeaderText = "金融機関区分コード";
            dgv.Columns[nameof(imp_ahk.f025_bankcode)].HeaderText = "金融機関コード";
            dgv.Columns[nameof(imp_ahk.f026_banktenpo)].HeaderText = "金融機関店舗コード";
            dgv.Columns[nameof(imp_ahk.f027_bankyokin)].HeaderText = "預金種別コード";
            dgv.Columns[nameof(imp_ahk.f028_accountnum)].HeaderText = "口座番号";
            dgv.Columns[nameof(imp_ahk.f029_accountname)].HeaderText = "口座名義人氏名（カナ）";
            dgv.Columns[nameof(imp_ahk.f030_losedate)].HeaderText = "喪失年月日yyyymmdd";
            dgv.Columns[nameof(imp_ahk.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(imp_ahk.clinicnum)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(imp_ahk.uketsukedatead)].HeaderText = "受付年月日西暦";
            dgv.Columns[nameof(imp_ahk.startdate1ad)].HeaderText = "診療開始日西暦";
            dgv.Columns[nameof(imp_ahk.finishdate1ad)].HeaderText = "診療終了日西暦";
            dgv.Columns[nameof(imp_ahk.losedatead)].HeaderText = "喪失日西暦";
        }


        private void InitGrid_jyu()
        {
            dgv.Columns[nameof(imp_jyu.f000_importid)].Width = 60;
            dgv.Columns[nameof(imp_jyu.f001_insnum)].Width = 80;//
            dgv.Columns[nameof(imp_jyu.f002_hihonum)].Width = 80;//
            dgv.Columns[nameof(imp_jyu.f003_hihokana)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f004_hihoname)].Width = 80;//
            dgv.Columns[nameof(imp_jyu.f005_hihozip)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f006_hihoadd)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f007_sym)].Width = 60;//
            dgv.Columns[nameof(imp_jyu.f008_clinicno)].Width = 80;//
            dgv.Columns[nameof(imp_jyu.f009_clinickana)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f010_clinicname)].Width = 120;//
            dgv.Columns[nameof(imp_jyu.f011_ratio)].Width = 40;//
            dgv.Columns[nameof(imp_jyu.f012_counteddays)].Width = 70;//
            dgv.Columns[nameof(imp_jyu.f013_total)].Width = 70;//
            dgv.Columns[nameof(imp_jyu.f014_charge)].Width = 70;//
            dgv.Columns[nameof(imp_jyu.f015_partial)].Width = 70;//
            dgv.Columns[nameof(imp_jyu.f016_futan)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f017_recekey)].Width = 40;
            dgv.Columns[nameof(imp_jyu.f018_losedate)].Width = 40;
            dgv.Columns[nameof(imp_jyu.cym)].Width = 60;
            dgv.Columns[nameof(imp_jyu.losedatead)].Width = 100;

            dgv.Columns[nameof(imp_jyu.f000_importid)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f001_insnum)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f002_hihonum)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f003_hihokana)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f004_hihoname)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f005_hihozip)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f006_hihoadd)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f007_sym)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f008_clinicno)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f009_clinickana)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f010_clinicname)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f011_ratio)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f012_counteddays)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f013_total)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f014_charge)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f015_partial)].Visible = true;
            dgv.Columns[nameof(imp_jyu.f016_futan)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f017_recekey)].Visible = false;
            dgv.Columns[nameof(imp_jyu.f018_losedate)].Visible = false;
            dgv.Columns[nameof(imp_jyu.cym)].Visible = true;
            dgv.Columns[nameof(imp_jyu.losedatead)].Visible = false;

            dgv.Columns[nameof(imp_jyu.f000_importid)].HeaderText = "インポートID";
            dgv.Columns[nameof(imp_jyu.f001_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(imp_jyu.f002_hihonum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(imp_jyu.f003_hihokana)].HeaderText = "氏名（カナ）";
            dgv.Columns[nameof(imp_jyu.f004_hihoname)].HeaderText = "氏名（漢字）";
            dgv.Columns[nameof(imp_jyu.f005_hihozip)].HeaderText = "被保険者郵便番号";
            dgv.Columns[nameof(imp_jyu.f006_hihoadd)].HeaderText = "被保険者住所";
            dgv.Columns[nameof(imp_jyu.f007_sym)].HeaderText = "診療年月";
            dgv.Columns[nameof(imp_jyu.f008_clinicno)].HeaderText = "医療機関番号";
            dgv.Columns[nameof(imp_jyu.f009_clinickana)].HeaderText = "医療機関名（カナ）";
            dgv.Columns[nameof(imp_jyu.f010_clinicname)].HeaderText = "医療機関名（漢字）";
            dgv.Columns[nameof(imp_jyu.f011_ratio)].HeaderText = "給付割合";
            dgv.Columns[nameof(imp_jyu.f012_counteddays)].HeaderText = "診療実日数";
            dgv.Columns[nameof(imp_jyu.f013_total)].HeaderText = "医療費";
            dgv.Columns[nameof(imp_jyu.f014_charge)].HeaderText = "保険者負担額";
            dgv.Columns[nameof(imp_jyu.f015_partial)].HeaderText = "一部負担相当額";
            dgv.Columns[nameof(imp_jyu.f016_futan)].HeaderText = "自己負担額";
            dgv.Columns[nameof(imp_jyu.f017_recekey)].HeaderText = "レセプト管理番号";
            dgv.Columns[nameof(imp_jyu.f018_losedate)].HeaderText = "喪失年月日";
            dgv.Columns[nameof(imp_jyu.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(imp_jyu.losedatead)].HeaderText = "喪失日西暦";
        }


        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid_ahk(string importid="")
        {
            List<imp_ahk> lstimp = new List<imp_ahk>();
            
            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            
            foreach (imp_ahk item in lstImport_ahk)
            {
                //診療年月、被保険者証記号/番号、合計金額で合致させる
                if (item.f010_mediym==intymad.ToString() && item.f001_hihonum==strhnum && int.Parse(item.f020_total)==intTotal)
                {
                    lstimp.Add(item);
                }
            }
            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            InitGrid_ahk();        

            if (importid != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    //20220629150709 furukawa st ////////////////////////
                    //グリッドのフィールド名が間違ってる                    
                    if (r.Cells[nameof(imp_ahk.f000_importid)].Value.ToString() == importid)
                    //  if (r.Cells["importid"].Value.ToString() == importid)
                    //20220629150709 furukawa ed ////////////////////////{
                    { 
                        r.Selected = true;                        
                    }
                    else
                    {
                        r.Selected = false;
                    }                    
                }
            }            
            //importidがなく（入力前）、マッチング条件で2行以上の場合、必ず選択させる(こっちで判定できない)為Selectedしない            
            else if (dgv.Rows.Count>1 && (importid == string.Empty))
            {
                foreach(DataGridViewRow r in dgv.Rows)
                {
                    r.Selected = false;
                }
            }            

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }       
            //マッチングOK条件に選択行が1行の場合も追加
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }      
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }
        }



        private void createGrid_jyu(string importid = "")
        {
            List<imp_jyu> lstimp = new List<imp_jyu>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

            foreach (imp_jyu item in lstImport_jyu)
            {
                //診療年月、被保険者証記号/番号、合計金額で合致させる
                if (item.f007_sym == intymad.ToString() && item.f002_hihonum == strhnum && int.Parse(item.f013_total) == intTotal)
                {
                    lstimp.Add(item);
                }
            }
            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            InitGrid_jyu();

            if (importid != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (r.Cells[nameof(imp_ahk.f000_importid)].Value.ToString() == importid)
                    {
                        r.Selected = true;
                    }
                    else
                    {
                        r.Selected = false;
                    }
                }
            }
            //importidがなく（入力前）、マッチング条件で2行以上の場合、必ず選択させる(こっちで判定できない)為Selectedしない            
            else if (dgv.Rows.Count > 1 && (importid == string.Empty))
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    r.Selected = false;
                }
            }

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            //マッチングOK条件に選択行が1行の場合も追加
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }
        }


        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            if (inputMode == InputMode.MatchCheck) scanGroup = ScanGroup.Select(app.GroupID); //20220908_2 ito st /////マッチングチェック時用

            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            phnum.Visible = false;
            panelTotal.Visible = false;            
            
            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            
            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {                    
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:

                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証記号番号
                    string strhnum = app.HihoNum; 
                    setValue(verifyBoxHnum, strhnum, firstTime, nv);

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                    //往療有無
                    setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);

                    //加算有無
                    setValue(checkBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);
           
                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);


                    //新規継続
                    if (app.NewContType == NEW_CONT.新規) setValue(verifyBoxNewCont, (int)NEW_CONT.新規, firstTime, nv);
                    if (app.NewContType == NEW_CONT.継続) setValue(verifyBoxNewCont, (int)NEW_CONT.継続, firstTime, nv);

                    //負傷数（柔整）
                    setValue(verifyBoxBui,app.TaggedDatas.count,firstTime,nv);


                    //グリッド選択する

                    //20220629150830 furukawa st ////////////////////////
                    //愛媛広域のグリッド選択のIDはComNumでなくRRID
                    //20220906_1 ito st /////
                    if (scan.AppType == APP_TYPE.柔整)
                    {
                        createGrid_jyu(app.RrID.ToString());
                    }
                    else
                    {
                        createGrid_ahk(app.RrID.ToString());
                    }
                    //createGrid(app.RrID.ToString());
                    //20220906_1 ito end /////
                    //  createGrid(app.ComNum.ToString());
                    //20220629150830 furukawa ed ////////////////////////

                    break;
            }
        }


        #endregion

        #region 各種更新


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);
                                    
                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //新規継続
                    int newcont = verifyBoxNewCont.GetIntValue();
                    setStatus(verifyBoxNewCont, !new int[] { 1, 2 }.Contains(newcont));

                    //負傷数
                    int fushocount = verifyBoxBui.GetIntValue();
                    if (scan.AppType == APP_TYPE.柔整) setStatus(verifyBoxBui, fushocount > 5 || fushocount < 1);

                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月
                    app.HihoNum = hnumN;                //被保険者証記号番号
                    

                    //申請書種別
                    app.AppType = scan.AppType;

                    //初検日
                    app.FushoFirstDate1 = f1FirstDt;    //初検日
                                        
                    //往療有無
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;

                    //加算有無
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;

                    //合計
                    app.Total = total;

                    //新規継続
                    if (newcont == 1) app.NewContType = NEW_CONT.新規;
                    if (newcont == 2) app.NewContType = NEW_CONT.継続;


                    //負傷数
                    app.TaggedDatas.count = fushocount;


                    if (dgv.Rows.Count > 0)
                    {

                        if (scan.AppType == APP_TYPE.柔整)
                        {
                            //ID
                            app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f000_importid)].Value.ToString());

                            //◆提供データ 鍼灸・マッサージ情報.csv
                            //請求年月=>cym                        
                            //診療年月=>入力
                            //診療実日数
                            app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f012_counteddays)].Value.ToString());
                            //費用金額
                            app.Total = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f013_total)].Value.ToString());
                            //保険者負担額
                            app.Charge = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f014_charge)].Value.ToString());
                            //一部負担相当額
                            app.Partial = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f015_partial)].Value.ToString());
                            //給付割合％
                            app.Ratio = int.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.f011_ratio)].Value.ToString()) / 10;
                            //保険者番号
                            app.InsNum = dgv.CurrentRow.Cells[nameof(imp_jyu.f001_insnum)].Value.ToString();
                            ////開始日
                            //app.FushoStartDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.startdate1ad)].Value.ToString());
                            ////終了日
                            //app.FushoFinishDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_jyu.finishdate1ad)].Value.ToString());
                            ////口座番号
                            //app.AccountNumber = dgv.CurrentRow.Cells[nameof(imp_jyu.f028_accountnum)].Value.ToString();
                            ////口座名義
                            //app.AccountKana = dgv.CurrentRow.Cells[nameof(imp_jyu.f029_accountname)].Value.ToString();
                            ////支店番号
                            //if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_jyu.f026_banktenpo)].Value.ToString(), out int tmpBankTempo)) app.BankBranchType = tmpBankTempo;
                            ////銀行コード
                            //if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_jyu.f025_bankcode)].Value.ToString(), out int tmpBank)) app.BankType = tmpBank;
                            ////預金種別
                            //if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_jyu.f027_bankyokin)].Value.ToString(), out int tmpBankYokin)) app.AccountType = tmpBankYokin;
                            //医療機関コード
                            string strClinicNum = dgv.CurrentRow.Cells[nameof(imp_jyu.f008_clinicno)].Value.ToString();
                            
                            //◆医療機関.csv
                            List<imp_jyu_clinic> lstClinic = imp_jyu_clinic.Select(app.CYM, strClinicNum);
                            if (lstClinic != null && lstClinic.Count > 0)
                            {
                                app.ClinicNum = lstClinic[0].f001_clinicnum;
                                app.ClinicName = lstClinic[0].f004_clinicname;
                                app.ClinicZip = lstClinic[0].f005_cliniczip;
                                app.ClinicAdd = lstClinic[0].f006_clinicadd;
                                app.ClinicTel = lstClinic[0].f002_clinictel;
                            }

                            //20220926_1 ito st //////
                            app.HihoName = dgv.CurrentRow.Cells[nameof(imp_jyu.f004_hihoname)].Value.ToString();
                            app.TaggedDatas.Kana = dgv.CurrentRow.Cells[nameof(imp_jyu.f003_hihokana)].Value.ToString();
                            app.HihoZip = dgv.CurrentRow.Cells[nameof(imp_jyu.f005_hihozip)].Value.ToString();
                            app.HihoAdd = dgv.CurrentRow.Cells[nameof(imp_jyu.f006_hihoadd)].Value.ToString();
                            app.TaggedDatas.GeneralString6 = dgv.CurrentRow.Cells[nameof(imp_jyu.f000_importid)].Value.ToString();
                            //20220926_1 ito end //////

                            ////◆鍼灸あんま用被保険者マスタ.xlsx
                            //List<imp_ahk_hihomaster> lstHiho = imp_ahk_hihomaster.select(app.CYM, app.HihoNum);
                            //if (lstHiho != null && lstHiho.Count > 0)
                            //{
                            //    app.HihoName = lstHiho[0].f003_hihoname;
                            //    app.TaggedDatas.Kana = lstHiho[0].f002_hihokana;
                            //    app.HihoZip = lstHiho[0].f004_hihozip;
                            //    app.HihoAdd = lstHiho[0].f005_hihoadd;
                            //    app.TaggedDatas.DestZip = lstHiho[0].f006_sendzip;
                            //    app.TaggedDatas.DestAdd = lstHiho[0].f007_sendadd;
                            //    app.TaggedDatas.DestName = lstHiho[0].f008_sendname;
                            //    app.TaggedDatas.GeneralString1 = lstHiho[0].f009_newzip;
                            //    app.TaggedDatas.GeneralString2 = lstHiho[0].f010_newadd;
                            //    app.TaggedDatas.GeneralString6 = lstHiho[0].f000_importid.ToString();
                            //}

                            //◆提供データ2 入居証明書.Excel
                            List<imp_jyu_nyukyo> lstNyukyo = imp_jyu_nyukyo.select(app.CYM, app.HihoNum);
                            if (lstNyukyo != null && lstNyukyo.Count > 0)
                            {
                                //医療機関名は医療機関マスタを優先
                                if (app.ClinicName == string.Empty) app.ClinicName = lstNyukyo[0].f002_clinicname;
                                app.TaggedDatas.InsName = lstNyukyo[0].f003_insurer;
                                app.Birthday = lstNyukyo[0].pbirthdayad;
                                app.TaggedDatas.GeneralString3 = lstNyukyo[0].f007_residence;
                                app.TaggedDatas.GeneralString4 = lstNyukyo[0].f008_residencename;
                            }

                            //◆提供データ2 居住地現況届提出一覧.Excel
                            List<imp_jyu_kyojuchi> lstkyojuchi = imp_jyu_kyojuchi.select(app.CYM, app.HihoNum);
                            if (lstkyojuchi != null && lstkyojuchi.Count > 0)
                            {
                                app.TaggedDatas.GeneralString5 = lstkyojuchi[0].f007_curradd;
                            }
                        }
                        else if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
                        {
                            app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f000_importid)].Value.ToString());           //ID
                            //マッチングなしを考慮し画面入力を優先させる
                            //app.HihoNum= $"{dgv.CurrentRow.Cells["hihomark"].Value.ToString()}-{dgv.CurrentRow.Cells["hihonum"].Value.ToString()}";      //被保険者証記号番号

                            //◆提供データ 鍼灸・マッサージ情報.csv
                            //請求年月=>cym                        
                            //診療年月=>入力
                            //診療実日数
                            app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f019_counteddays)].Value.ToString());
                            //費用金額
                            app.Total = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f020_total)].Value.ToString());
                            //保険者負担額
                            app.Charge = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f021_charge)].Value.ToString());
                            //一部負担相当額
                            app.Partial = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f022_partial)].Value.ToString());
                            //給付割合％
                            app.Ratio = int.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.f018_ratio)].Value.ToString()) / 10;
                            //保険者番号
                            app.InsNum = dgv.CurrentRow.Cells[nameof(imp_ahk.f002_insnum)].Value.ToString();
                            //開始日
                            app.FushoStartDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.startdate1ad)].Value.ToString());
                            //終了日
                            app.FushoFinishDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(imp_ahk.finishdate1ad)].Value.ToString());
                            //口座番号
                            app.AccountNumber = dgv.CurrentRow.Cells[nameof(imp_ahk.f028_accountnum)].Value.ToString();
                            //口座名義
                            app.AccountKana = dgv.CurrentRow.Cells[nameof(imp_ahk.f029_accountname)].Value.ToString();
                            //支店番号
                            if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ahk.f026_banktenpo)].Value.ToString(), out int tmpBankTempo)) app.BankBranchType = tmpBankTempo;
                            //銀行コード
                            if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ahk.f025_bankcode)].Value.ToString(), out int tmpBank)) app.BankType = tmpBank;
                            //預金種別
                            if (int.TryParse(dgv.CurrentRow.Cells[nameof(imp_ahk.f027_bankyokin)].Value.ToString(), out int tmpBankYokin)) app.AccountType = tmpBankYokin;
                            //医療機関コード
                            //20220629155429 furukawa st ////////////////////////
                            //組み合わせてある医療機関コードを使う                            
                            string strClinicNum = dgv.CurrentRow.Cells[nameof(imp_ahk.clinicnum)].Value.ToString();
                            //          string strClinicNum =
                            //              dgv.CurrentRow.Cells[nameof(imp_ahk.f013_pref)].Value.ToString() +
                            //              dgv.CurrentRow.Cells[nameof(imp_ahk.f014_tensu)].Value.ToString() +
                            //              dgv.CurrentRow.Cells[nameof(imp_ahk.f015_cliniccity)].Value.ToString() +
                            //              dgv.CurrentRow.Cells[nameof(imp_ahk.f016_clinicnumber)].Value.ToString();
                            //20220629155429 furukawa ed ////////////////////////

                            //◆医療機関.csv
                            List<imp_ahk_clinic> lstClinic = imp_ahk_clinic.Select(app.CYM, strClinicNum);
                            if (lstClinic != null && lstClinic.Count > 0)
                            {
                                //20220629154649 furukawa st ////////////////////////
                                //医療機関コードの登録が漏れてた
                                app.ClinicNum = lstClinic[0].f001_clinicnum;
                                //20220629154649 furukawa ed ////////////////////////
                                app.ClinicName = lstClinic[0].f004_clinicname;
                                app.ClinicZip = lstClinic[0].f005_cliniczip;
                                app.ClinicAdd = lstClinic[0].f006_clinicadd;
                                app.ClinicTel = lstClinic[0].f002_clinictel;
                            }

                            //◆鍼灸あんま用被保険者マスタ.xlsx
                            List<imp_ahk_hihomaster> lstHiho = imp_ahk_hihomaster.select(app.CYM, app.HihoNum);
                            if (lstHiho != null && lstHiho.Count > 0)
                            {
                                app.HihoName = lstHiho[0].f003_hihoname;
                                app.TaggedDatas.Kana = lstHiho[0].f002_hihokana;
                                app.HihoZip = lstHiho[0].f004_hihozip;
                                app.HihoAdd = lstHiho[0].f005_hihoadd;
                                app.TaggedDatas.DestZip = lstHiho[0].f006_sendzip;
                                app.TaggedDatas.DestAdd = lstHiho[0].f007_sendadd;
                                app.TaggedDatas.DestName = lstHiho[0].f008_sendname;
                                app.TaggedDatas.GeneralString1 = lstHiho[0].f009_newzip;
                                app.TaggedDatas.GeneralString2 = lstHiho[0].f010_newadd;
                                app.TaggedDatas.GeneralString6 = lstHiho[0].f000_importid.ToString();
                            }

                            //◆提供データ2 入居証明書.Excel
                            List<imp_ahk_nyukyo> lstNyukyo = imp_ahk_nyukyo.select(app.CYM, app.HihoNum);
                            if (lstNyukyo != null && lstNyukyo.Count > 0)
                            {
                                //医療機関名は医療機関マスタを優先
                                if (app.ClinicName == string.Empty) app.ClinicName = lstNyukyo[0].f002_clinicname;
                                app.TaggedDatas.InsName = lstNyukyo[0].f003_insurer;
                                app.Birthday = lstNyukyo[0].pbirthdayad;
                                app.TaggedDatas.GeneralString3 = lstNyukyo[0].f007_residence;
                                app.TaggedDatas.GeneralString4 = lstNyukyo[0].f008_residencename;
                            }

                            //◆提供データ2 居住地現況届提出一覧.Excel
                            List<imp_ahk_kyojuchi> lstkyojuchi = imp_ahk_kyojuchi.select(app.CYM, app.HihoNum);
                            if (lstkyojuchi != null && lstkyojuchi.Count > 0)
                            {
                                app.TaggedDatas.GeneralString5 = lstkyojuchi[0].f007_curradd;
                            }
                        }
                    }

                    //マッチング無し確認は申請書のみ
                    if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    {
                        if (dgv.Rows.Count == 0)
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
                    #endregion
                    break;
            }
            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;
            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
               
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            //20220906_1 ito st /////
            if (scan.AppType == APP_TYPE.柔整)
            {
                createGrid_jyu();
            }
            else
            {
                createGrid_ahk();
            }
            //createGrid();
            //20220906_1 ito end /////
        }

     
        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            verifyBoxHnum.Text = verifyBoxHnum.Text.Trim().PadLeft(8, '0');
        }

        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }


        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;

            //20200805155835 furukawa st ////////////////////////
            //同意書の同意年月を下に要望あり
            
            //同意書の同意年月は上
            else if ((verifyBoxY.Text == clsInputKind.施術同意書) && (douiConts.Contains(sender))) p = posTotal;
            //20200805155835 furukawa ed ////////////////////////

            else if (douiConts.Contains(sender)) p = posHname;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
