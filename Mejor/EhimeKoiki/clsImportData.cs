﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.EhimeKoiki
{
    public partial class imp_jyu_clinic
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                           //インポートID;
        public string f001_clinicnum { get; set; } = string.Empty;         //医療機関CD;
        public string f002_clinictel { get; set; } = string.Empty;         //医療機関電話番号;
        public string f003_clinickana { get; set; } = string.Empty;        //医療機関名カナ;
        public string f004_clinicname { get; set; } = string.Empty;        //医療機関名;
        public string f005_cliniczip { get; set; } = string.Empty;         //医療機関郵便番号;
        public string f006_clinicadd { get; set; } = string.Empty;         //医療機関住所;
        public int cym { get; set; } = 0;                                  //メホール請求年月;

        #endregion

        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_jyu_clinic> SelectAll(int cym)
        {
            List<imp_jyu_clinic> lst = new List<imp_jyu_clinic>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_jyu_clinic where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_jyu_clinic cls = new imp_jyu_clinic();
                    cls.f000_importid = int.Parse(item[0].ToString());
                    cls.cym = int.Parse(item[21].ToString().Trim());

                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 医療機関コード指定取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strClinicNum">医療機関コード</param>
        /// <returns></returns>
        public static List<imp_jyu_clinic> Select(int cym, string strClinicNum)
        {
            List<imp_jyu_clinic> lst = new List<imp_jyu_clinic>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_jyu_clinic " +
                $"where cym={cym} and f001_clinicnum='{strClinicNum}' order by f000_importid;");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_jyu_clinic cls = new imp_jyu_clinic();
                    cls.f000_importid = int.Parse(item[0].ToString());
                    cls.f001_clinicnum = item[1].ToString();
                    cls.f002_clinictel = item[2].ToString();
                    cls.f003_clinickana = item[3].ToString();
                    cls.f004_clinicname = item[4].ToString();
                    cls.f005_cliniczip = item[5].ToString();
                    cls.f006_clinicadd = item[6].ToString();

                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }


    public partial class imp_jyu_nyukyo
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                                  //インポートID;
        public string f001_number { get; set; } = string.Empty;                   //NO.;
        public string f002_clinicname { get; set; } = string.Empty;               //施術所名;
        public string f003_insurer { get; set; } = string.Empty;                  //保険者;
        public string f004_hihonum { get; set; } = string.Empty;                  //被保険者番号;
        public string f005_pname { get; set; } = string.Empty;                    //氏名;
        public string f006_pbirthday { get; set; } = string.Empty;                //生年月日;
        public string f007_residence { get; set; } = string.Empty;                //施設住所;
        public string f008_residencename { get; set; } = string.Empty;            //施設名;
        public string f009_nyukyodate { get; set; } = string.Empty;               //入居日;
        public string f010_shomeidate { get; set; } = string.Empty;               //証明日;
        public string f011_uketsukedate { get; set; } = string.Empty;             //受付日;
        public string f012_biko { get; set; } = string.Empty;                     //備考;
        public int cym { get; set; } = 0;                                         //メホール請求年月;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;            //生年月日西暦;
        public DateTime nyukyodatead { get; set; } = DateTime.MinValue;           //入居日西暦但し変換可能なデータのみ;
        public DateTime shomeidatead { get; set; } = DateTime.MinValue;           //証明日西暦;
        public DateTime uketsukedatead { get; set; } = DateTime.MinValue;         //受付日西暦;        
        public string zip { get; set; } = string.Empty;                           //郵便番号; //20220912_1 ito /////


        /// <summary>
        /// 被保番指定抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_jyu_nyukyo> select(int cym, string strHihonum)
        {
            List<imp_jyu_nyukyo> lstRet = new List<imp_jyu_nyukyo>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($" * ");
            sb.AppendLine($" from imp_jyu_nyukyo where cym={cym} and f004_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_jyu_nyukyo imp = new imp_jyu_nyukyo();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_number = item[1].ToString();
                    imp.f002_clinicname = item[2].ToString();
                    imp.f003_insurer = item[3].ToString();
                    imp.f004_hihonum = item[4].ToString();
                    imp.f005_pname = item[5].ToString();
                    imp.f006_pbirthday = item[6].ToString();
                    imp.f007_residence = item[7].ToString();
                    imp.f008_residencename = item[8].ToString();
                    imp.f009_nyukyodate = item[9].ToString();
                    imp.f010_shomeidate = item[10].ToString();
                    imp.f011_uketsukedate = item[11].ToString();
                    imp.f012_biko = item[12].ToString();

                    imp.cym = int.Parse(item[13].ToString());
                    imp.pbirthdayad = DateTime.Parse(item[14].ToString());
                    imp.nyukyodatead = DateTime.Parse(item[15].ToString());
                    imp.shomeidatead = DateTime.Parse(item[16].ToString());
                    imp.uketsukedatead = DateTime.Parse(item[17].ToString());
                    imp.zip = item[18].ToString(); //20220912_1 ito /////

                    lstRet.Add(imp);
                }
                return lstRet;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }

    public partial class imp_jyu_kyojuchi
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                              //インポートID;
        public string f001_number { get; set; } = string.Empty;               //NO.;
        public string f002_clinicname { get; set; } = string.Empty;           //施術所名;
        public string f003_insurer { get; set; } = string.Empty;              //保険者;
        public string f004_hihonum { get; set; } = string.Empty;              //被保険者番号;
        public string f005_pname { get; set; } = string.Empty;                //氏名;
        public string f006_pbirthday { get; set; } = string.Empty;            //生年月日;
        public string f007_curradd { get; set; } = string.Empty;              //現在の居住地;
        public string f008_mainname { get; set; } = string.Empty;             //世帯主氏名;
        public string f009_zokugara { get; set; } = string.Empty;             //世帯主から見た続柄;
        public string f010_tenkyodate { get; set; } = string.Empty;           //転居日;
        public string f011_shomeidate { get; set; } = string.Empty;           //証明日;
        public string f012_uketsukedate { get; set; } = string.Empty;         //受付日;
        public string f013_biko { get; set; } = string.Empty;                 //備考;
        public int cym { get; set; } = 0;                                     //メホール請求年月;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;        //生年月日西暦;
        public DateTime tenkyodatead { get; set; } = DateTime.MinValue;       //転居日西暦;
        public DateTime shomeidatead { get; set; } = DateTime.MinValue;       //証明日西暦;
        public DateTime uketsukedatead { get; set; } = DateTime.MinValue;     //受付日西暦;
        public string zip { get; set; } = string.Empty;                       //郵便番号;//20220912_1 ito /////



        /// <summary>
        /// 被保番指定抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_jyu_kyojuchi> select(int cym, string strHihonum)
        {
            List<imp_jyu_kyojuchi> lstRet = new List<imp_jyu_kyojuchi>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($" * ");
            sb.AppendLine($" from imp_jyu_kyojuchi where cym={cym} and f004_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_jyu_kyojuchi imp = new imp_jyu_kyojuchi();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_number = item[1].ToString();
                    imp.f002_clinicname = item[2].ToString();
                    imp.f003_insurer = item[3].ToString();
                    imp.f004_hihonum = item[4].ToString();
                    imp.f005_pname = item[5].ToString();
                    imp.f006_pbirthday = item[6].ToString();
                    imp.f007_curradd = item[7].ToString();
                    imp.f008_mainname = item[8].ToString();
                    imp.f009_zokugara = item[9].ToString();
                    imp.f010_tenkyodate = item[10].ToString();
                    imp.f011_shomeidate = item[11].ToString();
                    imp.f012_uketsukedate = item[12].ToString();
                    imp.f013_biko = item[13].ToString();
                    imp.cym = int.Parse(item[14].ToString());
                    imp.pbirthdayad = DateTime.Parse(item[15].ToString());
                    imp.tenkyodatead = DateTime.Parse(item[16].ToString());
                    imp.shomeidatead = DateTime.Parse(item[17].ToString());
                    imp.uketsukedatead = DateTime.Parse(item[18].ToString());
                    imp.zip = item[19].ToString(); //20220912_1 ito /////

                    lstRet.Add(imp);
                }
                return lstRet;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }


    public partial class imp_jyu
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;//インポートID;
        public string f001_insnum { get; set; } = string.Empty;//保険者番号;
        public string f002_hihonum { get; set; } = string.Empty;//被保険者番号;
        public string f003_hihokana { get; set; } = string.Empty; //氏名（カナ）;
        public string f004_hihoname { get; set; } = string.Empty;//氏名（漢字）;
        public string f005_hihozip { get; set; } = string.Empty;//被保険者郵便番号;
        public string f006_hihoadd { get; set; } = string.Empty;//被保険者住所;
        public string f007_sym { get; set; } = string.Empty;//診療年月;
        public string f008_clinicno { get; set; } = string.Empty;//医療機関番号;
        public string f009_clinickana { get; set; } = string.Empty;//医療機関名（カナ）;
        public string f010_clinicname { get; set; } = string.Empty;//医療機関名（漢字）;
        public string f011_ratio { get; set; } = string.Empty;//給付割合;
        public string f012_counteddays { get; set; } = string.Empty;//診療実日数;
        public string f013_total { get; set; } = string.Empty;//医療費;
        public string f014_charge { get; set; } = string.Empty;//保険者負担額;
        public string f015_partial { get; set; } = string.Empty;//一部負担相当額;
        public string f016_futan { get; set; } = string.Empty;//自己負担額;
        public string f017_recekey { get; set; } = string.Empty;//レセプト管理番号;
        public string f018_losedate { get; set; } = string.Empty;//喪失年月日;
        public int cym { get; set; } = 0;//メホール請求年月;
        public DateTime losedatead { get; set; } = DateTime.MinValue;//喪失日西暦;



        public static List<imp_jyu> SelectAll(int _cym)
        {
            List<imp_jyu> lstRet = new List<imp_jyu>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from imp_jyu where cym={_cym}");
            var lst = cmd.TryExecuteReaderList();
            for (int r = 0; r < lst.Count; r++)
            {
                int c = 0;
                imp_jyu cls = new imp_jyu();

                cls.f000_importid  = int.Parse(lst[r][c++].ToString());
                cls.f001_insnum = lst[r][c++].ToString();
                cls.f002_hihonum = lst[r][c++].ToString();
                cls.f003_hihokana = lst[r][c++].ToString();
                cls.f004_hihoname = lst[r][c++].ToString();
                cls.f005_hihozip = lst[r][c++].ToString();
                cls.f006_hihoadd = lst[r][c++].ToString();
                cls.f007_sym = lst[r][c++].ToString();
                cls.f008_clinicno = lst[r][c++].ToString();
                cls.f009_clinickana = lst[r][c++].ToString();
                cls.f010_clinicname = lst[r][c++].ToString();
                cls.f011_ratio = lst[r][c++].ToString();
                cls.f012_counteddays = lst[r][c++].ToString();
                cls.f013_total = lst[r][c++].ToString();
                cls.f014_charge = lst[r][c++].ToString();
                cls.f015_partial = lst[r][c++].ToString();
                cls.f016_futan = lst[r][c++].ToString();
                cls.f017_recekey = lst[r][c++].ToString();
                cls.f018_losedate = lst[r][c++].ToString();
                cls.cym = int.Parse(lst[r][c++].ToString());

                if (DateTime.TryParse(lst[r][c++].ToString(), out DateTime tmp_losedate)) cls.losedatead = tmp_losedate;

                lstRet.Add(cls);
            }

            return lstRet;
        }
    }



    /// <summary>
    /// あはき医療機関マスタ
    /// </summary>
    public partial class imp_ahk_clinic
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                           //インポートID;
        public string f001_clinicnum { get; set; } = string.Empty;         //医療機関CD;
        public string f002_clinictel { get; set; } = string.Empty;         //医療機関電話番号;
        public string f003_clinickana { get; set; } = string.Empty;        //医療機関名カナ;
        public string f004_clinicname { get; set; } = string.Empty;        //医療機関名;
        public string f005_cliniczip { get; set; } = string.Empty;         //医療機関郵便番号;
        public string f006_clinicadd { get; set; } = string.Empty;         //医療機関住所;
        public int cym { get; set; } = 0;                                  //メホール請求年月;

        #endregion

        /// <summary>
        /// 全リスト取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_ahk_clinic> SelectAll(int cym)
        {
            List<imp_ahk_clinic> lst = new List<imp_ahk_clinic>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_ahk_clinic where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_ahk_clinic cls = new imp_ahk_clinic();
                    cls.f000_importid = int.Parse(item[0].ToString());

                    //cls.f001_chargeym = item[1].ToString().Trim();
                    //cls.f002_rezenum = item[2].ToString().Trim();
                    //cls.f003_hihonum = item[3].ToString().Trim();
                    //cls.f004_kind = item[4].ToString().Trim();
                    //cls.f005_kbn = item[5].ToString().Trim();
                    //cls.f006_mediym = item[6].ToString().Trim();

                    cls.cym = int.Parse(item[21].ToString().Trim());
   
                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 医療機関コード指定取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strClinicNum">医療機関コード</param>
        /// <returns></returns>
        public static List<imp_ahk_clinic> Select(int cym,string strClinicNum)
        {
            List<imp_ahk_clinic> lst = new List<imp_ahk_clinic>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_ahk_clinic " +
                $"where cym={cym} and f001_clinicnum='{strClinicNum}' order by f000_importid;");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_ahk_clinic cls = new imp_ahk_clinic();
                    cls.f000_importid = int.Parse(item[0].ToString());
                    cls.f001_clinicnum = item[1].ToString();
                    cls.f002_clinictel = item[2].ToString();
                    cls.f003_clinickana = item[3].ToString();
                    cls.f004_clinicname = item[4].ToString();
                    cls.f005_cliniczip = item[5].ToString();
                    cls.f006_clinicadd = item[6].ToString();

                    lst.Add(cls);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }


    /// <summary>
    /// あはきデータ
    /// </summary>
    public partial class imp_ahk 
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                    //インポートID;
        public string f001_hihonum { get; set; } = string.Empty;                    //被保険者番号;
        public string f002_insnum { get; set; } = string.Empty;                     //保険者番号;
        public string f003_uketsukedate { get; set; } = string.Empty;               //受付年月日yyyymmdd;
        public string f004_shinseiname { get; set; } = string.Empty;                //申請者氏名（漢字）;
        public string f005_shinseizip { get; set; } = string.Empty;                 //申請者郵便番号;
        public string f006_shinseiadd { get; set; } = string.Empty;                 //申請者住所（漢字）;
        public string f007_shinseizokugara { get; set; } = string.Empty;            //申請者被保険者との関係;
        public string f008_shinseitel { get; set; } = string.Empty;                 //申請者電話番号;
        public string f009_chargeym { get; set; } = string.Empty;                   //請求年月yyyymm;
        public string f010_mediym { get; set; } = string.Empty;                     //診療年月yyyymm;
        public string f011_startdate1 { get; set; } = string.Empty;                 //診療開始年月日yyyymmdd;
        public string f012_finishdate1 { get; set; } = string.Empty;                //診療終了年月日yyyymmdd;
        public string f013_pref { get; set; } = string.Empty;                       //都道府県コード;
        public string f014_tensu { get; set; } = string.Empty;                      //点数表コード;
        public string f015_cliniccity { get; set; } = string.Empty;                 //医療機関市区町村コード;
        public string f016_clinicnumber { get; set; } = string.Empty;               //医療機関コード;
        public string f017_ratiokbn { get; set; } = string.Empty;                   //給付区分コード;
        public string f018_ratio { get; set; } = string.Empty;                      //給付割合%;
        public string f019_counteddays { get; set; } = string.Empty;                //診療実日数;
        public string f020_total { get; set; } = string.Empty;                      //費用金額;
        public string f021_charge { get; set; } = string.Empty;                     //保険者負担額;
        public string f022_partial { get; set; } = string.Empty;                    //一部負担相当額;
        public string f023_ryouyuhikbn { get; set; } = string.Empty;                //療養費区分コード;
        public string f024_bankkbn { get; set; } = string.Empty;                    //金融機関区分コード;
        public string f025_bankcode { get; set; } = string.Empty;                   //金融機関コード;
        public string f026_banktenpo { get; set; } = string.Empty;                  //金融機関店舗コード;
        public string f027_bankyokin { get; set; } = string.Empty;                  //預金種別コード;
        public string f028_accountnum { get; set; } = string.Empty;                 //口座番号;
        public string f029_accountname { get; set; } = string.Empty;                //口座名義人氏名（カナ）;
        public string f030_losedate { get; set; } = string.Empty;                   //喪失年月日yyyymmdd;
        public int cym { get; set; } = 0;                                          //メホール請求年月;
        public string clinicnum { get; set; } = string.Empty;                       //都道府県＋点数表＋市区町村＋医療機関コード;
        public DateTime uketsukedatead { get; set; } = DateTime.MinValue;           //受付年月日西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;             //診療開始日西暦;
        public DateTime finishdate1ad { get; set; } = DateTime.MinValue;            //診療終了日西暦;
        public DateTime losedatead { get; set; } = DateTime.MinValue;               //喪失日西暦;


        /// <summary>
        /// あはきデータの取得
        /// </summary>
        /// <returns></returns>
        public static List<imp_ahk> SelectAll(int _cym)
        {
            List<imp_ahk> lstRet = new List<imp_ahk>();
            DB.Command cmd = new DB.Command(DB.Main, $"select * from imp_ahk where cym={_cym}");
            var lst = cmd.TryExecuteReaderList();
            for (int r = 0; r < lst.Count; r++)
            {
                int c = 0;
                imp_ahk cls = new imp_ahk();

                cls.f000_importid = int.Parse(lst[r][c++].ToString());
                cls.f001_hihonum = lst[r][c++].ToString();
                cls.f002_insnum = lst[r][c++].ToString();
                cls.f003_uketsukedate = lst[r][c++].ToString();
                cls.f004_shinseiname = lst[r][c++].ToString();
                cls.f005_shinseizip = lst[r][c++].ToString();
                cls.f006_shinseiadd = lst[r][c++].ToString();
                cls.f007_shinseizokugara = lst[r][c++].ToString();
                cls.f008_shinseitel = lst[r][c++].ToString();
                cls.f009_chargeym = lst[r][c++].ToString();
                cls.f010_mediym = lst[r][c++].ToString();
                cls.f011_startdate1 = lst[r][c++].ToString();
                cls.f012_finishdate1 = lst[r][c++].ToString();
                cls.f013_pref = lst[r][c++].ToString();
                cls.f014_tensu = lst[r][c++].ToString();
                cls.f015_cliniccity = lst[r][c++].ToString();
                cls.f016_clinicnumber = lst[r][c++].ToString();
                cls.f017_ratiokbn = lst[r][c++].ToString();
                cls.f018_ratio = lst[r][c++].ToString();
                cls.f019_counteddays = lst[r][c++].ToString();
                cls.f020_total = lst[r][c++].ToString();
                cls.f021_charge = lst[r][c++].ToString();
                cls.f022_partial = lst[r][c++].ToString();
                cls.f023_ryouyuhikbn = lst[r][c++].ToString();
                cls.f024_bankkbn = lst[r][c++].ToString();
                cls.f025_bankcode = lst[r][c++].ToString();
                cls.f026_banktenpo = lst[r][c++].ToString();
                cls.f027_bankyokin = lst[r][c++].ToString();
                cls.f028_accountnum = lst[r][c++].ToString();
                cls.f029_accountname = lst[r][c++].ToString();
                cls.f030_losedate = lst[r][c++].ToString();
                cls.cym = int.Parse(lst[r][c++].ToString());
                cls.clinicnum = lst[r][c++].ToString();

                if (DateTime.TryParse(lst[r][c++].ToString(), out DateTime tmp_uketsukedate)) cls.uketsukedatead = tmp_uketsukedate;
                if (DateTime.TryParse(lst[r][c++].ToString(), out DateTime tmp_startdate1ad)) cls.startdate1ad = tmp_startdate1ad;
                if (DateTime.TryParse(lst[r][c++].ToString(), out DateTime tmp_finishdate1ad)) cls.finishdate1ad = tmp_finishdate1ad;
                if (DateTime.TryParse(lst[r][c++].ToString(), out DateTime tmp_losedatead)) cls.losedatead = tmp_losedatead;



                lstRet.Add(cls);
            }

            return lstRet;

        }

    }

    /// <summary>
    /// あはき入居証明
    /// </summary>
    public partial class imp_ahk_nyukyo
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                  //インポートID;
        public string f001_number { get; set; } = string.Empty;                   //NO.;
        public string f002_clinicname { get; set; } = string.Empty;               //施術所名;
        public string f003_insurer { get; set; } = string.Empty;                  //保険者;
        public string f004_hihonum { get; set; } = string.Empty;                  //被保険者番号;
        public string f005_pname { get; set; } = string.Empty;                    //氏名;
        public string f006_pbirthday { get; set; } = string.Empty;                //生年月日;
        public string f007_residence { get; set; } = string.Empty;                //施設住所;
        public string f008_residencename { get; set; } = string.Empty;            //施設名;
        public string f009_nyukyodate { get; set; } = string.Empty;               //入居日;
        public string f010_shomeidate { get; set; } = string.Empty;               //証明日;
        public string f011_uketsukedate { get; set; } = string.Empty;             //受付日;
        public string f012_biko { get; set; } = string.Empty;                     //備考;
        public int cym { get; set; } = 0;                                         //メホール請求年月;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;            //生年月日西暦;
        public DateTime nyukyodatead { get; set; } = DateTime.MinValue;           //入居日西暦但し変換可能なデータのみ;
        public DateTime shomeidatead { get; set; } = DateTime.MinValue;           //証明日西暦;
        public DateTime uketsukedatead { get; set; } = DateTime.MinValue;         //受付日西暦;


        /// <summary>
        /// 被保番指定抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_ahk_nyukyo> select (int cym,string strHihonum)
        {
            List<imp_ahk_nyukyo> lstRet = new List<imp_ahk_nyukyo>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($" * ");            
            sb.AppendLine($" from imp_ahk_nyukyo where cym={cym} and f004_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_ahk_nyukyo imp = new imp_ahk_nyukyo();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_number = item[1].ToString();
                    imp.f002_clinicname = item[2].ToString();
                    imp.f003_insurer = item[3].ToString();
                    imp.f004_hihonum = item[4].ToString();
                    imp.f005_pname = item[5].ToString();
                    imp.f006_pbirthday = item[6].ToString();
                    imp.f007_residence = item[7].ToString();
                    imp.f008_residencename = item[8].ToString();
                    imp.f009_nyukyodate = item[9].ToString();
                    imp.f010_shomeidate = item[10].ToString();
                    imp.f011_uketsukedate = item[11].ToString();
                    imp.f012_biko = item[12].ToString();

                    imp.cym = int.Parse(item[13].ToString());
                    imp.pbirthdayad= DateTime.Parse(item[14].ToString());
                    imp.nyukyodatead = DateTime.Parse(item[15].ToString());
                    imp.shomeidatead = DateTime.Parse(item[16].ToString());
                    imp.uketsukedatead = DateTime.Parse(item[17].ToString());
                    


                    

                    lstRet.Add(imp);
                }
                return lstRet;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }

    /// <summary>
    /// あはき居住地
    /// </summary>
    public partial class imp_ahk_kyojuchi
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                              //インポートID;
        public string f001_number { get; set; } = string.Empty;               //NO.;
        public string f002_clinicname { get; set; } = string.Empty;           //施術所名;
        public string f003_insurer { get; set; } = string.Empty;              //保険者;
        public string f004_hihonum { get; set; } = string.Empty;              //被保険者番号;
        public string f005_pname { get; set; } = string.Empty;                //氏名;
        public string f006_pbirthday { get; set; } = string.Empty;            //生年月日;
        public string f007_curradd { get; set; } = string.Empty;              //現在の居住地;
        public string f008_mainname { get; set; } = string.Empty;             //世帯主氏名;
        public string f009_zokugara { get; set; } = string.Empty;             //世帯主から見た続柄;
        public string f010_tenkyodate { get; set; } = string.Empty;           //転居日;
        public string f011_shomeidate { get; set; } = string.Empty;           //証明日;
        public string f012_uketsukedate { get; set; } = string.Empty;         //受付日;
        public string f013_biko { get; set; } = string.Empty;                 //備考;
        public int cym { get; set; } = 0;                                     //メホール請求年月;
        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;        //生年月日西暦;
        public DateTime tenkyodatead { get; set; } = DateTime.MinValue;       //転居日西暦;
        public DateTime shomeidatead { get; set; } = DateTime.MinValue;       //証明日西暦;
        public DateTime uketsukedatead { get; set; } = DateTime.MinValue;     //受付日西暦;



        /// <summary>
        /// 被保番指定抽出
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static List<imp_ahk_kyojuchi> select(int cym, string strHihonum)
        {
            List<imp_ahk_kyojuchi> lstRet = new List<imp_ahk_kyojuchi>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($" * ");
            sb.AppendLine($" from imp_ahk_kyojuchi where cym={cym} and f004_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_ahk_kyojuchi imp = new imp_ahk_kyojuchi();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_number = item[1].ToString();
                    imp.f002_clinicname = item[2].ToString();
                    imp.f003_insurer = item[3].ToString();
                    imp.f004_hihonum = item[4].ToString();
                    imp.f005_pname = item[5].ToString();
                    imp.f006_pbirthday = item[6].ToString();
                    imp.f007_curradd = item[7].ToString();
                    imp.f008_mainname = item[8].ToString();
                    imp.f009_zokugara = item[9].ToString();
                    imp.f010_tenkyodate = item[10].ToString();
                    imp.f011_shomeidate = item[11].ToString();
                    imp.f012_uketsukedate = item[12].ToString();
                    imp.f013_biko = item[13].ToString();
                    imp.cym = int.Parse(item[14].ToString());
                    imp.pbirthdayad = DateTime.Parse(item[15].ToString());
                    imp.tenkyodatead =DateTime.Parse(item[16].ToString());
                    imp.shomeidatead =DateTime.Parse(item[17].ToString());
                    imp.uketsukedatead = DateTime.Parse(item[18].ToString());





                    lstRet.Add(imp);
                }
                return lstRet;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }

 
    /// <summary>
    /// あはき被保険者マスタ
    /// </summary>
    public partial class imp_ahk_hihomaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                             //インポートID;
        public string f001_hihonum { get; set; } = string.Empty;             //被保険者番号;
        public string f002_hihokana { get; set; } = string.Empty;            //被保険者カナ氏名;
        public string f003_hihoname { get; set; } = string.Empty;            //被保険者氏名;
        public string f004_hihozip { get; set; } = string.Empty;             //被保険者郵便番号;
        public string f005_hihoadd { get; set; } = string.Empty;             //被保険者住所;
        public string f006_sendzip { get; set; } = string.Empty;             //送付先郵便番号;
        public string f007_sendadd { get; set; } = string.Empty;             //送付先住所;
        public string f008_sendname { get; set; } = string.Empty;            //送付先氏名;
        public string f009_newzip { get; set; } = string.Empty;              //転出先郵便番号;
        public string f010_newadd { get; set; } = string.Empty;              //転出先住所;
        public string f011_losecode { get; set; } = string.Empty;            //喪失事由CD;
        public string f012_loseziyu { get; set; } = string.Empty;            //喪失事由;
        public string f013_losedate { get; set; } = string.Empty;            //喪失年月日yyyymmdd;
        public int cym { get; set; } = 0;                                    //メホール請求年月;




        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns></returns>
        public static List<imp_ahk_hihomaster> selectall(int cym)
        {
            List<imp_ahk_hihomaster> lst = new List<imp_ahk_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_hihomaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_ahk_hihomaster imp = new imp_ahk_hihomaster();

                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    //imp.f002_pname = item[2].ToString();
                    //imp.f003_pkana = item[3].ToString();
                    //imp.f004_pzip = item[4].ToString();
                    //imp.f005_padd = item[5].ToString();                    
                    imp.f006_sendzip = item[7].ToString();
                    imp.f007_sendadd = item[8].ToString();
                    imp.cym = int.Parse(item[9].ToString());

                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// cymと被保険者証番号でリスト抽出
        /// </summary>
        /// <param name="cym">cym</param>
        /// <param name="strHihonum">被保険者証番号</param>
        /// <returns></returns>
        public static List<imp_ahk_hihomaster> select(int cym, string strHihonum)
        {
            List<imp_ahk_hihomaster> lst = new List<imp_ahk_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($" f000_importid");
            sb.AppendLine($" ,f001_hihonum");
            sb.AppendLine($" ,f002_hihokana");
            sb.AppendLine($" ,f003_hihoname");
            sb.AppendLine($" ,f004_hihozip");
            sb.AppendLine($" ,f005_hihoadd");
            sb.AppendLine($" ,f006_sendzip");
            sb.AppendLine($" ,f007_sendadd");
            sb.AppendLine($" ,f008_sendname");
            sb.AppendLine($" ,f009_newzip");
            sb.AppendLine($" ,f010_newadd");
            sb.AppendLine($" ,f011_losecode");
            sb.AppendLine($" ,f012_loseziyu");
            sb.AppendLine($" ,f013_losedate");
            sb.AppendLine($" ,cym");

            sb.AppendLine($" from imp_ahk_hihomaster where cym={cym} and f001_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();


                foreach (var item in l)
                {
                    imp_ahk_hihomaster imp = new imp_ahk_hihomaster();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_hihokana = item[2].ToString();
                    imp.f003_hihoname = item[3].ToString();
                    imp.f004_hihozip = item[4].ToString();
                    imp.f005_hihoadd = item[5].ToString();
                    imp.f006_sendzip = item[6].ToString();
                    imp.f007_sendadd = item[7].ToString();
                    imp.f008_sendname = item[8].ToString();
                    imp.f009_newzip = item[9].ToString();
                    imp.f010_newadd = item[10].ToString();
                    imp.f011_losecode = item[11].ToString();
                    imp.f012_loseziyu = item[12].ToString();
                    imp.f013_losedate = item[13].ToString();
                    imp.cym = int.Parse(item[14].ToString());


                    lst.Add(imp);
                }
                return lst;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }

        }


    }
}


