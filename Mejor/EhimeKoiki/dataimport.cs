﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.EhimeKoiki

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_AHK_main(int _cym)
        {
           
        }

        public static int intcym = 0;

        /// <summary>
        /// Excel
        /// </summary>
        public static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        /// <summary>
        /// ファイルチェックの結果
        /// </summary>
        public enum checkState
        {
            ok,
            row_zero,
            row_headeronly,
            col_ng,
            none_sheet,
            type_exception,
        };

        /// <summary>
        /// インポートするファイルのチェック
        /// </summary>
        /// <param name="lst">csv文字列リスト</param>
        /// <param name="header">ヘッダ有り=true</param>
        /// <param name="colcount">列数</param>
        /// <param name="colNumeric">数字判定する列</param>
        /// <returns></returns>
        public static checkState checkImportFile(List<string[]> lst, bool header, int colcount, int colNumeric)
        {
            if (lst[0].Length != colcount)
            {
                System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
                return checkState.col_ng;
            }

            if (header)
            {
                if (lst.Count == 1)
                {
                    System.Windows.Forms.MessageBox.Show($"ヘッダ以外のデータが存在しません。取り込みしません");
                    return checkState.row_headeronly;
                }

                if (!long.TryParse(lst[1][colNumeric].ToString(), out long tmp))
                {
                    System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
                    return checkState.type_exception;
                }
            }
            else
            {
                if (lst.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show($"行数が0です。取り込みしません");
                    return checkState.row_zero;
                }

                if (!long.TryParse(lst[0][colNumeric].ToString(), out long tmp))
                {
                    System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
                    return checkState.type_exception;
                }
            }
            return checkState.ok;
        }

        /// <summary>
        /// インポートするファイルのチェック(NPOI excel)
        /// </summary>
        /// <param name="ws">シート</param>
        /// <param name="header">ヘッダ有無</param>
        /// <param name="datastartrow">データ開始行</param>
        /// <param name="colcount">列数</param>
        /// <param name="colNumeric">数字判定する列</param>
        /// <returns></returns>
        public static checkState checkImportFile(NPOI.SS.UserModel.ISheet ws, bool header, int datastartrow, int colcount, int colNumeric)
        {

            //データ行で判断
            NPOI.SS.UserModel.IRow row = ws.GetRow(datastartrow);

            if (row.Cells.Count != colcount)
            {
                System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
                return checkState.col_ng;
            }


            if (header)
            {
                if (ws.LastRowNum == 1)
                {
                    System.Windows.Forms.MessageBox.Show($"ヘッダ以外のデータが存在しません。取り込みしません");
                    return checkState.row_headeronly;
                }

                if (!long.TryParse(row.GetCell(colNumeric).StringCellValue, out long tmp))
                {
                    System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
                    return checkState.type_exception;
                }
            }
            else
            {
                if (ws.LastRowNum == 0)
                {
                    System.Windows.Forms.MessageBox.Show($"行数が0です。取り込みしません");
                    return checkState.row_zero;
                }

                if (!long.TryParse(row.GetCell(colNumeric).StringCellValue, out long tmp))
                {
                    System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
                    return checkState.type_exception;
                }
            }


            return checkState.ok;
        }

        /// <summary>
        /// あはき医療機関マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool import_clinic(int _cym, WaitForm wf, string strFileName, APP_TYPE apptype)
        {
            List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

            if (dataImport.checkImportFile(lst, true, 6, 0) != dataImport.checkState.ok) return false;

            string clinicTable;
            if (apptype == APP_TYPE.柔整) clinicTable = "imp_jyu_clinic";
            else clinicTable = "imp_ahk_clinic";

            DB.Command cmd = DB.Main.CreateCmd($"delete from {clinicTable} where cym={_cym};");

            DB.Transaction tran = DB.Main.CreateTransaction();
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            try
            {
                cmd.TryExecuteNonQuery();

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //1列目が数字でない場合ヘッダと見なして飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }

                    if (apptype == APP_TYPE.柔整)
                    {

                        imp_jyu_clinic cls = new imp_jyu_clinic();
                        wf.LogPrint($"柔整医療機関マスタインポート 医療機関コード : {item[0]}");

                        cls.f001_clinicnum = item[0];
                        cls.f002_clinictel = item[1];
                        cls.f003_clinickana = item[2];
                        cls.f004_clinicname = item[3];
                        cls.f005_cliniczip = item[4];
                        cls.f006_clinicadd = item[5];
                        cls.cym = _cym;

                        if (!DB.Main.Insert<imp_jyu_clinic>(cls, tran)) return false;
                        wf.InvokeValue++;
                    }
                    else
                    {
                        imp_ahk_clinic cls = new imp_ahk_clinic();
                        wf.LogPrint($"あはき医療機関マスタインポート 医療機関コード : {item[0]}");

                        cls.f001_clinicnum = item[0];
                        cls.f002_clinictel = item[1];
                        cls.f003_clinickana = item[2];
                        cls.f004_clinicname = item[3];
                        cls.f005_cliniczip = item[4];
                        cls.f006_clinicadd = item[5];
                        cls.cym = _cym;

                        if (!DB.Main.Insert<imp_ahk_clinic>(cls, tran)) return false;
                        wf.InvokeValue++;
                    }
                }

                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

                cmd.Dispose();
            }
        }

        /// <summary>
        /// 入居証明書提出一覧,居住地現況届提出一覧
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool import_nyukyo(int _cym, WaitForm wf, string strFileName, APP_TYPE apptype)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);

            string nyukyoTable;
            if (apptype == APP_TYPE.柔整) nyukyoTable = "imp_jyu_nyukyo";
            else nyukyoTable = "imp_ahk_nyukyo";

            try
            {
                using (DB.Command cmd = new DB.Command($"delete from {nyukyoTable} where cym={intcym}", tran))
                {
                    cmd.TryExecuteNonQuery();
                }

                #region NPOI
                wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                NPOI.SS.UserModel.ISheet ws = wb.GetSheet("入居証明書提出一覧");
                if (ws == null)
                {
                    System.Windows.Forms.MessageBox.Show($"[入居証明書提出一覧]シートがありません。処理を終了します");
                    return false;
                }
                wf.SetMax(ws.LastRowNum + 1); //20221018_1 ito /////wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                //ファイルチェック
                if (checkImportFile(ws, true, 3, 13, 3) != checkState.ok) return false; //20220912_1 ito st /////if (checkImportFile(ws, true, 3, 12, 3) != checkState.ok) return false;

                for (int r = 0; r < ws.LastRowNum + 1; r++)//20221018_1 ito /////for (int r = 0; r < ws.LastRowNum; r++)
                {
                    NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

                    //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす
                    if (row1 == null) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //柔整の場合
                    if (apptype == APP_TYPE.柔整)
                    {
                        imp_jyu_nyukyo cls = new imp_jyu_nyukyo();
                        for (int c = 0; c < row1.Cells.Count; c++)
                        {
                            NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            if (c == 0)
                            {
                                //1列目が数字でない場合は飛ばす                                
                                if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;

                                wf.LogPrint($"入居証明書提出一覧取得 {getCellValueToString(cell)}");
                                cls.cym = _cym; //20221018_1 ito /////
                                cls.f001_number = getCellValueToString(cell);
                            }
                            if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                            if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                            if (c == 3) cls.f004_hihonum = getCellValueToString(cell);
                            if (c == 4) cls.f005_pname = getCellValueToString(cell);
                            if (c == 5)
                            {
                                //生年月日だけはシリアル値でしか取れない
                                if (long.TryParse(getCellValueToString(cell), out long tmp))
                                {
                                    DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                    cls.pbirthdayad = dtBirth;
                                    cls.f006_pbirthday =
                                        DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                        + $".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                                }
                            }
                            if (c == 6) cls.f007_residence = getCellValueToString(cell);
                            if (c == 7) cls.f008_residencename = getCellValueToString(cell);
                            if (c == 8)
                            {
                                cls.f009_nyukyodate = getCellValueToString(cell);
                                cls.nyukyodatead = cngWarekiToAD(cls.f009_nyukyodate);
                            }
                            if (c == 9)
                            {
                                cls.f010_shomeidate = getCellValueToString(cell);
                                cls.shomeidatead = cngWarekiToAD(cls.f010_shomeidate);
                            }
                            if (c == 10)
                            {
                                cls.f011_uketsukedate = getCellValueToString(cell);
                                cls.uketsukedatead = cngWarekiToAD(cls.f011_uketsukedate);
                            }
                            //20220912_1 ito st /////
                            if (c == 11) cls.zip = getCellValueToString(cell); 
                            //if (c == 11)
                            if (c == 12) 
                            //20220912_1 ito end /////
                            {
                                cls.f012_biko = getCellValueToString(cell);
                                //cls.cym = _cym; //20221018_1 ito ///// 罫線が抜けている行において、列数が最後まで認識されずcym=0で登録されていたため、上に移動
                            }
                        }
                        if (cls.f004_hihonum != string.Empty)
                        {
                            if (!DB.Main.Insert<imp_jyu_nyukyo>(cls, tran)) return false;
                        }
                        wf.InvokeValue++;

                    }
                    //あはきの場合
                    else
                    {
                        imp_ahk_nyukyo cls = new imp_ahk_nyukyo();
                        for (int c = 0; c < row1.Cells.Count; c++)
                        {
                            NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            if (c == 0)
                            {
                                //1列目が数字でない場合は飛ばす
                                if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;

                                wf.LogPrint($"入居証明書提出一覧取得 {getCellValueToString(cell)}");
                                cls.cym = _cym; //20221018_1 ito /////
                                cls.f001_number = getCellValueToString(cell);
                            }
                            if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                            if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                            if (c == 3) cls.f004_hihonum = getCellValueToString(cell);
                            if (c == 4) cls.f005_pname = getCellValueToString(cell);
                            if (c == 5)
                            {
                                //生年月日だけはシリアル値でしか取れない
                                if (long.TryParse(getCellValueToString(cell), out long tmp))
                                {
                                    DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                    cls.pbirthdayad = dtBirth;
                                    cls.f006_pbirthday =
                                        DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                        + $".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                                }
                            }
                            if (c == 6) cls.f007_residence = getCellValueToString(cell);
                            if (c == 7) cls.f008_residencename = getCellValueToString(cell);
                            if (c == 8)
                            {
                                cls.f009_nyukyodate = getCellValueToString(cell);
                                cls.nyukyodatead = cngWarekiToAD(cls.f009_nyukyodate);
                            }
                            if (c == 9)
                            {
                                cls.f010_shomeidate = getCellValueToString(cell);
                                cls.shomeidatead = cngWarekiToAD(cls.f010_shomeidate);
                            }
                            if (c == 10)
                            {
                                cls.f011_uketsukedate = getCellValueToString(cell);
                                cls.uketsukedatead = cngWarekiToAD(cls.f011_uketsukedate);
                            }
                            if (c == 11) 
                            {
                                cls.f012_biko = getCellValueToString(cell);

                                //cls.cym = _cym; //20221018_1 ito ///// 罫線が抜けている行において、列数が右端まで認識されずcym=0で登録されていたため、上に移動
                            }
                        }
                        if (cls.f004_hihonum != string.Empty)
                        {
                            if (!DB.Main.Insert<imp_ahk_nyukyo>(cls, tran)) return false;
                        }
                        wf.InvokeValue++;
                    }
                }

                #endregion

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                if (wb != null) wb.Close();

            }

        }

        public static bool import_kyojuchi(int _cym, WaitForm wf, string strFileName, APP_TYPE apptype)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);

            string kyojuchiTable;
            if (apptype == APP_TYPE.柔整) kyojuchiTable = "imp_jyu_kyojuchi";
            else kyojuchiTable = "imp_ahk_kyojuchi";

            try
            {
                using (DB.Command cmd = new DB.Command($"delete from {kyojuchiTable} where cym={intcym}", tran))
                {
                    cmd.TryExecuteNonQuery();
                }


                #region npoiコード
                wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                NPOI.SS.UserModel.ISheet ws = wb.GetSheet("居住地現況届提出一覧 ");
                if (ws == null)
                {
                    System.Windows.Forms.MessageBox.Show($"[居住地現況届提出一覧]シートがありません。処理を終了します");
                    return false;
                }
                wf.SetMax(ws.LastRowNum + 1); //20221018_1 ito /////wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                //ファイルチェック
                if (checkImportFile(ws, true, 3, 14, 3) != checkState.ok) return false; //20220912_1 ito st /////if (checkImportFile(ws, true, 3, 13, 3) != checkState.ok) return false;

                for (int r = 0; r < ws.LastRowNum + 1; r++)//20221018_1 ito /////for (int r = 0; r < ws.LastRowNum; r++)

                {
                    NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

                    //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす
                    if (row1 == null) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    if (apptype == APP_TYPE.柔整)
                    {
                        imp_jyu_kyojuchi cls = new imp_jyu_kyojuchi();
                        for (int c = 0; c < row1.LastCellNum; c++)
                        {
                            NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            if (c == 0)
                            {
                                //1列目が数字でない場合は飛ばす
                                if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;

                                wf.LogPrint($"居住地現況届提出一覧 取得 {getCellValueToString(cell)}");
                                cls.cym = _cym; //20221018_1 ito /////
                                cls.f001_number = getCellValueToString(cell);
                            }
                            if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                            if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                            if (c == 3) cls.f004_hihonum = getCellValueToString(cell);
                            if (c == 4) cls.f005_pname = getCellValueToString(cell);
                            if (c == 5)
                            {
                                //生年月日だけはシリアル値でしか取れない
                                if (long.TryParse(getCellValueToString(cell), out long tmp))
                                {
                                    DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                    cls.pbirthdayad = dtBirth;
                                    cls.f006_pbirthday =
                                        DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                        + $".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                                }
                            }
                            if (c == 6) cls.f007_curradd = getCellValueToString(cell);
                            if (c == 7) cls.f008_mainname = getCellValueToString(cell);
                            if (c == 8) cls.f009_zokugara = getCellValueToString(cell);
                            if (c == 9)
                            {
                                cls.f010_tenkyodate = getCellValueToString(cell);
                                cls.tenkyodatead = cngWarekiToAD(cls.f010_tenkyodate);
                            }
                            if (c == 10)
                            {
                                cls.f011_shomeidate = getCellValueToString(cell);
                                cls.shomeidatead = cngWarekiToAD(cls.f011_shomeidate);
                            }
                            if (c == 11)
                            {
                                cls.f012_uketsukedate = getCellValueToString(cell);
                                cls.uketsukedatead = cngWarekiToAD(cls.f012_uketsukedate);
                            }
                            //20220912_1 ito st /////
                            if (c == 12) cls.zip = getCellValueToString(cell);
                            //if (c == 12)
                            if (c == 13)
                            //20220912_1 ito /////
                            {
                                cls.f013_biko = getCellValueToString(cell);
                                //cls.cym = _cym; //20221018_1 ito ///// 罫線が抜けている行において、列数が最後まで認識されずcym=0で登録されていたため、上に移動
                            }
                        }
                        if (cls.f004_hihonum != string.Empty)
                        {
                            if (!DB.Main.Insert<imp_jyu_kyojuchi>(cls, tran)) return false;
                        }
                        wf.InvokeValue++;
                    }
                    else
                    {
                        imp_ahk_kyojuchi cls = new imp_ahk_kyojuchi();
                        for (int c = 0; c < row1.LastCellNum; c++)
                        {
                            NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            if (c == 0)
                            {
                                //1列目が数字でない場合は飛ばす
                                if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;

                                wf.LogPrint($"居住地現況届提出一覧 取得 {getCellValueToString(cell)}");
                                cls.cym = _cym; //20221018_1 ito /////
                                cls.f001_number = getCellValueToString(cell);
                            }
                            if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                            if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                            if (c == 3) cls.f004_hihonum = getCellValueToString(cell);
                            if (c == 4) cls.f005_pname = getCellValueToString(cell);
                            if (c == 5)
                            {
                                //生年月日だけはシリアル値でしか取れない
                                if (long.TryParse(getCellValueToString(cell), out long tmp))
                                {
                                    DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                    cls.pbirthdayad = dtBirth;
                                    cls.f006_pbirthday =
                                        DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                        + $".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                                }
                            }
                            if (c == 6) cls.f007_curradd = getCellValueToString(cell);
                            if (c == 7) cls.f008_mainname = getCellValueToString(cell);
                            if (c == 8) cls.f009_zokugara = getCellValueToString(cell);
                            if (c == 9)
                            {
                                cls.f010_tenkyodate = getCellValueToString(cell);
                                cls.tenkyodatead = cngWarekiToAD(cls.f010_tenkyodate);
                            }
                            if (c == 10)
                            {
                                cls.f011_shomeidate = getCellValueToString(cell);
                                cls.shomeidatead = cngWarekiToAD(cls.f011_shomeidate);
                            }
                            if (c == 11)
                            {
                                cls.f012_uketsukedate = getCellValueToString(cell);
                                cls.uketsukedatead = cngWarekiToAD(cls.f012_uketsukedate);
                            }
                            if (c == 12)
                            {
                                cls.f013_biko = getCellValueToString(cell);
                                //cls.cym = _cym; //20221018_1 ito ///// 罫線が抜けている行において、列数が最後まで認識されずcym=0で登録されていたため、上に移動
                            }
                        }
                        if (cls.f004_hihonum != string.Empty)
                        {
                            if (!DB.Main.Insert<imp_ahk_kyojuchi>(cls, tran)) return false;
                        }
                        wf.InvokeValue++;
                    }
                }
                tran.Commit();
                return true;
                #endregion

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

                if (dataImport.wb != null) dataImport.wb.Close();
            }

        }




        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        public static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();

                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();

                    }
                    else return string.Empty;

                default:
                    return string.Empty;

            }
        }

        /// <summary>
        /// 和暦西暦変換
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime cngWarekiToAD(string str)
        {
            string strPtn = "^[T|S|H|R][0-9]{1,2}[.][0-9]{1,2}[.][0-9]{1,2}$";

            if (System.Text.RegularExpressions.Regex.IsMatch(str, strPtn))
            {
                string strf011 = str.Substring(1);
                int y = DateTimeEx.GetAdYearFromHs(int.Parse(strf011.Split('.')[0].PadLeft(2, '0') + strf011.Split('.')[1].PadLeft(2, '0')));

                return new DateTime(
                    y,
                    int.Parse(str.Split('.')[1]),
                    int.Parse(str.Split('.')[2]));
            }
            return DateTime.MinValue;
        }
    }


    partial class dataImport_JYU
    {
        /// <summary>
        /// 柔整インポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool import_jyu(int _cym, WaitForm wf, string strFileName)
        {
            List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

            if (dataImport.checkImportFile(lst, true, 18, 0) != dataImport.checkState.ok) return false;

            DB.Command cmd = DB.Main.CreateCmd($"delete from imp_jyu where cym={_cym};");
            DB.Transaction tran = DB.Main.CreateTransaction();
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            try
            {
                cmd.TryExecuteNonQuery();

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                    //1列目が数字でない場合ヘッダと見なして飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }
                    imp_jyu cls = new imp_jyu();
                    wf.LogPrint($"柔整データインポート : {item[0]}");
                    cls.f001_insnum = item[0];
                    cls.f002_hihonum = item[1];
                    cls.f003_hihokana = item[2];
                    cls.f004_hihoname = item[3];
                    cls.f005_hihozip = item[4];
                    cls.f006_hihoadd = item[5];
                    cls.f007_sym = item[6];
                    cls.f008_clinicno = item[7];
                    cls.f009_clinickana = item[8];
                    cls.f010_clinicname = item[9];
                    cls.f011_ratio = item[10];
                    cls.f012_counteddays = item[11];
                    cls.f013_total = item[12];
                    cls.f014_charge = item[13];
                    cls.f015_partial = item[14];
                    cls.f016_futan = item[15];
                    cls.f017_recekey = item[16];
                    cls.f018_losedate  = item[17];

                    cls.cym = _cym;
                    cls.losedatead = DateTimeEx.ToDateTime(cls.f018_losedate);

                    if (!DB.Main.Insert<imp_jyu>(cls, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

                cmd.Dispose();
            }
        }

    }





    partial class dataImport_AHK
    {
        #region チェック・医療機関コード
        /*
        //static int intcym = 0;

        ///// <summary>
        ///// Excel
        ///// </summary>
        //static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        ///// <summary>
        ///// ファイルチェックの結果
        ///// </summary>
        //public enum checkState
        //{
        //    ok,
        //    row_zero,
        //    row_headeronly,
        //    col_ng,
        //    none_sheet,
        //    type_exception,
        //};


        ///// <summary>
        ///// インポートするファイルのチェック
        ///// </summary>
        ///// <param name="lst">csv文字列リスト</param>
        ///// <param name="header">ヘッダ有り=true</param>
        ///// <param name="colcount">列数</param>
        ///// <param name="colNumeric">数字判定する列</param>
        ///// <returns></returns>
        //private static checkState checkImportFile(List<string[]> lst, bool header,int colcount,int colNumeric)
        //{
        //    if (lst[0].Length != colcount)
        //    {
        //        System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
        //        return checkState.col_ng;
        //    }


        //    if (header)
        //    {
        //        if (lst.Count == 1)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"ヘッダ以外のデータが存在しません。取り込みしません");
        //            return checkState.row_headeronly;
        //        }

        //        if(!long.TryParse(lst[1][colNumeric].ToString(),out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }
        //    else
        //    {
        //        if (lst.Count == 0)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"行数が0です。取り込みしません");
        //            return checkState.row_zero;
        //        }

        //        if (!long.TryParse(lst[0][colNumeric].ToString(), out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }




        //    return checkState.ok;
        //}


        ///// <summary>
        ///// インポートするファイルのチェック(NPOI excel)
        ///// </summary>
        ///// <param name="ws">シート</param>
        ///// <param name="header">ヘッダ有無</param>
        ///// <param name="datastartrow">データ開始行</param>
        ///// <param name="colcount">列数</param>
        ///// <param name="colNumeric">数字判定する列</param>
        ///// <returns></returns>
        //private static checkState checkImportFile(NPOI.SS.UserModel.ISheet ws, bool header, int datastartrow, int colcount, int colNumeric)
        //{

        //    //データ行で判断
        //    NPOI.SS.UserModel.IRow row = ws.GetRow(datastartrow);

        //    if (row.Cells.Count != colcount)
        //    {
        //        System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
        //        return checkState.col_ng;
        //    }


        //    if (header)
        //    {
        //        if (ws.LastRowNum == 1)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"ヘッダ以外のデータが存在しません。取り込みしません");
        //            return checkState.row_headeronly;
        //        }

        //        if (!long.TryParse(row.GetCell(colNumeric).StringCellValue, out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }
        //    else
        //    {
        //        if (ws.LastRowNum == 0)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"行数が0です。取り込みしません");
        //            return checkState.row_zero;
        //        }

        //        if (!long.TryParse(row.GetCell(colNumeric).StringCellValue, out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }


        //    return checkState.ok;
        //}


        #region closedxmlのcheckImportFile
        /// <summary>
        /// インポートするファイルのチェック(closedxml excel)
        /// </summary>
        /// <param name="ws">シート</param>
        /// <param name="header">ヘッダ有無</param>
        /// <param name="datastartrow">データ開始行</param>
        /// <param name="colcount">列数</param>
        /// <param name="colNumeric">数字判定する列</param>
        /// <returns></returns>
        //private static checkState checkImportFile(ClosedXML.Excel.IXLWorksheet ws, bool header, int datastartrow, int colcount, int colNumeric)
        //{

        //    //データ行で判断
        //    //NPOI.SS.UserModel.IRow row = ws.GetRow(datastartrow);
        //    ClosedXML.Excel.IXLRow row = ws.Row(datastartrow);
        //    if (ws.LastColumnUsed().ColumnNumber() != colcount)
        //    //if (row.LastCellUsed().Address.ColumnNumber != colcount)
        //    {
        //        System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
        //        return checkState.col_ng;
        //    }


        //    if (header)
        //    {
        //        if (ws.RowCount() == 1)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"ヘッダ以外のデータが存在しません。取り込みしません");
        //            return checkState.row_headeronly;
        //        }

        //        if (ws.LastColumnUsed().ColumnNumber() != colcount)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"列数が{colcount}ではありません。取り込みしません");
        //            return checkState.col_ng;
        //        }

        //        if (!long.TryParse(row.Cell(colNumeric+1).Value.ToString(), out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }
        //    else
        //    {
        //        if (ws.RowCount() == 0)
        //        {
        //            System.Windows.Forms.MessageBox.Show($"行数が0です。取り込みしません");
        //            return checkState.row_zero;
        //        }

        //        if (!long.TryParse(row.Cell(colNumeric+1).Value.ToString(), out long tmp))
        //        {
        //            System.Windows.Forms.MessageBox.Show($"データが想定外の型です。取り込みしません");
        //            return checkState.type_exception;
        //        }
        //    }


        //    return checkState.ok;
        //}
        #endregion

        /// <summary>
        /// あはき医療機関マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool import_ahk_clinic(int _cym,WaitForm wf,string strFileName)
        {
            List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

            //20220624103604 furukawa st ////////////////////////
            //チェック列変更
            
            if (dataImport.checkImportFile(lst,true,6,0) != dataImport.checkState.ok) return false;
            //if (checkImportFile(lst, true, 6, 1) != checkState.ok) return false;
            //20220624103604 furukawa ed ////////////////////////

            //20220629191736 furukawa st ////////////////////////
            //削除条件cym忘れ
            
            DB.Command cmd = DB.Main.CreateCmd($"delete from imp_ahk_clinic where cym={_cym};");
            //DB.Command cmd = DB.Main.CreateCmd($"delete from imp_ahk_clinic;");
            //20220629191736 furukawa ed ////////////////////////


            DB.Transaction tran = DB.Main.CreateTransaction();
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            try
            {
                cmd.TryExecuteNonQuery();

                foreach(var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    //1列目が数字でない場合ヘッダと見なして飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }

                    imp_ahk_clinic cls = new imp_ahk_clinic();
                    wf.LogPrint($"あはき医療機関マスタインポート 医療機関コード : {item[0]}");

                    cls.f001_clinicnum = item[0];
                    cls.f002_clinictel = item[1];
                    cls.f003_clinickana = item[2];
                    cls.f004_clinicname = item[3];
                    cls.f005_cliniczip = item[4];
                    cls.f006_clinicadd = item[5];
                    cls.cym = _cym;

                    if (!DB.Main.Insert<imp_ahk_clinic>(cls, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();

                return true;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                
                cmd.Dispose();
            }
        }
        */
        #endregion

        /// <summary>
        /// あはきインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool import_ahk(int _cym, WaitForm wf, string strFileName)
        {
            List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

            //20220624103527 furukawa st ////////////////////////
            //チェック列変更
            
            if (dataImport.checkImportFile(lst, true, 30, 0) != dataImport.checkState.ok) return false;
            //if (checkImportFile(lst, true, 30, 1) != checkState.ok) return false;
            //20220624103527 furukawa ed ////////////////////////

            DB.Command cmd = DB.Main.CreateCmd($"delete from imp_ahk where cym={_cym};");
            DB.Transaction tran = DB.Main.CreateTransaction();
            wf.SetMax(lst.Count);
            wf.InvokeValue = 0;
            try
            {
                cmd.TryExecuteNonQuery();

                foreach (var item in lst)
                {
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;
                    //1列目が数字でない場合ヘッダと見なして飛ばす
                    if (!long.TryParse(item[0].ToString(), out long tmp))
                    {
                        wf.InvokeValue++;
                        continue;
                    }
                    imp_ahk cls = new imp_ahk();
                    wf.LogPrint($"あはきデータインポート : {item[0]}");
                    cls.f001_hihonum = item[0];
                    cls.f002_insnum = item[1];
                    cls.f003_uketsukedate = item[2];
                    cls.f004_shinseiname = item[3];
                    cls.f005_shinseizip = item[4];
                    cls.f006_shinseiadd = item[5];
                    cls.f007_shinseizokugara = item[6];
                    cls.f008_shinseitel = item[7];
                    cls.f009_chargeym = item[8];
                    cls.f010_mediym = item[9];
                    cls.f011_startdate1 = item[10];
                    cls.f012_finishdate1 = item[11];
                    cls.f013_pref = item[12];
                    cls.f014_tensu = item[13];
                    cls.f015_cliniccity = item[14];
                    cls.f016_clinicnumber = item[15];
                    cls.f017_ratiokbn = item[16];
                    cls.f018_ratio = item[17];
                    cls.f019_counteddays = item[18];
                    cls.f020_total = item[19];
                    cls.f021_charge = item[20];
                    cls.f022_partial = item[21];
                    cls.f023_ryouyuhikbn = item[22];
                    cls.f024_bankkbn = item[23];
                    cls.f025_bankcode = item[24];
                    cls.f026_banktenpo = item[25];
                    cls.f027_bankyokin = item[26];
                    cls.f028_accountnum = item[27];
                    cls.f029_accountname = item[28];
                    cls.f030_losedate = item[29];

                    cls.cym = _cym;
                    cls.clinicnum= cls.f013_pref+cls.f014_tensu + cls.f015_cliniccity + cls.f016_clinicnumber;
                    cls.uketsukedatead = DateTimeEx.ToDateTime(cls.f003_uketsukedate);
                    cls.startdate1ad = DateTimeEx.ToDateTime(cls.f011_startdate1);
                    cls.finishdate1ad = DateTimeEx.ToDateTime(cls.f012_finishdate1);
                    cls.losedatead = DateTimeEx.ToDateTime(cls.f030_losedate);

                    if (!DB.Main.Insert<imp_ahk>(cls, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {

                cmd.Dispose();
            }
        }

        #region 入居証明・居住地現況届
        /*
        /// <summary>
        /// 入居証明書提出一覧,居住地現況届提出一覧
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool import_ahk_nyukyo(int _cym, WaitForm wf, string strFileName)
        {
           
            DB.Transaction tran = new DB.Transaction(DB.Main);
          

            try
            {
                using (DB.Command cmd = new DB.Command($"delete from imp_ahk_nyukyo where cym={dataImport.intcym}", tran))
                {
                    cmd.TryExecuteNonQuery();
                }
                #region 
                //ファイルを開けない時点で無理

                //ClosedXML.Excel.XLWorkbook wkbook = null;
                ////ブック開く                
                //wkbook = new ClosedXML.Excel.XLWorkbook(strFileName);

                ////シート
                //ClosedXML.Excel.IXLWorksheet sht = null;

                ////シート存在確認
                //if (!wkbook.TryGetWorksheet("入居証明書提出一覧", out sht))
                //{
                //    System.Windows.Forms.MessageBox.Show($"[入居証明書提出一覧]シートがありません。処理を終了します"
                //        , System.Windows.Forms.Application.ProductName
                //        , System.Windows.Forms.MessageBoxButtons.OK
                //        , System.Windows.Forms.MessageBoxIcon.Exclamation);
                //    return false;
                //}
                //int cols = 12;
                ////取得範囲設定
                //ClosedXML.Excel.IXLRange rng = sht.Range(2, 1, sht.Column(1).LastCellUsed().Address.RowNumber, cols);

                ////ファイルチェック
                //if (checkImportFile(sht, true, 3, cols, 3) != checkState.ok) return false;

                //wf.SetMax(sht.Column(1).LastCellUsed().Address.RowNumber);
                //wf.InvokeValue = 0;





                //for (int r = 1; r <= sht.LastCellUsed().Address.RowNumber; r++)
                //{

                //    ClosedXML.Excel.IXLRow row = sht.Row(r);
                //    imp_ahk_nyukyo cls = new imp_ahk_nyukyo();

                //    for (int c = 1; c <= row.LastCellUsed().Address.ColumnNumber; c++)
                //    {

                //        if (!long.TryParse(row.Cell(4).Value.ToString(), out long tmp)) break;
                //        if (row.Cell(4).Value.ToString() == string.Empty) break;
                //        if (c == 1)
                //        {
                //            wf.LogPrint($"入居証明書提出一覧 取得 {row.Cell(c).Value.ToString()}");
                //            cls.f001_number = row.Cell(c).Value.ToString();
                //            wf.InvokeValue++;

                //        }

                //        cls.cym = _cym;

                //        if (c == 2) cls.f002_clinicname = row.Cell(c).Value.ToString();
                //        if (c == 3) cls.f003_insurer = row.Cell(c).Value.ToString();
                //        if (c == 4) cls.f004_hihonum = row.Cell(c).Value.ToString();
                //        if (c == 5) cls.f005_pname = row.Cell(c).Value.ToString();
                //        if (c == 6 && row.Cell(c).Value.ToString().Trim() != string.Empty)
                //        {
                //            cls.f006_pbirthday = row.Cell(c).GetDateTime().ToJDateShortStr();
                //            if (DateTime.TryParse(row.Cell(c).GetDateTime().ToShortDateString(), out DateTime tmpf006)) cls.pbirthdayad = tmpf006;
                //            else { }
                //        }
                //        if (c == 7) cls.f007_residence = row.Cell(c).Value.ToString();
                //        if (c == 8) cls.f008_residencename= row.Cell(c).Value.ToString();
                //        if (c == 9) cls.f009_nyukyodate= row.Cell(c).Value.ToString();
                //        if (c == 10) cls.f010_shomeidate = row.Cell(c).Value.ToString();
                //        if (c == 11) cls.f011_uketsukedate = row.Cell(c).Value.ToString();
                //        if (c == 12) cls.f012_biko = row.Cell(c).Value.ToString();



                //    }

                //    if (cls.f004_hihonum != string.Empty)
                //        if (!DB.Main.Insert<imp_ahk_nyukyo>(cls, tran)) return false;

                //}

                #endregion


                #region NPOI
                dataImport.wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                NPOI.SS.UserModel.ISheet ws = dataImport.wb.GetSheet("入居証明書提出一覧");
                if (ws == null)
                {
                    System.Windows.Forms.MessageBox.Show($"[入居証明書提出一覧]シートがありません。処理を終了します");
                    return false;
                }
                wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                //ファイルチェック
                if (dataImport.checkImportFile(ws, true, 3, 12, 3) != dataImport.checkState.ok) return false;

                for (int r = 0; r < ws.LastRowNum; r++)
                {
                    NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

                    //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす
                    if (row1 == null) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    imp_ahk_nyukyo cls = new imp_ahk_nyukyo();
                    for (int c = 0; c < row1.Cells.Count; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //1列目が数字でない場合は飛ばす
                        if (c == 0)
                        {
                            if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;
                        }



                        if (c == 0)
                        {
                            wf.LogPrint($"入居証明書提出一覧取得 {getCellValueToString(cell)}");
                            cls.f001_number = getCellValueToString(cell);
                        }
                        if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                        if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                        if (c == 3) cls.f004_hihonum = getCellValueToString(cell);                            
                        if (c == 4) cls.f005_pname = getCellValueToString(cell);
                        if (c == 5)
                        {
                            //生年月日だけはシリアル値でしか取れない
                            if (long.TryParse(getCellValueToString(cell), out long tmp))
                            {
                                DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                cls.pbirthdayad = dtBirth;
                                cls.f006_pbirthday =
                                    DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                    + $".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                            }

                        }
                        if (c == 6) cls.f007_residence = getCellValueToString(cell);
                        if (c == 7) cls.f008_residencename = getCellValueToString(cell);
                        if (c == 8)
                        {
                            cls.f009_nyukyodate = getCellValueToString(cell);
                            cls.nyukyodatead = cngWarekiToAD(cls.f009_nyukyodate);
                           
                        }

                        if (c == 9)
                        {
                            cls.f010_shomeidate = getCellValueToString(cell);
                            cls.shomeidatead = cngWarekiToAD(cls.f010_shomeidate);
                        
                        }
                        if (c == 10)
                        {
                            cls.f011_uketsukedate = getCellValueToString(cell);
                            cls.uketsukedatead = cngWarekiToAD(cls.f011_uketsukedate);
                          
                        }
                        if (c == 11)
                        {
                            cls.f012_biko = getCellValueToString(cell);

                            cls.cym = _cym;
                         

                        }

                    }
                    if (cls.f004_hihonum != string.Empty)
                    {
                        if (!DB.Main.Insert<imp_ahk_nyukyo>(cls, tran)) return false;
                    }
                    wf.InvokeValue++;
                }

                #endregion

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {                
                if(dataImport.wb !=null) dataImport.wb.Close();
                
            }

        }


        /// <summary>
        /// 居住地現況届提出一覧 
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool import_ahk_kyojuchi(int _cym, WaitForm wf, string strFileName)
        {
          
            DB.Transaction tran = new DB.Transaction(DB.Main);

            
       

            try
            {
                using (DB.Command cmd = new DB.Command($"delete from imp_ahk_kyojuchi where cym={dataImport.intcym}", tran))
                {
                    cmd.TryExecuteNonQuery();
                }

                #region closedxml
                //ファイルを開けない時点で無理

                //ClosedXML.Excel.XLWorkbook wkbook = null;
                ////ブック開く                
                //wkbook = new ClosedXML.Excel.XLWorkbook(strFileName);

                ////シート
                //ClosedXML.Excel.IXLWorksheet sht = null;

                ////シート存在確認
                //if (!wkbook.TryGetWorksheet("居住地現況届提出一覧 ", out sht))                               
                //{
                //    System.Windows.Forms.MessageBox.Show($"[居住地現況届提出一覧 ]シートがありません。処理を終了します"
                //        ,System.Windows.Forms.Application.ProductName
                //        ,System.Windows.Forms.MessageBoxButtons.OK
                //        ,System.Windows.Forms.MessageBoxIcon.Exclamation);
                //    return false;
                //}
                //int cols = 13;
                ////取得範囲設定
                //ClosedXML.Excel.IXLRange rng = sht.Range(2,1,sht.Column(1).LastCellUsed().Address.RowNumber, cols);

                ////ファイルチェック
                //if (checkImportFile(sht, true, 3, cols, 3) != checkState.ok) return false;

                //wf.SetMax(sht.Column(1).LastCellUsed().Address.RowNumber);
                //wf.InvokeValue = 0;


                //for(int r = 1; r <=sht.LastCellUsed().Address.RowNumber; r++)
                //{

                //    ClosedXML.Excel.IXLRow row = sht.Row(r);
                //    imp_ahk_kyojuchi cls = new imp_ahk_kyojuchi();

                //    for (int c = 1; c <= row.LastCellUsed().Address.ColumnNumber; c++)
                //    {

                //        if (!long.TryParse(row.Cell(4).Value.ToString(), out long tmp)) break;
                //        if (row.Cell(4).Value.ToString() == string.Empty) break;
                //        if (c == 1)
                //        {
                //            wf.LogPrint($"居住地現況届提出一覧 取得 {row.Cell(c).Value.ToString()}");
                //            cls.f001_number = row.Cell(c).Value.ToString();
                //            wf.InvokeValue++;

                //        }

                //        cls.cym = _cym;

                //        if (c == 2) cls.f002_clinicname = row.Cell(c).Value.ToString();
                //        if (c == 3) cls.f003_insurer = row.Cell(c).Value.ToString();
                //        if (c == 4) cls.f004_hihonum = row.Cell(c).Value.ToString();
                //        if (c == 5) cls.f005_pname = row.Cell(c).Value.ToString();
                //        if (c == 6 && row.Cell(c).Value.ToString().Trim()!=string.Empty)
                //        {
                //            cls.f006_pbirthday = row.Cell(c).GetDateTime().ToJDateShortStr();
                //            if (DateTime.TryParse(row.Cell(c).GetDateTime().ToShortDateString(), out DateTime tmpf006)) cls.pbirthdayad = tmpf006;
                //            else { }
                //        }
                //        if (c == 7) cls.f007_curradd = row.Cell(c).Value.ToString();
                //        if (c == 8) cls.f008_mainname = row.Cell(c).Value.ToString();
                //        if (c == 9) cls.f009_zokugara = row.Cell(c).Value.ToString();
                //        if (c == 10) cls.f010_tenkyodate = row.Cell(c).Value.ToString();
                //        if (c == 11) cls.f011_shomeidate = row.Cell(c).Value.ToString();                        
                //        if (c == 12) cls.f012_uketsukedate = row.Cell(c).Value.ToString();
                //        if (c == 13) cls.f013_biko = row.Cell(c).Value.ToString();


                //    }

                //    if (cls.f004_hihonum != string.Empty) 
                //        if (!DB.Main.Insert<imp_ahk_kyojuchi>(cls, tran)) return false;

                //}

                //tran.Commit();
                //return true;
                #endregion


                #region npoiコード
                dataImport.wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                NPOI.SS.UserModel.ISheet ws = dataImport.wb.GetSheet("居住地現況届提出一覧 ");
                if (ws == null)
                {
                    System.Windows.Forms.MessageBox.Show($"[居住地現況届提出一覧]シートがありません。処理を終了します");
                    return false;
                }
                wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                //ファイルチェック
                if (dataImport.checkImportFile(ws, true, 3, 13, 3) != dataImport.checkState.ok) return false;
                
                for (int r = 0; r < ws.LastRowNum; r++)

                {
                    NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);
                    
                    //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす
                    if (row1 == null) continue;
                    
                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    imp_ahk_kyojuchi cls = new imp_ahk_kyojuchi();
                    for (int c = 0; c < row1.LastCellNum; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //1列目が数字でない場合は飛ばす
                        if (c == 0)
                        {
                            if (!int.TryParse(getCellValueToString(cell), out int tmp)) break;
                        }

                        if (c == 0)
                        {
                            wf.LogPrint($"居住地現況届提出一覧 取得 {getCellValueToString(cell)}");
                            cls.f001_number = getCellValueToString(cell);
                        }
                        if (c == 1) cls.f002_clinicname = getCellValueToString(cell);
                        if (c == 2) cls.f003_insurer = getCellValueToString(cell);
                        if (c == 3) cls.f004_hihonum = getCellValueToString(cell);
                        if (c == 4) cls.f005_pname = getCellValueToString(cell);
                        if (c == 5)
                        {
                            //生年月日だけはシリアル値でしか取れない
                            if (long.TryParse(getCellValueToString(cell),out long tmp))
                            {
                                DateTime dtBirth = DateTime.FromOADate(cell.NumericCellValue);
                                cls.pbirthdayad = dtBirth;
                                cls.f006_pbirthday = 
                                    DateTimeEx.GetInitialEraJpYear(int.Parse($"{dtBirth.Year.ToString()}{dtBirth.Month.ToString().PadLeft(2, '0')}"))
                                    +$".{dtBirth.Month.ToString().PadLeft(2, '0')}.{dtBirth.Day.ToString().PadLeft(2, '0')}";
                            }
                        }
                        if (c == 6) cls.f007_curradd = getCellValueToString(cell);
                        if (c == 7) cls.f008_mainname = getCellValueToString(cell);
                        if (c == 8) cls.f009_zokugara = getCellValueToString(cell);
                        if (c == 9) {
                            cls.f010_tenkyodate = getCellValueToString(cell);
                            cls.tenkyodatead = cngWarekiToAD(cls.f010_tenkyodate);
                    
                        }
                        if (c == 10)
                        {
                            cls.f011_shomeidate = getCellValueToString(cell);
                            cls.shomeidatead = cngWarekiToAD(cls.f011_shomeidate);

                        }
                        if (c == 11)
                        {
                            cls.f012_uketsukedate = getCellValueToString(cell);
                            cls.uketsukedatead = cngWarekiToAD(cls.f012_uketsukedate);
                         
                        }
                        if (c == 12)
                        {
                            cls.f013_biko = getCellValueToString(cell);
                            cls.cym = _cym;
                            
                        }

                    }
                    if (cls.f004_hihonum != string.Empty)
                    {
                        if (!DB.Main.Insert<imp_ahk_kyojuchi>(cls, tran)) return false;
                    }
                    wf.InvokeValue++;
                }
                tran.Commit();
                return true;
                #endregion

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
                if (dataImport.wb !=null) dataImport.wb.Close();
            }

        }
        */
        #endregion

        /// <summary>
        /// あはき被保険者マスタ
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool import_ahk_hihomaster(int _cym, WaitForm wf, string strFileName)
        {
        
            DB.Transaction tran = new DB.Transaction(DB.Main);


            try
            {
                using (DB.Command cmd = new DB.Command($"delete from imp_ahk_hihomaster where cym={dataImport.intcym}", tran))
                {
                    cmd.TryExecuteNonQuery();
                }

                #region closedxml
                //ファイルを開けない時点で無理
                //ClosedXML.Excel.XLWorkbook wkbook = null;
                ////ブック開く                
                //wkbook = new ClosedXML.Excel.XLWorkbook(strFileName);

                ////シート
                //ClosedXML.Excel.IXLWorksheet sht = null;

                ////シート存在確認
                //if (!wkbook.TryGetWorksheet("_18_鍼灸あんま用被保険者マスタ", out sht))
                //{
                //    System.Windows.Forms.MessageBox.Show($"[_18_鍼灸あんま用被保険者マスタ]シートがありません。処理を終了します"
                //        , System.Windows.Forms.Application.ProductName
                //        , System.Windows.Forms.MessageBoxButtons.OK
                //        , System.Windows.Forms.MessageBoxIcon.Exclamation);
                //    return false;
                //}
                //int cols = 13;
                ////取得範囲設定
                //ClosedXML.Excel.IXLRange rng = sht.Range(2, 1, sht.Column(1).LastCellUsed().Address.RowNumber, cols);

                ////ファイルチェック
                //if (checkImportFile(sht, true,2, cols, 0) != checkState.ok) return false;

                //wf.SetMax(sht.Column(1).LastCellUsed().Address.RowNumber);
                //wf.InvokeValue = 0;



                //for (int r = 1; r <= sht.LastCellUsed().Address.RowNumber; r++)
                //{

                //    ClosedXML.Excel.IXLRow row = sht.Row(r);
                //    imp_ahk_hihomaster cls = new imp_ahk_hihomaster();

                //    for (int c = 1; c <= row.LastCellUsed().Address.ColumnNumber; c++)
                //    {

                //        if (!long.TryParse(row.Cell(4).Value.ToString(), out long tmp)) break;
                //        if (row.Cell(4).Value.ToString() == string.Empty) break;
                //        if (c == 1)
                //        {
                //            wf.LogPrint($"_18_鍼灸あんま用被保険者マスタ 取得 {row.Cell(c).Value.ToString()}");
                //            cls.f001_hihonum = row.Cell(c).Value.ToString();
                //            wf.InvokeValue++;

                //        }

                //        cls.cym = _cym;

                //        if (c == 2) cls.f002_hihokana = row.Cell(c).Value.ToString();
                //        if (c == 3) cls.f003_hihoname = row.Cell(c).Value.ToString();
                //        if (c == 4) cls.f004_hihozip= row.Cell(c).Value.ToString();
                //        if (c == 5) cls.f005_hihoadd = row.Cell(c).Value.ToString();
                //        if (c == 6) cls.f006_sendzip=row.Cell(c).Value.ToString();                        
                //        if (c == 7) cls.f007_sendadd = row.Cell(c).Value.ToString();
                //        if (c == 8) cls.f008_sendname = row.Cell(c).Value.ToString();
                //        if (c == 9) cls.f009_newzip = row.Cell(c).Value.ToString();
                //        if (c == 10) cls.f010_newadd = row.Cell(c).Value.ToString();
                //        if (c == 11) cls.f011_losecode = row.Cell(c).Value.ToString();
                //        if (c == 12) cls.f012_loseziyu = row.Cell(c).Value.ToString();
                //        if (c == 13) cls.f013_losedate = row.Cell(c).Value.ToString();


                //    }

                //    if (cls.f001_hihonum != string.Empty)
                //        if (!DB.Main.Insert<imp_ahk_hihomaster>(cls, tran)) return false;

                //}
                #endregion

                #region NPOI

                dataImport.wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
                if (!dataImport.wb.GetSheetAt(0).SheetName.Contains($"鍼灸あんま用被保険者マスタ"))
                {
                    System.Windows.Forms.MessageBox.Show($"[鍼灸あんま用被保険者マスタ]シートがありません。処理を終了します");
                    return false;
                }

                NPOI.SS.UserModel.ISheet ws = dataImport.wb.GetSheetAt(0);
                wf.SetMax(ws.LastRowNum + 1); //20221018_1 ito /////wf.SetMax(ws.LastRowNum);
                wf.InvokeValue = 0;

                //ファイルチェック
                // if (checkImportFile(ws, true, 0, 13, 0) != checkState.ok) return false;

                for (int r = 0; r < ws.LastRowNum + 1; r++)//20221018_1 ito /////for (int r = 0; r < ws.LastRowNum; r++)

                {
                    NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

                    //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす
                    if (row1 == null) continue;

                    if (CommonTool.WaitFormCancelProcess(wf, tran)) return false;

                    imp_ahk_hihomaster cls = new imp_ahk_hihomaster();
                    for (int c = 0; c < row1.Cells.Count; c++)
                    {
                        NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);


                        //1列目が数字でない場合は飛ばす
                        if (c == 0)
                        {
                            if (!long.TryParse(dataImport.getCellValueToString(cell), out long tmp)) break;

                            wf.LogPrint($"あはき被保険者マスタ 取得 {dataImport.getCellValueToString(cell)}");
                            cls.f001_hihonum = dataImport.getCellValueToString(cell);
                            cls.cym = _cym;
                        }

                        if (c == 1) cls.f002_hihokana = dataImport.getCellValueToString(cell);
                        if (c == 2) cls.f003_hihoname = dataImport.getCellValueToString(cell);
                        if (c == 3) cls.f004_hihozip = dataImport.getCellValueToString(cell);
                        if (c == 4) cls.f005_hihoadd = dataImport.getCellValueToString(cell);
                        if (c == 5) cls.f006_sendzip = dataImport.getCellValueToString(cell);
                        if (c == 6) cls.f007_sendadd = dataImport.getCellValueToString(cell);
                        if (c == 7) cls.f008_sendname = dataImport.getCellValueToString(cell);
                        if (c == 8) cls.f009_newzip = dataImport.getCellValueToString(cell);
                        if (c == 9) cls.f010_newadd = dataImport.getCellValueToString(cell);
                        if (c == 10) cls.f011_losecode = dataImport.getCellValueToString(cell);
                        if (c == 11) cls.f012_loseziyu = dataImport.getCellValueToString(cell);
                        if (c == 12) cls.f013_losedate = dataImport.getCellValueToString(cell);


                    }
                    if (cls.f001_hihonum != string.Empty)
                    {
                        if (!DB.Main.Insert<imp_ahk_hihomaster>(cls, tran)) return false;
                    }
                    wf.InvokeValue++;
                }
                #endregion


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                
                if(dataImport.wb !=null) dataImport.wb.Close();
               
            }

        }

        ///// <summary>
        ///// 和暦西暦変換
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //private static DateTime cngWarekiToAD(string str)
        //{
        //    string strPtn = "^[T|S|H|R][0-9]{1,2}[.][0-9]{1,2}[.][0-9]{1,2}$";

        //    if (System.Text.RegularExpressions.Regex.IsMatch(str, strPtn))
        //    {
        //        string strf011 = str.Substring(1);
        //        int y = DateTimeEx.GetAdYearFromHs(int.Parse(strf011.Split('.')[0].PadLeft(2, '0') + strf011.Split('.')[1].PadLeft(2, '0')));

        //        return new DateTime(
        //            y,
        //            int.Parse(str.Split('.')[1]),
        //            int.Parse(str.Split('.')[2]));
        //    }
        //    return DateTime.MinValue;
        //}

        /// <summary>
        /// あはきインポートメイン
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_main(int _cym)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            dataImport.intcym = _cym;

            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileNameAHKClinic = frm.strFileAHKClinic;
            string strFileNameAHKNyukyo = frm.strFileAHKNyukyo;
            string strFileNameAHK = frm.strFileAHK;
            string strFileNameAHKHiho = frm.strFileAHKHiho;
            //20220905_1 ito st /////
            string strFileNameJyu = frm.strFileJYU;
            string strFileNameJyuClinic = frm.strFileJyuClinic;
            string strFileNameJyuNyukyo = frm.strFileJyuNyukyo;
            //20220905_1 ito ed /////


            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                //20220905_1 ito st /////
                if (strFileNameJyuClinic != string.Empty)
                {
                    wf.LogPrint("柔整医療機関マスタインポート");
                    if (!dataImport.import_clinic(_cym, wf, strFileNameJyuClinic, APP_TYPE.柔整))
                    {
                        wf.LogPrint("柔整医療機関マスタインポート異常終了");
                        return false;
                    }

                    wf.LogPrint("柔整医療機関マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("柔整医療機関マスタのファイルが指定されていないため処理しない");
                }

                if (strFileNameJyuNyukyo != string.Empty)
                {
                    wf.LogPrint("柔整入居証明書インポート");
                    if (!dataImport.import_nyukyo(_cym, wf, strFileNameJyuNyukyo, APP_TYPE.柔整))
                    {
                        wf.LogPrint("柔整入居証明書インポート異常終了");
                        return false;
                    }

                    wf.LogPrint("柔整入居証明書インポート終了");

                    wf.LogPrint("柔整居住地現況届提出一覧インポート");
                    if (!dataImport.import_kyojuchi(_cym, wf, strFileNameJyuNyukyo, APP_TYPE.柔整))
                    {
                        wf.LogPrint("柔整居住地現況届提出一覧インポート異常終了");
                        return false;
                    }

                    wf.LogPrint("柔整居住地現況届提出一覧インポート終了");
                }
                else
                {
                    wf.LogPrint("柔整入居証明書・居住地現況届提出一覧のファイルが指定されていないため処理しない");
                }

                if (strFileNameJyu != string.Empty)
                {
                    wf.LogPrint("柔整データインポート");
                    if (!dataImport_JYU.import_jyu(_cym, wf, strFileNameJyu))
                    {
                        wf.LogPrint("柔整データインポート異常終了");
                        return false;
                    }

                    wf.LogPrint("柔整データインポート終了");
                }
                else
                {
                    wf.LogPrint("柔整データのファイルが指定されていないため処理しない");
                }
                //20220905_1 ito ed /////

                if (strFileNameAHKClinic != string.Empty)
                {
                    wf.LogPrint("あはき医療機関マスタインポート");
                    if (!dataImport.import_clinic(_cym, wf, strFileNameAHKClinic, APP_TYPE.あんま))
                    {
                        wf.LogPrint("あはき医療機関マスタインポート異常終了");
                        return false;
                    }

                    wf.LogPrint("あはき医療機関マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("あはき医療機関マスタのファイルが指定されていないため処理しない");
                }

                if (strFileNameAHKNyukyo != string.Empty)
                {
                    wf.LogPrint("あはき入居証明書インポート");
                    if (!dataImport.import_nyukyo(_cym, wf, strFileNameAHKNyukyo, APP_TYPE.あんま))
                    {
                        wf.LogPrint("あはき入居証明書インポート異常終了");
                        return false;
                    }

                    wf.LogPrint("あはき入居証明書インポート終了");

                    wf.LogPrint("あはき居住地現況届提出一覧インポート");
                    if (!dataImport.import_kyojuchi(_cym, wf, strFileNameAHKNyukyo, APP_TYPE.あんま))
                    {
                        wf.LogPrint("あはき居住地現況届提出一覧インポート異常終了");
                        return false;
                    }

                    wf.LogPrint("あはき居住地現況届提出一覧インポート終了");
                }
                else
                {
                    wf.LogPrint("あはき入居証明書・居住地現況届提出一覧のファイルが指定されていないため処理しない");
                }

                if (strFileNameAHK != string.Empty)
                {
                    wf.LogPrint("あはきデータインポート");
                    if (!dataImport_AHK.import_ahk(_cym, wf, strFileNameAHK))
                    {
                        wf.LogPrint("あはきデータインポート異常終了");
                        return false;
                    }

                    wf.LogPrint("あはきデータインポート終了");
                }
                else
                {
                    wf.LogPrint("あはきデータのファイルが指定されていないため処理しない");
                }


                if (strFileNameAHKHiho != string.Empty)
                {
                    wf.LogPrint("あはき被保険者マスタインポート");
                    if (!dataImport_AHK.import_ahk_hihomaster(_cym, wf, strFileNameAHKHiho))
                    {
                        wf.LogPrint("あはき被保険者マスタインポート異常終了");
                        return false;
                    }

                    wf.LogPrint("あはき被保険者マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("あはき被保険者マスタのファイルが指定されていないため処理しない");
                }
                


                System.Windows.Forms.MessageBox.Show("終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {

            string strsql =
                "update application set    " +
                " inum         = ahk.insnum" +                   //保険者番号

                //20200817180739 furukawa st ////////////////////////
                //被保険者証記号番号のあいだはハイフン
                
                ",hnum		   = ahk.hihomark || '-' || ahk.hihonum " +    //証記号 //証番号                
                //",hnum		   = ahk.hihomark || ahk.hihonum " +    //証記号 //証番号          
                //20200817180739 furukawa ed ////////////////////////


                ",ym           = cast(ahk.ymad as int) " +                  //診療年月西暦
                ",sid          = ahk.clinicnum" +                           //機関コード
                ",sname        = ahk.clinicname" +                          //医療機関名
                ",pbirthday    = ahk.pbirthad" +                            //受療者生年月日西暦
                ",psex         = cast(ahk.psex as int) " +                  //性別
                //",apptype    = ahk.clinicnum" +                           //種別２ あんま・鍼灸 →画像登録時にとる
                ",aratio       = cast(ahk.ratio as int) " +                 //給付割合                
                ",acounteddays = cast(ahk.counteddays as int) " +           //実日数
                ",atotal       = cast(ahk.total as int) " +                 //決定点数
                ",comnum       = ahk.comnum" +                              //レセプト全国共通キー
                ",hzip         = ahk.hihozip" +                             //郵便番号
                ",haddress     = ahk.hihoadd" +                             //住所
               // ",hname     = ahk.hihoname" +                               //氏名

                ",pname     = ahk.pname " +                             //20200818185850 furukawa 受療者名納品データに必要なので入れとく
                                                                        

                ",taggeddatas	              " +
                "       = 'GeneralString1:\"' || ahk.atenanum || '\"' " +//宛名番号


                " from dataimport_ahk ahk " +
                " where " +
                $" application.comnum=ahk.comnum and application.cym={cym}";

            DB.Command cmd = new DB.Command(DB.Main, strsql,true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        private partial class names
        {
            public string comnum { get; set; } = string.Empty;
            public string zip { get; set; } = string.Empty;
            public string address { get; set; } = string.Empty;
            public string name { get; set; } = string.Empty;
            public string kana { get; set; } = string.Empty;
        }

        /// <summary>
        /// 提供データを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        //private static bool EntryExcelData(string strFileName, WaitForm wf)
        //{
            
        //    wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            
        //    NPOI.SS.UserModel.ISheet ws = wb.GetSheet("給付記録に並び替え");

        //    wf.LogPrint($"住所、受療者取得");
        //    List<names> lstname = new List<names>();

        //    //20200817200745 furukawa st ////////////////////////
        //    //最終行が拾えてなかった
            
        //    for (int r = 1; r <= ws.LastRowNum; r++)
        //    //for (int r = 1; r < ws.LastRowNum; r++)
        //    //20200817200745 furukawa ed ////////////////////////

        //    {
        //        NPOI.SS.UserModel.IRow row1 = ws.GetRow(r);

        //        //20210610151011 furukawa st ////////////////////////
        //        //空白でも余計なものが混じっていると、最終行数を間違えるので行=nullの場合は飛ばす

        //        if (row1 == null) continue;
        //        //20210610151011 furukawa ed ////////////////////////


        //        names n = new names();
        //        for (int c = 0; c < row1.Cells.Count; c++)
        //        {                    
        //            NPOI.SS.UserModel.ICell cell = row1.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                   

        //            if (c == 57)
        //            {
        //                if (getCellValueToString(cell) == string.Empty)
        //                {
        //                    System.Windows.Forms.MessageBox.Show("住所、受療者が指定の位置にありません");
        //                    wb.Close();
        //                    return false;
        //                }
        //            }
        //            if (c == 24) n.comnum = getCellValueToString(cell);               //レセプト全国共通キー
        //            if (c == 57) n.zip = getCellValueToString(cell);                  //郵便番号
        //            if (c == 58) n.address = getCellValueToString(cell);              //住所
        //            if (c == 59) n.name = getCellValueToString(cell);                 //氏名(受療者名)
        //            if (c == 60) n.kana = getCellValueToString(cell);                 //氏名（受療者名）kana

        //        }
        //        lstname.Add(n);
        //    }



        //    ws = wb.GetSheet("療養費審査結果一覧");

        //    DB.Transaction tran = new DB.Transaction(DB.Main);

        //    DB.Command cmd = new DB.Command($"delete from dataimport_ahk where cym={intcym}", tran);
        //    cmd.TryExecuteNonQuery();

        //    wf.InvokeValue = 0;
        //    wf.LogPrint($"{intcym}削除");

        //    int col = 0;

        //    try
        //    {
        //        //20200817180644 furukawa st ////////////////////////
        //        //最終行を最大値とする
                
        //        wf.SetMax(ws.LastRowNum);
        //        //wf.SetMax(ws.LastRowNum - 1);
        //        //20200817180644 furukawa ed ////////////////////////

        //        //20200817202130 furukawa st ////////////////////////
        //        //最終行が拾えてなかった
                
        //        if (lstname.Count != ws.LastRowNum)
        //        //if (lstname.Count != ws.LastRowNum-1)
        //        //20200817202130 furukawa ed ////////////////////////
        //        {
        //            System.Windows.Forms.MessageBox.Show("住所氏名とデータの行数が一致しません");
        //            wb.Close();
        //            return false;
        //        }
        //            //int c = 0;

        //            //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
        //            int cellcnt = ws.GetRow(1).Cells.Count;

        //        //20200817180603 furukawa st ////////////////////////
        //        //最終行が拾えてなかった
                
        //        for (int r = 1; r <= ws.LastRowNum; r++)
        //        //for (int r = 1; r < ws.LastRowNum; r++)
        //        //20200817180603 furukawa ed ////////////////////////
        //        {
        //            dataImport_AHK imp = new dataImport_AHK();

        //            NPOI.SS.UserModel.IRow row = ws.GetRow(r);

        //            imp.cym = intcym;//	メホール上の処理年月 メホール管理用    

        //            int inttotal = 0;
        //            int intcharge = 0;
                    

        //            for (int c = 0; c < cellcnt; c++)
        //            {
        //                NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        //                col = c;
        //                //2枚めのシートから取得するとき
        //                if (c == 30) imp.insnum = getCellValueToString(cell);                      //保険者番号
        //                if (c == 6) imp.hihomark = getCellValueToString(cell);                     //証記号
        //                if (c == 7) imp.hihonum = getCellValueToString(cell);                      //証番号

        //                if (c == 15) imp.pname = getCellValueToString(cell);                        //20200818191443 furukawa 受療者名納品データに必要
                                                                                                    

        //                //20200818143002 furukawa st ////////////////////////
        //                //宛名番号ぬけてた

        //                if (c == 13) imp.atenanum = getCellValueToString(cell);                      //宛名番号
        //                //20200818143002 furukawa ed ////////////////////////


        //                if (c == 21)
        //                {
        //                    imp.shinryoym = getCellValueToString(cell);     //診療年月
        //                    DateTimeEx.TryGetYearMonthTimeFromJdate(imp.shinryoym + ".01",out DateTime tmp);
        //                    imp.ymad = (tmp.Year*100+tmp.Month).ToString();                         //診療年月西暦

        //                }
        //                if (c == 25) imp.clinicnum = getCellValueToString(cell);                    //機関コード
        //                if (c == 26) imp.clinicname = getCellValueToString(cell);                   //医療機関名

        //                if (c == 44)
        //                {
        //                    imp.pbirthday = getCellValueToString(cell);                              //生年月日
        //                    imp.pbirthad =DateTime.Parse(cell.DateCellValue.ToString());             //生年月日西暦

        //                }

        //                if (c == 45) imp.psex = getCellValueToString(cell);                       //性別
        //                if (c == 32) imp.kind = getCellValueToString(cell);                       //種別２
                        
                        
        //                if (c == 27)
        //                {
        //                    inttotal = int.Parse(getCellValueToString(cell));                       //給付割合(合計金額)
        //                }
        //                if (c == 29)
        //                {
        //                    intcharge = int.Parse(getCellValueToString(cell));

        //                    decimal dectotal = decimal.Parse(inttotal.ToString());
        //                    decimal deccharge = decimal.Parse(intcharge.ToString());
        //                    decimal decratio =  deccharge/ dectotal;

        //                    imp.ratio = int.Parse(Math.Truncate(decratio * 10).ToString());           //給付割合（請求金額）
        //                }

        //                if (c == 38) imp.shinsaym = getCellValueToString(cell);                    //審査年月
        //                if (c == 22) imp.counteddays = int.Parse(getCellValueToString(cell));      //実日数
        //                if (c == 27) imp.total = int.Parse(getCellValueToString(cell));            //決定点数
        //                if (c == 28) imp.partial = int.Parse(getCellValueToString(cell));          //一部負担金
        //                if (c == 29) imp.charge = int.Parse(getCellValueToString(cell));           //請求金額

        //                if (c == 10)
        //                {
        //                    string tmp= getCellValueToString(cell);
        //                    if(System.Text.RegularExpressions.Regex.IsMatch(tmp,"[E\\+]"))
        //                    {
        //                        System.Windows.Forms.MessageBox.Show("レセプト全国共通キーが壊れています。インポートデータを修正してください");
        //                        return false;
        //                    }
        //                    imp.comnum = getCellValueToString(cell);                      //レセプト全国共通キー
        //                }

        //                //レセプト全国共通キーで合致させる
        //                if (c == 11)
        //                {
        //                    foreach (names n in lstname)
        //                    {
        //                        if (n.comnum == imp.comnum)
        //                        {
        //                            imp.hihozip = lstname[r - 1].zip.ToString();                     //郵便番号
        //                            imp.hihoadd = lstname[r - 1].address.ToString();                 //住所

        //                            //imp.hihoname = lstname[r - 1].name.ToString();                     //氏名

        //                            //20200903130024 furukawa st ////////////////////////
        //                            //1シート目にメディで入れている「氏名」は受療者名                                    
        //                            imp.pname= lstname[r - 1].name.ToString();//受療者名
        //                            //20200903130024 furukawa ed ////////////////////////


        //                            break;
        //                        }
        //                    }
        //                }
        //                //if (lstname[r - 1].comnum == imp.comnum)
        //                //{
        //                //    if (c == 11) imp.hihozip = lstname[r - 1].zip.ToString();                     //郵便番号
        //                //    if (c == 11) imp.hihoadd = lstname[r - 1].address.ToString();                      //住所
        //                //    if (c == 11) imp.hihoname = lstname[r - 1].name.ToString();                     //氏名
        //                //}

        //                if (c == 38) imp.shinsaymad = getCellValueToString(cell);  //審査年月西暦




        //                //if (c == 0) imp.insnum = getCellValueToString(cell);                       //保険者番号
        //                //if (c == 1) imp.hihomark = getCellValueToString(cell);                     //証記号
        //                //if (c == 2) imp.hihonum = getCellValueToString(cell);                      //証番号
        //                //if (c == 4) imp.shinryoym = 
        //                //        System.Text.RegularExpressions.Regex.Replace(getCellValueToString(cell), "[R]([0-9]{2})\\.([0-9]{2})", "5$1$2");
        //                //if (c == 5) imp.clinicnum = getCellValueToString(cell);                    //機関コード
        //                //if (c == 6) imp.clinicname = getCellValueToString(cell);                   //医療機関名
        //                //if (c == 10) imp.pbirthday = getCellValueToString(cell);                   //生年月日
        //                //if (c == 11) imp.psex = getCellValueToString(cell);                        //性別
        //                //if (c == 13) imp.kind = getCellValueToString(cell);                        //種別２
        //                //if (c == 16) imp.ratio = int.Parse(getCellValueToString(cell));            //給付割合
        //                //if (c == 20) imp.shinsaym = getCellValueToString(cell);                    //審査年月
        //                //if (c == 21) imp.counteddays = int.Parse(getCellValueToString(cell));      //実日数
        //                //if (c == 22) imp.total = int.Parse(getCellValueToString(cell));            //決定点数
        //                //if (c == 24) imp.comnum = getCellValueToString(cell);                      //レセプト全国共通キー
        //                //if (c == 57) imp.hihozip = getCellValueToString(cell);                     //郵便番号
        //                //if (c == 58) imp.hihoadd = getCellValueToString(cell);                     //住所
        //                //if (c == 59) imp.hihoname = getCellValueToString(cell);                    //氏名

        //                //if (c == 20) imp.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.shinsaym)).ToString();  //審査年月西暦
        //                //if (c == 4) imp.ymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.shinryoym)).ToString();         //診療年月西暦
        //                //if (c == 10) imp.pbirthad = DateTimeEx.GetDateFromJstr7(imp.pbirthday);                                //受療者生年月日西暦

        //            }

        //            wf.InvokeValue++;
        //            wf.LogPrint($"あはきデータ　レセプト全国共通キー:{imp.comnum}");

        //            if (!DB.Main.Insert<dataImport_AHK>(imp, tran)) return false;
        //        }

        //        tran.Commit();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col +"\r\n" + ex.Message);
        //        tran.Rollback();
        //        return false;
        //    }
        //    finally
        //    {
                
        //        wb.Close();
        //    }

        //}

        ///// <summary>
        ///// セルの値を取得
        ///// </summary>
        ///// <param name="cell">セル</param>
        ///// <returns>string型</returns>
        //private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        //{
        //    switch (cell.CellType)
        //    {                
        //        case NPOI.SS.UserModel.CellType.String:
        //            return cell.StringCellValue;
        //        case NPOI.SS.UserModel.CellType.Numeric:
        //            return cell.NumericCellValue.ToString();
                    
        //        case NPOI.SS.UserModel.CellType.Formula:
        //            if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
        //            {
        //                return cell.StringCellValue;
        //            }
        //            else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
        //            {
        //                return cell.NumericCellValue.ToString();
                       
        //            }
        //            else return string.Empty;
                    
        //        default:
        //            return string.Empty;

        //    }


        //}



    }


    /// <summary>
    /// 既出テーブルだが使わないかも
    /// (既に出力した人は出さない場合があるので、その人を記録しておくテーブル)
    /// </summary>
    partial class hihoSent
    {
        public string hMark           {get;set;}=string.Empty;      //記号
        public string hNum            {get;set;}=string.Empty;      //番号
        public string psex            {get;set;}=string.Empty;      //性別
        public string pbirthday       {get;set;}=string.Empty;      //受療者生年月日
        public string pname           {get;set;}=string.Empty;      //受療者名
        public string hname           {get;set;}=string.Empty;      //被保険者名
        public string family { get; set; } = string.Empty;          //本人家族区分 2:本人6:家族    

    }
}
