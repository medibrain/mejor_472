﻿namespace Mejor.EhimeKoiki
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxJyuClnic = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnJYU = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxJYU = new System.Windows.Forms.TextBox();
            this.btnJyuNyukyo = new System.Windows.Forms.Button();
            this.btnJyuClinic = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxJyuNyukyo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImpJyusei = new System.Windows.Forms.Button();
            this.gbA = new System.Windows.Forms.GroupBox();
            this.btnAHKNyukyo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAHkNyukyo = new System.Windows.Forms.TextBox();
            this.btnAHKHiho = new System.Windows.Forms.Button();
            this.btnAHK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAHKHiho = new System.Windows.Forms.TextBox();
            this.textBoxAHK = new System.Windows.Forms.TextBox();
            this.btnAHKClinic = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAHkClnic = new System.Windows.Forms.TextBox();
            this.gbJ.SuspendLayout();
            this.gbA.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxJyuClnic
            // 
            this.textBoxJyuClnic.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJyuClnic.Location = new System.Drawing.Point(180, 30);
            this.textBoxJyuClnic.Multiline = true;
            this.textBoxJyuClnic.Name = "textBoxJyuClnic";
            this.textBoxJyuClnic.Size = new System.Drawing.Size(512, 24);
            this.textBoxJyuClnic.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.Controls.Add(this.btnJYU);
            this.gbJ.Controls.Add(this.label7);
            this.gbJ.Controls.Add(this.textBoxJYU);
            this.gbJ.Controls.Add(this.btnJyuNyukyo);
            this.gbJ.Controls.Add(this.btnJyuClinic);
            this.gbJ.Controls.Add(this.label2);
            this.gbJ.Controls.Add(this.textBoxJyuNyukyo);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxJyuClnic);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(43, 12);
            this.gbJ.Name = "gbJ";
            this.gbJ.Size = new System.Drawing.Size(750, 181);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            this.gbJ.Text = "柔整";
            // 
            // btnJYU
            // 
            this.btnJYU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJYU.Location = new System.Drawing.Point(700, 130);
            this.btnJYU.Name = "btnJYU";
            this.btnJYU.Size = new System.Drawing.Size(40, 24);
            this.btnJYU.TabIndex = 6;
            this.btnJYU.Text = "...";
            this.btnJYU.UseVisualStyleBackColor = true;
            this.btnJYU.Click += new System.EventHandler(this.btnJYU_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "柔整データ(CSV)";
            // 
            // textBoxJYU
            // 
            this.textBoxJYU.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJYU.Location = new System.Drawing.Point(180, 130);
            this.textBoxJYU.Multiline = true;
            this.textBoxJYU.Name = "textBoxJYU";
            this.textBoxJYU.Size = new System.Drawing.Size(512, 24);
            this.textBoxJYU.TabIndex = 4;
            // 
            // btnJyuNyukyo
            // 
            this.btnJyuNyukyo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJyuNyukyo.Location = new System.Drawing.Point(700, 80);
            this.btnJyuNyukyo.Name = "btnJyuNyukyo";
            this.btnJyuNyukyo.Size = new System.Drawing.Size(40, 24);
            this.btnJyuNyukyo.TabIndex = 3;
            this.btnJyuNyukyo.Text = "...";
            this.btnJyuNyukyo.UseVisualStyleBackColor = true;
            this.btnJyuNyukyo.Click += new System.EventHandler(this.btnJyuNyukyo_Click);
            // 
            // btnJyuClinic
            // 
            this.btnJyuClinic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJyuClinic.Location = new System.Drawing.Point(700, 30);
            this.btnJyuClinic.Name = "btnJyuClinic";
            this.btnJyuClinic.Size = new System.Drawing.Size(40, 24);
            this.btnJyuClinic.TabIndex = 3;
            this.btnJyuClinic.Text = "...";
            this.btnJyuClinic.UseVisualStyleBackColor = true;
            this.btnJyuClinic.Click += new System.EventHandler(this.btnJyuClinic_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "入居証明・居住地(Excel)";
            // 
            // textBoxJyuNyukyo
            // 
            this.textBoxJyuNyukyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJyuNyukyo.Location = new System.Drawing.Point(180, 80);
            this.textBoxJyuNyukyo.Multiline = true;
            this.textBoxJyuNyukyo.Name = "textBoxJyuNyukyo";
            this.textBoxJyuNyukyo.Size = new System.Drawing.Size(512, 24);
            this.textBoxJyuNyukyo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "医療機関マスタ(CSV)";
            // 
            // btnImpJyusei
            // 
            this.btnImpJyusei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImpJyusei.Location = new System.Drawing.Point(714, 449);
            this.btnImpJyusei.Name = "btnImpJyusei";
            this.btnImpJyusei.Size = new System.Drawing.Size(115, 34);
            this.btnImpJyusei.TabIndex = 3;
            this.btnImpJyusei.Text = "取込";
            this.btnImpJyusei.UseVisualStyleBackColor = true;
            this.btnImpJyusei.Click += new System.EventHandler(this.btnImp_Click);
            // 
            // gbA
            // 
            this.gbA.Controls.Add(this.btnAHKNyukyo);
            this.gbA.Controls.Add(this.label3);
            this.gbA.Controls.Add(this.textBoxAHkNyukyo);
            this.gbA.Controls.Add(this.btnAHKHiho);
            this.gbA.Controls.Add(this.btnAHK);
            this.gbA.Controls.Add(this.label5);
            this.gbA.Controls.Add(this.label6);
            this.gbA.Controls.Add(this.textBoxAHKHiho);
            this.gbA.Controls.Add(this.textBoxAHK);
            this.gbA.Controls.Add(this.btnAHKClinic);
            this.gbA.Controls.Add(this.label4);
            this.gbA.Controls.Add(this.textBoxAHkClnic);
            this.gbA.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbA.Location = new System.Drawing.Point(43, 217);
            this.gbA.Name = "gbA";
            this.gbA.Size = new System.Drawing.Size(750, 220);
            this.gbA.TabIndex = 1;
            this.gbA.TabStop = false;
            this.gbA.Text = "あはき";
            // 
            // btnAHKNyukyo
            // 
            this.btnAHKNyukyo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAHKNyukyo.Location = new System.Drawing.Point(700, 80);
            this.btnAHKNyukyo.Name = "btnAHKNyukyo";
            this.btnAHKNyukyo.Size = new System.Drawing.Size(40, 24);
            this.btnAHKNyukyo.TabIndex = 15;
            this.btnAHKNyukyo.Text = "...";
            this.btnAHKNyukyo.UseVisualStyleBackColor = true;
            this.btnAHKNyukyo.Click += new System.EventHandler(this.btnAHKNyukyo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "入居証明・居住地(Excel)";
            // 
            // textBoxAHkNyukyo
            // 
            this.textBoxAHkNyukyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHkNyukyo.Location = new System.Drawing.Point(180, 80);
            this.textBoxAHkNyukyo.Multiline = true;
            this.textBoxAHkNyukyo.Name = "textBoxAHkNyukyo";
            this.textBoxAHkNyukyo.Size = new System.Drawing.Size(512, 24);
            this.textBoxAHkNyukyo.TabIndex = 13;
            // 
            // btnAHKHiho
            // 
            this.btnAHKHiho.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAHKHiho.Location = new System.Drawing.Point(700, 180);
            this.btnAHKHiho.Name = "btnAHKHiho";
            this.btnAHKHiho.Size = new System.Drawing.Size(40, 24);
            this.btnAHKHiho.TabIndex = 12;
            this.btnAHKHiho.Text = "...";
            this.btnAHKHiho.UseVisualStyleBackColor = true;
            this.btnAHKHiho.Click += new System.EventHandler(this.btnAHKHiho_Click);
            // 
            // btnAHK
            // 
            this.btnAHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAHK.Location = new System.Drawing.Point(700, 130);
            this.btnAHK.Name = "btnAHK";
            this.btnAHK.Size = new System.Drawing.Size(40, 24);
            this.btnAHK.TabIndex = 12;
            this.btnAHK.Text = "...";
            this.btnAHK.UseVisualStyleBackColor = true;
            this.btnAHK.Click += new System.EventHandler(this.btnAHK_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "被保険者マスタ(Excel)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "あはきデータ(CSV)";
            // 
            // textBoxAHKHiho
            // 
            this.textBoxAHKHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHKHiho.Location = new System.Drawing.Point(180, 180);
            this.textBoxAHKHiho.Multiline = true;
            this.textBoxAHKHiho.Name = "textBoxAHKHiho";
            this.textBoxAHKHiho.Size = new System.Drawing.Size(512, 24);
            this.textBoxAHKHiho.TabIndex = 10;
            // 
            // textBoxAHK
            // 
            this.textBoxAHK.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHK.Location = new System.Drawing.Point(180, 130);
            this.textBoxAHK.Multiline = true;
            this.textBoxAHK.Name = "textBoxAHK";
            this.textBoxAHK.Size = new System.Drawing.Size(512, 24);
            this.textBoxAHK.TabIndex = 10;
            // 
            // btnAHKClinic
            // 
            this.btnAHKClinic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAHKClinic.Location = new System.Drawing.Point(700, 30);
            this.btnAHKClinic.Name = "btnAHKClinic";
            this.btnAHKClinic.Size = new System.Drawing.Size(40, 24);
            this.btnAHKClinic.TabIndex = 3;
            this.btnAHKClinic.Text = "...";
            this.btnAHKClinic.UseVisualStyleBackColor = true;
            this.btnAHKClinic.Click += new System.EventHandler(this.btnAHKClinic_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "医療機関マスタ(CSV)";
            // 
            // textBoxAHkClnic
            // 
            this.textBoxAHkClnic.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHkClnic.Location = new System.Drawing.Point(180, 30);
            this.textBoxAHkClnic.Multiline = true;
            this.textBoxAHkClnic.Name = "textBoxAHkClnic";
            this.textBoxAHkClnic.Size = new System.Drawing.Size(512, 24);
            this.textBoxAHkClnic.TabIndex = 0;
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 495);
            this.Controls.Add(this.btnImpJyusei);
            this.Controls.Add(this.gbA);
            this.Controls.Add(this.gbJ);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.gbA.ResumeLayout(false);
            this.gbA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxJyuClnic;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImpJyusei;
        private System.Windows.Forms.Button btnJyuNyukyo;
        private System.Windows.Forms.Button btnJyuClinic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxJyuNyukyo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbA;
        private System.Windows.Forms.Button btnAHKClinic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAHkClnic;
        private System.Windows.Forms.Button btnAHKNyukyo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAHkNyukyo;
        private System.Windows.Forms.Button btnAHKHiho;
        private System.Windows.Forms.Button btnAHK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAHKHiho;
        private System.Windows.Forms.TextBox textBoxAHK;
        private System.Windows.Forms.Button btnJYU;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxJYU;
    }
}