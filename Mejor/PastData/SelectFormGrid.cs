﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.PastData

//20201109191744 furukawa st ////////////////////////
//過去データロードしたときの選択画面
{
    public partial class SelectFormGrid : Form
    {
        public string selectedstring = string.Empty;
        public TextBox _txt;

        public DataGridViewRow retDr;

      
        public SelectFormGrid()
   
        {
            InitializeComponent();           

        }


        private void SelectForm_Load(object sender, EventArgs e)
        {
            dgv.Rows[0].Selected = true;
            
        }

     
        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (dgv.SelectedRows.Count>0))
            {
                retDr = dgv.Rows[dgv.SelectedRows[0].Index];

                this.Close();
            }

        }

        private void SelectFormGrid_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            retDr = null;
            this.Close();
        }
    }
}
