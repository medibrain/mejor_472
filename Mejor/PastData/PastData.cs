﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//20201109191700 furukawa st ////////////////////////

/// <summary>
/// 過去データロードクラス
/// </summary>
namespace Mejor.PastData
{
  
    partial class PersonalData
    {
        [DB.DbAttribute.PrimaryKey]
        public string hnum { get; set; }
        public string hname { get; set; }
        public string pname { get; set; }
        public int psex { get; set; }
        public DateTime pbirthday { get; set; }
        
        public string hzip { get; set; }
        public string haddress { get; set; }


        public static System.Data.DataTable dtPersonal = new System.Data.DataTable();
        
        /// <summary>
        /// どこまで過去にさかのぼるかを取得する（月数）
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        private static int getStartYM(int _cym)
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            try
            {
                //20201123115333 furukawa st ////////////////////////
                //メディベリー使用後、戻るとcommonを見ているので強制的にjyuseiに戻さないとテーブルが見えない
                
                DB db = new DB("jyusei");
                //20201123115333 furukawa ed ////////////////////////


                //20201129090005 furukawa st ////////////////////////
                //aws用接続
                
                if (DB.strConnJyusei.Contains("aws")) 
                {
                    //20220214133402 furukawa st ////////////////////////
                    //NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                    
                    cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}';sslmode=1;convert infinity datetime=true";
                    //20220214133402 furukawa ed ////////////////////////

                }
                //20201129090005 furukawa ed ////////////////////////
                else
                {
                    //20220214133457 furukawa st ////////////////////////
                    //NPGSQL2.2.7から4.0.12への変更によりdatetime -infinityのconvertも必要になった
                    
                    cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}';convert infinity datetime=true";
                    //20220214133457 furukawa ed ////////////////////////
                }

                cn.Open();
                cmd.Connection = cn;

                //メホールで使用している保険者のみ
                string strsql=
                  $"select " +
                  $"personaldata " +
                  $"from insurer_option o inner join insurer i on o.insurerid=i.insurerid " +
                  $"where o.insurerid={Insurer.CurrrentInsurer.InsurerID} and i.enabled=true";
                cmd.CommandText = strsql;
                var res = cmd.ExecuteScalar();
                
                if (!int.TryParse(res.ToString(), out int months)) return 0;

                return months;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return 0;
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }
            
        }



        /// <summary>
        /// 個人情報部ロード
        /// </summary>
        /// <param name="_cym"></param>
        public static void LoadPersonalData(int _cym)
        {
            int start = getStartYM(_cym)*-1;
            if (start == 0) return;
            
            dtPersonal.Clear();
            if (!dtPersonal.Columns.Contains("hnum")) dtPersonal.Columns.Add("hnum");
            if (!dtPersonal.Columns.Contains("hname")) dtPersonal.Columns.Add("hname");
            if (!dtPersonal.Columns.Contains("pname")) dtPersonal.Columns.Add("pname");
            if (!dtPersonal.Columns.Contains("psex")) dtPersonal.Columns.Add("psex");
            if (!dtPersonal.Columns.Contains("pbirthday")) dtPersonal.Columns.Add("pbirthday");

            if (!dtPersonal.Columns.Contains("hzip")) dtPersonal.Columns.Add("hzip");
            if (!dtPersonal.Columns.Contains("haddress")) dtPersonal.Columns.Add("haddress");


            int startYM=DateTimeEx.Int6YmAddMonth(_cym,start);

            //applicationから取得。大阪広域のような例外は別関数にした方が？
            DB.Command cmd = DB.Main.CreateCmd(
                $"select " +
                $"trim(hnum) hnum,trim(hname) hname ,trim(pname) pname ,psex ,pbirthday ,hzip,haddress " +
                //$"from list_pdata where cym between {startYM} and {_cym} " +
                $"from application where cym between {startYM} and {_cym} " +
                $"group by trim(hnum), trim(hname), trim(pname) ,psex ,pbirthday ,hzip,haddress " +

                //20201124175845 furukawa st ////////////////////////
                //値があるものを上に（降順）しないと、1レコード選択時に空しか出なくなる
                
                $"order by trim(hnum), trim(hname) desc, trim(pname) desc,psex desc ,pbirthday desc ,hzip desc,haddress  desc");
            //$"order by trim(hnum), trim(hname) desc, trim(pname) desc,psex desc ,pbirthday desc ,hzip desc,haddress  desc");

            //20201124175845 furukawa ed ////////////////////////

            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    System.Data.DataRow dr = dtPersonal.NewRow();
                    dr["hnum"] = item[0].ToString();
                    dr["hname"] = item[1].ToString();
                    dr["pname"] = item[2].ToString();
                    dr["psex"] = int.Parse(item[3].ToString());
                    dr["pbirthday"] = DateTime.Parse(item[4].ToString()).ToString("yyyy/MM/dd");

                    dr["hzip"] = item[5].ToString();
                    dr["haddress"] = item[6].ToString();

                    dtPersonal.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// ロードしたデータから、一行を抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static PersonalData SelectRecord(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();

            if (dtPersonal.Rows.Count == 0) return null;
            System.Data.DataRow[] dr = dtPersonal.Select(strFilter);

            //選択結果が０件の場合
            if (dr.Length <= 0) return null;

            System.Data.DataTable srcdt = dtPersonal.Clone();
            for(int r=0;r<dr.Length;r++)
            {
                
                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);

            }

  
            frmgrid.dgv.DataSource = srcdt;
            frmgrid.dgv.Columns[0].Width = 100;
            frmgrid.dgv.Columns[1].Width = 120;
            frmgrid.dgv.Columns[2].Width = 120;
            frmgrid.dgv.Columns[3].Width = 80;
            frmgrid.dgv.Columns[4].Width = 100;
            frmgrid.dgv.Columns[5].Width = 100;
            frmgrid.dgv.Columns[6].Width = 500;

            frmgrid.dgv.Columns[0].HeaderText = "記号番号";
            frmgrid.dgv.Columns[1].HeaderText = "被保険者名";
            frmgrid.dgv.Columns[2].HeaderText = "受療者名";
            frmgrid.dgv.Columns[3].HeaderText = "受療者性別";
            frmgrid.dgv.Columns[4].HeaderText = "受療者生年月日";
            frmgrid.dgv.Columns[5].HeaderText = "被保険者郵便番号";
            frmgrid.dgv.Columns[6].HeaderText = "被保険者住所";


            PastData.PersonalData p = new PersonalData();
            //2件以上の時選択リスト
            if (srcdt.Rows.Count > 1)
            {
                
                frmgrid.ShowDialog();
                if (frmgrid.retDr == null)
                {
                   
                    return null;
                }
                
                p.hnum = frmgrid.retDr.Cells[0].Value.ToString();
                p.hname = frmgrid.retDr.Cells[1].Value.ToString();
                p.pname = frmgrid.retDr.Cells[2].Value.ToString();
                p.psex = int.Parse(frmgrid.retDr.Cells[3].Value.ToString());
                p.pbirthday = DateTime.Parse(frmgrid.retDr.Cells[4].Value.ToString());

                p.hzip = frmgrid.retDr.Cells[5].Value.ToString();
                p.haddress = frmgrid.retDr.Cells[6].Value.ToString();
            }
            else {
                
                p.hnum = srcdt.Rows[0][0].ToString();
                p.hname = srcdt.Rows[0][1].ToString();
                p.pname = srcdt.Rows[0][2].ToString();
                p.psex = int.Parse(srcdt.Rows[0][3].ToString());
                p.pbirthday = DateTime.Parse(srcdt.Rows[0][4].ToString());
                p.hzip = srcdt.Rows[0][5].ToString();
                p.haddress = srcdt.Rows[0][6].ToString();
            }

            return p;
        }




        /// <summary>
        /// 20201124181013 furukawa 1行のみ抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static PersonalData SelectRecordOne(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();

            if (dtPersonal.Rows.Count == 0) return null;
            System.Data.DataRow[] dr = dtPersonal.Select(strFilter);

            //選択結果が０件の場合
            if (dr.Length <= 0) return null;

            System.Data.DataTable srcdt = dtPersonal.Clone();
            for (int r = 0; r < dr.Length; r++)
            {

                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);

            }
        
            PastData.PersonalData p = new PersonalData();           

            p.hnum = srcdt.Rows[0][0].ToString();
            p.hname = srcdt.Rows[0][1].ToString();
            p.pname = srcdt.Rows[0][2].ToString();
            p.psex = int.Parse(srcdt.Rows[0][3].ToString());
            p.pbirthday = DateTime.Parse(srcdt.Rows[0][4].ToString());
            p.hzip = srcdt.Rows[0][5].ToString();
            p.haddress = srcdt.Rows[0][6].ToString();
           
            return p;
        }



        //public static void EntryPersonalData(App a)
        //{
        //    DB.Command cmd = DB.Main.CreateCmd(
        //      $"insert into list_pdata values ( " +
        //      $"{a.HihoNum},{a.HihoName},{a.PersonName},{a.Sex},{a.Birthday}");

        //    try
        //    {
        //        cmd.TryExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        //Insertエラーの場合、既に入っていると想定しすっ飛ばす
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }

        //}
    }


    partial class ClinicData
    {
        [DB.DbAttribute.PrimaryKey]
        public string hnum { get; set; }
        public string clinicid { get; set; }
        public string clinicname { get; set; }
        public string clinicaddress { get; set; }
        public string cliniczip { get; set; }
        public string clinictel { get; set; }
        public string doctorname { get; set; }
        public string doctorkana { get; set; }
        public string doctornum { get; set; }//柔整師登録記号番号
        
        public static System.Data.DataTable dtClinic = new System.Data.DataTable();

        /// <summary>
        /// どこまで過去にさかのぼるかを取得する（月数）
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        private static int getStartYM(int _cym)
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            try
            {
                //20220616181618 furukawa st ////////////////////////
                //メディベリー使用後、戻るとcommonを見ているので強制的にjyuseiに戻さないとテーブルが見えない                
                DB db = new DB("jyusei");
                //20220616181618 furukawa ed ////////////////////////


                //20220616181509 furukawa st ////////////////////////
                //NPGSQL2.2.7から4.0.12への変更によりdatetime -infinityのconvertも必要になった

                //aws用接続

                if (DB.strConnJyusei.Contains("aws"))
                {                    
                    //NPGSQL2.2.7から4.0.12への変更によりSSLMode書式が変わり、datetime -infinityのconvertも必要になった
                    cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}';sslmode=1;convert infinity datetime=true";                   
                }                
                else
                {                    
                    //NPGSQL2.2.7から4.0.12への変更によりdatetime -infinityのconvertも必要になった
                    cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}';convert infinity datetime=true";                    
                }


                //cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}'";


                //20220616181509 furukawa ed ////////////////////////

                cn.Open();
                cmd.Connection = cn;

                //メホールで使用している保険者のみ
                string strsql =
                  $"select " +
                  $"clinicdata " +
                  $"from insurer_option o inner join insurer i on o.insurerid=i.insurerid " +
                  $"where o.insurerid={Insurer.CurrrentInsurer.InsurerID} and i.enabled=true";
                cmd.CommandText = strsql;
                var res = cmd.ExecuteScalar();

                if (!int.TryParse(res.ToString(), out int months)) return 0;

                return months;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }

        }



        /// <summary>
        /// 施術所情報部ロード
        /// </summary>
        /// <param name="_cym"></param>
        public static void LoadClinicData(int _cym)
        {
            int start = getStartYM(_cym) * -1;
            if (start == 0) return;

            dtClinic.Clear();
            if (!dtClinic.Columns.Contains("hnum")) dtClinic.Columns.Add("hnum");
            if (!dtClinic.Columns.Contains("clinicid")) dtClinic.Columns.Add("clinicid");
            if (!dtClinic.Columns.Contains("clinicname")) dtClinic.Columns.Add("clinicname");
            if (!dtClinic.Columns.Contains("cliniczip")) dtClinic.Columns.Add("cliniczip");
            if (!dtClinic.Columns.Contains("clinicaddress")) dtClinic.Columns.Add("clinicaddress");
            if (!dtClinic.Columns.Contains("clinictel")) dtClinic.Columns.Add("clinictel");

            if (!dtClinic.Columns.Contains("doctorname")) dtClinic.Columns.Add("doctorname");
            if (!dtClinic.Columns.Contains("doctorkana")) dtClinic.Columns.Add("doctorkana");

            //20220404141456 furukawa st ////////////////////////
            //柔整師登録記号番号追加
            
            if (!dtClinic.Columns.Contains("doctornum")) dtClinic.Columns.Add("doctornum");
            //20220404141456 furukawa ed ////////////////////////

            int startYM = DateTimeEx.Int6YmAddMonth(_cym, start);
            
            DB.Command cmd = DB.Main.CreateCmd(
                $"select " +

                //20220404141327 furukawa st ////////////////////////
                //sregnumber追加
                
                $"trim(hnum) hnum,sid,sname,szip,saddress,stel,sdoctor,skana,sregnumber  " +
                //      $"trim(hnum) hnum,sid,sname,szip,saddress,stel,sdoctor,skana  " +

                //20220404141327 furukawa ed ////////////////////////


                //$"from list_pdata where cym between {startYM} and {_cym} " +
                $"from application where cym between {startYM} and {_cym} " +

                //20220404141427 furukawa st ////////////////////////
                //sregnumber追加
                
                $"group by trim(hnum),sid,sname,szip,saddress,stel,sdoctor,skana,sregnumber  " +
                //      $"group by trim(hnum),sid,sname,szip,saddress,stel,sdoctor,skana  " +

                //20220404141427 furukawa ed ////////////////////////


                //20201124180237 furukawa st ////////////////////////
                //値があるものを上に（降順）しないと、1レコード選択時に空しか出なくなる

                //20220404141525 furukawa st ////////////////////////
                //sregnumber追加
                
                $"order by trim(hnum) desc ,sid desc ,sname desc ,szip desc ,saddress desc ,stel desc ,sdoctor desc ,skana  desc,sregnumber desc ");
                //      $"order by trim(hnum) desc ,sid desc ,sname desc ,szip desc ,saddress desc ,stel desc ,sdoctor desc ,skana  desc ");
                //20220404141525 furukawa ed ////////////////////////

            //          $"order by trim(hnum)");
            //20201124180237 furukawa ed ////////////////////////

            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    System.Data.DataRow dr = dtClinic.NewRow();
                    dr["hnum"] = item[0].ToString();
                    dr["clinicid"] = item[1].ToString();
                    dr["clinicname"] = item[2].ToString();
                    dr["cliniczip"] = item[3].ToString();
                    dr["clinicaddress"] = item[4].ToString();
                    dr["clinictel"] = item[5].ToString();
                    dr["doctorname"] = item[6].ToString();
                    dr["doctorkana"] = item[7].ToString();
                    //20220404141550 furukawa st ////////////////////////
                    //柔整師登録記号番号追加
                    
                    dr["doctornum"] = item[8].ToString();
                    //20220404141550 furukawa ed ////////////////////////


                    dtClinic.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// ロードしたデータから、一行を抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static ClinicData SelectRecord(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();
            if (dtClinic.Rows.Count == 0) return null;
            System.Data.DataRow[] dr = dtClinic.Select(strFilter);

            //選択結果が０件の場合
            if (dr.Length <= 0) return null;

            System.Data.DataTable srcdt = dtClinic.Clone();
            for (int r = 0; r < dr.Length; r++)
            {

                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);

            }


            frmgrid.dgv.DataSource = srcdt;
            frmgrid.dgv.Columns[0].Width = 100;
            frmgrid.dgv.Columns[1].Width = 100;
            frmgrid.dgv.Columns[2].Width = 200;
            frmgrid.dgv.Columns[3].Width = 80;
            frmgrid.dgv.Columns[4].Width = 200;
            frmgrid.dgv.Columns[5].Width = 100;
            frmgrid.dgv.Columns[6].Width = 100;
            frmgrid.dgv.Columns[7].Width = 160;

            frmgrid.dgv.Columns[0].HeaderText = "被保険者番号";
            frmgrid.dgv.Columns[1].HeaderText = "施術所ID";
            frmgrid.dgv.Columns[2].HeaderText = "施術所名";
            frmgrid.dgv.Columns[3].HeaderText = "施術所郵便番号";
            frmgrid.dgv.Columns[4].HeaderText = "施術所住所";
            frmgrid.dgv.Columns[5].HeaderText = "施術所電話";
            frmgrid.dgv.Columns[6].HeaderText = "施術師名";
            frmgrid.dgv.Columns[7].HeaderText = "施術師カナ";


            PastData.ClinicData cd = new ClinicData();
            //2件以上の時選択リスト
            if (srcdt.Rows.Count > 1)
            {

                frmgrid.ShowDialog();
                if (frmgrid.retDr == null)
                {

                    return null;
                }

                cd.hnum = frmgrid.retDr.Cells[0].Value.ToString();
                cd.clinicid = frmgrid.retDr.Cells[1].Value.ToString();
                cd.clinicname = frmgrid.retDr.Cells[2].Value.ToString();
                cd.cliniczip = frmgrid.retDr.Cells[3].Value.ToString();
                cd.clinicaddress = frmgrid.retDr.Cells[4].Value.ToString();
                cd.clinictel = frmgrid.retDr.Cells[5].Value.ToString();
                cd.doctorname = frmgrid.retDr.Cells[6].Value.ToString();
                cd.doctorkana = frmgrid.retDr.Cells[7].Value.ToString();
                
            }
            else
            {

                cd.hnum = srcdt.Rows[0][0].ToString();
                cd.clinicid = srcdt.Rows[0][1].ToString();
                cd.clinicname= srcdt.Rows[0][2].ToString();
                cd.cliniczip = srcdt.Rows[0][3].ToString();
                cd.clinicaddress = srcdt.Rows[0][4].ToString();
                cd.clinictel = srcdt.Rows[0][5].ToString();
                cd.doctorname = srcdt.Rows[0][6].ToString();
                cd.doctorkana = srcdt.Rows[0][7].ToString();

            }

            return cd;
        }




        /// <summary>
        /// 20201124181136 furukawa 1行のみ抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static ClinicData SelectRecordOne(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();
            if (dtClinic.Rows.Count == 0) return null;
            System.Data.DataRow[] dr = dtClinic.Select(strFilter);

            //選択結果が０件の場合
            if (dr.Length <= 0) return null;

            System.Data.DataTable srcdt = dtClinic.Clone();
            for (int r = 0; r < dr.Length; r++)
            {

                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);

            }


        

            PastData.ClinicData cd = new ClinicData();
          
            cd.hnum = srcdt.Rows[0][0].ToString();
            cd.clinicid = srcdt.Rows[0][1].ToString();
            cd.clinicname = srcdt.Rows[0][2].ToString();
            cd.cliniczip = srcdt.Rows[0][3].ToString();
            cd.clinicaddress = srcdt.Rows[0][4].ToString();
            cd.clinictel = srcdt.Rows[0][5].ToString();
            cd.doctorname = srcdt.Rows[0][6].ToString();
            cd.doctorkana = srcdt.Rows[0][7].ToString();
            //20220404141626 furukawa st ////////////////////////
            //柔整師登録記号番号追加
            
            cd.doctornum = srcdt.Rows[0][8].ToString();
            //20220404141626 furukawa ed ////////////////////////

            return cd;
        }



        //public static void EntryPersonalData(App a)
        //{
        //    DB.Command cmd = DB.Main.CreateCmd(
        //      $"insert into list_pdata values ( " +
        //      $"{a.HihoNum},{a.HihoName},{a.PersonName},{a.Sex},{a.Birthday}");

        //    try
        //    {
        //        cmd.TryExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        //Insertエラーの場合、既に入っていると想定しすっ飛ばす
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }

        //}
    }


    partial class AccountData
    {
        [DB.DbAttribute.PrimaryKey]
        public string baccnumber { get; set; }         //口座番号
        public string baccname { get; set; }           //口座名義
        public string bname { get; set; }              //銀行名
        public string bkana { get; set; }              //銀行名カナ
        public string bbranch { get; set; }            //支店名
        public string bbranchtype { get; set; }        //支店種別
        public string bacctype { get; set; }           //口座種別　普通/当座/等
        public string btype { get; set; }              //銀行種別　銀行/信用金庫/ゆうちょ/等


        public static System.Data.DataTable dtAccount = new System.Data.DataTable();

        /// <summary>
        /// どこまで過去にさかのぼるかを取得する（月数）
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        private static int getStartYM(int _cym)
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            try
            {
                cn.ConnectionString = DB.strConnJyusei + $"username='{Settings.JyuseiDBUser}';password='{Settings.JyuseiDBPassword}'";

                cn.Open();
                cmd.Connection = cn;

                //メホールで使用している保険者のみ
                string strsql =
                  $"select " +
                  $"accountdata " +
                  $"from insurer_option o inner join insurer i on o.insurerid=i.insurerid " +
                  $"where o.insurerid={Insurer.CurrrentInsurer.InsurerID} and i.enabled=true";
                cmd.CommandText = strsql;
                var res = cmd.ExecuteScalar();

                if (!int.TryParse(res.ToString(), out int months)) return 0;

                return months;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }

        }



        /// <summary>
        /// 施術所情報部ロード
        /// </summary>
        /// <param name="_cym"></param>
        public static void LoadBAccountData(int _cym)
        {
            int start = getStartYM(_cym) * -1;
            if (start == 0) return;

            dtAccount.Clear();

            if (!dtAccount.Columns.Contains("baccnumber")) dtAccount.Columns.Add("baccnumber");
            if (!dtAccount.Columns.Contains("baccname")) dtAccount.Columns.Add("baccname");
            if (!dtAccount.Columns.Contains("bname")) dtAccount.Columns.Add("bname");
            if (!dtAccount.Columns.Contains("bkana")) dtAccount.Columns.Add("bkana");
            if (!dtAccount.Columns.Contains("bbranch")) dtAccount.Columns.Add("bbranch");
            if (!dtAccount.Columns.Contains("bbranchtype")) dtAccount.Columns.Add("bbranchtype");
            if (!dtAccount.Columns.Contains("bacctype")) dtAccount.Columns.Add("bacctype");
            if (!dtAccount.Columns.Contains("btype")) dtAccount.Columns.Add("btype");

            int startYM = DateTimeEx.Int6YmAddMonth(_cym, start);

            DB.Command cmd = DB.Main.CreateCmd(
                $"select " +
                $"baccnumber,baccname,bname,bkana,bbranch,bbranchtype,bacctype,btype  " +
                //$"from list_pdata where cym between {startYM} and {_cym} " +
                $"from application where cym between {startYM} and {_cym} " +
                $"and trim(baccname)<>'' " +//口座名義が空欄のレコードは出さない
                $"group by baccnumber,baccname,bname,bkana,bbranch,bbranchtype,bacctype,btype " +

                //20201124180111 furukawa st ////////////////////////
                //値があるものを上に（降順）しないと、1レコード選択時に空しか出なくなる
                
                $"order by baccnumber desc,baccname desc,bname desc,bkana desc,bbranch desc,bbranchtype desc,bacctype desc,btype desc ");
            //$"order by baccnumber") ;
            //20201124180111 furukawa ed ////////////////////////

            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    System.Data.DataRow dr = dtAccount.NewRow();

                    dr["baccnumber"] = item[0].ToString();
                    dr["baccname"] = item[1].ToString();
                    dr["bname"] = item[2].ToString();
                    dr["bkana"] = item[3].ToString();
                    dr["bbranch"] = item[4].ToString();
                    dr["bbranchtype"] = item[5].ToString();
                    dr["bacctype"] = item[6].ToString();
                    dr["btype"] = item[7].ToString();


                    dtAccount.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

        }


        /// <summary>
        /// ロードしたデータから、一行を抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static AccountData SelectRecord(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();
            if (dtAccount.Rows.Count == 0) return null;
            
            //選択結果が０件の場合
            System.Data.DataRow[] dr = dtAccount.Select(strFilter);
            if (dr.Length <= 0) return null;


            System.Data.DataTable srcdt = dtAccount.Clone();
            for (int r = 0; r < dr.Length; r++)
            {
                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);
            }


            frmgrid.dgv.DataSource = srcdt;
            frmgrid.dgv.Columns[0].Width = 100;
            frmgrid.dgv.Columns[1].Width = 300;
            frmgrid.dgv.Columns[2].Width = 100;
            frmgrid.dgv.Columns[3].Width = 160;
            frmgrid.dgv.Columns[4].Width = 100;
            frmgrid.dgv.Columns[5].Width = 100;
            frmgrid.dgv.Columns[6].Width = 100;
            frmgrid.dgv.Columns[7].Width = 100;

            frmgrid.dgv.Columns[0].HeaderText = "口座番号";
            frmgrid.dgv.Columns[1].HeaderText = "口座名義";
            frmgrid.dgv.Columns[2].HeaderText = "銀行名";
            frmgrid.dgv.Columns[3].HeaderText = "銀行名カナ";
            frmgrid.dgv.Columns[4].HeaderText = "支店名";
            frmgrid.dgv.Columns[5].HeaderText = "支店種別";
            frmgrid.dgv.Columns[6].HeaderText = "口座種別";
            frmgrid.dgv.Columns[7].HeaderText = "銀行種別";


            PastData.AccountData ad = new AccountData();
            //2件以上の時選択リスト
            if (srcdt.Rows.Count > 1)
            {

                frmgrid.ShowDialog();
                if (frmgrid.retDr == null)
                {

                    return null;
                }

                ad.baccnumber = frmgrid.retDr.Cells[0].Value.ToString();
                ad.baccname = frmgrid.retDr.Cells[1].Value.ToString();
                ad.bname = frmgrid.retDr.Cells[2].Value.ToString();
                ad.bkana = frmgrid.retDr.Cells[3].Value.ToString();
                ad.bbranch = frmgrid.retDr.Cells[4].Value.ToString();
                ad.bbranchtype = frmgrid.retDr.Cells[5].Value.ToString();
                ad.bacctype = frmgrid.retDr.Cells[6].Value.ToString();
                ad.btype = frmgrid.retDr.Cells[7].Value.ToString();

            }
            else
            {

                ad.baccnumber = srcdt.Rows[0][0].ToString();
                ad.baccname = srcdt.Rows[0][1].ToString();
                ad.bname = srcdt.Rows[0][2].ToString();
                ad.bkana = srcdt.Rows[0][3].ToString();
                ad.bbranch = srcdt.Rows[0][4].ToString();
                ad.bbranchtype = srcdt.Rows[0][5].ToString();
                ad.bacctype = srcdt.Rows[0][6].ToString();
                ad.btype = srcdt.Rows[0][7].ToString();

            }

            return ad;
        }


        
        
        /// <summary>
        /// 20201124181249 furukawa 1行のみ抽出
        /// </summary>
        /// <param name="strFilter">抽出条件</param>
        /// <returns></returns>
        public static AccountData SelectRecordOne(string strFilter)
        {
            SelectFormGrid frmgrid = new SelectFormGrid();
            if (dtAccount.Rows.Count == 0) return null;

            //選択結果が０件の場合
            System.Data.DataRow[] dr = dtAccount.Select(strFilter);
            if (dr.Length <= 0) return null;


            System.Data.DataTable srcdt = dtAccount.Clone();
            for (int r = 0; r < dr.Length; r++)
            {
                System.Data.DataRow srcr = srcdt.NewRow();

                for (int c = 0; c < dr[r].ItemArray.Length; c++)
                {
                    srcr[c] = dr[r][c];
                }
                srcdt.Rows.Add(srcr);
            }


         

            PastData.AccountData ad = new AccountData();
          

            ad.baccnumber = srcdt.Rows[0][0].ToString();
            ad.baccname = srcdt.Rows[0][1].ToString();
            ad.bname = srcdt.Rows[0][2].ToString();
            ad.bkana = srcdt.Rows[0][3].ToString();
            ad.bbranch = srcdt.Rows[0][4].ToString();
            ad.bbranchtype = srcdt.Rows[0][5].ToString();
            ad.bacctype = srcdt.Rows[0][6].ToString();
            ad.btype = srcdt.Rows[0][7].ToString();

           

            return ad;
        }


        //public static void EntryPersonalData(App a)
        //{
        //    DB.Command cmd = DB.Main.CreateCmd(
        //      $"insert into list_pdata values ( " +
        //      $"{a.HihoNum},{a.HihoName},{a.PersonName},{a.Sex},{a.Birthday}");

        //    try
        //    {
        //        cmd.TryExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        //Insertエラーの場合、既に入っていると想定しすっ飛ばす
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }

        //}
    }

}
