﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    class ViewerShokaiData
    {
        public int AID { get; set; }
        public string ShokaiID { get; set; } = string.Empty;
        public ShokaiReason ShokaiReason { get; set; } = ShokaiReason.なし;
        public SHOKAI_RESULT ShokaiResult { get; set; } = SHOKAI_RESULT.なし;
        public SHOKAI_STATUS ShokaiStatus { get; set; } = SHOKAI_STATUS.なし;
        public KAGO_REASON KagoReason { get; set; } = KAGO_REASON.なし;
        public SAISHINSA_REASON SaishinsaReason { get; set; } = SAISHINSA_REASON.なし;
        public string Note { get; set; } = string.Empty;
        public string ShokaiFile { get; set; } = string.Empty;
        public bool HenreiFlag { get; set; } = false;

        public static string Header =>
            "AID,ShokaiID,ShokaiReason,ShokaiResult,ShokaiStatus,KagoReason,"+
            "SaishinsaReason,Note,ShokaiFile,HenreiFlag";

        public string CreateCsvLine()
        {
            var s = new string[10];
            int i = 0;
            //"AID,ShokaiID,ShokaiReason,ShokaiResult,ShokaiStatus,KagoReason,"
            s[i++] = AID.ToString();
            s[i++] = ShokaiID;
            s[i++] = ((int)ShokaiReason).ToString();
            s[i++] = ((int)ShokaiResult).ToString();
            s[i++] = ((int)ShokaiStatus).ToString();
            s[i++] = ((int)KagoReason).ToString();
            //"SaishinsaReason,Note,ShokaiFile,HenreiFlag"
            s[i++] = ((int)SaishinsaReason).ToString();
            s[i++] = Note;
            s[i++] = ShokaiFile;
            s[i++] = HenreiFlag ? "True" : "False";

            for (int c = 0; c < s.Length; c++)
                s[c]=$"\"{s[c].Replace("\"", "\"\"")}\"";

            return string.Join(",", s);
        }
    }
}
