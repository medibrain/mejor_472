﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;

namespace Mejor
{
    class ImagePrint
    {
        //List<string> imageNames;
        int frameIndex = 0;
        int receIndex = 0;
        string printerName;

        //参考レセ
        bool printSukashiSign = false;
        bool printMediYM = false;
        bool printTotal = false;
        List<KeyValuePair<string, bool>> printPathList;
        Dictionary<string, App> relationDic = new Dictionary<string, App>();

        public ImagePrint(string printerName)
        {
            this.printerName = printerName;
        }

        public bool CheckPrinterName()
        {
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                if (PrinterSettings.InstalledPrinters[i] == printerName)
                    return true;
            }
            return false;
        }

        private Image getImage(string fileName)
        {
            if (!System.IO.File.Exists(fileName)) return null;
            var readms = new System.IO.MemoryStream();

            using (var tifFS = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    tifFS.CopyTo(readms);
                    return Image.FromStream(tifFS);
                }
                catch
                {
                    return null;
                }
            }
        }

        public void Print(List<string> imageNames, bool preview, bool addMargin)
        {
            if (!CheckPrinterName())
            {
                var res = System.Windows.Forms.MessageBox.Show(
                    "プリンターの設定が不正です。このままデフォルトのプリンターで印刷しますか？",
                    "プリンター設定確認", System.Windows.Forms.MessageBoxButtons.OKCancel,
                    System.Windows.Forms.MessageBoxIcon.Question);
                if (res != System.Windows.Forms.DialogResult.OK) return;

                printerName = string.Empty;
            }

            printPathList = new List<KeyValuePair<string, bool>>();
            imageNames.ForEach(n => printPathList.Add(new KeyValuePair<string, bool>(n, false)));

            printCore(preview, addMargin);
        }

        private void printCore(bool preview, bool addMargin)
        {
            using (var printDocument = new PrintDocument())
            {
                printDocument.PrintPage += printDocument_PrintPage;

                //　プリンタ名と余白設定を読み込む
                int printMargin = Settings.PrintMargin;

                // 印刷ドキュメントに、プリンタを設定する。
                if (printerName != string.Empty) printDocument.PrinterSettings.PrinterName = printerName;

                // 用紙の向きを設定(横：true、縦：false)
                printDocument.DefaultPageSettings.Landscape = false;

                // 余白を0.01インチ単位に変換  (＝ 余白ミリ/ 25.4ミリ * 100) し、設定する
                printMargin = addMargin ? (int)((double)printMargin * 100.0 / 25.4) : 0;
                printDocument.DefaultPageSettings.Margins.Top = printMargin;
                printDocument.DefaultPageSettings.Margins.Bottom = printMargin;
                printDocument.DefaultPageSettings.Margins.Left = printMargin;
                printDocument.DefaultPageSettings.Margins.Right = printMargin;

                // 用紙設定（A4）
                foreach (PaperSize ps in printDocument.PrinterSettings.PaperSizes)
                {
                    if (ps.Kind == PaperKind.A4)
                    {
                        printDocument.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                        break;
                    }
                }

                try
                {
                    if (preview)
                    {
                        using (var printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog())
                        {
                            // 印刷プレビューの実行
                            printPreviewDialog.Document = printDocument;
                            printPreviewDialog.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                            printPreviewDialog.ShowDialog();
                        }
                    }
                    else
                    {
                        // 印刷の実行
                        printDocument.Print();
                    }
                }
                catch(Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apps">印刷対象Appリスト</param>
        /// <param name="preview"></param>
        /// <param name="addMargin"></param>
        /// <param name="printSukashiSign"></param>
        /// <param name="printMediYM"></param>
        /// <param name="printTotal"></param>
        /// <param name="relationTable"></param>
        public void PrintWithRelations(List<KeyValuePair<App, bool>> appsWithRelationFlag, bool preview,
            bool addMargin, bool printSukashiSign, bool printMediYM, bool printTotal)
        {
            this.printSukashiSign = printSukashiSign;
            this.printMediYM = printMediYM;
            this.printTotal = printTotal;
            printPathList = new List<KeyValuePair<string, bool>>();

            foreach (var item in appsWithRelationFlag)
            {
                var n = item.Key.GetImageFullPath();
                printPathList.Add(new KeyValuePair<string, bool>(n, item.Value));
                relationDic.Add(item.Key.GetImageFullPath(), item.Key);
            }

            printCore(preview, addMargin);
        }

        public void Print(List<App> apps, bool preview, bool addMargin)
        {
            var l = new List<string>();
            l.AddRange(apps.Select(x => x.GetImageFullPath()));
            Print(l, preview, addMargin);
        }
                
        public void Print(App app, bool preview, bool addMargin)
        {
            var l = new List<string>();
            l.Add(app.GetImageFullPath());
            Print(l, preview, addMargin);
        }

        /// <summary>
        /// 参考レセを含めて印刷します
        /// </summary>
        /// <param name="app"></param>
        /// <param name="preview"></param>
        /// <param name="addMargin"></param>
        /// <param name="wf"></param>
        /// <param name="app2"></param>
        public void Print(App app, bool preview, bool addMargin, App app2)
        {
            var l = new List<string>();
            l.Add(app.GetImageFullPath());
            l.Add(app2.GetImageFullPath());
            relationDic.Add(app2.GetImageFullPath(), app);

            Print(l, preview, addMargin);
        }

        /** 印刷イベント処理 */
        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                // 画像を取得
                var kvp = printPathList[receIndex];
                var img = getImage(kvp.Key);

                //マルチページ処理
                var fd = new FrameDimension(img.FrameDimensionsList[0]);
                int frameCount = img.GetFrameCount(fd);
                if (frameCount != 1)
                {
                    img.SelectActiveFrame(fd, frameIndex);
                    frameIndex++;

                    if (frameIndex == frameCount)
                    {
                        frameIndex = 0;
                        receIndex++;
                    }
                }
                else
                {
                    receIndex++;
                }

                var rect = e.PageBounds;
                var pw = rect.Width - e.PageSettings.Margins.Left - e.PageSettings.Margins.Right;
                var ph = rect.Height - e.PageSettings.Margins.Top - e.PageSettings.Margins.Bottom;

                // 単位を0.01インチ単位に合わせる
                float imageW = img.Width / 2;         // 画像幅 （ピクセル数 / 200dpi）インチ x100
                float imageH = img.Height / 2;        // 画像高 （ピクセル数 / 200dpi）インチ x100


                // 縦・横の調整 
                // 画像が横向き（幅>長）&& 画像の横がページ内（余白より内側）に収まらないとき、90度回転させる！
                if (imageW > imageH && imageW > e.MarginBounds.Width)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);    // 反転させずに時計回りに回転させる
                    var tmp = imageW;
                    imageW = imageH;
                    imageH = tmp;
                }

                // まず、サイズ合わせを行う！ （画像サイズか、ページサイズのうちの最小値）
                float w = Math.Min(imageW, e.MarginBounds.Width);         // 印刷幅 = 画像幅 or ページ幅 の最小
                float h = Math.Min(imageH, e.MarginBounds.Height);        // 印刷高 = 画像高 or ページ高 の最小

                // 縦横比の調整 (印刷範囲の縦横比を、元の画像と同じにする)
                float rate = Math.Min(w / imageW, h / imageH);          // 縮尺 = 印刷幅/画像幅 or 印刷高/画像高 の最小
                w = imageW * rate;
                h = imageH * rate;

                // 印刷位置のセンタリング (余白を調整する)
                // 余白 = 中心位置の差分 = (ページサイズ - 印刷サイズ ) / 2.0
                float x = (e.PageBounds.Width - w) / 2.0f;
                float y = (e.PageBounds.Height - h) / 2.0f;

                // プレビューではなく実際の印刷の時はプリンタの物理的な余白分ずらす
                if (((PrintDocument)sender).PrintController.IsPreview == false)
                {
                    x -= e.PageSettings.HardMarginX;
                    y -= e.PageSettings.HardMarginY;
                }

                // 印刷対象の画像のリストの現在ページを出力
                e.Graphics.DrawImage(img, x, y, w, h);

                //参考レセ時　半透明で「前月参考」と記載
                if (kvp.Value)
                {
                    if (printSukashiSign)
                    {
                        using (var b = new SolidBrush(Color.FromArgb(64, Color.Black)))
                        using (var f = new Font("メイリオ", 70))
                        {
                            e.Graphics.DrawString("参考申請書", f, b, 12, 12);
                        }
                    }

                    if (printMediYM || printTotal)
                    {
                        var a = relationDic[kvp.Key];
                        var s = "対象申請書  " +
                            $"{(printMediYM ? "診療:" + DateTimeEx.GetInitialEraJpYearMonth( a.YM).ToString() : string.Empty)}  " +
                            $"{(printTotal ? "合計:" + a.Total.ToString() : string.Empty)}  ";
                        using (var f = new Font("メイリオ", 10))
                        {
                            e.Graphics.DrawString(s, f, Brushes.Black, 800, 20, new StringFormat { Alignment = StringAlignment.Far });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                e.HasMorePages = false;
                receIndex = 0;
                return;
            }

            if (printPathList.Count == receIndex)
            {
                e.HasMorePages = false;
                receIndex = 0;
            }
            else
            {
                e.HasMorePages = true;
            }
        }
    }
}
