﻿    using System;
using System.Collections.Generic;
using System.Linq;

namespace Mejor
{
    public class Account
    {
        [DB.DbAttribute.PrimaryKey]
        public string Code { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public DateTime Insert_Date { get; set; }
        public int Insert_CYM { get; set; }
        public DateTime Update_Date { get; set; }
        public int Update_CYM { get; set; }
        public bool Verified { get; set; }
        public string HenreiWord { get; set; } = "了承済み";

        /// <summary>
        /// コードに対応した団体情報を取得します。
        /// 取得できなかった場合はnullを返します。
        /// また引数がnullもしくは空欄の場合でもnullを返します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static Account GetAccount(string code)
        {
            var l = DB.Main.Select<Account>(new { code = code });
            if (l == null || l.Count() != 1) return null;
            return l.First();
        }

        /// <summary>
        /// コードに対応した団体情報を取得します。
        /// 取得できなかった場合はnullを返します。
        /// また引数がnullもしくは空欄の場合でもnullを返します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static IEnumerable<Account> GetAllAccount()
        {
            var l = DB.Main.SelectAll<Account>();
            if (l == null) return new List<Account>();
            return l;
        }

        /// <summary>
        /// 団体情報を登録します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert(DB.Transaction tran = null)
        {
            return DB.Main.Insert(this, tran);
        }

        /// <summary>
        /// 団体情報を更新します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Update(DB.Transaction tran = null)
        {
            return DB.Main.Update(this, tran);
        }

        /// <summary>
        /// 団体情報の確定フラグを更新します。（更新日時と更新年月を含む）
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool UpdateVerified(DB.Transaction tran = null)
        {
            var sql = "UPDATE account " +
                "SET update_date=@update_date, update_cym=@update_cym, verified=@verified " +
                "WHERE code=@code";

            return (tran != null) ?
                DB.Main.Excute(sql, this, tran) : DB.Main.Excute(sql, this);
        }
    }
}
