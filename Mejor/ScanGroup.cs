﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;
using NPOI.SS.Formula.Functions;
using static System.Net.Mime.MediaTypeNames;
using static NPOI.HSSF.Util.HSSFColor;

namespace Mejor
{
    public enum GroupStatus { 
        OCR待ち = 0, OCR中 = 1, OCR済み = 2, 
        入力中 = 3, 入力済み = 4, ベリファイ済み = 5, 
        点検中 = 6, 点検済み = 7, 
        外部1入力中 = 8, 外部1済み = 9, 外部2入力中 = 10, 外部2済み = 11,  //20221216 ito プロジェクトK入力
    };

    public class ScanGroup
    {
        [DB.DbAttribute.PrimaryKey]
        public int GroupID { get; private set; }
        public GroupStatus Status { get; private set; }
        public int ScanID { get; set; }
        public DateTime ScanDate { get; set; } = DateTimeEx.DateTimeNull;
        public int ScanUser { get; set; }
        public DateTime CheckDate { get; set; } = DateTimeEx.DateTimeNull;
        public int CheckUser { get; set; }
        public DateTime InquiryDate { get; set; } = DateTimeEx.DateTimeNull;
        public int InquiryUser { get; set; }
        [DB.DbAttribute.UpdateIgnore]
        public string WorkingUsers { get; private set; } = string.Empty;

        //ここから下のプロパティはScanGroupテーブルにはありません
        [DB.DbAttribute.Ignore]
        public int cyear { get; private set; }
        [DB.DbAttribute.Ignore]
        public int cmonth { get; private set; }
        [DB.DbAttribute.Ignore]
        public string note1 { get; private set; } = string.Empty;
        [DB.DbAttribute.Ignore]
        public string note2 { get; private set; } = string.Empty;
        [DB.DbAttribute.Ignore]
        public APP_TYPE AppType { get; private set; } = APP_TYPE.NULL;

        [DB.DbAttribute.Ignore]
        public int YesOcr { get; set; }
        [DB.DbAttribute.Ignore]
        public int YesMatch { get; set; }
        [DB.DbAttribute.Ignore]
        public int NoCheck { get; set; }
        [DB.DbAttribute.Ignore]
        public int YesInput { get; set; }
        [DB.DbAttribute.Ignore]
        public int YesVerify { get; set; }
        [DB.DbAttribute.Ignore]
        public int NeedExInput { get; set; }
        [DB.DbAttribute.Ignore]
        public int NoExInput { get; set; }
        [DB.DbAttribute.Ignore]
        public int YesExInput { get; set; }
        [DB.DbAttribute.Ignore]
        public int YesExVerify { get; set; }
        [DB.DbAttribute.Ignore]
        public int CYM { get; set; }

        //20221216 ito st プロジェクトK入力
        [DB.DbAttribute.Ignore]
        public int GaibuInputFirst { get; set; }
        [DB.DbAttribute.Ignore]
        public int GaibuInputSecond { get; set; }
        [DB.DbAttribute.Ignore]
        public int GroupAppTotal { get; set; }
        //20221216 ito end プロジェクトK入力


        public ScanGroup() { }

        public ScanGroup(int cym)
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //this.cyear = DateTimeEx.GetHsYearFromAd(cym / 100);
            this.cmonth = cym % 100;
            this.cyear = DateTimeEx.GetHsYearFromAd(cym / 100,this.cmonth);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.CYM = cym;
        }

        public static bool CreateScanGroup(Scan s, int gid, DB.Transaction tran)
        {
            var g = new ScanGroup();
            g.GroupID = gid;
            g.Status = GroupStatus.OCR待ち;
            g.ScanID = s.SID;
            g.ScanDate = s.ScanDate;

            if (!g.Insert(tran))
            {
                Log.ErrorWriteWithMsg("ScanGroupの作成に失敗しました");
                return false;
            }

            return true;
        }

        public static ScanGroup Select(int groupID)
        {
            return DB.Main.Select<ScanGroup>($"groupid={groupID}").SingleOrDefault();
        }
        
        /// <summary>
        /// groupテーブルにgroupIDを登録
        /// </summary>
        /// <returns></returns>
        private bool Insert(DB.Transaction tran)
        {
            return DB.Main.Insert(this, tran);
        }

        /// <summary>
        /// GroupIDで単一のグループを取得
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        public static ScanGroup SelectWithScanData(int gid)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "g.status, g.scanid, g.scandate, g.scanuser, g.checkdate, " +
                "g.checkuser, g.inquirydate, g.inquiryuser, g.workingusers, " +
                "s.apptype, s.cyear, s.cmonth, s.note1, s.note2, s.cym " +
                "FROM scangroup AS g, scan AS s " +
                "WHERE g.scanid=s.sid AND g.groupid =:gid"))
            {
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = gid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var g = new ScanGroup();
                var item = res[0];
                g.GroupID = gid;
                g.Status = (GroupStatus)(int)item[0];
                g.ScanID = (int)(item[1] ?? 0);
                g.ScanDate = (DateTime)(item[2] ?? DateTimeEx.DateTimeNull);
                g.ScanUser = (int)(item[3] ?? 0);
                g.CheckDate = (DateTime)(item[4] ?? DateTimeEx.DateTimeNull);
                g.CheckUser = (int)(item[5] ?? 0);
                g.InquiryDate = (DateTime)(item[6] ?? DateTimeEx.DateTimeNull);
                g.InquiryUser = (int)(item[7] ?? 0);
                g.WorkingUsers = (string)item[8];
                g.AppType = (APP_TYPE)(int)(item[9] ?? 0);
                g.cyear = (int)(item[10] ?? 0);
                g.cmonth = (int)(item[11] ?? 0);
                g.note1 = (string)(item[12] ?? string.Empty);
                g.note2 = (string)(item[13] ?? string.Empty);
                g.CYM = (int)(item[14] ?? 0);
                return g;
            }
        }

        public enum WorkType { 入力 = 1, 点検 = 2 }
        public static bool WorkStart(int groupID, WorkType wt)
        {
            var name = User.CurrentUser.Name + " / ";
            var sql = "UPDATE scangroup SET " +
                $"workingusers=workingusers||'{name}', " +
                $"checkdate='{DateTime.Today.ToString("yyyy-MM-dd")}', " +
                $"status={(int)(wt == WorkType.入力 ? GroupStatus.入力中 : GroupStatus.点検中)} " +
                $"WHERE groupid={groupID}";

            return DB.Main.Excute(sql);
        }

        public static bool WorkFinish(int groupID)
        {
            var name = User.CurrentUser.Name + " / ";
            var sql = "UPDATE scangroup SET " +
                $"workingusers=REPLACE(workingusers,'{name}',''), " +
                $"status={(int)getStatusFromApps(groupID)} " +
                $"WHERE groupid={groupID}";

            return DB.Main.Excute(sql);
        }

        public static bool InputReset(int groupID)
        {
            var sql = "UPDATE scangroup SET workingusers='' " +
                $"WHERE groupid={groupID}";

            return DB.Main.Excute(sql);
        }

        public static int GetInquiryUID(int gid)
        {
            var sql = "SELECT inquiryuser FROM scangroup WHERE groupid=:gid";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                try
                {
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = gid;
                    var res = cmd.TryExecuteReaderList();
                    if (res == null || res.Count != 1) return 0;
                    return (int)res[0][0];
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static bool SetInquiryUID(int gid, int uid)
        {
            var sql = "UPDATE scangroup SET inquiryuser=:uid WHERE groupid=:gid";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                try
                {
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = gid;
                    cmd.Parameters.Add("uid", NpgsqlDbType.Integer).Value = uid;
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// 全てのグループを取得(idのみ)
        /// </summary>
        /// <returns></returns>
        public static List<ScanGroup> GetGroupList(List<Scan> scanList)
        {
            if (scanList==null || scanList.Count == 0) return new List<ScanGroup>();

            //20221202 ito st プロジェクトKマスキング
            //var sql = $"SELECT g.* FROM scangroup AS g, scan AS s " +
            //    $"WHERE g.scanid=s.sid AND s.cym={scanList[0].CYM} " +
            //    $"ORDER BY scanid";
            string sql = "";
            if (User.CurrentUser.limiteduser == true)
            {
                sql = $"SELECT g.* FROM scangroup AS g, scan AS s " +
                    $"WHERE g.scanid=s.sid AND s.cym={scanList[0].CYM} AND s.apptype = 6 " +
                    $"ORDER BY scanid";
            }
            else
            {
                sql = $"SELECT g.* FROM scangroup AS g, scan AS s " +
                    $"WHERE g.scanid=s.sid AND s.cym={scanList[0].CYM} " +
                    $"ORDER BY scanid";
            }
            //20221202 ito st プロジェクトKマスキング

            var l = DB.Main.Query<ScanGroup>(sql).ToList();

            foreach (var item in l)
            {
                var scan = scanList.FirstOrDefault(x => x.SID == item.ScanID);

                item.cyear = scan.Cyear;
                item.cmonth = scan.Cmonth;
                item.note1 = scan.Note1;
                item.note2 = scan.Note2;
                item.AppType = scan.AppType;
                item.CYM = scan.CYM;
            }
            return l;
        }

        /// <summary>
        /// ScanId単位で申請書を削除
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static bool DeleteBySID(int sid)
        {
            using (var cmd = DB.Main.CreateCmd("DELETE FROM scangroup " +
                "WHERE scanid=:sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = sid;
                var res = cmd.TryExecuteReaderList();

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// ScanGroupに属する全Appをチェックし、ステータスを取得します
        /// </summary>
        /// <param name="sg"></param>
        private static GroupStatus getStatusFromApps(int groupID)
        {
            var sql = "SELECT statusflags FROM application " +
                $"WHERE groupid={groupID} " +
                "GROUP BY statusflags;";
            var fs = DB.Main.Query<int>(sql).ToList();

            bool flagCheck(int flags, StatusFlag flag) => ((StatusFlag)flags & flag) == flag;
            var inq = fs.Where(x => flagCheck(x, StatusFlag.点検対象)).ToList();

            if (inq.Count > 0 && inq.All(x => flagCheck(x, StatusFlag.点検済)))
                return GroupStatus.点検済み;
            if (inq.Count > 0 && inq.Exists(x => flagCheck(x, StatusFlag.点検済)))
                return GroupStatus.点検中;
            if (fs.All(x => flagCheck(x, StatusFlag.ベリファイ済)))
                return GroupStatus.ベリファイ済み;
            if (fs.All(x => flagCheck(x, StatusFlag.入力済)))
                return GroupStatus.入力済み;

            //20221216 ito st プロジェクトK入力
            if (MainForm.fixedMaskingInsurer(Insurer.CurrrentInsurer))
            {
                //string countSql = "SELECT count(*) FROM application " +
                //    $"WHERE groupid = {groupID} AND statusflags in (0, 1073741824, 1073741825) " +
                //    "group by statusflags " +
                //"order by statusflags;";                 
                var appCount = DB.Main.Query<int>($"SELECT count(*) FROM application WHERE groupid = {groupID} ").ToList();
                int groupTotal = appCount[0];
                double ratio;

                var gaibuFirst = fs.Where(x => flagCheck(x, StatusFlag.外部1回目済)).ToList();
                var gaibuSecond = fs.Where(x => flagCheck(x, StatusFlag.入力済)).ToList();

                appCount = DB.Main.Query<int>($"SELECT count(*) FROM application WHERE groupid = {groupID} AND statusflags = 1073741824").ToList();
                ratio = (double)appCount[0] / groupTotal * 100;
                if (ratio > 90)
                    return GroupStatus.外部1済み;
                appCount = DB.Main.Query<int>($"SELECT count(*) FROM application WHERE groupid = {groupID} AND statusflags = 1073741825").ToList();
                ratio = (double)appCount[0] / groupTotal * 100;
                if (ratio > 90)
                    return GroupStatus.外部2済み;

                if (fs.Exists(x => flagCheck(x, StatusFlag.外部1回目済)))
                    return GroupStatus.外部1入力中;
                if (fs.Exists(x => flagCheck(x, StatusFlag.入力済)))
                    return GroupStatus.入力中;
            }
            else
            {
                if (fs.Exists(x => flagCheck(x, StatusFlag.入力済)))
                    return GroupStatus.入力中;
            }
            //if (fs.Exists(x => flagCheck(x, StatusFlag.入力済)))
            //    return GroupStatus.入力中;
            //20221216 ito end プロジェクトK入力


            var g = Select(groupID);
            var s = Scan.Select(g.ScanID);
            return s.Status == SCAN_STATUS.MATCHED || s.Status == SCAN_STATUS.OCR済み ?
                GroupStatus.OCR済み : GroupStatus.OCR待ち;
        }
    }
}
