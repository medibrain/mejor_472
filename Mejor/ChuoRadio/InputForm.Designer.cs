﻿using System;

namespace Mejor.ChuoRadio
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstE = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyCheckBoxOryo = new Mejor.VerifyCheckBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxInsNum2 = new Mejor.VerifyBox();
            this.verifyBoxNumN = new Mejor.VerifyBox();
            this.verifyBoxNumM = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxKouhi = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxJuryoType = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.verifyBoxFinish = new Mejor.VerifyBox();
            this.verifyBoxFirstD = new Mejor.VerifyBox();
            this.verifyBoxFirstM = new Mejor.VerifyBox();
            this.verifyBoxFirstY = new Mejor.VerifyBox();
            this.verifyBoxStart = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(666, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 70;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(126, 32);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(176, 32);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(422, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 12;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(64, 32);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(29, 12);
            this.labelH.TabIndex = 1;
            this.labelH.Text = "和暦";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(120, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(107, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(107, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(24, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(107, 721);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.label31);
            this.panelRight.Controls.Add(this.verifyBoxF1FirstE);
            this.panelRight.Controls.Add(this.label15);
            this.panelRight.Controls.Add(this.verifyCheckBoxOryo);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.verifyBoxInsNum2);
            this.panelRight.Controls.Add(this.verifyBoxNumN);
            this.panelRight.Controls.Add(this.verifyBoxNumM);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.verifyBoxKouhi);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.verifyBoxJuryoType);
            this.panelRight.Controls.Add(this.verifyBoxBD);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxBM);
            this.panelRight.Controls.Add(this.verifyBoxNumbering);
            this.panelRight.Controls.Add(this.verifyBoxCharge);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.verifyBoxBY);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.verifyBoxBE);
            this.panelRight.Controls.Add(this.verifyBoxNewCont);
            this.panelRight.Controls.Add(this.verifyBoxFinish);
            this.panelRight.Controls.Add(this.verifyBoxFirstD);
            this.panelRight.Controls.Add(this.verifyBoxFirstM);
            this.panelRight.Controls.Add(this.verifyBoxFirstY);
            this.panelRight.Controls.Add(this.verifyBoxStart);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.label1);
            this.panelRight.Controls.Add(this.label30);
            this.panelRight.Controls.Add(this.label22);
            this.panelRight.Controls.Add(this.label24);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.label13);
            this.panelRight.Controls.Add(this.label21);
            this.panelRight.Controls.Add(this.label20);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.label2);
            this.panelRight.Controls.Add(this.label23);
            this.panelRight.Controls.Add(this.label26);
            this.panelRight.Controls.Add(this.label14);
            this.panelRight.Controls.Add(this.label10);
            this.panelRight.Controls.Add(this.label28);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label27);
            this.panelRight.Controls.Add(this.label19);
            this.panelRight.Controls.Add(this.label25);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.label7);
            this.panelRight.Controls.Add(this.label9);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.labelH);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.labelDays);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.label29);
            this.panelRight.Controls.Add(this.labelCharge);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.label12);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.label18);
            this.panelRight.Controls.Add(this.label17);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 0;
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label31.Location = new System.Drawing.Point(42, 527);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 36);
            this.label31.TabIndex = 74;
            this.label31.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // verifyBoxF1FirstE
            // 
            this.verifyBoxF1FirstE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1FirstE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstE.Location = new System.Drawing.Point(15, 528);
            this.verifyBoxF1FirstE.MaxLength = 1;
            this.verifyBoxF1FirstE.Name = "verifyBoxF1FirstE";
            this.verifyBoxF1FirstE.NewLine = false;
            this.verifyBoxF1FirstE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1FirstE.TabIndex = 34;
            this.verifyBoxF1FirstE.TextV = "";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(435, 527);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 24);
            this.label15.TabIndex = 51;
            this.label15.Text = "新規：1\r\n継続：2";
            // 
            // verifyCheckBoxOryo
            // 
            this.verifyCheckBoxOryo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxOryo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxOryo.CheckedV = false;
            this.verifyCheckBoxOryo.Location = new System.Drawing.Point(498, 526);
            this.verifyCheckBoxOryo.Name = "verifyCheckBoxOryo";
            this.verifyCheckBoxOryo.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxOryo.Size = new System.Drawing.Size(83, 25);
            this.verifyCheckBoxOryo.TabIndex = 53;
            this.verifyCheckBoxOryo.Text = "往療あり";
            this.verifyCheckBoxOryo.UseVisualStyleBackColor = false;
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(248, 639);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(234, 23);
            this.verifyBoxF5.TabIndex = 69;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(8, 639);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(234, 23);
            this.verifyBoxF4.TabIndex = 67;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxInsNum2
            // 
            this.verifyBoxInsNum2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInsNum2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxInsNum2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxInsNum2.Location = new System.Drawing.Point(214, 21);
            this.verifyBoxInsNum2.Name = "verifyBoxInsNum2";
            this.verifyBoxInsNum2.NewLine = false;
            this.verifyBoxInsNum2.Size = new System.Drawing.Size(35, 23);
            this.verifyBoxInsNum2.TabIndex = 7;
            this.verifyBoxInsNum2.TextV = "";
            // 
            // verifyBoxNumN
            // 
            this.verifyBoxNumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumN.Location = new System.Drawing.Point(481, 21);
            this.verifyBoxNumN.Name = "verifyBoxNumN";
            this.verifyBoxNumN.NewLine = false;
            this.verifyBoxNumN.Size = new System.Drawing.Size(67, 23);
            this.verifyBoxNumN.TabIndex = 14;
            this.verifyBoxNumN.TextV = "";
            // 
            // verifyBoxNumM
            // 
            this.verifyBoxNumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumM.Location = new System.Drawing.Point(424, 21);
            this.verifyBoxNumM.Name = "verifyBoxNumM";
            this.verifyBoxNumM.NewLine = false;
            this.verifyBoxNumM.Size = new System.Drawing.Size(57, 23);
            this.verifyBoxNumM.TabIndex = 13;
            this.verifyBoxNumM.TextV = "";
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(488, 586);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(234, 23);
            this.verifyBoxF3.TabIndex = 65;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxKouhi
            // 
            this.verifyBoxKouhi.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKouhi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKouhi.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKouhi.Location = new System.Drawing.Point(322, 21);
            this.verifyBoxKouhi.Name = "verifyBoxKouhi";
            this.verifyBoxKouhi.NewLine = false;
            this.verifyBoxKouhi.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKouhi.TabIndex = 10;
            this.verifyBoxKouhi.TextV = "";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(672, 21);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 19;
            this.verifyBoxFamily.TextV = "";
            // 
            // verifyBoxJuryoType
            // 
            this.verifyBoxJuryoType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxJuryoType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxJuryoType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxJuryoType.Location = new System.Drawing.Point(564, 21);
            this.verifyBoxJuryoType.Name = "verifyBoxJuryoType";
            this.verifyBoxJuryoType.NewLine = false;
            this.verifyBoxJuryoType.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxJuryoType.TabIndex = 16;
            this.verifyBoxJuryoType.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(949, 21);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 31;
            this.verifyBoxBD.TextV = "";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 80);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 427);
            this.scrollPictureControl1.TabIndex = 33;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(906, 21);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 29;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(786, 528);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(72, 23);
            this.verifyBoxNumbering.TabIndex = 59;
            this.verifyBoxNumbering.TextV = "";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(695, 528);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(72, 23);
            this.verifyBoxCharge.TabIndex = 57;
            this.verifyBoxCharge.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(570, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 71;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(602, 528);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(72, 23);
            this.verifyBoxTotal.TabIndex = 55;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(863, 21);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 27;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(340, 528);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 48;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(248, 586);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(234, 23);
            this.verifyBoxF2.TabIndex = 63;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(812, 21);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 25;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(407, 528);
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxNewCont.TabIndex = 50;
            this.verifyBoxNewCont.TextV = "";
            // 
            // verifyBoxFinish
            // 
            this.verifyBoxFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFinish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFinish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFinish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFinish.Location = new System.Drawing.Point(286, 528);
            this.verifyBoxFinish.MaxLength = 2;
            this.verifyBoxFinish.Name = "verifyBoxFinish";
            this.verifyBoxFinish.NewLine = false;
            this.verifyBoxFinish.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFinish.TabIndex = 45;
            this.verifyBoxFinish.TextV = "";
            // 
            // verifyBoxFirstD
            // 
            this.verifyBoxFirstD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstD.Location = new System.Drawing.Point(164, 528);
            this.verifyBoxFirstD.MaxLength = 2;
            this.verifyBoxFirstD.Name = "verifyBoxFirstD";
            this.verifyBoxFirstD.NewLine = false;
            this.verifyBoxFirstD.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFirstD.TabIndex = 39;
            this.verifyBoxFirstD.TextV = "";
            // 
            // verifyBoxFirstM
            // 
            this.verifyBoxFirstM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstM.Location = new System.Drawing.Point(120, 528);
            this.verifyBoxFirstM.MaxLength = 2;
            this.verifyBoxFirstM.Name = "verifyBoxFirstM";
            this.verifyBoxFirstM.NewLine = false;
            this.verifyBoxFirstM.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFirstM.TabIndex = 37;
            this.verifyBoxFirstM.TextV = "";
            // 
            // verifyBoxFirstY
            // 
            this.verifyBoxFirstY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstY.Location = new System.Drawing.Point(76, 528);
            this.verifyBoxFirstY.MaxLength = 2;
            this.verifyBoxFirstY.Name = "verifyBoxFirstY";
            this.verifyBoxFirstY.NewLine = false;
            this.verifyBoxFirstY.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFirstY.TabIndex = 35;
            this.verifyBoxFirstY.TextV = "";
            // 
            // verifyBoxStart
            // 
            this.verifyBoxStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxStart.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStart.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStart.Location = new System.Drawing.Point(232, 528);
            this.verifyBoxStart.MaxLength = 2;
            this.verifyBoxStart.Name = "verifyBoxStart";
            this.verifyBoxStart.NewLine = false;
            this.verifyBoxStart.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxStart.TabIndex = 42;
            this.verifyBoxStart.TextV = "";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(141, 21);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(749, 21);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 22;
            this.verifyBoxSex.TextV = "";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(8, 586);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(234, 23);
            this.verifyBoxF1.TabIndex = 61;
            this.verifyBoxF1.TextV = "";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(93, 21);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "保険者番号上2ケタ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(320, 5);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 12);
            this.label30.TabIndex = 9;
            this.label30.Text = "公費コード上2ケタ";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(247, 624);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 68;
            this.label22.Text = "負傷名5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(670, 5);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 18;
            this.label24.Text = "本人家族";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(562, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 15;
            this.label13.Text = "受診者区分";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 624);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 66;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(486, 571);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 64;
            this.label20.Text = "負傷名3";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 72;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(247, 571);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 62;
            this.label11.Text = "負傷名2";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(810, 5);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 24;
            this.labelBirthday.Text = "生年月日";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(405, 513);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 49;
            this.label2.Text = "新規継続";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(284, 513);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 44;
            this.label23.Text = "終了日";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(74, 513);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 12);
            this.label26.TabIndex = 34;
            this.label26.Text = "初検日";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(499, 513);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 52;
            this.label14.Text = "往療有無";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(230, 513);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 41;
            this.label10.Text = "開始日";
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(194, 539);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 40;
            this.label28.Text = "日";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(253, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "一般: 06\r\n特退: 63";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(150, 539);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 38;
            this.label27.Text = "月";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(316, 539);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 46;
            this.label19.Text = "日";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(106, 539);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 36;
            this.label25.Text = "年";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(8, 18);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(47, 24);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(262, 539);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 43;
            this.label7.Text = "日";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 571);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 60;
            this.label9.Text = "負傷名1";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(747, 6);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 21;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(838, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 24);
            this.label5.TabIndex = 26;
            this.label5.Text = "昭:3\r\n平:4";
            // 
            // labelDays
            // 
            this.labelDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(340, 513);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(53, 12);
            this.labelDays.TabIndex = 47;
            this.labelDays.Text = "診療日数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(776, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 23;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(602, 513);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(53, 12);
            this.labelTotal.TabIndex = 54;
            this.labelTotal.Text = "合計金額";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(786, 513);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 58;
            this.label6.Text = "ナンバリング";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label29.Location = new System.Drawing.Point(349, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(49, 24);
            this.label29.TabIndex = 11;
            this.label29.Text = "子/乳:88\r\n併用:88";
            // 
            // labelCharge
            // 
            this.labelCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(695, 513);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(53, 12);
            this.labelCharge.TabIndex = 56;
            this.labelCharge.Text = "請求金額";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label8.Location = new System.Drawing.Point(699, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 24);
            this.label8.TabIndex = 20;
            this.label8.Text = "本人:2\r\n家族:6";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(975, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 32;
            this.label16.Text = "日";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label12.Location = new System.Drawing.Point(591, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 24);
            this.label12.TabIndex = 17;
            this.label12.Text = "本人:2 六歳:4\r\n家族:6 高齢:8";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(889, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 28;
            this.label18.Text = "年";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(932, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 30;
            this.label17.Text = "月";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxNumM;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxJuryoType;
        private VerifyBox verifyBoxStart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private VerifyCheckBox verifyCheckBoxOryo;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxNewCont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxInsNum2;
        private VerifyBox verifyBoxFinish;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxNumN;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label8;
        private VerifyBox verifyBoxFirstD;
        private VerifyBox verifyBoxFirstM;
        private VerifyBox verifyBoxFirstY;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxKouhi;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxF1FirstE;
    }
}