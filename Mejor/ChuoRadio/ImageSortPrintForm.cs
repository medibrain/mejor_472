﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ChuoRadio
{
    public partial class ImageSortPrintForm : Form
    {
        int cym;
        List<string> imagePaths = new List<string>();
        List<string> fileNames = new List<string>();

        public ImageSortPrintForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            Text += "  " + cym.ToString();
        }

        class AppGroup
        {
            public App MainApp;
            public int Family => MainApp.Family % 100;
            public string PayCode => MainApp.PayCode;
            public string Numbering => MainApp.Numbering;
            public string HihoMark => MainApp.HihoNum.Split('-').Length == 2 ? MainApp.HihoNum.Split('-')[0].PadLeft(10) : "";
            public string HihoNum => MainApp.HihoNum.Split('-').Length == 2 ? MainApp.HihoNum.Split('-')[1].PadLeft(10) : MainApp.HihoNum.PadLeft(10);
            public int Aid => MainApp.Aid;

            public List<App> SubApps = new List<App>();
            public List<string> ImagePaths = new List<string>();

            public AppGroup(App app) => MainApp = app;
        }

        private void buttonDiriSelect_Click(object sender, EventArgs e)
        {
            //画像フォルダの指定
            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != DialogResult.OK) return;

            //画像がそろっているか確認
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                imagePaths.Clear();
                textBox1.Clear();
                var l = System.IO.Directory.GetFiles(d.Name, "*", System.IO.SearchOption.TopDirectoryOnly);

                foreach (var item in fileNames)
                {
                    var fn = d.Name + "\\" + item;
                    if (!l.Contains(fn))
                    {
                        MessageBox.Show($"画像ファイルが見つかりませんでした:{item}\r\n");
                        imagePaths.Clear();
                        buttonOK.Enabled = false;
                        return;
                    }
                    imagePaths.Add(fn);
                }
            }

            buttonOK.Enabled = true;
            textBox1.Text = d.Name;
        }

        private void ImageSortPrintForm_Shown(object sender, EventArgs e)
        {
            //続紙をまとめる
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");

                AppGroup ag = null;
                var ags = new List<AppGroup>();

                var l = App.GetApps(cym);
                foreach (var item in l)
                {
                    if (item.AppType > 0)
                    {
                        ag = new AppGroup(item);
                        ags.Add(ag);
                    }
                    else if (item.AppType == APP_TYPE.続紙 && ag != null)
                    {
                        ag.SubApps.Add(item);
                    }
                }

                wf.LogPrint("並び替えています");
                //ソート 本人家族、支払先コード、被保険者番号の順番
                ags.Sort((x, y) =>
                        x.Family != y.Family ? x.Family.CompareTo(y.Family) :
                        x.PayCode != y.PayCode ? x.PayCode.CompareTo(y.PayCode) :
                        x.HihoMark != y.HihoMark ? x.HihoMark.CompareTo(y.HihoMark) :
                        x.HihoNum != y.HihoNum ? x.HihoNum.CompareTo(y.HihoNum) :
                        x.Aid.CompareTo(y.Aid));

                //画像ファイル一覧作成
                fileNames.Clear();
                foreach (var item in ags)
                {
                    fileNames.Add($"J01{cym.ToString() + item.Numbering}0000.tif");

                    for (int i = 0; i < item.SubApps.Count; i++)
                    {
                        fileNames.Add($"J01{cym.ToString() + item.Numbering + (i + 1).ToString("0000")}.tif");
                    }
                }

                labelInfo.Text = $"申請書数：{ags.Count}\r\n" +
                    $"印刷枚数：{fileNames.Count}";

                numericUpDown1.Maximum = fileNames.Count;
                numericUpDown2.Maximum = fileNames.Count;
                numericUpDown2.Value = fileNames.Count;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int start = (int)numericUpDown1.Value - 1;
            int end = (int)numericUpDown2.Value;

            var l = new List<string>();
            for (int i = start; i < end && i < imagePaths.Count; i++)
            {
                l.Add(imagePaths[i]);
            }

            if (l.Count == 0)
            {
                MessageBox.Show("印刷範囲を正しく指定してください");
                return;
            }

            var pn = Settings.DefaultPrinterName;
            var res = MessageBox.Show(
                $"プリンター:{pn}\r\n" +
                $"印刷枚数:{l.Count}\r\n\r\n" +
                $"印刷を開始します。よろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res != DialogResult.OK) return;

            //印刷
            var ip = new ImagePrint(pn);
            ip.Print(l, false, false);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
