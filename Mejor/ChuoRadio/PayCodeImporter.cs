﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using Mejor.Pay;

namespace Mejor.ChuoRadio
{
    class PayCodeImporter
    {
        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "相手先マスタリスト|相手先マスタリスト*.xls";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データベースの情報を取り込んでいます");
                var dic = new Dictionary<string, PayCode>();
                var l = DB.Main.SelectAll<PayCode>();
                foreach (var item in l) dic.Add(item.Key, item);

                wf.LogPrint("Excelファイルを読み込んでいます");

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    var ex = System.IO.Path.GetExtension(fileName);

                    if (ex == ".xls")
                    {
                        var xls = new HSSFWorkbook(fs);

                        var sheet = xls.GetSheetAt(0);
                        var rowCount = sheet.LastRowNum + 2;

                        for (int i = 0; i < rowCount; i++)
                        {
                            try
                            {
                                var row = sheet.GetRow(i);
                                if (row.GetCell(1).CellType == CellType.Blank)
                                    continue;

                                var p = new PayCode();
                                p.Code = row.GetCell(1).StringCellValue.Trim();
                                p.UpdateDate = int.Parse(row.GetCell(37).StringCellValue.Trim());
                                p.BankNo = row.GetCell(27).StringCellValue.Trim();
                                p.BankName = row.GetCell(28).StringCellValue.Trim();
                                p.BranchNo = row.GetCell(29).StringCellValue.Trim();
                                p.BranchName = row.GetCell(30).StringCellValue.Trim();
                                p.AccountNo = row.GetCell(35).StringCellValue.Trim();
                                p.AccountName = row.GetCell(3).StringCellValue.Trim();
                                p.AccountKana = Utility.ToNarrowKatakana(row.GetCell(36).StringCellValue.Trim());
                                if (dic.ContainsKey(p.Key))
                                {
                                    if (dic[p.Key].UpdateDate > p.UpdateDate) continue;
                                    dic[p.Key] = p;
                                }
                                else
                                {
                                    dic.Add(p.Key, p);
                                }
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("対応しているのは旧Excelファイル形式(.xls)です。");
                    }
                }

                wf.SetMax(dic.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {
                    if (!DB.Main.Excute("DELETE FROM paycode", null, tran))
                    {
                        MessageBox.Show("インポートに失敗しました");
                        return false;
                    }

                    var g = dic.Values
                        .Select((p, i) => new { Index = i, PayCode = p })
                        .GroupBy(x => x.Index / 10)
                        .Select(gr => gr.Select(x => x.PayCode));

                    foreach (var item in g)
                    {
                        wf.InvokeValue += item.Count();
                        if (!DB.Main.Inserts(item, tran))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
                MessageBox.Show("インポートが正常に終了しました");
            }

            return true;
        }


        public static List<PayCode> Select(string accountNo)
        {
            if (accountNo.Length < 7) return new List<PayCode>();
            return DB.Main.Select<PayCode>($"accountno='{accountNo}'").ToList();
        }
    }
}
