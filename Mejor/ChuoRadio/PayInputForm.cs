﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ChuoRadio
{
    public partial class PayInputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsPayCode = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(400, 2000);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 400);
        Point posFusho = new Point(100, 800);
        Point posCost = new Point(800, 2000);
        Point posDays = new Point(600, 800);
        Point posNewCont = new Point(800, 1200);
        Point posOryo = new Point(400, 1200);
        Point posNumbering = new Point(800, 2600);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, numberingControls;

        public PayInputForm(ScanGroup sGroup, bool firstTime, int aid)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxAccountNo };
            hihoNumControls = new Control[] { };
            personControls = new Control[] { };
            dayControls = new Control[] { };
            costControls = new Control[] { };
            fushoControls = new Control[] { };
            newContControls = new Control[] { };
            oryoControls = new Control[] { };
            numberingControls = new Control[] { };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            list = list.FindAll(x => x.YM > 0);
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            var l = new List<PayCode>();
            bsPayCode.DataSource = l;
            dataGridPayCode.DataSource = bsPayCode;
            dataGridPayCode.Columns[nameof(PayCode.BankName)].Width = 70;
            dataGridPayCode.Columns[nameof(PayCode.BankName)].HeaderText = "銀行名";
            dataGridPayCode.Columns[nameof(PayCode.BranchName)].Width = 70;
            dataGridPayCode.Columns[nameof(PayCode.BranchName)].HeaderText = "支店名";
            dataGridPayCode.Columns[nameof(PayCode.Code)].Width = 54;
            dataGridPayCode.Columns[nameof(PayCode.Code)].HeaderText = "コード";
            dataGridPayCode.Columns[nameof(PayCode.AccountName)].Width = 240;
            dataGridPayCode.Columns[nameof(PayCode.AccountName)].HeaderText = "口座名義";
            dataGridPayCode.Columns[nameof(PayCode.AccountKana)].Width = 240;
            dataGridPayCode.Columns[nameof(PayCode.AccountKana)].HeaderText = "口座名義カナ";
            dataGridPayCode.Columns[nameof(PayCode.BankNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.BranchNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.AccountNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.UpdateDate)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.Key)].Visible = false;
            dataGridPayCode.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridPayCode.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            bsPayCode.CurrentChanged += BsPayCode_CurrentChanged;

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxAccountNo.Focus();
        }

        private void BsPayCode_CurrentChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(verifyBoxPayCode.Text)) return;
            if (bsPayCode.Count != 1) return;

            var listCode = ((PayCode)dataGridPayCode.CurrentRow?.DataBoundItem)?.Code ?? string.Empty;
            verifyBoxPayCode.Text = listCode;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
            bsPayCode.ResetBindings(false);
            
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (numberingControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            int aNumber = verifyBoxAccountNo.GetIntValue();
            setStatus(verifyBoxAccountNo, aNumber < 1 || verifyBoxAccountNo.Text.Trim().Length != 7);

            int gCode = verifyBoxPayCode.GetIntValue();
            setStatus(verifyBoxPayCode, gCode < 1 || verifyBoxPayCode.Text.Trim().Length < 3);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //値の反映
            app.AccountNumber = verifyBoxAccountNo.Text.Trim();
            app.PayCode = verifyBoxPayCode.Text.Trim();
            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(STATUS_FLAG.拡張ベリ済)) return true;

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInputEx : App.UPDATE_TYPE.SecondInputEx;
                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);
            bsPayCode.Clear();

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.UfirstEx) +
                "\r\n入力2:  " + User.GetUserName(app.UsecondEx);

            //App_Flagのチェック
            if (app.StatusFlagCheck(STATUS_FLAG.拡張入力済))
            {
                setValues(app);
            }
            else
            {
                bsPayCode.ResetBindings(false);
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        private void verifyBoxAccountNo_Leave(object sender, EventArgs e)
        {
            bsPayCode.DataSource = PayCode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.ResetBindings(false);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (numberingControls.Contains(t)) posNumbering = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(STATUS_FLAG.拡張入力済)) return;
            var nv = !app.StatusFlagCheck(STATUS_FLAG.拡張ベリ済);

            //申請書
            setValue(verifyBoxAccountNo, app.AccountNumber, firstTime, nv);
            setValue(verifyBoxPayCode, app.PayCode, firstTime, nv);

            //リスト
            var l = PayCode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.DataSource = l;
            bsPayCode.ResetBindings(false);
            if (l.Count > 1) dataGridPayCode.CurrentCell = null;

            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxAccountNo.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
