﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ChuoRadio
{
    public class Export
    {
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,ナンバリング,本人/家族,処理年,処理月,保険者番号,記号,番号," +
                        "性別,生年月日,被保険者名,受療者名,郵便番号,住所,請求区分,診療年,診療月," +
                        "診療日数,合計金額,請求金額,施術所名,点検結果,照会理由,メモ");

                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100,int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


                    var notifyNumber = $"{jcy}{jcm}-{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<string, string[]> toAddresses = address =>
                    {
                        var result = new string[] { "", "" };
                        if (!address.Contains("　"))
                        {
                            result[0] = address;
                        }
                        else
                        {
                            var index = address.IndexOf("　");
                            result[0] = address.Substring(0, index);
                            result[1] = address.Substring(index + 1);
                        }
                        return result;
                    };

                    foreach (var item in list)
                    {
                        var adds = toAddresses(item.HihoAdd);
                        //"AID,ナンバリング,本人/家族,処理年,処理月,保険者番号,記号,番号,"
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.Numbering);
                        ss.Add(item.Family % 100 == 2 ? "本人" : item.Family % 100 == 6 ? "家族" : "");
                        ss.Add(jcy);
                        ss.Add(jcm);
                        ss.Add(item.InsNum + "133169");
                        var hs = item.HihoNum.Split('-');
                        ss.Add(hs.Length == 2 ? hs[0] : throw new Exception($"AID:{item.Aid} 被保険者記号番号が不正です"));
                        ss.Add(hs.Length == 2 ? hs[1] : throw new Exception($"AID:{item.Aid} 被保険者記号番号が不正です"));

                        //"性別,生年月日,被保険者名,受療者名,郵便番号,住所,請求区分,診療年,診療月,"
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToShortDateString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd);
                        ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());

                        //"診療日数,合計金額,請求金額,施術所名,点検結果,照会理由,メモ");
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.ClinicName);
                        ss.Add(item.TenkenResult);
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add("\"" + item.Memo + "\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
