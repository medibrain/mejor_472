﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Mejor.ChuoRadio
{
    class SubmitData
    {
        App app;

        string BranchCode => "01";
        string SubmitYM => app.CYM.ToString();
        string Numbering => app.Numbering.PadLeft(7, '0');
        string SEQ => "00";
        string ChargeCode => app.PayCode;
        string SpecialType => app.InsNum == "63" ? "3" : "1";
        string KyufuCode => "11" +
            (app.AppType == APP_TYPE.あんま ? "3" : app.AppType == APP_TYPE.鍼灸 ? "2" : "1") +
            ((app.Family / 100).ToString());
        string Mark => app.HihoNum.Split('-')[0];
        string Number => app.HihoNum.Split('-')[1];
        string Sex => app.Sex == 2 ? "6" : "5";
        string Birth => DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday).ToString();
        string Teishotoku => "9";
        string Daisansha => "9";
        string KouhiCode => app.PublcExpense;
        string Tenki => string.Empty;
        string Shippei1 => "1905";
        string Start1 => DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFirstDate1).ToString();
        string Shippei2 => string.Empty;
        string Start2 => string.Empty;
        string Shippei3 => string.Empty;
        string Start3 => string.Empty;
        string Shippei4 => string.Empty;
        string Start4 => string.Empty;
        string Shippei5 => string.Empty;
        string Start5 => string.Empty;
        string StartDate => DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1).ToString();
        string FinishDate => DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFinishDate1).ToString();
        string Days => app.CountedDays.ToString();
        string Total => app.Total.ToString();
        string Partial => string.Empty;
        string Charge => string.Empty;
        string Saigai => "9";

        string createCsvLine()
        {
            var ss = new string[] { BranchCode, SubmitYM,Numbering, SEQ,
                ChargeCode, SpecialType, KyufuCode, Mark, Number, Sex, Birth,
                Teishotoku, Daisansha, KouhiCode, Tenki, Shippei1, Start1,
                Shippei2, Start2, Shippei3, Start3, Shippei4, Start4, Shippei5, Start5,
                StartDate, FinishDate, Days, Total, Partial, Charge, Saigai
            };

            return string.Join(",", ss);
        }

        public SubmitData(App app)
        {
            this.app = app;
        }

        public static bool Export(int cym)
        {
            string csvName;
            string dir;

            using (var f = new System.Windows.Forms.SaveFileDialog())
            {
                f.FileName = "KENPO7.CSV";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;

                csvName = f.FileName;
                dir = System.IO.Path.GetDirectoryName(csvName);
            }

            using (var wf = new WaitForm())
            {
                try
                {
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("対象データを取得しています");
                    var l = App.GetApps(cym);

                    var lastNumbering = string.Empty;
                    var page = 0;
                    var fn = string.Empty;

                    var fc = new TiffUtility.FastCopy();

                    wf.LogPrint("出力しています");
                    wf.SetMax(l.Count);
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                    var fImage = Properties.Resources.family;

                    // TIFF 形式のエンコーダ
                    var imageEncoders = ImageCodecInfo.GetImageEncoders();
                    var pre = new Predicate<ImageCodecInfo>(input => input.FormatID == ImageFormat.Tiff.Guid);
                    var tiffEncoder = Array.Find(imageEncoders, pre);

                    // TIFFエンコーダのパラメータ
                    var tiffEncoderParameters = new EncoderParameters(1);
                    tiffEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression,
                        (long)EncoderValue.CompressionLZW);

                    using (var ep = new EncoderParameters(1))
                    using (var sw = new System.IO.StreamWriter(csvName, false, Encoding.GetEncoding("Shift_JIS")))
                    using (var f = new System.Drawing.Font("ＭＳゴシック", 14))
                    using (var p = new Pen(Brushes.Black, 3))
                    {
                        ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionLZW);

                        for (int i = 0; i < l.Count; i++)
                        {
                            wf.InvokeValue++;

                            if (l[i].AppType == APP_TYPE.続紙)
                            {
                                //続紙画像出力
                                page++;
                                fn = dir + "\\" + lastNumbering + page.ToString("0000") + ".tif";
                                fc.FileCopy(l[i].GetImageFullPath(), fn);
                                continue;
                            }
                            else if ((int)l[i].AppType < 1)
                            {
                                //申請書以外
                                continue;
                            }
                            else
                            {
                                //申請書画像出力
                                var family = l[i].Family % 100;
                                using (var fs = File.OpenRead(l[i].GetImageFullPath()))
                                using (var img = Image.FromStream(fs, false, false))
                                using (var g = Graphics.FromImage(img))
                                {
                                    g.DrawString(l[i].PayCode, f, Brushes.Black, 1150, 50);

                                    //20200527171254 furukawa st ////////////////////////
                                    //家スタンプの印字位置を固定値ではなく横ピクセルの85％の位置にする
                                    
                                    if (family == 6) g.DrawImage(fImage, float.Parse((img.Width * 0.85).ToString()), 50, 180, 180);
                                    //if (family == 6) g.DrawImage(fImage, 2250, 50, 180, 180);
                                    //20200527171254 furukawa ed ////////////////////////



                                    lastNumbering = "J01" + cym.ToString() + l[i].Numbering;
                                    page = 0;
                                    fn = dir + "\\" + lastNumbering + page.ToString("0000") + ".tif";
                                    //ImageUtility.ToGrayScale(img);

                                    TiffUtility.CreateTiffWrappedJpeg(img, fn, 80);
                                }
                                //csv出力
                                var sf = new SubmitData(l[i]);
                                sw.WriteLine(sf.createCsvLine());
                            }
                        }

                        System.Windows.Forms.MessageBox.Show("出力が完了しました");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    System.Windows.Forms.MessageBox.Show("出力に失敗しました", "",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }
    }
}
