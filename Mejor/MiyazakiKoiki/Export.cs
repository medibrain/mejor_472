﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;

namespace Mejor.MiyazakiKoiki
{
    class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var wf = new WaitForm();

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,照会ID,記号番号,被保険者名,性別,生年月日,施術年,施術月,実日数,合計,施術所番号,施術所名," +
                        "宛名氏名,郵便番号,住所,電算番号,照会理由,点検結果,過誤理由,再審査理由,点検メモ,返戻理由,メモ";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    string insNoStr = ((int)InsurerID.MIYAZAKI_KOIKI).ToString();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        var kd = RefRece.Select(item.Numbering);

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(insNoStr + ymStr + no.ToString("0000"));
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.PersonName);
                        ss.Add(item.Sex.ToString());
                        ss.Add(DateTimeEx.GetShortJpDateStr(item.Birthday));
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(string.Empty);
                        ss.Add(kd?.DestName ?? string.Empty);
                        ss.Add(kd?.Zip ?? string.Empty);
                        ss.Add(kd == null ? string.Empty : kd.PrefName + kd.CityName + kd.Add);
                        ss.Add(item.Numbering);
                        ss.Add(item.ShokaiReason.ToString().Replace(",", " "));
                        ss.Add(item.TenkenResult);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add(item.MemoInspect);
                        ss.Add(item.HenreiReasonStr);
                        ss.Add(item.Memo);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        /// <summary>
        /// 新ビューア形式出力
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool ExportNewViewerData(int cym)
        {
            string infoFileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Infoファイル|Info.txt";
                if (f.ShowDialog() != DialogResult.OK) return false;
                infoFileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(infoFileName);
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //提供データ取得
                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                var hs = new HashSet<string>();
                rrs.ForEach(rr => hs.Add(rr.Numbering));

                //マッチング確認
                wf.LogPrint("マッチングを確認しています");
                foreach (var app in apps)
                {
                    if (app.YM.ToString() ==string.Empty) MessageBox.Show("");
                    if (app.YM < 0) continue;
                    if (!hs.Contains(app.ComNum))
                    {
                        wf.LogPrint($"突合データがない申請書があります AID:{app.Aid}");
                        continue;
                    }
                }

                //突合なしデータ
                wf.LogPrint("突合なしデータを抽出しています");
                string notMatchCSV = dir + $"\\突合なし広域データ{cym}.csv";
                var notList = RefRece.GetNotMatchDatas(cym);
                using (var sw = new StreamWriter(notMatchCSV, false, Encoding.UTF8))
                {
                    sw.WriteLine("\"電算管理番号\",\"保険者番号\"," +
                        "\"被保険者番号\",\"性別\",\"生年月日\",\"医療機関番号\"," +
                        "\"請求年月\",\"診療年月\",\"初検日\",\"実日数\"," +
                        "\"負担率\",\"合計額\",\"請求額\",\"負担額\",\"氏名\"," +
                        "\"郵便番号\",\"都道府県\",\"市町村\",\"住所\",\"送付先氏名\"");

                    foreach (var item in notList)
                    {
                        if (!item.CreateNotMatchCsv(sw))
                        {
                            var res = MessageBox.Show("エラーが発生しました。出力を続けますか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (res != DialogResult.OK) return false;
                        }
                    }
                }

                //ビューアデータ
                wf.LogPrint("新形式のViewerデータで処理します");
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nマッチングなしデータ数 :{notList.Count}" +
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }
    }
}
