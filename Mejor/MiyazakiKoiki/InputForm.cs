﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;


namespace Mejor.MiyazakiKoiki
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHosCode = new Point(400, 800);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 250);
        Point posCost = new Point(400, 800);
        Point posDays = new Point(400, 300);
        Point posNumbering = new Point(300, 0);
        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);
        Point posOryo = new Point(100, 400);

        Control[] ymControls, fushoControls, oryoControls;

        public InputForm(ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            oryoControls = new Control[] {checkBoxVisit,checkBoxVisitKasan,
                verifyBoxKasanKm, labelOryoKasan, verifyBoxOryoCost};

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox || item is CheckBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (Control)sender;
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //往療料のチェック
            decimal visitDistance = 0;
            int kihonOryo = 0;
            int kasanOryo = 0;
            
            if (checkBoxVisit.Checked && checkBoxVisitKasan.Checked)
            {   
                if (!string.IsNullOrEmpty(verifyBoxKasanKm.Text))
                {
                    //加算Kmの入力がある場合
                    if (checkBoxVisitKasan.Checked &&
                        !decimal.TryParse(verifyBoxKasanKm.Text, out visitDistance))
                    {
                        setStatus(verifyBoxKasanKm, true);
                    }
                }
                else
                {
                    //Kmではなく金額で入力している場合
                    if (string.IsNullOrEmpty(verifyBoxOryoCost.Text) ||
                        !int.TryParse(verifyBoxOryoCost.Text, out kihonOryo))
                    {
                        setStatus(verifyBoxOryoCost, true);
                    }

                    if (string.IsNullOrEmpty(verifyBoxKasanCost.Text) ||
                        !int.TryParse(verifyBoxKasanCost.Text, out kasanOryo))
                    {
                        setStatus(verifyBoxKasanCost, true);
                    }

                    if (!hasError)
                    {
                        // 加算距離を計算
                        int visitTimes = kihonOryo / 1800;
                        int incrementKasan = kasanOryo / visitTimes / 800;
                        visitDistance = incrementKasan * 2;
                        // 距離を消していたら対応
                        if (string.IsNullOrEmpty(verifyBoxKasanKm.Text))
                        {
                            visitDistance = 0;
                        }
                    }
                }
            }

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //広域データからデータ取得
            var rr = RefRece.Select(app.Numbering);
            if (rr == null)
            {
                MessageBox.Show("広域提供データから一致する情報を見つけられませんでした。登録できません。\r\n",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //値の反映
            var ymd = DateTimeEx.ToDateTime(rr.YM * 100 + 1);
            app.MediYear = DateTimeEx.GetJpYear(ymd);
            app.MediMonth = rr.YM % 100;
            app.InsNum = rr.InsNumber;
            app.HihoNum = rr.Number;
            app.Family = 2;
            app.HihoType = 0;
            app.Sex = rr.Sex;
            app.Birthday = rr.Birth;
            app.PersonName = rr.Name;
            app.HihoZip = rr.Zip;
            app.HihoAdd = rr.PrefName + rr.CityName + rr.Add;
            app.CountedDays = rr.Days;
            app.Total = rr.Total;
            app.Charge = rr.Charge;
            app.Partial = rr.Futan;
            app.NewContType = ymd.Year == rr.FirstDay.Year && ymd.Month == rr.FirstDay.Month ? NEW_CONT.新規 : NEW_CONT.継続;
            app.FushoFirstDate1 = rr.FirstDay;
            app.Ratio = rr.Ratio;
            app.DrNum = rr.DrNumber;
            app.Distance = checkBoxVisit.Checked ? 999 : 0;
            app.VisitAdd = (int)(visitDistance * 1000m);
            app.VisitFee = kihonOryo;
            app.VisitTimes = kasanOryo;
            app.AppType = scan.AppType;
            app.ComNum = rr.Numbering;

            app.TaggedDatas.DestName = rr.DestName == rr.Name ? string.Empty : rr.DestName;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                var com = app.Numbering;
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
                app.Numbering = com;
                app.ComNum = com;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                var com = app.Numbering;
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                app.Numbering = com;
                app.ComNum = com;
            }
            else
            {
                //データベース登録用のAppインスタンスを作成
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = App.UPDATE_TYPE.FirstInput;

                if (app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力:  " + User.GetUserName(app.Ufirst);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="a"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
                scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App app)
        {
            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else
            {
                //申請書
                ChangeVisitControls(false);
                ChangeVisitKasanControls(false);

                //往療
                if (app.Distance == 999) checkBoxVisit.Checked = true;

                // 加算
                if (app.VisitAdd > 0)
                {
                    decimal visitAdd = Convert.ToDecimal(app.VisitAdd) / 1000m;
                    checkBoxVisitKasan.Checked = true;
                    verifyBoxKasanKm.Text = string.Format("{0:0.#}", visitAdd);
                }

                // 基本往療料
                if (app.VisitFee > 0)
                {
                    checkBoxVisitKasan.Checked = true;
                    verifyBoxOryoCost.Text = app.VisitFee.ToString();
                }

                // 加算往療料
                if (app.VisitTimes > 0)
                {
                    checkBoxVisitKasan.Checked = true;
                    verifyBoxKasanCost.Text = app.VisitTimes.ToString();
                }

                if (app.FushoFirstDate1.IsNullDate())
                {
                    var ocr = app.OcrData.Split(',');
                    if (ocr.Length > 40 && ocr[40] != null)
                    {
                        var f = Fusho.Change(ocr[40]);
                        if (Fusho.BasicWordsCheck(f)) verifyBoxF1.Text = f;
                    }
                    if (ocr.Length > 57 && ocr[57] != null)
                    {
                        var f = Fusho.Change(ocr[57]);
                        if (Fusho.BasicWordsCheck(f)) verifyBoxF2.Text = f;
                    }
                    if (ocr.Length > 74 && ocr[74] != null)
                    {
                        var f = Fusho.Change(ocr[74]);
                        if (Fusho.BasicWordsCheck(f)) verifyBoxF3.Text = f;
                    }
                    if (ocr.Length > 91 && ocr[91] != null)
                    {
                        var f = Fusho.Change(ocr[91]);
                        if (Fusho.BasicWordsCheck(f)) verifyBoxF4.Text = f;
                    }
                    if (!string.IsNullOrWhiteSpace(app.OcrData) && ocr[108] != null)
                    {
                        var f = Fusho.Change(ocr[108]);
                        if (Fusho.BasicWordsCheck(f)) verifyBoxF5.Text = f;
                    }
                }
                else
                {
                    verifyBoxF1.Text = app.FushoName1;
                    verifyBoxF2.Text = app.FushoName2;
                    verifyBoxF3.Text = app.FushoName3;
                    verifyBoxF4.Text = app.FushoName4;
                    verifyBoxF5.Text = app.FushoName5;
                }
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelTotal.Visible = false;
            }
            else
            {
                //申請書の場合
                panelTotal.Visible = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisit.Checked;
            ChangeVisitControls(bEnable);
            ChangeVisitKasanControls(checkBoxVisitKasan.Checked && bEnable);
        }

        private void checkBoxVisitKasan_CheckedChanged(object sender, EventArgs e)
        {
            bool bEnable = checkBoxVisitKasan.Checked;
            ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitControls(bool bEnable)
        {
            checkBoxVisitKasan.Enabled = bEnable;
            if (checkBoxVisitKasan.Checked) ChangeVisitKasanControls(bEnable);
        }

        private void ChangeVisitKasanControls(bool bEnable)
        {
            labelOuryo.Enabled = bEnable;
            labelOuryo2.Enabled = bEnable;
            verifyBoxKasanKm.Enabled = bEnable;
            ChangeVisitKasanExtraControls(bEnable);
        }

        private void ChangeVisitKasanExtraControls(bool bEnable)
        {
            labelOryoBasic.Enabled = bEnable;
            labelOryoKasan.Enabled = bEnable;
            verifyBoxOryoCost.Enabled = bEnable;
            verifyBoxKasanCost.Enabled = bEnable;
        }

        private void label25_Click(object sender, EventArgs e)
        {
            checkBoxVisit.Focus();
            checkBoxVisit.Checked = !checkBoxVisit.Checked;
        }

        private void labelOuryoKasan_Click(object sender, EventArgs e)
        {
            checkBoxVisitKasan.Focus();
            checkBoxVisitKasan.Checked = !checkBoxVisitKasan.Checked;
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var t = (Control)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
