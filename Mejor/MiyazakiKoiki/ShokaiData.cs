﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.MiyazakiKoiki
{
    class ShokaiData// : Shokai.ShokaiData
    {
        public static bool Import(string fileName)
        {
            var sql = "INSERT INTO shokai (aid, shokai_id, ym, barcode, " +
                "shokai_reason, shokai_status, shokai_result, henrei, " +
                "note, update_date, update_uid) " +
                "VALUES (:aid, :shokai_id, :ym, :barcode, " +
                ":shokai_reason, :shokai_status, :shokai_result, :henrei, " +
                ":note, :update_date, :update_uid);";

            using (var tran = DB.Main.CreateTransaction())
            using (var cmd = DB.Main.CreateCmd(sql, tran))
            using (var sr = new System.IO.StreamReader(fileName, Encoding.GetEncoding("Shift_JIS")))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer);
                cmd.Parameters.Add("shokai_id", NpgsqlDbType.Integer);
                cmd.Parameters.Add("ym", NpgsqlDbType.Integer);
                cmd.Parameters.Add("barcode", NpgsqlDbType.Text);
                cmd.Parameters.Add("shokai_reason", NpgsqlDbType.Integer);
                cmd.Parameters.Add("shokai_status", NpgsqlDbType.Integer).Value = (int)SHOKAI_STATUS.照会中;
                cmd.Parameters.Add("shokai_result", NpgsqlDbType.Integer).Value = (int)SHOKAI_RESULT.なし;
                cmd.Parameters.Add("henrei", NpgsqlDbType.Boolean).Value = false;
                cmd.Parameters.Add("note", NpgsqlDbType.Text).Value = string.Empty;
                cmd.Parameters.Add("update_date", NpgsqlDbType.Date).Value = DateTime.Today;
                cmd.Parameters.Add("update_uid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;

                //1行目はヘッダ
                sr.ReadLine();
                while (sr.Peek() > 0)
                {
                    var ss = sr.ReadLine().Split(',');
                    if (ss.Length < 7) continue;

                    int aid, sid, ym;
                    int.TryParse(ss[0], out aid);
                    int.TryParse(ss[4], out sid);
                    int.TryParse(ss[1], out ym);
                    if (aid == 0 || ym == 0) continue;

                    var reason = ShokaiReason.なし;
                    if (ss[22].Contains("頻回")) reason |= ShokaiReason.施術日数;
                    if (ss[22].Contains("多部位")) reason |= ShokaiReason.部位数;
                    if (reason == ShokaiReason.なし) reason = ShokaiReason.その他;

                    cmd.Parameters["aid"].Value = aid;
                    cmd.Parameters["shokai_id"].Value = sid;
                    cmd.Parameters["ym"].Value = ym;
                    cmd.Parameters["barcode"].Value = ss[4];
                    cmd.Parameters["shokai_reason"].Value = (int)reason;

                    if (!cmd.TryExecuteNonQuery())
                    {
                        tran.Rollback();
                        return false;
                    }
                }

                tran.Commit();
                return true;
            }
        }
    }
}
