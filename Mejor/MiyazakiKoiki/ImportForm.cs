﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.MiyazakiKoiki
{
    public partial class ImportForm : Form
    {
        [DB.DbAttribute.DifferentTableName("refrece")]
        class RefReceCounter
        {            
            public int Cym { get; set; }
            public int ReceCount { get; set; }

            public static List<RefReceCounter> GetCount()
            {
                var sql = "SELECT cym, count(cym) AS rececount FROM refrece " +
                    "GROUP BY cym ORDER BY cym DESC;";

                return DB.Main.Query<RefReceCounter>(sql).ToList();
            }
        }


        public ImportForm()
        {
            InitializeComponent();
            dataGridView1.DataSource = RefReceCounter.GetCount();
            dataGridView1.Columns[nameof(RefReceCounter.Cym)].HeaderText = "処理年月";
            dataGridView1.Columns[nameof(RefReceCounter.Cym)].DefaultCellStyle.Format = "0000/00";
            dataGridView1.Columns[nameof(RefReceCounter.ReceCount)].HeaderText = "件数";
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonBefore_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "事前パンチデータファイル|REZEPT*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                RefRece.BeforeImport(f.FileName);
            }
        }

        private void buttonAfter_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "柔整申請書点検用データ|*柔整申請書点検用データ.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                RefRece.AfterImport(f.FileName);
            }
        }
    }
}
