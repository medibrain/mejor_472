﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20211105170816 furukawa ステータスフラグ管理画面

namespace Mejor
{    
    public partial class StatusFlagsMng : Form
    {
        public int cym;

        public StatusFlagsMng(int _cym)
        {
            InitializeComponent();
            cym = _cym;
            Init();
            InitGrid();
        }


        private void InitGrid()
        {

            dgv.Columns[0].Frozen = true;
            dgv.Columns[0].Width = 100;
            dgv.Columns[1].Width = 300;
            dgv.Columns[2].Width = 100;
            dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            //DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            //bc.Text = "調査";
            //bc.UseColumnTextForButtonValue = true;
            //dgv.Columns.Add(bc);
            //dgv.Columns[3].Width = 80;
            
        }

        private void Init()
        {
            
            DataTable dt = new DataTable();

            dt.Columns.Add("aid");
            dt.Columns.Add("ステータスフラグ（ビット）");
            dt.Columns.Add("ステータスフラグ（10進）");
            for (int c = 0;c<30;c++) {
                dt.Columns.Add();
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select aid,statusflags::bit(30),statusflags, ");

            sb.AppendLine(" case  when (statusflags & 1)=1 then '入力済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 2)=2 then 'ベリファイ済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 4)=4 then '自動マッチ済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 8)=8 then '入力時エラー' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 16)=16 then '点検対象' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 32)=32 then '点検済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 64)=64 then '返戻' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 128)=128 then '支払保留' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 256)=256 then '照会対象' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 512)=512 then '保留' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 1024)=1024 then '過誤' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 2048)=2048 then '再審査' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 4096)=4096 then '往療点検対象' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 8192)=8192 then '往療疑義' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 16384)=16384 then '往療疑義なし' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 32768)=32768 then '往療点検済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 65536)=65536 then '追加入力済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 131072)=131072 then '追加ベリ済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 262144)=262144 then '拡張入力済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 524288)=524288 then '拡張ベリ済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 1048576)=1048576 then '架電対象' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 2097152)=2097152 then '支払済' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 4194304)=4194304 then '処理1' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 8388608)=8388608 then '処理2' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 16777216)=16777216 then '処理3' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 33554432)=33554432 then '処理4' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 67108864)=67108864 then '処理5' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 134217728)=134217728 then 'なし' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 268435456)=268435456 then '独自処理1' else '' end  ,");
            sb.AppendLine(" case  when (statusflags & 536870912)=536870912 then '独自処理2' else '' end  ");


            sb.AppendLine($" from application where cym={cym} order by aid;");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());

            try
            {
                
                var list = cmd.TryExecuteReaderList();
                foreach (var item in list)
                {

                    DataRow dr = dt.NewRow();
                    dr[0] = int.Parse(item[0].ToString());
                    dr[1] = item[1].ToString();
                    dr[2] = item[2].ToString();

                    for(int c = 0; c < 30; c++)
                    {
                        dr[c] = item[c].ToString();
                    }
                    dt.Rows.Add(dr);

                }


                dgv.DataSource = dt;

               // AddStatus();

            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                cmd.Dispose();   
            }

        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Update文出力";
            sfd.Filter = "*.txt|txt";
            sfd.FileName = $"statusflags_Update_{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}.txt";
            sfd.ShowDialog();

            
            System.IO.StreamWriter sw = new System.IO.StreamWriter(sfd.FileName,false,System.Text.Encoding.GetEncoding("utf-8"));

            try
            {
                List<string> lst = CreateUpdateFile();
                foreach(string s in lst)
                {
                    sw.WriteLine(s);
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                sw.Close();
                System.Diagnostics.Process.Start(sfd.FileName);
            }
        }

        private List<string> CreateUpdateFile()
        {
            List<string> lst = new List<string>();
            foreach (DataGridViewRow r in dgv.Rows)
            {
                string strUpd = string.Empty;
                strUpd = $"Update application set statusflags = {r.Cells[2].Value.ToString()} " +
                    $" where aid = {r.Cells[0].Value.ToString()} " +
                    $" and cym = {cym};";
                lst.Add(strUpd);
            }
            return lst;
            
        }

        private void AddStatus()
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(dgv.Rows.Count);
           

            foreach (DataGridViewRow r in dgv.Rows)
            {
                if (CommonTool.WaitFormCancelProcess(wf))
                {
                    wf.Dispose();
                    return;
                }
                wf.LogPrint($"調査中 aid:{r.Cells["aid"].Value.ToString()}");
                
                App app = App.GetApp(int.Parse(r.Cells["aid"].Value.ToString()));

                string strSF = string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.未処理) == true ? "未処理" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.入力済) == true ? "入力済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.ベリファイ済) == true ? "ベリファイ済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.自動マッチ済) == true ? "自動マッチ済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.入力時エラー) == true ? "入力時エラー" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.点検対象) == true ? "点検対象" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.点検済) == true ? "点検済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.返戻) == true ? "返戻" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.支払保留) == true ? "支払保留" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.照会対象) == true ? "照会対象" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.保留) == true ? "保留" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.過誤) == true ? "過誤" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.再審査) == true ? "再審査" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.往療点検対象) == true ? "往療点検対象" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.往療疑義) == true ? "往療疑義" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.往療点検済) == true ? "往療点検済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.追加入力済) == true ? "追加入力済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.追加ベリ済) == true ? "追加ベリ済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.拡張入力済) == true ? "拡張入力済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.拡張ベリ済) == true ? "拡張ベリ済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.架電対象) == true ? "架電対象" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.支払済) == true ? "支払済" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.処理1) == true ? "処理1" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.処理2) == true ? "処理2" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.処理3) == true ? "処理3" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.処理4) == true ? "処理4" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.処理5) == true ? "処理5" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.独自処理1) == true ? "独自処理1" + "|" : string.Empty;
                strSF += app.StatusFlagCheck(StatusFlag.独自処理2) == true ? "独自処理2" + "|" : string.Empty;


                r.Cells[3].Value = strSF;

                wf.InvokeValue++;
            }
            wf.Dispose();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow r in dgv.Rows)
            {
                if (r.Cells[0].Value.ToString() == textBoxAID.Text.Trim())
                {
                    dgv.FirstDisplayedCell = r.Cells[0];
                    r.Selected = true;
                    break;
                }
            }
        }

  
    }
}
