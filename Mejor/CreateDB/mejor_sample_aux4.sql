PGDMP         $    
            y            test    11.10    11.7 ;    Ú           0    0    ENCODING    ENCODING     (   SET client_encoding = 'SHIFT_JIS_2004';
                       false            Û           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            Ü           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            Ý           1262    550303    test    DATABASE     v   CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE test;
             postgres    false            Þ           0    0    SCHEMA public    ACL     ¢   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    3            Ï            1255    550304    addmonth(integer, integer)    FUNCTION       CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public       postgres    false            Ð            1255    550305    appcounter_update()    FUNCTION       CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public       postgres    false            Ä            1259    550331 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public         postgres    false            Å            1259    550335    application    TABLE     à  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public         postgres    false            Æ            1259    550464    application_aux    TABLE     ®  CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
 #   DROP TABLE public.application_aux;
       public         postgres    false            ß           0    0    COLUMN application_aux.aid    COMMENT     7   COMMENT ON COLUMN public.application_aux.aid IS 'aid';
            public       postgres    false    198            à           0    0    COLUMN application_aux.cym    COMMENT     D   COMMENT ON COLUMN public.application_aux.cym IS 'z[¿N';
            public       postgres    false    198            á           0    0    COLUMN application_aux.scanid    COMMENT     A   COMMENT ON COLUMN public.application_aux.scanid IS 'XLID';
            public       postgres    false    198            â           0    0    COLUMN application_aux.groupid    COMMENT     B   COMMENT ON COLUMN public.application_aux.groupid IS 'O[vID';
            public       postgres    false    198            ã           0    0 !   COLUMN application_aux.aimagefile    COMMENT     O   COMMENT ON COLUMN public.application_aux.aimagefile IS 'Ï·ãæt@C¼';
            public       postgres    false    198            ä           0    0    COLUMN application_aux.origfile    COMMENT     I   COMMENT ON COLUMN public.application_aux.origfile IS '³æt@C¼';
            public       postgres    false    198            å           0    0 '   COLUMN application_aux.multitiff_pageno    COMMENT     Y   COMMENT ON COLUMN public.application_aux.multitiff_pageno IS '}`tiffæy[WÔ';
            public       postgres    false    198            æ           0    0    COLUMN application_aux.aapptype    COMMENT     F   COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDÌaapptype';
            public       postgres    false    198            ç           0    0     COLUMN application_aux.parentaid    COMMENT     K   COMMENT ON COLUMN public.application_aux.parentaid IS '±Ìe\¿AID';
            public       postgres    false    198            è           0    0    COLUMN application_aux.batchaid    COMMENT     F   COMMENT ON COLUMN public.application_aux.batchaid IS '®ob`AID';
            public       postgres    false    198            é           0    0 #   COLUMN application_aux.matchingid01    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid01 IS '}b`Oµ½f[^ÌID1';
            public       postgres    false    198            ê           0    0 #   COLUMN application_aux.matchingid02    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid02 IS '}b`Oµ½f[^ÌID2';
            public       postgres    false    198            ë           0    0 #   COLUMN application_aux.matchingid03    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid03 IS '}b`Oµ½f[^ÌID3';
            public       postgres    false    198            ì           0    0 #   COLUMN application_aux.matchingid04    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid04 IS '}b`Oµ½f[^ÌID4';
            public       postgres    false    198            í           0    0 #   COLUMN application_aux.matchingid05    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid05 IS '}b`Oµ½f[^ÌID5';
            public       postgres    false    198            î           0    0 '   COLUMN application_aux.matchingid01date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid01date IS '}b`O1';
            public       postgres    false    198            ï           0    0 '   COLUMN application_aux.matchingid02date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid02date IS '}b`O2';
            public       postgres    false    198            ð           0    0 '   COLUMN application_aux.matchingid03date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid03date IS '}b`O3';
            public       postgres    false    198            ñ           0    0 '   COLUMN application_aux.matchingid04date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid04date IS '}b`O4';
            public       postgres    false    198            ò           0    0 '   COLUMN application_aux.matchingid05date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid05date IS '}b`O5';
            public       postgres    false    198            Ç            1259    550490    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public         postgres    false            È            1259    550493    refrece    TABLE     @  CREATE TABLE public.refrece (
    rrid integer NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    sex integer DEFAULT 0 NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    partial integer DEFAULT 0 NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    insname text DEFAULT ''::text NOT NULL,
    distance100 integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.refrece;
       public         postgres    false            ó           0    0    COLUMN refrece.distance100    COMMENT     D   COMMENT ON COLUMN public.refrece.distance100 IS 'Ã£km x 100';
            public       postgres    false    200            É            1259    550525    scan    TABLE     è   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public         postgres    false            Ê            1259    550533    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public       postgres    false    201            ô           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
            public       postgres    false    202            Ë            1259    550535 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public         postgres    false            Ì            1259    550542    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public         postgres    false            Í            1259    550554    shokaiimage    TABLE     Í   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public         postgres    false            Î            1259    550563    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public         postgres    false            8           2604    550566    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public       postgres    false    202    201            D           2606    550568    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public         postgres    false    196            N           2606    550572 %   application_aux application_aux_pkey1 
   CONSTRAINT     d   ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_aux_pkey1 PRIMARY KEY (aid);
 O   ALTER TABLE ONLY public.application_aux DROP CONSTRAINT application_aux_pkey1;
       public         postgres    false    198            J           2606    550574    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public         postgres    false    197            W           2606    550576    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public         postgres    false    203            P           2606    550578    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public         postgres    false    199            S           2606    550580    refrece refrece_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (rrid);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public         postgres    false    200            U           2606    550582    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public         postgres    false    201            ]           2606    550584 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public         postgres    false    206            [           2606    550586    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public         postgres    false    205            Y           2606    550588     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public         postgres    false    204            E           1259    550589    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public         postgres    false    197            F           1259    550590    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public         postgres    false    197            G           1259    550591    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public         postgres    false    197            H           1259    550592    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public         postgres    false    197            K           1259    550593    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public         postgres    false    197            L           1259    550594    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public         postgres    false    197            Q           1259    550595    refrece_num_idx    INDEX     B   CREATE INDEX refrece_num_idx ON public.refrece USING btree (num);
 #   DROP INDEX public.refrece_num_idx;
       public         postgres    false    200            ^           2620    550596 %   application trigger_appcounter_update    TRIGGER        CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public       postgres    false    197    208           