--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12
-- Dumped by pg_dump version 14.1

-- Started on 2022-02-22 19:07:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 208 (class 1255 OID 2459967)
-- Name: addmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;


ALTER FUNCTION public.addmonth(ym integer, addm integer) OWNER TO postgres;

--
-- TOC entry 209 (class 1255 OID 2459968)
-- Name: appcounter_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION public.appcounter_update() OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 196 (class 1259 OID 2459969)
-- Name: appcounter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.appcounter OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 2459973)
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
)
PARTITION BY LIST (cym);


ALTER TABLE public.application OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 2460486)
-- Name: application_aux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
)
PARTITION BY LIST (cym);


ALTER TABLE public.application_aux OWNER TO postgres;

--
-- TOC entry 4050 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aid IS 'aid';


--
-- TOC entry 4051 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.cym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.cym IS 'メホール請求年月';


--
-- TOC entry 4052 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.scanid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.scanid IS 'スキャンID';


--
-- TOC entry 4053 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.groupid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.groupid IS 'グループID';


--
-- TOC entry 4054 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aimagefile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aimagefile IS '変換後画像ファイル名';


--
-- TOC entry 4055 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.origfile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.origfile IS '元画像ファイル名';


--
-- TOC entry 4056 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.multitiff_pageno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.multitiff_pageno IS 'マルチtiff画像ページ番号';


--
-- TOC entry 4057 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aapptype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDのaapptype';


--
-- TOC entry 4058 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.parentaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.parentaid IS '続紙の親申請書AID';


--
-- TOC entry 4059 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.batchaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.batchaid IS '所属バッチAID';


--
-- TOC entry 4060 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid01; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01 IS 'マッチングしたデータのID1';


--
-- TOC entry 4061 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid02; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02 IS 'マッチングしたデータのID2';


--
-- TOC entry 4062 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid03; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03 IS 'マッチングしたデータのID3';


--
-- TOC entry 4063 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid04; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04 IS 'マッチングしたデータのID4';


--
-- TOC entry 4064 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid05; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05 IS 'マッチングしたデータのID5';


--
-- TOC entry 4065 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid01date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01date IS 'マッチング時刻1';


--
-- TOC entry 4066 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid02date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02date IS 'マッチング時刻2';


--
-- TOC entry 4067 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid03date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03date IS 'マッチング時刻3';


--
-- TOC entry 4068 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid04date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04date IS 'マッチング時刻4';


--
-- TOC entry 4069 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid05date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05date IS 'マッチング時刻5';


--
-- TOC entry 199 (class 1259 OID 2460767)
-- Name: refrece; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refrece (
    numbering text NOT NULL,
    insnumber text,
    number text,
    sex integer,
    birth date,
    drnumber text,
    cym integer NOT NULL,
    ym integer,
    firstday date,
    days integer,
    ratio integer,
    total integer,
    charge integer,
    futan integer,
    name text,
    zip text,
    prefname text,
    cityname text,
    add text,
    destname text
)
PARTITION BY LIST (cym);


ALTER TABLE public.refrece OWNER TO postgres;

--
-- TOC entry 4070 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE refrece; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.refrece IS '保険者提供のデータ';


--
-- TOC entry 200 (class 1259 OID 2460794)
-- Name: refreceimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refreceimport (
    importid integer NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    rececount integer DEFAULT 0 NOT NULL,
    importdate date DEFAULT '0001-01-01'::date NOT NULL,
    filename text DEFAULT ''::text NOT NULL,
    userid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.refreceimport OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 2460823)
-- Name: scan_sid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scan_sid_seq OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 2460825)
-- Name: scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan (
    sid integer DEFAULT nextval('public.scan_sid_seq'::regclass) NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer NOT NULL
)
PARTITION BY LIST (cym);


ALTER TABLE public.scan OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 2460858)
-- Name: scangroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date NOT NULL,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
)
PARTITION BY LIST (scandate);


ALTER TABLE public.scangroup OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 2460910)
-- Name: shokai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokai (
    aid integer NOT NULL,
    shokai_id integer NOT NULL,
    ym integer NOT NULL,
    barcode text NOT NULL,
    shokai_reason integer DEFAULT 0 NOT NULL,
    shokai_status integer DEFAULT 0 NOT NULL,
    shokai_result integer DEFAULT 0 NOT NULL,
    henrei boolean DEFAULT false NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    update_date date DEFAULT '0001-01-01'::date NOT NULL,
    update_uid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokai OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 2460923)
-- Name: shokaiexclude; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.shokaiexclude OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 2460935)
-- Name: shokaiimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokaiimage OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 2460944)
-- Name: shokaiimageimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);


ALTER TABLE public.shokaiimageimport OWNER TO postgres;

--
-- TOC entry 3896 (class 2606 OID 2470065)
-- Name: appcounter appcounter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);


--
-- TOC entry 3904 (class 2606 OID 2470078)
-- Name: application_aux application_p_aux_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_p_aux_pkey1 PRIMARY KEY (aid, cym);


--
-- TOC entry 3898 (class 2606 OID 2470067)
-- Name: application application_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_p_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 3913 (class 2606 OID 2470095)
-- Name: scangroup group_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_p_pkey PRIMARY KEY (groupid, scandate);


--
-- TOC entry 3907 (class 2606 OID 2470099)
-- Name: refrece refrece_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_p_pkey PRIMARY KEY (numbering, cym);


--
-- TOC entry 3909 (class 2606 OID 2470111)
-- Name: refreceimport refreceimport_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refreceimport
    ADD CONSTRAINT refreceimport_p_pkey PRIMARY KEY (importid, cym);


--
-- TOC entry 3911 (class 2606 OID 2470115)
-- Name: scan scan_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_p_pkey PRIMARY KEY (sid, cym);


--
-- TOC entry 3922 (class 2606 OID 2470133)
-- Name: shokaiimageimport shokai_image_import_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);


--
-- TOC entry 3920 (class 2606 OID 2470135)
-- Name: shokaiimage shokai_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);


--
-- TOC entry 3916 (class 2606 OID 2470137)
-- Name: shokai shokai_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokai
    ADD CONSTRAINT shokai_pkey PRIMARY KEY (aid);


--
-- TOC entry 3918 (class 2606 OID 2470139)
-- Name: shokaiexclude shokaiexclude_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);


--
-- TOC entry 3899 (class 1259 OID 2470795)
-- Name: idx_application_cym_desc_aid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_cym_desc_aid ON ONLY public.application USING btree (cym DESC NULLS LAST, aid);


--
-- TOC entry 3900 (class 1259 OID 2470792)
-- Name: idx_application_scan; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan ON ONLY public.application USING btree (scanid);


--
-- TOC entry 3901 (class 1259 OID 2470793)
-- Name: idx_application_scan_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan_scangroup ON ONLY public.application USING btree (scanid, groupid);


--
-- TOC entry 3902 (class 1259 OID 2470791)
-- Name: idx_application_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scangroup ON ONLY public.application USING btree (groupid);


--
-- TOC entry 3905 (class 1259 OID 2470147)
-- Name: refrece_p_numbering_cym; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX refrece_p_numbering_cym ON ONLY public.refrece USING btree (numbering, cym DESC NULLS LAST);


--
-- TOC entry 3914 (class 1259 OID 2470151)
-- Name: shokai_barcode_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX shokai_barcode_idx ON public.shokai USING btree (barcode);


--
-- TOC entry 4049 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

--
-- Name: application trigger_appcounter_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-02-22 19:07:56

--
-- PostgreSQL database dump complete
--

