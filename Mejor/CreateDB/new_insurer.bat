@echo off

REM 20210120180735 furukawa db作成、基本dbリストア処理
SET PATHPOS1=c:\\program files\\postgresql\\11\\bin
SET PATHPOS2=d:\\program files\\postgresql\\11\\bin


REM 20210404105018 furukawa psqlのフォルダ存在確認、ある方を使う
if exist %PATHPOS1% (
cd /d %PATHPOS1%
)
if exist %PATHPOS2%(
cd /d %PATHPOS2%
)




REM 設定値 SERVER PORT DBNAME

SET SERVER=%1
SET PORT=%2
SET DBNAME=%3

REM mejor_sample.sqlは最小限のdb
REM SET DBTEMPLATE=%~dp0\\mejor_sample.sql
REM 2021/06/03 furukawa db差し替え
REM SET DBTEMPLATE=%~dp0\\mejor_sample_aux3.sql

rem 20220223133327 furukawa st ////////////////////////
rem テンプレート平文形式に差し替え
		REM 20210713111135 furukawa st ////////////////////////
		REM リストアファイルを4に更新
		rem SET DBTEMPLATE=%~dp0\\mejor_sample_aux4.sql
SET DBTEMPLATE=%~dp0\\mejor_sample_aux5.sql
rem 20220223133327 furukawa ed ////////////////////////


REM ocr用db

rem 20220223133410 furukawa st ////////////////////////
rem テンプレート平文形式に差し替え
		rem SET DBTEMPLATE_OCR=%~dp0\\mejor_ocr_sample.sql
SET DBTEMPLATE_OCR=%~dp0\\mejor_ocr_sample2.sql
rem 20220223133410 furukawa ed ////////////////////////

echo create database %DBNAME%
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname postgres --command " create database %DBNAME% ;"

echo restore database schema
rem 20220223133145 furukawa st ////////////////////////
rem 平文形式にしたためpsqlに変更
		rem pg_restore --host %SERVER% --port %PORT% --username postgres --dbname %DBNAME% < %DBTEMPLATE%
psql --host %SERVER% --port %PORT% --username postgres --dbname %DBNAME% < %DBTEMPLATE%
rem 20220223133145 furukawa ed ////////////////////////


echo create ocr database %DBNAME%
psql --quiet --host 192.168.111.2 --port 5440 --username postgres --dbname postgres --command " create database %DBNAME% ;"

echo restore ocr database schema
rem 20220223133251 furukawa st ////////////////////////
rem 平文形式にしたためpsqlに変更
		rem pg_restore --host 192.168.111.2 --port 5440 --username postgres --dbname %DBNAME% < %DBTEMPLATE_OCR%
psql --host 192.168.111.2 --port 5440 --username postgres --dbname %DBNAME% < %DBTEMPLATE_OCR%
rem 20220223133251 furukawa ed ////////////////////////

