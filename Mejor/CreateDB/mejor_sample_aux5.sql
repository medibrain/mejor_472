--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12
-- Dumped by pg_dump version 14.1

-- Started on 2022-02-23 13:27:22

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 210 (class 1255 OID 2498534)
-- Name: addmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;


ALTER FUNCTION public.addmonth(ym integer, addm integer) OWNER TO postgres;

--
-- TOC entry 211 (class 1255 OID 2498535)
-- Name: appcounter_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION public.appcounter_update() OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 196 (class 1259 OID 2498536)
-- Name: appcounter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.appcounter OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 2499050)
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);


ALTER TABLE public.application OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 2499186)
-- Name: application_aux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.application_aux OWNER TO postgres;

--
-- TOC entry 4324 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.aid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aid IS 'aid';


--
-- TOC entry 4325 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.cym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.cym IS 'メホール請求年月';


--
-- TOC entry 4326 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.scanid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.scanid IS 'スキャンID';


--
-- TOC entry 4327 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.groupid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.groupid IS 'グループID';


--
-- TOC entry 4328 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.aimagefile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aimagefile IS '変換後画像ファイル名';


--
-- TOC entry 4329 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.origfile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.origfile IS '元画像ファイル名';


--
-- TOC entry 4330 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.multitiff_pageno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.multitiff_pageno IS 'マルチtiff画像ページ番号';


--
-- TOC entry 4331 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.aapptype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDのaapptype';


--
-- TOC entry 4332 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.parentaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.parentaid IS '続紙の親申請書AID';


--
-- TOC entry 4333 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.batchaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.batchaid IS '所属バッチAID';


--
-- TOC entry 4334 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid01; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01 IS 'マッチングしたデータのID1';


--
-- TOC entry 4335 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid02; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02 IS 'マッチングしたデータのID2';


--
-- TOC entry 4336 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid03; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03 IS 'マッチングしたデータのID3';


--
-- TOC entry 4337 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid04; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04 IS 'マッチングしたデータのID4';


--
-- TOC entry 4338 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid05; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05 IS 'マッチングしたデータのID5';


--
-- TOC entry 4339 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid01date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01date IS 'マッチング時刻1';


--
-- TOC entry 4340 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid02date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02date IS 'マッチング時刻2';


--
-- TOC entry 4341 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid03date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03date IS 'マッチング時刻3';


--
-- TOC entry 4342 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid04date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04date IS 'マッチング時刻4';


--
-- TOC entry 4343 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN application_aux.matchingid05date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05date IS 'マッチング時刻5';


--
-- TOC entry 197 (class 1259 OID 2498689)
-- Name: application_external; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_external (
    aid integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum character varying DEFAULT ''::character varying NOT NULL,
    hnum character varying DEFAULT ''::character varying NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname character varying DEFAULT ''::character varying NOT NULL,
    hzip character varying DEFAULT ''::character varying NOT NULL,
    haddress character varying DEFAULT ''::character varying NOT NULL,
    pname character varying DEFAULT ''::character varying NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense character varying DEFAULT ''::character varying NOT NULL,
    emptytext1 character varying DEFAULT ''::character varying NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 character varying DEFAULT ''::character varying NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    emptytext3 character varying DEFAULT ''::character varying NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 character varying DEFAULT ''::character varying NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 character varying DEFAULT ''::character varying NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 character varying DEFAULT ''::character varying NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 character varying DEFAULT ''::character varying NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 character varying DEFAULT ''::character varying NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid character varying DEFAULT ''::character varying NOT NULL,
    sregnumber character varying DEFAULT ''::character varying NOT NULL,
    szip character varying DEFAULT ''::character varying NOT NULL,
    saddress character varying DEFAULT ''::character varying NOT NULL,
    sname character varying DEFAULT ''::character varying NOT NULL,
    stel character varying DEFAULT ''::character varying NOT NULL,
    sdoctor character varying DEFAULT ''::character varying NOT NULL,
    skana character varying DEFAULT ''::character varying NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname character varying DEFAULT ''::character varying NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch character varying DEFAULT ''::character varying NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname character varying DEFAULT ''::character varying NOT NULL,
    bkana character varying DEFAULT ''::character varying NOT NULL,
    baccnumber character varying DEFAULT ''::character varying NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering character varying DEFAULT ''::character varying NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note character varying DEFAULT ''::character varying NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai character varying DEFAULT ''::character varying NOT NULL,
    memo_inspect character varying DEFAULT ''::character varying NOT NULL,
    memo character varying DEFAULT ''::character varying NOT NULL,
    paycode character varying DEFAULT ''::character varying NOT NULL,
    shokaicode character varying DEFAULT ''::character varying NOT NULL,
    ocrdata character varying DEFAULT ''::character varying NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas character varying DEFAULT ''::character varying NOT NULL,
    comnum character varying DEFAULT ''::character varying NOT NULL,
    groupnum character varying DEFAULT ''::character varying NOT NULL,
    outmemo character varying DEFAULT ''::character varying NOT NULL,
    kagoreasons_xml xml DEFAULT ''::xml NOT NULL,
    importuserid integer DEFAULT 0 NOT NULL,
    importdate date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.application_external OWNER TO postgres;

--
-- TOC entry 4344 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN application_external.importuserid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_external.importuserid IS 'インポートユーザ';


--
-- TOC entry 4345 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN application_external.importdate; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_external.importdate IS 'インポート日時';


--
-- TOC entry 198 (class 1259 OID 2498822)
-- Name: compare_result; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compare_result (
    aid integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum integer DEFAULT 0 NOT NULL,
    hnum integer DEFAULT 0 NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname integer DEFAULT 0 NOT NULL,
    hzip integer DEFAULT 0 NOT NULL,
    haddress integer DEFAULT 0 NOT NULL,
    pname integer DEFAULT 0 NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday integer DEFAULT 0 NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense integer DEFAULT 0 NOT NULL,
    emptytext1 integer DEFAULT 0 NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 integer DEFAULT 0 NOT NULL,
    ainspectdate integer DEFAULT 0 NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile integer DEFAULT 0 NOT NULL,
    emptytext3 integer DEFAULT 0 NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 integer DEFAULT 0 NOT NULL,
    idate1 integer DEFAULT 0 NOT NULL,
    ifirstdate1 integer DEFAULT 0 NOT NULL,
    istartdate1 integer DEFAULT 0 NOT NULL,
    ifinishdate1 integer DEFAULT 0 NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 integer DEFAULT 0 NOT NULL,
    idate2 integer DEFAULT 0 NOT NULL,
    ifirstdate2 integer DEFAULT 0 NOT NULL,
    istartdate2 integer DEFAULT 0 NOT NULL,
    ifinishdate2 integer DEFAULT 0 NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 integer DEFAULT 0 NOT NULL,
    idate3 integer DEFAULT 0 NOT NULL,
    ifirstdate3 integer DEFAULT 0 NOT NULL,
    istartdate3 integer DEFAULT 0 NOT NULL,
    ifinishdate3 integer DEFAULT 0 NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 integer DEFAULT 0 NOT NULL,
    idate4 integer DEFAULT 0 NOT NULL,
    ifirstdate4 integer DEFAULT 0 NOT NULL,
    istartdate4 integer DEFAULT 0 NOT NULL,
    ifinishdate4 integer DEFAULT 0 NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 integer DEFAULT 0 NOT NULL,
    idate5 integer DEFAULT 0 NOT NULL,
    ifirstdate5 integer DEFAULT 0 NOT NULL,
    istartdate5 integer DEFAULT 0 NOT NULL,
    ifinishdate5 integer DEFAULT 0 NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid integer DEFAULT 0 NOT NULL,
    sregnumber integer DEFAULT 0 NOT NULL,
    szip integer DEFAULT 0 NOT NULL,
    saddress integer DEFAULT 0 NOT NULL,
    sname integer DEFAULT 0 NOT NULL,
    stel integer DEFAULT 0 NOT NULL,
    sdoctor integer DEFAULT 0 NOT NULL,
    skana integer DEFAULT 0 NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname integer DEFAULT 0 NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch integer DEFAULT 0 NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname integer DEFAULT 0 NOT NULL,
    bkana integer DEFAULT 0 NOT NULL,
    baccnumber integer DEFAULT 0 NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note integer DEFAULT 0 NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai integer DEFAULT 0 NOT NULL,
    memo_inspect integer DEFAULT 0 NOT NULL,
    memo integer DEFAULT 0 NOT NULL,
    paycode integer DEFAULT 0 NOT NULL,
    shokaicode integer DEFAULT 0 NOT NULL,
    ocrdata integer DEFAULT 0 NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas integer DEFAULT 0 NOT NULL,
    comnum integer DEFAULT 0 NOT NULL,
    groupnum integer DEFAULT 0 NOT NULL,
    outmemo integer DEFAULT 0 NOT NULL,
    kagoreasons_xml integer DEFAULT 0 NOT NULL,
    cym_chargeym integer DEFAULT 0 NOT NULL,
    result integer DEFAULT 0 NOT NULL,
    comparedate date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.compare_result OWNER TO postgres;

--
-- TOC entry 4346 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.aid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.aid IS 'aid';


--
-- TOC entry 4347 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.scanid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.scanid IS 'OK=0,NG=1';


--
-- TOC entry 4348 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.groupid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.groupid IS 'OK=0,NG=1';


--
-- TOC entry 4349 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ayear; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ayear IS 'OK=0,NG=1';


--
-- TOC entry 4350 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.amonth; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.amonth IS 'OK=0,NG=1';


--
-- TOC entry 4351 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.inum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.inum IS 'OK=0,NG=1';


--
-- TOC entry 4352 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.hnum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.hnum IS 'OK=0,NG=1';


--
-- TOC entry 4353 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.hpref; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.hpref IS 'OK=0,NG=1';


--
-- TOC entry 4354 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.htype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.htype IS 'OK=0,NG=1';


--
-- TOC entry 4355 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.hname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.hname IS 'OK=0,NG=1';


--
-- TOC entry 4356 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.hzip; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.hzip IS 'OK=0,NG=1';


--
-- TOC entry 4357 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.haddress; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.haddress IS 'OK=0,NG=1';


--
-- TOC entry 4358 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.pname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.pname IS 'OK=0,NG=1';


--
-- TOC entry 4359 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.psex; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.psex IS 'OK=0,NG=1';


--
-- TOC entry 4360 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.pbirthday; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.pbirthday IS 'OK=0,NG=1';


--
-- TOC entry 4361 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.asingle; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.asingle IS 'OK=0,NG=1';


--
-- TOC entry 4362 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.afamily; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.afamily IS 'OK=0,NG=1';


--
-- TOC entry 4363 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.aratio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.aratio IS 'OK=0,NG=1';


--
-- TOC entry 4364 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.publcexpense; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.publcexpense IS 'OK=0,NG=1';


--
-- TOC entry 4365 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptytext1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptytext1 IS 'OK=0,NG=1';


--
-- TOC entry 4366 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptyint1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptyint1 IS 'OK=0,NG=1';


--
-- TOC entry 4367 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptytext2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptytext2 IS 'OK=0,NG=1';


--
-- TOC entry 4368 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ainspectdate; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ainspectdate IS 'OK=0,NG=1';


--
-- TOC entry 4369 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptyint2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptyint2 IS 'OK=0,NG=1';


--
-- TOC entry 4370 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptyint3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptyint3 IS 'OK=0,NG=1';


--
-- TOC entry 4371 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.aimagefile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.aimagefile IS 'OK=0,NG=1';


--
-- TOC entry 4372 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.emptytext3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.emptytext3 IS 'OK=0,NG=1';


--
-- TOC entry 4373 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.achargeyear; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.achargeyear IS 'OK=0,NG=1';


--
-- TOC entry 4374 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.achargemonth; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.achargemonth IS 'OK=0,NG=1';


--
-- TOC entry 4375 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.iname1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.iname1 IS 'OK=0,NG=1';


--
-- TOC entry 4376 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idate1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idate1 IS 'OK=0,NG=1';


--
-- TOC entry 4377 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifirstdate1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifirstdate1 IS 'OK=0,NG=1';


--
-- TOC entry 4378 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.istartdate1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.istartdate1 IS 'OK=0,NG=1';


--
-- TOC entry 4379 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifinishdate1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifinishdate1 IS 'OK=0,NG=1';


--
-- TOC entry 4380 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idays1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idays1 IS 'OK=0,NG=1';


--
-- TOC entry 4381 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.icourse1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.icourse1 IS 'OK=0,NG=1';


--
-- TOC entry 4382 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifee1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifee1 IS 'OK=0,NG=1';


--
-- TOC entry 4383 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.iname2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.iname2 IS 'OK=0,NG=1';


--
-- TOC entry 4384 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idate2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idate2 IS 'OK=0,NG=1';


--
-- TOC entry 4385 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifirstdate2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifirstdate2 IS 'OK=0,NG=1';


--
-- TOC entry 4386 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.istartdate2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.istartdate2 IS 'OK=0,NG=1';


--
-- TOC entry 4387 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifinishdate2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifinishdate2 IS 'OK=0,NG=1';


--
-- TOC entry 4388 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idays2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idays2 IS 'OK=0,NG=1';


--
-- TOC entry 4389 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.icourse2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.icourse2 IS 'OK=0,NG=1';


--
-- TOC entry 4390 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifee2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifee2 IS 'OK=0,NG=1';


--
-- TOC entry 4391 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.iname3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.iname3 IS 'OK=0,NG=1';


--
-- TOC entry 4392 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idate3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idate3 IS 'OK=0,NG=1';


--
-- TOC entry 4393 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifirstdate3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifirstdate3 IS 'OK=0,NG=1';


--
-- TOC entry 4394 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.istartdate3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.istartdate3 IS 'OK=0,NG=1';


--
-- TOC entry 4395 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifinishdate3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifinishdate3 IS 'OK=0,NG=1';


--
-- TOC entry 4396 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idays3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idays3 IS 'OK=0,NG=1';


--
-- TOC entry 4397 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.icourse3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.icourse3 IS 'OK=0,NG=1';


--
-- TOC entry 4398 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifee3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifee3 IS 'OK=0,NG=1';


--
-- TOC entry 4399 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.iname4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.iname4 IS 'OK=0,NG=1';


--
-- TOC entry 4400 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idate4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idate4 IS 'OK=0,NG=1';


--
-- TOC entry 4401 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifirstdate4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifirstdate4 IS 'OK=0,NG=1';


--
-- TOC entry 4402 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.istartdate4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.istartdate4 IS 'OK=0,NG=1';


--
-- TOC entry 4403 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifinishdate4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifinishdate4 IS 'OK=0,NG=1';


--
-- TOC entry 4404 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idays4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idays4 IS 'OK=0,NG=1';


--
-- TOC entry 4405 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.icourse4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.icourse4 IS 'OK=0,NG=1';


--
-- TOC entry 4406 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifee4; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifee4 IS 'OK=0,NG=1';


--
-- TOC entry 4407 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.iname5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.iname5 IS 'OK=0,NG=1';


--
-- TOC entry 4408 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idate5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idate5 IS 'OK=0,NG=1';


--
-- TOC entry 4409 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifirstdate5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifirstdate5 IS 'OK=0,NG=1';


--
-- TOC entry 4410 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.istartdate5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.istartdate5 IS 'OK=0,NG=1';


--
-- TOC entry 4411 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifinishdate5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifinishdate5 IS 'OK=0,NG=1';


--
-- TOC entry 4412 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.idays5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.idays5 IS 'OK=0,NG=1';


--
-- TOC entry 4413 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.icourse5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.icourse5 IS 'OK=0,NG=1';


--
-- TOC entry 4414 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ifee5; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ifee5 IS 'OK=0,NG=1';


--
-- TOC entry 4415 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.fchargetype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.fchargetype IS 'OK=0,NG=1';


--
-- TOC entry 4416 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.fdistance; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.fdistance IS 'OK=0,NG=1';


--
-- TOC entry 4417 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.fvisittimes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.fvisittimes IS 'OK=0,NG=1';


--
-- TOC entry 4418 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.fvisitfee; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.fvisitfee IS 'OK=0,NG=1';


--
-- TOC entry 4419 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.fvisitadd; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.fvisitadd IS 'OK=0,NG=1';


--
-- TOC entry 4420 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.sid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.sid IS 'OK=0,NG=1';


--
-- TOC entry 4421 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.sregnumber; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.sregnumber IS 'OK=0,NG=1';


--
-- TOC entry 4422 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.szip; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.szip IS 'OK=0,NG=1';


--
-- TOC entry 4423 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.saddress; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.saddress IS 'OK=0,NG=1';


--
-- TOC entry 4424 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.sname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.sname IS 'OK=0,NG=1';


--
-- TOC entry 4425 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.stel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.stel IS 'OK=0,NG=1';


--
-- TOC entry 4426 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.sdoctor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.sdoctor IS 'OK=0,NG=1';


--
-- TOC entry 4427 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.skana; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.skana IS 'OK=0,NG=1';


--
-- TOC entry 4428 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bacctype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bacctype IS 'OK=0,NG=1';


--
-- TOC entry 4429 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bname IS 'OK=0,NG=1';


--
-- TOC entry 4430 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.btype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.btype IS 'OK=0,NG=1';


--
-- TOC entry 4431 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bbranch; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bbranch IS 'OK=0,NG=1';


--
-- TOC entry 4432 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bbranchtype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bbranchtype IS 'OK=0,NG=1';


--
-- TOC entry 4433 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.baccname; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.baccname IS 'OK=0,NG=1';


--
-- TOC entry 4434 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bkana; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bkana IS 'OK=0,NG=1';


--
-- TOC entry 4435 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.baccnumber; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.baccnumber IS 'OK=0,NG=1';


--
-- TOC entry 4436 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.atotal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.atotal IS 'OK=0,NG=1';


--
-- TOC entry 4437 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.apartial; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.apartial IS 'OK=0,NG=1';


--
-- TOC entry 4438 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.acharge; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.acharge IS 'OK=0,NG=1';


--
-- TOC entry 4439 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.acounteddays; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.acounteddays IS 'OK=0,NG=1';


--
-- TOC entry 4440 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.numbering; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.numbering IS 'OK=0,NG=1';


--
-- TOC entry 4441 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.aapptype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.aapptype IS 'OK=0,NG=1';


--
-- TOC entry 4442 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.note; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.note IS 'OK=0,NG=1';


--
-- TOC entry 4443 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ufirst; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ufirst IS 'OK=0,NG=1';


--
-- TOC entry 4444 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.usecond; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.usecond IS 'OK=0,NG=1';


--
-- TOC entry 4445 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.uinquiry; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.uinquiry IS 'OK=0,NG=1';


--
-- TOC entry 4446 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.bui; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.bui IS 'OK=0,NG=1';


--
-- TOC entry 4447 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.cym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.cym IS 'OK=0,NG=1';


--
-- TOC entry 4448 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ym IS 'OK=0,NG=1';


--
-- TOC entry 4449 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.statusflags; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.statusflags IS 'OK=0,NG=1';


--
-- TOC entry 4450 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.shokaireason; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.shokaireason IS 'OK=0,NG=1';


--
-- TOC entry 4451 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.rrid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.rrid IS 'OK=0,NG=1';


--
-- TOC entry 4452 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.inspectreasons; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.inspectreasons IS 'OK=0,NG=1';


--
-- TOC entry 4453 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.additionaluid1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.additionaluid1 IS 'OK=0,NG=1';


--
-- TOC entry 4454 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.additionaluid2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.additionaluid2 IS 'OK=0,NG=1';


--
-- TOC entry 4455 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.memo_shokai; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.memo_shokai IS 'OK=0,NG=1';


--
-- TOC entry 4456 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.memo_inspect; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.memo_inspect IS 'OK=0,NG=1';


--
-- TOC entry 4457 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.memo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.memo IS 'OK=0,NG=1';


--
-- TOC entry 4458 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.paycode; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.paycode IS 'OK=0,NG=1';


--
-- TOC entry 4459 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.shokaicode; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.shokaicode IS 'OK=0,NG=1';


--
-- TOC entry 4460 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ocrdata; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ocrdata IS 'OK=0,NG=1';


--
-- TOC entry 4461 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.ufirstex; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.ufirstex IS 'OK=0,NG=1';


--
-- TOC entry 4462 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.usecondex; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.usecondex IS 'OK=0,NG=1';


--
-- TOC entry 4463 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.kagoreasons; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.kagoreasons IS 'OK=0,NG=1';


--
-- TOC entry 4464 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.saishinsareasons; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.saishinsareasons IS 'OK=0,NG=1';


--
-- TOC entry 4465 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.henreireasons; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.henreireasons IS 'OK=0,NG=1';


--
-- TOC entry 4466 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.taggeddatas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.taggeddatas IS 'OK=0,NG=1';


--
-- TOC entry 4467 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.comnum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.comnum IS 'OK=0,NG=1';


--
-- TOC entry 4468 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.groupnum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.groupnum IS 'OK=0,NG=1';


--
-- TOC entry 4469 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.outmemo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.outmemo IS 'OK=0,NG=1';


--
-- TOC entry 4470 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.kagoreasons_xml; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.kagoreasons_xml IS 'OK=0,NG=1';


--
-- TOC entry 4471 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.cym_chargeym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.cym_chargeym IS 'メホール請求年月';


--
-- TOC entry 4472 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.result; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.result IS '全項目を足した結果0の場合OK、それ以外NG';


--
-- TOC entry 4473 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN compare_result.comparedate; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.compare_result.comparedate IS '比較年月';


--
-- TOC entry 207 (class 1259 OID 2499214)
-- Name: refrece; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refrece (
    numbering text NOT NULL,
    insnumber text,
    number text,
    sex integer,
    birth date,
    drnumber text,
    cym integer NOT NULL,
    ym integer,
    firstday date,
    days integer,
    ratio integer,
    total integer,
    charge integer,
    futan integer,
    name text,
    zip text,
    prefname text,
    cityname text,
    add text,
    destname text
);


ALTER TABLE public.refrece OWNER TO postgres;

--
-- TOC entry 4474 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE refrece; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.refrece IS '保険者提供のデータ';


--
-- TOC entry 199 (class 1259 OID 2498956)
-- Name: refreceimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refreceimport (
    importid integer NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    rececount integer DEFAULT 0 NOT NULL,
    importdate date DEFAULT '0001-01-01'::date NOT NULL,
    filename text DEFAULT ''::text NOT NULL,
    userid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.refreceimport OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 2498968)
-- Name: scan_sid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scan_sid_seq OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 2499223)
-- Name: scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan (
    sid integer DEFAULT nextval('public.scan_sid_seq'::regclass) NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer NOT NULL
);


ALTER TABLE public.scan OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 2499235)
-- Name: scangroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date NOT NULL,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.scangroup OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 2498980)
-- Name: shokai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokai (
    aid integer NOT NULL,
    shokai_id integer NOT NULL,
    ym integer NOT NULL,
    barcode text NOT NULL,
    shokai_reason integer DEFAULT 0 NOT NULL,
    shokai_status integer DEFAULT 0 NOT NULL,
    shokai_result integer DEFAULT 0 NOT NULL,
    henrei boolean DEFAULT false NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    update_date date DEFAULT '0001-01-01'::date NOT NULL,
    update_uid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokai OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 2498993)
-- Name: shokaiexclude; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.shokaiexclude OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 2499005)
-- Name: shokaiimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokaiimage OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 2499014)
-- Name: shokaiimageimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);


ALTER TABLE public.shokaiimageimport OWNER TO postgres;

--
-- TOC entry 4166 (class 2606 OID 2499018)
-- Name: appcounter appcounter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);


--
-- TOC entry 4168 (class 2606 OID 2499020)
-- Name: application_external application_external_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_external
    ADD CONSTRAINT application_external_pkey PRIMARY KEY (aid);


--
-- TOC entry 4189 (class 2606 OID 2499213)
-- Name: application_aux application_p_aux_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_p_aux_pkey1 PRIMARY KEY (aid);


--
-- TOC entry 4183 (class 2606 OID 2499180)
-- Name: application application_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_p_pkey PRIMARY KEY (aid);


--
-- TOC entry 4170 (class 2606 OID 2499026)
-- Name: compare_result compare_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compare_result
    ADD CONSTRAINT compare_result_pkey PRIMARY KEY (aid);


--
-- TOC entry 4196 (class 2606 OID 2499243)
-- Name: scangroup group_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_p_pkey PRIMARY KEY (groupid);


--
-- TOC entry 4192 (class 2606 OID 2499221)
-- Name: refrece refrece_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_p_pkey PRIMARY KEY (numbering, cym);


--
-- TOC entry 4172 (class 2606 OID 2499032)
-- Name: refreceimport refreceimport_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refreceimport
    ADD CONSTRAINT refreceimport_p_pkey PRIMARY KEY (importid, cym);


--
-- TOC entry 4194 (class 2606 OID 2499233)
-- Name: scan scan_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_p_pkey PRIMARY KEY (sid, cym);


--
-- TOC entry 4181 (class 2606 OID 2499036)
-- Name: shokaiimageimport shokai_image_import_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);


--
-- TOC entry 4179 (class 2606 OID 2499038)
-- Name: shokaiimage shokai_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);


--
-- TOC entry 4175 (class 2606 OID 2499040)
-- Name: shokai shokai_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokai
    ADD CONSTRAINT shokai_pkey PRIMARY KEY (aid);


--
-- TOC entry 4177 (class 2606 OID 2499042)
-- Name: shokaiexclude shokaiexclude_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);


--
-- TOC entry 4184 (class 1259 OID 2499181)
-- Name: idx_application_cym_desc_aid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_cym_desc_aid ON public.application USING btree (cym DESC NULLS LAST, aid);


--
-- TOC entry 4185 (class 1259 OID 2499182)
-- Name: idx_application_scan; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan ON public.application USING btree (scanid);


--
-- TOC entry 4186 (class 1259 OID 2499183)
-- Name: idx_application_scan_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan_scangroup ON public.application USING btree (scanid, groupid);


--
-- TOC entry 4187 (class 1259 OID 2499184)
-- Name: idx_application_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scangroup ON public.application USING btree (groupid);


--
-- TOC entry 4190 (class 1259 OID 2499222)
-- Name: refrece_p_numbering_cym; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX refrece_p_numbering_cym ON public.refrece USING btree (numbering, cym DESC NULLS LAST);


--
-- TOC entry 4173 (class 1259 OID 2499048)
-- Name: shokai_barcode_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX shokai_barcode_idx ON public.shokai USING btree (barcode);


--
-- TOC entry 4323 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

--
-- Name: application trigger_appcounter_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();


REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-02-23 13:27:27

--
-- PostgreSQL database dump complete
--

