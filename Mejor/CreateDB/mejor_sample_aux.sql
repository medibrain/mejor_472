PGDMP     %                    y         	   arakawaku    11.10    11.7 r               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    24936 	   arakawaku    DATABASE     {   CREATE DATABASE arakawaku WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE arakawaku;
             postgres    false                       0    0    SCHEMA public    ACL     ¢   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    3            Ð            1255    24937    addmonth(integer, integer)    FUNCTION     y  CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public       postgres    false            Ñ            1255    24938    appcounter_update()    FUNCTION     ~  CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public       postgres    false            Ä            1259    24939 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public         postgres    false            Å            1259    24943    application    TABLE     à  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public         postgres    false            Ï            1259    508247    application_aux    TABLE     2  CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
 #   DROP TABLE public.application_aux;
       public         postgres    false                       0    0    TABLE application_aux    COMMENT     O   COMMENT ON TABLE public.application_aux IS 'ç³è«æ¸è£å®ãã¼ã¿IDç®¡ç';
            public       postgres    false    207                       0    0    COLUMN application_aux.aid    COMMENT     7   COMMENT ON COLUMN public.application_aux.aid IS 'aid';
            public       postgres    false    207                       0    0    COLUMN application_aux.cym    COMMENT     L   COMMENT ON COLUMN public.application_aux.cym IS 'ã¡ãã¼ã«è«æ±å¹´æ';
            public       postgres    false    207                       0    0    COLUMN application_aux.scanid    COMMENT     E   COMMENT ON COLUMN public.application_aux.scanid IS 'ã¹ã­ã£ã³ID';
            public       postgres    false    207                       0    0    COLUMN application_aux.groupid    COMMENT     F   COMMENT ON COLUMN public.application_aux.groupid IS 'ã°ã«ã¼ãID';
            public       postgres    false    207                       0    0 !   COLUMN application_aux.aimagefile    COMMENT     Y   COMMENT ON COLUMN public.application_aux.aimagefile IS 'å¤æå¾ç»åãã¡ã¤ã«å';
            public       postgres    false    207                       0    0    COLUMN application_aux.origfile    COMMENT     Q   COMMENT ON COLUMN public.application_aux.origfile IS 'åç»åãã¡ã¤ã«å';
            public       postgres    false    207                       0    0 '   COLUMN application_aux.multitiff_pageno    COMMENT     c   COMMENT ON COLUMN public.application_aux.multitiff_pageno IS 'ãã«ãtiffç»åãã¼ã¸çªå·';
            public       postgres    false    207                       0    0 #   COLUMN application_aux.matchingid01    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid01 IS 'ãããã³ã°ãããã¼ã¿ã®ID1';
            public       postgres    false    207                       0    0 #   COLUMN application_aux.matchingid02    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid02 IS 'ãããã³ã°ãããã¼ã¿ã®ID2';
            public       postgres    false    207                       0    0 #   COLUMN application_aux.matchingid03    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid03 IS 'ãããã³ã°ãããã¼ã¿ã®ID3';
            public       postgres    false    207                       0    0 #   COLUMN application_aux.matchingid04    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid04 IS 'ãããã³ã°ãããã¼ã¿ã®ID4';
            public       postgres    false    207                       0    0 #   COLUMN application_aux.matchingid05    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid05 IS 'ãããã³ã°ãããã¼ã¿ã®ID5';
            public       postgres    false    207                       0    0 '   COLUMN application_aux.matchingid01date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid01date IS 'ãããã³ã°æå»1';
            public       postgres    false    207                       0    0 '   COLUMN application_aux.matchingid02date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid02date IS 'ãããã³ã°æå»2';
            public       postgres    false    207                        0    0 '   COLUMN application_aux.matchingid03date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid03date IS 'ãããã³ã°æå»3';
            public       postgres    false    207            !           0    0 '   COLUMN application_aux.matchingid04date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid04date IS 'ãããã³ã°æå»4';
            public       postgres    false    207            "           0    0 '   COLUMN application_aux.matchingid05date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid05date IS 'ãããã³ã°æå»5';
            public       postgres    false    207            Æ            1259    25108    export    TABLE     ï  CREATE TABLE public.export (
    aid character varying(16) DEFAULT ''::character varying NOT NULL,
    cym character varying(5) DEFAULT ''::character varying NOT NULL,
    ippantaishoku character varying(1) DEFAULT ''::character varying NOT NULL,
    ryouyouhikubun character varying(1) DEFAULT ''::character varying NOT NULL,
    clinicnum character varying(10) DEFAULT ''::character varying NOT NULL,
    mediym character varying(5) DEFAULT ''::character varying NOT NULL,
    hnum character varying(6) DEFAULT ''::character varying NOT NULL,
    afamily character varying(1) DEFAULT ''::character varying NOT NULL,
    aratio character varying(2) DEFAULT ''::character varying NOT NULL,
    psex character varying(1) DEFAULT ''::character varying NOT NULL,
    pbirthday character varying(7) DEFAULT ''::character varying NOT NULL,
    minstartdate character varying(7) DEFAULT ''::character varying NOT NULL,
    maxfinishdate character varying(7) DEFAULT ''::character varying NOT NULL,
    acounteddays character varying(3) DEFAULT ''::character varying NOT NULL,
    atotal character varying(7) DEFAULT ''::character varying NOT NULL,
    apartial character varying(7) DEFAULT ''::character varying NOT NULL,
    acharge character varying(7) DEFAULT ''::character varying NOT NULL,
    ifirstdate1 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee1 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate1 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate1 character varying(7) DEFAULT ''::character varying NOT NULL,
    days1 character varying(3) DEFAULT ''::character varying NOT NULL,
    ifirstdate2 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee2 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate2 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate2 character varying(7) DEFAULT ''::character varying NOT NULL,
    days2 character varying(3) DEFAULT ''::character varying NOT NULL,
    ifirstdate3 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee3 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate3 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate3 character varying(7) DEFAULT ''::character varying NOT NULL,
    days3 character varying(3) DEFAULT ''::character varying NOT NULL,
    ifirstdate4 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee4 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate4 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate4 character varying(7) DEFAULT ''::character varying NOT NULL,
    days4 character varying(3) DEFAULT ''::character varying NOT NULL,
    ifirstdate5 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee5 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate5 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate5 character varying(7) DEFAULT ''::character varying NOT NULL,
    days5 character varying(3) DEFAULT ''::character varying NOT NULL,
    ifirstdate6 character varying(5) DEFAULT ''::character varying NOT NULL,
    ifee6 character varying(7) DEFAULT ''::character varying NOT NULL,
    istartdate6 character varying(7) DEFAULT ''::character varying NOT NULL,
    ifinishdate6 character varying(7) DEFAULT ''::character varying NOT NULL,
    days6 character varying(3) DEFAULT ''::character varying NOT NULL,
    kohif1 character varying(8) DEFAULT ''::character varying NOT NULL,
    kohij1 character varying(8) DEFAULT ''::character varying NOT NULL,
    kohif2 character varying(8) DEFAULT ''::character varying NOT NULL,
    kohij2 character varying(8) DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.export;
       public         postgres    false            #           0    0    TABLE export    COMMENT        COMMENT ON TABLE public.export IS 'åºåãã­ã¹ã(åºå®é·ï¼ãã®ã¾ã¾å¥ã£ã¦ãã®ã§åã¹ãã¼ã¹ã¨ããã£ã¦æ­£è§£';
            public       postgres    false    198            $           0    0    COLUMN export.aid    COMMENT     @   COMMENT ON COLUMN public.export.aid IS 'ãã©ã¤ããªã­ã¼';
            public       postgres    false    198            %           0    0    COLUMN export.cym    COMMENT     H   COMMENT ON COLUMN public.export.cym IS 'è«æ±å¹´æ  ãã³ãå¹´æ';
            public       postgres    false    198            &           0    0    COLUMN export.ippantaishoku    COMMENT     [   COMMENT ON COLUMN public.export.ippantaishoku IS 'ä¸è¬éè·åºå  å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            '           0    0    COLUMN export.ryouyouhikubun    COMMENT     Z   COMMENT ON COLUMN public.export.ryouyouhikubun IS 'çé¤è²»åºåãå¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            (           0    0    COLUMN export.clinicnum    COMMENT     \   COMMENT ON COLUMN public.export.clinicnum IS 'å»çæ©é¢ã³ã¼ã æ°å­10æ¡ å¨æ¡ï¼';
            public       postgres    false    198            )           0    0    COLUMN export.mediym    COMMENT     N   COMMENT ON COLUMN public.export.mediym IS 'æ½è¡å¹´æ  å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            *           0    0    COLUMN export.hnum    COMMENT     U   COMMENT ON COLUMN public.export.hnum IS 'ä¿éºèè¨¼çªå· è¨å·çªå· å¨æ¡ï¼';
            public       postgres    false    198            +           0    0    COLUMN export.afamily    COMMENT     j   COMMENT ON COLUMN public.export.afamily IS 'æ¬æ¶åºå æ¬å®¶åºåï¼ãï¼ãï¼ å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            ,           0    0    COLUMN export.aratio    COMMENT     o   COMMENT ON COLUMN public.export.aratio IS 'çµ¦ä»å²å ï¼ï¼ãï¼ï¼ãï¼ï¼ãï¼ï¼ãï¼ï¼ å¨æ¡ï¼';
            public       postgres    false    198            -           0    0    COLUMN export.psex    COMMENT     O   COMMENT ON COLUMN public.export.psex IS 'æ§å¥ ï¼ãï¼ ååããå¤æ­';
            public       postgres    false    198            .           0    0    COLUMN export.pbirthday    COMMENT     [   COMMENT ON COLUMN public.export.pbirthday IS 'çå¹´ææ¥ 5010312 ï¼ï¼ï¼ï¼ï¼ï¼ï¼';
            public       postgres    false    198            /           0    0    COLUMN export.minstartdate    COMMENT        COMMENT ON COLUMN public.export.minstartdate IS 'æ½è¡éå§å¹´ææ¥ æ½è¡æ¥ã®æå°å¤ãï¼ï¼ï¼ï¼ï¼ï¼ï¼ å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            0           0    0    COLUMN export.maxfinishdate    COMMENT        COMMENT ON COLUMN public.export.maxfinishdate IS 'æ½è¡çµäºå¹´ææ¥ æ½è¡æ¥ã®æå¤§å¤ãï¼ï¼ï¼ï¼ï¼ï¼ï¼ å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            1           0    0    COLUMN export.acounteddays    COMMENT     [   COMMENT ON COLUMN public.export.acounteddays IS 'è¨ºçå®æ¥æ° æ½è¡ã®åè¨æ¥æ° 1';
            public       postgres    false    198            2           0    0    COLUMN export.atotal    COMMENT     [   COMMENT ON COLUMN public.export.atotal IS 'ç·è²»ç¨é¡åè¨ 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            3           0    0    COLUMN export.apartial    COMMENT     Z   COMMENT ON COLUMN public.export.apartial IS 'ä¸é¨è² æé 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            4           0    0    COLUMN export.acharge    COMMENT     V   COMMENT ON COLUMN public.export.acharge IS 'è«æ±éé¡ 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            5           0    0    COLUMN export.ifirstdate1    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate1 IS 'æ½è¡å¹´æ1 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            6           0    0    COLUMN export.ifee1    COMMENT     O   COMMENT ON COLUMN public.export.ifee1 IS 'éé¡1 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            7           0    0    COLUMN export.istartdate1    COMMENT     v   COMMENT ON COLUMN public.export.istartdate1 IS 'æ½è¡éå§å¹´ææ¥1 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            8           0    0    COLUMN export.ifinishdate1    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate1 IS 'æ½è¡çµäºå¹´ææ¥1 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            9           0    0    COLUMN export.days1    COMMENT     j   COMMENT ON COLUMN public.export.days1 IS 'è¨ºçå®æ¥æ°1 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            :           0    0    COLUMN export.ifirstdate2    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate2 IS 'æ½è¡å¹´æ2 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            ;           0    0    COLUMN export.ifee2    COMMENT     O   COMMENT ON COLUMN public.export.ifee2 IS 'éé¡2 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            <           0    0    COLUMN export.istartdate2    COMMENT     v   COMMENT ON COLUMN public.export.istartdate2 IS 'æ½è¡éå§å¹´ææ¥2 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            =           0    0    COLUMN export.ifinishdate2    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate2 IS 'æ½è¡çµäºå¹´ææ¥2 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            >           0    0    COLUMN export.days2    COMMENT     j   COMMENT ON COLUMN public.export.days2 IS 'è¨ºçå®æ¥æ°2 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            ?           0    0    COLUMN export.ifirstdate3    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate3 IS 'æ½è¡å¹´æ3 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            @           0    0    COLUMN export.ifee3    COMMENT     O   COMMENT ON COLUMN public.export.ifee3 IS 'éé¡3 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            A           0    0    COLUMN export.istartdate3    COMMENT     v   COMMENT ON COLUMN public.export.istartdate3 IS 'æ½è¡éå§å¹´ææ¥3 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            B           0    0    COLUMN export.ifinishdate3    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate3 IS 'æ½è¡çµäºå¹´ææ¥3 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            C           0    0    COLUMN export.days3    COMMENT     j   COMMENT ON COLUMN public.export.days3 IS 'è¨ºçå®æ¥æ°3 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            D           0    0    COLUMN export.ifirstdate4    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate4 IS 'æ½è¡å¹´æ4 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            E           0    0    COLUMN export.ifee4    COMMENT     O   COMMENT ON COLUMN public.export.ifee4 IS 'éé¡4 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            F           0    0    COLUMN export.istartdate4    COMMENT     v   COMMENT ON COLUMN public.export.istartdate4 IS 'æ½è¡éå§å¹´ææ¥4 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            G           0    0    COLUMN export.ifinishdate4    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate4 IS 'æ½è¡çµäºå¹´ææ¥4 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            H           0    0    COLUMN export.days4    COMMENT     j   COMMENT ON COLUMN public.export.days4 IS 'è¨ºçå®æ¥æ°4 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            I           0    0    COLUMN export.ifirstdate5    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate5 IS 'æ½è¡å¹´æ5 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            J           0    0    COLUMN export.ifee5    COMMENT     O   COMMENT ON COLUMN public.export.ifee5 IS 'éé¡5 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            K           0    0    COLUMN export.istartdate5    COMMENT     v   COMMENT ON COLUMN public.export.istartdate5 IS 'æ½è¡éå§å¹´ææ¥5 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            L           0    0    COLUMN export.ifinishdate5    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate5 IS 'æ½è¡çµäºå¹´ææ¥5 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            M           0    0    COLUMN export.days5    COMMENT     j   COMMENT ON COLUMN public.export.days5 IS 'è¨ºçå®æ¥æ°5 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            N           0    0    COLUMN export.ifirstdate6    COMMENT     h   COMMENT ON COLUMN public.export.ifirstdate6 IS 'æ½è¡å¹´æ6 åæ¦å¹´æã50203 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            O           0    0    COLUMN export.ifee6    COMMENT     O   COMMENT ON COLUMN public.export.ifee6 IS 'éé¡6 0123456 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            P           0    0    COLUMN export.istartdate6    COMMENT     v   COMMENT ON COLUMN public.export.istartdate6 IS 'æ½è¡éå§å¹´ææ¥6 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            Q           0    0    COLUMN export.ifinishdate6    COMMENT     w   COMMENT ON COLUMN public.export.ifinishdate6 IS 'æ½è¡çµäºå¹´ææ¥6 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            R           0    0    COLUMN export.days6    COMMENT     j   COMMENT ON COLUMN public.export.days6 IS 'è¨ºçå®æ¥æ°6 åæ¦å¹´ææ¥ã5020312 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            S           0    0    COLUMN export.kohif1    COMMENT     {   COMMENT ON COLUMN public.export.kohif1 IS 'ç¬¬1å¬è²»è² æèçªå· åã¹ãã¼ã¹åã â¡1234567 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            T           0    0    COLUMN export.kohij1    COMMENT     {   COMMENT ON COLUMN public.export.kohij1 IS 'ç¬¬1å¬è²»åçµ¦èçªå· åã¹ãã¼ã¹åã â¡1234567 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            U           0    0    COLUMN export.kohif2    COMMENT     {   COMMENT ON COLUMN public.export.kohif2 IS 'ç¬¬2å¬è²»è² æèçªå· åã¹ãã¼ã¹åã â¡1234567 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            V           0    0    COLUMN export.kohij2    COMMENT     {   COMMENT ON COLUMN public.export.kohij2 IS 'ç¬¬2å¬è²»åçµ¦èçªå· åã¹ãã¼ã¹åã â¡1234567 å¨æ¡ã¹ãã¼ã¹';
            public       postgres    false    198            Ç            1259    25163    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public         postgres    false            È            1259    25166    refrece    TABLE     ñ  CREATE TABLE public.refrece (
    rrid integer NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    destname text DEFAULT ''::text NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.refrece;
       public         postgres    false            É            1259    25189    scan    TABLE     è   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public         postgres    false            Ê            1259    25203    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public       postgres    false    201            W           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
            public       postgres    false    202            Ë            1259    25205 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public         postgres    false            Ì            1259    25212    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public         postgres    false            Í            1259    25224    shokaiimage    TABLE     Í   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public         postgres    false            Î            1259    25233    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public         postgres    false            S           2604    401384    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public       postgres    false    202    201            p           2606    25647    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public         postgres    false    196                       2606    508271 $   application_aux application_aux_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_aux_pkey PRIMARY KEY (aid);
 N   ALTER TABLE ONLY public.application_aux DROP CONSTRAINT application_aux_pkey;
       public         postgres    false    207            u           2606    25649    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public         postgres    false    197            {           2606    25651    export export_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.export
    ADD CONSTRAINT export_pkey PRIMARY KEY (aid);
 <   ALTER TABLE ONLY public.export DROP CONSTRAINT export_pkey;
       public         postgres    false    198                       2606    25653    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public         postgres    false    203            }           2606    25655    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public         postgres    false    199                       2606    25657    refrece refrece_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (rrid);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public         postgres    false    200                       2606    25659    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public         postgres    false    201                       2606    25661     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public         postgres    false    204                       2606    25663    shokaiimage shokaiimage_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokaiimage_pkey PRIMARY KEY (imageid);
 F   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokaiimage_pkey;
       public         postgres    false    205                       2606    25665 (   shokaiimageimport shokaiimageimport_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokaiimageimport_pkey PRIMARY KEY (importid);
 R   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokaiimageimport_pkey;
       public         postgres    false    206            q           1259    25666    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public         postgres    false    197            r           1259    25667    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public         postgres    false    197            s           1259    25668    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public         postgres    false    197            v           1259    25669    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public         postgres    false    197            w           1259    25670    application_scanid_idx    INDEX     P   CREATE INDEX application_scanid_idx ON public.application USING btree (scanid);
 *   DROP INDEX public.application_scanid_idx;
       public         postgres    false    197            x           1259    25671    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public         postgres    false    197                       1259    508273    idx_aux_cym_groupid    INDEX     W   CREATE INDEX idx_aux_cym_groupid ON public.application_aux USING btree (cym, groupid);
 '   DROP INDEX public.idx_aux_cym_groupid;
       public         postgres    false    207    207                       1259    508272    idx_aux_scanid_groupid    INDEX     ]   CREATE INDEX idx_aux_scanid_groupid ON public.application_aux USING btree (scanid, groupid);
 *   DROP INDEX public.idx_aux_scanid_groupid;
       public         postgres    false    207    207                       1259    414732 
   idx_scanid    INDEX     A   CREATE UNIQUE INDEX idx_scanid ON public.scan USING btree (sid);
    DROP INDEX public.idx_scanid;
       public         postgres    false    201            ~           1259    25672    refrece_num_idx    INDEX     B   CREATE INDEX refrece_num_idx ON public.refrece USING btree (num);
 #   DROP INDEX public.refrece_num_idx;
       public         postgres    false    200            y           1259    411143    uidx_aimagefile    INDEX     T   CREATE UNIQUE INDEX uidx_aimagefile ON public.application USING btree (aimagefile);
 #   DROP INDEX public.uidx_aimagefile;
       public         postgres    false    197                       2620    25673 %   application trigger_appcounter_update    TRIGGER        CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public       postgres    false    197    209           