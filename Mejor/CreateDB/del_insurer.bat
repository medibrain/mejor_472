@echo off
REM //20210713111343 furukawa db削除用バッチ
SET PATHPOS1=c:\\program files\\postgresql\\11\\bin
SET PATHPOS2=d:\\program files\\postgresql\\11\\bin


REM 20210404105018 furukawa psqlのフォルダ存在確認、ある方を使う
if exist %PATHPOS1% (
cd /d %PATHPOS1%
)
if exist %PATHPOS2%(
cd /d %PATHPOS2%
)


REM 設定値 SERVER PORT DBNAME

SET SERVER=%1
SET PORT=%2
SET DBNAME=%3
SET INS_ID=%4

SET BACKUPPATH=%~dp0\\


echo ///// dump database %DBNAME%
pg_dump --host %SERVER% --port %PORT% --username postgres --dbname %dbname% --disable-triggers --data-only> %BACKUPPATH%%DBNAME%.dump
echo %DBNAME% バックアップ完了。戻すときはpsqlコマンド

echo ///// drop database %DBNAME%
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname postgres --command "select pid,pg_terminate_backend(pid) from pg_stat_activity where datname='%DBNAME%';"
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname postgres --command " drop database %DBNAME% ;"

echo ///// delete optional record from jyusei
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname jyusei --command " delete from medivery_setting where insurerid=%INS_ID%;"
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname jyusei --command " delete from insurer where insurerid=%INS_ID%;"
psql --quiet --host %SERVER% --port %PORT% --username postgres --dbname jyusei --command " delete from insurer_option where insurerid=%INS_ID%;"

echo ///// drop ocr database %DBNAME%
psql --quiet --host 192.168.111.2 --port 5440 --username postgres --dbname postgres --command "select pid,pg_terminate_backend(pid) from pg_stat_activity where datname='%DBNAME%';"
psql --quiet --host 192.168.111.2 --port 5440 --username postgres --dbname postgres --command " drop database %DBNAME% ;"


pause

