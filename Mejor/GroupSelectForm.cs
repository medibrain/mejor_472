﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Mejor
{
    public partial class GroupSelectForm : Form
    {
        int cym;
        Insurer ins;
        BindingSource bs;
        bool needEx = false;

        public GroupSelectForm(int cym, Insurer ins)
        {
            InitializeComponent();
            this.cym = cym;
            this.ins = ins;

            //20210708220758 furukawa //フォーム色を環境に合わす
            CommonTool.setFormColor(this);
            
            var iID = Insurer.CurrrentInsurer.EnumInsID;

            label1.Text = $"{cym / 100}年 {cym % 100} 月請求分";
            this.Text += " - Insurer: " + ins.InsurerName;
            bs = new BindingSource();

            //請求月に含まれるグループ一覧を取得
            GetGroupListGID();
            dataGridView1.DataSource = bs;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            var cs = new DataGridViewCellStyle();

            foreach (DataGridViewColumn col in dataGridView1.Columns) col.Visible = false;
            
            dataGridView1.Columns[nameof(ScanGroup.GroupID)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.GroupID)].HeaderText = "グループID";
            dataGridView1.Columns[nameof(ScanGroup.GroupID)].Width = 70;
            dataGridView1.Columns[nameof(ScanGroup.GroupID)].DisplayIndex = 0;
            dataGridView1.Columns[nameof(ScanGroup.Status)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.Status)].HeaderText = "ステータス";
            dataGridView1.Columns[nameof(ScanGroup.Status)].Width = 120;
            dataGridView1.Columns[nameof(ScanGroup.Status)].DisplayIndex = 1;
            dataGridView1.Columns[nameof(ScanGroup.WorkingUsers)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.WorkingUsers)].HeaderText = "作業中ユーザー";
            dataGridView1.Columns[nameof(ScanGroup.WorkingUsers)].Width = 300;
            dataGridView1.Columns[nameof(ScanGroup.WorkingUsers)].DisplayIndex = 2;
            dataGridView1.Columns[nameof(ScanGroup.note1)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.note1)].HeaderText = "Note1";
            dataGridView1.Columns[nameof(ScanGroup.note1)].Width = 90;
            dataGridView1.Columns[nameof(ScanGroup.note1)].DisplayIndex = 3;
            dataGridView1.Columns[nameof(ScanGroup.note2)].Visible = false;
            dataGridView1.Columns[nameof(ScanGroup.AppType)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.AppType)].HeaderText = "種別";
            dataGridView1.Columns[nameof(ScanGroup.AppType)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.AppType)].DisplayIndex= 4;
            dataGridView1.Columns[nameof(ScanGroup.YesOcr)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesOcr)].HeaderText = "OCR";
            dataGridView1.Columns[nameof(ScanGroup.YesOcr)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.YesOcr)].DisplayIndex = 6;
            dataGridView1.Columns[nameof(ScanGroup.YesMatch)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesMatch)].HeaderText = "マッチ";
            dataGridView1.Columns[nameof(ScanGroup.YesMatch)].Width =60;
            dataGridView1.Columns[nameof(ScanGroup.YesMatch)].DisplayIndex= 7;
            dataGridView1.Columns[nameof(ScanGroup.NoCheck)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.NoCheck)].HeaderText = "未入力";
            dataGridView1.Columns[nameof(ScanGroup.NoCheck)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.NoCheck)].DisplayIndex= 8;
            dataGridView1.Columns[nameof(ScanGroup.YesInput)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesInput)].HeaderText = "入力済";
            dataGridView1.Columns[nameof(ScanGroup.YesInput)].Width =60;
            dataGridView1.Columns[nameof(ScanGroup.YesInput)].DisplayIndex= 11;
            dataGridView1.Columns[nameof(ScanGroup.YesVerify)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesVerify)].HeaderText = "ベリ";
            dataGridView1.Columns[nameof(ScanGroup.YesVerify)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.YesVerify)].DisplayIndex= 12;
            dataGridView1.Columns[nameof(ScanGroup.NeedExInput)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.NeedExInput)].HeaderText = "拡対象";
            dataGridView1.Columns[nameof(ScanGroup.NeedExInput)].Width =60;
            dataGridView1.Columns[nameof(ScanGroup.NeedExInput)].DisplayIndex = 13;
            dataGridView1.Columns[nameof(ScanGroup.NoExInput)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.NoExInput)].HeaderText = "拡未入";
            dataGridView1.Columns[nameof(ScanGroup.NoExInput)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.NoExInput)].DisplayIndex = 14;
            dataGridView1.Columns[nameof(ScanGroup.YesExInput)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesExInput)].HeaderText = "拡入力";
            dataGridView1.Columns[nameof(ScanGroup.YesExInput)].Width =60;
            dataGridView1.Columns[nameof(ScanGroup.YesExInput)].DisplayIndex= 15;
            dataGridView1.Columns[nameof(ScanGroup.YesExVerify)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.YesExVerify)].HeaderText = "拡ベリ";
            dataGridView1.Columns[nameof(ScanGroup.YesExVerify)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.YesExVerify)].DisplayIndex= 16;

            //20221216 ito st プロジェクトK入力
            dataGridView1.Columns[nameof(ScanGroup.GroupAppTotal)].Visible = true;
            dataGridView1.Columns[nameof(ScanGroup.GroupAppTotal)].HeaderText = "総枚数";
            dataGridView1.Columns[nameof(ScanGroup.GroupAppTotal)].Width = 60;
            dataGridView1.Columns[nameof(ScanGroup.GroupAppTotal)].DisplayIndex = 5;
            if (MainForm.fixedMaskingInsurer(Insurer.CurrrentInsurer))
            {
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputFirst)].Visible = true;
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputFirst)].HeaderText = "外部1";
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputFirst)].Width = 60;
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputFirst)].DisplayIndex = 9;
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputSecond)].Visible = true;
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputSecond)].HeaderText = "外部2";
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputSecond)].Width = 60;
                dataGridView1.Columns[nameof(ScanGroup.GaibuInputSecond)].DisplayIndex = 10;
            }
            //20221216 ito end プロジェクトK入力


            //20201110120623 furukawa st ////////////////////////
            //過去データを選択した保険者でロード                       
            PastData.PersonalData.LoadPersonalData(cym);
            PastData.ClinicData.LoadClinicData(cym);
            PastData.AccountData.LoadBAccountData(cym);
            //20201110120623 furukawa ed ////////////////////////

            //ベリファイボタン表示
            buttonVerify.Visible = Insurer.CurrrentInsurer.NeedVerify;

            //拡張入力ボタン表示
            needEx =
                iID == InsurerID.CHUO_RADIO ||
                iID == InsurerID.KYOTO_KOIKI ||
                iID == InsurerID.MITSUBISHI_SEISHI ||
                iID == InsurerID.NAGOYA_MOKUZAI ||
                iID == InsurerID.OSAKA_KOIKI ||
                iID == InsurerID.NAGOYA_KOUWAN ||       //20190130105834 furukawa名古屋港湾追加
                iID == InsurerID.SHARP_KENPO ||         //20190212111104 furukawaシャープ健保追加
                iID == InsurerID.MIYAGI_KOKUHO ||       //20200318113534 furukawa 宮城県柔整追加
                iID == InsurerID.NAGOYASHI_KOKUHO ||    //20200513175552 furukawa 名古屋市国保追加
                iID == InsurerID.CHIBA_KOIKI_AHK ||     //20220323163619 furukawa 千葉広域あはき追加
                iID == InsurerID.AKISHIMASHI;           //20220403165545 furukawa 昭島市追加

            if (needEx)
            {
                buttonExInput.Visible = true;
                buttonExVerify.Visible = true;
            }
            else
            {
                buttonExInput.Visible = false;
                buttonExVerify.Visible = false;
            }

            //20210708220204 furukawa st //「バッチシートと申請書を紐付け」ボタン
            if (iID == InsurerID.HAKODATESHI)
            {
                buttonBatch.Visible = true;
            }
            else
            {
                buttonBatch.Visible = false;
            }
            //20210708220204 furukawa ed ////////////////////////

            //20200513175750 furukawa st //保険者個別機能追加ボタン使用可否
            switch (iID)
            {
                case InsurerID.NAGOYASHI_KOKUHO:
                    buttonSP.Visible = true;
                    buttonSPVerifyList.Visible = true;//20201216120509 furukawa 名古屋市国保用負傷理由入力リスト表示ボタン

                    if (needEx) buttonSPVerify.Visible = true;
                    else buttonSPVerify.Visible = false;

                    break;

                default:
                    buttonSP.Visible = false;
                    buttonSPVerify.Visible = false;
                    buttonSPVerifyList.Visible = false;//20201216120509 furukawa 名古屋市国保用負傷理由入力リスト表示ボタン

                    break;
            }
            //20200513175750 furukawa ed ////////////////////////

            //20221215 ito st /////プロジェクトK入力
            if (MainForm.fixedMaskingInsurer(Insurer.CurrrentInsurer))
            {
                //固定マスキング対象保険者の場合
                if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
                {
                    buttonInputStart.Visible = false;
                    buttonVerify.Visible = false;
                }
                else
                {
                    buttonInputStart.Visible = true;
                    buttonVerify.Visible = true;
                }

                buttonGaibu1st.Visible = true;
                buttonGaibu2nd.Visible = true;
                //buttonGaibu3rd.Visible = true;
                //buttonGaibu4th.Visible = true;
            }
            else
            {
                //固定マスキング対象外保険者の場合
                buttonInputStart.Visible = true;
                buttonVerify.Visible = true;

                buttonGaibu1st.Visible = false;
                buttonGaibu2nd.Visible = false;
            }
            //20221215 ito ed

            //20220623104130 furukawa st ////////////////////////
            //外部業者には拡張入力ボタン見せない           
            if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
            {
                buttonExInput.Visible = false;
                buttonExVerify.Visible=false;

                //20221206_1 ito st
                buttonRelate.Visible = false;
                buttonError.Visible = false;
                //20221206_1 ito end
            }
            //20220623104130 furukawa ed ////////////////////////
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 処理年月のScanGroupを取得し、含まれるAppを集計
        /// </summary>
        private void GetGroupListGID()
        {
            var list = Scan.GetScanListYM(cym);
            var glist = ScanGroup.GetGroupList(list);
            GetScanGroupPages(glist, cym);

            glist.Sort((x, y) => x.note1 == y.note1 ?
                    x.GroupID.CompareTo(y.GroupID) : x.note1.CompareTo(y.note1));

            bs.DataSource = glist;
            bs.ResetBindings(false);

            //表示関連、適合率計算
            int noInput = 0, yesInput = 0, yesMatch = 0, verify = 0, total = 0;
            int ptNoCheck = 0, ptOcr = 0, ptYesMatch = 0;
            int totalEx = 0, yesEx = 0, yesExVerify = 0;

            foreach (var t in glist)
            {
                noInput += t.NoCheck;
                yesInput += t.YesInput;
                yesMatch += t.YesMatch;
                verify += t.YesVerify;

                if (t.Status == GroupStatus.OCR済み)
                {
                    ptNoCheck += t.NoCheck;
                    ptOcr += t.YesOcr;
                    ptYesMatch += t.YesMatch;
                }

                if(needEx)
                {
                    totalEx += t.NeedExInput;
                    yesEx += t.YesExInput;
                    yesExVerify += t.YesExVerify;

                }
            }

            total = noInput + yesInput;
            float tp = (float)yesInput / (float)total * (float)100;
            tp = (float)Utility.ToRoundDown(tp, 1);

            float vp = (float)verify / (float)total * (float)100;
            vp = (float)Utility.ToRoundDown(vp, 1);

            float extp = totalEx == 0 ? 0 : (float)yesEx / (float)totalEx * (float)100;
            extp = (float)Utility.ToRoundDown(extp, 1);

            float exvp = totalEx == 0 ? 0 : (float)yesExVerify / (float)totalEx * (float)100;
            exvp = (float)Utility.ToRoundDown(exvp, 1);

            //20210928111939 furukawa st ////////////////////////
            //AUXから続紙数取得           
            string strsql = $" cym={cym} and parentaid>0";
            List<Application_AUX> lstAUX=Application_AUX.Select(strsql);
            //20210928111939 furukawa ed ////////////////////////


            //2016.01.15 松本 ベリファイ済みもパーセント表示
            var iID = ins.InsurerID;
            if (Insurer.CurrrentInsurer.NeedVerify)
            {
                label2.Text = glist.Count() + " グループ / 総数: " + total.ToString("#,0") +
                    " / 初入済: " + yesInput.ToString("#,0") + " (" + tp.ToString("0.0") + "%)" +
                    " / ベリ済:" + verify.ToString("#,0") + " (" + vp.ToString("0.0") + "%)" +
                    " / 拡張初入済:" + yesEx.ToString("#,0") + " (" + extp.ToString("0.0") + "%)" +
                    " / 拡張ベリ済:" + yesExVerify.ToString("#,0") + " (" + exvp.ToString("0.0") + "%)"+//;
                    " / 続紙:" + lstAUX.Count.ToString("#,0");//20210928112025 furukawa 続紙数も出す                                                            
            }
            else
            {
                label2.Text = glist.Count() + " グループ / 総数: " + total + " / 完了: " + yesInput + " (" + tp.ToString("0.0") + "%)";
            }
        }

        /// <summary>
        /// ScanGroupリストに含まれるAppレコードをカウント
        /// </summary>
        /// <param name="sgl"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        private void GetScanGroupPages(List<ScanGroup> sgl, int cym)
        {
            using (var cmd = DB.Main.CreateLongTimeoutCmd("SELECT " +
                "groupid, statusflags, aapptype>0 AS at, ocrdata<>'' AS ocr, COUNT(aid) " +
                "FROM application " +
                "WHERE cym=:cym " +
                "GROUP BY groupid, statusflags, at, ocr " +
                "ORDER BY groupid, statusflags;"))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;

                var l = cmd.TryExecuteReaderList();
                var dic = new Dictionary<int, ScanGroup>();
                foreach (var item in sgl)
                {
                    dic.Add(item.GroupID, item);
                }

                foreach (var item in l)
                {
                    int gid = (int)item[0];
                    if (!dic.ContainsKey(gid)) continue;

                    var sf = (StatusFlag)(int)item[1];
                    bool t = (bool)item[2];
                    int c = (int)(long)item[4];
                    bool ocr = (bool)item[3];

                    if (needEx)
                    {
                        if (t) dic[gid].NeedExInput += c;
                        if (sf.HasFlag(StatusFlag.拡張ベリ済)) dic[gid].YesExVerify += c;
                        if (sf.HasFlag(StatusFlag.拡張入力済)) dic[gid].YesExInput += c;
                        else if (t) dic[gid].NoExInput += c;
                    }
                    if (sf.HasFlag(StatusFlag.ベリファイ済)) dic[gid].YesVerify += c;
                    if (sf.HasFlag(StatusFlag.入力済)) dic[gid].YesInput += c;
                    if (sf.HasFlag(StatusFlag.自動マッチ済)) dic[gid].YesMatch += c;
                    //20221216 ito st プロジェクトK入力
                    dic[gid].GroupAppTotal+= c;
                    if (MainForm.fixedMaskingInsurer(Insurer.CurrrentInsurer))
                    {
                        if (sf.HasFlag(StatusFlag.外部1回目済)) dic[gid].GaibuInputFirst += c;
                        if (sf.HasFlag(StatusFlag.外部1回目済) && sf.HasFlag(StatusFlag.入力済)) dic[gid].GaibuInputSecond += c;
                    }
                    else if (!sf.HasFlag(StatusFlag.入力済)) dic[gid].NoCheck += c;
                    //20221216 ito end プロジェクトK入力
                    if (ocr) dic[gid].YesOcr += c;
                }
                sgl = dic.Values.ToList();
                return;
            }
        }

        /// <summary>
        /// 入力スタート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInputStart_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.First);
        }

        private void buttonVerify_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.Second);
        }

        private void checkStart(INPUT_TYPE iType)
        {
            //チェックするグループIDの取得
            if (dataGridView1.CurrentCell == null) return;
            int row = dataGridView1.CurrentCellAddress.Y;
            int gid = (int)(dataGridView1[0, row].Value);

            //表示位置を記憶
            var firstIndex = dataGridView1.FirstDisplayedScrollingRowIndex;

            InputStarter.Start(gid, iType);

            //リストの更新
            dataGridView1.SuspendLayout();
            GetGroupListGID();

            //選択行を、直前まで作業していたScanGroupに移す
            for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
            {
                if ((int)dataGridView1[0, i].Value == gid)
                {
                    dataGridView1.CurrentCell = dataGridView1[0, i];
                    break;
                }
            }

            dataGridView1.FirstDisplayedScrollingRowIndex = firstIndex;
            dataGridView1.ResumeLayout();
        }


        //ソート
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<ScanGroup>)bs.DataSource;
            var name = dataGridView1.Columns[e.ColumnIndex].Name;

            if (name == nameof(ScanGroup.GroupID))
            {
                glist.Sort((x, y) =>
                {
                    return x.GroupID.CompareTo(y.GroupID);
                });
            }
            else if (name == nameof(ScanGroup.Status))
            {
                glist.Sort((x, y) =>
                {
                    if (x.Status == y.Status)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.Status.CompareTo(y.Status);
                    }
                });
            }
            else if (name == nameof(ScanGroup.WorkingUsers))
            {
                glist.Sort((y, x) =>
                {
                    if (x.WorkingUsers == y.WorkingUsers)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.WorkingUsers.CompareTo(y.WorkingUsers);
                    }
                });
            }
            else if (name == "note1")
            {
                glist.Sort((x, y) =>
                {
                    if (x.note1 == y.note1)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.note1.CompareTo(y.note1);
                    }
                });
            }
            else if (name == "note2")
            {
                glist.Sort((x, y) =>
                {
                    if (x.note2 == y.note2)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.note2.CompareTo(y.note2);
                    }
                });
            }
            //20200514190030 furukawa st //入力済み、ベリファイ済みを昇順に
            else if (name ==nameof(ScanGroup.YesInput))
            {
                glist.Sort((x, y) =>
                {
                    if (x.YesInput == y.YesInput)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.YesInput.CompareTo(y.YesInput);
                    }
                });
            }
            else if (name == nameof(ScanGroup.YesVerify))
            {
                glist.Sort((x, y) =>
                {
                    if (x.YesVerify == y.YesVerify)
                    {
                        return x.GroupID.CompareTo(y.GroupID);
                    }
                    else
                    {
                        return x.YesVerify.CompareTo(y.YesVerify);
                    }
                });
            }
            //20200514190030 furukawa ed ////////////////////////

            bs.ResetBindings(false);
        }


        private void 入力中ユーザーリセットToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //チェックするグループIDの取得
            if (dataGridView1.CurrentCell == null) return;
            int row = dataGridView1.CurrentCellAddress.Y;
            int gid = (int)(dataGridView1[0, row].Value);

            if (MessageBox.Show("入力中ユーザーをリセットします。よろしいですか？\r\n\r\n" +
                "この操作はアプリが強制終了して名前が残ってしまった時など、" +
                "非常時のみに使用してください。", "入力者強制リセット",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2) != DialogResult.OK) return;

            var firstIndex = dataGridView1.FirstDisplayedScrollingRowIndex;
            ScanGroup.InputReset(gid);

            //リストの更新
            dataGridView1.SuspendLayout();
            GetGroupListGID();

            //選択行を、直前まで作業していたScanGroupに移す
            for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
            {
                if ((int)dataGridView1[0, i].Value == gid)
                {
                    dataGridView1.CurrentCell = dataGridView1[0, i];
                    break;
                }
            }

            dataGridView1.FirstDisplayedScrollingRowIndex = firstIndex;
            dataGridView1.ResumeLayout();
        }

        private void buttonError_Click(object sender, EventArgs e)
        {
            using (var f = new MemoErrorListForm(cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonExInput_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.ExFirst);
        }

        private void buttonExVerify_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.ExSecond);
        }

      
        /// <summary>
        /// 20191106114558 furukawa グループ番号検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            if (txtFind.Text == string.Empty) return;
            for (int r = 0; r < dataGridView1.Rows.Count; r++)
            {
                if (dataGridView1.Rows[r].Cells[0].Value.ToString() == txtFind.Text)
                {
                    //20190507174437 furukawa st //行が0未満対応
                    int dispRow = r - 10;
                    if (dispRow <= 0) dispRow = r;
                    dataGridView1.FirstDisplayedScrollingRowIndex = dispRow;
                    //dataGridView1.FirstDisplayedScrollingRowIndex = r;
                    //20190507174437 furukawa ed ////////////////////////

                    dataGridView1.Rows[r].Selected = true;
                    break;
                }
            }
        }

        /// <summary>
        /// 20200513174238 furukawa 保険者個別機能フラグをあげる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSP_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.Extra1);
        }

        /// <summary>
        /// 20200524105547 furukawa 保険者個別機能フラグをあげる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSPVerify_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.Extra2);
        }

        /// <summary>
        /// 20201216120656 furukawa 名古屋市国保用負傷理由入力リスト表示ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSPVerifyList_Click(object sender, EventArgs e)
        {
            NagoyashiKokuho.fusho_counter frm = new NagoyashiKokuho.fusho_counter(cym);
            frm.Show();
        }

        /// <summary>
        /// 20210629165613 furukawa 続紙に申請書を関連付けボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRelate_Click(object sender, EventArgs e)
        {
            //20220829_1 ito st /////
            App.CreateRelationQuary(cym);
            //App.CreateRelation(cym);
            //20220829_1 ito ed /////

            switch (Insurer.CurrrentInsurer.InsurerID)
            {
                case (int)InsurerID.IBARAKI_KOIKI:
                    //申請書の被保険者住所を往療内訳書に入れる
                    UpdateHAddress();
                    break;
                default:
                    break;
            }

            GetGroupListGID();  //20210928113521 furukawa st //件数表示更新
        }

        /// <summary>
        /// 申請書の被保険者住所を往療内訳書に入れる
        /// </summary>
        /// <returns></returns>
        private bool UpdateHAddress()
        {
            DB.Transaction tran =new DB.Transaction(DB.Main);
            DB.Command cmd = null;
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {

                cmd = DB.Main.CreateCmd($"select aid from application where cym={cym} and aapptype=-6");
                var list = cmd.TryExecuteReaderList();
                wf.SetMax(list.Count);
                wf.LogPrint($"申請書の被保険者住所を往療内訳書に適用");
                foreach (var item in list)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Clear();
                    sb.AppendLine($"update application set haddress=tmp.haddress");
                    sb.AppendLine($",fdistance=tmp.fdistance");
                    sb.AppendLine($",fvisitadd=tmp.fvisitadd");
                    sb.AppendLine($" from (select a.haddress,a.fdistance,a.fvisitadd from application a inner join application_aux x on a.aid=x.parentaid ");
                    sb.AppendLine($"where x.aid={item[0]}) tmp ");
                    sb.AppendLine($"where aid={item[0]}");

                    //20220601134047 furukawa st //更新コマンドを分けてトランザクションする
                    using (DB.Command cmdupd = DB.Main.CreateCmd(sb.ToString(),tran))
                    {                      
                        cmdupd.TryExecuteNonQuery();
                    }
                    wf.LogPrint($"申請書の被保険者住所を往療内訳書に適用 aid: {item[0]}");
                    //  cmd = DB.Main.CreateCmd(sb.ToString());
                    //  cmdTryExecuteNonQuery();
                    //20220601134047 furukawa ed ////////////////////////

                    wf.InvokeValue++;
                }
                tran.Commit();
                return true;

            }
            catch(Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 20210708220132 furukawa バッチシートと申請書を紐付け
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBatch_Click(object sender, EventArgs e)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            //バッチ関連付け
            if (!App.RelateBatch(cym, wf))
            {
                wf.Dispose();
                return;
            }

            switch (Insurer.CurrrentInsurer.InsurerID)
            {
                case (int)InsurerID.HAKODATESHI:
                    if (!Hakodateshi.clsClinicUpdate.UpdateClinic(cym, wf))
                    {
                        MessageBox.Show("施術所関連付け更新処理失敗");
                        wf.Dispose();
                        return;
                    }
                    break;

                default:
                    break;
            }

            MessageBox.Show("正常終了");
            wf.Dispose();
        }

        //20221215 ito st /////プロジェクトK入力
        private void buttonGaibu1st_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.GaibuInput1);

        }

        private void buttonGaibu2nd_Click(object sender, EventArgs e)
        {
            checkStart(INPUT_TYPE.GaibuInput2);

        }
        //20221215 ito end
    }
}