﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class TotalCalcForm : Form
    {
        public TotalCalcForm()
        {
            InitializeComponent();
        }

        class Month
        {
            public int Order { get; set; }
            public decimal Total { get; set; }
            public decimal NormalRatio { get; set; }
            public int CalcCharge { get; set; }
            public decimal NormalRatioCost => Total * NormalRatio / 10m;
            public int GetRaitoCost(decimal ratio) => (int)Math.Round(Total * ratio / 10m);
        }

        private void buttonCalc_Click(object sender, EventArgs e)
        {
            decimal total = verifyBoxTotal.GetIntValue();
            decimal m1 = verifyBoxM1.GetIntValue();
            decimal m2 = verifyBoxM2.GetIntValue();
            decimal m3 = verifyBoxM3.GetIntValue();
            decimal m4 = verifyBoxM4.GetIntValue();

            if (total < 1)
            {
                MessageBox.Show("合計請求額が判別できません。入力された金額を確認して下さい。");
                return;
            }

            if (m1 < 1 || m2 < 1 || 
                (m3 < 1 && m3 != (int)VerifyBox.ERROR_CODE.NULL) ||
                (m4 < 1 && m4 != (int)VerifyBox.ERROR_CODE.NULL))
            {
                MessageBox.Show("各月の合計額が判別できません。入力された金額を確認して下さい。");
                return;
            }

            if (m3 < 0) m3 = 0;
            if (m4 < 0) m4 = 0;
            decimal seikyu = m1 + m2 + m3 + m4;
            labelTotal.Text = "合計：" + seikyu.ToString("#,0");

            //割合が小数点第一位が9,0,1のどれかのみOK
            decimal ratio = total / seikyu * 10;
            int checkKeta = (int)(ratio * 10m % 10);
            if (checkKeta != 9 && checkKeta != 0 && checkKeta != 1)
            {
                MessageBox.Show("負担割合が判別できません。入力された金額を確認して下さい。");
                return;
            }

            //割合が7,8,9のどれかのみOK
            ratio = Math.Round(ratio);
            if (ratio != 7 && ratio != 8 && ratio != 9)
            {
                MessageBox.Show("負担割合が判別できません。入力された金額を確認して下さい。");
                return;
            }
            labelRatio.Text = "負担割合：" + ratio.ToString("0.00");

            var l = new List<Month>();

            var month1 = new Month();
            month1.Order = 1;
            month1.Total = m1;
            month1.NormalRatio = ratio;
            l.Add(month1);
            labelR1.Text = "※ " + month1.NormalRatioCost.ToString("#,0");

            var month2 = new Month();
            month2.Order = 2;
            month2.Total = m2;
            month2.NormalRatio = ratio;
            l.Add(month2);
            labelR2.Text = "※ " + month2.NormalRatioCost.ToString("#,0");

            if (m3 != 0)
            {
                var month3 = new Month();
                month3.Order = 3;
                month3.Total = m3;
                month3.NormalRatio = ratio;
                l.Add(month3);
                labelR3.Text = "※ " + month3.NormalRatioCost.ToString("#,0");
            }

            if (m4 != 0)
            {
                var month4 = new Month();
                month4.Order = 4;
                month4.Total = m4;
                month4.NormalRatio = ratio;
                l.Add(month4);
                labelR4.Text = "※ " + month4.NormalRatioCost.ToString("#,0");
            }

            decimal calcRatio = ratio;
            int rmTotal = l.Sum(x => x.GetRaitoCost(calcRatio));
            bool asc = total < rmTotal;

            l.Reverse();

            int index = 0;
            decimal d = 1 / total;
            while (true)
            {
                if(total == rmTotal)
                {
                    l.ForEach(x => x.CalcCharge = x.GetRaitoCost(calcRatio));
                    break;
                }

                if (asc)
                {
                    if (total > rmTotal)
                    {
                        l.Sort((x, y) => x.Total == y.Total ?
                            y.Order.CompareTo(x.Order) : x.Total.CompareTo(y.Total));
                        l.ForEach(x =>  x.CalcCharge = x.GetRaitoCost(calcRatio));

                        do
                        {
                            l[index].CalcCharge++;
                            index++;
                            if (index == l.Count) index = 0;

                        } while (total != l.Sum(x => x.CalcCharge));
                        break;
                    }

                    calcRatio -= d;
                }
                else
                {
                    if (total < rmTotal)
                    {
                        l.Sort((x, y) => x.Total == y.Total ?
                            y.Order.CompareTo(x.Order) : x.Total.CompareTo(y.Total));
                        l.ForEach(x => x.CalcCharge = x.GetRaitoCost(calcRatio));

                        do
                        {
                            l[index].CalcCharge--;
                            index++;
                            if (index == l.Count) index = 0;

                        } while (total != l.Sum(x => x.CalcCharge));
                        break;
                    }
                    calcRatio += d;
                }

                rmTotal = l.Sum(x => x.GetRaitoCost(calcRatio));
            }

            verifyBoxR1.Text = l.FirstOrDefault(x => x.Order == 1).CalcCharge.ToString();
            verifyBoxR2.Text = l.FirstOrDefault(x => x.Order == 2).CalcCharge.ToString();
            verifyBoxR3.Text = l.FirstOrDefault(x => x.Order == 3)?.CalcCharge.ToString()??string.Empty;
            verifyBoxR4.Text = l.FirstOrDefault(x => x.Order == 4)?.CalcCharge.ToString()??string.Empty;
        }

        private void verifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxR1.Clear();
            verifyBoxR2.Clear();
            verifyBoxR3.Clear();
            verifyBoxR4.Clear();
            labelRatio.Text = "負担割合：";
            labelR1.Text = "※";
            labelR2.Text = "※";
            labelR3.Text = "※";
            labelR4.Text = "※";
            labelTotal.Text = "合計：";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TotalCalcForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)Keys.Enter) return;
            SendKeys.Send("{TAB}");
            e.Handled = true;
        }
    }
}
