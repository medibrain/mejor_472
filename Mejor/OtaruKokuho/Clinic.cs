﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OtaruKokuho
{
    class Clinic
    {
        [DB.DbAttribute.PrimaryKey]
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public int LastAID { get; set; }
        public bool Verified { get; set; } = false;

        public static Clinic Select(string num)
        {
            return DB.Main.Select<Clinic>(new { num = num }).FirstOrDefault();
        }

        public bool Insert(DB.Transaction tran)
        {
            return DB.Main.Insert(this, tran);
        }

        public bool Update(DB.Transaction tran)
        {
            return DB.Main.Update(this, tran);
        }
    }
}