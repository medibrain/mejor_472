﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.OtaruKokuho
{
    class Export
    {
        /// <summary>
        /// 提出データを出力します
        /// </summary>
        /// <param name="cym">西暦6ケタ年月(yyyymm)</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool ExportDeliveryData(int cym)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = $"小樽市{cym}.csv";
                f.Filter = "*.csv|csvファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var wf = new WaitFormSimple();
            Task.Factory.StartNew(() => wf.ShowDialog());
            while (!wf.Visible) System.Threading.Thread.Sleep(10);

            var l = App.GetApps(cym);
            l.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            try
            {
                using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine("No.,診療年月,都道府県コード,保険者処理年月,レセプト管理番号,本家区分," +
                        "記号・番号,性別,生年月日,実日数,金額,氏名,施術院名,施術機関コード,傷病コード");

                    int num = 0;
                    foreach (var item in l)
                    {
                        if (item.YM <= 0) continue;

                        num++;
                        var ss = new string[15];
                        ss[0] = num.ToString();
                        ss[1] = $"平成{item.MediYear.ToString("00")}年{item.MediMonth.ToString("00")}月";
                        ss[2] = item.HihoPref.ToString("00");
                        ss[3] = $"平成{item.ChargeYear.ToString("00")}年{item.ChargeMonth.ToString("00")}月";
                        ss[4] = item.Numbering;
                        ss[5] = item.Family.ToString();
                        ss[6] = $"小樽{item.HihoNum}";
                        ss[7] = item.Sex == 1 ? "男" : "女";
                        ss[8] = item.Birthday.ToJDateStr();
                        ss[9] = item.CountedDays.ToString();
                        ss[10] = item.Total.ToString();
                        ss[11] = item.PersonName;
                        ss[12] = item.ClinicName;
                        ss[13] = item.ClinicNum;
                        ss[14] = "1905";

                        sw.WriteLine(string.Join(",", ss));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.InvokeCloseDispose();
            }

            MessageBox.Show("出力が完了しました");
            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "ID,処理年,処理月,診療年,診療月,保険者番号,被保険者番号,郵便番号,住所,被保険者名,受療者名,性別,生年月日," +
                        "請求区分,往療料,施術所記号,施術所名,合計金額,請求金額,診療日数,ナンバリング,照会理由,点検結果,点検詳細";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    string insNoStr = Insurer.CurrrentInsurer.InsNumber.ToString();
                    foreach (var item in list)
                    {
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(insNoStr);
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoZip);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToShortDateString());

                        ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        ss.Add(item.Distance == 999 ? "あり" : "");
                        ss.Add(item.ClinicNum);
                        ss.Add(item.ClinicName);
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Numbering);
                        ss.Add(item.ShokaiReason.ToString().Replace(", ", "、"));
                        ss.Add(item.InspectInfo);
                        ss.Add(item.InspectDescription);
                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}
