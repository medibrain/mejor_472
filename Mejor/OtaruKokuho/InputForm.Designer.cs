﻿namespace Mejor.OtaruKokuho
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.verifyBoxBsEra = new Mejor.VerifyBox();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxBsY = new Mejor.VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.verifyBoxBsM = new Mejor.VerifyBox();
            this.verifyBoxBsD = new Mejor.VerifyBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.verifyBoxName = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.verifyBoxClinicName = new Mejor.VerifyBox();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxSkY = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxSkM = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxClinicNum = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.verifyBoxPref = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxNum = new Mejor.VerifyBox();
            this.labelUser = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.label11 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 753);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 37;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(115, 21);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(165, 21);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(6, 73);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(43, 13);
            this.labelHnum.TabIndex = 12;
            this.labelHnum.Text = "被保番";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(6, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 26);
            this.label6.TabIndex = 0;
            this.label6.Text = "続紙: --\r\n不要: ++";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(346, 125);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 32;
            this.labelTotal.Text = "合計";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 782);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 782);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 738);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 738);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 738);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(7, 738);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 782);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.label22);
            this.panelRight.Controls.Add(this.verifyBoxBsEra);
            this.panelRight.Controls.Add(this.label23);
            this.panelRight.Controls.Add(this.verifyBoxBsY);
            this.panelRight.Controls.Add(this.label24);
            this.panelRight.Controls.Add(this.verifyBoxBsM);
            this.panelRight.Controls.Add(this.verifyBoxBsD);
            this.panelRight.Controls.Add(this.label25);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label13);
            this.panelRight.Controls.Add(this.verifyBoxName);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.label20);
            this.panelRight.Controls.Add(this.panelBottom);
            this.panelRight.Controls.Add(this.label21);
            this.panelRight.Controls.Add(this.label42);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.label9);
            this.panelRight.Controls.Add(this.label7);
            this.panelRight.Controls.Add(this.label12);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.verifyBoxClinicNum);
            this.panelRight.Controls.Add(this.verifyBoxNumbering);
            this.panelRight.Controls.Add(this.verifyBoxPref);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.verifyBoxNum);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.labelUser);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 782);
            this.panelRight.TabIndex = 0;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label22.Location = new System.Drawing.Point(481, 61);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 26);
            this.label22.TabIndex = 21;
            this.label22.Text = "昭：3\r\n平：4";
            // 
            // verifyBoxBsEra
            // 
            this.verifyBoxBsEra.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBsEra.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBsEra.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBsEra.Location = new System.Drawing.Point(455, 59);
            this.verifyBoxBsEra.Name = "verifyBoxBsEra";
            this.verifyBoxBsEra.NewLine = false;
            this.verifyBoxBsEra.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxBsEra.TabIndex = 20;
            this.verifyBoxBsEra.TextV = "";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(542, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "年";
            // 
            // verifyBoxBsY
            // 
            this.verifyBoxBsY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBsY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBsY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBsY.Location = new System.Drawing.Point(510, 59);
            this.verifyBoxBsY.MaxLength = 2;
            this.verifyBoxBsY.Name = "verifyBoxBsY";
            this.verifyBoxBsY.NewLine = false;
            this.verifyBoxBsY.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBsY.TabIndex = 22;
            this.verifyBoxBsY.TextV = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(591, 74);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "月";
            // 
            // verifyBoxBsM
            // 
            this.verifyBoxBsM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBsM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBsM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBsM.Location = new System.Drawing.Point(559, 59);
            this.verifyBoxBsM.MaxLength = 2;
            this.verifyBoxBsM.Name = "verifyBoxBsM";
            this.verifyBoxBsM.NewLine = false;
            this.verifyBoxBsM.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBsM.TabIndex = 24;
            this.verifyBoxBsM.TextV = "";
            // 
            // verifyBoxBsD
            // 
            this.verifyBoxBsD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBsD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBsD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBsD.Location = new System.Drawing.Point(608, 59);
            this.verifyBoxBsD.MaxLength = 2;
            this.verifyBoxBsD.Name = "verifyBoxBsD";
            this.verifyBoxBsD.NewLine = false;
            this.verifyBoxBsD.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBsD.TabIndex = 26;
            this.verifyBoxBsD.TextV = "";
            this.verifyBoxBsD.Leave += new System.EventHandler(this.textBoxBsD_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(640, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "日";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(510, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "北海道外: * + 登録番号";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label13.Location = new System.Drawing.Point(216, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 26);
            this.label13.TabIndex = 16;
            this.label13.Text = "本人：2　六歳：4\r\n家族：6　高齢：8";
            // 
            // verifyBoxName
            // 
            this.verifyBoxName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxName.Location = new System.Drawing.Point(47, 113);
            this.verifyBoxName.Name = "verifyBoxName";
            this.verifyBoxName.NewLine = false;
            this.verifyBoxName.Size = new System.Drawing.Size(190, 23);
            this.verifyBoxName.TabIndex = 29;
            this.verifyBoxName.TextV = "";
            this.verifyBoxName.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(352, 61);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 18;
            this.verifyBoxSex.TextV = "";
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(297, 113);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxDays.TabIndex = 31;
            this.verifyBoxDays.TextV = "";
            this.verifyBoxDays.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(323, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "性別";
            // 
            // panelBottom
            // 
            this.panelBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelBottom.Controls.Add(this.label17);
            this.panelBottom.Controls.Add(this.verifyBoxClinicName);
            this.panelBottom.Controls.Add(this.label26);
            this.panelBottom.Controls.Add(this.verifyBoxSkY);
            this.panelBottom.Controls.Add(this.label14);
            this.panelBottom.Controls.Add(this.verifyBoxSkM);
            this.panelBottom.Controls.Add(this.label15);
            this.panelBottom.Controls.Add(this.verifyBoxNewCont);
            this.panelBottom.Controls.Add(this.label19);
            this.panelBottom.Controls.Add(this.label18);
            this.panelBottom.Location = new System.Drawing.Point(0, 686);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(673, 62);
            this.panelBottom.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(475, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "初検年月";
            // 
            // verifyBoxClinicName
            // 
            this.verifyBoxClinicName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxClinicName.Location = new System.Drawing.Point(157, 20);
            this.verifyBoxClinicName.Name = "verifyBoxClinicName";
            this.verifyBoxClinicName.NewLine = false;
            this.verifyBoxClinicName.Size = new System.Drawing.Size(262, 23);
            this.verifyBoxClinicName.TabIndex = 4;
            this.verifyBoxClinicName.TextV = "";
            this.verifyBoxClinicName.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(155, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "施術機関名";
            // 
            // verifyBoxSkY
            // 
            this.verifyBoxSkY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSkY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSkY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSkY.Location = new System.Drawing.Point(477, 20);
            this.verifyBoxSkY.Name = "verifyBoxSkY";
            this.verifyBoxSkY.NewLine = false;
            this.verifyBoxSkY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxSkY.TabIndex = 6;
            this.verifyBoxSkY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSkY.TextV = "";
            this.verifyBoxSkY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxSkY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(560, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "月分";
            // 
            // verifyBoxSkM
            // 
            this.verifyBoxSkM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSkM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSkM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSkM.Location = new System.Drawing.Point(527, 20);
            this.verifyBoxSkM.Name = "verifyBoxSkM";
            this.verifyBoxSkM.NewLine = false;
            this.verifyBoxSkM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxSkM.TabIndex = 8;
            this.verifyBoxSkM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSkM.TextV = "";
            this.verifyBoxSkM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(510, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "年";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(42, 20);
            this.verifyBoxNewCont.MaxLength = 1;
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxNewCont.TabIndex = 1;
            this.verifyBoxNewCont.TextV = "";
            this.verifyBoxNewCont.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(82, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 26);
            this.label19.TabIndex = 2;
            this.label19.Text = "新規：1\r\n継続：2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 26);
            this.label18.TabIndex = 0;
            this.label18.Text = "新規\r\n継続";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(426, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 26);
            this.label21.TabIndex = 19;
            this.label21.Text = "生年\r\n月日";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(256, 125);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 30;
            this.label42.Text = "実日数";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(1, 167);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 519);
            this.scrollPictureControl1.TabIndex = 34;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 36;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(375, 113);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(92, 23);
            this.verifyBoxTotal.TabIndex = 33;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 26);
            this.label5.TabIndex = 28;
            this.label5.Text = "受療者\r\n氏名";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(471, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 26);
            this.label9.TabIndex = 10;
            this.label9.Text = "施術機関\r\nコード";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(373, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "県コード";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(154, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 26);
            this.label12.TabIndex = 14;
            this.label12.Text = "本家\r\n区分";
            // 
            // verifyBoxClinicNum
            // 
            this.verifyBoxClinicNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxClinicNum.Location = new System.Drawing.Point(524, 9);
            this.verifyBoxClinicNum.Name = "verifyBoxClinicNum";
            this.verifyBoxClinicNum.NewLine = false;
            this.verifyBoxClinicNum.Size = new System.Drawing.Size(116, 23);
            this.verifyBoxClinicNum.TabIndex = 11;
            this.verifyBoxClinicNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxClinicNum.TextV = "";
            this.verifyBoxClinicNum.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxClinicNum.Leave += new System.EventHandler(this.verifyBoxClinic_Leave);
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(285, 9);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(58, 23);
            this.verifyBoxNumbering.TabIndex = 7;
            this.verifyBoxNumbering.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxNumbering.Leave += new System.EventHandler(this.verifyBoxNumbering_Leave);
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPref.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxPref.Location = new System.Drawing.Point(417, 9);
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.NewLine = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxPref.TabIndex = 9;
            this.verifyBoxPref.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxPref.TextV = "";
            this.verifyBoxPref.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(183, 61);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxFamily.TabIndex = 15;
            this.verifyBoxFamily.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxNum
            // 
            this.verifyBoxNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNum.Location = new System.Drawing.Point(47, 61);
            this.verifyBoxNum.Name = "verifyBoxNum";
            this.verifyBoxNum.NewLine = false;
            this.verifyBoxNum.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxNum.TabIndex = 13;
            this.verifyBoxNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxNum.TextV = "";
            this.verifyBoxNum.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelUser
            // 
            this.labelUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(115, 752);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(43, 26);
            this.labelUser.TabIndex = 39;
            this.labelUser.Text = "入力1：\r\n入力2：";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(82, 9);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(132, 9);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(226, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "ナンバリング";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 782);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 782);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(1360, 820);
            this.Name = "InputForm";
            this.Text = "35";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_AichiToshi_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxNum;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Panel panelBottom;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Label label42;
        private VerifyBox verifyBoxName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxClinicNum;
        private VerifyBox verifyBoxNumbering;
        private VerifyBox verifyBoxPref;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private VerifyBox verifyBoxSkY;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxSkM;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxNewCont;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private VerifyBox verifyBoxBsEra;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxBsY;
        private System.Windows.Forms.Label label24;
        private VerifyBox verifyBoxBsM;
        private VerifyBox verifyBoxBsD;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxClinicName;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label3;
    }
}