﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OtaruKokuho
{
    class Person
    {
        [DB.DbAttribute.PrimaryKey]
        public string Num { get; set; } = string.Empty;
        [DB.DbAttribute.PrimaryKey]
        public DateTime Birth { get; set; } = DateTimeEx.DateTimeNull;
        public SEX Sex { get; set; } = SEX.Null;
        public string Name { get; set; } = string.Empty;
        public int LastAID { get; set; }
        public bool Verified { get; set; } = false;

        public static Person Select(string num, DateTime birth)
        {
            return DB.Main.Select<Person>(new { num = num, birth = birth }).FirstOrDefault();
        }

        public bool Insert(DB.Transaction tran)
        {
            return DB.Main.Insert(this, tran);
        }

        public bool Update(DB.Transaction tran)
        {
            return DB.Main.Update(this, tran);
        }
    }
}
