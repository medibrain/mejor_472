﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.OtaruKokuho
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private bool firstTime;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 60);
        Point posPerson = new Point(60, 540);
        Point posTotal = new Point(800, 2060);
        Point posBuiName = new Point(120, 780);
        Point posFusho = new Point(100, 780);
        Point posBuiDate = new Point(950, 780);
        Point posVisit = new Point(120, 700);
        Point posClinicName = new Point(100, 2500);
        Point posNewCont = new Point(820, 1200);

        Control[] ymConts, hnumConts, personConts, totalConts, fushoConts, buiDateConts, clinicNameConts, newContConts;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            scrollPictureControl1.Ratio = 1.5f;

            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            ymConts = new Control[] { verifyBoxY, verifyBoxM, verifyBoxNumbering };
            hnumConts = new Control[] { verifyBoxPref, verifyBoxClinicNum, verifyBoxNum, verifyBoxFamily };
            personConts = new Control[] { verifyBoxName, verifyBoxSex, verifyBoxBsEra, verifyBoxBsY, verifyBoxBsM, verifyBoxBsD };
            totalConts = new Control[] { verifyBoxTotal };
            fushoConts = new Control[] { verifyBoxSkY, verifyBoxSkM };
            buiDateConts = new Control[] { verifyBoxDays, };
            clinicNameConts = new Control[] { verifyBoxClinicName, };
            newContConts = new Control[] { verifyBoxNewCont, };

            if(!firstTime)
            {
                //ベリファイ不要項目
                panelBottom.Enabled = false;
            }

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        //フォーム表示時
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            focusBack(false);
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (!updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
            }
            else
            {
                dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri + 1];
            }
        }

        //EnterキーでTABキーの動作をさせる
        private void FormOCRCheck_AichiToshi_KeyDown(object sender, KeyEventArgs e)
        {   
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            bool err = false;
            var setError = new Action<TextBox, bool>((t, e) =>
                {
                    if (e)
                    {
                        t.BackColor = Color.Pink;
                        err = true;
                    }
                    else
                    {
                        t.BackColor = SystemColors.Info;
                    }
                });


            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //月
            int month = verifyBoxM.GetIntValue();
            setError(verifyBoxM, month < 1 || 12 < month);

            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            setError(verifyBoxY, DateTime.Today.Year < adYear || adYear < DateTime.Today.Year - 5);


            ////年
            //int year = verifyBoxY.GetIntValue();
            //int adYear = DateTimeEx.GetAdYearFromHs(year);
            //setError(verifyBoxY, DateTime.Today.Year < adYear || adYear < DateTime.Today.Year - 5);

            ////月
            //int month = verifyBoxM.GetIntValue();
            //setError(verifyBoxM, month < 1 || 12 < month);

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊



            //ナンバリング
            var numbering = verifyBoxNumbering.GetIntValue();
            setError(verifyBoxNumbering, numbering < 1);

            //県コード
            var pref = verifyBoxPref.GetIntValue();
            setError(verifyBoxPref, pref < 1 || 47 < pref);

            //医療機関コード
            var clinicNum = verifyBoxClinicNum.GetIntValue();
            if (pref == 1) setError(verifyBoxClinicNum, clinicNum < 10000);
            else setError(verifyBoxClinicNum, !string.IsNullOrWhiteSpace(verifyBoxClinicNum.Text));

            //被保険者番号
            int num = verifyBoxNum.GetIntValue();
            setError(verifyBoxNum, num < 1);
            string hnum = verifyBoxNum.Text.Trim();

            //本家区分
            int family = verifyBoxFamily.GetIntValue();
            setError(verifyBoxFamily, !(new int[] { 2, 4, 6, 8, 0 }).Contains(family));

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setError(verifyBoxSex, !(new int[] { 1, 2 }).Contains(sex));

            //生年月日
            int bg = verifyBoxBsEra.GetIntValue();
            int by = verifyBoxBsY.GetIntValue();
            int bm = verifyBoxBsM.GetIntValue();
            int bd = verifyBoxBsD.GetIntValue();
            var birthError = (bg < 0 || by < 0 || bm < 0 || bd < 0);

            //20190816121343 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更

                    //by = DateTimeEx.GetAdYearFromEraYear(bg, by);
            by = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(bg.ToString() + by.ToString("00") + bm.ToString("00")))/100;
            //20190816121343 furukawa ed ////////////////////////



            birthError &= !DateTimeEx.IsDate(by, bm, bd);
            setError(verifyBoxBsEra, birthError);
            setError(verifyBoxBsY, birthError);
            setError(verifyBoxBsM, birthError);
            setError(verifyBoxBsD, birthError);
            var birth = birthError ? DateTimeEx.DateTimeNull : new DateTime(by, bm, bd);

            //受療者氏名
            var name = verifyBoxName.Text.Trim().Replace(" ","　");
            setError(verifyBoxName, string.IsNullOrWhiteSpace(name) || !name.Contains("　"));

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setError(verifyBoxDays, days < 1 || 31 < days);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setError(verifyBoxNewCont, newCont < 1 || 31 < newCont);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setError(verifyBoxTotal, total < 100 || total > 200000);

            //施術機関名
            setError(verifyBoxClinicName, verifyBoxClinicName.Text.Trim().Length < 4);

            //初検年月
            int shokenY = verifyBoxSkY.GetIntValue();
            setError(verifyBoxSkY, shokenY < 1);
            int shokenM = verifyBoxSkM.GetIntValue();
            setError(verifyBoxSkM, shokenM < 1 || 12 < shokenM);

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (err) return false;

            //診療年月の警告
            int ms = (app.ChargeYear - year) * 12 + app.ChargeMonth - month;
            if (ms > 30)
            {
                if (MessageBox.Show("診療年月が古いですが、このまま登録してよろしいですか？",
                    "診療年月確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) != DialogResult.OK)
                {
                    verifyBoxY.BackColor = Color.Pink;
                    verifyBoxM.BackColor = Color.Pink;
                    return false;
                }
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.HihoPref = pref;
            app.ClinicNum = verifyBoxClinicNum.Text.Trim();
            app.HihoNum = hnum;
            app.Family = family;
            app.Sex = sex;
            app.Birthday = birth;
            app.PersonName = name;
            app.CountedDays = days;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.Total = total;

            //申請書種別
            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
            app.AppType = appsg.AppType;

            app.ClinicName = verifyBoxClinicName.Text.Trim();

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            shokenY = DateTimeEx.GetAdYearFromHs(shokenY * 100 + shokenM);
            //shokenY = DateTimeEx.GetAdYearFromHs(shokenY);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.FushoFirstDate1 = new DateTime(shokenY, shokenM, 1);

            //チェックOKならデータを格納したインスタンスを返す
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            using (var tran = DB.Main.CreateTransaction())
            {
                if (verifyBoxY.Text == "--")
                {
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                }
                else if (verifyBoxY.Text == "++")
                {
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                }
                else if (verifyBoxY.Text == "**")
                {
                    //エラー
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                }
                else
                {
                    if (!checkApp(app))
                    {
                        focusBack(true);
                        return false;
                    }

                    //被保険者情報(氏名)登録
                    var ps = getPerson();
                    if (ps == null)
                    {
                        ps = new Person();
                        ps.Num = app.HihoNum;
                        ps.Birth = app.Birthday;
                        ps.Sex = (SEX)app.Sex;
                        ps.Name = app.PersonName;
                        ps.Verified = false;
                        ps.LastAID = app.Aid;
                        if (!ps.Insert(tran)) return false;
                    }
                    else
                    {
                        if (ps.Sex != (SEX)app.Sex || ps.Name != app.PersonName)
                        {
                            var res = MessageBox.Show(
                                "既に登録されている療養者情報と性別、または名前が一致しません。" +
                                "このまま登録してもよろしいですか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            if (res != DialogResult.OK) return false;
                            ps.Verified = false;
                        }
                        else if (ps.Verified)
                        {
                            goto psbreak;
                        }
                        else if (ps.LastAID != app.Aid || !firstTime)
                        {
                            ps.Verified = true;
                        }
                        ps.LastAID = app.Aid;
                        ps.Sex = (SEX)app.Sex;
                        ps.Name = app.PersonName;
                        if (!ps.Update(tran)) return false;
                    }
                    psbreak:

                    //施術所情報登録
                    var cl = getClinic();
                    if (cl == null)
                    {
                        cl = new Clinic();
                        cl.Num = app.ClinicNum;
                        cl.Name = app.ClinicName;
                        cl.Verified = false;
                        cl.LastAID = app.Aid;
                        if (!cl.Insert(tran)) return false;
                    }
                    else
                    {
                        if (cl.Name != app.ClinicName)
                        {
                            var res = MessageBox.Show(
                                "既に登録されている施術所情報と名前が一致しません。" +
                                "このまま登録してもよろしいですか？", "",
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            if (res != DialogResult.OK) return false;
                            cl.Verified = false;
                        }
                        else if (cl.Verified)
                        {
                            goto clbreak;
                        }
                        else if (cl.LastAID != app.Aid)
                        {
                            cl.Verified = true;
                        }
                        cl.LastAID = app.Aid;
                        cl.Name = app.ClinicName;
                        if (!cl.Update(tran)) return false;
                    }
                }
                clbreak:

                //ベリファイチェック
                if (!firstTime && !checkVerify()) return false;

                //データベースへ反映
                var db = new DB("jyusei");
                using (var jyuTran = db.CreateTransaction())
                {
                    var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                    if (firstTime && app.Ufirst == 0)
                    {
                        if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                    }
                    else if (!firstTime && app.Usecond == 0)
                    {
                        if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                        if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                    }

                    if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                    //20211012101218 furukawa AUXにApptype登録
                    if (!Application_AUX.Update(app.Aid, app.AppType, tran)) return false;

                    jyuTran.Commit();
                    tran.Commit();
                    return true;
                }
            }
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //画像の表示
            setImaage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力者情報
            labelUser.Text = $"入力1：{User.GetUserName(app.Ufirst)}\r\n入力2：{User.GetUserName(app.Usecond)}";

            //App_Flagのチェック
            if (!firstTime)
            {
                //ベリファイ済みの際、データセットし、すべてのenabledをfalseに
                setVerify(app);
            }
            else if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setInputedApp(app);
            }
            else
            {
                //何も表示する項目なし
            }

            changedReset(app);
        }

        private void setVerify(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                setVerifyVal(verifyBoxY, "**", nv);
            }
            else
            {
                //申請書
                setVerifyVal(verifyBoxY, app.MediYear, nv);
                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                setVerifyVal(verifyBoxNumbering, app.Numbering, nv);
                setVerifyVal(verifyBoxPref, app.HihoPref, nv);
                setVerifyVal(verifyBoxClinicNum, app.ClinicNum, nv);

                //被保険者情報
                setVerifyVal(verifyBoxNum, app.HihoNum, nv);
                setVerifyVal(verifyBoxFamily, app.Family, nv);
                setVerifyVal(verifyBoxSex, app.Sex, nv);
                setVerifyVal(verifyBoxBsEra, DateTimeEx.GetEraNumber(app.Birthday), nv);
                setVerifyVal(verifyBoxBsY, DateTimeEx.GetJpYear(app.Birthday), nv);
                setVerifyVal(verifyBoxBsM, app.Birthday.Month, nv);
                setVerifyVal(verifyBoxBsD, app.Birthday.Day, nv);
                setVerifyVal(verifyBoxName, app.PersonName, nv);

                //申請情報
                setVerifyVal(verifyBoxDays, app.CountedDays, nv);
                setVerifyVal(verifyBoxTotal, app.Total, nv);

                //ベリファイ不要項目
                setVerifyVal(verifyBoxNewCont, ((int)app.NewContType), false);
                setVerifyVal(verifyBoxClinicName, app.ClinicName, false);
                setVerifyVal(verifyBoxSkY, DateTimeEx.GetJpYear(app.FushoFirstDate1), false);
                setVerifyVal(verifyBoxSkM, app.FushoFirstDate1.Month, false);
            }
            missCounterReset();
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImaage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else if(app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxNumbering.Text = app.Numbering;
                verifyBoxPref.Text = app.HihoPref.ToString();
                verifyBoxClinicNum.Text = app.ClinicNum;
                
                //被保険者情報
                verifyBoxNum.Text = app.HihoNum;
                verifyBoxFamily.Text = app.Family.ToString();
                verifyBoxSex.Text = app.Sex.ToString();
                verifyBoxBsEra.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                verifyBoxBsY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                verifyBoxBsM.Text = app.Birthday.Month.ToString();
                verifyBoxBsD.Text = app.Birthday.Day.ToString();
                verifyBoxName.Text = app.PersonName;

                //申請情報
                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxNewCont.Text = ((int)app.NewContType).ToString();
                verifyBoxTotal.Text = app.Total.ToString();

                verifyBoxClinicName.Text = app.ClinicName;
                verifyBoxSkY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString();
                verifyBoxSkM.Text = app.FushoFirstDate1.Month.ToString();
            }
        }


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var setCanInput = new Action<TextBox, bool>((t, b) =>
                {
                    t.ReadOnly = !b;
                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、不要、エラー時、入力項目は無い
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is TextBox)) continue;
                    if (item == verifyBoxY) continue;
                    setCanInput((TextBox)item, false);
                }

                //1回目入力時
                if (firstTime)
                {
                    foreach (Control item in panelBottom.Controls)
                    {
                        if (!(item is TextBox)) continue;
                        setCanInput((TextBox)item, false);
                    }
                }
            }
            else
            {
                //申請書の場合
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is TextBox)) continue;
                    setCanInput((TextBox)item, true);
                }

                //1回目入力時
                if(firstTime)
                {
                    foreach (Control item in panelBottom.Controls)
                    {
                        if (!(item is TextBox)) continue;
                        setCanInput((TextBox)item, true);
                    }
                }
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImaage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImaage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImaage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;
            else if (personConts.Contains(sender)) p = posPerson;
            else if (totalConts.Contains(sender)) p = posTotal;
            else if (fushoConts.Contains(sender)) p = posFusho;
            else if (buiDateConts.Contains(sender)) p = posBuiDate;
            else if (clinicNameConts.Contains(sender)) p = posClinicName;
            else if (newContConts.Contains(sender)) p = posNewCont;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        private void verifyBoxClinic_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(verifyBoxClinicName.Text)) return;
            var cl = getClinic();
            verifyBoxClinicName.Text = (cl == null || !cl.Verified) ?
                string.Empty : cl.Name;
        }

        private Clinic getClinic()
        {
            return Clinic.Select(verifyBoxClinicNum.Text.Trim());
        }

        private void textBoxBsD_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(verifyBoxName.Text)) return;
            var ps = getPerson();
            if (ps == null || !ps.Verified) return;

            verifyBoxName.Text = ps.Name;
        }

        /// <summary>
        /// 現在の入力情報をもとに、DBからPerson情報を取得します。
        /// </summary>
        /// <returns></returns>
        private Person getPerson()
        {
            int g, y, m, d;
            int.TryParse(verifyBoxBsEra.Text, out g);
            int.TryParse(verifyBoxBsY.Text, out y);
            int.TryParse(verifyBoxBsM.Text, out m);
            int.TryParse(verifyBoxBsD.Text, out d);

            //20190816121751 furukawa st ////////////////////////
            //月がないためGetAdYearFromHSに変更
            
                    //y = DateTimeEx.GetAdYearFromEraYear(g, y);
            y = DateTimeEx.GetAdYearFromHs(int.Parse(y.ToString("00") + m.ToString("00")));
            //20190816121751 furukawa ed ////////////////////////

            if (!DateTimeEx.IsDate(y, m, d)) return null;

            var birth = new DateTime(y, m, d);
            return Person.Select(verifyBoxNum.Text.Trim(), birth);
        }

        private void verifyBoxNumbering_Leave(object sender, EventArgs e)
        {
            verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(4, '0');
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            else if (personConts.Any(c => c.Focused)) posPerson = pos;
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (fushoConts.Any(c => c.Focused)) posFusho = pos;
            else if (buiDateConts.Any(c => c.Focused)) posBuiDate = pos;
            else if (clinicNameConts.Any(c => c.Focused)) posClinicName = pos;
            else if (newContConts.Any(c => c.Focused)) posNewCont = pos;
        }
    }      
}
