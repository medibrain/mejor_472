﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Mejor.YachiyoKokuho
{
    class ListExport
    {
        public static bool Export(List<App> list, string fileName)
        {
            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var header = "No.,処理年,処理月,番号,被保険者番号,被保険者名,受療者名,本家区分,生年月日," +
                        "施術年,施術月,実日数,合計金額,請求金額,前期高齢,新規継続," +
                        "負傷名1,負傷名2,負傷名3,負傷名4,負傷名5," +
                        "柔整師名,施術所名,郵便番号,住所,照会理由,点検結果,負傷数,過誤理由,再審査理由";
                    sw.WriteLine(header);

                    var ss = new List<string>();
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };

                    int no = 0;
                    foreach (var item in list)
                    {
                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(item.Family.ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add((item.Family == 8 || item.Family == 0) ? "○" : "");
                        ss.Add(item.NewContType.ToString());
                        ss.Add(item.FushoName1);
                        ss.Add(item.FushoName2);
                        ss.Add(item.FushoName3);
                        ss.Add(item.FushoName4);
                        ss.Add(item.FushoName5);
                        ss.Add(item.DrName);
                        ss.Add(item.ClinicName);
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd);
                        ss.Add(item.ShokaiReason.ToString().Replace(","," "));
                        ss.Add(item.TenkenResult);
                        ss.Add(item.FushoCount.ToString());
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;


        }


    }
}
