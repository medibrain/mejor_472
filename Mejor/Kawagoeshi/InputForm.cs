﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Kawagoeshi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;
      
        /// <summary>
        /// 国保データ柔整あはき両方
        /// </summary>
        List<dataimport> lstData = new List<dataimport>();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount,verifyBoxCountedDays};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge,checkBoxVisit,checkBoxVisitKasan };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5 };

           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null)
            {
                //提供データのリスト（柔整）                
                lstData = dataimport.select(app.CYM);

                setApp(app);
            }

            setInputControlbyAppType();
            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            setInputControlbyAppType();
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {      
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelTotal.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelTotal.Visible = false;
                    pDoui.Visible = true;
                    break;
                    
                default:
                    panelTotal.Visible = true;
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            
            if (lstData.Count == 0) return;
            //提供データ
            dataimport k = lstData[0];

            dgv.Columns[nameof(k.f000importid)].Width = 50;
            dgv.Columns[nameof(k.f001shoriym)].Width = 50;
            dgv.Columns[nameof(k.f002comnum)].Width = 140;
            dgv.Columns[nameof(k.f006clinicnum)].Width = 80;
            dgv.Columns[nameof(k.f008clinicname)].Width = 80;
            dgv.Columns[nameof(k.f011accountname)].Width = 80;
            dgv.Columns[nameof(k.f012insnum)].Width = 50;
            dgv.Columns[nameof(k.f014hmark)].Width = 50;
            dgv.Columns[nameof(k.f015hnum)].Width = 50;
            dgv.Columns[nameof(k.f016hmarknum)].Width = 100;
            dgv.Columns[nameof(k.f018pkana)].Width = 100;
            dgv.Columns[nameof(k.f019pname)].Width = 100;
            dgv.Columns[nameof(k.f020pgender)].Width = 50;

            //20211209100923 furukawa st ////////////////////////
            //宛名番号個人追加
            
            dgv.Columns[nameof(k.f022atenano_kojin)].Width = 100;
            //20211209100923 furukawa ed ////////////////////////


            dgv.Columns[nameof(k.f025atenano)].Width = 100;
            dgv.Columns[nameof(k.f028pbirthday)].Width = 80;
            dgv.Columns[nameof(k.f029ym)].Width = 80;
            dgv.Columns[nameof(k.f033counteddays)].Width = 50;
            dgv.Columns[nameof(k.f045ratio)].Width = 50;
            dgv.Columns[nameof(k.f046family)].Width = 50;
            dgv.Columns[nameof(k.f079startdate1)].Width = 80;
            dgv.Columns[nameof(k.f080finishdate1)].Width = 80;
            dgv.Columns[nameof(k.f090total)].Width = 50;
            dgv.Columns[nameof(k.f091charge)].Width = 50;
            dgv.Columns[nameof(k.f093partial)].Width = 50;
            dgv.Columns[nameof(k.f103clinickana)].Width = 50;
            dgv.Columns[nameof(k.cym)].Width = 50;
            dgv.Columns[nameof(k.ymAD)].Width = 50;
            dgv.Columns[nameof(k.shoriymAD)].Width = 50;
            dgv.Columns[nameof(k.birthAD)].Width = 50;
            dgv.Columns[nameof(k.startdate1AD)].Width = 50;
            dgv.Columns[nameof(k.finishdate1AD)].Width = 50;
            dgv.Columns[nameof(k.hmark_nr)].Width = 50;
            dgv.Columns[nameof(k.hnum_nr)].Width = 50;
            dgv.Columns[nameof(k.hmarkhnum_nr)].Width = 80;


            dgv.Columns[nameof(k.f000importid)].HeaderText = "rrid";
            dgv.Columns[nameof(k.f001shoriym)].HeaderText = "処理年月";
            dgv.Columns[nameof(k.f002comnum)].HeaderText = "レセプト全国共通キー";
            dgv.Columns[nameof(k.f006clinicnum)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(k.f008clinicname)].HeaderText = "施術所名";
            dgv.Columns[nameof(k.f011accountname)].HeaderText = "口座名称";
            dgv.Columns[nameof(k.f012insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(k.f014hmark)].HeaderText = "記号";
            dgv.Columns[nameof(k.f015hnum)].HeaderText = "番号";
            dgv.Columns[nameof(k.f016hmarknum)].HeaderText = "記号番号";
            dgv.Columns[nameof(k.f018pkana)].HeaderText = "受診者カナ";
            dgv.Columns[nameof(k.f019pname)].HeaderText = "受診者名";
            dgv.Columns[nameof(k.f020pgender)].HeaderText = "性別";

            //20211209101004 furukawa st ////////////////////////
            //宛名番号個人追加            
            dgv.Columns[nameof(k.f022atenano_kojin)].HeaderText = "宛名番号個人";
            //20211209101004 furukawa ed ////////////////////////


            dgv.Columns[nameof(k.f025atenano)].HeaderText = "宛名番号世帯主";
            dgv.Columns[nameof(k.f028pbirthday)].HeaderText = "生年月日";
            dgv.Columns[nameof(k.f029ym)].HeaderText = "施術年月";
            dgv.Columns[nameof(k.f033counteddays)].HeaderText = "実日数";
            dgv.Columns[nameof(k.f045ratio)].HeaderText = "割合%";
            dgv.Columns[nameof(k.f046family)].HeaderText = "本人家族入外";
            dgv.Columns[nameof(k.f079startdate1)].HeaderText = "開始日";
            dgv.Columns[nameof(k.f080finishdate1)].HeaderText = "終了日";
            dgv.Columns[nameof(k.f090total)].HeaderText = "合計額";
            dgv.Columns[nameof(k.f091charge)].HeaderText = "請求額";
            dgv.Columns[nameof(k.f093partial)].HeaderText = "一部負担金";
            dgv.Columns[nameof(k.f103clinickana)].HeaderText = "施術所カナ";
            dgv.Columns[nameof(k.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(k.ymAD)].HeaderText = "施術年月西暦";
            dgv.Columns[nameof(k.shoriymAD)].HeaderText = "処理年月西暦";
            dgv.Columns[nameof(k.birthAD)].HeaderText = "生年月日西暦";
            dgv.Columns[nameof(k.startdate1AD)].HeaderText = "施術開始日西暦";
            dgv.Columns[nameof(k.finishdate1AD)].HeaderText = "施術終了日西暦";
            dgv.Columns[nameof(k.hmark_nr)].HeaderText = "記号半角";
            dgv.Columns[nameof(k.hnum_nr)].HeaderText = "番号半角";
            dgv.Columns[nameof(k.hmarkhnum_nr)].HeaderText = "記号+番号";

            dgv.Columns[nameof(k.f000importid)].Visible = true;
            dgv.Columns[nameof(k.f001shoriym)].Visible = true;
            dgv.Columns[nameof(k.f002comnum)].Visible = true;
            dgv.Columns[nameof(k.f006clinicnum)].Visible = true;
            dgv.Columns[nameof(k.f008clinicname)].Visible = true;
            dgv.Columns[nameof(k.f011accountname)].Visible = true;
            dgv.Columns[nameof(k.f012insnum)].Visible = false;
            dgv.Columns[nameof(k.f014hmark)].Visible = false;
            dgv.Columns[nameof(k.f015hnum)].Visible = false;
            dgv.Columns[nameof(k.f016hmarknum)].Visible = true;
            dgv.Columns[nameof(k.f018pkana)].Visible = true;
            dgv.Columns[nameof(k.f019pname)].Visible = true;
            dgv.Columns[nameof(k.f020pgender)].Visible = true;

            //20211209101020 furukawa st ////////////////////////
            //宛名番号個人追加
            dgv.Columns[nameof(k.f022atenano_kojin)].Visible = true;
            //20211209101020 furukawa ed ////////////////////////

            dgv.Columns[nameof(k.f025atenano)].Visible = true;
            dgv.Columns[nameof(k.f028pbirthday)].Visible = true;
            dgv.Columns[nameof(k.f029ym)].Visible = true;
            dgv.Columns[nameof(k.f033counteddays)].Visible = true;
            dgv.Columns[nameof(k.f045ratio)].Visible = true;
            dgv.Columns[nameof(k.f046family)].Visible = true;
            dgv.Columns[nameof(k.f079startdate1)].Visible = true;
            dgv.Columns[nameof(k.f080finishdate1)].Visible = true;
            dgv.Columns[nameof(k.f090total)].Visible = true;
            dgv.Columns[nameof(k.f091charge)].Visible = true;
            dgv.Columns[nameof(k.f093partial)].Visible = true;
            dgv.Columns[nameof(k.f103clinickana)].Visible = false;

            dgv.Columns[nameof(k.cym)].Visible = true;
            dgv.Columns[nameof(k.ymAD)].Visible = false;
            dgv.Columns[nameof(k.shoriymAD)].Visible = false;
            dgv.Columns[nameof(k.birthAD)].Visible = false;
            dgv.Columns[nameof(k.startdate1AD)].Visible = false;
            dgv.Columns[nameof(k.finishdate1AD)].Visible = false;
            dgv.Columns[nameof(k.hmark_nr)].Visible = false;
            dgv.Columns[nameof(k.hnum_nr)].Visible = false;
            dgv.Columns[nameof(k.hmarkhnum_nr)].Visible = false;


        }

        /// <summary>
        /// 国保用グリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid(App app = null)
        {
            List<dataimport> lstimp = new List<dataimport>();
                   
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

            string strHmarkHnum = verifyBoxHmark.Text + verifyBoxHnum.Text;

            //生年月日
            DateTime dtPBirthday = 
                DateTimeEx.GetDateFromJstr7(
                    verifyBoxBirthE.Text + 
                    verifyBoxBirthY.Text.PadLeft(2, '0') +
                    verifyBoxBirthM.Text.PadLeft(2, '0') + 
                    verifyBoxBirthD.Text.PadLeft(2, '0'));

            foreach (dataimport item in lstData)
            {
                //被保番、メホール請求年月、合計金額、性別、生年月日で探す
                if (item.hmarkhnum_nr == strHmarkHnum &&                    
                    item.cym == scan.CYM &&
                    item.f090total.ToString() == verifyBoxTotal.Text.ToString().Trim() &&
                    item.f020pgender==verifyBoxPsex.Text && 
                    item.birthAD==dtPBirthday &&
                    item.ymAD==intymad)//20211213135556 furukawa 条件に診療年月追加
                                            

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            //if (lstimp.Count == 0) return;

            InitGrid();

            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();

            if (app != null && app.RrID.ToString() != string.Empty)//&& app.ComNum != string.Empty)
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    string strymad = r.Cells["ymad"].Value.ToString();
                    int intRRid = int.Parse(r.Cells["f000importid"].Value.ToString());
                    int intTotal = int.Parse(r.Cells["f090total"].Value.ToString());
                    string strHmark= r.Cells["hmark_nr"].Value.ToString();
                    string strHnum = r.Cells["hnum_nr"].Value.ToString();                    
                    string strPBirthAD = r.Cells["birthAD"].Value.ToString();
                    string strPgender = r.Cells["f020pgender"].Value.ToString();

                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells["f002comnum"].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (intRRid.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {
                        if (intTotal == verifyBoxTotal.GetIntValue() &&
                            strHmark == verifyBoxHmark.Text.Trim() &&
                            strHnum == verifyBoxHnum.Text.Trim() &&
                            strPBirthAD == dtPBirthday.ToString() &&
                            strPgender == verifyBoxPsex.Text.Trim() &&
                            strymad == intymad.ToString())//20211213135855 furukawa条件に診療年月追加
                            

                        {
                            //キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;

                        }
                        //提供データIDと違う場合の処理抜け                        
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }
            
            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }                        
            //マッチングOK条件に選択行が1行の場合も追加
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count == 1)            
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }
    
        #endregion


        private void setInputControlbyAppType()
        {

            labelF1.Visible = false;
            verifyBoxF1.Visible = false;
            verifyBoxF2.Visible = false;
            verifyBoxF3.Visible = false;
            verifyBoxF4.Visible = false;
            verifyBoxF5.Visible = false;
            checkBoxVisit.Visible = false;
            checkBoxVisitKasan.Visible = false;


        }

        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

        

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
                if (app.StatusFlagCheck(StatusFlag.ベリファイ済)) createGrid(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                    createGrid(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    string strHmarkNum = app.HihoNum;

                    //被保険者証記号
                    setValue(verifyBoxHmark,strHmarkNum.Substring(0,3), firstTime, nv);
                    //被保険者証番号
                    setValue(verifyBoxHnum, strHmarkNum.Substring(3,4), firstTime, nv);

                    //受療者性別
                    setValue(verifyBoxPsex, app.Sex, firstTime, nv);

                    //受療者生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                    
                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime,nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //実日数はcsvに絶対あるとは限らないのでappの値を使用
                    setValue(verifyBoxCountedDays, app.CountedDays.ToString(), firstTime, nv);

                    //20211213143151 furukawa st ////////////////////////
                    //負傷数入力
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count.ToString(), firstTime, nv);
                    //20211213143151 furukawa ed ////////////////////////



                    //グリッド選択する                    
                    //       createGrid_kokuho(app);



                    break;
            }
        }


        #endregion

        #region 各種更新


        private bool CheckNumbering(int intNumbering)
        {
            if (intPrevNumbering == 0) return true;
            if (intPrevNumbering > intNumbering) return true;//戻ったときの対策
            if (Math.Abs(intNumbering - intPrevNumbering) == 1) return true;
            else return false;
            
        }

        /// <summary>
        /// 提供データ(国保システムCSV)をAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistImportData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    dataimport k = lstData[0];
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(k.f000importid)].Value.ToString()); //ID


                    
                    //被保険者証番号は入力を優先させる


                    app.InsNum = dgv.CurrentRow.Cells[nameof(k.f012insnum)].Value.ToString();                     //保険者番号
                    app.HihoName = dgv.CurrentRow.Cells[nameof(k.f019pname)].Value.ToString();                    //被保険者名                    
                    app.PersonName = dgv.CurrentRow.Cells[nameof(k.f019pname)].Value.ToString();                  //受療者名＝被保険者名
                    //app.Sex = int.Parse(dgv.CurrentRow.Cells[nameof(k.f020pgender)].Value.ToString());          //受療者性別  入力優先
                    //app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.birthAD)].Value.ToString());    //受療者生年月日 入力優先

                    app.Ratio = int.Parse(dgv.CurrentRow.Cells[nameof(k.f045ratio)].Value.ToString())/10;           //給付割合

                    app.FushoStartDate1=DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.startdate1AD)].Value.ToString());          //開始
                    app.FushoFinishDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(k.finishdate1AD)].Value.ToString());      //終了

                    app.ClinicName = dgv.CurrentRow.Cells[nameof(k.f008clinicname)].Value.ToString();                           //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(k.f006clinicnum)].Value.ToString();                             //医療機関コード
                    app.ComNum = dgv.CurrentRow.Cells[nameof(k.f002comnum)].Value.ToString();                                   //レセプト全国共通キー

                    //実日数はcsvに無い場合があるので入力優先にする
                    //app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(k.f033counteddays)].Value.ToString()  );            //施術日数
                    
                    app.AccountName = dgv.CurrentRow.Cells[nameof(k.f011accountname)].Value.ToString();//口座名義

                    app.AppType = APP_TYPE.柔整;          //種別

                    app.Charge= int.Parse(dgv.CurrentRow.Cells[nameof(k.f091charge)].Value.ToString());                         //請求金額
                    app.Partial= int.Parse(dgv.CurrentRow.Cells[nameof(k.f093partial)].Value.ToString());                       //一部負担金

                    //20211209100741 furukawa st ////////////////////////
                    //宛名番号個人追加（リスト作成画面から検索するのでナンバリング希望）
                    
                    app.Numbering = dgv.CurrentRow.Cells[nameof(k.f022atenano_kojin)].Value.ToString();              //宛名番号個人
                    //20211209100741 furukawa ed ////////////////////////


                    app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells[nameof(k.f025atenano)].Value.ToString();              //宛名番号
                    app.TaggedDatas.GeneralString2 = dgv.CurrentRow.Cells[nameof(k.shoriymAD)].Value.ToString();               //処理年月
                    app.TaggedDatas.GeneralString3 = dgv.CurrentRow.Cells[nameof(k.f001shoriym)].Value.ToString();             //処理年月和暦


                    
                    string strfamily = dgv.CurrentRow.Cells[nameof(k.f046family)].Value.ToString();
                    app.Family = int.Parse(strfamily);

                    //app.Family = int.Parse(strfamily) * 100;

                    //switch (strHonke)
                    //{
                    //    case "本人":
                    //        app.Family += 2;
                    //        break;

                    //    case "家族":
                    //        app.Family += 6;
                    //        break;

                    //    case "高齢者一般":
                    //        app.Family += 8;
                    //        break;

                    //    case "高齢者７割":
                    //        app.Family += 0;
                    //        break;

                    //    default:
                    //        app.Family += 99;//エラー用
                    //        break;
                    //}


                    


                }
                else
                {
                    if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;
                    
                }

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


    
        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);


                    //被保険者証記号
                    string hmark = verifyBoxHmark.Text.Trim();
                    setStatus(verifyBoxHmark, hmark == string.Empty);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //受療者生年月日
                    DateTime dtpbirthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //受療者性別
                    int intverifyBoxPsex = verifyBoxPsex.GetIntValue();
                    setStatus(verifyBoxPsex, intverifyBoxPsex<0 || intverifyBoxPsex>2);

                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM);

                    //負傷1負傷名
                    string strverifyBoxF1Name = verifyBoxF1.Text.Trim();

                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //実日数 0未満31超でエラー
                    int intCountedDays = verifyBoxCountedDays.GetIntValue();
                    setStatus(verifyBoxCountedDays,intCountedDays<0 || intCountedDays>31);

                    //20211213143328 furukawa st ////////////////////////
                    //負傷数入力
                    int intCountFusho = verifyBoxFushoCount.GetIntValue();
                    setStatus(verifyBoxFushoCount, intCountFusho < 0);
                    //20211213143328 furukawa ed ////////////////////////

                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

            

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月
                    
                    app.HihoNum =$"{hmark}{hnumN}";                //被保険者証記号・番号                                       

                    //受療者性別
                    app.Sex = intverifyBoxPsex;

                    //受療者生年月日
                    app.Birthday = dtpbirthday;


                    //負傷1負傷名　本来鍼だけだがテキスト項目かつ、コントロールが非表示なだけなので区分せず取得
                    //app.FushoName1 = strverifyBoxF1Name;

                    //初検日1
                    app.FushoFirstDate1 = f1FirstDt;
                    
                    //申請書種別
                    app.AppType = scan.AppType;
             
                    //合計　
                    app.Total = total;


                    //実日数
                    app.CountedDays = intCountedDays;


                    //20211213143428 furukawa st ////////////////////////
                    //負傷数入力
                    app.TaggedDatas.count = intCountFusho;
                    //20211213143428 furukawa ed ////////////////////////



                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == adYear &&
                        app.FushoFirstDate1.Month == month)
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }


                    //提供データをappに反映
                    if(!RegistImportData(app)) return false;


                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            createGrid(app);

            //実日数が提供データにあったら入れる(1回め入力時のみ
            if (dgv.RowCount > 0 && verifyBoxCountedDays.Text.Trim() == string.Empty && firstTime)
            {
                verifyBoxCountedDays.Text = dgv.CurrentRow.Cells["f033counteddays"].Value.ToString();
                //verifyBoxCountedDays.BackColor = Color.LightGray;
                //verifyBoxCountedDays.TabStop = false;
            }

        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }


            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
               
        

        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            //往療加算は、有無がチェックされたときに有効とする　おかしな申請書が入力できなくなるのでやめとく
            //VerifyCheckBox c = (VerifyCheckBox)sender;
            //checkBoxVisitKasan.Enabled = c.Checked;
        }

        private void verifyBoxHmark_Validating(object sender, CancelEventArgs e)
        {
            verifyBoxHmark.Text = verifyBoxHmark.Text.Trim().PadLeft(3, '0');
        }

        private void verifyBoxHnum_Validating(object sender, CancelEventArgs e)
        {
            verifyBoxHnum.Text = verifyBoxHnum.Text.Trim().PadLeft(4, '0');
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
           // else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
