﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Kawagoeshi
{
   
    public class dataimport
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000importid { get; set; } = 0;                                  //インポートID　管理用;

        public string f001shoriym { get; set; } = string.Empty;                    //処理年月和暦;
        public string f002comnum { get; set; } = string.Empty;                      //レセプト全国共通キー;
        public string f006clinicnum { get; set; } = string.Empty;                   //医療機関コード;
        public string f008clinicname { get; set; } = string.Empty;                  //施術所名漢字;
        public string f011accountname { get; set; } = string.Empty;                 //口座名称;
        public string f012insnum { get; set; } = string.Empty;                      //保険者番号;
        public string f014hmark { get; set; } = string.Empty;                       //被保険者証記号;
        public string f015hnum { get; set; } = string.Empty;                        //被保険者証番号;
        public string f016hmarknum { get; set; } = string.Empty;                    //被保険者記号番号;
        public string f018pkana { get; set; } = string.Empty;                       //被保険者名カナ＝受療者として扱う;
        public string f019pname { get; set; } = string.Empty;                       //被保険者名漢字＝受療者として扱う;
        public string f020pgender { get; set; } = string.Empty;                     //性別;       
        
        //20211209095957 furukawa st ////////////////////////
        //宛名番号個人追加                                                                                    
        public string f022atenano_kojin { get; set; } = string.Empty;                     //宛名番号個人　2021/12/09追加;
        //20211209095957 furukawa ed ////////////////////////

        public string f025atenano { get; set; } = string.Empty;                     //宛名番号;        
        public string f028pbirthday { get; set; } = string.Empty;                   //生年月日和暦;
        public string f029ym { get; set; } = string.Empty;                          //施術年月和暦;
        public string f033counteddays { get; set; } = string.Empty;                               //実日数;
        public string f045ratio { get; set; } = string.Empty;                                     //給付割合%;
        public string f046family { get; set; } = string.Empty;                      //本人家族入外;
        public string f079startdate1 { get; set; } = string.Empty;                  //施術開始日和暦;
        public string f080finishdate1 { get; set; } = string.Empty;                 //施術終了日和暦;
        public string f090total { get; set; } = string.Empty;                                     //合計額;
        public string f091charge { get; set; } = string.Empty;                                    //請求額;
        public string f093partial { get; set; } = string.Empty;                                   //一部負担金;
        public string f103clinickana { get; set; } = string.Empty;                  //施術所カナ;

        public int cym { get; set; } = 0;                                           //メホール請求年月;
        public int ymAD { get; set; } = 0;                                          //施術年月西暦;
        public int shoriymAD { get; set; } = 0;                                    //処理年月西暦;
        public DateTime birthAD { get; set; } = DateTime.MinValue;                  //生年月日西暦;
        public DateTime startdate1AD { get; set; } = DateTime.MinValue;             //施術開始日西暦;
        public DateTime finishdate1AD { get; set; } = DateTime.MinValue;            //施術終了日西暦;

        public string hmark_nr { get; set; } = string.Empty;                        //被保険者証記号半角;
        public string hnum_nr { get; set; } = string.Empty;                         //被保険者証番号半角;
        public string hmarkhnum_nr { get; set; } = string.Empty;                    //記号半角＋番号半角;


        #endregion

        public static List<dataimport> lstclsData = new List<dataimport>();


        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool ImportMain(int _cym)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.InvokeValue = 0;

            try
            {
                if (!LoadFile(wf, _cym)) return false;

                //db登録
                if (!EntryDB(wf, _cym)) return false;

                System.Windows.Forms.MessageBox.Show("取込完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("取込失敗");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }



        /// <summary>
        /// db登録
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryDB(WaitForm wf, int _cym)
        {

            wf.InvokeValue = 0;
            wf.SetMax(lstclsData.Count);
            DB.Transaction tran = new DB.Transaction(DB.Main);

            try
            {
                wf.LogPrint("DB削除");
                DB.Command cmd = new DB.Command($"delete from dataimport where cym={_cym}", tran);
                cmd.TryExecuteNonQuery();

                wf.LogPrint("DB登録");
                foreach (dataimport cls in lstclsData)
                {
                    if (!DB.Main.Insert<dataimport>(cls, tran)) return false;
                    wf.LogPrint($"レセプト全国共通キー：{cls.f002comnum}");
                    wf.InvokeValue++;
                }

                tran.Commit();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }

        private static int ChangeToADYM(string strtmp)
        {
            return DateTimeEx.GetAdYear(strtmp.Substring(0, 4)) * 100 + int.Parse(strtmp.Substring(5, 2));
        }

        /// <summary>
        /// csvをロードする
        /// </summary>
        /// <returns></returns>
        private static bool LoadFile(WaitForm wf, int _cym)
        {

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            
            try
            {
                dlg.Filter = "csv|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

                string strFileName = dlg.FileName;


                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);
                wf.InvokeValue = 0;
                lstclsData.Clear();

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {


                    try
                    {
                        //合計額が数字ならデータ行と判断
                        int.Parse(tmp[89].ToString());
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataimport cls = new dataimport();
                    
                    cls.f001shoriym = tmp[0];                     //処理年月和暦;
                    cls.f002comnum = tmp[1];                       //レセプト全国共通キー;
                    cls.f006clinicnum = tmp[5];                    //医療機関コード;
                    cls.f008clinicname = tmp[7];                   //施術所名漢字;
                    cls.f011accountname = tmp[10];                 //口座名称;
                    cls.f012insnum = tmp[11];                      //保険者番号;
                    cls.f014hmark = tmp[13];                       //被保険者証記号全角;
                    cls.f015hnum = tmp[14];                        //被保険者証番号全角;
                    cls.f016hmarknum = tmp[15];                    //被保険者記号番号;
                    cls.f018pkana = tmp[17];                       //受診者カナ(元々は被保険者名;
                    cls.f019pname = tmp[18];                       //受診者名(元々は被保険者名;
                    cls.f020pgender = tmp[19];                     //性別;

                    //20211209100423 furukawa st ////////////////////////
                    //宛名番号個人追加
                    
                    cls.f022atenano_kojin = tmp[21];                     //宛名番号個人　2021/12/09追加;
                    //20211209100423 furukawa ed ////////////////////////


                    cls.f025atenano = tmp[24];                     //宛名番号;
                    
                    cls.f028pbirthday = tmp[27];                   //生年月日和暦;
                    cls.f029ym = tmp[28];                          //施術年月和暦;
                    cls.f033counteddays = tmp[32];      //実日数;
                    cls.f045ratio = tmp[44];            //給付割合%;
                    cls.f046family = tmp[45];                      //本人家族入外;
                    cls.f079startdate1 = tmp[78];                  //施術開始日和暦;
                    cls.f080finishdate1 = tmp[79];                 //施術終了日和暦;
                    cls.f090total = tmp[89];            //合計額;
                    cls.f091charge = tmp[90];           //請求額;
                    cls.f093partial = tmp[92];          //一部負担金;
                    cls.f103clinickana = tmp[102];                 //施術所カナ;


                    //西暦を控えておく

                    cls.startdate1AD = DateTimeEx.GetDateFromJstr7(cls.f079startdate1);                       //施術開始日西暦;
                    cls.finishdate1AD = DateTimeEx.GetDateFromJstr7(cls.f080finishdate1);                     //施術終了日西暦;
                    cls.ymAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f029ym));                   //診療年月西暦;
                    cls.shoriymAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f001shoriym));       //処理年月西暦;

                    cls.birthAD = DateTimeEx.GetDateFromJstr7(cls.f028pbirthday);                       //生年月日西暦;
                    //int y = DateTimeEx.GetAdYear(cls.f028pbirthday.Substring(0, 4));
                    //int m= int.Parse(cls.f028pbirthday.Substring(5, 2));
                    //int d =int.Parse( cls.f028pbirthday.Substring(8, 2));
                    //cls.birthAD = new DateTime(y,m,d);        //生年月日西暦メホール用;


                    //半角で持たせておく
                    //被保険者証記号
                    cls.hmark_nr = Microsoft.VisualBasic.Strings.StrConv(cls.f014hmark, Microsoft.VisualBasic.VbStrConv.Narrow);
                    //被保険者証番号
                    cls.hnum_nr = Microsoft.VisualBasic.Strings.StrConv(cls.f015hnum, Microsoft.VisualBasic.VbStrConv.Narrow);

                    //被保険者証記号番号としているが、もとは世帯主管理番号。提供データに入っていないレコードもあるので
                    //記号＋番号を生成する
                    cls.hmarkhnum_nr = cls.hmark_nr + cls.hnum_nr;


                    cls.cym = _cym; //メホール請求年月;

                    lstclsData.Add(cls);

                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                return false;
            }
        }


     
        /// <summary>
        /// 処理年月単位で取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<dataimport> select(int cym)
        {
            lstclsData.Clear();

            //データ取得
            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where cym={cym}");
            List<object[]> lst = cmd.TryExecuteReaderList();

            try
            {


                //クラスのリストに登録
                foreach (object[] tmp in lst)
                {
                    try
                    {
                        //合計金額が入ってないレコードは飛ばす(あったら提供データがおかしい
                        int.Parse(tmp[21].ToString());
                    }
                    catch
                    {
                    //    wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataimport cls = new dataimport();
                    cls.f000importid = int.Parse(tmp[0].ToString());

                    cls.f001shoriym = tmp[1].ToString();                       //処理年月和暦;
                    cls.f002comnum = tmp[2].ToString();                         //レセプト全国共通キー
                    cls.f006clinicnum = tmp[3].ToString();                      //医療機関コード;
                    cls.f008clinicname = tmp[4].ToString();                     //施術所名漢字;
                    cls.f011accountname = tmp[5].ToString();                    //口座名称;
                    cls.f012insnum = tmp[6].ToString();                         //保険者番号;
                    cls.f014hmark = tmp[7].ToString();                          //被保険者証記号;
                    cls.f015hnum = tmp[8].ToString();                           //被保険者証番号;
                    cls.f016hmarknum = tmp[9].ToString();                       //被保険者記号番号;
                    cls.f018pkana = tmp[10].ToString();                         //受診者カナ(元々は被保険者名;
                    cls.f019pname = tmp[11].ToString();                         //受診者名(元々は被保険者名;
                    cls.f020pgender = tmp[12].ToString();                       //性別;

                    //20211209100443 furukawa st ////////////////////////
                    //宛名番号個人追加
                    
                    cls.f022atenano_kojin = tmp[13].ToString();                 //宛名番号個人　2021/12/09追加;
                    //20211209100443 furukawa ed ////////////////////////


                    cls.f025atenano = tmp[14].ToString();                     	//宛名番号;
                    cls.f028pbirthday = tmp[15].ToString();                     //生年月日和暦;            
                    cls.f029ym = tmp[16].ToString();                            //施術年月和暦;
                    cls.f033counteddays = tmp[17].ToString();                   //実日数;   
                    cls.f045ratio = tmp[18].ToString();                         //給付割合%;                                       
                    cls.f046family = tmp[19].ToString();                        //本人家族入外;                                   
                    cls.f079startdate1 = tmp[20].ToString();                    //施術開始日和暦;                                      
                    cls.f080finishdate1 = tmp[21].ToString();                   //施術終了日和暦;
                    cls.f090total = tmp[22].ToString();                         //合計額;
                    cls.f091charge = tmp[23].ToString();                        //請求額;
                    cls.f093partial = tmp[24].ToString();                       //一部負担金;
                    cls.f103clinickana = tmp[25].ToString();		            //施術所カナ;



                    cls.ymAD = int.Parse(tmp[27].ToString());                   //診療年月西暦
                    cls.shoriymAD = int.Parse(tmp[28].ToString());             //処理年月西暦
                    cls.birthAD = DateTime.Parse(tmp[29].ToString());           //生年月日西暦
                    cls.startdate1AD = DateTime.Parse(tmp[30].ToString());      //開始日西暦
                    cls.finishdate1AD = DateTime.Parse(tmp[31].ToString());     //終了日西暦


                    cls.hmark_nr = tmp[32].ToString();                          //被保険者証記号
                    cls.hnum_nr = tmp[33].ToString();                           //被保険者証番号
                    cls.hmarkhnum_nr = tmp[34].ToString();                      //被保険者証記号＋番号
                    cls.cym = cym;                                              //メホール請求年月;

                    lstclsData.Add(cls);

                    //wf.InvokeValue++;
                }

           //     wf.LogPrint("CSVロード終了");
                return lstclsData;

            }


            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return lstclsData;
            }
        }
    

        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {
            
            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            
            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport group by cym order by cym desc");
            List<object[]> lst=cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
