﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace Mejor.HigashiOsakaKokuho
{
    class RefRece : RefReceCore
    {
        public DateTime Birthday { get; set; }
        public SEX Sex { get; set; }
        public DateTime StartDate { get; set; }
        public int Ratio { get; set; }

        [DB.DbAttribute.Ignore]
        public string JpBirthday => Birthday == DateTimeEx.DateTimeNull ?
            string.Empty : DateTimeEx.ToJDateShortStr(Birthday);

        public static bool Import()
        {
            string fileName;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "CSVファイル|*.csv|Excelマクロファイル|*.xlsm";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                return import(fileName, wf);
            }
        }

        private static bool import(string fileName, WaitForm wf)
        {
            List<RefRece> list;
            var extension = Path.GetExtension(fileName);
            
            try
            {
                wf.LogPrint("申請書を読み込んでいます");
                list = extension.ToLower() == ".csv" ?
                    getRefReceFromCsv(fileName) :
                    getRefReceFromXlsm(fileName);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("インポートに失敗しました");
                return false;
            }

            if (list.Count == 0) return true;

            var res = MessageBox.Show($"{list[0].CYM.ToString("0000年00月")}分として、" +
                $"{list.Count}件をインポートします。よろしいですか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res != DialogResult.OK) return true;

            using (var tran = DB.Main.CreateTransaction())
            {
                wf.LogPrint("インポート情報をDBに登録しています");
                var rri = RefReceImport.GetNextImport();
                rri.FileName = fileName;
                rri.CYM = list[0].CYM;
                rri.ReceCount = list.Count;
                rri.AppType = APP_TYPE.柔整;
                rri.Insert(tran);

                list.ForEach(r => r.ImportID = rri.ImportID);

                wf.SetMax(rri.ReceCount);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                wf.LogPrint("柔整申請書情報をDBに登録しています");
                foreach (var item in list)
                {
                    if (!DB.Main.Insert(item, tran))
                    {
                        MessageBox.Show("インポートに失敗しました");
                        return false;
                    }
                    wf.InvokeValue++;
                }

                tran.Commit();
            }
            MessageBox.Show("インポートが完了しました");
            return true;
        }

        private static List<RefRece> getRefReceFromCsv(string fileName)
        {
            var rrs = new List<RefRece>();
            var l = CommonTool.CsvImportShiftJis(fileName);

            foreach (var item in l)
            {
                if (item.Length < 20) continue;

                var rr = new RefRece();

                //20190816163220 furukawa st ////////////////////////
                //月がないためGetAdYearMonthFromJyymmに変更
                
                int jymToAdym(int jym) =>
                       DateTimeEx.GetAdYearMonthFromJyymm(jym);
                            //int jymToAdym(int jym) =>
                            //    DateTimeEx.GetAdYearFromEraYear(jym / 10000, jym / 100 % 100) * 100 + jym % 100;
                //20190816163220 furukawa ed ////////////////////////

                DateTime jymdToDt(int gyymmdd)
                {
                    int g = gyymmdd / 1000000;

                    //20190816162830 furukawa st ////////////////////////
                    //月がないためGetAdYearMonthFromJyymmに変更し、年だけにする
                    
                            //int y = DateTimeEx.GetAdYearFromEraYear(g, gyymmdd / 10000 % 100);
                    int y = DateTimeEx.GetAdYearMonthFromJyymm(gyymmdd / 100) / 100;
                    //20190816162830 furukawa ed ////////////////////////

                    int m = gyymmdd / 100 % 100;
                    int d = gyymmdd % 100;
                    return DateTimeEx.IsDate(y, m, d) ?
                        new DateTime(y, m, d) : DateTimeEx.DateTimeNull;
                }

                int.TryParse(item[0], out int refCym);
                rr.CYM = jymToAdym(refCym);
                rr.ClinicNum = item[2].Remove(2) + item[2].Substring(3);
                rr.InsNum = item[3];
                rr.Num = item[4];
                int.TryParse(item[5], out int birth);
                rr.Birthday = jymdToDt(birth);
                rr.Sex = item[6] == "1" ? SEX.男 : item[6] == "2" ? SEX.女 : SEX.Null;
                int.TryParse(item[13], out int mym);
                rr.MYM = jymToAdym(mym);
                int.TryParse(item[21], out int sDate);
                rr.StartDate = jymdToDt(sDate);
                int.TryParse(item[47], out int ratio);
                rr.Ratio = ratio;
                int.TryParse(item[57], out int days);
                rr.Days = days;
                int.TryParse(item[128], out int total);
                rr.Total = total;
                int.TryParse(item[129], out int charge);
                rr.Charge = charge;
                rr.AppType = APP_TYPE.柔整;

                rrs.Add(rr);
            }
            return rrs;
        }

        private static List<RefRece> getRefReceFromXlsm(string fileName)
        {
            //20190816131237 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更
            
            int jymToAdym(int jym) =>
                DateTimeEx.GetAdYearMonthFromJyymm(jym);
                //int jymToAdym(int jym) =>
                //    DateTimeEx.GetAdYearFromEraYear(jym / 10000, jym / 100 % 100) * 100 + jym % 100;
            //20190816131237 furukawa ed ////////////////////////


            DateTime jymdToDt(int gyymmdd)
            {
                int g = gyymmdd / 1000000;

                //20190816162717 furukawa st ////////////////////////
                //月がないためGetAdYearMonthFromJyymmに変更し、年だけにする                
                int y = DateTimeEx.GetAdYearMonthFromJyymm(gyymmdd / 100) / 100;
                            //int y = DateTimeEx.GetAdYearFromEraYear(g, gyymmdd / 10000 % 100);
                //20190816162717 furukawa ed ////////////////////////


                int m = gyymmdd / 100 % 100;
                int d = gyymmdd % 100;
                return DateTimeEx.IsDate(y, m, d) ?
                    new DateTime(y, m, d) : DateTimeEx.DateTimeNull;
            }

            var rrs = new List<RefRece>();

            using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
            {
                var xls = new XSSFWorkbook(fs);
                var sheet = xls.GetSheetAt(0);
                var rowCount = sheet.LastRowNum + 2;

                for (int i = 0; i < rowCount; i++)
                {
                    try
                    {
                        var row = sheet.GetRow(i);
                        if ((row?.GetCell(13)?.CellType ?? CellType.Blank) != CellType.Numeric) continue;
                        if ((row?.GetCell(19)?.CellType ?? CellType.Blank) != CellType.Numeric) continue;
                        if ((row?.GetCell(20)?.CellType ?? CellType.Blank) != CellType.Numeric) continue;

                        var rr = new RefRece();
                        rr.CYM = jymToAdym(int.Parse(row.GetCell(6).StringCellValue));
                        var cnum = row.GetCell(8).StringCellValue.Trim();
                        rr.ClinicNum = cnum.Remove(2) + cnum.Substring(3);
                        rr.InsNum = row.GetCell(5).StringCellValue.Trim();
                        rr.Num = Utility.Abc123ToHalf(row.GetCell(9).StringCellValue.Trim());
                        rr.Birthday = jymdToDt(int.Parse(row.GetCell(10).StringCellValue));
                        rr.Sex = row.GetCell(11).StringCellValue.Trim() == "1" ? SEX.男 :
                            row.GetCell(11).StringCellValue.Trim() == "2" ? SEX.女 :
                            SEX.Null;
                        rr.MYM = jymToAdym(int.Parse(row.GetCell(7).StringCellValue));
                        rr.Days = (int)row.GetCell(13).NumericCellValue;
                        rr.Total = (int)row.GetCell(19).NumericCellValue;
                        rr.AppType = APP_TYPE.柔整;

                        rrs.Add(rr);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            return rrs;
        }


        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="mym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cym, int mym, string num, int total) =>
            DB.Main.Select<RefRece>(new { num, mym, total, cym }).ToList();

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cym, string num, int total) =>
            DB.Main.Select<RefRece>(new { cym, num, total }).ToList();

        public static List<RefRece> SelectAll(int cym) =>
            DB.Main.Select<RefRece>(new { cym }).ToList();
    }
}
