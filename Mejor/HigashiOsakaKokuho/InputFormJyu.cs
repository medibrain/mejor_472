﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.HigashiOsakaKokuho
{
    public partial class InputFormJyu : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsRefReceData;
        private InputMode inputMode;
        protected override Control inputPanel => panelRight;
        int cym;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 0);
        Point posHnum = new Point(800, 0);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1100);
        Point posTotal = new Point(800, 1800);
        Point posBui = new Point(80, 750);

        //20190712152852 furukawa st ////////////////////////
        //あはき区別のため入力タイプの引数追加
        public InputFormJyu(InputMode mode, ScanGroup sGroup, int inputcount, int aid = 0)
        //public InputFormJyu(InputMode mode, ScanGroup sGroup,  int aid = 0)
        //20190712152852 furukawa ed ////////////////////////
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode == InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            panelInfo.Visible = true;
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;

            //20190712141537 furukawa st ////////////////////////
            //柔整は2回め入力させない
            
            if (inputcount > 1) return;
            //20190712141537 furukawa ed ////////////////////////



            this.scanGroup = sGroup;
            var list = App.GetAppsGID(scanGroup.GroupID);

            //柔整200dpi
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                posYM = new Point(80, 0);
                posHnum = new Point(800, 0);
                posNew = new Point(800, 500);
                posVisit = new Point(350, 500);
                posTotal = new Point(800, 1250);
                posBui = new Point(80, 500);

            }



            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            
            //Appリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
        }

        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputFormJyu(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }

        private void initialize()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            //広域データ欄
            bsRefReceData = new BindingSource();
            bsRefReceData.DataSource = new List<RefRece>();
            dataGridRefRece.DataSource = bsRefReceData;

            foreach (DataGridViewColumn c in dataGridRefRece.Columns) c.Visible = false;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Width = 60;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Width = 40;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].HeaderText = "年月";
            dataGridRefRece.Columns[nameof(RefRece.Num)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Num)].Width = 70;
            dataGridRefRece.Columns[nameof(RefRece.Num)].HeaderText = "被保番";
            dataGridRefRece.Columns[nameof(RefRece.JpBirthday)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.JpBirthday)].Width = 100;
            dataGridRefRece.Columns[nameof(RefRece.JpBirthday)].HeaderText = "生年月日";
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].Width = 95;
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].HeaderText = "開始日";
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Width = 80;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].HeaderText = "施術師番号";
            dataGridRefRece.Columns[nameof(RefRece.Total)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Total)].Width = 50;
            dataGridRefRece.Columns[nameof(RefRece.Total)].HeaderText = "合計";
            dataGridRefRece.Columns[nameof(RefRece.AID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.AID)].Width = 60;

            //pgupで選択行が変わるのを防ぐ
            dataGridRefRece.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            if (app == null) clearApp();
            else setApp(app);
        }

        //フォーム表示時
        private void inputForm_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void inputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectKoikiData(app);
        }

        private void selectKoikiData(App app)
        {
            bsRefReceData.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefReceData.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時


                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear * 100 + scanGroup.cmonth) * 100 + scanGroup.cmonth;
                int ym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;

                //int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear) * 100 + scanGroup.cmonth;
                //int ym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


                l = RefRece.SerchByInput(cym, ym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefReceData.DataSource = l;
            bsRefReceData.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridRefRece.RowCount; i++)
                {
                    int aid = (int)dataGridRefRece[nameof(RefRece.AID), i].Value;
                    var rrid = (int)dataGridRefRece[nameof(RefRece.RrID), i].Value;

                    if (app.RrID == rrid && app.Aid == aid)
                    {
                        //既に一意の広域データとマッチング済みの場合
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridRefRece.CurrentCell = dataGridRefRece[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridRefRece.CurrentCell = null;
            }
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;
            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);

            //年
            int year = verifyBoxY.GetIntValue();

            //20190426093650 furukawa st ////////////////////////
            //施術年のイレギュラーのエラーチェックを解除

            //イレギュラー年対応用（18/4/3久保）
            ////setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);
            setStatus(verifyBoxY, false);  

            //20190426093650 furukawa ed ////////////////////////

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out long hnumTemp));

            //長期理由
            bool longReason = verifyCheckBoxLong.Checked;
            
            //新規・継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //ここまでのチェックで必須エラーが検出されたらfalse
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //診療年月の警告
            int ms = (app.ChargeYear - year) * 12 + app.ChargeMonth - month;
            if (ms > 30)
            {
                if (MessageBox.Show("診療年月が古いですが、このまま登録してよろしいですか？",
                    "診療年月確認",　MessageBoxButtons.OKCancel, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    verifyBoxY.BackColor = SystemColors.Info;
                    verifyBoxM.BackColor = SystemColors.Info;
                }
                else
                {
                    verifyBoxY.BackColor = Color.Pink;
                    verifyBoxM.BackColor = Color.Pink;
                    return false;
                }
            }

            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text;
            app.Total = total;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //往療
            app.Distance = verifyCheckBoxVisit.Checked ? 999 : 0;

            //申請書種別
            app.AppType = APP_TYPE.柔整;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //広域データからのデータコピー
            var rr = (RefRece)dataGridRefRece.CurrentRow.DataBoundItem;
            app.PersonName = rr.Name;
            app.FushoStartDate1 = rr.StartDate;
            app.Numbering = rr.SearchNum;
            app.ClinicName = rr.ClinicName;
            app.ClinicNum = rr.ClinicNum;
            app.DrNum = rr.DrNum;
            app.CountedDays = rr.Days;
            app.DrName = rr.DrName;
            app.RrID = rr.RrID;
            app.FushoStartDate1 = rr.StartDate;

            app.Birthday = rr.Birthday;
            app.Ratio = rr.Ratio;
            app.InsNum = rr.InsNum;

            if (longReason) app.StatusFlagSet(StatusFlag.処理2);
            else app.StatusFlagRemove(StatusFlag.処理2);

            return true;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else
            {   
                //申請書の場合
                if (dataGridRefRece.CurrentCell == null)
                {
                    //20220511095741 furukawa st ////////////////////////
                    //メッセージ変更
                    
                    if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                        Application.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;

                    //MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                    //    "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                    //    "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //return false;
                    //20220511095741 furukawa ed ////////////////////////
                }

                int oldDid;
                int.TryParse(app.Numbering, out oldDid);
                var oldDis = app.Distance;

                if (!checkApp(app))
                {
                    focusBack();
                    return false;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //入力ログ
                //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                if (app.Ufirst == 0 && !InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now-dtstart_core, jyuTran))
                    return false;

                //データ記録
                app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput, tran);

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                //広域データ更新
                var rr = (RefRece)dataGridRefRece.CurrentRow?.DataBoundItem;
                if (app.RrID == 0)
                {
                    RefRece.AidDelete(app.Aid, tran);
                }
                else
                {
                    if (rr.AID != app.Aid) RefRece.AidUpdate(rr.RrID, app.Aid, tran);
                }

                //往療料データ更新
                //var oldDis = app.Distance;

                //if (rr != null && (rr.AID != app.Aid || oldDis != app.Distance))
                //    Kyori.CheckDistance(app.Aid, app.DrNum, (double)app.VisitAdd / 1000d);

                tran.Commit();
                jyuTran.Commit();
            }

            return true;
        }

        private void clearApp()
        {
            //画像クリア
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            //情報クリア
            var clearTB = new Action<TextBox>((tb) =>
            {
                tb.Text = "";
                tb.BackColor = SystemColors.Info;
            });
            
            Action<Control> act = null;
            act = new Action<Control>(c =>
            {
                if (c == panelMatchWhere) return;
                if (c is TextBox)
                {
                    c.Text = "";
                    c.BackColor = SystemColors.Info;
                }
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
                foreach (Control item in c.Controls) act(item);
            });
            act(panelRight);

            bsRefReceData.Clear();
            bsRefReceData.ResetBindings(false);
        }

        /// <summary>
        /// Appを表示します
        /// </summary>
        /// <param name="r"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //OCR 入力者情報をリセット
            labelStatusOCR.Text = string.Empty;
            labelInputUser.Text = string.Empty;

            if (app == null) return;

            if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                if (verifyCheckBoxLong.Enabled) verifyCheckBoxLong.Focus();
                else verifyCheckBoxVisit.Focus();
            }
            else verifyBoxY.Focus();

            //App_Flagのチェック
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }
            else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }

            //広域データ表示
            selectKoikiData(app);

            //全選択
            if (ActiveControl is TextBox) ((TextBox)ActiveControl).SelectAll();

            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                    scrollPictureControl1.Ratio = 0.4f;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            labelInputUser.Text = "入力：";

            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (string.IsNullOrEmpty(appsg.note2))
                    {
                        var ocr = app.OcrData.Split(',');
                        //OCRデータがあれば、部位のみ挿入
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                            verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                            verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                            verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                            verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }

                    labelStatusOCR.Text = "OCR有り";
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    //OCR情報をリセット
                    labelStatusOCR.Text = "OCRエラー";
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            labelInputUser.Text = "入力者：";

            try
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxHnum.Text = app.HihoNum;
                verifyBoxTotal.Text = app.Total.ToString();

                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (string.IsNullOrEmpty(appsg.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }
                }

                labelStatusOCR.Text = "OCRデータ有";
            }
            catch (Exception ex)
            {
                labelStatusOCR.Text = "OCRエラー";
                Log.ErrorWrite(ex);
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            labelInputUser.Text = $"入力者：{User.GetUserName(app.Ufirst)}";

            if (app.MediYear == -3)
            {
                //続紙
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == -4)
            {
                //白バッジ、その他
                verifyBoxY.Text = "++";
            }
            else if(app.MediYear == -999)
            {
                //エラー
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書
                //申請書年月
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();

                //被保険者番号
                verifyBoxHnum.Text = app.HihoNum;

                //合計金額
                verifyBoxTotal.Text = app.Total.ToString();

                //新規継続
                verifyBoxNewCont.Text = app.NewContType == NEW_CONT.新規 ? "1" : "2";

                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;

                //長期理由
                verifyCheckBoxLong.Checked = app.StatusFlagCheck(StatusFlag.処理2);

                //往療
                verifyCheckBoxVisit.Checked = app.Distance == 999;
                    
                //突合情報
                int oldDid = 0;
                int.TryParse(app.Numbering, out oldDid);
            }
        }
        
        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            var appsg = scanGroup ?? (app == null ? null : ScanGroup.SelectWithScanData(app.GroupID));

            var setCanInput = new Action<Control, bool>((t, b) =>
                {
                    if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                    else t.Enabled = b;

                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                setCanInput(verifyBoxM, false);
                setCanInput(verifyBoxHnum, false);
                setCanInput(verifyBoxTotal, false);
                setCanInput(verifyCheckBoxLong, false);
                setCanInput(verifyBoxNewCont, false);
                setCanInput(verifyBoxF1, false);
                setCanInput(verifyBoxF2, false);
                setCanInput(verifyBoxF3, false);
                setCanInput(verifyBoxF4, false);
                setCanInput(verifyBoxF5, false);

                setCanInput(verifyCheckBoxVisit, false);
            }
            else
            {
                //申請書の場合
                setCanInput(verifyBoxM, true);
                setCanInput(verifyBoxHnum, true);
                setCanInput(verifyBoxTotal, true);
                setCanInput(verifyCheckBoxLong, true);
                setCanInput(verifyBoxNewCont, true);
                setCanInput(verifyBoxF1, true);
                setCanInput(verifyBoxF2, true);
                setCanInput(verifyBoxF3, true);
                setCanInput(verifyBoxF4, true);
                setCanInput(verifyBoxF5, true);
                setCanInput(verifyCheckBoxVisit, true);
                buiTabStopAdjust();
            }

        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            if (sender is TextBox) ((TextBox)sender).SelectAll();

            Point p;

            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;
            else if (sender == verifyBoxTotal) p = posTotal;
            else if (sender == verifyCheckBoxLong) p = posHnum;
            else if (sender == verifyBoxNewCont) p = posNew;
            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;
            else if (sender == verifyCheckBoxVisit) p = posVisit;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }


        /// <summary>
        /// データエラーチェック後、画面に復帰する際にエラー箇所にフォーカスを移動
        /// </summary>
        private void focusBack()
        {
            var cs = new Control[] { verifyBoxY, verifyBoxM, verifyBoxHnum,verifyBoxTotal,
                verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5,
                verifyCheckBoxLong, verifyBoxNewCont, verifyCheckBoxVisit, };

            foreach (var item in cs)
            {
                if (item.Enabled && item.BackColor != SystemColors.Info)
                {
                    item.Focus();
                    return;
                }
            }
            verifyBoxY.Focus();
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (ActiveControl == verifyBoxY) posYM = pos;
            else if (ActiveControl == verifyBoxM) posYM = pos;
            else if (ActiveControl == verifyBoxHnum) posHnum = pos;
            else if (ActiveControl == verifyCheckBoxLong) posHnum = pos;
            else if (ActiveControl == verifyBoxNewCont) posNew = pos;
            else if (ActiveControl == verifyBoxTotal) posTotal = pos;
            else if (ActiveControl == verifyBoxF1) posBui = pos;
            else if (ActiveControl == verifyBoxF2) posBui = pos;
            else if (ActiveControl == verifyBoxF3) posBui = pos;
            else if (ActiveControl == verifyBoxF4) posBui = pos;
            else if (ActiveControl == verifyBoxF5) posBui = pos;
        }

        private void checkBoxMatch_CheckedChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectKoikiData(app);
        }
    }
}
