﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// 宮城県国保用として実装した画面
/// </summary>
namespace Mejor.OtherInput
{
    public class OtherInputListStatus
    {
        public int GroupID { get; private set; }
        public APP_TYPE AppType { get; private set; }
        //public string Note1 { get; private set; }
        //public string Note2 { get; private set; }
        //public string UserName { get; private set; }
        public string WorkingUsers { get; private set; }

        //宮城県国保独自

        //入力カウント　先行入力済　
        public int cntInputPriority => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.入力済)).Select(a => a.Count).Sum();


        //20200713163900 furukawa st ////////////////////////
        //件数カウント条件変更
        
        //件数カウント　患者照会対象数
        public int cntKanjaShokaiCount => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.照会対象)).Select(a => a.Count).Sum();
        //Public int cntKanjaShokaiCount => list.Where(a => a.FlagCheck(StatusFlag.照会対象)).Select(a => a.Count).Sum();
        //20200713163900 furukawa ed ////////////////////////


        //20200713164030 furukawa st ////////////////////////
        //件数カウント条件変更
        
        //入力カウント　独自処理1（患者照会対象入力1）
        public int cntKanjaInput1st => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.独自処理1) && a.StatusFlags.HasFlag(StatusFlag.照会対象)).Select(a => a.Count).Sum();
        //public int cntKanjaInput1st => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.独自処理1)).Select(a => a.Count).Sum();
        //20200713164030 furukawa ed ////////////////////////




        //進捗率
        public string perKanjaInput1st => 
            cntKanjaInput1st > 0 ? (double.Parse(cntKanjaInput1st.ToString()) / double.Parse(cntKanjaShokaiCount.ToString())).ToString("0%") : 0.ToString("0%");

        //入力カウント　独自処理2 患者照会対象入力2
        public int cntKanjaInput2nd => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.独自処理2) && a.StatusFlags.HasFlag(StatusFlag.照会対象)).Select(a => a.Count).Sum();
        //public int cntKanjaInput2nd => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.独自処理2)).Select(a => a.Count).Sum();


        //進捗率
        public string perKanjaInput2nd =>
            cntKanjaInput2nd > 0 ? (double.Parse(cntKanjaInput2nd.ToString()) / double.Parse(cntKanjaShokaiCount.ToString())).ToString("0%") : 0.ToString("0%");

        //20200713164046 furukawa st ////////////////////////
        //件数カウント条件変更
        

        //入力カウント　独自処理1(施術所照会対象)
        public int ClinicShokaiCount => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.処理4) || a.StatusFlags.HasFlag(StatusFlag.処理3)).Select(a => a.Count).Sum();
        //public int ClinicShokaiCount => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.処理4)).Select(a => a.Count).Sum();
        //20200713164046 furukawa ed ////////////////////////

        //20200713164100 furukawa st ////////////////////////
        //件数カウント条件変更
        
        //入力カウント　施術所照会対象1回目
        public int cntInputClinic1st => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.追加入力済) && 
                (a.StatusFlags.HasFlag(StatusFlag.処理4) || a.StatusFlags.HasFlag(StatusFlag.処理3))).Select(a => a.Count).Sum();
        //public int cntInputClinic1st => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.追加入力済)).Select(a => a.Count).Sum();
        //20200713164100 furukawa ed ////////////////////////



        //進捗率
        public string perClinicInput1st =>
            cntInputClinic1st > 0 ? (double.Parse(cntInputClinic1st.ToString()) / double.Parse(ClinicShokaiCount.ToString())).ToString("0%") : 0.ToString("0%");

        //20200713164138 furukawa st ////////////////////////
        //件数カウント条件変更
        
        //入力カウント　施術所照会対象2回目
        public int cntInputClinic2nd => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.追加ベリ済) &&
                (a.StatusFlags.HasFlag(StatusFlag.処理4) || a.StatusFlags.HasFlag(StatusFlag.処理3))).Select(a => a.Count).Sum();
        //public int cntInputClinic2nd => list.Where(a => a.StatusFlags.HasFlag(StatusFlag.追加ベリ済)).Select(a => a.Count).Sum();
        //20200713164138 furukawa ed ////////////////////////


        //進捗率
        public string perClinicInput2nd => 
            cntInputClinic2nd>0?(double.Parse(cntInputClinic2nd.ToString()) / double.Parse(ClinicShokaiCount.ToString())).ToString("0%") : 0.ToString("0%");



        public string TenkenStatus => TenkenCount == 0 ? "点検なし" :
            TenkenFinishCount == 0 ? "未点検" :
            TenkenCount == TenkenFinishCount ? "点検済" : "点検中";
        public int TenkenCount => list.Where(a => a.FlagCheck(StatusFlag.点検対象)).Select(a => a.Count).Sum();
        public int TenkenFinishCount => list.Where(a => a.FlagCheck(StatusFlag.点検済)).Select(a => a.Count).Sum();
        public int KagoCount => list.Where(a => a.FlagCheck(StatusFlag.過誤)).Select(a => a.Count).Sum();
        public int SaishinCount => list.Where(a => a.FlagCheck(StatusFlag.再審査)).Select(a => a.Count).Sum();
        public int HoryuCount => list.Where(a => a.FlagCheck(StatusFlag.保留)).Select(a => a.Count).Sum();
        public string OryoStatus => OryoCount == 0 ? "点検なし" :
            OryoFinishCount == 0 ? "未点検" :
            OryoCount == OryoFinishCount ? "点検済" : "点検中";
        public int OryoCount => list.Where(a => a.FlagCheck(StatusFlag.往療点検対象)).Select(a => a.Count).Sum();
        public int OryoFinishCount => list.Where(a => a.FlagCheck(StatusFlag.往療点検済)).Select(a => a.Count).Sum();
        public int OryoGigiCount => list.Where(a => a.FlagCheck(StatusFlag.往療疑義)).Select(a => a.Count).Sum();
        public int HenreiCount => list.Where(a => a.FlagCheck(StatusFlag.返戻)).Select(a => a.Count).Sum();
    
        private List<StatusOnlyApp> list = new List<StatusOnlyApp>();
        

        [DB.DbAttribute.DifferentTableName("application")]
        class StatusOnlyApp
        {
            public int GroupID { get; private set; }
            public StatusFlag StatusFlags { get; set; }
            public int Count { get; private set; }
            public bool FlagCheck(StatusFlag flag) => (StatusFlags & flag) == flag;

            public static IEnumerable<StatusOnlyApp> GetList(int cym)
            {
                var sql = "SELECT groupid, statusflags, COUNT(statusflags) " +
                    "FROM application WHERE cym=:cym " +
                    " and statusflags>1 " +
                    "GROUP BY groupid, statusflags;";

                var l = new List<StatusOnlyApp>();
                using (var cmd = DB.Main.CreateCmd(sql))
                {
                    cmd.Parameters.Add("cym", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                    var res  = cmd.TryExecuteReaderList();
                    if (res == null) return l;
                    foreach (var item in res)
                    {
                        var sa = new StatusOnlyApp();
                        sa.GroupID = (int)item[0];
                        sa.StatusFlags = (StatusFlag)(int)item[1];
                        sa.Count = (int)(long)item[2];                        
                        l.Add(sa);
                    }
                    return l;
                }
            }
        }

        public static List<OtherInputListStatus>GetList(int cym)
        {
            var dic = new Dictionary<int, OtherInputListStatus>();
            var l = StatusOnlyApp.GetList(cym);

            foreach (var item in l)
            {
                if (!dic.ContainsKey(item.GroupID))
                {
                    dic.Add(item.GroupID, new OtherInputListStatus());
                    dic[item.GroupID].GroupID = item.GroupID;
                }

                dic[item.GroupID].list.Add(item);
            }

            var sql = "SELECT g.groupid, s.apptype, s.note1, s.note2, g.inquiryuser ,a.statusflags ,g.workingusers " +
                "FROM scangroup as g, scan as s ,application as a " +
                "WHERE g.scanid=s.sid AND s.cym=:cym AND s.sid=a.scanid;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("cym", NpgsqlTypes.NpgsqlDbType.Integer).Value = cym;
                var res = cmd.TryExecuteReaderList();

                foreach (var item in res)
                {
                    int gid = (int)item[0];
                    if (!dic.ContainsKey(gid)) continue;

                    dic[gid].AppType = (APP_TYPE)(int)item[1];
                    //dic[gid].Note1 = (string)item[2];
                    //dic[gid].Note2 = (string)item[3];
                    //dic[gid].UserName = User.GetUserName((int)item[4]);
                    dic[gid].WorkingUsers = item[6].ToString();// != string.Empty ? User.GetUserName((int)item[6]) : string.Empty;

                }
            }

            var rl = dic.Values.ToList();
            rl.Sort((x, y) => x.GroupID.CompareTo(y.GroupID));
            return rl;
        }

    }
}
