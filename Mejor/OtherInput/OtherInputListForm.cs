﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace Mejor.OtherInput
{
    public partial class OtherInputListForm : Form
    {
        /// <summary>
        /// どのボタンから入ったかフラグ
        /// </summary>
        enum inputbutton
        {
            input1_1,
            input1_2,
            input2_1,
            input2_2,
        }


        private BindingSource bs = new BindingSource();
        private int cym;
        

        public OtherInputListForm(int cym,Insurer ins)
        {
            InitializeComponent();
            this.cym = cym;

            init();
        }



        /// <summary>
        /// グリッド/数字初期化
        /// </summary>
        private void init()
        {

            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Alignment = DataGridViewContentAlignment.MiddleRight;
            cs.BackColor = Color.PaleGreen;

            #region グリッド
            var l = OtherInputListStatus.GetList(cym);
            bs.DataSource = l;
            dataGridView1.DataSource = bs;

            dataGridView1.Columns[nameof(OtherInputListStatus.GroupID)].Width = 60;
            dataGridView1.Columns[nameof(OtherInputListStatus.GroupID)].HeaderText = "GroupID";
            dataGridView1.Columns[nameof(OtherInputListStatus.AppType)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.AppType)].HeaderText = "種類";
            //dataGridView1.Columns[nameof(OtherInputListStatus.Note1)].Width = 70;
            //dataGridView1.Columns[nameof(OtherInputListStatus.Note1)].HeaderText = "Note1";
            //dataGridView1.Columns[nameof(OtherInputListStatus.Note2)].Width = 70;
            //dataGridView1.Columns[nameof(OtherInputListStatus.Note2)].HeaderText = "Note2";
            //dataGridView1.Columns[nameof(OtherInputListStatus.UserName)].Width = 80;
            //dataGridView1.Columns[nameof(OtherInputListStatus.UserName)].HeaderText = "ユーザー";

            dataGridView1.Columns[nameof(OtherInputListStatus.WorkingUsers)].Width = 100;
            dataGridView1.Columns[nameof(OtherInputListStatus.WorkingUsers)].HeaderText = "ユーザー";

            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputPriority)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputPriority)].HeaderText = "先行入力";

            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaShokaiCount)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaShokaiCount)].HeaderText = "患者照会件数";

            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaInput1st)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaInput1st)].HeaderText = "患者照会1回";

            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput1st)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput1st)].HeaderText = "進捗%";
            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput1st)].DefaultCellStyle = cs;

            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaInput2nd)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntKanjaInput2nd)].HeaderText = "患者照会2回";

            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput2nd)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput2nd)].HeaderText = "進捗%";
            dataGridView1.Columns[nameof(OtherInputListStatus.perKanjaInput2nd)].DefaultCellStyle = cs;

            dataGridView1.Columns[nameof(OtherInputListStatus.ClinicShokaiCount)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.ClinicShokaiCount)].HeaderText = "施術所照会件数";

            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputClinic1st)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputClinic1st)].HeaderText = "施術所照会1回";

            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput1st)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput1st)].HeaderText = "進捗%";
            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput1st)].DefaultCellStyle = cs;

            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputClinic2nd)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.cntInputClinic2nd)].HeaderText = "施術所照会2回";

            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput2nd)].Width = 70;
            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput2nd)].HeaderText = "進捗%";
            dataGridView1.Columns[nameof(OtherInputListStatus.perClinicInput2nd)].DefaultCellStyle = cs;






            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenStatus)].Width = 80;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenStatus)].HeaderText = "点検状況";
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenCount)].HeaderText = "対象";
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenFinishCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenFinishCount)].HeaderText = "点検済";
            dataGridView1.Columns[nameof(OtherInputListStatus.SaishinCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.SaishinCount)].HeaderText = "再審査";
            dataGridView1.Columns[nameof(OtherInputListStatus.KagoCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.KagoCount)].HeaderText = "過誤";
            dataGridView1.Columns[nameof(OtherInputListStatus.HoryuCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.HoryuCount)].HeaderText = "保留";
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoStatus)].Width = 80;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoStatus)].HeaderText = "往療状況";
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoCount)].HeaderText = "対象";
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoFinishCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoFinishCount)].HeaderText = "点検済";
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoGigiCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoGigiCount)].HeaderText = "疑義";
            dataGridView1.Columns[nameof(OtherInputListStatus.HenreiCount)].Width = 50;
            dataGridView1.Columns[nameof(OtherInputListStatus.HenreiCount)].HeaderText = "返戻";

            //不要な列見せない
            //dataGridView1.Columns[nameof(OtherInputListStatus.UserName)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenStatus)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.TenkenFinishCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.SaishinCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.KagoCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.HoryuCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoStatus)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoFinishCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.OryoGigiCount)].Visible = false;
            dataGridView1.Columns[nameof(OtherInputListStatus.HenreiCount)].Visible = false;


            dataGridView1.ColumnHeadersHeight = 40;


            #endregion

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年度{cym % 100}月処理分";
            //labelY.Text = $"{DateTimeEx.GetHsYearFromAd(cym / 100)}年度{cym % 100}月処理分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.Text += " - " + Mejor.Insurer.CurrrentInsurer.InsurerName;

            showCount(l);
            Shown += (s, e) => dataGridView1.Focus();
        }


        #region 件数テキストボックス
        private void showCount(List<OtherInputListStatus> l)
        {
           
            int allKanjaShokai = l.Sum(g => g.cntKanjaShokaiCount);
            int inputKanjaShokai1 = l.Sum(g => g.cntKanjaInput1st);
            int inputKanjaShokai2 = l.Sum(g => g.cntKanjaInput2nd);

            int allClinicShokai = l.Sum(g => g.ClinicShokaiCount);
            int inputClinicShokai1 = l.Sum(g => g.cntInputClinic1st);
            int inputClinicShokai2 = l.Sum(g => g.cntInputClinic2nd);


            string strInput1Per = string.Empty;
            string strInput2Per = string.Empty;
            
            string strClinic1Per = string.Empty;
            string strClinic2Per = string.Empty;

            
            if (inputKanjaShokai1 == 0) strInput1Per = 0.ToString("0%");
            else strInput1Per = (double.Parse(inputKanjaShokai1.ToString()) / double.Parse(allKanjaShokai.ToString())).ToString("#%");
            if (inputKanjaShokai2 == 0) strInput2Per = 0.ToString("0%");
            else strInput2Per = (double.Parse(inputKanjaShokai2.ToString()) / double.Parse(allKanjaShokai.ToString())).ToString("#%");

            if (inputClinicShokai1 == 0) strClinic1Per = 0.ToString("0%");
            else strClinic1Per = (double.Parse(inputClinicShokai1.ToString()) / double.Parse(allClinicShokai.ToString())).ToString("#%");
            if (inputClinicShokai2 == 0) strClinic2Per = 0.ToString("0%");
            else strClinic2Per = (double.Parse(inputClinicShokai2.ToString()) / double.Parse(allClinicShokai.ToString())).ToString("#%");

            textBoxKanjashokai.Text = allKanjaShokai.ToString();
            textBoxKanja1Percent.Text = strInput1Per;
            textBoxKanja2Percent.Text = strInput2Per;

            textBoxClinicShokai.Text = allClinicShokai.ToString();
            textBoxClinic1Per.Text = strClinic1Per;
            textBoxClinic2Per.Text = strClinic2Per;

        }
        #endregion

        #region ソート
        string sortColumnName;
        bool sortASC;
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var l = (List<OtherInputListStatus>)bs.DataSource;
            if (l == null) return;
            if (e.ColumnIndex < 0) return;
            
            var name = dataGridView1.Columns[e.ColumnIndex].Name;
            sortASC = (name == sortColumnName) ? !sortASC : true;
            sortColumnName = name;

            var ps = typeof(OtherInputListStatus).GetProperties();
            var p = Array.Find(ps, pp => pp.Name == name);
            if (p == null) return;

            l.Sort((x, y) =>
            {
                var xp = p.GetValue(x, null);
                var yp = p.GetValue(y, null);

                //nullを最小に
                if (xp == null && yp == null) return 0;
                if (xp == null) return sortASC ? -1 : 1;
                if (yp == null) return sortASC ? 1 : -1;

                int compareRes = 0;
                if (p.PropertyType == typeof(int?)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(int)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(DateTime)) compareRes = ((DateTime)xp).CompareTo(((DateTime)yp));
                else if (p.PropertyType == typeof(string)) compareRes = ((string)xp).CompareTo(((string)yp));
                else compareRes = (xp.ToString()).CompareTo(yp.ToString());

                return sortASC ? compareRes : -compareRes;
            });
            bs.ResetBindings(false);
        }
        #endregion


        
        private void resetBindings()
        {
            var l = OtherInputListStatus.GetList(cym);
            showCount(l);

            bs.DataSource = l;
            bs.ResetBindings(false);
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            resetBindings();
        }

        /// <summary>
        /// 閉じる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region 入力用ボタン
        /// <summary>
        /// 患者照会1回目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput1_1_Click(object sender, EventArgs e)
        {
            starter(INPUT_TYPE.ListInput1,inputbutton.input1_1);            
        }

        /// <summary>
        /// 患者照会2回目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput1_2_Click(object sender, EventArgs e)
        {
            starter(INPUT_TYPE.ListInput2,inputbutton.input1_2);
        }

        /// <summary>
        /// 施術所照会1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput2_1_Click(object sender, EventArgs e)
        {
            starter(INPUT_TYPE.Extra1, inputbutton.input2_1);
        }

        /// <summary>
        /// 施術所照会2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput2_2_Click(object sender, EventArgs e)
        {
            starter(INPUT_TYPE.Extra2, inputbutton.input2_2);
        }
        #endregion


        #region 起動ボタン
        /// <summary>
        /// 画面起動
        /// </summary>
        /// <param name="iType"></param>
        private void starter(INPUT_TYPE iType,inputbutton ib)
        {
            //チェックするグループIDの取得
            if (dataGridView1.CurrentCell == null) return;
            int row = dataGridView1.CurrentCellAddress.Y;
            int gid = (int)(dataGridView1[0, row].Value);

            //表示位置を記憶
            var firstIndex = dataGridView1.FirstDisplayedScrollingRowIndex;


            var groupID = gid;

            var sg = ScanGroup.SelectWithScanData(groupID);

            if (!string.IsNullOrWhiteSpace(sg.WorkingUsers))
            {
                if (MessageBox.Show("このグループは現在他のユーザーが作業中です。\r\n" +
                    "このグループを編集しますか？", "ユーザー重複警告",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) != DialogResult.OK) return;
            }

            var lastStatus = sg.Status;

            //ScanGroupに現在のステータスを記録
            ScanGroup.WorkStart(groupID, ScanGroup.WorkType.入力);


            switch (ib)
            {
                case inputbutton.input1_1:
                    CallInput1(INPUT_TYPE.ListInput1, sg);
                    break;
                case inputbutton.input1_2:
                    CallInput1(INPUT_TYPE.ListInput2, sg);
                    break;
                case inputbutton.input2_1:
                    CallInput2(INPUT_TYPE.Extra1, sg);
                    break;
                case inputbutton.input2_2:
                    CallInput2(INPUT_TYPE.Extra2, sg);
                    break;

                default:
                    break;
            }

            ScanGroup.WorkFinish(sg.GroupID);

            var l = OtherInputListStatus.GetList(cym);
            bs.DataSource = l;
            dataGridView1.DataSource = bs;

            //グリッド初期化/再計算
            init();

        }

        /// <summary>
        /// 宮城県国保　患者照会項目入力
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="sg"></param>
        private void CallInput1(INPUT_TYPE iType,ScanGroup sg)
        {
            var firsttime = INPUT_TYPE.ListInput1;
            var verify = iType == INPUT_TYPE.ListInput2;


            InsurerID iid = (InsurerID)Insurer.CurrrentInsurer.InsurerID;
            switch (iid)
            {
                case InsurerID.MIYAGI_KOKUHO:
                    MiyagiKokuho.InputForm2 f = new MiyagiKokuho.InputForm2(sg, !verify);
                    f.ShowDialog();
                    
                    break;

                default:
                    break;

            }

        }

        /// <summary>
        /// 宮城県国保　施術所照会入力
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="sg"></param>
        private void CallInput2(INPUT_TYPE iType, ScanGroup sg)
        {
            var firsttime = INPUT_TYPE.Extra1;
            var verify = iType == INPUT_TYPE.Extra2;

            

            InsurerID iid = (InsurerID)Insurer.CurrrentInsurer.InsurerID;

            switch (iid)
            {
                case InsurerID.MIYAGI_KOKUHO:
                    MiyagiKokuho.InputForm3 f = new MiyagiKokuho.InputForm3(sg, !verify);
                    f.ShowDialog();
                   

                    break;

                default:
                    break;


            }
            
        }
        #endregion
    }
}
