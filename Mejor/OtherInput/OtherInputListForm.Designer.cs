﻿namespace Mejor.OtherInput
{
    partial class OtherInputListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxClinic2Per = new System.Windows.Forms.TextBox();
            this.textBoxClinic1Per = new System.Windows.Forms.TextBox();
            this.textBoxClinicShokai = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxKanja2Percent = new System.Windows.Forms.TextBox();
            this.textBoxKanja1Percent = new System.Windows.Forms.TextBox();
            this.textBoxKanjashokai = new System.Windows.Forms.TextBox();
            this.labelY = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonInput2_2 = new System.Windows.Forms.Button();
            this.buttonInput2_1 = new System.Windows.Forms.Button();
            this.buttonInput1_2 = new System.Windows.Forms.Button();
            this.buttonInput1_1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxClinic2Per);
            this.panel1.Controls.Add(this.textBoxClinic1Per);
            this.panel1.Controls.Add(this.textBoxClinicShokai);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxKanja2Percent);
            this.panel1.Controls.Add(this.textBoxKanja1Percent);
            this.panel1.Controls.Add(this.textBoxKanjashokai);
            this.panel1.Controls.Add(this.labelY);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1034, 33);
            this.panel1.TabIndex = 0;
            // 
            // textBoxClinic2Per
            // 
            this.textBoxClinic2Per.Location = new System.Drawing.Point(855, 6);
            this.textBoxClinic2Per.Name = "textBoxClinic2Per";
            this.textBoxClinic2Per.ReadOnly = true;
            this.textBoxClinic2Per.Size = new System.Drawing.Size(50, 19);
            this.textBoxClinic2Per.TabIndex = 10;
            // 
            // textBoxClinic1Per
            // 
            this.textBoxClinic1Per.Location = new System.Drawing.Point(780, 6);
            this.textBoxClinic1Per.Name = "textBoxClinic1Per";
            this.textBoxClinic1Per.ReadOnly = true;
            this.textBoxClinic1Per.Size = new System.Drawing.Size(50, 19);
            this.textBoxClinic1Per.TabIndex = 10;
            // 
            // textBoxClinicShokai
            // 
            this.textBoxClinicShokai.Location = new System.Drawing.Point(711, 6);
            this.textBoxClinicShokai.Name = "textBoxClinicShokai";
            this.textBoxClinicShokai.ReadOnly = true;
            this.textBoxClinicShokai.Size = new System.Drawing.Size(50, 19);
            this.textBoxClinicShokai.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(594, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "施術所照会対象　全：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(838, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(763, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(496, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(421, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(259, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "患者照会対象　全：";
            // 
            // textBoxKanja2Percent
            // 
            this.textBoxKanja2Percent.Location = new System.Drawing.Point(514, 6);
            this.textBoxKanja2Percent.Name = "textBoxKanja2Percent";
            this.textBoxKanja2Percent.ReadOnly = true;
            this.textBoxKanja2Percent.Size = new System.Drawing.Size(50, 19);
            this.textBoxKanja2Percent.TabIndex = 4;
            // 
            // textBoxKanja1Percent
            // 
            this.textBoxKanja1Percent.Location = new System.Drawing.Point(440, 6);
            this.textBoxKanja1Percent.Name = "textBoxKanja1Percent";
            this.textBoxKanja1Percent.ReadOnly = true;
            this.textBoxKanja1Percent.Size = new System.Drawing.Size(50, 19);
            this.textBoxKanja1Percent.TabIndex = 4;
            // 
            // textBoxKanjashokai
            // 
            this.textBoxKanjashokai.Location = new System.Drawing.Point(365, 6);
            this.textBoxKanjashokai.Name = "textBoxKanjashokai";
            this.textBoxKanjashokai.ReadOnly = true;
            this.textBoxKanjashokai.Size = new System.Drawing.Size(50, 19);
            this.textBoxKanjashokai.TabIndex = 4;
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(12, 9);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(121, 12);
            this.labelY.TabIndex = 0;
            this.labelY.Text = "平成27年度　月処理分";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonExit);
            this.panel2.Controls.Add(this.buttonInput2_2);
            this.panel2.Controls.Add(this.buttonInput2_1);
            this.panel2.Controls.Add(this.buttonInput1_2);
            this.panel2.Controls.Add(this.buttonInput1_1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 743);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1034, 53);
            this.panel2.TabIndex = 2;
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.Location = new System.Drawing.Point(947, 18);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonInput2_2
            // 
            this.buttonInput2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInput2_2.Location = new System.Drawing.Point(603, 18);
            this.buttonInput2_2.Name = "buttonInput2_2";
            this.buttonInput2_2.Size = new System.Drawing.Size(111, 23);
            this.buttonInput2_2.TabIndex = 0;
            this.buttonInput2_2.Text = "施術所照会入力2";
            this.buttonInput2_2.UseVisualStyleBackColor = true;
            this.buttonInput2_2.Click += new System.EventHandler(this.buttonInput2_2_Click);
            // 
            // buttonInput2_1
            // 
            this.buttonInput2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInput2_1.Location = new System.Drawing.Point(480, 18);
            this.buttonInput2_1.Name = "buttonInput2_1";
            this.buttonInput2_1.Size = new System.Drawing.Size(117, 23);
            this.buttonInput2_1.TabIndex = 0;
            this.buttonInput2_1.Text = "施術所照会入力1";
            this.buttonInput2_1.UseVisualStyleBackColor = true;
            this.buttonInput2_1.Click += new System.EventHandler(this.buttonInput2_1_Click);
            // 
            // buttonInput1_2
            // 
            this.buttonInput1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInput1_2.Location = new System.Drawing.Point(321, 18);
            this.buttonInput1_2.Name = "buttonInput1_2";
            this.buttonInput1_2.Size = new System.Drawing.Size(125, 23);
            this.buttonInput1_2.TabIndex = 0;
            this.buttonInput1_2.Text = "患者照会入力2";
            this.buttonInput1_2.UseVisualStyleBackColor = true;
            this.buttonInput1_2.Click += new System.EventHandler(this.buttonInput1_2_Click);
            // 
            // buttonInput1_1
            // 
            this.buttonInput1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInput1_1.Location = new System.Drawing.Point(197, 18);
            this.buttonInput1_1.Name = "buttonInput1_1";
            this.buttonInput1_1.Size = new System.Drawing.Size(118, 23);
            this.buttonInput1_1.TabIndex = 0;
            this.buttonInput1_1.Text = "患者照会入力1";
            this.buttonInput1_1.UseVisualStyleBackColor = true;
            this.buttonInput1_1.Click += new System.EventHandler(this.buttonInput1_1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1034, 710);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // OtherInputListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 796);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "OtherInputListForm";
            this.Text = "リスト抽出入力選択";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.TextBox textBoxKanjashokai;
        private System.Windows.Forms.TextBox textBoxClinicShokai;
        private System.Windows.Forms.Button buttonInput1_1;
        private System.Windows.Forms.Button buttonInput1_2;
        private System.Windows.Forms.Button buttonInput2_2;
        private System.Windows.Forms.Button buttonInput2_1;
        private System.Windows.Forms.TextBox textBoxKanja1Percent;
        private System.Windows.Forms.TextBox textBoxKanja2Percent;
        private System.Windows.Forms.TextBox textBoxClinic2Per;
        private System.Windows.Forms.TextBox textBoxClinic1Per;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}