﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.ZenkokuGakko
{
    class Common
    {
        private static InsurerID[] BatchLength6Insrers = 
            new[] { InsurerID.GAKKO_13TOKYO, InsurerID.OSAKA_GAKKO };

        /// <summary>
        /// 現在作業中学校共済保険者のバッチ番号が6ケタかどうかを返します
        /// </summary>
        public static bool IsBatchLength6Insrers => 
            BatchLength6Insrers.Contains(Insurer.CurrrentInsurer.EnumInsID);

        public static int BatchLength =>
            IsBatchLength6Insrers ? 6 : 4;

        public static int NumberingLength =>
            IsBatchLength6Insrers ? 4 : 6;

        public static int MaxNumbering =>
            IsBatchLength6Insrers ? 9999 : 999999;


    }
}
