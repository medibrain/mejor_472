﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    class Export
    {
        static string logText;
        static string bankNo = string.Empty;
        static string insurerNo = string.Empty;
        static string sinkyuHosCode = string.Empty;

        public static bool DoExport(Insurer ins, int cYear, int cMonth, string yyyyMM, string folderpath, bool inBatchFolder = true)
        {
            insurerNo = ins.InsNumber;
            string dir;
            dir = folderpath;

            var fw = new WaitForm();
            Task.Factory.StartNew(() => fw.ShowDialog());
            while (!fw.Visible) System.Threading.Thread.Sleep(10);

            var logText = "";

            bool imgOutput = true;

            //特定の月のスキャンID一覧を取得
            List<int> sids = new List<int>();
            using (var cmd = DB.Main.CreateCmd("SELECT sid, note1 FROM scan " +
                "WHERE cyear=:cy AND cmonth=:cm " +
                "ORDER BY note1, sid"))
            {
                cmd.Parameters.Add("cy", NpgsqlTypes.NpgsqlDbType.Integer).Value = cYear;
                cmd.Parameters.Add("cm", NpgsqlTypes.NpgsqlDbType.Integer).Value = cMonth;
                var res = cmd.TryExecuteReaderList();
                if (res == null)
                {
                    Log.ErrorWriteWithMsg(cYear + "年" + cMonth + "月分のスキャンIDを取得できませんでした");
                    return false;
                }

                foreach (var item in res)
                {
                    //対象の県以外を排除
                    var n1 = (string)item[1];
                    sids.Add((int)item[0]);
                }
            }

            fw.SetMax(sids.Count);
            fw.BarStyle = ProgressBarStyle.Continuous;
            int scanCount = 0;

            try
            {
                foreach (var item in sids)
                {
                    fw.LogPrint("SID:" + item.ToString() + "の出力を開始します");
                    fw.InvokeValue = scanCount;
                    scanCount++;
                    fw.InvokeValue++;
                    var scan = Scan.Select(item);
                    var appList = App.GetAppsSID(item);
                    if (!ExportBySID(appList, dir, yyyyMM.Substring(2), scan, imgOutput, inBatchFolder, fw)) return false;
                    fw.LogPrint("SID:" + item.ToString() + "の出力が終わりました");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
            }
            finally
            {
                fw.InvokeCloseDispose();
                bankNo = string.Empty;
            }

            //データチェック結果表示
            if (logText != "")
            {
                Log.ErrorWriteWithMsg(logText);
            }

            return true;
        }

        private static bool ExportBySID(List<App> appList, string exportFolder, string yyMM, Scan scan, bool imgOutput, bool inBatchFolder, WaitForm fw)
        {
            int numCheck = 0;
            string batchNo = scan.Note1;
            if (batchNo.Length != 4) throw new Exception("Note1に指定されたバッチNoに誤りがあります");

            string saveFolder = exportFolder;// + "\\" + inputDate;            
            if (inBatchFolder)
            {
                System.IO.Directory.CreateDirectory(saveFolder);
                saveFolder += "\\" + batchNo;
                if (imgOutput) System.IO.Directory.CreateDirectory(saveFolder);
            }

            appList.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            var fileRES = exportFolder + "\\RES" + DateTime.Today.ToString("yyMMdd") + ".csv";
            string writeYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString()
                + scan.Cyear.ToString("00") + scan.Cmonth.ToString("00");

            using (var swRes = new System.IO.StreamWriter(fileRES, true, Encoding.GetEncoding("Shift_JIS")))
            {
                string appNumbering = "";
                int pageNumber = 1;

                foreach (var app in appList)
                {
                    if (fw.Cancel) return false;

                    string originalImgName = scan.ImageFolder + "\\" + app.ImageFile;
                    string baseFileName = yyMM + batchNo;

                    if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
                    {
                        bankNo = app.AccountNumber;
                        //鍼灸の場合、バッチシートに登録記号番号 記載がない場合も、値リセットのため代入
                        sinkyuHosCode = app.DrNum;
                        continue;
                    }
                    else if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
                    {
                        //続紙
                        pageNumber++;
                        string zokushiFilename = baseFileName + appNumbering + pageNumber.ToString("00") + ".tif";

                        if (imgOutput)
                        {
                            if (!ImageUtility.SaveOne(originalImgName, saveFolder + "\\" + zokushiFilename))
                            {
                                Log.ErrorWriteWithMsg("\r\n\r\nナンバリング " +
                                    appNumbering + " 番が重複している可能性があります");
                                return false;
                            }
                        }
                        continue;
                    }
                    else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
                    {
                        continue;
                    }

                    //以下、申請書の場合
                    appNumbering = app.Numbering.PadLeft(6, '0');
                    pageNumber = 1;
                    string saveFilename = baseFileName + appNumbering + pageNumber.ToString("00") + ".tif";

                    if (imgOutput == true)
                    {
                        if (!ImageUtility.SaveOne(originalImgName, saveFolder + "\\" + saveFilename))
                        {
                            Log.ErrorWriteWithMsg("\r\n\r\nナンバリング " +
                                appNumbering + " 番が重複している可能性があります");
                            return false;
                        }
                    }

                    //ナンバリング連番チェック
                    int n;
                    if (int.TryParse(app.Numbering, out n))
                    {
                        if (numCheck == 0)
                        {
                            numCheck = n;
                        }
                        else if (n == 1)
                        {
                            numCheck = 1;
                        }
                        else
                        {
                            numCheck++;
                            if (n != numCheck)
                            {
                                logText = logText + batchNo + ": Numbering " + n +
                                    " でナンバリング不連続が検出されました。\r\n";
                            }
                        }
                    }

                    string bd = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                    bd += DateTimeEx.GetJpYear(app.Birthday).ToString("00");
                    bd += app.Birthday.ToString("MM");//. ToString("MMdd");

                    var batch = batchNo;
                    var num = app.Numbering;
                    var sinryoYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString() +
                        app.MediYear.ToString("00") + app.MediMonth.ToString("00");
                    var kubun = "0";                        //仕様書で0固定
                    var honke = app.Family.ToString();
                    var pref = app.HihoPref.ToString();
                    var kumiaiNo = app.HihoNum;
                    var sex = app.Sex == 1 ? "1" : "2";    //stringに変換する意味合いもあり
                    var birth = bd;
                    var days = app.CountedDays.ToString();
                    var totalCost = app.Total.ToString();
                    var seikyu = app.Charge.ToString();
                    var yuyo = app.HihoType == (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予 ? "9" : string.Empty;
                    var kigoubango = app.ClinicNum;               //記号番号は？
                    var hosCode = app.DrNum;
                    var futan = string.Empty;
                    if (app.Partial != 0 && app.Total - app.Charge != app.Partial)
                        futan = app.Partial.ToString();     //患者負担額

                    //鍼灸マッサージの際、バッチシートに登録番号が記載されている
                    if (scan.Note2 == "7" || scan.Note2 == "8")
                    {
                        if (string.IsNullOrEmpty(sinkyuHosCode)) throw new Exception("鍼灸マッサージの登録記号番号を判別できませんでした。");
                        hosCode = sinkyuHosCode;
                    }

                    var res = new string[18];
                    res[0] = writeYM;
                    res[1] = batch;
                    res[2] = num;
                    res[3] = sinryoYM;
                    res[4] = kubun;
                    res[5] = honke;
                    res[6] = string.Empty;
                    res[7] = insurerNo;
                    res[8] = kumiaiNo;
                    res[9] = sex;
                    res[10] = birth;
                    res[11] = days;
                    res[12] = totalCost;
                    res[13] = seikyu;
                    res[14] = yuyo;
                    res[15] = hosCode;
                    res[16] = bankNo;
                    res[17] = futan;

                    try
                    {
                        swRes.WriteLine(string.Join(",", res));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                        return false;
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cy, int cm)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var sdic = new Dictionary<int, Scan>();

            foreach (var item in scans) sdic.Add(item, Scan.Select(item));

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(
                        "AID,通知番号,保険者番号,処理年,処理月,バッチ,ナンバリング," +
                        "組合員番号,組合員氏名,受療者氏名,性別,年号,生年,月,日,郵便番号,送付先住所1,送付先住所2," +
                        "診療年,診療月,実日数,合計金額,請求金額,施術所番号,施術所名,照会理由,過誤理由,疑義理由,詳細");
                    var ss = new List<string>();
                    var jcy = cy.ToString("00");
                    var jcm = cm.ToString("00");
                    var notifyNumber = $"{jcy}{jcm}-{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<string, string[]> toAddresses = address =>
                    {
                        var result = new string[] { "", "" };
                        if (!address.Contains("　"))
                        {
                            result[0] = address;
                        }
                        else
                        {
                            var index = address.IndexOf("　");
                            result[0] = address.Substring(0, index);
                            result[1] = address.Substring(index + 1);
                        }
                        return result;
                    };

                    foreach (var item in list)
                    {
                        var adds = toAddresses(item.HihoAdd);

                        ss.Add(item.Aid.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(Insurer.CurrrentInsurer.InsNumber.ToString());
                        ss.Add(jcy);
                        ss.Add(jcm);
                        ss.Add(sdic[item.ScanID].Note1);
                        ss.Add(item.Numbering.ToString());
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(DateTimeEx.GetEra(item.Birthday));
                        ss.Add(toJYear(DateTimeEx.GetEraNumberYear(item.Birthday)));
                        ss.Add(item.Birthday.Month.ToString());
                        ss.Add(item.Birthday.Day.ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(adds[0]);
                        ss.Add(adds[1]);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(item.ClinicName.ToString());
                        ss.Add("\"" + item.ShokaiReason.ToString() + "\"");//区切り文字が「, (半角カンマ)(半角スペース)」なのでエスケープ処理
                        ss.Add("\"" + item.AppNote.KagoReason.ToString() + "\"");
                        ss.Add("\"" + item.AppNote.GigiReason.ToString() + "\"");
                        ss.Add("\"" + item.AppNote.Note.ToString() +"\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool MediMatchingExport(Insurer ins, int cy, int cm)
        {
            string fn;
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv";
                f.FileName = ins.InsurerName + " 医科突用" + cy.ToString("00") + cm.ToString("00") + ".csv";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fn = f.FileName;
            }

            var wfs = new WaitFormSimple();
            Task.Factory.StartNew(() => wfs.ShowDialog());
            var list = App.GetApps(cy, cm);

            try
            {
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.UTF8))
                {
                    sw.WriteLine("診療年月,柔整シーケンス番号,レセプト区分,保険者番号,"
                        + "保険証記号,保険証番号,性別,生年月日");

                    var l = new List<string>();
                    foreach (var a in list)
                    {
                        if (a.MediYear < 0) continue;
                        var gym = "4" + a.MediYear.ToString("00") + a.MediMonth.ToString("00");
                        l.Add(gym);
                        l.Add(a.Aid.ToString());
                        l.Add(a.Family.ToString());
                        l.Add(ins.InsNumber);
                        l.Add("");
                        l.Add(a.HihoNum);
                        l.Add(a.Sex.ToString());
                        l.Add(DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString());
                        //l.Add(a.Iname1);
                        //l.Add(a.Iname2);
                        //l.Add(a.Iname3);
                        //l.Add(a.Iname4);
                        //l.Add(a.Iname5);

                        sw.WriteLine(string.Join(",", l));
                        l.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wfs.InvokeCloseDispose();
            }


            return true;
        }

        public static bool AnalysisExport(Insurer ins, int cy, int cm)
        {
            string fn;
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv";
                f.FileName = ins.InsurerName + " 分析用" + cy.ToString("00") + cm.ToString("00") + ".csv";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fn = f.FileName;
            }

            var wfs = new WaitFormSimple();
            Task.Factory.StartNew(() => wfs.ShowDialog());

            var list = App.GetApps(cy, cm);

            try
            {
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.UTF8))
                {
                    sw.WriteLine("処理年,処理月,診療年,診療月,被保番,家族区分,災害支払猶予,性別,生年月日," +
                        "実日数,合計,請求,負担,新規継続,柔整師登録番号,ナンバリング," +
                        "初検年,初検月,負傷1,負傷2,負傷3,負傷4,負傷5,部位数");
                    var l = new List<string>();
                    foreach (var a in list)
                    {
                        if (a.MediYear < 0) continue;
                        l.Add(a.ChargeYear.ToString());
                        l.Add(a.ChargeMonth.ToString());
                        l.Add(a.MediYear.ToString());
                        l.Add(a.MediMonth.ToString());
                        l.Add(a.HihoNum);
                        l.Add(a.Family.ToString());
                        l.Add(a.HihoType.ToString());
                        l.Add(a.Sex.ToString());
                        l.Add(a.Birthday.ToString("yyyy/MM/dd"));
                        l.Add(a.CountedDays.ToString());
                        l.Add(a.Total.ToString());
                        l.Add(a.Charge.ToString());
                        l.Add(a.Partial.ToString());
                        l.Add(a.NewContType.ToString());
                        l.Add(a.DrNum);
                        l.Add(a.Numbering);
                        l.Add(DateTimeEx.GetHsYearFromAd(a.FushoFirstDate1.Year).ToString());
                        l.Add(a.FushoFirstDate1.Month.ToString());
                        l.Add(a.FushoName1);
                        l.Add(a.FushoName2);
                        l.Add(a.FushoName3);
                        l.Add(a.FushoName4);
                        l.Add(a.FushoName5);
                        int b = 0;
                        if (!string.IsNullOrWhiteSpace(a.FushoName1)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName2)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName3)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName4)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName5)) b++;
                        l.Add(b.ToString());
                        sw.WriteLine(string.Join(",", l));
                        l.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wfs.InvokeCloseDispose();
            }

            return true;
        }

        public static bool MediMatchingExportWithRESAndImage(Insurer ins, int cy, int cm)
        {
            string saveFolder = "";
            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
                saveFolder = f.SelectedPath;
            }

            var yyyyMM = $"{DateTimeEx.GetAdYearFromHs(cy).ToString("00")}{cm.ToString("00")}";
            return DoExport(ins, cy, cm, yyyyMM, saveFolder, false);
        }
    }
}