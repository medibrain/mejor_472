﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    class ExportRev
    {
        TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

        public bool DoExport(List<App> l, string dir, bool inBatchFolder, WaitForm wf)
        {
            wf.LogPrint("出力を開始します");

            try
            {
                l.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                List<List<App>> apps = new List<List<App>>();
                List<App> batchApps = new List<App>();

                //先頭バッチ検索
                int i = 0;
                for (; i < l.Count; i++)
                {
                    if (l[i].MediYear == (int)APP_SPECIAL_CODE.バッチ) break;
                }

                //バッチ分割
                for (; i < l.Count; i++)
                {
                    if (l[i].MediYear == (int)APP_SPECIAL_CODE.バッチ)
                    {
                        if (batchApps.Count != 0) apps.Add(batchApps);
                        batchApps = new List<App>();
                    }
                    batchApps.Add(l[i]);
                }
                if (batchApps.Count != 0) apps.Add(batchApps);

                //バッチごと出力
                //※同じバッチ番号が複数回出現あり　その際はナンバリング順
                apps.Sort((x, y) =>
                {
                    if (x[0].Numbering != y[0].Numbering)
                        return x[0].Numbering.CompareTo(y[0].Numbering);

                    var xFirst = x.FirstOrDefault(item => item.MediYear > 0);
                    var yFirst = y.FirstOrDefault(item => item.MediYear > 0);
                    return xFirst.Numbering.CompareTo(yFirst.Numbering);
                });

                var resFile = $"{dir}\\RES{DateTime.Today.ToString("yyMMdd")}.csv";
                var resCsvLines = new List<string[]>();

                foreach (var item in apps)
                {
                    ExportApp(item, dir, wf, resCsvLines, inBatchFolder);
                }

                //再度バッチとナンバリングでソート
                //1がバッチナンバー、2がナンバリング
                resCsvLines.Sort((x, y) => x[1] == y[1] ? x[2].CompareTo(y[2]) : x[1].CompareTo(y[1]));

                //csvへ書き込み
                using (var sw = new System.IO.StreamWriter(resFile, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    try
                    {
                        foreach (var item in resCsvLines)
                        {
                            sw.WriteLine(string.Join(",", item));
                        }
                    }
                    catch (Exception ex)
                    {
                        wf.LogPrint(ex.Message);
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                        return false;
                    }
                }
            }
            catch(Exception ex)
            {
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apps">先頭をバッチ画像とし、バッチ単位でまとめられたApp</param>
        /// <param name="batch"></param>
        /// <param name="sw"></param>
        /// <returns></returns>
        private bool ExportApp(List<App> apps, string dir, WaitForm wf, List<string[]> csvLines, bool inBatchFolder)
        {
            //書き込み日時

            //20190509160517 furukawa st ////////////////////////
            //書き込み日時取得修正

            //string writeYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString()
            //    + apps[0].ChargeYear.ToString("00") + apps[0].ChargeMonth.ToString("00");

            string strADChargeYM = DateTimeEx.GetAdYearFromHs(int.Parse(apps[0].ChargeYear.ToString("00") + 
                                                                        apps[0].ChargeMonth.ToString("00"))).ToString() 
                                                                        + apps[0].ChargeMonth.ToString("00");

            string writeYM = DateTimeEx.GetEraNumberYearFromYYYYMM(int.Parse(strADChargeYM.ToString()))+
                                                                                     apps[0].ChargeMonth.ToString("00");

            //string writeYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString()
            //    + apps[0].ChargeYear.ToString("00") + apps[0].ChargeMonth.ToString("00");
            //20190509160517 furukawa ed ////////////////////////


            //バッチ情報
            if (apps[0].MediYear != (int)APP_SPECIAL_CODE.バッチ)
                throw new Exception("先頭がバッチではありません");
            string revBatchNo, numberingHeader;
            if (Common.IsBatchLength6Insrers)
            {
                revBatchNo = apps[0].Numbering.Remove(4);
                numberingHeader = apps[0].Numbering.Substring(4, 2);
            }
            else
            {
                revBatchNo = apps[0].Numbering;
                numberingHeader = string.Empty;
            }
            var bankNo = apps[0].AccountNumber;
            var yymm = (apps[0].CYM % 10000).ToString("0000");
            var insurerNo = Insurer.CurrrentInsurer.InsNumber;
            var batchHosCode = apps[0].DrNum == "++" ? string.Empty : apps[0].DrNum;//登録記号番号++出力問題対応
            var firstNumbering = apps.FirstOrDefault(app => app.MediYear > 0).Numbering;
            wf.LogPrint($"バッチNo.[{revBatchNo}]-ナンバリング[{firstNumbering}～]の出力中です…");

            //バッチごとに画像フォルダ
            string imgDir;
            if (inBatchFolder)
            {
                imgDir = dir + "\\" + revBatchNo;
                System.IO.Directory.CreateDirectory(imgDir);
            }
            else
            {
                imgDir = dir;
            }

            int page = 1;
            string numbering = string.Empty;
            wf.InvokeValue++;

            for (int i = 1; i < apps.Count; i++)
            {
                wf.InvokeValue++;

                var app = apps[i];
                if(app.MediYear > 0)
                {
                    numbering = Common.IsBatchLength6Insrers ?
                        numberingHeader + app.Numbering.PadLeft(4, '0') :
                        app.Numbering.PadLeft(6, '0');

                    page = 1;
                }
                else if(app.MediYear == (int)APP_SPECIAL_CODE.不要)
                {
                    continue;
                }
                else if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
                {
                    page++;
                }
                else
                {
                    throw new Exception($"AID:[{app.Aid}] 不完全な入力が検出されました");
                }

                //画像保存
                var fileName = $"{imgDir}\\{yymm + revBatchNo + numbering + page.ToString("00")}.tif";
                fc.FileCopy(app.GetImageFullPath(), fileName);

                string bd = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                bd += DateTimeEx.GetJpYear(app.Birthday).ToString("00");
                bd += app.Birthday.ToString("MM");//. ToString("MMdd");

                //申請書以外はここで終了
                if (app.MediYear <= 0) continue;


                //20190509160409 furukawa st ////////////////////////
                //診療年月の取得修正

                //var sinryoYM = DateTimeEx.GetEraNumber(DateTime.Today).ToString() +
                //    app.MediYear.ToString("00") + app.MediMonth.ToString("00");
                

                string strADSinryoYM =DateTimeEx.GetAdYearFromHs(app.MediYear * 100 + int.Parse(app.MediMonth.ToString()) ) 
                                                    + app.MediMonth.ToString("00");
                int era = DateTimeEx.GetEraNumberYearFromYYYYMM(int.Parse(strADSinryoYM)) ;                
                string sinryoYM = era+ app.MediMonth.ToString("00");
                //20190509160409 furukawa ed ////////////////////////

                var kubun = "0";                        //仕様書で0固定
                var honke = app.Family.ToString();
                var pref = app.HihoPref.ToString();
                var kumiaiNo = app.HihoNum;
                var sex = app.Sex == 1 ? "1" : "2";
                var birth = bd;
                var days = app.CountedDays.ToString();
                var totalCost = app.Total.ToString();
                var seikyu = app.Charge.ToString();
                var yuyo = app.HihoType == (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予 ? "9" : string.Empty;
                var kigoubango = app.ClinicNum;

                var hosCode = app.DrNum;
                var futan = string.Empty;
                if (app.Partial != 0 && app.Total - app.Charge != app.Partial)
                    futan = app.Partial.ToString();     //患者負担額

                //鍼灸マッサージの際、バッチシートに登録番号が記載されている
                if (app.AppType== APP_TYPE.鍼灸 || app.AppType== APP_TYPE.あんま)
                {
                    hosCode = batchHosCode ?? throw new Exception("鍼灸マッサージの登録記号番号を判別できませんでした。");

                    //20210317121148 furukawa st ////////////////////////
                    //あはきバッチの登録記号番号が空欄の場合、所属する申請書も空欄とするルール                                        
                    //空でない場合は通常通り
                    hosCode = batchHosCode == string.Empty ? string.Empty : batchHosCode;
                    //20210317121148 furukawa ed ////////////////////////
                }
                //愛知、埼玉支部の時はバッチシートの番号優先
                else if(Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_23AICHI ||
                    Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_11SAITAMA)
                {
                    if (batchHosCode != string.Empty) hosCode = batchHosCode;
                }

                var res = new string[18];
                res[0] = writeYM;
                res[1] = revBatchNo;
                res[2] = numbering;
                res[3] = sinryoYM;
                res[4] = kubun;
                res[5] = honke;
                res[6] = string.Empty;
                res[7] = insurerNo;
                res[8] = kumiaiNo;
                res[9] = sex;
                res[10] = birth;
                res[11] = days;
                res[12] = totalCost;
                res[13] = seikyu;
                res[14] = yuyo;
                res[15] = hosCode;
                res[16] = bankNo;
                res[17] = futan;

                //CSV出力直前に再度ソートのため、ここでは書き込まないでリストに挿入のみに切り替え
                //try
                //{
                //    sw.WriteLine(string.Join(",", res));
                //}
                //catch (Exception ex)
                //{
                //    wf.LogPrint(ex.Message);
                //    Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                //    return false;
                //}
                csvLines.Add(res);
            }

            return true;
        }

        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            //バッチナンバー取得のため、バッチシート一覧を作成
            var batchList = App.GetAppsWithWhere($"WHERE a.cym={cym} AND a.aapptype={(int)APP_TYPE.バッチ}");
            batchList.Sort((x, y) => y.Aid.CompareTo(x.Aid));

            //バッチ取得
            App getBatch(int aid)
            {
                foreach (var b in batchList)
                {
                    if (b.Aid < aid) return b;
                }
                return null;
            }

            //団体名取得
            var accountDic = new Dictionary<string, string>();
            string getDantaiName(string code)
            {
                if (string.IsNullOrEmpty(code)) return string.Empty;
                if (!accountDic.ContainsKey(code))
                {
                    var account = Account.GetAccount(code);
                    if (account != null)
                    {
                        accountDic.Add(code, account.Name);
                        return account.Name;
                    }
                    else return string.Empty;
                }
                return accountDic[code];
            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(list.Count);
            wf.BarStyle = ProgressBarStyle.Continuous;
            wf.LogPrint("出力を開始します");
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,通知番号,保険者番号,処理年,処理月,バッチ,ナンバリング," +
                        "組合員番号,組合員氏名,受療者氏名,性別,年号,生年,月,日,郵便番号,送付先住所1,送付先住所2," +
                        "診療年,診療月,実日数,合計金額,請求金額,施術所番号,施術所名," +
                        "照会理由,過誤理由,再審査理由,返戻理由,詳細,団体名,口座番号,照会結果,メモ");
                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100,int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    var notifyNumber = $"{jcy}{jcm}-{Insurer.CurrrentInsurer.ViewIndex.ToString("00")}-";
                    var count = 1;
                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };
                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };
                    Func<string, string[]> toAddresses = address =>
                    {
                        var result = new string[] { "", "" };
                        if (!address.Contains("　"))
                        {
                            result[0] = address;
                        }
                        else
                        {
                            var index = address.IndexOf("　");
                            result[0] = address.Substring(0, index);
                            result[1] = address.Substring(index + 1);
                        }
                        return result;
                    };

                    foreach (var item in list)
                    {
                        var adds = toAddresses(item.HihoAdd);
                        var batch = getBatch(item.Aid);
                        var accountNumber = batch?.AccountNumber;

                        wf.LogPrint($"バッチ[{batch.Numbering}]－ナンバリング[{item.Numbering}]の出力中です…");

                        ss.Add(item.Aid.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(Insurer.CurrrentInsurer.InsNumber.ToString());
                        ss.Add(jcy);
                        ss.Add(jcm);
                        if (Common.IsBatchLength6Insrers)
                        {
                            ss.Add(batch?.Numbering?.Remove(4) ?? string.Empty);
                            ss.Add(batch?.Numbering?.Substring(4) ?? string.Empty + item.Numbering.ToString());
                        }
                        else
                        {
                            ss.Add(batch?.Numbering);
                            ss.Add(item.Numbering.ToString());
                        }
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.PersonName.ToString());
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(DateTimeEx.GetEra(item.Birthday));
                        ss.Add(toJYear(DateTimeEx.GetEraNumberYear(item.Birthday)));
                        ss.Add(item.Birthday.Month.ToString());
                        ss.Add(item.Birthday.Day.ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(adds[0]);
                        ss.Add(adds[1]);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add(item.HenreiReasonStr);
                        ss.Add("\"" + item.MemoInspect + "\"");
                        ss.Add(getDantaiName(accountNumber));
                        ss.Add(accountNumber);
                        ss.Add(item.ShokaiResultStr);
                        ss.Add("\"" + item.Memo + "\"");

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool MediMatchingExport(Insurer ins, int cym)
        {
            string fn;
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv";
                f.FileName = ins.InsurerName + " 医科突用" + cym.ToString() + ".csv";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fn = f.FileName;
            }

            var wfs = new WaitFormSimple();
            Task.Factory.StartNew(() => wfs.ShowDialog());
            var list = App.GetApps(cym);

            try
            {
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.UTF8))
                {
                    sw.WriteLine("診療年月,柔整シーケンス番号,レセプト区分,保険者番号,"
                        + "保険証記号,保険証番号,性別,生年月日");

                    var l = new List<string>();
                    foreach (var a in list)
                    {
                        if (a.MediYear < 0) continue;
                        var gym = "4" + a.MediYear.ToString("00") + a.MediMonth.ToString("00");
                        l.Add(gym);
                        l.Add(a.Aid.ToString());
                        l.Add(a.Family.ToString());
                        l.Add(ins.InsNumber);
                        l.Add("");
                        l.Add(a.HihoNum);
                        l.Add(a.Sex.ToString());
                        l.Add(DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday).ToString());

                        sw.WriteLine(string.Join(",", l));
                        l.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wfs.InvokeCloseDispose();
            }


            return true;
        }

        public static bool AnalysisExport(Insurer ins, int cym)
        {
            string fn;
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv";
                f.FileName = ins.InsurerName + " 分析用" + cym.ToString("000000") + ".csv";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fn = f.FileName;
            }

            var wfs = new WaitFormSimple();
            Task.Factory.StartNew(() => wfs.ShowDialog());

            var list = App.GetApps(cym);

            try
            {
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.UTF8))
                {
                    sw.WriteLine("処理年,処理月,診療年,診療月,被保番,家族区分,災害支払猶予,性別,生年月日," +
                        "実日数,合計,請求,負担,新規継続,柔整師登録番号,ナンバリング," +
                        "初検年,初検月,負傷1,負傷2,負傷3,負傷4,負傷5,部位数");
                    var l = new List<string>();
                    foreach (var a in list)
                    {
                        if (a.MediYear < 0) continue;
                        l.Add(a.ChargeYear.ToString());
                        l.Add(a.ChargeMonth.ToString());
                        l.Add(a.MediYear.ToString());
                        l.Add(a.MediMonth.ToString());
                        l.Add(a.HihoNum);
                        l.Add(a.Family.ToString());
                        l.Add(a.HihoType.ToString());
                        l.Add(a.Sex.ToString());
                        l.Add(a.Birthday.ToString("yyyy/MM/dd"));
                        l.Add(a.CountedDays.ToString());
                        l.Add(a.Total.ToString());
                        l.Add(a.Charge.ToString());
                        l.Add(a.Partial.ToString());
                        l.Add(((int)a.NewContType).ToString());
                        l.Add(a.DrNum);
                        l.Add(a.Numbering);

                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        l.Add(DateTimeEx.GetHsYearFromAd(a.FushoFirstDate1.Year, a.FushoFirstDate1.Month).ToString());
                        //l.Add(DateTimeEx.GetHsYearFromAd(a.FushoFirstDate1.Year).ToString());
                        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        l.Add(a.FushoFirstDate1.Month.ToString());
                        l.Add(a.FushoName1);
                        l.Add(a.FushoName2);
                        l.Add(a.FushoName3);
                        l.Add(a.FushoName4);
                        l.Add(a.FushoName5);
                        int b = 0;
                        if (!string.IsNullOrWhiteSpace(a.FushoName1)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName2)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName3)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName4)) b++;
                        if (!string.IsNullOrWhiteSpace(a.FushoName5)) b++;
                        l.Add(b.ToString());
                        sw.WriteLine(string.Join(",", l));
                        l.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wfs.InvokeCloseDispose();
            }

            return true;
        }

        public bool MediMatchingExportWithRESAndImage(Insurer ins, List<App> l)
        {
            string dir = "";
            using (var f = new FolderBrowserDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
                dir = f.SelectedPath;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                return DoExport(l, dir, false, wf);
            }
        }
    }
}