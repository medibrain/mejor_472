﻿namespace Mejor.ZenkokuGakko
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonNohin = new System.Windows.Forms.RadioButton();
            this.radioButtonBunseki = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSubmitAllCheck = new System.Windows.Forms.Button();
            this.checkBoxSubmitRer = new System.Windows.Forms.CheckBox();
            this.checkBoxSubmitHes = new System.Windows.Forms.CheckBox();
            this.checkBoxSubmitHesExcel = new System.Windows.Forms.CheckBox();
            this.checkBoxSubmitRes = new System.Windows.Forms.CheckBox();
            this.radioButtonMediMatching = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonNohin
            // 
            this.radioButtonNohin.AutoSize = true;
            this.radioButtonNohin.Checked = true;
            this.radioButtonNohin.Location = new System.Drawing.Point(35, 24);
            this.radioButtonNohin.Name = "radioButtonNohin";
            this.radioButtonNohin.Size = new System.Drawing.Size(155, 16);
            this.radioButtonNohin.TabIndex = 0;
            this.radioButtonNohin.TabStop = true;
            this.radioButtonNohin.Text = "納品データ　（出力先固定）";
            this.radioButtonNohin.UseVisualStyleBackColor = true;
            // 
            // radioButtonBunseki
            // 
            this.radioButtonBunseki.AutoSize = true;
            this.radioButtonBunseki.Location = new System.Drawing.Point(35, 217);
            this.radioButtonBunseki.Name = "radioButtonBunseki";
            this.radioButtonBunseki.Size = new System.Drawing.Size(87, 16);
            this.radioButtonBunseki.TabIndex = 1;
            this.radioButtonBunseki.Text = "分析用データ";
            this.radioButtonBunseki.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Tomato;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(385, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonSubmitAllCheck);
            this.groupBox1.Controls.Add(this.checkBoxSubmitRer);
            this.groupBox1.Controls.Add(this.checkBoxSubmitHes);
            this.groupBox1.Controls.Add(this.checkBoxSubmitHesExcel);
            this.groupBox1.Controls.Add(this.checkBoxSubmitRes);
            this.groupBox1.Location = new System.Drawing.Point(59, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 150);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力データを選択";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(269, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "※ファイル名と同じシートが対象";
            // 
            // buttonSubmitAllCheck
            // 
            this.buttonSubmitAllCheck.Location = new System.Drawing.Point(29, 113);
            this.buttonSubmitAllCheck.Name = "buttonSubmitAllCheck";
            this.buttonSubmitAllCheck.Size = new System.Drawing.Size(49, 23);
            this.buttonSubmitAllCheck.TabIndex = 5;
            this.buttonSubmitAllCheck.Text = "全選択";
            this.buttonSubmitAllCheck.UseVisualStyleBackColor = true;
            this.buttonSubmitAllCheck.Click += new System.EventHandler(this.buttonSubmitAllCheck_Click);
            // 
            // checkBoxSubmitRer
            // 
            this.checkBoxSubmitRer.AutoSize = true;
            this.checkBoxSubmitRer.Checked = true;
            this.checkBoxSubmitRer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitRer.Location = new System.Drawing.Point(29, 69);
            this.checkBoxSubmitRer.Name = "checkBoxSubmitRer";
            this.checkBoxSubmitRer.Size = new System.Drawing.Size(230, 16);
            this.checkBoxSubmitRer.TabIndex = 2;
            this.checkBoxSubmitRer.Text = "RER データ　（医科レセ全件データ）＋画像";
            this.checkBoxSubmitRer.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubmitHes
            // 
            this.checkBoxSubmitHes.AutoSize = true;
            this.checkBoxSubmitHes.Checked = true;
            this.checkBoxSubmitHes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitHes.Location = new System.Drawing.Point(29, 47);
            this.checkBoxSubmitHes.Name = "checkBoxSubmitHes";
            this.checkBoxSubmitHes.Size = new System.Drawing.Size(226, 16);
            this.checkBoxSubmitHes.TabIndex = 1;
            this.checkBoxSubmitHes.Text = "HES データ 直接出力（返戻対象者一覧）";
            this.checkBoxSubmitHes.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubmitHesExcel
            // 
            this.checkBoxSubmitHesExcel.AutoSize = true;
            this.checkBoxSubmitHesExcel.Location = new System.Drawing.Point(29, 91);
            this.checkBoxSubmitHesExcel.Name = "checkBoxSubmitHesExcel";
            this.checkBoxSubmitHesExcel.Size = new System.Drawing.Size(230, 16);
            this.checkBoxSubmitHesExcel.TabIndex = 3;
            this.checkBoxSubmitHesExcel.Text = "HES データ Excel変換（返戻対象者一覧）";
            this.checkBoxSubmitHesExcel.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubmitRes
            // 
            this.checkBoxSubmitRes.AutoSize = true;
            this.checkBoxSubmitRes.Checked = true;
            this.checkBoxSubmitRes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubmitRes.Location = new System.Drawing.Point(29, 25);
            this.checkBoxSubmitRes.Name = "checkBoxSubmitRes";
            this.checkBoxSubmitRes.Size = new System.Drawing.Size(234, 16);
            this.checkBoxSubmitRes.TabIndex = 0;
            this.checkBoxSubmitRes.Text = "RES データ　（柔整鍼灸全件データ）＋画像";
            this.checkBoxSubmitRes.UseVisualStyleBackColor = true;
            // 
            // radioButtonMediMatching
            // 
            this.radioButtonMediMatching.AutoSize = true;
            this.radioButtonMediMatching.Location = new System.Drawing.Point(35, 252);
            this.radioButtonMediMatching.Name = "radioButtonMediMatching";
            this.radioButtonMediMatching.Size = new System.Drawing.Size(111, 16);
            this.radioButtonMediMatching.TabIndex = 4;
            this.radioButtonMediMatching.Text = "医科突合用データ";
            this.radioButtonMediMatching.UseVisualStyleBackColor = true;
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 298);
            this.Controls.Add(this.radioButtonMediMatching);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.radioButtonBunseki);
            this.Controls.Add(this.radioButtonNohin);
            this.Name = "ExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonNohin;
        private System.Windows.Forms.RadioButton radioButtonBunseki;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonSubmitAllCheck;
        private System.Windows.Forms.CheckBox checkBoxSubmitRer;
        private System.Windows.Forms.CheckBox checkBoxSubmitHesExcel;
        private System.Windows.Forms.CheckBox checkBoxSubmitRes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonMediMatching;
        private System.Windows.Forms.CheckBox checkBoxSubmitHes;
    }
}