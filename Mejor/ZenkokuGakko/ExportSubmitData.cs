﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    class ExportSubmitData
    {
        private Insurer ins;
        private string defaultFolderPath;
        private string folderPath;
        private bool res = false;
        private bool hes = false;
        private bool rer = false;
        private bool hesExcel = false;

        private Pref pref;
        private int cym;

        private IWorkbook dataBK;
        private ISheet dataSH;

        private int hesCount;

        public ExportSubmitData(Insurer ins, string folderPath, int cym, bool res, bool hes, bool rer, bool hesExcel)
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            var fPath = folderPath;
            if (fPath.Contains("{ee}"))
            {
                var ee = month < 4 ? jYear - 1 : jYear;
                fPath = fPath.Replace("{ee}", ee.ToString("00"));
            }
            if (fPath.Contains("{eemm}"))
            {
                fPath = fPath.Replace("{eemm}", jYear.ToString("00") + month.ToString("00"));
            }
            var pid = Insurer.CurrrentInsurer.ViewIndex;
            var yyyyMM = cym.ToString();
            this.folderPath = $"{fPath}\\{yyyyMM}";
            defaultFolderPath = fPath;

            this.ins = ins;
            this.res = res;
            this.hes = hes;
            this.rer = rer;
            this.hesExcel = hesExcel;

            this.pref = Pref.GetPref(Insurer.CurrrentInsurer.ViewIndex);
            this.cym = cym;
        }

        /// <summary>
        /// 選択項目を出力します。
        /// </summary>
        /// <returns></returns>
        public bool Exportes()
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            hesCount = 0;
            try
            {
                if (!fileExistCheck()) return false;

                if (res && !dataExistCheck_RES()) return false;
                if (rer && !dataExistCheck_RER()) return false;
                if (hesExcel && !dataExistCheckAndLoad_HesExcel()) return false;

                var agree = MessageBox.Show(
                    $"【{Insurer.CurrrentInsurer.InsurerName}】{cym.ToString("0000年00月")}分\r\n" +
                    (res ? "　・RES ファイル＋画像\r\n" : "") +
                    (hesExcel ? $"　・HES ファイル（{hesCount}件）\r\n" : "") +
                    (rer ? "　・RER ファイル＋画像\r\n" : "") +
                    $"\r\n以上の出力を開始してもよろしいですか？\r\n\r\n\r\n保存先：\r\n{folderPath}",
                    "確認", MessageBoxButtons.OKCancel);
                if (agree != DialogResult.OK) return false;

                var wf = new WaitForm();
                try
                {
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("出力フォルダを作成します");
                    System.IO.Directory.CreateDirectory(folderPath);

                    wf.LogPrint("申請書データを取得しています");
                    var l = App.GetApps(cym);

                    if (res)
                    {
                        wf.LogPrint("RESファイルの出力を開始します");
                        if (!exportRES(l, true, wf))
                        {
                            MessageBox.Show("RESファイルの出力に失敗しました。", "エラー",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    if (hes)
                    {
                        wf.LogPrint("HESファイルの出力を開始します");
                        if (!exportHES(l, wf))
                        {
                            MessageBox.Show("HESファイルの出力に失敗しました。", "エラー",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    if (rer)
                    {
                        wf.LogPrint("RERファイルの出力を開始します");
                        if (!exportRER())
                        {
                            MessageBox.Show("RERファイルの出力に失敗しました。", "エラー",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    if (hesExcel)
                    {
                        wf.LogPrint("HES Excelファイルの変換を開始します");
                        if (!exportHES_Excel())
                        {
                            MessageBox.Show("HESファイルの変換に失敗しました。", "エラー",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    var rootFolderPath = System.IO.Path.GetDirectoryName(defaultFolderPath);    //１階層上がルートフォルダ（eemm）
                    var excelFolderPath = $"{rootFolderPath}\\各種一覧表";
                    if (!System.IO.Directory.Exists(excelFolderPath))
                    {
                        wf.LogPrint("各種一覧表フォルダの作成を開始します。");
                        System.IO.Directory.CreateDirectory(excelFolderPath);   //Excelファイル格納フォルダの作成
                    }
                    else
                    {
                        wf.LogPrint("各種一覧表フォルダは既に存在します。");
                    }
                }
                catch(Exception ex)
                {
                    wf.LogPrint(ex.Message);
                }
                finally
                {
                    //このログはなんの役にも立ってないので削除予定2021/11/16
                    //wf.LogSave($"{folderPath}\\Export{DateTime.Now.ToString("yyyyMMddHHmmss")}.log" );
                    wf.Dispose();
                }

                return true;
            }
            finally
            {
                if (dataBK != null)
                {
                    dataBK.Close();
                    dataBK = null;
                    dataSH = null;
                }
            }            
        }

        private bool fileExistCheck()
        {
            if (System.IO.Directory.Exists(folderPath))
            {
                foreach (var f in System.IO.Directory.GetFiles(folderPath))
                {
                    var fn = System.IO.Path.GetFileName(f);
                    if (res && fn.StartsWith("RES"))
                    {
                        MessageBox.Show($"RESファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if ((hes || hesExcel) && fn.StartsWith("HES"))
                    {
                        MessageBox.Show($"HESファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    if (rer && fn.StartsWith("RER"))
                    {
                        MessageBox.Show($"RERファイルがすでに存在しています。\r\n\r\n{f}", "出力失敗", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }

            return true;            
        }

        /// <summary>
        /// RESデータと画像を出力します
        /// </summary>
        /// <returns></returns>
        private bool exportRES(List<App> l, bool inBatchFolder, WaitForm wf)
        {
            var e = new ExportRev();
            return e.DoExport(l, folderPath, inBatchFolder, wf);
        }

        private bool exportHES(List<App> l, WaitForm wf)
        {
            string reason(App a)
            {
                var rs = new List<string>();

          
                
                //20201011150737 furukawa st ////////////////////////
                //過誤フラグ追加に伴ってxml列に変更
                
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.原因なし)) rs.Add("負傷原因なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし)) rs.Add("長期理由なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違)) rs.Add("負傷原因相違");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.署名違い)) rs.Add("署名違い");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.筆跡違い)) rs.Add("筆跡違い");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.家族同一筆跡)) rs.Add("家族同一筆跡");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.その他)) rs.Add(a.MemoInspect);


                //if (a.KagoReasonsCheck(KagoReasons.原因なし)) rs.Add("負傷原因なし");
                //if (a.KagoReasonsCheck(KagoReasons.長期理由なし)) rs.Add("長期理由なし");
                //if (a.KagoReasonsCheck(KagoReasons.負傷原因相違)) rs.Add("負傷原因相違");
                //if (a.KagoReasonsCheck(KagoReasons.署名違い)) rs.Add("署名違い");
                //if (a.KagoReasonsCheck(KagoReasons.筆跡違い)) rs.Add("筆跡違い");
                //if (a.KagoReasonsCheck(KagoReasons.家族同一筆跡)) rs.Add("家族同一筆跡");
                //if (a.KagoReasonsCheck(KagoReasons.その他)) rs.Add(a.MemoInspect);


                //20201011150737 furukawa ed ////////////////////////


                //20201028095154 furukawa st ////////////////////////
                //追加フラグの文言追加

                if (a.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違)) rs.Add("長期理由相違");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.療養費請求権の消滅時効)) rs.Add("療養費請求権の消滅時効");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.本家区分誤り)) rs.Add("本家区分誤り");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.保険者相違)) rs.Add("保険者相違");


                
                //16km以上単独は無い
                //if (a.KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上)) rs.Add("往療理由記載なし(16km以上)");

                if ((a.KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし)) &&
                    (a.KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上))) rs.Add("往療理由記載なし(16km以上)");
                else if (a.KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし)) rs.Add("往療理由記載なし");



                if (a.KagoReasonsCheck_xml(KagoReasons_Member.同意書添付なし)) rs.Add("同意書添付なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.同意書期限切れ)) rs.Add("同意書記載不備");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書添付なし)) rs.Add("施術報告書添付なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書記載不備)) rs.Add("施術報告書記載不備");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書添付なし)) rs.Add("施術継続理由・状態記入書添付なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書記載不備)) rs.Add("施術継続理由・状態記入書記載不備");
                //20201028095154 furukawa ed ////////////////////////


                //20210423173917 furukawa st ////////////////////////
                //往療内訳書追加

                //20210510214339 furukawa st ////////////////////////
                //文言が重複し、メモ欄を無駄に食うので不要
                //if (a.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書)) rs.Add("往療内訳書");
                //20210510214339 furukawa ed ////////////////////////


                if (a.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書添付なし)) rs.Add("往療内訳書添付なし");
                if (a.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書記載不備)) rs.Add("往療内訳書記載不備");
                //20210423173917 furukawa ed ////////////////////////



                if (a.HenreiReasonsCheck(HenreiReasons.負傷部位)) rs.Add("負傷部位");
                if (a.HenreiReasonsCheck(HenreiReasons.負傷原因)) rs.Add("負傷原因");
                if (a.HenreiReasonsCheck(HenreiReasons.負傷時期)) rs.Add("負傷時期");
                if (a.HenreiReasonsCheck(HenreiReasons.けが外)) rs.Add("けが以外");
                if (a.HenreiReasonsCheck(HenreiReasons.受療日数)) rs.Add("受療日数");


                if (a.HenreiReasonsCheck(HenreiReasons.その他) && rs.Count == 0) return a.MemoInspect;

                return string.Join(" ", rs); }

            var hesList = l.FindAll(a => a.StatusFlagCheck(StatusFlag.返戻));

            var cdt = DateTimeEx.ToDateTime6(cym);

            string chargeYM = DateTimeEx.GetEraNumber(cdt).ToString()
                + DateTimeEx.GetJpYear(cdt).ToString("00") + (cym % 100).ToString("00");
            var hesFileName = $"{folderPath}\\HES{DateTime.Today.ToString("yyMMdd")}.csv";
            var sw = new System.IO.StreamWriter(hesFileName, false, Encoding.GetEncoding("Shift_JIS"));
            try
            {
                foreach (var item in hesList)
                {
                    var line = new string[4];
                    var batch = BatchSelector.GetBatchApp(Insurer.CurrrentInsurer, item.Aid);
                    string hesBatch, hesNumbering;
                    if (Common.IsBatchLength6Insrers)
                    {
                        hesBatch = batch.Numbering.Remove(4);
                        hesNumbering = batch.Numbering.Substring(4, 2) + item.Numbering.PadLeft(4, '0');
                    }
                    else
                    {
                        hesBatch = batch.Numbering.PadLeft(4, '0');
                        hesNumbering = item.Numbering;
                    }

                    line[0] = chargeYM;
                    line[1] = hesBatch;
                    line[2] = hesNumbering;
                    line[3] = reason(item);

                    //20211116145053 furukawa st ////////////////////////
                    //25文字以上の文字列は25文字にカット
                    
                    if ((line[3] != null) && (line[3].Length > 25)) line[3] = line[3].Substring(0, 25);
                    else line[3] = reason(item);
                    //20211116145053 furukawa ed ////////////////////////


                    sw.WriteLine(string.Join(",", line));
                }


                //20190509162024 furukawa st ////////////////////////
                //返戻対象者一覧CSV不要


                //CSV出力
                //wf.LogPrint("返戻対象者一覧CSVを出力します");
                //var csvName = $"{folderPath}\\{Insurer.CurrrentInsurer.InsurerName}_{cym}返戻対象者一覧表.csv";
                //ExportRev.ListExport(hesList, csvName, cym);


                //20190509162024 furukawa ed ////////////////////////

            }
            catch (Exception ex)
            {
                wf.LogPrint(ex.Message);
                return false;
            }
            finally
            {
                sw.Dispose();
            }

            return true;
        }

        /// <summary>
        /// Excelデータを元にHESデータを出力します
        /// </summary>
        /// <returns></returns>
        private bool exportHES_Excel()
        {
            var wf = new WaitForm();
            try
            {
                wf.Max = dataSH.LastRowNum;
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.LogPrint("HESファイルを出力しています…");

                if (!System.IO.Directory.Exists(folderPath)) System.IO.Directory.CreateDirectory(folderPath);

                var outputHESPath = $"{folderPath}\\HES{DateTime.Today.ToString("yyMMdd")}.csv";
                using (var sw = new System.IO.StreamWriter(outputHESPath, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    var csv = new List<string>();

                    Func<ICell, string> getVal = cell =>
                    {
                        if (cell == null) return string.Empty;
                        if (cell.CellType == CellType.String) return cell.StringCellValue;
                        else if (cell.CellType == CellType.Numeric) return cell.NumericCellValue.ToString();
                        return string.Empty;
                    };
                    Func<string, string> toInsertVal = val =>
                    {
                        if (string.IsNullOrWhiteSpace(val)) return "";
                        return val;
                    };
                    Func<string, string, string, string> toInsertVals = (val1, val2, val3) =>
                    {
                        var result = "";
                        if (!string.IsNullOrWhiteSpace(val1)) result += val1;
                        if (!string.IsNullOrWhiteSpace(val2)) result += "：" + val2;
                        if (!string.IsNullOrWhiteSpace(val3)) result += "：" + val3;
                        return result;
                    };

                    for (var i = 1; i <= dataSH.LastRowNum; i++)//ヘッダは除く
                    {
                        var row = dataSH.GetRow(i);
                        if (row == null)
                        {
                            wf.InvokeValue++;
                            continue;//パス
                        }
                        var valEE = getVal(row.GetCell(3));
                        var valMM = getVal(row.GetCell(4));
                        var valBatch = getVal(row.GetCell(5));
                        var valNumbering = getVal(row.GetCell(6));
                        if(string.IsNullOrWhiteSpace(valEE)
                            && string.IsNullOrWhiteSpace(valMM)
                            && string.IsNullOrWhiteSpace(valBatch)
                            && string.IsNullOrWhiteSpace(valNumbering))
                        {
                            wf.InvokeValue++;
                            continue;//空白行はパス（書式だけ残ってる可能性大）
                        }
                        var cellHenrei1 = row.GetCell(25);
                        var valHenrei1 = "";
                        var valHenrei2 = "";
                        var valHenrei3 = "";
                        if (cellHenrei1.IsMergedCell)//結合セル
                        {
                            valHenrei1 = getVal(row.GetCell(25));
                        }
                        else
                        {
                            valHenrei1 = getVal(row.GetCell(25));
                            valHenrei2 = getVal(row.GetCell(26));
                            valHenrei3 = getVal(row.GetCell(27));
                        }

                        csv.Add($"4{valEE.PadLeft(2, '0')}{valMM.PadLeft(2, '0')}");//平成＝４
                        csv.Add(toInsertVal(valBatch));
                        csv.Add(toInsertVal(valNumbering.PadLeft(6, '0')));//6桁合わせ
                        csv.Add(toInsertVals(valHenrei1, valHenrei2, valHenrei3));

                        sw.WriteLine(string.Join(",", csv));
                        csv.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                wf.LogPrint("HESファイルの出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            wf.LogPrint("HESファイルを正常に出力しました");
            return true;
        }

        /// <summary>
        /// RERデータと画像を出力します
        /// </summary>
        /// <returns></returns>
        private bool exportRER()
        {
            if (pref == null) return false;
            var dbname = "zenkoku_gakko";
            var currDBName = DB.GetMainDBName();
            var change = currDBName != dbname;
            try
            {
                if (change) DB.SetMainDBName(dbname);
                return ExportMediApp.DoExport(cym, pref, folderPath, true);
            }
            finally
            {
                if (change) DB.SetMainDBName(currDBName);
            }            
        }

        private bool dataExistCheck_RES()
        {
            var list = Scan.GetScanListYM(cym);
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("RESファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool dataExistCheckAndLoad_HesExcel()
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            int yyMM = jYear * 100 + month;

            //出力先から4階層戻る
            var hesPath = this.folderPath;
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            hesPath = FileUtility.GetParentDirectoryPath(hesPath);
            //識別子取得
            var tmps = FileUtility.GetDirectoryName(hesPath).Replace("支部", string.Empty).Split(' ');
            if (tmps.Count() != 3) return false;
            var identifier = $"{tmps[0]} {tmps[1]}{tmps[2]}";
            //合体
            hesPath += $"\\{identifier} 作業フォルダ\\{identifier} {yyMM}作業フォルダ\\提出用\\{pref.ID.ToString("00")}{pref.Name2}_{yyMM}返戻対象者一覧表.xlsx";

            if (!System.IO.File.Exists(hesPath))
            {
                MessageBox.Show($"HESファイルの作成元Excelファイルがありません。\r\n\r\n{hesPath}", "エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!readExcel(hesPath))
            {
                MessageBox.Show("HESファイルの作成元Excelファイルの取得に失敗しました。\r\n" +
                    "該当のExcelファイルを開いている場合は閉じてください。\r\n" +
                    "また対象となるシート名はファイル名と同じである必要があります。", 
                    "エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dataSH.LastRowNum == 0)//ヘッダのみ
            {
                MessageBox.Show("HESファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            //件数の取得
            Func<ICell, string> getVal = cell =>
            {
                if (cell == null) return string.Empty;
                if (cell.CellType == CellType.String) return cell.StringCellValue;
                else if (cell.CellType == CellType.Numeric) return cell.NumericCellValue.ToString();
                return string.Empty;
            };
            var count = 0;
            for (var i = 1; i <= dataSH.LastRowNum; i++)//ヘッダは除く
            {
                var row = dataSH.GetRow(i);
                if (row == null)
                {
                    continue;//パス
                }
                var valEE = getVal(row.GetCell(3));
                var valMM = getVal(row.GetCell(4));
                var valBatch = getVal(row.GetCell(5));
                var valNumbering = getVal(row.GetCell(6));
                if (string.IsNullOrWhiteSpace(valEE)
                    && string.IsNullOrWhiteSpace(valMM)
                    && string.IsNullOrWhiteSpace(valBatch)
                    && string.IsNullOrWhiteSpace(valNumbering))
                {
                    continue;//空白行はパス（書式だけ残ってる可能性大）
                }
                count++;
            }
            hesCount = count;
            return true;
        }

        private bool dataExistCheck_RER()
        {
            var list = MediApp.GetMediAppsByPIDIYM(cym, pref.ID);
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("RERファイルの出力に必要なデータがありません", "出力失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// エクセルファイルを取得します。
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool readExcel(string path)
        {
            try
            {
                //ブック読み込み
                dataBK = WorkbookFactory.Create(path);
                //シート取得（ファイル名と同じシート名のもの）
                dataSH = dataBK.GetSheet(System.IO.Path.GetFileNameWithoutExtension(path));
                if (dataSH == null) return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
