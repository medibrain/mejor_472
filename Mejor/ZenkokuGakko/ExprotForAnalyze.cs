﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    //20210312155319 furukawa ////////////////////////
    //分析データ出力用フォーム
    

    public partial class ExprotForAnalyze : Form
    {
        
        /// <summary>
        /// 全支部結合Appリスト
        /// </summary>
        List<App> lstAppTotal = new List<App>();
        

        /// <summary>
        /// 元のDB名
        /// </summary>
        string strDBNameOrg = DB.GetMainDBName();

        /// <summary>
        /// 口座情報リスト
        /// </summary>
        List<Account> lstAccount = new List<Account>();

        /// <summary>
        /// バッチ用紙リスト
        /// </summary>
        List<App> batchList = new List<App>();

        int cym = 0;

        public ExprotForAnalyze(int _cym)
        {
            InitializeComponent();
            CommonTool.setFormColor(this);
            cym = _cym;

 
            eventsetting();

            userControlSearchAnalyze.SetCym(cym);

        }



        private void buttonExport_Click(object sender, EventArgs e)
        {
            Export();            
        }


        /// <summary>
        /// 出力処理
        /// </summary>
        /// <returns></returns>
        private void Export()
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            lstAppTotal.Clear();

            try
            {
                //検索条件取得 →app取得
                if(!SelectedInsurer(wf)) return ;
                wf.Dispose();
                
                //リスト出力画面と同じ出力設定画面
                ListCreate.ListExportForm frm = new ListCreate.ListExportForm(lstAppTotal);
                frm.ShowDialog();

                return ;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return ;
            }
            finally
            {
                DB.SetMainDBName(strDBNameOrg);
                if(wf!=null)wf.Dispose();
            }
        }

        /// <summary>
        /// 直接出力してもいいが、フォルダが分からんので自分で決めて貰う　ので、未使用
        /// </summary>        
        private void ExportFile()
        {
            DB db = new DB("jyusei");
            
            var l = new List<ListCreate.ListExportSetting>();
            var id = Insurer.CurrrentInsurer.InsurerID;
            l.Add(new ListCreate.ListExportSetting() { Name = "従来CSV出力" });
            l.AddRange(ListCreate.ListExportSetting.GetAllSetting(db, id));
            var s = l[12];

            var fn = string.Empty;
            var ex = (s.ExportType == "UTF8" || s.ExportType == "SJIS") ? ".csv" : ".xlsx";

            using (var f = new SaveFileDialog())
            {
                f.Filter="csv|*.csv";
                f.FileName = $"{s.FileName}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}{ex}";
                if (f.ShowDialog() != DialogResult.OK) return;
                fn = f.FileName;
            }

            if (!ListCreate.ListExport.Export(lstAppTotal, fn, s))
            {
                MessageBox.Show("出力に失敗しました");
                return;
            }
            MessageBox.Show("出力が完了しました");
            this.Close();
        }



        /// <summary>
        /// 保険者ごとのAPP取得
        /// </summary>
        private bool SelectedInsurer(WaitForm wf)
        {
            bool flgChecked = false;
            foreach(CheckBox c in panel1.Controls)
            {
                if (c.Checked)
                {
                    flgChecked = true;
                    break;
                }
            }
            if (!flgChecked)
            {
                MessageBox.Show("保険者を選択してください");
                return false;
            }

            if (checkBox1.Checked) if(!GetInsurerApps(InsurerID.GAKKO_01HOKKAIDO,wf)) return false;
            if (checkBox2.Checked) if(!GetInsurerApps(InsurerID.GAKKO_02AOMORI,wf)) return false;
            if (checkBox3.Checked) if(!GetInsurerApps(InsurerID.GAKKO_03IWATE,wf)) return false;
            if (checkBox4.Checked) if(!GetInsurerApps(InsurerID.GAKKO_04MIYAZAKI,wf)) return false;
            if (checkBox5.Checked) if(!GetInsurerApps(InsurerID.GAKKO_05AKITA,wf)) return false;
            if (checkBox6.Checked) if(!GetInsurerApps(InsurerID.GAKKO_06YAMAGATA,wf)) return false;
            if (checkBox7.Checked) if(!GetInsurerApps(InsurerID.GAKKO_07FUKUSHIMA,wf)) return false;
            if (checkBox8.Checked) if(!GetInsurerApps(InsurerID.GAKKO_08IBARAGI,wf)) return false;
            if (checkBox9.Checked) if(!GetInsurerApps(InsurerID.GAKKO_09TOCHIGI,wf)) return false;
            if (checkBox10.Checked) if(!GetInsurerApps(InsurerID.GAKKO_10GUNMA,wf)) return false;
            if (checkBox11.Checked) if(!GetInsurerApps(InsurerID.GAKKO_11SAITAMA,wf)) return false;
            if (checkBox12.Checked) if(!GetInsurerApps(InsurerID.GAKKO_12CHIBA,wf)) return false;
            if (checkBox13.Checked) if(!GetInsurerApps(InsurerID.GAKKO_13TOKYO,wf)) return false;
            if (checkBox14.Checked) if(!GetInsurerApps(InsurerID.GAKKO_14KANAGAWA,wf)) return false;
            if (checkBox15.Checked) if(!GetInsurerApps(InsurerID.GAKKO_15NIGATA,wf)) return false;
            if (checkBox16.Checked) if(!GetInsurerApps(InsurerID.GAKKO_16TOYAMA,wf)) return false;
            if (checkBox17.Checked) if(!GetInsurerApps(InsurerID.GAKKO_17ISHIKAWA,wf)) return false;
            if (checkBox18.Checked) if(!GetInsurerApps(InsurerID.GAKKO_18FUKUI,wf)) return false;
            if (checkBox19.Checked) if(!GetInsurerApps(InsurerID.GAKKO_19YAMANASHI,wf)) return false;
            if (checkBox20.Checked) if(!GetInsurerApps(InsurerID.GAKKO_20NAGANO,wf)) return false;
            if (checkBox21.Checked) if(!GetInsurerApps(InsurerID.GAKKO_21GIFU,wf)) return false;
            if (checkBox22.Checked) if(!GetInsurerApps(InsurerID.GAKKO_22SHIZUOKA,wf)) return false;
            if (checkBox23.Checked) if(!GetInsurerApps(InsurerID.GAKKO_23AICHI,wf)) return false;
            if (checkBox24.Checked) if(!GetInsurerApps(InsurerID.GAKKO_24MIE,wf)) return false;
            if (checkBox25.Checked) if(!GetInsurerApps(InsurerID.GAKKO_25SHIGA,wf)) return false;
            if (checkBox26.Checked) if(!GetInsurerApps(InsurerID.GAKKO_26KYOTO,wf)) return false;
            if (checkBox27.Checked) if(!GetInsurerApps(InsurerID.OSAKA_GAKKO,wf)) return false;
            if (checkBox28.Checked) if(!GetInsurerApps(InsurerID.GAKKO_28HYOGO,wf)) return false;
            if (checkBox29.Checked) if(!GetInsurerApps(InsurerID.GAKKO_29NARA,wf)) return false;
            if (checkBox30.Checked) if(!GetInsurerApps(InsurerID.GAKKO_30WAKAYAMA,wf)) return false;
            if (checkBox31.Checked) if(!GetInsurerApps(InsurerID.GAKKO_31TOTTORI,wf)) return false;
            if (checkBox32.Checked) if(!GetInsurerApps(InsurerID.GAKKO_32SHIMANE,wf)) return false;
            if (checkBox33.Checked) if(!GetInsurerApps(InsurerID.GAKKO_33OKAYAMA,wf)) return false;
            if (checkBox34.Checked) if(!GetInsurerApps(InsurerID.GAKKO_34HIROSHIMA,wf)) return false;
            if (checkBox35.Checked) if(!GetInsurerApps(InsurerID.GAKKO_35YAMAGUCHI,wf)) return false;
            if (checkBox36.Checked) if(!GetInsurerApps(InsurerID.GAKKO_36TOKUSHIMA,wf)) return false;
            if (checkBox37.Checked) if(!GetInsurerApps(InsurerID.GAKKO_37KAGAWA,wf)) return false;
            if (checkBox38.Checked) if(!GetInsurerApps(InsurerID.GAKKO_38EHIME,wf)) return false;
            if (checkBox39.Checked) if(!GetInsurerApps(InsurerID.GAKKO_39KOCHI,wf)) return false;
            if (checkBox40.Checked) if(!GetInsurerApps(InsurerID.GAKKO_40FUKUOKA,wf)) return false;
            if (checkBox41.Checked) if(!GetInsurerApps(InsurerID.GAKKO_41SAGA,wf)) return false;
            if (checkBox42.Checked) if(!GetInsurerApps(InsurerID.GAKKO_42NAGASAKI,wf)) return false;
            if (checkBox43.Checked) if(!GetInsurerApps(InsurerID.GAKKO_43KUMAMOTO,wf)) return false;
            if (checkBox44.Checked) if(!GetInsurerApps(InsurerID.GAKKO_44OITA,wf)) return false;
            if (checkBox45.Checked) if(!GetInsurerApps(InsurerID.GAKKO_45MIYAZAKI,wf)) return false;
            if (checkBox46.Checked) if(!GetInsurerApps(InsurerID.GAKKO_46KAGOSHIMA,wf)) return false;
            if (checkBox47.Checked) if(!GetInsurerApps(InsurerID.GAKKO_47OKINAWA,wf)) return false;

            return true;
        }

        /// <summary>
        /// チェックを付けたら背景色変えないと見落とす
        /// </summary>
        private void eventsetting()
        {
            checkBox1.CheckedChanged += new EventHandler(backcolor);
            checkBox2.CheckedChanged += new EventHandler(backcolor);
            checkBox3.CheckedChanged += new EventHandler(backcolor);
            checkBox4.CheckedChanged += new EventHandler(backcolor);
            checkBox5.CheckedChanged += new EventHandler(backcolor);
            checkBox6.CheckedChanged += new EventHandler(backcolor);
            checkBox7.CheckedChanged += new EventHandler(backcolor);
            checkBox8.CheckedChanged += new EventHandler(backcolor);
            checkBox9.CheckedChanged += new EventHandler(backcolor);
            checkBox10.CheckedChanged += new EventHandler(backcolor);
            checkBox11.CheckedChanged += new EventHandler(backcolor);
            checkBox12.CheckedChanged += new EventHandler(backcolor);
            checkBox13.CheckedChanged += new EventHandler(backcolor);
            checkBox14.CheckedChanged += new EventHandler(backcolor);
            checkBox15.CheckedChanged += new EventHandler(backcolor);
            checkBox16.CheckedChanged += new EventHandler(backcolor);
            checkBox17.CheckedChanged += new EventHandler(backcolor);
            checkBox18.CheckedChanged += new EventHandler(backcolor);
            checkBox19.CheckedChanged += new EventHandler(backcolor);
            checkBox20.CheckedChanged += new EventHandler(backcolor);
            checkBox21.CheckedChanged += new EventHandler(backcolor);
            checkBox22.CheckedChanged += new EventHandler(backcolor);
            checkBox23.CheckedChanged += new EventHandler(backcolor);
            checkBox24.CheckedChanged += new EventHandler(backcolor);
            checkBox25.CheckedChanged += new EventHandler(backcolor);
            checkBox26.CheckedChanged += new EventHandler(backcolor);
            checkBox27.CheckedChanged += new EventHandler(backcolor);
            checkBox28.CheckedChanged += new EventHandler(backcolor);
            checkBox29.CheckedChanged += new EventHandler(backcolor);
            checkBox30.CheckedChanged += new EventHandler(backcolor);
            checkBox31.CheckedChanged += new EventHandler(backcolor);
            checkBox32.CheckedChanged += new EventHandler(backcolor);
            checkBox33.CheckedChanged += new EventHandler(backcolor);
            checkBox34.CheckedChanged += new EventHandler(backcolor);
            checkBox35.CheckedChanged += new EventHandler(backcolor);
            checkBox36.CheckedChanged += new EventHandler(backcolor);
            checkBox37.CheckedChanged += new EventHandler(backcolor);
            checkBox38.CheckedChanged += new EventHandler(backcolor);
            checkBox39.CheckedChanged += new EventHandler(backcolor);
            checkBox40.CheckedChanged += new EventHandler(backcolor);
            checkBox41.CheckedChanged += new EventHandler(backcolor);
            checkBox42.CheckedChanged += new EventHandler(backcolor);
            checkBox43.CheckedChanged += new EventHandler(backcolor);
            checkBox44.CheckedChanged += new EventHandler(backcolor);
            checkBox45.CheckedChanged += new EventHandler(backcolor);
            checkBox46.CheckedChanged += new EventHandler(backcolor);
            checkBox47.CheckedChanged += new EventHandler(backcolor);

        }

        private void backcolor(object sender,EventArgs e)
        {
            CheckBox c = (CheckBox)sender;
            if (c.Checked) c.BackColor = Color.Orange;
            else c.BackColor = this.BackColor;
        }




        
        /// <summary>
        /// バッチ取得
        /// </summary>
        /// <param name="ins"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool getBatchList(InsurerID ins,WaitForm wf)
        {
            try
            {
                //呼び出し元で変更しているので不要かと思ったが、
                //Insurer.CurrrentInsurer = Insurer.GetInsurer((int)ins);
             //   DB.SetMainDBName(Insurer.CurrrentInsurer.dbName);
                wf.LogPrint($"{Insurer.CurrrentInsurer.InsurerName} バッチ取得中");

                if (batchList.Count > 0) batchList.Clear();

                int from = 0;
                int to = 0;
                userControlSearchAnalyze.GetCym(out from, out to);

                var sql = $"select aid,baccnumber,baccname FROM application AS a WHERE a.cym between {from} and {to} AND ym={(int)APP_SPECIAL_CODE.バッチ}";
                DB.Command cmd = DB.Main.CreateCmd(sql.ToString());
                List<Object[]> lst = cmd.TryExecuteReaderList();
                for (int r = 0; r < lst.Count; r++)
                {
                    App a = new App();
                    a.Aid = int.Parse(lst[r][0].ToString());
                    a.AccountNumber = lst[r][1].ToString();
                    a.AccountName = lst[r][2].ToString();
                    batchList.Add(a);

                }


                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }

        
        /// <summary>
        /// 口座リスト取得
        /// </summary>
        /// <param name="ins"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private bool getAccountList(InsurerID ins,WaitForm wf)
        {
            try
            {
             //   Insurer.CurrrentInsurer = Insurer.GetInsurer((int)ins);
           //     DB.SetMainDBName(Insurer.CurrrentInsurer.dbName);
                wf.LogPrint($"{Insurer.CurrrentInsurer.InsurerName} 口座取得中");

                if (lstAccount.Count > 0) lstAccount.Clear();

                var sql = $"select * FROM account order by code";
                DB.Command cmd = DB.Main.CreateCmd(sql.ToString());
                List<Object[]> lst = cmd.TryExecuteReaderList();
                for (int r = 0; r < lst.Count; r++)
                {
                    Account a = new Account();
                    a.Code = lst[r][0].ToString();
                    a.Name = lst[r][1].ToString();

                    lstAccount.Add(a);

                }
                
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 団体名取得
        /// </summary>
        /// <param name="strCode"></param>
        /// <returns></returns>
        private string getDantaiName(string strCode)
        {
            string strRes = string.Empty;
            for(int r=0;r<lstAccount.Count;r++)
            {
                if (lstAccount[r].Code == strCode) return lstAccount[r].Name;
            }
            return strRes;
        }



        /// <summary>
        /// 保険者の申請書リストを抽出し、全体リストに格納
        /// </summary>
        /// <param name="ins">Insurerクラス</param>
        /// <param name="wf">waitform</param>
        /// <returns></returns>
        private bool GetInsurerApps(InsurerID ins,WaitForm wf)
        {
            Insurer.CurrrentInsurer = Insurer.GetInsurer((int)ins);

            try
            {
                
                DB.SetMainDBName(Insurer.CurrrentInsurer.dbName);

                if (!getBatchList(ins, wf)) return false;
                batchList.Sort((x, y) => y.Aid.CompareTo(x.Aid));
                if (!getAccountList(ins, wf)) return false;


                wf.LogPrint($"{Insurer.CurrrentInsurer.InsurerName} 抽出中");

                List<App> lstApp = new List<App>();

                //条件無しの場合エラーメッセージだけで終わり、判定ができないので例外になる。それを回避するtry
                try
                {
                    //申請書取得
                    lstApp = userControlSearchAnalyze.GetApps();
                }
                catch (Exception ex) { return false; }

                
                //バッチ取得
                App getBatch(int aid)
                {
                    foreach (var b in batchList)
                    {
                        if (b.Aid < aid) return b;
                    }
                    return null;
                }
                
                lstApp.Sort((x, y) => x.Aid.CompareTo(x.Aid));



                foreach (App a in lstApp)
                {
                    if (CommonTool.WaitFormCancelProcess(wf)) return false;
                    App b=getBatch(a.Aid);//所属するバッチを取得
                    a.AccountNumber = b.AccountNumber;//取得した口座番号を設定
                    a.AccountName=getDantaiName(b.AccountNumber);//取得した口座番号から団体名を取得

                    a.InsNum = Insurer.CurrrentInsurer.InsNumber.ToString();
                    
                    lstAppTotal.Add(a);
                }
                wf.LogPrint($"{Insurer.CurrrentInsurer.InsurerName} {lstApp.Count}件");

                DB.SetMainDBName(strDBNameOrg);

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    Insurer.GetInsurerName((int)ins) + "\r\n" + ex.Message);
                return false;
            }

        }




        /// <summary>
        /// プラス1ヶ月する保険者
        /// </summary>
        string[] strPlus = { "09栃木", "10群馬", "22静岡", "24三重", "25滋賀", "27大阪", "28兵庫", "32島根", "40福岡", "41佐賀", "43熊本", "47沖縄" };

        private void checkBoxAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach(CheckBox c in panel1.Controls)
            {
                c.Checked = checkBoxAll.Checked;
            }
        }

        private void checkBoxPlus_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckBox c in panel1.Controls)
            {
                if (strPlus.Contains(c.Text)) c.Checked = checkBoxPlus.Checked;
            }
        }

        private void checkBoxNoPlus_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckBox c in panel1.Controls)
            {
                if (!strPlus.Contains(c.Text)) c.Checked = checkBoxNoPlus.Checked;
            }
        }
    }
}

