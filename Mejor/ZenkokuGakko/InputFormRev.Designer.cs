﻿using System;

namespace Mejor.ZenkokuGakko
{
    partial class InputFormRev
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelFamily = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.verifyBoxFushoCount = new Mejor.VerifyBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.verifyBoxFutan = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelBatch = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxBatch = new Mejor.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.labelShinkyuDr = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.verifyBoxShinkyuDr = new Mejor.VerifyBox();
            this.verifyBoxCount = new Mejor.VerifyBox();
            this.labelDrDescription = new System.Windows.Forms.Label();
            this.verifyBoxBank = new Mejor.VerifyBox();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.verifyCheckBoxSai = new Mejor.VerifyCheckBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.panelBatch.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(566, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 10;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 32);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(182, 32);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(180, 6);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 4;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(337, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 24);
            this.label6.TabIndex = 8;
            this.label6.Text = "本人:2 六歳:4\r\n家族:6 高齢:8";
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(308, 6);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(59, 12);
            this.labelFamily.TabIndex = 6;
            this.labelFamily.Text = "本人/家族";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(21, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.panelBatch);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 70);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1020, 499);
            this.scrollPictureControl1.TabIndex = 8;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(470, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 11;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(147, 21);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 21);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.label24);
            this.panelTotal.Controls.Add(this.verifyBoxFushoCount);
            this.panelTotal.Controls.Add(this.verifyBoxF5);
            this.panelTotal.Controls.Add(this.verifyBoxF4);
            this.panelTotal.Controls.Add(this.verifyBoxF3);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxFutan);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.verifyBoxDays);
            this.panelTotal.Controls.Add(this.verifyBoxF2);
            this.panelTotal.Controls.Add(this.verifyBoxF1M);
            this.panelTotal.Controls.Add(this.verifyBoxF1Y);
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.label22);
            this.panelTotal.Controls.Add(this.label21);
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.label10);
            this.panelTotal.Controls.Add(this.label9);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.label2);
            this.panelTotal.Controls.Add(this.label15);
            this.panelTotal.Controls.Add(this.labelDays);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.labelCharge);
            this.panelTotal.Controls.Add(this.label12);
            this.panelTotal.Controls.Add(this.label7);
            this.panelTotal.Location = new System.Drawing.Point(0, 572);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1020, 111);
            this.panelTotal.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(119, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 12);
            this.label24.TabIndex = 33;
            this.label24.Text = "負傷数";
            this.label24.Visible = false;
            // 
            // verifyBoxFushoCount
            // 
            this.verifyBoxFushoCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFushoCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFushoCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFushoCount.Location = new System.Drawing.Point(121, 22);
            this.verifyBoxFushoCount.MaxLength = 1;
            this.verifyBoxFushoCount.Name = "verifyBoxFushoCount";
            this.verifyBoxFushoCount.NewLine = false;
            this.verifyBoxFushoCount.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxFushoCount.TabIndex = 10;
            this.verifyBoxFushoCount.TextV = "";
            this.verifyBoxFushoCount.Visible = false;
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(818, 74);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF5.TabIndex = 54;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(614, 74);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF4.TabIndex = 52;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(410, 74);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF3.TabIndex = 50;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrCode.Location = new System.Drawing.Point(477, 22);
            this.verifyBoxDrCode.MaxLength = 10;
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxDrCode.TabIndex = 22;
            this.verifyBoxDrCode.Text = "1234567890";
            this.verifyBoxDrCode.TextV = "";
            // 
            // verifyBoxFutan
            // 
            this.verifyBoxFutan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFutan.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFutan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFutan.Location = new System.Drawing.Point(366, 22);
            this.verifyBoxFutan.Name = "verifyBoxFutan";
            this.verifyBoxFutan.NewLine = false;
            this.verifyBoxFutan.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxFutan.TabIndex = 20;
            this.verifyBoxFutan.TextV = "";
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(261, 22);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxCharge.TabIndex = 18;
            this.verifyBoxCharge.TextV = "";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(172, 22);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxTotal.TabIndex = 16;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(658, 22);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 25;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(207, 74);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF2.TabIndex = 48;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(56, 22);
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1M.TabIndex = 5;
            this.verifyBoxF1M.TextV = "";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(13, 22);
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1Y.TabIndex = 3;
            this.verifyBoxF1Y.TextV = "";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(5, 74);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(198, 23);
            this.verifyBoxF1.TabIndex = 41;
            this.verifyBoxF1.TextV = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(818, 59);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 13;
            this.label22.Text = "負傷名5";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(614, 59);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 11;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(410, 59);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 9;
            this.label20.Text = "負傷名3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(206, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 7;
            this.label11.Text = "負傷名2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "初検日";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷名1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "月";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "年";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label15.Location = new System.Drawing.Point(595, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 24);
            this.label15.TabIndex = 23;
            this.label15.Text = "[協]: 0\r\n[契]: 1";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(658, 8);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(53, 12);
            this.labelDays.TabIndex = 24;
            this.labelDays.Text = "診療日数";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(172, 8);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(53, 12);
            this.labelTotal.TabIndex = 15;
            this.labelTotal.Text = "合計金額";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(262, 9);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(53, 12);
            this.labelCharge.TabIndex = 17;
            this.labelCharge.Text = "請求金額";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(364, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 12);
            this.label12.TabIndex = 19;
            this.label12.Text = "※（請求金額の下）";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(475, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "柔整師登録番号";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 13;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(11, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 36);
            this.label3.TabIndex = 0;
            this.label3.Text = "バッジ: **\r\n続紙: --\r\n不要: ++";
            // 
            // panelBatch
            // 
            this.panelBatch.Controls.Add(this.label23);
            this.panelBatch.Controls.Add(this.verifyBoxBatch);
            this.panelBatch.Controls.Add(this.label14);
            this.panelBatch.Controls.Add(this.labelShinkyuDr);
            this.panelBatch.Controls.Add(this.label13);
            this.panelBatch.Controls.Add(this.verifyBoxShinkyuDr);
            this.panelBatch.Controls.Add(this.verifyBoxCount);
            this.panelBatch.Controls.Add(this.labelDrDescription);
            this.panelBatch.Controls.Add(this.verifyBoxBank);
            this.panelBatch.Location = new System.Drawing.Point(274, 0);
            this.panelBatch.Name = "panelBatch";
            this.panelBatch.Size = new System.Drawing.Size(745, 71);
            this.panelBatch.TabIndex = 6;
            this.panelBatch.TabStop = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 12);
            this.label23.TabIndex = 0;
            this.label23.Text = "バッチ番号";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxBatch
            // 
            this.verifyBoxBatch.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBatch.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBatch.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBatch.Location = new System.Drawing.Point(13, 21);
            this.verifyBoxBatch.Name = "verifyBoxBatch";
            this.verifyBoxBatch.NewLine = false;
            this.verifyBoxBatch.Size = new System.Drawing.Size(101, 23);
            this.verifyBoxBatch.TabIndex = 1;
            this.verifyBoxBatch.TextV = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(142, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "口座番号";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelShinkyuDr
            // 
            this.labelShinkyuDr.AutoSize = true;
            this.labelShinkyuDr.Location = new System.Drawing.Point(367, 8);
            this.labelShinkyuDr.Name = "labelShinkyuDr";
            this.labelShinkyuDr.Size = new System.Drawing.Size(53, 12);
            this.labelShinkyuDr.TabIndex = 6;
            this.labelShinkyuDr.Text = "機関番号";
            this.labelShinkyuDr.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(276, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 4;
            this.label13.Text = "合計枚数";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxShinkyuDr
            // 
            this.verifyBoxShinkyuDr.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxShinkyuDr.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxShinkyuDr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxShinkyuDr.Location = new System.Drawing.Point(369, 21);
            this.verifyBoxShinkyuDr.Name = "verifyBoxShinkyuDr";
            this.verifyBoxShinkyuDr.NewLine = false;
            this.verifyBoxShinkyuDr.Size = new System.Drawing.Size(108, 23);
            this.verifyBoxShinkyuDr.TabIndex = 7;
            this.verifyBoxShinkyuDr.TextV = "";
            // 
            // verifyBoxCount
            // 
            this.verifyBoxCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCount.Location = new System.Drawing.Point(278, 20);
            this.verifyBoxCount.Name = "verifyBoxCount";
            this.verifyBoxCount.NewLine = false;
            this.verifyBoxCount.Size = new System.Drawing.Size(59, 23);
            this.verifyBoxCount.TabIndex = 5;
            this.verifyBoxCount.TextV = "";
            // 
            // labelDrDescription
            // 
            this.labelDrDescription.AutoSize = true;
            this.labelDrDescription.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDrDescription.Location = new System.Drawing.Point(483, 25);
            this.labelDrDescription.Name = "labelDrDescription";
            this.labelDrDescription.Size = new System.Drawing.Size(59, 12);
            this.labelDrDescription.TabIndex = 8;
            this.labelDrDescription.Text = "空欄時: ++";
            // 
            // verifyBoxBank
            // 
            this.verifyBoxBank.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBank.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBank.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBank.Location = new System.Drawing.Point(142, 21);
            this.verifyBoxBank.Name = "verifyBoxBank";
            this.verifyBoxBank.NewLine = false;
            this.verifyBoxBank.Size = new System.Drawing.Size(101, 23);
            this.verifyBoxBank.TabIndex = 3;
            this.verifyBoxBank.TextV = "";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxNumbering);
            this.panelHnum.Controls.Add(this.labelNumbering);
            this.panelHnum.Controls.Add(this.verifyCheckBoxSai);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelFamily);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label6);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Location = new System.Drawing.Point(274, 0);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(746, 71);
            this.panelHnum.TabIndex = 7;
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(73, 21);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxNumbering.TabIndex = 3;
            this.verifyBoxNumbering.TextV = "";
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(71, 6);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(59, 12);
            this.labelNumbering.TabIndex = 2;
            this.labelNumbering.Text = "ナンバリング";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyCheckBoxSai
            // 
            this.verifyCheckBoxSai.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxSai.CheckedV = false;
            this.verifyCheckBoxSai.Location = new System.Drawing.Point(9, 21);
            this.verifyCheckBoxSai.Name = "verifyCheckBoxSai";
            this.verifyCheckBoxSai.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxSai.Size = new System.Drawing.Size(46, 24);
            this.verifyCheckBoxSai.TabIndex = 1;
            this.verifyCheckBoxSai.Text = "災";
            this.verifyCheckBoxSai.UseVisualStyleBackColor = false;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(182, 21);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(104, 23);
            this.verifyBoxHnum.TabIndex = 5;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(310, 21);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 7;
            this.verifyBoxFamily.TextV = "";
            this.verifyBoxFamily.Validating += new System.ComponentModel.CancelEventHandler(this.verifyBoxFamily_Validating);
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(613, 21);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 19;
            this.verifyBoxBD.TextV = "";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(570, 21);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 17;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(527, 21);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 15;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(476, 21);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 13;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(416, 21);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 10;
            this.verifyBoxSex.TextV = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "支払猶予";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(474, 6);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(53, 12);
            this.labelBirthday.TabIndex = 12;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(414, 6);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 9;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(502, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 24);
            this.label5.TabIndex = 14;
            this.label5.Text = "昭:3\r\n平:4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(443, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(639, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 20;
            this.label16.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(596, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 18;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(553, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 16;
            this.label18.Text = "年";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputFormRev
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputFormRev";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelBatch.ResumeLayout(false);
            this.panelBatch.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelNumbering;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panelHnum;
        private System.Windows.Forms.Panel panelBatch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxFamily;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxFutan;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private VerifyBox verifyBoxNumbering;
        private VerifyBox verifyBoxDrCode;
        private VerifyBox verifyBoxCount;
        private VerifyBox verifyBoxBank;
        private VerifyBox verifyBoxF1M;
        private VerifyBox verifyBoxF1Y;
        private System.Windows.Forms.Label labelShinkyuDr;
        private VerifyBox verifyBoxShinkyuDr;
        private System.Windows.Forms.Label labelDrDescription;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxBatch;
        private VerifyCheckBox verifyCheckBoxSai;
        private System.Windows.Forms.Label label24;
        private VerifyBox verifyBoxFushoCount;
    }
}