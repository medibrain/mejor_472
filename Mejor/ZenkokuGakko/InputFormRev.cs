﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.ZenkokuGakko
{
    public partial class InputFormRev : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //20190515154039 furukawa st ////////////////////////
        //レセタイプ変数
        
        private APP_TYPE currentAppType;
        //20190515154039 furukawa ed ////////////////////////


        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHosCode = new Point(400, 1900);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 300);
        Point posCost = new Point(400, 1900);
        Point posDays = new Point(400, 300);
        Point posNumbering = new Point(300, 0);

        Point posBatch = new Point(200, 1000);
        Point posBatchAccount = new Point(600, 1800);
        Point posBatchDrCode = new Point(200, 2400);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls,
            fushoControls, hosControls, numberControls, batchControls,
            batchAcountControls, batchDrCodeControls;

        //20211112125652 furukawa st ////////////////////////
        //過去データ表示用データ               
        List<App> lstPastData = new List<App>();
        //20211112125652 furukawa ed ////////////////////////


        public InputFormRev(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();
                        
            #region コントロールグループ化

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hnumControls = new Control[] { verifyBoxHnum, verifyBoxFamily, verifyBoxFamily };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, verifyBoxFutan, };

            
            //20190508161902 furukawa st ////////////////////////
            //負傷名１－５から負傷数に変更
            
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF1Y, verifyBoxF1M, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, verifyBoxFushoCount, };
            //fushoControls = new Control[] { verifyBoxF1, verifyBoxF1Y, verifyBoxF1M, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            //20190508161902 furukawa ed ////////////////////////
            
            hosControls = new Control[] { verifyBoxDrCode, };
            numberControls = new Control[] { verifyBoxNumbering, };
            batchControls = new Control[] { verifyBoxBatch, verifyBoxCount, };
            batchAcountControls = new Control[] { verifyBoxBank, };
            batchDrCodeControls = new Control[] { verifyBoxShinkyuDr, };
#endregion
            
            #region イベントを関連付ける
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion

            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            #region 左グリッド初期化
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            panelBatch.Visible = false;
            panelHnum.Visible = false;
            panelTotal.Visible = false;
            verifyBoxM.Visible = false;
            labelM.Visible = false;

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //20210329104934 furukawa st ////////////////////////
            //1枚目はBsApp_CurrentChangedイベントを通らないので直接入れる            
            currentAppType = scanGroup.AppType;
            //20210329104934 furukawa ed ////////////////////////

            if (app != null) setApp(app);
            verifyBoxY.Focus();


            //20211112125801 furukawa st ////////////////////////
            //先月処理したデータを取得しておく

            //20211201133516 furukawa st ////////////////////////
            //入力速度向上が見られないので中止
            //lstPastData = App.GetAppsWithWhere($" where a.cym={DateTimeEx.Int6YmAddMonth(app.CYM,-1)}");
            //20211201133516 furukawa ed ////////////////////////

            //20211112125801 furukawa ed ////////////////////////

        }




        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            //20190515154129 furukawa st ////////////////////////
            //レセタイプ判定
            
            currentAppType = scanGroup.AppType;
            //20190515154129 furukawa ed ////////////////////////

            setApp(app);
            focusBack(false);
        }



        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (hosControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHosCode;
            else if (numberControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
            else if (batchControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatch;
            else if (batchAcountControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatchAccount;
            else if (batchDrCodeControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBatchDrCode;
        }



        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：バッジ
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkBatch(App app)
        {
            hasError = false;

            //バッチ番号
            setStatus(verifyBoxBatch, verifyBoxBatch.Text.Trim().Length != Common.BatchLength);

            //口座番号
            setStatus(verifyBoxBank, verifyBoxBank.Text.Trim().Length != 7);

            //合計枚数
            int count = verifyBoxCount.GetIntValue();
            setStatus(verifyBoxCount, count < 1);

            //鍼灸マッサージ時、2～5ケタまたは10桁 記入ない場合は++
            var drNumber = string.Empty;
            if (scan.AppType == APP_TYPE.鍼灸 || scan.AppType == APP_TYPE.あんま
                    || Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_23AICHI
                    || Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_11SAITAMA)
            {
                //20211001104311 furukawa st ////////////////////////
                //愛知支部の柔整バッチシートの柔整師番号は不要とする
                
                if (scan.AppType == APP_TYPE.柔整 && Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_23AICHI)
                {
                    //チェックスルーする
                }
                //20211001104311 furukawa ed ////////////////////////
                else
                {
                    drNumber = verifyBoxShinkyuDr.Text.Trim();
                    setStatus(verifyBoxShinkyuDr,
                        drNumber != "++" && !(2 <= drNumber.Length && drNumber.Length <= 5) && drNumber.Length != 10);
                }
                
            }


            if (hasError)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
            app.MediMonth = 0;
            app.HihoNum = string.Empty;
            app.Family = 0;
            app.HihoType = 0;
            app.Sex = 0;
            app.Birthday = DateTime.MinValue;
            app.CountedDays = count;
            app.Total = 0;
            app.Charge = 0;
            app.Partial = 0;
            app.DrNum = drNumber;
            app.Numbering = verifyBoxBatch.Text.Trim();
            app.AccountNumber = verifyBoxBank.Text.Trim();
            app.AppType = APP_TYPE.バッチ;
            return true;
        }


        /// <summary>
        /// 柔整師登録記号番号チェック
        /// 20210329100420 furukawa 
        /// あはき:登録記号番号10桁数字であれば自由。柔整：5桁以下数字 or 10桁数字固定＋0or1のチェック
        /// </summary>
        private string checkDrCode()
        {

            //柔整師番号
            string drCode = verifyBoxDrCode.Text.Trim();

            //数字だけチェック
            //本来intだったが、intの最大値では10桁の頭が2以上になると範囲外になるので、longに変更した
            long.TryParse(drCode, out long drLongCode);

            if (drCode.Length == 0)
            {
                //20210401170303 furukawa st ////////////////////////
                //登録記号番号が空欄の時もある

                return string.Empty;


                //空欄
                //      setStatus(verifyBoxDrCode, false);

                
                //20210401170303 furukawa ed ////////////////////////
            }

            switch (currentAppType)
            {
                case APP_TYPE.柔整:

                  
                    //地方公務員共済組合協議会番号1-5桁
                    if (drCode.Length < 6 && 0 < drLongCode)
                    {
                        setStatus(verifyBoxDrCode, false);
                    }
                    //通常の柔整師番号
                    //1文字目は協=0か契=1のどちらかのみ
                    else if ((drCode.Length == 10 && 0 < drLongCode) &&
                        (drCode.Substring(0, 1) == "0" || drCode.Substring(0, 1) == "1"))
                    {
                        setStatus(verifyBoxDrCode, false);
                    }
                    else
                    {
                        setStatus(verifyBoxDrCode, true);
                    }
                    


                    break;

                case APP_TYPE.鍼灸:
                case APP_TYPE.あんま:

                    //10桁以下自由
                    if (drCode.Length <=10 && 0 < drLongCode)
                    {
                        setStatus(verifyBoxDrCode, false);
                    }
                    else
                    {
                        setStatus(verifyBoxDrCode, true);
                    }

                    break;
            }


          
            return drCode;
        }



        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();

            //20190501095028 furukawa st ////////////////////////
            //入力チェックを外す伸作さん要望
            ////setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);
            setStatus(verifyBoxY, false);     //イレギュラー年対応用（18/4/3久保）
            //20190501095028 furukawa ed ////////////////////////

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 2文字以上かつ英数字
            var r = Regex.IsMatch(verifyBoxHnum.Text, "^[0-9A-Z]{2,}$");
            setStatus(verifyBoxHnum, !r);

            //本家区分
            var fs = new[] { 0, 2, 4, 6, 8 };
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !fs.Contains(family) || verifyBoxFamily.Text.Trim().Length != 1);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 10 | total < charge);

            //患者負担額 基本入力なし
            int futan = verifyBoxFutan.GetIntValue();
            if (futan < 0) futan = 0;
            setStatus(verifyBoxFutan, (verifyBoxFutan.Text.Trim().Length > 0 && futan < 1) | total < futan);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            if ((family == 2 || family == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //診療日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);




            //20190515153923 furukawa st ////////////////////////
            //柔整のみ負傷数、初検日チェック
            
            int cntFusyosu = 0;
            DateTime shoken = DateTime.MinValue;


            //20210329115301 furukawa st ////////////////////////
            //初検年月は柔整あはき両方入力になったので判定しない

            //確認用フラグ
            bool flgCert = false;

            //初検年月
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue();
            shoken = DateTime.MinValue;
  
            sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);

            if (!DateTimeEx.IsDate(sY, sM, 1))
            {
                //20210401163730 furukawa st ////////////////////////
                //初検年月がない場合があるが、登録できるように確認扱い

                if (verifyBoxF1Y.Text.Trim() == string.Empty && 
                    verifyBoxF1M.Text.Trim() == string.Empty)
                {
                    //未入力の時は確認とする
                    flgCert = true;
                }
                //20210401163730 furukawa ed ////////////////////////
                else
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                }
            }
            else
            {
                setStatus(verifyBoxF1Y, false);
                setStatus(verifyBoxF1M, false);
                shoken = new DateTime(sY, sM, 1);
            }
            

            #region old

            //switch (currentAppType)

            //{
            //    case APP_TYPE.柔整:


            //        //初検年月
            //        int sY = verifyBoxF1Y.GetIntValue();
            //        int sM = verifyBoxF1M.GetIntValue();
            //        shoken = DateTime.MinValue;
            //        if (string.IsNullOrWhiteSpace(scanGroup.note2))
            //        {

            //            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //            sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);
            //            //sY = DateTimeEx.GetAdYearFromHs(sY);
            //            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //            if (!DateTimeEx.IsDate(sY, sM, 1))
            //            {
            //                setStatus(verifyBoxF1Y, true);
            //                setStatus(verifyBoxF1M, true);
            //            }
            //            else
            //            {
            //                setStatus(verifyBoxF1Y, false);
            //                setStatus(verifyBoxF1M, false);
            //                shoken = new DateTime(sY, sM, 1);
            //            }
            //        }

            //        //20200924141930 furukawa st ////////////////////////
            //        //負傷名を手入力に仕様変更により負傷数は手入力しない

            //                    //20190508162516 furukawa st ////////////////////////
            //                    //負傷数チェック
            //                    //setStatus(verifyBoxFushoCount, !int.TryParse(verifyBoxFushoCount.Text, out cntFusyosu) || cntFusyosu < 0);
            //                    //20190508162516 furukawa ed ////////////////////////
            //        //20200924141930 furukawa ed ////////////////////////

            //        break;

            //}
            ////20190515153923 furukawa ed ////////////////////////
            #endregion

            //20210329115301 furukawa ed ////////////////////////


            //20200924141543 furukawa st ////////////////////////
            //負傷名を手入力に仕様変更により復活


            //20190515150843 furukawa st ////////////////////////
            //負傷名チェックを削除（入力項目を削除した）

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            //20190515150843 furukawa ed ////////////////////////

            //20200924141543 furukawa ed ////////////////////////



            //20210329100618 furukawa st ////////////////////////
            //煩雑になるので関数化


            //柔整師番号
            string drCode = checkDrCode();

            #region old
            //      var drCode = verifyBoxDrCode.Text.Trim();
            //      int.TryParse(drCode, out int drIntCode);
            //      if (drCode.Length == 0)
            //      {
            //          //空欄
            //          setStatus(verifyBoxDrCode, false);
            //      }
            //      else if (drCode.Length < 6 && 0 < drIntCode)
            //      {
            //          //地方公務員共済組合協議会番号1-5桁
            //          setStatus(verifyBoxDrCode, false);
            //      }
            //      else if (drCode.Length == 10 && (drCode[0] == '0' || drCode[0] == '1') && 0 < drIntCode)
            //      {
            //          //通常の柔整師番号
            //          //1文字目は協=0か契=1のどちらかのみ
            //          setStatus(verifyBoxDrCode, false);
            //      }
            //      else
            //      {
            //          setStatus(verifyBoxDrCode, true);
            //      }
            #endregion

            //20210329100618 furukawa ed ////////////////////////


            //ナンバリングチェック 学校共済のナンバリングは6桁以内
            int numbering = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, numbering < 1 || Common.MaxNumbering < numbering);

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            
            //柔整で柔整登録番号がなければ確認

            //20210401170719 furukawa st ////////////////////////
            //登録記号番号が空欄の時も確認して登録にする
            
            if (drCode.Length == 0 && 
                (currentAppType==APP_TYPE.あんま || currentAppType==APP_TYPE.柔整 || currentAppType==APP_TYPE.鍼灸))
            //          if (scan.Note2 == string.Empty && drCode.Length == 0)
            //20210401170719 furukawa ed ////////////////////////
            {
                verifyBoxDrCode.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("柔整師登録番号が指定されていません。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //20210401163926 furukawa st ////////////////////////
            //初検年月がない場合登録時チェック

            if (flgCert)
            {
                verifyBoxF1Y.BackColor = Color.GreenYellow;
                verifyBoxF1M.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("初検年月が入力されていません。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }
            //20210401163926 furukawa ed ////////////////////////

            //ナンバリング抜けチェック
            var index = bsApp.Position;
            var l = (List<App>)bsApp.DataSource;
            int preNum = 0;
            for (int i = index - 1; i >= 0; i--)
            {
                var preApp = l[i];
                if (preApp.MediYear > 0)
                {
                    if (!int.TryParse(preApp.Numbering, out preNum)) continue;
                    break;
                }
            }

            if (numbering != 1 && preNum != 0 && preNum + 1 != numbering)
            {
                verifyBoxNumbering.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("直前のナンバリングと連番になっていません。" +
                    "このまま登録してもよろしいですか？", "ナンバリング確認",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text.Trim();
            app.Family = family;
            app.HihoType = verifyCheckBoxSai.Checked ? (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予 : 0;
            app.Sex = sex;
            app.Birthday = birthDt;
            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.Partial = futan;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            app.NewContType = DateTimeEx.GetAdYearFromHs(year * 100 + month) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = DateTimeEx.GetAdYearFromHs(year) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.DrNum = drCode;
            app.Numbering = numbering.ToString().PadLeft(Common.NumberingLength, '0');
            app.AccountNumber = string.Empty;

            app.AppType = scan.AppType;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            app.FushoFirstDate1 = shoken;

            //20190508162701 furukawa st ////////////////////////
            //負傷数登録（application.fusyoは自動計算のみで、直接登録できない)

            app.TaggedDatas.count = cntFusyosu;
            //20190508162701 furukawa ed ////////////////////////

            return true;
        }
        

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(Common.NumberingLength, '0');
            verifyBoxHnum.Text = verifyBoxHnum.Text.ToUpper();

            if (verifyBoxY.Text == "**")
            {
                //バッジの場合
                if (!checkBatch(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.MediMonth = 0;
                app.HihoNum = string.Empty;
                app.Family = 0;
                app.Sex = 0;
                app.Birthday = DateTime.MinValue;
                app.CountedDays = 0;
                app.Total = 0;
                app.Charge = 0;
                app.Partial = 0;
                app.DrNum = string.Empty;
                app.Numbering = string.Empty;
                app.AccountNumber = string.Empty;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.MediMonth = 0;
                app.HihoNum = string.Empty;
                app.Family = 0;
                app.Sex = 0;
                app.Birthday = DateTime.MinValue;
                app.CountedDays = 0;
                app.Total = 0;
                app.Charge = 0;
                app.Partial = 0;
                app.DrNum = string.Empty;
                app.Numbering = string.Empty;
                app.AccountNumber = string.Empty;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);
            verifyBoxHnum.TabStop = true;
            verifyBoxSex.TabStop = true;
            verifyBoxBE.TabStop = true;
            verifyBoxBY.TabStop = true;
            verifyBoxBM.TabStop = true;
            verifyBoxBD.TabStop = true;
            verifyBoxDrCode.TabStop = true;
            //verifyBoxTotal.TabStop = true;
            verifyBoxCharge.TabStop = true;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            //if (app.StatusFlagCheck(StatusFlag.入力済))
            if (app.StatusFlagCheck(StatusFlag.入力済) || app.StatusFlagCheck(StatusFlag.外部1回目済))  //20221216 ito プロジェクトK入力
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                var o = new Ocr.OcrDatas(app);
                verifyBoxF1.Text = Fusho.GetFushoName(o.Fusho1);
                verifyBoxF2.Text = Fusho.GetFushoName(o.Fusho2);
                verifyBoxF3.Text = Fusho.GetFushoName(o.Fusho3);
                verifyBoxF4.Text = Fusho.GetFushoName(o.Fusho4);
                verifyBoxF5.Text = Fusho.GetFushoName(o.Fusho5);

                void setOcr(VerifyBox vb, string s)
                {
                    vb.Text = s;
                    vb.BackColor = Color.LightGray;
                    vb.TabStop = false;
                }

                var r = Ocr.OcrHelper.Check(app, o);
                if (r.UsePerson)
                {
                    setOcr(verifyBoxHnum, r.HihoNum);
                    setOcr(verifyBoxSex, ((int)r.Sex).ToString());
                    setOcr(verifyBoxBE, DateTimeEx.GetEraNumber(r.Birth).ToString());
                    setOcr(verifyBoxBY, DateTimeEx.GetJpYear(r.Birth).ToString());
                    setOcr(verifyBoxBM, r.Birth.Month.ToString());
                    setOcr(verifyBoxBD, r.Birth.Day.ToString());
                }
                if (r.DrNum.Length != 0) setOcr(verifyBoxDrCode, r.DrNum);


                //20201221130923 furukawa st ////////////////////////
                //OCRから合計を取得すると使用不可能になるので取得しない

                        //20201206095240 furukawa st ////////////////////////
                        //OCRデータから合計に入れるところがコメントになってた                
                        //if (r.Total != 0) setOcr(verifyBoxTotal, r.Total.ToString());
                        //20201206095240 furukawa ed ////////////////////////

                //20201221130923 furukawa ed ////////////////////////


                if (r.Charge != 0) setOcr(verifyBoxCharge, r.Charge.ToString());
            }

            void setEnable(TextBox t, bool b)
            {
                t.Enabled = b;
                t.BackColor = b ? SystemColors.Info : SystemColors.Control;
            }

            //鍼灸時はバッチシートで柔整師番号を入力しているため、グレーアウト
            if (scan.AppType != APP_TYPE.柔整)
            {
                setEnable(verifyBoxF1, false);
                setEnable(verifyBoxF2, false);
                setEnable(verifyBoxF3, false);
                setEnable(verifyBoxF4, false);
                setEnable(verifyBoxF5, false);
                
                
                //20210317100544 furukawa st ////////////////////////
                //あはきも初検年月、柔整師登録記号番号を入力する

                //      setEnable(verifyBoxF1Y, false);
                //      setEnable(verifyBoxF1M, false);
                //      setEnable(verifyBoxDrCode, false);
                //20210317100544 furukawa ed ////////////////////////


                setEnable(verifyBoxFushoCount, false);
            }

        
            //画像の表示
            setImage(app);
            changedReset(app);
        }


     
        //20211119095919 furukawa st ////////////////////////
        //前月の情報を取得/自動設定
        
        private void verifyBoxFamily_Validating(object sender, CancelEventArgs e)
        {
            //20211201133630 furukawa st ////////////////////////
            //入力速度向上が見られないので中止            
            return;
            //20211201133630 furukawa ed ////////////////////////



            App app = (App)bsApp.Current;

            //1回め入力のみ自動
            if (app.StatusFlagCheck(StatusFlag.入力済)) return;

            //診療年月-1を取得し、前月条件とする
            int ayear = int.Parse(verifyBoxY.Text);
            int amonth = int.Parse(verifyBoxM.Text);
            int pastym = DateTimeEx.Int6YmAddMonth(DateTimeEx.GetAdYearFromHs(ayear * 100 + amonth) * 100 + amonth, -1);

            //ヒホバン前ゼロだけ取る(アルファベット入りもあるのでint.parseでの数値の比較は失敗する)
            string strHnum = verifyBoxHnum.Text.Trim();
            strHnum = Regex.Replace(strHnum, "^[0](.+)", "$1");

            foreach (App a in lstPastData)
            {
                //ヒホバン前ゼロだけ取る
                string strHnumAPP = Regex.Replace(a.HihoNum, "^[0](.+)", "$1");

                //被保番、申請書タイプ、診療年月、家族区分、本人だけの合致で取得する
                if ((strHnumAPP == strHnum) &&
                    (a.AppType == scan.AppType) &&
                    (pastym == a.YM) &&
                    (a.Family==int.Parse(verifyBoxFamily.Text.Trim()) &&
                    verifyBoxFamily.Text.Trim()=="2")
                    )

                {
                    //値が入ったらグレーにしてtabstopを飛ばす
                    if (a.Birthday != null)
                    {
                        
                        int wareki = DateTimeEx.GetIntJpDateWithEraNumber(a.Birthday);
                        verifyBoxBE.Text = wareki.ToString().Substring(0, 1);
                        verifyBoxBY.Text = int.Parse(wareki.ToString().Substring(1, 2)).ToString();
                        verifyBoxBM.Text = int.Parse(wareki.ToString().Substring(3, 2)).ToString();
                        verifyBoxBD.Text = int.Parse(wareki.ToString().Substring(5, 2)).ToString();

                        verifyBoxBE.BackColor = Color.LightGray;
                        verifyBoxBY.BackColor = Color.LightGray;
                        verifyBoxBM.BackColor = Color.LightGray;
                        verifyBoxBD.BackColor = Color.LightGray;

                        verifyBoxBE.TabStop = false;
                        verifyBoxBY.TabStop = false;
                        verifyBoxBM.TabStop = false;
                        verifyBoxBD.TabStop = false;

                    }
                    verifyBoxSex.Text = a.Sex.ToString();
                    verifyBoxSex.BackColor = Color.LightGray;
                    verifyBoxSex.TabStop = false;
                    
                    verifyBoxDrCode.Text = a.DrNum;
                    verifyBoxDrCode.BackColor = Color.LightGray;
                    verifyBoxDrCode.TabStop = false;



                    break;

                }
            }
        }
        //20211119095919 furukawa ed ////////////////////////





        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
                scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                //20220323180452 furukawa st ////////////////////////
                //ファイルが見つからない場合はクリアする                
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
                //20220323180452 furukawa ed ////////////////////////
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 入力済みのデータをセットします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);
            var ca = new CheckZenkokuGakkoInputAall(app);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {
                setValue(verifyBoxY, "**", firstTime, nv);

                setValue(verifyBoxBatch, app.Numbering, firstTime, nv);
                setValue(verifyBoxCount, app.CountedDays, firstTime, nv);
                setValue(verifyBoxBank, app.AccountNumber, firstTime, nv);
                setValue(verifyBoxShinkyuDr, app.DrNum == string.Empty ? "++" : app.DrNum, firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                setValue(verifyBoxFamily, app.Family.ToString(), firstTime, nv);
                setValue(verifyCheckBoxSai, app.HihoType == (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予, firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv && ca.ErrBirth);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);


                //20201002095015 furukawa st ////////////////////////
                //金額をベリファイに

                setValue(verifyBoxTotal, app.Total, firstTime, nv );
                setValue(verifyBoxCharge, app.Charge, firstTime, nv );

                    //setValue(verifyBoxTotal, app.Total, firstTime, nv && ca.ErrAtotal);
                    //setValue(verifyBoxCharge, app.Charge, firstTime, nv && ca.ErrAcharge);
                //20201002095015 furukawa ed ////////////////////////



                setValue(verifyBoxFutan, app.Partial, firstTime, nv);
                setValue(verifyBoxNumbering, app.Numbering, firstTime, nv);




                //20190508162807 furukawa st ////////////////////////
                //負傷数表示application.fusyoは自動計算のみで、直接登録できないのでTaggedDatas

                //20190513170809 furukawa st ////////////////////////
                //負傷数をベリファイ入力にする

                setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, true);
                //      setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);
                //20190513170809 furukawa ed ////////////////////////

                //20190508162807 furukawa ed ////////////////////////


                //20210329133240 furukawa st ////////////////////////
                //あはきでも登録記号番号入力。ベリファイだろう               
                setValue(verifyBoxDrCode, app.DrNum, firstTime, nv);
                //          柔整以外は不要
                //          setValue(verifyBoxDrCode, app.DrNum, firstTime, nv && app.AppType == APP_TYPE.柔整);
                //20210329133240 furukawa ed ////////////////////////

                //負傷名 はベリファイなし
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);


                //20201002081910 furukawa st ////////////////////////
                //初検日をベリファイに


                if (app.FushoFirstDate1.IsNullDate())
                {
                    setValue(verifyBoxF1Y, string.Empty, firstTime, nv);
                    setValue(verifyBoxF1M, string.Empty, firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, nv);
                    setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, nv);
                }

                
                        //if (app.FushoFirstDate1.IsNullDate())
                        //{
                        //    setValue(verifyBoxF1Y, string.Empty, firstTime, false);
                        //    setValue(verifyBoxF1M, string.Empty, firstTime, false);
                        //}
                        //else
                        //{
                        //    setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, false);
                        //    setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, false);
                        //}

                //20201002081910 furukawa ed ////////////////////////

            }
            missCounterReset();
        }


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text == "**")
            {
                //赤バッジの場合
                panelBatch.Visible = true;
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
                if (scan.AppType == APP_TYPE.鍼灸 || scan.AppType == APP_TYPE.あんま
                    //20210927125514 furukawa st ////////////////////////
                    //愛知支部の柔整は柔整師を入れなくなった
                    //|| Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_23AICHI //210816 IR
                    //20210927125514 furukawa ed ////////////////////////

                    || Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_11SAITAMA)
                {
                    labelShinkyuDr.Visible = true;
                    verifyBoxShinkyuDr.Visible = true;
                    labelDrDescription.Visible = true;
                }
                else
                {
                    labelShinkyuDr.Visible = false;
                    verifyBoxShinkyuDr.Visible = false;
                    labelDrDescription.Visible = false;
                }

                scrollPictureControl1.ScrollPosition = posBatch;
            }
            else if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text.Trim().Length == 0)
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelBatch.Visible = false;
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelBatch.Visible = false;
                panelHnum.Visible = true;
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (batchControls.Contains(t)) posBatch = pos;
            else if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (hosControls.Contains(t)) posHosCode = pos;
            else if (numberControls.Contains(t)) posNumbering = pos;
            else if (batchAcountControls.Contains(t)) posBatchAccount = pos;
            else if (batchDrCodeControls.Contains(t)) posBatchDrCode = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        
        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
