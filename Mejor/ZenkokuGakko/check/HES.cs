﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.OsakaGakko.check
{
    class HES : Columns
    {
        private const int REQUIRED_COLUMN_COUNT = 4;
        private bool lostColumn;
        private bool overColumn;
        public bool Error;
        public int Row { get; set; }
        public string 入力年月 => C_GEEMM.Val;
        public string ﾊﾞｯﾁ => Batch.Val;
        public string ﾅﾝﾊﾞﾘﾝｸﾞ => Numbering.Val;
        public string メモ => Memo.Val;
        public Property C_GEEMM { get; set; }
        public Property Batch { get; set; }
        public Property Numbering { get; set; }
        private Property Memo;
        public HES(int row, string[] record)
        {
            this.Row = row;

            lostColumn = record.Length < REQUIRED_COLUMN_COUNT;
            overColumn = record.Length > REQUIRED_COLUMN_COUNT;
            if (lostColumn || overColumn)
            {
                Error = true;
                var p = new Property();
                this.C_GEEMM = p;
                this.Batch = p;
                this.Numbering = p;
                this.Memo = p;
                return;
            }

            this.C_GEEMM = new Property(nameof(HES.入力年月), record[0], 5, Property.TYPE.GEEMM);
            this.Batch = new Property(nameof(HES.ﾊﾞｯﾁ), record[1], 4, Property.TYPE.NUMERIC);
            this.Numbering = new Property(nameof(HES.ﾅﾝﾊﾞﾘﾝｸﾞ), record[2], 6, Property.TYPE.NUMERIC);
            this.Memo = new Property(nameof(HES.メモ), record[3], 25, true, Property.TYPE.TEXT);//25文字*2byte=50まで
        }
        public bool AllValueCheck(out string message)
        {
            message = "";

            if (lostColumn)
            {
                message += $"({Row}行目) 列数が{REQUIRED_COLUMN_COUNT}ではありません\r\n";
                return false;
            }
            else if (overColumn)
            {
                message += $"({Row}行目) 列数が{REQUIRED_COLUMN_COUNT}を超過しています\r\n";
                return false;
            }

            string m;
            if (!C_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Batch.ValueCheck(Row, out m)) message += m;
            if (!Numbering.ValueCheck(Row, out m)) message += m;
            if (!memoCheck(Row, out m)) message += m;

            if (message != "") message = message.Substring(0, message.LastIndexOf("\r\n"));
            return message == "";
        }
        private bool memoCheck(int row, out string message)
        {
            message = "";
            var val = Memo.Val;
            var name = Memo.Name;
            if (val == null)
            {
                message += $"({row}行目)[{name}] 値が取得できません\r\n";
                return false;
            }
            if (Memo.Required && val == "")
            {
                message += $"({row}行目)[{name}] 値が必要ありません\r\n";
                return false;
            }
            if (val.Length > Memo.MaxLength)
            {
                message += $"({row}行目)[{name}] 最大文字数を超過しています（規定：{Memo.MaxLength}　値：{val.Length}）\r\n";
                return false;
            }
            if (val.Contains(",") || val.Contains("@") || val.Contains("'"))
            {
                message += $"({row}行目)[{name}] 不正な文字が含まれています（規定：,'@　値：{val}）\r\n";
                return false;
            }
            return true;
        }
    }
}
