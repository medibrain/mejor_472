﻿namespace Mejor.ZenkokuGakko.check
{
    partial class SubmitDataCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.richTextBoxLogConsole = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonLogOutput = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.progressBarCheck = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFolderPath = new System.Windows.Forms.TextBox();
            this.buttonDataPath = new System.Windows.Forms.Button();
            this.buttonCheckFileSubmit = new System.Windows.Forms.Button();
            this.buttonCheckStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCheckFilePath = new System.Windows.Forms.TextBox();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabRES = new System.Windows.Forms.TabPage();
            this.dataGridViewRES = new System.Windows.Forms.DataGridView();
            this.tabHES = new System.Windows.Forms.TabPage();
            this.dataGridViewHES = new System.Windows.Forms.DataGridView();
            this.tabRER = new System.Windows.Forms.TabPage();
            this.dataGridViewRER = new System.Windows.Forms.DataGridView();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonTest = new System.Windows.Forms.Button();
            this.panelLeft.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabRES.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRES)).BeginInit();
            this.tabHES.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHES)).BeginInit();
            this.tabRER.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRER)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelLeft.Controls.Add(this.panel4);
            this.panelLeft.Controls.Add(this.panel3);
            this.panelLeft.Controls.Add(this.panel2);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(456, 721);
            this.panelLeft.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 250);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel4.Size = new System.Drawing.Size(456, 433);
            this.panel4.TabIndex = 12;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.richTextBoxLogConsole);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(10, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(436, 433);
            this.panel5.TabIndex = 9;
            // 
            // richTextBoxLogConsole
            // 
            this.richTextBoxLogConsole.BackColor = System.Drawing.SystemColors.WindowText;
            this.richTextBoxLogConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxLogConsole.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBoxLogConsole.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxLogConsole.Name = "richTextBoxLogConsole";
            this.richTextBoxLogConsole.Size = new System.Drawing.Size(436, 433);
            this.richTextBoxLogConsole.TabIndex = 8;
            this.richTextBoxLogConsole.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonLogOutput);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 683);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(456, 38);
            this.panel3.TabIndex = 11;
            // 
            // buttonLogOutput
            // 
            this.buttonLogOutput.Location = new System.Drawing.Point(371, 3);
            this.buttonLogOutput.Name = "buttonLogOutput";
            this.buttonLogOutput.Size = new System.Drawing.Size(75, 23);
            this.buttonLogOutput.TabIndex = 9;
            this.buttonLogOutput.Text = "ログ出力";
            this.buttonLogOutput.UseVisualStyleBackColor = true;
            this.buttonLogOutput.Click += new System.EventHandler(this.buttonLogOutput_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonTest);
            this.panel2.Controls.Add(this.progressBarCheck);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBoxFolderPath);
            this.panel2.Controls.Add(this.buttonDataPath);
            this.panel2.Controls.Add(this.buttonCheckFileSubmit);
            this.panel2.Controls.Add(this.buttonCheckStart);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.textBoxCheckFilePath);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(456, 250);
            this.panel2.TabIndex = 10;
            // 
            // progressBarCheck
            // 
            this.progressBarCheck.Location = new System.Drawing.Point(12, 225);
            this.progressBarCheck.Name = "progressBarCheck";
            this.progressBarCheck.Size = new System.Drawing.Size(434, 21);
            this.progressBarCheck.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "提出データ所在：";
            // 
            // textBoxFolderPath
            // 
            this.textBoxFolderPath.Location = new System.Drawing.Point(53, 29);
            this.textBoxFolderPath.Multiline = true;
            this.textBoxFolderPath.Name = "textBoxFolderPath";
            this.textBoxFolderPath.ReadOnly = true;
            this.textBoxFolderPath.Size = new System.Drawing.Size(393, 32);
            this.textBoxFolderPath.TabIndex = 2;
            this.textBoxFolderPath.TabStop = false;
            // 
            // buttonDataPath
            // 
            this.buttonDataPath.Location = new System.Drawing.Point(6, 29);
            this.buttonDataPath.Name = "buttonDataPath";
            this.buttonDataPath.Size = new System.Drawing.Size(41, 32);
            this.buttonDataPath.TabIndex = 1;
            this.buttonDataPath.Text = "参照";
            this.buttonDataPath.UseVisualStyleBackColor = true;
            this.buttonDataPath.Click += new System.EventHandler(this.buttonDataPath_Click);
            // 
            // buttonCheckFileSubmit
            // 
            this.buttonCheckFileSubmit.Location = new System.Drawing.Point(6, 107);
            this.buttonCheckFileSubmit.Name = "buttonCheckFileSubmit";
            this.buttonCheckFileSubmit.Size = new System.Drawing.Size(41, 32);
            this.buttonCheckFileSubmit.TabIndex = 4;
            this.buttonCheckFileSubmit.Text = "参照";
            this.buttonCheckFileSubmit.UseVisualStyleBackColor = true;
            this.buttonCheckFileSubmit.Click += new System.EventHandler(this.buttonCheckFileSubmit_Click);
            // 
            // buttonCheckStart
            // 
            this.buttonCheckStart.BackColor = System.Drawing.Color.Tomato;
            this.buttonCheckStart.ForeColor = System.Drawing.Color.White;
            this.buttonCheckStart.Location = new System.Drawing.Point(40, 161);
            this.buttonCheckStart.Name = "buttonCheckStart";
            this.buttonCheckStart.Size = new System.Drawing.Size(365, 36);
            this.buttonCheckStart.TabIndex = 6;
            this.buttonCheckStart.Text = "提出するCD/DVDドライブを挿入してください";
            this.buttonCheckStart.UseVisualStyleBackColor = false;
            this.buttonCheckStart.Click += new System.EventHandler(this.buttonCheckStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "チェック証明書出力先：";
            // 
            // textBoxCheckFilePath
            // 
            this.textBoxCheckFilePath.Location = new System.Drawing.Point(53, 107);
            this.textBoxCheckFilePath.Multiline = true;
            this.textBoxCheckFilePath.Name = "textBoxCheckFilePath";
            this.textBoxCheckFilePath.ReadOnly = true;
            this.textBoxCheckFilePath.Size = new System.Drawing.Size(393, 32);
            this.textBoxCheckFilePath.TabIndex = 5;
            this.textBoxCheckFilePath.TabStop = false;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.labelTitle);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(456, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(888, 721);
            this.panelRight.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(888, 667);
            this.panel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabRES);
            this.tabControl1.Controls.Add(this.tabHES);
            this.tabControl1.Controls.Add(this.tabRER);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(888, 667);
            this.tabControl1.TabIndex = 1;
            // 
            // tabRES
            // 
            this.tabRES.Controls.Add(this.dataGridViewRES);
            this.tabRES.Location = new System.Drawing.Point(4, 22);
            this.tabRES.Name = "tabRES";
            this.tabRES.Padding = new System.Windows.Forms.Padding(3);
            this.tabRES.Size = new System.Drawing.Size(880, 641);
            this.tabRES.TabIndex = 0;
            this.tabRES.Text = "RES";
            this.tabRES.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRES
            // 
            this.dataGridViewRES.AllowUserToAddRows = false;
            this.dataGridViewRES.AllowUserToDeleteRows = false;
            this.dataGridViewRES.AllowUserToResizeRows = false;
            this.dataGridViewRES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewRES.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewRES.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewRES.MultiSelect = false;
            this.dataGridViewRES.Name = "dataGridViewRES";
            this.dataGridViewRES.RowHeadersVisible = false;
            this.dataGridViewRES.RowTemplate.Height = 21;
            this.dataGridViewRES.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewRES.Size = new System.Drawing.Size(874, 635);
            this.dataGridViewRES.TabIndex = 2;
            // 
            // tabHES
            // 
            this.tabHES.Controls.Add(this.dataGridViewHES);
            this.tabHES.Location = new System.Drawing.Point(4, 22);
            this.tabHES.Name = "tabHES";
            this.tabHES.Padding = new System.Windows.Forms.Padding(3);
            this.tabHES.Size = new System.Drawing.Size(936, 641);
            this.tabHES.TabIndex = 1;
            this.tabHES.Text = "HES";
            this.tabHES.UseVisualStyleBackColor = true;
            // 
            // dataGridViewHES
            // 
            this.dataGridViewHES.AllowUserToAddRows = false;
            this.dataGridViewHES.AllowUserToDeleteRows = false;
            this.dataGridViewHES.AllowUserToResizeRows = false;
            this.dataGridViewHES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewHES.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewHES.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewHES.MultiSelect = false;
            this.dataGridViewHES.Name = "dataGridViewHES";
            this.dataGridViewHES.RowHeadersVisible = false;
            this.dataGridViewHES.RowTemplate.Height = 21;
            this.dataGridViewHES.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewHES.Size = new System.Drawing.Size(930, 635);
            this.dataGridViewHES.TabIndex = 2;
            // 
            // tabRER
            // 
            this.tabRER.Controls.Add(this.dataGridViewRER);
            this.tabRER.Location = new System.Drawing.Point(4, 22);
            this.tabRER.Name = "tabRER";
            this.tabRER.Padding = new System.Windows.Forms.Padding(3);
            this.tabRER.Size = new System.Drawing.Size(936, 641);
            this.tabRER.TabIndex = 2;
            this.tabRER.Text = "RER";
            this.tabRER.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRER
            // 
            this.dataGridViewRER.AllowUserToAddRows = false;
            this.dataGridViewRER.AllowUserToDeleteRows = false;
            this.dataGridViewRER.AllowUserToResizeRows = false;
            this.dataGridViewRER.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewRER.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewRER.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewRER.MultiSelect = false;
            this.dataGridViewRER.Name = "dataGridViewRER";
            this.dataGridViewRER.RowHeadersVisible = false;
            this.dataGridViewRER.RowTemplate.Height = 21;
            this.dataGridViewRER.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewRER.Size = new System.Drawing.Size(930, 635);
            this.dataGridViewRER.TabIndex = 2;
            // 
            // labelTitle
            // 
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTitle.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.labelTitle.Size = new System.Drawing.Size(888, 54);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ここに都道府県番号と都道府県名、審査年月を記載";
            // 
            // buttonTest
            // 
            this.buttonTest.BackColor = System.Drawing.Color.SkyBlue;
            this.buttonTest.Location = new System.Drawing.Point(411, 174);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(35, 23);
            this.buttonTest.TabIndex = 8;
            this.buttonTest.Text = "ﾃｽﾄ";
            this.buttonTest.UseVisualStyleBackColor = false;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // SubmitDataCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelLeft);
            this.Name = "SubmitDataCheckForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "公立学校共済組合　提出データ最終確認システム";
            this.panelLeft.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabRES.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRES)).EndInit();
            this.tabHES.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHES)).EndInit();
            this.tabRER.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRER)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonDataPath;
        private System.Windows.Forms.TextBox textBoxFolderPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCheckStart;
        private System.Windows.Forms.RichTextBox richTextBoxLogConsole;
        private System.Windows.Forms.Button buttonCheckFileSubmit;
        private System.Windows.Forms.TextBox textBoxCheckFilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabRES;
        private System.Windows.Forms.TabPage tabHES;
        private System.Windows.Forms.TabPage tabRER;
        private System.Windows.Forms.DataGridView dataGridViewRES;
        private System.Windows.Forms.DataGridView dataGridViewHES;
        private System.Windows.Forms.DataGridView dataGridViewRER;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonLogOutput;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ProgressBar progressBarCheck;
        private System.Windows.Forms.Button buttonTest;
    }
}