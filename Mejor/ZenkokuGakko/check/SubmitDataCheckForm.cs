﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko.check
{
    public partial class SubmitDataCheckForm : Form
    {
        private Insurer ins;
        private Pref pref;
        private int cym;
        private int yyyy;
        private int mm;
        private int geemm;
        private int ee;

        private BindingSource bsRES = new BindingSource();
        private BindingSource bsHES = new BindingSource();
        private BindingSource bsRER = new BindingSource();
        private string checkFileName;
        private string drivePath;

        private bool idle;


        public SubmitDataCheckForm(Insurer ins, int cym)
        {
            InitializeComponent();
            this.ins = ins;
            this.cym = cym;
            this.yyyy = cym / 100;
            this.mm = cym - yyyy * 100;
            this.geemm = DateTimeEx.GetGyymmFromAdYM(cym);
            this.ee = (geemm - (geemm / 10000 * 10000)) / 100;

            ins = Insurer.CurrrentInsurer;
            pref = Pref.GetPref(ins.ViewIndex);
            var title = $"【{ins.InsurerName}】 {cym.ToString("0000年00月")}提出データ　最終確認";
            labelTitle.Text = title;

            //データ所在（初期：DVDドライブ）
            foreach (var drive in System.IO.DriveInfo.GetDrives())
            {
                if (drive.DriveType == System.IO.DriveType.CDRom)
                {
                    this.drivePath = drive.Name;
                    textBoxFolderPath.Text = drive.Name;
                    break;
                }
            }
            driveCheck(this.drivePath);

            //証明書出力先（初期：デスクトップ）
            var desktop = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            if (!string.IsNullOrWhiteSpace(desktop))
            {
                checkFileName = $"{pref.ID.ToString("00")}{pref.Name2}_{cym}_最終確認完了証明書.csv";
                textBoxCheckFilePath.Text = $"{desktop}\\{checkFileName}";
            }
        }

        /**
         * CD/DVDドライブの挿入を感知
         */
        private enum WM : uint
        {
            WM_DEVICECHANGE = 0x0219,
        }
        protected override void WndProc(ref Message m)
        {
            switch ((WM)m.Msg)
            {
                case WM.WM_DEVICECHANGE:
                    driveCheck(this.drivePath);
                    break;
            }

            base.WndProc(ref m);
        }
        private void driveCheck(string drivePath)
        {
            if (FileUtility.FolderExist(drivePath))
            {
                buttonCheckStart.BackColor = Color.Tomato;
                buttonCheckStart.Enabled = true;
                buttonCheckStart.Text = "確認開始できます";
                buttonCheckStart.Focus();
            }
            else
            {
                buttonCheckStart.BackColor = Color.Gray;
                buttonCheckStart.Enabled = false;
                buttonCheckStart.Text = "提出するCD/DVDドライブを挿入してください";
            }
        }

        /**
         * データフォルダ所在
         */
        private void buttonDataPath_Click(object sender, EventArgs e)
        {
            //20211116150048 furukawa st ////////////////////////
            //folderbrowser使いにくい
            
            var f = new OpenDirectoryDiarog();
            f.ShowDialog();
            textBoxFolderPath.Text = f.Name;

            //using (var f = new FolderBrowserDialog())
            //{

            //    f.ShowDialog();
            //    textBoxFolderPath.Text = f.SelectedPath;
            //}
            //20211116150048 furukawa ed ////////////////////////
        }

        /**
         * チェック証明書発行場所
         */
        private void buttonCheckFileSubmit_Click(object sender, EventArgs e)
        {
            using (var f = new SaveFileDialog())
            {
                f.FileName = checkFileName;
                f.Filter = "CSVファイル(*.csv;)|*.csv;";
                var result = f.ShowDialog();
                if (result == DialogResult.OK) textBoxCheckFilePath.Text = f.FileName;
                else textBoxCheckFilePath.Text = "";
            }
        }

        /**
         * ログ出力
         */
        private void buttonLogOutput_Click(object sender, EventArgs e)
        {
            using (var f = new SaveFileDialog())
            {
                f.FileName = $"{pref.ID.ToString("00")}{pref.Name2}_{cym}_最終確認_チェックログ.csv";
                f.Filter = "CSVファイル(*.csv;)|*.csv;";
                var result = f.ShowDialog();
                if (result == DialogResult.OK)
                {
                    try
                    {
                        using (var sw = new System.IO.StreamWriter(f.FileName, false, System.Text.Encoding.GetEncoding("shift_jis")))
                        {
                            sw.WriteLine(richTextBoxLogConsole.Text);
                        }
                        MessageBox.Show($"{f.FileName}\r\nに出力されました");
                    }
                    catch
                    {
                        MessageBox.Show("出力中にエラーが発生しました", "出力失敗", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        /**
         * 提出前確認用
         */
        private void buttonTest_Click(object sender, EventArgs e)
        {
            buttonCheckStart_Click(null, null);
        }

        /**
         * 確認開始
         */
        private void buttonCheckStart_Click(object sender, EventArgs e)
        {
            if (idle) return;
            try
            {
                idle = true;

                var con = new LogConsole(richTextBoxLogConsole);
                bsRES.Clear();
                bsHES.Clear();
                bsRER.Clear();
                updateTabRES(0);
                updateTabHES(0);
                updateTabRER(0);

                //#################################################
                //
                //パスチェック
                //
                //#################################################

                con.WriteLine("データの所在を確認しています...", LogConsole.LOG_TYPE.Info);

                string dPath, oPath;
                if (!checkExistPath(con, out dPath, out oPath))
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);

                //#################################################
                //
                //フォルダ内構造チェック（第一階層）
                //
                //#################################################

                con.WriteLine("第一フォルダの構造を確認しています...", LogConsole.LOG_TYPE.Info);
                string prefFolderPath;
                string listFolderPath;
                if (!checkRootFolder(con, dPath, out prefFolderPath, out listFolderPath))
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);

                //#################################################
                //
                //フォルダ内チェック（都道府県フォルダ）
                //
                //#################################################

                //構造の確認
                con.WriteLine("支部フォルダの構造を確認しています...", LogConsole.LOG_TYPE.Info);
                string mainPath;
                string[] images, csvs;
                if (!checkPrefFolderStep(con, prefFolderPath, out mainPath, out images, out csvs))
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);

                //CSV取得
                initProgress(3);
                con.WriteLine("RESデータを構築しています...", LogConsole.LOG_TYPE.Info);
                updateProgress();
                var csvRES = CommonTool.CsvImportShiftJisAllRecordByRecordSplitCRLFAndValueSplitComma(csvs[0]);
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                con.WriteLine("HESデータを構築しています...", LogConsole.LOG_TYPE.Info);
                updateProgress();
                var csvHES = CommonTool.CsvImportShiftJisAllRecordByRecordSplitCRLFAndValueSplitComma(csvs[1]);
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                List<string[]> csvRER = null;
                if (csvs[2] != null)
                {
                    con.WriteLine("RERデータを構築しています...", LogConsole.LOG_TYPE.Info);
                    updateProgress();
                    csvRER = CommonTool.CsvImportShiftJisAllRecordByRecordSplitCRLFAndValueSplitComma(csvs[2]);
                    con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                }
                closeProgress();

                //RES値の確認
                con.WriteLine("RESの値を確認しています...", LogConsole.LOG_TYPE.Info);
                initProgress(csvRES.Count);
                var listRES = new List<RES>();
                var errListRES = new List<RES>();
                RES rcRES;
                var rowRES = 1;
                string emRES;
                foreach (var record in csvRES)
                {
                    if (rowRES == csvRES.Count)
                    {
                        if (record.Length == 1 && record[0] == "")
                        {
                            break;//最終行だけは改行のみでも可
                        }
                    }
                    rcRES = new RES(rowRES++, record);
                    listRES.Add(rcRES);
                    if (!rcRES.AllValueCheck(out emRES))
                    {
                        con.WriteLine(emRES, LogConsole.LOG_TYPE.Error);
                        errListRES.Add(rcRES);
                    }
                    updateProgress();
                }
                if (errListRES.Count == 0) con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                else con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                closeProgress();

                //HES値の確認
                con.WriteLine("HESの値を確認しています...", LogConsole.LOG_TYPE.Info);
                initProgress(csvHES.Count);
                var listHES = new List<HES>();
                var errListHES = new List<HES>();
                HES rcHES;
                int rowHES = 1;
                string emHES;
                foreach (var record in csvHES)
                {
                    if (rowHES == csvHES.Count)
                    {
                        if (record.Length == 1 && record[0] == "")
                        {
                            break;//最終行だけは改行のみでも可
                        }
                    }
                    rcHES = new HES(rowHES++, record);
                    listHES.Add(rcHES);
                    if (!rcHES.AllValueCheck(out emHES))
                    {
                        con.WriteLine(emHES, LogConsole.LOG_TYPE.Error);
                        errListHES.Add(rcHES);
                    }
                    updateProgress();
                }
                if (errListHES.Count == 0) con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                else con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                closeProgress();

                //RER値の確認            
                var listRER = new List<RER>();
                var errListRER = new List<RER>();
                var existRER = false;
                if (csvRER != null)
                {
                    con.WriteLine("RERの値を確認しています...", LogConsole.LOG_TYPE.Info);
                    initProgress(csvRER.Count);
                    RER rcRER;
                    int rowRER = 1;
                    string emRER;
                    foreach (var record in csvRER)
                    {
                        if (rowRER == csvRER.Count)
                        {
                            if (record.Length == 1 && record[0] == "")
                            {
                                break;//最終行だけは改行のみでも可
                            }
                        }
                        rcRER = new RER(rowRER++, record);
                        listRER.Add(rcRER);
                        if (!rcRER.AllValueCheck(out emRER))
                        {
                            con.WriteLine(emRER, LogConsole.LOG_TYPE.Error);
                            errListRER.Add(rcRER);
                        }
                        updateProgress();
                    }
                    existRER = true;
                    if (errListRER.Count == 0) con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    else con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    closeProgress();
                }

                //リスト表示
                bsRES.DataSource = errListRES;
                dataGridViewRES.DataSource = bsRES;
                dataGridViewRES.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridViewRES.Columns[nameof(Columns.C_GEEMM)].Visible = false;
                dataGridViewRES.Columns[nameof(Columns.Batch)].Visible = false;
                dataGridViewRES.Columns[nameof(Columns.Numbering)].Visible = false;
                tabControl1.TabPages[0].Text = $"エラー：RES( {errListRES.Count} )";
                bsHES.DataSource = errListHES;
                dataGridViewHES.DataSource = bsHES;
                dataGridViewHES.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridViewHES.Columns[nameof(Columns.C_GEEMM)].Visible = false;
                dataGridViewHES.Columns[nameof(Columns.Batch)].Visible = false;
                dataGridViewHES.Columns[nameof(Columns.Numbering)].Visible = false;
                tabControl1.TabPages[1].Text = $"エラー：HES( {errListHES.Count} )";
                bsRER.DataSource = errListRER;
                dataGridViewRER.DataSource = bsRER;
                dataGridViewRER.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridViewRER.Columns[nameof(Columns.C_GEEMM)].Visible = false;
                dataGridViewRER.Columns[nameof(Columns.Batch)].Visible = false;
                dataGridViewRER.Columns[nameof(Columns.Numbering)].Visible = false;
                tabControl1.TabPages[2].Text = $"エラー：RER( {errListRER.Count} )";

                if (errListRES.Count != 0 || errListHES.Count != 0 || errListRER.Count != 0)
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var check1 = true;

                //RESのナンバリング連番の確認
                con.WriteLine("RESのﾅﾝﾊﾞﾘﾝｸﾞの連続性を確認しています...", LogConsole.LOG_TYPE.Info);
                if (!checkContinueNumbering(ins, con, listRES, errListRES))
                {
                    updateTabRES(errListRES.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check1 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //RERのナンバリング連番の確認
                if (existRER)
                {
                    con.WriteLine("RERのﾅﾝﾊﾞﾘﾝｸﾞの連続性を確認しています...", LogConsole.LOG_TYPE.Info);
                    if (!checkContinueNumbering(ins, con, listRER, errListRER))
                    {
                        updateTabRER(errListRER.Count);
                        con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                        check1 = false;
                    }
                    else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    closeProgress();
                }

                //RES,HES,RER入力年月同一性チェック
                con.WriteLine("各ファイル、各データの入力年月の同一性を確認しています...", LogConsole.LOG_TYPE.Info);
                if (!samenessRESHESRER(con, geemm, listRES, listHES, listRER, errListRES, errListHES, errListRER))
                {
                    updateTabRES(errListRES.Count);
                    updateTabHES(errListHES.Count);
                    updateTabRER(errListRER.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check1 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //RES,RER保険者番号統一チェック
                con.WriteLine("RESの保険者番号の統一性を確認しています...", LogConsole.LOG_TYPE.Info);
                if (!checkHokensyaNumRES(con, ins.InsNumber, listRES, listRER, errListRES, errListRER))
                {
                    updateTabRES(errListRES.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check1 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //RES,HES,RER重複確認
                con.WriteLine("各データの重複を確認しています...", LogConsole.LOG_TYPE.Info);
                var ovC = true;
                ovC &= checkOverlap(con, listRES, errListRES);
                ovC &= checkOverlap(con, listHES, errListHES);
                if (existRER) ovC &= checkOverlap(con, listRER, errListRER);
                if (!ovC)
                {
                    updateTabRES(errListRES.Count);
                    updateTabHES(errListHES.Count);
                    updateTabRER(errListRER.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check1 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                // >>> 途中終了
                if (!check1)
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.Info);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var check2 = true;

                //RES,HESマッチング
                con.WriteLine("RESとHESのマッチングを確認しています...", LogConsole.LOG_TYPE.Info);
                if (!matchingRESHES(con, listRES, listHES, errListHES))
                {
                    closeProgress();
                    updateTabHES(errListHES.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check2 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //RES、RER画像ファイル存在チェック
                con.WriteLine("データに該当する画像ファイルの存在を確認しています...", LogConsole.LOG_TYPE.Info);
                var yyMM = $"{yyyy.ToString().Substring(2, 2)}{(cym % 100).ToString("00")}";
                var efC = true;
                efC &= existFileRES(con, yyMM, mainPath, listRES, images, errListRES);
                if (existRER) efC &= existFileRER(con, yyMM, mainPath, listRER, images, errListRER);
                if (!efC)
                {
                    closeProgress();
                    updateTabRES(errListRES.Count);
                    updateTabRER(errListRER.Count);
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check2 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //画像ファイル名のチェック
                con.WriteLine("画像ファイル名から該当するデータの存在を確認しています...", LogConsole.LOG_TYPE.Info);
                if (!checkImageName(con, images, listRES, listRER))
                {
                    closeProgress();
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check2 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                //RES,RERと画像件数のチェック
                con.WriteLine("データ件数と申請書画像の件数を確認しています...", LogConsole.LOG_TYPE.Info);
                if (!checkImageCount(con, images, listRES, listRER))
                {
                    closeProgress();
                    con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.BoldError);
                    check2 = false;
                }
                else con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                closeProgress();

                // >>> 途中終了
                if (!check2)
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.Info);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //#################################################
                //
                //フォルダ内構造チェック（各種一覧表フォルダ内）
                //
                //#################################################

                //構造の確認
                con.WriteLine("各種一覧表フォルダの構造を確認しています...", LogConsole.LOG_TYPE.Info);
                string pSyokai;
                string pKekka;
                string pHenrei;
                string pSiharai;
                if (!checkListFolderStep(con, listFolderPath, out pSyokai, out pKekka, out pHenrei, out pSiharai))
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                bool exSyokai = pSyokai != null;
                bool exKekka = pKekka != null;
                bool exHenrei = pHenrei != null;
                bool exSiharai = pSiharai != null;
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);

                var check3 = true;

                //⑦返戻対象者データ内の年月合致確認
                if (exHenrei)
                {
                    con.WriteLine("返戻対象者ファイルを確認しています...", LogConsole.LOG_TYPE.Info);
                    if (checkHenreiYMN(con, pHenrei))
                    {
                        con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    }
                    else
                    {
                        check3 = false;
                        con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.Error);
                    }
                }

                //⑧返戻対象者（支払済み分）データ内の年月合致確認
                if (exSiharai)
                {
                    con.WriteLine("返戻対象者（支払済み分）ファイルを確認しています...", LogConsole.LOG_TYPE.Info);
                    if (checkSiharaiYMN(con, pSiharai))
                    {
                        con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    }
                    else
                    {
                        check3 = false;
                        con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.Error);
                    }
                }

                //文書照会結果データ内の年月合致確認
                if (exKekka)
                {
                    con.WriteLine("文書照会結果ファイルを確認しています...", LogConsole.LOG_TYPE.Info);
                    if (checkKekkaYMN(con, pKekka))
                    {
                        con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    }
                    else
                    {
                        check3 = false;
                        con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.Error);
                    }
                }

                //照会対象者データ内の年月合致確認
                if (exSyokai)
                {
                    con.WriteLine("照会対象者ファイルを確認しています...", LogConsole.LOG_TYPE.Info);
                    if (checkSyokaiYMN(con, pSyokai))
                    {
                        con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                    }
                    else
                    {
                        check3 = false;
                        con.WriteLine("　>>>　NG", LogConsole.LOG_TYPE.Error);
                    }
                }

                if (!check3)
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.BoldError);
                    MessageBox.Show("エラー箇所があります。\r\nログを確認してください。", "失敗",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //#################################################
                //
                //証明書発行
                //
                //#################################################

                con.WriteLine("証明書を発行しています...", LogConsole.LOG_TYPE.Info);

                //各種数取得
                var countRES = listRES.Count;
                var countHES = listHES.Count;
                var countRER = existRER ? listRER.Count : -1;
                var countFiles = getFileCounts(images);
                var countFile = countFiles[0];
                var countNextFile = countFiles[1];
                var fnSyokai = System.IO.Path.GetFileName(pSyokai);
                var fnKekka = System.IO.Path.GetFileName(pKekka);
                var fnHenrei = System.IO.Path.GetFileName(pHenrei);
                var fnSiharai = System.IO.Path.GetFileName(pSiharai);

                initProgress(1);
                if (!createCheckCertificateFile(
                    oPath, ins, pref, geemm, countRES, countHES, countRER, countFile, countNextFile,
                    fnSyokai, fnKekka, fnHenrei, fnSiharai))
                {
                    closeProgress();
                    con.WriteLine("結果：失敗、強制終了", LogConsole.LOG_TYPE.Info);
                    MessageBox.Show("証明書発行中にエラーが発生しました。\r\n再度やり直してください。", "失敗",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                con.WriteLine("　>>>　OK", LogConsole.LOG_TYPE.Info);
                updateProgress();
                closeProgress();

                // >>> 終了
                con.WriteLine("チェック終了", LogConsole.LOG_TYPE.Bold);
                con.WriteLine("結果：すべて問題ありません", LogConsole.LOG_TYPE.Bold);
                MessageBox.Show("チェック完了。\r\n証明書を発行しました。", "成功");
            }
            finally
            {
                idle = false;
            }
        }

        private void updateTabRES(int count)
        {
            bsRES.ResetBindings(false);
            tabControl1.TabPages[0].Text = $"エラー：RES（ {count} ）";
        }
        private void updateTabHES(int count)
        {
            bsHES.ResetBindings(false);
            tabControl1.TabPages[1].Text = $"エラー：HES（ {count} ）";
        }
        private void updateTabRER(int count)
        {
            bsRER.ResetBindings(false);
            tabControl1.TabPages[2].Text = $"エラー：RER（ {count} ）";
        }

        private bool createCheckCertificateFile(string filename, Insurer ins, Pref pref, int GEEMM,
            int resCount, int hesCount, int rerCount, int fileCount, int nextFileCount, 
            string fnSyokai, string fnKekka, string fnHenrei, string fnSiharai)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(filename, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    var rowPref1 = $"支部番号,{pref.ID.ToString("00")}";
                    var rowPref2 = $"支部名,{pref.Name2}";
                    var rowIns = $"保険者番号,{ins.InsNumber.PadLeft(8, '0')}";
                    var rowC_GEEMM = $"入力年月,{GEEMM}";
                    var rowCreateDate = $"確認日時,{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}";
                    var rowUID = $"出力ID,{User.CurrentUser.UserID}";

                    var rowBlank = "";

                    var valRES = resCount != -1 ? resCount.ToString() : "なし";
                    var valRER = rerCount != -1 ? rerCount.ToString() : "なし";
                    var valHES = hesCount != -1 ? hesCount.ToString() : "なし";
                    var rowRES = $"データ（RES）,{valRES}";
                    var rowRER = $"データ（RER）,{valRER}";
                    var rowHES = $"データ（HES）,{valHES}";
                    var rowImage1 = $"画像（申請書）,{fileCount}";
                    var rowImage2 = $"画像（続紙）,{nextFileCount}";

                    var valHenrei = fnHenrei ?? "なし";
                    var valSiharai = fnSiharai ?? "なし";
                    var valKekka = fnKekka ?? "なし";
                    var valSyokai = fnSyokai ?? "なし";
                    var rowHenrei = $"返戻対象者,{valHenrei}";
                    var rowSiharai = $"返戻対象者（支払済み分）,{valSiharai}";
                    var rowKekka = $"文書照会結果,{valKekka}";
                    var rowSyokai = $"照会対象者,{valSyokai}";
                    

                    sw.WriteLine(rowPref1);
                    sw.WriteLine(rowPref2);
                    sw.WriteLine(rowIns);
                    sw.WriteLine(rowC_GEEMM);
                    sw.WriteLine(rowCreateDate);
                    sw.WriteLine(rowUID);
                    sw.WriteLine(rowBlank);
                    sw.WriteLine(rowRES);
                    sw.WriteLine(rowRER);
                    sw.WriteLine(rowHES);
                    sw.WriteLine(rowImage1);
                    sw.WriteLine(rowImage2);
                    sw.WriteLine(rowBlank);
                    sw.WriteLine(rowHenrei);
                    sw.WriteLine(rowSiharai);
                    sw.WriteLine(rowKekka);
                    sw.WriteLine(rowSyokai);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private int toInt(string number)
        {
            if (string.IsNullOrWhiteSpace(number)) return -1;
            int result;
            if (!int.TryParse(number, out result)) return -1;
            return result;
        }

        private long toLong(string number)
        {
            if (string.IsNullOrWhiteSpace(number)) return -1;
            long result;
            if (!long.TryParse(number, out result)) return -1;
            return result;
        }

        private void initProgress(int max)
        {
            progressBarCheck.Minimum = 0;
            progressBarCheck.Maximum = max;
            closeProgress();
        }

        private void closeProgress()
        {
            progressBarCheck.Value = 0;
        }

        private void updateProgress()
        {
            progressBarCheck.Value++;
        }

        private string getIndent(int count)
        {
            return "".PadLeft(count, '　');
        }

        private class LogConsole
        {
            public enum LOG_TYPE { Info=1, Error=2, Bold=3, BoldError=4, Caution=5 }
            private List<string> logs = new List<string>();
            private RichTextBox tb;
            public LogConsole(RichTextBox textbox)
            {
                tb = textbox;
                tb.Clear();
            }
            public LogConsole WriteLine(string text, LOG_TYPE type)
            {
                var log = text.EndsWith("\r\n") ? text.Substring(0, text.Length - 2) : text;
                log = $"{log} ";

                logs.Add(log);
                tb.HideSelection = false;
                var currIndex = 0;
                switch (type)
                {
                    case LOG_TYPE.Info:
                        tb.AppendText(log);
                        break;
                    case LOG_TYPE.Error:
                        currIndex = tb.TextLength;
                        tb.AppendText(log);
                        tb.Select(currIndex, log.Length);
                        tb.SelectionColor = Color.Red;
                        tb.SelectionBackColor = Color.Pink;
                        tb.Select(tb.TextLength, 0);
                        break;
                    case LOG_TYPE.Bold:
                        currIndex = tb.TextLength;
                        tb.AppendText(log);
                        tb.Select(currIndex, log.Length);
                        tb.SelectionColor = Color.Black;
                        tb.SelectionBackColor = Color.Cyan;
                        tb.SelectionFont = new Font(tb.Font, FontStyle.Bold);
                        tb.Select(tb.TextLength, 0);
                        break;
                    case LOG_TYPE.BoldError:
                        currIndex = tb.TextLength;
                        tb.AppendText(log);
                        tb.Select(currIndex, log.Length);
                        tb.SelectionColor = Color.Red;
                        tb.SelectionBackColor = Color.Pink;
                        tb.SelectionFont = new Font(tb.Font, FontStyle.Bold);
                        tb.Select(tb.TextLength, 0);
                        break;
                    case LOG_TYPE.Caution:
                        currIndex = tb.TextLength;
                        tb.AppendText(log);
                        tb.Select(currIndex, log.Length);
                        tb.SelectionColor = Color.Black;
                        tb.SelectionBackColor = Color.Yellow;
                        tb.SelectionFont = new Font(tb.Font, FontStyle.Bold);
                        tb.Select(tb.TextLength, 0);
                        break;
                }
                tb.AppendText(Environment.NewLine);
                tb.Refresh();
                return this;
            }
        }
    }
}
