﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor.ZenkokuGakko.check
{
    class Property
    {
        public enum TYPE { NUMERIC, TEXT, GEEMM, GEEMMDD, KUMIAIINBANGO, SELECT, EXTRA }
        public string Name;
        public string Val;
        public int MaxLength;
        public int RequiredLength;
        public long MaxValue;
        public string[] SelectableValues;
        public bool Required;
        public TYPE Type;
        //
        public Property() { Val = ""; }
        //
        public Property(string name, string val, TYPE type)
            : this(name, val, -1, -1, -1, true, type) { }
        //
        public Property(string name, string val, int requiredLength, TYPE type)
            : this(name, val, -1, requiredLength, -1, true, type) { }
        //
        public Property(string name, string val, long maxValue, bool required)
            : this(name, val, -1, -1, maxValue, required, TYPE.NUMERIC) { }
        //
        public Property(string name, string val, int requiredLength, long maxValue)
            : this(name, val, -1, requiredLength, maxValue, true, TYPE.NUMERIC) { }
        //
        public Property(string name, string val, int maxLength, bool required, TYPE type)
            : this(name, val, maxLength, -1, -1, required, type) { }
        //
        public Property(string name, string val, bool required, TYPE type)
            : this(name, val, -1, -1, -1, required, type) { }
        //
        public Property(string name, string val, string[] selectableValues)
        {
            Name = name;
            Val = val;
            MaxLength = -1;
            RequiredLength = -1;
            MaxValue = -1;
            SelectableValues = selectableValues;
            Required = false;
            Type = TYPE.SELECT;
        }
        //
        public Property(string name, string val, int maxLength, int requiredLength, long maxValue, bool required, TYPE type)
        {
            Name = name;
            Val = val;
            MaxLength = maxLength;
            RequiredLength = requiredLength;
            MaxValue = maxValue;
            Required = required;
            Type = type;
        }
        public bool ValueCheck(int row, out string message)
        {
            message = "";
            var check = true;

            //取得チェック
            if (Val == null)
            {
                check = false;
                message += $"({row}行目)[{Name}] 値が取得できません\r\n";
                return check;
            }
            //文字数チェック
            if (MaxLength != -1 && Val.Length > MaxLength)
            {
                check = false;
                message += $"({row}行目)[{Name}] 最大文字数を超えています（規定：{MaxLength}　値：{Val.Length}）\r\n";
            }
            else if (MaxValue != -1 && Val.Length > MaxValue.ToString().Length)
            {
                check = false;
                message += $"({row}行目)[{Name}] 最大文字数を超えています（規定：{MaxValue.ToString().Length}　値：{Val.Length}）\r\n";
            }
            //必須チェック
            if (Required)
            {
                if (RequiredLength != -1 && Val.Length != RequiredLength)
                {
                    check = false;
                    message += $"({row}行目)[{Name}] 必要桁数ではありません（規定：{RequiredLength}　値：{Val.Length}）\r\n";
                }
                else if (RequiredLength != 0 && string.IsNullOrWhiteSpace(Val))
                {
                    check = false;
                    message += $"({row}行目)[{Name}] 値がありません\r\n";
                }
            }
            else
            {
                //任意
                if (Type != TYPE.SELECT && RequiredLength != -1)
                {
                    if (RequiredLength != -1 && Val.Length != RequiredLength)
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] 必要桁数ではありません（規定：{RequiredLength}　値：{Val.Length}）\r\n";
                    }
                }
                if (Type != TYPE.SELECT && Val == "")
                {
                    return true;
                }
            }
            //数値＆最大値チェック
            if (Type == TYPE.NUMERIC)
            {
                if (!string.IsNullOrEmpty(Val))
                {
                    long valLong;
                    if (!long.TryParse(Val, out valLong))
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] 数値ではありません（値：{Val}）\r\n";
                    }
                    else if (MaxValue != -1 && MaxValue < valLong)
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] 最大値を超えています（規定：{MaxValue}　値：{Val}）\r\n";
                    }
                    else if (valLong < 0)
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] ０未満です（値：{Val}）\r\n";
                    }
                }
            }
            //日付形式チェック
            else if (Type == TYPE.GEEMM || Type == TYPE.GEEMMDD)
            {
                if (!string.IsNullOrEmpty(Val))
                {
                    int valInt;
                    if (!int.TryParse(Val, out valInt))
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] 日付に文字が含まれています（値：{Val}）\r\n";
                    }
                    else
                    {
                        var GEEMMDD = Type == TYPE.GEEMM ? (valInt * 100 + 1) : valInt;
                        if (DateTimeEx.GetDateFromJInt7(GEEMMDD) == DateTime.MinValue)
                        {
                            check = false;
                            message += $"({row}行目)[{Name}] 年月に変換できません（値：{Val}）\r\n";
                        }
                    }
                }
            }
            //組合員番号チェック
            else if (Type == TYPE.KUMIAIINBANGO)
            {
                if (!string.IsNullOrEmpty(Val))
                {
                    if (!Regex.IsMatch(Val, "^[A-Z0-9]*$"))//大文字英数字のみで構成されている
                    {
                        check = false;
                        message += $"({row}行目)[{Name}] 不正な文字が含まれています（値：{Val}）\r\n";
                    }
                }
            }
            //選択式チェック
            else if (Type == TYPE.SELECT)
            {
                if (!SelectableValues.Contains(Val))
                {
                    check = false;
                    if (Val == "") message += $"({row}行目)[{Name}] 選択可能な値ではありません（値：空欄）\r\n";
                    else message += $"({row}行目)[{Name}] 選択可能な値ではありません（値：{Val}）\r\n";
                }
            }

            return check;
        }
    }
}
