﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.ZenkokuGakko.check
{
    partial class SubmitDataCheckForm
    {
        /// <summary>
        /// RES,HESのマッチング確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="listRES"></param>
        /// <param name="listHES"></param>
        /// <param name="errListHES"></param>
        /// <returns></returns>
        private bool matchingRESHES(LogConsole con, List<RES> listRES, List<HES> listHES, List<HES> errListHES)
        {
            var result = true;
            initProgress(listHES.Count);

            RES matchRC;
            HES searchRC;
            for (var row = 0; row < listHES.Count; row++)
            {
                searchRC = listHES[row];
                matchRC = listRES.Find(rc =>
                {
                    var c = true;
                    c &= rc.ﾊﾞｯﾁ == searchRC.ﾊﾞｯﾁ;
                    c &= rc.ﾅﾝﾊﾞﾘﾝｸﾞ == searchRC.ﾅﾝﾊﾞﾘﾝｸﾞ;
                    return c;
                });
                if (matchRC == null)
                {
                    con.WriteLine(
                        $"HES({row + 1}行目)[{searchRC.ﾊﾞｯﾁ}、{searchRC.ﾅﾝﾊﾞﾘﾝｸﾞ}] RESファイル内に該当データがありません",
                        LogConsole.LOG_TYPE.Error);
                    errListHES.Add(searchRC);
                    result = false;
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// RESの画像存在確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="yyMM"></param>
        /// <param name="mainPath"></param>
        /// <param name="listRES"></param>
        /// <param name="images"></param>
        /// <param name="errList"></param>
        /// <returns></returns>
        private bool existFileRES(LogConsole con, string yyMM, string mainPath,
            List<RES> listRES, string[] images, List<RES> errList)
        {
            var result = true;
            initProgress(listRES.Count);

            string path;
            for (var row = 0; row < listRES.Count; row++)
            {
                if (!existFile(yyMM, mainPath, listRES[row], images, out path))
                {
                    con.WriteLine($"RES({row + 1}行目) 画像ファイルが存在しません（パス：{path}）", LogConsole.LOG_TYPE.Error);
                    errList.Add(listRES[row]);
                    result = false;
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// RERの画像存在確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="yyMM"></param>
        /// <param name="mainPath"></param>
        /// <param name="listRER"></param>
        /// <param name="images"></param>
        /// <param name="errList"></param>
        /// <returns></returns>
        private bool existFileRER(LogConsole con, string yyMM, string mainPath,
            List<RER> listRER, string[] images, List<RER> errList)
        {
            var result = true;
            initProgress(listRER.Count);

            string path;
            for (var row = 0; row < listRER.Count; row++)
            {
                if (!existFile(yyMM, mainPath, listRER[row], images, out path))
                {
                    con.WriteLine($"RER({row + 1}行目) 画像ファイルが存在しません（パス：{path}）", LogConsole.LOG_TYPE.Error);
                    errList.Add(listRER[row]);
                    result = false;
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// 画像存在確認
        /// </summary>
        /// <param name="yyMM"></param>
        /// <param name="mainPath"></param>
        /// <param name="column"></param>
        /// <param name="images"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool existFile(string yyMM, string mainPath, Columns column, string[] images, out string path)
        {
            path = $"{mainPath}\\{column.Batch.Val}\\{yyMM}{column.Batch.Val}{column.Numbering.Val}01.tif";
            if (!images.Contains(path))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// ファイル数とデータの件数確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="images"></param>
        /// <param name="listRES"></param>
        /// <param name="listRER"></param>
        /// <returns></returns>
        private bool checkImageCount(LogConsole con, string[] images, List<RES> listRES, List<RER> listRER)
        {
            var result = true;
            initProgress(1);

            var imCount = getFileCounts(images)[0];
            var rsCount = listRES.Count;
            var rrCount = listRER.Count;
            if (imCount != (rsCount + rrCount))
            {
                con.WriteLine(
                    $"申請書画像の件数({imCount})とデータ件数({rsCount + rrCount})が一致しません。",
                    LogConsole.LOG_TYPE.Error);
                result = false;
            }
            updateProgress();
            return result;
        }

        /// <summary>
        /// ファイル数確認
        /// </summary>
        /// <param name="images"></param>
        /// <returns></returns>
        private int[] getFileCounts(string[] images)
        {
            var result = new int[2];
            result[0] = 0;
            result[1] = 0;
            string name;
            foreach (var f in images)
            {
                name = FileUtility.GetFileName(f);
                if (name.EndsWith("01")) result[0]++;
                else result[1]++;
            }
            return result;
        }

    }
}
