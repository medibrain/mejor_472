﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor.ZenkokuGakko.check
{
    partial class SubmitDataCheckForm
    {
        /// <summary>
        /// ナンバリングの連番確認
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ins"></param>
        /// <param name="con"></param>
        /// <param name="list"></param>
        /// <param name="errList"></param>
        /// <returns></returns>
        private bool checkContinueNumbering<T>(Insurer ins, LogConsole con, List<T> list, List<T> errList) where T : Columns
        {
            if (Common.IsBatchLength6Insrers)
            {
                //東京,大阪版
                return checkContinueNumbering_BacthLength6AndNumberingLength4(con, list, errList);
            }

            var result = true;
            initProgress(list.Count);

            var currBatch = "";
            int numbering = 0;
            foreach (var item in list)
            {
                if (currBatch != item.ﾊﾞｯﾁ)
                {
                    currBatch = item.ﾊﾞｯﾁ;
                    numbering = 1;
                    if (item.ﾅﾝﾊﾞﾘﾝｸﾞ != numbering.ToString("000000"))
                    {
                        con.WriteLine($"{typeof(T).Name}({item.Row}行目)[ﾅﾝﾊﾞﾘﾝｸﾞ] 000001から始まっていません（値：{item.ﾅﾝﾊﾞﾘﾝｸﾞ}）", LogConsole.LOG_TYPE.Error);
                        errList.Add(item);
                        result = false;
                    }
                }
                else
                {
                    numbering++;
                    if (item.ﾅﾝﾊﾞﾘﾝｸﾞ != numbering.ToString("000000"))
                    {
                        con.WriteLine($"{typeof(T).Name}({item.Row}行目)[ﾅﾝﾊﾞﾘﾝｸﾞ] 連番ではありません（値：{item.ﾅﾝﾊﾞﾘﾝｸﾞ}）", LogConsole.LOG_TYPE.Error);
                        errList.Add(item);
                        result = false;
                    }
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// 東京,大阪式ナンバリング確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="listRES"></param>
        /// <param name="errListRES"></param>
        /// <returns></returns>
        private bool checkContinueNumbering_BacthLength6AndNumberingLength4<T>(LogConsole con, List<T> list, List<T> errList) where T : Columns
        {
            var result = true;
            initProgress(list.Count);

            string currBatch = null;
            string currBatchPartNumbering = null;
            int numbering = 0;
            foreach (var item in list)
            {
                var itemBatchPartNumbering = item.ﾅﾝﾊﾞﾘﾝｸﾞ.Substring(0, 2);

                if (currBatch != item.ﾊﾞｯﾁ || currBatchPartNumbering != itemBatchPartNumbering) //次のバッチ
                {
                    currBatch = item.ﾊﾞｯﾁ;
                    currBatchPartNumbering = itemBatchPartNumbering;
                    numbering = 1;

                    if (item.ﾅﾝﾊﾞﾘﾝｸﾞ != $"{currBatchPartNumbering}{numbering.ToString("0000")}") //BBNNNN
                    {
                        con.WriteLine($"{typeof(T).Name}({item.Row}行目)[ﾅﾝﾊﾞﾘﾝｸﾞ] {currBatchPartNumbering}0001から始まっていません（値：{item.ﾅﾝﾊﾞﾘﾝｸﾞ}）", LogConsole.LOG_TYPE.Error);
                        errList.Add(item);
                        result = false;
                    }
                }
                else
                {
                    numbering++;
                    if (item.ﾅﾝﾊﾞﾘﾝｸﾞ != $"{currBatchPartNumbering}{numbering.ToString("0000")}")
                    {
                        con.WriteLine($"{typeof(T).Name}({item.Row}行目)[ﾅﾝﾊﾞﾘﾝｸﾞ] 連番ではありません（値：{item.ﾅﾝﾊﾞﾘﾝｸﾞ}）", LogConsole.LOG_TYPE.Error);
                        errList.Add(item);
                        result = false;
                    }
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// 重複確認
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="con"></param>
        /// <param name="list"></param>
        /// <param name="errList"></param>
        /// <returns></returns>
        private bool checkOverlap<T>(LogConsole con, List<T> list, List<T> errList) where T : Columns
        {
            var dic = new Dictionary<string, List<Columns>>();
            initProgress(list.Count);
            foreach (var item in list)
            {
                var key = $"{item.ﾊﾞｯﾁ}-{item.ﾅﾝﾊﾞﾘﾝｸﾞ}";
                if (!dic.ContainsKey(key)) dic.Add(key, new List<Columns>());
                dic[key].Add(item);
                updateProgress();
            }
            var ovList = dic.Values.ToList().FindAll(item => item.Count > 1);
            ovList.ForEach(ovs => ovs.ForEach(ov => errList.Add((T)ov)));
            return printOverlap(con, ovList, typeof(T).Name);
        }

        /// <summary>
        /// 重複項目をログに記載
        /// </summary>
        /// <param name="con"></param>
        /// <param name="ovList"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool printOverlap(LogConsole con, List<List<Columns>> ovList, string type)
        {
            if (ovList.Count != 0)
            {
                string m;
                foreach (var item in ovList)
                {
                    m = "";
                    foreach (var res in item)
                    {
                        m += $"{res.Row}、";
                    }
                    con.WriteLine($"{type}：{m}行目が重複しています", LogConsole.LOG_TYPE.Error);
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// RES,HES,RERの入力年月同一性確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="GEEMM"></param>
        /// <param name="listRES"></param>
        /// <param name="listHES"></param>
        /// <param name="listRER"></param>
        /// <param name="errListRES"></param>
        /// <param name="errListHES"></param>
        /// <param name="errListRER"></param>
        /// <returns></returns>
        private bool samenessRESHESRER(LogConsole con, int GEEMM,
            List<RES> listRES, List<HES> listHES, List<RER> listRER,
            List<RES> errListRES, List<HES> errListHES, List<RER> errListRER)
        {
            var result = true;
            var master = GEEMM.ToString();

            RES res;
            initProgress(listRES.Count + listHES.Count + listRER.Count);
            for (var row = 0; row < listRES.Count; row++)
            {
                res = listRES[row];
                if (res.入力年月 != master)
                {
                    con.WriteLine($"RES({row + 1}行目) 入力年月が異なります（値：{res.入力年月}）", LogConsole.LOG_TYPE.Error);
                    errListRES.Add(res);
                    result = false;
                }
                updateProgress();
            }
            HES hes;
            for (var row = 0; row < listHES.Count; row++)
            {
                hes = listHES[row];
                if (hes.入力年月 != master)
                {
                    con.WriteLine($"HES({row + 1}行目) 入力年月が異なります（値：{hes.入力年月}）", LogConsole.LOG_TYPE.Error);
                    errListHES.Add(hes);
                    result = false;
                }
                updateProgress();
            }
            RER rer;
            for (var row = 0; row < listRER.Count; row++)
            {
                rer = listRER[row];
                if (rer.入力年月 != master)
                {
                    con.WriteLine($"RER({row + 1}行目) 入力年月が異なります（値：{rer.入力年月}）", LogConsole.LOG_TYPE.Error);
                    errListRER.Add(rer);
                    result = false;
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// RESの保険者番号統一確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="masterHokensyaNum"></param>
        /// <param name="listRES"></param>
        /// <param name="listRER"></param>
        /// <param name="errListRES"></param>
        /// <param name="errListRER"></param>
        /// <returns></returns>
        private bool checkHokensyaNumRES(LogConsole con, string masterHokensyaNum,
            List<RES> listRES, List<RER> listRER,
            List<RES> errListRES, List<RER> errListRER)
        {
            var result = true;
            initProgress(listRES.Count + listRER.Count);
            RES res;
            for (var row = 0; row < listRES.Count; row++)
            {
                res = listRES[row];
                if (res.保険者番号 != masterHokensyaNum)
                {
                    con.WriteLine($"RES({row + 1}行目) 保険者番号が異なります（値：{res.保険者番号}）", LogConsole.LOG_TYPE.Error);
                    errListRES.Add(res);
                    result = false;
                }
                updateProgress();
            }
            RER rer;
            for (var row = 0; row < listRER.Count; row++)
            {
                rer = listRER[row];
                if (rer.保険者番号 != masterHokensyaNum)
                {
                    con.WriteLine($"RER({row + 1}行目) 保険者番号が異なります（値：{rer.保険者番号}）", LogConsole.LOG_TYPE.Error);
                    errListRER.Add(rer);
                    result = false;
                }
                updateProgress();
            }
            return result;
        }

        /// <summary>
        /// 照会対象者データの年月と支部番号を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="pSyokai"></param>
        /// <returns></returns>
        private bool checkSyokaiYMN(LogConsole con, string pSyokai)
        {
            using (var fs = new System.IO.FileStream(pSyokai, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                var book = new XSSFWorkbook(fs);

                con.WriteLine($"{getIndent(1)}{FileUtility.GetFileName(pSyokai, true)}", LogConsole.LOG_TYPE.Info);

                initProgress(1);

                var sheet = book.GetSheet("Sheet1");
                if (sheet == null)
                {
                    con.WriteLine("「Sheet1」シートがありません", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();

                var eemm = (ee * 100 + mm).ToString("0000");
                var nn = pref.ID.ToString("00");

                var result = checkYMN(con, sheet, 1, 1, eemm, nn);

                book.Close();

                return result;
            }
        }

        /// <summary>
        /// 文書照会結果データの年月と支部番号を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="pKekka"></param>
        /// <returns></returns>
        private bool checkKekkaYMN(LogConsole con, string pKekka)
        {
            using (var fs = new System.IO.FileStream(pKekka, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                var book = new XSSFWorkbook(fs);

                con.WriteLine($"{getIndent(1)}{FileUtility.GetFileName(pKekka, true)}", LogConsole.LOG_TYPE.Info);

                initProgress(2);

                var check1 = true;
                var nn = pref.ID.ToString("00");
                var nnxx = $"{nn}{pref.Name2}";

                var eemm1 = (ee * 100 + mm).ToString("0000");
                var eemm2 = DateTimeEx.GetPrevEEMM_ChangeGengo(ee, mm);
                //var eemm1 = (ee * 100 + mm).ToString("0000");
                //var eemm2 = "";
                //if (mm == 1) eemm2 = ((ee - 1) * 100 + 12).ToString("0000");
                //else eemm2 = (ee * 100 + (mm - 1)).ToString("0000");
                var sheet1 = book.GetSheet($"{nnxx}_{eemm1}");
                var sheet2 = book.GetSheet($"{nnxx}_{eemm2}");
                if (sheet1 == null)
                {
                    con.WriteLine($"「{nnxx}_{eemm1}」シートがありません", LogConsole.LOG_TYPE.Error);
                    check1 = false;
                }
                if (sheet2 == null)
                {
                    con.WriteLine($"「{nnxx}_{eemm2}」シートがありません", LogConsole.LOG_TYPE.Error);
                    check1 = false;
                }
                if (!check1) return false;
                updateProgress();

                var check2 = true;
                check2 &= checkYMN(con, sheet1, 1, 1, eemm1, nn, false);
                check2 &= checkYMN(con, sheet2, 1, 1, eemm2, nn, false);
                updateProgress();

                book.Close();

                return check2;
            }
        }

        /// <summary>
        /// 返戻データの年月と支部番号を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="pHenrei"></param>
        /// <returns></returns>
        private bool checkHenreiYMN(LogConsole con, string pHenrei)
        {
            using (var fs = new System.IO.FileStream(pHenrei, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                var book = new XSSFWorkbook(fs);
                con.WriteLine($"{getIndent(1)}{FileUtility.GetFileName(pHenrei, true)}", LogConsole.LOG_TYPE.Info);

                initProgress(1);

                var sheet = book.GetSheet("当月分");
                if (sheet == null)
                {
                    con.WriteLine("「当月分」シートがありません", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();

                var yymm = $"{yyyy.ToString("0000").Substring(2, 2)}{mm.ToString("00")}";
                var nn = pref.ID.ToString("00");

                var result = checkYMN(con, sheet, 1, 2, yymm, nn);

                book.Close();

                return result;
            }
        }

        /// <summary>
        /// 支払済みデータの年月と支部番号を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="pSiharai"></param>
        /// <returns></returns>
        private bool checkSiharaiYMN(LogConsole con, string pSiharai)
        {
            using (var fs = new System.IO.FileStream(pSiharai, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                var book = new XSSFWorkbook(fs);
                con.WriteLine($"{getIndent(1)}{FileUtility.GetFileName(pSiharai, true)}", LogConsole.LOG_TYPE.Info);

                initProgress(1);

                var sheet = book.GetSheet("支払済み分");
                if (sheet == null)
                {
                    con.WriteLine("「支払済み分」シートがありません", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();

                string yymm;
                if (mm == 1) yymm = $"{(yyyy - 1).ToString("0000").Substring(2, 2)}12";
                else yymm = $"{yyyy.ToString("0000").Substring(2, 2)}{(mm - 1).ToString("00")}";
                var nn = pref.ID.ToString("00");

                var result = checkYMN(con, sheet, 1, 2, yymm, nn);

                book.Close();

                return result;
            }
        }

        /// <summary>
        /// 指定したセルの形式と年月と支部番号を確認します。row,columnは0スタートです。
        /// </summary>
        /// <param name="con"></param>
        /// <param name="sheet"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="correctYYMM"></param>
        /// <param name="correctNN"></param>
        /// <param name="required"></param>
        /// <returns></returns>
        private bool checkYMN(LogConsole con, ISheet sheet, int row, int column, string correctYYMM, string correctNN, bool required = true)
        {
            con.WriteLine($"{getIndent(3)}├{sheet.SheetName}", LogConsole.LOG_TYPE.Info);

            var val = sheet.GetRow(row).GetCell(column).StringCellValue;

            if (string.IsNullOrWhiteSpace(val))
            {
                if (required)
                {
                    con.WriteLine("データが入っていません", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                else
                {
                    con.WriteLine($"{getIndent(3)}　>>>　データ無し", LogConsole.LOG_TYPE.Caution);
                    return true;
                }
            }

            con.WriteLine($"{getIndent(5)}└{val}", LogConsole.LOG_TYPE.Info);

            if (!Regex.IsMatch(val, @"\d{4}-\d{2}-\d{1,}", RegexOptions.ECMAScript)) //nnnn-nn-n...
            {
                con.WriteLine($"照会番号の形式が不正です（{val}）", LogConsole.LOG_TYPE.Error);
                con.WriteLine($"　>>> 「nnnn-nn-n...」の形式にしてください", LogConsole.LOG_TYPE.Error);
                return false;
            }

            var vals = val.Split('-');

            var check = true;
            if (vals[0] != correctYYMM)
            {
                con.WriteLine($"「{correctYYMM}」のデータではありません（{vals[0]}）", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            if (vals[1] != correctNN)
            {
                con.WriteLine($"「{correctNN}」のデータではありません（{vals[1]}）", LogConsole.LOG_TYPE.Error);
                check = false;
            }

            return check;
        }
    }
}
