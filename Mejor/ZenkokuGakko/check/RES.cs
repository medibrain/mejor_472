﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.ZenkokuGakko.check
{
    class RES : Columns
    {
        private const int REQUIRED_COLUMN_COUNT = 18;
        private bool lostColumn;
        private bool overColumn;
        public bool Error;
        public int Row { get; set; }
        public string 入力年月 => C_GEEMM.Val;
        public string ﾊﾞｯﾁ => Batch.Val;
        public string ﾅﾝﾊﾞﾘﾝｸﾞ => Numbering.Val;
        public string 診療年月 => V_GEEMM.Val;
        public string 明細区分 => Kubun.Val;
        public string 本人家族 => Honke.Val;
        public string 予備1 => Etc1.Val;
        public string 保険者番号 => InsurerNo.Val;
        public string 組合員番号 => KumiaiNo.Val;
        public string 性別 => Sex.Val;
        public string 和暦生年月 => Birth.Val;
        public string 診療日数 => Days.Val;
        public string 合計金額 => TotalCost.Val;
        public string 請求金額 => Seikyu.Val;
        public string 一部負担支払猶予 => Yuyo.Val;
        public string 登録記号番号 => HosCode.Val;
        public string 振込先口座番号 => BankNo.Val;
        public string 患者負担額 => Futan.Val;
        public Property C_GEEMM { get; set; }
        public Property Batch { get; set; }
        public Property Numbering { get; set; }
        private Property V_GEEMM;
        private Property Kubun;
        private Property Honke;
        private Property Etc1;
        private Property InsurerNo;
        private Property KumiaiNo;
        private Property Sex;
        private Property Birth;
        private Property Days;
        private Property TotalCost;
        private Property Seikyu;
        private Property Yuyo;
        private Property HosCode;
        private Property BankNo;
        private Property Futan;
        public RES(int row, string[] record)
        {
            this.Row = row;

            lostColumn = record.Length < REQUIRED_COLUMN_COUNT;
            overColumn = record.Length > REQUIRED_COLUMN_COUNT;
            if (lostColumn || overColumn)
            {
                Error = true;
                var p = new Property();
                this.C_GEEMM = p;
                this.Batch = p;
                this.Numbering = p;
                this.V_GEEMM = p;
                this.Kubun = p;
                this.Honke = p;
                this.Etc1 = p;
                this.InsurerNo = p;
                this.KumiaiNo = p;
                this.Sex = p;
                this.Birth = p;
                this.Days = p;
                this.TotalCost = p;
                this.Seikyu = p;
                this.Yuyo = p;
                this.HosCode = p;
                this.BankNo = p;
                this.Futan = p;
                return;
            }

            this.C_GEEMM = new Property(nameof(RES.入力年月), record[0], 5, Property.TYPE.GEEMM);
            this.Batch = new Property(nameof(RES.ﾊﾞｯﾁ), record[1], 4, Property.TYPE.NUMERIC);
            this.Numbering = new Property(nameof(RES.ﾅﾝﾊﾞﾘﾝｸﾞ), record[2], 6, Property.TYPE.NUMERIC);
            this.V_GEEMM = new Property(nameof(RES.診療年月), record[3], 5, Property.TYPE.GEEMM);
            this.Kubun = new Property(nameof(RES.明細区分), record[4], new string[] { "0" });
            this.Honke = new Property(nameof(RES.本人家族), record[5], 1, 9);
            this.Etc1 = new Property(nameof(RES.予備1), record[6], new string[] { "" });
            this.InsurerNo = new Property(nameof(RES.保険者番号), record[7], 8, Property.TYPE.NUMERIC);
            this.KumiaiNo = new Property(nameof(RES.組合員番号), record[8], 10, true, Property.TYPE.KUMIAIINBANGO);
            this.Sex = new Property(nameof(RES.性別), record[9], new string[] { "1", "2" });
            this.Birth = new Property(nameof(RES.和暦生年月), record[10], 5, true, Property.TYPE.GEEMM);
            this.Days = new Property(nameof(RES.診療日数), record[11], 31, true);
            this.TotalCost = new Property(nameof(RES.合計金額), record[12], 8, true, Property.TYPE.NUMERIC);
            this.Seikyu = new Property(nameof(RES.請求金額), record[13], 8, true, Property.TYPE.NUMERIC);
            this.Yuyo = new Property(nameof(RES.一部負担支払猶予), record[14], new string[] { "", "9" });
            this.HosCode = new Property(nameof(RES.登録記号番号), record[15], 10, false, Property.TYPE.NUMERIC);
            this.BankNo = new Property(nameof(RES.振込先口座番号), record[16], 7, Property.TYPE.NUMERIC);
            this.Futan = new Property(nameof(RES.患者負担額), record[17], 8, false, Property.TYPE.NUMERIC);
        }
        public bool AllValueCheck(out string message)
        {
            message = "";

            if (lostColumn)
            {
                message += $"({Row}行目) 列数が{REQUIRED_COLUMN_COUNT}ではありません\r\n";
                return false;
            }
            else if (overColumn)
            {
                message += $"({Row}行目) 列数が{REQUIRED_COLUMN_COUNT}を超過しています\r\n";
                return false;
            }

            string m;
            if (!C_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Batch.ValueCheck(Row, out m)) message += m;
            if (!Numbering.ValueCheck(Row, out m)) message += m;
            if (!V_GEEMM.ValueCheck(Row, out m)) message += m;
            if (!Kubun.ValueCheck(Row, out m)) message += m;
            if (!Honke.ValueCheck(Row, out m)) message += m;
            if (!Etc1.ValueCheck(Row, out m)) message += m;
            if (!InsurerNo.ValueCheck(Row, out m)) message += m;
            if (!KumiaiNo.ValueCheck(Row, out m)) message += m;
            if (!Sex.ValueCheck(Row, out m)) message += m;
            if (!Birth.ValueCheck(Row, out m)) message += m;
            if (!Days.ValueCheck(Row, out m)) message += m;
            if (!TotalCost.ValueCheck(Row, out m)) message += m;
            if (!Seikyu.ValueCheck(Row, out m)) message += m;
            if (!Yuyo.ValueCheck(Row, out m)) message += m;
            if (!hosCodeCheck(Row, out m)) message += m;
            if (!BankNo.ValueCheck(Row, out m)) message += m;
            if (!Futan.ValueCheck(Row, out m)) message += m;

            if (message != "") message = message.Substring(0, message.LastIndexOf("\r\n"));
            return message == "";
        }
        private bool hosCodeCheck(int row, out string message)
        {
            message = "";
            var val = HosCode.Val;
            var name = HosCode.Name;

            if (val == null)
            {
                message += $"({row}行目)[{name}] 値が取得できません\r\n";
                return false;
            }
            if (!HosCode.Required && val == "")
            {
                return true;
            }
            long valLong;
            if (!long.TryParse(val, out valLong))
            {
                message += $"({row}行目)[{name}] 文字が含まれています（値：{val}）\r\n";
                return false;
            }
            if (val.Length != 10 && val.Length > 5)
            {
                message += $"({row}行目)[{name}] 10桁もしくは5桁以下である必要があります（値：{val}）\r\n";
                return false;
            }

            return true;
        }
    }
}
