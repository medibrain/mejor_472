﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.ZenkokuGakko.check
{
    partial class SubmitDataCheckForm
    {
        /// <summary>
        /// 指定されたフォルダ・ファイルの実態確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="dataFolderPath"></param>
        /// <param name="outFilePath"></param>
        /// <returns></returns>
        private bool checkExistPath(LogConsole con, out string dataFolderPath, out string outFilePath)
        {
            dataFolderPath = null;
            outFilePath = null;

            initProgress(4);

            var dPath = textBoxFolderPath.Text;
            updateProgress();
            if (string.IsNullOrWhiteSpace(dPath))
            {
                con.WriteLine("提出データが含まれるフォルダが指定されていません。", LogConsole.LOG_TYPE.Error);
                return false;
            }
            updateProgress();
            if (!System.IO.Directory.Exists(dPath))
            {
                con.WriteLine("指定されたフォルダは存在しません。", LogConsole.LOG_TYPE.Error);
                return false;
            }

            var oPath = textBoxCheckFilePath.Text;
            updateProgress();
            if (string.IsNullOrWhiteSpace(oPath))
            {
                con.WriteLine("証明書ファイルの出力先が指定されていません。", LogConsole.LOG_TYPE.Error);
                return false;
            }
            updateProgress();
            if (System.IO.Directory.Exists(oPath))
            {
                con.WriteLine("指定されたファイルはすでに存在しています。", LogConsole.LOG_TYPE.Error);
                return false;
            }

            dataFolderPath = dPath;
            outFilePath = oPath;
            return true;
        }

        /// <summary>
        /// 第一階層のフォルダ構造をチェックします。
        /// 「支部番号_都道府県名」ﾌｫﾙﾀﾞと「各種一覧表」ﾌｫﾙﾀﾞのみ存在します。
        /// </summary>
        /// <param name="con"></param>
        /// <param name="rootFolderPath"></param>
        /// <param name="prefFolderPath"></param>
        /// <param name="listFolderPath"></param>
        /// <returns></returns>
        private bool checkRootFolder(LogConsole con, string rootFolderPath, out string prefFolderPath, out string listFolderPath)
        {
            prefFolderPath = null;
            listFolderPath = null;

            initProgress(2);

            var folder1s = FileUtility.GetFoldersInFolder(rootFolderPath, false);
            var successFolderName_Pref = $"{pref.ID.ToString("00")}_{pref.Name2}";
            var successFolderName_List = "各種一覧表";
            if (folder1s == null || folder1s.Count() == 0)
            {
                con.WriteLine("第一フォルダがありません。", LogConsole.LOG_TYPE.Error);
                con.WriteLine($"　>>> 「{successFolderName_Pref}」と「{successFolderName_List}」が必要です", LogConsole.LOG_TYPE.Error);
                return false;
            }

            con.WriteLine(FileUtility.GetDirectoryName(rootFolderPath), LogConsole.LOG_TYPE.Bold);

            //フォルダ有チェック
            var exOtherFolder = false;
            foreach (var f in folder1s)
            {
                var fn = FileUtility.GetDirectoryName(f);
                if (fn == successFolderName_Pref)
                {
                    prefFolderPath = f;
                    con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Info);
                }
                else if (fn == successFolderName_List)
                {
                    listFolderPath = f;
                    con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Info);
                }
                else
                {
                    exOtherFolder = true;
                    con.WriteLine($"第一フォルダに不正なフォルダが存在します", LogConsole.LOG_TYPE.Error);
                    con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Error);
                }
            }
            var check = true;
            if (prefFolderPath == null)
            {
                con.WriteLine($"第一フォルダに「{successFolderName_Pref}」フォルダがありません", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            if (listFolderPath == null)
            {
                con.WriteLine($"第一フォルダに「{successFolderName_List}」フォルダがありません", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            if (exOtherFolder)
            {                
                con.WriteLine($"　>>> 「{successFolderName_Pref}」と「{successFolderName_List}」のみ必要です", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            updateProgress();

            //ファイル無チェック
            var file1s = FileUtility.GetFilesInFolder(rootFolderPath, false);
            if (file1s != null && file1s.Count() != 0)
            {
                con.WriteLine("第一階層にファイルが存在します。", LogConsole.LOG_TYPE.Error);
                foreach (var f in file1s)
                {
                    con.WriteLine(FileUtility.GetFileName(f, true), LogConsole.LOG_TYPE.Error);
                }
                con.WriteLine($"　>>> 「{successFolderName_Pref}」と「{successFolderName_List}」のみ必要です", LogConsole.LOG_TYPE.Error);
                return false;
            }
            updateProgress();

            return check;
        }

        /// <summary>
        /// 第二階層（「支部番号_都道府県名」ﾌｫﾙﾀﾞ内）以降の構造を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="prefFolderPath"></param>
        /// <param name="mainPath"></param>
        /// <param name="images"></param>
        /// <param name="csvs"></param>
        /// <returns></returns>
        private bool checkPrefFolderStep(LogConsole con, string prefFolderPath, out string mainPath, out string[] images, out string[] csvs)
        {
            mainPath = null;
            images = null;
            csvs = null;


            //第一
            con.WriteLine($"{getIndent(0)}{FileUtility.GetDirectoryName(prefFolderPath)}", LogConsole.LOG_TYPE.Bold);


            //第二
            var folder2s = FileUtility.GetFoldersInFolder(prefFolderPath, false);
            var successFolderName2 = (yyyy * 100 + (cym % 100)).ToString();
            var folder2Path = "";
            if (folder2s == null || folder2s.Count() == 0)
            {
                con.WriteLine("第二フォルダがありません。", LogConsole.LOG_TYPE.Error);
                con.WriteLine($"　>>> 「{successFolderName2}」としてください", LogConsole.LOG_TYPE.Error);
                return false;
            }
            else
            {
                initProgress(3);
                if (folder2s.Count() > 1)
                {
                    con.WriteLine("第二フォルダが複数存在します。", LogConsole.LOG_TYPE.Error);
                    for (var i = 0; i < folder2s.Length; i++)
                    {
                        var fn = FileUtility.GetFileName(folder2s[i]);
                        if (i == folder2s.Length - 1) con.WriteLine($"{getIndent(1)}└{fn}", LogConsole.LOG_TYPE.Error);
                        else con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Error);
                    }
                    con.WriteLine($"　>>> フォルダ名「{successFolderName2}」のみ必要です", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();
                folder2Path = folder2s[0];
                var folder2Name = FileUtility.GetDirectoryName(folder2Path);
                if (folder2Name != successFolderName2)
                {
                    con.WriteLine($"第二フォルダ名が不正です。（フォルダ名：{folder2Name}）", LogConsole.LOG_TYPE.Error);
                    con.WriteLine($"　>>> 「{successFolderName2}」に変更してください", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();
                con.WriteLine($"{getIndent(1)}└{folder2Name}", LogConsole.LOG_TYPE.Info);
                var file2s = FileUtility.GetFilesInFolder(prefFolderPath, false);
                if (file2s != null && file2s.Count() != 0)
                {
                    con.WriteLine("第二階層にファイルが存在します。", LogConsole.LOG_TYPE.Error);
                    for (var i = 0; i < file2s.Length; i++)
                    {
                        var fn = FileUtility.GetFileName(file2s[i], true);
                        if (i == file2s.Length - 1) con.WriteLine($"{getIndent(1)}└{fn}", LogConsole.LOG_TYPE.Error);
                        else con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Error);
                    }
                    con.WriteLine($"　>>> 「{successFolderName2}」のみ必要です", LogConsole.LOG_TYPE.Error);
                    return false;
                }
                updateProgress();
            }

            var result = true;
            mainPath = folder2Path;

            //第三
            var folder3s = FileUtility.GetFoldersInFolder(folder2Path, false);
            if (folder3s == null || folder3s.Count() == 0)
            {
                con.WriteLine("第三フォルダ（バッチフォルダ）が一つもありません。", LogConsole.LOG_TYPE.Error);
                result = false;
            }
            else
            {
                initProgress(folder3s.Length);
                var listPath = new List<string>();
                for (var i = 0; i < folder3s.Count(); i++)
                {
                    var tmpResult = true;
                    var fName = FileUtility.GetDirectoryName(folder3s[i]);

                    if (toInt(fName) == -1)
                    {
                        con.WriteLine($"{getIndent(3)}├{fName}", LogConsole.LOG_TYPE.Info);
                        con.WriteLine($"バッチフォルダではありません。（フォルダ名：{fName}）", LogConsole.LOG_TYPE.Error);
                        result = false;
                        tmpResult = false;
                    }
                    //第四階層
                    var subFolders = FileUtility.GetFoldersInFolder(folder3s[i], false);
                    if (subFolders != null && subFolders.Count() > 0)
                    {
                        con.WriteLine($"{getIndent(3)}├{fName}", LogConsole.LOG_TYPE.Info);
                        for (var j = 0; j < subFolders.Length; j++)
                        {
                            if (j == subFolders.Length - 1)
                            {
                                con.WriteLine($"{getIndent(5)}└{FileUtility.GetFileName(subFolders[j])}", LogConsole.LOG_TYPE.Error);
                            }
                            else
                            {
                                con.WriteLine($"{getIndent(5)}├{FileUtility.GetFileName(subFolders[j])}", LogConsole.LOG_TYPE.Error);
                            }
                        }
                        con.WriteLine($"第四フォルダが存在しています。", LogConsole.LOG_TYPE.Error);
                        result = false;
                    }
                    if (tmpResult)
                    {
                        var subFiles = FileUtility.GetFilesInFolder(folder3s[i], false);
                        if (subFiles == null || subFiles.Count() == 0)
                        {
                            con.WriteLine($"{getIndent(3)}├{fName}", LogConsole.LOG_TYPE.Info);
                            con.WriteLine($"画像ファイルがありません。（フォルダ名：{fName}）", LogConsole.LOG_TYPE.Error);
                            result = false;
                        }
                        else
                        {
                            var onlyFileIsThumbs = false;
                            if (subFiles.Count() == 1)
                            {
                                if (FileUtility.GetFileName(subFiles[0], true).ToLower() == "thumbs.db")
                                {
                                    con.WriteLine($"{getIndent(3)}├{fName}", LogConsole.LOG_TYPE.Info);
                                    con.WriteLine($"画像ファイルがありません。（フォルダ名：{fName}）", LogConsole.LOG_TYPE.Error);
                                    onlyFileIsThumbs = true;
                                    result = false;
                                }
                            }
                            if (!onlyFileIsThumbs)
                            {
                                Func<string, string, bool> checkName = (fn, yymmbbbb) =>
                                {
                                    if (fn.Length != 16) return false;
                                    if (toLong(fn) == -1) return false;
                                    if (!fn.StartsWith(yymmbbbb)) return false;
                                    return true;
                                };
                                string iName;
                                var YYMMBBBB = $"{yyyy.ToString().Substring(2, 2)}{(cym % 100).ToString("00")}{fName}";
                                var ms = new List<string>();
                                ms.Add($"{getIndent(3)}├{fName} （@）");
                                var thumbs = false;
                                foreach (var f in subFiles)
                                {
                                    if (!Utility.CheckFileByte(f))
                                    {
                                        ms.Add($"100バイト以下のファイルが含まれています。（ファイル名：{f}）");
                                        result = false;
                                    }

                                    iName = FileUtility.GetFileName(f, true);
                                    if (iName.ToLower() == "thumbs.db")
                                    {
                                        //con.WriteLine($"　　　　　├{iName}　（サムネイルファイル）", LogConsole.LOG_TYPE.Caution);
                                        thumbs = true;
                                        continue;//サムネイルはパス
                                    }
                                    else if (FileUtility.GetExtension(f) != ".tif")
                                    {
                                        ms.Add($"tifファイル以外のものが含まれています。（ファイル名：{iName}）");
                                        result = false;
                                    }
                                    else if (!checkName(FileUtility.GetFileName(f), YYMMBBBB))
                                    {
                                        var em = "";
                                        em += $"不正な名前の画像ファイルが含まれています。（ファイル名：{iName}）\r\n";
                                        em += $"　>>> 形式は「{YYMMBBBB}nnnnnnpp.tif」である必要があります。";
                                        ms.Add(em);
                                        result = false;
                                    }
                                    else
                                    {
                                        listPath.Add(f);//画像パスを記録
                                    }
                                }
                                ms[0] = ms[0].Replace("@", thumbs ? (subFiles.Count() - 1).ToString() : subFiles.Count().ToString());
                                for (var j = 0; j < ms.Count; j++)
                                {
                                    if (j == 0) con.WriteLine(ms[0], LogConsole.LOG_TYPE.Info);
                                    else con.WriteLine(ms[j], LogConsole.LOG_TYPE.Error);
                                }
                            }
                        }
                    }
                    updateProgress();
                }
                images = listPath.ToArray();
            }

            //第三（ファイル）
            var file3s = FileUtility.GetFilesInFolder(folder2Path, false);
            if (file3s == null || file3s.Count() == 0)
            {
                con.WriteLine("第三階層にファイルが一つもありません。", LogConsole.LOG_TYPE.Error);
                result = false;
            }
            else
            {
                Func<string, string, bool> checkFile3Name = (fn, type) =>
                {
                    var check = true;
                    var fnNotEx = FileUtility.GetFileName(fn);
                    if (fnNotEx.Length != 9)
                    {
                        con.WriteLine($"{type}ファイルのファイル名が不正です。（ファイル名：{fn}）", LogConsole.LOG_TYPE.Error);
                        check = false;
                    }
                    else
                    {
                        var yyMMddStr = fnNotEx.Replace(type, "");
                        int yyMMdd = toInt(yyMMddStr);
                        if (yyMMdd == -1)
                        {
                            con.WriteLine($"{type}ファイルの日付部分が不正です。（ファイル名：{fn}）", LogConsole.LOG_TYPE.Error);
                            con.WriteLine($"　>>> 形式は「{type}年年月月日日」である必要があります", LogConsole.LOG_TYPE.Error);
                            check = false;
                        }
                        else if (DateTimeEx.ToDateTime("20" + yyMMddStr) == DateTimeEx.DateTimeNull)
                        {
                            con.WriteLine($"{type}ファイルの日付部分が不正です。（ファイル名：{fn}）", LogConsole.LOG_TYPE.Error);
                            con.WriteLine($"　>>> 形式は「{type}年年月月日日」である必要があります", LogConsole.LOG_TYPE.Error);
                            check = false;
                        }
                    }
                    if (FileUtility.GetExtension(fn) != ".csv")
                    {
                        con.WriteLine($"{type}ファイルの拡張子が.csvではありません。（ファイル名：{fn}）", LogConsole.LOG_TYPE.Error);
                        check = false;
                    }
                    return check;
                };
                initProgress(file3s.Length);
                bool res = false;
                bool hes = false;
                bool rer = false;
                string resPath = null;
                string hesPath = null;
                string rerPath = null;
                for (var i = 0; i < file3s.Count(); i++)
                {
                    var fName = FileUtility.GetFileName(file3s[i], true);

                    if (!fName.StartsWith("HES") && !Utility.CheckFileByte(file3s[i]))
                    {
                        con.WriteLine($"100バイト以下のファイルが含まれています。（ファイル名：{file3s[i]}）",
                            LogConsole.LOG_TYPE.Error);
                        result = false;
                    }

                    if (fName.StartsWith("RES"))
                    {
                        if (resPath != null)
                        {
                            updateProgress();
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Error);
                            con.WriteLine("RESファイルが複数存在します。", LogConsole.LOG_TYPE.Error);
                            result = false;
                            continue;
                        }
                        if (checkFile3Name(fName, "RES"))
                        {
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Info);
                            resPath = file3s[i];
                        }
                        else result = false;
                        res = true;
                    }
                    else if (fName.StartsWith("HES"))
                    {
                        if (hesPath != null)
                        {
                            updateProgress();
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Error);
                            con.WriteLine("HESファイルが複数存在します。", LogConsole.LOG_TYPE.Error);
                            result = false;
                            continue;
                        }
                        if (checkFile3Name(fName, "HES"))
                        {
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Info);
                            hesPath = file3s[i];
                        }
                        else result = false;
                        hes = true;
                    }
                    else if (fName.StartsWith("RER"))
                    {
                        if (rerPath != null)
                        {
                            updateProgress();
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Error);
                            con.WriteLine("RERファイルが複数存在します。", LogConsole.LOG_TYPE.Error);
                            result = false;
                            continue;
                        }
                        if (checkFile3Name(fName, "RER"))
                        {
                            con.WriteLine($"　　　├{fName}", LogConsole.LOG_TYPE.Info);
                            rerPath = file3s[i];
                        }
                        else result = false;
                        rer = true;
                    }
                    else if (fName.ToLower() == "thumbs.db")
                    {
                        updateProgress();
                        //con.WriteLine($"　　　├{fName}　（サムネイルファイル）", LogConsole.LOG_TYPE.Caution);
                        continue;
                    }
                    else
                    {
                        con.WriteLine($"不要なファイルが含まれています。（ファイル名：{fName}）", LogConsole.LOG_TYPE.Error);
                        result = false;
                    }
                    updateProgress();
                }
                if (!res)
                {
                    con.WriteLine($"RESファイルがありません。", LogConsole.LOG_TYPE.Error);
                    result = false;
                }
                if (!hes)
                {
                    con.WriteLine($"HESファイルがありません。", LogConsole.LOG_TYPE.Error);
                    result = false;
                }
                if (!rer)
                {
                    con.WriteLine($"RERファイルなし", LogConsole.LOG_TYPE.Caution);
                }

                //問題ない場合はパスを入れる
                if (result)
                {
                    csvs = new string[3];
                    csvs[0] = resPath;
                    csvs[1] = hesPath;
                    csvs[2] = rerPath;
                }
            }

            return result;
        }

        /// <summary>
        /// 第二階層（「各種一覧表」ﾌｫﾙﾀﾞ内）以降の構造とデータ内の年月を確認します
        /// </summary>
        /// <param name="con"></param>
        /// <param name="listFolderPath"></param>
        /// <param name="pSyokai"></param>
        /// <param name="pKekka"></param>
        /// <param name="pHenrei"></param>
        /// <param name="pSiharai"></param>
        /// <returns></returns>
        private bool checkListFolderStep(LogConsole con, string listFolderPath, out string pSyokai, out string pKekka, out string pHenrei, out string pSiharai)
        {
            pSyokai = null;
            pKekka = null;
            pHenrei = null;
            pSiharai = null;

            //第一
            con.WriteLine($"{getIndent(0)}{FileUtility.GetDirectoryName(listFolderPath)}", LogConsole.LOG_TYPE.Bold);

            //第二
            initProgress(2);
            var nnxx = $"{pref.ID.ToString("00")}{pref.Name2}";
            var yymm1 = (ee * 100 + mm).ToString("0000");
            var yymm2 = ""; //一月前
            yymm2 = DateTimeEx.GetPrevEEMM_ChangeGengo(ee, mm);
            //var yymm2 = ""; //一月前
            //if (mm == 1) yymm2 = ((ee - 1) * 100 + 12).ToString("0000");
            //else yymm2 = (ee * 100 + (mm - 1)).ToString("0000");
            var successFileName1 = $"⑥公立学校共済_{nnxx}_{yymm1}照会対象者一覧.xlsx";
            var successFileName2 = $"⑨⑩公立学校共済_{nnxx}_{yymm1}文書照会結果一覧表.xlsx";
            var successFileName3 = $"⑦公立学校共済_{nnxx}_{yymm1}返戻対象者一覧表.xlsx";
            var successFileName4 = $"⑧公立学校共済_{nnxx}_{yymm2}返戻対象者一覧表（支払済み分）.xlsx";
            var file2s = FileUtility.GetFilesInFolder(listFolderPath, false);
            var exOtherFile = false;
            foreach (var f in file2s)
            {
                var fn = FileUtility.GetFileName(f, true);

                if (fn == successFileName1)
                {
                    pSyokai = f;
                }
                else if (fn == successFileName2)
                {
                    pKekka = f;
                }
                else if (fn == successFileName3)
                {
                    pHenrei = f;
                }
                else if (fn == successFileName4)
                {
                    pSiharai = f;
                }
                else
                {
                    exOtherFile = true;
                    con.WriteLine("第二階層に不正なファイルが存在します", LogConsole.LOG_TYPE.Error);
                    con.WriteLine($"{getIndent(1)}├{fn}", LogConsole.LOG_TYPE.Error);
                }
            }

            var check = true;
            if (exOtherFile)
            {
                con.WriteLine($"　>>> 「{successFileName1}」「{successFileName2}」「{successFileName3}」「{successFileName4}」のみ有効です", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            updateProgress();

            var folder2s = FileUtility.GetFoldersInFolder(listFolderPath, false);
            if (folder2s != null && folder2s.Count() > 0)
            {
                con.WriteLine("第二階層にフォルダが存在します", LogConsole.LOG_TYPE.Error);
                foreach (var f in folder2s)
                {
                    con.WriteLine($"{getIndent(1)}├{FileUtility.GetDirectoryName(f)}", LogConsole.LOG_TYPE.Error);
                }
                con.WriteLine($"　>>> 「{successFileName1}」「{successFileName2}」「{successFileName3}」「{successFileName4}」のみ配置してください", LogConsole.LOG_TYPE.Error);
                check = false;
            }
            updateProgress();

            //ログ表示（順番指定）
            if (pHenrei != null) con.WriteLine($"{getIndent(1)}├{FileUtility.GetFileName(pHenrei, true)}", LogConsole.LOG_TYPE.Info);
            else con.WriteLine($"{successFileName3} がありません", LogConsole.LOG_TYPE.Caution);

            if (pSiharai != null) con.WriteLine($"{getIndent(1)}├{FileUtility.GetFileName(pSiharai, true)}", LogConsole.LOG_TYPE.Info);
            else con.WriteLine($"{successFileName4} がありません", LogConsole.LOG_TYPE.Caution);

            if (pKekka != null) con.WriteLine($"{getIndent(1)}├{FileUtility.GetFileName(pKekka, true)}", LogConsole.LOG_TYPE.Info);
            else con.WriteLine($"{successFileName2} がありません", LogConsole.LOG_TYPE.Caution);

            if (pSyokai != null) con.WriteLine($"{getIndent(1)}├{FileUtility.GetFileName(pSyokai, true)}", LogConsole.LOG_TYPE.Info);
            else con.WriteLine($"{successFileName1} がありません", LogConsole.LOG_TYPE.Caution);

            return check;
        }

        /// <summary>
        /// 画像ファイル名の確認
        /// </summary>
        /// <param name="con"></param>
        /// <param name="images"></param>
        /// <param name="listRES"></param>
        /// <param name="listRER"></param>
        /// <returns></returns>
        private bool checkImageName(LogConsole con, string[] images, List<RES> listRES, List<RER> listRER)
        {
            var result = true;
            initProgress(images.Length);

            string fn;
            string batch;
            string baseName = $"{yyyy.ToString().Substring(2, 2)}{(cym % 100).ToString("00")}";
            string numbering;
            Columns match;
            foreach (var i in images)
            {
                fn = FileUtility.GetFileName(i);
                batch = FileUtility.GetParentDirectoryName(i);
                if (fn.Length != 16)
                {
                    con.WriteLine($"({batch}) ファイル名が不正です（{fn}）", LogConsole.LOG_TYPE.Error);
                    result = false;
                }
                else if (toLong(fn) == -1)
                {
                    con.WriteLine($"({batch}) ファイル名に文字が含まれています（{fn}）", LogConsole.LOG_TYPE.Error);
                    result = false;
                }
                else
                {
                    if (!fn.StartsWith(baseName))
                    {
                        con.WriteLine($"({batch}) ファイル名の入力年月が異なります（{fn}）", LogConsole.LOG_TYPE.Error);
                        result = false;
                    }
                    else if (!fn.StartsWith($"{baseName}{batch}"))
                    {
                        con.WriteLine($"({batch}) 画像が指定のバッチフォルダ内にありません（{fn}）", LogConsole.LOG_TYPE.Error);
                        result = false;
                    }
                    else
                    {
                        numbering = fn.Substring(8, 6);
                        match = listRES.Find(res => res.ﾊﾞｯﾁ == batch && res.ﾅﾝﾊﾞﾘﾝｸﾞ == numbering);
                        if (match == null)
                        {
                            match = listRER.Find(rer => rer.ﾊﾞｯﾁ == batch && rer.ﾅﾝﾊﾞﾘﾝｸﾞ == numbering);
                            if (match == null)
                            {
                                con.WriteLine($"({batch}) 不正なファイル名もしくは該当するデータが存在しません（{fn}）", LogConsole.LOG_TYPE.Error);
                                result = false;
                            }
                        }
                    }
                }
                updateProgress();
            }
            return result;
        }

    }
}
