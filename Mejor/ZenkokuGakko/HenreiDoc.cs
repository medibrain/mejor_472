﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Printing;
using Musashi.RprtNET;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    class HenreiDoc
    {
        static List<App> apps;
        static int currentIndex = 0;

        static PrintDocument doc = new PrintDocument();
        static PrintControl cont = new PrintControl();

        class HenreiData
        {
            App app;

            public int Aid => app.Aid;
            public string Code => $"{app.CYM%10000}-" +
                $"{Insurer.CurrrentInsurer.InsurerName.Substring(7, 2)}-{app.Aid.ToString("00000000")}";
            public string HihoNum => app.HihoNum;
            public string PersonName => app.PersonName;
            public int Sex => app.Sex;
            public DateTime Birthday => app.Birthday;
            public int YM => app.YM;
            public string DrNum => app.DrNum;
            public string ClinicName => app.ClinicName;
            public int Charge => app.Charge;
            public string Note => app.MemoInspect;
            public string Memo => app.Memo;

            public bool NotContact => app.TaggedDatas.ContactStatusCheck(ContactStatus.連絡不要);
            public bool Dantai => app.TaggedDatas.ContactStatusCheck(ContactStatus.団体);
            public string ContactName => app.TaggedDatas.ContactName;
            public DateTime ContactDt => app.TaggedDatas.ContactDt;

            //20210413101150 furukawa st ////////////////////////
            //施術機関番号文言変更、Apptypeで表示切り替え            
            public APP_TYPE apptype => app.AppType;
            //20210413101150 furukawa ed ////////////////////////


            public List<string> TypeList = new List<string>();
            public List<string> ReasonList = new List<string>();

            public HenreiData(App app)
            {
                var buiList = new List<string>();

                if (Insurer.CurrrentInsurer.InsurerType != INSURER_TYPE.学校共済)
                {
                    throw new Exception("学校共済以外のデータは扱えません。");
                }

                this.app = app;

                #region 過誤チェック

                
                //20201011150601 furukawa st ////////////////////////
                //過誤フラグ追加に伴ってxml列に変更
                
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし))
                {
                    buiList.Clear();
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし1)) buiList.Add("1");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし2)) buiList.Add("2");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし3)) buiList.Add("3");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし4)) buiList.Add("4");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.原因なし5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷原因記載なし");
                    ReasonList.Add("負傷原因の記載をお願いします。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));

                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし))
                {
                    buiList.Clear();
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし1)) buiList.Add("1");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし2)) buiList.Add("2");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし3)) buiList.Add("3");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし4)) buiList.Add("4");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 長期理由記載なし");
                    ReasonList.Add("初検日より見て施術が３ヶ月を超える長期に及んでいます。長期理由の記載をお願いします。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));

                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違))
                {
                    buiList.Clear();
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違1)) buiList.Add("1");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違2)) buiList.Add("2");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違3)) buiList.Add("3");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違4)) buiList.Add("4");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷原因相違");
                    ReasonList.Add("負傷名と負傷原因が相違しています。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));
                }

                //20201026115450 furukawa st ////////////////////////
                //長期理由相違
                
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違))
                {
                    buiList.Clear();
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違1)) buiList.Add("1");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違2)) buiList.Add("2");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違3)) buiList.Add("3");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違4)) buiList.Add("4");
                    if (app.KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 長期理由相違");
                    ReasonList.Add("長期理由が相違しています。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));
                }
                //20201026115450 furukawa ed ////////////////////////

                if (app.KagoReasonsCheck_xml(KagoReasons_Member.署名違い))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 署名違い");
                    ReasonList.Add("組合員名と署名が違います。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.筆跡違い))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 筆跡違い");

                    //20211001101312 furukawa st ////////////////////////
                    //返戻付箋コメント変更                    
                    ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理人記入の場合は拇印又は押印が必要です。\r\n" +
                        "（柔整の場合は拇印、あんま・鍼灸の場合は押印が必要です）");
                    //      ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理人記入の場合は押印が必要です。");
                    //20211001101312 furukawa ed ////////////////////////
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.家族同一筆跡))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 同世帯同一筆跡");
                    //20211001101459 furukawa st ////////////////////////
                    //返戻付箋コメント変更
                    
                    ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理人記入の場合は拇印又は押印が必要です。\r\n" +
                        "（柔整の場合は拇印、あんま・鍼灸の場合は押印が必要です）");
                    //      ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理記入の場合は押印が必要です。");
                    //20211001101459 furukawa ed ////////////////////////
                }



                //20201028094248 furukawa st ////////////////////////
                //追加フラグの文言追加

                if (app.KagoReasonsCheck_xml(KagoReasons_Member.療養費請求権の消滅時効))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 療養費請求権の\r\n消滅時効");
                    ReasonList.Add("施術終了日の翌日から起算して2年以内が有効期限です。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.本家区分誤り))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 本家区分誤り");
                    ReasonList.Add("本家区分の記載が誤っています。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.保険者相違))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 保険者相違");
                    ReasonList.Add("提出先の保険者が相違しています。");
                }


                if ((app.KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし)) && 
                    (app.KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上)))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 往療理由記載なし\r\n(16km以上)");
                    ReasonList.Add("16kmを超える往療理由の記載をお願いします。");
                }
                else if(app.KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 往療理由記載なし");
                    ReasonList.Add("往療理由の記載をお願いします。");
                }


                //単独の16km以上は無い
                //if (app.KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上))
                //{
                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 往療理由記載なし\r\n(16km以上)");
                //    ReasonList.Add("16kmを超える往療理由の記載をお願いします。");
                //}


                if (app.KagoReasonsCheck_xml(KagoReasons_Member.同意書添付なし))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 同意書添付なし");
                    ReasonList.Add("同意書の添付が必要です。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.同意書期限切れ))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 同意書記載不備");
                    ReasonList.Add("備考欄をご確認ください。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書添付なし))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 施術報告書添付なし");
                    ReasonList.Add("施術報告書の添付が必要です。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.施術報告書記載不備))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 施術報告書記載不備");
                    ReasonList.Add("備考欄をご確認ください。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書添付なし))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 施術継続理由・\r\n状態記入書添付なし");
                    ReasonList.Add("施術継続理由・状態記入書の添付が必要です。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書記載不備))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 施術継続理由・\r\n状態記入書記載不備");
                    ReasonList.Add("備考欄をご確認ください");
                }


                //20201028094248 furukawa ed ////////////////////////

                #region old
                //if (app.KagoReasonsCheck(KagoReasons.原因なし))
                //{
                //    buiList.Clear();
                //    if (app.KagoReasonsCheck(KagoReasons.原因なし1)) buiList.Add("1");
                //    if (app.KagoReasonsCheck(KagoReasons.原因なし2)) buiList.Add("2");
                //    if (app.KagoReasonsCheck(KagoReasons.原因なし3)) buiList.Add("3");
                //    if (app.KagoReasonsCheck(KagoReasons.原因なし4)) buiList.Add("4");
                //    if (app.KagoReasonsCheck(KagoReasons.原因なし5)) buiList.Add("5");

                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷原因記載なし");
                //    ReasonList.Add("負傷原因の記載をお願いします。" +
                //        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));

                //}
                //if (app.KagoReasonsCheck(KagoReasons.長期理由なし))
                //{
                //    buiList.Clear();
                //    if (app.KagoReasonsCheck(KagoReasons.長期理由なし1)) buiList.Add("1");
                //    if (app.KagoReasonsCheck(KagoReasons.長期理由なし2)) buiList.Add("2");
                //    if (app.KagoReasonsCheck(KagoReasons.長期理由なし3)) buiList.Add("3");
                //    if (app.KagoReasonsCheck(KagoReasons.長期理由なし4)) buiList.Add("4");
                //    if (app.KagoReasonsCheck(KagoReasons.長期理由なし5)) buiList.Add("5");

                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 長期理由記載なし");
                //    ReasonList.Add("初検日より見て施術が３ヶ月を超える長期に及んでいます。長期理由の記載をお願いします。" +
                //        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));

                //}
                //if (app.KagoReasonsCheck(KagoReasons.負傷原因相違))
                //{
                //    buiList.Clear();
                //    if (app.KagoReasonsCheck(KagoReasons.負傷原因相違1)) buiList.Add("1");
                //    if (app.KagoReasonsCheck(KagoReasons.負傷原因相違2)) buiList.Add("2");
                //    if (app.KagoReasonsCheck(KagoReasons.負傷原因相違3)) buiList.Add("3");
                //    if (app.KagoReasonsCheck(KagoReasons.負傷原因相違4)) buiList.Add("4");
                //    if (app.KagoReasonsCheck(KagoReasons.負傷原因相違5)) buiList.Add("5");

                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷原因相違");
                //    ReasonList.Add("負傷名と負傷原因が相違しています。" +
                //        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));
                //}
                //if (app.KagoReasonsCheck(KagoReasons.署名違い))
                //{
                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 署名違い");
                //    ReasonList.Add("組合員名と署名が違います。");
                //}
                //if (app.KagoReasonsCheck(KagoReasons.筆跡違い))
                //{
                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 筆跡違い");
                //    ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理人記入の場合は押印が必要です。");
                //}
                //if (app.KagoReasonsCheck(KagoReasons.家族同一筆跡))
                //{
                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 同世帯同一筆跡");
                //    ReasonList.Add("署名は患者本人の自筆でお願いします。なお、代理記入の場合は押印が必要です。");
                //}
                #endregion

                //20201011150601 furukawa ed ////////////////////////

                #endregion


                if (app.HenreiReasonsCheck(HenreiReasons.負傷部位))
                {
                    buiList.Clear();
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷部位1)) buiList.Add("1");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷部位2)) buiList.Add("2");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷部位3)) buiList.Add("3");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷部位4)) buiList.Add("4");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷部位5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷名相違");
                    ReasonList.Add("患者照会の結果、申告のあった負傷名と相違しています。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));
                }
                if (app.HenreiReasonsCheck(HenreiReasons.負傷原因))
                {
                    buiList.Clear();
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷原因1)) buiList.Add("1");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷原因2)) buiList.Add("2");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷原因3)) buiList.Add("3");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷原因4)) buiList.Add("4");
                    if (app.HenreiReasonsCheck(HenreiReasons.負傷原因5)) buiList.Add("5");

                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷原因相違");
                    ReasonList.Add("患者照会の結果、負傷名と負傷原因が相違しています。" +
                        (buiList.Count != 0 ? $"\r\n　( 負傷名番号: {string.Join(", ", buiList)} )" : ""));
                }
                if (app.HenreiReasonsCheck(HenreiReasons.けが外))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. けが以外での受療");
                    ReasonList.Add("患者照会の結果、けが以外での受療とのことです。");
                }
                if (app.HenreiReasonsCheck(HenreiReasons.受療日数))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 受療日数相違");
                    ReasonList.Add("患者照会の結果、申告のあった受療日数と相違しています。");
                }
                if (app.HenreiReasonsCheck(HenreiReasons.負傷時期))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 負傷時期相違");
                    ReasonList.Add("患者照会の結果、申告のあった負傷時期と相違しています。");
                }




                
                //20201011150213 furukawa st ////////////////////////
                //過誤フラグ追加に伴ってxml列追加
                
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.その他) || app.HenreiReasonsCheck(HenreiReasons.その他))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. その他");
                    ReasonList.Add("備考欄をご確認下さい。");
                }
                //20201011150213 furukawa ed ////////////////////////

                //20210520130108 furukawa st ////////////////////////
                //旧フィールドが重複して出ているので削除

                //if (app.KagoReasonsCheck(KagoReasons.その他) || app.HenreiReasonsCheck(HenreiReasons.その他))
                //{
                //    TypeList.Add($"{(TypeList.Count + 1).ToString()}. その他");
                //    ReasonList.Add("備考欄をご確認下さい。");
                //}
                //20210520130108 furukawa ed ////////////////////////


                //20210422091437 furukawa st ////////////////////////
                //往療内訳書用の付箋文言が抜けてる（指示がなかった）

                if (app.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書添付なし))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 往療内訳書添付なし");
                    ReasonList.Add("往療内訳書の添付が必要です。");
                }
                if (app.KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書記載不備))
                {
                    TypeList.Add($"{(TypeList.Count + 1).ToString()}. 往療内訳書記載不備");
                    ReasonList.Add("備考欄をご確認下さい。");
                }

            }
        }

        static HenreiDoc()
        {
            doc.PrintPage += Doc_PrintPage;
        }

        public static bool HenreiPrints(List<App> henreiApps)
        {
            try
            {
                apps = henreiApps;
                doc.Print();
                return true;
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }

        private static void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (currentIndex >= apps.Count)
                throw new Exception("印刷するデータがありません。");

            if (currentIndex == 0)
            {
                //異字体セレクタチェック
                var ivss = apps.FindAll(r => Utility.ContainsVariationString(r.PersonName) ||
                    Utility.ContainsVariationString(r.ClinicName));

                if (ivss.Count != 0)
                {
                    string msg = string.Empty;
                    foreach (var item in ivss)
                    {
                        if (Utility.ContainsVariationString(item.PersonName))
                        {
                            msg += $"AID:{item.Aid} 受療者名:「{item.PersonName}」 エラー文字:「{Utility.GetVariationString(item.PersonName)}」\r\n";
                        }
                        if (Utility.ContainsVariationString(item.ClinicName))
                        {
                            msg += $"AID:{item.Aid} 施術機関名:「{item.ClinicName}」 エラー文字:「{Utility.GetVariationString(item.ClinicName)}」\r\n";
                        }
                    }

                    msg += "\r\n以上のデータには正常に印刷できない文字が含まれています。対象データの欄は空欄のまま印刷されますがよろしいですか？";
                    var res = MessageBox.Show(msg, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                    if (res != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }

            var g = e.Graphics;
            var ha = new HenreiData(apps[currentIndex]);
            drawPage(g, ha);

            currentIndex++;

            if (currentIndex == apps.Count)
            {
                e.HasMorePages = false;
                currentIndex = 0;
            }
            else
            {
                e.HasMorePages = true;
            }
        }

        private static void drawPage(Graphics g, HenreiData ha)
        {
            var ins = Insurer.CurrrentInsurer;
            cont.OpenFile("Reports\\HenreiDoc.rpr");
            cont.SetText("InsurerNameText",　ins.FormalName);
            
            //基本情報
            cont.SetTableCellText("InfoTable", 1, 0, ins.InsNumber);
            cont.SetTableCellText("InfoTable", 1, 1, ins.FormalName);
            cont.SetTableCellText("InfoTable", 3, 0, ha.HihoNum);
            cont.SetTableCellText("InfoTable", 3, 1,
                Utility.ContainsVariationString(ha.PersonName) ? string.Empty : ha.PersonName);
            cont.SetTableCellText("InfoTable", 5, 0, DateTimeEx.GetJpDateStr(ha.Birthday));
            cont.SetTableCellText("InfoTable", 5, 1, ha.Sex == 1 ? "男" : ha.Sex == 2 ? "女" : "");

            //20210413103814 furukawa st ////////////////////////
            //施術機関番号文言Apptypeで表示切り替え
            
            string strDrNum = string.Empty;
            strDrNum = ha.apptype == APP_TYPE.柔整 ? "(協：0  契：1)  " : "";
            strDrNum += ha.DrNum;

            cont.SetTableCellText("InfoTable", 7, 0, strDrNum);
            //cont.SetTableCellText("InfoTable", 7, 0, "(協：0  契：1)  " + ha.DrNum);
            //20210413103814 furukawa ed ////////////////////////

            cont.SetTableCellText("InfoTable", 7, 1, 
                Utility.ContainsVariationString(ha.ClinicName) ? string.Empty : ha.ClinicName);
            cont.SetTableCellText("InfoTable", 9, 0, DateTimeEx.GetEraJpYearMonth(ha.YM));
            cont.SetTableCellText("InfoTable", 9, 1, ha.Charge.ToString("#,0円"));

            //返戻内容
            var rect0 = cont.GetTableCellRectangle(g, "ReasonTable", 1, 0);
            var rect1 = cont.GetTableCellRectangle(g, "ReasonTable", 1, 1);
            float printY = rect0.Y + 2;
            using (var f = new Font("ＭＳ Ｐゴシック", 11))
            {
                float x = rect1.X + 8;
                float y = rect1.Y + 8;

                for (int i = 0; i < ha.TypeList.Count; i++)
                {
                    g.DrawString(ha.TypeList[i], f, Brushes.Black, rect0.X + 8, y);

                    int printingPosition = 0;
                    while (printingPosition < ha.ReasonList[i].Length)
                    {
                        string line = "";
                        while (true)
                        {
                            if (printingPosition >= ha.ReasonList[i].Length)
                            {
                                printingPosition++;
                                break;
                            }

                            if (ha.ReasonList[i][printingPosition] == '\r')
                            {
                                printingPosition++;
                                continue;
                            }
                            if (ha.ReasonList[i][printingPosition] == '\n')
                            {
                                //改行
                                printingPosition++;
                                break;
                            }

                            //一文字追加し、印刷幅を超えるか調べる
                            line += ha.ReasonList[i][printingPosition];
                            if (g.MeasureString(line, f).Width > rect1.Width - 16)
                            {
                                line = line.Substring(0, line.Length - 1);
                                break;
                            }
                            //印刷文字位置を次へ
                            printingPosition++;
                        }
                        //一行書き出す
                        g.DrawString(line, f, Brushes.Black, x, y);

                        //次の行の印刷位置を計算

                        //20201028111642 furukawa st ////////////////////////
                        //長い文言を改行

                        if (ha.TypeList[i].Contains("\r\n"))
                        {
                            y += (int)f.GetHeight(g) + 14;
                        }
                        else
                        {
                            y += (int)f.GetHeight(g) + 2;
                        }
                        //y += (int)f.GetHeight(g) + 2;

                        //20201028111642 furukawa ed ////////////////////////


                    }
                    y += 6;
                }
            }
            cont.SetTableCellText("ReasonTable",3 , 0, ha.Note);

            //連絡欄
            var contactStr = string.Empty;
            var batchApp = BatchSelector.GetBatchApp(Insurer.CurrrentInsurer, ha.Aid);
            var ac = Account.GetAccount(batchApp?.AccountNumber ?? string.Empty);
            
            if (ha.NotContact) contactStr = "<<事前連絡不要>>  ";
            if (ha.Dantai || ha.ContactName.Trim().Length > 0)
            {
                if (ha.Dantai && ac != null && !string.IsNullOrWhiteSpace(ac.Name)) contactStr += ac.Name + "  ";
                if (ha.ContactName.Trim().Length > 0) contactStr += ha.ContactName;
                contactStr = contactStr.TrimEnd() + "様";
                contactStr += ac == null ? "了承済み" :
                    string.IsNullOrEmpty(ac.HenreiWord) ? string.Empty :
                    "　" + ac.HenreiWord;
                if (!ha.ContactDt.IsNullDate()) contactStr += $"  ({ha.ContactDt.ToString("yyyy/MM/dd HH:mm")})";
                cont.SetTableCellText("MessageTable", 0, 1, contactStr);
            }
            else if(ha.NotContact)
            {
                cont.SetTableCellText("MessageTable", 0, 1, contactStr);
            }

            //返戻番号
            cont.SetTableCellText("NumberTable", 1, 1, ha.Code);

            cont.PrintPage(g);
        }

        public bool Print()
        {
            try
            {
                doc.Print();
                return true;
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }
    }
}
