﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    public partial class HenreiPrintForm : Form
    {
        BindingSource bs = new BindingSource();
        int cym = 0;

        public HenreiPrintForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
            bs.DataSource = new List<App>();

            var style = new DataGridViewCellStyle();
            style.SelectionBackColor = Utility.GridSelectColor;
            style.SelectionForeColor = DefaultForeColor;
            dataGridView1.DefaultCellStyle = style;

            dataGridView1.DataSource = bs;
            foreach (DataGridViewColumn c in dataGridView1.Columns)
            {
                c.ReadOnly = true;
                c.Visible = false;
            }

            dataGridView1.Columns[nameof(App.Select)].Visible = true;
            dataGridView1.Columns[nameof(App.Aid)].Visible = true;
            dataGridView1.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridView1.Columns[nameof(App.PersonName)].Visible = true;
            dataGridView1.Columns[nameof(App.DrNum)].Visible = true;
            dataGridView1.Columns[nameof(App.ClinicName)].Visible = true;
            dataGridView1.Columns[nameof(App.Numbering)].Visible = true;
            dataGridView1.Columns[nameof(App.Select)].DisplayIndex = 0;

            dataGridView1.Columns[nameof(App.Select)].Width = 70;
            dataGridView1.Columns[nameof(App.Aid)].Width = 80;
            dataGridView1.Columns[nameof(App.HihoNum)].Width = 80;
            dataGridView1.Columns[nameof(App.PersonName)].Width = 120;
            dataGridView1.Columns[nameof(App.DrNum)].Width = 120;
            dataGridView1.Columns[nameof(App.ClinicName)].Width = 200;
            dataGridView1.Columns[nameof(App.Numbering)].Width = 100;

            dataGridView1.Columns[nameof(App.Select)].HeaderText = "選択";
            dataGridView1.Columns[nameof(App.Aid)].HeaderText = "AID";
            dataGridView1.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridView1.Columns[nameof(App.PersonName)].HeaderText = "療養者名";
            dataGridView1.Columns[nameof(App.DrNum)].HeaderText = "施術師番号";
            dataGridView1.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridView1.Columns[nameof(App.ClinicName)].HeaderText = "施術師名";

            dataGridView1.Columns[nameof(App.Select)].ReadOnly = false;
        }

        private void HenreiPrintForm_Shown(object sender, EventArgs e)
        {
            using (var wf = new WaitForm())
            {
                wf.CancelButtonEnabled = false;
                wf.ShowDialogOtherTask();
                wf.LogPrint("返戻対象一覧を取得しています。");

                var l = App.GetAppsByStatusFlag(cym, StatusFlag.返戻);
                l.ForEach(x => x.Select = true);
                bs.DataSource = l;
                bs.ResetBindings(false);

                var warningStyle = new DataGridViewCellStyle();
                warningStyle.BackColor = Color.LightPink;
                warningStyle.SelectionBackColor = Color.LightPink;

                for (int i = 0; i < bs.Count; i++)
                {
                    if (Utility.ContainsVariationString(((App)bs[i]).PersonName))
                    {
                        dataGridView1[nameof(App.PersonName), i].Style = warningStyle;
                    }
                    if (Utility.ContainsVariationString(((App)bs[i]).ClinicName))
                    {
                        dataGridView1[nameof(App.ClinicName), i].Style = warningStyle;
                    }
                }
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            var rrs = ((List<App>)bs.DataSource).FindAll(r => r.Select);
            if(rrs.Count==0)
            {
                MessageBox.Show("選択されているデータがありません。");
                return;
            }

            HenreiDoc.HenreiPrints(rrs);
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            var rrs = ((List<App>)bs.DataSource);

            if (buttonAll.Text == "全選択")
            {
                rrs.ForEach(x => x.Select = true);
                buttonAll.Text = "全解除";
            }
            else
            {
                rrs.ForEach(x => x.Select = false);
                buttonAll.Text = "全選択";
            }

            bs.ResetBindings(false);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var app = (App)bs.Current;
            if (app == null) return;

            InputStarter.Start(app.GroupID, INPUT_TYPE.First, app.Aid);
        }
    }
}
