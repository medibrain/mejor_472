﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ZenkokuGakko
{
    public partial class ExportForm : Form
    {
        Insurer ins;
        int cym;
        string exportFolderPath;
        public ExportForm(Insurer ins, int cym)
        {
            this.ins = ins;
            this.cym = cym;
            exportFolderPath = ins.OutputPath;
            InitializeComponent();

            this.Text = ins.InsurerName + " " + cym.ToString("0000/00");
        }

        private void buttonSubmitAllCheck_Click(object sender, EventArgs e)
        {
            checkBoxSubmitRes.Checked = true;
            checkBoxSubmitRer.Checked = true;
            checkBoxSubmitHes.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButtonNohin.Checked)
            {
                if(!checkBoxSubmitRes.Checked && !checkBoxSubmitHes.Checked && !checkBoxSubmitHesExcel.Checked && !checkBoxSubmitRer.Checked)
                {
                    MessageBox.Show("出力するデータを選択してください。", "エラー");
                    return;
                }

                if(checkBoxSubmitHes.Checked && checkBoxSubmitHesExcel.Checked)
                {
                    MessageBox.Show("HES直接出力とHES_Excel変換を同時に選択することはできません。", "エラー");
                    return;
                }

                var esd = new ExportSubmitData(ins, exportFolderPath, cym,
                    checkBoxSubmitRes.Checked, checkBoxSubmitHes.Checked, checkBoxSubmitRer.Checked, checkBoxSubmitHesExcel.Checked);
                var result = esd.Exportes();

                if (result)
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
            }
            else if (radioButtonBunseki.Checked)
            {
                if (ExportRev.AnalysisExport(Insurer.CurrrentInsurer, cym))
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
                else
                {
                    MessageBox.Show("エラーが発生しました。", "失敗");
                }
            }
            else if (radioButtonMediMatching.Checked)
            {
                var export = new ExportRev();
                var l = App.GetApps(cym);

                if (export.MediMatchingExportWithRESAndImage(Insurer.CurrrentInsurer, l))
                {
                    MessageBox.Show("出力が完了しました。", "成功");
                }
                else
                {
                    MessageBox.Show("エラーが発生しました。", "失敗");
                }
            }
        }
    }
}
