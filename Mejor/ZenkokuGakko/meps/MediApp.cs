﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mejor.ZenkokuGakko
{
    public class MediApp
    {
        public enum MEDIAPP_TYPE { NULL = -1, 申請書 = 1, バッチ = 2, 続紙 = 3, 付箋 = 4, 不要 = 5, 再審査返戻 = 6 }
        public enum INPUT_STATUS { NULL = -1, スキャン済 = 0, 入力済 = 1 }

        /// <summary>
        /// ID [MediApp ID]
        /// </summary>
        public int MAID { get; set; }
        /// <summary>
        /// 申請書タイプ  [MediApp Type] Consts#MEDIAPP_TYPE参照
        /// </summary>
        public int MAType { get; set; }
        /// <summary>
        /// 支部番号 [Prefectures ID] Pref#PID
        /// </summary>
        public int PID { get; set; }
        /// <summary>
        /// 入力年月 [Input Year Month](yyyyMM)
        /// </summary>
        public int IYM { get; set; }
        /// <summary>
        /// バッチ [Batch]
        /// </summary>
        public string Batch { get; set; } = string.Empty;
        /// <summary>
        /// ナンバリング [Numbering]
        /// </summary>
        public int Numbering { get; set; } = -1;
        /// <summary>
        /// 診療年月 [Medical Year Month](yyyyMM)
        /// </summary>
        public int MYM { get; set; } = -1;
        /// <summary>
        /// 明細区分 [Meisai Type]
        /// </summary>
        public int MeisaiType { get; set; } = -1;
        /// <summary>
        /// 一般／老健 [Ippan Rouken]
        /// </summary>
        public int IppanRouken { get; set; } = -1;
        /// <summary>
        /// 社保／公費 [Syaho Kouhi]
        /// </summary>
        public int SyahoKouhi { get; set; } = -1;
        /// <summary>
        /// 本人／家族 [Honnin Kazoku]
        /// </summary>
        public int HonninKazoku { get; set; } = -1;
        /// <summary>
        /// 都道府県番号 [Prefectures Number]
        /// </summary>
        public string PrefNum { get; set; } = string.Empty;
        /// <summary>
        /// 医療機関コード [Hospital Code]
        /// </summary>
        public string HospitalCode { get; set; } = string.Empty;
        /// <summary>
        /// 公費（1）コード [Kouhi 1 Code]
        /// </summary>
        public string Kouhi1Code { get; set; } = string.Empty;
        /// <summary>
        /// 保険者番号 [Insurer Number]
        /// </summary>
        public string INum { get; set; } = string.Empty;
        /// <summary>
        /// 組合員番号 [Hi hokensya Number]
        /// </summary>
        public string HNum { get; set; } = string.Empty;
        /// <summary>
        /// 性別 [Sex]
        /// </summary>
        public int Sex { get; set; } = -1;
        /// <summary>
        /// 生年月日 [Birthday]
        /// </summary>
        public DateTime Birthday { get; set; } = DateTime.MinValue;
        /// <summary>
        /// 特記 [Tokki]
        /// </summary>
        public int Tokki { get; set; } = -1;
        /// <summary>
        /// 通知区分 [Tuuchi Type]
        /// </summary>
        public int TuuchiType { get; set; } = -1;
        /// <summary>
        /// 医保　日数 [Iho Days]
        /// </summary>
        public int IhoDays { get; set; } = -1;
        /// <summary>
        /// 医保　請求点数 [Iho Point]
        /// </summary>
        public int IhoPoint { get; set; } = -1;
        /// <summary>
        /// 医保　負担金額 [Iho Price]
        /// </summary>
        public int IhoPrice { get; set; } = -1;
        /// <summary>
        /// 公費（1）日数 [Kouhi 1 Days]
        /// </summary>
        public int Kouhi1Days { get; set; } = -1;
        /// <summary>
        /// 公費（1）請求点数 [Kouhi 1 Point]
        /// </summary>
        public int Kouhi1Point { get; set; } = -1;
        /// <summary>
        /// 公費（1）負担金額 [Kouhi 1 Price]
        /// </summary>
        public int Kouhi1Price { get; set; } = -1;
        /// <summary>
        /// 公費（2）日数 [Kouhi 2 Days]
        /// </summary>
        public int Kouhi2Days { get; set; } = -1;
        /// <summary>
        /// 公費（2）請求点数 [Kouhi 2 Point]
        /// </summary>
        public int Kouhi2Point { get; set; } = -1;
        /// <summary>
        /// 公費（2）負担金額 [Kouhi 2 Price]
        /// </summary>
        public int Kouhi2Price { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　日数 [Syokuji Iho Days]
        /// </summary>
        public int SyokujiIhoDays { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　療養金額 [Syokuji Medical Price]
        /// </summary>
        public int SyokujiIhoMediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　負担金額 [Syokuji Hutan Price]
        /// </summary>
        public int SyokujiIhoHutanPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　日数 [Syokuji Kouhi 1 Days]
        /// </summary>
        public int SyokujiKouhi1Days { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　療養金額 [Syokuji Kouhi 1 Medical Price]
        /// </summary>
        public int SyokujiKouhi1MediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　負担金額 [Syokuji Kouhi 1 Hutan Price]
        /// </summary>
        public int SyokujiKouhi1HutanPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　日数 [Syokuji Kouhi 2 Days]
        /// </summary>
        public int SyokujiKouhi2Days { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　療養金額 [Syokuji Kouhi 2 Medical Price]
        /// </summary>
        public int SyokujiKouhi2MediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　負担金額 [Syokuji Kouhi 2 Hutan Price]
        /// </summary>
        public int SyokujiKouhi2HutanPrice { get; set; } = -1;
        /// <summary>
        /// 高額療養費 [Kougaku Price]
        /// </summary>
        public int KougakuPrice { get; set; } = -1;
        /// <summary>
        /// 再審査再入力 [Again]
        /// </summary>
        public int Again { get; set; } = -1;
        /// <summary>
        /// 公費（2）コード [Kouhi 2 Code]
        /// </summary>
        public string Kouhi2Code { get; set; } = string.Empty;
        /// <summary>
        /// ファイル名（拡張子付）
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// １回目入力状況 [Input Status 1] Consts#INPUT_STATUS参照
        /// </summary>
        public int InputStatus1 { get; set; } = (int)INPUT_STATUS.スキャン済;
        /// <summary>
        /// １回目入力者 [Input User 1] User#UserID参照
        /// </summary>
        public int InputUser1 { get; set; } = 0;
        /// <summary>
        /// ２回目入力状況 [Input Status 2] Consts#INPUT_STATUS参照
        /// </summary>
        public int InputStatus2 { get; set; } = (int)INPUT_STATUS.スキャン済;
        /// <summary>
        /// ２回目入力者 [Input User 2] User#UserID参照
        /// </summary>
        public int InputUser2 { get; set; } = 0;
        /// <summary>
        /// DRG区分 [DRG Type]
        /// </summary>
        public int DrgType { get; set; } = -1;
        /// <summary>
        /// 【DRGレセのみ】診療年月日（自） [DRG Medical Date 1]
        /// </summary>
        public DateTime DrgMediDate1 { get; set; } = DateTime.MinValue;
        /// <summary>
        /// 【DRGレセのみ】診療年月日（至） [DRG Medical Date 2]
        /// </summary>
        public DateTime DrgMediDate2 { get; set; } = DateTime.MinValue;

        /// <summary>
        /// (Not In Table) １回目入力状況（表示用）
        /// </summary>
        public string InputStatusView1
        {
            get
            {
                if (InputStatus1 == (int)INPUT_STATUS.入力済)
                {
                    return "入力完了";
                }
                return "未入力";
            }
        }
        /// <summary>
        /// (Not In Table) ２回目入力状況（表示用）
        /// </summary>
        public string InputStatusView2
        {
            get
            {
                if (InputStatus2 == (int)INPUT_STATUS.入力済)
                {
                    return "ﾍﾞﾘﾌｧｲ済";
                }
                return "未入力";
            }
        }
        /// <summary>
        /// (Not In Table) 申請書タイプ（表示用）
        /// </summary>
        public string MediAppTypeView
        {
            get
            {
                if(Enum.IsDefined(typeof(MEDIAPP_TYPE), MAType))
                {
                    var matype= (MEDIAPP_TYPE)MAType;
                    if(matype == MEDIAPP_TYPE.NULL)
                    {
                        return "";
                    }
                    return matype.ToString();
                }
                return "不明";
            }
        }
        /// <summary>
        /// (Not In Table) ナンバリング（表示用）
        /// </summary>
        public string NumberingView
        {
            get
            {
                if (Numbering != -1)
                {
                    return Numbering.ToString("000000");
                }
                return "";
            }
        }

        /// <summary>
        /// 都道府県と審査年月から申請書一覧を取得します。（MAID昇順）
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="iYM"></param>
        /// <returns></returns>
        public static List<MediApp> GetMediAppsByPIDIYM(int IYM, int pid)
        {
            var dbname = "zenkoku_gakko";
            var currDBName = DB.GetMainDBName();
            var change = currDBName != dbname;
            try
            {
                if (change) DB.SetMainDBName(dbname);
                var sql = $"SELECT * FROM mediapp WHERE pid={pid} AND iym={IYM} ORDER BY maid;";
                var l = DB.Main.Query<MediApp>(sql);
                if (l == null)
                {
                    return null;
                }
                var maList = new List<MediApp>();
                l.ToList().ForEach(ma => maList.Add(ma));
                return maList;
            }
            finally
            {
                if (change) DB.SetMainDBName(currDBName);
            }
        }

    }
}
