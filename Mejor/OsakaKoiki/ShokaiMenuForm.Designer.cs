﻿namespace Mejor.OsakaKoiki
{
    partial class ShokaiMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonE = new System.Windows.Forms.Button();
            this.buttonF = new System.Windows.Forms.Button();
            this.buttonH = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonG = new System.Windows.Forms.Button();
            this.buttonB = new System.Windows.Forms.Button();
            this.buttonJigo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonC
            // 
            this.buttonC.Enabled = false;
            this.buttonC.Location = new System.Drawing.Point(38, 69);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(183, 44);
            this.buttonC.TabIndex = 0;
            this.buttonC.Text = "---";
            this.buttonC.UseVisualStyleBackColor = true;
            // 
            // buttonE
            // 
            this.buttonE.Enabled = false;
            this.buttonE.Location = new System.Drawing.Point(38, 119);
            this.buttonE.Name = "buttonE";
            this.buttonE.Size = new System.Drawing.Size(183, 44);
            this.buttonE.TabIndex = 1;
            this.buttonE.Text = "---";
            this.buttonE.UseVisualStyleBackColor = true;
            // 
            // buttonF
            // 
            this.buttonF.Enabled = false;
            this.buttonF.Location = new System.Drawing.Point(227, 119);
            this.buttonF.Name = "buttonF";
            this.buttonF.Size = new System.Drawing.Size(183, 44);
            this.buttonF.TabIndex = 4;
            this.buttonF.Text = "---";
            this.buttonF.UseVisualStyleBackColor = true;
            // 
            // buttonH
            // 
            this.buttonH.Enabled = false;
            this.buttonH.Location = new System.Drawing.Point(227, 169);
            this.buttonH.Name = "buttonH";
            this.buttonH.Size = new System.Drawing.Size(183, 44);
            this.buttonH.TabIndex = 5;
            this.buttonH.Text = "---";
            this.buttonH.UseVisualStyleBackColor = true;
            // 
            // buttonD
            // 
            this.buttonD.Enabled = false;
            this.buttonD.Location = new System.Drawing.Point(227, 69);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(183, 44);
            this.buttonD.TabIndex = 3;
            this.buttonD.Text = "---";
            this.buttonD.UseVisualStyleBackColor = true;
            // 
            // buttonG
            // 
            this.buttonG.Enabled = false;
            this.buttonG.Location = new System.Drawing.Point(38, 169);
            this.buttonG.Name = "buttonG";
            this.buttonG.Size = new System.Drawing.Size(183, 44);
            this.buttonG.TabIndex = 2;
            this.buttonG.Text = "---";
            this.buttonG.UseVisualStyleBackColor = true;
            // 
            // buttonB
            // 
            this.buttonB.Enabled = false;
            this.buttonB.Location = new System.Drawing.Point(227, 19);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(183, 44);
            this.buttonB.TabIndex = 7;
            this.buttonB.Text = "---";
            this.buttonB.UseVisualStyleBackColor = true;
            // 
            // buttonJigo
            // 
            this.buttonJigo.Location = new System.Drawing.Point(38, 19);
            this.buttonJigo.Name = "buttonJigo";
            this.buttonJigo.Size = new System.Drawing.Size(183, 44);
            this.buttonJigo.TabIndex = 6;
            this.buttonJigo.Text = "【事後】　未送付リスト作成用";
            this.buttonJigo.UseVisualStyleBackColor = true;
            this.buttonJigo.Click += new System.EventHandler(this.buttonJigo_Click);
            // 
            // ShokaiMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 231);
            this.Controls.Add(this.buttonB);
            this.Controls.Add(this.buttonJigo);
            this.Controls.Add(this.buttonH);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonF);
            this.Controls.Add(this.buttonG);
            this.Controls.Add(this.buttonE);
            this.Controls.Add(this.buttonC);
            this.Name = "ShokaiMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OptionsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonE;
        private System.Windows.Forms.Button buttonF;
        private System.Windows.Forms.Button buttonH;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonG;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Button buttonJigo;
    }
}