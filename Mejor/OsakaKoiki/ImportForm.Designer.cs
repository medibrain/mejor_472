﻿namespace Mejor.OsakaKoiki
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonDir = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtmdb = new System.Windows.Forms.TextBox();
            this.btnRef = new System.Windows.Forms.Button();
            this.btnJyuseiOnly = new System.Windows.Forms.Button();
            this.buttonClinicImp = new System.Windows.Forms.Button();
            this.btnRefAHK = new System.Windows.Forms.Button();
            this.txtAHK = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAHKOnly = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(238, 207);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "月分";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(158, 207);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "年";
            // 
            // textBoxM
            // 
            this.textBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM.Location = new System.Drawing.Point(195, 197);
            this.textBoxM.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(39, 23);
            this.textBoxM.TabIndex = 12;
            // 
            // textBoxY
            // 
            this.textBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY.Location = new System.Drawing.Point(115, 197);
            this.textBoxY.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(39, 23);
            this.textBoxY.TabIndex = 11;
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.Location = new System.Drawing.Point(552, 228);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(100, 29);
            this.buttonStart.TabIndex = 15;
            this.buttonStart.Text = "一括処理";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonDir
            // 
            this.buttonDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDir.Location = new System.Drawing.Point(721, 28);
            this.buttonDir.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDir.Name = "buttonDir";
            this.buttonDir.Size = new System.Drawing.Size(39, 24);
            this.buttonDir.TabIndex = 16;
            this.buttonDir.Text = "…";
            this.buttonDir.UseVisualStyleBackColor = true;
            this.buttonDir.Click += new System.EventHandler(this.buttonDir_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(29, 28);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(684, 22);
            this.textBox1.TabIndex = 17;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(660, 228);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 29);
            this.buttonCancel.TabIndex = 18;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 15);
            this.label1.TabIndex = 19;
            this.label1.Text = "対象フォルダ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 174);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 19;
            this.label5.Text = "処理年月";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 205);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 20;
            this.label2.Text = "平成/令和";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 55);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "提供データ柔整";
            // 
            // txtmdb
            // 
            this.txtmdb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmdb.Location = new System.Drawing.Point(29, 74);
            this.txtmdb.Margin = new System.Windows.Forms.Padding(4);
            this.txtmdb.Name = "txtmdb";
            this.txtmdb.Size = new System.Drawing.Size(684, 22);
            this.txtmdb.TabIndex = 22;
            // 
            // btnRef
            // 
            this.btnRef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRef.Location = new System.Drawing.Point(721, 74);
            this.btnRef.Margin = new System.Windows.Forms.Padding(4);
            this.btnRef.Name = "btnRef";
            this.btnRef.Size = new System.Drawing.Size(39, 24);
            this.btnRef.TabIndex = 21;
            this.btnRef.Text = "…";
            this.btnRef.UseVisualStyleBackColor = true;
            this.btnRef.Click += new System.EventHandler(this.btnRef_Click);
            // 
            // btnJyuseiOnly
            // 
            this.btnJyuseiOnly.Location = new System.Drawing.Point(495, 100);
            this.btnJyuseiOnly.Margin = new System.Windows.Forms.Padding(4);
            this.btnJyuseiOnly.Name = "btnJyuseiOnly";
            this.btnJyuseiOnly.Size = new System.Drawing.Size(265, 28);
            this.btnJyuseiOnly.TabIndex = 24;
            this.btnJyuseiOnly.Text = "柔整取込・マッチング処理";
            this.btnJyuseiOnly.UseVisualStyleBackColor = true;
            this.btnJyuseiOnly.Click += new System.EventHandler(this.buttonJyuseiOnly_Click);
            // 
            // buttonClinicImp
            // 
            this.buttonClinicImp.Location = new System.Drawing.Point(30, 236);
            this.buttonClinicImp.Name = "buttonClinicImp";
            this.buttonClinicImp.Size = new System.Drawing.Size(208, 54);
            this.buttonClinicImp.TabIndex = 28;
            this.buttonClinicImp.Text = "医療機関マスタ取込";
            this.buttonClinicImp.UseVisualStyleBackColor = true;
            this.buttonClinicImp.Click += new System.EventHandler(this.buttonClinicImp_Click);
            // 
            // btnRefAHK
            // 
            this.btnRefAHK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefAHK.Location = new System.Drawing.Point(721, 136);
            this.btnRefAHK.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefAHK.Name = "btnRefAHK";
            this.btnRefAHK.Size = new System.Drawing.Size(39, 24);
            this.btnRefAHK.TabIndex = 21;
            this.btnRefAHK.Text = "…";
            this.btnRefAHK.UseVisualStyleBackColor = true;
            this.btnRefAHK.Click += new System.EventHandler(this.btnRefAHK_Click);
            // 
            // txtAHK
            // 
            this.txtAHK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAHK.Location = new System.Drawing.Point(29, 136);
            this.txtAHK.Margin = new System.Windows.Forms.Padding(4);
            this.txtAHK.Name = "txtAHK";
            this.txtAHK.Size = new System.Drawing.Size(684, 22);
            this.txtAHK.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 117);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 15);
            this.label7.TabIndex = 23;
            this.label7.Text = "提供データあはき";
            // 
            // btnAHKOnly
            // 
            this.btnAHKOnly.Location = new System.Drawing.Point(495, 166);
            this.btnAHKOnly.Margin = new System.Windows.Forms.Padding(4);
            this.btnAHKOnly.Name = "btnAHKOnly";
            this.btnAHKOnly.Size = new System.Drawing.Size(265, 28);
            this.btnAHKOnly.TabIndex = 24;
            this.btnAHKOnly.Text = "あはき取込・マッチング処理";
            this.btnAHKOnly.UseVisualStyleBackColor = true;
            this.btnAHKOnly.Click += new System.EventHandler(this.btnAHKOnly_Click);
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 309);
            this.Controls.Add(this.buttonClinicImp);
            this.Controls.Add(this.btnAHKOnly);
            this.Controls.Add(this.btnJyuseiOnly);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAHK);
            this.Controls.Add(this.txtmdb);
            this.Controls.Add(this.btnRefAHK);
            this.Controls.Add(this.btnRef);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonDir);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxM);
            this.Controls.Add(this.textBoxY);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "ImportForm";
            this.Text = "大阪広域申請書データインポート";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonDir;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtmdb;
        private System.Windows.Forms.Button btnRef;
        private System.Windows.Forms.Button btnJyuseiOnly;
        private System.Windows.Forms.Button buttonClinicImp;
        private System.Windows.Forms.Button btnRefAHK;
        private System.Windows.Forms.TextBox txtAHK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAHKOnly;
    }
}